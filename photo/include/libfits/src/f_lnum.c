/*                                                          f_lnum
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_lnum(header, keyword)
                char *header[], *keyword;

Purpose:        Gets the line number of the header line with
                keyword (zero-reference)

Inputs:         'header' is the file header
                'keyword' is the FITS keyword requested

Returns:        The line number on success,
                -1 on failure

Notes:          The mechanism for marking headers: A non-space is
                in character header[n][81] for the line at which
                to start the search. The search continues until
                this element is found again. header[n][80] has
                the end-of-string in it.

History:        1986 Aug 6, Alan Uomoto, LSU Physics & Astronomy
                1986 Aug 16, fixed it

------------------------------------------------------------------

*/

#include <string.h>
#include "libfits.h"

int f_lnum(char **header, char *keyword)
{

        char    key[9];         /* place to put the keyword             */
        register int
                i, j,           /* loop index, temporary variable       */
                hstart,         /* header line to start searching from  */
                nlines;         /* number of header lines               */
        extern int
                f_debug(int flag),      /* error messages on or off             */
                f_smatch(register char *line, register char *keyword);     /* short match                          */

        if (strlen(keyword) > 8) {
                if (f_debug(QUERY)) {
                        fprintf(stderr,
                                "f_lnum: keyword too long: '%s'\n", keyword);
                        return(-1);
                }
        }
                                /* build a blank padded keyword         */
        (void) strcpy(key, keyword);
        for (i = strlen(key); i < 8; i++) {
                key[i] = ' ';
        }
        key[8] = '\0';

                        /* count the header lines       */
        for (nlines = 0; header[nlines] != NULL; nlines++)
                ;

                        /* search for starting line     */
        hstart = 0;
        for (i = 0; header[i] != NULL; i++) {
                if (header[i][81] != ' ') {
                        hstart = i;
                        break;
                }
        }

        j = hstart;
        for (i = 0; i < nlines; i++) {
                j = (i + hstart) % nlines;
                if (f_smatch(header[j], key)) {
                        header[(header[j+1]==NULL) ? 0 : j+1][81] = '.';
                        header[hstart][81] = ' ';
                        return(j);
                }
        }

        if (f_debug(QUERY)) {
                fprintf(stderr,
                        "f_lnum: '%s' not in header\n", keyword);
        }
        return(-1);

}

#ifdef DEBUG

#define HEADERSIZE (36 * 2)

main(argc, argv)        /* passed this test 1986 August 15      */
int argc;
char *argv[];

{

        char *header[HEADERSIZE], keyword[80];
        int i;
        FILE *fp;
        extern FILE *f_rdfits();
        extern void f_init();
        extern int f_debug();

        if (argc != 2) {
                printf("usage: f_lnum filename\n");
                exit(1);
        }
        f_setup();
        f_debug(TRUE);

        if ((fp = f_rdfits(argv[1], header, HEADERSIZE)) == NULL) {
                printf("f_lnum: Can't open input\n");
                exit(1);
        }
        for (i = 0; header[i] != NULL; i++) {
                printf("%2d: %s\n", i, header[i]);
        }
        for (;;) {
                printf("keyword: ");
                if (scanf("%s", keyword) != 1) {
                        exit(1);
                }
                else {
                        printf("%d\n", f_lnum(header, keyword));
                }
        }
}

#endif
