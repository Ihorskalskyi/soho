/*                                                         f_get32
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_get32(data, count, fp)
                int32 *data;
                int count;
                FILE *fp;

Purpose:        Reads 32 bit data from the stream fp

Inputs:         'data' is a pointer to at least count 32 bit words
                'count' is the number of 32 bit words to get
                'fp' is the file stream pointer

Returns:        TRUE on success, with data in 'data'
                FALSE on failure

Example:        #include "libfits.h"
                int32 data[2000];
                if (! f_get32(data, 2000, fp)) {
                        printf("can't get data");
                }

Notes:          Fastest with VMS fixed record format files, about
                2.5 times slower with VMS stream files
                Not tested

History:        1986 July 16, Alan Uomoto, LSU Physics & Astronomy
                1986 November 14, Added f_BYTESWAP, f_WORDSWAP.

------------------------------------------------------------------

*/

#include "libfits.h"

int f_get32(register int *data, int count, FILE *fp)
{

        register int32  *dp;    /* temporary data pointer               */
        register int    i,      /* loop index                           */
                        n;      /* temporary variable                   */
        int     nleft,          /* number of 32 bit words left in record*/
                nrec,           /* number of new records to read        */
                findex;         /* the file index                       */
        extern int
                f_debug(int flag),      /* debug messages on or off             */
                f_findex(FILE *fp),     /* gets file index from stream pointer  */
                f_rfr(char *bp, FILE *fp);        /* read fits record                     */
        extern void f_bswap4(register int *buffer); /* byte swap routine                    */
        extern struct f_finfo f_files;  /* global file info             */
        extern int f_BYTESWAP, f_WORDSWAP;

        findex = f_findex(fp);
        nleft = (2880 - f_files.nused[findex]) / 4;
        if (nleft > 0) {
                dp = (int32 *) (f_files.bp[findex] + f_files.nused[findex]);
                n = (count < nleft) ? count : nleft;
                for (i = 0; i < n; i++) {
                        *data++ = *dp++;
                }
                f_files.nused[findex] += n * 4;
                count -= n;
        }
        nrec = count / 720;
        for (i = 0; i < nrec; i++) {
                if (! f_rfr((char *) data, fp)) {
                        if (f_debug(QUERY)) {
                                fprintf(stderr,
                                        "f_get32: error reading file\n");
                        }
                        return(FALSE);
                }
                if (f_BYTESWAP && f_WORDSWAP) {
                        f_bswap4(data);
                }
                data += 720;
                count -= 720;
        }
        if (count > 0) {
                if (! f_rfr((char *) f_files.bp[findex], fp)) {
                        if (f_debug(QUERY)) {
                                fprintf(stderr,
                                        "f_get32: error reading file\n");
                        }
                        return(FALSE);
                }
                if (f_BYTESWAP && f_WORDSWAP) {
                        f_bswap4((int32 *) f_files.bp[findex]);
                }
                n = count;
                dp = (int32 *) f_files.bp[findex];
                for (i = 0; i < n; i++) {
                        *data++ = *dp++;
                }
                f_files.nused[findex] = n * 4;
        }
        return(TRUE);
}
