/*                                                          f_akey
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_akey(aval, header, keyword)
                char *aval;
                char *header[], *keyword;

Purpose:        Get an alphanumeric keyword string from a FITS
                header

Inputs:         'header' is the FITS file header
                'keyword' is the keyword to look for

Returns:        TRUE on success, with 'aval' pointing to the
                string

                FALSE on failure

Example:        if (! f_akey(aval, header, "DATE"))
                        printf("no date keyword");

                else
                        printf("Date = %s\n", aval);

History:        1986 July 7, Alan Uomoto, LSU Physics & Astronomy

------------------------------------------------------------------

*/

#include <stdio.h>
#include "libfits.h"
static void emess(char *line, char *keyword)        /* error messages       */
                     

{

        if (f_debug(-1)) {
                fprintf(stderr, "f_akey: non-alpha value for '%s':\n",
                        keyword);
                printf("%s\n", line);
        }

}

int f_akey(char *aval, char **header, char *keyword)
{

        char    line[81],       /* place to put the header line */
                temp[81];       /* place to put the alpha string*/
        register int i, j;      /* loop indices                 */
        void    emess();        /* prints error messages        */
        extern  char *strcpy(char *, const char *); /* standard library routine     */
        extern  int f_getlin(char *line, char **header, char *keyword); /* copies header lines          */

        if (! f_getlin(line, header, keyword)) {
                return(FALSE);
        }

        for (i = 9; i < 80; i++) {      /* clear leading blanks */
                if (line[i] != ' ')
                        break;
        }
                                        /* validate alpha string*/
        if (line[i] != '\'') {          /* leading tick (')     */
                emess(line, keyword);
                return(FALSE);
        }

        i++;

        for (j = 0; line[i] != '\''; j++) {
                if (line[i] == '\0') {          /* EOS before 2nd (')   */
                        emess(line, keyword);
                        return(FALSE);
                }
                temp[j] = line[i++];
                if (i == 80) {                  /* no 2nd (')   */
                        emess(line, keyword);
                        return(FALSE);
                }
        }

        temp[j] = '\0';
        (void) strcpy(aval, temp);
        return(TRUE);

}


#ifdef DEBUG

#define HEADERSIZE      (36 * 2)

main(argc, argv)        /* passed this test 1986 August 16 */
int argc;
char *argv[];

{

        char *header[HEADERSIZE], keyword[80], string[80];
        int i;
        FILE *fp, *f_rdfits();

        if (argc != 2) {
                printf("usage: f_akey filename\n");
                exit();
        }

        f_setup();
        f_debug(TRUE);

        if ((fp = f_rdfits(argv[1], header, HEADERSIZE)) == NULL) {
                printf("Can't open %s\n", argv[1]);
                exit(1);
        }

        for (i = 0; header[i] != NULL; i++) {
                printf("%s\n", header[i]);
        }

        for (;;) {
                printf("keyword: ");
                if (scanf("%s", keyword) != 1) {
                        exit(0);
                }
                if (f_akey(string, header, keyword)) {
                        printf("%s\n", string);
                }
                else {
                        printf("%s not found\n", keyword);
                }
        }
}

#endif
