/*                                                        f_smatch
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_smatch(long_str, short_str)
                char *long_str, *short_str;

Purpose:        Compares short_str against long_str for a match

Inputs:         long_str and shortstr contain strings to compare.
                shortstr must be '\0' terminated.

Returns:        TRUE on short match,
                FALSE if not

History:        1986 July 6, Alan Uomoto, LSU Physics & Astronomy

------------------------------------------------------------------

*/

#include <string.h>
#include "libfits.h"

int f_smatch(register char *line, register char *keyword)
{

        register int i, len;

        len = strlen(keyword);
        for (i = 0; i < len; i++) {
                if (*line++ != *keyword++) {
                        return(FALSE);
                }
        }

        return(TRUE);

}
