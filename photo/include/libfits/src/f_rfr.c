/*                                                           f_rfr
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_rfr(bp, fp);
                char *bp;
                FILE *fp;

Purpose:        Read a 2880 byte record from a FITS file

Inputs:         'fp' is the file stream to read from

Returns:        TRUE (1) on success, and 2880 bytes in bp
                FALSE (0) on failure

History:        1986 July 10, Alan Uomoto, LSU Physics & Astronomy

------------------------------------------------------------------

*/

#include "libfits.h"

int f_rfr(char *bp, FILE *fp)
{

#ifdef VAXC
	size_t fread();
#endif

        return( fread(bp, RECORDSIZE, 1, fp) );

}

#ifdef DEBUG

main(argc, argv)
int argc;
char *argv[];

{

        char buf[2880];
        int i, f_rfr();
        FILE *fp, *f_fopen();

        if (argc != 2) {
                printf("usage: f_rfr filename\n");
                exit(1);
        }

        if ((fp = fopen(argv[1], "r")) == NULL) {
                printf("Can't open '%s'", argv[1]);
                exit(1);
        }

        printf("reading records...");
        fflush(stdout);
        for (i = 0; f_rfr(buf, fp); i++)
                ;
        printf("%d records\n", i);

}

#endif
