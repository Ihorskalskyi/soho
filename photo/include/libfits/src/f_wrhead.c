/*                                                        f_wrhead
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_wrhead(header, fp)
                char *header[];
                FILE *fp;

Purpose:        Writes the header

Inputs:         'header' is the header to write
                'fp' is the file pointer to write to

Notes:          does not rewind fp first

Returns:        TRUE on success,
                FALSE on failure

History:        1986 July 9, Alan Uomoto, LSU Physics & Astronomy
                1986 Aug 21, removed rewind first, aku

------------------------------------------------------------------

*/

#include <string.h>
#include "libfits.h"

int f_wrhead(char **header, FILE *fp)
{

        register char
                *bp;            /* buffer pointer                       */
        char    buf[2880];      /* i/o buffer                           */
        register int
                j;              /* loop index                           */
        int     nused,          /* number of buffer elements used       */
                len,            /* length of a particular header line   */
                i;              /* loop index                           */
        extern int f_wfr(char *buf, FILE *fp);     /* write FITS record                    */

        bp = buf;
        nused = 0;
        for (i = 0; header[i] != NULL; i++) {
                len = strlen(header[i]);
                for (j = 0; j < len; j++) {
                        *bp++ = header[i][j];
                }
                for (j = len; j < 80; j++) {
                        *bp++ = ' ';
                }
                nused += 80;
                if (nused == 2880) {
                        if (! f_wfr(buf, fp)) {
                                return(FALSE);
                        }
                        bp = buf;
                        nused = 0;
                }
        }
        if (nused > 0) {
                for (j = nused; j < 2880; j++) {
                        *bp++ = ' ';
                }
                if (! f_wfr(buf, fp)) {
                        return(FALSE);
                }
        }

        return(TRUE);

}

#ifdef DEBUG

#define HEADERLINES 145

main(argc, argv)        /* passed this test 1986 August 16      */
int argc;
char *argv[];

{

        char *header[HEADERLINES];
        int i;
        FILE *fp, *fpout, *f_fopen(), *f_rdfits();
        long lt, lt1, lt2, time();

        f_setup();
        f_debug(TRUE);
        if (argc != 3) {
                printf("usage: f_wrhead input output\n");
                exit(1);
        }
        for (i = 0; i < HEADERLINES; i++) {
                header[i] = NULL;
        }

        if ((fp = f_rdfits(argv[1], header, HEADERLINES)) == NULL) {
                printf("can't deal with %s\n", argv[1]);
                exit(1);
        }

        if ((fpout = f_fopen(argv[2], "w")) == NULL) {
                printf("can't open output %s\n", argv[2]);
                exit(1);
        }
        if (! f_wrhead(header, fpout)) {
                printf("didn't write header\n");
                exit(1);
        }
}

#endif  /* DEBUG */
