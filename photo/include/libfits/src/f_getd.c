/*                                                          f_getd
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_getd(data, npts, header, fp)
                double *data;
                int npts;
                char *header[];
                FILE *fp;

Purpose:        Gets double data from a fits file

Inputs:         'data' points to at least npts doubles
                'npts' is the number of points to transfer
                'header' is the FITS header
                'fp' is the file pointer to read from

Returns:        TRUE on success, with unscaled data in data
                FALSE on failure

Notes:          Assumes that doubles are at least as big as int32,
                Does not apply bzero and bscale to the data

History:        1986 Aug 7, Alan Uomoto, LSU Physics and Astronomy

------------------------------------------------------------------

*/

#include "libfits.h"

/* 	Forward references */
static int get16(double *data, int npts, FILE* fp);
static int get32(double *data, int npts, FILE *fp);
static int get8 (double *data, int npts, FILE *fp);
static int getbp(char *header[]);


int f_getd(double *data, int npts, char **header, FILE *fp)
{

        int     getbp(),        /* returns bitpix from header   */
                get8(),         /* gets 8 bit double data       */
                get16(),        /* gets 16 bit double data      */
                get32();        /* gets 32 bit double data      */

        switch (getbp(header)) {

                case 8:   return(get8(data, npts, fp));

                case 16:  return(get16(data, npts, fp));

                case 32:  return(get32(data, npts, fp));

                default:  if (f_debug(QUERY)) {
                                fprintf(stderr,
                                        "f_getd: bad BITPIX value\n");
                          }
                          return(FALSE);
        }

}

static int get16(double *data, int npts, FILE *fp)
{

        register int i;
        register int16 *dptr;
        register double *fptr;
        extern int f_get16(register short int *data, int count, FILE *fp), f_debug(int flag);

        if (! f_get16((int16 *)data, npts, fp)) {
                if (f_debug(-1)) {
                        printf("f_getd: Can't get 16 bit data\n");
                }
                return(FALSE);
        }
        dptr = (int16 *) data + npts - 1;
        fptr = data + npts - 1;
        for (i = 0; i < npts; i++) {
                *fptr-- = (double) *dptr--;
        }
        return(TRUE);
}

static int get32(double *data, int npts, FILE *fp)
{

        register int i;
        register int32 *dptr;
        register double *fptr;
        extern int f_get32(register int *data, int count, FILE *fp), f_debug(int flag);

        if (! f_get32((int32 *)data, npts, fp)) {
                if (f_debug(-1)) {
                        printf("f_getd: Can't get 32 bit data\n");
                }
                return(FALSE);
        }
        dptr = (int32 *) data + npts - 1;
        fptr = data + npts - 1;
        for (i = 0; i < npts; i++) {
                *fptr-- = (double) *dptr--;
        }

        return(TRUE);

}

static int get8(double *data, int npts, FILE *fp)
{

        register int i;
        register int8 *dptr;
        register double *fptr;
        extern int f_get8(register unsigned char *data, int count, FILE *fp), f_debug(int flag);

        if (! f_get8((int8 *)data, npts, fp)) {
                if (f_debug(-1)) {
                        printf("f_getd: Can't get 8 bit data\n");
                }
                return(FALSE);
        }
        dptr = (int8 *) data + npts - 1;
        fptr = data + npts - 1;
        for (i = 0; i < npts; i++) {
                *fptr-- = *dptr--;
        }

        return(TRUE);

}

static int getbp(char **header)
{

        int bitpix;
        extern int f_ikey(int *ival, char **header, char *keyword), f_debug(int flag);

        if (! f_ikey(&bitpix, header, "BITPIX")) {
                if (f_debug(-1)) {
                        printf("f_getd: Can't find BITPIX\n");
                }
                return(0);
        }
        return(bitpix);
}

#ifdef DEBUG    /* passed this test for 16 and 32-bits, August 7, 1986 */

#define HEADERSIZE (36 * 2)

main(argc, argv)
int argc;
char *argv[];

{

        int i;
        FILE *fp, *f_rdfits();
        char *header[HEADERSIZE];
        double fdata[100];

        if (argc != 2) {
                printf("usage: f_getd filename\n");
                exit(1);
        }

        f_setup();
        f_debug(TRUE);

        fp = f_rdfits(argv[1], header, HEADERSIZE);
        if (fp == NULL) {
                printf("can't open '%s'\n", argv[1]);
                exit(1);
        }
        if (! f_getd(fdata, 100, header, fp)) {
                printf("error getting data\n");
                exit(1);
        }
        else {
                for (i = 1; i <= 100; i++) {
                        printf((i % 4) ? "%18.9E" : "%18.9E\n", fdata[i-1]);
                }
        }
}

#endif
