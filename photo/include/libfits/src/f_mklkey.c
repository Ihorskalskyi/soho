/*                                                        f_mklkey
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                char *f_mklkey(string, key, value, comment)
                char *string, *key, *value, *comment;

Purpose:        Builds a logical FITS header line

Inputs:         'string' - place to put the new line
                'key'    - the keyword
                'value'  - 1 for T, 0 for F
                'comment'- the header line comment (may be null)

Returns:        pointer to string

History:        1986 Dec 9, Alan Uomoto, JHU Physics & Astronomy

------------------------------------------------------------------

*/

#include <string.h>
#include "libfits.h"

char	*f_mklkey(char *ostring, char *key, int value, char *comment)
{
	char c;
	int		i;		/* loop index                   */
	int		ll;		/* strlen			*/
	register int	oo;		/* output index			*/
extern  int		f_debug(int flag);	/* checks LIBFITS debug status  */

    ll = strlen( key );
    if (ll > 8)
    {   if (f_debug(QUERY))
	{   fprintf( stderr, "f_mklkey: keyword too long [%s]\n", key );
	}
	key[8] = '\0';
	ll = 8;
    }

    oo = 0;
    for (i=0; i<ll; ) ostring[oo++] = key[i++]; /* copy key */
    for (   ; oo<8; ) ostring[oo++] = ' ';	/* pad if need be */
    ostring[oo++] = '=';
    for (   ; oo<29; ) ostring[oo++] = ' ';	/* pad to logical value spot */
    ostring[oo++] = value ? 'T' : 'F';

    if (comment[0] != 0)
    {   ostring[oo++] = ' ';
	ostring[oo++] = '/';
	ostring[oo++] = ' ';

	for (i=0; (i<37) && (comment[i]!='\0'); )	/* limit copy */
	{   ostring[oo++] = comment[i++];	/* copy comment */
	}
    }

    while (oo<80) ostring[oo++] = ' ';
    ostring[oo++] = '\0';

    return(ostring);
}

#ifdef DEBUG

main(argc, argv)
int argc;
char *argv[];

{

        char str[82], *f_mklkey();

        if (argc != 4) {
                printf("usage: mklkey key value comment\n");
                exit(1);
        }

        printf("%s\n", f_mklkey(str, argv[1], atoi(argv[2]), argv[3]));

}

#endif
