/*                                                          f_date
------------------------------------------------------------------

Synopsis:       #include <sys/time.h>
                char *f_date(datestr)
                char *datestr;

Purpose:        Gets the current date in FITS format

Inputs:         None

Returns:        Pointer to date string and the date string in
                'datestr'

Example:        char datestr[9], *f_date();
                printf("date is %s", f_date(datestr));

Notes:          System dependent, this works with VMS and UNIX

History:        1986 July 7, Alan Uomoto, LSU Physics & Astronomy

------------------------------------------------------------------

*/

#include "libfits.h"

char *f_date(char *datestr)
{

        long    bintim;         /* place to put the system time */
        struct tm *ltp,         /* place to hold converted time */
                *localtime(const time_t *);   /* system conversion routine    */

        (void) time(&bintim);
        ltp = localtime(&bintim);

        (void) sprintf(datestr, "%02d/%02d/%02d",
                ltp->tm_mday, ltp->tm_mon + 1, ltp->tm_year);

        return(datestr);

}

#ifdef DEBUG

main()

{

        char string[9], *f_date();

        printf("%s\n", f_date(string));

}

#endif
