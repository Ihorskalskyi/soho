/*                                                        f_wrfits
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                FILE *f_wrfits(name, header)
                char *name, *header[];

Purpose:        Opens a fits file for writing and writes the
                header

Inputs:         'name' is a pointer to a file name

Outputs:        'header' array filled with the header

Returns:        The file pointer on success,
                NULL on failure.

History:        1986 July 6, Alan Uomoto, LSU Physics & Astronomy

------------------------------------------------------------------

*/

#include "libfits.h"

FILE *f_wrfits(char *name, char **header)
{

        FILE    *fp;                    /* temporary file pointer       */
        extern int
                f_debug(int flag),              /* error messages flag          */
                f_save(FILE *fp, char **header, char *mode);               /* saves file info              */
        extern FILE *f_fopen(char *name, char *mode);         /* opens data files             */

        if ((fp = f_fopen(name, "w")) == NULL) {
                if (f_debug(QUERY))
                        fprintf(stderr,
                                "f_wrfits: Can't open file '%s'\n", name);
                return(NULL);
        }

        if (! f_save(fp, header, "w"))
                return(NULL);

        if (f_wrhead(header, fp) == FALSE)
                return(NULL);

        return(fp);

}

#ifdef DEBUG                    /* test program */

#define HEADERLINES (36 * 2)

main(argc, argv)
int argc;
char *argv[];

{

        char *header[HEADERLINES];
        int i;
        FILE *fp, *fpout, *f_fopen(), *f_rdfits();
        long lt, lt1, lt2, time();

        if (argc != 3) {
                printf("usage: f_wrfits input output\n");
                exit(1);
        }

        f_setup();
        f_debug(TRUE);

        if ((fp = f_rdfits(argv[1], header, HEADERLINES)) == NULL) {
                printf("can't deal with %s\n", argv[1]);
                exit(1);
        }

        if ((fpout = f_wrfits(argv[2], header)) == NULL) {
                printf("can't open output file %s", argv[2]);
                exit(1);
        }
}

#endif
