/*                                                         f_hinfo
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_hinfo(header,hdupos,hdutype,dataareasize,
                            rsasize,heapoff,heapsize)
                char *header[];
                unsigned int hdupos;
                FITSHDUTYPE *hdutype;
                unsigned long int *dataareasize;
                unsigned long int *rsasize;
                unsigned long int *heapoff;
                unsigned long int *heapsize;

Purpose:        Return information about the header data unit (HDU), such as
                its type and the size (in bytes) of the data area.  Header
                validity checking is performed (only the mandatory keywords
                are verified).

                The following HDU types are handled:

                    Primary HDU
                    Random Groups
                    TABLE extensions
                    BINTABLE extensions

                Special records are NOT handled by this routine.

Inputs:         'header' is the file header
                'hdupos' is the 1-indexed (ordinal) position of
                         the HDU in the FITS file.
                'hdutype' is the type of HDU the header describes.
                'dataareasize' is the data area size (bytes) including padding
                'rsasize' is the record storage area size (bytes). This is the
                          main data area less padding.
                'heapoff' is the byte offset to heap from the start of the HDU
                          data area, if applicable (such as for Binary Tables).
                'heapsize' is the heap size (bytes), if applicable.

Returns:        TRUE on success,
                FALSE on failure (invalid FITS header).

History:        1993 December 6, Tom Nicinski, Fermilab

------------------------------------------------------------------

*/

#include "libfits.h"

#define reterrnomsg                                                                                                  return(FALSE);
#define	reterr(m1)           {if (f_debug(QUERY)){fprintf(stderr,"f_hinfo: " #m1); }                                 return(FALSE);}
#define reterrarg(m1,a1)     {if (f_debug(QUERY)){fprintf(stderr,"f_hinfo: " #m1, a1); }                             return(FALSE);}
#define reterrarg2(m1,a1,a2) {if (f_debug(QUERY)){fprintf(stderr,"f_hinfo: " #m1, a1, a2); }                         return(FALSE);}
#define reterr2(m1, m2)      {if (f_debug(QUERY)){fprintf(stderr,"f_hinfo: " #m1);fprintf(stderr,"f_hinfo: " #m2); } return(FALSE);}

/******************************************************************************/

/******************************************************************************/

static int keyword_addint(char **header, char *keyword, unsigned long int *sum)
{
        int retval;
        int intval;

        if (retval = f_ikey(&intval, header, keyword)) {
                *sum += intval;
        }
        return(retval);
}

static int keyword_multint(char **header, char *keyword, unsigned long int *product)
{
        int retval;
        int intval;

        if (retval = f_ikey(&intval, header, keyword)) {
                *product *= intval;
        }
        return(retval);
}

static int keyword_multabsint(char **header, char *keyword, unsigned long int *product)
{
        int retval;
        int intval;

        if (retval = f_ikey(&intval, header, keyword)) {
                *product *= (intval < 0) ? -intval : intval;
        }
        return(retval);
}

static int keyword_product(char **header, char *keypfx, int keybeg, int keyend, unsigned long int *product)
{

        char     keyword[11+1];	/* Extra space for protection (only need 8+1) */
        int      keyidx;
        int      intval;
        int      prod = 1;

        for (keyidx = keybeg; keyidx <= keyend; keyidx++) {
                sprintf(keyword, "%s%d", keypfx, keyidx);
                if (! f_ikey(&intval, header, keyword)) { reterrarg ("%s keyword is missing or invalid\n", keyword); }
                prod *= intval;
        }
        *product = (keybeg <= keyend) ? prod : 0;
        return(TRUE);
}

static int bitstobytes(int bits)	/* Returns integral bytes */
         

{
        return((bits + 7) / 8);
}

static int rndbitstorec(int bits)	/* Returns bytes */
         

{
        return(((bitstobytes(bits) + (RECORDSIZE - 1)) / RECORDSIZE) * RECORDSIZE);
}

static int xtensionsize(char **header, unsigned long int *dataareasize, unsigned long int *rsasize)
               
                   		/* Size of data area (with padding)           */
              			/* Size of record storage area (w/o padding)  */

{
        int     keycnt;

        *dataareasize  = 0;		/* For error condition                */
        *rsasize       = 0;
                keycnt = 0;
        f_ikey(&keycnt, header, "NAXIS");
        if (! keyword_product   (header, "NAXIS", 1, keycnt, dataareasize)) {
                reterrnomsg;
        }
        *rsasize = *dataareasize;
        if (! keyword_multabsint(header, "BITPIX", rsasize)) {
                *rsasize = 0;		/* Return consistent value for error  */
                reterr ("BITPIX keyword missing or invalid\n");
        }
        *rsasize = bitstobytes(*rsasize);
        if (! keyword_addint    (header, "PCOUNT", dataareasize)) {
                reterr ("PCOUNT keyword missing or invalid\n");
        }
        if (! keyword_multint   (header, "GCOUNT", dataareasize)) {
                reterr ("GCOUNT keyword missing or invalid\n");
        }
        if (! keyword_multabsint(header, "BITPIX", dataareasize)) {
                reterr ("BITPIX keyword missing or invalid\n");
        }
        *dataareasize = rndbitstorec(*dataareasize);
        return(TRUE);
}

/******************************************************************************/

/******************************************************************************/

int f_hinfo(char **header, unsigned int hdupos, FITSHDUTYPE *hdutype, long unsigned int *dataareasize, long unsigned int *rsasize, long unsigned int *heapoff, long unsigned int *heapsize)
{

FITSHDUTYPE	 htype;		/* header type                          */
register int     hline;         /* header line                          */
         int     keycnt;
         int     keyidx;
         char    keyword[11+1];	/* Extra space for protection (only need 8+1) */
         int     logval;	/* logical value                        */
         int     intval;	/* integer value                        */
         int     f_smatch(register char *line, register char *keyword);    /* short string match                   */
         int     f_debug(int flag);     /* error messages on or off             */

/*
 * Indicate which keywords are mandatory for the different header types.
 *
 * NOTE:  The initialization of this structure is hardwired.  Make sure that the
 *        order of header types matches that in the definition of FITSHDUTYPE in
 *        libfits.h.
 *
 * NOTE:  The declaration of error messages MUST be as an array rather than a
 *        pointer to char.  This is necessary to allow the initialization of
 *        hdrReqd to be accepted by the compiler.
 */

static  char    badUnknown [] = "Invalid FITS file (unknown HDU type)\n";
static  char    badPrimary [] = "Invalid FITS file (Primary HDU)\n";
static  char    badGroups  [] = "Invalid FITS file (Random Groups)\n";
static  char    badTABLE   [] = "Invalid FITS file (TABLE extension)\n";
static  char    badBINTABLE[] = "Invalid FITS file (BINTABLE extension)\n";
static  char    badIMAGE[]    = "Invalid FITS file (IMAGE extension)\n";

#define hdrReqdMaxLine 10		/* Max possible mandatory header lines*/

static  struct {
                struct {
                        char    *match;		/* Line header must match     */
                        char	*errMsg[2];	/* Mismatch error messages    */
                }  line[hdrReqdMaxLine + 1];	/* + 1 space for null term    */
        }               hdrReqd[f_hduCnt] = {
        /*
         * Unknown header type.
         */
        { { ((char *)0) }
        },
        /*
         * Primary header.
         *              1         2         3         4         5         6
         *     123456789 123456789 123456789 123456789 123456789 123456789 12345
         */
        { { { "SIMPLE  = ",                     { ((char *)0), ((char *)0)                                               } },
            { "BITPIX  = ",                     { badPrimary,  "BITPIX keyword missing in Primary header"                } },
            { "NAXIS   = ",                     { badPrimary,  "NAXIS keyword missing in Primary header"                 } },
            { ((char *)0) }
        } },
        /*
         * Random Groups.
         *              1         2         3         4         5         6
         *     123456789 123456789 123456789 123456789 123456789 123456789 12345
         */
        { { { "SIMPLE  = ",                     { ((char *)0), ((char *)0)                                               } },
            { "BITPIX  = ",                     { badGroups,   "BITPIX keyword missing in Random Groups"                 } },
            { "NAXIS   = ",                     { badGroups,   "NAXIS keyword missing in Random Groups"                  } },
            { "NAXIS1  =                    0", { badGroups,   "NAXIS1 keyword missing or invalid in Random Groups"      } },
            { ((char *)0) }
        } },
        /*
         * TABLE extension.
         *              1         2         3         4         5         6
         *     123456789 123456789 123456789 123456789 123456789 123456789 12345
         */
        { { { "XTENSION= 'TABLE   '",           { ((char *)0), ((char *)0)                                               } },
            { "BITPIX  =                    8", { badTABLE,    "BITPIX keyword missing or invalid in TABLE extension"    } },
            { "NAXIS   =                    2", { badTABLE,    "NAXIS keyword missing or invalid in TABLE extension"     } },
            { "NAXIS1  = ",                     { badTABLE,    "NAXIS1 keyword missing in TABLE extension"               } },
            { "NAXIS2  = ",                     { badTABLE,    "NAXIS2 keyword missing in TABLE extension"               } },
            { "PCOUNT  =                    0", { badTABLE,    "PCOUNT keyword missing or invalid in TABLE extension"    } },
            { "GCOUNT  =                    1", { badTABLE,    "GCOUNT keyword missing or invalid in TABLE extension"    } },
            { "TFIELDS = ",                     { badTABLE,    "TFIELDS keyword missing in TABLE extension"              } },
            { ((char *)0) }
        } },
        /*
         * BINTABLE extension.
         *              1         2         3         4         5         6
         *     123456789 123456789 123456789 123456789 123456789 123456789 12345
         */
        { { { "XTENSION= 'BINTABLE'",           { ((char *)0), ((char *)0)                                               } },
            { "BITPIX  =                    8", { badBINTABLE, "BITPIX keyword missing or invalid in BINTABLE extension" } },
            { "NAXIS   =                    2", { badBINTABLE, "NAXIS keyword missing or invalid in BINTABLE extension"  } },
            { "NAXIS1  = ",                     { badBINTABLE, "NAXIS1 keyword missing in BINTABLE extension"            } },
            { "NAXIS2  = ",                     { badBINTABLE, "NAXIS2 keyword missing in BINTABLE extension"            } },
            { "PCOUNT  = ",                     { badBINTABLE, "PCOUNT keyword missing in BINTABLE extension"            } },
            { "GCOUNT  =                    1", { badBINTABLE, "GCOUNT keyword missing or invalid in BINTABLE extension" } },
            { "TFIELDS = ",                     { badBINTABLE, "TFIELDS keyword missing in BINTABLE extension"           } },
            { ((char *)0) }
        } },
        { { { "XTENSION= 'IMAGE   '",           { ((char *)0), ((char *)0)                                               } },
            { "BITPIX  = ", { badIMAGE, "BITPIX keyword missing or invalid in IMAGE extension" } },
            { "NAXIS   =                    2", { badIMAGE, "NAXIS keyword missing or invalid in IMAGE extension"  } },
            { "NAXIS1  = ",                     { badIMAGE, "NAXIS1 keyword missing in IMAGE extension"            } },
            { "NAXIS2  = ",                     { badIMAGE, "NAXIS2 keyword missing in IMAGE extension"            } },
            { "PCOUNT  = ",                     { badIMAGE, "PCOUNT keyword missing in IMAGE extension"            } },
            { "GCOUNT  =                    1", { badIMAGE, "GCOUNT keyword missing or invalid in IMAGE extension" } },
            { ((char *)0) }
        } },
        };

        /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

        *hdutype      = f_hduUnknown;	/* Protect against failure return */
        *dataareasize = 0;		/* Protect against failure return */
        *rsasize      = 0;		/* Protect against failure return */
        *heapoff      = 0;		/* Protect against failure return */
        *heapsize     = 0;		/* Protect against failure return */

        if (header    == NULL) { reterrnomsg; }
        if (header[0] == NULL) { reterrnomsg; }

        /*
         * Determine, from the first header line, which header type we are.
         */

#if (f_hduUnknown != 0)
#error	f_hduUnknown assumed to be zero (0)
#endif

        for (htype = (FITSHDUTYPE) 1; htype  < f_hduCnt; htype++) {
                if (f_smatch (header[0], hdrReqd[htype].line[0].match)) {
                        break;			/* Found our header type*/
                }
        }
        if (htype == f_hduCnt) { reterr2 (badUnknown, "Unknown header type encountered\n"); }

        /*
         *   o   Adjust our initial guess if some other keywords are present.
         */

        if (f_lkey (&logval, header, "GROUPS")) {
                if  (logval) {
                        htype = f_hduGroups;	/* Primary --> Random Groups  */
                }
        }

        *hdutype = htype;			/* Return header type         */

        /*
         * Check the rest of the header lines.
         */

        for (hline = 1; ((header[hline] != NULL) && (hdrReqd[htype].line[hline].match != NULL)); hline++) {
            if (! f_smatch(header[hline], hdrReqd[htype].line[hline].match)) {
                if (f_debug(QUERY)) {
                    if (hdrReqd[htype].line[hline].errMsg[0] != NULL) { fprintf(stderr, hdrReqd[htype].line[hline].errMsg[0]); }
                    if (hdrReqd[htype].line[hline].errMsg[1] != NULL) { fprintf(stderr, hdrReqd[htype].line[hline].errMsg[1]); }
                }
                return(FALSE);
            }
        }

        /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
        /*
         * Perform any additional header type-specific checking that could not
         * be done in a table driven fashion.
         *
         * The size of the data area is also determined here.
         *
         * NOTE:  This checking does not always guarantee complete FITS compli-
         *        ance.  For example, testing NAXISn for presence does not tell
         *        us where the NAXISn were in the header (they must be after
         *        NAXIS and in order).  That is a limitation of f_ikey () (well,
         *        f_lnum could be used, but that just complicates the code).
         */

        switch (htype) {
        case    f_hduPrimary  :
	case    f_hduIMAGE :
                        keycnt = 0;
                f_ikey(&keycnt, header, "NAXIS");
                if (keycnt <  0) { reterr ("NAXIS keyword invalid\n"); }
                if (! keyword_product   (header, "NAXIS", 1, keycnt, dataareasize)) {
                        reterr ("couldn't determine data size\n");
                }
                      keyword_multabsint(header, "BITPIX", dataareasize);
                *dataareasize = rndbitstorec(*dataareasize);
                break;

        case    f_hduGroups   :
                        keycnt = 0;
                f_ikey(&keycnt, header, "NAXIS");
                if (keycnt <= 0) { reterr ("NAXIS keyword invalid\n"); }
                if (! keyword_product   (header, "NAXIS", 2, keycnt, dataareasize)) {
                        reterrnomsg;
                }
                if (! keyword_addint    (header, "PCOUNT", dataareasize)) {
                        reterr ("PCOUNT keyword missing or invalid in Random Groups\n");
                }
                if (! keyword_multint   (header, "GCOUNT", dataareasize)) {
                        reterr ("GCOUNT keyword missing or invalid in Random Groups\n");
                }
                if (! keyword_multabsint(header, "BITPIX", dataareasize)) {
                        reterr ("BITPIX keyword missing or invalid in Random Groups\n");
                }
                *dataareasize = rndbitstorec(*dataareasize);
                break;

        case	f_hduTABLE    :
                if (! xtensionsize(header, dataareasize, rsasize)) {
                        reterrnomsg;
                }
                        keycnt = 0;
                f_ikey(&keycnt, header, "TFIELDS");
                if (keycnt <= 0) { reterr ("TFIELDS keyword invalid\n"); }
                for (keyidx = 1; keyidx <= keycnt; keyidx++) {
                        sprintf(keyword, "TBCOL%d", keyidx);
                        if (f_lnum(header, keyword) == -1) {
                                reterrarg ("%s keyword missing in TABLE extension\n", keyword);
                        }
                        sprintf(keyword, "TFORM%d", keyidx);
                        if (f_lnum(header, keyword) == -1) {
                                reterrarg ("%s keyword missing in TABLE extension\n", keyword);
                        }
                }
                break;

        case	f_hduBINTABLE :
                if (! xtensionsize(header, dataareasize, rsasize)) {
                        reterrnomsg;
                }
                        keycnt = 0;
                f_ikey(&keycnt, header,        "TFIELDS");
                if     (keycnt <= 0) { reterr ("TFIELDS keyword invalid\n"); }
                for (keyidx = 1; keyidx <= keycnt; keyidx++) {
                        sprintf(keyword, "TFORM%d", keyidx);
                        if (f_lnum(header, keyword) == -1) {
                                reterrarg ("%s keyword missing in BINTABLE extension\n", keyword);
                        }
                }
                *heapoff  = (f_ikey(&intval, header, "THEAP")) ? intval
                                                               : *rsasize;
                f_ikey(&intval, header, "PCOUNT"); /* Know it's there */
                *heapsize = (intval != 0) ? intval - (*heapoff - *rsasize) : 0;
                break;

        default               :
                reterrarg2 ("Code doesn't handle header type accepted earlier by f_info () [%s, line %d]\n", __FILE__, __LINE__);
                /* break; */
        }

        /*
         * All done.
         */

        return(TRUE);

}

/******************************************************************************/
