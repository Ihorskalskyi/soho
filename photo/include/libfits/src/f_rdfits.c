/*                                                        f_rdfits
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                FILE *f_rdfits(name, header, size)
                char *name, *header[];
                int size;

Purpose:        Opens a fits file for reading, and gets the
                header.

Inputs:         'name' is a character pointer to a file name
                'size' is the size of the header array

Outputs:        'header' array filled with the header

Returns:        The file pointer on success,
                NULL on failure.

Notes:          Fills 'header' with NULLs first

History:        1986 July 6, Alan Uomoto, LSU Physics & Astronomy

------------------------------------------------------------------

*/

#include "libfits.h"

FILE *f_rdfits(char *name, char **header, int size)
{

        register int i;                 /* loop index                   */
        int     f_debug(int flag),              /* error messages flag          */
                f_save(FILE *fp, char **header, char *mode),               /* saves file info              */
                f_rdhead(char **header, int size, FILE *fp);             /* read the header records      */
        FILE    *f_fopen(char *name, char *mode),             /* opens data files             */
                *fp;                    /* temporary file pointer       */

        for (i = 0; i < size; i++) {
                header[i] = NULL;
        }

        if ((fp = f_fopen(name, "r")) == NULL) {
                if (f_debug(QUERY)) {
                        fprintf(stderr, "f_rdfits: Can't open '%s'\n", name);
                }
                return(NULL);
        }

        if (! f_rdhead(header, size, fp))
                return(NULL);

        if (! f_save(fp, header, "r"))
                return(NULL);

        return(fp);

}

#ifdef DEBUG            /* test program */

#define HEADERLINES 145

main(argc, argv)
int argc;
char *argv[];

{

        char *header[HEADERLINES];
        int i;
        FILE *fp, *f_rdfits();

        if (argc != 2) {
                printf("usage: f_rdfits filename\n");
                exit(1);
        }
        for (i = 0; i < HEADERLINES; i++) {
                header[i] = NULL;
        }

        f_setup();
        f_debug(TRUE);

        if ((fp = f_rdfits(argv[1], header, HEADERLINES)) == NULL) {
                printf("f_rdfits error with %s\n", argv[1]);
                exit(1);
        }
        for (i = 0; header[i] != NULL; i++) {
                printf("%s\n", header[i]);
        }
}

#endif  /* DEBUG */
