/*                                                          f_kdel
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_kdel(header, keyword);
                char *header[], *keyword;

Purpose:        Deletes the header line with 'keyword' in it

Inputs:         'header' is the FITS header,
                'keyword' is the keyword to delete

Returns:        TRUE on success,
                FALSE if 'keyword' wasn't in the header

Notes:

History:        1986 Aug 19, Alan Uomoto, LSU Physics & Astronomy

------------------------------------------------------------------

*/

#include "libfits.h"

int f_kdel(char **header, char *keyword)
{

        int f_lnum(char **header, char *keyword), f_hldel(char **header, int lineno);

        if (! f_hldel(header, f_lnum(header, keyword))) {
                return(FALSE);
        }
        else {
                return(TRUE);
        }
}
