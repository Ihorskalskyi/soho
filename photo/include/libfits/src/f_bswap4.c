/*                                                        f_bswap4
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                void f_bswap4(bp)
                register int16 *bp;

Purpose:        Reverses the byte order in the long words of a FITS
                record

Inputs:         'bp' is a pointer to 720 32-bit integers

Returns:        Byte swapped integers in 'bp'

Notes:          There might be a faster way to do this.

                This routine is only for those machines whose
                architecture requires a byte swap (PDP, VAX, for
                example).

History:        1986 Aug 6, Alan Uomoto, LSU Physics & Astronomy

------------------------------------------------------------------

*/

#include "libfits.h"

void f_bswap4(register int *buffer)
{

        register int i;
        union {
                int16 i16[2];
                int32 i32;
        } a, b;
        extern void f_bswap2(register short int *in);

        f_bswap2((int16 *) buffer);     /* first swap the bytes */
        for (i = 0; i < 720; i++) {     /* then the words       */
                a.i32 = *buffer;
                b.i16[0] = a.i16[1];
                a.i32 <<= 16;
                a.i16[0] = b.i16[0];
                *buffer++ = a.i32;
        }
}
