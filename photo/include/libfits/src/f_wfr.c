/*                                                           f_wfr
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_wfr(buf, fp)
                char *buf;
                FILE *fp;

Purpose:        Write a 2880 byte FITS record

Inputs:         'buf' is the data to write
                'fp' is the file stream pointer

Returns:        TRUE on success,
                FALSE on failure

Notes:          This could be a macro

History:        1986 July 10, Alan Uomoto, LSU Physics & Astronomy

------------------------------------------------------------------

*/

#include "libfits.h"

int f_wfr(char *buf, FILE *fp)
{

#ifdef VAXC
	size_t fwrite();
#endif

        return( fwrite(buf, RECORDSIZE, 1, fp) );

}
