/*                                                         f_setup
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                void f_init()

Purpose:        Initializes global storage

Inputs:         none

Notes:          Must be called before any other f_ routines.

History:        1986 July 10, Alan Uomoto, LSU Physics & Astronomy
                1986 Nov 21, changed name from f_init() because
                        of libI77 conflict.
------------------------------------------------------------------

*/

#include "libfits.h"

static void f_setswap(void)         /* figures out byte swapping    */

{

        int8 *cp;
        int32 testval;
        extern int f_BYTESWAP, f_WORDSWAP;

        testval = 0x03020100;

        cp = (int8 *) &testval;

        switch (*cp) {
        case 0x03:      f_BYTESWAP = FALSE;
                        f_WORDSWAP = FALSE;
                        break;
        case 0x02:      f_BYTESWAP = TRUE;
                        f_WORDSWAP = FALSE;
                        break;
        case 0x01:      f_BYTESWAP = FALSE;
                        f_WORDSWAP = TRUE;
                        break;
        case 0x00:      f_BYTESWAP = TRUE;
                        f_WORDSWAP = TRUE;
                        break;
        default:        fprintf(stderr, "f_setup: Can't figure swaps\n");
                        exit(1);
        }
}

void f_setup(void)
{

        register int i;                 /* loop index                   */
        static int done = FALSE;        /* has it been called before?   */
        void f_setswap();               /* finds BYTE and WORD swap     */
        extern struct f_finfo f_files;  /* place where everything is    */

        if (done)
                return;

        for (i = 0; i < MAXFILES; i++) {
                f_files.streams[i] = NULL;
                f_files.bp[i] = NULL;
                f_files.bitpix[i] = 0;
                f_files.mode[i] = -1;
        }

        f_setswap();

        done = TRUE;

}
