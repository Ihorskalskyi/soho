/*                                                        f_rdhead
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_rdhead(header, size, fp)
                char *header[];
                int size;
                FILE *fp;

Purpose:        Reads the header

Inputs:         'fp' is the file pointer to an open file
                size is the dimension of header

Returns:        TRUE on success, with 'header' filled with the
                header

                FALSE on failure

Notes:          1. does not rewind fp first.
                2. Allocates memory for header (82 bytes per line,
                   terminated with '\0' at position 80) and sets the
                   next pointer to NULL. If the pointer is not NULL
                   to start with, it frees the memory.
                3. Leaves the file at the top of data position.
                4. The mechanism for searching through the header
                   requires that we know what the next header line
                   at which to start a search is. This line is
                   marked with a '.'.

History:        1986 July 6, Alan Uomoto, LSU Physics & Astronomy
                1986 Aug 21, Removed rewind first, aku

------------------------------------------------------------------

*/

#include "libfits.h"

#define ERROR   0
#define END     -1
#define NOTEND  1

static int check(char *buf, FILE *fp)       /* minimal verification of FITS header  */
          
         

{

        int     f_debug(int flag),      /* error messages flag          */
                f_smatch(register char *line, register char *keyword),     /* short string match           */
                f_rfr(char *bp, FILE *fp);        /* read FITS record             */

        if (fp == NULL) {
                if (f_debug(QUERY)) {
                        fprintf(stderr, "f_rdhead: Passed a NULL pointer\n");
                }
                return(FALSE);
        }

        if (! f_rfr(buf, fp)) {
                if (f_debug(QUERY)) {
                        fprintf(stderr, "f_rdhead: Not a FITS file\n");
                }
                return(FALSE);
        }
        if (! f_smatch(buf, "SIMPLE  =") &&
	    ! f_smatch(buf, "XTENSION=")) {
                if (f_debug(QUERY)) {
                        fprintf(stderr, "f_rdhead: SIMPLE and XTENSION keywords are both missing\n");
                }
                return(FALSE);
        }
        if (! f_smatch(buf+80, "BITPIX  =")) {
                if (f_debug(QUERY)) {
                        fprintf(stderr, "f_rdhead: BITPIX keyword missing\n");
                }
                return(FALSE);
        }
        if (! f_smatch(buf+160, "NAXIS   =")) {
                if (f_debug(QUERY)) {
                        fprintf(stderr, "f_rdhead: NAXIS keyword missing\n");
                }
                return(FALSE);
        }

        return(TRUE);

}

int f_unpk(char **header, char *buf, int size, int line)        /*  0 - bad header      */
                                                /*  1 - OK, but not end */
                                                /* -1 end card found    */

{

        int     i,              /* loop index                           */
                f_debug(int flag),      /* error messages flag                  */
                f_smatch(register char *line, register char *keyword);     /* short string match routine           */
        register int    j;      /* another loop index                   */
        char    *bp,            /* temporary buffer pointer             */
                *shMalloc();      /* standard i/o library memory routine  */

        bp = buf;
        for (i = 0; i < 36; i++) {
                if (line >= size - 1) {
                        if (f_debug(QUERY)) {
                                fprintf(stderr,
                                        "f_rdhead: header array too small\n");
                        }
                        return(ERROR);
                }
                if (header[line] == NULL) {
                        header[line] = shMalloc(82);
                        header[line][0] = '\0';
                }
                for (j = 0; j < 80; j++) {
                        header[line][j] = *bp++;
                }
                header[line][80] = '\0';
                header[line][81] = ' ';
                if (f_smatch(header[line], "END     ")) {
                        header[0][81] = '.';
                        header[line+1] = NULL;
                        return(END);
                }
                line++;
        }
        return(NOTEND);
}

int f_rdhead(char **header, int size, FILE *fp)
{

        char    buf[2880];      /* temporary buffer                     */
        int     line,           /* line number to start loading         */
                n,              /* temporary variable                   */
                f_debug(int flag),      /* checks for debug on or off           */
                check(),        /* reads first record, checks for FITS  */
                f_unpk(char **header, char *buf, int size, int line);         /* unpacks a record into header         */

        if (! check(buf, fp)) {
                return(FALSE);
        }

        line = 0;
        while ((n = f_unpk(header, buf, size, line)) != END) {
                if (n == ERROR) {
                        return(FALSE);
                }
                if (! f_rfr(buf, fp)) {
                        if (f_debug(QUERY)) {
                                fprintf(stderr, "f_rdhead: Unexpected EOF\n");
                        }
                        return(FALSE);
                }
                line += 36;     /* 36 card images per FITS record       */
        }

        return(TRUE);

}


#ifdef DEBUG    /* passed this test 1986 August 15      */

#define HEADERSIZE (36 * 2)

main(argc, argv)
int argc;
char *argv[];

{

        char *header[HEADERSIZE];
        int i;
        FILE *fp, *f_fopen();

        if (argc != 2) {
                printf("usage: f_rdhead filename\n");
                exit(1);
        }

        f_setup();
        f_debug(TRUE);
        if ((fp = f_fopen(argv[1], "r")) == NULL) {
                printf("f_rdhead: can't open input\n");
                exit(1);
        }

        if (! f_rdhead(header, HEADERSIZE, fp)) {
                printf("f_rdhead: Can't get header\n");
                exit(1);
        }
        for (i = 0; header[i] != NULL; i++) {
                printf("%s\n", header[i]);
        }
}

#endif
