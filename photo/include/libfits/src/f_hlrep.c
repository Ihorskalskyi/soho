/*                                                         f_hlrep
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_hlrep(header, lineno, line)
                char *header[], *line;
                int lineno;

Purpose:        Replace a header line

Inputs:         'header' is the FITS header
                'lineno' is the line number
                'line' is the line to add

Returns:        TRUE on success,
                FALSE on failure.

History:        1986 Aug 3, Alan Uomoto, LSU Physics & Astronomy

------------------------------------------------------------------

*/
#include <string.h>
#include "libfits.h"

int f_hlrep(char **header, int lineno, char *line)
{

        int     i,              /* loop index                           */
                len;            /* length of line to insert             */
        extern int f_debug(int flag);   /* error messages on or off             */

        for (i = 0; header[i] != NULL; i++)
                ;

        if (lineno >= i || lineno < 0) {
                if (f_debug(QUERY)) {
                        fprintf(stderr,
                                "f_hlrep: no line number %d\n", lineno);
                }
                return(FALSE);
        }

        len = strlen(line);
        for (i = 0; i < len; i++) {
                header[lineno][i] = line[i];
        }
        for (i = len; i < 82; i++) {
                header[lineno][i] = ' ';
        }
        header[lineno][80] = '\0';
        return(TRUE);

}

#ifdef DEBUG

#define HEADERSIZE (36 * 2)

main(argc, argv)        /* passed this test 1986 August 16      */
int argc;
char *argv[];

{

        int i, lnum;
        FILE *fp, *f_rdfits();
        extern int f_init(), f_debug();
        char *line = "This is the new header line";
        char *header[HEADERSIZE];

        f_setup();
        f_debug(TRUE);

        if (argc != 2) {
                printf("usage: f_hlrep input\n");
                exit(1);
        }
        fp = f_rdfits(argv[1], header, HEADERSIZE);
        if (fp == NULL) {
                printf("Can't open input\n");
                exit(1);
        }

        for (i = 0; header[i] != NULL; i++) {
                printf("%s\n", header[i]);
        }
        printf("\nline number? ");
        scanf("%d", &lnum);

        f_hlrep(header, lnum, line);
        for (i = 0; header[i] != NULL; i++) {
                printf("%s\n", header[i]);
        }
}

#endif
