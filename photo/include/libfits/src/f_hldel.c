/*                                                         f_hldel
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_hldel(header, lineno)
                char *header[];
                int lineno;

Purpose:        Deletes a line from the header

Inputs:         'header' is the file header
                'lineno' is the line number to delete

Returns:        TRUE on success,
                FALSE on failure (lineno out of range)

History:        1986 August 3, Alan Uomoto, LSU Physics & Astronomy

------------------------------------------------------------------

*/

#include "libfits.h"

int f_hldel(char **header, int lineno)
{

        int     i, j,           /* loop indices                         */
                hlines;         /* number of header lines               */
        extern int f_debug(int flag);   /* error messages on or off             */

        for (hlines = 0; header[hlines] != NULL; hlines++)
                ;

        if (lineno < 0 || lineno >= hlines) {
                if (f_debug(QUERY)) {
                        fprintf(stderr,
                                "f_hldel: No line number %d\n", lineno);
                }
                return(FALSE);
        }

        for (i = lineno; i < hlines - 1; i++) {
                for (j = 0; j < 82; j++) {
                        header[i][j] = header[i+1][j];
                }
        }

        shFree(header[hlines-1]);
        header[hlines-1] = NULL;
        return(TRUE);

}

#ifdef DEBUG    /* passed this test August 7, 1986 */

#define HEADERSIZE (36 * 2)

main(argc, argv)
int argc;
char *argv[];

{


        int line;
        char *header[HEADERSIZE];
        FILE *fp, *f_rdfits();
        extern void f_init();
        extern int f_debug(), f_hldel();

        if (argc != 2) {
                printf("usage: f_hldel input\n");
                exit(1);
        }

        f_setup();
        f_debug(TRUE);

        if ((fp = f_rdfits(argv[1], header, HEADERSIZE)) == NULL) {
                printf("Can't open %s\n", argv[1]);
                exit(1);
        }

        prhead(header);
        for (;;) {
                printf("Line number to delete: ");
                if (scanf("%d", &line) == EOF) {
                        exit(1);
                }
                if (! f_hldel(header, line)) {
                        printf("f_hldel: No line %d\n", line);
                }
                else {
                        prhead(header);
                }
        }
}

prhead(header)
char *header[];

{

        register int i, j;
        char line[80];


        for (i = 0; header[i] != NULL; i++) {
                for (j = 0; j < 80; j++) {
                        line[j] = header[i][j];
                }
                for (j = 79; j >= 0; j--) {
                        if (line[j] != ' ')
                                break;
                }
                line[j+1] = '\0';
                printf("%2d: %s\n", i, line);
        }
}

#endif /* DEBUG */
