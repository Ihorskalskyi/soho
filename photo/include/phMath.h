#ifndef PHMATH_H
#define PHMATH_H

	/* this file contains definitions and declarations for things
	   that WOULD appear in <math.h> if we had ANSI extensions */

	/* appears in "hybrid.c" */
extern double erf(double x);


#endif   /* PHMATH_H */
