#ifndef DTCL_H
#define DTCL_H

/* NAME: dtcl.h
 * PURPOSE: header file for Dart tcl utility Library
 * PROGRAMMER: Laura Mengel
 * DATE: 5/19/94
 *
 * SCCS release: 
 *      %Z% %M% %I% Delta: %E% %U% Extraction: %D% %T% %Z%
 *
 * ENVIRONMENT: ANSI C, callable from C++, POSIX compliant system calls
 */

/* ******** other includes ******** */

#include <tcl.h>
#include <ftcl.h>

/* ******** CONSTANTS and TYPEDEFS ******** */

/* in general Dart products should use these flags for ftcl_ParseArgv calls */

#define DART_PARSE_FLAGS (FTCL_ARGV_NO_LEFTOVERS)

/* ******** FUNCTION PROTOTYPES ******** */

#ifdef __cplusplus
extern "C" {
#endif

/* routines for generating standard command line usage and help strings */

char *dart_get_usage(Tcl_Interp *interp, ftclArgvInfo *argTable, 
                     int flags, char *cmd_name);
char *dart_get_help(Tcl_Interp *interp, ftclArgvInfo *argTable, 
                     int flags, char *cmd_name, char *facility);

/* routines for declaring new tcl verbs in DART (with defined help) */

int dart_declare(Tcl_Interp *interp, char *cmdName, Tcl_CmdProc *proc,
                 ClientData clientData, Tcl_CmdDeleteProc *deleteProc,
                 char *facility);

#ifdef __cplusplus
}
#endif

#endif
