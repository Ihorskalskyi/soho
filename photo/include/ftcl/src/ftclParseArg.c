#include <string.h>
#include <tcl.h>
#include <ftclParseArgv.h>

/*
** ftclParseArg.c contains TCL c functions that can be used by TCL procedure writers to
** parse and obtain help for TCL commands.  The TCL callable functions are:
** 
**   ftclParseArg, ftclPassed, ftclGetArgInfo, ftclGetHelp, ftclGetUsage
** 
** These functions are interfaces to a set of C functions (vijays's) that do the actual work. 
** All of the TCL functions require a parse table paramter that the command writer specifies 
** as TCL lists.   The outer functions all parse their paramters and then call createArgvInfo which 
** converts the TCL table to a c structure as required by the ftcl argument handling routines.
** 
** These outer routines pass a callback function address to createArgvInfo and createArgvInfo
** calls the callback with the c structure. Then the callback calls the lower level ftcl argument 
** handling routines.  Finally the callback return the results to the user.
** 
** The callback routines are:
** 
**   ParseArg, Passed, GetArgInfo, GetHelp, GetUsage
** 
** The callback routines have 3 parameters that are generally just passed thru from
** the tcl call to the call back routine which passes them to the the ftclParseArgv routine.
*/

/*
 *   A general error message formater that appends results to the TCL result.
 */
static char *build_errmsg(
   Tcl_Interp *interp, 
   char* facil, 			/* the ftcl command that originated the call */
   char *diag				/* diagnostic */
)
{
static char buff[132];
   sprintf(buff,"\nftcl%s: %s\n", facil, diag);
   Tcl_AppendResult(interp,buff,NULL);
}
/*
**    The callback routines which call the ftcl low level parsing routines and pass back results
**    to ftcl caller.
*/
static int ParseArg(Tcl_Interp  *interp, 
   ftclArgvInfo *formal_list, 
   char  **result_list ,		/* list of result addresses */
   char  ***t_argv, 			/* parse table as a list of lists */
   char  **u_argv,                      /* list of user arguments */
   int   u_argc,       			/* count of user args     */
   int   user_flags, 			/* call back args  */
   char  *cb1,				/* unused */
   char  *cb2,
   char  *cb3
)
{
int i; 
int rstatus;
int intdummy;
double dbldummy;
/* 
 *  parse the users command with c parse table
 */
   rstatus = ftcl_ParseArgv(interp, &u_argc, u_argv, formal_list, user_flags);
   if ( rstatus == FTCL_ARGV_GIVEHELP)
   {
       Tcl_SetResult(interp,"help", TCL_STATIC);
       return(TCL_OK);
   } else if ( rstatus != FTCL_ARGV_SUCCESS) 
   {
     build_errmsg(interp,"ParseArg","Error parsing the command");
     return(TCL_ERROR);
   };
/*
 *  Check that results are valid - since all destination values are forced to STRING
 *  we check the destination value according to the user type specified but leave it to pass
 *  back as string
 */
   for ( i=0 ; formal_list[i+1].type != FTCL_ARGV_END; i++)
   {
      if (result_list[i] != NULL)
      {
         if( strcmp(t_argv[i+1][1], "INTEGER") == 0)
         {
            if (Tcl_GetInt(interp, result_list[i], &intdummy) != TCL_OK)
            {
               build_errmsg(interp,"ParseArg","Invalid integer value");
               return (TCL_ERROR);
            }
         } 
         else if (strcmp(t_argv[i+1][1], "DOUBLE") == 0)  
         { 
            if (Tcl_GetDouble(interp, result_list[i], &dbldummy) !=TCL_OK )
            {
               build_errmsg(interp,"ParseArg","Invalid double value");
               return (TCL_ERROR);
            }
         }
/* 
 * this seems not to fail if the variable we are setting is a numeric string, e.g. 1.3,
 * but no variable is set.  The function value is &result_list[i] and interp->result is NIL
 */
/*
 * Set the user's tcl variable.  
 * CONSTANT type's are -options w/o a value. If it is CONSTANT then ftcl_ParseArgv set the i
 * destination  to an int so we use the user's default value. 
 */
         if( strcmp(t_argv[i+1][1], "CONSTANT") == 0)
         {
            if(! (Tcl_SetVar(interp, t_argv[i+1][3],t_argv[i+1][2] , TCL_LEAVE_ERR_MSG)))
            {
               build_errmsg(interp, "ParseArg", "Error setting variable");
               return(TCL_ERROR);
            }
         } 
/*
 * If the type is anything else then it was forced to STRING and we use the the dest value
 * that ftcl_ParseArgv set.
 */
         else if(! (Tcl_SetVar(interp, t_argv[i+1][3], result_list[i], TCL_LEAVE_ERR_MSG)))
         {
            build_errmsg(interp, "ParseArg", "Error setting variable");
            return(TCL_ERROR);
         }
      }
   };
   return(TCL_OK);
}

/*
 *  Call back routine for ftclPassed
 */
static int Passed(Tcl_Interp  *interp,
   ftclArgvInfo *formal_list,
   char  **result_list ,		/* list of result addresses */
   char  ***t_argv, 			/* parse table as a list of lists */
   char  **u_argv,                      /* list of user arguments */
   int   u_argc,       			/* count of user args     */
   int   user_flags, 			/* call back args  user flags */
   char  *option,       		/* passed thru as option to ftcl_ArgIsPresent */
   char  *cb2,
   char  *cb3
)
{
/*
 *  parse the users command with c parse table
 */
int rstatus;
   if (ftcl_ParseArgv(interp, &u_argc, u_argv, formal_list, user_flags) 
         != FTCL_ARGV_SUCCESS)
   {
     build_errmsg(interp,"Passed","Error parsing the command");
     return(TCL_ERROR);
   };
/*
 * Pass the c parse table and see if option is present.
 */
   rstatus = ftcl_ArgIsPresent(u_argc, u_argv, option, formal_list) ;   
   if (rstatus == 0)
   {
       Tcl_SetResult(interp, "0", TCL_STATIC);
   }
   else if (rstatus == 1)
   {
       Tcl_SetResult(interp, "1", TCL_STATIC);
   }
   else
   {
      build_errmsg(interp, "Passed","Error checking ftcl_ArgIsPresent");
      return (TCL_ERROR);  
   }
   return (TCL_OK);
}
/*
 * Call back routine for ftclGetArgInfo
 */
static int GetArgInfo(Tcl_Interp  *interp,
   ftclArgvInfo *formal_list,
   char  **result_list ,		/* list of result addresses */
   char  ***t_argv, 			/* parse table as a list of lists */
   char  **u_argv,                      /* list of user arguments */
   int   u_argc,       			/* count of user args     */
   int   indent, 			/* call back args  user flags		*/
   char  *cb1,
   char  *cb2,
   char  *cb3
)
{
/*
 * Pass the c arg table to ftcl_GetArgInfo and return result
 */
   Tcl_AppendResult( 
     interp,  
     ftcl_GetArgInfo (interp, formal_list, 0, u_argv[0], indent),
     TCL_STATIC);
   if (!interp->result) 
   {
      build_errmsg(interp, "GetArgInfo","Error");
      return (TCL_ERROR);  
   }
   return(TCL_OK);
}
/*
 * Call back routine for ftclGetHelp
 */
static int GetHelp(Tcl_Interp  *interp,
   ftclArgvInfo *formal_list,
   char  **result_list ,		/* list of result addresses */
   char  ***t_argv, 			/* parse table as a list of lists */
   char  **u_argv,                      /* list of user arguments */
   int   u_argc,       			/* count of user args     */
   int   indent, 			/* call back args  */
   char  *u_prefix,                     /* args passed thru from tcl caller to ftcl_GetHelp */
   char  *u_suffix,
   char  *help_suffix
)
{
/*
 * Pass the c arg table to  ftcl_GetHelp and return result
 */
   Tcl_AppendResult(
      interp,
      ftcl_GetHelp(interp, formal_list, 0, u_argv[0], 
                     indent, u_prefix,u_suffix,help_suffix),
      TCL_STATIC);
   if (!interp->result) 
   {
      build_errmsg(interp, "GetHelp","Error");
      return (TCL_ERROR);  
   }
   return(TCL_OK);
}
/*
 * Call back routine for ftclGetUsage
 */
static int GetUsage(Tcl_Interp  *interp,
   ftclArgvInfo *formal_list,
   char  **result_list ,		/* list of result addresses */
   char  ***t_argv, 			/* parse table as a list of lists */
   char  **u_argv,                      /* list of user arguments */
   int   u_argc,       			/* count of user args     */
   int   indent, 			/* call back args  */
   char  *u_prefix,			/* args passed thru from tcl caller to ftcl_GetHelp */
   char  *u_suffix,
   char  *cb3				/* unused */
)
{
/*
 *  Pass the c arg table to ftcl_GetUsage and return result
 */
   Tcl_AppendResult(
      interp,
      ftcl_GetUsage (interp, formal_list, 0, u_argv[0], indent, u_prefix, u_suffix),
      TCL_STATIC);
   if (!interp->result)
   {
      build_errmsg(interp, "GetUsage","Error");
      return (TCL_ERROR);  
   }
   return(TCL_OK);
}

/*
 *     Following are the FTCL callable functions.  Generally they parse their arguments and
 *     then call createArgvInfo which in turn calls the associated call back routine. 
 */
static ftclArgvInfo ftclParseArgInfo[] = {
    {NULL, FTCL_ARGV_HELP, NULL, NULL,"Parse a TCL $args variable"},
    {"<usr_cmd>",   FTCL_ARGV_STRING,NULL,NULL, "User's command - normally $args"},
    {"<parse_tab>", FTCL_ARGV_STRING,NULL,NULL, "Formal Argument list - a list of 5 element lists"},
    {"-nodefaults", FTCL_ARGV_CONSTANT,NULL,NULL, "dont internal default table"},
    {"-noleftovers",FTCL_ARGV_CONSTANT,NULL,NULL, "user must provide exact positional parameters"},
    {"-noabbrev",   FTCL_ARGV_CONSTANT,NULL,NULL, "dont detect argument abbreviations"},
    {"-skipfirst",     FTCL_ARGV_CONSTANT,NULL,NULL, "dont skip first argument"},
    {NULL, FTCL_ARGV_END, NULL, NULL, NULL}
 };
/*
 *  The routine that the user calls
 */
int ftclParseArg(                      /* function registered with ftcl as a proc */
   ClientData clientData,
   Tcl_Interp *interp,
   int argc,			/* These are the arguments passed by the user's ftcl call */	
   char *argv[]
)
{
char *user_cmd=NULL;
char *parse_tbl=NULL;
char *nodefaults=NULL;
char *noleftovers=NULL;
char *noabbrev=NULL;
char *noskip=NULL;
int user_flags=FTCL_ARGV_DONT_SKIP_FIRST_ARG+FTCL_ARGV_NO_LEFTOVERS;
int i;
int rstatus;
/*
 *  parse the ftcl parameters into usr_cmd and parse_tab and options.
 *  usr_cmd is original user's call as a list and parse_tab is the ftcl procedures
 *  parse table as a list of lists.  
 */
   ftclParseArgInfo[1].dst=&user_cmd;
   ftclParseArgInfo[2].dst=&parse_tbl;
   ftclParseArgInfo[3].dst=&nodefaults;
   ftclParseArgInfo[4].dst=&noleftovers;
   ftclParseArgInfo[5].dst=&noabbrev;
   ftclParseArgInfo[6].dst=&noskip;
   rstatus = ftcl_ParseArgv(interp, &argc, argv, ftclParseArgInfo, 
                 FTCL_ARGV_NO_LEFTOVERS+FTCL_PARSEARG);
   if (rstatus == FTCL_ARGV_SUCCESS)
   {
      if (ftcl_ArgIsPresent (argc, argv, "-nodefaults", ftclParseArgInfo))
         user_flags = user_flags+FTCL_ARGV_NO_DEFAULTS;
      if (ftcl_ArgIsPresent (argc, argv, "-noleftovers", ftclParseArgInfo))
         user_flags = user_flags+FTCL_ARGV_NO_LEFTOVERS;
      if (ftcl_ArgIsPresent (argc, argv, "-noabbrev", ftclParseArgInfo))
         user_flags = user_flags+FTCL_ARGV_NO_ABBREV;
      if (ftcl_ArgIsPresent (argc, argv, "-skipfirst", ftclParseArgInfo))
         user_flags = user_flags-FTCL_ARGV_DONT_SKIP_FIRST_ARG;
/*
 * createArgvInfo will break up user_cmd and parse_tbl from their lists of list
 * into c structures and pass these structures to ftcl_ParseArgv. ParseArg is callback
 * that will pass the ftcl_ParseArgv results back to the user.
 */
      return(
         createArgvInfo(clientData,interp,user_cmd, parse_tbl, ParseArg, "ParseArg",user_flags,
            NULL,NULL,NULL)
      );
   }
   else if ( rstatus == FTCL_ARGV_GIVEHELP)
   {
       Tcl_SetResult(interp,"help", TCL_STATIC);
       return(TCL_OK);
   }
   else
   {
      build_errmsg(interp, "ParseArg", "error parsing command");
      return (TCL_ERROR);
   }
   return (TCL_OK);
}

/*
 * users call ftclPassed with two parameters, usr_cmd & parse_tab.  We can not and need not
 * use ftcl_ParseArgv here because they are not compatible with ftcl_Present.
 */
int ftclPassed(                      /* function reGIstered with ftcl as a proc */
   ClientData clientData,
   Tcl_Interp *interp,
   int argc,		/* These are the arguments passed by the ftcl call */	
   char *argv[]
)
{
    if (argc == 4)
       return( 
          createArgvInfo(clientData, interp, argv[1], argv[2], Passed, "Passed",
             FTCL_ARGV_DONT_SKIP_FIRST_ARG,argv[3],
             NULL,NULL) 
       );
    build_errmsg(interp, "Passed", "requires 3 parameters");
    return (TCL_ERROR);
}

static ftclArgvInfo GetArgInfoInfo[] = {
    {NULL, FTCL_ARGV_HELP, NULL, NULL,"Return 1 if an optional argument is passed else 0"},
    {"<usr_cmd>",    FTCL_ARGV_STRING,NULL,NULL,"User's command - normally $args"},
    {"<parse_tab>",  FTCL_ARGV_STRING,NULL,NULL,"Formal Argument list - a list of 5 element lists"},
    {"[indent]",      FTCL_ARGV_INT   ,0,    NULL,"Number of chars to indent usage line"},
    {NULL, FTCL_ARGV_END, NULL, NULL, NULL}
 };

int ftclGetArgInfo(                       /* function registered with ftcl as a proc */
   ClientData clientData,
   Tcl_Interp *interp,
   int argc,		/* These are the arguments passed by the ftcl call */	
   char *argv[]
)
{
char *user_cmd=NULL;
char *parse_tbl=NULL;
int   *indent=NULL;
   GetArgInfoInfo[1].dst=&user_cmd;
   GetArgInfoInfo[2].dst=&parse_tbl;
   GetArgInfoInfo[3].dst=&indent;
   if (ftcl_ParseArgv(interp, &argc, argv, GetArgInfoInfo, 
            FTCL_ARGV_NO_LEFTOVERS+FTCL_PARSEARG) == FTCL_ARGV_SUCCESS)
      return
      (
         createArgvInfo(clientData, interp, user_cmd, parse_tbl, GetArgInfo, "GetArgInfo", 
            indent,NULL,NULL,NULL)
      );
   else
   {
      build_errmsg(interp, "GetArgInfo", "error parsing command");
      return TCL_ERROR;
   }
}

/*
 * The user callable routine
 */
static ftclArgvInfo GetHelpInfo[] = {
    {NULL, FTCL_ARGV_HELP, NULL, NULL,"Return 1 if an optional argument is passed else 0"},
    {"<usr_cmd>",   FTCL_ARGV_STRING,NULL,NULL,"User's command - normally $args"},
    {"<parse_tab>", FTCL_ARGV_STRING,NULL,NULL,"Formal Argument list - a list of 5 element lists"},
    {"[indent]",     FTCL_ARGV_INT,   0,   NULL,"Number of chars to indent usage line"},
    {"[u_prefix]" ,  FTCL_ARGV_STRING,"",  NULL,"Prefix to command syntax"},
    {"[u_suffix]" ,  FTCL_ARGV_STRING,"",  NULL,"Suffix to command syntax"},
    {"[h_suffix]" ,  FTCL_ARGV_STRING,"",  NULL,"Suffix after arginfo"},
    {NULL, FTCL_ARGV_END, NULL, NULL, NULL}
 };

int ftclGetHelp(                      /* function registered with ftcl as a proc */
   ClientData clientData,
   Tcl_Interp *interp,
   int argc,			/* These are the arguments passed by the ftcl call */	
   char *argv[]
)
{
char *user_cmd=NULL;
char *parse_tbl=NULL;
int  *indent=NULL;
char *u_prefix=NULL;
char *u_suffix=NULL;
char *h_suffix=NULL;
   GetHelpInfo[1].dst=&user_cmd;
   GetHelpInfo[2].dst=&parse_tbl;
   GetHelpInfo[3].dst=&indent;
   GetHelpInfo[4].dst=&u_prefix;
   GetHelpInfo[5].dst=&u_suffix;
   GetHelpInfo[6].dst=&h_suffix;
   if (ftcl_ParseArgv(interp, &argc, argv, GetHelpInfo, 
             FTCL_ARGV_NO_LEFTOVERS+ FTCL_ARGV_NO_DEFAULTS+FTCL_PARSEARG) == FTCL_ARGV_SUCCESS)
      return (
         createArgvInfo(clientData, interp, user_cmd, parse_tbl, GetHelp, "GetHelp", 
            indent, u_prefix, u_suffix, h_suffix)
      );
   else
   {
      build_errmsg(interp, "GetHelp", "error parsing command");
      return TCL_ERROR;
   }
}
/*
 * The user callable routine
 */
static ftclArgvInfo GetUsageInfo[] = {
    {NULL, FTCL_ARGV_HELP, NULL, NULL,"Return 1 if an optional argument is passed else 0"},
    {"<usr_cmd>",   FTCL_ARGV_STRING,NULL,NULL,"Command name"},
    {"<parse_tab>", FTCL_ARGV_STRING,NULL,NULL,"Formal Argument list - a list of 5 element lists"},
    {"[indent]",     FTCL_ARGV_INT,   NULL,NULL,"Number of chars to indent usage line"},
    {"[u_prefix]",   FTCL_ARGV_STRING,"",  NULL,"Prefix to command syntax"},
    {"[u_suffix]",   FTCL_ARGV_STRING,"",  NULL,"Suffix to command syntax"},
    {NULL, FTCL_ARGV_END, NULL, NULL, NULL}
 };

int ftclGetUsage(                      /* function registered with ftcl as a proc */
   ClientData clientData,
   Tcl_Interp *interp,
   int argc,			/* These are the arguments passed by the ftcl call */	
   char *argv[]
)
{
char *user_cmd=NULL;
char *parse_tbl=NULL;
char *indent=NULL;
char *u_prefix=NULL;
char *u_suffix=NULL;
   GetUsageInfo[1].dst=&user_cmd;
   GetUsageInfo[2].dst=&parse_tbl;
   GetUsageInfo[3].dst=&indent;
   GetUsageInfo[4].dst=&u_prefix;
   GetUsageInfo[5].dst=&u_suffix;
/*
 * Parse the ftcl command and pass thru the usual
 */
   if (ftcl_ParseArgv(interp, &argc, argv, GetUsageInfo, 
         FTCL_ARGV_NO_LEFTOVERS+ FTCL_ARGV_NO_DEFAULTS+FTCL_PARSEARG) == FTCL_ARGV_SUCCESS)
      return
      (
         createArgvInfo(clientData, interp, user_cmd, parse_tbl, GetUsage, "GetUsage", 
            indent, u_prefix, u_suffix, NULL)
      );
   else
   {
      build_errmsg(interp, "GetUsage", "error parsing command");
      return TCL_ERROR;
   }
}

/*
 *    Fill an entry in formal c argument list for ftcl_ParseArgv & its relatives
 */
static int fill_parse_entry( 
   ftclArgvInfo *formal_entry,		/* OUT - Formal parse table entry in c format */
   char **t_argv,			/* IN  - Formal Parse args as tcl strings */
   char **value_buffer			/* IN  - adr of parse result buffer */
)
{
   formal_entry->dst  = value_buffer;	/* target buffer for value of the parameter */
   formal_entry->src  = t_argv[2];	/* addr of the vaule for the paramter */
   formal_entry->key  = t_argv[0];	/* addr of the name of the parameter */
   formal_entry->help = t_argv[4];	/* addr of help sting */
/*
 * For every type but CONSTANT we get a string address back.   For constant's
 * we get an int back and throw it away.
 */
   if ( !strcmp( t_argv[1], "STRING" ) ||  !strcmp( t_argv[1], "DOUBLE" ) || 
        !strcmp( t_argv[1], "INTEGER" ) )
   {
      formal_entry->type = FTCL_ARGV_STRING;
   }
   else   if ( !strcmp( t_argv[1], "CONSTANT" ) )
   {
       formal_entry->type = FTCL_ARGV_CONSTANT;
   }
   else 
      return(TCL_ERROR);
   return (TCL_OK);
}

/*
**    Fill the first entry in the c parse table
*/
static fill_first_entry(
   ftclArgvInfo *formal_entry,		/* OUT - Formal parse arg in c format */
   char *help				/* IN  - address of help string for command */
)
{
   formal_entry->type = FTCL_ARGV_HELP;
   formal_entry->help = help;                  
   formal_entry->key  = NULL;
   formal_entry->src  = NULL;
   formal_entry->dst  = NULL;
   return(TCL_OK);
}

/*
**    Fill the last entry in the c parse table
*/
static fill_last_entry( 
   ftclArgvInfo *formal_entry		/* OUT - Formal parse arg in c format */
)
{
   formal_entry->type = FTCL_ARGV_END;
   formal_entry->help = NULL;
   formal_entry->key  = NULL;
   formal_entry->src  = NULL;
   formal_entry->dst  = NULL;
   return(TCL_OK);
}


/*
** cpp defines for error handling
** They append a diagnostic in the tcl result; free malloced memory; and return TCL_ERROR
** They are freed in the reverse order that they are malloced so things might be contigous.
*/
#define FREE(malloc_ptr) if (malloc_ptr) free((char*) malloc_ptr)
#define BAILOUT(diag) {build_errmsg(interp,routine,diag);FREE(t_argv);FREE(p_argv);FREE(u_argv);FREE(formal_list); FREE(result_list);return(TCL_ERROR);}

/*
 * Called by all high level routines.   It break up the ftcl args into c args and
 * calls the appropriate call back.
 */
int createArgvInfo(		
   ClientData clientData, 
   Tcl_Interp *interp, 
   char *user_cmd, 
   char *parse_tbl,
   int function_callback(),
   char *routine,
   char *callback_param, 
   char *cb2,  
   char *cb3, 
   char *cb4
)
{
   ftclArgvInfo *formal_list=0; 	/* malloc'd ptr to formal parse struct for ftcl_ParseArgv */
   char         **result_list=0;	/* malloc'd ptr to ray of addr returned by ftcl_ParseArgv */

   int u_argc;				/* user_cmd  users command -> u_argv for ftcl_ParseArgv*/
   char **u_argv=NULL;

   int p_argc;          		/* parse_tbl lists of the command definition's syntax */
   char **p_argv=NULL;

   int t_argc;          		/* parse_tbl is split into separate lines of syntax */
   char ***t_argv=NULL;			/*   which are then split into elements */
					/* t_argv is an array of ptr to an array of ptr */
   int i, rstatus;

/*
 *  Break up the end users command from tcl lists to a c array.  u_argc is a the # of user params
 */
   if (Tcl_SplitList (interp, user_cmd, &u_argc, &u_argv) != TCL_OK)    
      BAILOUT("The first argument to ftclParseArg must be $args");
/*
 *  Break up the parse_tbl parse table which is a tcl list of lists into a c array of 
 *  pointers to a list for each parameter
 */
   if (Tcl_SplitList (interp, parse_tbl, &p_argc, &p_argv)!= TCL_OK)    /* split parse table */
      BAILOUT("The second argument to ftclParseArg must be a list of lists");

/*
 *  For each p_argc allocate memory for the list of the 5 parameter fields 
 *  We use +1 to add a marker for the end and +2 for a header marker.
 */
   formal_list = (ftclArgvInfo *) malloc ( (p_argc+2) * sizeof(ftclArgvInfo) );
   result_list = (char**)         malloc ( (p_argc+1) * sizeof(char *) );
   t_argv      = (char***)        malloc ( (p_argc+1) * sizeof(char *) );

   if ( !formal_list || !result_list || !t_argv) 
      BAILOUT("Internal malloc failure");
   for ( i = 0; i<p_argc+1; i++) result_list[i] = NULL;
/*
 * Break up the 1st param which is the cmd help line
 */
   rstatus = Tcl_SplitList (interp, p_argv[0], &t_argc, &t_argv[0]);
   if (rstatus != TCL_OK || t_argc != 2)
      BAILOUT("The first formal entry must have 2 entries - <command_name> <command_help>");
   fill_first_entry(formal_list, t_argv[0][1]);	/* build the formal param table */
   p_argc--; 
/*
 * For each formal paramter split the 5 fields and put the fields into a c structure.
 */
   for ( i=0 ; i < p_argc; i++) 
   {
      rstatus = Tcl_SplitList (interp, p_argv[i+1], &t_argc, &t_argv[i+1]);
      if (rstatus != TCL_OK || t_argc != 5)
         BAILOUT("Formal parameter list is invalid - must have 5 entries"); 
      if (fill_parse_entry(formal_list+i+1, t_argv[i+1], result_list+i)  != TCL_OK) 
         BAILOUT("Parameter type must be STRING, INT, DOUBLE"); 
   };
   fill_last_entry(formal_list+i+1);
/*
 *  Finally call vijay's routines with c parameters. function_callback is a parameterized routine.
 */
   rstatus = function_callback(
      interp, formal_list, result_list, t_argv, u_argv, u_argc, callback_param,cb2,cb3,cb4); 
/*
 * Free the meory in the reverse order it was allocated.
 */
   for ( i=0 ; i < p_argc+1; i++) 
   {
      free( (char *) t_argv[i]);
   };
   free( (char *) t_argv );
   free( (char *) result_list );
   free( (char *) formal_list);
   free( (char *) p_argv );
   free( (char *) u_argv );
   return (rstatus);
}
/*
 * A test driver - normaly commented out.
 */
/*
main(int argc, char *argv[])
{
  int code;
  Tcl_Interp *interp;

  interp = Tcl_CreateInterp();
  Tcl_CreateCommand(interp, "ftclParseArg", ftclParseArg,
     (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL);
  Tcl_CreateCommand(interp, "ftclPassed", ftclPassed,
     (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL);
  Tcl_CreateCommand(interp, "ftclGetUsage", ftclGetUsage,
     (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL);
  Tcl_CreateCommand(interp, "ftclGetArgInfo", ftclGetArgInfo,
     (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL);
  Tcl_CreateCommand(interp, "ftclGetHelp", ftclGetHelp,
     (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL);
  code = Tcl_EvalFile (interp, argv[1]);
  if( *interp->result != 0) printf("%s\n", interp->result );
  if (code != TCL_OK)  exit (1);
  exit (0);
}
*/
