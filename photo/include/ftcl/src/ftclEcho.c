/* ===================================================================  *
 * File: ftclEcho.c							*
 *									* 
 * ftcl_EchoCmd and ftcl_EchonnCmd to implement echo and echonn 	*
 * commands.								*
 * 									*			
 *----------------------------------------------------------------------
 *
 * ftcl_EchoCmd --
 *    Implements the TCL echo command:
 *        echo str1 [str2..]
 *
 * Results:
 *      Always returns TCL_OK.
 *
 *
 *----------------------------------------------------------------------
 */
#include "ftcl.h"

int ftcl_EchoCmd ( ClientData clientData, Tcl_Interp *interp, int argc, char **argv)
{
    int i;

    for (i = 1; ; i++) {
	if (argv[i] == NULL) {
	    if (i != argc) {
		echoError:
		sprintf(interp->result,
		    "argument list wasn't properly NULL-terminated in \"%s\" command",
		    argv[0]);
	    }
	    break;
	}
	if (i >= argc) {
	    goto echoError;
	}
	fputs(argv[i], stdout);
	if (i < (argc-1)) {
	    printf(" ");
	}
    }
    fputc ('\n',stdout);
    printf("\n");
    return TCL_OK;
}

/*
 *----------------------------------------------------------------------
 *
 * ftcl_EchonnCmd --
 *    Implements the TCL echo command:
 *        echo str1 [str2..]
 *    no new line is printed.
 *
 * Results:
 *      Always returns TCL_OK.
 *
 *
 *----------------------------------------------------------------------
 */

int ftcl_EchonnCmd ( ClientData clientData, Tcl_Interp *interp, int argc, char **argv)
{
    int idx;

    for (idx = 1; idx < argc; idx++) {
        fputs (argv [idx], stdout);
        if (idx < (argc - 1))
            printf(" ");
    }
    return TCL_OK;
}

/* ================================================================================ */
