/* 
 * cmdloop --
 *
 *   Interactive command loop, C and Tcl callable.
 *---------------------------------------------------------------------------
 * Copyright 1992 Karl Lehenbauer and Mark Diekhans.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose and without fee is hereby granted, provided
 * that the above copyright notice appear in all copies.  Karl Lehenbauer and
 * Mark Diekhans make no representations about the suitability of this
 * software for any purpose.  It is provided "as is" without express or
 * implied warranty.
 *---------------------------------------------------------------------------
 *	Modifications:   The Tcl_CommandLoop routine has been modify
 *			 to allow command line editing in the interactive
 *			 loop.
 *			  The command line editing requires the include files
 *			  termio.h, signal.h and ftclCmdLine.h			 
 *			 
 *				M.Vittone ,Fermilab Online Support
 *				3 - Dec - 1992
 *                        Modified the inbut buffer size from 256 to 1024
 *                              18 -Jun -1993
 *---------------------------------------------------------------------------

/* == This is for cmd editing == */

#ifdef __sgi			/* Problems with ansi switch	*/

#ifndef __EXTENSIONS__
#define __EXTENSIONS__
#endif

#endif

#include "tcl.h"
#include <termio.h>
#include <signal.h>
 
#include "ftclCmdLine.h"

#include "tclExtdInt.h"

/* == */
/*
 * Pointer to eval procedure to use.  This way bring in the history module
 * from a library can be made optional.  This only works because the calling
 * sequence of Tcl_Eval is a superset of Tcl_RecordAndEval.  This defaults
 * to no history, set this variable to Tcl_RecordAndEval to use history.
 */

int (*tclShellCmdEvalProc) () = Tcl_Eval;

extern int tclReceivedSignal;  /* Set when a signal comes in */

/*
 * Prototypes of internal functions.
 */
static int
IsSetVarCmd _ANSI_ARGS_((Tcl_Interp *interp,
                         char       *command));

static void
OutStr _ANSI_ARGS_((FILE *filePtr,
                    char *string));

static void
OutFlush _ANSI_ARGS_((FILE *filePtr));

static void
Tcl_PrintResult _ANSI_ARGS_((FILE   *fp,
                             int     returnval,
                             char   *resultText));

static void
OutputPrompt _ANSI_ARGS_((Tcl_Interp *interp,
                          FILE       *outFP,
                          int         topLevel));

static int
SetPromptVar _ANSI_ARGS_((Tcl_Interp  *interp,
                          char        *hookVarName,
                          char        *newHookValue,
                          char       **oldHookValuePtr));


/*
 *----------------------------------------------------------------------
 *
 * IsSetVarCmd --
 *
 *      Determine if the current command is a `set' command that set
 *      a variable (i.e. two arguments).  This routine should only be
 *      called if the command returned TCL_OK.
 *
 *----------------------------------------------------------------------
 */
static int
IsSetVarCmd (interp, command)
    Tcl_Interp *interp;
    char       *command;
{
    char  *nextPtr;

    if ((!STRNEQU (command, "set", 3)) || (!isspace (command [3])))
        return FALSE;  /* Quick check */

    nextPtr = TclWordEnd (command, FALSE, (int *) NULL);
    if (*nextPtr == '\0')
        return FALSE;
    nextPtr = TclWordEnd (nextPtr, FALSE, (int *) NULL);
    if (*nextPtr == '\0')
        return FALSE;

    while (*nextPtr != '\0') {
        if (!isspace (*nextPtr))
            return TRUE;
        nextPtr++;
    }
    return FALSE;
}

/*
 *----------------------------------------------------------------------
 *
 * OutStr --
 *
 *   Print a string to the specified file handle and check for errors.
 *
 *----------------------------------------------------------------------
 */
static void
OutStr (filePtr, string)
    FILE *filePtr;
    char *string;
{
    int stat;

    stat = fputs (string, filePtr);
    if (stat == EOF)
        panic ("command loop: error writing to output file: %s\n",
               strerror (errno));
}

/*
 *----------------------------------------------------------------------
 *
 * OutFlush --
 *
 *   Flush a stdio file and check for errors.
 *
 *----------------------------------------------------------------------
 */
static void
OutFlush (filePtr)
    FILE *filePtr;
{
    int stat;

    stat = fflush (filePtr);
    if (stat == EOF)
        panic ("command loop: error flushing output file: %s\n",
               strerror (errno));
}

/*
 *----------------------------------------------------------------------
 *
 * Tcl_PrintResult --
 *
 *      Print a Tcl result
 *
 * Results:
 *
 *      Takes an open file pointer, a return value and some result
 *      text.  Prints the result text if the return value is TCL_OK,
 *      prints "Error:" and the result text if it's TCL_ERROR,
 *      else prints "Bad return code:" and the result text.
 *
 *----------------------------------------------------------------------
 */
static void
Tcl_PrintResult (fp, returnval, resultText)
    FILE   *fp;
    int     returnval;
    char   *resultText;
{

    if (returnval == TCL_OK) {
        if (resultText [0] != '\0') {
            OutStr (fp, resultText);
            OutStr (fp, "\n");
        }
    } else {
        OutFlush (fp);
        OutStr (stderr, (returnval == TCL_ERROR) ? "Error" : 
                                                   "Bad return code");
        OutStr (stderr, ": ");
        OutStr (stderr, resultText);
        OutStr (stderr, "\n");
    }
}

/*
 *----------------------------------------------------------------------
 *
 * OutputPromp --
 *     Outputs a prompt by executing either the command string in
 *     TCLENV(topLevelPromptHook) or TCLENV(downLevelPromptHook).
 *
 *----------------------------------------------------------------------
 */
static void
OutputPrompt (interp, outFP, topLevel)
    Tcl_Interp *interp;
    FILE       *outFP;
    int         topLevel;
{
    char *hookName;
    char *promptHook;
    int   result;
    int   promptDone = FALSE;

    hookName = topLevel ? "topLevelPromptHook"
                        : "downLevelPromptHook";
    if (((promptHook = Tcl_GetVar2 (interp, "TCLENV", hookName, 1)) != 
          NULL) && (*promptHook != '\0')) {
        result = Tcl_Eval(interp, promptHook);
        if (!((result == TCL_OK) || (result == TCL_RETURN))) {
            OutStr (stderr, "Error in prompt hook: ");
            OutStr (stderr, interp->result);
            OutStr (stderr, "\n");
            Tcl_PrintResult (outFP, result, interp->result);
        } else {
            OutStr (outFP, interp->result);
            promptDone = TRUE;
        }
    } 
    if (!promptDone) {
        if (topLevel)
            OutStr (outFP, "%");
        else
            OutStr (outFP, ">");
    }
    OutFlush (outFP);

}

/*=============insert new test here==============================

/*
 *----------------------------------------------------------------------
 *
 * Tcl_CommandLoop --
 *
 *   Run a Tcl command loop.  The command loop interactively prompts for,
 * reads and executes commands. Two entries in the global array TCLENV
 * contain prompt hooks.  A prompt hook is Tcl code that is executed and
 * its result is used as the prompt string.  The element `topLevelPromptHook'
 * is the hook that generates the main prompt.  The element
 * `downLevelPromptHook' is the hook to generate the prompt for reading
 * continuation lines for incomplete commands.  If a signal occurs while
 * in the command loop, it is reset and ignored.  EOF terminates the loop.
 *
 * Parameters:
 *   o interp (I) - A pointer to the interpreter
 *   o inFile (I) - The file to read commands from.
 *   o outFile (I) - The file to write the prompts to. 
 *   o evalProc (I) - The function to call to evaluate a command.
 *     Should be either Tcl_Eval or Tcl_RecordAndEval if history is desired.
 *   o options (I) - Currently unused.
 *
 *	Modifications:   The Tcl_CommandLoop routine has been modify
 *			 to allow command line editing in the interactive
 *			 loop.
 *				M.Vittone ,Fermilab Online Support
 *				3 - Dec - 1992
 *----------------------------------------------------------------------
 *
 *	Modifications:	Replace Tcl_CreateCmdBuf(), Tcl_AssembleCmd(),
 *			and Tcl_DeleteCmdBuf() with appropriate dynamic
 *			string functions.
 *
 *				C.-H. Huang, Fermilab OLS
 *				10 - May -1994
 *				* Solar Eclipse in Chicago
 *----------------------------------------------------------------------
 */


void
ftcl_CommandLoop (
    Tcl_Interp *interp,
    FILE       *inFile,
    FILE       *outFile,
    int         (*evalProc) (),
    unsigned    options)
{


CMD_EDITHNDL cmdHandle;
CMD l_line;
int lineEntered;
CMD_EDITHIST history;
unsigned long sstatus;

int  tclstat;


Tcl_DString cmdBuf;

char       inputBuf[1024];
char	   partbuf[1024];	/* used when partial command is entered */

int        topLevel = TRUE;
int        result;
char	   *ret_get;
char 	   program_prompt[50];


/* ------------------------------------------------------------------------------------- */
	Tcl_DStringInit(&cmdBuf);

	inputBuf[0] = 0;
	partbuf[0]  = 0;	

/* Get a prompt  for the interactive session: if programName is defined, use the name
   of current application as ftcl prompt; if not ,then just give a simple ftcl prompt    */

	   ret_get  = Tcl_GetVar(interp,"programName",TCL_LEAVE_ERR_MSG);

    	   if (ret_get == NULL)
    	   {
		sprintf (program_prompt, "ftcl");
	   }
	   else
	   {
		sprintf (program_prompt, "%s",ret_get);
	
	   }
	   strcat (program_prompt," > "); 

/* Initialize interrupt handler  */

#ifndef VXWORKS
	ftclCmd_INTDec(2);
#endif

/* Initialize history  */

	ftclCmd_HistInit(&history,interp);

	while(1)		/* Loop forever */
	{

/** If a signal came in, reset the signals and drop any pending command.   */

#ifndef VXWORKS

	   if (ftclCmd_INTChk() )
	   {
		Tcl_DStringFree(&cmdBuf);
            	topLevel = TRUE;
	   }

#endif		    

#ifndef VXWORKS

           clearerr (inFile);
           clearerr (outFile);
#else
           clearerr (stdin);
           clearerr (stdout);
#endif

/* Get ready for line entry : if the command is not complete and is split over multiple
   lines, send a continuation line prompt 					           */


	   if (topLevel == FALSE) 
              {(ftclCmd_LineStart(&cmdHandle, "=>", &history, 0) );}
	   if (topLevel == TRUE) 
              {(ftclCmd_LineStart(&cmdHandle, program_prompt, &history, 0) );}

	   do			/* Loop until line entry is complete */
	   {

/* Get single character input from stdin */

	         ftclCmd_GetChar(&cmdHandle);

/* Now process the entered character */

	         lineEntered = ftclCmd_ProcChar(&cmdHandle, l_line);

           } while (!lineEntered);		/* Complete line   */

	   if (lineEntered == -1)
           {
                Tcl_DStringFree(&cmdBuf);
 
                break;
           }

	   strcpy (inputBuf,l_line);
	   strcat (inputBuf,"\n");		/* Adding new line character */

	   strcat (partbuf,l_line);
	   strcat (partbuf,"\n");

	Tcl_DStringAppend(&cmdBuf, inputBuf, -1);
           if (!Tcl_CommandComplete(Tcl_DStringValue(&cmdBuf)))
	   {
	            topLevel = FALSE;

   	   	    lineEntered = 0;		/* Resetting line */

	            continue;  			/* Next line */
           }

           if ( (!strcmp(l_line, "exit")) |
                (!strcmp(l_line, "quit")) |
                (!strcmp(l_line, "logout")) )
           {
                Tcl_DStringFree(&cmdBuf);
 
                break;
           }

	   topLevel = TRUE; 		/* reset flag */
	   
           tclstat = Tcl_RecordAndEval(interp, partbuf, 0 );

	   strcpy (partbuf,"");			   /* Reinitialize buffer  */

           if (tclstat != TCL_OK)
      	   {
	      printf("TCL Error");
	      if (*interp->result != 0)
	      {printf(": %s\n", interp->result);}

	      else
              {printf("\n");}
      	   }
   	   else
           {
	      if (*interp->result != 0)
              {printf("%s\n", interp->result);}
 	   }

   	   lineEntered = 0;

   } /* while(1)... */

}


/*
 *----------------------------------------------------------------------
 *
 * SetPromptVar --
 *     Set one of the prompt hook variables, saving a copy of the old
 *     value, if it exists.
 *
 * Parameters:
 *   o hookVarName (I) - The name of the prompt hook, which is an element
 *     of the TCLENV array.  One of topLevelPromptHook or downLevelPromptHook.
 *   o newHookValue (I) - The new value for the prompt hook.
 *   o oldHookValuePtr (O) - If not NULL, then a pointer to a copy of the
 *     old prompt value is returned here.  NULL is returned if there was not
 *     old value.  This is a pointer to a malloc-ed string that must be
 *     freed when no longer needed.
 * Result:
 *   TCL_OK if the hook variable was set ok, TCL_ERROR if an error occured.
 *----------------------------------------------------------------------
 */
static int
SetPromptVar (interp, hookVarName, newHookValue, oldHookValuePtr)
    Tcl_Interp *interp;
    char       *hookVarName;
    char       *newHookValue;
    char      **oldHookValuePtr;
{
    char *hookValue;    
    char *oldHookPtr = NULL;

    if (oldHookValuePtr != NULL) {
        hookValue = Tcl_GetVar2 (interp, "TCLENV", hookVarName, 
                                 TCL_GLOBAL_ONLY);
        if (hookValue != NULL) {
            oldHookPtr = ckalloc (strlen (hookValue) + 1);
            strcpy (oldHookPtr, hookValue);
        }
    }
    if (Tcl_SetVar2 (interp, "TCLENV", hookVarName, newHookValue, 
                     TCL_GLOBAL_ONLY | TCL_LEAVE_ERR_MSG) == NULL) {
        if (oldHookPtr != NULL)
            ckfree (oldHookPtr);
        return TCL_ERROR;
    }    
    if (oldHookValuePtr != NULL)
        *oldHookValuePtr = oldHookPtr;
    return TCL_OK;
}

/*
 *----------------------------------------------------------------------
 *
 * ftcl_CommandloopCmd --
 *     Implements the TCL commandloop command:
 *       commandloop prompt prompt2
 *
 * Results:
 *     Standard TCL results.
 *
 *----------------------------------------------------------------------
 */
int
ftcl_CommandloopCmd(clientData, interp, argc, argv)
    ClientData  clientData;
    Tcl_Interp *interp;
    int         argc;
    char      **argv;
{
    char *oldTopLevelHook  = NULL;
    char *oldDownLevelHook = NULL;
    int   result = TCL_ERROR;

    if (argc > 3) {
        Tcl_AppendResult (interp, "wrong # args: ", argv[0],
                          " [prompt] [prompt2]", (char *) NULL);
        return TCL_ERROR;
    }
    if (argc > 1) {
        if (SetPromptVar (interp, "topLevelPromptHook", argv[1],
                          &oldTopLevelHook) != TCL_OK)
            goto exitPoint;
    }
    if (argc > 2) {
        if (SetPromptVar (interp, "downLevelPromptHook", argv[2], 
                          &oldDownLevelHook) != TCL_OK)
            goto exitPoint;
    }

    ftcl_CommandLoop (interp, stdin, stdout, tclShellCmdEvalProc, 0);

    if (oldTopLevelHook != NULL)
        SetPromptVar (interp, "topLevelPromptHook", oldTopLevelHook, NULL);
    if (oldDownLevelHook != NULL)
        SetPromptVar (interp, "downLevelPromptHook", oldDownLevelHook, NULL);
        
    result = TCL_OK;
exitPoint:
    if (oldTopLevelHook != NULL)
        ckfree (oldTopLevelHook);
    if (oldDownLevelHook != NULL)
        ckfree (oldDownLevelHook);
    return result;
}
