/*
 * NAME: dtcl.c
 * PURPOSE: Higher level routines for declaring tcl commands that use
 *          ftcl_ParseArgv and for producing help and usage strings.
 *          These implement a specific format for help and usage strings.
 *          In general, projects should write their own versions of these
 *          routines using the "desired" format for the given project.
 *
 * PROGRAMMER: Laura Mengel
 * DATE: 5/19/94
 *
 * SCCS release:
 *      %Z% %M% %I% Delta: %E% %U% Extraction: %D% %T% %Z%
 *
 * ENVIRONMENT: ANSI C, callable from C++, POSIX compliant system calls
 *
 *
 * MODIFICATIONS:
 *
 *	16-SEP-1994	C.Moore 	Added local copy of the process
 *					Tcl interpreter to be passed to 
 *					the ftcl_GetUsage and GetArgInfo 
 *					routines in dart_get_usage and
 *					dart_get_help.
 *
 *      20-SEP-1994     L. Mengel       Changed routines to use tcl interp
 *                                      from caller instead of trying to
 *                                      use local one that may not exist.
 *
 *	13-oct-1995	M.Vittone	Added conditional when building a subset
 *					library for VxWorks; we need dart_declare
 *					but not the functions that use the help.
 * ROUTINES:
 *            dart_declare
 *            dart_get_help
 *            dart_get_usage
 */

#include <tcl.h>
#include <ftcl.h>

/* NAME: dart_declare
 * PURPOSE: bind a tcl verb to a C routine and invoke that tcl verb with the
 *          -help option. IMPORTANT: The tcl verb is expected to understand 
 *          the "-help" (either by use of the default argtable or by defining
 *          -help in its own argtable). Upon receiving the -help switch,
 *          the tcl verb is expected to define ftcl help for the verb (using 
 *          ftclDefineHelp) if ftcl help has not already been defined for that
 *          verb (!IsFtclHelpDefined). If help has already been defined for
 *          the tcl verb, then the help message should just be put in 
 *          interp->result. (i.e. Call dart_get_help or use dart_get_help
 *          as an example.)
 *
 *          Having the tcl verb itself do ftclHelpDefine avoids the need for
 *          a global argTable. dart_declare simply "forces" an initial call
 *          so that the verb can define help for itself before the user
 *          is put in to the tcl command loop. In this way, the user is able
 *          to get help about the verb, without first having to use it.
 *
 * PARAMETERS:
 *             interp     - tcl interpreter to create new verb in
 *             cmdName    - name of new tcl verb
 *             proc       - C routine to bind to it
 *             ClientData - clientdata to pass as arg to the C routine
 *             deleteproc - C routine to call when tcl verb is removed
 *             facility   - string to use as facility name (unused now)
 *
 *             (See man page for Tcl_CreateCommand for more detailed 
 *             description of other arguments.)
 *
 * RETURNS:  0 on success
 *          -1 on failure
 */

int 
dart_declare(Tcl_Interp *interp, char *cmdName, Tcl_CmdProc *proc,
             ClientData clientData, Tcl_CmdDeleteProc *deleteProc,
             char *facility)
{
    Tcl_CreateCommand(interp, cmdName, proc, clientData, deleteProc);

    if (TCL_OK != Tcl_VarEval(interp, cmdName, " -help", (char *) NULL))
        return -1;

    return 0;
}

#ifndef SUBSET

/* NAME: dart_get_usage
 * PURPOSE: return a ptr to a usage string for the given argTable
 *          in a standard DART format.
 *
 * PARAMETERS:
 *             interp    - tcl interpreter
 *             argTable  - description of command line arguments
 *             flags     - flags to control command line parsing and help
 *             cmd_name  - name of command to provide usage for
 *
 *             (See man page for ftcl_ParseArgv for more detailed
 *             description of argTable and flags arguments)
 *
 * RETURNS: ptr to constructed usage message. This space is re-used
 *          so it is overwritten on the next call to dart_get_usage,
 *          dart_get_help, or ftcl_GetUsage. (i.e. Copy it if you
 *          need to keep it around.) NULL is returned if an error
 *          is encountered.
 */

char *
dart_get_usage(Tcl_Interp *interp, ftclArgvInfo *argTable, 
               int flags, char *cmd_name)
{
    return (ftcl_GetUsage(interp,argTable,flags,cmd_name,0,"\nUsage:","\n"));
}

/* NAME: dart_get_help
 * PURPOSE: return a ptr to a help string for the given argTable
 *          in a standard DART format.
 *
 * PARAMETERS:
 *             interp    - tcl interpreter
 *             argTable  - description of command line arguments
 *             flags     - flags to control command line parsing and help
 *             cmd_name  - name of command to provide help for
 *             facility  - string to use as facility name for ftclHelpDefine
 *
 *             (See man page for ftcl_ParseArgv for more detailed
 *             description of argTable and flags arguments)
 *
 * RETURNS: ptr to constructed help message. This space is re-used
 *          so it is overwritten on the next call to dart_get_help,
 *          (i.e. Copy it if you need to keep it around.) NULL is 
 *          returned if an error is encountered.
 */

char *
dart_get_help(Tcl_Interp *interp, ftclArgvInfo *argTable, 
              int flags, char *cmd_name, char *facility)
{
    char *help_str;                 /* for ptr to constructed help string */
    int help_defined_flag = 0;      /* flag if ftcl help already defined
                                     * for this verb
                                     */

    help_str = ftcl_GetHelp(interp, argTable, flags, cmd_name, 8,
                            "\n        Usage:", "\n", "");
    if (NULL == help_str)
        return NULL;

    help_defined_flag = ftclHelpIsDefined(interp, facility, cmd_name);

    /* clear result left by ftclHelpIsDefined */
    Tcl_ResetResult(interp);

    if (!help_defined_flag)
        {
        if (1 != ftclHelpDefine(interp, facility, cmd_name, help_str))
            return NULL; 
        }

    return help_str;
}
#endif		/* On subset */
