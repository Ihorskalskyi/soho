/* ========================================================================== */
/* ftclCreateCommands.c 						      *
 *									      *
 *	Function to declare fermi functions, and add onbe line help	      *
 *	This function must be called in the user's main program		      *
 *									      *
 *							mvw,jms 6jan93	      *
 *									      *
 *	Adding parameter handling commands		mvw,jms 9jun93	      *
 *									      *
 * ========================================================================== */

#include "ftcl.h"
/* ------------------------------------------------------------------------- */
int ftclCreateCommands(Tcl_Interp *interp)
{

  Tcl_CreateCommand(interp,"echo",ftcl_EchoCmd,(ClientData) 0,
        (Tcl_CmdDeleteProc *) NULL);

  Tcl_CreateCommand(interp,"echonn",ftcl_EchonnCmd,(ClientData) 0,
        (Tcl_CmdDeleteProc *) NULL);

  Tcl_CreateCommand(interp, "ftclParseArg", ftclParseArg,
     (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL);
  Tcl_CreateCommand(interp, "ftclPassed", ftclPassed,
     (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL);
  Tcl_CreateCommand(interp, "ftclGetUsage", ftclGetUsage,
     (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL);
  Tcl_CreateCommand(interp, "ftclGetArgInfo", ftclGetArgInfo,
     (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL);
  Tcl_CreateCommand(interp, "ftclGetHelp", ftclGetHelp,
     (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL);

#ifdef VXWORKS
  ftclVxSystemCmdDeclare(interp, "exec");

/* To have cmdtrace command defined		(from tclX)		     */
  Tcl_InitDebug(interp);

/* To have the keyed lists commands defined	(from tclX)		     */

  Tcl_CreateCommand(interp, "keyldel", Tcl_KeyldelCmd,
                     (ClientData)NULL, (void (*)())NULL);
  Tcl_CreateCommand(interp, "keylget", Tcl_KeylgetCmd,
                     (ClientData)NULL, (void (*)())NULL);
  Tcl_CreateCommand(interp, "keylset", Tcl_KeylsetCmd,
                     (ClientData)NULL, (void (*)())NULL);
 

#else
   Tcl_CreateCommand(interp,"envscan",ftclEnvscan,(ClientData) 0,
	(Tcl_CmdDeleteProc *) NULL);

#endif

ftclAliasInit(interp);

/* Creating commands for parameters handling			*/

#ifndef VXWORKS

   Tcl_CreateCommand (interp,"inipar",ftclInitParam,(ClientData) 0,
	(Tcl_CmdDeleteProc *) NULL);
   Tcl_CreateCommand (interp,"getparf",ftclGetParamF,(ClientData) 0,
	(Tcl_CmdDeleteProc *) NULL);
   Tcl_CreateCommand (interp,"getpari",ftclGetParamI,(ClientData) 0,
	(Tcl_CmdDeleteProc *) NULL);
   Tcl_CreateCommand (interp,"getpars",ftclGetParamS,(ClientData) 0,
	(Tcl_CmdDeleteProc *) NULL);

#endif
#ifdef VMS
   ftclCreateDCLCmd(interp);
#endif

   return;
}
