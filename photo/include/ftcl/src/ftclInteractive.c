/* ========================================================================== *
 * ftclInteractive.c 						      	      *
 *									      *
 *	Function to set tcl global variables tcl_interactive                  *
 *      and programName to be able to ahvve the application name 	      *
 *      as a prompt and have the command line editing feature                 *
 *									      *
 *							mvw, may 11th,95      *
 *									      *
 * ========================================================================== */

#include "ftcl.h"


int ftcl_Interactive (Tcl_Interp *interp, int argc, char **argv)
{

    char   *scanPtr, *programName;

/* Determine file name of the application without the directory */

    scanPtr = programName = argv[0];
    while (*scanPtr != '\0') {
        if (*scanPtr == '/')
            programName = scanPtr + 1;
        scanPtr++;
    }

    if ( Tcl_SetVar(interp, "tcl_interactive","1", TCL_GLOBAL_ONLY) == NULL)
	return (TCL_ERROR);
    if ( Tcl_SetVar(interp, "programName",programName, TCL_GLOBAL_ONLY) == NULL)
	return (TCL_ERROR);

    return(TCL_OK);
}
