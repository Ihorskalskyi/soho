/*
 * ftcl_ParseArgv.c - Contains user-callable routine to parse command line
 *              options. Heavily inspired by the Tk command line parser.
 *
 * Definitions:
 * Switches              : command line arguments preceeded by a '-'. May or
 *                         may not take an argument. Example: 
 *                         -toggle           # No argument
 *                         -debugLevel 10    # Needs an extra argument
 * positional parameters : command line arguments where position matters. Are
 *                         NOT preceeded by a '-'. 
 *
 * For a complete list of rules, as they were agreed upon in the meeting,
 * please see the man page.
 *
 * Vijay K. Gurbani
 * Fermi National Accelerator Laboratory
 * Batavia, Illinois
 *
 * Usage routines (ftcl_GetUsage, ftcl_GetArgInfo, ftcl_MakeUsage, 
 * ftcl_MakeArgInfo) modified by Laura Appleton
 *
 * Public Functions:
 *
 * ftcl_ParseArgv()  - Parse command line options
 * ftcl_GetUsage()   - Get the usage string for a command
 * ftcl_GetArgInfo() - Get the ArgInfo string for a command
 *
 * $Author: jhendry $   $Date: 2003/02/10 21:20:53 $   $Revision: 1.1.1.1 $
 */
 
#ifdef VXWORKS
#include        "vxWorks.h"
#include        "stdioLib.h"
#include        "taskVarLib.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "ftcl.h"

/*
 * Default table of argument descriptors.  These are normally available
 * in every application.
 */

static ftclArgvInfo defaultTable[] = {
    {"-help",	FTCL_ARGV_HELP,	NULL,	NULL,
	"Print summary of command-line options and abort"},
    {NULL,	FTCL_ARGV_END,	NULL,	NULL, (char *) NULL}
};

/*
 * Forward declarations for procedures defined in this file:
 */

static int   ftcl_Pass1(Tcl_Interp *, int *, char **, 
                        ftclArgvInfo *, int);
static int   ftcl_Pass2(Tcl_Interp *, int *, char **, 
                        ftclArgvInfo *, int);
static int   assign_defaults(Tcl_Interp *, ftclArgvInfo *);
static char *ftcl_MakeUsage(Tcl_Interp *interp, ftclArgvInfo *argTable, 
                            int flags, char *cmd_name, int indent, 
                            char *usage_prefix, char *usage_suffix);
static char *ftcl_MakeArgInfo(Tcl_Interp *interp, ftclArgvInfo *argTable, 
                              int flags, char *cmd_name, int indent);
static int   ftcl_NeedHelp(const int, char **, const ftclArgvInfo *,
                           const int);   
/*
 * There is one global variable, g_Specified_args. This variable is a pointer
 * to a string of all options/parameters that appear on the command line.
 * This variable is taskVar'ed for VxWorks.
 */
#define ARGSIZ  (BUFSIZ/2)
static char g_Specified_args[ARGSIZ];

/* define names of tcl array where ftcl_ParseArgv values will be stored 
 * and define the array element names for each value
 */
#define FTCL_PARSE_TCLARRAY   "FTCL_PARSE"
#define FTCL_USAGE_TCLVAR     "USAGE"
#define FTCL_ARGINFO_TCLVAR   "ARGINFO"
#define FTCL_HELP_TCLVAR      "HELP"

/* define flags to use in usage/arginfo/help Tcl_SetVar */

#define FTCL_SETVAR_FLAGS    (TCL_GLOBAL_ONLY)
#define FTCL_APPENDVAR_FLAGS (TCL_APPEND_VALUE | TCL_GLOBAL_ONLY)


/*
 * ROUTINE:
 *    ftcl_ParseArgv
 *
 *      Process an argv array according to a table of expected
 *      command-line options.  See the manual page for more details.
 *
 * CALL:
 *   (int ) ftcl_ParseArgv(Tcl_Interp *interp, int *argcPtr, char **argv, 
 *                         shArgvInfo *argTable, int flags)
 *            interp   - TCL Interpreter
 *            argcPtr  - Pointer to the argument count address
 *            argv     - Command line argument array
 *            argTable - Table of expected command-line options
 *            flags    - Flags to modify the behavior while parsing
 *                       command line options.
 *    Note: parameters marked by # will be changed as side effects.
 *
 * DESCRIPTION:
 *   Processes an argv array according to a table of expected command
 *   line options. See the man page for more details (man page should
 *   be somewhere under $FTCL_DIR)
 *
 * RETURNS:
 *    FTCL_ARGV_SUCCESS   : if command-line options parsed successfully
 *    FTCL_ARGV_GIVEHELP  : if user gave option to request help for command
 *    FTCL_ARGV_BADSYNTAX : on error. Reason for error is put in interp->result
 */
int
ftcl_ParseArgv(interp, argcPtr, argv, argTable, flags)
    Tcl_Interp *interp;		/* Place to store error message. */
    int *argcPtr;		/* Number of arguments in argv.  Modified
				 * to hold # args left in argv at end. */
    char **argv;		/* Array of arguments.  Modified to hold
				 * those that couldn't be processed here. */
    ftclArgvInfo *argTable;	/* Array of option descriptions */
    int flags;			/* Or'ed combination of various flag bits,
				 * such as FTCL_ARGV_NO_DEFAULTS. */
{
   int retValue, length;
   register ftclArgvInfo *ptr;
   int allowed_left;
   /*
    * Populate g_Specified_args, the buffer that holds all recognized command
    * line options and  parameters. We want to make sure that we do not
    * overflow it. So lets count all characters in the options/parameters
    * specified in the argument table. Their count should be <= ARGSIZ - 1 
    * (since g_Specified_args is dimensioned for ARGSIZ)
    */
   for (length = 0, ptr = argTable; ptr->type != FTCL_ARGV_END; ptr++)
        if (ptr->key)
            length += strlen(ptr->key) + 1;


   if (length >= ARGSIZ)  {
       printf("Internal buffer overflow in ftcl_ParseArgv(). Please notify "
              "ftcl maintainer!\n");
       /*
        * If this message is encountered, increase size of g_Specified_args
        * and recompile.
        */
       abort();
   }

   /*
    * If the user needs help for the command, bail out right away. We do not
    * want to modify the argument table, if the user simply needs help.
    */
   if (ftcl_NeedHelp(*argcPtr, argv, argTable, flags))
       return FTCL_ARGV_GIVEHELP;

#if VXWORKS
       if (taskVarGet(0, g_Specified_args) == ERROR)  {
           taskVarDelete(0, g_Specified_args);
           taskVarAdd(0, g_Specified_args);
       }
#endif

   g_Specified_args[0] = NULL;

    /*
     * Pass 1 gets all the switches from argTable[]. Switches are defined
     * as tokens on the command line that are preceeded by a '-' and
     * may or may not use the next token as an argument. Examples:
     *
     * -toggle          # Option that does not have any argument
     * -name my.file    # Option that takes a string argument
     * -d 10            # Option that takes an integer argument
     */
    retValue = ftcl_Pass1(interp, argcPtr, argv, argTable, flags);

    if (FTCL_ARGV_SUCCESS == retValue)
       /*
        * Pass 2 extracts all the positional parameters from argTable[].
        * These parameters may be required, or optional.
        */
       retValue = ftcl_Pass2(interp, argcPtr, argv, argTable, flags);
 
    if (FTCL_ARGV_SUCCESS == retValue)  {
       if (flags &  FTCL_ARGV_DONT_SKIP_FIRST_ARG)
          allowed_left = 0;
       else 
          allowed_left = 1;
       if (flags & FTCL_ARGV_NO_LEFTOVERS)  {
           if (*argcPtr > allowed_left)  {         
               /*
                * If the user did not want any leftovers, but there's still
                * stuff left in argv[], flag that as an error. Also, note
                * that I use Tcl_SetResult() as opposed to Tcl_AppendResult(),
                * since we want to discard previous error messages in 
                * interp->result, and only return this error message.
                */
               Tcl_SetResult(interp,    
                  "Syntax Error: unprocessed command line parameters remain \n(check for extra parameters or invalid options)", TCL_VOLATILE);
               retValue = FTCL_ARGV_BADSYNTAX;
	   }
       }
   }

   return retValue;
}

/*
 * NAME
 *   ftcl_NeedHelp - See if the user needs help for the command
 *
 * CALL:
 *   (static int) ftcl_NeedHelp(const int argc, char **argv,
 *                              const ftclArgvInfo *argTable, const int flags)
 *                a_argc   - Argument count
 *                argv     - Command line argument array
 *                argTable - Table of known arguments
 *                flags    - Flags that modify the processing of argTable[]
 *
 * DESCRIPTION:
 *   ftcl_NeedHelp() goes thru argv and matches each argument against 
 *   argTable[] to see if the user specified a command line argument that
 *   matches a FTCL_ARGV_HELP key in argTable[]. Ordinarily, the search being
 *   done here could be accomplished in ftcl_Pass1(). But that function
 *   changes the state of the argument table by assigning values to
 *   the destination fields. The help needs to be created from a pristine
 *   table.
 *
 * RETURNS:
 *   1 : on success
 *   0 : otherwise
 */
static int ftcl_NeedHelp(const int a_argc, char **argv, 
                         const ftclArgvInfo *argTable, const int flags)
{
    register ftclArgvInfo *infoPtr;
				/* Pointer to the current entry in the
				 * table of argument descriptions. */
    ftclArgvInfo *matchPtr;	/* Descriptor that matches current argument. */
    char *curArg;		/* Current argument */
    register char c;		/* Second character of current arg (used for
				 * quick check for matching;  use 2nd char.
				 * because first char. will almost always
				 * be '-'). */
    int length;			/* Number of characters in current argument. */
    int srcIndex = 0;
    int argc;
    int i;

    argc = a_argc;
 
    /*
     * Do the real parsing...
     */
    while (argc > 0) {
	curArg = argv[srcIndex++];
	argc--;
	c = curArg[1];
	length = strlen(curArg);
	if (length == 0) {
	  /*
	   * Someone passed a NULL string.  Just move on.
	   */
	   continue;
	}

	/*
	 * Loop throught the argument descriptors searching for one with
	 * the matching key string.  If found, leave a pointer to it in
	 * matchPtr.
	 */

	matchPtr = NULL;
	for (i = 0; i < 2; i++) {
	    if (i == 0) {
		infoPtr = (ftclArgvInfo *) argTable;
	    } else {
                if (!(flags & FTCL_ARGV_NO_DEFAULTS))
		    infoPtr = defaultTable;
                else
                    continue;
	    }
	    for (; infoPtr->type != FTCL_ARGV_END; infoPtr++) {
		 if (infoPtr->key == NULL) {
		     continue;
		 }

                 /*
                  * We're only interested in FTCL_ARGV_HELP
                  */
                 if (infoPtr->type != FTCL_ARGV_HELP)
                     continue;

		 if ((infoPtr->key[1] != c)
			 || (strncmp(infoPtr->key, curArg, length) != 0)) {
		     continue;
		 }
		 if (infoPtr->key[length] == 0) {
		     matchPtr = infoPtr;
		     goto gotMatch;
		 }
		 if (flags & FTCL_ARGV_NO_ABBREV) {
		     continue;
		 }
		 if (matchPtr != NULL)
		     return 0;

		 matchPtr = infoPtr;
	    }
        }

	if (matchPtr == NULL)
	    continue;

	/*
	 * Take the appropriate action based on the option type. Of course,
         * here we are only interested in the FTCL_ARGV_HELP option. If it's
         * found, return immedeately.
	 */
gotMatch:
	infoPtr = matchPtr;
	switch (infoPtr->type) {
	    case FTCL_ARGV_CONSTANT:
	    case FTCL_ARGV_INT:
	    case FTCL_ARGV_STRING:
            case FTCL_ARGV_DOUBLE :
            case FTCL_ARGV_FUNC:
		break;
	    case FTCL_ARGV_HELP:
                return 1;
	    default:
                break;
	}

    }

    return 0;
}

/*
 * NAME
 *   ftcl_Pass1 - Do a first pass through argv[], weeding out all known 
 *                switches specified in argTable[].
 *
 * CALL: 
 *   (static int) ftcl_Pass1(Tcl_Interp *interp, int *argcPtr,
 *                           char **argv, ftclArgvInfo *argTable, int flags)
 *                interp   - TCL Interpreter
 *                argcPtr  - Pointer to the address of argument count
 *                argv     - Command line argument array
 *                argTable - Table of known arguments
 *                flags    - Flags that modify the processing of argTable[]
 *
 * DESCRIPTION:
 *   ftcl_Pass1() performs a first pass through the command line arguments
 *   specified in argv[]. As it goes thru it's motions, it strips out all
 *   known arguments. Known arguments are specified in argTable[]. As an
 *   argument is recognized and processed, it is removed from argv[]. 
 *   *argcPtr will be decremented to account for this. Only switches
 *   are weeded out in this pass. Positional parameters are searched for 
 *   in the next pass.
 *
 *   Example - Consider the following command line argument:
 *
 *      myCommand reg1 20 -debugLevel 3 -useMalloc -delay 10 -o
 *
 *   Also consider that argTable[] knows about the following arguments
 *   <reg1>       - Positional parameter
 *   [<numCols>]  - Optional positional parameter
 *   -debugLevel  - Switch that takes an argument
 *   -useMalloc   - Switch that takes no argument
 *
 *   After a call to this function, argv[] will look like:
 *              argv[0] ---> myCommand
 *              argv[1] ---> reg1
 *              argv[2] ---> 20
 *              argv[3] ---> -delay
 *              argv[4] ---> 10
 *              argv[5] ---> -o
 *   argcPtr will have been modified to contain 6.
 *
 * RETURNS:
 *    FTCL_ARGV_SUCCESS   : if command-line options parsed successfully
 *    FTCL_ARGV_GIVEHELP  : if user gave option to request help for command
 *    FTCL_ARGV_BADSYNTAX : on error. Reason for error is put in interp->result
 */
static int
ftcl_Pass1(Tcl_Interp *interp, int *argcPtr, char **argv, 
           ftclArgvInfo *argTable, int flags)
{
    register ftclArgvInfo *infoPtr;
				/* Pointer to the current entry in the
				 * table of argument descriptions. */
    ftclArgvInfo *matchPtr;	/* Descriptor that matches current argument. */
    char *curArg;		/* Current argument */
    register char c;		/* Second character of current arg (used for
				 * quick check for matching;  use 2nd char.
				 * because first char. will almost always
				 * be '-'). */
    int srcIndex;		/* Location from which to read next argument
				 * from argv. */
    int dstIndex;		/* Index into argv to which next unused
				 * argument should be copied (never greater
				 * than srcIndex). */
    int argc;			/* # arguments in argv still to process. */
    int length;			/* Number of characters in current argument. */
    int i;
    char tmpKeyBuf[30];

   if (flags & FTCL_ARGV_DONT_SKIP_FIRST_ARG)  {
       srcIndex = dstIndex = 0;
       argc = *argcPtr;
   } else  {
       srcIndex = dstIndex = 1;
       argc = *argcPtr - 1;
   }

   if (assign_defaults(interp, argTable) == 0)
       return FTCL_ARGV_BADSYNTAX;

    /*
     * Do the real parsing...
     */
    while (argc > 0) {
	curArg = argv[srcIndex];
	srcIndex++;
	argc--;
	c = curArg[1];
	length = strlen(curArg);
	if (length == 0) {
	  /*
	   * Someone passed a NULL string.  Just move on.
	   */
	  argv[dstIndex] = curArg;
	  dstIndex++;
	  continue;
	}

	/*
	 * Loop throught the argument descriptors searching for one with
	 * the matching key string.  If found, leave a pointer to it in
	 * matchPtr.
	 */

	matchPtr = NULL;
	for (i = 0; i < 2; i++) {
	    if (i == 0) {
		infoPtr = argTable;
	    } else {
                if (!(flags & FTCL_ARGV_NO_DEFAULTS))
		    infoPtr = defaultTable;
                else
                    continue;
	    }
	    for (; infoPtr->type != FTCL_ARGV_END; infoPtr++) {
		 if (infoPtr->key == NULL) {
		     continue;
		 }
		 if ((infoPtr->key[1] != c)
			 || (strncmp(infoPtr->key, curArg, length) != 0)) {
		     continue;
		 }
		 if (infoPtr->key[length] == 0) {
		     matchPtr = infoPtr;
		     goto gotMatch;
		 }
		 if (flags & FTCL_ARGV_NO_ABBREV) {
		     continue;
		 }
		 if (matchPtr != NULL) {
		     Tcl_AppendResult(interp, "Syntax Error: ambiguous option \"", 
                             curArg, "\"", (char *) NULL);
		     return FTCL_ARGV_BADSYNTAX;
		 }
		 matchPtr = infoPtr;
	    }
	}
	if (matchPtr == NULL) {

	    /*
	     * Unrecognized argument.  Just copy it down
	     */
	    argv[dstIndex] = curArg;
	    dstIndex++;
	    continue;
	}

	/*
	 * Take the appropriate action based on the option type
	 */

	gotMatch:
	infoPtr = matchPtr;
	switch (infoPtr->type) {
	    case FTCL_ARGV_CONSTANT:
		*((int *) infoPtr->dst) = (int) infoPtr->src;
                sprintf(tmpKeyBuf, "%s,", infoPtr->key);
                strcat(g_Specified_args, tmpKeyBuf);
		break;
	    case FTCL_ARGV_INT:
		if (argc == 0) {
		    goto missingArg;
		} else {
		    char *endPtr;

		    *((int *) infoPtr->dst) =
			    strtoul(argv[srcIndex], &endPtr, 0);
		    if ((endPtr == argv[srcIndex]) || (*endPtr != 0)) {
			Tcl_AppendResult(interp, "Syntax Error: expected integer argument ",
				"for \"", infoPtr->key, "\" but got \"",
				argv[srcIndex], "\"", (char *) NULL);
			return FTCL_ARGV_BADSYNTAX;
		    }
		    srcIndex++;
		    argc--;
		  }
                sprintf(tmpKeyBuf, "%s,", infoPtr->key);
                strcat(g_Specified_args, tmpKeyBuf);
		break;
	    case FTCL_ARGV_STRING:
		if (argc == 0) {
		    goto missingArg;
		} else {
		    *((char **)infoPtr->dst) = argv[srcIndex];
		    srcIndex++;
		    argc--;
		}
                sprintf(tmpKeyBuf, "%s,", infoPtr->key);
                strcat(g_Specified_args, tmpKeyBuf);
		break;
            case FTCL_ARGV_DOUBLE :
		if (argc == 0) {
		    goto missingArg;
		} else {
		    char *endPtr;

		    *((double *) infoPtr->dst) =
			    strtod(argv[srcIndex], &endPtr);
		    if ((endPtr == argv[srcIndex]) || (*endPtr != 0)) {
			Tcl_AppendResult(interp, "Syntax Error: expected floating-point ",
				"argument for \"", infoPtr->key,
				"\" but got \"", argv[srcIndex], "\"",
				(char *) NULL);
			return FTCL_ARGV_BADSYNTAX;
		    }
		    srcIndex++;
		    argc--;
		}
                sprintf(tmpKeyBuf, "%s,", infoPtr->key);
                strcat(g_Specified_args, tmpKeyBuf);
		break;
	    case FTCL_ARGV_FUNC: {
		int (*handlerProc)();

		handlerProc = (int (*)())infoPtr->src;
		
		if ((*handlerProc)(interp, infoPtr->dst, infoPtr->key,
			argv[srcIndex])) {
		    srcIndex += 1;
		    argc -= 1;
		}
                sprintf(tmpKeyBuf, "%s,", infoPtr->key);
                strcat(g_Specified_args, tmpKeyBuf);
		break;
	    }
	    default:
		sprintf(interp->result, "Programmer Error: bad argument type %d in ftclArgvInfo",
			infoPtr->type);
		return FTCL_ARGV_BADSYNTAX;
	}
    }

    /*
     * Copy the remaining arguments down.
     */
    while (argc > 0) {
	argv[dstIndex] = argv[srcIndex];
	srcIndex++;
	dstIndex++;
	argc--;
    }
    argv[dstIndex] = (char *) NULL;
    *argcPtr = dstIndex;

    return FTCL_ARGV_SUCCESS;

    missingArg:
    Tcl_AppendResult(interp, "Syntax Error: \"", curArg,
	    "\" option requires an additional argument", (char *) NULL);
    return FTCL_ARGV_BADSYNTAX;
}

/*
 * NAME
 *   ftcl_Pass2 - Do a second pass through argv[], weeding out all known
 *                positional parameters specified in argTable[].
 *
 * CALL:
 *   (static int) ftcl_Pass2(Tcl_Interp *interp, int *argcPtr,
 *                           char **argv, ftclArgvInfo *argTable, int flags)
 *                interp   - TCL Interpreter
 *                argcPtr  - Pointer to the address of argument count
 *                argv     - Command line argument array
 *                argTable - Table of known arguments
 *                flags    - Flags that modify the processing of argTable[]
 *
 * DESCRIPTION:
 *   ftcl_Pass2() performs a second pass through the command line arguments
 *   specified in argv[]. Note that this argv[] has been (probably) modified
 *   by ftcl_Pass1(). Thus all known switches have been stripped out of argv.
 *   As ftcl_Pass2() goes thru it's motions, it strips out all positional
 *   parameters. Positional parameters are specified in argTable[]. As an
 *   parameter is recognized and processed, it is removed from argv[].
 *   *argcPtr will be decremented to account for this. 
 *
 *   Example - Working on the command line argument from the comments in
 *             ftcl_Pass1(), let's assume argv[] is now the following:
 *
 *           myCommand reg1 20 -delay 10 -o
 *
 *   i.e. all known switches have been removed from it.
 *
 *   Also consider that argTable[] knows about the following arguments
 *   <reg1>       - Positional parameter
 *   [<numCols>]  - Optional positional parameter
 *   -debugLevel  - Switch that takes an argument   # removed in ftcl_Pass1()
 *   -useMalloc   - Switch that takes no argument   # removed in ftcl_Pass1()
 *
 *   After a call to this function, argv[] will look like:
 *              argv[0] ---> myCommand
 *              argv[3] ---> -delay
 *              argv[4] ---> 10
 *              argv[5] ---> -o
 *   argcPtr will have been modified to contain 6.
 *
 * RETURNS:
 *   FTCL_ARGV_SUCCESS    : on success
 *   FTCL_ARGV_BADSYNTAX  : otherwise. Reason will be saved in interp->result
 */
static int
ftcl_Pass2(Tcl_Interp *interp, int *argcPtr, char **argv,
           ftclArgvInfo *argTable, int flags)
{
   register ftclArgvInfo *infoPtr;
   int      reqPosParams,          /* Count of required positional params */
            optPosParams,          /* Count of optional positional params */
            i;
   short    flag;                  /* General purpose flag */
   char     *sp,                   /* Utility pointer used in various places */
            tmpBuf[30];            /* Temporary buffer area used many places */
   int start_pos;
   reqPosParams = 0;
   optPosParams = 0;
   if (flags & FTCL_ARGV_DONT_SKIP_FIRST_ARG)
      start_pos=0;
   else
      start_pos=1;
   flag = 0;
   /*
    * Do some housekeeping first: see how many positional parameters
    * are needed.
    */
   for (infoPtr = argTable; infoPtr->type != FTCL_ARGV_END; infoPtr++)  {
        if (infoPtr->type == FTCL_ARGV_HELP)
            continue;

        if (infoPtr->key == NULL)  {
            flag = 1;
            break;
	}

        if (*infoPtr->key == '-')
            continue;

        if (*infoPtr->key == '<')
            reqPosParams++;
        else if (*infoPtr->key == '[')
            optPosParams++;
        else  {
            flag = 1;
            break;
        }
   }

   if (flag)  {
       char *p;

       p = (infoPtr->key == NULL ? "NULL" : infoPtr->key);

       Tcl_AppendResult(interp, "Programmer Error: \"", p, "\" - ",
          "argument table key must be non-null and begin with\n",
          "either '<', '[', or '-' only\n", (char *) NULL);
       return FTCL_ARGV_BADSYNTAX;
   }
 
   if (reqPosParams == 0 && optPosParams == 0) /* No sense sticking around */
       return FTCL_ARGV_SUCCESS;               /* if pos. params. not needed */
 
   if (reqPosParams && *argcPtr <= start_pos)  {
       Tcl_AppendResult(interp,
          "Syntax Error: expected at least one positional parameter", (char *) NULL);
       return FTCL_ARGV_BADSYNTAX;
   }

   if (*argcPtr <= start_pos)   /* Boundary case: if by this pass only argv[0]*/
       return FTCL_ARGV_SUCCESS;                   /* remains, return now */
   /* Go thru the formal parameters */
   for (infoPtr = argTable; infoPtr->type != FTCL_ARGV_END; infoPtr++)  {

        if (infoPtr->key == NULL)
            continue;
        if (*infoPtr->key == '-')   /* Skip all switches */
            continue;
        /*
         * Now set argv[] to the first non-switch argument. A switch
         * argument is anything preceeding a '-'. While searching for 
         * the first non-switch argument, check the first character
         * beyond the '-'. If that character is an alpha, we have
         * a bonafide switch. If that character is a digit or it is a '.' we 
         * will recognize it as a positional parameter (a negative number).
         * Note that when we start searching argv[], we start from
         * argv[1], effectively skipping argv[0]
         */
        flag = 0;
        i=start_pos;
        for (; argv[i] != NULL; i++)  {
             if ((*argv[i] == '-') && !(flags&FTCL_ARGV_IGNORE_FLAGS) )  {
                 if (isdigit(argv[i][1]) || argv[i][1] == '.')  {
                     /*
                      * argv[i] could be specified as -0.99 or -.99
                      */
                     flag = 1;
                     break; 
		 }
	         else 
                 if (flags&FTCL_PARSEARG) 
                     break;
             }
             else
                break;
        }
      
        /*
         * Boundary condition: see if argv[i] == NULL. If it is, error
         * out. argv[i] should not be NULL if more positional parameters
         * are expected.
         */
        
        if (*infoPtr->key == '[')  {
            if (argv[i] == NULL)
                break;
        }
        else if (*infoPtr->key == '<')  {
            if (argv[i] == NULL)  {
                sprintf(tmpBuf, "%d", reqPosParams);
                Tcl_AppendResult(interp, "Syntax Error: expected ", tmpBuf, 
                   " more required positional parameters", (char *) NULL);
                return FTCL_ARGV_BADSYNTAX;
               
            }
            reqPosParams--;
	}

        /*
         * Now make the address passed in the table for this positional 
         * parameter point to argv[i]
         */
        switch (infoPtr->type)  {
                char keyBuf[50],
                     *pSp;

            case FTCL_ARGV_INT :
                 *((int *) infoPtr->dst) = strtoul(argv[i], &sp, 0);
                  if (sp == argv[i] || *sp != 0)  {
                      Tcl_AppendResult(interp,
                         "Syntax Error: expected integer positional parameter for ",
                         "\"", infoPtr->key, "\" but got \"",
                         argv[i], "\"", (char *) NULL);
                      return FTCL_ARGV_BADSYNTAX;
		  }
                  sprintf(keyBuf, "%s,", infoPtr->key);
                  strcat(g_Specified_args, keyBuf);
                  break;
            case FTCL_ARGV_DOUBLE :
                 *((double *) infoPtr->dst) = strtod(argv[i], &sp);
                 if (sp == argv[i] || *sp != 0)  {
                     Tcl_AppendResult(interp,
                        "Syntax Error: expected floating-point positional parameter",
                        " for \"", infoPtr->key, "\" but got \"",
                        argv[i], "\"", (char *) NULL);
                     return FTCL_ARGV_BADSYNTAX;
                 }
                 sprintf(keyBuf, "%s,", infoPtr->key);
                 strcat(g_Specified_args, keyBuf);
                 break;
            case FTCL_ARGV_STRING :
                 *((char **) infoPtr->dst) = argv[i];
                 sprintf(keyBuf, "%s,", infoPtr->key);
                 strcat(g_Specified_args, keyBuf);
                 break;
            case FTCL_ARGV_FUNC :
            case FTCL_ARGV_HELP :
            case FTCL_ARGV_CONSTANT :
                 /*
                  * None of these types can have positional parameters.
                  * Get rid of positional parameter delimiters '<...>' or
                  * '[...]' from infoPtr->key. Store the result in keyBuf[].
                  */
                 sprintf(keyBuf, "%s", &infoPtr->key[1]);
                 for (pSp = keyBuf; *pSp != NULL; pSp++)
                      if (*pSp == '>' || *pSp == ']')
                          *pSp = NULL;

                 Tcl_AppendResult(interp, "Programmer Error: \"", infoPtr->key, "\" ",
                    "is not a supported object for positional\n",
                    "parameters. Positional parameters must be of ",
                    "type FTCL_ARGV_INT,\nFTCL_ARGV_DOUBLE, or ",
                    "FTCL_ARGV_STRING only. Please modify ",
                    "your argument\ntable and declare \"", infoPtr->key,
                    "\" as \"-", keyBuf, "\" instead\n", 
                    (char *) NULL);
                  
                    return FTCL_ARGV_BADSYNTAX;
                 break; /* NOTREACHED */
	}
      
        /* 
         * Now comes the fun part: shuffle the argv[] array to account
         * for the positional parameter just handled above. Example:
         * assume that the argv[] array contained:
         *       a.out -X 10 -name Region 20 -test
         * and that 20 is the positional parameter. Once the code above
         * has been executed and 20 has been recognized, argv[] should
         * be compacted so that it looks like:
         *       a.out -X 10 -name Region -test
         */
        for (; argv[i] != NULL; i++)
             argv[i] = argv[i+1];
        argv[i] = NULL;
         
   }

   for (i = 0; argv[i] != NULL; i++)   /* Set *argcPtr to contain the number */
        ;                              /* of elements in argv[] */
   *argcPtr = i;
 
   return FTCL_ARGV_SUCCESS;
}

/*
 * NAME: assign_defaults()
 *
 * CALL:
 *   (int) assign_defaults(Tcl_Interp *a_interp, ftclArgvInfo *argTable)
 *         a_interp - TCL Interpreter
 *         argTable - Argument table
 *
 * DESCRIPTION:
 *   assign_defaults() goes through the argument table assigning default 
 *   values from the src field to the dst field. The src field contains a
 *   string representation of the default value. It may be NULL. If the src
 *   field is not NULL, the value in the field is converted to the appropriate 
 *   representation of dst and saved there (in dst).
 *
 *   If the user later specifies a different value for a command line 
 *   argument, the value of dst will be over-written with the new one.
 *
 * RETURNS: 
 *   1 : on success
 *   0 : on failure. TCL Interp will contain the result of the failure.
 */
static int
assign_defaults(Tcl_Interp *interp, ftclArgvInfo *argTable)
{
   register ftclArgvInfo *infoPtr;

   /*
    * Go thru the table assigning default values if specified. Default
    * values are specified in the src field. So, if the src field is not
    * NULL, convert the src field to the proper representation of the dst
    * field, and save that value in the dst field. 
    *
    * This way, if the user specifies switch value on the command line, the
    * default values can always be overwritten later. If the user did not
    * specify a command line value, it will be properly defaulted.
    */
   for (infoPtr = argTable; infoPtr->type != FTCL_ARGV_END; infoPtr++)  {
            char   *pSrc,
                   *pEnd;

            pSrc = (char *) infoPtr->src;

            switch (infoPtr->type)  {
               case FTCL_ARGV_STRING  :
                    if ((pSrc != NULL) && (infoPtr->dst != NULL))
                        *((char **)infoPtr->dst) = pSrc;
                    break;
               case FTCL_ARGV_CONSTANT :
               case FTCL_ARGV_FUNC     :
               case FTCL_ARGV_HELP     :
                    break;
               case FTCL_ARGV_INT      :
                    if ((pSrc != NULL) && (infoPtr->dst != NULL))
                    {
                        *((int *) infoPtr->dst) = strtoul(pSrc, &pEnd, 0);
                        if (pEnd == pSrc || *pEnd != 0)  {
                            Tcl_AppendResult(interp, 
                               "Parsing error: cannot convert default", 
                               " value \"", pSrc, "\" to an integer",
                               (char *) NULL);
                            return 0;
                        }
                    }
                    break;
   	       case FTCL_ARGV_DOUBLE   :
                    if ((pSrc != NULL) && (infoPtr->dst != NULL))
                    {
                        *((double *)infoPtr->dst) = strtod(pSrc, &pEnd);
                        if (pEnd == pSrc || *pEnd != 0)  {
                            Tcl_AppendResult(interp, 
                               "Parsing error: cannot convert default", 
                               " value \"", pSrc, "\" to a double",
                               (char *) NULL);
                            return 0;
                        }
                    }
                    break;
	    }
   }

   return 1;
}

/*
 * NAME: ftcl_ArgIsPresent
 *
 * CALL:
 *   (int) ftcl_ArgIsPresent(int argc, char **argv, 
 *                           char *arg, ftclArgvInfo *unused)
 *         argc     - Current argument count (Unused)
 *         argv     - Current argument array (Unused)
 *         arg      - Argument to search for
 *         argTable - Argument table. (Unused)
 *
 * DESCRIPTION:
 *   ftcl_ArgIsPresent() ascertains if a given switch/parameter was present
 *   on the command line. As of this not all the arguments to this function
 *   are used. However, I have decided to make the signature of this
 *   function as above in the eventuality that they are needed at some later 
 *   date.
 *
 * RETURNS:
 *   1 : if arg was present on the command line
 *   0 : if it was not
 *  -1 : if arg was not specified in the argument table
 *
 * CAVEATS:
 *   This function does not save the state of the table being parsed. So, if
 *    multiple tables are being used in the same function, all information
 *    for a table should be extracted before parsing the next table. That is,
 *    the caller should do something like so:
 *
 *   ftclArgvInfo Tbl_A, Tbl_B;
 *   ...
 *   ftcl_ParseArgv(..., Tbl_A, ...);
 *   var1 = ftcl_ArgIsPresent(...);
 *   var2 = ftcl_ArgIsPresent(...);
 *   ...
 *   ftcl_ParseArgv(..., Tbl_B, ...);
 *   var3 = ftcl_ArgIsPresent(...);
 *   var4 = ftcl_ArgIsPresent(...);
 *   ...
 *
 *   This function should be called AFTER the initial call to ftcl_ParseArgv().
 *
 *   Ostensibly, we can make the package infinitely more complex by introducing
 *   state information for a table. If so, then calls to ftcl_ParseArgv() and
 *   ftcl_ArgIsPresent() can be interspersed. But I don't think it's worth
 *   the time and effort to do so. Furthermore if we do so, we will have to
 *   provide APIs to release the state of a table. A user might forget to call
 *   the release API thus resulting in all these memory leaks. I think we 
 *   should subscribe to the KISS rule. The package is enough complex as it is.
 */
int
ftcl_ArgIsPresent(int argc, char **argv, char *arg, ftclArgvInfo *argTable)
{
   char *pSwitchArray,
         tmpBuf[50];
   int  retValue,
        i;

   if (strlen(g_Specified_args) == 0)
       return 0;

   retValue = i = 0;
   pSwitchArray = g_Specified_args;

   /*
    * g_Specified_args is an array  containing all the command line
    * switches/parameters passed to ftcl_ParseArgv(). It's contents will be 
    * of the form:
    *
    *    -switch1,-switch2,<param1>,-switch3,[param2],...,-switchn,
    *
    * Finding out if a certain command line switch/parameter was specified 
    * by the user now simply boils down to tokenizing this array and searching
    * each token for a match.
    */
   for (; *pSwitchArray != NULL; pSwitchArray++)  {
        if (*pSwitchArray == ',')  {
            tmpBuf[i] = NULL;
            if (strcmp(tmpBuf, arg) == 0)  {
                retValue = 1;
                break;
            } else  {
              i = 0;
            }
        } else  {
            tmpBuf[i++] = *pSwitchArray;
        }
   }  

   if (retValue == 0)  {
       /*
        * If arg was not specified on the command line, let's see if it was
        * present in the argument table. If it was not, we want to return
        * a -1 instead of a 0
        */
       register ftclArgvInfo *ptr;

       for (ptr = argTable; ptr->type != FTCL_ARGV_END; ptr++)
            if (ptr->key)
                if (strcmp(ptr->key, arg) == 0)
                     break;

       if (ptr->type == FTCL_ARGV_END)
           retValue = -1;
   }

   return retValue;
}

/* **** private defines for constructing usage, arginfo, and help **** */

#define FTCL_ARGV_INDENT_BUFSIZE 101

/* macros for accumulating usage, arginfo, and help strings */

/* NOTE: These macros needed if using string (instead of tcl variables)
 * to store/return usage, arginfo, and help strings
 */
#define FTCL_ARGV_APPENDRESULT(cur_str, new_str, cur_len, max_len) {  \
    if ((char *) NULL != new_str && (char *) NULL != cur_str) {       \
        cur_len += strlen(new_str);                                   \
        if (cur_len > max_len)                                        \
            return (char *) NULL;                                     \
        else                                                          \
            strcat(cur_str, new_str);                                 \
	}                                                             \
    }
#define FTCL_ARGV_APPENDUSAGE(new_str) { \
    FTCL_ARGV_APPENDRESULT(usage_str, new_str, usage_length, \
                           (FTCL_ARGV_USAGE_BUFSIZE - 1))    \
    }
#define FTCL_ARGV_APPENDARGINFO(new_str) {                       \
    FTCL_ARGV_APPENDRESULT(arginfo_str, new_str, arginfo_length, \
                           (FTCL_ARGV_ARGINFO_BUFSIZE - 1))      \
    }

/* NOTE: These macros needed if using tcl variables (instead of strings)
 * to store/return usage, arginfo, and help strings
 */
#define FTCL_ARGV_TCL_STORE(str, tclvar, flags) { \
    Tcl_SetVar2(interp, FTCL_PARSE_TCLARRAY, tclvar, str, flags); \
    }
/* this one leaves off the semicolon, so can use it as function arg */
#define FTCL_ARGV_TCL_GET(tclvar) Tcl_GetVar2(interp, FTCL_PARSE_TCLARRAY, \
                                              tclvar, FTCL_SETVAR_FLAGS)

#define FTCL_ARGV_TCL_STARTUSAGE(str) { \
    FTCL_ARGV_TCL_STORE(str, FTCL_USAGE_TCLVAR, FTCL_SETVAR_FLAGS) \
    }
#define FTCL_ARGV_TCL_APPENDUSAGE(str) { \
    FTCL_ARGV_TCL_STORE(str, FTCL_USAGE_TCLVAR, FTCL_APPENDVAR_FLAGS) \
    }
#define FTCL_ARGV_TCL_STARTARGINFO(str) { \
    FTCL_ARGV_TCL_STORE(str, FTCL_ARGINFO_TCLVAR, FTCL_SETVAR_FLAGS) \
    }
#define FTCL_ARGV_TCL_APPENDARGINFO(str) { \
    FTCL_ARGV_TCL_STORE(str, FTCL_ARGINFO_TCLVAR, FTCL_APPENDVAR_FLAGS) \
    }
#define FTCL_ARGV_TCL_STARTHELP(str) { \
    FTCL_ARGV_TCL_STORE(str, FTCL_HELP_TCLVAR, FTCL_SETVAR_FLAGS) \
    }
#define FTCL_ARGV_TCL_APPENDHELP(str) { \
    FTCL_ARGV_TCL_STORE(str, FTCL_HELP_TCLVAR, FTCL_APPENDVAR_FLAGS) \
    }

/*
 * NAME 
 *   ftcl_GetUsage - Public interface to private function ftcl_MakeUsage()
 *
 * CALL:
 *   char *ftcl_GetUsage(Tcl_Interp *interp, ftclArgvInfo *argTable, int flags, 
 *                       char *cmd_name, int indent, char *usage_prefix,
 *                       char *usage_suffix)
 *
 *          interp        - tcl interpreter
 *          argTable      - Table of known arguments
 *          flags         - Flags to modify processing of argTable[]
 *          cmd_name      - name of command (for usage string)
 *          indent        - num chars to indent usage line
 *          usage_prefix  - prefix to put before command syntax
 *          usage_suffix  - suffix to put after command syntax
 *
 * DESCRIPTION:
 *   Create brief usage message from argTable information
 *
 * RETURNS:
 *   ptr to usage message or NULL on error (the value of a tcl variable)
 *   NOTE: The string pointed to by the returned pointer belongs to Tcl
 *   and should not be modified; furthermore, the string is only valid
 *   up until the next call to Tcl_SetVar for the variable. (Tcl book
 *   page 329.) So you should copy the string if you need to keep it around.
 */
char *
ftcl_GetUsage(Tcl_Interp *interp, ftclArgvInfo *argTable, int flags,
              char *cmd_name, int indent, char *usage_prefix,
              char *usage_suffix)
{
   return ( ftcl_MakeUsage(interp, argTable, flags, cmd_name, indent, 
                           usage_prefix, usage_suffix) );
}


/*
 * NAME 
 *   ftcl_GetArgInfo - Public interface to private function ftcl_MakeArgInfo()
 *
 * CALL:
 *   char *ftcl_GetArgInfo(Tcl_Interp *interp, ftclArgvInfo *argTable, 
 *                         int flags, char *cmd_name, int indent)
 *
 *          interp        - tcl interpreter
 *          argTable      - Table of known arguments
 *          flags         - Flags to modify processing of argTable[]
 *          cmd_name      - name of command this arginfo is for
 *          indent        - num chars to indent ArgInfo text
 *
 * DESCRIPTION:
 *   Create Argument description / help message from argTable information. 
 *   This ArgInfo text will contain argument key names, a help string for 
 *   each argument key (if the programmer has supplied a help string), and 
 *   default values for arguments (when available). Also help text will be 
 *   printed for argTable entries with type = FTCL_ARGV_HELP and key=NULL.
 *
 * RETURNS:
 *   ptr to arginfo message or NULL on error (the value of a tcl variable)
 *   NOTE: The string pointed to by the returned pointer belongs to Tcl
 *   and should not be modified; furthermore, the string is only valid
 *   up until the next call to Tcl_SetVar for the variable. (Tcl book
 *   page 329.) So you should copy the string if you need to keep it around.
 *
 */
char *
ftcl_GetArgInfo(Tcl_Interp *interp, ftclArgvInfo *argTable, int flags,
		char *cmd_name, int indent)
{
   return ( ftcl_MakeArgInfo(interp, argTable, flags, cmd_name, indent) );
}

/*
 * NAME 
 *   ftcl_GetHelp - Public interface to get help string
 *
 * CALL:
 *   char *ftcl_GetHelp(Tcl_Interp *interp, ftclArgvInfo *argTable, int flags, 
 *                      char *cmd_name, int indent, char *usage_prefix,
 *                      char *usage_suffix, char *help_suffix)
 *
 *          interp        - tcl interpreter
 *          argTable      - Table of known arguments
 *          flags         - Flags to modify processing of argTable[]
 *          cmd_name      - name of command (for usage string)
 *          indent        - num chars to indent usage line
 *          usage_prefix  - prefix to put before command syntax
 *          usage_suffix  - suffix to put after command syntax (and before arginfo)
 *          help_suffix   - suffix to put after arginfo
 *
 * DESCRIPTION:
 *   Create help message (with usage and argument decriptions) from 
 *   argTable information
 *
 * RETURNS:
 *   ptr to help message or NULL on error (the value of a tcl variable)
 *   NOTE: The string pointed to by the returned pointer belongs to Tcl
 *   and should not be modified; furthermore, the string is only valid
 *   up until the next call to Tcl_SetVar for the variable. (Tcl book
 *   page 329.) So you should copy the string if you need to keep it around.
 */
char *
ftcl_GetHelp(Tcl_Interp *interp, ftclArgvInfo *argTable, int flags,
              char *cmd_name, int indent, char *usage_prefix,
              char *usage_suffix, char *help_suffix)
{
   char *usage_str;
   char *arginfo_str;

   usage_str = ftcl_MakeUsage(interp, argTable, flags, cmd_name, indent, 
                              usage_prefix, usage_suffix);
   if (NULL != usage_str)
       FTCL_ARGV_TCL_STARTHELP(usage_str)
   else
       return NULL;

   arginfo_str = ftcl_MakeArgInfo(interp, argTable, flags, cmd_name, indent);
   if (NULL != arginfo_str)
       FTCL_ARGV_TCL_APPENDHELP(arginfo_str)
   else
       return NULL;

   /* user might pass NULL here, that's ok (not an error) */
   if (NULL != help_suffix)
       FTCL_ARGV_TCL_APPENDHELP(help_suffix)

    /* return ptr to help string */
    return FTCL_ARGV_TCL_GET(FTCL_HELP_TCLVAR);
}

/*
 * ROUTINE:
 *    ftcl_MakeUsage
 *
 *      private routine to create a brief usage statement from argTable info.
 *
 * CALL:
 *   char *ftcl_MakeUsage(Tcl_Interp *interp, ftclArgvInfo *argTable, int flags, 
 *                        char *cmd_name, int indent, 
 *                        char *usage_prefix, char *usage_suffix);
 *
 *          interp        - tcl interpreter
 *          argTable      - Table of known command-line options
 *          flags         - Flags that control the parsing the of argTable
 *          cmd_name      - name of command giving usage for
 *          indent        - num chars to indent usage line
 *          usage_prefix  - prefix to put before command syntax
 *          usage_suffix  - suffix to put after command syntax
 *
 * DESCRIPTION:
 *   Generates a brief usage string describing command-line options.
 *
 * RETURNS:
 *   ptr to usage message or NULL on error (the value of a tcl variable)
 *   NOTE: The string pointed to by the returned pointer belongs to Tcl
 *   and should not be modified; furthermore, the string is only valid
 *   up until the next call to Tcl_SetVar for the variable. (Tcl book
 *   page 329.) So you should copy the string if you need to keep it around.
 */
static char *
ftcl_MakeUsage(Tcl_Interp *interp, ftclArgvInfo *argTable, int flags, 
               char *cmd_name, int indent, 
               char *usage_prefix, char *usage_suffix)
{
    register ftclArgvInfo *infoPtr;  /* ptr to entry in argTable */
    char indent_str[FTCL_ARGV_INDENT_BUFSIZE];
                                     /* string form of "indent" spaces */
    int i;                           /* index variable */

    char empty_string[] = "";
    char one_space[]    = " ";

    char *after_prefix_spacing;
    char *after_cmd_spacing;

    /* NOTE: usage_length and usage_str only needed if not using tcl 
     * variables for storage 
     *
     * int usage_length;                
     * static char *usage_str = (char *) NULL;
     */

    /* only do malloc if haven't done it before or last try failed.
     * If this malloc fails, return
     */
    /* NOTE: Use this if not using tcl variables for storage
     * if ( (char *) NULL == usage_str)
     *    usage_str = (char *) malloc((size_t) FTCL_ARGV_USAGE_BUFSIZE);
     * if ( (char *) NULL == usage_str)
     *    return(usage_str);
     *
     * usage_str[0] = NULL;
     * usage_length = 0;
     */

    /* initialize usage to contain empty string */
    FTCL_ARGV_TCL_STARTUSAGE("")

    /* put in default values for parameters if requested */

    if (NULL == usage_prefix)
        usage_prefix = empty_string;
    if (NULL == usage_suffix)
        usage_suffix = empty_string;
    if (NULL == cmd_name)
        cmd_name = empty_string;
    if (indent < 0)
        indent = 0;
    if (indent > (FTCL_ARGV_INDENT_BUFSIZE - 1))
        indent = FTCL_ARGV_INDENT_BUFSIZE - 1;

    if (0 == strlen(usage_prefix))
        after_prefix_spacing = empty_string;
    else
        after_prefix_spacing = one_space;

    if (0 == strlen(cmd_name))
        after_cmd_spacing = empty_string;
    else
        after_cmd_spacing = one_space;

    /* put in indent, usage prefix, and command name */
    sprintf(indent_str,"%*s", indent, "");

    /* NOTE: use these if not using tcl variables for storage
     * FTCL_ARGV_APPENDUSAGE(indent_str)
     * FTCL_ARGV_APPENDUSAGE(usage_prefix)
     * FTCL_ARGV_APPENDUSAGE(after_prefix_spacing) 
     * FTCL_ARGV_APPENDUSAGE(cmd_name)
     * FTCL_ARGV_APPENDUSAGE(after_cmd_spacing)
     */

    FTCL_ARGV_TCL_APPENDUSAGE(indent_str)
    FTCL_ARGV_TCL_APPENDUSAGE(usage_prefix)
    FTCL_ARGV_TCL_APPENDUSAGE(after_prefix_spacing)
    FTCL_ARGV_TCL_APPENDUSAGE(cmd_name)
    FTCL_ARGV_TCL_APPENDUSAGE(after_cmd_spacing)

    /* put in keys for arguments in argTable */
    for (i = 0; i < 2; i++) {
	for (infoPtr = i ? defaultTable : argTable;
		infoPtr->type != FTCL_ARGV_END; infoPtr++) {
	    if (infoPtr->key == NULL) {
		continue;
	    }
            /* NOTE: use these if not using tcl variables for storage
             * FTCL_ARGV_APPENDUSAGE(infoPtr->key)
             * FTCL_ARGV_APPENDUSAGE(" ")
             */
        FTCL_ARGV_TCL_APPENDUSAGE(infoPtr->key)
        FTCL_ARGV_TCL_APPENDUSAGE(" ")
	}
	if ((flags & FTCL_ARGV_NO_DEFAULTS) || (i > 0)) {
	    break;
	}
    }

    /* put in usage suffix */
    /* NOTE: use this if not using tcl variables for storage
     * FTCL_ARGV_APPENDUSAGE(usage_suffix)
     */
    FTCL_ARGV_TCL_APPENDUSAGE(usage_suffix)

    /* return ptr to usage string */

    /* NOTE: use this if not using tcl variables for storage
     * return usage_str;
     */

    return FTCL_ARGV_TCL_GET(FTCL_USAGE_TCLVAR);
}

/*
 * ROUTINE:
 *    ftcl_MakeArgInfo
 *
 *      private routine to create string with argument descriptions/help
 *      from argTable info.
 *
 * CALL:
 *   char *ftcl_MakeArgInfo(Tcl_Interp *interp, ftclArgvInfo *argTable, 
 *                          int flags, char *cmd_name, int indent);
 *
 *          interp        - tcl interpreter
 *          argTable      - Table of known command-line options
 *          flags         - Flags that control the parsing the of argTable
 *          cmd_name      - name of command giving usage for
 *          indent        - num chars to indent ArgInfo text
 *
 * DESCRIPTION:
 *   Create Argument description / help message from argTable information. 
 *   This ArgInfo text will contain argument key names, a help string for 
 *   each argument key (if the programmer has supplied a help string), and 
 *   default values for arguments (when available). Also help text will be 
 *   printed for argTable entries with type = FTCL_ARGV_HELP and key=NULL.
 *
 * RETURNS:
 *   ptr to arginfo message or NULL on error (the value of a tcl variable)
 *   NOTE: The string pointed to by the returned pointer belongs to Tcl
 *   and should not be modified; furthermore, the string is only valid
 *   up until the next call to Tcl_SetVar for the variable. (Tcl book
 *   page 329.) So you should copy the string if you need to keep it around.
 */
static char *
ftcl_MakeArgInfo(Tcl_Interp *interp, ftclArgvInfo *argTable, int flags, 
                 char *cmd_name, int indent)
{
#define NUM_SPACES 20

    register ftclArgvInfo *infoPtr;
    int width, i, numSpaces;
    char spaces[] = "                    ";
    char tmp[FTCL_ARGV_ARGINFO_BUFSIZE];

    char indent_str[FTCL_ARGV_INDENT_BUFSIZE]; 
                                   /* string form of "indent" spaces */
    char indent_arg_str[FTCL_ARGV_INDENT_BUFSIZE*2];
                                   /* string form of # spaces to precede
                                    * printing of new line of arg help str */
    char indent_default_str[FTCL_ARGV_INDENT_BUFSIZE*2];  
                                   /* string form of # spaces to precede
                                    * printing of arg default value */

    char empty_string[] = "";

/* There is no reason to do so. Removed by Chih-Hao Huang 12/20/95

    if (assign_defaults(interp, argTable) == 0)
        return NULL;
 */

    /* NOTE: arginfo_length and arginfo_str only needed if not using tcl 
     * variables for storage 
     *
     * int arginfo_length; 
     * static char *arginfo_str = (char *) NULL;
     */

    /* only do malloc if haven't done it before or last try failed.
     * If this malloc fails, return
     */
    /* NOTE: Use this if not using tcl variables for storage
     * if ( (char *) NULL == arginfo_str)
     *    arginfo_str = (char *) malloc((size_t) FTCL_ARGV_ARGINFO_BUFSIZE);
     * if ( (char *) NULL == arginfo_str)
     *    return(arginfo_str);
     *
     * arginfo_str[0] = NULL;
     * arginfo_length = 0;
     */

    /* put in default values for parameters if requested */

    if (NULL == cmd_name)
        cmd_name = empty_string;
    if (indent < 0)
        indent = 0;
    if (indent > (FTCL_ARGV_INDENT_BUFSIZE - 1))
        indent = FTCL_ARGV_INDENT_BUFSIZE - 1;

    /* put newline as first char of arginfo string */
    /* NOTE: Use this if not using tcl variables for storage
     * FTCL_ARGV_APPENDARGINFO("\n")
     */
    FTCL_ARGV_TCL_STARTARGINFO("\n")

    /*
     * First, compute the width of the widest option key, so that we
     * can make everything line up.
     */
    width = 4;
    for (i = 0; i < 2; i++) {
	for (infoPtr = i ? defaultTable : argTable;
		infoPtr->type != FTCL_ARGV_END; infoPtr++) {
	    int length;
	    if (infoPtr->key == NULL) {
		continue;
	    }
	    length = strlen(infoPtr->key);
	    if (length > width) {
		width = length;
	    }
	}
    }

    /* make strings for indent that arg elements use and indent that default
     * values for arg elements will use. Put Default values at a 4 space 
     * indent further than the starting point of help string for a particular 
     * arg. The regular help string of an arg starts after 
     * indent + width_of_longest_key + ": " separator
     */
    sprintf(indent_str,"%*s", indent, "");
    sprintf(indent_arg_str, "%s%*s", indent_str, width+strlen(": "), "");
    sprintf(indent_default_str, "%s%*s", indent_arg_str, 4, "");

    for (i = 0; ; i++) {
	for (infoPtr = i ? defaultTable : argTable;
		infoPtr->type != FTCL_ARGV_END; infoPtr++) {

            char buf[FTCL_ARGV_ARGINFO_BUFSIZE];
            char *start, *end;   /* start and end of segments of help string */
            int len;             /* length of segment of help string */

	    if ((infoPtr->type == FTCL_ARGV_HELP) && (infoPtr->key == NULL)) {
                start = infoPtr->help;
                if (NULL != infoPtr->help)
                    len = strlen(infoPtr->help);

                while ((char *)NULL != start && NULL != *start) {
                    end = strchr(start,'\n');
                    if ((char *) NULL == end)
                        end = infoPtr->help + len;
                    else
                        while ('\n' == *end) end++;

                    if (end - start >  FTCL_ARGV_ARGINFO_BUFSIZE - 1)
                        return((char *) NULL);

                    strncpy(buf, start, end - start);
                    buf[end-start] = NULL;
                    /* NOTE: use these if not using tcl variables for storage
                     * FTCL_ARGV_APPENDARGINFO(indent_str)
                     * FTCL_ARGV_APPENDARGINFO(buf)
                     */
                    Tcl_SetVar2(interp, FTCL_PARSE_TCLARRAY, FTCL_ARGINFO_TCLVAR,
                                indent_str, FTCL_APPENDVAR_FLAGS);
                    Tcl_SetVar2(interp, FTCL_PARSE_TCLARRAY, FTCL_ARGINFO_TCLVAR,
                                buf, FTCL_APPENDVAR_FLAGS);
                    start = end;
                }
                /* always finish specifig arg help string with newline */
                /* NOTE: use this if not using tcl variables for storage
                 * FTCL_ARGV_APPENDARGINFO("\n") 
                 */
                FTCL_ARGV_TCL_APPENDARGINFO("\n")

		continue;
	    }

            /* NOTE: use this if not using tcl variables for storage
	     * FTCL_ARGV_APPENDARGINFO(indent_str)
             * FTCL_ARGV_APPENDARGINFO(infoPtr->key)
             * FTCL_ARGV_APPENDARGINFO(":")
             */
            FTCL_ARGV_TCL_APPENDARGINFO(indent_str)

            if (NULL != infoPtr->key)
	        {
                FTCL_ARGV_TCL_APPENDARGINFO(infoPtr->key)
	        }
            FTCL_ARGV_TCL_APPENDARGINFO(":")
              
	    numSpaces = width + 1;
            if ((char *) NULL != infoPtr->key)
                numSpaces -= strlen(infoPtr->key);
	    while (numSpaces > 0) {
		if (numSpaces >= NUM_SPACES) {
                    /* NOTE: use this if not using tcl variables for storage
		     * FTCL_ARGV_APPENDARGINFO(spaces)
                     */
                    FTCL_ARGV_TCL_APPENDARGINFO(spaces)
		} else {
                    /* NOTE: use this if not using tcl variables for storage
		     * FTCL_ARGV_APPENDARGINFO(spaces+NUM_SPACES-numSpaces)
                     */
                    FTCL_ARGV_TCL_APPENDARGINFO(spaces+NUM_SPACES-numSpaces)
		}
		numSpaces -= NUM_SPACES;
	    }

            start = infoPtr->help;
            if (NULL != infoPtr->help)
                len = strlen(infoPtr->help);

            while ((char *)NULL != start && NULL != *start) {
                end = strchr(start,'\n');
                if ((char *) NULL == end)
                    end = infoPtr->help + len;
                else
                    while ('\n' == *end) end++;

                if (end - start >  FTCL_ARGV_ARGINFO_BUFSIZE - 1)
                    return((char *) NULL);

                strncpy(buf, start, end - start);
                buf[end-start] = NULL;
                /* already have the proper indent if at start of help_str */
                if (start != infoPtr->help)
		    {
                    /* NOTE: use this if not using tcl variables for storage
                     * FTCL_ARGV_APPENDARGINFO(indent_arg_str)
                     */
                    FTCL_ARGV_TCL_APPENDARGINFO(indent_arg_str)
		    }
                /* NOTE: use this if not using tcl variables for storage
                 * FTCL_ARGV_APPENDARGINFO(buf)
                 */
                FTCL_ARGV_TCL_APPENDARGINFO(buf)
                start = end;
            }
            /* always finish specific arg help string with newline */

            /* NOTE: use this if not using tcl variables for storage
	     * FTCL_ARGV_APPENDARGINFO("\n")
             */
            FTCL_ARGV_TCL_APPENDARGINFO("\n")

            /* don't print default values for required parameters
             * required parameters have "<" as the first char
             */
            if (NULL != infoPtr->key && '<' == (infoPtr->key)[0])
                continue;

            /* Don't try to get default values if dst is not defined */
/* why not? Modified by Chih-Hao Huang 12/20/95

            if (NULL == infoPtr->dst)
                continue;
*/

        if (infoPtr->src != NULL)
	{
            switch (infoPtr->type) {
                case FTCL_ARGV_INT: {
                    /* NOTE: use this if not using tcl variables for storage
                     * FTCL_ARGV_APPENDARGINFO(indent_default_str)
                     * FTCL_ARGV_APPENDARGINFO("Default value: ")
                     * FTCL_ARGV_APPENDARGINFO(infoPtr->src)
                     * FTCL_ARGV_APPENDARGINFO("\n")
                     */
                    FTCL_ARGV_TCL_APPENDARGINFO(indent_default_str)
                    FTCL_ARGV_TCL_APPENDARGINFO("Default value: ")
                    FTCL_ARGV_TCL_APPENDARGINFO(infoPtr->src)
                    FTCL_ARGV_TCL_APPENDARGINFO("\n")
                    break;
                }
                case FTCL_ARGV_DOUBLE :  {
                    /* NOTE: use this if not using tcl variables for storage
                     * FTCL_ARGV_APPENDARGINFO(indent_default_str)
                     * FTCL_ARGV_APPENDARGINFO("Default value: ")
                     * FTCL_ARGV_APPENDARGINFO(infoPtr->src)
                     * FTCL_ARGV_APPENDARGINFO("\n")
                     */
                    FTCL_ARGV_TCL_APPENDARGINFO(indent_default_str)
                    FTCL_ARGV_TCL_APPENDARGINFO("Default value: ")
                    FTCL_ARGV_TCL_APPENDARGINFO(infoPtr->src)
                    FTCL_ARGV_TCL_APPENDARGINFO("\n")
                    break;
                }
                case FTCL_ARGV_STRING: {
                    /* NOTE: use this if not using tcl variables for storage
                     * FTCL_ARGV_APPENDARGINFO(indent_default_str)
                     * FTCL_ARGV_APPENDARGINFO("Default value: \"")
                     * FTCL_ARGV_APPENDARGINFO(infoPtr->src)
                     * FTCL_ARGV_APPENDARGINFO("\"")
                     * FTCL_ARGV_APPENDARGINFO("\n")
                     */
                    FTCL_ARGV_TCL_APPENDARGINFO(indent_default_str)
                    FTCL_ARGV_TCL_APPENDARGINFO("Default value: \"")
                    FTCL_ARGV_TCL_APPENDARGINFO(infoPtr->src)
                    FTCL_ARGV_TCL_APPENDARGINFO("\"")
                    FTCL_ARGV_TCL_APPENDARGINFO("\n")
                    break;
                    }
                default: {
                    break;
                }
            }
        }
    }

	if ((flags & FTCL_ARGV_NO_DEFAULTS) || (i > 0)) {
	    break;
	}
    }

    /* return ptr to arginfo string */
    /* NOTE: use this if not using tcl variables for storage
     * return arginfo_str;
     */
    return FTCL_ARGV_TCL_GET(FTCL_ARGINFO_TCLVAR);
}
