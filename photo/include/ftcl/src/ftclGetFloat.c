
#include "tcl.h"


int Tcl_GetFloat(Tcl_Interp *interp, char *string, float *value)
{
  double        dvalue;
  int           stat;

  if((stat=Tcl_GetDouble(interp, string, &dvalue)) != TCL_OK) return(stat);
  *value = (float) dvalue;
  return(TCL_OK);
}

