#if !defined(RANLIB_H)
#define RANLIB_H

void phPoissonSeedSet(long seed1, long seed2);
long phPoissonDevGet(float mu);

#endif
