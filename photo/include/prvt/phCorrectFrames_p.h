/*
 * Private definitions for Correct_Frames                 
 */
#define MAXBUF 4096		/* Buffer for image I/O             */
#define FSHIFT 2048             /* flat field scale factor          */

#define NBAD1COLMAX 2048	/* maximum number of bad columns */
