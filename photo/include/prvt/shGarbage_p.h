#ifndef _SHGARGAGE_P_H
#define _SHGARBAGE_P_H

/*
 *
 * FILE:
 *      shGarbage_p.h
 *
 * ABSTRACT:
 *    This is the PRIVATE region for APIs associated with Dervish memory 
 * management package. The APIs declared here are not availaible for general
 * use. 
 *
 *          +------------------------------------------------+
 *          |  THEY ARE ONLY TO BE USED BY DERVISH DEVELOPERS. |
 *          +------------------------------------------------+
 *
 * ENVIRONMENT:
 *      ANSI C.
 */

#endif
