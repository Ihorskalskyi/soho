# This file is a Tcl script to test the code in the file tkTextMark.c.
# This file is organized in the standard fashion for Tcl tests.
#
# Copyright (c) 1994 The Regents of the University of California.
# Copyright (c) 1994 Sun Microsystems, Inc.
#
# See the file "license.terms" for information on usage and redistribution
# of this file, and for a DISCLAIMER OF ALL WARRANTIES.
#
# @(#) textMark.test 1.3 94/12/17 15:54:54

if {[string compare test [info procs test]] == 1} then \
  {source defs}

catch {destroy .t}
if [catch {text .t \
	-font -adobe-courier-medium-r-normal--12-120-75-75-m-70-iso8859-1 \
	-width 20 -height 10}] {
    puts "The font needed by these tests isn't available, so I'm"
    puts "going to skip the tests."
    return
}
pack append . .t {top expand fill}
update
.t debug on
wm geometry . {}
  
# The statements below reset the main window;  it's needed if the window
# manager is mwm to make mwm forget about a previous minimum size setting.

wm withdraw .
wm minsize . 1 1
wm positionfrom . user
wm deiconify .

entry .t.e
.t insert 1.0 "Line 1
abcdefghijklm
12345
Line 4
bOy GIrl .#@? x_yz
!@#$%
Line 7"

test textMark-1.1 {TkTextMarkCmd - "gravity" option} {
    list [catch {.t mark} msg] $msg
} {1 {wrong # args: should be ".t mark option ?arg arg ...?"}}
test textMark-1.2 {TkTextMarkCmd - "gravity" option} {
    list [catch {.t mark gorp} msg] $msg
} {1 {bad mark option "gorp":  must be gravity, names, set, or unset}}
test textMark-1.3 {TkTextMarkCmd - "gravity" option} {
    list [catch {.t mark gravity foo} msg] $msg
} {1 {there is no mark named "foo"}}
test textMark-1.4 {TkTextMarkCmd - "gravity" option} {
    .t mark unset x
    .t mark set x 1.3
    .t insert 1.3 x
    list [.t mark gravity x] [.t index x]
} {right 1.4}
test textMark-1.5 {TkTextMarkCmd - "gravity" option} {
    .t mark unset x
    .t mark set x 1.3
    .t mark g x left
    .t insert 1.3 x
    list [.t mark gravity x] [.t index x]
} {left 1.3}
test textMark-1.6 {TkTextMarkCmd - "gravity" option} {
    .t mark unset x
    .t mark set x 1.3
    .t mark gravity x right
    .t insert 1.3 x
    list [.t mark gravity x] [.t index x]
} {right 1.4}
test textMark-1.7 {TkTextMarkCmd - "gravity" option} {
    list [catch {.t mark gravity x gorp} msg] $msg
} {1 {bad mark gravity "gorp": must be left or right}}

test textMark-2.1 {TkTextMarkCmd - "names" option} {
    list [catch {.t mark names 2} msg] $msg
} {1 {wrong # args: should be ".t mark names"}}
.t mark unset x
test textMark-2.2 {TkTextMarkCmd - "names" option} {
    lsort [.t mark n]
} {current insert}
test textMark-2.3 {TkTextMarkCmd - "names" option} {
    .t mark set a 1.1
    .t mark set "b c" 2.3
    lsort [.t mark names]
} {a {b c} current insert}

test textMark-3.1 {TkTextMarkCmd - "set" option} {
    list [catch {.t mark set a} msg] $msg
} {1 {wrong # args: should be ".t mark set markName index"}}
test textMark-3.2 {TkTextMarkCmd - "set" option} {
    list [catch {.t mark s a b c} msg] $msg
} {1 {wrong # args: should be ".t mark set markName index"}}
test textMark-3.3 {TkTextMarkCmd - "set" option} {
    list [catch {.t mark set a @x} msg] $msg
} {1 {bad text index "@x"}}
test textMark-3.4 {TkTextMarkCmd - "set" option} {
    .t mark set a 1.2
    .t index a
} 1.2
test textMark-3.5 {TkTextMarkCmd - "set" option} {
    .t mark set a end
    .t index a
} {8.0}

test textMark-4.1 {TkTextMarkCmd - "unset" option} {
    list [catch {.t mark unset} msg] $msg
} {0 {}}
test textMark-4.2 {TkTextMarkCmd - "unset" option} {
    .t mark set a 1.2
    .t mark set b 2.3
    .t mark unset a b
        list [catch {.t index a} msg] $msg [catch {.t index b} msg2] $msg2
} {1 {bad text index "a"} 1 {bad text index "b"}}
test textMark-4.3 {TkTextMarkCmd - "unset" option} {
    .t mark set a 1.2
    .t mark set b 2.3
    .t mark set 49ers 3.1
    eval .t mark unset [.t mark names]
    lsort [.t mark names]
} {current insert}

test textMark-5.1 {TkTextMarkCmd - miscellaneous} {
    list [catch {.t mark} msg] $msg
} {1 {wrong # args: should be ".t mark option ?arg arg ...?"}}
test textMark-5.2 {TkTextMarkCmd - miscellaneous} {
    list [catch {.t mark foo} msg] $msg
} {1 {bad mark option "foo":  must be gravity, names, set, or unset}}

test textMark-6.1 {TkTextMarkSegToIndex} {
    .t mark set a 1.2
    .t mark set b 1.2
    .t mark set c 1.2
    .t mark set d 1.4
    list [.t index a] [.t index b] [.t index c ] [.t index d]
} {1.2 1.2 1.2 1.4}

catch {destroy .t}
concat {}
