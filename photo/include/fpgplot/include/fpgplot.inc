C
C   This include file has been added to define storage for holding the x and
C   y positions of the pgplot window.  GRIMAX is defined in the pgplot
C   include file grpckg1.inc
C
	INTEGER GRXPOS(GRIMAX), GRYPOS(GRIMAX)
	COMMON /FPGPOS/ GRXPOS, GRYPOS
	SAVE /FPGPOS/
