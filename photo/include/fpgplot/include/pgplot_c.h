#ifndef PGPLOT_C_H
#define PGPLOT_C_H

#ifdef __cplusplus
extern "C"
{
#endif  /* ifdef cpluplus */

/*
**	Protype functions for the c binding routines to pgplot.  Most of the
**      C bindings are included in the pgplot product except for these here.
*/
int pgGeomGet(unsigned int *a_width, unsigned int *a_height, int *a_x,
	      int *a_y);

void pgGeomSet(unsigned int a_width, unsigned int a_height, int a_x, int a_y);

long int pgplot_getmfile(char *metaFile);

long int pgplot_gmfplot (char *fileName, char *deviceName, long int *ierr);

void pgplot_pgconx (float *a, int *idim, int *jdim, int *i1, int *i2,
              int *j1, int *j2, float *c, int *nc, 
              void (*plot)(int *vs, float *x, float *y, float *z));

void pgplot_pgfunt (float (*fx)(float *t), float (*fy)(float *t), 
                    int *n, float *tmin, float *tmax, int *flag);

void pgplot_pgfunx (float (*fy)(float *x), int *n, 
                    float *xmin, float *xmax, int *pgflag);

void pgplot_pgfuny (float (*fx)(float *y), int *n, 
                    float *ymin, float *ymax, int *pgflag);

#ifdef __cplusplus
}
#endif  /* ifdef cpluplus */

#endif

