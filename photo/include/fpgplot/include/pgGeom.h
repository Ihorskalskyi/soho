#ifndef PGGEOM_H
#define PGGEOM_H

#define TRUE 1
#define FALSE 0
#define ARG_GEOMETRY "geometry"
#define PROGRAM_NAME "pgPlot"

#ifdef __cplusplus
extern "C"
{
#endif  /* ifdef cpluplus */


void pgGeomSet(unsigned int a_width, unsigned int a_height, int a_x, int a_y);
int pgGeomGet(unsigned int *a_width, unsigned int *a_height, int *a_x, int *a_y);


#ifdef __cplusplus
}
#endif  /* ifdef cpluplus */

#endif
