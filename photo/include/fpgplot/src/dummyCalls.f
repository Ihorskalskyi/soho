C This routine exists solely for the purpose of forcing all routines in
C   libfpgplot.a to get loaded before the loaded encounters the original files
C   in libpgplot.a.  Thus any routine that is taken from pgplot, moved into
C   fpgplot and altered needs to be included in this file so that the altered
C   copy will be the one loaded into the executable.
C
C   Currently the files that this pertains to are the following -
C
C             grbpic.f
C             grexec.f
C	      grsetli.f
C	      grtran.f
C	      mfdriv.f
C	      xwdriv.c
C
C
      SUBROUTINE DUMMYCALL
C
      INTEGER IDEV, IFUNC, NBUF, LCHR, IN
      REAL    RBUF(10)
      CHARACTER*(10) CHR
      INTEGER  IDENT
      REAL     XORG, YORG, XSCALE, YSCALE
      INTEGER   TYPE, DUMMY
      CHARACTER*(20) FILE
C
      CALL GREXEC(IDEV, IFUNC, RBUF, NBUF, CHR, LCHR)
C
      CALL GRSETLI(IN)
C
      CALL GRTRAN(IDENT, XORG, YORG, XSCALE, YSCALE)
C
      CALL MFDRIV (IFUNC, RBUF, NBUF, CHR, LCHR)
C
      CALL GRBPIC
C
      CALL GROPEN(TYPE,DUMMY,FILE,IDENT)
C
      CALL PGPAGE
C
      CALL XWDRIV(IFUNC, RBUF, NBUF, CHR, LCHR, 1)
C     
      RETURN
      END






