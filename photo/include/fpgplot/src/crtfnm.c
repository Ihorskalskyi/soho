#include <stdio.h>
#include <unistd.h>

#define FPGSUCCESS   0
#define BUFTOOSHORT 1
#define TMPNAMERR   2

/* crtfnm.c
 *
 *	This routine does the following -
 *           * try to delete the file currently in a_buf
 *           * concoct a unique filename using tmpnam
 *           * get the length of the filename, space fill the rest
 *               (we are being called by fortran)
 *
 *      This routine is called from FORTRAN by mfdriv - the metafile
 *      driver.    
 *
 *  24-jun-1994  move to fpgplot product (efb)
 *  8-dec-1993   Eileen Berman
 *
 */

void crtfnm_ 
   (
   short int *a_bufSize,        /* IN  : max size of a_buf */
   char      *a_buf,            /* MOD : contains filename */
   short int *a_bufLen,         /* MOD : occupied length of a_buf */
   short int *a_istatus         /* OUT : error signal */
   )
{
char *dumRtn;
int  i;

/* First, try to delete the current file in a_buf.  If there is no file or
   the filename is invalid, unlink will return with an error.  In either
   of these cases we do not care. In this manner we will not fill up all
   empty space on the disk. */
if (a_buf[0] != '\0')
   {
   unlink(a_buf);
   }

/* Now try and create a new unique filename. First check that our buffer is
   long enough. */
if (*a_bufSize < L_tmpnam)
   {
   /* Buffer is not long enough. */
   *a_istatus = BUFTOOSHORT;
   }
else
   {
   /* Create a unique filename */
   dumRtn = tmpnam(a_buf);

   if ((dumRtn[0] == '\0') && (a_buf == '\0'))
      {
      /* tmpnam could not create a unique filename. */
      *a_istatus = TMPNAMERR;
      }
   else
      {
      /* Get the length of a_buf */
      *a_bufLen = strlen(a_buf);

      /* Space fill whatever is left for FORTRAN. */
      for (i = *a_bufLen ; i < *a_bufSize ; ++i)
	 {a_buf[i] = ' ';}

      *a_istatus = FPGSUCCESS;
      }
   }
}






