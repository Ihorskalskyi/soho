SHELL = /bin/sh
#
# This generates the FPGPLOT binary files (libraries and gmfplot) in the
# current default directory (which need not be the source directory).
#
#	Directory containing source code
SRC		= ..
SRCDIR		= $(SRC)/src
BINDIR		= $(SRC)/bin
LIBDIR		= $(SRC)/lib
ARFLAGS         = ru
FPGPLOT		= -L$(LIBDIR) -lfpgplot
FPGPLOT_LIB	= $(LIBDIR)/libfpgplot.a

PRODDIR		=   FPGPLOT
PRODDIRNAME	=   FPGPLOT_DIR
PRODDIR		= $(FPGPLOT_DIR)
PRODINSTALLNAME	=   FPGPLOT_INSTALL_DIR
PRODINSTALLDIR	= $(FPGPLOT_INSTALL_DIR)

DESTSUBDIR	= src

#	Fortran compiler (-pic required for shared libary)
###FCOMPL=f77 
#	C compiler (-pic required for shared library)
#CCOMPL=cc 

#	Rules for updating library from fortran sources

.f.o:
	$(FCOMPL) -c $(FFLAGC)  $(FFLAGS) -o $*.o $(SRCDIR)/$*.f

#	Rules for updating library from C sources
.c.o:
	$(CCOMPL) -c $(CFLAGC) $(SDSS_CFLAGS) -I$(PGPLOT_DIR)/dst -I$(FPGPLOT_DIR)/include -o $*.o $(SRCDIR)/$*.c
#-----------------------------------------------------------------------
DISPATCH_ROUTINE = grexec.o 

BINDING_ROUTINES = pgplot_c.o dummyCalls.o

BINDING_DUMMY_ROUTINES = pgplot_c_dummy.o

GMFPLOT_MAIN = gmfplot_c.o

METAFILE_SUPPORT_ROUTINES = gmfplot.o crtfnm.o getmfile.o shdriv.o svstate.o \
	grsetli.o grtran.o

PGPLOT_SUPPORT_ROUTINES = pgGeom.o resetWin.o grbpic.o gropen.o pgpage.o

#-----------------------------------------------------------------------
# Device drivers
#-----------------------------------------------------------------------
MFDRIV = mfdriv.o
XWDRIV = xwdriv.o

# Drivers. 
DRIVERS = $(XWDRIV)  gidriv.o
MFDRIVERS = $(MFDRIV) 

#-----------------------------------------------------------------------
# Target "library" is used to built the PGPLOT subroutiune library.
# libpgplot.a is the primary PGPLOT library.
#-----------------------------------------------------------------------

all :	library dummy_library gmfplot

gmfplot : $(BINDIR)/gmfplot
	@ echo  

$(BINDIR)/gmfplot : library metafiles
	$(DCOMPL) $(FFLAGC) -o $@ gmfplot_c.o  $(MFDRIV) $(FPGPLOT) $(LIBS)

metafiles : $(METAFILE_SUPPORT_ROUTINES) $(GMFPLOT_MAIN)

library : $(LIBDIR)/libfpgplot.a

$(LIBDIR)/libfpgplot.a : $(DISPATCH_ROUTINE) $(DRIVERS) $(MFDRIVERS) \
	   $(BINDING_ROUTINES) $(GMFPLOT_ROUTINES) \
           $(METAFILE_SUPPORT_ROUTINES) $(PGPLOT_SUPPORT_ROUTINES)
	ar $(ARFLAGS) $(LIBDIR)/libfpgplot.a $(DISPATCH_ROUTINE) 
	ar $(ARFLAGS) $(LIBDIR)/libfpgplot.a $(BINDING_ROUTINES)
	ar $(ARFLAGS) $(LIBDIR)/libfpgplot.a $(DRIVERS) 
	ar $(ARFLAGS) $(LIBDIR)/libfpgplot.a $(MFDRIVERS) 
	ar $(ARFLAGS) $(LIBDIR)/libfpgplot.a $(METAFILE_SUPPORT_ROUTINES)
	ar $(ARFLAGS) $(LIBDIR)/libfpgplot.a $(GMFPLOT_ROUTINES)
	ar $(ARFLAGS) $(LIBDIR)/libfpgplot.a $(PGPLOT_SUPPORT_ROUTINES)
	-@ if test -f /usr/bin/ranlib; then ranlib $(LIBDIR)/libfpgplot.a; fi

dummy_library : $(LIBDIR)/libfpgplot_dummy.a

$(LIBDIR)/libfpgplot_dummy.a : $(BINDING_DUMMY_ROUTINES)
	ar $(ARFLAGS) $(LIBDIR)/libfpgplot_dummy.a $(BINDING_DUMMY_ROUTINES)
	-@ if test -f /usr/bin/ranlib; then ranlib $(LIBDIR)/libfpgplot_dummy.a; fi

gidriv.o : gidriv.F
	$(FCOMPL) -c $(FFLAGC)  $(FFLAGS) -o gidriv.o $(SRCDIR)/gidriv.F

pgplot_c_dummy.c :
	../bin/makePgDummySrc

#-----------------------------------------------------------------------
# Target "install" is required for Figaro.
#-----------------------------------------------------------------------
install:
	@ if [ "$(PRODINSTALLDIR)" = '' ]; then \
		echo "The destination directory has not been specified.  Set the environment"	>&2; \
		echo "variable $(PRODINSTALLNAME)"							>&2; \
		exit 1; \
	fi
	@ if [ `(cd $(PRODINSTALLDIR)/$(DESTSUBDIR); pwd)` = `pwd` ]; then \
		echo "The destination directory is the same as the current directory."		>&2; \
		echo "The install will be aborted."						>&2; \
		exit 1; \
	fi
	cp makefile   $(PRODINSTALLDIR)/$(DESTSUBDIR)
	cp premake    $(PRODINSTALLDIR)/$(DESTSUBDIR)
	cp *.f   *.c  $(PRODINSTALLDIR)/$(DESTSUBDIR)
	cp dummyCalls.o        $(PRODINSTALLDIR)/$(DESTSUBDIR)

#-----------------------------------------------------------------------
# Target "clean" is used to remove all the intermediate files.
#-----------------------------------------------------------------------
clean :
	- rm -f core *~ *.bak *.jou *.orig *.old .'#'* '#'*'#'
	- rm -f *.o pgplot_c_dummy.c
	- rm -f $(PG_ROUTINES) $(PG_NON_STANDARD) $(GR_ROUTINES)\
		$(DISPATCH_ROUTINE) $(DRIVERS) $(SYSTEM_ROUTINES)\
		$(OBSOLETE_ROUTINES) $(DATAOBJS) strings xs.o\

#-----------------------------------------------------------------------

