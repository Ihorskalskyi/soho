C shdriv.c
C
C	This routine does the following -
C           * gets the value of environment variable PGPLOT_SAVE
C           * copies the input parameters to local variables if 
C                   PGPLOT_SAVE = 1
C           * calls the metafile driver if PGPLOT_SAVE = 1
C
C      NOTE: The values of PGPLOT_SAVE should only be retrieved once
C            per plot.  The value of PGPLOT_GET signals whether a
C            new value for PGPLOT_SAVE should be gotten.  PGPLOT_GET
C            is reset when the metafile is closed [close workstation]
C            function.  Thus each time a new plot is started [after
C            a pgEnd], PGPLOT_SAVE will be gotten again.
C
C            Also, the input parameters need to be copied to local
C            values as they may be changed by the metafile driver
C            and we want the original values for the current display
C            driver.
C
C
C  24-jun-1993 Moved to fpgplot product
C  9-dec-1993  Eileen Berman
C
C
      subroutine shdriv(a_func, a_rbuf, a_nbuf, a_chr, a_lchr)
C
      integer       a_func              ! These parameters are the 
      real          a_rbuf(*)           !   same as those to all   
      integer       a_nbuf              !   the other pgplot       
      character*(*) a_chr               !   drivers.               
      integer       a_lchr              !   [this one too]         
C
      integer      l_func, l_nbuf, l_lchr
      real         l_rbuf(6)
      character*32 l_chr
      integer      rstatus
      character    value(20)
C
      INTEGER*2  PGPLOT_SAVE, PGPLOT_GET
      CHARACTER  UFNAME_BUF*50
      INTEGER*2  UFNAME_LEN
      COMMON /UFNAME/ UFNAME_BUF, UFNAME_LEN, PGPLOT_SAVE, PGPLOT_GET
C
C   See if we are supposed to get a new value for PGPLOT_SAVE
C
      if (PGPLOT_GET .eq. 1) then
         call GRGENV('SAVE', value, rstatus)
         PGPLOT_GET = 0
C
C   See if PGPLOT_SAVE was defined.  It does not matter what the value is.
C
         if (rstatus .eq. 0) then
            PGPLOT_SAVE = 0                  ! do not save in metafile too
         else
            PGPLOT_SAVE = 1                  ! save plot in metafile file
         endif
      endif
C
C   Now see if we need to call the metafile driver too.  This allows the
C     plot commands to be displayed on the device the user wants and
C     saved in a metafile too.  Then the user can save the metafile to a
C     local file to be displayed later.  This last step is accomplished
C     through the Tcl command 'pgSave'.  Copy the input parameters to local
C     values so as not to corrupt them.
C
      if (PGPLOT_SAVE .eq. 1) then
         l_func = a_func
         l_rbuf(1) = a_rbuf(1)
         l_rbuf(2) = a_rbuf(2)
         l_rbuf(3) = a_rbuf(3)
         l_rbuf(4) = a_rbuf(4)
         l_rbuf(5) = a_rbuf(5)
         l_rbuf(6) = a_rbuf(6)
         l_nbuf = a_nbuf
         l_chr = a_chr
         l_lchr = a_lchr
         CALL MFDRIV(l_func, l_rbuf, l_nbuf, l_chr, l_lchr)
      endif
C
      return
      end
