#if !defined(PHBRIGHTOBJECTS_H)
#define PHBRIGHTOBJECTS_H

#include "phCalib1.h"
#include "phDgpsf.h"
#include "phObjects.h"
#include "phOffset.h"
/*
 * Encapsulate enough information to subtract a star's wings
 */
typedef struct {
   float p0;				/* central intensity */
   float rowc, colc;			/* row/column centre */
   float beta;				/* slope of wings */
   float rtrans;			/* transition radius */
   float sigmap;			/* power-law `sigma' */
} WINGOBJECT;				/* pragma SCHEMA */

WINGOBJECT *phWingobjectNew(void);
void phWingobjectDel(WINGOBJECT *);

void
phCompositeProfileCopy(COMP_PROFILE *cprof1,   /* target cprof */  
                       COMP_PROFILE *cprof2);   /* source cprof */

int 
phCompositeProfileAdd(COMP_PROFILE *prof, const OBJECT1 *obj1,
			   const OBJMASK *satur_mask, int is_it_wing, 
                           float profmin);
int 
phCompositeProfileFind(COMP_PROFILE *cprof, int medflg);

int
phSaturatedStarCentroid(const REGION *reg, /* region containing star */
			const OBJMASK *objmask, /* pixels in bright star */
			const DGPSF *psf, /* the core PSF */
			float sky,	/* sky level */
			float *xc, float *yc); /* returned position */

WINGOBJECT *
phBrightStarWings(OBJECT1 *obj1,	/* the object in question */
		  const COMP_PROFILE *cprof, /* a composite profile */
		  const FRAMEPARAMS *fparams, /* info on offsets etc. */
		  int medflg,		/* use median profile? */
		  float r,		/* use only profile to this radius */
		  BINREGION *wingreg,	/* add wings into this BINREGION */
		  float beta,		/* power law slope to assume */
		  float sigmap,		/* "sigma" for power law wings */
		  float frac,		/* fraction of power in wings (wrt
					   PSF counts) */
		  float rtrans,		/* transition radius to a parabola */
		  float nsigma);	/* mark pixels where we subtracted more
					   than nsigma*skysigma as NOTCHECKED*/

DGPSF *
phStarParamsFind(const COMP_PROFILE *comp_prof,	/* fitted profile */
		 const DGPSF *psf,	/* input values of parameters */
		 float profmin,		/* minimum profile value to consider */
		 int medflg,		/* use median not mean profile? */
		 int fit_g2,		/* fit a second Gaussian component */
		 int fit_p,		/* fit a power-law component */
		 int fit_sigmap,	/* fit "sigma" for power law */
		 int fit_beta,		/* fit index of power law */
		 float *chisq);		/* reduced chisq of fit */

float
phPSFCountsGetFromProfile(const OBJECT1 *obj1, /* the object */
			  const COMP_PROFILE *cprof, /* the composite */
			  int i0,	/* and the first good radial point */
			  int medflg,	/* use median profile? */
			  float r_max);	/* use only profile to this radius */

#endif

