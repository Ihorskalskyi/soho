'\"
'\" Copyright (c) 1993 The Regents of the University of California.
'\" Copyright (c) 1994 Sun Microsystems, Inc.
'\"
'\" See the file "license.terms" for information on usage and redistribution
'\" of this file, and for a DISCLAIMER OF ALL WARRANTIES.
'\" 
'\" @(#) flush.n 1.3 95/05/06 15:18:41
'\" 
.so man.macros
.TH flush n "" Tcl "Tcl Built-In Commands"
.BS
'\" Note:  do not modify the .SH NAME line immediately below!
.SH NAME
flush \- Flush buffered output for a file
.SH SYNOPSIS
\fBflush \fIfileId\fR
.BE

.SH DESCRIPTION
.PP
Flushes any output that has been buffered for \fIfileId\fR.
\fIFileId\fR must have been the return
value from a previous call to \fBopen\fR, or it may be
\fBstdout\fR or \fBstderr\fR to access one of the standard I/O streams;
it must refer to a file that was opened for writing.
The command returns an empty string.

.SH KEYWORDS
buffer, file, flush, output
