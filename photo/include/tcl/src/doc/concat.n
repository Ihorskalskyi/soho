'\"
'\" Copyright (c) 1993 The Regents of the University of California.
'\" Copyright (c) 1994 Sun Microsystems, Inc.
'\"
'\" See the file "license.terms" for information on usage and redistribution
'\" of this file, and for a DISCLAIMER OF ALL WARRANTIES.
'\" 
'\" @(#) concat.n 1.4 95/05/06 15:18:32
'\" 
.so man.macros
.TH concat n "" Tcl "Tcl Built-In Commands"
.BS
'\" Note:  do not modify the .SH NAME line immediately below!
.SH NAME
concat \- Join lists together
.SH SYNOPSIS
.VS
\fBconcat\fI \fR?\fIarg arg ...\fR?
.VE
.BE

.SH DESCRIPTION
.PP
This command treats each argument as a list and concatenates them
into a single list.
It also eliminates leading and trailing spaces in the \fIarg\fR's
and adds a single separator space between \fIarg\fR's.
It permits any number of arguments.  For example,
the command
.DS
\fBconcat a b {c d e} {f {g h}}\fR
.DE
will return
.DS
\fBa b c d e f {g h}\fR
.DE
as its result.
.PP
.VS
If no \fIarg\fRs are supplied, the result is an empty string.
.VE

.SH KEYWORDS
concatenate, join, lists
