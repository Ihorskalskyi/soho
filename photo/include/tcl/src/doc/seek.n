'\"
'\" Copyright (c) 1993 The Regents of the University of California.
'\" Copyright (c) 1994 Sun Microsystems, Inc.
'\"
'\" See the file "license.terms" for information on usage and redistribution
'\" of this file, and for a DISCLAIMER OF ALL WARRANTIES.
'\" 
'\" @(#) seek.n 1.3 95/05/06 15:19:23
'\" 
.so man.macros
.TH seek n "" Tcl "Tcl Built-In Commands"
.BS
'\" Note:  do not modify the .SH NAME line immediately below!
.SH NAME
seek \- Change the access position for an open file
.SH SYNOPSIS
\fBseek \fIfileId offset \fR?\fIorigin\fR?
.BE

.SH DESCRIPTION
.PP
Change the current access position for \fIfileId\fR.
\fIFileId\fR must have been the return
value from a previous call to \fBopen\fR, or it may be \fBstdin\fR,
\fBstdout\fR, or \fBstderr\fR to refer to one of the standard I/O
channels.
The \fIoffset\fR and \fIorigin\fR arguments specify the position at
which the next read or write will occur for \fIfileId\fR.
\fIOffset\fR must be an integer (which may be negative) and \fIorigin\fR
must be one of the following:
.TP
\fBstart\fR
The new access position will be \fIoffset\fR bytes from the start
of the file.
.TP
\fBcurrent\fR
The new access position will be \fIoffset\fR bytes from the current
access position; a negative \fIoffset\fR moves the access position
backwards in the file.
.TP
\fBend\fR
The new access position will be \fIoffset\fR bytes from the end of
the file.  A negative \fIoffset\fR places the access position before
the end-of-file, and a positive \fIoffset\fR places the access position
after the end-of-file.
.LP
The \fIorigin\fR argument defaults to \fBstart\fR.
This command returns an empty string.

.SH KEYWORDS
access position, file, seek
