'\"
'\" Copyright (c) 1993 The Regents of the University of California.
'\" Copyright (c) 1994 Sun Microsystems, Inc.
'\"
'\" See the file "license.terms" for information on usage and redistribution
'\" of this file, and for a DISCLAIMER OF ALL WARRANTIES.
'\" 
'\" @(#) eof.n 1.3 95/05/06 15:18:34
'\" 
.so man.macros
.TH eof n "" Tcl "Tcl Built-In Commands"
.BS
'\" Note:  do not modify the .SH NAME line immediately below!
.SH NAME
eof \- Check for end-of-file condition on open file
.SH SYNOPSIS
\fBeof \fIfileId\fR
.BE

.SH DESCRIPTION
.PP
Returns 1 if an end-of-file condition has occurred on \fIfileId\fR,
0 otherwise.
\fIFileId\fR must have been the return
value from a previous call to \fBopen\fR, or it may be \fBstdin\fR,
\fBstdout\fR, or \fBstderr\fR to refer to one of the standard I/O
channels.

.SH KEYWORDS
end, file
