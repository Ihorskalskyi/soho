'\"
'\" Copyright (c) 1993 The Regents of the University of California.
'\" Copyright (c) 1994 Sun Microsystems, Inc.
'\"
'\" See the file "license.terms" for information on usage and redistribution
'\" of this file, and for a DISCLAIMER OF ALL WARRANTIES.
'\" 
'\" @(#) close.n 1.3 95/05/06 15:18:31
'\" 
.so man.macros
.TH close n "" Tcl "Tcl Built-In Commands"
.BS
'\" Note:  do not modify the .SH NAME line immediately below!
.SH NAME
close \- Close an open file
.SH SYNOPSIS
\fBclose \fIfileId\fR
.BE

.SH DESCRIPTION
.PP
Closes the file given by \fIfileId\fR.
\fIFileId\fR must be the return value from a previous invocation
of the \fBopen\fR command; after this command, it should not be
used anymore.
If \fIfileId\fR refers to a command pipeline instead of a file,
then \fBclose\fR waits for the children to complete.
The normal result of this command is an empty string, but errors
are returned if there are problems in closing the file or waiting
for children to complete.

.SH KEYWORDS
close, file
