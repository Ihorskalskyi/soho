#!/usr/local/bin/perl -w

print STDOUT "FIXPIX program to avoid make_io glitches \n";

while (<>) {
    if(/PIX_TYPE = (\S*)/){
	$$PIX=$$1
	}
}

open(OFD,">.phObjc.h") || die("Cannot open .phObjc.h");

open(FD, "$(INC)/phObjc.h") || die("Cannot open $(INC)/phObjc.h");

while ($line = <FD>) {
    $line =~ s/PIX/$$PIX/g; 
    print OFD $line
    }

print "End of pixel fixation via Perl"; 
