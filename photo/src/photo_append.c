/*****************************************************************************
 ******************************************************************************
 **
 ** FILE:
 **	photo_append.c
 **
 ** ABSTRACT:
 **	This is the appended routines necessary for build up of PHOTO
 **     library without a main. This file has been obtained by deleting
 **     "main" from photo_main.c
 **
 **	This product is built on top of DERVISH and FTCL.
 **
 ** ENVIRONMENT:
 **	ANSI C.
 **
 ******************************************************************************
 ******************************************************************************
 */
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <signal.h>
#include "dervish.h"
#include "tclMath.h"
#include "astrotools.h"
#include "phSpanUtil.h"
#include "phSignals.h"
#include "phMeschach.h"			/* for phMeschachLoad() */

void phTclDervishDeclare(Tcl_Interp *interp);
void phTclVerbsDeclare(Tcl_Interp *interp);

#define	L_PHPROMPT	"photo> "   /* Command line prompt for PHOTO */
/*
 * declare variables/functions for the signal catching facilities
 */
DECLARE_CATCH;

/*
 * This is an shMalloc callback function. If you set the C variable
 * g_Serial_threshold to a number, this function will be called when
 * that block is allocated.
 *
 * If write_malloc_trace is true, a file called shMalloc.dat will be written
 * recording that call to malloc; if the threshold is ~0 this file will
 * be _large_, containing all calls to malloc (the same file can be written
 * for shFree --- see p_free_trace)
 *
 * I expect that g_Serial_threshold and write_malloc_trace will be set
 * using a debugger (you may have to set the scope of the variable with
 * something like shGarbage.g_Serial_threshold).
 */
static volatile int write_malloc_trace = 0;
static FILE *shMalloc_fil = NULL;

static void
p_malloc_trace(unsigned long thresh,
	       const SH_MEMORY *mem)
{
   static volatile int frequency = 0;	/* frequency of breaking */

   if(write_malloc_trace) {
      if(shMalloc_fil == NULL) {
	 if((shMalloc_fil = fopen("shMalloc.dat","w")) == NULL) {
	    shFatal("Can't open shMalloc.dat");
	 }
      }
      fprintf(shMalloc_fil,"shMalloc %ld %ld  %ld %s %d %ld %ld\n",
	      mem->serial_number, shMemBytesInUse(), shMemBytesInPool(),
	      mem->file, mem->line,
	      (long)mem->user_bytes, (long)mem->actual_bytes);
   }

   if(frequency > 0) {
      shMemSerialCB(thresh + frequency, p_malloc_trace);
   }
}

/*
 * And here's another, which can be used to check the heap for
 * corruption at any desired granularity (set by the variable frequency)
 *
 * Note that it resets the callback trigger
 */
static void
p_malloc_check(unsigned long thresh,
	       const SH_MEMORY *mem)	/* NOTUSED */
{
   static volatile int abort_on_error = 1; /* abort on first error? */
   static volatile int check_allocated = 1; /* check allocated blocks? */
   static volatile int check_free = 1;	/* check free blocks? */
   static volatile int frequency = 10;	/* frequency of checks */

   shAssert(mem != NULL);		/* use it for something */

   if(frequency > 0) {
#if DERVISH_VERSION >= 6 && DERVISH_MINOR_VERSION >= 8
      if(p_shMemCheck(check_allocated, check_free, abort_on_error)) {
	 shError("Continuing after memory error was detected");
      }
#endif
      shMemSerialCB(thresh + frequency, p_malloc_check);
   }
}

/*
 * Here's a callback function for shFree. See comments above
 * p_malloc_trace
 */
static void
p_free_trace(unsigned long thresh,	/* NOTUSED */
	     const SH_MEMORY *mem)
{
   if(write_malloc_trace) {
      if(shMalloc_fil == NULL) {
	 if((shMalloc_fil = fopen("shMalloc.dat","w")) == NULL) {
 	    shFatal("Can't open shMalloc.dat");
	 }
      }
      fprintf(shMalloc_fil,"shFree %ld %ld  %ld %s %d %ld %ld\n",
	      mem->serial_number, shMemBytesInUse(), shMemBytesInPool(),
	      mem->file, mem->line,
	      (long)mem->user_bytes, (long)mem->actual_bytes);
   }
}

/*****************************************************************************/
/*
 * Source a file given by an environment variable
 */
static int
source(Tcl_Interp *interp, char *file)
{
   char cmd[200];
   char *script = getenv(file);
   
   if(script != NULL) {
      if(*script != '\0') {
	 sprintf(cmd, "source %s",script);
	 printf("Executing commands in %s: ",script);
	 Tcl_Eval(interp, cmd);
	 putchar('\n');
	 if(*interp->result != '\0') {
	    printf("--> %s\n", interp->result);
	 }
	 fflush(stdout);
      }
      return(0);
   } else {
      return(-1);
   }
}

/*****************************************************************************/

static jmp_buf jmp_env;
static int initialised_setjmp = 0;	/* did we call setjmp() ? */

void
p_phSignalTrap(int sig)
{
   signal(sig, p_phSignalTrap);		/* re-install signal handler */
   
   if(initialised_setjmp) {
      longjmp(jmp_env, sig);
   } else {
      shError("trap: you must call setjmp before trapping signals");
   }
}

/*****************************************************************************/
/* 
Module main deleted for cracking reasons.
*/
