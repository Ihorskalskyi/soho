<HTML>
<TITLE>Deblend</TITLE>
<H1>Photo's Deblender</H1>


<UL>
<LI> <A HREF="#overview">Overview of Deblender</A>

<LI> <A HREF="#algorithms">Photo's Deblending Algorithm</A>

<LI> <A HREF="#book_keeping">Book Keeping for the Deblender</A>
</UL>
<P>

<H2><A NAME="overview">Overview of Deblender</A></H2>

The photo deblender is based on the peaks found in each object.
The basic idea is that we take the list
of all peaks found within the parent, in any band (a peak need only
appear in one band to be considered); each of these peaks is taken
to define the centre of a possible child.
<P>

For each child, in each band we create a <DFN>template</DFN>, an
approximate light distribution for the child (for a star, this
template would be simply the PSF). We then assume that in each band
<PRE>
    parent = sum_i (weight_i template_i)
</PRE>
and solve for the weights. With these
in hand we can then assign the flux associated with each child in each pixel
according to the value of <CODE>weight_i template_i</CODE>, and our task is
complete.
<P>

Note that, because the children are associated with the master list
of peaks detected in <EM>any</EM> band, the resulting children are
defined consistently in each band so it makes sense to (e.g.) ask
questions about their colours.

<H2><A NAME="algorithms">Photo's Deblending Algorithm</A></H2>

Photo's deblender works by looking at the list of
<CODE><A HREF="datatypes.html#PEAK">PEAK</A></CODE>s in an
<CODE><A HREF="datatypes.html#OBJC">OBJC</A></CODE>, and treats each
as the centre of a child. This list was produced by
<A HREF="measureObjects.html#">Measure Objects</A>.
The deblender proceeds as follows:

<UL>
<LI>
When an OBJC is passed to the
deblender, the first thing that's checked is if it has too many peaks
(set as <CODE>child_max</CODE> in the
<CITE><A HREF="planfiles.html#fpParams">fpParams.par</A></CITE>); if it does
the <CODE>OBJECT1_NODEBLEND</CODE> bit is set, and the deblender gives up.

<LI>
Next, each child (i.e. peak) is examined in each band. If, in that band,
the peak is consistent with being a PSF the
<CODE>OBJECT1_DEBLENDED_AS_PSF</CODE> bit in <CODE>flags</CODE> is set, the
star is subtracted, and
bits are set in an <CODE><A HREF="datatypes.html#OBJMASK">OBJMASK</A></CODE>
that indicate which pixels were modified.

<LI> Then photo goes through each child creating a <DFN>template</DFN>, that
is:
<UL>
<LI> If the object is too large for the deblender to handle (basically,
more rows or columns than half a frame) the <CODE>OBJECT1_TOO_LARGE</CODE>
bit is set and photo gives up on that child.

<LI> If the object reaches the edge of the frame in any band, the
<CODE>OBJECT1_EDGE</CODE> bit is set, 
and photo gives up on that child.

<LI> If the <CODE>OBJECT1_DEBLENDED_AS_PSF</CODE> bit is set, the template
is taken to be a PSF, centred at the centre of the child. Otherwise, photo
goes through the object looking at pairs of pixels
<CODE>(rowc + y, colc + x)</CODE> and <CODE>(rowc - y, colc - x)</CODE>,
where <CODE>(rowc, colc)</CODE> is the centre of the child. The template
is made up of the <EM>smaller</EM> of the two pixel values, corrected
for <A HREF="misc.html#debias">bias</A> by adding <CODE>0.56(sky sigma)</CODE>.
<P>

For pixels whose `mirror' lies outside the parent object's OBJMASK, the
value of the template is taken to be zero. If one of the pixels
lies in an area where a PSF has been subtracted (see above), the
other pixel's value is used. If both
pixels were PSF contaminated, use the average of the two. In all of these
cases, no bias correction is applied.

<LI> Next, we run an object finder on the smoothed template; the smoothing
length and threshold are those used in
<A HREF="findFaintObjects.html#ffo">Find Faint Objects</A>. If the template
is not detected at this stage it is presumably because the peak that
corresponds to the child being processed was too faint to have been
detected as an isolated object; the <CODE>OBJECT1_NOTDETECTED</CODE>
bit is set and photo gives up on that
<CODE><A HREF="datatypes.html#OBJECT1">OBJECT1</A></CODE>.

<LI> Once we've found a template in each band, we check that it
was detected in at least one of them; if not photo gives up on that child.

<LI> Then minimal templates are created in all bands where we failed to
find one; these are simply multiples of the PSF; the
<CODE>OBJECT1_DEBLENDED_AS_PSF</CODE> bit is set to indicate this.

<LI> Finally, all parts of templates that do not lie within the OBJECT1s
OBJMASK are set to zero.

</UL>

<LI> With templates available for all children (in all colours), we solve
for their weights. This is done using (unweighted) least-squares. The
Normal Equations are solved by eigen value decomposition, which allows us
to discover if the problem is ill-posed (i.e. the matrix is singular).
<P>

We achieve this by going through all the children in each band looking for
the smallest eigenvalue; if it is too small, that child is rejected,
and the parent has the <CODE>OBJECT1_DEBLEND_PRUNED</CODE> bit set (both
for the OBJC and also for all the OBJECT1s). The least squares procedure
is then repeated, until a solution is reached. If this solution consists
of only a single child, the parent's <CODE>OBJECT1_BLENDED</CODE> bit
is turned off, and the deblender stops.

<LI> With the weights for each template in hand, and after setting any
negative weights to zero, for each pixel in the parent we evaluate
<CODE>sum == sum_i (weight_i * template_i)</CODE>. If this is at least 10,
that pixel in the i-th deblended child has value
<CODE>weight_i * template_i</CODE>; otherwise we re-evaluate <CODE>sum</CODE>
using the <EM>smoothed</EM> templates. If the resulting <CODE>sum</CODE>
is greater than 2, the pixels are given values of
<CODE>weight_i * smoothed_template_i</CODE>
otherwise we share the flux equally among all the children. XXX

</UL>

At the end of this procedure, each child knows the values of all its pixels
that lie within the OBJMASK defining the parent, and it is ready to be
passed to <A HREF="measureObjects.html#">Measure Objects</A>.

<H2><A NAME="book_keeping">Book Keeping for the Deblender</A></H2>

Once an object is deblended, it is represented as an
<CODE><A HREF="datatypes.html#OBJC">OBJC</A></CODE> as usual, but
with a non-<CODE>NULL</CODE> <CODE>children</CODE> field which points
at one of its children; in addition its
<CODE>OBJECT1_BLENDED</CODE> bit is set, and <CODE>nchild</CODE>
is one or more.
<P>

Each child has a non-<CODE>NULL</CODE> <CODE>parent</CODE> field which
points to its parent, and may have a <CODE>sibbs</CODE> field too. It
has its
<CODE>OBJECT1_CHILD</CODE> bit set.
<P>

</HTML>
