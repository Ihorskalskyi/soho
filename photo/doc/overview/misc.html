<HTML>
<TITLE>Miscellaneous Algorithms</TITLE>
<H1>Miscellaneous Algorithms</H1>

Here we discuss algorithms whose discussion doesn't seem to belong in
any specific part of the overview.
<P>

<UL>
<LI>
Using <A href="#bresenham_interp">Bresenham algorithm's</A> to interpolate.
<LI>
<A href=#interpolation>Interpolating</A> over bad pixels.
<LI>
The <A href="#softBias">Software Bias</A>.
<LI>
Removing the <A HREF="#debias">Bias</A> due to Choosing
the Minimum of two Pixel Values
<LI>
Determining a Frame's Statistics using
<A HREF="#regStatsFromQuartiles">regStatsFromQuartiles</A>

<LI>
Fitting <DFN><A HREF="misc.html#taut_spline">Taut Splines</A></DFN> to Data

<LI>
<A HREF="#asinh">Using <CODE>asinh</CODE> to Control the Dynamic Range of
Data that can be Negative</A>

<LI>
How to <A HREF="#mode">Estimate the Mode</A> of a distribution

</UL>

<H2>Using <A name="bresenham_interp">Bresenham algorithm's</A>
to interpolate</H2>

When doing linear interpolation, it is possible to use a variant
of Bresenham's algorithm (usually used in driving bit-mapped displays).
Doing so is fast, and photo makes use of this trick. Apart from speed,
and rounding errors, this is equivalent to a naive linear interpolator.

<H2><A name="interpolation">Interpolation</A></H2>
Given a set of pixel values, <CODE>I[i]</CODE>, with one value (say
<CODE>I[0]</CODE>) missing, how should we go about replacing it? Due
to laziness on the part of RHL, we restrict ourselves to 1-dimensional
interpolation; the adopted algorithm would trivially extend to working
in 2-d, but the book-keeping is a little intimidating.
<P>

The simplest solution would be to interpolate linearily,
<PRE>
    I'[0] = (I[-1] + I[1])/2,
</PRE>
but this doesn't perform well for marginally sampled data.
<P>

We know something about the auto-correlation present in our data, as
it has been convolved with the PSF; we should look for interpolation schemes
that minimise errors when interpolating over missing data in a signal
consisting solely of the PSF.
<P>
<H3>Interpolation: Theoretical Considerations</H3>

As usual, there are a variety of ways of considering this problem, all
of which are closely related. We give three approaches below.
<P>

<UL>
<LI>Brute Force
<BR>
If we approximate the PSF as a
Gaussian, experimentation indicates that the mean error in interpolating
is minimised for a filter of the form
<PRE>
    I'[0] = (-I[-2] + 3*I[-1] + 3*I[1] - I[2])/4
</PRE>
for a reasonably wide range of FWHM (providing that the data is at least
marginally sampled). This is the filter that we've adopted for the
photometric pipeline.
<P>

<LI>Arguments based directly on Nyquist's Theorem
<BR>
Consider the following argument:<BR>
If the data
were perfectly band-limited, with no power at the Nyquist limit, we could
consider our signal to be the original data added to a delta function
of amplitude <CODE>-I[0]</CODE>; consider Fourier transforming this ---
it's clear that we'll have power <CODE>-I[0]</CODE> at the Nyquist limit,
which is the value that we want. The formula giving the Nyquist power is
<PRE>
    ... - I[-3] + I[-2] - I[-1] - I[1] + I[2] - I[3] + ...
</PRE>
so our estimate becomes
<PRE>
    I'[0] = ... + I[-3] - I[-2] + I[-1] + I[1] - I[2] + I[3] + ...
</PRE>
and the formula given above is seen as a suitably belled version
of this result.

<LI>Linear Prediction
<BR>
We would like to thank Bill Press for discussions on this topic.
Note carefully that the <I>Linear</I> in Linear Prediction does not mean
linear interpolation.
<P>
The basic idea is to divide the I[i] into signal and noise,
I[i] == s[i] + n[i], 
and then to estimate value of pixel ?, S[?], as a weighted sum:
<PRE>
    S[?] = Sum_i d_i I[i] == Sum_i d_i (s[i] + n[i])
</PRE>
<P>
We can then attempt to mimimise the expectation value, E, of the squared
residual S[?] - s[?]:
<PRE>
    E == <(S[?] - s[?])^2> = <(Sum_i d_i (s[i] + n[i]) - s[?])^2>
</PRE>
i.e., differentiating with respect to d_i,
<PRE>
    <(s[i] + n[i])(S[?] - Sum_j d_j (s[j] + n[j]))> = 0
</PRE>
or
<PRE>
    < s[i]S[?] > = Sum_j d_j < (s[i] + n[i])(s[j] + n[j]) >
                 = Sum_j d_j < s[i]s[j] > + < n[i]n[j] >
</PRE>
if we assume that signal and noise are uncorrelated. If we assume that our
images consists of uncorrelated PSFs, then
&lt;s[i]s[j]&gt; == e^{-(i-j)^2/alpha^2} and
&lt;n[i]n[j]&gt; == N_i delta_{ij} and we may invert this matrix equation to
estimate the d_i.

Details (and figures) are provided in
<A HREF="http://www.astro.princeton.edu/~rhl/photomisc/interpolate.ps">Linear Prediction and Interpolation</A>.

</UL>

<H2><A name="softBias">Software Bias</A></H2>
As we are using unsigned 16-bit integers, bias and sky subtraction could
lead to negative pixels, which would be a problem;
photo gets around this by adding an arbitrary constant to the data just
after removing the bias (it's currently 1000 and is a #define in the C code);
when sky is subtracted the
soft_bias is added back in (and removed from the sky estimate!).
As there was a hardware bias of this order in
the raw frames, doing so doesn't reduce the dynamic range of the data.


<H2><A NAME="debias">Removing the Bias due to Choosing
the Minimum of Two Pixel Values</A></H2>

It is easy enough to calculate that the expectation value of the minimum of two
Gaussian variates (with zero mean and unit variance; N(0,1)) is
-0.5641895835. If we ever replace a pixel value by the minimum
of two estimates of its true value, we will systematically
underestimate its value; we should (and do) add a correction of
approximately 0.56sigma.


<H2> Determining a Frame's Statistics using
<A NAME="regStatsFromQuartiles">regStatsFromQuartiles</A></H2>

The routine <CODE>regStatsFromQuartiles</CODE> constructs a histogram
of pixel intensities for an image, and derives any of a wide range of
parameters. See the source code for details.

<H2><A NAME="taut_spline">Fitting <DFN>Taut Splines</DFN> to Data</A></H2>

A <DFN>Taut Spline</DFN> is a regular (cubic) spline with the extra
constraint that there be no superfluous turning points; it's rather as
if you took the piece of flexible wood that provides the term
spline and pulled on each end to remove unwanted wiggles.
<P>

This is achieved by inserting extra knots at cleverly chosen places, and
seems to work well in practice.

<H2><A NAME="asinh">Using <CODE>asinh</CODE> to Control the Dynamic Range of Data that can be Negative</A></H2>

It is a common practive to take logarithm of numbers that span many orders
of magnitude, e.g. to display an object's radial profile. This has two main
disadvantages:
<UL>
<LI> If the data is non-positive, the logarithm is badly behaved
<LI> The errors on very small values become vary large (and 1-sigma ranges
can easily be negative).
</UL>

Both of these problems can be circumvented by using <CODE>asinh(x/Delta)</CODE>
instead of <CODE>lg(x)</CODE>, where <CODE>Delta</CODE> is
some constant with magnitude comparable to the expected error in x. This has
the nice property of being (within a scale) a simple logarithm for x &gt;&gt; Delta,
and approximately linear for x &lt;~ Delta.
<P>

For more details, see the <A HREF="http://www.journals.uchicago.edu/AJ/journal/issues/v118n3/990094/990094.html">paper</A>
by Szalay, Lupton, and Gunn.

<H2><A name=mode>How to Estimate the Mode</A></H2>

The result that
<SAMP>mode = 3*median - 2*mean</SAMP> is readily proved from a Gram-Charlier
series up to the third Hermite polynomial; to the same order it's easily
shown that
<SAMP>
<PRE>
      mode = median - (Q25 + Q75 - 2*median)/eta^2
</PRE>
</SAMP>
where <SAMP>eta ~ 0.673</SAMP> is the 75th percentile of an
<SAMP>N(0,1)</SAMP> Gaussian. We can then estimate the mode as
<SAMP>
<PRE>
      5.4*median - 2.2*(Q25 + Q75)
</PRE>
</SAMP>
The variance of this estimate is approximately
<SAMP>
<PRE>
      (4.78)^2 sigma^2/N;
</PRE>
</SAMP>
much worse then the mean (with variance <CODE>sigma^2/N</CODE>),
and 3.8 times worse than the median (whose variance is
<CODE>(pi/2)sigma^2/N</CODE>).
<P>


</HTML>
