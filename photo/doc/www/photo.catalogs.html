<title>Using Simulation Catalogs and Decision Trees </title>
<h1>Using Simulation Catalogs and Decision Trees </h1>
<h4>Michael Richmond</h4>
<h4>March 29, 1996</h4>

<P>
If you want to get to the nitty-gritty, select one of these:
<UL>
<LI><a href="#match_output">The match_output procedure</a>
<LI><a href="#example">An example of using match_output</a>
<LI><a href="#tree_create">An example of creating a decision tree</a>
<LI><a href="#tree_apply">An example of classifying with a decision tree</a>
<LI><a href="#chain_plot">chain_plot: a useful plotting procedure</a>
</UL>

<P>
<b>Warning: you must link against the checked-in version of ASTROTOOLS
(which is later than version v2_3) in order to use the function
described in this document. </b>


<h2>Introduction</h2>
<P>
It is sometimes useful to be able to run PHOTO (or other
SDSS pipelines) on simulated data, in order to check that
the output values match those in the catalogs.
This document briefly describes the catalog format
and gives one example of a means to compare catalog information
against that produced by the Frames Pipeline.

<P>
In addition, once one has matched catalog information with
detected objects, one can attempt to discover rules which
separate objects into different categories.  PHOTO has routines
based on decision trees to do this job.  This document shows
examples of creating and using decision trees for classification.

<P>
At the time of writing (May 29, 1996), the decision tree
routines use information in one bandpass only; the code
will have to be extended in the future to allow the user
to base classification on information from several 
bandpasses considered together.

<h2>Catalog format</h2>
<P>
The following describes the format of information used to 
create a simulated 10-degree-by-10-degree patch of sky
at Fermilab, during the year 1995.  It may not apply to
later simulations.

<P>
The catalogs are in a set of ASCII files, one per type
of objects (for example, all asteroids are in <i>asteroidcat</i>).
The files are 
<UL>
<LI><i>asteroidcat</i> - 100 asteroids
<LI><i>galaxycat</i> - a very large file (124 Meg) containing 1.18 million
      fake galaxies from a large N-body run
<LI><i>gscprunecat</i> - a subset of the HST Guide Star Catalog, consisting
      of real stars, largely in the range 10 < V < 15
<LI><i>ppmcat</i> - a subset of the PPM catalog, consisting of real stars,
      largely in the range 7 < V < 10 (?).  There are only 519 of these
      in the entire area.
<LI><i>qq</i> - a set of 1079 objects; I don't know their type or source
<LI><i>starqsocat</i> - another large file (32 Meg), consisting of 303,000
      fake stars, based on Bahcall-Soneira models of the galaxy
<LI><i>ugccat</i> - a subset of the Uppsala General Catalog of galaxies,
      containing 116 galaxies over the area
</UL>
There is also a file <i>KEY</i>, which explains the format of these
files.  I include it below:
<pre>
----------------------------  start of file KEY ------------------------------
(the columns are: (make your screen wide))

    lambda       eta      redshift  PA     mag   j T    AX    RE      u       g       r       i       z
         (degrees)          z      (deg)   (g/r) j=junk axis  eff.            SDSS mags

</pre>
<P>
Where lambda ranges from -5.0 to 5.0 degrees, 
eta ranges from -3.75 to 6.25 degrees
(survey coords, use surveyToEq in astrotools to convert to RA, DEC 2000)
z is redshift, all stars have 0 redshift
PA is position angle in degrees for galaxies
mag is a fiducal mag, which can be ignored, it duplicates g or r mag later
j is junk type, can be ignored,
T=Type: 0=star/qso, 1= Ellip, 2=S0, 3=Sa, 4=Sb, 5=Sc, 6=Sc, 7=Irr
AX is axis ratio, 1= round object 0.1 is elongated object
RE = pixels of the semi-major axis or scale radius in pixels
ugriz are SDSSmags, no errors are included.


<P>
The catalogs are (all have same format):
<P>
asteroidcat:  a few asteroids which move in position from filter to filter.  
<br>
galaxycat: the DHW simulated galaxy catalog, with improved colors from AJC. DHW Clusters are included. 
<br>
gscprunecat: Hubble Guide star catalog stars for the lambda,eta box.  PPM duplicates pruned.
<br>
ppmcat: PPM (astrometric standards) in the lambda,eta box.
<br>
starqsocat: DPS generated point-like stars and QSOs in sdss colors, randomly distributed in position.
<br>
ugccat: actual UGC and other bright galaxies in the lambda,eta box.
<br>

<pre>
----------------------------  end   of file KEY ------------------------------
</pre>
      
<P>
As of March 11, 1996, you can find copies of these files in 
<UL>
<LI> sdss.fnal.gov: /data/versant/bigsimcats
<LI> lucifer.princeton.edu: /scr0/richmond/catalogs
</UL>

<h2>Reading catalogs into PHOTO</h2>
<P>
There is a structure designed to contain the information kept in
these catalogs: 
<a href="../../include/phCatobj.h">CATOBJ</a>
You can read a given catalog into a CHAIN of 
<a href="../../include/phCatobj.h">CATOBJ</a>
structures with the 
<a href="./photo.c.html#phSimCatalogToChain">phSimCatalogToChain</a>
function (or its TCL binding, simCatalogToChain).

<h2>Comparing PHOTO output with catalogs</h2>
<P>
Since the catalogs contain positions in (lambda, eta), and the
PHOTO output files positions in (row, col), it's necessary to
perform some sort of coordinate transformation before
trying to match up items in the two sets.
Fortunately, the Postage Stamp pipeline stores the
<a href="http://www-sdss.fnal.gov:8000/astrotools/doc/www/atTrans.html">TRANS</a>
structures which relate (row, col) to Great Circle coords (mu, nu)
in a file with prefix 
<a href="http://www-sdss.fnal.gov:8000/dm/flatFiles/psCB.html">psCB</a>
for each run.  It is possible, with some tedious calculations, to
convert the (row, col) of each detected object into (lambda, eta),
or vice versa.

<P>
One difficulty is that the entire simulation catalogs are <b>very</b>
large, containing about 1.5 million objects.  If one were to check
each of these objects to see if it was inside the boundaries of 
a single CCD field, one would waste a lot of time.  It is therefore
useful, when one is running tests on just a small portion of the
entire 10x10-degree field, to create a single file which contains
only those input objects will fall into the portion of interest.
I urge users to do this, and will assume that they have done
so below.

<P>
To save the user time and effort, there is a single high-level TCL procedure
which calls other procedures to to the coordinate conversion, 
pattern-matching, and so forth, necessary to match objects
in catalogs with those in PHOTO output.  You can find this procedure,
<i>match_output</i>, in the file
<a href="../../etc/catalogs.tcl">$PHOTO_DIR/etc/catalogs.tcl</a>
First I will give its arguments, then an example of its usage.

<h4><a name="match_output">The match_output procedure</a></h4> 
<pre>
  USAGE: match_output catalog_chain catalog_magname \ 
                      photo_dir run col field magame magindex ref_filt \ 
                      cat_to_det_trans \
                      matched_cat matched_det unmatched_cat unmatched_det 

  where input arguments are

        catalog_chain          is a CHAIN of CATOBJ structures
        catalog_magname        is the name of the field in CATOBJ used as
                                     magnitude for matching purposes
        photo_dir              is a directory containing PHOTO output
        run                    is the run number of the output
        col                    is the column number in the imaging camera
        field                  is the field number of interest
        magname                is the name of the field in PHOTO output used
                                     as magnitude for matching purposes
        magindex               is an index into the "magname" array 
                                     or -1, if "magname" is not an array
        ref_filt               is the filter whose TRANS structure to the sky
                                     is used for a preliminary solution

  and output arguments are
  
        cat_to_det_trans       is a TRANS structure taking (lambda, eta) from
                                     the catalog into (row, col) of the image
        matched_cat            is a CHAIN of catalog objects which match
                                     objects detected by PHOTO
        matched_det            is a CHAIN of detected PHOTO objects which 
                                     match those in the catalog
        unmatched_cat          is a CHAIN of catalog objects which DON'T match
                                     objects detected by PHOTO
        unmatched_det          is a CHAIN of detected PHOTO objects which 
                                     DON'T match those in the catalog

</pre>


<P>
The <i>ref_filt</i> argument may almost always be set to any 
integer between 0 and 4 with no resulting difference.  It is 
used only in a preliminary calculation to figure out what
area of the sky is covered by the given <i>field</i>.

<P>
One unfortunate side-effect of this routine is that the 
particular PHOTO measurement used as the "fiducial measurement
of brightness" for the matching procedure, the field
<i>magname[magindex]</i>, will have been converted from
its original flux units to magnitudes and then back again.
In the process, any fluxes originally negative will
be set to exactly 0.0.

<P>
The output arguments <i>matched_cat</i>, etc., are completely
analagous to those created by the ASTROTOOLS
function 
<a href="http://www-sdss.fnal.gov:8000/astrotools/doc/www/tclPMatch.html#atMatchChains">atMatchChains</a>.  
It might help to read the 
<a href="http://www-sdss.fnal.gov:8000/astrotools/doc/www/astrotools.pmatch.html">ASTROTOOLS matching routines documentation</a>
The CHAINs <i>matched_cat</i> and <i>matched_det</i> 
will have exactly the same number of items, with corresponding
items in each being matches.  The user can then examine
these matched pairs at his leisure.

<h4><a name="example">An example of using match_output</a></h4> 
<P>
Here's an example showing how one might prepare to run this procedure
(it's stolen from the 
<a href="../../etc/catalogs.tcl">catalogs.tcl</a> file, actually):

<pre>

# 
# This is an example of how one might use the TCL proc "match_output"
#    to end up with a set of 4 CHAINs containing items which
#    did/didn't match from catalog/detected.  The output parameters
#    in the call to "match_output" are
#
#    cat_to_det_trans    TRANS taking (lambda, eta) -> (row, col)
#    matched_cat         CHAIN of CATOBJs which match detected objects
#    matched_det         CHAIN of detected objects which match catalog
#    unmatched_cat       CHAIN of CATOBJs which DON'T match detected objects
#    unmatched_det       CHAIN of detected objects which DON'T match catalog
#

  set catch [read_catalogs]
  set catmag rmag
  set photo_dir /scr0/richmond/photodata/frames_7/output 
  set run 581 
  set col 1 
  set field 20
  set magfield fibercounts
  set magindex 2
  set ref_filt 2

  match_output $catch $catmag \
                    $photo_dir $run $col $field $magfield $magindex $ref_filt \
                    cat_to_det_trans \
                    matched_cat matched_det unmatched_cat unmatched_det 

</pre>

<h2>Decision Trees: what are they, and how to use them? </h2>
<P>
Decision trees are entities which separate objects into groups
based on the properties of those objects.  They are primitive,
in a sense, since they act by creating hyper-planes in 
multi-dimensional parameter space.  More sophisticated 
structures, such as neural networks, divide parameter
space with curved surfaces.  However, the simplicity
of decision trees makes their results easy for humans
to interpret, and quick to calculate.

<P>
I used the book
<b>Classification and Regression Trees</b>,
by Leo Breiman, Jerome H. Friedman, Richard A. Olhen and
Charles J. Stone
(Wadsworth International Group: Belmont CA) 1984,
as the source for all the routines.  You can
read it for more information about decision trees.

<P>
You can find an example of using decision trees from TCL
in the script
<A href="../../etc/catalogs.tcl">catalogs.tcl</a>.
The following section follows that script closely.

<h4><a name="tree_create">Creating a decision tree</h4>
<P>
As a first step, we must have both a catalog of information
on objects in a field, and a list of objects detected
and measured by PHOTO in the same field.
The catalog information must have a format like that of the
giant simulations created by David Weinberg, Brian Yanny
and Chris Stoughton; the 
<a href="../../include/phCatobj.h">CATOBJ structure</a>
holds this information, and is used by the routines described
below.
The PHOTO output must be in the form written to disk
by the Frames pipeline, in
<a href="../../include/phObjc.h">OBJC_IO</a> structures.
The 
<a href="#match_output">match_output</a> procedure described above
creates CHAINs of CATOBJ and OBJC_IO structures with corresponding
elements, so I suggest that you use it to set up for the following steps.

<P> 
So, here's how to create a decision tree, which will divide the
objects into classes:
<OL>
<LI>use <a href="#match_output">match_output</a> to create a pair
    of CHAINs, one with CATOBJ structures and one with OBJC_IO structures.
<pre>
            match_output $catch $catmag \
                    $photo_dir $run $col $field $magfield $magindex $ref_filt \
                    cat_to_det_trans \
                    matched_cat matched_det unmatched_cat unmatched_det 
</pre>
<LI>use the catalog information to assign a type to each detected object,
    via the <a href="photo.tcl.html#simTypesAssign">simTypesAssign</a>
    procedure:
<pre>
            simTypesAssign $matched_cat $matched_det
</pre>
<LI>initialize the parameters which will be used for classification.
    <b>This must be done once in each session before any other decision
    tree code can be run</b>.  One uses the 
    <a href="photo.tcl.html#dTreeSetParams">dTreeSetParams</a> routine,
    which takes a list of parameter names in the 
    <a href="../../include/phObjc.h">OBJC_IO</a> structure, enclosed
    inside curly brackets:
<pre>
            dTreeSetParams { fiberCounts w_cc w_cr petroRad fracPSF }
</pre>
<LI>create a tree (which will consist of a single, root node) via
    <a href="photo.tcl.html#dTreeCreate">dTreeCreate</a>:
<pre>
            set tree [dTreeCreate $matched_det $treeband]
</pre>
    where <i>$treeband</i> is the index of the bandpass in which this 
    decision tree
    will operate.  The current routines are limited to use information in only
    one bandpass.  It is likely that <i>$treeband = 0</i> will use
    parameters measured in <i>u'</i>, <i>$treeband = 1</i> parameters
    measured in <i>g'</i>, and so on.
<LI>grow this initial tree into a maximal tree, which divides the
    objects into as many groups as possible.  The maximal tree will
    be much larger than any tree we actually will use.  We call
    the
    <a href="photo.tcl.html#dTreeGrow">dTreeGrow</a> procedure to do the job:
<pre>
            dTreeGrow $tree
</pre>
<LI>prune the maximal tree down in steps to smaller and smaller trees,
    ending ultimately with a tree consisting of a single node.
    Each tree is written to an ASCII disk file with name <i>treeDump.N</i>,
    where <i>N</i> is some number.  Large trees have small numbers, and
    vice versa.  This pruning is done by the 
    <a href="photo.tcl.html#dTreePrune">dTreePrune</a> procedure,
    which prints out the name of each tree as it prunes, and also
    returns the number of trees created:
<pre>
            set num [dTreePrune $tree]
</pre>
</UL>

<P>
After executing these steps, you should have a large number of ASCII
files on disk, with names like 
<i>treeDump.0</i>,
<i>treeDump.1</i>, up to
<i>treeDump.num-1</i>.
Each one contains a version of the tree intermediate between the
maximal and minimal tree.  The tree itself (pointed to by <i>$tree</i>)
is now in its minimal size.
You can read a description of the ASCII file format, and what it means,
in the documentation for
<a href="photo.tcl.html#dTreeWriteAsAscii">dTreeWriteAsAscii</a>.

<P>
We can wade through all the disk files and decide which one we
want to use for classifying the objects in some other field.
Suppose that we choose <i>treeDump.5</i>.
We can then read it back into memory with 
<a href="photo.tcl.html#dTreeReadAsAscii">dTreeReadAsAscii</a>.
<pre>
    set goodtree [dTreeReadAsAscii treeDump.5]
</pre>

<h4><a name="tree_apply">An example of classifying with a decision tree</a></h4>
<P>
Suppose now that we have settled on the proper tree to use for 
classifying objects, and we have read it into the handle <i>$goodtree</i>
via 
<a href="photo.tcl.html#dTreeReadAsAscii">dTreeReadAsAscii</a>.

<P>
We can now consider objects in a <i>different</i> field for which
we have both catalog information and PHOTO output.
Let us once again use the 
<a href="#match_output">match_output</a> routine to create
a pair of CHAINs with corresponding 
<a href="../../include/phCatobj.h">CATOBJ</a> and 
<a href="../../include/phObjc.h">OBJC_IO</a> structures.
<pre>
       match_output $catch $catmag \
                    $photo_dir $run $col $field \
                    $magfield $magindex $ref_filt \
                    cat_to_det_trans \
                    matched_cat matched_det unmatched_cat unmatched_det
</pre>

<P>
Once again, we assign known types to the detected OBJC_IO structures
with 
<a href="photo.tcl.html#simTypesAssign">simTypesAssign</a>:
<pre>
       simTypesAssign $matched_cat $matched_det
</pre>

<P>
It might be useful at this point to run a little TCL procedure
(defined in 
<a href="../../etc/catalogs.tcl>catalogs.tcl</a>)
called <b>count_types</b>.  It counts the number of objects
of each type (STAR, GALAXY, QSO) which are in the
set of detected objects, based on the catalog classifications.

<P>
Now we are ready to run these detected objects through the decision
tree and see how well it does at classifying objects in this field;
remember, the tree was built using information from a different
area on the sky, so its performance on these objects will be a good
test of its accuracy.

<P>
The 
<a href="photo.tcl.html#dTreeTest">dTreeTest</a> 
routine passes each item in a CHAIN of OBJC_IOs, in turn,
through a given decision tree.  It compares the 
classification given by the tree with that recorded
in the OBJC_IO itself, and keeps track of the number
of "right" and "wrong" values which occur.
<pre>
         set res [dTreeTest $matched_det $treeband $goodtree]
</pre>
As before, <i>$treeband</i> tells the code which parameters to
use in the classification process.  You must make sure to use
the same passband index when you create a tree, and when you
apply that tree to new objects.

<P>
The output of this test will be printed to the screen, and look
something like this:
<pre>
         5 nod   364 obj:   297 right (82%)     67 wrong (18%)  Rts 0.18 (0.02)
</pre>
Here, the <i>$goodtree</i> had 5 terminal nodes, and, of the 364 objects
in the <i>$matched_det</i> set, it correctly classified 82%, and
mis-classified 18%.  You can ignore the other numbers.

<P>
By running the
<a href="photo.tcl.html#dTreeTest">dTreeTest</a> 
routine on several different trees (based on different <i>treeDump</i>
files), you can decide which tree does the best job of classification.
See the discussions in Breiman on this topic -- it can be
a bit tricky.

<h4>What's missing</h4>
<P>
Ultimately, we will need a routine which, when handed a
CHAIN of detected OBJC_IO structures, and a tree, 
will assign types to the OBJC_IO structures.  The
<a href="photo.tcl.html#dTreeTest">dTreeTest</a> 
<b>does not actually assign types</b>; it merely records
what <i>would</i> happen if we were to use the tree 
for classification.

<P>
As of March 29, 1996, this necessary routine has not been written.

<h4>Further examples of using decision trees</h4>
<P>
If you look near the end of the file
<a href="../../etc/catalogs.tcl">catalogs.tcl</a>,
you'll see a section after all the procedures are defined
which is enclosed in 
<pre>
      if { 0 } {
         ...
      }
</pre>
Look carefully at this code for examples of the above routines.
The latter half of the example compares the performance
of trees of various sizes in data from different fields.

<P>
If you want to run the code yourself, you'll have to modify
the global variables at the top of the file, and the quantities
defined immediately after the 
<pre>
      if { 0 } {
</pre>
You'll also need a set of catalog files, and a set of PHOTO
output files.


<h2><a name="chain_plot">chain_plot: a useful plotting procedure</a></h2>
<P>
It is often nice, when trying classification schemes, to be able
to plot one measured quantity against another -- it can help
to see if there is a clear separation between stars and galaxies,
for example, in some plane of parameter space.
In the file
<a href="../../etc/catalogs.tcl">catalogs.tcl</a>,
there is a TCL procedure called <b>chain_plot</b>
which may be useful.  Basically, given a CHAIN of OBJC_IO 
structures from the output of some run of PHOTO, it allows
you to plot quickly one measured quantity against another.
For example,
<pre>
         chain_plot $matched_det psfCounts w_cc 2
</pre>
will make a plot of the measured <i>w_cc</i> (on the Y-axis) vs. 
the measured <i>psfCounts</i> (on the X-axis), using
values in the 2'nd element of each parameter's array
(that is, using quantities measured in the 2'nd passband,
which is probably r').  
Objects are
plotted with different symbols, depending on their "type" 
fields.  
The user may specify explicit ranges for the plot:
<pre>
         chain_plot $matched_det psfCounts w_cc 2 $xmin $xmax $ymin $ymax
</pre>
but, if he does so, must specify all four extremes.




</html>

