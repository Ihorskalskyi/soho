<HTML>
<TITLE>Outputs of the Photometric Pipeline</TITLE>
<BODY>

<H1>Outputs of the Photometric Pipeline</H1>
<h2>August 4, 1995 </h2>

<P>Go back to 
<A HREF=photo.home.html>Photo Home Page</A>

<P>
This is a companion to the document
<a href="./photo.inputs.html">Inputs of the Photometric Pipeline</a>,
but, instead of listing the files required to run each 
pipeline, this document describes the files produced by
both the Postage-stamp and Frames pipelines.
Note that some of the outputs of the Postage-stamp pipeline
are inputs to the Frames pipeline.

<UL>
<LI> <a href="#PSoutputs">Postage-stamp pipeline outputs </a>
<LI> <a href="#Foutputs">Frames pipeline outputs </a>
</ul>

<h2> <a name="PSoutputs">Postage-stamp pipeline outputs </a> </h2>

<UL> 
<LI> <a href="#ps_bias">Bias vector for each frame </a>
<LI> <a href="#ps_drift">Bias drift for each frame </a>
<LI> <a href="#ps_calib">Calibration data, per field </a>
<LI> <a href="#ps_ct">Photometric calibration, by time  </a>
<LI> <a href="#ps_flatfield">Flatfield vector for each frame </a>
</UL>

<h3> <a name="ps_bias">Bias vector for each frame</a> </h3>
<P>
This file contains the 1-D bias vector which will be subtracted
from each row of its frame.  
The vector is just wide enough to cover the data region of
the frame -- in the current thinking, 1 row x 2048 columns.

<P>
This is a FITS image, with type U16.
The pixel values are multiplied by <b>bias_scale</b>, which at
the moment is 32.

<P>
File name has form
<pre>

         B-0-1-g.fit
           ^ ^ ^ 
           | | ------------- filter in front of this CCD chip
           | |                 (equivalent to position of CCD in row of camera)
           | --------------- position of CCD in column of camera
           ----------------- run number
  
</pre>

<h3> <a name="ps_drift">Bias drift for each frame</a> </h3>
<P>
This file, one per chip per run, provides an estimate of
the difference between the bias level in each frame of that run,
and the bias level in the frames used to create the 
master bias image.
Since there are two amplifiers on each chip, there are
two numbers needed per frame: the difference between 
the left-half bias and the master bias, and the difference
between the right-half bias and the master bias.

<P>
These differences are kept in a scaled format, multiplied
by <b>bias_scale</b>.  For example, if <b>bias_scale</b>=32,
then a value of -64 in the bias-drift file would mean that 
the bias level in
a frame is 2 (64/32) DN lower than the bias level in
the average dark frame.

<P>
This file is a FITS image, in 32-bit signed integer
format. 
If one is reducing <b>N</b> consecutive fields in
a run of the Postage-stamp pipeline, then
this file contains <b>N</b> rows, one per field.
Each row contains 2 columns, for the left and right
halves of the chip, respectively.

<P>
File name has form
<pre>

         BD-0-1-g.fit
            ^ ^ ^ 
            | | ------------ filter in front of this CCD chip
            | |                (equivalent to position of CCD in row of camera)
            | -------------- position of CCD in column of camera
            ---------------- run number
  
</pre>

<h3> <a name="ps_calib">Calibration data, per field</a> </h3>
<P>
This file contains several different kinds of calibration
for a given field (that is, for the images in all the passbands
in that field).  This FITS binary table contains the
following information:

<UL>
<LI> the frame number 
<LI> number of filters 
<P>
Then follows a set of quantities pertaining to the zero'th filter,
then a set for the first filter, then the second filter, and so on.
Each set consists of:
<P>
<LI> filter name, as a NULL-terminated string
<LI> sky value for the image in this filter (DN)
<LI> slope of the sky value, from the top to the bottom of the image (DN)
<LI> bias drift for left-hand side (DN*<b>bias_scale</b>)
<LI> bias drift for right-hand side (DN*<b>bias_scale</b>)
<LI> total number of counts in a 20'th mag star in this filter (DN)
<LI> uncertainty in the number of counts in 20'th mag star (DN)
<LI> sigma (width) for the inner component of DGPSF, in the row-direction
       (in units of pixels)
<LI> sigma (width) for the outer component of DGPSF, in the row-direction
<LI> sigma (width) for the inner component of DGPSF, in the col-direction
<LI> sigma (width) for the outer component of DGPSF, in the col-direction
<LI> ratio of the inner to outer component of DGPSF
<LI> a-component of the TRANS structure that converts coords in this
       filter to coords in the reference filter's frame
<LI> b-component of the TRANS
<LI> c-component of the TRANS
<LI> d-component of the TRANS
<LI> e-component of the TRANS
<LI> f-component of the TRANS
<LI> node of the great circle upon which this frame was scanned
       (I believe this must be the same for all filters, actually)
<LI> inclination  of the great circle upon which this frame was scanned
       (I believe this, too, must be the same for all filters)
<LI> a-component of the TRANS structure that converts coords in this
       filter to great-circle coords (mu, nu) on the sky
<LI> b-component of the TRANS
<LI> c-component of the TRANS
<LI> d-component of the TRANS
<LI> e-component of the TRANS
<LI> f-component of the TRANS
</UL>


<P>
File name has form
<pre>

         CB-0-1-0.fit
            ^ ^ ^ 
            | | ------------ field number 
            | -------------- position of CCD in column of camera
            ---------------- run number
  
  
</pre>

<h3> <a name="ps_ct">Photometric calibration, by time </a> </h3>
<P>
In addition to the <a href="#ps_calib">field-by-field calibration data</a>,
the Postage-stamp pipeline produces a smaller file which contains
only the photometric calibration data on a (roughly) hourly basis;
in fact, the field-by-field photometric calibrations are derived
by interpolation from these hourly figures.

<P>
These numbers are derived by comparing the instrumental magnitudes
of <i>secondary standards</i>, also called <i>overlap stars</i>,
observed by the Monitor Telescope and by the 2.5-m telescope.
Since the Monitor Telescope pipeline produces calibrated
magnitudes on the standard system for these stars, one can
calculate the zero-point offset of the 2.5-m telescope's
instrumental system from the standard system.  This file
contains those zero-point offsets, together with estimated
uncertainties.

<p>
The file is a FITS binary table, which contains:

<UL>
<LI> the time at which a set of secondary standards were observed
       with the 2.5-m telescope (MJD)
<LI> number of filters 
<P>
Then follows a set of quantities pertaining to the zero'th filter,
then a set for the first filter, then the second filter, and so on.
Each set consists of:
<P>
<LI> filter name, as a NULL-terminated string
<LI> total number of counts in a 20'th mag star in this filter (DN)
<LI> uncertainty in the number of counts in 20'th mag star (DN)
</UL>

<P>
File name has form
<pre>
         CT-0-1.fit
            ^ ^ 
            | -------------- position of CCD in column of camera
            ---------------- run number
</pre>
Note that there is just a single such file produced for
each column of the camera, for each run.  This file contains
zero-points derived from all the <i>patches</i> scanned
by both the MT and the 2.5-m in a particular run.


<h3> <a name="ps_flatfield">Flatfield vector </a> </h3>
<P>
The Postage-stamp pipeline creates an individual flatfield-vector
for each raw image.  This vector is 1-row by 2048-columns,
covering only the data portion of the raw frame.
Actually, the file contains an <i>inverse</i> flatfield vector,
meaning that the raw frame must be multiplied (not divided) 
by the vector to recover the desired values.
For example, if there is some vignetting, so that less light
falls on the edges of the frame than the middle, a
true flatfield vector would have values that varied
across columns like so: 

<pre>
   left edge: low     middle: high    right edge: low
</pre>

<p>
This file, on the other hand, being an <i>inverse</i>
flatfield vector, would contain values in the following
pattern:

<pre>
   left edge: high    middle: low     right edge: high
</pre>

<P>
The file is 16-bit unsigned integer FITS image.
All pixel values are scaled by the factor <b>FSHIFT</b>
(see <a href="../../include/prvt/phCorrectFrames_p.h">phCorrectFrames_p.h</a>).
Thus, if raw data were perfect, with no corrections needed,
all pixels would have value <b>FSHIFT</b>.  If, on the other hand, 
column 100 receives twice as much light as the average,
column 101 receives the average amount, and
column 102 receives half the average amount, then
the vector would have values:

<pre>
   col 100: 0.5*<b>FSHIFT</b>    col 101: <b>FSHIFT</b>   col 102: 2*<b>FSHIFT</b>
</pre>


<P>
File name has form
<pre>
         FF-0-1-g-0.fit
            ^ ^ ^ ^
            | | | ---------- field number
            | | ------------ filter in front of this CCD chip
            | |                (equivalent to position of CCD in row of camera)
            | -------------- position of CCD in column of camera
            ---------------- run number
</pre>


<h2> <a name="Foutputs">Frames pipeline outputs </a> </h2>

<P>
The Frames Pipeline produces the following files:

<UL> 
<LI> <a href="#fp_corrected">Full-size corrected frames </a>
<LI> <a href="#fp_binned">Binned corrected frames </a>
<LI> <a href="#fp_mask">Mask per frame</a>
<LI> <a href="#fp_noise">Noise, per frame</a>
<LI> <a href="#fp_framestat">Statistics of the properties of each field</a>
<LI> <a href="#fp_object">Object list, per field</A>
<LI> <a href="#fp_dump">Dump file, per field</A>
</UL>

<h3> <a name="fp_corrected">Full-size corrected frames</a> </h3>
<P>
These are the full-size FITS images after all processing
has been completed.
The bias and overscan regions have been removed,
leaving only the sections of the raw frame upon which
photons fall.
Each one is 1482-rows by 2048-columns,
and so contains an <i>overlap area</i> of 128 extra
rows, taken from the succeeding frame in the scan.
The images are in 16-bit unsigned integer FITS format, with
a <b>BZERO</b> value of 32768 subtracted from each pixel
to force the 16-bit unsigned data into the range of the
standard FITS 16-bit signed int.

<P>
In addition to the usual bias-subtraction and flat-fielding,
these frames have been modified in additional ways.
First, bright stars (and their wings) <i>may</i> have
been subtracted from the image; the <b>boc_psffit</b>
parameter in the
<a href="./photo.inputs.html#fp_plan">fp_software_parameters</a>
file controls this subtraction.  Any object which fits
the PSF with a chi-sq smaller than <b>boc_psffit</b>
is judged to be a star, and will be subtracted from the
image (although it is measured and its properties stored for
later reference).

<P>
Another extra processing step is sky-subtraction: after
bright objects have been found and, if necessary, subtracted,
the 
<a href="../../src/sky.c">phSkySubtract</a> 
function fits a smooth model to the large-scale structure
of the background in the frame and subtracts it.
In order to avoid negative pixel values, the function
then adds a fixed amount, <b>soft_bias</b>
(another entry in the 
<a href="./photo.inputs.html#fp_plan">fp_software_parameters</a>
file)
back to each pixel in the frame.
Every corrected image, therefore, should have a "sky" value
of approximately <b>soft_bias</b>.

<P>
File name has form
<pre>
          C-0-1-g-0.fit
            ^ ^ ^ ^
            | | | ---------- field number
            | | ------------ filter in front of this CCD chip
            | |                (equivalent to position of CCD in row of camera)
            | -------------- position of CCD in column of camera
            ---------------- run number
</pre>


<h3> <a name="fp_binned">Binned corrected images</a> </h3>
<P>
These are simply copies of the corrected frames which
have been binned 4x4 using the PHOTO utility function
<b>shRegIntBin</b>.
Each one is 370-rows by 512-columns.
Like the full-size corrected frames, 
each image is a 16-bit signed integer FITS file
with <b>BZERO</b> set to 32768 to convert
unsigned 16-bit integers into the signed range.

<P>
Note that <b>shRegIntBin</b> averages pixel values after
binning them, so that, just as in the full-size corrected
frame, the typical sky value is <b>soft_bias</b>,
and the value of a saturated super-pixel is 65535.

<P>
In some cases, a binned image may show boundaries of 
the sky-subtraction procedure, or vertical stripes.
By its nature, a binned image must emphasize any
low-contrast but extended features.

<P>
File name has form
<pre>
        BIN-0-1-g-0.fit
            ^ ^ ^ ^
            | | | ---------- field number
            | | ------------ filter in front of this CCD chip
            | |                (equivalent to position of CCD in row of camera)
            | -------------- position of CCD in column of camera
            ---------------- run number
</pre>

<h3> <a name="fp_mask">Mask per frame</a> </h3>
<P>
As data is reduced, a <i>mask</i> is produced to keep track
of any defects or features present in each pixel of every
frame.  This <i>mask</i> consists of a single 8-bit byte for
every pixel in the image.  By default, a pixel's mask
value is set to 0; when some feature is detected,
one particular bit is changed to 1.  The meaning 
of each bit in the mask is encoded in the file
<a href="../../include/phMaskbittype.h">phMaskbittype.h</a>.


<P>
Typically, one might use a <i>mask</i> in the following
way, as an overlay on a displayed image:

<pre>
photo> set reg [regReadAsFits [regNew] C-0-1-g-0.fit]
h1
photo> set mask [maskReadAsFits [maskNew] M-0-1-g-0.fit]
h2
photo> set sao [saoDisplay h1 ]
1
photo> saoMaskColorSet {green orange yellow red blue purple magenta cyan}
photo> saoMaskDisplay $mask -p bitset -s $sao
</pre>

<P>
The <i>mask</i> is stored as an 8-bit unsigned integer
FITS image.
It is the same size as a corrected image, 1482-rows by 2048-columns.

<P>
File name has form
<pre>
          M-0-1-g-0.fit
            ^ ^ ^ ^
            | | | ---------- field number
            | | ------------ filter in front of this CCD chip
            | |                (equivalent to position of CCD in row of camera)
            | -------------- position of CCD in column of camera
            ---------------- run number
</pre>

<h3> <a name="fp_noise">Noise, per frame</a> </h3>
<P>
Based on the known gain and readout noise of a CCD chip,
and assuming Poisson statistics in the incoming
photons,
one can calculate, pixel-by-pixel, the total variance
in the number of electrons which should be present.
One can then calculate a standard deviation from the variance,
and convert from units of electrons to DN.
The result will be the expected standard deviation
of counts in each pixel of an image.
This file contains this "noise" for all the pixels in
one given image.

<P>
The "noise" image is stored as an 8-bit unsigned integer FITS
image, with the same number of rows (1482) and columns (2048)
as the corrected image to which it corresponds.

<P>
File name has form
<pre>
          N-0-1-g-0.fit
            ^ ^ ^ ^
            | | | ---------- field number
            | | ------------ filter in front of this CCD chip
            | |                (equivalent to position of CCD in row of camera)
            | -------------- position of CCD in column of camera
            ---------------- run number
</pre>

<P>
<i>At the time of writing (August 4, 1995), the Frames
Pipeline still produces a noise image for each corrected
image.  However, there are plans to delete this 
output file in the future and use the 
<a href="#fp_binned">binned image</a> as a 
source from which to calculate the noise in each pixel. </i>


<h3> <a name="fp_framestat">Statistics on the properties of each field</a> </h3>
<P>
This file contains a set of numbers which describe overall
properties of a field.  
Astronomers running the survey can
use these as quick checks on the quality of an image --
if the sky value in a frame is 4 times higher than
that of its predecessor, and 15 times as many objects
are found, one might reasonably conclude that a very
bright star has just passed through the field of view.

<P>
Each file is a FITS binary table, containing:

<UL>
<LI> the field number
<LI> the total number of objects found (in the union of all passbands)
<LI> the total number of stars found
<LI> the total number of galaxies found
<LI> the number of filters in which data was taken
<P>
For each filter, there follows a set of additional values:
<P>
<LI> minimum pixel value (DN) in the corrected frame, before sky-subtraction. 
       Note that the sky-subtraction function will cause most
       areas of the sky to have values of roughly <b>soft_bias</b>,
       which is typically much larger than the pixel values present
       before sky-subtraction.
<LI> maximum pixel value (DN) in the corrected frame, before sky-subtraction 
<LI> mean pixel value (DN) in the corrected frame, before sky-subtraction 
<LI> standard deviation of pixel values (DN) in the corrected frame, 
       before sky-subtraction
<LI> sky value (DN) derived for the frame, before sky-subtraction
<LI> number of "bad" pixels in the frame
<LI> number of bright objects detected in this filter's image alone
<LI> number of faint objects detected in this filter's image alone
</UL>


<P>
File name has form
<pre>
  framestat-0-1-0.fit
            ^ ^ ^ 
            | | ------------ field number
            | -------------- position of CCD in column of camera
            ---------------- run number
</pre>

<h3> <a name="fp_object">Object list, per field</a> </h3>
<P>
This <i>large</i> file contains the main result of the
Photometric Pipeline: a list of all the objects found
in the field (in all filters, combined), each of which
has a measured position, brightness, and other properties.
In addition, a cutout region, the <i>atlas image</i>,
of the area around every object,
together with a mask, is stored in this file as well.
Since there are typically hundreds of objects in a single
field, for which the Pipeline calculates several tens of properties,
and cuts an <i>atlas image</i> which may be tens of pixels on
a side, this file can easily grow very large.  
Simulated images of the Survey area yield files that
can range from 3 to 20 Megabytes.

<P>
This FITS binary table is created with the PHOTO utilities
<b>fitsBinTblOpen</b>, 
<b>fitsBinTblHdrWrite</b>,
<b>fitsBinTblRowWrite</b>, and
<b>fitsBinTblClose</b>.
These functions are defined in the source code file
<a href="../../src/photoFitsIO.c">photoFitsIO.c</a>.
Basically, there is one row in the FITS binary table
for each object; within that row are entries for
the measured parameters in each passband.
The file can be read with the standard DERVISH utility
<a href="http://www-sdss.fnal.gov:8000/dervish/contrib/www/fits2Schema.html#fits2Schema">fits2Schema</a>,
although it will ignore the <i>atlas images</i>.


<P>
File name has form
<pre>
       objc-0-1-0.fit
            ^ ^ ^ 
            | | ------------ field number
            | -------------- position of CCD in column of camera
            ---------------- run number
</pre>

<h3> <a name="fp_dump">Dump file, per field</a> </h3>
<P>
This optional output file is produced only if 
the <a href="./photo.inputs.html#fp_plan">fp_data_processing_plan</a>
contains the entry
<pre>
     write_dump     1
</pre>

<P>
This file is very similar to the 
<a href="#fp_object">object list</a>
FITS binary table, but has a slightly different 
format. 
It is the <i>dump</i> of the complete measured object list,
which is a chain of 
<a href="../../include/phObjc.h">OBJC</a>
structures.
You can read more about the <i>dump</i> format 
<a href="http://www-sdss.fnal.gov:8000/dervish/doc/www/diskio.feature.html">here</a>.
To restore a <i>dump</i> file back into a chain of 
<a href="../../include/phObjc.h">OBJCs</a>
is simple:
<pre>
photo> dumpOpen file.dump r
photo> set objchain [dumpHandleRead]
h1
photo> dumpClose
</pre>

<P>
In this manner, one can access not only the measured parameters
of every object (as one can with the FITS binary tables),
but also display and interact with the <i>atlas image</i>
and mask attached to each object.

<P>
File name (if present) has form
<pre>
       objc-0-1-0.dump
            ^ ^ ^ 
            | | ------------ field number
            | -------------- position of CCD in column of camera
            ---------------- run number
</pre>

</body>
</html>
