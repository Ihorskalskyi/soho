#
# Quality assurance parameters for postage stamp pipeline
#

### parameters used by QA processor after running PSP for 1 column
 # sky
qa_sky_w1                1.0            # lower values trigger warning             
qa_sky_w2            10000.0            # higher values trigger warning            
qa_sky_f1                0.0            # lower values cause fatal error            
qa_sky_f2            66000.0            # higher values cause fatal error          
 # drift
qa_drift_w1           -100.0            # lower values trigger warning             
qa_drift_w2            100.0            # higher values trigger warning            
qa_drift_f1          -1000.0            # lower values cause fatal error            
qa_drift_f2           1000.0            # higher values cause fatal error 
 # PSF parameters        
   # sigma1
qa_sigma1_w1             0.1            # lower values trigger warning             
qa_sigma1_w2            10.0            # higher values trigger warning            
qa_sigma1_f1             0.0            # lower values cause fatal error            
qa_sigma1_f2            20.0            # higher values cause fatal error          
   # sigma2
qa_sigma2_w1             1.0            # lower values trigger warning             
qa_sigma2_w2            20.0            # higher values trigger warning            
qa_sigma2_f1             0.0            # lower values cause fatal error            
qa_sigma2_f2            50.0            # higher values cause fatal error  
   # b
qa_b_w1                  0.0            # lower values trigger warning             
qa_b_w2                  1.0            # higher values trigger warning            
qa_b_f1                  0.0            # lower values cause fatal error            
qa_b_f2                100.0            # higher values cause fatal error 
   # sigmap
qa_sigmap_w1             0.0            # lower values trigger warning             
qa_sigmap_w2            50.0            # higher values trigger warning            
qa_sigmap_f1             0.0            # lower values cause fatal error            
qa_sigmap_f2           100.0            # higher values cause fatal error  
   # p0
qa_p0_w1                -0.00001        # lower values trigger warning             
qa_p0_w2                10.0            # higher values trigger warning            
qa_p0_f1                -0.1            # lower values cause fatal error            
qa_p0_f2              1000.0            # higher values cause fatal error 
   # beta
qa_beta_w1               2.0            # lower values trigger warning             
qa_beta_w2              10.0            # higher values trigger warning            
qa_beta_f1               0.0            # lower values cause fatal error            
qa_beta_f2             100.0            # higher values cause fatal error  
   # chi2
qa_chi2_w1               0.1            # lower values trigger warning             
qa_chi2_w2               2.0            # higher values trigger warning            
qa_chi2_f1              -0.1            # lower values cause fatal error            
qa_chi2_f2              10.0            # higher values cause fatal error        
 # app_corr
qa_app_corr_w1           0.4            # lower values trigger warning             
qa_app_corr_w2           2.5            # higher values trigger warning            
qa_app_corr_f1           0.0            # lower values cause fatal error            
qa_app_corr_f2          10.0            # higher values cause fatal error          
 # PTchi2  (chi2 obtained for a fit to data for flux20(t) given by PT)
qa_PTchi2_w1            -0.01           # lower values trigger warning             
qa_PTchi2_w2             2.0            # higher values trigger warning            
qa_PTchi2_f1            -0.01           # lower values cause fatal error            
qa_PTchi2_f2            10.0            # higher values cause fatal error        
 # flux20
qa_flux20_w1             1.0            # lower values trigger warning             
qa_flux20_w2         10000.0            # higher values trigger warning            
qa_flux20_f1             0.0            # lower values cause fatal error            
qa_flux20_f2         65000.0            # higher values cause fatal error  
  


### parameters used by makePSPtables and makeQAtables ###

## output directories
pspQAoutdir   default   # dump all PSP qa plots to this dir, default is /objcs 
pspQAhttpdir  default   # dump all PSP qa html to this dir, default is /objcs 

## for triggering "red" entries 
# PSF width statistics
PSFcol          6   # PSF width is taken from this column
PSFband         r   # PSF width is taken for this band
Cwidth        4.0   # max. allowed PSF width (also triggers HOLE field quality)
badPSFmax       5   # max. percentage of fields to have PSF worse than Cwidth
NstarsMin      10   # minimum required number of PSF stars
statusMax       5   # max. percentage of fields with PSP status > 0
badFieldMaxCol  5   # max. percentage of unacceptable fields in a single column
badFieldMaxAll 15   # max. percentage of unacceptable fields in all 6 columns
noiseMinAll   0.7   # min. normalized noise
noiseMaxAll   1.3   # max. normalized noise
noiseIgnore    u2   # noisy chips with more relaxed thresholds 
noiseMinSpecial 0.5 # min. normalized noise for noisy chips
noiseMaxSpecial 2.0 # max. normalized noise for noisy chips

# for catching bad fields with spikes in the PSFwidth
delwidthMax   0.3   # maximum difference between seeing and the mean value for
                    # adjacent fields (in arcsec)

# the brightest acceptable sky (mag/arcsec2) 
uSkyMin      21.2
gSkyMin      21.0
rSkyMin      20.0
iSkyMin      19.2
zSkyMin      18.0

# maximum acceptable aperture correction errors (mag)
# these values also trigger HOLE field quality
uApCerrMax   0.10    
gApCerrMax   0.07   
rApCerrMax   0.05   
iApCerrMax   0.07  
zApCerrMax   0.10  
ApCbiasMax   0.15

### temp until psfmag-apmag errors understood XXX (ZI)
uApCerrMax   0.15    
gApCerrMax   0.10   
rApCerrMax   0.10   
iApCerrMax   0.10  
zApCerrMax   0.15  

### parameters used when gauging fields quality
fieldBuffer  100   # field range for declaring chunks of bad fields
