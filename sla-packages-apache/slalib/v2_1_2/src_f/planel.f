      SUBROUTINE sla_PLANEL (DATE, JFORM, EPOCH, ORBINC, ANODE, PERIH,
     :                       AORQ, E, AORL, DM, PV, JSTAT)
*+
*     - - - - - - -
*      P L A N E L
*     - - - - - - -
*
*  Heliocentric position and velocity of a planet, asteroid or comet,
*  starting from orbital elements.
*
*  Given:
*     DATE      d      date, Modified Julian Date (JD - 2400000.5)
*     JFORM     i      choice of element set (1-3, see Note 3, below)
*     EPOCH     d      epoch of elements (TT MJD)
*     ORBINC    d      inclination (radians)
*     ANODE     d      longitude of the ascending node (radians)
*     PERIH     d      longitude or argument of perihelion (radians)
*     AORQ      d      mean distance or perihelion distance (AU)
*     E         d      eccentricity
*     AORL      d      mean anomaly or longitude (radians, JFORM=1,2 only)
*     DM        d      daily motion (radians, JFORM=1 only)
*
*  Returned:
*     PV        d(6)   heliocentric x,y,z,xdot,ydot,zdot of date,
*                                       J2000 equatorial triad (AU,AU/s)
*     JSTAT     i      status:  0 = OK
*                              -1 = illegal JFORM
*                              -2 = illegal E
*                              -3 = illegal AORQ
*                              -4 = illegal DM
*                              -5 = failed to converge
*
*  Notes
*
*  1  DATE is the instant for which the prediction is required.  It is
*     in the TT timescale (formerly Ephemeris Time, ET) and is a
*     Modified Julian Date (JD-2400000.5).
*
*  2  The elements are with respect to the J2000 ecliptic and equinox.
*
*  3  Three different element-format options are available:
*
*     Option JFORM=1, suitable for the major planets:
*
*     EPOCH  = epoch of elements (TT MJD)
*     ORBINC = inclination i (radians)
*     ANODE  = longitude of the ascending node, big omega (radians)
*     PERIH  = longitude of perihelion, curly pi (radians)
*     AORQ   = mean distance, a (AU)
*     E      = eccentricity, e (range 0 to <1)
*     AORL   = mean longitude L (radians)
*     DM     = daily motion (radians)
*
*     Option JFORM=2, suitable for minor planets:
*
*     EPOCH  = epoch of elements (TT MJD)
*     ORBINC = inclination i (radians)
*     ANODE  = longitude of the ascending node, big omega (radians)
*     PERIH  = argument of perihelion, little omega (radians)
*     AORQ   = mean distance, a (AU)
*     E      = eccentricity, e (range 0 to <1)
*     AORL   = mean anomaly M (radians)
*
*     Option JFORM=3, suitable for comets:
*
*     EPOCH  = epoch of perihelion (TT MJD)
*     ORBINC = inclination i (radians)
*     ANODE  = longitude of the ascending node, big omega (radians)
*     PERIH  = argument of perihelion, little omega (radians)
*     AORQ   = perihelion distance, q (AU)
*     E      = eccentricity, e (range 0 to 10)
*
*  4  Unused elements (DM for JFORM=2, AORL and DM for JFORM=3) are
*     not accessed.
*
*  5  The reference frame for the result is with respect to the mean
*     equator and equinox of epoch J2000.
*
*  6  The algorithm is adapted from the EPHSLA program of
*     D.H.P.Jones (private communication, 1996).  The method is
*     based on Stumpff's Universal Variables;  see Everhart and
*     Pitkin (1983, Am.J.Phys.51,712).
*
*  P.T.Wallace   Starlink   29 May 1998
*
*  Copyright (C) 1998 Rutherford Appleton Laboratory
*-

      IMPLICIT NONE

      DOUBLE PRECISION DATE
      INTEGER JFORM
      DOUBLE PRECISION EPOCH,ORBINC,ANODE,PERIH,AORQ,E,AORL,DM,PV(6)
      INTEGER JSTAT

*  Gaussian gravitational constant (exact)
      DOUBLE PRECISION GCON
      PARAMETER (GCON=0.01720209895D0)

*  Canonical days to seconds
      DOUBLE PRECISION CD2S
      PARAMETER (CD2S=GCON/86400D0)

*  Sin and cos of J2000 mean obliquity (IAU 1976)
      DOUBLE PRECISION SE,CE
      PARAMETER (SE=0.3977771559319137D0,
     :           CE=0.9174820620691818D0)

*  Test value for solution and maximum number of iterations
      DOUBLE PRECISION TEST
      INTEGER NITMAX
      PARAMETER (TEST=1D-11,NITMAX=20)

      INTEGER NIT,N

      DOUBLE PRECISION PHT,ARGPH,Q,W,CM,ALPHA,
     :                 DT,FC,FP,PSI,PSJ,BETA,S0,S1,S2,S3,
     :                 FF,FDOT,PHS,SW,CW,SI,CI,SO,CO,
     :                 X,Y,Z,XDOT,YDOT,ZDOT



*  Validate arguments.
      IF (JFORM.LT.1.OR.JFORM.GT.3) THEN
         JSTAT = -1
         GO TO 999
      END IF
      IF (E.LT.0D0.OR.E.GT.10D0.OR.(E.GE.1D0.AND.JFORM.NE.3)) THEN
         JSTAT = -2
         GO TO 999
      END IF
      IF (AORQ.LE.0D0) THEN
         JSTAT = -3
         GO TO 999
      END IF
      IF (JFORM.EQ.1.AND.DM.LE.0D0) THEN
         JSTAT = -4
         GO TO 999
      END IF

*
*  Transform elements into standard form:
*
*  PHT   = epoch of perihelion passage
*  ARGPH = argument of perihelion (little omega)
*  Q     = perihelion distance (q)
*
*  Also computed is the combined mass CM = (M+m).

      IF (JFORM.EQ.1) THEN
         PHT = EPOCH-(AORL-PERIH)/DM
         ARGPH = PERIH-ANODE
         Q = AORQ*(1D0-E)
         W = DM/GCON
         CM = W*W*AORQ*AORQ*AORQ
      ELSE IF (JFORM.EQ.2) THEN
         PHT = EPOCH-AORL*SQRT(AORQ*AORQ*AORQ)/GCON
         ARGPH = PERIH
         Q = AORQ*(1D0-E)
         CM = 1D0
      ELSE IF (JFORM.EQ.3) THEN
         PHT = EPOCH
         ARGPH = PERIH
         Q = AORQ
         CM = 1D0
      END IF

*  The universal variable alpha.  This is proportional to the total
*  energy of the orbit:  -ve for an ellipse, zero for a parabola,
*  +ve for a hyperbola.

      ALPHA = CM*(E-1D0)/Q

*  Time from perihelion to date (in Canonical Days:  a canonical day
*  is 58.1324409... days, defined as 1/GCON).

      DT = (DATE-PHT)*GCON

*  First Approximation to the Universal Eccentric Anomaly, PSI,
*  based on the circle (FC) and parabola (FP) values.

      FC = DT/Q
      W = (3D0*DT+SQRT(9D0*DT*DT+8D0*Q*Q*Q))**(1D0/3D0)
      FP = W-2D0*Q/W
      PSI = (1D0-E)*FC+E*FP

*  Successive Approximations to Universal Eccentric Anomaly.

      NIT = 1
      W = 1D0
      DO WHILE (ABS(W).GE.TEST)

*     Form half angles until BETA below maximum (0.7).
         N = 0
         PSJ = PSI
         BETA = ALPHA*PSI*PSI
         DO WHILE (ABS(BETA).GT.0.7D0)
            N = N+1
            BETA = BETA/4D0
            PSJ = PSJ/2D0
         END DO

*     Calculate Universal Variables S0,S1,S2,S3 by nested series.
         S3 = PSJ*PSJ*PSJ*((((((BETA/210D0+1D0)
     :                          *BETA/156D0+1D0)
     :                          *BETA/110D0+1D0)
     :                          *BETA/72D0+1D0)
     :                          *BETA/42D0+1D0)
     :                          *BETA/20D0+1D0)/6D0
         S2 = PSJ*PSJ*((((((BETA/182D0+1D0)
     :                      *BETA/132D0+1D0)
     :                      *BETA/90D0+1D0)
     :                      *BETA/56D0+1D0)
     :                      *BETA/30D0+1D0)
     :                      *BETA/12D0+1D0)/2D0
         S1 = PSJ+ALPHA*S3
         S0 = 1D0+ALPHA*S2

*     Double angles until N vanishes.
         DO WHILE (N.GT.0)
            S3 = 2D0*(S0*S3+PSJ*S2)
            S2 = 2D0*S1*S1
            S1 = 2D0*S0*S1
            S0 = 2D0*S0*S0-1D0
            N = N-1
            PSJ = 2D0*PSJ
         END DO

*     Improve the approximation.
         FF = Q*S1+CM*S3-DT
         FDOT = Q*S0+CM*S2
         W = FF/FDOT
         PSI = PSI-W
         IF (NIT.LT.NITMAX) THEN
            NIT = NIT+1
         ELSE
            JSTAT = -5
            GO TO 999
         END IF
      END DO

*  Speed at perihelion.

      PHS = SQRT(ALPHA+2D0*CM/Q)

*  In a Cartesian coordinate system which has the x-axis pointing
*  to perihelion and the z-axis normal to the orbit (such that the
*  object orbits counter-clockwise as seen from +ve z), the
*  perihelion position and velocity vectors are:
*
*    position   [Q,0,0]
*    velocity   [0,PHS,0]
*
*  Using the Universal Variables we project these vectors to the
*  given date:

      X = Q-CM*S2
      Y = PHS*Q*S1
      XDOT = -CM*S1/FDOT
      YDOT = PHS*(1D0-CM*S2/FDOT)

*  To express the results in J2000 equatorial coordinates we make a
*  series of four rotations of the Cartesian axes:
*
*            axis     Euler angle
*
*     1      z        argument of perihelion (little omega)
*     2      x        inclination (i)
*     3      z        longitude of the ascending node (big omega)
*     4      x        J2000 obliquity (epsilon)
*
*  In each case the rotation is clockwise as seen from the +ve end
*  of the axis concerned.

*  Functions of the Euler angles.
      SW = SIN(ARGPH)
      CW = COS(ARGPH)
      SI = SIN(ORBINC)
      CI = COS(ORBINC)
      SO = SIN(ANODE)
      CO = COS(ANODE)

*  Position.
      W = X*CW-Y*SW
      Y = X*SW+Y*CW
      X = W
      Z = Y*SI
      Y = Y*CI
      W = X*CO-Y*SO
      Y = X*SO+Y*CO
      PV(1) = W
      PV(2) = Y*CE-Z*SE
      PV(3) = Y*SE+Z*CE

*  Velocity (scaled to AU/s).
      W = XDOT*CW-YDOT*SW
      YDOT = XDOT*SW+YDOT*CW
      XDOT = W
      ZDOT = YDOT*SI
      YDOT = YDOT*CI
      W = XDOT*CO-YDOT*SO
      YDOT = XDOT*SO+YDOT*CO
      PV(4) = CD2S*W
      PV(5) = CD2S*(YDOT*CE-ZDOT*SE)
      PV(6) = CD2S*(YDOT*SE+ZDOT*CE)

*  Wrap up.
      JSTAT = 0
 999  CONTINUE

      END
