/*
 * This file was machine generated from ../include/atTrans.h ../include/atSlalib.h ../include/atHgMath.h 
 * on Tue Sep 27 18:40:56 2005
 *
 * Don't even think about editing it by hand
 */
#include <stdio.h>
#include <string.h>
#include "dervish_msg_c.h"
#include "atTrans.h"
#include "atSlalib.h"
#include "atHgMath.h"
#include "shCSchema.h"
#include "shCUtils.h"
#include "shCDiskio.h"
#include "prvt/utils_p.h"

#define NSIZE 100		/* size to read strings*/

#define _STRING(S) #S
#define STRING(S) _STRING(S)		/* convert a cpp value to a string*/

static SCHEMA_ELEM schema_elems0[] = { /* TRANS */
   { "id", "int", 0, 0, 0, NULL, 0, offsetof(TRANS,id) },
   { "a", "double", 0, 0, 0, NULL, 0, offsetof(TRANS,a) },
   { "b", "double", 0, 0, 0, NULL, 0, offsetof(TRANS,b) },
   { "c", "double", 0, 0, 0, NULL, 0, offsetof(TRANS,c) },
   { "d", "double", 0, 0, 0, NULL, 0, offsetof(TRANS,d) },
   { "e", "double", 0, 0, 0, NULL, 0, offsetof(TRANS,e) },
   { "f", "double", 0, 0, 0, NULL, 0, offsetof(TRANS,f) },
   { "dRow0", "double", 0, 0, 0, NULL, 0, offsetof(TRANS,dRow0) },
   { "dRow1", "double", 0, 0, 0, NULL, 0, offsetof(TRANS,dRow1) },
   { "dRow2", "double", 0, 0, 0, NULL, 0, offsetof(TRANS,dRow2) },
   { "dRow3", "double", 0, 0, 0, NULL, 0, offsetof(TRANS,dRow3) },
   { "dCol0", "double", 0, 0, 0, NULL, 0, offsetof(TRANS,dCol0) },
   { "dCol1", "double", 0, 0, 0, NULL, 0, offsetof(TRANS,dCol1) },
   { "dCol2", "double", 0, 0, 0, NULL, 0, offsetof(TRANS,dCol2) },
   { "dCol3", "double", 0, 0, 0, NULL, 0, offsetof(TRANS,dCol3) },
   { "csRow", "double", 0, 0, 0, NULL, 0, offsetof(TRANS,csRow) },
   { "csCol", "double", 0, 0, 0, NULL, 0, offsetof(TRANS,csCol) },
   { "ccRow", "double", 0, 0, 0, NULL, 0, offsetof(TRANS,ccRow) },
   { "ccCol", "double", 0, 0, 0, NULL, 0, offsetof(TRANS,ccCol) },
   { "riCut", "double", 0, 0, 0, NULL, 0, offsetof(TRANS,riCut) },
   { "mjd", "double", 0, 0, 0, NULL, 0, offsetof(TRANS,mjd) },
   { "airmass", "double", 0, 0, 0, NULL, 0, offsetof(TRANS,airmass) },
   { "muErr", "double", 0, 0, 0, NULL, 0, offsetof(TRANS,muErr) },
   { "nuErr", "double", 0, 0, 0, NULL, 0, offsetof(TRANS,nuErr) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems1[] = { /* TSTAMP */
   { "year", "int", 0, 0, 0, NULL, 0, offsetof(TSTAMP,year) },
   { "month", "int", 0, 0, 0, NULL, 0, offsetof(TSTAMP,month) },
   { "day", "int", 0, 0, 0, NULL, 0, offsetof(TSTAMP,day) },
   { "hour", "int", 0, 0, 0, NULL, 0, offsetof(TSTAMP,hour) },
   { "minute", "int", 0, 0, 0, NULL, 0, offsetof(TSTAMP,minute) },
   { "second", "double", 0, 0, 0, NULL, 0, offsetof(TSTAMP,second) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems2[] = { /* HG2 */
   { "reg", "REGION", 0, 1, 0, NULL, 0, offsetof(HG2,reg) },
   { "xmin", "double", 0, 0, 0, NULL, 0, offsetof(HG2,xmin) },
   { "xmax", "double", 0, 0, 0, NULL, 0, offsetof(HG2,xmax) },
   { "delx", "double", 0, 0, 0, NULL, 0, offsetof(HG2,delx) },
   { "ymin", "double", 0, 0, 0, NULL, 0, offsetof(HG2,ymin) },
   { "ymax", "double", 0, 0, 0, NULL, 0, offsetof(HG2,ymax) },
   { "dely", "double", 0, 0, 0, NULL, 0, offsetof(HG2,dely) },
   { "nx", "int", 0, 0, 0, NULL, 0, offsetof(HG2,nx) },
   { "ny", "int", 0, 0, 0, NULL, 0, offsetof(HG2,ny) },
   { "xlab", "char", 0, 0, 0, STRING(100), 0, offsetof(HG2,xlab) },
   { "ylab", "char", 0, 0, 0, STRING(100), 0, offsetof(HG2,ylab) },
   { "name", "char", 0, 0, 0, STRING(100), 0, offsetof(HG2,name) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems3[] = { /* TYPE */
   { "", "TYPE", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems4[] = { /* CHAR */
   { "", "char", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems5[] = { /* UCHAR */
   { "", "unsigned char", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems6[] = { /* INT */
   { "", "int", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems7[] = { /* UINT */
   { "", "unsigned int", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems8[] = { /* SHORT */
   { "", "short", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems9[] = { /* USHORT */
   { "", "unsigned short", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems10[] = { /* LONG */
   { "", "long", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems11[] = { /* ULONG */
   { "", "unsigned long", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems12[] = { /* FLOAT */
   { "", "float", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems13[] = { /* DOUBLE */
   { "", "double", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems14[] = { /* LOGICAL */
   { "", "logical", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems15[] = { /* STR */
   { "", "char", 0, 1, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems16[] = { /* PTR */
   { "", "ptr", 0, 1, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems17[] = { /* FUNCTION */
   { "", "function", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems18[] = { /* SCHAR */
   { "", "signed char", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA schema[] = {
   { "TRANS", STRUCT, sizeof(TRANS), 24, schema_elems0,
     (void *(*)(void))atTransNew,
     (void *(*)(void *))atTransDel,
     (RET_CODE(*)(FILE *,void *))shDumpGenericRead,
     (RET_CODE(*)(FILE *,void *))shDumpGenericWrite,
     0,
   },
   { "TSTAMP", STRUCT, sizeof(TSTAMP), 6, schema_elems1,
     (void *(*)(void))atTstampNew,
     (void *(*)(void *))atTstampDel,
     (RET_CODE(*)(FILE *,void *))shDumpGenericRead,
     (RET_CODE(*)(FILE *,void *))shDumpGenericWrite,
     0,
   },
   { "HG2", STRUCT, sizeof(HG2), 12, schema_elems2,
     (void *(*)(void))atHg2New,
     (void *(*)(void *))atHg2Del,
     (RET_CODE(*)(FILE *,void *))shDumpGenericRead,
     (RET_CODE(*)(FILE *,void *))shDumpGenericWrite,
     0,
   },
   { "TYPE", PRIM, sizeof(TYPE), 1, schema_elems3,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpTypeRead,
     (RET_CODE(*)(FILE *,void *))shDumpTypeWrite,
     3,
   },
   { "CHAR", PRIM, sizeof(char), 1, schema_elems4,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpCharRead,
     (RET_CODE(*)(FILE *,void *))shDumpCharWrite,
     3,
   },
   { "UCHAR", PRIM, sizeof(unsigned char), 1, schema_elems5,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpUcharRead,
     (RET_CODE(*)(FILE *,void *))shDumpUcharWrite,
     3,
   },
   { "INT", PRIM, sizeof(int), 1, schema_elems6,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpIntRead,
     (RET_CODE(*)(FILE *,void *))shDumpIntWrite,
     3,
   },
   { "UINT", PRIM, sizeof(unsigned int), 1, schema_elems7,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpUintRead,
     (RET_CODE(*)(FILE *,void *))shDumpUintWrite,
     3,
   },
   { "SHORT", PRIM, sizeof(short), 1, schema_elems8,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpShortRead,
     (RET_CODE(*)(FILE *,void *))shDumpShortWrite,
     3,
   },
   { "USHORT", PRIM, sizeof(unsigned short), 1, schema_elems9,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpUshortRead,
     (RET_CODE(*)(FILE *,void *))shDumpUshortWrite,
     3,
   },
   { "LONG", PRIM, sizeof(long), 1, schema_elems10,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpLongRead,
     (RET_CODE(*)(FILE *,void *))shDumpLongWrite,
     3,
   },
   { "ULONG", PRIM, sizeof(unsigned long), 1, schema_elems11,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpUlongRead,
     (RET_CODE(*)(FILE *,void *))shDumpUlongWrite,
     3,
   },
   { "FLOAT", PRIM, sizeof(float), 1, schema_elems12,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpFloatRead,
     (RET_CODE(*)(FILE *,void *))shDumpFloatWrite,
     3,
   },
   { "DOUBLE", PRIM, sizeof(double), 1, schema_elems13,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpDoubleRead,
     (RET_CODE(*)(FILE *,void *))shDumpDoubleWrite,
     3,
   },
   { "LOGICAL", PRIM, sizeof(unsigned char), 1, schema_elems14,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpLogicalRead,
     (RET_CODE(*)(FILE *,void *))shDumpLogicalWrite,
     3,
   },
   { "STR", PRIM, sizeof(char *), 1, schema_elems15,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpStrRead,
     (RET_CODE(*)(FILE *,void *))shDumpStrWrite,
     3,
   },
   { "PTR", PRIM, sizeof(void *), 1, schema_elems16,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpPtrRead,
     (RET_CODE(*)(FILE *,void *))shDumpPtrWrite,
     3,
   },
   { "FUNCTION", PRIM, 0, 1, schema_elems17,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     3,
   },
   { "SCHAR", PRIM, sizeof(signed char), 1, schema_elems18,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpScharRead,
     (RET_CODE(*)(FILE *,void *))shDumpScharWrite,
     3,
   },
   { "", UNKNOWN, 0, 0,  NULL, NULL, NULL, NULL, NULL, SCHEMA_LOCK},
};

RET_CODE
atSchemaLoadFromAstrotools(void)
{
   return(p_shSchemaLoad(schema));
}

