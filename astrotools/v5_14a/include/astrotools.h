#ifndef ASTROTOOLS_H
#define ASTROTOOLS_H

/* Include this one file to get everything you need to use ASTROTOOLS */
/* #include "atHgMath.h" */
#include "atLinearFits.h"
/* #include "atMatch.h" */
#include "atSlalib.h"
#include "atSlalib_c.h"
#include "atTclVerbs.h"
#include "atTrans.h"
#include "atSurveyGeometry.h"
#include "atConversions.h"
#include "atAirmass.h"
#include "atGalaxy.h"
#include "atObject.h"
#include "atGauss.h"
#include "atImSim.h"
#include "atRegSci.h"
#include "atCali.h"
#include "atEphemeris.h"
#include "atPMatch.h"
#include "atCentroid.h"
#include "atGaussianWidth.h"
#include "atQuasar.h"
#include "atAperture.h"
#endif
