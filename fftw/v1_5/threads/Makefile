FFTWDIR = ../src
FFTW_INCLUDE = -I$(FFTWDIR)
FFTW_LIB = $(FFTWDIR)/libfftw.a

LIBFFTW_THREADS = libfftw_threads.a

# Set where you want to install the library for "make install"
prefix = /usr/local
LIBDIR = $(prefix)/lib
INCLUDEDIR = $(prefix)/include

##### Set the thread library that is used #####

# POSIX threads:
FFTW_THREADS = FFTW_USING_POSIX_THREADS  # the default
FFTW_THREADLIB = -lpthread

# Solaris threads:
# FFTW_THREADS = FFTW_USING_SOLARIS_THREADS
# FFTW_THREADLIB = -lthread

# BeOS threads: (tested for DR8.2)
# FFTW_THREADS = FFTW_USING_BEOS_THREADS
# FFTW_THREADLIB =   # ...add any libraries here that need to be linked in

##### Defining the compilers used, etc.   #####

# On systems that have ranlib:
RANLIB = ranlib

# On systems that don't have ranlib
# RANLIB = echo ranlib

RM = rm

# Generic gcc
CC = gcc
CFLAGS = -pedantic -ansi -O6 -fomit-frame-pointer -Wall $(INCLUDE) \
         -D$(FFTW_THREADS) -D_POSIX_C_SOURCE

# Solaris:
# CC = cc
# CFLAGS = -DSOLARIS -native -fast -xO5 -dalign $(INCLUDE) -D$(FFTW_THREADS)

# IBM RS/6000:
# CC=cc
# CFLAGS = -O3 -qarch=pwrx -qtune=pwrx $(INCLUDE) -D$(FFTW_THREADS)

# SGI Onyx
# CC=cc
# CFLAGS=-64 -r10000 -OPT:fold_arith_limit=10000 -O3 $(INCLUDE) -D$(FFTW_THREADS)


###################### Building fftw_threads #######################

INCLUDE = $(FFTW_INCLUDE) -I.
HEADERS = $(FFTWDIR)/fftw.h fftw_threads.h
LIBS = $(FFTW_LIB) $(FFTW_THREADLIB) -lm

LIBFFTW_THREADS_OBJ = executor_threads.o fftwnd_threads.o fftw_threads.o

all:	$(LIBFFTW_THREADS) tests

tests: test_threads time_threads

clean:	
	$(RM) -f *.o core a.out *~ *.s *.bak

realclean: clean
	$(RM) -f $(LIBFFTW_THREADS) test_threads time_threads

%.o:	%.c $(HEADERS)
	$(CC) $(CFLAGS) -c $< -o $@

$(LIBFFTW_THREADS): $(LIBFFTW_THREADS_OBJ)
	$(RM) -f $(LIBFFTW_THREADS)
	$(AR) rv $(LIBFFTW_THREADS) $(LIBFFTW_THREADS_OBJ)
	$(RANLIB) $(LIBFFTW_THREADS)

install: $(LIBFFTW_THREADS)
	$(CP) $(LIBFFTW_THREADS) $(LIBDIR)
	$(RANLIB) $(LIBDIR)/$(LIBFFTW_THREADS)
	$(CP) fftw_threads.h $(INCLUDEDIR)

test_threads: test_threads.o $(LIBFFTW_THREADS)
	$(CC) $(LDFLAGS) test_threads.o \
              $(LIBS) $(LIBFFTW_THREADS) -o test_threads

time_threads: time_threads.o $(LIBFFTW_THREADS)
	$(CC) $(LDFLAGS) time_threads.o \
              $(LIBS) $(LIBFFTW_THREADS) -o time_threads

