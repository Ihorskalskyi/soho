/*
 * Copyright (c) 1997,1998 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * executor_threads.c -- execute the fft in parallel using threads
 */

#include <stdio.h>
#include <stdlib.h>

#include <fftw-int.h>
#include <fftw_threads.h>

static void executor_simple_threads(int n, const FFTW_COMPLEX *in,
				    FFTW_COMPLEX *out,
				    fftw_plan_node *p,
				    int istride,
				    int ostride,
				    int nthreads);

typedef struct {
  int min, max;
  int m,r;
  const FFTW_COMPLEX *in;
  FFTW_COMPLEX *out;
  fftw_plan_node *p;
  int istride, ostride;
  int nthreads;
} executor_simple_data;

static void *executor_simple_thread(executor_simple_data *d)
{
     int min, max, m, r;
     const FFTW_COMPLEX *in;
     FFTW_COMPLEX *out;
     fftw_plan_node *p;
     int istride, ostride;
     int nthreads;
     
     min = d->min; max = d->max;
     m = d->m; r = d->r;
     in = d->in; out = d->out;
     p = d->p;
     istride = d->istride;
     ostride = d->ostride;
     nthreads = d->nthreads;
     
     for (; min < max; ++min) {
	  executor_simple_threads(m, in + min * istride,
				  out + min * (m * ostride),
				  p,
				  istride * r, ostride,
				  nthreads);
     }

     return 0;
}

typedef struct {
     int min, max;
     twiddle_codelet *codelet;
     int m,r, ostride;
     FFTW_COMPLEX *out, *W;
} twiddle_thread_data;

static void *twiddle_thread(twiddle_thread_data *d)
{
     (d->codelet)(d->out + d->ostride * d->min,
		  d->W + (d->r - 1) * d->min,
		  d->m * d->ostride,
		  d->max - d->min,
		  d->ostride);
     return 0;
}

static void executor_simple_threads(int n, const FFTW_COMPLEX *in,
				    FFTW_COMPLEX *out,
				    fftw_plan_node *p,
				    int istride,
				    int ostride,
				    int nthreads)
{
     switch (p->type) {
	 case FFTW_NOTW:
	      (p->nodeu.notw.codelet) (in, out, istride, ostride);
	      break;

	 case FFTW_TWIDDLE:
	      {
		   int r = p->nodeu.twiddle.size;
		   int m = n / r;
		   int i;

		   if (nthreads <= 1) {
			twiddle_codelet *codelet;
			FFTW_COMPLEX *W;

			for (i = 0; i < r; ++i) {
			     fftw_executor_simple(m, in + i * istride,
						  out + i * (m * ostride),
						  p->nodeu.twiddle.recurse,
						  istride * r, ostride);
			}
			codelet = p->nodeu.twiddle.codelet;
			W = p->nodeu.twiddle.tw->twarray;
			codelet(out, W, m * ostride, m, ostride);
		   }
		   else {
			{
			     executor_simple_data *d;
			     int subthread_nthreads, new_nthreads;
			     
			     if (nthreads > r) {
				  subthread_nthreads = nthreads / r;
				  new_nthreads = r;
			     }
			     else {
				  subthread_nthreads = 1;
				  new_nthreads = nthreads;
			     }

			     d = alloca(new_nthreads * 
					sizeof(executor_simple_data));
			     
			     d->m = m; d->r = r;
			     d->in = in; d->out = out;
			     d->p = p->nodeu.twiddle.recurse;
			     d->istride = istride;
			     d->ostride = ostride;
			     d->nthreads = subthread_nthreads;
			     for (i = 1; i < new_nthreads; ++i)
				  d[i] = *d;
			     
			     fftw_thread_spawn_loop(r,new_nthreads,
						    executor_simple_thread,d);
			}
			{
			     twiddle_thread_data *d;

			     if (nthreads > m)
				  nthreads = m;

			     d = alloca(nthreads * 
					sizeof(twiddle_thread_data));
			     d->codelet = p->nodeu.twiddle.codelet;
			     d->m = m;
			     d->r = r;
			     d->ostride = ostride;
			     d->out = out;
			     d->W = p->nodeu.twiddle.tw->twarray;
			     for (i = 1; i < nthreads; ++i)
				  d[i] = *d;

			     fftw_thread_spawn_loop(m,nthreads,
						    twiddle_thread,d);
			}
		   }

		   break;
	      }

	 case FFTW_GENERIC:
	      {
		   int r = p->nodeu.generic.size;
		   int m = n / r;
		   int i;
		   generic_codelet *codelet;
		   FFTW_COMPLEX *W;

		   if (nthreads <= 1)
			for (i = 0; i < r; ++i) {
			     fftw_executor_simple(m, in + i * istride,
						  out + i * (m * ostride),
						  p->nodeu.generic.recurse,
						  istride * r, ostride);
			}
		   else {
			executor_simple_data *d;

			d = alloca(nthreads * sizeof(executor_simple_data));

			d->m = m; d->r = r;
			d->in = in; d->out = out;
			d->p = p->nodeu.generic.recurse;
			d->istride = istride;
			d->ostride = ostride;
			if (nthreads > r) {
			     d->nthreads = nthreads / r;
			     nthreads = r;
			}
			else
			     d->nthreads = 1;
			for (i = 1; i < nthreads; ++i)
			     d[i] = *d;

			fftw_thread_spawn_loop(r,nthreads,
					       executor_simple_thread,d);
		   }

		   codelet = p->nodeu.generic.codelet;
		   W = p->nodeu.generic.tw->twarray;
		   codelet(out, W, m, r, n, ostride);

		   break;
	      }

	 default:
	      fftw_die("BUG in executor: illegal plan\n");
	      break;
     }
}

static void executor_simple_inplace_threads(int n, FFTW_COMPLEX *in,
					    FFTW_COMPLEX *out,
					    fftw_plan_node *p,
					    int istride,
					    int nthreads)
{
     switch (p->type) {
	 case FFTW_NOTW:
	      (p->nodeu.notw.codelet) (in, in, istride, istride);
	      break;

	 default:
	      {
		   FFTW_COMPLEX *tmp;

		   tmp = (FFTW_COMPLEX *)
			fftw_malloc(n * sizeof(FFTW_COMPLEX));
		   
		   executor_simple_threads(n, in, tmp, p, istride, 1, 
					   nthreads);
		   fftw_strided_copy(n, tmp, istride, in);

		   fftw_free(tmp);
	      }
     }
}

typedef struct {
     int min, max;
     union {
	  notw_codelet *codelet;
	  struct {
	       int n;
	       fftw_plan_node *p;
	  } plan;
     } u;
     const FFTW_COMPLEX *in;
     FFTW_COMPLEX*out;
     int idist, odist, istride, ostride;
} executor_many_data;

static void *executor_many_codelet_thread(executor_many_data *d)
{
     int min, max;
     notw_codelet *codelet;
     const FFTW_COMPLEX *in;
     FFTW_COMPLEX *out;
     int idist, odist, istride, ostride;
     
     min = d->min; max = d->max;
     codelet = d->u.codelet;
     in = d->in;
     out = d->out;
     idist = d->idist;
     odist = d->odist;
     istride = d->istride;
     ostride = d->ostride;

     for (; min < max; ++min)
	  codelet(in + min * idist,
		  out + min * odist,
		  istride, ostride);

     return 0;
}

static void *executor_many_simple_thread(executor_many_data *d)
{
     int min, max;
     int n;
     fftw_plan_node *p;
     const FFTW_COMPLEX *in;
     FFTW_COMPLEX *out;
     int idist, odist, istride, ostride;
     
     min = d->min; max = d->max;
     n = d->u.plan.n;
     p = d->u.plan.p;
     in = d->in;
     out = d->out;
     idist = d->idist;
     odist = d->odist;
     istride = d->istride;
     ostride = d->ostride;

     for (; min < max; ++min)
	  fftw_executor_simple(n, in + min * idist,
			       out + min * odist,
			       p, istride, ostride);

     return 0;
}

static void executor_many_threads(int n, const FFTW_COMPLEX *in,
				  FFTW_COMPLEX *out,
				  fftw_plan_node *p,
				  int istride,
				  int ostride,
				  int howmany, int idist, int odist,
				  int nthreads)
{
     if (nthreads > howmany)
	  nthreads = howmany;     

     switch (p->type) {
	 case FFTW_NOTW:
	      {
		   int s;
		   
		   if (nthreads <= 1) {
			notw_codelet *codelet = p->nodeu.notw.codelet;
			for (s = 0; s < howmany; ++s)
			     codelet(in + s * idist,
				     out + s * odist,
				     istride, ostride);
		   }
		   else {
			executor_many_data *d;
			
			d = alloca(nthreads * sizeof(executor_many_data));
			
			d->in = in;
			d->out = out;
			d->u.codelet = p->nodeu.notw.codelet;
			d->istride = istride;
			d->ostride = ostride;
			d->idist = idist;
			d->odist = odist;
			for (s = 1; s < nthreads; ++s)
			     d[s] = *d;
			fftw_thread_spawn_loop(howmany,nthreads,
				   executor_many_codelet_thread,d);
		   }

		   break;
	      }

	 default:
	      {
		   int s;

		   if (nthreads <= 1)
			for (s = 0; s < howmany; ++s) {
			     fftw_executor_simple(n, in + s * idist,
						  out + s * odist,
						  p, istride, ostride);
			}
		   else {
			executor_many_data *d;
			
			d = alloca(nthreads * sizeof(executor_many_data));
			
			d->in = in; d->out = out;
			d->u.plan.n = n;
			d->u.plan.p = p;
			d->istride = istride;
			d->ostride = ostride;
			d->idist = idist;
			d->odist = odist;
			for (s = 1; s < nthreads; ++s)
			     d[s] = *d;
			fftw_thread_spawn_loop(howmany,nthreads,
				   executor_many_simple_thread,d);
		   }
	      }
     }
}

typedef struct {
     int min, max;
     union {
	  notw_codelet *codelet;
	  struct {
	       int n;
	       fftw_plan_node *p;
	       FFTW_COMPLEX *tmp;
	  } plan;
     } u;
     FFTW_COMPLEX *in;
     int idist, istride;
} executor_many_inplace_data;

static void *executor_many_inplace_codelet_thread(executor_many_inplace_data *d)
{
     int min, max;
     notw_codelet *codelet;
     FFTW_COMPLEX *in;
     int idist, istride;
     
     min = d->min; max = d->max;
     codelet = d->u.codelet;
     in = d->in;
     idist = d->idist;
     istride = d->istride;

     for (; min < max; ++min)
	  codelet(in + min * idist,
		  in + min * idist,
		  istride, istride);

     return 0;
}

static void *executor_many_inplace_simple_thread(executor_many_inplace_data *d)
{
     int min, max;
     int n;
     fftw_plan_node *p;
     FFTW_COMPLEX *in, *tmp;
     int idist, istride;
     
     min = d->min; max = d->max;
     n = d->u.plan.n;
     p = d->u.plan.p;
     tmp = d->u.plan.tmp;
     in = d->in;
     idist = d->idist;
     istride = d->istride;

     for (; min < max; ++min) {
	  fftw_executor_simple(n, in + min * idist,
			       tmp,
			       p, istride, 1);
	  fftw_strided_copy(n, tmp, istride, in + min * idist);
     }

     return 0;
}

static void executor_many_inplace_threads(int n, FFTW_COMPLEX *in,
					  FFTW_COMPLEX *out,
					  fftw_plan_node *p,
					  int istride,
					  int howmany, int idist,
					  int nthreads)
{
     if (nthreads > howmany)
	  nthreads = howmany;     

     switch (p->type) {
	 case FFTW_NOTW:
	      {
		   int s;

		   if (nthreads <= 1) {
			notw_codelet *codelet = p->nodeu.notw.codelet;
			for (s = 0; s < howmany; ++s)
			     codelet(in + s * idist,
				     in + s * idist,
				     istride, istride);
		   }
		   else {
			executor_many_inplace_data *d;
			
			d = alloca(nthreads * 
				   sizeof(executor_many_inplace_data));
			
			d->in = in;
			d->u.codelet = p->nodeu.notw.codelet;
			d->istride = istride;
			d->idist = idist;
			for (s = 1; s < nthreads; ++s)
			     d[s] = *d;
			fftw_thread_spawn_loop(howmany,nthreads,
				   executor_many_inplace_codelet_thread,d);
		   }
		   break;
	      }

	 default:
	      {
		   int s;
		   FFTW_COMPLEX *tmp;

		   if (nthreads <= 1) {
			tmp = (FFTW_COMPLEX *)
			     fftw_malloc(n * sizeof(FFTW_COMPLEX));

			for (s = 0; s < howmany; ++s) {
			     fftw_executor_simple(n,
						  in + s * idist,
						  tmp,
						  p, istride, 1);
			     fftw_strided_copy(n, tmp, istride, 
					       in + s * idist);
			}
		   }
		   else {
			executor_many_inplace_data *d;
			
			tmp = (FFTW_COMPLEX *)
			     fftw_malloc(nthreads * n * sizeof(FFTW_COMPLEX));

			d = alloca(nthreads * 
				   sizeof(executor_many_inplace_data));
			
			d->in = in;
			d->u.plan.n = n;
			d->u.plan.p = p;
			d->u.plan.tmp = tmp;
			d->istride = istride;
			d->idist = idist;
			for (s = 1; s < nthreads; ++s) {
			     d[s] = *d;
			     d[s].u.plan.tmp = tmp + n*s;
			}
			fftw_thread_spawn_loop(howmany,nthreads,
				   executor_many_inplace_simple_thread,d);
		   }

		   fftw_free(tmp);
	      }
     }
}

/* user interface */
void fftw_threads(int nthreads,
		  fftw_plan plan, int howmany, FFTW_COMPLEX *in, int istride,
		  int idist, FFTW_COMPLEX *out, int ostride, int odist)
{
     int n = plan->n;

     if (plan->flags & FFTW_IN_PLACE) {
	  if (howmany == 1) {
	       executor_simple_inplace_threads(n, in, out, plan->root, 
					       istride, nthreads);
	  } else {
	       executor_many_inplace_threads(n, in, out, plan->root, istride, 
					     howmany, idist, nthreads);
	  }
     } else {
	  if (howmany == 1) {
	       executor_simple_threads(n, in, out, plan->root, 
				       istride, ostride, nthreads);
	  } else {
	       executor_many_threads(n, in, out, plan->root, istride, ostride,
				     howmany, idist, odist, nthreads);
	  }
     }
}
