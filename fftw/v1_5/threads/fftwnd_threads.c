
/*
 * Copyright (c) 1997,1998 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <stdlib.h>

#include <fftw-int.h>
#include <fftw_threads.h>

static void fftwnd_threads_serial_aux(fftwnd_plan p, int cur_dim,
				      FFTW_COMPLEX *in, int istride,
				      FFTW_COMPLEX *out, int ostride,
				      FFTW_COMPLEX *work)
{
     int n_after = p->n_after[cur_dim], n = p->n[cur_dim];

     if (cur_dim == p->rank - 2) {
          /* just do the last dimension directly: */
          if (p->is_in_place)
               fftw(p->plans[p->rank - 1], n,
                    in, istride, n_after * istride,
                    work, 1, 0);
          else
               fftw(p->plans[p->rank - 1], n,
                    in,  istride, n_after * istride,
                    out, ostride, n_after * ostride);
     }
     else { /* we have at least two dimensions to go */
          int i;

          /* process the subsequent dimensions recursively, in hyperslabs,
             to get maximum locality: */
          for (i = 0; i < n; ++i)
               fftwnd_threads_serial_aux(p, cur_dim + 1,
					 in + i * n_after * istride, istride,
					 out + i * n_after * ostride, ostride,
					 work);
     }

     /* do the current dimension (in-place): */
     fftw(p->plans[cur_dim], n_after,
	  out, n_after * ostride, ostride,
	  work, 1, 0);
}

typedef struct {
     int min, max;
     fftwnd_plan plan;
     int cur_dim;
     int distance;
     FFTW_COMPLEX *in, *out;
     int istride, ostride;
     FFTW_COMPLEX *tmp;
} fftwnd_aux_many_data;

static void *fftwnd_aux_many_thread(fftwnd_aux_many_data *d)
{
     int min, max;
     int distance, cur_dim;
     fftwnd_plan plan;
     FFTW_COMPLEX *in, *out;
     int istride, ostride;
     FFTW_COMPLEX *work;

     min = d->min; max = d->max;
     distance = d->distance;
     cur_dim = d->cur_dim;
     plan = d->plan;
     in = d->in;
     out = d->out;
     istride = d->istride;
     ostride = d->ostride;
     work = d->tmp;

     for (; min < max; ++min)
	  fftwnd_threads_serial_aux(plan,cur_dim,
				    in + min*istride*distance,istride,
				    out + min*ostride*distance,ostride,
				    work);

     return 0;
}

static void fftwnd_aux_many_threads(int nthreads, int n, int n_after,
				    fftwnd_plan plan, int cur_dim,
				    FFTW_COMPLEX *in, int istride,
				    FFTW_COMPLEX *out, int ostride)
{
     int i;
     FFTW_COMPLEX *tmp;
     fftwnd_aux_many_data *d;
     int maxdim = plan->n[cur_dim];
     
     for (i = cur_dim + 1; i < plan->rank; ++i)
	  if (plan->n[i] > maxdim)
	       maxdim = plan->n[i];
     
     if (nthreads > n)
	  nthreads = n;
     
     tmp = (FFTW_COMPLEX *) fftw_malloc((nthreads - 1) * maxdim
					* sizeof(FFTW_COMPLEX));
     
     d = alloca(nthreads * sizeof(fftwnd_aux_many_data));
     d->plan = plan;
     d->cur_dim = cur_dim;
     d->distance = n_after;
     d->in = in;
     d->out = out;
     d->istride = istride;
     d->ostride = ostride;
     d->tmp = plan->work;
     for (i = 1; i < nthreads; ++i) {
	  d[i] = *d;
	  d[i].tmp = tmp + (i - 1)*maxdim;
     }
     
     fftw_thread_spawn_loop(n,nthreads,fftwnd_aux_many_thread,d);

     fftw_free(tmp);
}

static void fftwnd_threads_aux(int nthreads, fftwnd_plan p, int cur_dim, 
			       FFTW_COMPLEX *in, int istride,
			       FFTW_COMPLEX *out, int ostride)
{
     int n_after = p->n_after[cur_dim], n = p->n[cur_dim];

     if (cur_dim == p->rank - 2) {
	  /* just do the last dimension directly: */
	  if (p->is_in_place)
	       fftw_threads(nthreads, p->plans[p->rank - 1], n,
			    in, istride, n_after * istride,
			    (FFTW_COMPLEX*)NULL, 0, 0);
	  else
	       fftw_threads(nthreads, p->plans[p->rank - 1], n,
			    in,  istride, n_after * istride,
			    out, ostride, n_after * ostride);
     }
     else { /* we have at least two dimensions to go */
	  /* process the subsequent dimensions recursively, in hyperslabs,
	     to get maximum locality: */
	  fftwnd_aux_many_threads(nthreads, n, n_after,
				  p, cur_dim + 1,
				  in, istride, out, ostride);
     }

     /* do the current dimension (in-place): */
     fftw_threads(nthreads, p->plans[cur_dim], n_after,
		  out, n_after * ostride, ostride,
		  (FFTW_COMPLEX*)NULL, 0, 0);
}

void fftwnd_threads(int nthreads, fftwnd_plan p, int howmany,
		    FFTW_COMPLEX *in, int istride, int idist,
		    FFTW_COMPLEX *out, int ostride, int odist)
{
     switch (p->rank) {
	 case 0:
	      break;
	 case 1:
	      if (p->is_in_place)	/* fft is in-place */
		   fftw_threads(nthreads, p->plans[0], howmany,
				in, istride, idist,
				(FFTW_COMPLEX*) NULL, 0, 0);
	      else
		   fftw_threads(nthreads, p->plans[0], howmany,
				in, istride, idist,
				out, ostride, odist);
	      break;
	 default: /* rank >= 2 */
	 {
	      int i;
	      
	      if (p->is_in_place) {
		   out = in;
		   ostride = istride;
		   odist = idist;
	      }

	      if (nthreads <= 1)
		   for (i = 0; i < howmany; ++i)
			fftwnd_aux(p, 0,
				   in + i*idist, istride,
				   out + i*odist, ostride);
	      else
		   for (i = 0; i < howmany; ++i)
			fftwnd_threads_aux(nthreads, p, 0,
					   in + i*idist, istride,
					   out + i*odist, ostride);
	 }
     }
}
