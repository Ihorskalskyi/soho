/*
 * Copyright (c) 1997,1998 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <stdio.h>
#include <math.h>

#include <fftw_threads.h>
#include <fftw-int.h>

void zero_arr(FFTW_COMPLEX *a, int n)
{
     int i;
     for (i = 0; i < n; ++i)
          a[i].re = a[i].im = 0.0;
}

#define TIME_FFT(fft,t) \
{ \
     fftw_time ts,te; \
     double total_t; \
     int iters = 1,iter; \
     do { \
          ts = fftw_get_time(); \
          for (iter = 0; iter < iters; ++iter) fft; \
          te = fftw_get_time(); \
          t = (total_t = fftw_time_to_sec(fftw_time_diff(te,ts))) / iters; \
          iters *= 2; \
     } while (total_t < 2.0); \
}

double mflops(int N, double t)
{
     return (5.0 * N * log((double) N) / (t * 1.0e6));
}

#define MIN_RANK 1
#define MAX_RANK 5

typedef struct {
     int rank;
     int n[MAX_RANK];
} dims;

int prod_dims(dims d)
{
     int i, N = 1;

     for (i = 0; i < d.rank; ++i)
	  N *= d.n[i];
     return N;
}

char *dims_string(dims d)
{
     int i;
     static char s[200];
     
     if (d.rank > 0)
	  sprintf(s,"%d",d.n[0]);
     for (i = 1; i < d.rank; ++i)
	  sprintf(s + strlen(s),"x%d",d.n[i]);
     return s;
}

int main(int argc, char **argv)
{
     dims testdims[] = {
	  { 1, { 16 } },
	  { 1, { 32 } },
	  { 1, { 64 } },
	  { 1, { 128 } },
	  { 1, { 256 } },
	  { 1, { 512 } },
	  { 1, { 1000 } },
	  { 1, { 1024 } },
	  { 1, { 1960 } },
	  { 1, { 2048 } },
	  { 1, { 4096 } },
	  { 1, { 4725 } },
	  { 1, { 8192 } },
	  { 1, { 10368 } },
	  { 1, { 16384 } },
	  { 1, { 27000 } },
	  { 1, { 32768 } },
	  { 1, { 65536 } },
	  { 1, { 75600 } },
	  { 1, { 165375 } },
	  { 1, { 262144 } },

	  { 2, { 512, 512 } },
	  { 2, { 1024, 1024 } },
	  { 2, { 1500, 1500 } },

	  { 3, { 16, 16, 16 } },
	  { 3, { 32, 32, 32 } },
	  { 3, { 64, 64, 64 } },
	  { 3, { 80, 80, 80 } },
	  { 3, { 100, 100, 100 } },
	  { 3, { 128, 128, 128 } }
     };
     int ntests = sizeof(testdims) / sizeof(dims);
     int i, test;
     FFTW_COMPLEX *arr, *out;
     int max_size = 0, max_1d_size = 0;
     fftw_plan plan_1d = 0;
     fftwnd_plan plan_nd = 0;
     double t, t_threads;
     int rank = 0;
     dims command_line_dims;
     int nthreads = 0;
     
     if (argc > 1)
          nthreads = atoi(argv[1]);

     if (nthreads < 1) {
          printf("Usage: time_threads nthreads [rank {dims}]\n"
                 "  -- benchmarks fftw_threads and fftwnd_threads "
                 "against fftw and fftwnd\n"
                 "     using nthreads parallel threads of execution.\n");
          return 1;
     }

     fftw_threads_init();
     
     if (argc >= 3)
          command_line_dims.rank = rank = atoi(argv[2]);

     if (command_line_dims.rank >= MIN_RANK 
	 && argc >= 3 + command_line_dims.rank) {
          for (i = 0; i < command_line_dims.rank; ++i) {
               command_line_dims.n[i] = atoi(argv[3 + i]);
               if (command_line_dims.n[i] <= 0)
                    fftw_die("Illegal param: dimensions must be positive.\n");
          }
	  testdims[0] = command_line_dims;
	  ntests = 1;
     }

     for (i = 0; i < ntests; ++i) 
	  if (rank && rank == testdims[i].rank) {
	       int N = prod_dims(testdims[i]);
	       if (N > max_size)
		    max_size = N;
	       if (testdims[i].rank == 1 && testdims[i].n[0] > max_1d_size)
		    max_1d_size = testdims[i].n[0];
	  }
     
     arr = (FFTW_COMPLEX *) fftw_malloc(max_size * sizeof(FFTW_COMPLEX));
     out = (FFTW_COMPLEX *) fftw_malloc(max_1d_size * sizeof(FFTW_COMPLEX));

     if (!arr || !out) {
	  fprintf(stderr,
		  "Not enough memory!  At least %d bytes needed.\n",
		  (max_size + max_1d_size) * sizeof(FFTW_COMPLEX));
	  fftw_die("out of memory.\n");
     }
     
     zero_arr(arr,max_size);

     printf("%15s%15s%15s%15s\n", 
	    "array size", "fftw",
	    "fftw_threads", "speedup");
     
     for (test = 0; test < ntests; ++test) 
	  if (rank && rank == testdims[test].rank) {
	       printf("%15s",dims_string(testdims[test]));
	       fflush(stdout);
	       
	       if (testdims[test].rank == 1)
		    plan_1d = fftw_create_plan(testdims[test].n[0],
					       FFTW_FORWARD,
					       FFTW_MEASURE | FFTW_USE_WISDOM);
	       else
		    plan_nd = fftwnd_create_plan(testdims[test].rank,
						 testdims[test].n,
						 FFTW_FORWARD,
						 FFTW_MEASURE | FFTW_IN_PLACE
						 | FFTW_USE_WISDOM);
	       
	       if (testdims[test].rank == 1) {
		    TIME_FFT(fftw(plan_1d, 1, arr, 1, 0, out, 1, 0), t);
	       }
	       else {
		    TIME_FFT(fftwnd(plan_nd, 1, arr, 1, 0, arr, 1, 0), t);
	       }
	       
	       printf("%15g",mflops(prod_dims(testdims[test]),t));
	       fflush(stdout);
	       
	       if (testdims[test].rank == 1) {
		    TIME_FFT(fftw_threads(nthreads,
					  plan_1d, 1, arr, 1, 0, out, 1, 0),
			     t_threads);
	       }
	       else {
		    TIME_FFT(fftwnd_threads(nthreads,
					    plan_nd, 1, arr, 1, 0, arr, 1, 0),
			     t_threads);
	       }
	       
	       printf("%15g",mflops(prod_dims(testdims[test]),t_threads));
	       printf("%15g\n", t / t_threads);
	       fflush(stdout);
	       
	       if (testdims[test].rank == 1)
		    fftw_destroy_plan(plan_1d);
	       else
		    fftwnd_destroy_plan(plan_nd);
	  }
     
     fftw_free(arr);
     fftw_free(out);
     
     return 0;
}
