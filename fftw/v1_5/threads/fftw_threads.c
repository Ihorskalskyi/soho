/*
 * Copyright (c) 1997,1998 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <fftw-int.h>
#include <fftw_threads.h>

#ifdef FFTW_USING_POSIX_THREADS
pthread_attr_t fftw_pthread_attributes; /* global attrs for POSIX threads */
#endif

/* This routine does any initialization that is necessary to use
   threads.  It must be called before calling fftw_threads or
   fftwnd_threads. 
   
   Returns 0 if successful, and non-zero if there is an error.
   Do not call fftw_threads or fftw_threads if fftw_threads_init
   is not successful! */

int fftw_threads_init(void)
{
     #ifdef FFTW_USING_POSIX_THREADS
     int err;

     err = pthread_attr_init(&fftw_pthread_attributes); /* set to defaults */
     if (err) return err;
     
     /* set to use global resource competition */
     err = pthread_attr_setscope(&fftw_pthread_attributes,
				 PTHREAD_SCOPE_SYSTEM);
     if (err) return err;
     #endif

     #ifdef FFTW_USING_MACOS_THREADS
     /* Must use MPAllocate and MPFree instead of malloc and free: */
     if (MPLibraryIsLoaded()) {
	  fftw_malloc_hook = MPAllocate;
	  fftw_free_hook = MPFree;
     }
     #endif

     return 0; /* no error */
}
