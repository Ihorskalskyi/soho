/*
 * Copyright (c) 1997,1998 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#ifndef RFFTW_H
#define RFFTW_H

#include <fftw.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* RFFTW: real<->complex transform in one dimension */

typedef struct {
    fftw_plan plan;
    fftw_twiddle *tw;
    int which_case;
} rfftw_plan_struct;

typedef rfftw_plan_struct *rfftw_plan;

typedef enum {
    REAL_TO_COMPLEX,
    COMPLEX_TO_REAL
} rfftw_type;

rfftw_plan rfftw_create_plan(int n, fftw_direction dir, int flags, rfftw_type type);

void rfftw_destroy_plan(rfftw_plan plan);

void rfftw(rfftw_plan plan, int howmany,
	   FFTW_COMPLEX * in, int istride, int idist,
	   FFTW_COMPLEX * out, int ostride, int odist);

/* RFFTWND: real<->complex transform in multiple dimensions */

typedef struct {
    rfftw_type type;
    rfftw_plan rplan;
    fftwnd_plan plan_nd;
    int real_n;
} rfftwnd_plan_struct;

typedef rfftwnd_plan_struct *rfftwnd_plan;

/* Initializing the RFFTWND Auxiliary Data */
rfftwnd_plan rfftw2d_create_plan(int nx, int ny, fftw_direction dir, int flags, rfftw_type type);
rfftwnd_plan rfftw3d_create_plan(int nx, int ny, int nz,
			 fftw_direction dir, int flags, rfftw_type type);
rfftwnd_plan rfftwnd_create_plan(int rank, int *n, fftw_direction dir,
				 int flags, rfftw_type type);

/* Freeing the RFFTWND Auxiliary Data */
void rfftwnd_destroy_plan(rfftwnd_plan plan);

/* Computing the real-complex N-Dimensional FFT */
void rfftwnd(rfftwnd_plan plan, int howmany,
	     FFTW_COMPLEX * in, int istride, int idist,
	     FFTW_COMPLEX * out, int ostride, int odist);

#ifdef __cplusplus
} /* extern "C" */
#endif /* __cplusplus */

#endif /* RFFTW_H */
