/*
 * Copyright (c) 1997,1998 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include "rfftw.h"

#define SMALL_TESTS 0

void zero_arr(FFTW_COMPLEX *a, int n)
{
     int i;
     for (i = 0; i < n; ++i)
          a[i].re = a[i].im = 0.0;
}

#define TIME_FFT(fft,t) \
{ \
     clock_t ts,te; \
     double total_t; \
     int iters = 1,iter; \
     do { \
          ts = clock(); \
          for (iter = 0; iter < iters; ++iter) fft; \
          te = clock(); \
          t = (total_t = ((te - ts) * 1.0 / CLOCKS_PER_SEC)) / iters; \
          iters *= 2; \
     } while (total_t < 2.0); \
}

int main(void)
{
    int n[] =
    {
#if SMALL_TESTS
	2,
	4,
	8,
	16,
	32,
	64,
	128,
#endif
	256,
	512,
	1024,
	2048,
	4096,
	8192,
	16384,
	32768,
	65536,
	131072
    };
    int ntests = sizeof(n) / sizeof(int);
    int i, test;
    FFTW_COMPLEX *cin, *out;
    FFTW_REAL *rin;
    int max_size = 0;
    fftw_plan cplan;
    rfftw_plan rplan;
    double ctime, rtime;

    for (i = 0; i < ntests; ++i)
	if (n[i] > max_size)
	    max_size = n[i];

    cin = (FFTW_COMPLEX *)fftw_malloc(max_size * sizeof(FFTW_COMPLEX));
    out = (FFTW_COMPLEX *)fftw_malloc(max_size * sizeof(FFTW_COMPLEX));
    rin = (FFTW_REAL *) cin;

    zero_arr(cin,max_size);

    if (!cin || !out) {
	printf("Not enough memory!  At least %d bytes needed.\n",
	       max_size * sizeof(FFTW_COMPLEX) * 2);
	exit(1);
    }
    printf("%10s%20s\n", "N", "RFFTW Speedup");

    for (test = 0; test < ntests; ++test) {
	printf("%10d", n[test]);
	fflush(stdout);

	cplan = fftw_create_plan(n[test], FFTW_FORWARD,
				 FFTW_MEASURE | FFTW_USE_WISDOM);
	rplan = rfftw_create_plan(n[test], FFTW_FORWARD,
				  FFTW_MEASURE | FFTW_USE_WISDOM, 
				  REAL_TO_COMPLEX);

	TIME_FFT(fftw(cplan, 1, cin, 1, 0, out, 1, 0), ctime);

	TIME_FFT(rfftw(rplan, 1, cin, 1, 0, out, 1, 0), rtime);

	printf("%20g\n", ctime / rtime);
	fflush(stdout);

	fftw_destroy_plan(cplan);
	rfftw_destroy_plan(rplan);
    }

    fftw_free(cin);
    fftw_free(out);

    return 0;
}


