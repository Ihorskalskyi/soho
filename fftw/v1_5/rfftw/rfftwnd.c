/*
 * Copyright (c) 1997,1998 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <stdlib.h>

#include "rfftw.h"

/* Prototypes for functions used internally in this file: */

static void rfftw2d_out_of_place_rc_aux(rfftwnd_plan p, int howmany,
				    FFTW_COMPLEX * in, int istride, int idist,
				  FFTW_COMPLEX * out, int ostride, int odist);
static void rfftw3d_out_of_place_rc_aux(rfftwnd_plan p, int howmany,
				    FFTW_COMPLEX * in, int istride, int idist,
				  FFTW_COMPLEX * out, int ostride, int odist);
static void rfftwnd_out_of_place_rc_aux(rfftwnd_plan p, int howmany,
				    FFTW_COMPLEX * in, int istride, int idist,
				  FFTW_COMPLEX * out, int ostride, int odist);

static void rfftw2d_in_place_rc_aux(rfftwnd_plan p, int howmany,
			       FFTW_COMPLEX * in_out, int istride, int idist);
static void rfftw3d_in_place_rc_aux(rfftwnd_plan p, int howmany,
			       FFTW_COMPLEX * in_out, int istride, int idist);
static void rfftwnd_in_place_rc_aux(rfftwnd_plan p, int howmany,
			       FFTW_COMPLEX * in_out, int istride, int idist);

static void rfftw2d_out_of_place_cr_aux(rfftwnd_plan p, int howmany,
				    FFTW_COMPLEX * in, int istride, int idist,
				  FFTW_COMPLEX * out, int ostride, int odist);
static void rfftw3d_out_of_place_cr_aux(rfftwnd_plan p, int howmany,
				    FFTW_COMPLEX * in, int istride, int idist,
				  FFTW_COMPLEX * out, int ostride, int odist);
static void rfftwnd_out_of_place_cr_aux(rfftwnd_plan p, int howmany,
				    FFTW_COMPLEX * in, int istride, int idist,
				  FFTW_COMPLEX * out, int ostride, int odist);

static void rfftw2d_in_place_cr_aux(rfftwnd_plan p, int howmany,
			       FFTW_COMPLEX * in_out, int istride, int idist);
static void rfftw3d_in_place_cr_aux(rfftwnd_plan p, int howmany,
			       FFTW_COMPLEX * in_out, int istride, int idist);
static void rfftwnd_in_place_cr_aux(rfftwnd_plan p, int howmany,
			       FFTW_COMPLEX * in_out, int istride, int idist);

/*********** Initializing the FFTWND Auxiliary Data **********/

rfftwnd_plan rfftw2d_create_plan(int nx, int ny, fftw_direction dir,
				 int flags, rfftw_type type)
{
    int n[2];

    n[0] = nx;
    n[1] = ny;

    return rfftwnd_create_plan(2, n, dir, flags, type);
}

rfftwnd_plan rfftw3d_create_plan(int nx, int ny, int nz, fftw_direction dir,
				 int flags, rfftw_type type)
{
    int n[3];

    n[0] = nx;
    n[1] = ny;
    n[2] = nz;

    return rfftwnd_create_plan(3, n, dir, flags, type);
}

rfftwnd_plan rfftwnd_create_plan(int rank, int *n, fftw_direction dir,
				 int flags, rfftw_type type)
{
    rfftwnd_plan p;
    int nreal = 0, j;

    if (rank < 0 || (rank > 0 && (n[rank - 1] & 1)))
        /* last dimension must be even */
	return 0;

    p = (rfftwnd_plan) fftw_malloc(sizeof(rfftwnd_plan_struct));
    if (!p)
	return 0;

    p->plan_nd = 0;
    p->rplan = 0;

    p->type = type;

    if (rank > 0) {
	nreal = n[rank - 1];	/* save size of last dimension (real array) */
	/* compute effective size of input & output arrays */
	n[rank - 1] = nreal / 2 + 1;	/* output is padded by extra complex element */
	p->real_n = nreal / 2;
    }
    p->plan_nd = fftwnd_create_plan(rank, n, dir, flags);
    if (rank > 0)
	n[rank - 1] = nreal;	/* restore to previous value */
    if (!p->plan_nd) {
	rfftwnd_destroy_plan(p);
	return 0;
    }
    if (rank == 0)
	return p;

    /* it is a little bit of a waste to have created this plan
       for the last rank, only to destroy it, but oh well.  We
       don't care too much about setup time anyway. */
    for (j = rank - 2; j >= 0 && p->plan_nd->plans[rank - 1] != p->plan_nd->plans[j]; --j);
    if (j < 0)			/* only destroy the plan if it isn't being used elsewhere */
	fftw_destroy_plan(p->plan_nd->plans[rank - 1]);
    p->plan_nd->plans[rank - 1] = 0;

    p->rplan = rfftw_create_plan(nreal, dir, flags, type);
    if (!p->rplan) {
	rfftwnd_destroy_plan(p);
	return 0;
    }
    return p;
}

/************* Freeing the FFTWND Auxiliary Data *************/

void rfftwnd_destroy_plan(rfftwnd_plan plan)
{
    if (plan) {
	if (plan->rplan)
	    rfftw_destroy_plan(plan->rplan);
	if (plan->plan_nd)
	    fftwnd_destroy_plan(plan->plan_nd);
	fftw_free(plan);
    }
}

/************** Computing the N-Dimensional FFT **************/

void rfftwnd(rfftwnd_plan plan, int howmany,
	     FFTW_COMPLEX * in, int istride, int idist,
	     FFTW_COMPLEX * out, int ostride, int odist)
{
    if (plan->type == REAL_TO_COMPLEX) {
	if (plan->plan_nd->is_in_place)		/* fft is in-place */
	    switch (plan->plan_nd->rank) {
	    case 0:
		break;
	    case 1:
		rfftw(plan->rplan, howmany, in, istride, idist,
		      plan->plan_nd->work, 1, 0);
		break;
	    case 2:
		rfftw2d_in_place_rc_aux(plan, howmany, in, istride, idist);
		break;
	    case 3:
		rfftw3d_in_place_rc_aux(plan, howmany, in, istride, idist);
		break;
	    default:
		rfftwnd_in_place_rc_aux(plan, howmany, in, istride, idist);
	} else {
	    if (in == out || out == 0)
		fftw_die("Illegal attempt to perform in-place FFT!\n");
	    switch (plan->plan_nd->rank) {
	    case 0:
		break;
	    case 1:
		rfftw(plan->rplan, howmany, in, istride, idist,
		      out, ostride, odist);
		break;
	    case 2:
		rfftw2d_out_of_place_rc_aux(plan, howmany, in, istride,
					    idist, out, ostride, odist);
		break;
	    case 3:
		rfftw3d_out_of_place_rc_aux(plan, howmany, in, istride,
					    idist, out, ostride, odist);
		break;
	    default:
		rfftwnd_out_of_place_rc_aux(plan, howmany, in, istride,
					    idist, out, ostride, odist);
	    }
	}
    } else {			/* COMPLEX_TO_REAL */
	if (plan->plan_nd->is_in_place)		/* fft is in-place */
	    switch (plan->plan_nd->rank) {
	    case 0:
		break;
	    case 1:
		rfftw(plan->rplan, howmany, in, istride, idist,
		      plan->plan_nd->work, 1, 0);
		break;
	    case 2:
		rfftw2d_in_place_cr_aux(plan, howmany, in, istride, idist);
		break;
	    case 3:
		rfftw3d_in_place_cr_aux(plan, howmany, in, istride, idist);
		break;
	    default:
		rfftwnd_in_place_cr_aux(plan, howmany, in, istride, idist);
	} else {
	    if (in == out || out == 0)
		fftw_die("Illegal attempt to perform in-place FFT!\n");
	    switch (plan->plan_nd->rank) {
	    case 0:
		break;
	    case 1:
		rfftw(plan->rplan, howmany, in, istride, idist,
		      out, ostride, odist);
		break;
	    case 2:
		rfftw2d_out_of_place_cr_aux(plan, howmany, in, istride,
					    idist, out, ostride, odist);
		break;
	    case 3:
		rfftw3d_out_of_place_cr_aux(plan, howmany, in, istride,
					    idist, out, ostride, odist);
		break;
	    default:
		rfftwnd_out_of_place_cr_aux(plan, howmany, in, istride,
					    idist, out, ostride, odist);
	    }
	}
    }
}

static void rfftw2d_out_of_place_rc_aux(rfftwnd_plan p, int howmany,
				    FFTW_COMPLEX * in, int istride, int idist,
				   FFTW_COMPLEX * out, int ostride, int odist)
{
    int fft_iter;

    for (fft_iter = 0; fft_iter < howmany; ++fft_iter) {
	/* FFT y dimension (out-of-place): */
	rfftw(p->rplan, p->plan_nd->n[0],
	      in + fft_iter * idist, istride, p->real_n * istride,
	    out + fft_iter * odist, ostride, p->plan_nd->n[1] * ostride);
	/* FFT x dimension (in-place): */
	fftw(p->plan_nd->plans[0], p->plan_nd->n[1],
	     out + fft_iter * odist, p->plan_nd->n[1] * ostride, ostride,
	     p->plan_nd->work, 1, 1);
    }
}

static void rfftw3d_out_of_place_rc_aux(rfftwnd_plan p, int howmany,
				    FFTW_COMPLEX * in, int istride, int idist,
				   FFTW_COMPLEX * out, int ostride, int odist)
{
    int fft_iter;
    int i;

    for (fft_iter = 0; fft_iter < howmany; ++fft_iter) {
	/* FFT z dimension (out-of-place): */
	rfftw(p->rplan, p->plan_nd->n[0] * p->plan_nd->n[1],
	      in + fft_iter * idist, istride, p->real_n * istride,
	    out + fft_iter * odist, ostride, p->plan_nd->n[2] * ostride);
	/* FFT y dimension (in-place): */
	for (i = 0; i < p->plan_nd->n[0]; ++i)
	    fftw(p->plan_nd->plans[1], p->plan_nd->n[2],
		 out + fft_iter * odist + i * ostride * p->plan_nd->n[1] * p->plan_nd->n[2],
	    p->plan_nd->n[2] * ostride, ostride, p->plan_nd->work, 1, 0);
	/* FFT x dimension (in-place): */
	fftw(p->plan_nd->plans[0], p->plan_nd->n[1] * p->plan_nd->n[2],
	     out + fft_iter * odist, ostride * p->plan_nd->n[1] * p->plan_nd->n[2], ostride,
	     p->plan_nd->work, 1, 0);
    }
}

static void rfftwnd_out_of_place_rc_aux(rfftwnd_plan p, int howmany,
				    FFTW_COMPLEX * in, int istride, int idist,
				   FFTW_COMPLEX * out, int ostride, int odist)
{
    int fft_iter;
    int j, i;

    /* Do FFT for rank > 3: */

    for (fft_iter = 0; fft_iter < howmany; ++fft_iter) {
	/* do last dimension (out-of-place): */
	rfftw(p->rplan, p->plan_nd->n_before[p->plan_nd->rank - 1],
	      in + fft_iter * idist, istride, p->real_n * istride,
	      out + fft_iter * odist, ostride, p->plan_nd->n[p->plan_nd->rank - 1] * ostride);

	/* do first dimension (in-place): */
	fftw(p->plan_nd->plans[0], p->plan_nd->n_after[0],
	out + fft_iter * odist, p->plan_nd->n_after[0] * ostride, ostride,
	     p->plan_nd->work, 1, 0);

	/* do other dimensions (in-place): */
	for (j = 1; j < p->plan_nd->rank - 1; ++j)
	    for (i = 0; i < p->plan_nd->n_before[j]; ++i)
		fftw(p->plan_nd->plans[j], p->plan_nd->n_after[j],
		out + fft_iter * odist + i * ostride * p->plan_nd->n[j] *
		p->plan_nd->n_after[j], p->plan_nd->n_after[j] * ostride,
		     ostride, p->plan_nd->work, 1, 0);
    }
}

static void rfftw2d_in_place_rc_aux(rfftwnd_plan p, int howmany,
				FFTW_COMPLEX * in_out, int istride, int idist)
{
    int fft_iter;

    for (fft_iter = 0; fft_iter < howmany; ++fft_iter) {
	/* FFT y dimension: */
	rfftw(p->rplan, p->plan_nd->n[0],
	  in_out + fft_iter * idist, istride, istride * p->plan_nd->n[1],
	      p->plan_nd->work, 1, 0);
	/* FFT x dimension: */
	fftw(p->plan_nd->plans[0], p->plan_nd->n[1],
	  in_out + fft_iter * idist, istride * p->plan_nd->n[1], istride,
	     p->plan_nd->work, 1, 0);
    }
}

static void rfftw3d_in_place_rc_aux(rfftwnd_plan p, int howmany,
				FFTW_COMPLEX * in_out, int istride, int idist)
{
    int i;
    int fft_iter;

    for (fft_iter = 0; fft_iter < howmany; ++fft_iter) {
	/* FFT z dimension: */
	rfftw(p->rplan, p->plan_nd->n[0] * p->plan_nd->n[1],
	  in_out + fft_iter * idist, istride, p->plan_nd->n[2] * istride,
	      p->plan_nd->work, 1, 0);
	/* FFT y dimension: */
	for (i = 0; i < p->plan_nd->n[0]; ++i)
	    fftw(p->plan_nd->plans[1], p->plan_nd->n[2],
	     in_out + fft_iter * idist + i * istride * p->plan_nd->n[1] *
		 p->plan_nd->n[2], p->plan_nd->n[2] * istride, istride, p->plan_nd->work, 1, 0);
	/* FFT x dimension: */
	fftw(p->plan_nd->plans[0], p->plan_nd->n[1] * p->plan_nd->n[2],
	     in_out + fft_iter * idist, istride * p->plan_nd->n[1] * p->plan_nd->n[2], istride,
	     p->plan_nd->work, 1, 0);
    }
}

static void rfftwnd_in_place_rc_aux(rfftwnd_plan p, int howmany,
				FFTW_COMPLEX * in_out, int istride, int idist)
/* Do FFT for rank > 3: */
{
    int fft_iter;
    int j, i;

    for (fft_iter = 0; fft_iter < howmany; ++fft_iter) {
	/* do last dimension: */
	rfftw(p->rplan, p->plan_nd->n_before[p->plan_nd->rank - 1],
	      in_out + fft_iter * idist, istride, p->plan_nd->n[p->plan_nd->rank - 1] * istride,
	      p->plan_nd->work, 1, 0);

	/* do first dimension: */
	fftw(p->plan_nd->plans[0], p->plan_nd->n_after[0],
	     in_out + fft_iter * idist, p->plan_nd->n_after[0] * istride, istride,
	     p->plan_nd->work, 1, 0);

	/* do other dimensions: */
	for (j = 1; j < p->plan_nd->rank - 1; ++j)
	    for (i = 0; i < p->plan_nd->n_before[j]; ++i)
		fftw(p->plan_nd->plans[j], p->plan_nd->n_after[j],
		     in_out + fft_iter * idist + i * istride * p->plan_nd->n[j] *
		     p->plan_nd->n_after[j], p->plan_nd->n_after[j] * istride, istride,
		     p->plan_nd->work, 1, 0);
    }
}


static void rfftw2d_out_of_place_cr_aux(rfftwnd_plan p, int howmany,
				    FFTW_COMPLEX * in, int istride, int idist,
				   FFTW_COMPLEX * out, int ostride, int odist)
{
    int fft_iter;

    for (fft_iter = 0; fft_iter < howmany; ++fft_iter) {
	/* FFT x dimension (in-place): */
	fftw(p->plan_nd->plans[0], p->plan_nd->n[1],
	     in + fft_iter * idist, p->plan_nd->n[1] * istride, istride,
	     p->plan_nd->work, 1, 1);
	/* FFT y dimension (out-of-place): */
	rfftw(p->rplan, p->plan_nd->n[0],
	      in + fft_iter * idist, istride, p->plan_nd->n[1] * istride,
	      out + fft_iter * odist, ostride, p->real_n * ostride);
    }
}

static void rfftw3d_out_of_place_cr_aux(rfftwnd_plan p, int howmany,
				    FFTW_COMPLEX * in, int istride, int idist,
				   FFTW_COMPLEX * out, int ostride, int odist)
{
    int fft_iter;
    int i;

    for (fft_iter = 0; fft_iter < howmany; ++fft_iter) {
	/* FFT y dimension (in-place): */
	for (i = 0; i < p->plan_nd->n[0]; ++i)
	    fftw(p->plan_nd->plans[1], p->plan_nd->n[2],
		 in + fft_iter * idist + i * istride * p->plan_nd->n[1] * p->plan_nd->n[2],
	    p->plan_nd->n[2] * istride, istride, p->plan_nd->work, 1, 0);
	/* FFT x dimension (in-place): */
	fftw(p->plan_nd->plans[0], p->plan_nd->n[1] * p->plan_nd->n[2],
	     in + fft_iter * idist, istride * p->plan_nd->n[1] * p->plan_nd->n[2], istride,
	     p->plan_nd->work, 1, 0);

	/* FFT z dimension (out-of-place): */
	rfftw(p->rplan, p->plan_nd->n[0] * p->plan_nd->n[1],
	      in + fft_iter * idist, istride, p->plan_nd->n[2] * istride,
	      out + fft_iter * odist, ostride, p->real_n * ostride);
    }
}

static void rfftwnd_out_of_place_cr_aux(rfftwnd_plan p, int howmany,
				    FFTW_COMPLEX * in, int istride, int idist,
				   FFTW_COMPLEX * out, int ostride, int odist)
{
    int fft_iter;
    int j, i;

    /* Do FFT for rank > 3: */

    for (fft_iter = 0; fft_iter < howmany; ++fft_iter) {
	/* do first dimension (in-place): */
	fftw(p->plan_nd->plans[0], p->plan_nd->n_after[0],
	in + fft_iter * idist, p->plan_nd->n_after[0] * istride, istride,
	     p->plan_nd->work, 1, 0);

	/* do other dimensions (in-place): */
	for (j = 1; j < p->plan_nd->rank - 1; ++j)
	    for (i = 0; i < p->plan_nd->n_before[j]; ++i)
		fftw(p->plan_nd->plans[j], p->plan_nd->n_after[j],
		 in + fft_iter * idist + i * istride * p->plan_nd->n[j] *
		p->plan_nd->n_after[j], p->plan_nd->n_after[j] * istride,
		     istride, p->plan_nd->work, 1, 0);

	/* do last dimension (out-of-place): */
	rfftw(p->rplan, p->plan_nd->n_before[p->plan_nd->rank - 1],
	      in + fft_iter * idist, istride, p->plan_nd->n[p->plan_nd->rank - 1] * istride,
	      out + fft_iter * odist, ostride, p->real_n * ostride);
    }
}

static void rfftw2d_in_place_cr_aux(rfftwnd_plan p, int howmany,
				FFTW_COMPLEX * in_out, int istride, int idist)
{
    int fft_iter;

    for (fft_iter = 0; fft_iter < howmany; ++fft_iter) {
	/* FFT x dimension: */
	fftw(p->plan_nd->plans[0], p->plan_nd->n[1],
	  in_out + fft_iter * idist, istride * p->plan_nd->n[1], istride,
	     p->plan_nd->work, 1, 0);
	/* FFT y dimension: */
	rfftw(p->rplan, p->plan_nd->n[0],
	  in_out + fft_iter * idist, istride, istride * p->plan_nd->n[1],
	      p->plan_nd->work, 1, 0);
    }
}

static void rfftw3d_in_place_cr_aux(rfftwnd_plan p, int howmany,
				FFTW_COMPLEX * in_out, int istride, int idist)
{
    int i;
    int fft_iter;

    for (fft_iter = 0; fft_iter < howmany; ++fft_iter) {
	/* FFT y dimension: */
	for (i = 0; i < p->plan_nd->n[0]; ++i)
	    fftw(p->plan_nd->plans[1], p->plan_nd->n[2],
	     in_out + fft_iter * idist + i * istride * p->plan_nd->n[1] *
		 p->plan_nd->n[2], p->plan_nd->n[2] * istride, istride, p->plan_nd->work, 1, 0);
	/* FFT x dimension: */
	fftw(p->plan_nd->plans[0], p->plan_nd->n[1] * p->plan_nd->n[2],
	     in_out + fft_iter * idist, istride * p->plan_nd->n[1] * p->plan_nd->n[2], istride,
	     p->plan_nd->work, 1, 0);
	/* FFT z dimension: */
	rfftw(p->rplan, p->plan_nd->n[0] * p->plan_nd->n[1],
	  in_out + fft_iter * idist, istride, p->plan_nd->n[2] * istride,
	      p->plan_nd->work, 1, 0);
    }
}

static void rfftwnd_in_place_cr_aux(rfftwnd_plan p, int howmany,
				FFTW_COMPLEX * in_out, int istride, int idist)
/* Do FFT for rank > 3: */
{
    int fft_iter;
    int j, i;

    for (fft_iter = 0; fft_iter < howmany; ++fft_iter) {
	/* do first dimension: */
	fftw(p->plan_nd->plans[0], p->plan_nd->n_after[0],
	     in_out + fft_iter * idist, p->plan_nd->n_after[0] * istride, istride,
	     p->plan_nd->work, 1, 0);

	/* do other dimensions: */
	for (j = 1; j < p->plan_nd->rank - 1; ++j)
	    for (i = 0; i < p->plan_nd->n_before[j]; ++i)
		fftw(p->plan_nd->plans[j], p->plan_nd->n_after[j],
		     in_out + fft_iter * idist + i * istride * p->plan_nd->n[j] *
		     p->plan_nd->n_after[j], p->plan_nd->n_after[j] * istride, istride,
		     p->plan_nd->work, 1, 0);

	/* do last dimension: */
	rfftw(p->rplan, p->plan_nd->n_before[p->plan_nd->rank - 1],
	      in_out + fft_iter * idist, istride, p->plan_nd->n[p->plan_nd->rank - 1] * istride,
	      p->plan_nd->work, 1, 0);
    }
}
