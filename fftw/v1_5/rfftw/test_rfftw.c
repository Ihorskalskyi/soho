/*
 * Copyright (c) 1997,1998 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#ifndef RAND_MAX
#include <values.h>
#define RAND_MAX MAXINT
#endif

#include "rfftw.h"
#include <config.h>

void test_rfftw(int max_n, int istride, int ostride, int in_place,
		fftw_direction dir);
void test_rfftwnd(int rank, int max_n, int num_iter, int istride, int ostride, int in_place,
		  fftw_direction dir);

#define MAX_N_TO_TEST 50

int main(void)
{
    int istride, ostride;

    srand((unsigned int) clock());

    for (istride = 1; istride < 4; ++istride)
	for (ostride = 1; ostride < 4; ++ostride) {
	    test_rfftw(MAX_N_TO_TEST, istride, ostride, 0, FFTW_FORWARD);
	    test_rfftw(MAX_N_TO_TEST, istride, ostride, 1, FFTW_FORWARD);
	    test_rfftw(MAX_N_TO_TEST, istride, ostride, 0, FFTW_BACKWARD);
	    test_rfftw(MAX_N_TO_TEST, istride, ostride, 1, FFTW_BACKWARD);

	    test_rfftwnd(1, MAX_N_TO_TEST, 20, istride, ostride, 0, FFTW_FORWARD);
	    test_rfftwnd(1, MAX_N_TO_TEST, 20, istride, ostride, 1, FFTW_FORWARD);
	    test_rfftwnd(1, MAX_N_TO_TEST, 20, istride, ostride, 0, FFTW_BACKWARD);
	    test_rfftwnd(1, MAX_N_TO_TEST, 20, istride, ostride, 1, FFTW_BACKWARD);

	    test_rfftwnd(2, 40, 20, istride, ostride, 0, FFTW_FORWARD);
	    test_rfftwnd(2, 40, 20, istride, ostride, 1, FFTW_FORWARD);
	    test_rfftwnd(2, 40, 20, istride, ostride, 0, FFTW_BACKWARD);
	    test_rfftwnd(2, 40, 20, istride, ostride, 1, FFTW_BACKWARD);

	    test_rfftwnd(3, 20, 20, istride, ostride, 0, FFTW_FORWARD);
	    test_rfftwnd(3, 20, 20, istride, ostride, 1, FFTW_FORWARD);
	    test_rfftwnd(3, 20, 20, istride, ostride, 0, FFTW_BACKWARD);
	    test_rfftwnd(3, 20, 20, istride, ostride, 1, FFTW_BACKWARD);

	    test_rfftwnd(4, 10, 20, istride, ostride, 0, FFTW_FORWARD);
	    test_rfftwnd(4, 10, 20, istride, ostride, 1, FFTW_FORWARD);
	    test_rfftwnd(4, 10, 20, istride, ostride, 0, FFTW_BACKWARD);
	    test_rfftwnd(4, 10, 20, istride, ostride, 1, FFTW_BACKWARD);

	    test_rfftwnd(5, 6, 20, istride, ostride, 0, FFTW_FORWARD);
	    test_rfftwnd(5, 6, 20, istride, ostride, 1, FFTW_FORWARD);
	    test_rfftwnd(5, 6, 20, istride, ostride, 0, FFTW_BACKWARD);
	    test_rfftwnd(5, 6, 20, istride, ostride, 1, FFTW_BACKWARD);
	}
    return 0;
}

#ifdef FFTW_ENABLE_FLOAT
#define NOT_CLOSE(a,b) (fabs((a) - (b))*2.0/(fabs(a)+fabs(b)+0.1) >= 1e-2)
#else
#define NOT_CLOSE(a,b) (fabs((a) - (b))*2.0/(fabs(a)+fabs(b)+0.1) >= 1e-5)
#endif

void test_rfftw(int max_n, int istride, int ostride, int in_place,
		fftw_direction dir)
{
    FFTW_REAL *rin;
    FFTW_COMPLEX *in, *out, *rout;
    fftw_plan plan, plan_inv;
    rfftw_plan rplan, rplan_inv;
    int flags = FFTW_ESTIMATE;
    int i, j, n;
    int howmany;

    if (in_place) {
	flags |= FFTW_IN_PLACE;
	if (ostride != istride)
	    return;
    }
    printf("\nTESTING rfftw: stride=%d/%d, dir=%+d, %s\n",
	   istride, ostride, dir, in_place ? "in-place" : "out-of-place");
    fflush(stdout);

    howmany = istride;
    if (ostride < istride)
	howmany = ostride;

    for (n = 2; n <= max_n; n += 2) {
	printf("   Testing N=%d...", n);
	fflush(stdout);

	plan = fftw_create_plan(n, dir, flags);
	plan_inv = fftw_create_plan(n, (fftw_direction) -dir, flags);
	rplan = rfftw_create_plan(n, dir, flags, REAL_TO_COMPLEX);
	rplan_inv = rfftw_create_plan(n, (fftw_direction) -dir,
				      flags, COMPLEX_TO_REAL);

	printf(".");
	fflush(stdout);

	rin = (FFTW_REAL *)fftw_malloc((n + 2) * sizeof(FFTW_REAL) * istride);
	in = (FFTW_COMPLEX *)fftw_malloc(n * sizeof(FFTW_COMPLEX) * istride);
	if (in_place) {
	    out = in;
	    rout = (FFTW_COMPLEX *) rin;
	} else {
	    out = (FFTW_COMPLEX *)
		 fftw_malloc(n * sizeof(FFTW_COMPLEX) * ostride);
	    rout = (FFTW_COMPLEX *)
		 fftw_malloc((n / 2 + 1) * sizeof(FFTW_COMPLEX) * ostride);
	}
	if (!rin || !in || !out || !rout || !plan || !plan_inv || !rplan || !rplan_inv) {
	    printf("Out of memory!!!\n");
	    exit(1);
	}
	/* initialize arrays to random values */
	for (j = 0; j < howmany; ++j)
	    for (i = 0; i < n; ++i) {
		rin[i / 2 * istride * 2 + i % 2 + j * 2] = rand() * 2.0 / RAND_MAX - 1.0;	/* random number in [-1,1] */
		c_re(in[i * istride + j]) = rin[i / 2 * istride * 2 + i % 2 + j * 2];
		c_im(in[i * istride + j]) = 0.0;
	    }

	printf("R->C...");
	fflush(stdout);

	if (in_place) {
	    fftw(plan, howmany, in, istride, 1, 0, 0, 0);
	    rfftw(rplan, howmany, (FFTW_COMPLEX *) rin, istride, 1, 0, 0, 0);
	} else {
	    fftw(plan, howmany, in, istride, 1, out, ostride, 1);
	    rfftw(rplan, howmany, (FFTW_COMPLEX *) rin, istride, 1, rout, ostride, 1);
	}

	printf("checking...");
	fflush(stdout);

	for (j = 0; j < howmany; ++j)
	    for (i = 0; i <= n / 2; ++i)
		if (NOT_CLOSE(c_re(out[i * ostride + j]), c_re(rout[i * ostride + j])) ||
		    NOT_CLOSE(c_im(out[i * ostride + j]), c_im(rout[i * ostride + j]))) {
		    printf("\nERROR! Output different at i=%d in array %d!\n"
			   " -- (%g,%g) instead of (%g,%g)\n", i, j,
			   c_re(rout[i * ostride + j]), c_im(rout[i * ostride + j]), c_re(out[i * ostride + j]), c_im(out[i * ostride + j]));
		    exit(1);
		}
	printf("C->R...");
	fflush(stdout);

	if (in_place) {
	    fftw(plan_inv, howmany, in, istride, 1, 0, 0, 0);
	    rfftw(rplan_inv, howmany, (FFTW_COMPLEX *) rin, istride, 1, 0, 0, 0);
	} else {
	    fftw(plan_inv, howmany, out, ostride, 1, in, istride, 1);
	    rfftw(rplan_inv, howmany, rout, ostride, 1, (FFTW_COMPLEX *) rin, istride, 1);
	}

	printf("checking...");
	fflush(stdout);

	for (j = 0; j < howmany; ++j) {
	    for (i = 0; i <= n / 2; ++i)
		if (NOT_CLOSE(c_re(in[i * istride + j]), rin[i / 2 * istride * 2 + i % 2 + j * 2])) {
		    printf("\nERROR! Output different at i=%d in array %d!\n"
			   " -- %g instead of (%g,%g)\n", i, j,
			   rin[i / 2 * istride * 2 + i % 2 + j * 2], c_re(in[i * istride + j]), c_im(in[i * istride + j]));
		    exit(1);
		}
	}

	printf("ok...");
	fflush(stdout);

	fftw_destroy_plan(plan);
	fftw_destroy_plan(plan_inv);
	rfftw_destroy_plan(rplan);
	rfftw_destroy_plan(rplan_inv);

	fftw_free(in);
	fftw_free(rin);
	if (!in_place) {
	    fftw_free(out);
	    fftw_free(rout);
	}
	printf("okay!\n");
	fflush(stdout);

	fftw_check_memory_leaks();
    }
}

#define MAX_RANK 10

void test_rfftwnd(int rank, int max_n, int num_iter, int istride, int ostride, int in_place,
		  fftw_direction dir)
{
    FFTW_REAL *rin, val;
    FFTW_COMPLEX *in, *out, *rout;
    fftwnd_plan plan, plan_inv;
    rfftwnd_plan rplan, rplan_inv;
    int flags = FFTW_ESTIMATE;
    int i, j, k;
    int n[MAX_RANK], iter, N, N_other_dims,
	 N_last = 0, N_last_rc = 0, N_last_r = 0;
    int howmany, dim;

    if (rank > MAX_RANK)
	return;

    if (in_place) {
	flags |= FFTW_IN_PLACE;
	if (ostride != istride)
	    return;
    }
    printf("\nTESTING rfftwnd: stride=%d/%d, dir=%+d, %s\n",
	   istride, ostride, dir, in_place ? "in-place" : "out-of-place");
    fflush(stdout);

    howmany = istride;
    if (ostride < istride)
	howmany = ostride;

    for (iter = 0; iter <= num_iter; ++iter) {
	printf("   Testing size ");
	N = 1;
	for (dim = 0; dim < rank - 1; ++dim) {
	    n[dim] = rand() % max_n + 1;
	    if (dim)
		printf("x");
	    printf("%d", n[dim]);
	    N *= n[dim];
	}
	N_other_dims = N;
	if (rank > 0) {
	    n[rank - 1] = (rand() % (max_n / 2) + 1) * 2;	/* must be even */
	    N *= n[rank - 1];
	    N_last = n[rank - 1];
	    N_last_rc = n[rank - 1] / 2 + 1;
	    if (in_place)
		N_last_r = N_last_rc * 2;
	    else
		N_last_r = N_last;
	    if (rank > 1)
		printf("x");
	    printf("%d", n[rank - 1]);
	}
	printf(" array...");
	fflush(stdout);

	plan = fftwnd_create_plan(rank, n, dir, flags);
	plan_inv = fftwnd_create_plan(rank, n, 
				      (fftw_direction) -dir, flags);
	rplan = rfftwnd_create_plan(rank, n, dir, flags, REAL_TO_COMPLEX);
	rplan_inv = rfftwnd_create_plan(rank, n, 
					(fftw_direction) -dir, flags,
					COMPLEX_TO_REAL);

	printf(".");
	fflush(stdout);

	rin = (FFTW_REAL *)fftw_malloc(N_last_r * N_other_dims * sizeof(FFTW_REAL) * istride);
	in = (FFTW_COMPLEX *)fftw_malloc(N * sizeof(FFTW_COMPLEX) * istride);
	if (in_place) {
	    out = in;
	    rout = (FFTW_COMPLEX *) rin;
	} else {
	    out = (FFTW_COMPLEX *)
		 fftw_malloc(N * sizeof(FFTW_COMPLEX) * ostride);
	    rout = (FFTW_COMPLEX *)
		 fftw_malloc(N_last_rc * N_other_dims * sizeof(FFTW_COMPLEX) * ostride);
	}
	if (!rin || !in || !out || !rout || !plan || !plan_inv || !rplan || !rplan_inv) {
	    printf("Out of memory!!!\n");
	    exit(1);
	}
	/* initialize arrays to random values */
	for (j = 0; j < howmany; ++j)
	    for (k = 0; k < N_other_dims; ++k)
		for (i = 0; i < N_last; ++i) {
		    val = rand() * 2.0 / RAND_MAX - 1.0;	/* random number in [-1,1] */
		    rin[(i + k * N_last_r) / 2 * istride * 2 + (i + k * N_last_r) % 2 + j * 2] = val;
		    c_re(in[(i + k * N_last) * istride + j]) = val;
		    c_im(in[(i + k * N_last) * istride + j]) = 0.0;
		}

	printf("R->C...");
	fflush(stdout);

	if (in_place) {
	    fftwnd(plan, howmany, in, istride, 1, 0, 0, 0);
	    rfftwnd(rplan, howmany, (FFTW_COMPLEX *) rin, istride, 1, 0, 0, 0);
	} else {
	    fftwnd(plan, howmany, in, istride, 1, out, ostride, 1);
	    rfftwnd(rplan, howmany, (FFTW_COMPLEX *) rin, istride, 1, rout, ostride, 1);
	}

	printf("checking...");
	fflush(stdout);

	for (j = 0; j < howmany; ++j)
	    for (k = 0; k < N_other_dims; ++k)
		for (i = 0; i < N_last_rc; ++i)
		    if (NOT_CLOSE(c_re(out[(i + k * N_last) * ostride + j]), c_re(rout[(i + k * N_last_rc) * ostride + j])) ||
			NOT_CLOSE(c_im(out[(i + k * N_last) * ostride + j]), c_im(rout[(i + k * N_last_rc) * ostride + j]))) {
			printf("\nERROR! Output different at i=%d,k=%d in array %d!\n"
			     " -- (%g,%g) instead of (%g,%g)\n", i, k, j,
			       c_re(rout[(i + k * N_last_rc) * ostride + j]), c_im(rout[(i + k * N_last_rc) * ostride + j]), c_re(out[(i + k * N_last) * ostride + j]), c_im(out[(i + k * N_last) * ostride + j]));
			exit(1);
		    }
	printf("C->R...");
	fflush(stdout);

	if (in_place) {
	    fftwnd(plan_inv, howmany, in, istride, 1, 0, 0, 0);
	    rfftwnd(rplan_inv, howmany, (FFTW_COMPLEX *) rin, istride, 1, 0, 0, 0);
	} else {
	    fftwnd(plan_inv, howmany, out, ostride, 1, in, istride, 1);
	    rfftwnd(rplan_inv, howmany, rout, ostride, 1, (FFTW_COMPLEX *) rin, istride, 1);
	}

	printf("checking...");
	fflush(stdout);

	for (j = 0; j < howmany; ++j)
	    for (k = 0; k < N_other_dims; ++k)
		for (i = 0; i < N_last; ++i)
		    if (NOT_CLOSE(c_re(in[(i + k * N_last) * istride + j]), rin[(i + k * N_last_r) / 2 * istride * 2 + (i + k * N_last_r) % 2 + j * 2])) {
			printf("\nERROR! Output different at i=%d,k=%d in array %d!\n"
			       " -- %g instead of (%g,%g)\n", i, k, j,
			       rin[(i + k * N_last_r) / 2 * istride * 2 + (i + k * N_last_r) % 2 + j * 2],
			       c_re(in[(i + k * N_last) * istride + j]),
			       c_im(in[(i + k * N_last) * istride + j]));
			exit(1);
		    }
	printf("ok...");
	fflush(stdout);

	fftwnd_destroy_plan(plan);
	fftwnd_destroy_plan(plan_inv);
	rfftwnd_destroy_plan(rplan);
	rfftwnd_destroy_plan(rplan_inv);

	fftw_free(in);
	fftw_free(rin);
	if (!in_place) {
	    fftw_free(out);
	    fftw_free(rout);
	}
	printf("okay!\n");
	fflush(stdout);

	fftw_check_memory_leaks();
    }
}
