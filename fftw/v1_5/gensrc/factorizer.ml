(*
 * Copyright (c) 1997,1998 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *)

(* Functions to recursively factorize z^n - 1.  These factorizers
   require their own representation of polynomials to enable analytic
   transformations. 

   The actual recursive factorizers are at the end of this file. *)

open List
open Util

type factorizer_poly_term =
| Zero
| Omega of int * int
| Plus of factorizer_poly_term list

let negate_omega n i = lowest_terms (n * 2) (i*2 + n)

let rec negate_fpt = function
       | Zero -> Zero
       | Omega (n, i) -> let (m,j) = (negate_omega n i) in Omega (m,j)
       | Plus a -> Plus (map negate_fpt a)

let one = Omega (1,0)
let mone = negate_fpt one

let is_zero = function
       | Zero -> true
       | _ -> false

let is_one = function
       | Omega (n, i) -> i mod n == 0
       | _ -> false

let rec times u v = match (u,v) with
    | (Zero,a) -> Zero
    | (a,Zero) -> Zero
    | (Omega (n,i), Omega (m,j)) -> Omega (n*m, i*m + j*n)
    | (a, Plus b) -> Plus (map (times a) b)
    | (Plus b, a) -> times v u

(************ Simplifier ***********)

let rec flatten_plus = function
       | [] -> []
       | Plus a :: b -> (flatten_plus a) @ (flatten_plus b)
       | x :: b -> x :: (flatten_plus b)
 
let rec simplify_plus_zeros = function
       | [] -> []
       | Zero :: s -> simplify_plus_zeros s
       | x :: s -> x :: (simplify_plus_zeros s)
			   
(* In the following functions (rooted at simplify_plus_omegas), we
   try to simplify sums of omegas using various identities.  It
   seems to be hard to do this right, and there are still (rare) cases
   that aren't simplified properly.  =/  It is also slow.  We don't
   actually use it at the moment, although it is handy for debugging. *)

let rec remove_first filter = function
       | [] -> []
       | a :: r -> if (filter a) then r else a :: (remove_first filter r)

let equals_omega (n,i) = 
    let (nn,ii) = lowest_terms n i 
    in function
	   | Omega (m,j) -> m == nn && j == ii
	   | _ -> false

let remove_omegas omega_list a =
    let newa =
	fold_left (fun b om -> remove_first (equals_omega om) b)
	a omega_list
    in if ((length newa) + (length omega_list) == (length a)) then
      newa
    else a

let rec first_simplification simplifier a = function
       | [] -> a
       | x :: r -> let newa = simplifier a x in
	   if ((length newa) < (length a)) then
	     newa
	   else
	     first_simplification simplifier a r

let simplify_omega n i curlst =
    if ((n == 1 && i == 0) || (n == 2 && i == 1)) then
      remove_omegas [(1,0); (2,1)] curlst
    else
      let facts = 1 :: (factors n)
      in let newlst = first_simplification
	  (fun a f -> remove_omegas 
	      (forall_flat 0 (n/f) (fun k -> [(n, k * f + (i mod f))])) 
	      a)
	  (one :: curlst) facts
      in if ((length newlst) < (length curlst) + 1) then begin
	if (exists (equals_omega (1,0)) newlst) then
	  remove_first (equals_omega (1,0)) newlst
	else
	  mone :: newlst
      end
      else curlst

let rec simplify_omegas curlst = function
       | [] -> curlst
       | Omega (n,i) :: r -> let newlst = simplify_omega n i curlst in
	   if (newlst == curlst) then
	     simplify_omegas curlst r
	   else
	     simplify_omegas newlst newlst
       | x :: r -> x :: (simplify_omegas curlst r)

let simplify_plus_omegas lst = simplify_omegas lst lst

let simplify_plus_result = function
	   | [] -> Zero
	   | x :: [] -> x
	   | a -> Plus a

let rec simplify_fpt = function
       | Omega (n,i) -> let (m,j) = (lowest_terms n i) in Omega (m,j)
       | Plus a -> simplify_plus_result (simplify_plus_zeros
					 (simplify_plus_omegas
					  (map simplify_fpt 
					   (flatten_plus a))))
       | term -> term

(******* manipulating polynomials (arrays) of factorizer_poly_terms ********)

let degree p = (Array.length p) - 1

let make_monomial deg a =
    let result = Array.make (deg + 1) Zero
    in begin
      result.(deg) <- a;
      result
    end

let make_binomial deg a =
    let result = Array.make (deg + 1) Zero
    in begin
      result.(deg) <- one;
      result.(0) <- a;
      result
    end

let factorizer_start_binomial n = make_binomial n mone

let all_zeros a = Array.fold_left (&&) true (Array.map is_zero a)

let is_binomial poly =
    if ((degree poly) < 1) then
      false
    else if (not (is_one poly.(degree poly))) then
      false
    else 
      all_zeros (Array.sub poly 1 ((degree poly) - 1))

let fix_degree poly =
    let real_deg =
        max_list (Array.to_list 
		  (Array.mapi (fun i c -> if (is_zero c) then 0 else i)
		   poly))
    in Array.sub poly 0 (real_deg + 1)

let simplify_polyf poly = fix_degree (Array.map simplify_fpt poly)

let add_polyf_nosimplify p1 p2 =
    let (pa,pb) = if ((degree p1) > (degree p2)) then (p1,p2) else (p2,p1)
    in Array.mapi (fun i c ->
        if (i > (degree pb)) then c
        else Plus [c; pb.(i)]) pa

let add_polyf p1 p2 = simplify_polyf (add_polyf_nosimplify p1 p2)

let mult_polyf_nosimplify p1 p2 =
    let result = Array.make (degree p1 + degree p2 + 1) Zero
    in begin
        for i = 0 to degree p1 do
          for j = 0 to degree p2 do
            result.(i+j) <- Plus [result.(i+j); times p1.(i) p2.(j)]
          done;
        done;
      result
    end

let mult_polyf p1 p2 = simplify_polyf (mult_polyf_nosimplify p1 p2)

(********** Converting to ordinary (poly.ml) polynomials **********)

let rec to_cexpr fpt = match (simplify_fpt fpt) with
       | Zero -> Complex.zero
       | Omega (n,i) -> Complex.exp n i
       | Plus a -> Complex.simplify (Complex.plus (map to_cexpr a))

let to_poly fp = Array.map to_cexpr fp

(********** Factorizers **********)

(* A factorizer is a function that takes a factorizer polynomial,
   poly, (array of factorizer_poly_term's) and returns a list of pairs
   (p,fp), where fp is a factorizer polynomial and p is the
   corresponding poly.ml polynomial (array of complex expressions).
   These polynomials are the factors of poly.

   If the factorizer cannot factor poly, it returns the empty list, []. *)

let cooley_tukey_factorizer poly =
    let n = degree poly
    in let f1 = first_factor n
    in let f = if (f1 == 1) then n else f1
    in let result =
	if ((degree poly) > 1 && (is_binomial poly) && f > 1) then
      	  match poly.(0) with
	      | Omega (m,k) ->
		let (mi,ki) = negate_omega m k in
	      	    Array.to_list (Array.init f (fun j -> 
		    	let (new_m,new_k) = negate_omega (f*mi) (ki + j*mi) in
			    make_binomial (n/f) (Omega (new_m,new_k))))
	      | _ -> []
    	else
      	  []
    in map (fun fp -> (to_poly fp, fp)) result

