(*
 * Copyright (c) 1997,1998 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *)

(* $Id: genfft.ml,v 1.1 1998/06/23 21:56:30 dcschmid Exp $ *)

(* This file contains the entry point for the genfft program: it
   parses the command-line parameters and calls the rest of the
   program as needed to generate the requested code. *)

open To_c
open Expr

let totally_mangle expr =
  let simple =  Exprdag.simplify_to_alist expr in
  let scheduled = Schedule.schedule simple in
  let annotated = Asched.annotate scheduled
  in annotated


let make_expr name = Var (Variable.make_named name)

let iarray = "input"
let oarray = "output"
let istride = "istride"
let ostride = "ostride"
let twiddle_order = "twiddle_order"

type codelet_type = TWIDDLE | NO_TWIDDLE

let rec list_to_c = function
    [] -> ""
  | [a] -> (string_of_int a)
  | a :: b -> (string_of_int a) ^ ", " ^ (list_to_c b)

let codelet_description n dir ty f =
  let Fcn (_, name, _, _) = f 
  and (a, m, v, mem) = arith_complexity f
  and (ctype, itype) = match ty with
    TWIDDLE -> "FFTW_TWIDDLE", 0
  | NO_TWIDDLE -> "FFTW_NOTW", 1
  and (cdir, idir) = match dir with
    Fft.FORWARD -> "FFTW_FORWARD", 0
  | Fft.BACKWARD -> "FFTW_BACKWARD", 1

  and (_, num_twiddle, tw_o) = Fft.twiddle_policy ()
  in let (declare_order, order, nt) =
    match ty with
      TWIDDLE -> ("static const int " ^ twiddle_order ^ "[] = {" ^
		  (list_to_c (tw_o n)) ^ "};\n"),
	twiddle_order,
	num_twiddle n
    | NO_TWIDDLE -> "", "(const int *) 0", 0

  (* this should be replaced by CRC/MD5 of the codelet *)
  and signature = 2 * (2 * n + idir) + itype 

  in "\n\n" ^ declare_order ^
  "fftw_codelet_desc " ^ name ^ "_desc = {\n" ^
  "\"" ^ name ^ "\",\n" ^
  "(void (*)()) " ^ name ^ ",\n" ^
  (string_of_int n) ^ ",\n" ^
  cdir ^ ",\n" ^
  ctype ^ ",\n" ^
  (string_of_int signature) ^ ",\n" ^
  (string_of_int nt) ^ ",\n" ^
  order ^ ",\n" ^
  "{ " ^ (string_of_int a) ^ "," ^ (string_of_int m) ^ "," ^
  (string_of_int v) ^ "," ^ (string_of_int mem) ^ "}\n" ^
  "};\n"

let no_twiddle_gen n dir =
  let asch = totally_mangle (Fft.no_twiddle_gen_expr n Fft.no_sym dir)
  and ns = string_of_int n
  and (name, sign) = match dir with
    Fft.FORWARD -> "fftw_no_twiddle_", (-1)
  | Fft.BACKWARD -> "fftwi_no_twiddle_", 1
  and unparse_var = 
    Variable.make_unparser (iarray, Some istride) 
      (oarray, Some ostride) ("BUG", None)
  in let tree = 
	Fcn ("void", name ^ ns,
	     [Decl ("const FFTW_COMPLEX *", iarray);
	      Decl ("FFTW_COMPLEX *", oarray);
	      Decl ("int", istride);
	      Decl ("int", ostride)],
	     Asch asch)
  in let desc = codelet_description n dir NO_TWIDDLE tree
  in ((make_unparser unparse_var) tree) ^ desc

let ioarray = "inout"
let iostride = "iostride"
let twarray = "W"

let twiddle_gen n dir =
  let asch = totally_mangle (Fft.twiddle_gen_expr n dir)
  and ns = string_of_int n
  and m = "m"
  and dist = "dist"
  and a = "A"
  and i = "i"

  in let me = make_expr m
  and diste = make_expr dist
  and ae = make_expr a
  and ie = make_expr i
  and ioarraye = make_expr ioarray
  and twarraye = make_expr twarray

  and (name, sign) = match dir with
    Fft.FORWARD -> "fftw_twiddle_", (-1)
  | Fft.BACKWARD -> "fftwi_twiddle_", 1

  and unparse_var =
    Variable.make_unparser (ioarray, Some iostride) 
      (ioarray, Some iostride) (twarray, None)

  and (_, num_twiddles, _) = Fft.twiddle_policy ()

  in let body = Block (
    [Decl ("int", i); Decl ("FFTW_COMPLEX *", ioarray)],
    [Stmt_assign (ioarraye, ae);
      For (Expr_assign (ie, Integer 0),
	   Binop (" < ", ie, me),
	   Comma (Expr_assign (ie, Plus [ie; Integer 1]),
		  (Comma (Expr_assign (ioarraye, Plus [ioarraye; diste]),
                          (Expr_assign (twarraye, 
					Plus [twarraye;
					      Integer (num_twiddles n)]))))),
	   Asch asch)])
  in let fcnname = name ^ ns
  in let tree = 
    Fcn ("void", fcnname,
	 [Decl ("FFTW_COMPLEX *", a);
	   Decl ("const FFTW_COMPLEX *", twarray);
	   Decl ("int", iostride);
	   Decl ("int", m);
	   Decl ("int", dist)],
         body)

  in let desc = codelet_description n dir TWIDDLE tree
  in ((make_unparser unparse_var) tree) ^ desc

let usage = "Usage: genfft [-notwiddle | -notwiddleinv | -twiddle | -twiddleinv] <number>"

type mode = 
  | GEN_NOTHING
  | GEN_TWIDDLE of int
  | GEN_NOTWID of int
  | GEN_TWIDDLEI of int
  | GEN_NOTWIDI of int

let mode = ref GEN_NOTHING

let undocumented = " Undocumented voodoo parameter"

let main () =
  Arg.parse [
  "-notwiddle", Arg.Int(fun i -> mode := GEN_NOTWID i), "<n> : Generate a no twiddle codelet of size <n>";
  "-notwiddleinv", Arg.Int(fun i -> mode := GEN_NOTWIDI i), "<n> : Generate a no twiddle inverse codelet of size <n>";
  "-twiddle", Arg.Int(fun i -> mode := GEN_TWIDDLE i), "<n> : Generate a twiddle codelet of size <n>";
  "-twiddleinv", Arg.Int(fun i -> mode := GEN_TWIDDLEI i), "<n> : Generate a twiddle inverse codelet of size <n>";

  "-inline-konstants", 
  Arg.Unit(fun () -> Magic.inline_konstants := true),
  " Inline floating point constants";
  "-no-inline-konstants", 
  Arg.Unit(fun () -> Magic.inline_konstants := false),
  " Do not inline floating point constants";

  "-rader-min", Arg.Int(fun i -> Magic.rader_min := i),
  "<n> : Use Rader's algorithm for prime sizes >= <n>";

  "-magic-window", Arg.Int(fun i -> Magic.window := i), undocumented;
  "-magic-variables", Arg.Int(fun i -> Magic.number_of_variables := i),
  undocumented;

  "-magic-loopo", 
  Arg.Unit(fun () -> Magic.loopo := true),
  undocumented;
  "-magic-loopi", 
  Arg.Unit(fun () -> Magic.loopo := false),
  undocumented;

  "-magic-inline-single", 
  Arg.Unit(fun () -> Magic.inline_single := true),
  undocumented;
  "-magic-no-inline-single", 
  Arg.Unit(fun () -> Magic.inline_single := false),
  undocumented;

  "-magic-enable-flatten-sum", 
  Arg.Unit(fun () -> Magic.enable_flatten_sum := true),
  undocumented;

  "-magic-in-order-loads", 
  Arg.Unit(fun () -> Magic.in_order_loads := true),
  undocumented;
  "-magic-in-order-stores", 
  Arg.Unit(fun () -> Magic.in_order_stores := true),
  undocumented;
  "-magic-no-in-order-loads", 
  Arg.Unit(fun () -> Magic.in_order_loads := false),
  undocumented;
  "-magic-no-in-order-stores", 
  Arg.Unit(fun () -> Magic.in_order_stores := false),
  undocumented;

  "-magic-real-imag-loads", 
  Arg.Unit(fun () -> Magic.real_imag_loads := true),
  undocumented;
  "-magic-real-imag-stores", 
  Arg.Unit(fun () -> Magic.real_imag_stores := true),
  undocumented;
  "-magic-no-real-imag-loads", 
  Arg.Unit(fun () -> Magic.real_imag_loads := false),
  undocumented;
  "-magic-no-real-imag-stores", 
  Arg.Unit(fun () -> Magic.real_imag_stores := false),
  undocumented;

  "-magic-use-wsquare", 
  Arg.Unit(fun () -> Magic.use_wsquare := true),
  undocumented;
  "-magic-no-wsquare", 
  Arg.Unit(fun () -> Magic.use_wsquare := false),
  undocumented;

  "-magic-twiddle-load-all", 
  Arg.Unit(fun () -> Magic.twiddle_policy := Magic.TWIDDLE_LOAD_ALL),
  undocumented;
  "-magic-twiddle-iter", 
  Arg.Unit(fun () -> Magic.twiddle_policy := Magic.TWIDDLE_ITER),
  undocumented;
  "-magic-twiddle-load-odd", 
  Arg.Unit(fun () -> Magic.twiddle_policy := Magic.TWIDDLE_LOAD_ODD),
  undocumented;
  "-magic-twiddle-square1", 
  Arg.Unit(fun () -> Magic.twiddle_policy := Magic.TWIDDLE_SQUARE1),
  undocumented;
  "-magic-twiddle-square2", 
  Arg.Unit(fun () -> Magic.twiddle_policy := Magic.TWIDDLE_SQUARE2),
  undocumented;
  "-magic-twiddle-square3", 
  Arg.Unit(fun () -> Magic.twiddle_policy := Magic.TWIDDLE_SQUARE3),
  undocumented;
] (fun _ -> failwith "too many arguments") usage;

  let out = 
    match !mode with
    | GEN_TWIDDLE i -> (twiddle_gen i Fft.FORWARD)
    | GEN_TWIDDLEI i -> (twiddle_gen i Fft.BACKWARD)
    | GEN_NOTWID i -> (no_twiddle_gen i Fft.FORWARD)
    | GEN_NOTWIDI i -> (no_twiddle_gen i Fft.BACKWARD)
    |	_ -> failwith "one of -notwiddle, -notwiddleinv, -twiddle or -twiddleinv must be specified"

  in begin
    print_string out;
    exit 0;
  end

let _ = main()

