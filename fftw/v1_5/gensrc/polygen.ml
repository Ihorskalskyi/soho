(*
 * Copyright (c) 1997,1998 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *)

(* Generate instruction lists to convolve polynomials or compute
   them modulo some recursive factorization *)

open Complex
open Poly
open Array

let temps_of_poly poly =
    let temps = make_temporary_array ((degree poly) + 1)
    in
	(fold_left (@) [] (mapi (fun i p -> assign temps.(i) p) poly),
	 map expr_of_var temps)

(* generate a the convolution of a with b (1st 5 arguments are the
   same as to gen_convolution in fft.ml).  This corresponds to
   multiplying the polynomials a and b modulo z^n - 1, which we
   compute by recursive factorization of z^n - 1.  factorizer is a
   function to recursively factorize the polynomial z^n - 1.  *)

let gen_convolution_poly n a b sumvar conv factorizer =
    let polya = poly_of_cvar_func a n
    and polyb = poly_of_cvar_func b n
    
	(* if we ever compute a mod (z - 1) then that is equal to the
	   sum of the components of a; otherwise, we will have to sum a
	   manually *)
    and sumvar_assigned = ref false
    in let assign_sumvar a p =
	if ((degree p) == 1 && (is_one p.(degree p)) && (is_mone p.(0))
	    && not !sumvar_assigned) then begin
	      sumvar_assigned := true;
	      assign sumvar a.(0)
	    end
	else
	  []

    in let rec factorize_conv a b poly factorizer_poly =
	let (assign_a, newa) = temps_of_poly a
	and (assign_b, newb) = temps_of_poly b
        in let (conv_assigns, conv_poly) =
	    if ((degree newa) == 0 || (degree newb) == 0) then
	      ([], mult_poly newa newb)
	    else
	      let facts = factorizer factorizer_poly in
		  if ((List.length facts) == 0) then
	    	    ([], mult_poly newa newb)
		  else
		    let (alist_list, convs) = 
		    	List.split (List.map (fun (p,fp) -> 
			    (factorize_conv (mod_poly newa p)
			     (mod_poly newb p) p fp)) facts)
		    and (crts, crtp) = find_CRT_coefs (List.map fst facts)
		    in let (scaled_convs) = 
			List.map2 (fun p s -> scale_poly p s) convs crts
  		    in let (scta, sct) = 
			List.split (List.map temps_of_poly scaled_convs)
		    in 
		    	((List.flatten alist_list) @ (List.flatten scta),
			 add_polys (List.map2 (fun crt p -> mult_poly crt p)
				    crtp sct))
	in let (assigns1, temp1) = temps_of_poly conv_poly
	in let (assigns2, temp2) = temps_of_poly (mod_poly temp1 poly)
	in (assign_a @ assign_b @ 
	    (assign_sumvar newa poly) @ conv_assigns @ assigns1 @ assigns2,
	    temp2)

    in let start_binomial = Factorizer.factorizer_start_binomial n
    in let (conv_assigns, conv_poly) =
	factorize_conv polya polyb (Factorizer.to_poly start_binomial)
	start_binomial
    in let assign_result = fold_left (@) [] 
	(mapi (fun i p -> assign (conv i) p) conv_poly)
    in if (!sumvar_assigned) then
      conv_assigns @ assign_result
    else
      conv_assigns @ assign_result @ (assign sumvar (plus (to_list polya)))

			
