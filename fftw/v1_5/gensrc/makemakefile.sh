#! /bin/sh

# This file generates the FFTW makefile, starting from Makefile.in

. ./config

# Compute the list of file names
notw_codelets=""
notwi_codelets=""

for i in $NOTW
do
    notw_codelets="$notw_codelets ${NOTW_PREFIX}${i}.o"
    notwi_codelets="$notwi_codelets ${NOTWI_PREFIX}${i}.o"
done

twiddle_codelets=""
twiddlei_codelets=""

for i in $TWIDDLE
do
    twiddle_codelets="$twiddle_codelets ${TWID_PREFIX}${i}.o"
    twiddlei_codelets="$twiddlei_codelets ${TWIDI_PREFIX}${i}.o"
done

# now substitute list in Makefile.in, to get Makefile.fftw
# (the two cats are redundant, but the script is clearer this way)
cat Makefile.in |
    sed -e "s/@NOTW_CODELETS@/$notw_codelets/g" |
    sed -e "s/@NOTWI_CODELETS@/$notwi_codelets/g" |
    sed -e "s/@TWID_CODELETS@/$twiddle_codelets/g" |
    sed -e "s/@TWIDI_CODELETS@/$twiddlei_codelets/g" |
cat >Makefile.fftw




