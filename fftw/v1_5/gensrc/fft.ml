(*
 * Copyright (c) 1997,1998 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *)

(* $Id: fft.ml,v 1.1 1998/06/23 21:56:29 dcschmid Exp $ *)

(* This is the part of the generator that actually computes the FFT,
   in symbolic form, and outputs the assignment lists (which assign
   the DFT of the input array to the output array). *)

open Complex
open Util
open Lazy

let freeze n f =
  let a = Array.init n (fun i -> lazy (f i))
  in fun i -> force a.(i)

(* various kinds of symmetries *)
(*
 * symmetries are encoded as symmetries of the *input*.  A symmetry
 * determines the symmetry of the output (osym), and symmetries at
 * intermediate stages of divide and conquer.
 *)
type symmetry = {
  apply: int -> (int -> Complex.expr) -> int -> Complex.expr;
  store: int -> (int -> Complex.expr) -> int -> Exprdag.node list;
  osym: symmetry;
  isym1: symmetry;
  isym2: symmetry}

let rec no_sym = {
  isym1 = no_sym; 
  isym2 = no_sym;
  osym = no_sym;
  store = (fun _ f i -> store_var (access_output i) (f i));
  apply = fun _ f -> f
} and real_sym = {
  osym = hermitian_sym;
  isym1 = real_sym;
  isym2 = no_sym;
  store = (fun _ f i -> store_real (access_output i) (f i));
  apply = fun _ f -> real @@ f
} and hermitian_sym = {
  osym = real_sym;
  isym1 = no_sym;
  isym2 = hermitian_sym;
  apply = (fun n f i ->
    if (i = 0) then real (f 0)
    else if (i < n - i)  then (f i)
    else if (i > n - i)  then conj (f (n - i))
    else real (f i));
  store = fun n f i ->
    if (i = 0) then store_real (access_output i) (f i)
    else if (i < n - i) then store_var (access_output i) (f i)
    else if (i == n - i) then store_real (access_output i) (f i)
    else []
} 

  
let rec fftgen_prime n input sign = 
  let sum filter i =
    sigma 0 n (fun j ->
      let coeff = filter (exp n (sign * i * j))
      in [times coeff (input j)]) in
  let computation_even = freeze n (sum identity)
  and computation_odd =
    freeze n (fun i ->
      if (i = 0) then
	(* expose some common subexpressions *)
	plus [input 0; 
	       sigma 1 ((n + 1) / 2) (fun j ->
		 [plus [(input j); (input (n - j))]])]
      else
	let i' = min i (n - i) in
	let tmpr = sum real i'
	and tmpi = sum imag i' in
	if (i < n - i) then 
	  plus [tmpr; tmpi]
	else
	  plus [tmpr; uminus tmpi]) in
  if (n >= !Magic.rader_min) then
    fftgen_rader n input sign
  else if (n == 2) then
    computation_even
  else
    computation_odd 


and fftgen_rader p input sign =

  (* generates a convolution using ffts.  (all arguments are the
     same as to gen_convolution, below) *)
  let gen_convolution_by_fft n a b addtoall =
    let fft_a = fftgen n no_sym a 1
    and fft_b = fftgen n no_sym b 1
    and scale_factor = inverse_int n in
    let fft_ab = freeze n (fun i ->
	let ab = (times (fft_a i) 
	 	  (times (fft_b i) scale_factor))
	in if (i == 0) then
	  plus [addtoall; ab]
	else
	  ab)
    and sum = (fft_a 0) in
    let conv = fftgen n no_sym fft_ab (-1) in
    (sum, conv)

    (* generator of assignment list assigning conv to the convolution of
       a and b, all of which are of length n.  addtoall is added to
       all of the elements of the result.  Returns (sum, convolution) pair
       where sum is the sum of the elements of a. *)

  in let gen_convolution n a b addtoall = 
    gen_convolution_by_fft n a b addtoall


  (* fft generator for prime n = p using Rader's algorithm for
     turning the fft into a convolution, which then can be
     performed in a variety of ways *)
  in  
    let g = find_generator p in
    let ginv = pow_mod g (p - 2) p in
    let input_perm i = input (pow_mod g i p) 
    and omega_perm i = exp p (sign * (pow_mod ginv i p))
    and output_perm i = pow_mod ginv i p
    in let (sum, conv) = 
      (gen_convolution (p - 1) input_perm omega_perm (input 0))
    in freeze p (fun i ->
      if (i = 0) then
	plus [input 0; sum]
      else
	let i' = suchthat 0 (fun i' -> i = output_perm i')
	in conv i')

and fftgen n sym =
  let rec cooley_tukey n1 n2 input sign =
    let tmp1 = freeze n2 (fun i2 ->
      fftgen n1 sym.isym1 (fun i1 -> input (i1 * n2 + i2)) sign) in
    let tmp2 =  freeze n1 (fun i1 ->
      freeze n2 (fun i2 ->
	(times (exp n (sign * i1 * i2)) (tmp1 i2 i1)))) in
    let tmp3 = freeze n1 (fun i1 ->
      fftgen n2 sym.isym2 (tmp2 i1) sign) in
    (fun i -> tmp3 (i mod n1) (i / n1))

  (* This is "exponent -1" split-radix by Dan Bernstein *)
  and split_radix_dit n input sign =
    let f0 = fftgen (n / 2) sym.isym1 (fun i -> input (i * 2)) sign
    and f10 = fftgen (n / 4) sym.isym1 (fun i -> input (i * 4 + 1)) sign
    and f11 = fftgen (n / 4) sym.isym1 (fun i -> input 
	((n + i * 4 - 1) mod n)) sign in
    let g10 = freeze n (fun k ->
      times (exp n (sign * k)) (f10 (k mod (n / 4))))
    and g11 = freeze n (fun k ->
      times (exp n (- sign * k)) (f11 (k mod (n / 4)))) in
    let g1 = freeze n (fun k -> plus [g10 k; g11 k]) in
    freeze n (fun k -> plus [f0 (k mod (n / 2)); g1 k])

  and split_radix_dif n input sign =
    let n2 = n / 2 and n4 = n / 4 in
    let x0 = freeze n2 (fun i -> plus [input i; input (i + n2)])
    and x10 = freeze n4 (fun i -> plus [input i; uminus (input (i + n2))])
    and x11 = freeze n4 (fun i ->
	(plus [input (i + n4); uminus (input (i + n2 + n4))])) in
    let x1 k i = 
      times (exp n (k * i * sign)) 
	(plus [x10 i; times (exp 4 (k * sign)) (x11 i)]) in
    let f0 = fftgen n2 sym.isym2 x0 sign 
    and f1 = freeze 4 (fun k -> fftgen n4 sym.isym2 (x1 k) sign) in
    freeze n (fun k ->
      if k mod 2 = 0 then f0 (k / 2)
      else let k' = k mod 4 in f1 k' ((k - k') / 4))

  and prime_factor n1 n2 input sign =
    let tmp1 = freeze n2 (fun i2 ->
      fftgen n1 sym.isym1 (fun i1 -> input ((i1 * n2 + i2 * n1) mod n)) sign)
    in let tmp2 = freeze n1 (fun i1 ->
      fftgen n2 sym.isym2 (fun k2 -> tmp1 k2 i1) sign)
    in fun i -> tmp2 (i mod n1) (i mod n2)

  in let r = factor n 
  in let fft =
    if (r == 1) then  (* n is prime *)
      fftgen_prime n
    else if (gcd r (n / r)) == 1 then
      prime_factor r (n / r)
    else if (n mod 4 = 0 && n > 4) then
      (if sym == hermitian_sym then
	split_radix_dif n
      else
	split_radix_dit n)
    else 
      cooley_tukey r (n / r)
  in fun input sign ->
    freeze n (sym.osym.apply n (fft (sym.apply n input) sign))

type direction = FORWARD | BACKWARD

let sign_of_dir = function
    FORWARD -> (-1)
  | BACKWARD -> 1

let dagify n sym f =
  let a = Array.init n (sym.osym.store n f)
  in Exprdag.make (List.flatten (Array.to_list a))
  
let no_twiddle_gen_expr n sym dir =
  let sign = sign_of_dir dir in
  let fft = fftgen n sym (fun i -> (load_var (access_input i))) sign
  in dagify n sym fft

let square x = 
  if (!Magic.use_wsquare) then
    wsquare x
  else
    times x x

(* various policies for computing/loading twiddle factors *)
(* load all twiddle factors *)
let twiddle_policy_load_all =
  let twiddle_expression n i _ =
    load_var (access_twiddle (i - 1))
  and num_twiddle n = (n - 1)
  and twiddle_order n = forall_flat 1 n (fun i -> [i])
  in twiddle_expression, num_twiddle, twiddle_order

(*
 * if n is even, compute w^n = (w^{n/2})^2, else
 * load it
 *)
let twiddle_policy_load_odd =
  let twiddle_expression n i twiddles =
    if ((i mod 2) == 0) then
      square (twiddles (i / 2))
    else load_var (access_twiddle ((i - 1) / 2))
  and num_twiddle n = (n / 2)
  and twiddle_order n = forall_flat 1 n (fun i -> 
    if ((i mod 2) == 1) then [i] else [])
  in twiddle_expression, num_twiddle, twiddle_order

(* compute w^n = w w^{n-1} *)
let twiddle_policy_iter =
  let twiddle_expression n i twiddles =
    if (i == 1) then load_var (access_twiddle (i - 1))
    else times (twiddles (i - 1)) (twiddles 1)
  and num_twiddle n = 1
  and twiddle_order n = [1]
  in twiddle_expression, num_twiddle, twiddle_order

(*
 * if n is even, compute w^n = (w^{n/2})^2, else
 *  w^n = w w^{n-1}
 *)
let twiddle_policy_square1 =
  let twiddle_expression n i twiddles =
    if (i == 1) then load_var (access_twiddle (i - 1))
    else if ((i mod 2) == 0) then
      square (twiddles (i / 2))
    else times (twiddles (i - 1)) (twiddles 1)
  and num_twiddle n = 1
  and twiddle_order n = [1]
  in twiddle_expression, num_twiddle, twiddle_order

(*
 * if n is even, compute w^n = (w^{n/2})^2, else
 * compute  w^n from w^{n-1}, w^{n-2}, and w
 *)
let twiddle_policy_square2 =
  let twiddle_expression n i twiddles =
    if (i == 1) then load_var (access_twiddle (i - 1))
    else if ((i mod 2) == 0) then
      square (twiddles (i / 2))
    else 
      wthree (twiddles (i - 1)) (twiddles (i - 2)) (twiddles 1)
  and num_twiddle n = 1
  and twiddle_order n = [1]
  in twiddle_expression, num_twiddle, twiddle_order

(*
 * if n is even, compute w^n = (w^{n/2})^2, else
 *  w^n = w^{floor(n/2)} w^{ceil(n/2)}
 *)
let twiddle_policy_square3 =
  let twiddle_expression n i twiddles =
    if (i == 1) then load_var (access_twiddle (i - 1))
    else if ((i mod 2) == 0) then
      square (twiddles (i / 2))
    else times (twiddles (i / 2)) (twiddles (i - i / 2))
  and num_twiddle n = 1
  and twiddle_order n = [1]
  in twiddle_expression, num_twiddle, twiddle_order

let twiddle_policy () = 
  match !Magic.twiddle_policy with
    Magic.TWIDDLE_LOAD_ALL -> twiddle_policy_load_all
  | Magic.TWIDDLE_ITER -> twiddle_policy_iter
  | Magic.TWIDDLE_LOAD_ODD -> twiddle_policy_load_odd
  | Magic.TWIDDLE_SQUARE1 -> twiddle_policy_square1
  | Magic.TWIDDLE_SQUARE2 -> twiddle_policy_square2
  | Magic.TWIDDLE_SQUARE3 -> twiddle_policy_square3

let twiddle_gen_expr n dir =
  let sign = sign_of_dir dir in
  let conj_maybe = match dir with
    FORWARD -> identity
  | BACKWARD -> conj in

  let loaded_input = freeze n (fun i -> (load_var (access_input i))) in

  (* `let rec f i = g i' instead of `let rec f = g' is a way to
     get around ocaml's limitations in handling let rec *)
  let rec twiddle_expression i =
    freeze n 
      (fun i ->
	(let (t, _, _) = twiddle_policy () in t n i twiddle_expression))
      i in

  let input_by_twiddle = 
    freeze n (fun i ->
      if (i == 0) then
	loaded_input 0
      else
	(times (conj_maybe (twiddle_expression i))
	   (loaded_input i)))

  in let fft = fftgen n no_sym input_by_twiddle sign
  in dagify n no_sym fft
