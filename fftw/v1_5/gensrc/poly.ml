(*
 * Copyright (c) 1997,1998 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *)

(* Routines for doing multiplication, division, remainder, and other
   operations on polynomials whose coefficients are complex
   expressions.  Implements Chinese Remainder Theorem for polynomials. *)

(* a polynomial is represented an an array c of expressions, where
   the polynomial is c.(0) + c.(1) * z + ... + c.(degree) * z ^ (degree) *)

open Array
open Complex

(* given a function f where (f i) is a complex variable, express it as
   a polynomial of degree n - 1 *)
let poly_of_cvar_func f n = init n (fun i -> expr_of_var (f i))

let degree poly = ((length poly) - 1)

let fix_degree poly =
    let real_deg = 
	Util.max_list (to_list (mapi (fun i c -> if (is_zero c) then 0 else i)
				poly))
    in sub poly 0 (real_deg + 1)

let simplify_poly poly =
    fix_degree (map (fun c -> simplify c) poly)

let make_monomial deg coef =
    let result = Array.make (deg + 1) zero in begin
      result.(deg) <- coef;
      result
    end

let poly_one = make_monomial 0 one
let poly_zero = make_monomial 0 zero

let add_poly_nosimplify p1 p2 =
    let (pa,pb) = if ((degree p1) > (degree p2)) then (p1,p2) else (p2,p1)
    in mapi (fun i c ->
        if (i > (degree pb)) then c
        else plus [c; pb.(i)]) pa

let add_poly p1 p2 = simplify_poly (add_poly_nosimplify p1 p2)

let add_polys polys = 
    simplify_poly (List.fold_left add_poly_nosimplify poly_zero polys)

let mult_poly p1 p2 =
    let result = Array.make (degree p1 + degree p2 + 1) zero
    in begin
        for i = 0 to degree p1 do
          for j = 0 to degree p2 do
            result.(i+j) <- plus [result.(i+j); times p1.(i) p2.(j)]
          done;
        done;
      simplify_poly result
   end

let scale_poly poly s = simplify_poly (map (fun c -> times s c) poly)

let negate_poly poly = simplify_poly (map (fun c -> uminus c) poly)

let rec divide_poly p_num p_den =
    if (degree p_num < degree p_den) then
      (poly_zero, p_num)
    else if (degree p_den == 0) then
      (scale_poly p_num (div_num one p_den.(0)), poly_zero)
    else
      let q = make_monomial ((degree p_num) - (degree p_den))
          (div_num p_num.(degree p_num) p_den.(degree p_den))
      in let r = add_poly p_num (mult_poly p_den (negate_poly q))
      in let rr =
	  if ((degree r) == (degree p_num)) then
	    fix_degree (sub r 0 (degree p_num))
	  else r
      in let (q2,r2) = divide_poly rr p_den
      in (add_poly q q2, r2)

let mod_poly p p_mod = let (q,r) = divide_poly p p_mod in r

exception Polynomials_Not_Coprime

(* invert_poly_int, if given polynomials with integer coefficients,
   will return a tuple (p_inv,scale) where p_inv*scale is the inverse
   of p mod p_mod and p_inv has integer coefficients. (Will still
   invert polynomials with non-integer coefficients, but p_inv
   will not have integer coefficients.) *)
let invert_poly_int p p_mod =
    (* using gcd thm., find x,y,d where x*a + y*b = d = gcd(a,b) *)
    let rec gcd_thm a b =
        let (q,r) = divide_poly a b in
            if (degree r == 0) then
              begin
                if (is_zero r.(0)) then raise Polynomials_Not_Coprime
                else (poly_one, negate_poly q, r.(0))
              end
            else let (x,y,d) = gcd_thm b r in
		(y,add_poly x (mult_poly (negate_poly y) q),d)
    in let (x,y,d) = gcd_thm p p_mod in
        (x,simplify (div_num one d))

let invert_poly p p_mod =
    let (p_inv,scale) = invert_poly_int p p_mod in scale_poly p_inv scale

(* Find coefficient polynomials for the Chinese Remainder Theorem (CRT).

   Given a list of coprime polynomials p[i], this function returns the
   pair of lists (scale[i], coef[i]) that solve the following problem:

   You want to find the unknown polynomial x mod p (p = product p[i])
   that satisfies x mod p[i] = a[i], for p[i] coprime. The solution is
   given by the sum over i of crt[i]*a[i], where crt[i] is
   scale[i]*coef[i].

   Note that scale[i] is a complex expression and coef[i] is a
   polynomial.  (We return scale[i] and coef[i] instead of scale[i]
   * coef[i] because the former allows us, sometimes, to factor
   a common multiplication our of our calculations.) *)

let find_CRT_coefs polys =
    let poly_prod plist = List.fold_left mult_poly poly_one plist

    (* Given a list of polynomials p[i], produce a list of polynomials
       M[i] where M[i] = product of p[j] for j != i. *)
    in let find_partial_products plist =
    	let rec f_p_p_aux prod_before = function
               | [] -> []
               | poly :: rst -> (mult_poly prod_before (poly_prod rst)) ::
		 (f_p_p_aux (mult_poly prod_before poly) rst)
	in f_p_p_aux poly_one plist

    (* Now, find crt[i] = scale[i] * coef[i].  b[i] is equal to
       M[i] * N[i] mod p, where N[i] is the inverse of M[i] mod p[i]. *)

    in let part_products = find_partial_products polys
    and product = poly_prod polys
    in let p_p_inv_int = List.map2 invert_poly_int part_products polys
    in let (p_p_inv, p_p_inv_scale) = List.split p_p_inv_int
    in let coefs = List.map2 mult_poly part_products p_p_inv
    in
        (p_p_inv_scale, 
	 List.map (function poly -> mod_poly poly product) coefs)


(* a binomial is something of the form z^n + a.  The following functions
   make and test for binomials: *)

let make_binomial deg const_term =
    let result = make_monomial deg one
    in begin
      result.(0) <- const_term;
      result
    end

