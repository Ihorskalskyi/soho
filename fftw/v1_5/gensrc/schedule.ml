(*
 * Copyright (c) 1997,1998 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *)

(* $Id: schedule.ml,v 1.1 1998/06/23 21:56:35 dcschmid Exp $ *)

(* This file contains the instruction scheduler, which finds an
   efficient ordering for a given list of instructions.

   The scheduler analyzes the DAG (Directed, Acyclic Graph) formed by
   the instruction dependencies, and recursively partitions it.  The
   resulting schedule data structure expresses a "good" ordering
   and structure for the computation.

   The scheduler makes use of utilties in Dag and other packages to
   manipulate the Dag and the instruction list. *)

open Dag
(*************************************************
 *               Dag scheduler
 *************************************************)
let to_assignment node = (Expr.Assign (node.assigned, node.expression))
let makedag l = Dag.makedag 
    (List.map (function Expr.Assign (v, x) -> (v, x)) l)

let return x = x
let has_color c n = (n.color = c)

let infinity = 100000 

let cc dag inputs =
  begin
    Dag.for_all dag (fun node -> 
      node.label <- infinity);
    
    (match inputs with 
      a :: _ -> bfs dag a 0
    | _ -> failwith "connected");

    return
      ((List.map to_assignment (Util.filter (fun n -> n.label < infinity)
				  (Dag.to_list dag))),
       (List.map to_assignment (Util.filter (fun n -> n.label == infinity) 
				  (Dag.to_list dag))))
  end

let rec connected_components alist =
  let dag = makedag alist in
  let inputs = 
    Util.filter (fun node -> Util.null node.predecessors) 
      (Dag.to_list dag) in
  match cc dag inputs with
    (a, []) -> [a]
  | (a, b) -> a :: connected_components b

let partition alist =
  let dag = makedag alist in
  let inputs = 
    Util.filter (fun node -> Util.null node.predecessors) 
      (Dag.to_list dag) 
  and outputs = 
    Util.filter (fun node -> Util.null node.successors) 
      (Dag.to_list dag) 
  and dag' = Dag.to_list dag in
  begin
    Dag.for_all dag (fun node -> 
      begin
      	node.color <- BLACK;
      end);

    Util.for_list inputs (fun node -> 
      	node.color <- RED);

    Util.for_list outputs (fun node -> 
      	node.color <- BLUE);

    let rec loopi donep = 
      match (Util.filter
	       (fun node -> node.color == BLACK &&
		 List.for_all (has_color RED) node.predecessors)
	       dag') with
	[] -> if (donep) then () else loopo true
      |	i -> begin
	  Util.for_list i (fun node -> 
      	    node.color <- RED);
	  loopo false; 
	end

    and loopo donep =
      match (Util.filter
	       (fun node -> node.color == BLACK &&
		 List.for_all (has_color BLUE) node.successors)
	       dag') with
	[] -> if (donep) then () else loopi true
      |	o -> begin
	  Util.for_list o (fun node -> 
      	    node.color <- BLUE);
	  loopi false; 
      end

    (* among the magic parameters, this is the most obscure *)
    in if !Magic.loopo then 
      loopo false
    else
      loopi false;

    return
      ((List.map to_assignment (Util.filter (has_color RED)
				  (Dag.to_list dag))),
       (List.map to_assignment (Util.filter (has_color BLUE)
				  (Dag.to_list dag))))
  end

type schedule = 
    Done
  | Instr of Expr.assignment
  | Seq of (schedule * schedule)
  | Par of schedule list


let schedule =
  let rec schedule_alist = function
      [] -> Done
    | [a] -> Instr a
    | alist -> 
	match connected_components alist with
	  ([a]) -> schedule_connected a
	| l -> Par (List.map schedule_alist l)

  and schedule_connected alist = 
    match partition alist with
    | (a, b) -> Seq (schedule_alist a, schedule_alist b)

  in schedule_alist
