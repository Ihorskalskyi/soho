(*
 * Copyright (c) 1997,1998 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *)

(* $Id: variable.ml,v 1.1 1998/06/23 21:56:38 dcschmid Exp $ *)

(* Data types and functions for dealing with variables in symbolic
 * expressions and the abstract syntax tree. *)

(* Variables fall into one of four categories: temporary variables
 * (which the generator can add or delete at will), named (fixed)
 * variables, and the real/imaginary parts of complex arrays.  Arrays
 * can be either input arrays, output arrays, or arrays of precomputed
 * twiddle factors (roots of unity). *)

type array = 
    Input
  | Output
  | Twiddle

type variable =
    Temporary of int
  | Named of string
  | RealArrayElem of (array * int)
  | ImagArrayElem of (array * int)
  | Dummy

let make_temporary =
  let tmp_count = ref 0
  in fun () -> begin
    tmp_count := !tmp_count + 1;
    Temporary !tmp_count
  end

let make_dummy () = Dummy

let is_dummy = function
    Dummy -> true
  | _ -> false

let is_temporary = function
    Temporary _ -> true
  | _ -> false

let is_output = function
    RealArrayElem (Output, _) -> true
  | ImagArrayElem (Output, _) -> true
  | _ -> false

let is_input = function
    RealArrayElem (Input, _) -> true
  | ImagArrayElem (Input, _) -> true
  | _ -> false

let same = (=)

let similar a b = 
  same a b or
  (match (a, b) with
    (RealArrayElem (a1, k1), ImagArrayElem (a2, k2)) -> 
      a1 = a2 && k1 = k2
  | (ImagArrayElem (a1, k1), RealArrayElem (a2, k2)) -> 
      a1 = a2 && k1 = k2
  | _ -> false)
    
(* true if assignment of a clobbers variable b *)
let clobbers a b =
  match (a, b) with
    (RealArrayElem (Output, k1), RealArrayElem (Input, k2)) -> k1 = k2
  | (ImagArrayElem (Output, k1), ImagArrayElem (Input, k2)) -> k1 = k2
  | _ -> false

(* true if a is the real part and b the imaginary of the same array *)
let real_imag a b =
  match (a, b) with
    (RealArrayElem (a1, k1), ImagArrayElem (a2, k2)) -> 
      a1 = a2 && k1 = k2 
  | _ -> false

(* true if a and b are elements of the same array, and a has smaller index *)
let increasing_indices a b =
  match (a, b) with
    (RealArrayElem (a1, k1), RealArrayElem (a2, k2)) -> 
      a1 = a2 && k1 < k2 
  | (RealArrayElem (a1, k1), ImagArrayElem (a2, k2)) -> 
      a1 = a2 && k1 < k2 
  | (ImagArrayElem (a1, k1), RealArrayElem (a2, k2)) -> 
      a1 = a2 && k1 < k2 
  | (ImagArrayElem (a1, k1), ImagArrayElem (a2, k2)) -> 
      a1 = a2 && k1 < k2 
  | _ -> false

let access array k =
  (RealArrayElem (array, k),  ImagArrayElem (array, k))

let access_input = access Input
let access_output = access Output
let access_twiddle = access Twiddle

let make_named name = Named name

let make_unparser (input_name, input_stride)
    (output_name, output_stride)
    (twiddle_name, twiddle_stride) =
  let rec unparse_var = function
      Temporary k -> "tmp" ^ (string_of_int k)
    | Named s -> s
    | RealArrayElem (array, k) ->
        "c_re(" ^ (unparse_array array k) ^ ")"
    | ImagArrayElem (array, k) ->
        "c_im(" ^ (unparse_array array k) ^ ")"
    | Dummy -> failwith "attempt to unparse dummy variable"

  and unparse_array = function
      Input -> unparse_aux input_name input_stride
    | Output -> unparse_aux output_name output_stride
    | Twiddle -> unparse_aux twiddle_name twiddle_stride

  and unparse_aux name stride k = 
    let index = 
      match (stride, k) with
	(_, 0) -> "0"
      | (Some s, 1) -> s
      | (None, k) -> (string_of_int k)
      | (Some s, k) -> (string_of_int k) ^ " * " ^ s 
    in name ^ "[" ^ index ^ "]"

  in unparse_var

    
    




