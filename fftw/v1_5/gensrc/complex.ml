(*
 * Copyright (c) 1997,1998 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *)

(* $Id: complex.ml,v 1.1 1998/06/23 21:56:24 dcschmid Exp $ *)

(* abstraction layer for complex operations *)

(* type of complex expressions *)
open Exprdag

type expr = CE of node * node

let one = CE (Num Number.one, Num Number.zero)
let zero = CE (Num Number.zero, Num Number.zero)

let inverse_int n = CE (Num (Number.div Number.one 
			       (Number.of_int n)),
			Num Number.zero)

let times (CE (a, b)) (CE (c, d)) = 
  CE (Plus [Times (a, c); Uminus (Times (b, d))],
      Plus [Times (a, d); Times (b, c)])


let uminus (CE (a, b)) =  CE (Uminus a, Uminus b)

(* complex exponential (of root of unity); returns exp(2*pi*i/n * m) *)
let exp n i =
  let (c, s) = Number.cexp n i
  in CE (Num c, Num s)
    
(* complex sum *)
let plus a =
  let rec unzip_complex = function
      [] -> ([], [])
    | ((CE (a, b)) :: s) ->
        let (r,i) = unzip_complex s
	in
	(a::r), (b::i) in
  let (c, d) = unzip_complex a in
  CE (Plus c, Plus d)

(* extract real/imaginary *)
let real (CE (a, b)) = CE (a, Num Number.zero)
let imag (CE (a, b)) = CE (Num Number.zero, b)
let conj (CE (a, b)) = CE (a, Uminus b)
    
let abs_sqr (CE (a, b)) = Plus [Times (a, a);
                                 Times (b, b)]

(*
  * special cases for complex numbers w where |w| = 1 
  *)
(* (a + bi)^2 = (2a^2 - 1) + 2abi *)
let wsquare (CE (a, b)) =
  let twoa = Times (Num Number.two, a)
  in let twoasq = Times (twoa, a)
  and twoab = Times (twoa, b) in
  CE (Plus [twoasq; Uminus (Num Number.one)], twoab)

(*
  * compute w^n given w^{n-1}, w^{n-2}, and w, using the identity 
  * 
  * w^n + w^{n-2} = w^{n-1} (w + w^{-1}) = 2 w^{n-1} Re(w)
  *)
let wthree (CE (an1, bn1)) wn2 (CE (a, b)) =
  let twoa = Times (Num Number.two, a)
  in let twoa_wn1 = CE (Times (twoa, an1), 
			Times (twoa, bn1))
  in plus [twoa_wn1; (uminus wn2)]

(* abstraction of sum_{i=0}^{n-1} *)
let sigma a b f = plus (Util.forall_flat a b f)

(* complex variables *)
type variable = CV of Variable.variable * Variable.variable

let load_var (CV (vr, vi)) = 
  CE (Load vr, Load vi)

let store_var (CV (vr, vi)) (CE (xr, xi))  = 
  [Store (vr, xr); Store (vi, xi)]

let store_real (CV (vr, vi)) (CE (xr, xi))  = 
  [Store (vr, xr)]

let access what k =
  let (r, i) = what k
  in CV (r, i)

let access_input = access Variable.access_input
let access_output = access Variable.access_output
let access_twiddle = access Variable.access_twiddle
