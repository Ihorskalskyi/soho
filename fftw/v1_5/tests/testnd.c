/*
 * Copyright (c) 1997 Massachusetts Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to use, copy, modify, and distribute the Software without
 * restriction, provided the Software, including any modified copies made
 * under this license, is not distributed for a fee, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE MASSACHUSETTS INSTITUTE OF TECHNOLOGY BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name of the Massachusetts
 * Institute of Technology shall not be used in advertising or otherwise
 * to promote the sale, use or other dealings in this Software without
 * prior written authorization from the Massachusetts Institute of
 * Technology.
 *
 */

/* $Id: testnd.c,v 1.1.1.1 1997/10/21 17:06:47 stoughto Exp $ */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <fftw.h>

/* Maximum array sizes in test gets multiplied by this.  Change it
   to a bigger number (e.g. 10) if you want to run bigger tests
   and have enough memory. */
#define SIZE_MULT 2

#ifndef RAND_MAX
#define RAND_MAX 32767
#endif

#define NUM_ITER 20

#define USE_RANDOM  0 /* use superior random() function instead of rand() */

#if USE_RANDOM
     long random(void);
     void srandom(unsigned int seed);

#define rand random
#define srand srandom
#define RANDOM_MAX 2147483647
#else
#define RANDOM_MAX RAND_MAX
#endif

void test_fftwnd_strided(int rank, int istride, int ostride, long max_size,
		     long num_iters, short in_place, fftw_direction dir);
int go_fft(FFTW_REAL *a, FFTW_REAL *b, long ntot, long n,
	   long nspan, long isn);

void main(void)
{
    srand(clock());

    /* Test forward transforms: */

    test_fftwnd_strided(1, 1, 1, 64 * SIZE_MULT, NUM_ITER, 0, FFTW_FORWARD);
    test_fftwnd_strided(1, 1, 1, 64 * SIZE_MULT, NUM_ITER, 1, FFTW_FORWARD);
    test_fftwnd_strided(2, 1, 1, 32 * SIZE_MULT, NUM_ITER, 0, FFTW_FORWARD);
    test_fftwnd_strided(2, 1, 1, 32 * SIZE_MULT, NUM_ITER, 1, FFTW_FORWARD);
    test_fftwnd_strided(3, 1, 1, 10 * SIZE_MULT, NUM_ITER, 0, FFTW_FORWARD);
    test_fftwnd_strided(3, 1, 1, 10 * SIZE_MULT, NUM_ITER, 1, FFTW_FORWARD);
    test_fftwnd_strided(4, 1, 1, 4 * SIZE_MULT, NUM_ITER, 0, FFTW_FORWARD);
    test_fftwnd_strided(4, 1, 1, 4 * SIZE_MULT, NUM_ITER, 1, FFTW_FORWARD);
    test_fftwnd_strided(5, 1, 1, 2 * SIZE_MULT, NUM_ITER, 0, FFTW_FORWARD);
    test_fftwnd_strided(5, 1, 1, 2 * SIZE_MULT, NUM_ITER, 1, FFTW_FORWARD);

    test_fftwnd_strided(1, 2, 3, 64 * SIZE_MULT, NUM_ITER, 0, FFTW_FORWARD);
    test_fftwnd_strided(1, 2, 3, 64 * SIZE_MULT, NUM_ITER, 1, FFTW_FORWARD);
    test_fftwnd_strided(2, 2, 3, 32 * SIZE_MULT, NUM_ITER, 0, FFTW_FORWARD);
    test_fftwnd_strided(2, 2, 3, 32 * SIZE_MULT, NUM_ITER, 1, FFTW_FORWARD);
    test_fftwnd_strided(3, 2, 3, 10 * SIZE_MULT, NUM_ITER, 0, FFTW_FORWARD);
    test_fftwnd_strided(3, 2, 3, 10 * SIZE_MULT, NUM_ITER, 1, FFTW_FORWARD);
    test_fftwnd_strided(4, 2, 3, 4 * SIZE_MULT, NUM_ITER, 0, FFTW_FORWARD);
    test_fftwnd_strided(4, 2, 3, 4 * SIZE_MULT, NUM_ITER, 1, FFTW_FORWARD);
    test_fftwnd_strided(5, 2, 3, 2 * SIZE_MULT, NUM_ITER, 0, FFTW_FORWARD);
    test_fftwnd_strided(5, 2, 3, 2 * SIZE_MULT, NUM_ITER, 1, FFTW_FORWARD);

    test_fftwnd_strided(1, 3, 2, 64 * SIZE_MULT, NUM_ITER, 0, FFTW_FORWARD);
    test_fftwnd_strided(1, 3, 2, 64 * SIZE_MULT, NUM_ITER, 1, FFTW_FORWARD);
    test_fftwnd_strided(2, 3, 2, 32 * SIZE_MULT, NUM_ITER, 0, FFTW_FORWARD);
    test_fftwnd_strided(2, 3, 2, 32 * SIZE_MULT, NUM_ITER, 1, FFTW_FORWARD);
    test_fftwnd_strided(3, 3, 2, 10 * SIZE_MULT, NUM_ITER, 0, FFTW_FORWARD);
    test_fftwnd_strided(3, 3, 2, 10 * SIZE_MULT, NUM_ITER, 1, FFTW_FORWARD);
    test_fftwnd_strided(4, 3, 2, 4 * SIZE_MULT, NUM_ITER, 0, FFTW_FORWARD);
    test_fftwnd_strided(4, 3, 2, 4 * SIZE_MULT, NUM_ITER, 1, FFTW_FORWARD);
    test_fftwnd_strided(5, 3, 2, 2 * SIZE_MULT, NUM_ITER, 0, FFTW_FORWARD);
    test_fftwnd_strided(5, 3, 2, 2 * SIZE_MULT, NUM_ITER, 1, FFTW_FORWARD);

    test_fftwnd_strided(1, 3, 3, 64 * SIZE_MULT, NUM_ITER, 0, FFTW_FORWARD);
    test_fftwnd_strided(1, 3, 3, 64 * SIZE_MULT, NUM_ITER, 1, FFTW_FORWARD);
    test_fftwnd_strided(2, 3, 3, 32 * SIZE_MULT, NUM_ITER, 0, FFTW_FORWARD);
    test_fftwnd_strided(2, 3, 3, 32 * SIZE_MULT, NUM_ITER, 1, FFTW_FORWARD);
    test_fftwnd_strided(3, 3, 3, 10 * SIZE_MULT, NUM_ITER, 0, FFTW_FORWARD);
    test_fftwnd_strided(3, 3, 3, 10 * SIZE_MULT, NUM_ITER, 1, FFTW_FORWARD);
    test_fftwnd_strided(4, 3, 3, 4 * SIZE_MULT, NUM_ITER, 0, FFTW_FORWARD);
    test_fftwnd_strided(4, 3, 3, 4 * SIZE_MULT, NUM_ITER, 1, FFTW_FORWARD);
    test_fftwnd_strided(5, 3, 3, 2 * SIZE_MULT, NUM_ITER, 0, FFTW_FORWARD);
    test_fftwnd_strided(5, 3, 3, 2 * SIZE_MULT, NUM_ITER, 1, FFTW_FORWARD);

    /* Test backward transforms */

    test_fftwnd_strided(1, 1, 1, 64 * SIZE_MULT, NUM_ITER, 0, FFTW_BACKWARD);
    test_fftwnd_strided(1, 1, 1, 64 * SIZE_MULT, NUM_ITER, 1, FFTW_BACKWARD);
    test_fftwnd_strided(2, 1, 1, 32 * SIZE_MULT, NUM_ITER, 0, FFTW_BACKWARD);
    test_fftwnd_strided(2, 1, 1, 32 * SIZE_MULT, NUM_ITER, 1, FFTW_BACKWARD);
    test_fftwnd_strided(3, 1, 1, 10 * SIZE_MULT, NUM_ITER, 0, FFTW_BACKWARD);
    test_fftwnd_strided(3, 1, 1, 10 * SIZE_MULT, NUM_ITER, 1, FFTW_BACKWARD);
    test_fftwnd_strided(4, 1, 1, 4 * SIZE_MULT, NUM_ITER, 0, FFTW_BACKWARD);
    test_fftwnd_strided(4, 1, 1, 4 * SIZE_MULT, NUM_ITER, 1, FFTW_BACKWARD);
    test_fftwnd_strided(5, 1, 1, 2 * SIZE_MULT, NUM_ITER, 0, FFTW_BACKWARD);
    test_fftwnd_strided(5, 1, 1, 2 * SIZE_MULT, NUM_ITER, 1, FFTW_BACKWARD);

    test_fftwnd_strided(1, 2, 3, 64 * SIZE_MULT, NUM_ITER, 0, FFTW_BACKWARD);
    test_fftwnd_strided(1, 2, 3, 64 * SIZE_MULT, NUM_ITER, 1, FFTW_BACKWARD);
    test_fftwnd_strided(2, 2, 3, 32 * SIZE_MULT, NUM_ITER, 0, FFTW_BACKWARD);
    test_fftwnd_strided(2, 2, 3, 32 * SIZE_MULT, NUM_ITER, 1, FFTW_BACKWARD);
    test_fftwnd_strided(3, 2, 3, 10 * SIZE_MULT, NUM_ITER, 0, FFTW_BACKWARD);
    test_fftwnd_strided(3, 2, 3, 10 * SIZE_MULT, NUM_ITER, 1, FFTW_BACKWARD);
    test_fftwnd_strided(4, 2, 3, 4 * SIZE_MULT, NUM_ITER, 0, FFTW_BACKWARD);
    test_fftwnd_strided(4, 2, 3, 4 * SIZE_MULT, NUM_ITER, 1, FFTW_BACKWARD);
    test_fftwnd_strided(5, 2, 3, 2 * SIZE_MULT, NUM_ITER, 0, FFTW_BACKWARD);
    test_fftwnd_strided(5, 2, 3, 2 * SIZE_MULT, NUM_ITER, 1, FFTW_BACKWARD);

    test_fftwnd_strided(1, 3, 2, 64 * SIZE_MULT, NUM_ITER, 0, FFTW_BACKWARD);
    test_fftwnd_strided(1, 3, 2, 64 * SIZE_MULT, NUM_ITER, 1, FFTW_BACKWARD);
    test_fftwnd_strided(2, 3, 2, 32 * SIZE_MULT, NUM_ITER, 0, FFTW_BACKWARD);
    test_fftwnd_strided(2, 3, 2, 32 * SIZE_MULT, NUM_ITER, 1, FFTW_BACKWARD);
    test_fftwnd_strided(3, 3, 2, 10 * SIZE_MULT, NUM_ITER, 0, FFTW_BACKWARD);
    test_fftwnd_strided(3, 3, 2, 10 * SIZE_MULT, NUM_ITER, 1, FFTW_BACKWARD);
    test_fftwnd_strided(4, 3, 2, 4 * SIZE_MULT, NUM_ITER, 0, FFTW_BACKWARD);
    test_fftwnd_strided(4, 3, 2, 4 * SIZE_MULT, NUM_ITER, 1, FFTW_BACKWARD);
    test_fftwnd_strided(5, 3, 2, 2 * SIZE_MULT, NUM_ITER, 0, FFTW_BACKWARD);
    test_fftwnd_strided(5, 3, 2, 2 * SIZE_MULT, NUM_ITER, 1, FFTW_BACKWARD);

    test_fftwnd_strided(1, 3, 3, 64 * SIZE_MULT, NUM_ITER, 0, FFTW_BACKWARD);
    test_fftwnd_strided(1, 3, 3, 64 * SIZE_MULT, NUM_ITER, 1, FFTW_BACKWARD);
    test_fftwnd_strided(2, 3, 3, 32 * SIZE_MULT, NUM_ITER, 0, FFTW_BACKWARD);
    test_fftwnd_strided(2, 3, 3, 32 * SIZE_MULT, NUM_ITER, 1, FFTW_BACKWARD);
    test_fftwnd_strided(3, 3, 3, 10 * SIZE_MULT, NUM_ITER, 0, FFTW_BACKWARD);
    test_fftwnd_strided(3, 3, 3, 10 * SIZE_MULT, NUM_ITER, 1, FFTW_BACKWARD);
    test_fftwnd_strided(4, 3, 3, 4 * SIZE_MULT, NUM_ITER, 0, FFTW_BACKWARD);
    test_fftwnd_strided(4, 3, 3, 4 * SIZE_MULT, NUM_ITER, 1, FFTW_BACKWARD);
    test_fftwnd_strided(5, 3, 3, 2 * SIZE_MULT, NUM_ITER, 0, FFTW_BACKWARD);
    test_fftwnd_strided(5, 3, 3, 2 * SIZE_MULT, NUM_ITER, 1, FFTW_BACKWARD);
}

#define MAX_RANK 10

#define CLOSE(a,b) (fabs((a) - (b))*2.0/(fabs(a)+fabs(b)+0.1) < 1e-3)

int factors_ok(long num);

void test_fftwnd_strided(int rank, int istride, int ostride, long max_size,
		      long num_iters, short in_place, fftw_direction dir)
{
    int dims[MAX_RANK];
    long dims_reverse[MAX_RANK];
    FFTW_REAL *a1, *a2, *a3;
    long i, j, dim;
    int howmany;
    long n_total, iter, prod;
    fftwnd_plan plan;

    if (rank > MAX_RANK || rank < 1)
	return;

    if (in_place)
	printf("TESTING RANK %d, stride %d/%d, sign %+d (in-place fftwnd)...\n", rank, istride, ostride, (int) dir);
    else
	printf("TESTING RANK %d, stride %d/%d, sign %+d (out-of-place fftwnd)...\n", rank, istride, ostride, (int) dir);

    for (iter = 0; iter < num_iters; ++iter) {
	printf("   Testing ");
	fflush(stdout);
	n_total = 1;
	for (i = 0; i < rank; ++i) {
	    do {
		dims[i] = (rand() % max_size) + 1;
	    } while
		(!factors_ok(dims[i]));
	    if (i)
		printf("x%d", dims[i]);
	    else
		printf("%d", dims[i]);
	    dims_reverse[rank - 1 - i] = dims[i];
	    n_total *= dims[i];
	}

	printf("...");
	fflush(stdout);

	if ((rank != 2 && rank != 3) || (rand() & 1)) {
	    if (in_place)
		plan =
		    fftwnd_create_plan(rank, dims, dir, FFTW_ESTIMATE | FFTW_IN_PLACE);
	    else
		plan =
		    fftwnd_create_plan(rank, dims, dir, FFTW_ESTIMATE);
	}
	/* for rank 2 and 3 arrays, use the optional interface 1/2 the
	 * time: */
	else if (rank == 2) {
	    printf("(opt. interface)...");
	    if (in_place)
		plan =
		    fftw2d_create_plan(dims[0], dims[1], dir, FFTW_ESTIMATE | FFTW_IN_PLACE);
	    else
		plan =
		    fftw2d_create_plan(dims[0], dims[1], dir, FFTW_ESTIMATE);
	} else if (rank == 3) {
	    printf("(opt. interface)...");
	    if (in_place)
		plan =
		    fftw3d_create_plan(dims[0], dims[1], dims[2], dir, FFTW_ESTIMATE | FFTW_IN_PLACE);
	    else
		plan =
		    fftw3d_create_plan(dims[0], dims[1], dims[2], dir, FFTW_ESTIMATE);
	}
	if (!plan) {
	    printf("\nError creating plan!\n");
	    exit(1);
	}
	a1 = malloc(n_total * 2 * sizeof(FFTW_REAL));
	a2 = malloc(n_total * 2 * sizeof(FFTW_REAL) * istride);
	if (in_place) {
	    a3 = a2;
	    ostride = istride;
	} else
	    a3 = malloc(n_total * 2 * sizeof(FFTW_REAL) * ostride);

	if (!a1 || !a2 || !a3) {
	    printf("\nERROR: Out of memory (need at least %ld bytes)\n", (n_total * 2 + n_total * 4) * sizeof(FFTW_REAL));
	    exit(1);
	}
	for (i = 0; i < n_total; ++i) {
	    a1[2 * i] = (rand() - (RANDOM_MAX / 2)) * 2.0 / RANDOM_MAX;
	    a1[2 * i + 1] = (rand() - (RANDOM_MAX / 2)) * 2.0 /
		RANDOM_MAX;
	    for (j = 0; j < istride; ++j) {
		a2[2 * (i * istride + j)] = a1[2 * i];
		a2[2 * (i * istride + j) + 1] = a1[2 * i + 1];
	    }
	}

	howmany = istride;
	if (howmany > ostride)
	    howmany = ostride;

	printf("fftwnd...");
	fflush(stdout);

	/* FFT using fftw */

	fftwnd(plan, howmany, (FFTW_COMPLEX *) a2, istride, 1, (FFTW_COMPLEX *) a3, ostride, 1);

	printf("GO...");
	fflush(stdout);

	/* FFT using GO */
	prod = 1;
	for (dim = 0; dim < rank; ++dim) {
	    prod *= dims_reverse[dim];
	    go_fft(a1, a1 + 1, n_total, dims_reverse[dim], prod, 2 *
		   (int) dir);
	}

	/* Check results: */

	for (i = 0; i < n_total; ++i)
	    for (j = 0; j < howmany; ++j) {
		if (!CLOSE(a1[2 * i], a3[2 * (i * ostride + j)])) {
		    printf("error!\nFFT's different in real part at i=%ld: %g vs. %g\n",
			   i, a1[2*i], a3[2*(i * ostride + j)]);
		    exit(1);
		}
		if (!CLOSE(a1[2 * i + 1], a3[2 * (i * ostride +
						  j) + 1])) {
		    printf("error!\nFFT's different in imag. part at i=%ld: %g vs. %g\n",
			   i, a1[2*i+1], a3[2*(i * ostride + j)+1]);
		    exit(1);
		}
	    }

	free(a1);
	free(a2);
	if (a3 != a2)
	    free(a3);

	fftwnd_destroy_plan(plan);
	fftw_check_memory_leaks();

	printf("okay!\n");
	fflush(stdout);
    }
}

int factors_ok(long num)
{
    if (num <= 0)
	return 0;

    while (num % 2 == 0)
	num /= 2;
    while (num % 3 == 0)
	num /= 3;
    while (num % 5 == 0)
	num /= 5;
    while (num % 7 == 0)
	num /= 7;
    while (num % 11 == 0)
	num /= 11;
    while (num % 13 == 0)
	num /= 13;
    while (num % 15 == 0)
	num /= 15;
    while (num % 17 == 0)
	num /= 17;
    while (num % 19 == 0)
	num /= 19;
    while (num % 23 == 0)
	num /= 23;

    return (num == 1);
}
