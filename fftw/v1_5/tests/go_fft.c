#include <fftw.h> /* get definition of REAL */

/************************************************************************************/
/****************************** GO FFT from Netlib **********************************/
/* The results of this ND FFT are compared to fftwnd's output to check correctness. */
/****************** This file was converted from Fortran with f2c. ******************/
/************************************************************************************/
/************************************************************************************/

/*  multivariate complex fourier transform, computed in place */
/*    using mixed-radix fast fourier transform algorithm. */
/*  by r. c. singleton, stanford research institute, sept. 1968 */
/*  arrays a and b originally hold the real and imaginary */
/*    components of the data, and return the real and */
/*    imaginary components of the resulting fourier coefficients. */
/*  multivariate data is indexed according to the fortran */
/*    array element successor function, without limit */
/*    on the number of implied multiple subscripts. */
/*    the subroutine is called once for each variate. */
/*    the calls for a multivariate transform may be in any order. */
/*  ntot is the total number of complex data values. */
/*  n is the dimension of the current variable. */
/*  nspan/n is the spacing of consecutive data values */
/*    while indexing the current variable. */
/*  the sign of isn determines the sign of the complex */
/*    exponential, and the magnitude of isn is normally one. */
/*  a tri-variate transform with a(n1,n2,n3), b(n1,n2,n3) */
/*    is computed by */
/*      call fft(a,b,n1*n2*n3,n1,n1,1) */
/*      call fft(a,b,n1*n2*n3,n2,n1*n2,1) */
/*      call fft(a,b,n1*n2*n3,n3,n1*n2*n3,1) */
/*  for a single-variate transform, */
/*    ntot = n = nspan = (number of complex data values), e.g. */
/*      call fft(a,b,n,n,n,1) */
/*  the data can alternatively be stored in a single complex array c */
/*    in standard fortran fashion, i.e. alternating real and imaginary */
/*    parts. then with most fortran compilers, the complex array c can */
/*    be equivalenced to a real array a, the magnitude of isn changed */
/*    to two to give correct indexing increment, and a(1) and a(2) used */
/*    to pass the initial addresses for the sequences of real and */
/*    imaginary values, e.g. */
/*       complex c(ntot) */
/*       real    a(2*ntot) */
/*       equivalence (c(1),a(1)) */
/*       call fft(a(1),a(2),ntot,n,nspan,2) */
/*  arrays at(maxf), ck(maxf), bt(maxf), sk(maxf), and np(maxp) */
/*    are used for temporary storage.  if the available storage */
/*    is insufficient, the program is terminated by a stop. */
/*    maxf must be .ge. the maximum prime factor of n. */
/*    maxp must be .gt. the number of prime factors of n. */
/*    in addition, if the square-free portion k of n has two or */
/*    more prime factors, then maxp must be .ge. k-1. */
/*  array storage in nfac for a maximum of 15 prime factors of n. */
/*  if n has more than one square-free factor, the product of the */
/*    square-free factors must be .le. 1010 */
/*  array storage for maximum prime factor of 23 */


int go_fft(FFTW_REAL *a, FFTW_REAL *b, long ntot, long n, 
	long nspan, long isn)
{

    /* System generated locals */
    long i__1;
    FFTW_REAL d__1, d__2;
    long equiv_0[1];

    /* Builtin functions */
    double sin(double), cos(double);

    /* Local variables */
    long nfac[11];
    FFTW_REAL radf;
    long maxf, maxp;
#define i_ (equiv_0)
    long j, k, m, kspan;
    FFTW_REAL c1, c2, c3;
    long kspnn, k1, k2, k3, k4;
    FFTW_REAL s1, s2, s3, aa, bb, cd, aj, c72;
    long jc;
    FFTW_REAL ck[23], ak;
    long jf;
    FFTW_REAL bk;
#define ii (equiv_0)
    FFTW_REAL bj;
    long jj;
    FFTW_REAL at[23], bt[23], sd;
    long kk;
    FFTW_REAL s72;
    long nn, np[1009];
    FFTW_REAL sk[23];
    long ks, kt, nt;
    FFTW_REAL s120, rad, ajm, akm;
    long inc;
    FFTW_REAL ajp, akp, bkp, bkm, bjp, bjm;

/*  the following two constants should agree with the array dimensions. */
    /* Parameter adjustments */
    --b;
    --a;

    /* Function Body */
    maxp = 1009;

/* Date: Wed, 9 Aug 1995 09:38:49 -0400 */
/* From: ldm@apollo.numis.nwu.edu */
    maxf = 23;

    if (n < 2) {
	return 0;
    }
    inc = isn;
    c72 = .30901699437494742;
    s72 = .95105651629515357;
    s120 = .86602540378443865;
    rad = 6.2831853071796;
    if (isn >= 0) {
	goto L10;
    }
    s72 = -s72;
    s120 = -s120;
    rad = -rad;
    inc = -inc;
L10:
    nt = inc * ntot;
    ks = inc * nspan;
    kspan = ks;
    nn = nt - inc;
    jc = ks / n;
    radf = rad * (FFTW_REAL) jc * .5;
    *i_ = 0;
    jf = 0;
/*  determine the factors of n */
    m = 0;
    k = n;
    goto L20;
L15:
    ++m;
    nfac[m - 1] = 4;
    k /= 16;
L20:
    if (k - (k / 16 << 4) == 0) {
	goto L15;
    }
    j = 3;
    jj = 9;
    goto L30;
L25:
    ++m;
    nfac[m - 1] = j;
    k /= jj;
L30:
    if (k % jj == 0) {
	goto L25;
    }
    j += 2;
/* Computing 2nd power */
    i__1 = j;
    jj = i__1 * i__1;
    if (jj <= k) {
	goto L30;
    }
    if (k > 4) {
	goto L40;
    }
    kt = m;
    nfac[m] = k;
    if (k != 1) {
	++m;
    }
    goto L80;
L40:
    if (k - (k / 4 << 2) != 0) {
	goto L50;
    }
    ++m;
    nfac[m - 1] = 2;
    k /= 4;
L50:
    kt = m;
    j = 2;
L60:
    if (k % j != 0) {
	goto L70;
    }
    ++m;
    nfac[m - 1] = j;
    k /= j;
L70:
    j = ((j + 1) / 2 << 1) + 1;
    if (j <= k) {
	goto L60;
    }
L80:
    if (kt == 0) {
	goto L100;
    }
    j = kt;
L90:
    ++m;
    nfac[m - 1] = nfac[j - 1];
    --j;
    if (j != 0) {
	goto L90;
    }
/*  compute fourier transform */
L100:
    sd = radf / (FFTW_REAL) kspan;
/* Computing 2nd power */
    d__1 = sin(sd);
    cd = d__1 * d__1 * 2.;
    sd = sin(sd + sd);
    kk = 1;
    ++(*i_);
    if (nfac[*i_ - 1] != 2) {
	goto L400;
    }
/*  transform for factor of 2 (including rotation factor) */
    kspan /= 2;
    k1 = kspan + 2;
L210:
    k2 = kk + kspan;
    ak = a[k2];
    bk = b[k2];
    a[k2] = a[kk] - ak;
    b[k2] = b[kk] - bk;
    a[kk] += ak;
    b[kk] += bk;
    kk = k2 + kspan;
    if (kk <= nn) {
	goto L210;
    }
    kk -= nn;
    if (kk <= jc) {
	goto L210;
    }
    if (kk > kspan) {
	goto L800;
    }
L220:
    c1 = 1. - cd;
    s1 = sd;
L230:
    k2 = kk + kspan;
    ak = a[kk] - a[k2];
    bk = b[kk] - b[k2];
    a[kk] += a[k2];
    b[kk] += b[k2];
    a[k2] = c1 * ak - s1 * bk;
    b[k2] = s1 * ak + c1 * bk;
    kk = k2 + kspan;
    if (kk < nt) {
	goto L230;
    }
    k2 = kk - nt;
    c1 = -c1;
    kk = k1 - k2;
    if (kk > k2) {
	goto L230;
    }
    ak = c1 - (cd * c1 + sd * s1);
    s1 = sd * c1 - cd * s1 + s1;
/* Computing 2nd power */
    d__1 = ak;
/* Computing 2nd power */
    d__2 = s1;
    c1 = 2. - (d__1 * d__1 + d__2 * d__2);
    s1 = c1 * s1;
    c1 *= ak;
    kk += jc;
    if (kk < k2) {
	goto L230;
    }
    k1 = k1 + inc + inc;
    kk = (k1 - kspan) / 2 + jc;
    if (kk <= jc + jc) {
	goto L220;
    }
    goto L100;
/*  transform for factor of 3 (optional code) */
L320:
    k1 = kk + kspan;
    k2 = k1 + kspan;
    ak = a[kk];
    bk = b[kk];
    aj = a[k1] + a[k2];
    bj = b[k1] + b[k2];
    a[kk] = ak + aj;
    b[kk] = bk + bj;
    ak = aj * -.5 + ak;
    bk = bj * -.5 + bk;
    aj = (a[k1] - a[k2]) * s120;
    bj = (b[k1] - b[k2]) * s120;
    a[k1] = ak - bj;
    b[k1] = bk + aj;
    a[k2] = ak + bj;
    b[k2] = bk - aj;
    kk = k2 + kspan;
    if (kk < nn) {
	goto L320;
    }
    kk -= nn;
    if (kk <= kspan) {
	goto L320;
    }
    goto L700;
/*  transform for factor of 4 */
L400:
    if (nfac[*i_ - 1] != 4) {
	goto L600;
    }
    kspnn = kspan;
    kspan /= 4;
L410:
    c1 = 1.;
    s1 = 0.;
L420:
    k1 = kk + kspan;
    k2 = k1 + kspan;
    k3 = k2 + kspan;
    akp = a[kk] + a[k2];
    akm = a[kk] - a[k2];
    ajp = a[k1] + a[k3];
    ajm = a[k1] - a[k3];
    a[kk] = akp + ajp;
    ajp = akp - ajp;
    bkp = b[kk] + b[k2];
    bkm = b[kk] - b[k2];
    bjp = b[k1] + b[k3];
    bjm = b[k1] - b[k3];
    b[kk] = bkp + bjp;
    bjp = bkp - bjp;
    if (isn < 0) {
	goto L450;
    }
    akp = akm - bjm;
    akm += bjm;
    bkp = bkm + ajm;
    bkm -= ajm;
    if (s1 == 0.) {
	goto L460;
    }
L430:
    a[k1] = akp * c1 - bkp * s1;
    b[k1] = akp * s1 + bkp * c1;
    a[k2] = ajp * c2 - bjp * s2;
    b[k2] = ajp * s2 + bjp * c2;
    a[k3] = akm * c3 - bkm * s3;
    b[k3] = akm * s3 + bkm * c3;
    kk = k3 + kspan;
    if (kk <= nt) {
	goto L420;
    }
L440:
    c2 = c1 - (cd * c1 + sd * s1);
    s1 = sd * c1 - cd * s1 + s1;
/* Computing 2nd power */
    d__1 = c2;
/* Computing 2nd power */
    d__2 = s1;
    c1 = 2. - (d__1 * d__1 + d__2 * d__2);
    s1 = c1 * s1;
    c1 *= c2;
/* Computing 2nd power */
    d__1 = c1;
/* Computing 2nd power */
    d__2 = s1;
    c2 = d__1 * d__1 - d__2 * d__2;
    s2 = c1 * 2. * s1;
    c3 = c2 * c1 - s2 * s1;
    s3 = c2 * s1 + s2 * c1;
    kk = kk - nt + jc;
    if (kk <= kspan) {
	goto L420;
    }
    kk = kk - kspan + inc;
    if (kk <= jc) {
	goto L410;
    }
    if (kspan == jc) {
	goto L800;
    }
    goto L100;
L450:
    akp = akm + bjm;
    akm -= bjm;
    bkp = bkm - ajm;
    bkm += ajm;
    if (s1 != 0.) {
	goto L430;
    }
L460:
    a[k1] = akp;
    b[k1] = bkp;
    a[k2] = ajp;
    b[k2] = bjp;
    a[k3] = akm;
    b[k3] = bkm;
    kk = k3 + kspan;
    if (kk <= nt) {
	goto L420;
    }
    goto L440;
/*  transform for factor of 5 (optional code) */
L510:
/* Computing 2nd power */
    d__1 = c72;
/* Computing 2nd power */
    d__2 = s72;
    c2 = d__1 * d__1 - d__2 * d__2;
    s2 = c72 * 2. * s72;
L520:
    k1 = kk + kspan;
    k2 = k1 + kspan;
    k3 = k2 + kspan;
    k4 = k3 + kspan;
    akp = a[k1] + a[k4];
    akm = a[k1] - a[k4];
    bkp = b[k1] + b[k4];
    bkm = b[k1] - b[k4];
    ajp = a[k2] + a[k3];
    ajm = a[k2] - a[k3];
    bjp = b[k2] + b[k3];
    bjm = b[k2] - b[k3];
    aa = a[kk];
    bb = b[kk];
    a[kk] = aa + akp + ajp;
    b[kk] = bb + bkp + bjp;
    ak = akp * c72 + ajp * c2 + aa;
    bk = bkp * c72 + bjp * c2 + bb;
    aj = akm * s72 + ajm * s2;
    bj = bkm * s72 + bjm * s2;
    a[k1] = ak - bj;
    a[k4] = ak + bj;
    b[k1] = bk + aj;
    b[k4] = bk - aj;
    ak = akp * c2 + ajp * c72 + aa;
    bk = bkp * c2 + bjp * c72 + bb;
    aj = akm * s2 - ajm * s72;
    bj = bkm * s2 - bjm * s72;
    a[k2] = ak - bj;
    a[k3] = ak + bj;
    b[k2] = bk + aj;
    b[k3] = bk - aj;
    kk = k4 + kspan;
    if (kk < nn) {
	goto L520;
    }
    kk -= nn;
    if (kk <= kspan) {
	goto L520;
    }
    goto L700;
/*  transform for odd factors */
L600:
    k = nfac[*i_ - 1];
    kspnn = kspan;
    kspan /= k;
    if (k == 3) {
	goto L320;
    }
    if (k == 5) {
	goto L510;
    }
    if (k == jf) {
	goto L640;
    }
    jf = k;
    s1 = rad / (FFTW_REAL) k;
    c1 = cos(s1);
    s1 = sin(s1);
    if (jf > maxf) {
	goto L998;
    }
    ck[jf - 1] = 1.;
    sk[jf - 1] = 0.;
    j = 1;
L630:
    ck[j - 1] = ck[k - 1] * c1 + sk[k - 1] * s1;
    sk[j - 1] = ck[k - 1] * s1 - sk[k - 1] * c1;
    --k;
    ck[k - 1] = ck[j - 1];
    sk[k - 1] = -sk[j - 1];
    ++j;
    if (j < k) {
	goto L630;
    }
L640:
    k1 = kk;
    k2 = kk + kspnn;
    aa = a[kk];
    bb = b[kk];
    ak = aa;
    bk = bb;
    j = 1;
    k1 += kspan;
L650:
    k2 -= kspan;
    ++j;
    at[j - 1] = a[k1] + a[k2];
    ak = at[j - 1] + ak;
    bt[j - 1] = b[k1] + b[k2];
    bk = bt[j - 1] + bk;
    ++j;
    at[j - 1] = a[k1] - a[k2];
    bt[j - 1] = b[k1] - b[k2];
    k1 += kspan;
    if (k1 < k2) {
	goto L650;
    }
    a[kk] = ak;
    b[kk] = bk;
    k1 = kk;
    k2 = kk + kspnn;
    j = 1;
L660:
    k1 += kspan;
    k2 -= kspan;
    jj = j;
    ak = aa;
    bk = bb;
    aj = 0.;
    bj = 0.;
    k = 1;
L670:
    ++k;
    ak = at[k - 1] * ck[jj - 1] + ak;
    bk = bt[k - 1] * ck[jj - 1] + bk;
    ++k;
    aj = at[k - 1] * sk[jj - 1] + aj;
    bj = bt[k - 1] * sk[jj - 1] + bj;
    jj += j;
    if (jj > jf) {
	jj -= jf;
    }
    if (k < jf) {
	goto L670;
    }
    k = jf - j;
    a[k1] = ak - bj;
    b[k1] = bk + aj;
    a[k2] = ak + bj;
    b[k2] = bk - aj;
    ++j;
    if (j < k) {
	goto L660;
    }
    kk += kspnn;
    if (kk <= nn) {
	goto L640;
    }
    kk -= nn;
    if (kk <= kspan) {
	goto L640;
    }
/*  multiply by rotation factor (except for factors of 2 and 4) */
L700:
    if (*i_ == m) {
	goto L800;
    }
    kk = jc + 1;
L710:
    c2 = 1. - cd;
    s1 = sd;
L720:
    c1 = c2;
    s2 = s1;
    kk += kspan;
L730:
    ak = a[kk];
    a[kk] = c2 * ak - s2 * b[kk];
    b[kk] = s2 * ak + c2 * b[kk];
    kk += kspnn;
    if (kk <= nt) {
	goto L730;
    }
    ak = s1 * s2;
    s2 = s1 * c2 + c1 * s2;
    c2 = c1 * c2 - ak;
    kk = kk - nt + kspan;
    if (kk <= kspnn) {
	goto L730;
    }
    c2 = c1 - (cd * c1 + sd * s1);
    s1 += sd * c1 - cd * s1;
/* Computing 2nd power */
    d__1 = c2;
/* Computing 2nd power */
    d__2 = s1;
    c1 = 2. - (d__1 * d__1 + d__2 * d__2);
    s1 = c1 * s1;
    c2 = c1 * c2;
    kk = kk - kspnn + jc;
    if (kk <= kspan) {
	goto L720;
    }
    kk = kk - kspan + jc + inc;
    if (kk <= jc + jc) {
	goto L710;
    }
    goto L100;
/*  permute the results to normal order---done in two stages */
/*  permutation for square factors of n */
L800:
    np[0] = ks;
    if (kt == 0) {
	goto L890;
    }
    k = kt + kt + 1;
    if (m < k) {
	--k;
    }
    j = 1;
    np[k] = jc;
L810:
    np[j] = np[j - 1] / nfac[j - 1];
    np[k - 1] = np[k] * nfac[j - 1];
    ++j;
    --k;
    if (j < k) {
	goto L810;
    }
    k3 = np[k];
    kspan = np[1];
    kk = jc + 1;
    k2 = kspan + 1;
    j = 1;
    if (n != ntot) {
	goto L850;
    }
/*  permutation for single-variate transform (optional code) */
L820:
    ak = a[kk];
    a[kk] = a[k2];
    a[k2] = ak;
    bk = b[kk];
    b[kk] = b[k2];
    b[k2] = bk;
    kk += inc;
    k2 = kspan + k2;
    if (k2 < ks) {
	goto L820;
    }
L830:
    k2 -= np[j - 1];
    ++j;
    k2 = np[j] + k2;
    if (k2 > np[j - 1]) {
	goto L830;
    }
    j = 1;
L840:
    if (kk < k2) {
	goto L820;
    }
    kk += inc;
    k2 = kspan + k2;
    if (k2 < ks) {
	goto L840;
    }
    if (kk < ks) {
	goto L830;
    }
    jc = k3;
    goto L890;
/*  permutation for multivariate transform */
L850:
    k = kk + jc;
L860:
    ak = a[kk];
    a[kk] = a[k2];
    a[k2] = ak;
    bk = b[kk];
    b[kk] = b[k2];
    b[k2] = bk;
    kk += inc;
    k2 += inc;
    if (kk < k) {
	goto L860;
    }
    kk = kk + ks - jc;
    k2 = k2 + ks - jc;
    if (kk < nt) {
	goto L850;
    }
    k2 = k2 - nt + kspan;
    kk = kk - nt + jc;
    if (k2 < ks) {
	goto L850;
    }
L870:
    k2 -= np[j - 1];
    ++j;
    k2 = np[j] + k2;
    if (k2 > np[j - 1]) {
	goto L870;
    }
    j = 1;
L880:
    if (kk < k2) {
	goto L850;
    }
    kk += jc;
    k2 = kspan + k2;
    if (k2 < ks) {
	goto L880;
    }
    if (kk < ks) {
	goto L870;
    }
    jc = k3;
L890:
    if ((kt << 1) + 1 >= m) {
	return 0;
    }
    kspnn = np[kt];
/*  permutation for square-free factors of n */
    j = m - kt;
    nfac[j] = 1;
L900:
    nfac[j - 1] *= nfac[j];
    --j;
    if (j != kt) {
	goto L900;
    }
    ++kt;
    nn = nfac[kt - 1] - 1;
    if (nn > maxp) {
	goto L998;
    }
    jj = 0;
    j = 0;
    goto L906;
L902:
    jj -= k2;
    k2 = kk;
    ++k;
    kk = nfac[k - 1];
L904:
    jj = kk + jj;
    if (jj >= k2) {
	goto L902;
    }
    np[j - 1] = jj;
L906:
    k2 = nfac[kt - 1];
    k = kt + 1;
    kk = nfac[k - 1];
    ++j;
    if (j <= nn) {
	goto L904;
    }
/*  determine the permutation cycles of length greater than 1 */
    j = 0;
    goto L914;
L910:
    k = kk;
    kk = np[k - 1];
    np[k - 1] = -kk;
    if (kk != j) {
	goto L910;
    }
    k3 = kk;
L914:
    ++j;
    kk = np[j - 1];
    if (kk < 0) {
	goto L914;
    }
    if (kk != j) {
	goto L910;
    }
    np[j - 1] = -j;
    if (j != nn) {
	goto L914;
    }
    maxf = inc * maxf;
/*  reorder a and b, following the permutation cycles */
    goto L950;
L924:
    --j;
    if (np[j - 1] < 0) {
	goto L924;
    }
    jj = jc;
L926:
    kspan = jj;
    if (jj > maxf) {
	kspan = maxf;
    }
    jj -= kspan;
    k = np[j - 1];
    kk = jc * k + *ii + jj;
    k1 = kk + kspan;
    k2 = 0;
L928:
    ++k2;
    at[k2 - 1] = a[k1];
    bt[k2 - 1] = b[k1];
    k1 -= inc;
    if (k1 != kk) {
	goto L928;
    }
L932:
    k1 = kk + kspan;
    k2 = k1 - jc * (k + np[k - 1]);
    k = -np[k - 1];
L936:
    a[k1] = a[k2];
    b[k1] = b[k2];
    k1 -= inc;
    k2 -= inc;
    if (k1 != kk) {
	goto L936;
    }
    kk = k2;
    if (k != j) {
	goto L932;
    }
    k1 = kk + kspan;
    k2 = 0;
L940:
    ++k2;
    a[k1] = at[k2 - 1];
    b[k1] = bt[k2 - 1];
    k1 -= inc;
    if (k1 != kk) {
	goto L940;
    }
    if (jj != 0) {
	goto L926;
    }
    if (j != 1) {
	goto L924;
    }
L950:
    j = k3 + 1;
    nt -= kspnn;
    *ii = nt - inc + 1;
    if (nt >= 0) {
	goto L924;
    }
    return 0;
/*  error finish, insufficient array storage */
L998:
    isn = 0;
    return 1;
} /* fft_ */

#undef ii
#undef i_


