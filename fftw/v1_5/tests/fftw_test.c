/*
 * Copyright (c) 1997,1998 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * fftw_test.c : test program
 */

/* $Id: fftw_test.c,v 1.2 1998/06/23 21:58:00 dcschmid Exp $ */
#include <fftw-int.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>

#ifdef HAVE_GETOPT
#    ifdef HAVE_GETOPT_H
#        include <getopt.h>
#    elif defined(HAVE_UNISTD_H)
#        include <unistd.h>
#    endif
#endif

/********************
 *   macrology: 
 ********************/
#ifndef TRUE
#define TRUE 42
#endif
#ifndef FALSE
#define FALSE (!TRUE)
#endif

#define CHECK(condition, complaint)      \
    if (!(condition)) {                  \
        fprintf(stderr, "FATAL ERROR: %s\n", complaint);      \
        exit(1);                         \
    }

#define WHEN_VERBOSE(a, x)  if (verbose >= a) x


#ifdef FFTW_ENABLE_FLOAT
#define TOLERANCE (1e-3)
#else
#define TOLERANCE (1e-8)
#endif

#define DRAND() mydrand()

double mydrand(void) {
     double d = rand();
     return  (d / (double)RAND_MAX) - .5;
}

void test_ergun(int n, fftw_direction dir, fftw_plan plan);

#define SPECIFICP(x) (x ? "specific" : "generic")

/*******************
 * global variables
 *******************/
int verbose;
int wisdom_flag, measure_flag;
int dimensions, dimensions_specified = 0;
int chk_mem_leak;
int paranoid;
int howmany_fields = 1;

/*******************
 * procedures
 *******************/

/* smart time printer */
static char *smart_sprint_time(double x)
{
     static char buf[128];

     if (x < 1.0E-6)
	  sprintf(buf, "%f ns", x * 1.0E9);
     else if (x < 1.0E-3)
	  sprintf(buf, "%f us", x * 1.0E6);
     else if (x < 1.0)
	  sprintf(buf, "%f ms", x * 1.0E3);
     else
	  sprintf(buf, "%f s", x);

     return buf;
}

/* greet the user */
/* jokes stolen from http://whereis.mit.edu/bin/map */
void please_wait(void)
{
     int i;
     const char *s[] = {
	  "(while a large software vendor in Seattle takes over the world)",
	  "(and remember, this is faster than Java)",
	  "(and dream of faster computers)",
	  "(checking the gravitational constant in your locale)",
	  "(at least you are not on hold)",
	  "(while X11 grows by another kilobyte)",
	  "(while Windows NT reboots)",
	  "(correcting for the phase of the moon)",
     };
     int choices = sizeof(s) / sizeof(*s);

     i = rand() % choices;
     printf("Please wait %s.\n", s[i < 0 ? -i : i]);
}

void please_wait_forever(void)
{
     int i;
     const char *s[] = {
	  "(but it won't crash, either)",
	  "(at least in theory)",
	  "(please be patient)",
	  "(our next release will complete it more quickly)",
#if defined(__WIN32__) || defined(WIN32) || defined(_WINDOWS)
	  "(by the way, Linux executes infinite loops faster)",
#endif
     };
     int choices = sizeof(s) / sizeof(*s);

     i = rand() % choices;
     printf("This test does not terminate %s.\n", s[i < 0 ? -i : i]);
}

/*************************************************
 * Speed tests
 *************************************************/

double mflops(double t, int N)
{
     return(5.0 * N * log((double) N) / (log(2.0) * t * 1.0e6));
}

/* Time an FFT routine, invoked by fft.  a is the array being
   transformed, n is its total length.  t should be a variable
   --the time (sec) per fft is assigned to it. */

#define FFTW_TIME_FFT(fft,a,n,t) \
{ \
     fftw_time ts,te; \
     double total_t; \
     int tfft_iters = 1, tfft_iter; \
     for (tfft_iter = 0; tfft_iter < (n); ++tfft_iter) \
          c_re(a[tfft_iter]) = c_im(a[tfft_iter]) = 0.0; \
     do { \
          ts = fftw_get_time(); \
          for (tfft_iter = 0; tfft_iter < tfft_iters; ++tfft_iter) fft; \
          te = fftw_get_time(); \
          t = (total_t=fftw_time_to_sec(fftw_time_diff(te,ts))) / tfft_iters; \
          tfft_iters *= 2; \
     } while (total_t < 2.0); \
}

void test_speed_aux(int n, fftw_direction dir, int flags, int specific)
{
     FFTW_COMPLEX *in, *out;
     fftw_plan plan;
     double t;
     fftw_time begin, end;

     in = (FFTW_COMPLEX *) fftw_malloc(n * howmany_fields
				       * sizeof(FFTW_COMPLEX));
     out = (FFTW_COMPLEX *) fftw_malloc(n * howmany_fields
					* sizeof(FFTW_COMPLEX));

     if (specific) {
	  begin = fftw_get_time();
	  plan = fftw_create_plan_specific(n, dir, 
					   FFTW_MEASURE | flags | wisdom_flag,
					   in,howmany_fields,
					   out,howmany_fields);
	  end = fftw_get_time();
     }
     else {
	  begin = fftw_get_time();
	  plan = fftw_create_plan(n, dir, FFTW_MEASURE | flags | wisdom_flag);
	  end = fftw_get_time();
     }
     CHECK(plan != NULL, "can't create plan");

     t = fftw_time_to_sec(fftw_time_diff(end,begin));
     WHEN_VERBOSE(2, printf("time for planner: %f s\n", t));

     WHEN_VERBOSE(2, fftw_print_plan(plan));

     if (paranoid && !(flags & FFTW_IN_PLACE)) {
	  begin = fftw_get_time();
	  test_ergun(n, dir, plan);
	  end = fftw_get_time();
	  t = fftw_time_to_sec(fftw_time_diff(end,begin));
	  WHEN_VERBOSE(2, printf("time for validation: %f s\n", t));
     }

     FFTW_TIME_FFT(fftw(plan, howmany_fields, 
			in, howmany_fields, 1, out, howmany_fields, 1),
		   in, n * howmany_fields, t);

     fftw_destroy_plan(plan);

     WHEN_VERBOSE(1, printf("time for one fft: %s", smart_sprint_time(t)));
     WHEN_VERBOSE(1, printf(" (%s/point)\n", smart_sprint_time(t / n)));
     WHEN_VERBOSE(1, printf("\"mflops\" = 5 (n log2 n) / (t in microseconds)"
			    " = %f\n", howmany_fields * mflops(t,n)));

     fftw_free(in);
     fftw_free(out);

     WHEN_VERBOSE(1, printf("\n"));
}

void test_speed(int n)
{
     int specific;

     please_wait();

     if (howmany_fields > 1)
	  WHEN_VERBOSE(1,printf("TIMING MULTIPLE-FIELD FFT: "
				"howmany=%d, stride=%d, dist=%d\n\n",
				howmany_fields,howmany_fields,1));

     for (specific = 0; specific <= 1; ++specific) {
	  WHEN_VERBOSE(1, 
		printf("SPEED TEST: n = %d, FFTW_FORWARD, out of place, %s\n",
		       n, SPECIFICP(specific)));
	  test_speed_aux(n, FFTW_FORWARD, 0, specific);
	  
	  WHEN_VERBOSE(1, 
            printf("SPEED TEST: n = %d, FFTW_FORWARD, in place, %s\n",
		       n, SPECIFICP(specific)));
	  test_speed_aux(n, FFTW_FORWARD, FFTW_IN_PLACE, specific);
	  
	  WHEN_VERBOSE(1, 
            printf("SPEED TEST: n = %d, FFTW_BACKWARD, out of place, %s\n",
		       n, SPECIFICP(specific)));
	  test_speed_aux(n, FFTW_BACKWARD, 0, specific);
	  
	  WHEN_VERBOSE(1, 
            printf("SPEED TEST: n = %d, FFTW_BACKWARD, in place, %s\n",
		       n, SPECIFICP(specific)));
	  test_speed_aux(n, FFTW_BACKWARD, FFTW_IN_PLACE, specific);
     }
}

void test_speed_nd_aux(int n, fftw_direction dir, int flags, int specific)
{
     FFTW_COMPLEX *in;
     fftwnd_plan plan;
     double t;
     fftw_time begin, end;
     int *narr, i, N;

     flags |= FFTW_IN_PLACE;  /* only bench in-place multi-dim transforms */

     narr = (int *) fftw_malloc(dimensions * sizeof(int));
     N = 1;
     for (i = 0; i < dimensions; ++i)
	  N *= (narr[i] = n);

     in = (FFTW_COMPLEX *) fftw_malloc(N * howmany_fields * 
				       sizeof(FFTW_COMPLEX));

     if (specific) {
	  begin = fftw_get_time();
	  plan = fftwnd_create_plan_specific(dimensions, narr, dir, 
					   FFTW_MEASURE | flags | wisdom_flag,
					   in,howmany_fields,0,1);
     }
     else {
	  begin = fftw_get_time();
	  plan = fftwnd_create_plan(dimensions, narr, 
				    dir, FFTW_MEASURE | flags | wisdom_flag);
     }
     end = fftw_get_time();
     CHECK(plan != NULL, "can't create plan");

     t = fftw_time_to_sec(fftw_time_diff(end,begin));
     WHEN_VERBOSE(2, printf("time for planner: %f s\n", t));

     WHEN_VERBOSE(2, printf("\n"));
     WHEN_VERBOSE(2, (fftwnd_print_plan(plan)));
     WHEN_VERBOSE(2, printf("\n"));

     FFTW_TIME_FFT(fftwnd(plan, howmany_fields, 
			  in, howmany_fields, 1,  0, 0, 0),
		   in, N * howmany_fields, t);

     fftwnd_destroy_plan(plan);

     WHEN_VERBOSE(1, printf("time for one fft: %s", smart_sprint_time(t)));
     WHEN_VERBOSE(1, printf(" (%s/point)\n", smart_sprint_time(t / N)));
     WHEN_VERBOSE(1, printf("\"mflops\" = 5 (N log2 N) / (t in microseconds)"
			    " = %f\n", howmany_fields * mflops(t,N)));

     fftw_free(in);

     fftw_free(narr);

     WHEN_VERBOSE(1, printf("\n"));
}

void print_dims(int n)
{
     int i;

     printf("%d",n);
     for (i = 1; i < dimensions; ++i)
	  printf("x%d",n);
}

void test_speed_nd(int n)
{
     int specific;

     please_wait();

     if (howmany_fields > 1)
	  WHEN_VERBOSE(1,printf("TIMING MULTIPLE-FIELD FFT: "
				"howmany=%d, stride=%d, dist=%d\n\n",
				howmany_fields,howmany_fields,1));

     for (specific = 0; specific <= 1; ++specific) {
	  printf("SPEED TEST: ");
	  WHEN_VERBOSE(1, print_dims(n));
	  WHEN_VERBOSE(1, printf(", FFTW_FORWARD, in place, %s\n",
				 SPECIFICP(specific)));
	  test_speed_nd_aux(n, FFTW_FORWARD, FFTW_IN_PLACE, specific);
	  
	  WHEN_VERBOSE(1, printf("SPEED TEST: "));
	  print_dims(n);
	  WHEN_VERBOSE(1, printf(", FFTW_BACKWARD, in place, %s\n",
				 SPECIFICP(specific)));
	  test_speed_nd_aux(n, FFTW_BACKWARD, FFTW_IN_PLACE, specific);
     }
}

/*************************************************
 * correctness tests
 *************************************************/     
void fill_random(FFTW_COMPLEX *a, int n)
{
     int i;

     /* generate random inputs */
     for (i = 0; i < n; ++i) {
	  c_re(a[i]) = DRAND();
	  c_im(a[i]) = DRAND();
     }
}

void array_copy(FFTW_COMPLEX *out, FFTW_COMPLEX *in, int n)
{
     int i;

     for (i = 0; i < n; ++i) 
	  out[i] = in[i];
}

/* C = A + B */
void array_add(FFTW_COMPLEX *C, FFTW_COMPLEX *A, FFTW_COMPLEX *B, int n)
{
     int i;

     for (i = 0; i < n; ++i) {
	  c_re(C[i]) = c_re(A[i]) + c_re(B[i]);
	  c_im(C[i]) = c_im(A[i]) + c_im(B[i]);
     }
}

/* C = A - B */
void array_sub(FFTW_COMPLEX *C, FFTW_COMPLEX *A, FFTW_COMPLEX *B, int n)
{
     int i;

     for (i = 0; i < n; ++i) {
	  c_re(C[i]) = c_re(A[i]) - c_re(B[i]);
	  c_im(C[i]) = c_im(A[i]) - c_im(B[i]);
     }
}

/* B = rotate left A */
void array_rol(FFTW_COMPLEX *B, FFTW_COMPLEX *A, 
	       int n, int n_before, int n_after)
{
     int i, ib, ia;

     for (ib = 0; ib < n_before; ++ib) {
	  for (i = 0; i < n - 1; ++i)
	       for (ia = 0; ia < n_after; ++ia)
		    B[(ib*n + i)*n_after + ia] = 
			 A[(ib*n + i + 1)*n_after + ia];
	       
	  for (ia = 0; ia < n_after; ++ia)
	       B[(ib*n + n - 1)*n_after + ia] = A[ib*n*n_after + ia];
     }
}

/* A = alpha * A  (in place) */
void array_scale(FFTW_COMPLEX *A, FFTW_COMPLEX alpha, int n)
{
     int i;

     for (i = 0; i < n; ++i) {
	  FFTW_COMPLEX a = A[i];
	  c_re(A[i]) = c_re(a) * c_re(alpha) - c_im(a) * c_im(alpha);
	  c_im(A[i]) = c_re(a) * c_im(alpha) + c_im(a) * c_re(alpha);
     }
}

#define SQR(x) ((x) * (x))

double compute_error(FFTW_COMPLEX *A, int astride, 
		     FFTW_COMPLEX *B, int bstride, int n)
{
     /* compute the relative error */
     double error = 0.0;
     int i;

     for (i = 0; i < n; ++i) {
          double a;
          double mag;
          a = sqrt(SQR(c_re(A[i*astride]) - c_re(B[i*bstride])) +
                   SQR(c_im(A[i*astride]) - c_im(B[i*bstride])));
          mag = 0.5 * (sqrt(SQR(c_re(A[i*astride])) 
			    + SQR(c_im(A[i*astride]))) +
                       sqrt(SQR(c_re(B[i*bstride])) 
			    + SQR(c_im(B[i*bstride]))));

          a /= mag;
          if (a > error)
               error = a;

          #ifdef HAVE_ISNAN
	       CHECK(!isnan(a), "NaN in answer");
          #endif
     }
     return error;
}

void array_compare(FFTW_COMPLEX *A, FFTW_COMPLEX *B, int n)
{
     CHECK(compute_error(A,1,B,1,n) < TOLERANCE, 
	   "failure in Ergun's verification procedure");
}


/*
 * Implementation of the FFT tester described in
 *
 * Funda Erg�n. Testing multivariate linear functions: Overcoming the
 * generator bottleneck. In Proceedings of the Twenty-Seventh Annual
 * ACM Symposium on the Theory of Computing, pages 407-416, Las Vegas,
 * Nevada, 29 May--1 June 1995.
 */
void test_ergun(int n, fftw_direction dir, fftw_plan plan)
{
     FFTW_COMPLEX *inA, *inB, *inC, *outA, *outB, *outC;
     FFTW_COMPLEX *tmp;
     FFTW_COMPLEX impulse;

     int i, which_impulse;
     int rounds = 20;
     double twopi = 6.28318530717958647692528676655900576839433879875021164195;
     double twopin = twopi / (double) n;

     inA = (FFTW_COMPLEX *) fftw_malloc(n * sizeof(FFTW_COMPLEX));
     inB = (FFTW_COMPLEX *) fftw_malloc(n * sizeof(FFTW_COMPLEX));
     inC = (FFTW_COMPLEX *) fftw_malloc(n * sizeof(FFTW_COMPLEX));
     outA = (FFTW_COMPLEX *) fftw_malloc(n * sizeof(FFTW_COMPLEX));
     outB = (FFTW_COMPLEX *) fftw_malloc(n * sizeof(FFTW_COMPLEX));
     outC = (FFTW_COMPLEX *) fftw_malloc(n * sizeof(FFTW_COMPLEX));
     tmp = (FFTW_COMPLEX *) fftw_malloc(n * sizeof(FFTW_COMPLEX));

     WHEN_VERBOSE(2, 
      printf("Validating plan, n = %d, dir = %s\n", n, 
	     dir == FFTW_FORWARD ? "FORWARD" : "BACKWARD"));

     /* test 1: check linearity */
     for (i = 0; i < rounds; ++i) {
	  FFTW_COMPLEX alpha, beta;
	  c_re(alpha) = DRAND();
	  c_im(alpha) = DRAND();
	  c_re(beta) = DRAND();
	  c_im(beta) = DRAND();
	  fill_random(inA, n);
	  fill_random(inB, n);
	  fftw(plan, 1, inA, 1, n, outA, 1, n);
	  fftw(plan, 1, inB, 1, n, outB, 1, n);
	  array_scale(outA, alpha, n);
	  array_scale(outB, beta, n);
	  array_add(tmp, outA, outB, n);
	  array_scale(inA, alpha, n);
	  array_scale(inB, beta, n); 
	  array_add(inC, inA, inB, n);
	  fftw(plan, 1, inC, 1, n, outC, 1, n);
	  array_compare(outC, tmp, n); 
     }

     /* test 2: check that the unit impulse is transformed properly
        -- we need to test both the real and imaginary impulses */

     for (which_impulse = 0; which_impulse < 2; ++which_impulse) {
	  if (which_impulse == 0) {      /* real impulse */
	       c_re(impulse) = 1.0;
	       c_im(impulse) = 0.0;
	  }
	  else {                         /* imaginary impulse */
	       c_re(impulse) = 0.0;
	       c_im(impulse) = 1.0;
	  }
	  
	  for (i = 0; i < n; ++i) {
	       /* impulse */
	       c_re(inA[i]) = 0.0;
	       c_im(inA[i]) = 0.0;
	       
	       /* transform of the impulse */
	       outA[i] = impulse;
	  }
	  inA[0] = impulse;

	  for (i = 0; i < rounds; ++i) {
	       fill_random(inB, n);
	       array_sub(inC, inA, inB, n);
	       fftw(plan, 1, inB, 1, n, outB, 1, n);
	       fftw(plan, 1, inC, 1, n, outC, 1, n);
	       array_add(tmp, outB, outC, n);
	       array_compare(tmp, outA, n);
	  }
     }
	  
     /* test 3: check the time-shift property */
     /* the paper performs more tests, but this code should be fine too */
     for (i = 0; i < rounds; ++i) {
	  int j;

	  fill_random(inA, n);
	  array_rol(inB, inA, n, 1, 1);
	  fftw(plan, 1, inA, 1, n, outA, 1, n);
	  fftw(plan, 1, inB, 1, n, outB, 1, n);
	  for (j = 0; j < n; ++j) {
	       double s = dir * sin(j * twopin);
	       double c = cos(j * twopin);
	       c_re(tmp[j]) = c_re(outB[j]) * c - c_im(outB[j]) * s;
	       c_im(tmp[j]) = c_re(outB[j]) * s + c_im(outB[j]) * c;
	  }

	  array_compare(tmp, outA, n);
     }
	  
     WHEN_VERBOSE(2,  printf("Validation done\n"));
     
     fftw_free(tmp);
     fftw_free(outC);
     fftw_free(outB);
     fftw_free(outA);
     fftw_free(inC);
     fftw_free(inB);
     fftw_free(inA);
}

/* Same as test_ergun, but for multi-dimensional transforms: */
void testnd_ergun(int rank, int *n, fftw_direction dir, fftwnd_plan plan)
{
     FFTW_COMPLEX *inA, *inB, *inC, *outA, *outB, *outC;
     FFTW_COMPLEX *tmp;
     FFTW_COMPLEX impulse;

     int N, n_before, n_after, dim;
     int i, which_impulse;
     int rounds = 20;
     double twopi = 6.28318530717958647692528676655900576839433879875021164195;
     double twopin;

     N = 1;
     for (dim = 0; dim < rank; ++dim)
	  N *= n[dim];

     inA = (FFTW_COMPLEX *) fftw_malloc(N * sizeof(FFTW_COMPLEX));
     inB = (FFTW_COMPLEX *) fftw_malloc(N * sizeof(FFTW_COMPLEX));
     inC = (FFTW_COMPLEX *) fftw_malloc(N * sizeof(FFTW_COMPLEX));
     outA = (FFTW_COMPLEX *) fftw_malloc(N * sizeof(FFTW_COMPLEX));
     outB = (FFTW_COMPLEX *) fftw_malloc(N * sizeof(FFTW_COMPLEX));
     outC = (FFTW_COMPLEX *) fftw_malloc(N * sizeof(FFTW_COMPLEX));
     tmp = (FFTW_COMPLEX *) fftw_malloc(N * sizeof(FFTW_COMPLEX));

     WHEN_VERBOSE(2,
      printf("Validating plan, N = %d, dir = %s\n", N,
             dir == FFTW_FORWARD ? "FORWARD" : "BACKWARD"));

     /* test 1: check linearity */
     for (i = 0; i < rounds; ++i) {
          FFTW_COMPLEX alpha, beta;
          c_re(alpha) = DRAND();
          c_im(alpha) = DRAND();
          c_re(beta) = DRAND();
          c_im(beta) = DRAND();
          fill_random(inA, N);
          fill_random(inB, N);
          fftwnd(plan, 1, inA, 1, N, outA, 1, N);
          fftwnd(plan, 1, inB, 1, N, outB, 1, N);
          array_scale(outA, alpha, N);
          array_scale(outB, beta, N);
          array_add(tmp, outA, outB, N);
          array_scale(inA, alpha, N);
          array_scale(inB, beta, N);
          array_add(inC, inA, inB, N);
          fftwnd(plan, 1, inC, 1, N, outC, 1, N);
          array_compare(outC, tmp, N);
     }

     /* test 2: check that the unit impulse is transformed properly
        -- we need to test both the real and imaginary impulses */

     for (which_impulse = 0; which_impulse < 2; ++which_impulse) {
          if (which_impulse == 0) {      /* real impulse */
               c_re(impulse) = 1.0;
               c_im(impulse) = 0.0;
          }
          else {                         /* imaginary impulse */
               c_re(impulse) = 0.0;
               c_im(impulse) = 1.0;
          }

          for (i = 0; i < N; ++i) {
               /* impulse */
               c_re(inA[i]) = 0.0;
               c_im(inA[i]) = 0.0;

               /* transform of the impulse */
               outA[i] = impulse;
          }
          inA[0] = impulse;

	  for (i = 0; i < rounds; ++i) {
               fill_random(inB, N);
               array_sub(inC, inA, inB, N);
               fftwnd(plan, 1, inB, 1, N, outB, 1, N);
               fftwnd(plan, 1, inC, 1, N, outC, 1, N);
               array_add(tmp, outB, outC, N);
               array_compare(tmp, outA, N);
          }
     }

     /* test 3: check the time-shift property */
     /* the paper performs more tests, but this code should be fine too */
     /* -- we have to check shifts in each dimension */
     
     n_before = 1;
     n_after = N;
     for (dim = 0; dim < rank; ++dim) {
	  int n_cur = n[dim];

	  n_after /= n_cur;
	  twopin = twopi / (double) n_cur;

	  for (i = 0; i < rounds; ++i) {
	       int j, jb, ja;
	       
	       fill_random(inA, N);
	       array_rol(inB, inA, n_cur, n_before, n_after);
	       fftwnd(plan, 1, inA, 1, N, outA, 1, N);
	       fftwnd(plan, 1, inB, 1, N, outB, 1, N);
	       
	       for (jb = 0; jb < n_before; ++jb)
		    for (j = 0; j < n_cur; ++j) {
			 double s = dir * sin(j * twopin);
			 double c = cos(j * twopin);

			 for (ja = 0; ja < n_after; ++ja) {
			      c_re(tmp[(jb*n_cur + j)*n_after + ja]) =
				  c_re(outB[(jb*n_cur + j)*n_after + ja]) * c 
			        - c_im(outB[(jb*n_cur + j)*n_after + ja]) * s;
			      c_im(tmp[(jb*n_cur + j)*n_after + ja]) = 
				  c_re(outB[(jb*n_cur + j)*n_after + ja]) * s 
			        + c_im(outB[(jb*n_cur + j)*n_after + ja]) * c;
			 }
		    }
	       
	       array_compare(tmp, outA, N);
	  }

	  n_before *= n_cur;
     }

     WHEN_VERBOSE(2,  printf("Validation done\n"));

     fftw_free(tmp);
     fftw_free(outC);
     fftw_free(outB);
     fftw_free(outA);
     fftw_free(inC);
     fftw_free(inB);
     fftw_free(inA);
}
	  
void test_out_of_place(int n, int istride, int ostride, 
		       int howmany, fftw_direction dir,
		       fftw_plan validated_plan,
		       int specific)
{
     FFTW_COMPLEX *in1, *in2, *out1, *out2;
     fftw_plan plan;
     int i, j;

     in1 = (FFTW_COMPLEX *) fftw_malloc(istride * n * sizeof(FFTW_COMPLEX) * howmany);
     in2 = (FFTW_COMPLEX *) fftw_malloc(n * sizeof(FFTW_COMPLEX) * howmany);
     out1 = (FFTW_COMPLEX *) fftw_malloc(ostride * n * sizeof(FFTW_COMPLEX) * howmany);
     out2 = (FFTW_COMPLEX *) fftw_malloc(n * sizeof(FFTW_COMPLEX) * howmany);


     if (!specific)
	  plan = fftw_create_plan(n, dir, measure_flag | wisdom_flag);
     else
	  plan = fftw_create_plan_specific(n, dir, 
					   measure_flag | wisdom_flag,
					   in1,istride,out1,ostride);

     /* generate random inputs */
     for (i = 0; i < n * howmany; ++i) {
	  c_re(in1[i * istride]) = c_re(in2[i]) = DRAND();
	  c_im(in1[i * istride]) = c_im(in2[i]) = DRAND();
     }

     /* 
      * fill in other positions of the array, to make sure that
      * fftw doesn't overwrite them 
      */
     for (j = 1; j < istride; ++j)
	  for (i = 0; i < n * howmany; ++i) {
	       c_re(in1[i * istride + j]) = i * istride + j;
	       c_im(in1[i * istride + j]) = i * istride - j;
	  }

     for (j = 1; j < ostride; ++j)
	  for (i = 0; i < n * howmany; ++i) {
	       c_re(out1[i * ostride + j]) = -i * ostride + j;
	       c_im(out1[i * ostride + j]) = -i * ostride - j;
	  }

     CHECK(plan != NULL, "can't create plan");
     WHEN_VERBOSE(2, fftw_print_plan(plan));

     /* fft-ize */
     fftw(plan, howmany, in1, istride, n * istride, out1, ostride,
	  n * ostride);

     fftw_destroy_plan(plan);

     /* check for overwriting */
     for (j = 1; j < istride; ++j)
	  for (i = 0; i < n * howmany; ++i)
	       CHECK(c_re(in1[i * istride + j]) == i * istride + j &&
		     c_im(in1[i * istride + j]) == i * istride - j,
		     "input has been overwritten");
     for (j = 1; j < ostride; ++j)
	  for (i = 0; i < n * howmany; ++i)
	       CHECK(c_re(out1[i * ostride + j]) == -i * ostride + j &&
		     c_im(out1[i * ostride + j]) == -i * ostride - j,
		     "input has been overwritten");

     for (i = 0; i < howmany; ++i) {
	  fftw(validated_plan, 1, in2 + n * i, 1, n, out2 + n * i, 1, n);
     }

     CHECK(compute_error(out1,ostride,out2,1,n*howmany) < TOLERANCE, 
	   "test_out_of_place: wrong answer");
     WHEN_VERBOSE(2, printf("OK\n"));

     fftw_free(in1);  
     fftw_free(in2);
     fftw_free(out1);
     fftw_free(out2);
}

void test_out_of_place_both(int n, int istride, int ostride,
			    int howmany,
			    fftw_plan validated_plan_forward,
			    fftw_plan validated_plan_backward)
{
     int specific;

     for (specific = 0; specific <= 1; ++specific) {
	  WHEN_VERBOSE(2,
	       printf("TEST CORRECTNESS (out of place, FFTW_FORWARD, %s)"
		      " n = %d  istride = %d  ostride = %d  howmany = %d\n",
		      SPECIFICP(specific),
		      n, istride, ostride, howmany));
	  test_out_of_place(n, istride, ostride, howmany, FFTW_FORWARD, 
			    validated_plan_forward, specific);
	  
	  WHEN_VERBOSE(2, 
	       printf("TEST CORRECTNESS (out of place, FFTW_BACKWARD, %s)"
		      " n = %d  istride = %d  ostride = %d  howmany = %d\n",
		      SPECIFICP(specific),
		      n, istride, ostride, howmany));
	  test_out_of_place(n, istride, ostride, howmany, FFTW_BACKWARD,
			    validated_plan_backward, specific);
     }
}

void test_in_place(int n, int istride, int howmany, fftw_direction dir,
		   fftw_plan validated_plan, int specific)
{
     FFTW_COMPLEX *in1, *in2, *out2;
     fftw_plan plan;
     int i, j;

     in1 = (FFTW_COMPLEX *) fftw_malloc(istride * n * sizeof(FFTW_COMPLEX) * howmany);
     in2 = (FFTW_COMPLEX *) fftw_malloc(n * sizeof(FFTW_COMPLEX) * howmany);
     out2 = (FFTW_COMPLEX *) fftw_malloc(n * sizeof(FFTW_COMPLEX) * howmany);


     if (!specific)
	  plan = fftw_create_plan(n, dir, FFTW_IN_PLACE | measure_flag | 
				  wisdom_flag);
     else
	  plan = fftw_create_plan_specific(n, dir,
					   FFTW_IN_PLACE | measure_flag | 
					   wisdom_flag,
					   in1, istride,
					   (FFTW_COMPLEX *)NULL, 0);

     /* generate random inputs */
     for (i = 0; i < n * howmany; ++i) {
	  c_re(in1[i * istride]) = c_re(in2[i]) = DRAND();
	  c_im(in1[i * istride]) = c_im(in2[i]) = DRAND();
     }

     /* 
      * fill in other positions of the array, to make sure that
      * fftw doesn't overwrite them 
      */
     for (j = 1; j < istride; ++j)
	  for (i = 0; i < n * howmany; ++i) {
	       c_re(in1[i * istride + j]) = i * istride + j;
	       c_im(in1[i * istride + j]) = i * istride - j;
	  }
     CHECK(plan != NULL, "can't create plan");
     WHEN_VERBOSE(2, fftw_print_plan(plan));

     /* fft-ize */
     fftw(plan, howmany, in1, istride, n * istride, (FFTW_COMPLEX *)NULL, 0,
	  0);

     fftw_destroy_plan(plan);

     /* check for overwriting */
     for (j = 1; j < istride; ++j)
	  for (i = 0; i < n * howmany; ++i)
	       CHECK(c_re(in1[i * istride + j]) == i * istride + j &&
		     c_im(in1[i * istride + j]) == i * istride - j,
		     "input has been overwritten");

     for (i = 0; i < howmany; ++i) {
	  fftw(validated_plan, 1, in2 + n * i, 1, n, out2 + n * i, 1, n);
     }

     CHECK(compute_error(in1,istride,out2,1,n*howmany) < TOLERANCE, 
	   "test_in_place: wrong answer");
     WHEN_VERBOSE(2, printf("OK\n"));

     fftw_free(in1);  
     fftw_free(in2);
     fftw_free(out2);
}

void test_in_place_both(int n, int istride, int howmany,
			fftw_plan validated_plan_forward,
			fftw_plan validated_plan_backward)
{
     int specific;

     for (specific = 0; specific <= 1; ++specific) {
	  WHEN_VERBOSE(2,
		  printf("TEST CORRECTNESS (in place, FFTW_FORWARD, %s) "
			 "n = %d  istride = %d  howmany = %d\n",
			 SPECIFICP(specific),
			 n, istride, howmany));
	  test_in_place(n, istride, howmany, FFTW_FORWARD, 
			validated_plan_forward, specific);

	  WHEN_VERBOSE(2,
		  printf("TEST CORRECTNESS (in place, FFTW_BACKWARD, %s) "
			 "n = %d  istride = %d  howmany = %d\n",
			 SPECIFICP(specific),
			 n, istride, howmany));
	  test_in_place(n, istride, howmany, FFTW_BACKWARD,
			validated_plan_backward, specific);
     }
}

#define MAX_STRIDE 3
#define MAX_HOWMANY 3

void test_correctness(int n)
{
     int istride, ostride, howmany;
     fftw_plan validated_plan_forward, validated_plan_backward;

     WHEN_VERBOSE(1,
		  printf("Testing correctness for n = %d...",n); 
		  fflush(stdout));

     /* produce a *good* plan (validated by Ergun's test procedure) */
     validated_plan_forward = 
	  fftw_create_plan(n, FFTW_FORWARD, measure_flag | wisdom_flag);
     test_ergun(n, FFTW_FORWARD, validated_plan_forward); 
     validated_plan_backward = 
	  fftw_create_plan(n, FFTW_BACKWARD, measure_flag | wisdom_flag);
     test_ergun(n, FFTW_BACKWARD, validated_plan_backward); 

     for (istride = 1; istride <= MAX_STRIDE; ++istride) 
	  for (ostride = 1; ostride <= MAX_STRIDE; ++ostride) 
	       for (howmany = 1; howmany <= MAX_HOWMANY; ++howmany)
		    test_out_of_place_both(n, istride, ostride, howmany,
					   validated_plan_forward,
					   validated_plan_backward);

     for (istride = 1; istride <= MAX_STRIDE; ++istride) 
	  for (howmany = 1; howmany <= MAX_HOWMANY; ++howmany)
	       test_in_place_both(n, istride, howmany, 
				  validated_plan_forward, 
				  validated_plan_backward);

     fftw_destroy_plan(validated_plan_forward);
     fftw_destroy_plan(validated_plan_backward);

     if (!(wisdom_flag & FFTW_USE_WISDOM) && chk_mem_leak)
	  fftw_check_memory_leaks();

     WHEN_VERBOSE(1,printf("OK\n"));
}

/* test forever */
void test_all(void)
{
     int n;

     please_wait_forever();
     for (n = 1; ; ++n) {
	  test_correctness(n);
	  if (!(wisdom_flag & FFTW_USE_WISDOM) && chk_mem_leak)
	       fftw_check_memory_leaks();
     }
}

#define MAX_FACTOR 13

int rand_small_factors(int N)
{
     int f, n = 1;

     f = rand() % MAX_FACTOR + 1;

     while (n * f <= N) {
	  n *= f;
	  f = rand() % MAX_FACTOR + 1;
     }
     
     return n;
}

#define MAX_N 16384

void random_dims(int rank, int *n)
{
     int maxsize, dim;
     double maxsize_d;

     /* workaround to weird gcc warning */
     maxsize_d = pow((double) (rank == 1 ? MAX_N / 4 : MAX_N), 
		     1.0 / (double) rank);
     maxsize = (int) maxsize_d;

     if (maxsize < 1)
	  maxsize = 1;

     for (dim = 0; dim < rank; ++dim)
	  n[dim] = rand_small_factors(maxsize);
}

void test_random(void)
{
     static int counter = 0;
     int n;

     if ((++counter) % 16 == 0)
	  n = rand() % (MAX_N / 16) + 1;
     else
	  random_dims(1,&n);

     test_correctness(n);
}

/*************************************************
 * multi-dimensional correctness tests
 *************************************************/     

void testnd_out_of_place(int rank, int *n, fftw_direction dir,
			 fftwnd_plan validated_plan)
{
     int istride, ostride;
     int N, dim, i;
     FFTW_COMPLEX *in1, *in2, *out1, *out2;
     fftwnd_plan p;

     N = 1;
     for (dim = 0; dim < rank; ++dim)
          N *= n[dim];

     in1 = (FFTW_COMPLEX*) fftw_malloc(N * MAX_STRIDE * sizeof(FFTW_COMPLEX));
     out1 = (FFTW_COMPLEX*) fftw_malloc(N * MAX_STRIDE * sizeof(FFTW_COMPLEX));
     in2 = (FFTW_COMPLEX*) fftw_malloc(N * sizeof(FFTW_COMPLEX));
     out2 = (FFTW_COMPLEX*) fftw_malloc(N * sizeof(FFTW_COMPLEX));

     p = fftwnd_create_plan(rank, n, dir, measure_flag | wisdom_flag);

     for (istride = 1; istride <= MAX_STRIDE; ++istride) {
	  /* generate random inputs */
	  for (i = 0; i < N; ++i) {
	       int j;
	       c_re(in2[i]) = DRAND();
	       c_im(in2[i]) = DRAND();
	       for (j = 0; j < istride; ++j) {
		    c_re(in1[i * istride + j]) = c_re(in2[i]);
		    c_im(in1[i * istride + j]) = c_im(in2[i]);
	       }
	  }

	  for (ostride = 1; ostride <= MAX_STRIDE; ++ostride) {
	       int howmany = (istride < ostride) ? istride : ostride;

	       fftwnd(p,howmany, in1, istride, 1, out1, ostride, 1);
	       fftwnd(validated_plan,1, in2, 1, 1, out2, 1, 1);
	       
	       for (i = 0; i < howmany; ++i)
		    CHECK(compute_error(out1 + i,ostride,out2,1,N) 
			  < TOLERANCE, 
			  "testnd_out_of_place: wrong answer");
	  }
     }

     fftwnd_destroy_plan(p);

     fftw_free(out2);
     fftw_free(in2);
     fftw_free(out1);
     fftw_free(in1);
}

void testnd_in_place(int rank, int *n, fftw_direction dir,
		     fftwnd_plan validated_plan,
		     int alternate_api, int specific, int force_buffered)
{
     int istride;
     int N, dim, i;
     FFTW_COMPLEX *in1, *in2, *out2;
     fftwnd_plan p;
     int force_buffered_flag;

     if (force_buffered)
	  force_buffered_flag = FFTWND_FORCE_BUFFERED;
     else
	  force_buffered_flag = 0;

     N = 1;
     for (dim = 0; dim < rank; ++dim)
          N *= n[dim];

     in1 = (FFTW_COMPLEX*) fftw_malloc(N * MAX_STRIDE * sizeof(FFTW_COMPLEX));
     in2 = (FFTW_COMPLEX*) fftw_malloc(N * sizeof(FFTW_COMPLEX));
     out2 = (FFTW_COMPLEX*) fftw_malloc(N * sizeof(FFTW_COMPLEX));

     if (!specific) {
	  if (alternate_api && (rank == 2 || rank == 3)) {
	       if (rank == 2)
		    p = fftw2d_create_plan(n[0],n[1], dir, 
					   measure_flag | wisdom_flag
					   | force_buffered_flag
					   | FFTW_IN_PLACE);
	       else
		    p = fftw3d_create_plan(n[0],n[1],n[2], dir, 
					   measure_flag | wisdom_flag
					   | force_buffered_flag
					   | FFTW_IN_PLACE);
	  }
	  else /* standard api */
	       p = fftwnd_create_plan(rank, n, dir, measure_flag | wisdom_flag
				      | force_buffered_flag | FFTW_IN_PLACE);
     }
     else { /* specific plan creation */
	  if (alternate_api && (rank == 2 || rank == 3)) {
	       if (rank == 2)
		    p = fftw2d_create_plan_specific(n[0],n[1], dir, 
						    measure_flag | wisdom_flag
						    | force_buffered_flag
						    | FFTW_IN_PLACE,
						    in1, 1,
						    (FFTW_COMPLEX*)NULL,1);
	       else
		    p = fftw3d_create_plan_specific(n[0],n[1],n[2], dir, 
						    measure_flag | wisdom_flag
						    | force_buffered_flag
						    | FFTW_IN_PLACE,
						    in1, 1,
						    (FFTW_COMPLEX*)NULL,1);
	  }
	  else /* standard api */
	       p = fftwnd_create_plan_specific(rank, n, dir, 
					       measure_flag | wisdom_flag
					       | force_buffered_flag 
					       | FFTW_IN_PLACE,
					       in1, 1,
					       (FFTW_COMPLEX*)NULL,1);

     }

     for (istride = 1; istride <= MAX_STRIDE; ++istride) {
	  /* generate random inputs */
	  for (i = 0; i < N; ++i) {
	       int j;
	       c_re(in2[i]) = DRAND();
	       c_im(in2[i]) = DRAND();
	       for (j = 0; j < istride; ++j) {
		    c_re(in1[i * istride + j]) = c_re(in2[i]);
		    c_im(in1[i * istride + j]) = c_im(in2[i]);
	       }
	  }

	  fftwnd(p,istride, in1, istride, 1, (FFTW_COMPLEX*)NULL, 1, 1);
	  fftwnd(validated_plan,1, in2, 1, 1, out2, 1, 1);

	  for (i = 0; i < istride; ++i)
	       CHECK(compute_error(in1 + i,istride,out2,1,N) < TOLERANCE, 
		     "testnd_in_place: wrong answer");
     }

     fftwnd_destroy_plan(p);

     fftw_free(out2);
     fftw_free(in2);
     fftw_free(in1);
}

void testnd_correctness(int rank, int *n, fftw_direction dir,
			int alt_api, int specific, int force_buf)
{
     fftwnd_plan validated_plan;

     validated_plan = fftwnd_create_plan(rank,n,dir, measure_flag
					 | wisdom_flag);
     testnd_ergun(rank,n,dir,validated_plan);

     testnd_out_of_place(rank,n,dir,validated_plan);
     testnd_in_place(rank,n,dir,validated_plan,alt_api,specific,force_buf);

     fftwnd_destroy_plan(validated_plan);
}

void testnd_correctness_both(int rank, int *n,
			     int alt_api, int specific, int force_buf)
{
     int dim;

     WHEN_VERBOSE(1,
		  printf("Testing fftwnd correctness for size = %d",n[0]);
		  for (dim = 1; dim < rank; ++dim)
		  printf("x%d",n[dim]);
		  printf("...");
		  fflush(stdout));

     if (alt_api)
	  WHEN_VERBOSE(1,printf("alt. api..."));
     if (specific)
	  WHEN_VERBOSE(1,printf("specific..."));
     if (force_buf)
	  WHEN_VERBOSE(1,printf("force buf..."));

     testnd_correctness(rank,n,FFTW_FORWARD,  alt_api,specific,force_buf);
     testnd_correctness(rank,n,FFTW_BACKWARD, alt_api,specific,force_buf);

     WHEN_VERBOSE(1,printf("OK\n"));
}

void testnd_random(int rank)
{
     int *n;

     n = (int *) fftw_malloc(sizeof(int) * rank);
     random_dims(rank,n);
     testnd_correctness_both(rank,n,rand()%2,rand()%2,rand()%2);
     fftw_free(n);
}

#define MAX_RANK 5

/* loop forever */
void test_all_random(void)
{
     static int counter = 0;
     please_wait_forever();
     while (1) {
	  if (dimensions_specified)
	       testnd_random(dimensions);
	  else if ((++counter) % 2 == 0)
	       test_random();
	  else
	       testnd_random(rand() % MAX_RANK + 1);
     }
}

void testnd_correctness_square(int rank, int size)
{
     int *n, dim;
     int alt_api, specific, force_buf;

     n = (int *) fftw_malloc(sizeof(int) * rank);
     for (dim = 0; dim < rank; ++dim)
	  n[dim] = size;

     for (alt_api = 0; alt_api <= 1; ++alt_api)
	  for (specific = 0; specific <= 1; ++specific)
	       for (force_buf = 0; force_buf <= 1; ++force_buf)
		    testnd_correctness_both(rank,n,alt_api,specific,force_buf);

     fftw_free(n);
}

/* test forever */
void testnd_all(int rank)
{
     int n;

     please_wait_forever();
     for (n = 1; ; ++n)
	  testnd_correctness_square(rank,n);
}

/*************************************************
 * planner tests
 *************************************************/     
#define PLANNER_TEST_SIZE 100

void test_planner()
{
     /*
      * create and destroy many plans, at random.  Check the
      * garbage-collecting allocator of twiddle factors 
      */
     int i,dim;
     int r, s;
     fftw_plan p[PLANNER_TEST_SIZE];
     fftwnd_plan pnd[PLANNER_TEST_SIZE];
     int *narr;

     chk_mem_leak = 0;
     verbose--;

     please_wait();
     narr = (int *) fftw_malloc(dimensions * sizeof(int));

     for (i = 0; i < PLANNER_TEST_SIZE; ++i) {
	  p[i] = (fftw_plan) 0;
	  pnd[i] = (fftwnd_plan) 0;
     }

     for (i = 0; i < PLANNER_TEST_SIZE * PLANNER_TEST_SIZE; ++i) {
	  r = rand();
	  if (r < 0)
	       r = -r;
	  r = r % PLANNER_TEST_SIZE;

	  for (dim = 0; dim < dimensions; ++dim) {
	       do {
		    s = rand();
		    if (s < 0)
			 s = -s;
		    s = s % 10000;
	       } while (s == 0);
	       narr[dim] = s;
	  }

	  if (dimensions == 1) {
	       if (p[r]) 
		    fftw_destroy_plan(p[r]);
	       
	       p[r] = fftw_create_plan(narr[0], FFTW_FORWARD, measure_flag | 
				       wisdom_flag);
	       if (paranoid && narr[0] < 200)
		    test_correctness(narr[0]);
	  }

	  if (pnd[r]) 
	       fftwnd_destroy_plan(pnd[r]);

	  pnd[r] = fftwnd_create_plan(dimensions, narr, 
				      FFTW_FORWARD, measure_flag | 
				      wisdom_flag);

	  if (i % (PLANNER_TEST_SIZE*PLANNER_TEST_SIZE/20) == 0) {
	       WHEN_VERBOSE(0, printf("test planner: so far so good\n"));
	       WHEN_VERBOSE(0, printf("test planner: iteration %d out of %d\n",
				   i, PLANNER_TEST_SIZE * PLANNER_TEST_SIZE));
	  }
     }

     for (i = 0; i < PLANNER_TEST_SIZE; ++i) {
	  if (p[i]) 
	       fftw_destroy_plan(p[i]);
	  if (pnd[i]) 
	       fftwnd_destroy_plan(pnd[i]);
     }

     fftw_free(narr);
     verbose++;
     chk_mem_leak = 1;
}

static double hack_sum;
static int hack_sum_i;

void negative_time(void) 
{
     fprintf(stderr,
	     "* PROBLEM: I measured a negative time interval.\n"
	     "* Please make sure you defined the timer correctly\n"
	     "* or contact fftw@theory.lcs.mit.edu for help.\n");
}

/*
 * paranoid test to see if time is monotonic.  If not, you are
 * really in trouble
 */
void test_timer_paranoid(void)
{
     fftw_time start_t, end_t;
     double sec;
     int i;

     start_t = fftw_get_time();

     /* waste some time */
     for (i = 0; i < 10000; ++i)
	  hack_sum_i = i;

     end_t = fftw_get_time();
     sec = fftw_time_to_sec(fftw_time_diff(end_t,start_t));
     if (sec < 0.0) 
	  negative_time();
}

void test_timer(void)
{
     double times[32], acc, min_time = 10000.00;
     unsigned long iters, iter;
     fftw_time begin, end, start;
     double t, tmax, tmin;
     int last = 0, i, repeat;

     please_wait();
     test_timer_paranoid();

     start = fftw_get_time();

     for (i = 0; i < 32; i++) {
          double sum = 0.0, x = 1.0;
          double sum1 = 0.0, x1 = 1.0;

          iters = 1 << i;
	  tmin = 1.0E10;
	  tmax = -1.0E10;

	  for (repeat = 0; repeat < FFTW_TIME_REPEAT; ++repeat) {
	       begin = fftw_get_time();
	       for (iter = 0; iter < iters; ++iter) {
		    /* some random calculations for timing... */
		    sum += x; x = .5*x + 0.2*x1; sum1 += x+x1; 
		    x1 = .4*x1 + 0.1*x;
		    sum += x; x = .5*x + 0.2*x1; sum1 += x+x1; 
		    x1 = .4*x1 + 0.1*x;
		    sum += x; x = .5*x + 0.2*x1; sum1 += x+x1; 
		    x1 = .4*x1 + 0.1*x;
		    sum += x; x = .5*x + 0.2*x1; sum1 += x+x1; 
		    x1 = .4*x1 + 0.1*x;
	       }
	       end = fftw_get_time();

	       hack_sum = sum;
	       t = fftw_time_to_sec(fftw_time_diff(end, begin));
	       if (t < tmin)
		    tmin = t;
	       if (t > tmax)
		    tmax = t;

	       /* do not run for too long */
	       t = fftw_time_to_sec(fftw_time_diff(end, start));
	       if (t > FFTW_TIME_LIMIT)
		    break;
	  }

	  if (tmin < 0.0) 
	       negative_time();
	       
          times[i] = tmin;

          WHEN_VERBOSE(2, 
		       printf("Number of iterations = 2^%d = %lu, time = %g, "
			      "time/iter = %g\n",
			      i, iters, times[i], 
			      times[i] / iters));
          WHEN_VERBOSE(2,
		       printf("   (out of %d tries, tmin = %g, tmax = %g)\n",
			      FFTW_TIME_REPEAT,tmin,tmax));

	  last = i;
	  if (times[i] > 10.0) 
	       break;
     }

     /*
      * at this point, `last' is the last valid element in the
      * `times' array.
      */

     for (i = 0; i <= last; ++i)
	  if (times[i] > 0.0 && times[i] < min_time)
	       min_time = times[i];

     WHEN_VERBOSE(1, printf("\nMinimum resolvable time interval = %g seconds.\n\n",
	    min_time));
     
     for (acc = 0.1; acc > 0.0005; acc *= 0.1) {
          double t_final;
	  t_final = times[last] / (1 << last);
	  
          for (i = last; i >= 0; --i) {
               double t_cur, error;
               iters = 1 << i;
               t_cur = times[i] / iters;
               error = (t_cur - t_final) / t_final;
               if (error < 0.0)
		    error = -error;
               if (error > acc)
		    break;
	  }

	  ++i;
	  
          WHEN_VERBOSE(1,
		 printf("Minimum time for %g%% accuracy = %g seconds.\n",
                 acc * 100.0, times[i]));
     }
     WHEN_VERBOSE(1, 
		  printf("\nMinimum time used in FFTW timing (FFTW_TIME_MIN)"
			 " = %g seconds.\n", FFTW_TIME_MIN));
}

/*************************************************
 * help
 *************************************************/     
void usage(void)
{
     printf("Usage:  fftw_test [options]\n");
     printf("  -r        : test correctness for random sizes "
	    "(does not terminate)\n");
     printf("  -d <n>    : -s/-c/-a/-p/-r applies to"
	    " n-dim. transforms (default n=1)\n");
     printf("  -s <n>    : test speed for size n\n");
     printf("  -c <n>    : test correctness for size n\n");
     printf("  -a        : test correctness for all sizes "
	    "(does not terminate)\n");
     printf("  -p        : test planner\n");
     printf("  -m        : use FFTW_MEASURE in correctness tests\n");
     printf("  -w <file> : use wisdom & read/write it from/to file\n");
     printf("  -t        : test timer resolution\n");
     printf("  -v        : verbose output for subsequent options\n");
     printf("  -P        : enable paranoid tests\n");
     printf("  -h        : this help\n");
#ifndef HAVE_GETOPT
     printf("(When run with no arguments, an interactive mode is used.)\n");
#endif
}

char wfname[128];

void handle_option(char opt, char *optarg)
{
     FILE *wf;
     int n;

     switch (opt) {
	 case 'd':
	      n = atoi(optarg);
	      CHECK(n > 0, "-d requires a positive integer argument");
	      dimensions = n;
	      dimensions_specified = 1;
	      break;
	      
	 case 's':
	      n = atoi(optarg);
	      CHECK(n > 0, "-s requires a positive integer argument");
	      if (dimensions == 1)
		   test_speed(n);
	      else
		   test_speed_nd(n);
	      break;
	      
	 case 'c':
	      n = atoi(optarg);
	      CHECK(n > 0, "-c requires a positive integer argument");
	      if (dimensions == 1 && !dimensions_specified)
		   test_correctness(n);
	      else
		   testnd_correctness_square(dimensions,n);
	      break;
	      
	 case 'p':
	      test_planner();
	      break;

	 case 'P':
	      paranoid = 1;
	      break;

	 case 'r':
	      test_all_random();
	      
	 case 'a':
	      if (dimensions == 1)
		   test_all();
	      else
		   testnd_all(dimensions);
	      break;
	      
	 case 't':
	      test_timer();
	      break;
	      
	 case 'f':
	      n = atoi(optarg);
	      CHECK(n > 0, "-f requires a positive integer argument");
	      howmany_fields = n;
	      break;
	      
	 case 'm':
	      measure_flag = FFTW_MEASURE;
	      break;

	 case 'w':
	      wisdom_flag = FFTW_USE_WISDOM;
	      strcpy(wfname,optarg);
	      wf = fopen(wfname,"r");
	      if (wf == 0) {
		   printf("Couldn't open wisdom file \"%s\".\n",wfname);
		   printf("This file will be created upon completion.\n");
	      }
	      else {
		   CHECK(FFTW_SUCCESS == fftw_import_wisdom_from_file(wf),
			 "illegal wisdom file format");
		   fclose(wf);
	      }
	      break;
	      
	 case 'v':
	      verbose++;
	      break;
	      
	 case 'h':
	      usage();
     }
     
     /* every test must free all the used FFTW memory */
     if (!(wisdom_flag & FFTW_USE_WISDOM) && chk_mem_leak)
	  fftw_check_memory_leaks();
}

short askuser(const char *s)
{
     char line[200] = "", c;
     int i, count = 0;

     do {
          if (count++ > 0)
               printf("Invalid response.  Please enter \"y\" or \"n\".\n");
          printf("%s (y/n) ",s);
          while (line[0] == 0 || line[0] == '\n') /* skip blank lines */
               fgets(line,200,stdin);
          for (i = 0; line[i] && (line[i] == ' ' || line[i] == '\t'); ++i)
               ;
          c = line[i];
     } while (c != 'n' && c != 'N' && c != 'y' && c != 'Y');

     return(c == 'y' || c == 'Y');
}

int main(int argc, char *argv[])
{
     verbose = 1;
     wisdom_flag = 0;
     measure_flag = FFTW_ESTIMATE;
     dimensions = 1;
     chk_mem_leak = 1;
     paranoid = 0;

     srand((unsigned int) time(NULL));

     /* To parse the command line, we use getopt, but this
	does not seem to be in the ANSI standard (it is only
	available on UNIX, apparently). */
#ifndef HAVE_GETOPT
     if (argc > 1)
	  printf("Sorry, command-line arguments are not available on\n"
		 "this system.  Run fftw_test with no arguments to\n"
		 "use it in interactive mode.\n");

     if (argc <= 1) {
	  int n = 0;
	  char s[128] = "";

	  usage();

	  printf("\n");

	  if (askuser("Perform random correctness tests (non-terminating)?"))
	       handle_option('r',"");

	  if (askuser("Verbose output?"))
	       handle_option('v',"");
	  if (askuser("Paranoid test?"))
	       handle_option('P',"");

	  if (askuser("Test multi-dimensional transforms?")) {
	       printf("  Enter dimensions: ");
	       scanf("%d",&n);
	       sprintf(s,"%d",n);
	       handle_option('d',s);
	  }

	  if (askuser("Use/test wisdom?")) {
	       printf("  Enter wisdom file name to use: ");
	       gets(s);
	       handle_option('w',s);
	  }
	  if (askuser("Test correctness?")) {
	       if (askuser("  -- for all sizes?"))
		    handle_option('a',"");
	       else {
		    printf("  Enter n: ");
		    scanf("%d",&n);
		    sprintf(s,"%d",n);
		    handle_option('c',s);
	       }
	  }
	  if (askuser("Test speed?")) {
	       printf("  Enter n: ");
	       scanf("%d",&n);
	       sprintf(s,"%d",n);
	       handle_option('s',s);
	  }
	  if (askuser("Test planner?"))
	       handle_option('p',"");
	  if (askuser("Test timer?"))
	       handle_option('t',"");
     }

#else /* read command-line args using getopt facility */
     {
	  extern char *optarg;
	  extern int optind;
	  int c;

	  if (argc <= 1) 
	       usage();
	  while ((c = getopt(argc, argv, "s:c:w:d:f:pPartvmh")) != -1)
	       handle_option(c, optarg);
	  if (argc != optind)
	       usage();
     }
#endif

     if (wisdom_flag & FFTW_USE_WISDOM) {
	  char *ws;
	  FILE *wf;

	  ws = fftw_export_wisdom_to_string();
	  CHECK(ws != 0,"error exporting wisdom to string");
	  printf("\nAccumulated wisdom:\n     %s\n",ws);
	  fftw_forget_wisdom();
	  CHECK(FFTW_SUCCESS == fftw_import_wisdom_from_string(ws),
		"unexpected error reading in wisdom from string");
	  fftw_free(ws);

	  wf = fopen(wfname,"w");
	  CHECK(wf != 0,"error creating wisdom file");
	  fftw_export_wisdom_to_file(wf);
	  fclose(wf);
     }

     /* make sure to dispose of wisdom before checking for memory leaks */
     fftw_forget_wisdom();

     fftw_check_memory_leaks();

     exit(0);
}
