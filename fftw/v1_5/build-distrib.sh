#!/bin/sh

(cd gensrc; make)
(autoconf)
(cd src; rm -f fftw.h; 
 awk '{handled=0} /^#undef .*$/ {print "/* " $line " */"; handled=1} {if (handled == 0) print}' fftw.h.in > fftw.h)
(cd src; rm -f config.h;
 awk '{handled=0} /^#undef .*$/ {print "/* " $line " */"; handled=1} {if (handled == 0) print}' config.h.in > config.h)
(cd doc; make; make clean)
(cd FAQ; make clean; make)
