/*
 * Copyright (c) 1997,1998 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <fftw.h>

/* fftw_f77.c:

   FORTRAN-callable "wrappers" for some of the FFTW routines.  To
   make these routines callable from FORTRAN, three things had to
   be done:

   * The routine names have to be in the style that is expected by
     the FORTRAN linker.  This is accomplished with the FORTRANIZE
     macro.

   * All parameters must be passed by reference.

   * Return values had to be converted into parameters (some
     Fortran implementations seem to have trouble calling C functions
     that return a value).

   Note that the "fftw_plan" and "fftwnd_plan" types are pointers.
   The calling FORTRAN code should use a type of the same size
   (probably "integer").

   The wrapper routines have the same name as the wrapped routine,
   except that "fftw" and "fftwnd" are replaced by "fftw_f77" and
   "fftwnd_f77".

*/

/************************************************************************/

/*
 * convert C name -> FORTRAN name.  On some systems,
 * append an underscore. On other systems, use all caps.
 *
 * x is the lower case name, X is the all caps name.
 */

#if defined(CRAY) || defined(_UNICOS) || defined(_CRAYMPP)
#define FORTRANIZE(x,X) X /* all upper-case on the Cray */

#elif defined(SOLARIS)
#define FORTRANIZE(x,X) x##_  /* append an underscore for Solaris */

#elif defined(IBM6000) || defined(_AIX)
#define FORTRANIZE(x,X) x  /* all lower-case on RS/6000 */

#elif defined(USING_G77) /* users should define this when using with the g77
			    Fortran compiler */
#define FORTRANIZE(x,X) x##__  /* g77 expects *two* underscores after
				  names with an underscore */

#else
#define FORTRANIZE(x,X) x##_  /* use all lower-case with underscore
				 by default */

#endif

/************************************************************************/

void FORTRANIZE(fftw_f77_create_plan,FFTW_F77_CREATE_PLAN)
(fftw_plan *p, int *n, int *idir, int *flags)
{
     fftw_direction dir = *idir < 0 ? FFTW_FORWARD : FFTW_BACKWARD;

     *p = fftw_create_plan(*n,dir,*flags);
}

void FORTRANIZE(fftw_f77_destroy_plan,FFTW_F77_DESTROY_PLAN)
(fftw_plan *p)
{
     fftw_destroy_plan(*p);
}

void FORTRANIZE(fftw_f77,FFTW_F77)
(fftw_plan *p, int *howmany, FFTW_COMPLEX *in, int *istride, int *idist,
 FFTW_COMPLEX *out, int *ostride, int *odist)
{
     fftw(*p,*howmany,in,*istride,*idist,out,*ostride,*odist);
}

void reverse_array(int *a, int n)
{
     int i;

     for (i = 0; i < n/2; ++i) {
	  int swap_dummy = a[i];
	  a[i] = a[n - 1 - i];
	  a[n - 1 - i] = swap_dummy;
     }
}

void FORTRANIZE(fftwnd_f77_create_plan,FFTWND_F77_CREATE_PLAN)
(fftwnd_plan *p, int *rank, int *n, int *idir, int *flags)
{
     fftw_direction dir = *idir < 0 ? FFTW_FORWARD : FFTW_BACKWARD;

     reverse_array(n,*rank);  /* column-major -> row-major */
     *p = fftwnd_create_plan(*rank,n,dir,*flags);
     reverse_array(n,*rank);  /* reverse back */
}

void FORTRANIZE(fftw2d_f77_create_plan,FFTW2D_F77_CREATE_PLAN)
(fftwnd_plan *p, int *nx, int *ny, int *idir, int *flags)
{
     fftw_direction dir = *idir < 0 ? FFTW_FORWARD : FFTW_BACKWARD;

     *p = fftw2d_create_plan(*ny,*nx,dir,*flags);
}

void FORTRANIZE(fftw3d_f77_create_plan,FFTW3D_F77_CREATE_PLAN)
(fftwnd_plan *p, int *nx, int *ny, int *nz, int *idir, int *flags)
{
     fftw_direction dir = *idir < 0 ? FFTW_FORWARD : FFTW_BACKWARD;

     *p = fftw3d_create_plan(*nz,*ny,*nx,dir,*flags);
}

void FORTRANIZE(fftwnd_f77_destroy_plan,FFTWND_F77_DESTROY_PLAN)
(fftwnd_plan *p)
{
     fftwnd_destroy_plan(*p);
}

void FORTRANIZE(fftwnd_f77,FFTWND_F77)
(fftwnd_plan *p, int *howmany, FFTW_COMPLEX *in, int *istride, int *idist,
 FFTW_COMPLEX *out, int *ostride, int *odist)
{
     fftwnd(*p,*howmany,in,*istride,*idist,out,*ostride,*odist);
}
