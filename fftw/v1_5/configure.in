dnl Process this file with autoconf to produce a configure script.
AC_INIT(src/planner.c)
AC_CONFIG_HEADER(src/config.h src/fftw.h)

AC_ARG_WITH(gcc, [  --with-gcc              use gcc instead of the native compiler cc], ok=$withval, ok=no)
if test "$ok" = "yes"; then
	CC=gcc
fi

AC_ARG_ENABLE(float, [  --enable-float          compile fftw for single precision], ok=$enableval, ok=no)
if test "$ok" = "yes"; then
	AC_DEFINE(FFTW_ENABLE_FLOAT)
fi

AC_ARG_ENABLE(i386-hacks, [  --enable-i386-hacks     enable gcc/x86 specific performance hacks], ok=$enableval, ok=no)
if test "$ok" = "yes"; then
	AC_DEFINE(FFTW_ENABLE_I386_HACKS)
fi

AC_ARG_ENABLE(pentium-timer, [  --enable-pentium-timer  enable high resolution Pentium timer], ok=$enableval, ok=no)
if test "$ok" = "yes"; then
	AC_DEFINE(FFTW_ENABLE_PENTIUM_TIMER)
fi

# Use native cc if present
AC_MSG_CHECKING([for vendor's cc to be used instead of gcc])
AC_CHECK_PROG(CC, cc, cc)

dnl Checks for programs.
AC_PROG_CC
AC_PROG_INSTALL
AC_PROG_MAKE_SET
AC_PROG_RANLIB
AC_PROG_LN_S

AC_CANONICAL_HOST

# Try to determine "good" native compiler flags.
case "${host_cpu}-${host_os}" in
i?86-linux*)	echo "*******************************************************"
		echo "*       Congratulations! You are running linux.       *"
		echo "*******************************************************"
		CFLAGS="-O6 -malign-double -fomit-frame-pointer -Wall -W  -Wbad-function-cast -Wcast-qual -Wpointer-arith -Wcast-align -pedantic";;
sparc-solaris2*) if test $CC = cc; then
                    CFLAGS="-native -fast -xO5 -dalign"
                 fi;;
alpha-osf*)     if test $CC = cc; then
                    CFLAGS="-newc -w0 -O5 -ansi_alias -ansi_args -fp_reorder -tune host -std1"
                fi;;
rs6000-aix*)    if test $CC = cc; then
                    CFLAGS="-O3 -qarch=pwrx -qtune=pwrx"
                fi;;
*)		echo ""
		echo "********************************************************"
                echo "* WARNING: Don't know the best CFLAGS for this system  *"
                echo "* Use  make CFLAGS=..., or edit the top level Makefile *"
		echo "********************************************************"
		echo "";;
esac
	
dnl Checks for libraries.
AC_CHECK_LIB(m, sqrt)

dnl Checks for header files.
AC_HEADER_STDC
AC_CHECK_HEADERS(sys/time.h unistd.h getopt.h malloc.h)

dnl Checks for typedefs, structures, and compiler characteristics.
AC_C_CONST
AC_TYPE_SIZE_T
AC_HEADER_TIME

dnl Checks for library functions.
AC_FUNC_ALLOCA
AC_CHECK_FUNCS(gettimeofday)
AC_CHECK_FUNCS(BSDgettimeofday)
AC_CHECK_FUNCS(gethrtime)
AC_CHECK_FUNCS(getopt)

AC_TRY_LINK([#include <math.h>
], if (!isnan(3.14159)) isnan(2.7183);, AC_DEFINE(HAVE_ISNAN))

AC_OUTPUT(src/Makefile tests/Makefile rfftw/Makefile Makefile)

case "${host_cpu}" in
i?86)	if test "${enable_i386_hacks-no}" != "yes"; then
	if test "${enable_float-no}" != "yes"; then
	 echo "**************************************************************"
	 echo "* If you are running an x86 system with gcc, also try        *"
	 echo "*    configure --enable-i386-hacks                           *"
	 echo "* which might produce significant speed improvements.        *"
	 echo "* (See the Installation chapter of the manual for more       *"
         echo "*  details)                                                  *"
	 echo "**************************************************************"
	fi
	fi
esac

case "${host_cpu}" in
i[[56]]86)
	if test "${enable_pentium_timer-no}" != "yes"; then
	 echo "**************************************************************"
	 echo "* This appears to be a Pentium or PentiumPro system.         *"
	 echo "* You can use the Pentium cycle counter by typing            *"
 	 echo "*    configure --enable-pentium-timer                        *"
 	 echo "* (This option is deprecated. See the Installation chapter   *"
	 echo "*  of the manual for more details)                           *"
 	 echo "**************************************************************"
	fi
esac

