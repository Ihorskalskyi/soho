/*
 * Copyright (c) 1997,1998 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * planner.c -- find the optimal plan
 */

/* $Id: planner.c,v 1.2 1998/06/23 21:57:47 dcschmid Exp $ */
#ifdef FFTW_USING_CILK
#include <cilk.h>
#include <cilk-compat.h>
#endif

#include <fftw-int.h>
#include <stdlib.h>
#include <stdio.h>

int fftw_node_cnt = 0;
int fftw_plan_cnt = 0;

/* These two constants are used for the FFTW_ESTIMATE flag to help
 * create a heuristic plan.  They don't affect FFTW_MEASURE. */
#define NOTW_OPTIMAL_SIZE 32
#define TWIDDLE_OPTIMAL_SIZE 12

#define IS_POWER_OF_TWO(n) ((n & (n - 1)) == 0)

/* wisdom prototypes */
extern int fftw_wisdom_lookup(int n, int flags, fftw_direction dir,
			      int istride, int ostride,
			      enum fftw_node_type *type,
			      int *signature, int replace_p);
extern void fftw_wisdom_add(int n, int flags, fftw_direction dir,
			    int istride, int ostride,
			    enum fftw_node_type type,
			    int signature);

/* functions for manipulating fftw_op_count data structures: */

void fftw_zero_ops(fftw_op_count * ops)
{
     ops->fp_additions = ops->fp_multiplications =
	 ops->vars = ops->memory_accesses = 0;
}

void fftw_set_ops(fftw_op_count * ops, int a, int m, int v, int mem)
{
     ops->fp_additions = a;
     ops->fp_multiplications = m;
     ops->vars = v;
     ops->memory_accesses = mem;
}

void fftw_add_ops(fftw_op_count * ops,
		  const fftw_op_count * ops_add,
		  int times)
{
#define DO_ADD_OPS(field) ops->field += ops_add->field * times
     DO_ADD_OPS(fp_additions);
     DO_ADD_OPS(fp_multiplications);
     ops->vars += ops_add->vars;	/* only add variables once */
     DO_ADD_OPS(memory_accesses);
#undef DO_ADD_OPS
}

/* constructors --- I wish I had ML */
static fftw_plan_node *make_node(void)
{
     fftw_plan_node *p = (fftw_plan_node *)
     fftw_malloc(sizeof(fftw_plan_node));
     p->refcnt = 0;
     fftw_zero_ops(&p->ops);
     fftw_node_cnt++;
     return p;
}

static void use_node(fftw_plan_node *p)
{
     ++p->refcnt;
}

static fftw_plan_node *make_node_notw(int size,
				      const fftw_codelet_desc * config)
{
     fftw_plan_node *p = make_node();

     p->type = FFTW_NOTW;
     p->nodeu.notw.size = size;
     p->nodeu.notw.codelet = (notw_codelet *) config->codelet;
     p->nodeu.notw.codelet_desc = config;
     fftw_add_ops(&p->ops, &config->ops, 1);
     return p;
}

static fftw_plan_node *make_node_twiddle(int n,
					 const fftw_codelet_desc * config,
					 fftw_plan_node *recurse,
					 int flags)
{
     fftw_plan_node *p = make_node();

     p->type = FFTW_TWIDDLE;
     p->nodeu.twiddle.size = config->size;
     p->nodeu.twiddle.codelet = (twiddle_codelet *) config->codelet;
     fftw_add_ops(&p->ops, &config->ops, n / config->size);
     p->nodeu.twiddle.recurse = recurse;
     p->nodeu.twiddle.codelet_desc = config;
     use_node(recurse);
     if (flags & FFTW_MEASURE)
	  p->nodeu.twiddle.tw = fftw_create_twiddle(n, config);
     else
	  p->nodeu.twiddle.tw = 0;
     return p;
}

static fftw_plan_node *make_node_generic(int n, int size,
					 generic_codelet *codelet,
					 fftw_plan_node *recurse,
					 int flags)
{
     fftw_plan_node *p = make_node();

     p->type = FFTW_GENERIC;
     p->nodeu.generic.size = size;
     p->nodeu.generic.codelet = codelet;
     {
	  int r = size;
	  int m = n / size;
	  fftw_set_ops(&p->ops, m * r * r * 4,
		       m * r * r * 4, 2 * r + 14,
		       4 * m * r * (r + 1));
     }
     p->nodeu.generic.recurse = recurse;
     use_node(recurse);

     if (flags & FFTW_MEASURE)
	  p->nodeu.generic.tw = fftw_create_twiddle(n,
					  (const fftw_codelet_desc *) 0);
     else
	  p->nodeu.generic.tw = 0;
     return p;
}

static void destroy_tree(fftw_plan_node *p)
{
     if (p) {
	  --p->refcnt;
	  if (p->refcnt == 0) {
	       switch (p->type) {
		   case FFTW_NOTW:
			break;

		   case FFTW_TWIDDLE:
			if (p->nodeu.twiddle.tw)
			     fftw_destroy_twiddle(p->nodeu.twiddle.tw);
			destroy_tree(p->nodeu.twiddle.recurse);
			break;

		   case FFTW_GENERIC:
			if (p->nodeu.generic.tw)
			     fftw_destroy_twiddle(p->nodeu.generic.tw);
			destroy_tree(p->nodeu.generic.recurse);
			break;
	       }

	       fftw_free(p);
	       fftw_node_cnt--;
	  }
     }
}

/* create a plan with twiddle factors, and other bells and whistles */
static fftw_plan make_plan(int n, fftw_direction dir,
			   fftw_plan_node *root, int flags,
			   enum fftw_node_type wisdom_type,
			   int wisdom_signature)
{
     fftw_plan p = (fftw_plan) fftw_malloc(sizeof(struct fftw_plan_struct));

     p->n = n;
     p->dir = dir;
     p->flags = flags;
     use_node(root);
     p->root = root;
     p->cost = 0.0;
     p->wisdom_type = wisdom_type;
     p->wisdom_signature = wisdom_signature;
     p->next = (fftw_plan) 0;
     p->refcnt = 0;
     fftw_plan_cnt++;
     return p;
}

/*
 * complete with twiddle factors (because nodes don't have
 * them when FFTW_ESTIMATE is set)
 */
static void complete_twiddle(fftw_plan_node *p, int n)
{
     int r;
     switch (p->type) {
	 case FFTW_NOTW:
	      break;

	 case FFTW_TWIDDLE:
	      r = p->nodeu.twiddle.size;
	      if (!p->nodeu.twiddle.tw)
		   p->nodeu.twiddle.tw =
		       fftw_create_twiddle(n, p->nodeu.twiddle.codelet_desc);
	      complete_twiddle(p->nodeu.twiddle.recurse, n / r);
	      break;

	 case FFTW_GENERIC:
	      r = p->nodeu.generic.size;
	      if (!p->nodeu.generic.tw)
		   p->nodeu.generic.tw =
		       fftw_create_twiddle(n, (const fftw_codelet_desc *) 0);
	      complete_twiddle(p->nodeu.generic.recurse, n / r);
	      break;
     }
}

static void use_plan(fftw_plan p)
{
     ++p->refcnt;
}

static void destroy_plan(fftw_plan p)
{
     --p->refcnt;

     if (p->refcnt == 0) {
	  destroy_tree(p->root);
	  fftw_plan_cnt--;
	  fftw_free(p);
     }
}

/* end of constructors */

/* management of plan tables */
static void make_empty_table(fftw_plan *table)
{
     *table = (fftw_plan) 0;
}

static void insert(fftw_plan *table, fftw_plan this_plan, int n)
{
     use_plan(this_plan);
     this_plan->n = n;
     this_plan->next = *table;
     *table = this_plan;
}

static fftw_plan lookup(fftw_plan *table, int n, int flags)
{
     fftw_plan p;

     for (p = *table; p &&
	  ((p->n != n) || (p->flags != flags)); p = p->next);

     return p;
}

static void destroy_table(fftw_plan *table)
{
     fftw_plan p, q;

     for (p = *table; p; p = q) {
	  q = p->next;
	  destroy_plan(p);
     }
}

static double estimate_node(fftw_plan_node *p)
{
     int k;

     switch (p->type) {
	 case FFTW_NOTW:
	      k = p->nodeu.notw.size;
	      return 1.0 + 0.1 * (k - NOTW_OPTIMAL_SIZE) *
		  (k - NOTW_OPTIMAL_SIZE);

	 case FFTW_TWIDDLE:
	      k = p->nodeu.twiddle.size;
	      return 1.0 + 0.1 * (k - TWIDDLE_OPTIMAL_SIZE) *
		  (k - TWIDDLE_OPTIMAL_SIZE)
		  + estimate_node(p->nodeu.twiddle.recurse);

	 case FFTW_GENERIC:
	      k = p->nodeu.generic.size;
	      return 10.0 + k * k
		  + estimate_node(p->nodeu.generic.recurse);
     }
     return 1.0E20;
}

/* auxiliary functions */
static void compute_cost(fftw_plan plan,
			 FFTW_COMPLEX *in, int istride,
			 FFTW_COMPLEX *out, int ostride)
{
     if (plan->flags & FFTW_MEASURE)
	  plan->cost = fftw_measure_runtime(plan, in, istride, out, ostride);
     else {
	  double c;
	  c = plan->n * estimate_node(plan->root);
	  plan->cost = c;
     }
}

/* pick the better of two plans and destroy the other one. */
static fftw_plan pick_better(fftw_plan p1, fftw_plan p2)
{
     if (!p1)
	  return p2;

     if (!p2)
	  return p1;

     if (p1->cost > p2->cost) {
	  destroy_plan(p1);
	  return p2;
     } else {
	  destroy_plan(p2);
	  return p1;
     }
}

/* find the smallest prime factor of n */
static int factor(int n)
{
     int r;

     /* try 2 */
     if ((n & 1) == 0)
	  return 2;

     /* try odd numbers up to sqrt(n) */
     for (r = 3; r * r <= n; r += 2)
	  if (n % r == 0)
	       return r;

     /* n is prime */
     return n;
}

/* macrology */
#define FOR_ALL_CODELETS(p) \
   fftw_codelet_desc **__q, *p;                         \
   for (__q = &fftw_config[0]; (p = (*__q)); ++__q)

/******************************************
 *      Recursive planner                 *
 ******************************************/
fftw_plan planner(fftw_plan *table, int n, fftw_direction dir, int flags,
		  FFTW_COMPLEX *, int, FFTW_COMPLEX *, int);

/*
 * the planner consists of two parts: one that tries to
 * use accumulated wisdom, and one that does not.
 * A small driver invokes both parts in sequence
 */

/* planner with wisdom: look up the codelet suggested by the wisdom */
fftw_plan planner_wisdom(fftw_plan *table, int n,
			 fftw_direction dir, int flags,
			 FFTW_COMPLEX *in, int istride,
			 FFTW_COMPLEX *out, int ostride)
{
     fftw_plan best = (fftw_plan) 0;
     fftw_plan_node *node;
     int have_wisdom;
     enum fftw_node_type wisdom_type;
     int wisdom_signature;

     /* see if we remember any wisdom for this case */
     have_wisdom = fftw_wisdom_lookup(n, flags, dir, istride, ostride,
				      &wisdom_type, &wisdom_signature, 0);

     if (!have_wisdom)
	  return best;

     if (wisdom_type == FFTW_NOTW) {
	  FOR_ALL_CODELETS(p) {
	       if (p->dir == dir && p->type == wisdom_type) {
		    /* see if wisdom applies */
		    if (wisdom_signature == p->signature &&
			p->size == n) {
			 node = make_node_notw(n, p);
			 best = make_plan(n, dir, node, flags,
					  FFTW_NOTW, p->signature);
			 use_plan(best);
			 return best;
		    }
	       }
	  }
     }
     if (wisdom_type == FFTW_TWIDDLE) {
	  FOR_ALL_CODELETS(p) {
	       if (p->dir == dir && p->type == wisdom_type) {

		    /* see if wisdom applies */
		    if (wisdom_signature == p->signature &&
			(n % p->size) == 0) {
			 fftw_plan r = planner(table, n / p->size, dir, flags,
					       in, istride, out, ostride);
			 node = make_node_twiddle(n, p,
						  r->root, flags);
			 best = make_plan(n, dir, node, flags,
					  FFTW_TWIDDLE, p->signature);
			 use_plan(best);
			 destroy_plan(r);
			 return best;
		    }
	       }
	  }
     }
     /* 
      * BUG (or: TODO)  Can we have generic wisdom? This is probably
      * an academic question
      */

     return best;
}

/*
 * planner with no wisdom: try all combinations and pick
 * the best
 */
fftw_plan planner_normal(fftw_plan *table, int n, fftw_direction dir,
			 int flags,
			 FFTW_COMPLEX *in, int istride,
			 FFTW_COMPLEX *out, int ostride)
{
     fftw_plan best = (fftw_plan) 0;
     fftw_plan newplan;
     fftw_plan_node *node;

     /* see if we have any codelet that solves the problem */
     {
	  FOR_ALL_CODELETS(p) {
	       if (p->dir == dir && p->type == FFTW_NOTW) {
		    if (p->size == n) {
			 node = make_node_notw(n, p);
			 newplan = make_plan(n, dir, node, flags,
					     FFTW_NOTW, p->signature);
			 use_plan(newplan);
			 compute_cost(newplan, in, istride, out, ostride);
			 best = pick_better(newplan, best);
		    }
	       }
	  }
     }

     /* Then, try all available twiddle codelets */
     {
	  FOR_ALL_CODELETS(p) {
	       if (p->dir == dir && p->type == FFTW_TWIDDLE) {
		    if ((n % p->size) == 0 &&
			(!best || n != p->size)) {
			 fftw_plan r = planner(table, n / p->size, dir, flags,
					       in, istride, out, ostride);
			 node = make_node_twiddle(n, p,
						  r->root, flags);
			 newplan = make_plan(n, dir, node, flags,
					     FFTW_TWIDDLE, p->signature);
			 use_plan(newplan);
			 destroy_plan(r);
			 compute_cost(newplan, in, istride, out, ostride);
			 best = pick_better(newplan, best);
		    }
	       }
	  }
     }

     /* 
      * if no plan has been found so far, resort to generic codelets 
      */
     if (!best) {
	  generic_codelet *codelet = (dir == FFTW_FORWARD ?
			   fftw_twiddle_generic : fftwi_twiddle_generic);
	  int size = factor(n);
	  fftw_plan r = planner(table, n / size, dir, flags,
				in, istride, out, ostride);

	  node = make_node_generic(n, size, codelet, r->root, flags);
	  newplan = make_plan(n, dir, node, flags, FFTW_GENERIC, 0);
	  use_plan(newplan);
	  destroy_plan(r);
	  compute_cost(newplan, in, istride, out, ostride);
	  best = pick_better(newplan, best);
     }
     return best;
}

fftw_plan planner(fftw_plan *table, int n, fftw_direction dir, int flags,
		  FFTW_COMPLEX *in, int istride,
		  FFTW_COMPLEX *out, int ostride)
{
     fftw_plan best = (fftw_plan) 0;

     /* see if plan has already been computed */
     best = lookup(table, n, flags);
     if (best) {
	  use_plan(best);
	  return best;
     }
     /* try a wise plan */
     best = planner_wisdom(table, n, dir, flags,
			   in, istride, out, ostride);

     if (!best) {
	  /* No wisdom.  Plan normally. */
	  best = planner_normal(table, n, dir, flags,
				in, istride, out, ostride);
     }
     if (best) {
	  insert(table, best, n);

	  /* remember the wisdom */
	  fftw_wisdom_add(n, flags, dir, istride, ostride,
			  best->wisdom_type,
			  best->wisdom_signature);
     }
     return best;
}

fftw_plan fftw_create_plan_specific(int n, fftw_direction dir, int flags,
				    FFTW_COMPLEX *in, int istride,
				    FFTW_COMPLEX *out, int ostride)
{
     fftw_plan table;
     fftw_plan p1;

     /* validate parameters */
     if (n <= 0)
	  return (fftw_plan) 0;

     if ((dir != FFTW_FORWARD) && (dir != FFTW_BACKWARD))
	  return (fftw_plan) 0;

     make_empty_table(&table);
     p1 = planner(&table, n, dir, flags,
		  in, istride, out, ostride);
     destroy_table(&table);

     complete_twiddle(p1->root, n);
     return p1;
}

fftw_plan fftw_create_plan(int n, fftw_direction dir, int flags)
{
     FFTW_COMPLEX *tmp_in, *tmp_out;
     fftw_plan p;

     if (flags & FFTW_MEASURE) {
	  tmp_in = (FFTW_COMPLEX *) fftw_malloc(2 * n * sizeof(FFTW_COMPLEX));
	  if (!tmp_in)
	       return 0;
	  tmp_out = tmp_in + n;

	  p = fftw_create_plan_specific(n, dir, flags,
					tmp_in, 1, tmp_out, 1);

	  fftw_free(tmp_in);
     } else
	  p = fftw_create_plan_specific(n, dir, flags,
			   (FFTW_COMPLEX *) 0, 1, (FFTW_COMPLEX *) 0, 1);

     return p;
}

void fftw_destroy_plan(fftw_plan plan)
{
     destroy_plan(plan);
}

static void count_plan_ops(int n, fftw_plan_node *p, fftw_op_count * ops)
{
     fftw_op_count tmp_ops;

     if (p) {
	  switch (p->type) {
	      case FFTW_NOTW:
		   fftw_add_ops(ops, &p->ops, 1);
		   break;
	      case FFTW_TWIDDLE:
		   fftw_add_ops(ops, &p->ops, 1);
		   fftw_zero_ops(&tmp_ops);
		   count_plan_ops(n / p->nodeu.twiddle.size,
				  p->nodeu.twiddle.recurse, &tmp_ops);
		   fftw_add_ops(ops, &tmp_ops, p->nodeu.twiddle.size);
		   break;
	      case FFTW_GENERIC:
		   fftw_add_ops(ops, &p->ops, 1);
		   fftw_zero_ops(&tmp_ops);
		   count_plan_ops(n / p->nodeu.generic.size,
				  p->nodeu.generic.recurse, &tmp_ops);
		   fftw_add_ops(ops, &tmp_ops, p->nodeu.generic.size);
		   break;
	  }
     }
}

void fftw_count_plan_ops(fftw_plan p, fftw_op_count * ops)
{
     fftw_zero_ops(ops);
     count_plan_ops(p->n, p->root, ops);
}

static void print_node(FILE *f, fftw_plan_node *p, int indent)
{
     if (p) {
	  switch (p->type) {
	      case FFTW_NOTW:
		   fprintf(f, "%*sFFTW_NOTW %d\n", indent, "",
			   p->nodeu.notw.size);
		   break;
	      case FFTW_TWIDDLE:
		   fprintf(f, "%*sFFTW_TWIDDLE %d\n", indent, "",
			   p->nodeu.twiddle.size);
		   print_node(f, p->nodeu.twiddle.recurse, indent);
		   break;
	      case FFTW_GENERIC:
		   fprintf(f, "%*sFFTW_GENERIC %d\n", indent, "",
			   p->nodeu.generic.size);
		   print_node(f, p->nodeu.generic.recurse, indent);
		   break;
	  }
     }
}

void fftw_fprint_plan(FILE *f, fftw_plan p)
{
     fftw_op_count ops;

     fftw_count_plan_ops(p, &ops);
     fprintf(f, "plan: (cost = %e)\n"
	"[ fp adds = %d, fp mults = %d, vars = %d, load/stores = %d ]\n",
	     p->cost,
	     ops.fp_additions, ops.fp_multiplications,
	     ops.vars, ops.memory_accesses);
     print_node(f, p->root, 0);
}

void fftw_print_plan(fftw_plan p)
{
     fftw_fprint_plan(stdout, p);
}
