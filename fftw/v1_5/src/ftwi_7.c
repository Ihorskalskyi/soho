/*
 * Copyright (c) 1997,1998 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* This file has been automatically generated --- DO NOT EDIT */

#include <fftw-int.h>
#include <fftw.h>

/* $Id: ftwi_7.c,v 1.2 1998/06/23 21:57:44 dcschmid Exp $ */

/* Generated by: ./genfft -magic-twiddle-load-all -magic-variables 4 -magic-loopi -twiddleinv 7 */

/* This function contains 72 FP additions, 60 FP multiplications, */
/* 24 stack variables, and 28 memory accesses */
static const FFTW_REAL K222520933 = ((FFTW_REAL) +0.222520933956314404288902564496794759466355569);
static const FFTW_REAL K900968867 = ((FFTW_REAL) +0.900968867902419126236102319507445051165919162);
static const FFTW_REAL K623489801 = ((FFTW_REAL) +0.623489801858733530525004884004239810632274731);
static const FFTW_REAL K433883739 = ((FFTW_REAL) +0.433883739117558120475768332848358754609990728);
static const FFTW_REAL K781831482 = ((FFTW_REAL) +0.781831482468029808708444526674057750232334519);
static const FFTW_REAL K974927912 = ((FFTW_REAL) +0.974927912181823607018131682993931217232785801);

void fftwi_twiddle_7(FFTW_COMPLEX *A, const FFTW_COMPLEX *W, int iostride, int m, int dist)
{
     int i;
     FFTW_COMPLEX *inout;
     inout = A;
     for (i = 0; i < m; i = i + 1, inout = inout + dist, W = W + 6) {
	  FFTW_REAL tmp1;
	  FFTW_REAL tmp35;
	  FFTW_REAL tmp12;
	  FFTW_REAL tmp51;
	  FFTW_REAL tmp38;
	  FFTW_REAL tmp46;
	  FFTW_REAL tmp23;
	  FFTW_REAL tmp52;
	  FFTW_REAL tmp41;
	  FFTW_REAL tmp47;
	  FFTW_REAL tmp34;
	  FFTW_REAL tmp53;
	  FFTW_REAL tmp44;
	  FFTW_REAL tmp48;
	  tmp1 = c_re(inout[0]);
	  tmp35 = c_im(inout[0]);
	  {
	       FFTW_REAL tmp6;
	       FFTW_REAL tmp36;
	       FFTW_REAL tmp11;
	       FFTW_REAL tmp37;
	       {
		    FFTW_REAL tmp2;
		    FFTW_REAL tmp4;
		    FFTW_REAL tmp3;
		    FFTW_REAL tmp5;
		    tmp2 = c_re(W[0]);
		    tmp4 = c_im(W[0]);
		    tmp3 = c_re(inout[iostride]);
		    tmp5 = c_im(inout[iostride]);
		    tmp6 = (tmp2 * tmp3) + (tmp4 * tmp5);
		    tmp36 = (tmp2 * tmp5) - (tmp4 * tmp3);
	       }
	       {
		    FFTW_REAL tmp7;
		    FFTW_REAL tmp9;
		    FFTW_REAL tmp8;
		    FFTW_REAL tmp10;
		    tmp7 = c_re(W[5]);
		    tmp9 = c_im(W[5]);
		    tmp8 = c_re(inout[6 * iostride]);
		    tmp10 = c_im(inout[6 * iostride]);
		    tmp11 = (tmp7 * tmp8) + (tmp9 * tmp10);
		    tmp37 = (tmp7 * tmp10) - (tmp9 * tmp8);
	       }
	       tmp12 = tmp6 + tmp11;
	       tmp51 = tmp6 - tmp11;
	       tmp38 = tmp36 + tmp37;
	       tmp46 = tmp37 - tmp36;
	  }
	  {
	       FFTW_REAL tmp17;
	       FFTW_REAL tmp39;
	       FFTW_REAL tmp22;
	       FFTW_REAL tmp40;
	       {
		    FFTW_REAL tmp13;
		    FFTW_REAL tmp15;
		    FFTW_REAL tmp14;
		    FFTW_REAL tmp16;
		    tmp13 = c_re(W[1]);
		    tmp15 = c_im(W[1]);
		    tmp14 = c_re(inout[2 * iostride]);
		    tmp16 = c_im(inout[2 * iostride]);
		    tmp17 = (tmp13 * tmp14) + (tmp15 * tmp16);
		    tmp39 = (tmp13 * tmp16) - (tmp15 * tmp14);
	       }
	       {
		    FFTW_REAL tmp18;
		    FFTW_REAL tmp20;
		    FFTW_REAL tmp19;
		    FFTW_REAL tmp21;
		    tmp18 = c_re(W[4]);
		    tmp20 = c_im(W[4]);
		    tmp19 = c_re(inout[5 * iostride]);
		    tmp21 = c_im(inout[5 * iostride]);
		    tmp22 = (tmp18 * tmp19) + (tmp20 * tmp21);
		    tmp40 = (tmp18 * tmp21) - (tmp20 * tmp19);
	       }
	       tmp23 = tmp17 + tmp22;
	       tmp52 = tmp17 - tmp22;
	       tmp41 = tmp39 + tmp40;
	       tmp47 = tmp40 - tmp39;
	  }
	  {
	       FFTW_REAL tmp28;
	       FFTW_REAL tmp42;
	       FFTW_REAL tmp33;
	       FFTW_REAL tmp43;
	       {
		    FFTW_REAL tmp24;
		    FFTW_REAL tmp26;
		    FFTW_REAL tmp25;
		    FFTW_REAL tmp27;
		    tmp24 = c_re(W[2]);
		    tmp26 = c_im(W[2]);
		    tmp25 = c_re(inout[3 * iostride]);
		    tmp27 = c_im(inout[3 * iostride]);
		    tmp28 = (tmp24 * tmp25) + (tmp26 * tmp27);
		    tmp42 = (tmp24 * tmp27) - (tmp26 * tmp25);
	       }
	       {
		    FFTW_REAL tmp29;
		    FFTW_REAL tmp31;
		    FFTW_REAL tmp30;
		    FFTW_REAL tmp32;
		    tmp29 = c_re(W[3]);
		    tmp31 = c_im(W[3]);
		    tmp30 = c_re(inout[4 * iostride]);
		    tmp32 = c_im(inout[4 * iostride]);
		    tmp33 = (tmp29 * tmp30) + (tmp31 * tmp32);
		    tmp43 = (tmp29 * tmp32) - (tmp31 * tmp30);
	       }
	       tmp34 = tmp28 + tmp33;
	       tmp53 = tmp28 - tmp33;
	       tmp44 = tmp42 + tmp43;
	       tmp48 = tmp43 - tmp42;
	  }
	  {
	       FFTW_REAL tmp56;
	       FFTW_REAL tmp55;
	       FFTW_REAL tmp58;
	       FFTW_REAL tmp57;
	       c_re(inout[0]) = tmp1 + (tmp12 + tmp23 + tmp34);
	       tmp56 = (K974927912 * tmp46) - (K781831482 * tmp48) - (K433883739 * tmp47);
	       tmp55 = tmp1 + (K623489801 * tmp34) - (K900968867 * tmp23) - (K222520933 * tmp12);
	       c_re(inout[2 * iostride]) = tmp55 + tmp56;
	       c_re(inout[5 * iostride]) = tmp55 - tmp56;
	       {
		    FFTW_REAL tmp60;
		    FFTW_REAL tmp59;
		    FFTW_REAL tmp49;
		    FFTW_REAL tmp45;
		    tmp60 = (K433883739 * tmp46) + (K974927912 * tmp48) - (K781831482 * tmp47);
		    tmp59 = tmp1 + (K623489801 * tmp23) - (K222520933 * tmp34) - (K900968867 * tmp12);
		    c_re(inout[3 * iostride]) = tmp59 + tmp60;
		    c_re(inout[4 * iostride]) = tmp59 - tmp60;
		    tmp49 = (K781831482 * tmp46) + (K974927912 * tmp47) + (K433883739 * tmp48);
		    tmp45 = tmp1 + (K623489801 * tmp12) - (K900968867 * tmp34) - (K222520933 * tmp23);
		    c_re(inout[iostride]) = tmp45 + tmp49;
		    c_re(inout[6 * iostride]) = tmp45 - tmp49;
	       }
	       c_im(inout[0]) = tmp35 + (tmp38 + tmp41 + tmp44);
	       tmp58 = (K974927912 * tmp51) - (K781831482 * tmp53) - (K433883739 * tmp52);
	       tmp57 = tmp35 + (K623489801 * tmp44) - (K900968867 * tmp41) - (K222520933 * tmp38);
	       c_im(inout[2 * iostride]) = tmp57 + tmp58;
	       c_im(inout[5 * iostride]) = tmp57 - tmp58;
	       {
		    FFTW_REAL tmp62;
		    FFTW_REAL tmp61;
		    FFTW_REAL tmp54;
		    FFTW_REAL tmp50;
		    tmp62 = (K433883739 * tmp51) + (K974927912 * tmp53) - (K781831482 * tmp52);
		    tmp61 = tmp35 + (K623489801 * tmp41) - (K222520933 * tmp44) - (K900968867 * tmp38);
		    c_im(inout[3 * iostride]) = tmp61 + tmp62;
		    c_im(inout[4 * iostride]) = tmp61 - tmp62;
		    tmp54 = (K781831482 * tmp51) + (K974927912 * tmp52) + (K433883739 * tmp53);
		    tmp50 = tmp35 + (K623489801 * tmp38) - (K900968867 * tmp44) - (K222520933 * tmp41);
		    c_im(inout[iostride]) = tmp50 + tmp54;
		    c_im(inout[6 * iostride]) = tmp50 - tmp54;
	       }
	  }
     }
}

static const int twiddle_order[] =
{1, 2, 3, 4, 5, 6};
fftw_codelet_desc fftwi_twiddle_7_desc =
{
     "fftwi_twiddle_7",
     (void (*)()) fftwi_twiddle_7,
     7,
     FFTW_BACKWARD,
     FFTW_TWIDDLE,
     30,
     6,
     twiddle_order,
     {72, 60, 24, 28}
};
