/*
 * Copyright (c) 1997,1998 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * twiddle.c -- compute twiddle factors
 * These are the twiddle factors for *direct* fft.  Flip sign to get
 * the inverse
 */

/* $Id: twiddle.c,v 1.2 1998/06/23 21:57:49 dcschmid Exp $ */
#ifdef FFTW_USING_CILK
#include <cilk.h>
#include <cilk-compat.h>
#endif

#include <fftw-int.h>
#include <math.h>
#include <stdlib.h>

#define FFTW_K2PI 6.28318530717958647692528676655900576839433879875021164194989

#ifndef TRUE
#define TRUE (1 == 1)
#endif

#ifndef FALSE
#define FALSE (1 == 0)
#endif

/*
 * compute the W coefficients (that is, powers of the root of 1)
 * and store them into an array.
 */
static FFTW_COMPLEX *fftw_compute_twiddle(int n, const fftw_codelet_desc *d)
{
     double twoPiOverN;
     int i, j;
     FFTW_COMPLEX *W;

     twoPiOverN = FFTW_K2PI / (double) n;

     if (d) {
	  int r = d->size;
	  int m = n / r;
	  int r1 = d->ntwiddle;

	  if (d->type != FFTW_TWIDDLE) 
	       fftw_die("compute_twiddle: illegal argument\n");

	  W = (FFTW_COMPLEX *)fftw_malloc(r1 * m * sizeof(FFTW_COMPLEX));
	  for (i = 0; i < m; ++i)
	       for (j = 0; j < r1; ++j) {
               int k = i * r1 + j;
	       double ij = (double)(i * d->twiddle_order[j]);
               c_re(W[k]) = cos(twoPiOverN * ij);
               c_im(W[k]) = -sin(twoPiOverN * ij);
          }
     } else {
	  /* generic codelet, needs all twiddles in order */
	  W = (FFTW_COMPLEX *)fftw_malloc(n * sizeof(FFTW_COMPLEX));
	  for (i = 0; i < n; ++i) {
	       c_re(W[i]) = cos(twoPiOverN * (double) i);
	       c_im(W[i]) = -sin(twoPiOverN * (double) i);
	  }

     }

     return W;
}

/*
 * these routines implement a simple reference-count-based 
 * management of twiddle structures
 */
static fftw_twiddle *twlist = (fftw_twiddle *) 0;
int fftw_twiddle_size = 0;	/* total allocated size, for debugging */

/* true if the two codelets can share the same twiddle factors */
static int compatible(const fftw_codelet_desc *d1, const fftw_codelet_desc *d2)
{
     int i;

     /* true if they are the same codelet */
     if (d1 == d2)
	  return TRUE;
     
     /* false if one is null and the other is not */
     if (!d1 || !d2)
	  return FALSE;

     /* false if size is different */
     if (d1->size != d2->size)
	  return FALSE;

     /* false if they need different # of twiddles */
     if (d1->ntwiddle != d2->ntwiddle)
	  return FALSE;

     /* false if the twiddle orders are different */
     for (i = 0; i < d1->ntwiddle; ++i)
	  if (d1->twiddle_order[i] != d2->twiddle_order[i])
	       return FALSE;

     return TRUE;
}

     
fftw_twiddle *fftw_create_twiddle(int n, const fftw_codelet_desc *d)
{
     fftw_twiddle *tw;

     /* lookup this n in the twiddle list */
     for (tw = twlist; tw; tw = tw->next)
	  if (n == tw->n && compatible(d, tw->cdesc)) {
	       ++tw->refcnt;
	       return tw;
	  }

     /* not found --- allocate a new struct twiddle */
     tw = (fftw_twiddle *) fftw_malloc(sizeof(fftw_twiddle));
     fftw_twiddle_size += n;

     tw->n = n;
     tw->cdesc = d;
     tw->twarray = fftw_compute_twiddle(n, d);
     tw->refcnt = 1;

     /* enqueue the new struct */
     tw->next = twlist;
     twlist = tw;

     return tw;
}

void fftw_destroy_twiddle(fftw_twiddle *tw)
{
     fftw_twiddle **p;
     --tw->refcnt;

     if (tw->refcnt == 0) {
	  /* remove from the list of known twiddle factors */
	  for (p = &twlist; p; p = &((*p)->next))
	       if (*p == tw) {
		    *p = tw->next;
		    fftw_twiddle_size -= tw->n;
		    fftw_free(tw->twarray);
		    fftw_free(tw);
		    return;
	       }
	  fftw_die("BUG in fftw_destroy_twiddle\n");
     }
}
