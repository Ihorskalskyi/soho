/*
 * Copyright (c) 1997,1998 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * timer.c -- this file measures the execution time of 
 *            ffts.  This information is used by the planner.
 */

/* $Id: timer.c,v 1.2 1998/06/23 21:57:48 dcschmid Exp $ */

#include <time.h>
#include <fftw-int.h>
#include <math.h>
#include <stdlib.h>

/* define this to 1 to spit out all sorts of timing data */
#define FFTW_OUTPUT_TIMING 0

#if FFTW_OUTPUT_TIMING

/* For debugging and/or diagnostics, print out timing results: */

static void output_timing_results(fftw_plan p, int n, double tmin, double tmax)
{
     FILE *f;
     fftw_op_count ops;
     int fp_ops;
     double fp_ops_est;

     tmin *= 1.0e6; /* times in microseconds */
     tmax *= 1.0e6; 

     f = fopen("fftw_timer.dat", "a");
     if (f == 0) {
	  perror("Error opening timer data file");
	  fftw_die("can't write timer diagnostics");
     }

     fftw_count_plan_ops(p,&ops);

     fp_ops_est = 5.0 * n * log(n) * 1.442695041;
     fp_ops = ops.fp_additions + ops.fp_multiplications;

     fprintf(f,"%d, %d,%d,%d,%d, %g,%d, %g,%g %g,%g\n",
	     n,
	     ops.fp_additions, ops.fp_multiplications,
             ops.vars, ops.memory_accesses,
	     fp_ops_est, fp_ops,
	     tmin, tmax,
	     fp_ops_est / tmin,
	     fp_ops / tmin);

     fclose(f);
}
#else
#define output_timing_results(p, n, tmin, tmax)
#endif

static void fftw_init_test_array(FFTW_COMPLEX *arr, int stride, int n)
{
     int j;
     
     for (j = 0; j < n; ++j) {
	  c_re(arr[stride * j]) = 0.0;
	  c_im(arr[stride * j]) = 0.0;
     }
}

/*
 * The timer keeps doubling the number of iterations
 * until the program runs for more than FFTW_TIME_MIN
 */
double fftw_measure_runtime(fftw_plan plan,
			    FFTW_COMPLEX *in, int istride,
			    FFTW_COMPLEX *out, int ostride)
{
     fftw_time begin, end, start;
     double t, tmax, tmin;
     int i, iter;
     int n;
     int repeat;

     n = plan->n;

     iter = 1;

     for (;;) {
	  tmin = 1.0E10;
	  tmax = -1.0E10;
	  fftw_init_test_array(in, istride, n);

	  start = fftw_get_time();
	  /* repeat the measurement FFTW_TIME_REPEAT times */
	  for (repeat = 0; repeat < FFTW_TIME_REPEAT; ++repeat) {
	       begin = fftw_get_time();
	       for (i = 0; i < iter; ++i) {
		    fftw(plan, 1, in, istride, 0, out, ostride, 0);
	       }
	       end = fftw_get_time();

	       t = fftw_time_to_sec(fftw_time_diff(end, begin));
	       if (t < tmin)
		    tmin = t;
	       if (t > tmax)
		    tmax = t;

	       /* do not run for too long */
	       t = fftw_time_to_sec(fftw_time_diff(end, start));
	       if (t > FFTW_TIME_LIMIT)
		    break;
	  }

	  if (tmin >= FFTW_TIME_MIN) 
	       break;

	  iter *= 2;
     }
	  

     tmin /= (double) iter;
     tmax /= (double) iter;

     output_timing_results(plan, n, tmin, tmax);

     return tmin;
}


/*
 * Same as fftw_measure_runtime, except for fftwnd plan.
 */
double fftwnd_measure_runtime(fftwnd_plan plan,
			      FFTW_COMPLEX *in, int istride,
			      FFTW_COMPLEX *out, int ostride)
{
     fftw_time begin, end, start;
     double t, tmax, tmin;
     int i, iter;
     int n;
     int repeat;

     if (plan->rank == 0)
	  return 0.0;

     n = 1;
     for (i = 0; i < plan->rank; ++i)
	  n *= plan->n[i];

     iter = 1;

     for (;;) {
	  tmin = 1.0E10;
	  tmax = -1.0E10;
	  fftw_init_test_array(in, istride, n);

	  start = fftw_get_time();
	  /* repeat the measurement FFTW_TIME_REPEAT times */
	  for (repeat = 0; repeat < FFTW_TIME_REPEAT; ++repeat) {
	       begin = fftw_get_time();
	       for (i = 0; i < iter; ++i) {
		    fftwnd(plan, 1, in, istride, 0, out, ostride, 0);
	       }
	       end = fftw_get_time();

	       t = fftw_time_to_sec(fftw_time_diff(end, begin));
	       if (t < tmin)
		    tmin = t;
	       if (t > tmax)
		    tmax = t;

	       /* do not run for too long */
	       t = fftw_time_to_sec(fftw_time_diff(end, start));
	       if (t > FFTW_TIME_LIMIT)
		    break;
	  }

	  if (tmin >= FFTW_TIME_MIN) 
	       break;

	  iter *= 2;
     }
	  

     tmin /= (double) iter;
     tmax /= (double) iter;

     return tmin;
}

/********************* System-specific Timing Support *********************/

#if defined(HAVE_MAC_TIMER) && !defined(HAVE_MAC_PCI_TIMER)

/* Use Macintosh Time Manager to get the time: */

#pragma only_std_keywords off	/* 
				 * make sure compiler (CW) recognizes the
				 * pascal keywords that are in Timer.h 
				 */

#include <Timer.h>

#pragma only_std_keywords reset

fftw_time get_Mac_microseconds(void)
{
     fftw_time t;
     UnsignedWide microsec;	/* 
				 * microsec.lo and microsec.hi are
				 * unsigned long's, and are the two parts
				 * of a 64 bit unsigned integer 
				 */

     Microseconds(&microsec);	/* get time in microseconds */

     /* store lo and hi words into our structure: */
     t.lo = microsec.lo;
     t.hi = microsec.hi;

     return t;
}

fftw_time fftw_time_diff(fftw_time t1, fftw_time t2)
/*
 * This function takes the difference t1 - t2 of two 64 bit
 * integers, represented by the 32 bit lo and hi words.
 * if t1 < t2, returns 0. 
 */
{
     fftw_time diff;

     if (t1.hi < t2.hi) {	/* something is wrong...t1 < t2! */
	  diff.hi = diff.lo = 0;
	  return diff;
     } else
	  diff.hi = t1.hi - t2.hi;

     if (t1.lo < t2.lo) {
	  if (diff.hi > 0)
	       diff.hi -= 1;	/* carry */
	  else {		/* something is wrong...t1 < t2! */
	       diff.hi = diff.lo = 0;
	       return diff;
	  }
     }
     diff.lo = t1.lo - t2.lo;

     return diff;
}

#endif

#ifdef HAVE_WIN32_TIMER
#include <windows.h>

static LARGE_INTEGER gFreq;
static int gHaveHiResTimer = 0;
static int gFirstTime = 1;

unsigned long GetPerfTime(void)
{
     LARGE_INTEGER lCounter;

     if (gFirstTime) {
	  gFirstTime = 0;

	  if (QueryPerformanceFrequency(&gFreq)) {
	       gHaveHiResTimer = 1;
	  }
     }
     if (gHaveHiResTimer) {
	  QueryPerformanceCounter(&lCounter);
	  return lCounter.u.LowPart;
     } else {
	  return (unsigned long) clock();
     }
}

double GetPerfSec(double pTime)
{
     if (gHaveHiResTimer) {
	  return pTime / gFreq.u.LowPart;	// assumes HighPart==0

     } else {
	  return pTime / CLOCKS_PER_SEC;
     }
}

#endif				/* HAVE_WIN32_TIMER */

#if defined(FFTW_USE_GETTIMEOFDAY)

/* timer support routines for systems having gettimeofday */

#ifdef HAVE_BSDGETTIMEOFDAY
#define gettimeofday BSDgettimeofday
#endif

fftw_time fftw_gettimeofday_get_time(void)
{
     struct timeval tv;
     gettimeofday(&tv, 0);
     return tv;
}

fftw_time fftw_gettimeofday_time_diff(fftw_time t1, fftw_time t2)
{
     fftw_time diff;

     diff.tv_sec = t1.tv_sec - t2.tv_sec;
     diff.tv_usec = t1.tv_usec - t2.tv_usec;
     /* normalize */
     while (diff.tv_usec < 0) {
	  diff.tv_usec += 1000000L;
	  diff.tv_sec -= 1;
     }
     
     return diff;
}
#endif
