/*
 * Copyright (c) 1997,1998 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* This file has been automatically generated --- DO NOT EDIT */

#include <fftw-int.h>
#include <fftw.h>

/* $Id: fni_6.c,v 1.2 1998/06/23 21:57:25 dcschmid Exp $ */

/* Generated by: ./genfft -magic-twiddle-load-all -magic-variables 4 -magic-loopi -notwiddleinv 6 */

/* This function contains 36 FP additions, 8 FP multiplications, */
/* 20 stack variables, and 24 memory accesses */
static const FFTW_REAL K866025403 = ((FFTW_REAL) +0.866025403784438646763723170752936183471402627);
static const FFTW_REAL K500000000 = ((FFTW_REAL) +0.500000000000000000000000000000000000000000000);

void fftwi_no_twiddle_6(const FFTW_COMPLEX *input, FFTW_COMPLEX *output, int istride, int ostride)
{
     FFTW_REAL tmp3;
     FFTW_REAL tmp21;
     FFTW_REAL tmp13;
     FFTW_REAL tmp29;
     FFTW_REAL tmp6;
     FFTW_REAL tmp22;
     FFTW_REAL tmp9;
     FFTW_REAL tmp23;
     FFTW_REAL tmp10;
     FFTW_REAL tmp24;
     FFTW_REAL tmp16;
     FFTW_REAL tmp27;
     FFTW_REAL tmp19;
     FFTW_REAL tmp26;
     FFTW_REAL tmp20;
     FFTW_REAL tmp30;
     {
	  FFTW_REAL tmp1;
	  FFTW_REAL tmp2;
	  FFTW_REAL tmp11;
	  FFTW_REAL tmp12;
	  tmp1 = c_re(input[0]);
	  tmp2 = c_re(input[3 * istride]);
	  tmp3 = tmp1 + tmp2;
	  tmp21 = tmp1 - tmp2;
	  tmp11 = c_im(input[0]);
	  tmp12 = c_im(input[3 * istride]);
	  tmp13 = tmp11 + tmp12;
	  tmp29 = tmp11 - tmp12;
     }
     {
	  FFTW_REAL tmp4;
	  FFTW_REAL tmp5;
	  FFTW_REAL tmp7;
	  FFTW_REAL tmp8;
	  tmp4 = c_re(input[2 * istride]);
	  tmp5 = c_re(input[5 * istride]);
	  tmp6 = tmp4 + tmp5;
	  tmp22 = tmp4 - tmp5;
	  tmp7 = c_re(input[4 * istride]);
	  tmp8 = c_re(input[istride]);
	  tmp9 = tmp7 + tmp8;
	  tmp23 = tmp7 - tmp8;
     }
     tmp10 = tmp6 + tmp9;
     tmp24 = tmp22 + tmp23;
     {
	  FFTW_REAL tmp14;
	  FFTW_REAL tmp15;
	  FFTW_REAL tmp17;
	  FFTW_REAL tmp18;
	  tmp14 = c_im(input[2 * istride]);
	  tmp15 = c_im(input[5 * istride]);
	  tmp16 = tmp14 + tmp15;
	  tmp27 = tmp14 - tmp15;
	  tmp17 = c_im(input[4 * istride]);
	  tmp18 = c_im(input[istride]);
	  tmp19 = tmp17 + tmp18;
	  tmp26 = tmp17 - tmp18;
     }
     tmp20 = tmp16 + tmp19;
     tmp30 = tmp27 + tmp26;
     {
	  FFTW_REAL tmp33;
	  FFTW_REAL tmp34;
	  FFTW_REAL tmp35;
	  FFTW_REAL tmp36;
	  c_re(output[0]) = tmp3 + tmp10;
	  tmp33 = tmp3 - (K500000000 * tmp10);
	  tmp34 = K866025403 * (tmp19 - tmp16);
	  c_re(output[2 * ostride]) = tmp33 - tmp34;
	  c_re(output[4 * ostride]) = tmp33 + tmp34;
	  c_im(output[0]) = tmp13 + tmp20;
	  tmp35 = tmp13 - (K500000000 * tmp20);
	  tmp36 = K866025403 * (tmp6 - tmp9);
	  c_im(output[2 * ostride]) = tmp35 - tmp36;
	  c_im(output[4 * ostride]) = tmp35 + tmp36;
     }
     {
	  FFTW_REAL tmp25;
	  FFTW_REAL tmp28;
	  FFTW_REAL tmp31;
	  FFTW_REAL tmp32;
	  c_re(output[3 * ostride]) = tmp21 + tmp24;
	  tmp25 = tmp21 - (K500000000 * tmp24);
	  tmp28 = K866025403 * (tmp26 - tmp27);
	  c_re(output[ostride]) = tmp25 + tmp28;
	  c_re(output[5 * ostride]) = tmp25 - tmp28;
	  c_im(output[3 * ostride]) = tmp29 + tmp30;
	  tmp31 = tmp29 - (K500000000 * tmp30);
	  tmp32 = K866025403 * (tmp22 - tmp23);
	  c_im(output[ostride]) = tmp31 + tmp32;
	  c_im(output[5 * ostride]) = tmp31 - tmp32;
     }
}

fftw_codelet_desc fftwi_no_twiddle_6_desc =
{
     "fftwi_no_twiddle_6",
     (void (*)()) fftwi_no_twiddle_6,
     6,
     FFTW_BACKWARD,
     FFTW_NOTW,
     27,
     0,
     (const int *) 0,
     {36, 8, 20, 24}
};
