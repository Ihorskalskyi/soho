/*
 * Copyright (c) 1997,1998 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* This file has been automatically generated --- DO NOT EDIT */

#include <fftw-int.h>
#include <fftw.h>

/* $Id: fn_13.c,v 1.2 1998/06/23 21:57:08 dcschmid Exp $ */

/* Generated by: ./genfft -magic-twiddle-load-all -magic-variables 4 -magic-loopi -notwiddle 13 */

/* This function contains 216 FP additions, 76 FP multiplications, */
/* 66 stack variables, and 52 memory accesses */
static const FFTW_REAL K011599105 = ((FFTW_REAL) +0.011599105605768290721655456654083252189827041);
static const FFTW_REAL K300238635 = ((FFTW_REAL) +0.300238635966332641462884626667381504676006424);
static const FFTW_REAL K156891391 = ((FFTW_REAL) +0.156891391051584611046832726756003269660212636);
static const FFTW_REAL K256247671 = ((FFTW_REAL) +0.256247671582936600958684654061725059144125175);
static const FFTW_REAL K287570364 = ((FFTW_REAL) +0.287570364737001560684192773727726694230500804);
static const FFTW_REAL K087069300 = ((FFTW_REAL) +0.087069300576067952502830397464632371308482338);
static const FFTW_REAL K290717241 = ((FFTW_REAL) +0.290717241470841097409665469769578623301993740);
static const FFTW_REAL K075902986 = ((FFTW_REAL) +0.075902986037193865983102897245103540356428373);
static const FFTW_REAL K083333333 = ((FFTW_REAL) +0.083333333333333333333333333333333333333333333);
static const FFTW_REAL K153555685 = ((FFTW_REAL) +0.153555685579541400120879959489337734301614667);
static const FFTW_REAL K258260390 = ((FFTW_REAL) +0.258260390311744861420450644284508567852516811);
static const FFTW_REAL K300462606 = ((FFTW_REAL) +0.300462606288665774426601772289207995520941381);
static const FFTW_REAL K866025403 = ((FFTW_REAL) +0.866025403784438646763723170752936183471402627);
static const FFTW_REAL K500000000 = ((FFTW_REAL) +0.500000000000000000000000000000000000000000000);

void fftw_no_twiddle_13(const FFTW_COMPLEX *input, FFTW_COMPLEX *output, int istride, int ostride)
{
     FFTW_REAL tmp1;
     FFTW_REAL tmp25;
     FFTW_REAL tmp24;
     FFTW_REAL tmp98;
     FFTW_REAL tmp113;
     FFTW_REAL tmp131;
     FFTW_REAL tmp48;
     FFTW_REAL tmp148;
     FFTW_REAL tmp110;
     FFTW_REAL tmp132;
     FFTW_REAL tmp87;
     FFTW_REAL tmp102;
     FFTW_REAL tmp94;
     FFTW_REAL tmp103;
     FFTW_REAL tmp117;
     FFTW_REAL tmp137;
     FFTW_REAL tmp120;
     FFTW_REAL tmp138;
     FFTW_REAL tmp64;
     FFTW_REAL tmp99;
     FFTW_REAL tmp79;
     FFTW_REAL tmp100;
     FFTW_REAL tmp124;
     FFTW_REAL tmp135;
     FFTW_REAL tmp127;
     FFTW_REAL tmp134;
     tmp1 = c_re(input[0]);
     tmp25 = c_im(input[0]);
     {
	  FFTW_REAL tmp6;
	  FFTW_REAL tmp50;
	  FFTW_REAL tmp66;
	  FFTW_REAL tmp30;
	  FFTW_REAL tmp65;
	  FFTW_REAL tmp51;
	  FFTW_REAL tmp11;
	  FFTW_REAL tmp53;
	  FFTW_REAL tmp69;
	  FFTW_REAL tmp35;
	  FFTW_REAL tmp68;
	  FFTW_REAL tmp54;
	  FFTW_REAL tmp17;
	  FFTW_REAL tmp57;
	  FFTW_REAL tmp73;
	  FFTW_REAL tmp41;
	  FFTW_REAL tmp72;
	  FFTW_REAL tmp58;
	  FFTW_REAL tmp22;
	  FFTW_REAL tmp60;
	  FFTW_REAL tmp76;
	  FFTW_REAL tmp46;
	  FFTW_REAL tmp75;
	  FFTW_REAL tmp61;
	  {
	       FFTW_REAL tmp2;
	       FFTW_REAL tmp3;
	       FFTW_REAL tmp4;
	       FFTW_REAL tmp5;
	       tmp2 = c_re(input[istride]);
	       tmp3 = c_re(input[3 * istride]);
	       tmp4 = c_re(input[9 * istride]);
	       tmp5 = tmp3 + tmp4;
	       tmp6 = tmp2 + tmp5;
	       tmp50 = tmp2 - (K500000000 * tmp5);
	       tmp66 = K866025403 * (tmp3 - tmp4);
	  }
	  {
	       FFTW_REAL tmp26;
	       FFTW_REAL tmp27;
	       FFTW_REAL tmp28;
	       FFTW_REAL tmp29;
	       tmp26 = c_im(input[istride]);
	       tmp27 = c_im(input[3 * istride]);
	       tmp28 = c_im(input[9 * istride]);
	       tmp29 = tmp27 + tmp28;
	       tmp30 = tmp26 + tmp29;
	       tmp65 = tmp26 - (K500000000 * tmp29);
	       tmp51 = K866025403 * (tmp28 - tmp27);
	  }
	  {
	       FFTW_REAL tmp7;
	       FFTW_REAL tmp8;
	       FFTW_REAL tmp9;
	       FFTW_REAL tmp10;
	       tmp7 = c_re(input[12 * istride]);
	       tmp8 = c_re(input[10 * istride]);
	       tmp9 = c_re(input[4 * istride]);
	       tmp10 = tmp8 + tmp9;
	       tmp11 = tmp7 + tmp10;
	       tmp53 = tmp7 - (K500000000 * tmp10);
	       tmp69 = K866025403 * (tmp8 - tmp9);
	  }
	  {
	       FFTW_REAL tmp31;
	       FFTW_REAL tmp32;
	       FFTW_REAL tmp33;
	       FFTW_REAL tmp34;
	       tmp31 = c_im(input[12 * istride]);
	       tmp32 = c_im(input[10 * istride]);
	       tmp33 = c_im(input[4 * istride]);
	       tmp34 = tmp32 + tmp33;
	       tmp35 = tmp31 + tmp34;
	       tmp68 = tmp31 - (K500000000 * tmp34);
	       tmp54 = K866025403 * (tmp33 - tmp32);
	  }
	  {
	       FFTW_REAL tmp13;
	       FFTW_REAL tmp14;
	       FFTW_REAL tmp15;
	       FFTW_REAL tmp16;
	       tmp13 = c_re(input[8 * istride]);
	       tmp14 = c_re(input[11 * istride]);
	       tmp15 = c_re(input[7 * istride]);
	       tmp16 = tmp14 + tmp15;
	       tmp17 = tmp13 + tmp16;
	       tmp57 = tmp13 - (K500000000 * tmp16);
	       tmp73 = K866025403 * (tmp14 - tmp15);
	  }
	  {
	       FFTW_REAL tmp37;
	       FFTW_REAL tmp38;
	       FFTW_REAL tmp39;
	       FFTW_REAL tmp40;
	       tmp37 = c_im(input[8 * istride]);
	       tmp38 = c_im(input[11 * istride]);
	       tmp39 = c_im(input[7 * istride]);
	       tmp40 = tmp38 + tmp39;
	       tmp41 = tmp37 + tmp40;
	       tmp72 = tmp37 - (K500000000 * tmp40);
	       tmp58 = K866025403 * (tmp39 - tmp38);
	  }
	  {
	       FFTW_REAL tmp18;
	       FFTW_REAL tmp19;
	       FFTW_REAL tmp20;
	       FFTW_REAL tmp21;
	       tmp18 = c_re(input[5 * istride]);
	       tmp19 = c_re(input[2 * istride]);
	       tmp20 = c_re(input[6 * istride]);
	       tmp21 = tmp19 + tmp20;
	       tmp22 = tmp18 + tmp21;
	       tmp60 = tmp18 - (K500000000 * tmp21);
	       tmp76 = K866025403 * (tmp19 - tmp20);
	  }
	  {
	       FFTW_REAL tmp42;
	       FFTW_REAL tmp43;
	       FFTW_REAL tmp44;
	       FFTW_REAL tmp45;
	       tmp42 = c_im(input[5 * istride]);
	       tmp43 = c_im(input[2 * istride]);
	       tmp44 = c_im(input[6 * istride]);
	       tmp45 = tmp43 + tmp44;
	       tmp46 = tmp42 + tmp45;
	       tmp75 = tmp42 - (K500000000 * tmp45);
	       tmp61 = K866025403 * (tmp44 - tmp43);
	  }
	  {
	       FFTW_REAL tmp12;
	       FFTW_REAL tmp23;
	       FFTW_REAL tmp108;
	       FFTW_REAL tmp109;
	       tmp12 = tmp6 + tmp11;
	       tmp23 = tmp17 + tmp22;
	       tmp24 = tmp12 + tmp23;
	       tmp98 = K300462606 * (tmp12 - tmp23);
	       {
		    FFTW_REAL tmp111;
		    FFTW_REAL tmp112;
		    FFTW_REAL tmp36;
		    FFTW_REAL tmp47;
		    tmp111 = tmp6 - tmp11;
		    tmp112 = tmp41 - tmp46;
		    tmp113 = tmp111 + tmp112;
		    tmp131 = tmp111 - tmp112;
		    tmp36 = tmp30 + tmp35;
		    tmp47 = tmp41 + tmp46;
		    tmp48 = tmp36 + tmp47;
		    tmp148 = K300462606 * (tmp36 - tmp47);
	       }
	       tmp108 = tmp30 - tmp35;
	       tmp109 = tmp17 - tmp22;
	       tmp110 = tmp108 - tmp109;
	       tmp132 = tmp108 + tmp109;
	       {
		    FFTW_REAL tmp83;
		    FFTW_REAL tmp115;
		    FFTW_REAL tmp93;
		    FFTW_REAL tmp116;
		    FFTW_REAL tmp86;
		    FFTW_REAL tmp119;
		    FFTW_REAL tmp90;
		    FFTW_REAL tmp118;
		    {
			 FFTW_REAL tmp81;
			 FFTW_REAL tmp82;
			 FFTW_REAL tmp91;
			 FFTW_REAL tmp92;
			 tmp81 = tmp50 - tmp51;
			 tmp82 = tmp53 - tmp54;
			 tmp83 = tmp81 + tmp82;
			 tmp115 = tmp81 - tmp82;
			 tmp91 = tmp72 - tmp73;
			 tmp92 = tmp75 - tmp76;
			 tmp93 = tmp91 + tmp92;
			 tmp116 = tmp91 - tmp92;
		    }
		    {
			 FFTW_REAL tmp84;
			 FFTW_REAL tmp85;
			 FFTW_REAL tmp88;
			 FFTW_REAL tmp89;
			 tmp84 = tmp57 - tmp58;
			 tmp85 = tmp60 - tmp61;
			 tmp86 = tmp84 + tmp85;
			 tmp119 = tmp84 - tmp85;
			 tmp88 = tmp65 - tmp66;
			 tmp89 = tmp68 - tmp69;
			 tmp90 = tmp88 + tmp89;
			 tmp118 = tmp88 - tmp89;
		    }
		    tmp87 = tmp83 + tmp86;
		    tmp102 = tmp83 - tmp86;
		    tmp94 = tmp90 + tmp93;
		    tmp103 = tmp90 - tmp93;
		    tmp117 = tmp115 + tmp116;
		    tmp137 = tmp115 - tmp116;
		    tmp120 = tmp118 - tmp119;
		    tmp138 = tmp118 + tmp119;
	       }
	       {
		    FFTW_REAL tmp56;
		    FFTW_REAL tmp122;
		    FFTW_REAL tmp78;
		    FFTW_REAL tmp123;
		    FFTW_REAL tmp63;
		    FFTW_REAL tmp126;
		    FFTW_REAL tmp71;
		    FFTW_REAL tmp125;
		    {
			 FFTW_REAL tmp52;
			 FFTW_REAL tmp55;
			 FFTW_REAL tmp74;
			 FFTW_REAL tmp77;
			 tmp52 = tmp50 + tmp51;
			 tmp55 = tmp53 + tmp54;
			 tmp56 = tmp52 + tmp55;
			 tmp122 = tmp52 - tmp55;
			 tmp74 = tmp72 + tmp73;
			 tmp77 = tmp75 + tmp76;
			 tmp78 = tmp74 + tmp77;
			 tmp123 = tmp74 - tmp77;
		    }
		    {
			 FFTW_REAL tmp59;
			 FFTW_REAL tmp62;
			 FFTW_REAL tmp67;
			 FFTW_REAL tmp70;
			 tmp59 = tmp57 + tmp58;
			 tmp62 = tmp60 + tmp61;
			 tmp63 = tmp59 + tmp62;
			 tmp126 = tmp59 - tmp62;
			 tmp67 = tmp65 + tmp66;
			 tmp70 = tmp68 + tmp69;
			 tmp71 = tmp67 + tmp70;
			 tmp125 = tmp67 - tmp70;
		    }
		    tmp64 = tmp56 + tmp63;
		    tmp99 = tmp56 - tmp63;
		    tmp79 = tmp71 + tmp78;
		    tmp100 = tmp71 - tmp78;
		    tmp124 = tmp122 + tmp123;
		    tmp135 = tmp122 - tmp123;
		    tmp127 = tmp125 - tmp126;
		    tmp134 = tmp125 + tmp126;
	       }
	  }
     }
     c_re(output[0]) = tmp1 + tmp24;
     c_im(output[0]) = tmp25 + tmp48;
     {
	  FFTW_REAL tmp106;
	  FFTW_REAL tmp168;
	  FFTW_REAL tmp183;
	  FFTW_REAL tmp152;
	  FFTW_REAL tmp182;
	  FFTW_REAL tmp169;
	  FFTW_REAL tmp97;
	  FFTW_REAL tmp165;
	  FFTW_REAL tmp180;
	  FFTW_REAL tmp147;
	  FFTW_REAL tmp179;
	  FFTW_REAL tmp166;
	  FFTW_REAL tmp130;
	  FFTW_REAL tmp186;
	  FFTW_REAL tmp173;
	  FFTW_REAL tmp158;
	  FFTW_REAL tmp172;
	  FFTW_REAL tmp187;
	  FFTW_REAL tmp141;
	  FFTW_REAL tmp189;
	  FFTW_REAL tmp176;
	  FFTW_REAL tmp163;
	  FFTW_REAL tmp175;
	  FFTW_REAL tmp190;
	  {
	       FFTW_REAL tmp101;
	       FFTW_REAL tmp104;
	       FFTW_REAL tmp105;
	       FFTW_REAL tmp149;
	       FFTW_REAL tmp150;
	       FFTW_REAL tmp151;
	       tmp101 = (K258260390 * tmp99) - (K153555685 * tmp100);
	       tmp104 = (K258260390 * tmp102) + (K153555685 * tmp103);
	       tmp105 = tmp101 + tmp104;
	       tmp106 = tmp98 + tmp105;
	       tmp168 = tmp98 - (K500000000 * tmp105);
	       tmp183 = K866025403 * (tmp104 - tmp101);
	       tmp149 = (K153555685 * tmp99) + (K258260390 * tmp100);
	       tmp150 = (K258260390 * tmp103) - (K153555685 * tmp102);
	       tmp151 = tmp149 + tmp150;
	       tmp152 = tmp148 + tmp151;
	       tmp182 = tmp148 - (K500000000 * tmp151);
	       tmp169 = K866025403 * (tmp149 - tmp150);
	  }
	  {
	       FFTW_REAL tmp49;
	       FFTW_REAL tmp80;
	       FFTW_REAL tmp95;
	       FFTW_REAL tmp96;
	       tmp49 = tmp1 - (K083333333 * tmp24);
	       tmp80 = (K075902986 * tmp64) + (K290717241 * tmp79);
	       tmp95 = (K075902986 * tmp87) - (K290717241 * tmp94);
	       tmp96 = tmp80 + tmp95;
	       tmp97 = tmp49 + tmp96;
	       tmp165 = tmp49 - (K500000000 * tmp96);
	       tmp180 = K866025403 * (tmp95 - tmp80);
	  }
	  {
	       FFTW_REAL tmp143;
	       FFTW_REAL tmp144;
	       FFTW_REAL tmp145;
	       FFTW_REAL tmp146;
	       tmp143 = tmp25 - (K083333333 * tmp48);
	       tmp144 = (K075902986 * tmp79) - (K290717241 * tmp64);
	       tmp145 = (K290717241 * tmp87) + (K075902986 * tmp94);
	       tmp146 = tmp144 + tmp145;
	       tmp147 = tmp143 + tmp146;
	       tmp179 = tmp143 - (K500000000 * tmp146);
	       tmp166 = K866025403 * (tmp144 - tmp145);
	  }
	  {
	       FFTW_REAL tmp114;
	       FFTW_REAL tmp121;
	       FFTW_REAL tmp128;
	       FFTW_REAL tmp129;
	       tmp114 = (K087069300 * tmp110) - (K287570364 * tmp113);
	       tmp121 = (K256247671 * tmp117) + (K156891391 * tmp120);
	       tmp128 = (K300238635 * tmp124) + (K011599105 * tmp127);
	       tmp129 = tmp121 - tmp128;
	       tmp130 = tmp114 + tmp129;
	       tmp186 = tmp114 - (K500000000 * tmp129);
	       tmp173 = K866025403 * (tmp128 + tmp121);
	  }
	  {
	       FFTW_REAL tmp157;
	       FFTW_REAL tmp154;
	       FFTW_REAL tmp155;
	       FFTW_REAL tmp156;
	       tmp157 = (K087069300 * tmp113) + (K287570364 * tmp110);
	       tmp154 = (K011599105 * tmp124) - (K300238635 * tmp127);
	       tmp155 = (K256247671 * tmp120) - (K156891391 * tmp117);
	       tmp156 = tmp154 + tmp155;
	       tmp158 = tmp156 - tmp157;
	       tmp172 = tmp157 + (K500000000 * tmp156);
	       tmp187 = K866025403 * (tmp154 - tmp155);
	  }
	  {
	       FFTW_REAL tmp133;
	       FFTW_REAL tmp136;
	       FFTW_REAL tmp139;
	       FFTW_REAL tmp140;
	       tmp133 = (K287570364 * tmp131) + (K087069300 * tmp132);
	       tmp136 = (K156891391 * tmp134) - (K256247671 * tmp135);
	       tmp139 = (K300238635 * tmp137) - (K011599105 * tmp138);
	       tmp140 = tmp136 + tmp139;
	       tmp141 = tmp133 + tmp140;
	       tmp189 = tmp133 - (K500000000 * tmp140);
	       tmp176 = K866025403 * (tmp139 - tmp136);
	  }
	  {
	       FFTW_REAL tmp159;
	       FFTW_REAL tmp160;
	       FFTW_REAL tmp161;
	       FFTW_REAL tmp162;
	       tmp159 = (K287570364 * tmp132) - (K087069300 * tmp131);
	       tmp160 = (K011599105 * tmp137) + (K300238635 * tmp138);
	       tmp161 = (K156891391 * tmp135) + (K256247671 * tmp134);
	       tmp162 = tmp160 - tmp161;
	       tmp163 = tmp159 + tmp162;
	       tmp175 = tmp159 - (K500000000 * tmp162);
	       tmp190 = K866025403 * (tmp161 + tmp160);
	  }
	  {
	       FFTW_REAL tmp107;
	       FFTW_REAL tmp142;
	       FFTW_REAL tmp209;
	       FFTW_REAL tmp210;
	       tmp107 = tmp97 + tmp106;
	       tmp142 = tmp130 + tmp141;
	       c_re(output[ostride]) = tmp107 + tmp142;
	       c_re(output[12 * ostride]) = tmp107 - tmp142;
	       {
		    FFTW_REAL tmp211;
		    FFTW_REAL tmp212;
		    FFTW_REAL tmp153;
		    FFTW_REAL tmp164;
		    tmp211 = tmp147 - tmp152;
		    tmp212 = tmp130 - tmp141;
		    c_im(output[5 * ostride]) = tmp211 + tmp212;
		    c_im(output[8 * ostride]) = tmp211 - tmp212;
		    tmp153 = tmp147 + tmp152;
		    tmp164 = tmp158 + tmp163;
		    c_im(output[ostride]) = tmp153 + tmp164;
		    c_im(output[12 * ostride]) = tmp153 - tmp164;
	       }
	       tmp209 = tmp97 - tmp106;
	       tmp210 = tmp158 - tmp163;
	       c_re(output[5 * ostride]) = tmp209 - tmp210;
	       c_re(output[8 * ostride]) = tmp209 + tmp210;
	       {
		    FFTW_REAL tmp199;
		    FFTW_REAL tmp213;
		    FFTW_REAL tmp208;
		    FFTW_REAL tmp214;
		    FFTW_REAL tmp202;
		    FFTW_REAL tmp216;
		    FFTW_REAL tmp205;
		    FFTW_REAL tmp215;
		    {
			 FFTW_REAL tmp197;
			 FFTW_REAL tmp198;
			 FFTW_REAL tmp206;
			 FFTW_REAL tmp207;
			 tmp197 = tmp165 + tmp166;
			 tmp198 = tmp168 + tmp169;
			 tmp199 = tmp197 + tmp198;
			 tmp213 = tmp197 - tmp198;
			 tmp206 = tmp173 - tmp172;
			 tmp207 = tmp175 + tmp176;
			 tmp208 = tmp206 + tmp207;
			 tmp214 = tmp206 - tmp207;
		    }
		    {
			 FFTW_REAL tmp200;
			 FFTW_REAL tmp201;
			 FFTW_REAL tmp203;
			 FFTW_REAL tmp204;
			 tmp200 = tmp186 + tmp187;
			 tmp201 = tmp189 - tmp190;
			 tmp202 = tmp200 + tmp201;
			 tmp216 = tmp200 - tmp201;
			 tmp203 = tmp179 + tmp180;
			 tmp204 = tmp182 + tmp183;
			 tmp205 = tmp203 + tmp204;
			 tmp215 = tmp203 - tmp204;
		    }
		    c_re(output[4 * ostride]) = tmp199 - tmp202;
		    c_re(output[9 * ostride]) = tmp199 + tmp202;
		    c_im(output[4 * ostride]) = tmp205 - tmp208;
		    c_im(output[9 * ostride]) = tmp205 + tmp208;
		    c_re(output[6 * ostride]) = tmp213 - tmp214;
		    c_re(output[7 * ostride]) = tmp213 + tmp214;
		    c_im(output[6 * ostride]) = tmp215 + tmp216;
		    c_im(output[7 * ostride]) = tmp215 - tmp216;
	       }
	       {
		    FFTW_REAL tmp171;
		    FFTW_REAL tmp193;
		    FFTW_REAL tmp192;
		    FFTW_REAL tmp194;
		    FFTW_REAL tmp178;
		    FFTW_REAL tmp196;
		    FFTW_REAL tmp185;
		    FFTW_REAL tmp195;
		    {
			 FFTW_REAL tmp167;
			 FFTW_REAL tmp170;
			 FFTW_REAL tmp188;
			 FFTW_REAL tmp191;
			 tmp167 = tmp165 - tmp166;
			 tmp170 = tmp168 - tmp169;
			 tmp171 = tmp167 - tmp170;
			 tmp193 = tmp167 + tmp170;
			 tmp188 = tmp186 - tmp187;
			 tmp191 = tmp189 + tmp190;
			 tmp192 = tmp188 - tmp191;
			 tmp194 = tmp188 + tmp191;
		    }
		    {
			 FFTW_REAL tmp174;
			 FFTW_REAL tmp177;
			 FFTW_REAL tmp181;
			 FFTW_REAL tmp184;
			 tmp174 = tmp172 + tmp173;
			 tmp177 = tmp175 - tmp176;
			 tmp178 = tmp174 + tmp177;
			 tmp196 = tmp177 - tmp174;
			 tmp181 = tmp179 - tmp180;
			 tmp184 = tmp182 - tmp183;
			 tmp185 = tmp181 - tmp184;
			 tmp195 = tmp181 + tmp184;
		    }
		    c_re(output[2 * ostride]) = tmp171 + tmp178;
		    c_re(output[11 * ostride]) = tmp171 - tmp178;
		    c_im(output[2 * ostride]) = tmp185 + tmp192;
		    c_im(output[11 * ostride]) = tmp185 - tmp192;
		    c_re(output[3 * ostride]) = tmp193 + tmp194;
		    c_re(output[10 * ostride]) = tmp193 - tmp194;
		    c_im(output[3 * ostride]) = tmp195 + tmp196;
		    c_im(output[10 * ostride]) = tmp195 - tmp196;
	       }
	  }
     }
}

fftw_codelet_desc fftw_no_twiddle_13_desc =
{
     "fftw_no_twiddle_13",
     (void (*)()) fftw_no_twiddle_13,
     13,
     FFTW_FORWARD,
     FFTW_NOTW,
     53,
     0,
     (const int *) 0,
     {216, 76, 66, 52}
};
