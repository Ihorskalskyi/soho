/*
 * Copyright (c) 1997,1998 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * executor.c -- execute the fft
 */

/* $Id: executor.c,v 1.2 1998/06/23 21:57:02 dcschmid Exp $ */
#include <fftw-int.h>
#include <stdio.h>
#include <stdlib.h>

const char *fftw_version = "FFTW V1.3 ($Id: executor.c,v 1.2 1998/06/23 21:57:02 dcschmid Exp $)";

/*
 * This function is called in other files, so we cannot declare
 * it static. 
 */

void fftw_strided_copy(int n, FFTW_COMPLEX *in, int ostride,
		       FFTW_COMPLEX *out)
{
     int i;
     FFTW_REAL r0, r1, i0, i1;
     FFTW_REAL r2, r3, i2, i3;

     i = 0;
     if (n & 3)
	  for (; i < (n & 3); ++i) {
	       out[i * ostride] = in[i];
	  }
     for (; i < n; i += 4) {
	  r0 = c_re(in[i]);
	  i0 = c_im(in[i]);
	  r1 = c_re(in[i + 1]);
	  i1 = c_im(in[i + 1]);
	  r2 = c_re(in[i + 2]);
	  i2 = c_im(in[i + 2]);
	  r3 = c_re(in[i + 3]);
	  i3 = c_im(in[i + 3]);
	  c_re(out[i * ostride]) = r0;
	  c_im(out[i * ostride]) = i0;
	  c_re(out[(i + 1) * ostride]) = r1;
	  c_im(out[(i + 1) * ostride]) = i1;
	  c_re(out[(i + 2) * ostride]) = r2;
	  c_im(out[(i + 2) * ostride]) = i2;
	  c_re(out[(i + 3) * ostride]) = r3;
	  c_im(out[(i + 3) * ostride]) = i3;
     }
}

/*
 * gcc 2.[78].x and x86 specific hacks.  These macros align the stack
 * pointer so that the double precision temporary variables in the
 * codelets will be aligned to a multiple of 8 bytes (*way* faster on
 * pentium and pentiumpro)
 */
#ifdef __GNUC__
#ifdef __i386__
#ifdef FFTW_ENABLE_I386_HACKS
#ifndef FFTW_ENABLE_FLOAT
#define HACK_ALIGN_STACK_EVEN() {                        \
     double __foo;                                       \
     if ((((int) &__foo) & 0x7)) __builtin_alloca(4);    \
}

#define HACK_ALIGN_STACK_ODD() {                         \
     double __foo;                                       \
     if (!(((int) &__foo) & 0x7)) __builtin_alloca(4);   \
}
#endif
#endif
#endif
#endif

#ifndef HACK_ALIGN_STACK_EVEN
#define HACK_ALIGN_STACK_EVEN()
#endif
#ifndef HACK_ALIGN_STACK_ODD
#define HACK_ALIGN_STACK_ODD()
#endif

/*
 * Do *not* declare simple executor static--we need to call it
 * from executor_cilk.cilk...also, preface its name with "fftw_"
 * to avoid any possible name collisions. 
 */
void fftw_executor_simple(int n, const FFTW_COMPLEX *in,
			  FFTW_COMPLEX *out,
			  fftw_plan_node *p,
			  int istride,
			  int ostride)
{
     switch (p->type) {
	 case FFTW_NOTW:
	      HACK_ALIGN_STACK_EVEN();
	      (p->nodeu.notw.codelet) (in, out, istride, ostride);
	      break;

	 case FFTW_TWIDDLE:
	      {
		   int r = p->nodeu.twiddle.size;
		   int m = n / r;
		   int i;
		   twiddle_codelet *codelet;
		   FFTW_COMPLEX *W;

		   for (i = 0; i < r; ++i) {
			fftw_executor_simple(m, in + i * istride,
					     out + i * (m * ostride),
					     p->nodeu.twiddle.recurse,
					     istride * r, ostride);
		   }

		   codelet = p->nodeu.twiddle.codelet;
		   W = p->nodeu.twiddle.tw->twarray;

		   HACK_ALIGN_STACK_ODD();
		   codelet(out, W, m * ostride, m, ostride);

		   break;
	      }

	 case FFTW_GENERIC:
	      {
		   int r = p->nodeu.generic.size;
		   int m = n / r;
		   int i;
		   generic_codelet *codelet;
		   FFTW_COMPLEX *W;

		   for (i = 0; i < r; ++i) {
			fftw_executor_simple(m, in + i * istride,
					     out + i * (m * ostride),
					     p->nodeu.generic.recurse,
					     istride * r, ostride);
		   }

		   codelet = p->nodeu.generic.codelet;
		   W = p->nodeu.generic.tw->twarray;
		   codelet(out, W, m, r, n, ostride);

		   break;
	      }

	 default:
	      fftw_die("BUG in executor: illegal plan\n");
	      break;
     }
}

static void executor_simple_inplace(int n, FFTW_COMPLEX *in,
				    FFTW_COMPLEX *out,
				    fftw_plan_node *p,
				    int istride)
{
     switch (p->type) {
	 case FFTW_NOTW:
	      HACK_ALIGN_STACK_EVEN();
	      (p->nodeu.notw.codelet) (in, in, istride, istride);
	      break;

	 default:
	      {
		   FFTW_COMPLEX *tmp;

		   if (out)
			tmp = out;
		   else
			tmp = (FFTW_COMPLEX *)
			    fftw_malloc(n * sizeof(FFTW_COMPLEX));

		   fftw_executor_simple(n, in, tmp, p, istride, 1);
		   HACK_ALIGN_STACK_EVEN();
		   fftw_strided_copy(n, tmp, istride, in);

		   if (!out)
			fftw_free(tmp);
	      }
     }
}

static void executor_many(int n, const FFTW_COMPLEX *in,
			  FFTW_COMPLEX *out,
			  fftw_plan_node *p,
			  int istride,
			  int ostride,
			  int howmany, int idist, int odist)
{
     switch (p->type) {
	 case FFTW_NOTW:
	      {
		   int s;
		   notw_codelet *codelet = p->nodeu.notw.codelet;

		   HACK_ALIGN_STACK_ODD();
		   for (s = 0; s < howmany; ++s)
			codelet(in + s * idist,
				out + s * odist,
				istride, ostride);
		   break;
	      }

	 default:
	      {
		   int s;
		   for (s = 0; s < howmany; ++s) {
			fftw_executor_simple(n, in + s * idist,
					     out + s * odist,
					     p, istride, ostride);
		   }
	      }
     }
}

static void executor_many_inplace(int n, FFTW_COMPLEX *in,
				  FFTW_COMPLEX *out,
				  fftw_plan_node *p,
				  int istride,
				  int howmany, int idist)
{
     switch (p->type) {
	 case FFTW_NOTW:
	      {
		   int s;
		   notw_codelet *codelet = p->nodeu.notw.codelet;

		   HACK_ALIGN_STACK_ODD();
		   for (s = 0; s < howmany; ++s)
			codelet(in + s * idist,
				in + s * idist,
				istride, istride);
		   break;
	      }

	 default:
	      {
		   int s;
		   FFTW_COMPLEX *tmp;
		   if (out)
			tmp = out;
		   else
			tmp = (FFTW_COMPLEX *)
			    fftw_malloc(n * sizeof(FFTW_COMPLEX));

		   for (s = 0; s < howmany; ++s) {
			fftw_executor_simple(n,
					     in + s * idist,
					     tmp,
					     p, istride, 1);
			HACK_ALIGN_STACK_EVEN();
			fftw_strided_copy(n, tmp, istride, in + s * idist);
		   }

		   if (!out)
			fftw_free(tmp);
	      }
     }
}

/* user interface */
void fftw(fftw_plan plan, int howmany, FFTW_COMPLEX *in, int istride,
	  int idist, FFTW_COMPLEX *out, int ostride, int odist)
{
     int n = plan->n;

     if (plan->flags & FFTW_IN_PLACE) {
	  if (howmany == 1) {
	       executor_simple_inplace(n, in, out, plan->root, istride);
	  } else {
	       executor_many_inplace(n, in, out, plan->root, istride, howmany,
				     idist);
	  }
     } else {
	  if (howmany == 1) {
	       fftw_executor_simple(n, in, out, plan->root, istride, ostride);
	  } else {
	       executor_many(n, in, out, plan->root, istride, ostride,
			     howmany, idist, odist);
	  }
     }
}
