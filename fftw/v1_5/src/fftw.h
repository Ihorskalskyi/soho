/* src/fftw.h.  Generated automatically by configure.  */
/* -*- C -*- */
/*
 * Copyright (c) 1997,1998 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* fftw.h -- system-wide definitions */
/* $Id: fftw.h.in,v 1.1 1998/06/23 21:57:04 dcschmid Exp $ */

#ifndef FFTW_H
#define FFTW_H

#include <stdlib.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif				/* __cplusplus */

/* Define for using single precision */
/* If you can, use configure --enable-float instead of changing this
   flag directly */
/* #undef FFTW_ENABLE_FLOAT */

/* our real numbers */
#ifdef FFTW_ENABLE_FLOAT
typedef float FFTW_REAL;
#else
typedef double FFTW_REAL;
#endif

/*********************************************
 * Complex numbers and operations 
 *********************************************/
typedef struct {
     FFTW_REAL re, im;
} FFTW_COMPLEX;

#define c_re(c)  ((c).re)
#define c_im(c)  ((c).im)

typedef enum {
     FFTW_FORWARD = -1, FFTW_BACKWARD = 1
} fftw_direction;

#ifndef FFTW_1_0_COMPATIBILITY
#define FFTW_1_0_COMPATIBILITY 1
#endif

#if FFTW_1_0_COMPATIBILITY
/* backward compatibility with FFTW-1.0 */
#define REAL FFTW_REAL
#define COMPLEX FFTW_COMPLEX
#endif

/*********************************************
 * Success or failure status
 *********************************************/

typedef enum {
     FFTW_SUCCESS = 0, FFTW_FAILURE = -1
} fftw_status;

/*********************************************
 *              Codelets
 *********************************************/
/*
 * There are two kinds of codelets:
 *
 * NO_TWIDDLE    computes the FFT of a certain size, operating
 *               out-of-place (i.e., take an input and produce a
 *               separate output)
 *
 * TWIDDLE       like no_twiddle, but operating in place.  Moreover,
 *               multiplies the input by twiddle factors.
 */

typedef void (notw_codelet) (const FFTW_COMPLEX *, FFTW_COMPLEX *, int, int);
typedef void (twiddle_codelet) (FFTW_COMPLEX *, const FFTW_COMPLEX *, int,
				int, int);
typedef void (generic_codelet) (FFTW_COMPLEX *, const FFTW_COMPLEX *, int,
				int, int, int);

/*********************************************
 *     Configurations
 *********************************************/
/*
 * A configuration is a database of all known codelets
 */

enum fftw_node_type {
     FFTW_NOTW, FFTW_TWIDDLE, FFTW_GENERIC
};

typedef struct {
     int fp_additions, fp_multiplications;
     int vars, memory_accesses;
} fftw_op_count;

/* description of a codelet */
typedef struct {
     const char *name;              /* name of the codelet */
     void (*codelet)();             /* pointer to the codelet itself */
     int size;                      /* size of the codelet */
     fftw_direction dir;            /* direction */
     enum fftw_node_type type;      /* TWIDDLE or NO_TWIDDLE */
     int signature;                 /* unique id */
     int ntwiddle;                  /* number of twiddle factors */
     const int *twiddle_order;      /* 
				     * array that determines the order 
				     * in which the codelet expects
				     * the twiddle factors
				     */
     fftw_op_count ops;
} fftw_codelet_desc;
     
extern fftw_codelet_desc *fftw_config[];

extern generic_codelet fftw_twiddle_generic;
extern generic_codelet fftwi_twiddle_generic;
extern const char *fftw_version;

/*****************************
 *        Plans
 *****************************/
/*
 * A plan is a sequence of reductions to compute a FFT of
 * a given size.  At each step, the FFT algorithm can:
 *
 * 1) apply a notw codelet, or
 * 2) recurse and apply a twiddle codelet, or
 * 3) apply the generic codelet.
 */

/* structure that contains twiddle factors */
typedef struct fftw_twiddle_struct {
     int n;
     const fftw_codelet_desc *cdesc;
     FFTW_COMPLEX *twarray;
     struct fftw_twiddle_struct *next;
     int refcnt;
} fftw_twiddle;

/* structure that holds all the data needed for a given step */
typedef struct fftw_plan_node_struct {
     enum fftw_node_type type;

     union {
	  /* nodes of type FFTW_NOTW */
	  struct {
	       int size;
	       notw_codelet *codelet;
	       const fftw_codelet_desc *codelet_desc;
	  } notw;

	  /* nodes of type FFTW_TWIDDLE */
	  struct {
	       int size;
	       twiddle_codelet *codelet;
	       fftw_twiddle *tw;
	       struct fftw_plan_node_struct *recurse;
	       const fftw_codelet_desc *codelet_desc;
	  } twiddle;

	  /* nodes of type FFTW_GENERIC */
	  struct {
	       int size;
	       generic_codelet *codelet;
	       fftw_twiddle *tw;
	       struct fftw_plan_node_struct *recurse;
	  } generic;
     } nodeu;

     fftw_op_count ops;

     int refcnt;
} fftw_plan_node;

struct fftw_plan_struct {
     int n;
     int refcnt;
     fftw_direction dir;
     int flags;
     int wisdom_signature;
     enum fftw_node_type wisdom_type;
     struct fftw_plan_struct *next;
     fftw_plan_node *root;
     double cost;
};

/* a plan is just an array of instructions */
typedef struct fftw_plan_struct *fftw_plan;

/* flags for the planner */
#define  FFTW_ESTIMATE (0)
#define  FFTW_MEASURE  (1)

#define FFTW_IN_PLACE (8)
#define FFTW_USE_WISDOM (16)

#define FFTWND_FORCE_BUFFERED (256) /* internal, undocumented flag */

extern fftw_plan fftw_create_plan_specific(int n, fftw_direction dir, 
					   int flags,
					   FFTW_COMPLEX *in, int istride,
					   FFTW_COMPLEX *out, int ostride);
#define FFTW_HAS_PLAN_SPECIFIC
extern fftw_plan fftw_create_plan(int n, fftw_direction dir, int flags);
extern void fftw_print_plan(fftw_plan plan);
extern void fftw_destroy_plan(fftw_plan plan);
void fftw(fftw_plan plan, int howmany, FFTW_COMPLEX *in, int istride,
	  int idist, FFTW_COMPLEX *out, int ostride, int odist);
extern void fftw_die(const char *s);
extern void *fftw_malloc(size_t n);
extern void fftw_free(void *p);
extern void fftw_check_memory_leaks(void);
extern void *(*fftw_malloc_hook) (size_t n);
extern void (*fftw_free_hook) (void *p);
extern void (*fftw_die_hook) (const char *error_string);

/* Wisdom: */
/*
 * define this symbol so that users know we are using a version of FFTW
 * with wisdom
 */
#define FFTW_HAS_WISDOM	
extern void fftw_forget_wisdom(void);
extern void fftw_export_wisdom(void (*emitter) (char c, void *), void *data);
extern fftw_status fftw_import_wisdom(int (*g) (void *), void *data);
extern void fftw_export_wisdom_to_file(FILE *output_file);
extern fftw_status fftw_import_wisdom_from_file(FILE *input_file);
extern char *fftw_export_wisdom_to_string(void);
extern fftw_status fftw_import_wisdom_from_string(const char *input_string);

/*
 * define symbol so we know this function is available (it is not in
 * older FFTWs)
 */
#define FFTW_HAS_FPRINT_PLAN
extern void fftw_fprint_plan(FILE *f, fftw_plan plan);

#define FFTW_HAS_COUNT_PLAN_OPS
extern void fftw_count_plan_ops(fftw_plan p, fftw_op_count *ops);
/* see below for fftwnd_count_plan_ops */

/*****************************
 *    N-dimensional code
 *****************************/
typedef struct {
     int is_in_place;	/* 1 if for in-place FFTs, 0 otherwise */

     int rank;		/* the rank (number of dimensions) of the
			 * array to be FFTed */
     int *n;		/* the dimensions of the array to the FFTed */
     fftw_direction dir;

     int *n_before;	/* n_before[i] = product of n[j] for j < i */
     int *n_after;	/* n_after[i] = product of n[j] for j > i */

     fftw_plan *plans;	/* 1d fftw plans for each dimension */

     int nbuffers;
     FFTW_COMPLEX *work; /* work array big enough to hold nbuffers+1
			    of the largest dimension */
} fftwnd_data;

typedef fftwnd_data *fftwnd_plan;

/* Initializing the FFTWND plan: */
extern fftwnd_plan fftw2d_create_plan(int nx, int ny, fftw_direction dir, 
				      int flags);
extern fftwnd_plan fftw3d_create_plan(int nx, int ny, int nz,
				      fftw_direction dir, int flags);
extern fftwnd_plan fftwnd_create_plan(int rank, const int *n,
				      fftw_direction dir,
				      int flags);

extern fftwnd_plan fftw2d_create_plan_specific(int nx, int ny, 
					       fftw_direction dir, 
					       int flags, 
					       FFTW_COMPLEX *in, int istride,
					       FFTW_COMPLEX *out, int ostride);
extern fftwnd_plan fftw3d_create_plan_specific(int nx, int ny, int nz,
					       fftw_direction dir, int flags, 
					       FFTW_COMPLEX *in, int istride, 
					       FFTW_COMPLEX *out, int ostride);
extern fftwnd_plan fftwnd_create_plan_specific(int rank, const int *n,
					       fftw_direction dir,
					       int flags, 
					       FFTW_COMPLEX *in, int istride, 
					       FFTW_COMPLEX *out, int ostride);

/* Freeing the FFTWND plan: */
extern void fftwnd_destroy_plan(fftwnd_plan plan);

/* Printing the plan: */
extern void fftwnd_fprint_plan(FILE *f, fftwnd_plan p);
extern void fftwnd_print_plan(fftwnd_plan p);
#define FFTWND_HAS_PRINT_PLAN

/* Computing the number of operations */
extern void fftwnd_count_plan_ops(fftwnd_plan p, fftw_op_count *ops);

/* Computing the N-Dimensional FFT */
extern void fftwnd(fftwnd_plan plan, int howmany,
		   FFTW_COMPLEX *in, int istride, int idist,
		   FFTW_COMPLEX *out, int ostride, int odist);

#ifdef __cplusplus
}				/* extern "C" */
#endif				/* __cplusplus */

#endif				/* FFTW_H */
