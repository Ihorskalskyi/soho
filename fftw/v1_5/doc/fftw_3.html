<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HTML>
<HEAD>
<!-- This HTML file has been created by texi2html 1.45
     from fftw.texi on 10 April 1998 -->

<TITLE>FFTW 1.3 User's Manual - Quick Start for Multi-dimensional Transforms</TITLE>
</HEAD>
<BODY TEXT="#000000" BGCOLOR="#FFFFFF">
Go to the <A HREF="fftw_1.html">first</A>, <A HREF="fftw_2.html">previous</A>, <A HREF="fftw_4.html">next</A>, <A HREF="fftw_9.html">last</A> section, <A HREF="fftw_toc.html">table of contents</A>.
<HR>


<H1><A NAME="SEC5">Quick Start for Multi-dimensional Transforms</A></H1>
<P>
<A NAME="IDX2"></A>
This chapter describes the basic usage of FFTW for computing complex
multi-dimensional Fourier Transforms. See section <A HREF="fftw_5.html#SEC11">FFTW Reference</A>, for more
information.


<P>
The syntax for performing multi-dimensional transforms in FFTW is very
similar to the syntax for one-dimensional transforms; the main
difference is that <SAMP>`fftw_'</SAMP> gets replaced by <SAMP>`fftwnd_'</SAMP>.
(<CODE>fftwnd</CODE> stands for "<CODE>fftw</CODE> in N dimensions.")


<P>
As before, include <CODE>&#60;fftw.h&#62;</CODE>
at the top of your program:



<PRE>
#include &#60;fftw.h&#62;
</PRE>

<P>
Then, just as in one dimension, you must create a plan before computing
a transform of any given size. The plan is created by
<CODE>fftwnd_create_plan</CODE>.



<PRE>
fftwnd_plan fftwnd_create_plan(int rank, const int *n, 
                               fftw_direction dir, int flags);
</PRE>

<P>
The arguments of this function are analogous to those of
<CODE>fftw_create_plan</CODE>.  <CODE>rank</CODE> is the dimensionality of the
array, and can be any non-negative integer.  The next argument,
<CODE>n</CODE>, is a pointer to an array of length <CODE>rank</CODE> containing the
(positive) sizes of each dimension of the array.  (Note that the array
will be stored in row-major order. See section <A HREF="fftw_6.html#SEC29">Multi-dimensional Array Format</A>, for more information.)  <CODE>dir</CODE>, as in the one-dimensional
transform, specifies the direction of the transform, and can be
<CODE>FFTW_FORWARD</CODE> or <CODE>FFTW_BACKWARD</CODE> (or, equivalently, the sign,
<MATH>-1</MATH> or <MATH>+1</MATH> respectively, of the exponent of the transform).
The last argument, <CODE>flags</CODE>, is a bitwise OR (<SAMP>`|'</SAMP>) of options.
Two possible options are <CODE>FFTW_ESTIMATE</CODE> (the default) and
<CODE>FFTW_MEASURE</CODE>; just as in <CODE>fftw_create_plan</CODE>, these specify
whether the code just estimates the optimal plan or actually performs
timing measurements.  Also, if you intend to perform in-place
transforms, you must include the option <CODE>FFTW_IN_PLACE</CODE> in the
flags (the default is to perform out-of-place transforms). (An
<STRONG>in-place</STRONG> transform is one in which the output data overwrite the
input data.  It thus requires half as much memory as--and is often
faster than--its opposite, an <STRONG>out-of-place</STRONG> transform.)


<P>
For two and three dimensional transforms, there are optional,
alternative routines that allow you to simply pass the sizes of each
dimension directly, rather than specifying a rank and an array of sizes.
These are otherwise identical to <CODE>fftwnd_create_plan</CODE>, and are
sometimes more convenient:



<PRE>
fftwnd_plan fftw2d_create_plan(int nx, int ny, 
                               fftw_direction dir, int flags);
fftwnd_plan fftw3d_create_plan(int nx, int ny, int nz, 
                               fftw_direction dir, int flags);
</PRE>

<P>
Once the plan has been created, you can use it any number of times for
transforms of the same size.  The subroutine that performs the transform
is <CODE>fftwnd</CODE>:



<PRE>
void fftwnd(fftwnd_plan plan, int howmany,
            FFTW_COMPLEX *in, int istride, int idist,
            FFTW_COMPLEX *out, int ostride, int odist);
</PRE>

<P>
This means: compute <CODE>howmany</CODE> multi-dimensional FFTs according to
the plan <CODE>plan</CODE> (which contains the size and rank of the
transform).  The first (0th) element of the input array is pointed to by
<CODE>in</CODE>, and consists of <CODE>howmany</CODE> subarrays, placed at a
distance <CODE>idist</CODE> from each other, and whose elements are
<CODE>istride</CODE> apart and are stored in row-major order.
(See section <A HREF="fftw_6.html#SEC29">Multi-dimensional Array Format</A>, for more detail.)  In other
words, the element at the <CODE>i</CODE>-th location in the <CODE>j</CODE>-th
subarray will be in position <CODE>in[i * istride + j * idist]</CODE>, where
<CODE>i</CODE> and <CODE>j</CODE> are both zero-based.  (Note that <CODE>i</CODE> here
refers to the actual index into the row-major-ordered array, and not an
index in any particular dimension.)  If the plan was created with the
<CODE>FFTW_IN_PLACE</CODE> option, the results are stored in the <CODE>in</CODE>
array with the same stride, etcetera, and the last three arguments are
ignored.  Otherwise, the result will be stored in the <CODE>out</CODE> array,
which consists of <CODE>howmany</CODE> subarrays, placed at a distance
<CODE>odist</CODE> from each other, and whose elements are <CODE>ostride</CODE>
apart.


<P>
Finally, don't forget to destroy the plan once you are done using it:



<PRE>
void fftwnd_destroy_plan(fftwnd_plan plan);
</PRE>

<P>
This frees all the memory and data structures associated with the plan.




<H2><A NAME="SEC6">Examples of Use</A></H2>

<P>
As a simple example, let us compute an out-of-place, forward transform
of a four-dimensional array. In this case, the strides are all <CODE>1</CODE>
and, since we are only computing a single transform (<CODE>howmany</CODE> is
<CODE>1</CODE>), the <CODE>idist</CODE> and <CODE>odist</CODE> parameters are ignored.



<PRE>
/* declare static in/out arrays */
FFTW_COMPLEX in[4][5][6][7], out[4][5][6][7];   

/* the dimensions of the array */
int n[4] = { 4, 5, 6, 7 };  

fftwnd_plan plan;

/* Create the plan (just estimate the optimal strategy): */
plan = fftwnd_create_plan(4, n, FFTW_FORWARD, 
                          FFTW_ESTIMATE);

/* ... initialize the in array ... */

/* Compute the transform, writing the result to out: */
fftwnd(plan, 1, (FFTW_COMPLEX *) in, 1, 0, (FFTW_COMPLEX *) out, 1, 0);

fftwnd_destroy_plan(plan);
</PRE>

<P>
Note that the type-casts were necessary in order to get an ordinary
pointer to the first element.  Alternatively, we could have used
<CODE>&#38;in[0][0][0][0]</CODE> and <CODE>&#38;out[0][0][0][0]</CODE>.


<P>
The previous example is probably typical of the needs of most users.
However, more sophisticated uses of <CODE>fftwnd</CODE> are possible.  For a
more complicated example involving strides, consider the following:
Suppose that we have a three dimensional array, but the array elements,
rather than being single <CODE>FFTW_COMPLEX</CODE> values, are "vectors," or
arrays of <EM>three</EM> <CODE>FFTW_COMPLEX</CODE> values. (Thus, our data form a
complex vector field in three dimensions.)  We will declare the
(10x11x12) array as follows:



<PRE>
typedef FFTW_COMPLEX cvector[3];
cvector vector_field[10][11][12];
</PRE>

<P>
Now, we will create the plan for an in-place transform (using
<CODE>fftw3d_create_plan</CODE> instead of <CODE>fftwnd_create_plan</CODE> just for
variety):



<PRE>
fftwnd_plan plan;

plan = fftw3d_create_plan(10, 11, 12, FFTW_FORWARD,
                          FFTW_ESTIMATE | FFTW_IN_PLACE);
</PRE>

<P>
Finally, let us transform <EM>each component</EM> of the vector field
separately (i.e.  three 3D transforms, and <EM>not</EM> one 4D transform),
using three separate calls to <CODE>fftwnd</CODE> (note that the last three
parameters to <CODE>fftwnd</CODE> are ignored since we are transforming
in-place). We use an <CODE>istride</CODE> of 3 since each vector has three
components that are stored in adjacent locations in memory.



<PRE>
/* transform 1st component */
fftwnd(plan, 1, &#38;in[0][0][0][0], 3, 0, 0, 0, 0);

/* transform 2nd component */
fftwnd(plan, 1, &#38;in[0][0][0][1], 3, 0, 0, 0, 0);  

/* transform 3rd component */
fftwnd(plan, 1, &#38;in[0][0][0][2], 3, 0, 0, 0, 0);
</PRE>

<P>
Alternatively, we could use the <CODE>howmany</CODE> parameter to perform all
three transforms with one subroutine call.  Here, we use an <CODE>idist</CODE>
of 1 since the components of the first element of the array are stored
in adjacent locations.  <CODE>howmany</CODE> is 3 since there are three arrays
to transform, and the stride is unchanged:



<PRE>
/* transform all components at once */
fftwnd(plan, 3, &#38;in[0][0][0][0], 3, 1, 0, 0, 0); 
</PRE>

<P>
Note that we used the same plan in all instances, since the dimensions
of the data to transform were the same, even though other parameters
were different.




<H2><A NAME="SEC7">What FFTWND really computes.</A></H2>

<P>
The conventions that we follow for the multi-dimensional transform are
analogous to those for the one-dimensional transform. In particular, the
forward transform has a negative sign in the exponent and neither the
forward nor the backward transforms will perform any normalization.
Computing the backward transform of the forward transform will multiply
the array by the product of its dimensions.  The output is in-order, and
the zeroth element of the output is the amplitude of the zero frequency
component.


The Gods forbade using HTML to display mathematical formulas.  Please
see the TeX or Postscript version of this manual for the proper
definition of the <MATH>n</MATH>-dimensional Fourier Transform that FFTW
uses.  For completeness, we include a bitmap of the TeX output below:
<P><center><IMG SRC="equation-3.gif" ALIGN="top"></center>

<HR>
Go to the <A HREF="fftw_1.html">first</A>, <A HREF="fftw_2.html">previous</A>, <A HREF="fftw_4.html">next</A>, <A HREF="fftw_9.html">last</A> section, <A HREF="fftw_toc.html">table of contents</A>.
</BODY>
</HTML>
