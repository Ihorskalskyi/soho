<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HTML>
<HEAD>
<!-- This HTML file has been created by texi2html 1.45
     from fftw.texi on 10 April 1998 -->

<TITLE>FFTW 1.3 User's Manual - Multi-dimensional Array Format</TITLE>
</HEAD>
<BODY TEXT="#000000" BGCOLOR="#FFFFFF">
Go to the <A HREF="fftw_1.html">first</A>, <A HREF="fftw_5.html">previous</A>, <A HREF="fftw_7.html">next</A>, <A HREF="fftw_9.html">last</A> section, <A HREF="fftw_toc.html">table of contents</A>.
<HR>


<H1><A NAME="SEC29">Multi-dimensional Array Format</A></H1>

<P>
This chapter describes the format in which multi-dimensional arrays are
stored.  We felt that a detailed discussion of this topic was necessary,
since it is often a source of confusion among users and several
different formats are common.




<H2><A NAME="SEC30">Row-major Format</A></H2>

<P>
The multi-dimensional arrays passed to <CODE>fftwnd</CODE> are expected to be
stored as a single contiguous block in <STRONG>row-major</STRONG> order (sometimes
called "C order").  Basically, this means that as you step through
adjacent memory locations, the first dimension's index varies most
slowly and the last dimension's index varies most quickly.


<P>
To be more explicit, let us consider an array of rank <MATH>d</MATH> whose
dimensions are
n<sub>1</sub> x n<sub>2</sub> x n<sub>3</sub> x ... x n<sub>d</sub>.
Now, we specify a location in the array by a sequence of (zero-based) indices, 
one for each dimension:
(i<sub>1</sub>, i<sub>2</sub>, i<sub>3</sub>,..., i<sub>d</sub>).
If the array is stored in row-major
order, then this element is located at the position
i<sub>d</sub> + n<sub>d</sub> * (i<sub>d-1</sub> + n<sub>d-1</sub> * (... + n<sub>2</sub> * i<sub>1</sub>)).


<P>
Note that each element of the array must be of type <CODE>FFTW_COMPLEX</CODE>;
i.e. a (real, imaginary) pair of (double precision) numbers. Note also
that, in <CODE>fftwnd</CODE>, the expression above is multiplied by the stride
to get the actual array index--this is useful in situations where each
element of the multi-dimensional array is actually a data structure or
another array, and you just want to transform a single field. In most
cases, however, you use a stride of 1.




<H2><A NAME="SEC31">Column-major Format</A></H2>

<P>
Readers from the Fortran world are used to arrays stored in
<STRONG>column-major</STRONG> order (sometimes called "Fortran order").  This is
essentially the exact opposite of row-major order in that, here, the
<EM>first</EM> dimension's index varies most quickly.


<P>
If you have an array stored in column-major order and wish to transform
it using <CODE>fftwnd</CODE>, it is quite easy to do.  When creating the plan,
simply pass the dimensions of the array to <CODE>fftwnd_create_plan</CODE> in
<EM>reverse order</EM>.  For example, if your array is a rank three
<CODE>N x M x L</CODE> matrix in column-major order, you should pass the
dimensions of the array as if it were an <CODE>L x M x N</CODE> matrix (which
it is, from perspective of <CODE>fftwnd</CODE>).




<H2><A NAME="SEC32">Static Arrays in C</A></H2>

<P>
Multi-dimensional arrays declared statically (that is, at compile time,
not necessarily with the <CODE>static</CODE> keyword) in C are <EM>already</EM>
in row-major order.  You don't have to do anything special to transform
them.  (See section <A HREF="fftw_3.html#SEC5">Quick Start for Multi-dimensional Transforms</A>, for an
example of this sort of code.)




<H2><A NAME="SEC33">Dynamic Arrays in C</A></H2>

<P>
Often, especially for large arrays, it is desirable to allocate the
arrays dynamically, at runtime.  This isn't too hard to do, although it
is not as straightforward for multi-dimensional arrays as it is for
one-dimensional arrays.


<P>
Creating the array is simple: using a dynamic-allocation routine like
<CODE>malloc</CODE>, allocate an array big enough to store N <CODE>FFTW_COMPLEX</CODE>
values, where N is the product of the sizes of the array dimensions
(i.e. the total number of complex values in the array).  For example,
here is code to allocate a 5x12x27 rank 3 array:



<PRE>
FFTW_COMPLEX *an_array;

an_array = (FFTW_COMPLEX *) malloc(5 * 12 * 27 * sizeof(FFTW_COMPLEX));
</PRE>

<P>
Accessing the array elements, however, is more tricky--you can't simply
use multiple applications of the <SAMP>`[]'</SAMP> operator like you could for
static arrays.  Instead, you have to explicitly compute the offset into
the array using the formula given earlier for row-major arrays.  For
example, to reference the <MATH>(i,j,k)</MATH>-th element of the array
allocated above, you would use the expression <CODE>an_array[k + 27 * (j
+ 12 * i)]</CODE>.


<P>
This pain can be alleviated somewhat by defining appropriate macros, or,
in C++, creating a class and overloading the <SAMP>`()'</SAMP> operator.




<H2><A NAME="SEC34">Dynamic Arrays in C--The Wrong Way</A></H2>

<P>
A different method for allocating multi-dimensional arrays in C is often
suggested which, though somewhat more convenient, produces very poor
performance (and, more to the point, is incompatible with
<CODE>fftwnd</CODE>).  This method is to create arrays of pointers of arrays
of pointers of...etcetera.  For example, the analogue in this method to
the example above is:



<PRE>
int i,j;
FFTW_COMPLEX ***a_slow_array;  /* another way to make a 5x12x27 array */

a_slow_array = (FFTW_COMPLEX ***) malloc(5 * sizeof(FFTW_COMPLEX **));
for (i = 0; i &#60; 5; ++i) {
     a_slow_array[i] = (FFTW_COMPLEX **) malloc(12 * sizeof(FFTW_COMPLEX *));
     for (j = 0; j &#60; 12; ++j)
          a_slow_array[i][j] =
                (FFTW_COMPLEX *) malloc(27 * sizeof(FFTW_COMPLEX));
}
</PRE>

<P>
As you can see, this sort of array is inconvenient to allocate (and
deallocate).  On the other hand, it has the advantage that the
<MATH>(i,j,k)</MATH>-th element can be referenced simply by
<CODE>a_slow_array[i][j][k]</CODE>.


<P>
If performance is important, however, you should not allocate
multi-dimensional arrays in this way.  Expressions like
<CODE>a_slow_array[i][j][k]</CODE> really correspond to
<CODE>*(*(*(a_slow_array + i) + j) + k)</CODE>, whereas expressions like
<CODE>an_array[k + 27 * (j + 12 * i)]</CODE> from before correspond to
<CODE>*(an_array + k + 27 * (j + 12 * i))</CODE>.  As you can see, the former
expression substitutes two pointer dereferences for the two
multiplications in the latter expression. However, a pointer dereference
translates into a load from memory, and, as everyone should know, a
memory access is about the most expensive instruction you can possibly
execute--much more expensive than a multiplication, often even if the
memory access is in cache.


<P>
If you want to maximize convenience in accessing the array and
performance is not critical, but still want to pass the array to FFTW,
you can use a hybrid technique.  Allocate the array as one contiguous
block, but also declare an array of arrays of pointers that points to
appropriate places in the block.  This sort of trick is beyond the scope
of this documentation; for more information on multi-dimensional arrays
in C, see the <CODE>comp.lang.c</CODE> FAQ.


<HR>
Go to the <A HREF="fftw_1.html">first</A>, <A HREF="fftw_5.html">previous</A>, <A HREF="fftw_7.html">next</A>, <A HREF="fftw_9.html">last</A> section, <A HREF="fftw_toc.html">table of contents</A>.
</BODY>
</HTML>
