<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HTML>
<HEAD>
<!-- This HTML file has been created by texi2html 1.45
     from fftw.texi on 10 April 1998 -->

<TITLE>FFTW 1.3 User's Manual - FFTW Reference</TITLE>
</HEAD>
<BODY TEXT="#000000" BGCOLOR="#FFFFFF">
Go to the <A HREF="fftw_1.html">first</A>, <A HREF="fftw_4.html">previous</A>, <A HREF="fftw_6.html">next</A>, <A HREF="fftw_9.html">last</A> section, <A HREF="fftw_toc.html">table of contents</A>.
<HR>


<H1><A NAME="SEC11">FFTW Reference</A></H1>
<P>
This chapter provides a complete reference for all FFTW functions.
Programs using FFTW should be linked with <CODE>-lfftw -lm</CODE> on Unix
systems, or with the FFTW and standard math libraries in general.




<H2><A NAME="SEC12">Complex Numbers</A></H2>

<P>
By including <CODE>&#60;fftw.h&#62;</CODE>, you will have access to the following
definitions:



<PRE>
typedef double FFTW_REAL;

typedef struct {
     FFTW_REAL re, im;
} FFTW_COMPLEX;

#define c_re(c)  ((c).re)
#define c_im(c)  ((c).im)
</PRE>

<P>
All FFTW operations are performed on the <CODE>FFTW_COMPLEX</CODE> data type.
The two macros <CODE>c_re</CODE> and <CODE>c_im</CODE> retrieve, respectively,
the real and imaginary parts of a complex number.


<P>
Users who wish to work in single precision rather than double precision
merely need to <CODE>#define</CODE> the symbol <CODE>FFTW_ENABLE_FLOAT</CODE> in
<CODE>fftw.h</CODE> and then recompile the library.  On Unix systems, one can
instead use <CODE>configure --enable-float</CODE> (See section <A HREF="fftw_7.html#SEC35">Installation</A>, for
more information).




<H2><A NAME="SEC13">Plan Creation for One-dimensional Transforms</A></H2>


<PRE>
#include &#60;fftw.h&#62;

fftw_plan fftw_create_plan(int n, fftw_direction dir,
                           int flags);
</PRE>

<P>
The function <CODE>fftw_create_plan</CODE> creates a plan, which is
a data structure containing all the information that <CODE>fftw</CODE>
needs in order to compute the 1D Fourier Transform. You can
create as many plans as you need, but only one plan for a given
array size is required (a plan can be reused many times).


<P>
<CODE>fftw_create_plan</CODE> returns a valid plan, or <CODE>NULL</CODE>
if, for some reason, the plan can't be created.  In the
default installation, this can't happen, but it is possible
to configure FFTW in such a way that some input sizes are
forbidden, and FFTW cannot create a plan.


<P>
See also <CODE>fftw_create_plan_specific</CODE>, below.




<H3><A NAME="SEC14">Arguments</A></H3>

<UL>
<LI>

<CODE>n</CODE> is the size of the transform.  It can be
 any positive integer.
 

<UL>
<LI>

FFTW is best at handling sizes of the form
<MATH> 2<SUP>a</SUP> 3<SUP>b</SUP> 5<SUP>c</SUP> 7<SUP>d</SUP>
        11<SUP>e</SUP> 13<SUP>f</SUP> </MATH>
where the exponents of <MATH>11</MATH> and <MATH>13</MATH> are either <MATH>0</MATH> or
<MATH>1</MATH>, and the other exponents are arbitrary.  Other sizes are
computed by means of a slow, general-purpose routine.  (It is possible
to customize FFTW for different array sizes.  See section <A HREF="fftw_7.html#SEC35">Installation</A> for
more information.)  Transforms whose sizes are powers of <MATH>2</MATH> are
especially fast.
</UL>

<LI>

<CODE>dir</CODE> is the sign of the exponent in the formula that
defines the Fourier Transform.  It can be <MATH>-1</MATH> or <MATH>+1</MATH>.
The aliases <CODE>FFTW_FORWARD</CODE> and <CODE>FFTW_BACKWARD</CODE>
are provided, where <CODE>FFTW_FORWARD</CODE> stands for <MATH>-1</MATH>.

<LI>

<CODE>flags</CODE> is a boolean OR (<SAMP>`|'</SAMP>) of zero or more of the following:

<UL>
<LI>

<CODE>FFTW_MEASURE</CODE>: this flag tells FFTW to find the optimal plan by
actually <EM>computing</EM> several FFTs and measuring their
execution time.  Depending on the installation, this can take some
time.<A NAME="FOOT2" HREF="fftw_foot.html#FOOT2">(2)</A>

<LI>

<CODE>FFTW_ESTIMATE</CODE>: do not run any FFT and provide a "reasonable"
plan (for a RISC processor with many registers).  If neither
<CODE>FFTW_ESTIMATE</CODE> nor <CODE>FFTW_MEASURE</CODE> is provided, the default is
<CODE>FFTW_ESTIMATE</CODE>.

<LI>

<CODE>FFTW_OUT_OF_PLACE</CODE>: produce a plan assuming that the input
  and output arrays will be distinct (this is the default).

<LI>

<CODE>FFTW_IN_PLACE</CODE>: produce a plan assuming that you want the output
in the input array.  The algorithm used is not necessarily in place:
FFTW is able to compute true in-place transforms only for small values
of <CODE>n</CODE>.  If FFTW is not able to compute the transform in-place, it
will allocate a temporary array (unless you provide one yourself),
compute the transform out of place, and copy the result back.
<EM>Warning: This option changes the meaning of some 
parameters of <CODE>fftw</CODE></EM> (See section <A HREF="fftw_5.html#SEC18">Computing the one-dimensional transform</A>.)

The in-place option is mainly provided for people who want to write
their own in-place multi-dimensional Fourier Transform, using FFTW as a
base.  For example, consider a three-dimensional <CODE>n * n * n</CODE>
transform.  An out-of-place algorithm will need another array (which may
be huge).  However, FFTW can compute the in-place transform along
each dimension using only a temporary array of size <CODE>n</CODE>.
Moreover, if FFTW happens to be able to compute the transform truly
in-place, no temporary array and no copying are needed.  As distributed,
FFTW `knows' how to compute in-place transforms of size 1, 2, 3, 4, 5, 6,
7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 32 and 64.

The default mode of operation is <CODE>FFTW_OUT_OF_PLACE</CODE>.

<LI>

<CODE>FFTW_USE_WISDOM</CODE>: use any <CODE>wisdom</CODE> that is available to help
in the creation of the plan. (See section <A HREF="fftw_4.html#SEC8">Words of Wisdom</A>.)
This can greatly speed the creation of plans, especially with the
<CODE>FFTW_MEASURE</CODE> option. <CODE>FFTW_ESTIMATE</CODE> plans can also take
advantage of <CODE>wisdom</CODE> to produce a more optimal plan (based on past
measurements) than the estimation heuristic would normally
generate. When the <CODE>FFTW_MEASURE</CODE> option is used, new <CODE>wisdom</CODE>
will also be generated if the current transform size is not completely
understood by existing <CODE>wisdom</CODE>.

</UL>

</UL>



<H2><A NAME="SEC15">Plan Creation for Multi-dimensional Transforms</A></H2>


<PRE>
#include &#60;fftw.h&#62;

fftwnd_plan fftwnd_create_plan(int rank, const int *n,
                               fftw_direction dir, int flags);

fftwnd_plan fftw2d_create_plan(int nx, int ny,
                               fftw_direction dir, int flags);

fftwnd_plan fftw3d_create_plan(int nx, int ny, int nz,
                               fftw_direction dir, int flags);
</PRE>

<P>
The function <CODE>fftwnd_create_plan</CODE> creates a plan, which is a data
structure containing all the information that <CODE>fftwnd</CODE> needs in
order to compute a multi-dimensional Fourier Transform.  You can create
as many plans as you need, but only one plan for a given array size is
required (a plan can be reused many times).  The functions
<CODE>fftw2d_create_plan</CODE> and <CODE>fftw3d_create_plan</CODE> are optional,
alternative interfaces to <CODE>fftwnd_create_plan</CODE> for two and three
dimensions, respectively.


<P>
<CODE>fftwnd_create_plan</CODE> returns a valid plan, or <CODE>NULL</CODE> if, for
some reason, the plan can't be created.  This can happen if memory runs
out or if the arguments are invalid in some way (e.g.  if <CODE>rank</CODE> &#60;
0).


<P>
See also <CODE>fftwnd_create_plan_specific</CODE>, below, for an even better way
to create multi-dimensional plans.




<H3><A NAME="SEC16">Arguments</A></H3>

<UL>
<LI>

<CODE>rank</CODE> is the dimensionality of the arrays to be transformed.  It
can be any non-negative integer.

<LI>

<CODE>n</CODE> is a pointer to an array of <CODE>rank</CODE> integers, giving the
size of each dimension of the arrays to be transformed.  These sizes,
which must be positive integers, correspond to the dimensions of
row-major arrays--i.e. n[0] is the size of the dimension whose indices
vary most slowly, and so on. (See section <A HREF="fftw_6.html#SEC29">Multi-dimensional Array Format</A>, for
more information.)

<UL>
<LI>

See section <A HREF="fftw_5.html#SEC13">Plan Creation for One-dimensional Transforms</A>, for more information regarding
optimal array sizes.
</UL>

<LI>

<CODE>nx</CODE> and <CODE>ny</CODE> in <CODE>fftw2d_create_plan</CODE> are positive
integers specifying the dimensions of the rank 2 array to be
transformed. i.e. they specify that the transform will operate on
<CODE>nx x ny</CODE> arrays in row-major order.

<LI>

<CODE>nx</CODE>, <CODE>ny</CODE> and <CODE>nz</CODE> in <CODE>fftw3d_create_plan</CODE> are
positive integers specifying the dimensions of the rank 3 array to be
transformed. i.e. they specify that the transform will operate on
<CODE>nx x ny x nz</CODE> arrays in row-major order.

<LI>

<CODE>dir</CODE> is the sign of the exponent in the formula that defines the
Fourier Transform.  It can be <MATH>-1</MATH> or <MATH>+1</MATH>.  The aliases
<CODE>FFTW_FORWARD</CODE> and <CODE>FFTW_BACKWARD</CODE> are provided, where
<CODE>FFTW_FORWARD</CODE> stands for <MATH>-1</MATH>.

<LI>

<CODE>flags</CODE> is a boolean OR (<SAMP>`|'</SAMP>) of zero or more of the following:

<UL>
<LI>

<CODE>FFTW_MEASURE</CODE>: this flag tells FFTW to find the optimal plan by
actually <EM>computing</EM> several FFTs and measuring their execution
time.

<LI>

<CODE>FFTW_ESTIMATE</CODE>: do not run any FFT and provide a "reasonable"
plan (for a RISC processor with many registers).  If neither
<CODE>FFTW_ESTIMATE</CODE> nor <CODE>FFTW_MEASURE</CODE> is provided, the default is
<CODE>FFTW_ESTIMATE</CODE>.

<LI>

<CODE>FFTW_OUT_OF_PLACE</CODE>: produce a plan assuming that the input
  and output arrays will be distinct (this is the default).

<LI>

<CODE>FFTW_IN_PLACE</CODE>: produce a plan assuming that you want to perform
the transform in-place.  (Unlike the one-dimensional transform, this
"really"<A NAME="FOOT3" HREF="fftw_foot.html#FOOT3">(3)</A> performs the
transform in-place.) Note that, if you want to perform in-place
transforms, you <EM>must</EM> use a plan created with this option.

The default mode of operation is <CODE>FFTW_OUT_OF_PLACE</CODE>.

<LI>

<CODE>FFTW_USE_WISDOM</CODE>: use any <CODE>wisdom</CODE> that is available to help
in the creation of the plan. (See section <A HREF="fftw_4.html#SEC8">Words of Wisdom</A>.)
This can greatly speed the creation of plans, especially with the
<CODE>FFTW_MEASURE</CODE> option. <CODE>FFTW_ESTIMATE</CODE> plans can also take
advantage of <CODE>wisdom</CODE> to produce a more optimal plan (based on past
measurements) than the estimation heuristic would normally
generate. When the <CODE>FFTW_MEASURE</CODE> option is used, new <CODE>wisdom</CODE>
will also be generated if the current transform size is not completely
understood by existing <CODE>wisdom</CODE>. Note that the same <CODE>wisdom</CODE>
is shared between one-dimensional and multi-dimensional transforms.

</UL>

</UL>



<H2><A NAME="SEC17">Creating plans based on specific arrays</A></H2>


<PRE>
#include &#60;fftw.h&#62;

fftw_plan fftw_create_plan_specific(int n, fftw_direction dir,
                                    int flags,
                                    FFTW_COMPLEX *in, int istride,
                                    FFTW_COMPLEX *out, int ostride);
fftwnd_plan fftwnd_create_plan_specific(int rank, const int *n,
                                        fftw_direction dir,
                                        int flags,
                                        FFTW_COMPLEX *in, int istride,
                                        FFTW_COMPLEX *out, int ostride);
fftwnd_plan fftw2d_create_plan_specific(int nx, int ny,
                                        fftw_direction dir,
                                        int flags,
                                        FFTW_COMPLEX *in, int istride,
                                        FFTW_COMPLEX *out, int ostride);
fftwnd_plan fftw3d_create_plan_specific(int nx, int ny, int nz,
                                        fftw_direction dir, int flags,
                                        FFTW_COMPLEX *in, int istride,
                                        FFTW_COMPLEX *out, int ostride);
</PRE>

<P>
These functions are identical to the corresponding functions without the
<SAMP>`_specific'</SAMP>, except that they take as additional arguments specific
input/output arrays and their strides.


<P>
For the last four arguments, you should pass the arrays and strides that
you will eventually be passing to <CODE>fftw</CODE> or <CODE>fftwnd</CODE>.  The
resulting plans will be optimized for those arrays and strides, although
they may be used on other arrays as well.  Note: the contents of the in
and out arrays are <EM>destroyed</EM> by the specific planners (the
initial contents are ignored, so the arrays need not have been
initialized).


<P>
We recommend the use of the specific planners, even in cases where you
will be transforming arrays different from those passed to the specific
planners, as they confer the following advantages:



<UL>

<LI>

The resulting plans will be optimized for your specific arrays and
strides.  This may or may not make a significant difference, but it
certainly doesn't hurt.  (The ordinary planner does its planning based
upon a stride-one temporary array that it allocates.)

<LI>

Less intermediate storage is required during the planning process.  (The
ordinary planner uses O(<CODE>N</CODE>) temporary storage, where <CODE>N</CODE> is
the maximum dimension, while it is creating the plan.)

<LI>

For multi-dimensional transforms, new parameters become accessible for
optimization by the planner.  (Since multi-dimensional arrays can be
very large, we don't dare to allocate one in the ordinary planner for
experimentation.  This prevents us from doing certain optimizations
that can yield dramatic improvements in some cases.)

</UL>

<P>
Note that, for <CODE>FFTW_IN_PLACE</CODE> transforms, the <CODE>out</CODE> and
<CODE>ostride</CODE> parameters are interpreted just as they are for
<CODE>fftw</CODE> or <CODE>fftwnd</CODE>.




<H2><A NAME="SEC18">Computing the one-dimensional transform</A></H2>


<PRE>
#include &#60;fftw.h&#62;

void fftw(fftw_plan plan, int howmany,
          FFTW_COMPLEX *in, int istride, int idist,
          FFTW_COMPLEX *out, int ostride, int odist);
</PRE>

<P>
The function <CODE>fftw</CODE> computes the one-dimensional Fourier Transform,
using a plan created by <CODE>fftw_create_plan</CODE> (See section <A HREF="fftw_5.html#SEC13">Plan Creation for One-dimensional Transforms</A>.)




<H3><A NAME="SEC19">Arguments</A></H3>

<UL>
<LI>

<CODE>plan</CODE> is the plan created by <CODE>fftw_create_plan</CODE>
(See section <A HREF="fftw_5.html#SEC13">Plan Creation for One-dimensional Transforms</A>.)

<LI>

<CODE>howmany</CODE> is the number of transforms <CODE>fftw</CODE> will compute.
It is faster to tell FFTW to compute many transforms, instead of
simply calling <CODE>fftw</CODE> many times.

<LI>

<CODE>in</CODE>, <CODE>istride</CODE> and <CODE>idist</CODE> describe the input array(s).
There are <CODE>howmany</CODE> input arrays; the first one is pointed to by
<CODE>in</CODE>, the second one is pointed to by <CODE>in + idist</CODE>, and so on,
up to <CODE>in + (howmany - 1) * idist</CODE>.  Each input array consists of
complex numbers (See section <A HREF="fftw_5.html#SEC12">Complex Numbers</A>), which are not necessarily
contiguous in memory.  Specifically, <CODE>in[0]</CODE> is the first element
of the first array, <CODE>in[istride]</CODE> is the second element of the
first array, and so on.  In general, the <CODE>i</CODE>-th element of the
<CODE>j</CODE>-th input array will be in position <CODE>in[i * istride + j *
idist]</CODE>.

<LI>

<CODE>out</CODE>, <CODE>ostride</CODE> and <CODE>odist</CODE> describe the output
array(s).  The format is the same as for the input array.


<UL>
<LI><EM>In-place transforms</EM>:

If the <CODE>plan</CODE> specifies an in-place transform, <CODE>ostride</CODE> and
<CODE>odist</CODE> are always ignored.  If <CODE>out</CODE> is <CODE>NULL</CODE>,
<CODE>out</CODE> is ignored, too.  Otherwise, <CODE>out</CODE> is interpreted as a
pointer to an array of <CODE>n</CODE> complex numbers, that FFTW will use as
temporary space to perform the in-place computation.  <CODE>out</CODE> is used
as scratch space and its contents destroyed.  In this case, <CODE>out</CODE>
must be an ordinary array whose elements are contiguous in memory (no
striding).
</UL>

</UL>

<P>
To simply transform a single, contiguous input array to a contiguous
output array, pass 1 for <CODE>howmany</CODE>, <CODE>istride</CODE>, <CODE>idist</CODE>,
<CODE>ostride</CODE>, and <CODE>odist</CODE>.




<H2><A NAME="SEC20">Computing the multi-dimensional transform</A></H2>


<PRE>
#include &#60;fftw.h&#62;

void fftwnd(fftwnd_plan plan, int howmany,
            FFTW_COMPLEX *in, int istride, int idist,
            FFTW_COMPLEX *out, int ostride, int odist);
</PRE>

<P>
The function <CODE>fftwnd</CODE> computes the multi-dimensional Fourier Transform,
using a plan created by <CODE>fftwnd_create_plan</CODE>
(See section <A HREF="fftw_5.html#SEC15">Plan Creation for Multi-dimensional Transforms</A>.) (Note that the plan
determines the rank and dimensions of the array to be transformed.)




<H3><A NAME="SEC21">Arguments</A></H3>

<UL>
<LI>

<CODE>plan</CODE> is the plan created by <CODE>fftwnd_create_plan</CODE>.
(See section <A HREF="fftw_5.html#SEC15">Plan Creation for Multi-dimensional Transforms</A>.) (In the case of two and
three-dimensional transforms, it could also have been created by
<CODE>fftw2d_create_plan</CODE> or <CODE>fftw3d_create_plan</CODE>, respectively.)

<LI>

<CODE>howmany</CODE> is the number of transforms <CODE>fftw</CODE> will compute.

<LI>

<CODE>in</CODE>, <CODE>istride</CODE> and <CODE>idist</CODE> describe the input array(s).
There are <CODE>howmany</CODE> input arrays; the first one is pointed to by
<CODE>in</CODE>, the second one is pointed to by <CODE>in + idist</CODE>, and so on,
up to <CODE>in + (howmany - 1) * idist</CODE>.  Each input array consists of
complex numbers (See section <A HREF="fftw_5.html#SEC12">Complex Numbers</A>), stored in row-major format
(See section <A HREF="fftw_6.html#SEC29">Multi-dimensional Array Format</A>), which are not necessarily
contiguous in memory.  Specifically, <CODE>in[0]</CODE> is the first element
of the first array, <CODE>in[istride]</CODE> is the second element of the
first array, and so on.  In general, the <CODE>i</CODE>-th element of the
<CODE>j</CODE>-th input array will be in position <CODE>in[i * istride + j *
idist]</CODE>. Note that, here, <CODE>i</CODE> refers to an index into the row-major
format for the multi-dimensional array, rather than an index in any
particular dimension.


<UL>
<LI><EM>In-place transforms</EM>:

For plans created with the <CODE>FFTW_IN_PLACE</CODE> option, the transform is
computed in-place--the output is returned in the <CODE>in</CODE> array, using
the same strides, etcetera, as were used in the input.
</UL>

<LI>

<CODE>out</CODE>, <CODE>ostride</CODE> and <CODE>odist</CODE> describe the output array(s).
The format is the same as for the input array.


<UL>
<LI><EM>In-place transforms</EM>:

These parameters are ignored for plans created with the
<CODE>FFTW_IN_PLACE</CODE> option.
</UL>

</UL>

<P>
To simply transform a single, contiguous input array to a contiguous
output array, pass 1 for <CODE>howmany</CODE>, <CODE>istride</CODE>, <CODE>idist</CODE>,
<CODE>ostride</CODE>, and <CODE>odist</CODE>.




<H2><A NAME="SEC22">Destroying a one-dimensional plan</A></H2>


<PRE>
#include &#60;fftw.h&#62;

void fftw_destroy_plan(fftw_plan plan);
</PRE>

<P>
The function <CODE>fftw_destroy_plan</CODE> frees the plan <CODE>plan</CODE> and
releases all the memory associated with it.  After destruction, a plan
is no longer valid.




<H2><A NAME="SEC23">Destroying a multi-dimensional plan</A></H2>


<PRE>
#include &#60;fftw.h&#62;

void fftwnd_destroy_plan(fftwnd_plan plan);
</PRE>

<P>
The function <CODE>fftwnd_destroy_plan</CODE> frees the plan <CODE>plan</CODE>
and releases all the memory associated with it.  After destruction,
a plan is no longer valid.




<H2><A NAME="SEC24">Counting the number of operations</A></H2>


<PRE>
#include &#60;fftw.h&#62;

typedef struct {
     int fp_additions, fp_multiplications;
     int vars, memory_accesses;
} fftw_op_count;

void fftw_count_plan_ops(fftw_plan p, fftw_op_count *ops);
void fftwnd_count_plan_ops(fftwnd_plan p, fftw_op_count *ops);
</PRE>

<P>
FFTW can compute a count of the number of operations it requires in
order to perform an FFT with a given plan.  The
<CODE>fftw_count_plan_ops</CODE> and <CODE>fftwnd_count_plan_ops</CODE> functions,
given a plan, fill in the <CODE>ops</CODE> data structure with the operation
counts.


<P>
The <CODE>fftw_op_count</CODE> data structure, defined above, contains the
total number of floating-point additions and multiplications, the
maximum number of stack variables "live" at any point, and the total
number of reads or writes to arrays.


<P>
It should be noted that all of these counts may be rendered inaccurate
by a sufficiently aggressive compiler.  The <CODE>vars</CODE> count is
especially unreliable.  We don't think that operation counts are
especially important, in any case; they certainly aren't a good
predictor of performance on most machines.




<H2><A NAME="SEC25">How to install your own memory allocator</A></H2>


<PRE>
#include &#60;fftw.h&#62;

void *(*fftw_malloc_hook) (size_t n);
void (*fftw_free_hook) (void *p);
</PRE>

<P>
Whenever it has to allocate and release memory, FFTW ordinarily calls
<CODE>malloc</CODE> and <CODE>free</CODE>.  If <CODE>malloc</CODE> fails, FFTW prints an
error message and exits.  This behavior may be undesirable in some
applications. Also, special memory-handling functions may be necessary
in certain environments. Consequently, FFTW provides means by which you
can install your own memory allocator and take whatever error-correcting
action you find appropriate.  The variables <CODE>fftw_malloc_hook</CODE> and
<CODE>fftw_free_hook</CODE> are pointers to functions, and they are normally
<CODE>NULL</CODE>.  If you set those variables to point to other functions,
then FFTW will use your routines instead of <CODE>malloc</CODE> and
<CODE>free</CODE>.  <CODE>fftw_malloc_hook</CODE> must point to a <CODE>malloc</CODE>-like
function, and <CODE>fftw_free_hook</CODE> must point to a <CODE>free</CODE>-like
function.




<H2><A NAME="SEC26">Exporting wisdom</A></H2>


<PRE>
#include &#60;fftw.h&#62;

void fftw_export_wisdom(void (*emitter)(char c, void *), void *data);
void fftw_export_wisdom_to_file(FILE *output_file);
char *fftw_export_wisdom_to_string(void);
</PRE>

<P>
These functions allow you to export all currently accumulated
<CODE>wisdom</CODE> in a form from which it can be later imported and
restored, even during a separate run of the program. (See section <A HREF="fftw_4.html#SEC8">Words of Wisdom</A>.)  The current store of <CODE>wisdom</CODE> is not
affected by calling any of these routines.


<P>
<CODE>fftw_export_wisdom</CODE> exports the <CODE>wisdom</CODE> to any output
medium, as specified by the callback function
<CODE>emitter</CODE>. <CODE>emitter</CODE> is a <CODE>putc</CODE>-like function that
writes the character <CODE>c</CODE> to some output; its second parameter is
the <CODE>data</CODE> pointer passed to <CODE>fftw_export_wisdom</CODE>.  For
convenience, the following two "wrapper" routines are provided:


<P>
<CODE>fftw_export_wisdom_to_file</CODE> writes the <CODE>wisdom</CODE> to the
current position in <CODE>output_file</CODE>, which should be open with write
permission.  Upon exit, the file remains open and is positioned at the
end of the <CODE>wisdom</CODE> data.


<P>
<CODE>fftw_export_wisdom_to_string</CODE> returns a pointer to a
<CODE>NULL</CODE>-terminated string holding the <CODE>wisdom</CODE> data. This
string is dynamically allocated, and it is the responsibility of the
caller to deallocate it with <CODE>fftw_free</CODE> when it is no longer
needed.


<P>
All of these routines export the wisdom in the same format, which we
will not document here except to say that it is LISP-like ASCII text
that is insensitive to white space.




<H2><A NAME="SEC27">Importing wisdom</A></H2>


<PRE>
#include &#60;fftw.h&#62;

fftw_status fftw_import_wisdom(int (*get_input)(void *), void *data);
fftw_status fftw_import_wisdom_from_file(FILE *input_file);
fftw_status fftw_import_wisdom_from_string(const char *input_string);
</PRE>

<P>
These functions import <CODE>wisdom</CODE> into a program from data stored by
the <CODE>fftw_export_wisdom</CODE> functions above. (See section <A HREF="fftw_4.html#SEC8">Words of Wisdom</A>.) The imported <CODE>wisdom</CODE> supplements rather than
replaces any <CODE>wisdom</CODE> already accumulated by the running program (except
when there is conflicting <CODE>wisdom</CODE>, in which case the existing
wisdom is replaced).


<P>
<CODE>fftw_import_wisdom</CODE> imports <CODE>wisdom</CODE> from any input medium,
as specified by the callback function <CODE>get_input</CODE>. <CODE>get_input</CODE>
is a <CODE>getc</CODE>-like function that returns the next character in the
input; its parameter is the <CODE>data</CODE> pointer passed to
<CODE>fftw_import_wisdom</CODE>. If the end of the input data is reached
(which should never happen for valid data), it may return either
<CODE>NULL</CODE> (ASCII 0) or <CODE>EOF</CODE> (as defined in <CODE>&#60;stdio.h&#62;</CODE>).
For convenience, the following two "wrapper" routines are provided:


<P>
<CODE>fftw_import_wisdom_from_file</CODE> reads <CODE>wisdom</CODE> from the
current position in <CODE>input_file</CODE>, which should be open with read
permission.  Upon exit, the file remains open and is positioned at the
end of the <CODE>wisdom</CODE> data.


<P>
<CODE>fftw_import_wisdom_from_string</CODE> reads <CODE>wisdom</CODE> from the
<CODE>NULL</CODE>-terminated string <CODE>input_string</CODE>.


<P>
The return value of these routines is <CODE>FFTW_SUCCESS</CODE> if the wisdom
was read successfully, and <CODE>FFTW_FAILURE</CODE> otherwise. Note that, in
all of these functions, any data in the input stream past the end of the
<CODE>wisdom</CODE> data is simply ignored (it is not even read if the
<CODE>wisdom</CODE> data is well-formed).




<H2><A NAME="SEC28">Forgetting wisdom</A></H2>


<PRE>
#include &#60;fftw.h&#62;

void fftw_forget_wisdom(void);
</PRE>

<P>
Calling <CODE>fftw_forget_wisdom</CODE> causes all accumulated <CODE>wisdom</CODE>
to be discarded and its associated memory to be freed. (New
<CODE>wisdom</CODE> can still be gathered subsequently, however.)


<HR>
Go to the <A HREF="fftw_1.html">first</A>, <A HREF="fftw_4.html">previous</A>, <A HREF="fftw_6.html">next</A>, <A HREF="fftw_9.html">last</A> section, <A HREF="fftw_toc.html">table of contents</A>.
</BODY>
</HTML>
