<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HTML>
<HEAD>
<!-- This HTML file has been created by texi2html 1.45
     from fftw.texi on 10 April 1998 -->

<TITLE>FFTW 1.3 User's Manual - Words of Wisdom</TITLE>
</HEAD>
<BODY TEXT="#000000" BGCOLOR="#FFFFFF">
Go to the <A HREF="fftw_1.html">first</A>, <A HREF="fftw_3.html">previous</A>, <A HREF="fftw_5.html">next</A>, <A HREF="fftw_9.html">last</A> section, <A HREF="fftw_toc.html">table of contents</A>.
<HR>


<H1><A NAME="SEC8">Words of Wisdom</A></H1>
<P>
<A NAME="IDX3"></A>


<P>
FFTW implements a method to save plans to disk and restore them.  In
fact, what FFTW does is more general than just saving and loading plans.
The mechanism is called <STRONG><CODE>wisdom</CODE></STRONG>.  Here, we describe this
feature at a high level. See section <A HREF="fftw_5.html#SEC11">FFTW Reference</A>, for a less casual (but
more complete) discussion of how to use <CODE>wisdom</CODE> in FFTW.


<P>
Plans created with the <CODE>FFTW_MEASURE</CODE> option produce near-optimal
FFT performance, but it can take a long time to compute a plan because
FFTW must actually measure the runtime of many possible plans and select
the best one.  This is designed for the situations where so many
transforms of the same size must be computed that the start-up time is
irrelevant.  For short initialization times but slightly slower
transforms, we have provided <CODE>FFTW_ESTIMATE</CODE>.  The <CODE>wisdom</CODE>
mechanism is a way to get the best of both worlds.  There are, however,
certain caveats that the user must be aware of in using <CODE>wisdom</CODE>.
For this reason, <CODE>wisdom</CODE> is an optional feature which is not
enabled by default.


<P>
At its simplest, <CODE>wisdom</CODE> provides a way of saving plans to disk so
that they can be reused in other program runs.  You create a plan with
the flags <CODE>FFTW_MEASURE</CODE> and <CODE>FFTW_USE_WISDOM</CODE>, and then save
the <CODE>wisdom</CODE> using <CODE>fftw_export_wisdom</CODE>.  The next time you
run the program, you can restore the <CODE>wisdom</CODE> with
<CODE>fftw_import_wisdom</CODE>, and then recreate the plan using the same
flags as before.  This time, however, the same optimal plan will be
created very quickly without measurements. (FFTW still needs some
time to compute trigonometric tables, however.)


<P>
Wisdom is more than mere rote memorization, however.  FFTW's
<CODE>wisdom</CODE> encompasses all of the knowledge and measurements that
were used to create the plan for a given size.  Therefore, existing
<CODE>wisdom</CODE> is also applied to the creation of other plans of
different sizes.


<P>
Whenever a plan is created with the <CODE>FFTW_MEASURE</CODE> and
<CODE>FFTW_USE_WISDOM</CODE> flags, <CODE>wisdom</CODE> is generated.  Thereafter,
plans for any transform with a similar factorization will be computed
more quickly, so long as they use the <CODE>FFTW_USE_WISDOM</CODE> flag.  In
fact, for transforms with the same factors and of equal or lesser size,
no measurements at all need to be made and an optimal plan can be
created with negligible delay!


<P>
For example, suppose that you create a plan for
N&nbsp;=&nbsp;2<sup>16</sup>.
Then, for any equal or smaller power of two, FFTW can create a
plan (with the same direction and flags) quickly, using the
precomputed <CODE>wisdom</CODE>. Even for larger powers of two, or sizes that
are a power of two times some other prime factors, plans will be
computed more quickly than they would otherwise (although some
measurements still have to be made).


<P>
The <CODE>wisdom</CODE> is cumulative, and is stored in a global, private data
structure managed internally by FFTW.  The storage space required is
minimal, proportional to the logarithm of the sizes the <CODE>wisdom</CODE> was
generated from.  The <CODE>wisdom</CODE> can be forgotten (and its associated
memory freed) by a call to <CODE>fftw_forget_wisdom()</CODE>; otherwise, it is
remembered until the program terminates.  It can also be exported to a
file, a string, or any other medium using <CODE>fftw_export_wisdom</CODE> and
restored during a subsequent execution of the program (or a different
program) using <CODE>fftw_import_wisdom</CODE> (these functions are described
below).


<P>
Because <CODE>wisdom</CODE> is incorporated into FFTW at a very low level, the
same <CODE>wisdom</CODE> can be used for one-dimensional transforms,
multi-dimensional transforms, and even the real-complex and parallel
extensions to FFTW.  Just include <CODE>FFTW_USE_WISDOM</CODE> in the flags
for whatever plans you create (i.e., always plan wisely).


<P>
Plans created with the <CODE>FFTW_ESTIMATE</CODE> plan can use <CODE>wisdom</CODE>,
but cannot generate it;  only <CODE>FFTW_MEASURE</CODE> plans actually produce
<CODE>wisdom</CODE>.  Also, plans can only use <CODE>wisdom</CODE> generated from
plans created with the same direction and flags.  For example, a size
<CODE>42</CODE> <CODE>FFTW_BACKWARD</CODE> transform will not use <CODE>wisdom</CODE>
produced by a size <CODE>42</CODE> <CODE>FFTW_FORWARD</CODE> transform.  The only
exception to this rule is that <CODE>FFTW_ESTIMATE</CODE> plans can use
<CODE>wisdom</CODE> from <CODE>FFTW_MEASURE</CODE> plans.




<H2><A NAME="SEC9">Caveats in using wisdom</A></H2>


<BLOCKQUOTE>
<P>
For in much wisdom is much grief, and he that increaseth knowledge
increaseth sorrow. [Ecclesiastes 1:18]
</BLOCKQUOTE>

<P>
There are pitfalls to using <CODE>wisdom</CODE>, in that it can negate FFTW's
ability to adapt to changing hardware and other conditions. For example,
it would be perfectly possible to export <CODE>wisdom</CODE> from a program
running on one processor and import it into a program running on another
processor.  Doing so, however, would mean that the second program would
use plans optimized for the first processor, instead of the one it is
running on.


<P>
It should be safe to reuse <CODE>wisdom</CODE> as long as the hardware and
program binaries remain unchanged. (Actually, the optimal plan may
change even between runs of the same binary on identical hardware, due
to differences in the virtual memory environment, etcetera.  Users
really interested in performance should worry about this problem, too.)
It is likely that, if the same <CODE>wisdom</CODE> is used for two different
program binaries, even running on the same machine, the plans may be
sub-optimal because of differing code alignments.  It is therefore wise
to recreate <CODE>wisdom</CODE> every time an application is recompiled.  The
more the underlying hardware and software changes between the creation
of <CODE>wisdom</CODE> and its use, the greater grows the risk of sub-optimal
plans.


<P>
On an unrelated note, we should point out that the accumulation of
<CODE>wisdom</CODE> is <EM>not</EM> thread-safe, although the utilization of
<CODE>wisdom</CODE> is.




<H2><A NAME="SEC10">Importing and exporting wisdom</A></H2>


<PRE>
void fftw_export_wisdom_to_file(FILE *output_file);
fftw_status fftw_import_wisdom_from_file(FILE *input_file);
</PRE>

<P>
<CODE>fftw_export_wisdom_to_file</CODE> writes the <CODE>wisdom</CODE> to
<CODE>output_file</CODE>, which must be a file open for
writing. <CODE>fftw_import_wisdom_from_file</CODE> reads the <CODE>wisdom</CODE>
from <CODE>input_file</CODE>, which must be a file open for reading, and
returns <CODE>FFTW_SUCCESS</CODE> if successful and <CODE>FFTW_FAILURE</CODE>
otherwise. In both cases, the file is left open and must be closed by
the caller.  It is perfectly fine if other data lie before or after the
<CODE>wisdom</CODE> in the file, as long as the file is positioned at the
beginning of the <CODE>wisdom</CODE> data before import.



<PRE>
char *fftw_export_wisdom_to_string(void);
fftw_status fftw_import_wisdom_from_string(const char *input_string)
</PRE>

<P>
<CODE>fftw_export_wisdom_to_string</CODE> allocates a string, exports the
<CODE>wisdom</CODE> to it in <CODE>NULL</CODE>-terminated format, and returns a
pointer to the string.  If there is an error in allocating or writing
the data, it returns <CODE>NULL</CODE>.  The caller is responsible for
deallocating the string (with <CODE>fftw_free</CODE>) when she is done with
it. <CODE>fftw_import_wisdom_from_string</CODE> imports the <CODE>wisdom</CODE> from
<CODE>input_string</CODE>, returning <CODE>FFTW_SUCCESS</CODE> if successful and
<CODE>FFTW_FAILURE</CODE> otherwise.


<P>
Exporting <CODE>wisdom</CODE> does not affect the store of <CODE>wisdom</CODE>. Imported
<CODE>wisdom</CODE> supplements the current store rather than replacing it
(except when there is conflicting <CODE>wisdom</CODE>, in which case the older
<CODE>wisdom</CODE> is discarded). The format of the exported <CODE>wisdom</CODE> is
"nerd-readable" LISP-like ASCII text; we will not document it here
except to note that it is insensitive to white space (interested users
can contact us for more details).


<P>
See section <A HREF="fftw_5.html#SEC11">FFTW Reference</A>, for more information, and for a description of
how you can implement <CODE>wisdom</CODE> import/export for other media
besides files and strings.


<P>
The following is a brief example in which the <CODE>wisdom</CODE> is read from
a file, a plan is created (possibly generating more <CODE>wisdom</CODE>), and
then the <CODE>wisdom</CODE> is exported to a string and printed to
<CODE>stdout</CODE>.



<PRE>
{
     fftw_plan plan;
     char *wisdom_string;
     FILE *input_file;

     /* open file to read wisdom from */
     input_file = fopen("sample.wisdom", "r");
     if (FFTW_FAILURE == fftw_import_wisdom_from_file(input_file))
          printf("Error reading wisdom!\n");
     fclose(input_file); /* be sure to close the file! */

     /* create a plan for N=64, possibly creating and/or using wisdom */
     plan = fftw_create_plan(64,FFTW_FORWARD,
                             FFTW_MEASURE | FFTW_USE_WISDOM);

     /* ... do some computations with the plan ... */

     /* always destroy plans when you are done */
     fftw_destroy_plan(plan); 

     /* write the wisdom to a string */
     wisdom_string = fftw_export_wisdom_to_string();
     if (wisdom_string != NULL) {
          printf("Accumulated wisdom: %s\n",wisdom_string);

          /* Just for fun, destroy and restore the wisdom */
          fftw_forget_wisdom(); /* all gone! */
          fftw_import_wisdom_from_string(wisdom_string); 
          /* wisdom is back! */

          fftw_free(wisdom_string); /* deallocate it since we're done */
     }
}
</PRE>

<HR>
Go to the <A HREF="fftw_1.html">first</A>, <A HREF="fftw_3.html">previous</A>, <A HREF="fftw_5.html">next</A>, <A HREF="fftw_9.html">last</A> section, <A HREF="fftw_toc.html">table of contents</A>.
</BODY>
</HTML>
