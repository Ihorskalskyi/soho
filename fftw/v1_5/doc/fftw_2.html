<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HTML>
<HEAD>
<!-- This HTML file has been created by texi2html 1.45
     from fftw.texi on 10 April 1998 -->

<TITLE>FFTW 1.3 User's Manual - Quick Start</TITLE>
</HEAD>
<BODY TEXT="#000000" BGCOLOR="#FFFFFF">
Go to the <A HREF="fftw_1.html">first</A>, <A HREF="fftw_1.html">previous</A>, <A HREF="fftw_3.html">next</A>, <A HREF="fftw_9.html">last</A> section, <A HREF="fftw_toc.html">table of contents</A>.
<HR>


<H1><A NAME="SEC2">Quick Start</A></H1>
<P>
<A NAME="IDX1"></A>
This chapter describes the basic usage of FFTW for computing complex
one-dimensional Fourier Transforms.  This chapter tells the truth, but
not the <EM>whole</EM> truth. See section <A HREF="fftw_5.html#SEC11">FFTW Reference</A>, for more information.
(Note that you need to compile and install FFTW before you can use it in
a program.  See section <A HREF="fftw_7.html#SEC35">Installation</A>.)


<P>
Using FFTW is very simple.  First of all, include <CODE>&#60;fftw.h&#62;</CODE>
at the top of your program:



<PRE>
#include &#60;fftw.h&#62;
</PRE>

<P>
Then, in order to use FFTW, you must first create a <STRONG>plan</STRONG>.  A plan
is an object that contains all the data FFTW needs to compute the
Fourier Transform.  The plan is created by <CODE>fftw_create_plan</CODE>:



<PRE>
fftw_plan fftw_create_plan(int n, fftw_direction dir,
                           int flags);
</PRE>

<P>
The function <CODE>fftw_create_plan</CODE> accepts three arguments.  The first
argument, <CODE>n</CODE>, is the size of the transform you are trying to
compute.  It can be any integer greater than 0.  The second argument,
<CODE>dir</CODE>, can be either <CODE>FFTW_FORWARD</CODE> or <CODE>FFTW_BACKWARD</CODE>,
and indicates which transform you are interested in.  Alternatively, you
can use the sign of the exponent in the transform, <MATH>-1</MATH> or
<MATH>+1</MATH>, which correspond to <CODE>FFTW_FORWARD</CODE> and
<CODE>FFTW_BACKWARD</CODE>, respectively.  The <CODE>flags</CODE> argument is either
<CODE>FFTW_MEASURE</CODE> or <CODE>FFTW_ESTIMATE</CODE>.  <CODE>FFTW_MEASURE</CODE> means
that FFTW actually runs and measures the execution time of several FFTs
in order to find the best way to compute the transform of size <CODE>n</CODE>.
This may take some time, depending on your installation and on the
precision of your clock.  <CODE>FFTW_ESTIMATE</CODE> does not run any
computation, and just builds a reasonable plan--which may be
sub-optimal.  In other words, if your program performs many transforms
of the same size and initialization time is not important, use
<CODE>FFTW_MEASURE</CODE>; otherwise use the estimate.


<P>
Once the plan has been created, you can use it as many times as you like
for arrays of the same size.  The <CODE>fftw</CODE> function accepts several
arguments:



<PRE>
void fftw(fftw_plan plan, int howmany,
          FFTW_COMPLEX *in, int istride, int idist,
          FFTW_COMPLEX *out, int ostride, int odist);
</PRE>

<P>
This means: compute <CODE>howmany</CODE> FFTs<A NAME="FOOT1" HREF="fftw_foot.html#FOOT1">(1)</A> according to the plan <CODE>plan</CODE> (the size of
the transform is specified by the plan).  The input array is pointed to
by <CODE>in</CODE>, and consists of <CODE>howmany</CODE> subarrays, placed at a
distance <CODE>idist</CODE> from each other, and whose elements are
<CODE>istride</CODE> apart.  In other words, the <CODE>i</CODE>-th element of the
<CODE>j</CODE>-th subarray will be in position <CODE>in[i * istride + j *
idist]</CODE>, where <CODE>i</CODE> and <CODE>j</CODE> are both zero-based..  The result
will be stored in the <CODE>out</CODE> array, which consists of <CODE>howmany</CODE>
subarrays, placed at a distance <CODE>odist</CODE> from each other, and whose
elements are <CODE>ostride</CODE> apart.  <EM>The input and output arrays
must be distinct</EM>.  For example, in order to compute just one transform
of an array, use



<PRE>
fftw(plan, 1, in, 1, 0, out, 1, 0);
</PRE>

<P>
Finally, don't forget to destroy the plan once you are done using it:



<PRE>
void fftw_destroy_plan(fftw_plan plan)
</PRE>

<P>
This frees all the memory and data structures associated with the plan.




<H2><A NAME="SEC3">What FFTW really computes.</A></H2>

<P>
FFTW computes an unnormalized transform.  This is the convention that
<MATH>IFFT(FFT(X)) = n X</MATH>.  In other words, applying the forward and
then the backward transform will multiply the input by <MATH>n</MATH>.


<P>
An <CODE>FFTW_FORWARD</CODE> transform corresponds to a sign of <MATH>-1</MATH> in
the exponent of the DFT.  Note also that we use the standard
"in-order" output ordering--the <MATH>k</MATH>-th output corresponds to the
frequency <MATH>k/n</MATH> (or <MATH>k/T</MATH>, where <MATH>T</MATH> is your total
sampling period).  For those who like to think in terms of positive and
negative frequencies, this means that the positive frequencies are
stored in the first half of the output and the negative frequencies are
stored in backwards order in the second half of the output.  (Because of
aliasing, the frequency <MATH>-k/n</MATH> is the same as the frequency
<MATH>(n-k)/n</MATH>.)


<P>
More precisely, the forward transform of an array <MATH>X</MATH> of size
<MATH>n</MATH> computes an array <MATH>Y</MATH>, where
<center><IMG SRC="equation-1.gif" ALIGN="top"></center>


<P>
The backward transform computes
<center><IMG SRC="equation-2.gif" ALIGN="top"></center>




<H2><A NAME="SEC4">Example program using FFTW.</A></H2>

<P>
The following simple program illustrates the intended use
of FFTW (for one-dimensional transforms).



<PRE>
#include &#60;fftw.h&#62;
#include &#60;stdlib.h&#62;
#include &#60;stdio.h&#62;

void example(void)
{
     int n = 16;
     int i;
     FFTW_COMPLEX *in = (FFTW_COMPLEX *) malloc(n * sizeof(FFTW_COMPLEX));
     FFTW_COMPLEX *out = (FFTW_COMPLEX *) malloc(n * sizeof(FFTW_COMPLEX));
     fftw_plan plan;

     plan = fftw_create_plan(n, FFTW_FORWARD, FFTW_MEASURE);
     if (plan == NULL) {
          fprintf(stderr, "Error creating plan\n");
          exit(-1);
     }

     /* initialize to arbitrary values */
     for (i = 0; i &#60; n; ++i) {
          c_re(in[i]) = i;
          c_im(in[i]) = -i;
     }

     fftw(plan, 1, in, 1, 0, out, 1, 0);

     /* now use the output array */
     /* ... */
}
</PRE>

<P>
The program should be linked with <CODE>-lfftw -lm</CODE> on Unix systems, or
with the FFTW and standard math libraries in general.


<HR>
Go to the <A HREF="fftw_1.html">first</A>, <A HREF="fftw_1.html">previous</A>, <A HREF="fftw_3.html">next</A>, <A HREF="fftw_9.html">last</A> section, <A HREF="fftw_toc.html">table of contents</A>.
</BODY>
</HTML>
