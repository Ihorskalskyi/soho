/*
 * Leamax binding in C 
 */
#define DEBUG 0

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "dervish.h"
#include "leamax_bind.h"

#ifdef __cplusplus
extern "C"  {
#endif

/*
 * Prototypes for Leamax functions
 */

void dfunft_(
	     void (*subs)(
			  int *k,
			  double *x,
			  int *n, 
			  double *a, 
			  double *f, 
			  double *df, 
			  int *mode, 
			  int *nerror), 
	     int *k, 
	     int *m, 
	     int *n, 
	     int *nx,
	     int *nc, 
	     double *x, 
	     double *y, 
	     double *sy,
	     double *a, 
	     double *al, 
	     double *au, 
	     int *mode, 
	     double *eps, 
	     int *maxit, 
	     int *iprt, 
	     int *mfr, 
	     int *iafr, 
	     double *phi, 
	     double *dphi, 
	     double *cov, 
	     double *std, 
	     double *w, 
	     int *nerr
	     );

void dmaxlk_(
	     void (*subs)(
			  int *k,
			  double *x,
			  int *n, 
			  double *a, 
			  double *f, 
			  double *df, 
			  int *mode, 
			  int *nerror), 
	     int *k, 
	     int *m, 
	     int *n, 
	     int *nx,
	     double *x, 
	     double *a, 
	     double *al, 
	     double *au, 
	     int *mode, 
	     double *eps, 
	     int *maxit, 
	     int *iprt, 
	     int *mfr, 
	     int *iafr, 
	     double *phi, 
	     double *dphi, 
	     double *w, 
	     int *nerr
	     );

#ifdef __cplusplus
}
#endif


void smaDfunft(
	       void (*subs)(
			    int *k,
			    double *xOnePt,
			    int *n, 
			    double *a, 
			    double *f, 
			    double *df,
			    int *mode, 
			    int *nerror), 
	       int k,		/* 1st dimension of x */
	       int m,		/* number of data points */
	       int n,		/* number of parameters */
	       int nx,		/* first dimension of array X */
	       int nc,		/* first dimension of COV (usually n)*/
	       double *xAllPt,	/* independent variables */
	       double *y,	/* dependent variables */
	       double *sy,	/* errors in y */
               double *a,	/* the parameters */
	       double *al,	/* lower limits on a */
	       double *au,	/* upper limits on a */
	       int mode,	/* 1 if SUB calculates derivatives */
	       double eps,	/* tolerence:  1e-12 */
               int maxit,	/* maximum number of iterations */
	       int iprt,	/* 0 for quiet operation */
	       int *mfr,	/* number of free variables at end */
	       int *iafr,	/* scratch space, must be > 2*n */
	       double *phi,	/* chi2 at the end */
               double *dphi,	/* array of dphi/dparam */
	       double *cov,	/* 2d array (nc, n) of covariance */
	       double *std,	/* error in parameters */
	       double *w,	/* scratch space, > 9*n+4*m+2*m*n+3*n*n */
	       int *nerr	/* returned 0 if all is well */
	       ) {

  int nw, nf;
  nf = k;
  nw = 9*nc+4*nf+2*nf*nc+3*nc*nc;
  memset(iafr, 0, 2*nc*sizeof(int));
  *phi = 0;
  memset(dphi, 0, nc*sizeof(double));
  memset(cov,  0, nc*nc*sizeof(double));
  memset(std,  0, nc*sizeof(double));
  memset(w,    0, nw*sizeof(double));
  dfunft_(subs, 
	  &k, 
	  &m, 
	  &n, 
	  &nx, 
	  &nc, 
	  xAllPt, 
	  y, sy, 
	  a, al, au, &mode, &eps, &maxit, &iprt, mfr,
	  iafr, phi, dphi, cov, std, w, nerr);
}

void smaDmaxlk(
	       void (*subs)(
			    int *k,
			    double *xOnePt,
			    int *n, 
			    double *a, 
			    double *f, 
			    double *df,
			    int *mode, 
			    int *nerror), 
	       int k,		/* 1st dimension of x */
	       int m,		/* number of data points */
	       int n,		/* number of parameters */
	       int nx,		/* first dimension of array X */
	       double *xAllPt,	/* independent variables */
               double *a,	/* the parameters */
	       double *al,	/* lower limits on a */
	       double *au,	/* upper limits on a */
	       int mode,	/* 1 if SUB calculates derivatives */
	       double eps,	/* tolerence:  1e-12 */
               int maxit,	/* maximum number of iterations */
	       int iprt,	/* 0 for quiet operation */
	       int *mfr,	/* number of free variables at end */
	       int *iafr,       /* indices of the free variables */
	       double *phi,	/* chi2 at the end */
               double *dphi,	/* array of dphi/dparam */
	       int *nerr	/* returned 0 if all is well */
	       ) {
#if DEBUG
  int i;
#endif
  double *w;
  int nw;
  nw = 7*n + 2*n*n;
  w = (double *) shMalloc(nw*sizeof(double));
  memset(w, 0, nw*sizeof(double));

  memset(iafr, 0, n*sizeof(int));
  *phi = 0;
  memset(dphi, 0, n*sizeof(double));
#if DEBUG
  printf("before calling dmaxlk_\n");
  printf("k=%d  m=%d  n=%d  nx=%d  nw=%d\n", k, m, n, nx, nw);
  for (i=0; i<m; i++) {
    printf("i=%d  xAllPt[i]= %f\n", i, xAllPt[i]);
  }
  for (i=0; i<n; i++) {
    printf("i=%d  a=%f al=%f au=%f\n", i, a[i], al[i], au[i]);
  }
  printf("mode=%d   eps=%g   maxid=%d  iprt=%d\n", mode, eps, maxit, iprt);
  printf("mfr=%d\n", *mfr);
  for (i=0; i<n; i++) {
    printf("i=%d  iafr[i]=%d  dphi[i]=%f\n", i, iafr[i], dphi[i]);
  }
  for (i=0; i<nw; i++) {
    printf("i=%d  w[i]=%f\n", i, w[i]);
  }
#endif
  dmaxlk_(subs, &k, &m, &n, &nx, xAllPt, 
	  a, al, au, &mode, &eps, &maxit, &iprt, mfr,
	  iafr, phi, dphi, w, nerr);
  shFree(w);
}











