/* WARNING -- this file was generated automatically */
/* by sdssmath/etc/makeTclBindings.pl */
/* <AUTO>
FILE: tcl_func.c
Bindings for FUNC
</AUTO>
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include "dervish.h"
#include "sdssmath.h"
static char *tclFacil  = "slatec";
/*********************************************************
**
** ROUTINE tcl_funcNew 
**
**<AUTO EXTRACT>
** TCL VERB: funcNew
**</AUTO>
*/
char g_funcNew[] = "funcNew";
ftclArgvInfo g_funcNewTbl[] = {
{NULL, FTCL_ARGV_HELP, NULL, NULL,
"Return the handle to a func.\n"
"	gauss is a gaussian; parameters are position, sigma, and normalization\n"
"	hubble is Hubble power law; parameters are I0 and a\n"
"	devauc is de Vaucouleurs law; parameters are Ie and Re\n"
"	freeman is Freeman power law; parameters are I0 and r0\n"
"Return the handle to the new func.  \n"
},
{"<name>", FTCL_ARGV_STRING,NULL, NULL, "Kind of the func to return"},
{"[list]", FTCL_ARGV_STRING,NULL, NULL, "Optional list of VECTORs"},
{(char *) NULL, FTCL_ARGV_END, NULL, NULL, (char *) NULL}
};
static int
tcl_funcNew(ClientData clientData, 
Tcl_Interp *interp, int argc, char **argv) {
int rstat;
char tempString[100];
char* name = NULL; 
char* list = NULL; 
char *handle=NULL;
int nHandle=0;
VECTOR **va=NULL;
char handle_func[HANDLE_NAMELEN];
FUNC* func = NULL;
g_funcNewTbl[1].dst = &name;
g_funcNewTbl[2].dst = &list;
if ((rstat = shTclParseArgv(interp, &argc, argv,g_funcNewTbl, FTCL_ARGV_NO_LEFTOVERS, g_funcNew))!= FTCL_ARGV_SUCCESS) return rstat;

if (list !=NULL) {
	handle = strtok(list, " ," );
	while (handle != NULL) {
		nHandle++;
		handle = strtok(NULL, " ," );
	}

	if (nHandle > 0) {
		va = (VECTOR **) shMalloc(nHandle*sizeof(VECTOR *));
	}
	nHandle = 0;
	handle = strtok(list, " ,");
	while (handle != NULL) {
		if (shTclAddrGetFromName(interp, handle, 
			(void **) &va[nHandle],
			"VECTOR" ) != TCL_OK) {
			Tcl_SetResult(interp, "Bad VECTOR handle",
				TCL_VOLATILE);
			return TCL_ERROR;
		}
		p_shTclHandleDel(interp, handle);
		nHandle++;
		handle = strtok(NULL, " ," );
	}
}

func = smFuncNew(name, va, nHandle);

if (func == NULL) {
	Tcl_SetResult(interp, "funcNew: no func with that name", TCL_VOLATILE);
	return TCL_ERROR;
}
if (shTclHandleNew(interp, handle_func, "FUNC", func)
!= TCL_OK) { Tcl_SetResult(interp, 
"Error getting func handle",
TCL_VOLATILE); return TCL_ERROR;}
sprintf(tempString, "%s", handle_func);
Tcl_AppendElement(interp, tempString);
return TCL_OK;
}
/*********************************************************
**
** ROUTINE tcl_funcDel 
**
**<AUTO EXTRACT>
** TCL VERB: funcDel
**</AUTO>
*/
char g_funcDel[] = "funcDel";
ftclArgvInfo g_funcDelTbl[] = {
{NULL, FTCL_ARGV_HELP, NULL, NULL,
"Delete a FUNC and its handle\n"
},
{"<func>", FTCL_ARGV_STRING,NULL, NULL, "The FUNC to delete"},
{(char *) NULL, FTCL_ARGV_END, NULL, NULL, (char *) NULL}
};
static int
tcl_funcDel(ClientData clientData, 
Tcl_Interp *interp, int argc, char **argv) {
int rstat;
FUNC* func = NULL; 
char* handle_func = NULL;

g_funcDelTbl[1].dst = &handle_func;
if ((rstat = shTclParseArgv(interp, &argc, argv,g_funcDelTbl, FTCL_ARGV_NO_LEFTOVERS, g_funcDel))!= FTCL_ARGV_SUCCESS) return rstat;
if (handle_func==NULL) {
func = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_func,
(void **) &func, "FUNC") != TCL_OK) {
Tcl_SetResult(interp, "Bad FUNC for func",
TCL_VOLATILE); return TCL_ERROR;}
}
smFuncDel(func);
p_shTclHandleDel(interp, handle_func);
return TCL_OK;
}
/*********************************************************
**
** ROUTINE tcl_funcEval 
**
**<AUTO EXTRACT>
** TCL VERB: funcEval
**</AUTO>
*/
char g_funcEval[] = "funcEval";
ftclArgvInfo g_funcEvalTbl[] = {
{NULL, FTCL_ARGV_HELP, NULL, NULL,
"Evaluate a FUNC at x\n"
"Reurn the evaluated value \n"
},
{"<func>", FTCL_ARGV_STRING,NULL, NULL, "The FUNC to evaluate"},
{"<x>", FTCL_ARGV_DOUBLE,NULL, NULL, "Where to evaluate FUNC"},
{(char *) NULL, FTCL_ARGV_END, NULL, NULL, (char *) NULL}
};
static int
tcl_funcEval(ClientData clientData, 
Tcl_Interp *interp, int argc, char **argv) {
int rstat;
char tempString[100];
FUNC* func = NULL; 
char* handle_func = NULL;
double x = 0.0; 
double y = 0.0;
g_funcEvalTbl[1].dst = &handle_func;
g_funcEvalTbl[2].dst = &x;
if ((rstat = shTclParseArgv(interp, &argc, argv,g_funcEvalTbl, FTCL_ARGV_NO_LEFTOVERS, g_funcEval))!= FTCL_ARGV_SUCCESS) return rstat;
if (handle_func==NULL) {
func = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_func,
(void **) &func, "FUNC") != TCL_OK) {
Tcl_SetResult(interp, "Bad FUNC for func",
TCL_VOLATILE); return TCL_ERROR;}
}
if (smaFuncEval(func, x, &y) != SH_SUCCESS) {
	Tcl_SetResult(interp, "error from smaFuncEval\n", TCL_VOLATILE);
	shTclInterpAppendWithErrStack(interp);
	return TCL_ERROR;
}
sprintf(tempString, "%g", y);
Tcl_AppendElement(interp, tempString);
return TCL_OK;
}
/*********************************************************
**
** ROUTINE tcl_funcPlot 
**
**<AUTO EXTRACT>
** TCL VERB: funcPlot
**</AUTO>
*/
char g_funcPlot[] = "funcPlot";
ftclArgvInfo g_funcPlotTbl[] = {
{NULL, FTCL_ARGV_HELP, NULL, NULL,
"Plot a FUNC from xmin to xmax\n"
},
{"<pg>", FTCL_ARGV_STRING,NULL, NULL, "Where to plot"},
{"<func>", FTCL_ARGV_STRING,NULL, NULL, "The FUNC to evaluate"},
{"-resFunc", FTCL_ARGV_STRING,NULL, NULL, "FUNC to subract at each point"},
{"<xmin>", FTCL_ARGV_DOUBLE,NULL, NULL, "Starting x value"},
{"<xmax>", FTCL_ARGV_DOUBLE,NULL, NULL, "Ending x value"},
{"<npt>", FTCL_ARGV_INT,NULL, NULL, "Number of points to plot"},
{(char *) NULL, FTCL_ARGV_END, NULL, NULL, (char *) NULL}
};
static int
tcl_funcPlot(ClientData clientData, 
Tcl_Interp *interp, int argc, char **argv) {
int rstat;
char tempString[100];
PGSTATE* pg = NULL; 
char* handle_pg = NULL;
FUNC* func = NULL; 
char* handle_func = NULL;
FUNC* resFunc = NULL; 
char* handle_resFunc = NULL;
double xmin = -10.0; 
double xmax = 10.0; 
int npt = 100; 
int nptOut;
g_funcPlotTbl[1].dst = &handle_pg;
g_funcPlotTbl[2].dst = &handle_func;
g_funcPlotTbl[3].dst = &handle_resFunc;
g_funcPlotTbl[4].dst = &xmin;
g_funcPlotTbl[5].dst = &xmax;
g_funcPlotTbl[6].dst = &npt;
if ((rstat = shTclParseArgv(interp, &argc, argv,g_funcPlotTbl, FTCL_ARGV_NO_LEFTOVERS, g_funcPlot))!= FTCL_ARGV_SUCCESS) return rstat;
if (handle_pg==NULL) {
pg = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_pg,
(void **) &pg, "PGSTATE") != TCL_OK) {
Tcl_SetResult(interp, "Bad PGSTATE for pg",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_func==NULL) {
func = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_func,
(void **) &func, "FUNC") != TCL_OK) {
Tcl_SetResult(interp, "Bad FUNC for func",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_resFunc==NULL) {
resFunc = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_resFunc,
(void **) &resFunc, "FUNC") != TCL_OK) {
Tcl_SetResult(interp, "Bad FUNC for resFunc",
TCL_VOLATILE); return TCL_ERROR;}
}
nptOut = smaFuncPlot(pg, func, resFunc, xmin, xmax, npt);
sprintf(tempString, "%d", nptOut);
Tcl_AppendElement(interp, tempString);
return TCL_OK;
}
/*********************************************************
**
** ROUTINE tcl_methNew 
**
**<AUTO EXTRACT>
** TCL VERB: methNew
**</AUTO>
*/
char g_methNew[] = "methNew";
ftclArgvInfo g_methNewTbl[] = {
{NULL, FTCL_ARGV_HELP, NULL, NULL,
"Return the handle to a meth.\n"
"	chi2 is for least squares minimization\n"
"The handle.\n"
},
{"<name>", FTCL_ARGV_STRING,NULL, NULL, "Kind of meth to retrn"},
{(char *) NULL, FTCL_ARGV_END, NULL, NULL, (char *) NULL}
};
static int
tcl_methNew(ClientData clientData, 
Tcl_Interp *interp, int argc, char **argv) {
int rstat;
char tempString[100];
char* name = NULL; 
char handle_meth[HANDLE_NAMELEN];
METH* meth = NULL;
g_methNewTbl[1].dst = &name;
if ((rstat = shTclParseArgv(interp, &argc, argv,g_methNewTbl, FTCL_ARGV_NO_LEFTOVERS, g_methNew))!= FTCL_ARGV_SUCCESS) return rstat;
if ((meth = smMethNew(name)) == NULL) {
	Tcl_SetResult(interp, "methNew: no meth with that name", TCL_VOLATILE);
	return TCL_ERROR;
}
if (shTclHandleNew(interp, handle_meth, "METH", meth)
!= TCL_OK) { Tcl_SetResult(interp, 
"Error getting meth handle",
TCL_VOLATILE); return TCL_ERROR;}
sprintf(tempString, "%s", handle_meth);
Tcl_AppendElement(interp, tempString);
return TCL_OK;
}
/*********************************************************
**
** ROUTINE tcl_methDel 
**
**<AUTO EXTRACT>
** TCL VERB: methDel
**</AUTO>
*/
char g_methDel[] = "methDel";
ftclArgvInfo g_methDelTbl[] = {
{NULL, FTCL_ARGV_HELP, NULL, NULL,
"Delete a METH and its handle\n"
},
{"<meth>", FTCL_ARGV_STRING,NULL, NULL, "The METH to delete"},
{(char *) NULL, FTCL_ARGV_END, NULL, NULL, (char *) NULL}
};
static int
tcl_methDel(ClientData clientData, 
Tcl_Interp *interp, int argc, char **argv) {
int rstat;
METH* meth = NULL; 
char* handle_meth = NULL;

g_methDelTbl[1].dst = &handle_meth;
if ((rstat = shTclParseArgv(interp, &argc, argv,g_methDelTbl, FTCL_ARGV_NO_LEFTOVERS, g_methDel))!= FTCL_ARGV_SUCCESS) return rstat;
if (handle_meth==NULL) {
meth = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_meth,
(void **) &meth, "METH") != TCL_OK) {
Tcl_SetResult(interp, "Bad METH for meth",
TCL_VOLATILE); return TCL_ERROR;}
}
smMethDel(meth);
p_shTclHandleDel(interp, handle_meth);
return TCL_OK;
}
/*********************************************************
**
** ROUTINE tcl_dnls1 
**
**<AUTO EXTRACT>
** TCL VERB: dnls1
**</AUTO>
*/
char g_dnls1[] = "dnls1";
ftclArgvInfo g_dnls1Tbl[] = {
{NULL, FTCL_ARGV_HELP, NULL, NULL,
"The purpose of DNLS1 is to minimize the sum of the squares of M\n"
"nonlinear functions in N variables by a modification of the\n"
"Levenberg-Marquardt algorithm.  The user must provide a subrou-\n"
"tine which calculates the functions.  The user has the option\n"
"of how the Jacobian will be supplied.  The user can supply the\n"
"full Jacobian, or the rows of the Jacobian (to avoid storing\n"
"the full Jacobian), or let the code approximate the Jacobian by\n"
"forward-differencing.   This code is the combination of the\n"
"MINPACK codes (Argonne) LMDER, LMDIF, and LMSTR.\n"
"Returns a keyed list of func, info, nfev, njev\n"
},
{"<meth>", FTCL_ARGV_STRING,NULL, NULL, "how to minimize"},
{"<func>", FTCL_ARGV_STRING,NULL, NULL, "the function to fit"},
{"<x>", FTCL_ARGV_STRING,NULL, NULL, "vector of x values"},
{"<y>", FTCL_ARGV_STRING,NULL, NULL, "vector of y values"},
{"<w>", FTCL_ARGV_STRING,NULL, NULL, "vector of weights"},
{"-mask", FTCL_ARGV_STRING,NULL, NULL, "mask vector"},
{"-iopt", FTCL_ARGV_INT,NULL, NULL, "only 1 works now"},
{"-ftol", FTCL_ARGV_DOUBLE,NULL, NULL, "relative error in sum of squares to terminate iteration; defaults to square root of the machine precisionn"},
{"-xtol", FTCL_ARGV_DOUBLE,NULL, NULL, "relative error in successive iterations to terminate iteration; defaults to square root of the machine precision"},
{"-gtol", FTCL_ARGV_DOUBLE,NULL, NULL, "tolerence on cos of angle between fvec and any column of the Jacobian to terminate iteration; defaults to zero"},
{"-maxfev", FTCL_ARGV_INT,NULL, NULL, "maximum number of calls to meth; defaults to 200*(n+1)"},
{"-epsfcn", FTCL_ARGV_DOUBLE,NULL, NULL, "step size; defaults to machine precision"},
{"<diag>", FTCL_ARGV_STRING,NULL, NULL, "if mode=2, n entries that serve as implicit multiplicative scale factors for the parameters"},
{"-mode", FTCL_ARGV_INT,NULL, NULL, "1 variables scaled internally; 2 scaling specified by diag"},
{"-factor", FTCL_ARGV_DOUBLE,NULL, NULL, "used to determine initial step bound"},
{"-nprint", FTCL_ARGV_INT,NULL, NULL, "each nprint call to meth has iflag=0"},
{(char *) NULL, FTCL_ARGV_END, NULL, NULL, (char *) NULL}
};
static int
tcl_dnls1(ClientData clientData, 
Tcl_Interp *interp, int argc, char **argv) {
int rstat;
char tempString[100];
METH* meth = NULL; 
char* handle_meth = NULL;
FUNC* func = NULL; 
char* handle_func = NULL;
VECTOR* x = NULL; 
char* handle_x = NULL;
VECTOR* y = NULL; 
char* handle_y = NULL;
VECTOR* w = NULL; 
char* handle_w = NULL;
VECTOR* mask = NULL; 
char* handle_mask = NULL;
int iopt = 1; 
double ftol = 0; 
double xtol = 0; 
double gtol = 0; 
int maxfev = 0; 
double epsfcn = 0; 
VECTOR* diag = NULL; 
char* handle_diag = NULL;
int mode = 1; 
double factor = 100.0; 
int nprint = 0; 
int i, n, m;
double *param, *fvec, *fjac, *qtf, *wa1, *wa2, *wa3, *wa4;
int ldfjac, info, nfev, njev;
int *ipvt;

g_dnls1Tbl[1].dst = &handle_meth;
g_dnls1Tbl[2].dst = &handle_func;
g_dnls1Tbl[3].dst = &handle_x;
g_dnls1Tbl[4].dst = &handle_y;
g_dnls1Tbl[5].dst = &handle_w;
g_dnls1Tbl[6].dst = &handle_mask;
g_dnls1Tbl[7].dst = &iopt;
g_dnls1Tbl[8].dst = &ftol;
g_dnls1Tbl[9].dst = &xtol;
g_dnls1Tbl[10].dst = &gtol;
g_dnls1Tbl[11].dst = &maxfev;
g_dnls1Tbl[12].dst = &epsfcn;
g_dnls1Tbl[13].dst = &handle_diag;
g_dnls1Tbl[14].dst = &mode;
g_dnls1Tbl[15].dst = &factor;
g_dnls1Tbl[16].dst = &nprint;
if ((rstat = shTclParseArgv(interp, &argc, argv,g_dnls1Tbl, FTCL_ARGV_NO_LEFTOVERS, g_dnls1))!= FTCL_ARGV_SUCCESS) return rstat;
if (handle_meth==NULL) {
meth = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_meth,
(void **) &meth, "METH") != TCL_OK) {
Tcl_SetResult(interp, "Bad METH for meth",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_func==NULL) {
func = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_func,
(void **) &func, "FUNC") != TCL_OK) {
Tcl_SetResult(interp, "Bad FUNC for func",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_x==NULL) {
x = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_x,
(void **) &x, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for x",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_y==NULL) {
y = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_y,
(void **) &y, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for y",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_w==NULL) {
w = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_w,
(void **) &w, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for w",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_mask==NULL) {
mask = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_mask,
(void **) &mask, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for mask",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_diag==NULL) {
diag = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_diag,
(void **) &diag, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for diag",
TCL_VOLATILE); return TCL_ERROR;}
}
n = func->va[0]->dimen;
m = x->dimen;

param = (double *) shMalloc(n*sizeof(double));
for (i=0; i<n; i++) param[i] = func->va[0]->vec[i];
fvec = (double *) shMalloc(m*sizeof(double));
fjac = (double *) shMalloc(m*n*sizeof(double));
for (i=0; i<n*m; i++) fjac[i]=-1;

if ((iopt==1) || (iopt==2)) {
	ldfjac = m;
} else {
	ldfjac = n;
}
if (maxfev == 0) maxfev=200*(n+1);

ipvt =    (int *) shMalloc(n*sizeof(int));
qtf  = (double *) shMalloc(n*sizeof(double));
wa1  = (double *) shMalloc(n*sizeof(double));
wa2  = (double *) shMalloc(n*sizeof(double));
wa3  = (double *) shMalloc(n*sizeof(double));
wa4  = (double *) shMalloc(m*sizeof(double));

if (ftol==0) ftol=sqrt(smaD1MACH(4));
if (xtol==0) xtol=sqrt(smaD1MACH(4));
smaGlobalSet(x, y, w, func, mask);
smaDNLS1(meth->ma, iopt, m, n, param, fvec, fjac, ldfjac, ftol,
	xtol, gtol, maxfev, epsfcn, diag->vec, mode, factor, nprint, &info,
	&nfev, &njev, ipvt, qtf, wa1, wa2, wa3, wa4);
for (i=0; i<n; i++) func->va[0]->vec[i] = param[i];
smaCovFromFit(meth, func, epsfcn);

shFree(param); 
shFree(fvec); 
shFree(fjac); 
shFree(ipvt); 
shFree(qtf); 
shFree(wa1); 
shFree(wa2); 
shFree(wa3); 
shFree(wa4); 

sprintf(tempString, "%d info", info);
Tcl_AppendElement(interp, tempString);
sprintf(tempString, "%d nfev", nfev);
Tcl_AppendElement(interp, tempString);
sprintf(tempString, "%d njev", njev);
Tcl_AppendElement(interp, tempString);
return TCL_OK;
}
/*********************************************************
**
** ROUTINE tcl_dnls1e 
**
**<AUTO EXTRACT>
** TCL VERB: dnls1e
**</AUTO>
*/
char g_dnls1e[] = "dnls1e";
ftclArgvInfo g_dnls1eTbl[] = {
{NULL, FTCL_ARGV_HELP, NULL, NULL,
"The purpose of DNLS1E is to minimize the sum of the squares of M\n"
"nonlinear functions in N variables by a modification of the\n"
"Levenberg-Marquardt algorithm.  This is done by using the more\n"
"general least-squares solver DNLS1.  The user must provide a\n"
"subroutine which calculates the functions.  The user has the\n"
"option of how the Jacobian will be supplied.  The user can\n"
"supply the full Jacobian, or the rows of the Jacobian (to avoid\n"
"storing the full Jacobian), or let the code approximate the\n"
"Jacobian by forward-differencing.  This code is the combination\n"
"of the MINPACK codes (Argonne) LMDER1, LMDIF1, and LMSTR1.\n"
"Returns info:  1, 2, or 3 are OK\n"
},
{"<meth>", FTCL_ARGV_STRING,NULL, NULL, "how to minimize"},
{"<func>", FTCL_ARGV_STRING,NULL, NULL, "the function to fit"},
{"<x>", FTCL_ARGV_STRING,NULL, NULL, "vector of x values"},
{"<y>", FTCL_ARGV_STRING,NULL, NULL, "vector of y values"},
{"<w>", FTCL_ARGV_STRING,NULL, NULL, "vector of weights"},
{"-mask", FTCL_ARGV_STRING,NULL, NULL, "mask vector"},
{"-nprint", FTCL_ARGV_INT,NULL, NULL, "number of calls with iflag=0"},
{"-iopt", FTCL_ARGV_INT,NULL, NULL, "only 1 works now"},
{"<tol>", FTCL_ARGV_DOUBLE,NULL, NULL, "relative error to define termination"},
{(char *) NULL, FTCL_ARGV_END, NULL, NULL, (char *) NULL}
};
static int
tcl_dnls1e(ClientData clientData, 
Tcl_Interp *interp, int argc, char **argv) {
int rstat;
char tempString[100];
METH* meth = NULL; 
char* handle_meth = NULL;
FUNC* func = NULL; 
char* handle_func = NULL;
VECTOR* x = NULL; 
char* handle_x = NULL;
VECTOR* y = NULL; 
char* handle_y = NULL;
VECTOR* w = NULL; 
char* handle_w = NULL;
VECTOR* mask = NULL; 
char* handle_mask = NULL;
int nprint = 0; 
int iopt = 1; 
double tol = 0; 
int *iw;
double *wa, *fvec, *param;
int i, m, n, info, lwa;
g_dnls1eTbl[1].dst = &handle_meth;
g_dnls1eTbl[2].dst = &handle_func;
g_dnls1eTbl[3].dst = &handle_x;
g_dnls1eTbl[4].dst = &handle_y;
g_dnls1eTbl[5].dst = &handle_w;
g_dnls1eTbl[6].dst = &handle_mask;
g_dnls1eTbl[7].dst = &nprint;
g_dnls1eTbl[8].dst = &iopt;
g_dnls1eTbl[9].dst = &tol;
if ((rstat = shTclParseArgv(interp, &argc, argv,g_dnls1eTbl, FTCL_ARGV_NO_LEFTOVERS, g_dnls1e))!= FTCL_ARGV_SUCCESS) return rstat;
if (handle_meth==NULL) {
meth = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_meth,
(void **) &meth, "METH") != TCL_OK) {
Tcl_SetResult(interp, "Bad METH for meth",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_func==NULL) {
func = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_func,
(void **) &func, "FUNC") != TCL_OK) {
Tcl_SetResult(interp, "Bad FUNC for func",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_x==NULL) {
x = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_x,
(void **) &x, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for x",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_y==NULL) {
y = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_y,
(void **) &y, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for y",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_w==NULL) {
w = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_w,
(void **) &w, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for w",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_mask==NULL) {
mask = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_mask,
(void **) &mask, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for mask",
TCL_VOLATILE); return TCL_ERROR;}
}
n = func->va[0]->dimen;
param = (double *) shMalloc(n*sizeof(double));
for (i=0; i<n; i++) {
	param[i] = func->va[0]->vec[i];
}

m = x->dimen;
iw = (int *) shMalloc(n*sizeof(int));
if ( (iopt==1) || (iopt==2) ) {
	lwa = n*(m+5)+m;
} else {
	lwa = n*(n+5)+m;
}
wa = (double *) shMalloc(lwa*sizeof(double));
fvec = (double *) shMalloc(m*sizeof(double));
smaGlobalSet(x, y, w, func, mask);
smaDNLS1E(meth->ma, iopt, m, n, param, fvec, 
	tol, nprint, &info, iw, wa, lwa);
for (i=0; i<n; i++) {
	func->va[0]->vec[i] = param[i];
}
shFree(param); shFree(iw); shFree(wa); shFree(fvec);
sprintf(tempString, "%d", info);
Tcl_AppendElement(interp, tempString);
return TCL_OK;
}
/*******************************************************/
/*** declare these tcl verbs ***/
void tcl_func_declare(Tcl_Interp *interp) {
int flags = FTCL_ARGV_NO_LEFTOVERS;
shTclDeclare(interp, g_funcNew, (Tcl_CmdProc *) tcl_funcNew,
(ClientData) 0, (Tcl_CmdDeleteProc *)0, tclFacil,
shTclGetArgInfo(interp, g_funcNewTbl, flags, g_funcNew),
shTclGetUsage(interp, g_funcNewTbl, flags, g_funcNew));
shTclDeclare(interp, g_funcDel, (Tcl_CmdProc *) tcl_funcDel,
(ClientData) 0, (Tcl_CmdDeleteProc *)0, tclFacil,
shTclGetArgInfo(interp, g_funcDelTbl, flags, g_funcDel),
shTclGetUsage(interp, g_funcDelTbl, flags, g_funcDel));
shTclDeclare(interp, g_funcEval, (Tcl_CmdProc *) tcl_funcEval,
(ClientData) 0, (Tcl_CmdDeleteProc *)0, tclFacil,
shTclGetArgInfo(interp, g_funcEvalTbl, flags, g_funcEval),
shTclGetUsage(interp, g_funcEvalTbl, flags, g_funcEval));
shTclDeclare(interp, g_funcPlot, (Tcl_CmdProc *) tcl_funcPlot,
(ClientData) 0, (Tcl_CmdDeleteProc *)0, tclFacil,
shTclGetArgInfo(interp, g_funcPlotTbl, flags, g_funcPlot),
shTclGetUsage(interp, g_funcPlotTbl, flags, g_funcPlot));
shTclDeclare(interp, g_methNew, (Tcl_CmdProc *) tcl_methNew,
(ClientData) 0, (Tcl_CmdDeleteProc *)0, tclFacil,
shTclGetArgInfo(interp, g_methNewTbl, flags, g_methNew),
shTclGetUsage(interp, g_methNewTbl, flags, g_methNew));
shTclDeclare(interp, g_methDel, (Tcl_CmdProc *) tcl_methDel,
(ClientData) 0, (Tcl_CmdDeleteProc *)0, tclFacil,
shTclGetArgInfo(interp, g_methDelTbl, flags, g_methDel),
shTclGetUsage(interp, g_methDelTbl, flags, g_methDel));
shTclDeclare(interp, g_dnls1, (Tcl_CmdProc *) tcl_dnls1,
(ClientData) 0, (Tcl_CmdDeleteProc *)0, tclFacil,
shTclGetArgInfo(interp, g_dnls1Tbl, flags, g_dnls1),
shTclGetUsage(interp, g_dnls1Tbl, flags, g_dnls1));
shTclDeclare(interp, g_dnls1e, (Tcl_CmdProc *) tcl_dnls1e,
(ClientData) 0, (Tcl_CmdDeleteProc *)0, tclFacil,
shTclGetArgInfo(interp, g_dnls1eTbl, flags, g_dnls1e),
shTclGetUsage(interp, g_dnls1eTbl, flags, g_dnls1e));
}
