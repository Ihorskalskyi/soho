SHELL = /bin/sh
#
#
#  Makefile for the src directory of sdssmath.
#
#  1) This file is designed to be invoked by a shell script called sdssmake
#  which selects supported options.
#
#  2) Environment:
# Environmental variables (none)
#
# Expected variables on the command line:
# CC=        The name of the appropriate compiler, with suitable flags
# CCCHK=     A set of compiler flags which check ANSI C strictly
# LLIBS=     A set of libraries for linking against.
#
.c.o :
	$(CC) -c $(CCCHK) $(CFLAGS) $*.c
#
SRC	= ../src
INC	= ../include
LIB	= ../lib
DOC	= ../doc
UPS	= ../ups
BIN     = ../bin
INCDIRS	= -I$(INC)  -I$(FTCL_DIR)/include -I$(FTCL_DIR)/src \
			-I$(LIBFITS_DIR)/src -I$(FPGPLOT_DIR)/include \
			-I$(PGPLOT_DIR)/dst -I$(DERVISH_DIR)/include \
			-I$(DERVISH_DIR)/contrib \
			-I$(TCLX_DIR)/include \
			-I$(TCLX_DIR)/src/src -I$(TCL_DIR)/include \
			-I$(TCL_DIR)/src \
			-I$(FFTW_DIR)/src


# NOTE:	CFLAGS_DEFGP is provided because some IRIX systems do not handle -G 0
#	well when loading (ld) images.no  The 'defgp' stands for default global
#	pointer processing (that is, no -G n option).

CFLAGS		= $(SDSS_CFLAGS) $(INCDIRS)
CFLAGS_DEFGP	= $(SDSS_CFLAGS_DEFGP) $(INCDIRS)

#
#
TCLOBJECTS = sdssmathVerbsDeclare.o

OBJECTS = $(TCLOBJECTS) \
	diskio_gen.o smaFunc.o smaFft.o slatec.o tcl_slatec.o tcl_func.o

LDFLAGS = -L$(LIB) -lsdssmath  \
	-L$(SLATEC_DIR)/lib -lslatec \
	-L$(FFTW_DIR)/src -lfftw \
	  $(LIBSHV) $(LIBFITS) $(PGPLOTLIB) \
	  $(CONTRIBLIB) $(LIBFTCL) \
	  $(LLIBS) $(PGLIB) $(FLIBS) $(ELIBS)

DISKIO_FILES = ../include/smaFunc.h

#=============================================================================
# TARGETS
#=============================================================================

diskio_gen.c: $(DISKIO_FILES)
	@echo Make diskio_gen.c
	$(DERVISH_DIR)/bin/make_io -v1 -m Sdssmath -p sm \
	diskio_gen.c $(DISKIO_FILES)

default:
	@echo Please invoke this makefile using sdssmake. >&2
	@exit 1

all : $(BIN)/sdssmath

$(BIN)/sdssmath: sdssmath_main.c $(LIB)/libsdssmath.a
	$(CC) -o $@ $(CFLAGS_DEFGP) $(CCCHK) $(SRC)/sdssmath_main.c \
		$(LDFLAGS)



library :	$(LIB)/libsdssmath.a

#
# Compile SDSSMATH library source
#
$(LIB)/libsdssmath.a : $(OBJECTS)
	$(AR) $(ARFLAGS) $(LIB)/libsdssmath.a $?
	- if [ "$(RANLIB)" != "" -a -x "$(RANLIB)" ]; then \
		$(RANLIB) $(LIB)/libsdssmath.a; \
	fi

#
# Now standard targets
#
clean :
	rm -f core *~ *.bak *.jou MAIN.0 MAIN.0.* *.orig *.old .'#'* '#'*'#'
	rm -f *.o diskio_gen.c

install :
	@if [ "$(SDSSMATH_INSTALL_DIR)" = "" ]; then \
		echo You have not specified a destination directory >&2; \
		exit 1; \
	fi
	@ rm -rf $(SDSSMATH_INSTALL_DIR)/src
	@ mkdir $(SDSSMATH_INSTALL_DIR)/src
	cp -p *.c Makefile $(SDSSMATH_INSTALL_DIR)/src
	@chmod 644 $(SDSSMATH_INSTALL_DIR)/src/*.c
	@( \
		cd $(SDSSMATH_INSTALL_DIR)/src ;\
		find . \( -type d -name CVS -prune -o \
			-name Makefile -name \*~ \) -exec rm -rf {} \; \
	)
tags :
	/usr/local/bin/etags -aet -f $(ETC)/TAGS *.[ch]
#
make :
	@if [ "$(CCENV)" = "" ]; then ccenv=`uname`; else ccenv=$(CCENV); fi; \
	echo \
	"make_make -cc '$(CC) $(CFLAGS)' -nostd -file Makefile" \
							"-env $$ccenv *.c"; \
	 make_make -cc '$(CC) $(CFLAGS)' -nostd -file Makefile \
							 -env $$ccenv *.c
#
# include file dependencies.
# All lines below START_DEPEND are machine generated; Do Not Edit
# 
#START_DEPEND
leamax_bind.o:	leamax_bind.c
leamax_bind.o:	$(DERVISH_DIR)/include/dervish.h
leamax_bind.o:	$(TCLX_DIR)/include/tclExtend.h
leamax_bind.o:	$(TCL_DIR)/include/tcl.h
leamax_bind.o:	$(FTCL_DIR)/include/ftcl.h
leamax_bind.o:	$(FTCL_DIR)/include/ftclParseArgv.h
leamax_bind.o:	$(DERVISH_DIR)/include/dervish_msg_c.h
leamax_bind.o:	$(DERVISH_DIR)/include/region.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCAlign.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCSchema.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCHdr.h
leamax_bind.o:	$(DERVISH_DIR)/include/shC.h
leamax_bind.o:	$(DERVISH_DIR)/include/shChain.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCArray.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCSchema.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCVecExpr.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCUtils.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCSchema.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCErrStack.h
leamax_bind.o:	$(DERVISH_DIR)/include/shTclErrStack.h
leamax_bind.o:	$(FTCL_DIR)/include/ftcl.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCSchemaCnv.h
leamax_bind.o:	$(DERVISH_DIR)/include/shTclHandle.h
leamax_bind.o:	$(FTCL_DIR)/include/ftcl.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCSchema.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCList.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCSchema.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCSchema.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCTbl.h
leamax_bind.o:	$(LIBFITS_DIR)/src/libfits.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCSchema.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCFitsIo.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCSchemaTrans.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCSchema.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCSchemaSupport.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCSchema.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCFits.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCMaskUtils.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCRegUtils.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCRegPrint.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCHash.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCSchema.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCAssert.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCEnvscan.h
leamax_bind.o:	$(DERVISH_DIR)/include/shTclRegSupport.h
leamax_bind.o:	$(FTCL_DIR)/include/ftcl.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCSao.h
leamax_bind.o:	$(DERVISH_DIR)/include/imtool.h
leamax_bind.o:	$(DERVISH_DIR)/include/shTclParseArgv.h
leamax_bind.o:	$(FTCL_DIR)/include/ftcl.h
leamax_bind.o:	$(DERVISH_DIR)/include/shTclVerbs.h
leamax_bind.o:	$(DERVISH_DIR)/include/shTclSao.h
leamax_bind.o:	$(FTCL_DIR)/include/ftclCmdLine.h
leamax_bind.o:	$(DERVISH_DIR)/include/shTclUtils.h
leamax_bind.o:	$(FTCL_DIR)/include/ftcl.h
leamax_bind.o:	$(DERVISH_DIR)/include/shTk.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCGarbage.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCHg.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCLink.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCList.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCSchema.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCVecExpr.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCLut.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCVecExpr.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCVecChain.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCVecExpr.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCSchema.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCDemo.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCDiskio.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCSchema.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCList.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCHist.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCList.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCMask.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCMemory.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCRegPhysical.h
leamax_bind.o:	$(DERVISH_DIR)/include/shTclAlign.h
leamax_bind.o:	$(FTCL_DIR)/include/ftcl.h
leamax_bind.o:	$(DERVISH_DIR)/include/shCSchema.h
leamax_bind.o:	$(DERVISH_DIR)/include/shTclArray.h
leamax_bind.o:	$(FTCL_DIR)/include/ftcl.h
leamax_bind.o:	$(DERVISH_DIR)/include/shTclFits.h
leamax_bind.o:	$(FTCL_DIR)/include/ftcl.h
leamax_bind.o:	$(DERVISH_DIR)/include/shTclHdr.h
leamax_bind.o:	$(DERVISH_DIR)/include/shTclSupport.h
leamax_bind.o:	$(DERVISH_DIR)/include/shTclTree.h
sdssmathVerbsDeclare.o:	sdssmathVerbsDeclare.c
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/dervish.h
sdssmathVerbsDeclare.o:	$(TCLX_DIR)/include/tclExtend.h
sdssmathVerbsDeclare.o:	$(TCL_DIR)/include/tcl.h
sdssmathVerbsDeclare.o:	$(FTCL_DIR)/include/ftcl.h
sdssmathVerbsDeclare.o:	$(FTCL_DIR)/include/ftclParseArgv.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/dervish_msg_c.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/region.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCAlign.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCHdr.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shC.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shChain.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCArray.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCVecExpr.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCUtils.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCErrStack.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shTclErrStack.h
sdssmathVerbsDeclare.o:	$(FTCL_DIR)/include/ftcl.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCSchemaCnv.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shTclHandle.h
sdssmathVerbsDeclare.o:	$(FTCL_DIR)/include/ftcl.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCList.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCTbl.h
sdssmathVerbsDeclare.o:	$(LIBFITS_DIR)/src/libfits.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCFitsIo.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCSchemaTrans.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCSchemaSupport.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCFits.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCMaskUtils.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCRegUtils.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCRegPrint.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCHash.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCAssert.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCEnvscan.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shTclRegSupport.h
sdssmathVerbsDeclare.o:	$(FTCL_DIR)/include/ftcl.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCSao.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/imtool.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shTclParseArgv.h
sdssmathVerbsDeclare.o:	$(FTCL_DIR)/include/ftcl.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shTclVerbs.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shTclSao.h
sdssmathVerbsDeclare.o:	$(FTCL_DIR)/include/ftclCmdLine.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shTclUtils.h
sdssmathVerbsDeclare.o:	$(FTCL_DIR)/include/ftcl.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shTk.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCGarbage.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCHg.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCLink.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCList.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCVecExpr.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCLut.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCVecExpr.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCVecChain.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCVecExpr.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCDemo.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCDiskio.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCList.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCHist.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCList.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCMask.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCMemory.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCRegPhysical.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shTclAlign.h
sdssmathVerbsDeclare.o:	$(FTCL_DIR)/include/ftcl.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shTclArray.h
sdssmathVerbsDeclare.o:	$(FTCL_DIR)/include/ftcl.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shTclFits.h
sdssmathVerbsDeclare.o:	$(FTCL_DIR)/include/ftcl.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shTclHdr.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shTclSupport.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/shTclTree.h
sdssmathVerbsDeclare.o:	../include/sdssmath.h
sdssmathVerbsDeclare.o:	../include/tclMath.h
sdssmathVerbsDeclare.o:	../include/smaFunc.h
sdssmathVerbsDeclare.o:	$(DERVISH_DIR)/include/dervish.h
sdssmath_main.o:	sdssmath_main.c
sdssmath_main.o:	$(DERVISH_DIR)/include/dervish.h
sdssmath_main.o:	$(TCLX_DIR)/include/tclExtend.h
sdssmath_main.o:	$(TCL_DIR)/include/tcl.h
sdssmath_main.o:	$(FTCL_DIR)/include/ftcl.h
sdssmath_main.o:	$(FTCL_DIR)/include/ftclParseArgv.h
sdssmath_main.o:	$(DERVISH_DIR)/include/dervish_msg_c.h
sdssmath_main.o:	$(DERVISH_DIR)/include/region.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCAlign.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCHdr.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shC.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shChain.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCArray.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCVecExpr.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCUtils.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCErrStack.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shTclErrStack.h
sdssmath_main.o:	$(FTCL_DIR)/include/ftcl.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCSchemaCnv.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shTclHandle.h
sdssmath_main.o:	$(FTCL_DIR)/include/ftcl.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCList.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCTbl.h
sdssmath_main.o:	$(LIBFITS_DIR)/src/libfits.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCFitsIo.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCSchemaTrans.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCSchemaSupport.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCFits.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCMaskUtils.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCRegUtils.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCRegPrint.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCHash.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCAssert.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCEnvscan.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shTclRegSupport.h
sdssmath_main.o:	$(FTCL_DIR)/include/ftcl.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCSao.h
sdssmath_main.o:	$(DERVISH_DIR)/include/imtool.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shTclParseArgv.h
sdssmath_main.o:	$(FTCL_DIR)/include/ftcl.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shTclVerbs.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shTclSao.h
sdssmath_main.o:	$(FTCL_DIR)/include/ftclCmdLine.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shTclUtils.h
sdssmath_main.o:	$(FTCL_DIR)/include/ftcl.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shTk.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCGarbage.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCHg.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCLink.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCList.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCVecExpr.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCLut.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCVecExpr.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCVecChain.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCVecExpr.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCDemo.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCDiskio.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCList.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCHist.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCList.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCMask.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCMemory.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCRegPhysical.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shTclAlign.h
sdssmath_main.o:	$(FTCL_DIR)/include/ftcl.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shCSchema.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shTclArray.h
sdssmath_main.o:	$(FTCL_DIR)/include/ftcl.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shTclFits.h
sdssmath_main.o:	$(FTCL_DIR)/include/ftcl.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shTclHdr.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shTclSupport.h
sdssmath_main.o:	$(DERVISH_DIR)/include/shTclTree.h
sdssmath_main.o:	../include/tclMath.h
smaFunc.o:	smaFunc.c
smaFunc.o:	../include/sdssmath.h
smaFunc.o:	../include/tclMath.h
smaFunc.o:	$(DERVISH_DIR)/include/dervish.h
smaFunc.o:	$(TCLX_DIR)/include/tclExtend.h
smaFunc.o:	$(TCL_DIR)/include/tcl.h
smaFunc.o:	$(FTCL_DIR)/include/ftcl.h
smaFunc.o:	$(FTCL_DIR)/include/ftclParseArgv.h
smaFunc.o:	$(DERVISH_DIR)/include/dervish_msg_c.h
smaFunc.o:	$(DERVISH_DIR)/include/region.h
smaFunc.o:	$(DERVISH_DIR)/include/shCAlign.h
smaFunc.o:	$(DERVISH_DIR)/include/shCSchema.h
smaFunc.o:	$(DERVISH_DIR)/include/shCHdr.h
smaFunc.o:	$(DERVISH_DIR)/include/shC.h
smaFunc.o:	$(DERVISH_DIR)/include/shChain.h
smaFunc.o:	$(DERVISH_DIR)/include/shCArray.h
smaFunc.o:	$(DERVISH_DIR)/include/shCSchema.h
smaFunc.o:	$(DERVISH_DIR)/include/shCVecExpr.h
smaFunc.o:	$(DERVISH_DIR)/include/shCUtils.h
smaFunc.o:	$(DERVISH_DIR)/include/shCSchema.h
smaFunc.o:	$(DERVISH_DIR)/include/shCErrStack.h
smaFunc.o:	$(DERVISH_DIR)/include/shTclErrStack.h
smaFunc.o:	$(FTCL_DIR)/include/ftcl.h
smaFunc.o:	$(DERVISH_DIR)/include/shCSchemaCnv.h
smaFunc.o:	$(DERVISH_DIR)/include/shTclHandle.h
smaFunc.o:	$(FTCL_DIR)/include/ftcl.h
smaFunc.o:	$(DERVISH_DIR)/include/shCSchema.h
smaFunc.o:	$(DERVISH_DIR)/include/shCList.h
smaFunc.o:	$(DERVISH_DIR)/include/shCSchema.h
smaFunc.o:	$(DERVISH_DIR)/include/shCSchema.h
smaFunc.o:	$(DERVISH_DIR)/include/shCTbl.h
smaFunc.o:	$(LIBFITS_DIR)/src/libfits.h
smaFunc.o:	$(DERVISH_DIR)/include/shCSchema.h
smaFunc.o:	$(DERVISH_DIR)/include/shCFitsIo.h
smaFunc.o:	$(DERVISH_DIR)/include/shCSchemaTrans.h
smaFunc.o:	$(DERVISH_DIR)/include/shCSchema.h
smaFunc.o:	$(DERVISH_DIR)/include/shCSchemaSupport.h
smaFunc.o:	$(DERVISH_DIR)/include/shCSchema.h
smaFunc.o:	$(DERVISH_DIR)/include/shCFits.h
smaFunc.o:	$(DERVISH_DIR)/include/shCMaskUtils.h
smaFunc.o:	$(DERVISH_DIR)/include/shCRegUtils.h
smaFunc.o:	$(DERVISH_DIR)/include/shCRegPrint.h
smaFunc.o:	$(DERVISH_DIR)/include/shCHash.h
smaFunc.o:	$(DERVISH_DIR)/include/shCSchema.h
smaFunc.o:	$(DERVISH_DIR)/include/shCAssert.h
smaFunc.o:	$(DERVISH_DIR)/include/shCEnvscan.h
smaFunc.o:	$(DERVISH_DIR)/include/shTclRegSupport.h
smaFunc.o:	$(FTCL_DIR)/include/ftcl.h
smaFunc.o:	$(DERVISH_DIR)/include/shCSao.h
smaFunc.o:	$(DERVISH_DIR)/include/imtool.h
smaFunc.o:	$(DERVISH_DIR)/include/shTclParseArgv.h
smaFunc.o:	$(FTCL_DIR)/include/ftcl.h
smaFunc.o:	$(DERVISH_DIR)/include/shTclVerbs.h
smaFunc.o:	$(DERVISH_DIR)/include/shTclSao.h
smaFunc.o:	$(FTCL_DIR)/include/ftclCmdLine.h
smaFunc.o:	$(DERVISH_DIR)/include/shTclUtils.h
smaFunc.o:	$(FTCL_DIR)/include/ftcl.h
smaFunc.o:	$(DERVISH_DIR)/include/shTk.h
smaFunc.o:	$(DERVISH_DIR)/include/shCGarbage.h
smaFunc.o:	$(DERVISH_DIR)/include/shCHg.h
smaFunc.o:	$(DERVISH_DIR)/include/shCLink.h
smaFunc.o:	$(DERVISH_DIR)/include/shCList.h
smaFunc.o:	$(DERVISH_DIR)/include/shCSchema.h
smaFunc.o:	$(DERVISH_DIR)/include/shCVecExpr.h
smaFunc.o:	$(DERVISH_DIR)/include/shCLut.h
smaFunc.o:	$(DERVISH_DIR)/include/shCVecExpr.h
smaFunc.o:	$(DERVISH_DIR)/include/shCVecChain.h
smaFunc.o:	$(DERVISH_DIR)/include/shCVecExpr.h
smaFunc.o:	$(DERVISH_DIR)/include/shCSchema.h
smaFunc.o:	$(DERVISH_DIR)/include/shCDemo.h
smaFunc.o:	$(DERVISH_DIR)/include/shCDiskio.h
smaFunc.o:	$(DERVISH_DIR)/include/shCSchema.h
smaFunc.o:	$(DERVISH_DIR)/include/shCList.h
smaFunc.o:	$(DERVISH_DIR)/include/shCHist.h
smaFunc.o:	$(DERVISH_DIR)/include/shCList.h
smaFunc.o:	$(DERVISH_DIR)/include/shCMask.h
smaFunc.o:	$(DERVISH_DIR)/include/shCMemory.h
smaFunc.o:	$(DERVISH_DIR)/include/shCRegPhysical.h
smaFunc.o:	$(DERVISH_DIR)/include/shTclAlign.h
smaFunc.o:	$(FTCL_DIR)/include/ftcl.h
smaFunc.o:	$(DERVISH_DIR)/include/shCSchema.h
smaFunc.o:	$(DERVISH_DIR)/include/shTclArray.h
smaFunc.o:	$(FTCL_DIR)/include/ftcl.h
smaFunc.o:	$(DERVISH_DIR)/include/shTclFits.h
smaFunc.o:	$(FTCL_DIR)/include/ftcl.h
smaFunc.o:	$(DERVISH_DIR)/include/shTclHdr.h
smaFunc.o:	$(DERVISH_DIR)/include/shTclSupport.h
smaFunc.o:	$(DERVISH_DIR)/include/shTclTree.h
smaFunc.o:	../include/smaFunc.h
smaFunc.o:	$(DERVISH_DIR)/include/dervish.h
temp.o:	temp.c
