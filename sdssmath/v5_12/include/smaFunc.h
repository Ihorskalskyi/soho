#ifndef SMAFUNC_H
#define SMAFUNC_H

#include "dervish.h"

#ifdef __cplusplus
extern "C"
{
#endif

#define FUNC_LABEL_SIZE 25

typedef RET_CODE (*SMA_FUNC)(int, VECTOR **, double *, double , double *); /* pragma FUNCTION */
typedef     void (*SMA_METH)(int *iflag, int *m, int *n, double *param, double *fvec, double *fjac, int *ldfjac); /* pragma FUNCTION */


typedef struct Func {
  char kind[FUNC_LABEL_SIZE];
  int nVector;
  VECTOR **va;
  VECTOR *cov;
  VECTOR *sigma;
  double chi2;
  SMA_FUNC fa;
} FUNC;

typedef struct Meth {
  char kind[FUNC_LABEL_SIZE];
  SMA_METH ma;
} METH;


FUNC *smFuncNew(char *name, VECTOR **va, int nHandle);
RET_CODE smFuncDel(FUNC *func);

METH *smMethNew(char *name);
RET_CODE smMethDel(METH *meth);

RET_CODE smaFuncEval(
		     FUNC *func, 
		     double x, 
		     double *y
		     );

void smaGlobalSet(VECTOR *x, VECTOR *y, VECTOR *w, FUNC *func, VECTOR *mask);

void smaMethChi2 (
		  int *iflag, 
		  int *m,	 /* number of data points */
		  int *n,	 /* number of parameters */
		  double *param, /* input array of length n */
		  double *fvec,	 /* output array of length m */
		  double *fjac, 
		  int *ldfjac);


SMA_FUNC smaFuncAddrGetFromName(char *name, VECTOR **va, int nVector);

SMA_METH smaMethAddrGetFromName(char *name);
RET_CODE smaCovFromFit(METH *meth, FUNC *func, double epsfcn);

int smaFuncPlot(
		PGSTATE *pg,	/* where to plot and how to plot it */
		FUNC *func,	/* what to plot */
		FUNC *resFunc,  /* what to subtract at each point */
		double xmin,	/* beginning x */
		double xmax,	/* ending x */
		int npt		/* number of points to plot */
		);


#ifdef __cplusplus
}
#endif
#endif
