#ifndef SMAFFT_H
#define SMAFFT_H

#include "dervish.h"
#include "fftw.h"
#ifdef __cplusplus
extern "C"
{
#endif

RET_CODE smRegFft(
		  REGION *rReg,	/* the real part of the region to transform, 
				   of type FL32 */
		  REGION *iReg, /* the imaginary part of the region to
				   transform, of type FL32 */
		  int direction	/* +1 for forward; 0 for backwards */
		  );

RET_CODE smRegElementMult(
			  REGION *rReg1,	/* real region 1 */
			  REGION *iReg1,	/* imag region 1 */
			  REGION *rReg2,	/* real region 2 */
			  REGION *iReg2	        /* imag region 2 */
			  );

RET_CODE smRegWrapAround(
                          REGION *reg,   /* the region to wrap*/
			  float norm /* nrow*ncol/flux in psf */
                          );

/*============================================================================

The following copied from:

       fourier.h  -  Don Cross <dcross@intersrv.com>

       http://www.intersrv.com/~dcross/fft.html

       Contains definitions for doing Fourier transforms
       and inverse Fourier transforms.

============================================================================*/
/*
**   fft() computes the Fourier transform or inverse transform
**   of the complex inputs to produce the complex outputs.
**   The number of samples must be a power of two to do the
**   recursive decomposition of the FFT algorithm.
**   See Chapter 12 of "Numerical Recipes in FORTRAN" by
**   Press, Teukolsky, Vetterling, and Flannery,
**   Cambridge University Press.
**
**   Notes:  If you pass ImaginaryIn = NULL, this function will "pretend"
**           that it is an array of all zeroes.  This is convenient for
**           transforming digital samples of real number data without
**           wasting memory.
*/

void smFftDouble ( unsigned NumSamples,         /* must be a power of 2 */
		  int    InverseTransform,     
		  /* 0=forward FFT, 1=inverse FFT */
		  double *RealIn,              
		  /* array of input's real samples */
		  double *ImaginaryIn,         
		  /* array of input's imag samples */
		  double *RealOut,             
		  /* array of output's reals */
		  double *ImaginaryOut 
		  /* array of output's imaginaries */
		  );      


RET_CODE smFftFloat( 
		   unsigned NumSamples,         /* must be a power of 2 */
		   int    InverseTransform,     
		   /* 0=forward FFT, 1=inverse FFT */
		   float *RealIn,               
		   /* array of input's real samples */
		   float *ImaginaryIn,
		   /* array of input's imag samples */
		   float *RealOut,              
		   /* array of output's reals */
		   float *ImaginaryOut 
		   /* array of output's imaginaries */
		   );       


int IsPowerOfTwo ( unsigned x );
unsigned NumberOfBitsNeeded ( unsigned PowerOfTwo );
unsigned ReverseBits ( unsigned index, unsigned NumBits );

/*
**   The following function returns an "abstract frequency" of a
**   given index into a buffer with a given number of frequency samples.
**   Multiply return value by sampling rate to get frequency expressed in Hz.
*/
double Index_to_frequency ( unsigned NumSamples, unsigned Index );


RET_CODE smRegFftw(
		   REGION *rReg, /* the real part of the region to transform, 
				   of type FL32 */
		   REGION *iReg, /* the imaginary part of the region to
				   transform, of type FL32 */
		   int direction	/* +1 for forward; 0 for backwards */
		  );


#ifdef __cplusplus
}
#endif
#endif













