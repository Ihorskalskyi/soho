#ifndef SM_CONVERSIONS_H
#define SM_CONVERSIONS_H

#ifdef __cplusplus
extern "C" {
#endif

#include <values.h> /* This is where we get the value M_PI */

  /*To convert from a number, multiply by one of the following constants.
    For example:   3.14159 * rad2deg yields 180.0
    
    rad - radians
    
    hrs - hours
    tmin - time minutes
    tsec - time seconds
    
    deg - degrees
    amin - arc minutes
    asec - arc seconds
    
    */
  
  const double hrs2Rad = M_PI/12.0; /* Hours <--> Radians */
  const double rad2Hrs = 12.0/M_PI; 
  
  const double tmin2Rad = M_PI/(12.0 * 60.0); /* Time minutes <--> Radians */
  const double rad2Tmin = (12.0 * 60.0)/M_PI;
  
  const double tsec2Rad = M_PI/(12.0 * 3600.0);	/* Time seconds <--> Radians */
  const double rad2Tsec = (12.0 * 3600.0)/M_PI;
  
  const double deg2Rad = M_PI/180.0; /* Degrees <--> Radians */
  const double rad2Deg = 180.0/M_PI;
    
  const double amin2Rad = M_PI/(180.0 * 60.0); /* Arc Minutes <--> Radians */
  const double rad2Amin = (180.0 * 60.0)/M_PI;
	
  const double asec2Rad = M_PI/(180.0 * 3600.0); /* Arc Seconds <--> Radians */
  const double rad2Asec = (180.0 * 3600.0)/M_PI;
	    
#ifdef __cplusplus
}
#endif

#endif
