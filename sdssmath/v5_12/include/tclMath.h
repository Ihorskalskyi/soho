#ifndef TCL_MATH_H
#define TCL_MATH_H

#include "dervish.h"

#ifdef __cplusplus
extern "C"
{
#endif

void smaVerbsDeclare(Tcl_Interp *interp);
void smaTclMinuitDeclare(Tcl_Interp *interp);
void smaTclSlalibDeclare(Tcl_Interp *interp);
void smaTclMiscDeclare(Tcl_Interp *interp);
void smaTclHgMathDeclare(Tcl_Interp *interp);

void tcl_slatec_declare(Tcl_Interp *interp);
void tcl_func_declare(Tcl_Interp *interp);

#ifdef __cplusplus
}
#endif

#endif
