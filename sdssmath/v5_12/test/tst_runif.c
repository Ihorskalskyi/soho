/* Simple test of C binding smaRUNIF */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "sdssmath.h"
#include "slatec.h"

int main(void) {
#define NPLUS1 11
#define debug 0
  int n = NPLUS1 - 1;
  float t[NPLUS1];
  int i;
  float x;
  for (i=0; i<NPLUS1; i++) {
    x=smaRAND(0.0);
    t[i] = x;
#if debug
    printf("i=%2d x=%f \n", i, x);
#endif
  }

#if debug
  printf("call smaRUNIF to initialize\n");
#endif

  x = smaRUNIF(t, -n); 

#if debug
  for(i=0; i<NPLUS1; i++) { printf("t[%2d]=%f\n",i, t[i]); }
  printf("now x=%f\n",x);
  printf("call smaRUNIF again\n");
#endif

  x = smaRUNIF(t, n); 

#if debug
  for(i=0; i<NPLUS1; i++) { printf("t[%2d]=%f\n",i, t[i]); }
#endif

  if (fabs(x-0.552779) > 0.001) {
    printf("error in tst_runif\n");
    return 1;
  }

  /* Test smaRGAUSS.  As long as it doesn't crash, test is passed! */
  x = smaRGAUSS(0.0, 1.0);

  return 0;
}







