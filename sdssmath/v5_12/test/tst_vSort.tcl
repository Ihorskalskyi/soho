proc tst_vSort { } {
    set v [vFromL "10 20 30 16 8 222 9 101 -3 2 0.003 5 20"]
    set p [vSort $v +1]
    if {[exprGet $v.vec<[exprGet $p.vec<5>]>] != 9} {
	error "trouble in tst_vSort increasing"
    }
    set p [vSort $v -1]
    if {[exprGet $v.vec<[exprGet $p.vec<5>]>] != 16} {
	error "trouble in tst_vSort decreasing"
    }
    return 0
}

return [tst_vSort]

    
