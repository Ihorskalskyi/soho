proc tst_dbs { {doPlot 0} } {
    global dbs pg
    set ndata 20
    set pi 3.14159

    # set up the x and y vectors to fit
    set dbs(x) [vectorExprNew $ndata]
    set dbs(y) [vectorExprNew $ndata]

    loop i 0 $ndata {
	set x [expr 2*$pi*($i+0.5)/$ndata]
	set y [cos $x]
	handleSet $dbs(x).vec<$i> $x
	handleSet $dbs(y).vec<$i> $y
    }

    if {$doPlot} {
	echo Do the plot
	pgstateSet $pg -symb 4
	vPlot $pg $dbs(x) $dbs(y)
    }

    # define the work arrays the fitter will need
    set dbs(t) [vectorExprNew [expr $ndata+6]]
    set dbs(w) [vectorExprNew [expr 5*($ndata+2)]]

    # do the fit
    set dbs(r1) [dbint4 $dbs(x) $dbs(y) $dbs(t) $dbs(w)]

    if {$doPlot} {
	set lx ""
	set ly ""
	set xmin [expr [vExtreme $dbs(t) min]+0.001]
	set xmax [expr [vExtreme $dbs(t) max]-0.001]
	echo xmin=$xmin xmax=$xmax
	for {set x $xmin} {$x < $xmax} {set x [expr $x+0.01]} {
	    lappend lx $x
	    set ytrue [cos $x]
	    set yfit [funcEval $dbs(r1) $x]
	    lappend ly [expr $ytrue-$yfit]
	}
	set vx [vFromL $lx x]
	set vy [vFromL $ly "ytrue-yfit"]
	pgstateSet $pg -isLine 1 -icLine 2 -plotTitle "residual from dbint4"
	vPlot $pg $vx $vy
	loop i 0 [exprGet $dbs(t).dimen] {
	    set x [exprGet $dbs(t).vec<$i>]
	    pgPoint $x 0 4
	}
    }

    # evaluate the fit and residual
    for {set x 1} {$x < 6} {set x [expr $x+0.1]} {
	set yfit [funcEval $dbs(r1) $x]
	if {$doPlot} {
	    pgSci 2
	    pgPoint $x $yfit 1
	    pgSci 1
	}
    }

    set correct 0.955334
    set delta [expr $correct-$yfit]
    if {[abs $delta] > 0.0001} {
	error "expected dbvalu $correct  but got $yfit"
    }
    return 0
}


return [tst_dbs]
