/*** 
  Simple test of C binding for the sorting function smaDSORT 
***/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "sdssmath.h"
#include "slatec.h"

#define debug 0

int main(void) {

int ct, n, kflag, retcode;
double dx[10], dy[10];

n = 10;        /* number of items in array dx to sort */
kflag = 2;     /* sort dx in increasing order, carrying along dy */
retcode = 0;

/* put some numbers in the dx and dy arrays */
dx[0]  = 23.3;
dx[1]  = 41.4;
dx[2]  = 55.5;
dx[3]  = 22.6;
dx[4]  = 73.7;
dx[5]  = 57.8;
dx[6]  = 44.4;
dx[7]  = 33.4;
dx[8]  = 25.4;
dx[9]  = 94.3;

dy[0]  = 5.32;
dy[1]  = 4.41;
dy[2]  = 3.57;
dy[3]  = 2.67;
dy[4]  = 5.73;
dy[5]  = 6.84;
dy[6]  = 4.49;
dy[7]  = 0.42;
dy[8]  = 2.49;
dy[9]  = 1.32;

#if debug
printf("tst_sort: before sort\n");
printf("    dx     dy\n");
for (ct = 0; ct < n; ct++ ) {
  printf("%d  : %f  %f\n", ct, dx[ct], dy[ct]);
}
#endif

/* actually make the call and sort on dx */
smaDSORT( dx, dy, n, kflag );

#if debug
printf("tst_sort: after sort\n");
for ( ct = 0; ct < n; ct++ ) {
  printf("%d  : %f  %f\n", ct, dx[ct], dy[ct]);
}
#endif

for ( ct = 1; ct < n; ct++ ) {
  if ( dx[ct] < dx[ct-1] ) {
    retcode++;
  }
}

return retcode;

}
