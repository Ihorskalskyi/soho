proc tst_funcPlot { {dev /vcps} } {
    global pg dn

    set memBefore [memBytesInUse]
    echo -------------------------- begin tst_funcPlot --- memBytesInUse = $memBefore

    pgInit $dev
    pgstateSet $pg -nywindow 2
    set vTemp [vFromL "1.23 2.6 10000"]
    set dn(func) [funcNew gauss $vTemp]
    set lx ""; set ly ""; set lw ""; set ldiag ""
    loop i -10 10 {
	set y [funcEval $dn(func) $i]
	lappend lx $i
	lappend ly [expr $y+[randGet]-0.5]
	lappend lw 1
	lappend ldiag 1
    }
    set dn(x) [vFromL $lx x]
    set dn(y) [vFromL $ly y]
    set dn(w) [vFromL $lw w]
    set dn(diag) [vFromL $ldiag diag]

    pgstateSet $pg -symb 4
    vPlot $pg $dn(x) $dn(y)

    set dn(meth) [methNew chi2]
    set vTemp [vFromL "0.0 1.0 5000"]
    set dn(func2) [funcNew gauss $vTemp]


    pgSci 3
    for {set x -10} {$x < 10} {set x [expr $x+0.1]} {
	set y [funcEval $dn(func2) $x]
	pgPoint $x $y 1
    }

    set tol [sqrt [d1mach 4]]
    set dn(return) [dnls1 $dn(meth) $dn(func2) $dn(x) $dn(y) $dn(w) $dn(diag)]

    pgSci 2
    for {set x -10} {$x < 10} {set x [expr $x+0.1]} {
	set y [funcEval $dn(func2) $x]
	pgPoint $x $y 1
    }

    pgstateSet $pg -symb 3 -icMark 7
    vPlot $pg $dn(x) $dn(y)
    pgstateSet $pg -isNewplot 0 -isLine 1 -icLine 5 
    funcPlot $pg $dn(func2) -1 5 100
    
    dnls1Test 0 1.2302667347
    dnls1Test 1 2.60018172172
    dnls1Test 2 10000.4487923

    methDel $dn(meth)
    funcDel $dn(func)
    funcDel $dn(func2)
    
    vectorExprDel $dn(x)
    vectorExprDel $dn(y)
    vectorExprDel $dn(w)
    vectorExprDel $dn(diag)

    pgstateClose $pg
    pgstateDel $pg

    set memAfter [memBytesInUse]
    echo -------------------------- ending tst_funcPlot -- memBytesInUse = $memAfter
    if {$memBefore != $memAfter} {
	error "Memory leak in tst_funcPlot.tcl -- B4=$memBefore AFTER=$memAfter"
    }

    return 0
}

proc dnls1Test {i val} {
    global dn
    set value [exprGet $dn(func2).va<0>->vec<$i>]
    if  {[abs $value-$val]/$val > 0.001} {
	error "error in tst_dnls1:  i=$i val=$val value=$value"
    }
    return
}

tst_funcPlot /vcps
