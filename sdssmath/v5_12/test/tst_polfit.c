/* Simple test of C binding smaPOLFIT */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "sdssmath.h"
#include "slatec.h"

int main(void) {
#define debug 0
#define NDATA 11
#define MAXDEG 4
#define MYPI 3.1415926535897932384
  float x[NDATA], y[NDATA], w[NDATA];
  float eps, r[NDATA], a[3*NDATA+3*MAXDEG+3];
  int ndeg, ierr;

  float xx, yy, yfit, yp[1];

  int i;
  float delta;
  /* The tolerance has to be ten times bigger than for splines! */
  float tol=10000.0*smaR1MACH(4);

  for (i=0; i<NDATA; i++) {
    x[i] = (float) (i)/(NDATA-1);
    y[i] = sin(MYPI*x[i]);
    w[i] = 1.0;
  }

  eps = 0.0;
  smaPOLFIT(NDATA, x, y, w, MAXDEG, &ndeg, &eps, r, &ierr, a);

  for (xx=x[0]; xx<x[NDATA-1]; xx += 1.0/(10*NDATA)) {
    yy = sin(MYPI*xx);
    smaPVALUE(MAXDEG, 0, xx, &yfit, yp, a);
    delta = fabs(yfit-yy);
    /*
    printf("xx=%f  yy=%f  yfit=%f  delta=%f\n", xx, yy, yfit, delta);
    */
    if (delta > tol) {
      printf("error in tst_polfit: xx=%f yy=%f yfit=%f delta=%f tol=%f\n", 
	     xx, yy, yfit, delta, tol);
      return 1;
    }
  }

  return 0;
}







