proc tst_dpo { {doPlot 0} } {
    global dpo pg
    set ndata 20
    set pi 3.14159

    # first a _really_ easy test.
    set x [vFromL "0 1"]         
    set y [vFromL "0 0.5"]       
    set w [vFromL "1 1"]         
    set list [dpolft $x $y $w 1]
    set func [keylget list func]
    set coef [dpcoef $func 1]
    if {[exprGet $coef.vec<0>] != 0}   {error "trouble 0 in tst_dpo"}
    if {[exprGet $coef.vec<1>] != 0.5} {error "trouble 1 in tst_dpo"}


    # define the x,y points to fit.  All weights will be set to 1.0
    set dpo(x) [vectorExprNew $ndata]
    set dpo(y) [vectorExprNew $ndata]
    set dpo(w) [vectorExprNew $ndata]

    loop i 0 $ndata {
	set x [expr 2*$pi*($i+0.5)/$ndata]
	set y [cos $x]
	handleSet $dpo(x).vec<$i> $x
	handleSet $dpo(y).vec<$i> $y
	handleSet $dpo(w).vec<$i> 1
    }

    if {$doPlot} {
	echo Do the plot
	pgstateSet $pg -symb 4
	vPlot $pg $dpo(x) $dpo(y)
    }

    set maxdeg 10
    # do the fits and get the handle to the FUNC.
    set dpo(r1) [dpolft $dpo(x) $dpo(y) $dpo(w) $maxdeg]
    set dpo(func) [keylget dpo(r1) func]
    set dpo(r) [keylget dpo(r1) r]

    set yp [vectorExprNew 1]

    set nder 0
    set l 4

    if {$doPlot} {
	pgstateSet $pg -plotTitle "residual from dpolft order 6 8 10" 
	pgstateSet $pg -isNewplot 1
	foreach l "6 8 10" {
	    echo begin l=$l
	    set lx ""
	    set ly ""
	    set xmin [expr [vExtreme $dpo(x) min]+0.001]
	    set xmax [expr [vExtreme $dpo(x) max]-0.001]
	    for {set x $xmin} {$x < $xmax} {set x [expr $x+0.01]} {
		lappend lx $x
		set ytrue [cos $x]
		set yfit [dp1vlu $l $nder $x $yp $dpo(func)]
		lappend ly [expr $ytrue-$yfit]
	    }
	    set vx [vFromL $lx x]
	    set vy [vFromL $ly "ytrue-yfit"]
	    pgstateSet $pg -isLine 1 -icLine $l
	    vPlot $pg $vx $vy
	    vectorExprDel $vx
	    vectorExprDel $vy
	    pgstateSet $pg -isNewplot 0
	}
	loop i 0 [exprGet $dpo(x).dimen] {
	    set x [exprGet $dpo(x).vec<$i>]
	    pgPoint $x 0 4
	}
	pgstateSet $pg -isNewplot 1
    }


    # check the answer
    set correct -0.698861
    set fit [dp1vlu 4 0 2.34 $yp $dpo(func)]
    set delta [expr $correct-$fit]
    if {[abs $delta] > 0.0001} {
	error "expected correct=$correct but got fit=$fit"
    }

    set xmin [expr [vExtreme $dpo(x) min]+0.001]
    set xmax [expr [vExtreme $dpo(x) max]-0.001]

    set l [exprGet $dpo(func).va<1>->vec<0>]
    for {set x $xmin} {$x < $xmax} {set x [expr $x+0.01]} {
	set ytrue [cos $x]
	set yfit1 [dp1vlu $l $nder $x $yp $dpo(func)]
	set yfit2 [funcEval $dpo(func) $x]
	set delta [expr $yfit1-$yfit2]
	if {[abs $delta] > 0.0001} {
	    error "x=$x yfit1=$yfit1 yfit2=$yfit2"
	}
    }
    # now clean up
    foreach l "x y w r" {vectorExprDel $dpo($l)}
    funcDel $dpo(func)
    vectorExprDel $yp

    return 0
}


return [tst_dpo]









