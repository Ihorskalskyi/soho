proc tst_dnls1e { {doPlot 0} } {
    global pg dn
    set dn(func) [funcNew gauss "1.23 2.6 10000"]
    set lx ""; set ly ""; set lw ""
    loop i -10 10 {
	set y [funcEval $dn(func) $i]
	lappend lx $i
	lappend ly [expr $y+[randGet]-0.5]
	lappend lw 1
    }
    set dn(x) [vFromL $lx x]
    set dn(y) [vFromL $ly y]
    set dn(w) [vFromL $lw w]

    if {$doPlot} {
	pgstateSet $pg -symb 4
	vPlot $pg $dn(x) $dn(y)
    }

    set dn(meth) [methNew chi2]
    set dn(func2) [funcNew gauss "0.0 1.0 5000"]

    if {$doPlot} {
	pgSci 3
	for {set x -10} {$x < 10} {set x [expr $x+0.1]} {
	    set y [funcEval $dn(func2) $x]
	    pgPoint $x $y 1
	}
    }

    set tol [sqrt [d1mach 4]]
    dnls1e $dn(meth) $dn(func2) $dn(x) $dn(y) $dn(w) $tol
    if {$doPlot} {
	pgSci 2
	for {set x -10} {$x < 10} {set x [expr $x+0.1]} {
	    set y [funcEval $dn(func2) $x]
	    pgPoint $x $y 1
	}
    }

    dnls1eTest 0 1.2302667347
    dnls1eTest 1 2.60018172172
    dnls1eTest 2 10000.4487923

    methDel $dn(meth)
    funcDel $dn(func)
    funcDel $dn(func2)
    
    vectorExprDel $dn(x)
    vectorExprDel $dn(y)
    vectorExprDel $dn(w)

    return 0
}

proc dnls1eTest {i val} {
    global dn
    set value [exprGet $dn(func2).va<0>->vec<$i>]
    if  {[abs $value-$val]/$val > 0.001} {
	error "error in tst_dnls1e:  i=$i val=$val value=$value"
    }
    return
}

tst_dnls1e




