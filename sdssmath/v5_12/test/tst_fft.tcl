proc tst_fft1 { {nrow 512} {ncol 512} } {

    global tf
    set tf(r.image) [regNew $nrow $ncol -type FL32]
    set tf(i.image) [regNew $nrow $ncol -type FL32]
    regSetWithDbl $tf(r.image) 0.0
    regSetWithDbl $tf(i.image) 0.0

    set r0 [expr $nrow/2]
    set c0 [expr $ncol/2]
    set delta 10
    set sum 0
    loop r [expr $r0-$delta] [expr $r0+$delta-1] {
	set dr [expr 0.5+$r-$r0]
	loop c [expr $c0-$delta] [expr $c0+$delta-1] {
	    set dc [expr 0.5+$c-$c0]
	    set rad [sqrt $dr*$dr+$dc*$dc]
	    if {$rad < $delta} {
		set val [expr 1.0/$rad]
		set sum [expr $sum+$val]
		regPixSetWithDbl $tf(r.image) $r $c $val
	    }
	}
    }
    echo sum=$sum
    echo forward transform
    set tf(r.image.f) [regNewFromReg $tf(r.image)]
    set tf(i.image.f) [regNewFromReg $tf(i.image)]
    regFft $tf(r.image.f) $tf(i.image.f) 1

    echo inverse transform
    set tf(r.image.f.b) [regNewFromReg $tf(r.image.f)]
    set tf(i.image.f.b) [regNewFromReg $tf(i.image.f)]
    regFft $tf(r.image.f.b) $tf(i.image.f.b) 0

    set tf(original)  [regPixGet $tf(r.image) $r0 $c0]
    set tf(inverse-inverse) [regPixGet $tf(r.image.f.b) $r0 $c0]
    set tf(normal) [expr $tf(original)/$tf(inverse-inverse)]
    if {[abs $tf(normal)-1.0] > 0.0001} {
	foreach p "original inverse-inverse normal" {
	    echo tf($p) = $tf($p)
	}
	error TROUBLE in tst_fft1
    }
    regDel $tf(r.image)
    regDel $tf(i.image)
    regDel $tf(r.image.f)
    regDel $tf(i.image.f)
    regDel $tf(r.image.f.b)
    regDel $tf(i.image.f.b)

    return 0
}

proc tst_fft2 { {size 256} } {
    global tf


    echo make the psf and shuffle it
    set tf(r.psf) [regNew $size $size -type FL32]
    set tf(i.psf) [regNew $size $size -type FL32]
    regSetWithDbl $tf(r.psf) 0.0
    regSetWithDbl $tf(i.psf) 0.0
    set r0 [expr $size/2]
    set c0 [expr $size/2]
    set delta 10
    set sum 0.0
    loop r [expr $r0-$delta] [expr $r0+$delta-1] {
	set dr [expr 0.5+$r-$r0]
	loop c [expr $c0-$delta] [expr $c0+$delta-1] {
	    set dc [expr 0.5+$c-$c0]
	    set rad [sqrt $dr*$dr+$dc*$dc]
	    if {$rad < $delta} {
		set val [expr 1.0/$rad]
		regPixSetWithDbl $tf(r.psf) $r $c $val
		set sum [expr $sum+$val]
	    }
	}
    }
    set norm [expr $size*$size/$sum]
    regWrapAround $tf(r.psf) $norm

    echo forward transform psf
    set tf(r.psf.f) [regNewFromReg $tf(r.psf)]
    set tf(i.psf.f) [regNewFromReg $tf(i.psf)]
    regFft $tf(r.psf.f) $tf(i.psf.f) 1

    echo make the image
    set tf(r.image) [regNew $size $size -type FL32]
    set tf(i.image) [regNew $size $size -type FL32]
    regSetWithDbl $tf(r.image) 0.0
    regSetWithDbl $tf(i.image) 0.0

    set r0 [expr $size/2]
    set c0 [expr $size/2]
    set delta 30
    loop r [expr $r0-$delta] [expr $r0+$delta-1] {
	loop c [expr $c0-$delta] [expr $c0+$delta-1] {
	    regPixSetWithDbl $tf(r.image) $r $c 100000
	}
    }

    timerStart

    echo forward transform image
    set tf(r.image.f) [regNewFromReg $tf(r.image)]
    set tf(i.image.f) [regNewFromReg $tf(i.image)]
    regFft $tf(r.image.f) $tf(i.image.f) 1

    echo multiply transforms
    set tf(r.mult.f) [regNewFromReg $tf(r.image.f)]
    set tf(i.mult.f) [regNewFromReg $tf(i.image.f)]
    regElementMult $tf(r.mult.f) $tf(i.mult.f) $tf(r.psf.f) $tf(i.psf.f)

    echo inverse transform the multiplied transforms
    set tf(r.mult.f.b) [regNewFromReg $tf(r.mult.f)]
    set tf(i.mult.f.b) [regNewFromReg $tf(i.mult.f)]
    regFft $tf(r.mult.f.b) $tf(i.mult.f.b) 0

    set tf(lap) [timerLap]

    set tf(original)  [regPixGet $tf(r.image) [expr $size/2] [expr $size/2]]
    set tf(convolved) [regPixGet $tf(r.mult.f.b) [expr $size/2] [expr $size/2]]
    set tf(normal) [expr $tf(original)/$tf(convolved)]
    if {[abs $tf(normal)-1.0] > 0.0001} {
	error "TROUBLE in tst_fft2"
    }

    regDel $tf(r.psf)
    regDel $tf(i.psf)
    regDel $tf(r.psf.f)
    regDel $tf(i.psf.f)
    regDel $tf(r.image)
    regDel $tf(i.image)
    regDel $tf(r.image.f)
    regDel $tf(i.image.f)
    regDel $tf(r.mult.f)
    regDel $tf(i.mult.f)
    regDel $tf(r.mult.f.b)
    regDel $tf(i.mult.f.b)

    return 0
}


proc tst_fft3 { {size 256} } {
    global tf


    echo make the psf and shuffle it
    set tf(r.psf) [regNew $size $size -type FL32]
    set tf(i.psf) [regNew $size $size -type FL32]
    regSetWithDbl $tf(r.psf) 0.0
    regSetWithDbl $tf(i.psf) 0.0
    set r0 [expr $size/2]
    set c0 [expr $size/2]
    set delta 10
    set sum 0.0
    loop r [expr $r0-$delta] [expr $r0+$delta-1] {
	set dr [expr 0.5+$r-$r0]
	loop c [expr $c0-$delta] [expr $c0+$delta-1] {
	    set dc [expr 0.5+$c-$c0]
	    set rad [sqrt $dr*$dr+$dc*$dc]
	    if {$rad < $delta} {
		set val [expr 1.0/$rad]
		regPixSetWithDbl $tf(r.psf) $r $c $val
		set sum [expr $sum+$val]
	    }
	}
    }
    set norm [expr $size*$size]
    echo forward transform psf
    set tf(r.psf.f) [regNewFromReg $tf(r.psf)]
    set tf(i.psf.f) [regNewFromReg $tf(i.psf)]
    regFftw $tf(r.psf.f) $tf(i.psf.f) 1

    echo inverse transform psf
    set tf(r.psf.f.b) [regNewFromReg $tf(r.psf.f)]
    set tf(i.psf.f.b) [regNewFromReg $tf(i.psf.f)]
    regFftw $tf(r.psf.f.b) $tf(i.psf.f.b) 0

    echo normalize the inverse-inverse
    regMultiplyWithDbl $tf(r.psf.f.b) [expr 1.0/$norm]

    set tf(original)  [regPixGet $tf(r.psf) $r0 $c0]
    set tf(inverse-inverse) [regPixGet $tf(r.psf.f.b) $r0 $c0]
    set tf(normal) [expr $tf(original)/$tf(inverse-inverse)]
    if {[abs $tf(normal)-1.0] > 0.0001} {
	foreach p "original inverse-inverse normal" {
	    echo tf($p) = $tf($p)
	}
	error "TROUBLE in tst_fft3"
    }

    regDel $tf(r.psf)
    regDel $tf(i.psf)
    regDel $tf(r.psf.f)
    regDel $tf(i.psf.f)
    regDel $tf(r.psf.f.b)
    regDel $tf(i.psf.f.b)

    return 0

}


proc tst_fft4 { {nrow 256} {ncol 256}} {
    global tf


    echo make the psf and shuffle it
    set tf(r.psf) [regNew $nrow $ncol -type FL32]
    set tf(i.psf) [regNew $nrow $ncol -type FL32]
    regSetWithDbl $tf(r.psf) 0.0
    regSetWithDbl $tf(i.psf) 0.0
    set r0 [expr $nrow/2]
    set c0 [expr $ncol/2]
    set delta 10
    set sum 0.0
    loop r [expr $r0-$delta] [expr $r0+$delta-1] {
	set dr [expr 0.5+$r-$r0]
	loop c [expr $c0-$delta] [expr $c0+$delta-1] {
	    set dc [expr 0.5+$c-$c0]
	    set rad [sqrt $dr*$dr+$dc*$dc]
	    if {$rad < $delta} {
		set val [expr 1.0/$rad]
		regPixSetWithDbl $tf(r.psf) $r $c $val
		set sum [expr $sum+$val]
	    }
	}
    }
    set norm 1
    regWrapAround $tf(r.psf) $norm

    echo forward transform psf
    set tf(r.psf.f) [regNewFromReg $tf(r.psf)]
    set tf(i.psf.f) [regNewFromReg $tf(i.psf)]
    regFftw $tf(r.psf.f) $tf(i.psf.f) 1

    echo make the image
    set tf(r.image) [regNew $nrow $ncol -type FL32]
    set tf(i.image) [regNew $nrow $ncol -type FL32]
    regSetWithDbl $tf(r.image) 0.0
    regSetWithDbl $tf(i.image) 0.0

    set r0 [expr $nrow/2]
    set c0 [expr $ncol/2]
    set delta 30
    loop r [expr $r0-$delta] [expr $r0+$delta-1] {
	loop c [expr $c0-$delta] [expr $c0+$delta-1] {
	    regPixSetWithDbl $tf(r.image) $r $c 100000
	}
    }

    timerStart

    echo forward transform image
    set tf(r.image.f) [regNewFromReg $tf(r.image)]
    set tf(i.image.f) [regNewFromReg $tf(i.image)]
    regFftw $tf(r.image.f) $tf(i.image.f) 1

    echo multiply transforms
    set tf(r.mult.f) [regNewFromReg $tf(r.image.f)]
    set tf(i.mult.f) [regNewFromReg $tf(i.image.f)]
    regElementMult $tf(r.mult.f) $tf(i.mult.f) $tf(r.psf.f) $tf(i.psf.f)

    echo inverse transform the multiplied transforms
    set tf(r.mult.f.b) [regNewFromReg $tf(r.mult.f)]
    set tf(i.mult.f.b) [regNewFromReg $tf(i.mult.f)]
    regFftw $tf(r.mult.f.b) $tf(i.mult.f.b) 0
    regMultiplyWithDbl $tf(r.mult.f.b) \
	    [expr 1.0/([double $nrow]*$nrow*$ncol*$ncol)]
    set tf(lap) [timerLap]

    set tf(original)  [regPixGet $tf(r.image) [expr $nrow/2] [expr $ncol/2]]
    set tf(convolved) [regPixGet $tf(r.mult.f.b) [expr $nrow/2] [expr $ncol/2]]
    set tf(normal) [expr $tf(original)/$tf(convolved)]
    if {[abs $tf(normal)-1.0] > 0.0001} {
	foreach p "original convolved normal" {
	    echo tf($p) = $tf($p)
	}
	error TROUBLE in tst_fft4
    }

    regDel $tf(r.psf)
    regDel $tf(i.psf)
    regDel $tf(r.psf.f)
    regDel $tf(i.psf.f)
    regDel $tf(r.image)
    regDel $tf(i.image)
    regDel $tf(r.image.f)
    regDel $tf(i.image.f)
    regDel $tf(r.mult.f)
    regDel $tf(i.mult.f)
    regDel $tf(r.mult.f.b)
    regDel $tf(i.mult.f.b)

    return 0
}


proc tst_fft_all { } {
    foreach n "1 2 3 4" {
	set proc tst_fft$n
	set memBefore [memBytesInUse]
	echo -------------------------- begin $proc -- memBytesInUse = $memBefore
	$proc
	set memAfter [memBytesInUse]
	if {$memBefore != $memAfter} {
	    error "memory leaks in tst_fft somewhere...\n B4=$memBefore AFTER=$memAfter"
	}
    }

    return 0
}

return [tst_fft_all]
