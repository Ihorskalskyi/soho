proc tst_dgefs { } {
    global a atemp b btemp bxex delta

    set errcmp [pow [d1mach 4] 0.8]
    set nerr 0

    set a [vFromL \
	    "5.0  -1.0  4.5 0.5 0.0 \
	     1.0 -0.5 -1.0 2.0 0.0 \
	     0.3  1.0 -1.7 0.6 0.0 \
	     2.1  1.0  2.0 1.3 0.0"]
    set lda 5
    set n 4
    set b [vFromL "0 3.5 3.6 2.4"]    
    set work [vectorExprNew $n]
    set iwork [vectorExprNew $n]

    # First test case - normal
    set itask 1
    set btemp [vectorExprEval $b]
    set atemp [vectorExprNew 20]
    loop i 0 $n {
	loop j 0 $n {
	    set l [expr $lda*$i+$j]
	    set r [expr $lda*$j+$i]
	    set v [exprGet $a.vec<$r>]
	    handleSet $atemp.vec<$l> $v
	}
    }
    echo now call dgefs
    set ind [dgefs $atemp $lda $n $btemp $itask $work $iwork]
    echo ind=$ind

    # Now check the errors for the first case
    set bxex [vFromL "1 1 -1 1"]
    set delta [vectorExprEval abs($btemp-$bxex)]
    set maxDelta 0
    loop i 0 $n {
	if {[exprGet $delta.vec<$i>] > $maxDelta} {
	    set maxDelta [exprGet $delta.vec<$i>]
	}
    }
    if {$errcmp < $maxDelta} {
	echo "test 1:  errcmp=$errcmp  maxDelta=$maxDelta"
	incr nerr
    }


    # Second test case - singular
    set itask 1
    set btemp [vectorExprEval $b]
    set atemp [vectorExprNew 20]
    loop i 0 $n {
	loop j 0 $n {
	    set l [expr $lda*$i+$j]
	    set r [expr $lda*$j+$i]
	    set v [exprGet $a.vec<$r>]
	    if {$i==0} {set v 0.0}
	    handleSet $atemp.vec<$l> $v
	}
    }
    set ind [dgefs $atemp $lda $n $btemp $itask $work $iwork]


    return $nerr
}

return [tst_dgefs]





