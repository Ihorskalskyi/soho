proc tst_dnls1lin { {dev NULL} } {
    global pg dn

    set memBefore [memBytesInUse]
    echo -------------------------- begin tst_dnls1lin --- memBytesInUse = $memBefore

    rand 1.2345678
    set vTemp [vFromL "2.5 1.00"]
    set dn(func) [funcNew lin $vTemp]
    set lx ""; set ly ""; set lw ""; set ldiag ""
    loop i -10 10 {
	set y [funcEval $dn(func) $i]
	lappend lx $i
	lappend ly [expr $y+0.1*([rand]-0.5)]
	lappend lw 0.01
	lappend ldiag 1
    }
    set dn(x) [vFromL $lx x]
    set dn(y) [vFromL $ly y]
    set dn(w) [vFromL $lw w]
    set dn(diag) [vFromL $ldiag diag]

    set dn(meth) [methNew chi2]
    set vTemp [vFromL "2.0 1.1"]
    set dn(func2) [funcNew lin $vTemp]

    if {$dev != "NULL"} {
	pgInit $dev
	pgstateSet $pg -symb 4
	vPlot $pg $dn(x) $dn(y)
	pgSci 2
	for {set x -11} {$x < 11} {set x [expr $x+0.2]} {
	    set y [funcEval $dn(func2) $x]
	    pgPoint $x $y 1
	}
	pgSci 1
    }

    set dn(return) [dnls1 $dn(meth) $dn(func2) $dn(x) $dn(y) $dn(w) $dn(diag)]

    if {$dev != "NULL"} {
	pgSci 3
	for {set x -11} {$x < 11} {set x [expr $x+0.2]} {
	    set y [funcEval $dn(func2) $x]
	    pgPoint $x $y 1
	}
	pgSci 1
    }

    pc va<0>->vec<0> 2.49848562044
    pc va<0>->vec<1> 0.99959428279
    pc sigma->vec<0> 10.2395465546
    pc sigma->vec<1> 0.281838365903
    pc cov->vec<0>  10.2395465546
    pc cov->vec<1> 0.0665640400457
    pc cov->vec<2> 0.0665640400457
    pc cov->vec<3> 0.281838365903
    pc chi2  4.28911000035e-05         

    if {$dev != "NULL"} {pgstateDel $pg}
    funcDel $dn(func)
    funcDel $dn(func2)
    methDel $dn(meth)
    vectorExprDel $dn(x)
    vectorExprDel $dn(y)
    vectorExprDel $dn(w)
    vectorExprDel $dn(diag)

    set memAfter [memBytesInUse]
    echo -------------------------- ending tst_dnls1lin -- memBytesInUse = $memAfter
    if {$memBefore != $memAfter} {
	error "Memory leak in tst_dnls1lin.tcl -- B4=$memBefore AFTER=$memAfter"
    }

    return 0
}

proc pc {astring b} {
    global dn
    set a [exprGet $dn(func2).$astring]
    set diff [abs ($a-$b)/$b]
    if {$diff > 0.1} {
	error "astring=$astring a=$a b=$b diff=$diff"
    }
}

if {[tst_dnls1lin] != 0} {
    error "Error from tst_dnls1lin"
}






