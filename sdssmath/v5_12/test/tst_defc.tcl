proc tst_defc { {doPlot 0} } {
    global dbs pg

    seedSet 123455557
    set ndata 1000
    set nknot 12
    set nord 4

    set pi 3.14159
    set twopi [expr 2.0*$pi]

    # set up the x and y vectors to fit
    set dbs(x) [vectorExprNew $ndata]
    set dbs(y) [vectorExprNew $ndata]
    set dbs(sd) [vectorExprNew $ndata]
    set dbs(bkpt.sorted) [vectorExprNew $nknot]

    loop i 0 $ndata {
	set x [randGet -min 0.0 -max 5.0]
	set y [expr [cos $x]+[randNormalGet -mean 0.0 -sigma 0.1]]
	handleSet $dbs(x).vec<$i> $x
	handleSet $dbs(y).vec<$i> $y
	handleSet $dbs(sd).vec<$i> 1.0
    }

    set xmin [vExtreme $dbs(x) min]
    set xmax [vExtreme $dbs(x) max]
    set deltax [expr ($xmax-$xmin)/($nknot-$nord-3.0)]

    loop i 0 [expr $nord] {
	handleSet $dbs(bkpt.sorted).vec<$i> $xmin
	set j [expr $nknot-$i-1]
	handleSet $dbs(bkpt.sorted).vec<$j> $xmax
    }

    loop k [expr $nord] [expr $nknot-$nord] {
	handleSet $dbs(bkpt.sorted).vec<$k> [expr $xmin+($k-$nord+1)*$deltax]
    }



    set ymin [vExtreme $dbs(y) min]
    set ymax [vExtreme $dbs(y) max]

    if {$doPlot} {

	# Plot the points
	pgstateSet $pg -symb 1 -isLine 0
	vPlot $pg $dbs(x) $dbs(y)

	# Plot the knot locations
	pgSci 3
	loop i 0 [exprGet $dbs(bkpt.sorted).dimen] {
	    set x [exprGet $dbs(bkpt.sorted).vec<$i>]
	    pgLine "$x $x" "$ymin $ymax"
	    
	}
	pgSci 1

    }


    # do the fit
    set dbs(r1) [defc $dbs(x) $dbs(y) $dbs(sd) $nord $dbs(bkpt.sorted)]

    if {$doPlot} {

	# overplot the fit function
	pgstateSet $pg -isLine 1 -isNewplot 0 -icLine 2
	set xmin [expr [vExtreme $dbs(bkpt.sorted) min]+0.001]
	set xmax [expr [vExtreme $dbs(bkpt.sorted) max]-0.001]
	funcPlot $pg $dbs(r1) $xmin $xmax 100

	# plot the residual points
	set lx ""
	set ly ""
	loop i 0 [exprGet $dbs(x).dimen] {
	    set x [exprGet $dbs(x).vec<$i>]
	    set yfit [funcEval $dbs(r1) $x]
	    set y [exprGet $dbs(y).vec<$i>]
	    lappend lx $x
	    lappend ly [expr $y-$yfit]
	}
	set vx [vFromL $lx x]
	set vy [vFromL $ly y-yfit]
	pgstateSet $pg -isLine 0 -isNewplot 1 -plotTitle "residual from defc"
	vPlot $pg $vx $vy

	# overplot the fit residual
	set lx ""
	set ly ""
	for {set x $xmin} {$x < $xmax} {set x [expr $x+0.01]} {
	    lappend lx $x
	    set ytrue [cos $x]
	    set yfit [funcEval $dbs(r1) $x]
	    lappend ly [expr $ytrue-$yfit]
	}
	set vx [vFromL $lx x]
	set vy [vFromL $ly "ytrue-yfit"]
	pgstateSet $pg -isLine 1 -isNewplot 0 -icLine 2
	vPlot $pg $vx $vy
	pgstateSet $pg -isLine 0 -isNewplot 1 -icLine 1

    }

    set yfit [funcEval $dbs(r1) 2.7]
    set correct -0.908293
    set delta [expr $correct-$yfit]
    echo $delta
    if {[abs $delta] > 0.005} {
	error "expected dbvalu $correct  but got $yfit"
    }
    return 0
}


return [tst_defc]
