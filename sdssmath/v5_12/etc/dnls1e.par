DESCRIPTION
The purpose of DNLS1E is to minimize the sum of the squares of M
nonlinear functions in N variables by a modification of the
Levenberg-Marquardt algorithm.  This is done by using the more
general least-squares solver DNLS1.  The user must provide a
subroutine which calculates the functions.  The user has the
option of how the Jacobian will be supplied.  The user can
supply the full Jacobian, or the rows of the Jacobian (to avoid
storing the full Jacobian), or let the code approximate the
Jacobian by forward-differencing.  This code is the combination
of the MINPACK codes (Argonne) LMDER1, LMDIF1, and LMSTR1.
RETURN
Returns info:  1, 2, or 3 are OK
PARAMETER <meth> FTCL_ARGV_STRING METH* "how to minimize" NULL
PARAMETER <func> FTCL_ARGV_STRING FUNC* "the function to fit" NULL
PARAMETER <x> FTCL_ARGV_STRING VECTOR* "vector of x values" NULL
PARAMETER <y> FTCL_ARGV_STRING VECTOR* "vector of y values" NULL
PARAMETER <w> FTCL_ARGV_STRING VECTOR* "vector of weights" NULL
PARAMETER -mask FTCL_ARGV_STRING VECTOR* "mask vector" NULL
PARAMETER -nprint FTCL_ARGV_INT int "number of calls with iflag=0" 0
PARAMETER -iopt FTCL_ARGV_INT int "only 1 works now" 1
PARAMETER <tol> FTCL_ARGV_DOUBLE double "relative error to define termination" 0
DECLARE 
int *iw;
double *wa, *fvec, *param;
int i, m, n, info, lwa;
CALL
n = func->va[0]->dimen;
param = (double *) shMalloc(n*sizeof(double));
for (i=0; i<n; i++) {
	param[i] = func->va[0]->vec[i];
}

m = x->dimen;
iw = (int *) shMalloc(n*sizeof(int));
if ( (iopt==1) || (iopt==2) ) {
	lwa = n*(m+5)+m;
} else {
	lwa = n*(n+5)+m;
}
wa = (double *) shMalloc(lwa*sizeof(double));
fvec = (double *) shMalloc(m*sizeof(double));
smaGlobalSet(x, y, w, func, mask);
smaDNLS1E(meth->ma, iopt, m, n, param, fvec, 
	tol, nprint, &info, iw, wa, lwa);
for (i=0; i<n; i++) {
	func->va[0]->vec[i] = param[i];
}
shFree(param); shFree(iw); shFree(wa); shFree(fvec);
OUTPUT
%d info





