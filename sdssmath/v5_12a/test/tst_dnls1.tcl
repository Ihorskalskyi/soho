proc tst_dnls1 { {dev NULL} } {
    global pg dn

    set memBefore [memBytesInUse]
    echo -------------------------- begin tst_dnls1 --- memBytesInUse = $memBefore

    # Define a func which is a gaussian centered at 1.23, sigma=2.6,
    # and normalization of 10000.
    set vTemp [vFromL "1.23 2.6 10000"]
    set dn(func) [funcNew gauss $vTemp]
    set lx ""; set ly ""; set lw ""; set ldiag ""

    # Make x,y points on this gaussian with some variation
    loop i -10 10 {
	set y [funcEval $dn(func) $i]
	lappend lx $i
	lappend ly [expr $y+[randGet]-0.5]
	lappend lw 1
	lappend ldiag 1
    }
    set dn(x) [vFromL $lx x]
    set dn(y) [vFromL $ly y]
    set dn(w) [vFromL $lw w]
    set dn(diag) [vFromL $ldiag diag]

    if {$dev != "NULL"} {
	pgInit $dev
	pgstateSet $pg -symb 4
	vPlot $pg $dn(x) $dn(y)
    }

    # Set up the method to be used for the fit, and also a "guess" at
    # the function to be fit which is not quite right
    set dn(meth) [methNew chi2]
    set vTemp [vFromL "0.0 1.0 5000"]
    set dn(func2) [funcNew gauss $vTemp]


    # plot the points and the "incorrect" guess
    if {$dev != "NULL"} {
	pgSci 3
	for {set x -10} {$x < 10} {set x [expr $x+0.1]} {
	    set y [funcEval $dn(func2) $x]
	    pgPoint $x $y 1
	}
    }

    # do the fit and plot the correct fit
    set tol [sqrt [d1mach 4]]
    set dn(return) [dnls1 $dn(meth) $dn(func2) $dn(x) $dn(y) $dn(w) $dn(diag)]
    if {$dev != "NULL"} {
	pgSci 2
	for {set x -10} {$x < 10} {set x [expr $x+0.1]} {
	    set y [funcEval $dn(func2) $x]
	    pgPoint $x $y 1
	}
    }

    # check the answers
    dnls1Test 0 1.2302667347
    dnls1Test 1 2.60018172172
    dnls1Test 2 10000.4487923

    # clean up and look for memory leaks
    if {$dev != "NULL"} {pgstateDel $pg}

    methDel $dn(meth)
    funcDel $dn(func)
    funcDel $dn(func2)
    
    vectorExprDel $dn(x)
    vectorExprDel $dn(y)
    vectorExprDel $dn(w)
    vectorExprDel $dn(diag)

    set memAfter [memBytesInUse]
    echo -------------------------- ending tst_dnls1 -- memBytesInUse = $memAfter
    if {$memBefore != $memAfter} {
	error "Memory leak in tst_dnls1.tcl -- B4=$memBefore AFTER=$memAfter"
    }

    return 0
}

proc dnls1Test {i val} {
    global dn
    set value [exprGet $dn(func2).va<0>->vec<$i>]
    if  {[abs $value-$val]/$val > 0.001} {
	error "error in tst_dnls1:  i=$i val=$val value=$value"
    }
    return
}

tst_dnls1













