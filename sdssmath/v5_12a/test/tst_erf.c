/*** 
  Simple test of C binding for error function, DERF, and
  complimentary error function, DERFC
***/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "sdssmath.h"
#include "slatec.h"

#define debug 0

int main(void) {

int   retvalue = 0;
float answer, delta, x;

#if debug
printf("Test of error function:\n");
#endif
x = 0.3;
answer = smaDERF(x);
delta = answer - 0.3286;
if ( delta > 0.0001 ) {
  retvalue = 1;
  printf("Error in tst_erf with DERF\n");
}
#if debug
printf("  Correct answer for   erf(0.3) = 0.3286, we get: %f\n",answer);
#endif

#if debug
printf("Test of complimentary error function:\n");
#endif
x = 0.6;
answer = smaDERFC(x);
delta = answer - 0.3961;
if ( delta > 0.0001 ) {
  retvalue = 1;
  printf("Error in tst_erf with DERFC\n");
}
#if debug
printf("  Correct answer for  erfc(0.6) = 0.3961, we get: %f\n",answer);
#endif

return retvalue;
}
