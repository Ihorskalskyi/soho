/*** 
  Simple test of FUNC with "linMulti"

  Take linear equation: \Sum a_i x_i = y ; where, y,  all the a_i
  and all but one x_i are known and solve for the unknown x_i

***/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "sdssmath.h"
#include "slatec.h"
#include "smaFunc.h"
#include "libfits.h"

void *g_saoCmdHandle;

int main(void) {

  int ct, i, j;
  int dimVector, numVector;
  int retvalue;
  double fakeCov[4][4], fakeChi2;
  double xValue, yValue;
  double answer, delta;
  VECTOR **values;
  FUNC *testFunc;

  load_libfits(1);

  retvalue = 0;
  numVector = 3;
  dimVector = 4;
  answer = 16.166666666;

/* create vectors with parameters and constants */

  values = (VECTOR **)shMalloc( numVector * sizeof(VECTOR *) );
  for ( ct = 0; ct < numVector; ct++ ) {
    values[ct] = shVectorNew(dimVector);
  }

/* constants */
  values[0]->vec[0] =  1.0;
  values[0]->vec[1] = 12.0;
  values[0]->vec[2] =  6.0;
  values[0]->vec[3] = -7.0;

/* xValues, with place holder for x_2, which we'll solve for */
  values[1]->vec[0] =   1.0;
  values[1]->vec[1] =   2.0;
  values[1]->vec[2] =   0.0;
  values[1]->vec[3] =  16.0;

/* yValue, with place holders for unused part of vector */
  values[2]->vec[0] = 10.0;
  values[2]->vec[1] =  0.0;
  values[2]->vec[2] =  0.0;
  values[2]->vec[3] =  0.0;

/* create a fake covariance matrix and chi2 */
  for ( i = 0; i < dimVector; i++ ) {
    for ( j = 0; j < dimVector; j++ ) {
      fakeCov[i][j] = 0.05;
    }
  }
  fakeCov[0][0] = 0.1;
  fakeCov[1][1] = 0.2;
  fakeCov[2][2] = 0.3;
  fakeCov[3][3] = 0.4;

  fakeChi2 = 0.9;

/* create the LinMulti FUNC */

  testFunc = smFuncNew( "linMulti", values, 3);
  if ( testFunc == NULL ) { 
    retvalue = 1;
    printf("Error in tst_smaFuncUnknown: testFunc is NULL\n"); 
  } 

  testFunc->cov = shVectorNew(dimVector*dimVector);
  if ( testFunc->cov == NULL ) { 
    retvalue = 1;
    printf("Error in tst_smaFuncUnknown: testFunc->cov is NULL\n"); 
  }

  ct = 0;
  for ( i = 0; i < dimVector; i++ ) {
    for ( j = 0; j < dimVector; j++ ) {
      testFunc->cov->vec[ct] = fakeCov[i][j];
      ct++;
    }
  }

  testFunc->sigma = shVectorNew(dimVector);
  if ( testFunc->sigma == NULL ) { 
    retvalue = 1;
    printf("Error in tst_smaFuncUnknown: testFunc->cov is NULL\n"); 
  }
  
  for ( ct = 0; ct < dimVector; ct++ ) {
    testFunc->sigma->vec[ct] = sqrt( fakeCov[ct][ct] );
  }

/* evaluate this FUNC  */
  xValue = 2;
  yValue = 0;
  smaFuncEval( testFunc, xValue, &yValue );

  delta = ( yValue - answer );
  if ( delta > 0.00001 ) {
    retvalue = 1;
    printf("Error in tst_smaFuncLinMulti\n");
  }

/* clean up memory */

  shFree(values);
  return retvalue;
}
