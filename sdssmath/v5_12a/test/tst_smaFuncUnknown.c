/*** 
  Simple test of FUNC with "unknown"
***/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "sdssmath.h"
#include "slatec.h"
#include "smaFunc.h"
#include "libfits.h"

void *g_saoCmdHandle;

int main(void) {

  int ct, i, j;
  int dimVector, numVector;
  int retvalue = 0;
  double fakeNum[4], fakeCov[4][4], fakeChi2;
  double xValue, yValue, xValueSum;
  VECTOR **values;
  FUNC *testFunc;

  load_libfits(1);

  numVector = 1;
  dimVector = 4;

/* make fake parameters with fake covariance matrix */

  fakeNum[0] =  2.3;
  fakeNum[1] = 13.1;
  fakeNum[2] = 22.7;
  fakeNum[3] = 31.3;  

  for ( i = 0; i < dimVector; i++ ) {
    for ( j = 0; j < dimVector; j++ ) {
      fakeCov[i][j] = 0.05;
    }
  }
  fakeCov[0][0] = 0.1;
  fakeCov[1][1] = 0.2;
  fakeCov[2][2] = 0.3;
  fakeCov[3][3] = 0.4;

  fakeChi2 = 0.9;

/* create a FUNC and fill with fake values */

  values = (VECTOR **)shMalloc( numVector * sizeof(VECTOR *) );
  for ( ct = 0; ct < numVector; ct++ ) {
    values[ct] = shVectorNew(dimVector);
  }

  testFunc = smFuncNew( "unknown", values, 1);
  if ( testFunc == NULL ) { 
    retvalue = 1;
    printf("Error in tst_smaFuncUnknown: testFunc is NULL\n"); 
  } 

  for ( ct = 0; ct < dimVector; ct++ ) {
    testFunc->va[0]->vec[ct] = fakeNum[ct];
  }

  testFunc->cov = shVectorNew(dimVector*dimVector);
  if ( testFunc->cov == NULL ) { 
    retvalue = 1;
    printf("Error in tst_smaFuncUnknown: testFunc->cov is NULL\n"); 
  }

  ct = 0;
  for ( i = 0; i < dimVector; i++ ) {
    for ( j = 0; j < dimVector; j++ ) {
      testFunc->cov->vec[ct] = fakeCov[i][j];
      ct++;
    }
  }

  testFunc->sigma = shVectorNew(dimVector);
  if ( testFunc->sigma == NULL ) { 
    retvalue = 1;
    printf("Error in tst_smaFuncUnknown: testFunc->cov is NULL\n"); 
  }
  
  for ( ct = 0; ct < dimVector; ct++ ) {
    testFunc->sigma->vec[ct] = sqrt( fakeCov[ct][ct] );
  }

  testFunc->chi2 = fakeChi2;

/* evaluate this FUNC  
 * all inputs should give back zeros, so will just feed in lots
 * of quasi-random numbers to check this
 */

  /* Initialize xValueSum (but don't change the name!) */
  xValueSum = 0;
  xValue = 89.76;
  for ( ct = 0; ct < 30; ct++ ) {
    xValue *= 12.34 * ( ct + 2);
    smaFuncEval( testFunc, xValue, &yValue );  
    xValueSum += yValue;
    smaFuncEval( testFunc, 1.0/xValue, &yValue );  
    xValueSum += yValue;
  }
  if ( xValueSum != 0 ) {
    retvalue = 1;
    printf("Error in tst_smaFuncUnknown: xValueSum !=0\n"); 
  }

/* clean up memory */

  shFree(values);
  return retvalue;
}
