/* Simple test of C binding smaBINT4 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "sdssmath.h"
#include "slatec.h"

int main(void) {
#define debug 0
#define NDATA 11
#define MYPI 3.1415926535897932384
  float x[NDATA], y[NDATA], t[NDATA+6], bcoef[NDATA+2], w[5*(NDATA+2)];
  int kntopt, n, k;

  int inbv;
  float *work;
  float bv, xx, yy;

  int i;
  float delta;
  float tol=1000.0*smaR1MACH(4);

  for (i=0; i<NDATA; i++) {
    x[i] = (float) (i)/(NDATA-1);
    y[i] = sin(MYPI*x[i]);
  }

  kntopt=1;
  smaBINT4(x, y, NDATA, 2, 2, 0.0, 0.0, kntopt, t, bcoef, &n, &k, w);

  work = (float *) malloc(3*k);
  inbv = 1;
  for (i=0; i<NDATA; i++) {
    bv = smaBVALU(t, bcoef, n, k, 0, x[i], &inbv, work);
    delta = fabs(bv-y[i]);
    if (delta > tol) {
      printf("error in tst_bspline: i=%3d x[i]=%5.2f y[i]=%5.2f bv=%5.2f\n",
	     i, x[i], y[i], bv);
      printf("delta=%g  tol=%g\n", delta, tol);
      return 1;
    }
  }

  for (xx=x[0]; xx<x[NDATA-1]; xx += 1.0/(10*NDATA)) {
    yy = sin(MYPI*xx);
    bv = smaBVALU(t, bcoef, n, k, 0, xx, &inbv, work);
    delta = fabs(bv-yy);
    if (delta > tol) {
      printf("error in tst_bspline: xx=%f   yy=%f   bv=%f\n", xx, yy, bv);
      return 1;
    }
  }
  free(work);
  return 0;
}








