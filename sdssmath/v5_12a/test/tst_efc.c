/* Simple test of C binding smaEFC */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "sdssmath.h"
#include "slatec.h"

int main(void) {
#define debug 0
#define NDATA 11
#define NORD 4
#define NBKPT 2*NORD + 2
#define LW 256
#define MYPI 3.1415926535897932384

  float xdata[NDATA], ydata[NDATA], sddata[NDATA], bkpt[NBKPT], coeff[NORD], 
    w[LW], work[3*NORD];
  int mdein, mdeout, k;

  int inbv;
  float bv, xx, yy;

  int i, intKnots;
  float delta;
  float tol=1000.0*smaR1MACH(4);

  if (debug) printf("\nData\n");
  for (i=0; i<NDATA; i++) {
    xdata[i] = (float) (i)/(NDATA-1);
    ydata[i] = sin(MYPI*xdata[i]);
    sddata[i] = 1.0;
    if (debug) printf("%f %f %f\n", xdata[i], ydata[i], sddata[i]);
  }

  /* end knots.  NB having same-valued end knots gives fpe in bsplvn */
  for (i=0; i<NORD-1; i++) {
    bkpt[i] = xdata[0] - 0.1*(NORD + i + 1);
    bkpt[NBKPT-i-1] = xdata[NDATA-1] + 0.1*(NORD - i - 1);
  }

  /* internal knots */
  intKnots = NBKPT-NORD-2;
  for (i=0; i<intKnots; i++) {
    bkpt[NORD+i-1] = (float) (i)/(intKnots - 1.0);
  }

  if (debug) printf("\nKnots\n");
  for (i=0; i<NBKPT; i++) {
    if (debug) printf("%f\n", bkpt[i]);
  }

  mdein = 1;
  smaEFC(NDATA, xdata, ydata, sddata, NORD, NBKPT, bkpt, mdein, 
	 &mdeout, coeff, LW, w);

  if (mdeout == 1) return 0;

  printf("error in tst_efc: mdeout = %d\n", mdeout);
  return 1;
}
