/*
 * Simple test for Leamax functions
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "sdssmath.h"


static void subf(
		 int *k,
		 double *xOnePt,
		 int *n,
		 double *a,
		 double *f,
		 double *df,
		 int *mode,
		 int *nerror
		 ) {
  *f = a[0] + a[1]*xOnePt[0] + a[2]*xOnePt[0]*xOnePt[0];
  *nerror = 0;
  return;
}

int main(void) {
  int nc = 3;
  int nx = 1;
  int nf = 15;
  int maxit = 50;
  int nw = 9*nc+4*nf+2*nf*nc+3*nc*nc;
  double *x;
  double *y;
  double *sy;
  double *a;
  double *al;
  double *au;
  double *dphi;
  double *cov;
  double *std;
  int *iafr;
  double *w;
  double phi;
  int nerr;

  int k,n,m,mode,iprt;
  double eps;
  int mfr,i;

  k = nx;
  n = nc;
  m = nf;
  mode = 0;
  iprt = 0;
  eps = 1e-12;
  
  al = (double *) shMalloc(nc*sizeof(double));
  au = (double *) shMalloc(nc*sizeof(double));
  a  = (double *) shMalloc(nc*sizeof(double));
  for (i=0; i<nc; i++) {
    al[i] = -1e30;
    au[i] =  1e30;
    a[i] =   i+1;
  }


  x = (double *) shMalloc(nf*nx*sizeof(double));
  y = (double *) shMalloc(nf*sizeof(double));
  sy = (double *) shMalloc(nf*sizeof(double));
  for (i=0; i<nf; i++) {
    x[i*nx] = i+1;
    y[i] = 1.111 + 2.222*(i+1) + 3.333*(i+1)*(i+1);
    sy[i] = 1.0;
  }

/*  mfr = nc; */
  mfr = 0;
  iafr = (int *) shMalloc((2*nc)*sizeof(int));
  phi = 0.0;
  dphi = (double *) shMalloc(nc*sizeof(double));
  cov = (double *) shMalloc(nc*nc*sizeof(double));
  std = (double *) shMalloc(nc*sizeof(double));
  w = (double *) shMalloc((nw)*sizeof(double));
  nerr = 0;

  smaDfunft(subf, k, m, n, nx, nc, x, y, sy, a, al, au, mode, eps, 
	    maxit, iprt, &mfr, iafr, &phi, dphi, cov, std, w, &nerr);

  for (i=0; i<nc; i++) {
    printf(" i=%3d  a[i]=%f  sig[i]=%g \n",i, a[i], std[i]);
  }

  if ( fabs((double)a[0]-1.111) > 0.0001 ) {
    printf("bad value for a[0]\n %lg" );
    return 1;
  }
  if (fabs(a[1]-2.222) > 0.0001) {
    printf("bad value for a[1]\n");
    return 1;
  }
  if (fabs(a[2]-3.333) > 0.0001) {
    printf("bad value for a[2]\n");
    return 1;
  }

    
   return 0;
}




