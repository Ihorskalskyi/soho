/*** 
  Simple test of C binding for linear equation solver dgefs
  solve AX=B
***/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "sdssmath.h"
#include "slatec.h"

#define debug 0

int main(void) {

int i, j, nVar = 3;
int nRow, nCol, itask, ind;
int *iwork;
int retcode = 0;
double **A, **Atrans;
double *Asingle,*AtransSingle;
double *B,*work;
double answer1[3] = { 1.5, -1.0, 1.0 };
double answer2[2] = { 0.5, -3.5 }; 
double delta;

/******************************************************************
First solve, A X = B, where

      | 2  0 -1 |  |x|    | 2 |
      | 6  5  3 |  |y|  = | 7 |
      | 2 -1  0 |  |z|    | 4 |

and the solutions is: ( 1.5, -1, 1)
*/

/* allocate space for double and single indexed version of A */
A = (double **) shMalloc( nVar * sizeof(double *) );
Asingle = shMalloc( nVar*nVar * sizeof(double) );
for ( i = 1; i < (nVar+1); i++ ) {
  A[i-1] = Asingle + (i-1)*nVar;
}

/* allocate space fo double and single indexed versions of A-transpose */
Atrans = (double **) shMalloc( nVar * sizeof(double *) );
AtransSingle = shMalloc( nVar*nVar * sizeof(double) );
for ( i = 1; i < (nVar+1); i++ ) {
  Atrans[i-1] = AtransSingle + (i-1)*nVar;
}

/* allocate space for B vector, and the two temporary vectors */
B     = shMalloc( nVar * sizeof(double) );
work  = shMalloc( nVar * sizeof(double) );
iwork = shMalloc( nVar * sizeof(int) );

/* create A */
A[0][0] =  2;
A[0][1] =  0;
A[0][2] = -1;
A[1][0] =  6;
A[1][1] =  5;
A[1][2] =  3;
A[2][0] =  2;
A[2][1] = -1;
A[2][2] =  0;

/* now create A transpose */
for ( i = 0; i < nVar; i++ ) {
  for ( j = 0; j < nVar; j++ ) {
    Atrans[j][i] = A[i][j];
  }
}

/* fill B vector */
B[0] = 2;
B[1] = 7;
B[2] = 4;

/* now call the function */
nRow = 3;
nCol = 3;
itask = 1;
ind = 0;

#if debug
printf("\ntst_dgefs.c: before first call\n");
for ( i = 0; i < nRow * nCol; i++ ) {
  printf("AtransSingle[%d]  %5.1f\n", i, AtransSingle[i]);
}
printf("\n");
for ( i = 0; i < nRow; i++ ) {
  printf("           B[%d]  %5.1f\n", i, B[i]);  
}
#endif

smaDGEFS( AtransSingle, nRow, nCol, B, itask, &ind, work, iwork);

/* check the answer */
for ( i = 0; i < nVar; i++ ) {
  delta = abs(B[i] - answer1[i]);
  if ( delta > 0.0001 ) { retcode = 1; }
}
if ( retcode == 1 ) {
  printf("Error: mistake in first test of tst_dgefs\n");
}

#if debug
printf("\ntst_dgefs.c\n");
printf("First test, expect B = ( 1.5, -1.0, 1.0 )\n            get    B = (");
for ( i = 0; i < nVar; i++ ) {
  printf("%f  ", B[i]);
}
printf(" )\n\n");
#endif


/******************************************************************
Second, solve, A X = B, where

      | 3  4 |  |x|   | -12.5 |
      | 1 -1 |  |y| = |   4.0 |
      | 2 -6 |        |  22.0 |


and the solutions is: ( 0.5, -3.5 )
*/

nRow = 3;
nCol = 2;

/* create new A array */
A[0][0] =  3;
A[0][1] =  4;
A[1][0] =  1;
A[1][1] = -1;
A[2][0] =  2;
A[2][1] = -6;

/* now create new A transpose */
for ( i = 0; i < nRow; i++ ) {
  for ( j = 0; j < nCol; j++ ) {
    Atrans[j][i] = A[i][j];
  }
}

/* fill new B vector */
B[0] = -12.5;
B[1] =   4.0;
B[2] = -20.0;

/* now call the function */
itask = 1;
ind = 0;

#if debug
printf("\ntst_dgefs.c: before second call\n");
for ( i = 0; i < nRow * nCol; i++ ) {
  printf("AtransSingle[%d]  %5.1f\n", i, AtransSingle[i]);
}
printf("\n");
for ( i = 0; i < nRow; i++ ) {
  printf("           B[%d]  %5.1f\n", i, B[i]);  
}
#endif

smaDGEFS( AtransSingle, nRow, nCol, B, itask, &ind, work, iwork);

/* check the answer */
for ( i = 0; i < nCol; i++ ) {
  delta = abs( B[i] - answer2[i] );
  if ( delta > 0.0001 ) { retcode = 2; } 
}
if ( retcode == 2 ) {
  printf("Error: mistake in second test of tst_dgefs\n");
}

#if debug
printf("Second test, expect B = ( 0.5, -3.5 )\n             get    B = (");
for ( i = 0; i < nCol; i++ ) {
  printf("%f  ", B[i]);
}
printf(" )\n\n");
#endif

/* free up stuff and quit */
shFree(A);
shFree(Asingle);
shFree(Atrans);
shFree(AtransSingle);
shFree(B);
shFree(iwork);
shFree(work);

return retcode;
}
