/* <AUTO>
   FILE: smaFft.c
<HTML>
Interface to FFT functions
</HTML>
</AUTO> */

#include <string.h>
#include <math.h>
#include <smaValues.h>
#include <float.h>
#include "sdssmath.h"

/*<AUTO EXTRACT>
  Replace a region with its FFT (or inverse) using Press algorithms

Return:  SH_SUCCESS or SH_GENERIC_ERROR if direction is not +1 or -1,
or if reg->type != FL32

</AUTO>*/
RET_CODE smRegFft(
		  REGION *rReg,	/* the real part of the region to transform, 
				   of type FL32 */
		  REGION *iReg, /* the imaginary part of the region to
				   transform, of type FL32 */
		  int direction	/* +1 for forward; 0 for backwards */
		  ) {
  int ifac[15];
  float *rIn;
  float *rOut;
  float *iIn;
  float *iOut;
  int irow, icol;
  
  /* sanity clauses */
  if ((direction!=+1) && (direction != 0)) {
    shErrStackPush("smRegFft:  direction must be +1 or 0");
    return SH_GENERIC_ERROR;
  }
  if (rReg->type != TYPE_FL32) {
    shErrStackPush("smRegFft:  I can only deal with FL32 regions");
    return SH_GENERIC_ERROR;
  }  
  if (iReg->type != TYPE_FL32) {
    shErrStackPush("smRegFft:  I can only deal with FL32 regions");
    return SH_GENERIC_ERROR;
  }
  if ((rReg->nrow != iReg->nrow) || (rReg->ncol != iReg->ncol)) {
    shErrStackPush("smRegFft:  rReg and iReg have to be the same size");
    return SH_GENERIC_ERROR;
  }
  if ((!IsPowerOfTwo(rReg->nrow)) || (!IsPowerOfTwo(rReg->ncol))) {
    shErrStackPush("smRegFft:  nrow, ncol must be powers of two");
    return SH_GENERIC_ERROR;
  }

  /* FFT the rows */
  rOut = shMalloc(rReg->ncol * sizeof(float));
  iOut = shMalloc(rReg->ncol * sizeof(float));
  for (irow=0; irow<rReg->nrow; irow++) {
    smFftFloat(rReg->ncol, direction, 
	      rReg->rows_fl32[irow], iReg->rows_fl32[irow], rOut, iOut); 
    for (icol=0; icol<rReg->ncol; icol++) {
      rReg->rows_fl32[irow][icol] = rOut[icol];
      iReg->rows_fl32[irow][icol] = iOut[icol];
    }
  }
  shFree(rOut);
  shFree(iOut);

  /* FFT the cols */
  rIn  = shMalloc(rReg->nrow * sizeof(float));
  rOut = shMalloc(rReg->nrow * sizeof(float));
  iIn  = shMalloc(rReg->nrow * sizeof(float));
  iOut = shMalloc(rReg->nrow * sizeof(float));
  for (icol=0; icol<rReg->ncol; icol++) {
    for (irow=0; irow<rReg->nrow; irow++) {
      rIn[irow] = rReg->rows_fl32[irow][icol];
      iIn[irow] = iReg->rows_fl32[irow][icol];
    }
    smFftFloat(rReg->ncol, direction, rIn, iIn, rOut, iOut); 
    for (irow=0; irow < rReg->nrow; irow++) {
      rReg->rows_fl32[irow][icol] = rOut[irow];
      iReg->rows_fl32[irow][icol] = iOut[irow];
    }
  }
  shFree(rIn);
  shFree(rOut);
  shFree(iIn);
  shFree(iOut);

  /* get out of here */
  return SH_SUCCESS;

}

/*<AUTO EXTRACT>
Replace rReg1,iReg1 with the element-by-element complex multiplication
of (rReg1,iReg1) * (rReg2,iReg2).

Return:  SH_SUCCESS unless the types are not FL32 or the sizes
are not all the same.
</AUTO>*/
RET_CODE smRegElementMult(
			  REGION *rReg1,	/* real region 1 */
			  REGION *iReg1,	/* imag region 1 */
			  REGION *rReg2,	/* real region 2 */
			  REGION *iReg2	        /* imag region 2 */
			  ) {
  int irow, icol;
  float r1, i1, r2, i2;
  if (
      (rReg1->type != TYPE_FL32) || 
      (iReg1->type != TYPE_FL32) ||
      (rReg2->type != TYPE_FL32) || 
      (iReg2->type != TYPE_FL32) ) {
    shErrStackPush("smRegElementMult:  I can only deal with FL32 regions");
    return SH_GENERIC_ERROR;
  }
  if (
      (rReg1->ncol != iReg1->ncol) || 
      (rReg1->ncol != rReg2->ncol) || 
      (rReg1->ncol != iReg2->ncol) || 
      (rReg1->nrow != iReg1->nrow) || 
      (rReg1->nrow != rReg2->nrow) || 
      (rReg1->nrow != iReg2->nrow) ) {
    shErrStackPush("smRegElementMult:  regions must be the same size");
    return SH_GENERIC_ERROR;
  }

  for (irow=0; irow < rReg1->nrow; irow++) {
    for (icol=0; icol < rReg1->ncol; icol++) {
      r1 = rReg1->rows_fl32[irow][icol];
      r2 = rReg2->rows_fl32[irow][icol];
      i1 = iReg1->rows_fl32[irow][icol];
      i2 = iReg2->rows_fl32[irow][icol];
      rReg1->rows_fl32[irow][icol] = r1*r2 - i1*i2;
      iReg1->rows_fl32[irow][icol] = r1*i2 + i1*r2;
    }
  }

  return SH_SUCCESS;

}



/*<AUTO EXTRACT>
Normalize and huffle around the values in the region to put them in 
wrap around form so Press et al. can work their magic.  Do this to a region
which contains the PSF centered before FFT convolution.  

Return:  SH_SUCCESS or SH_GENERIC_ERROR if nrow, ncol are not multiples
of two or if it is not of type FL32
</AUTO>*/
RET_CODE smRegWrapAround(
                          REGION *reg,   /* the region to wrap*/
			  float norm  /* nrow*ncol/flux in psf */
                          ) {
  int irow, icol, n;
  float *tempRow;
  float tempVal;
  float sum = 0.0;

  if (reg->type != TYPE_FL32) {
    shErrStackPush("smRegWrapAround:  I can only deal with FL32 regions");
    return SH_GENERIC_ERROR;
  }

  if ((reg->nrow%2 != 0) || (reg->ncol%2 != 0)) {
    shErrStackPush("smRegWrapAround:  nrow, ncol must be even");
    return SH_GENERIC_ERROR;
  }
  /* Wrap in the row direction */
  n = reg->nrow/2;
  for (irow=0; irow<n; irow++) {
    tempRow = reg->rows_fl32[irow];
    reg->rows_fl32[irow] = reg->rows_fl32[irow+n];
    reg->rows_fl32[irow+n] = tempRow;
  }

  /* Wrap in the column direction */
  n = reg->ncol/2;
  for (irow=0; irow < reg->nrow; irow++) {
    for (icol=0; icol < n; icol++) {
      tempVal = reg->rows_fl32[irow][icol];
      reg->rows_fl32[irow][icol] = reg->rows_fl32[irow][icol+n];
      reg->rows_fl32[irow][icol+n] = tempVal;
      sum += tempVal;
      sum += reg->rows_fl32[irow][icol];
    }
  }
  norm = reg->nrow*reg->ncol/sum;
  for (irow=0; irow < reg->nrow; irow++) {
    for (icol=0; icol < reg->ncol; icol++) {
      reg->rows_fl32[irow][icol] *= norm;
    }
  }
  return SH_SUCCESS;
}


/*============================================================================
       fourierf.c  -  Don Cross <dcross@intersrv.com>
       http://www.intersrv.com/~dcross/fft.html

       Contains definitions for doing Fourier transforms
       and inverse Fourier transforms.

       This module performs operations on arrays of 'float'.

============================================================================*/

/*<AUTO EXTRACT>
Calculate the FFT of the input array.

Return:  SH_SUCCES or SH_GENERIC_ERROR if there is something wrong
with the inputs.
</AUTO>*/
RET_CODE smFftFloat ( unsigned NumSamples, /* number of element in the array,
					      must be a power of 2 */
		      int    InverseTransform, /* 1 for forward; 0 for back */
		      float  *RealIn, /* real input array */
		      float  *ImagIn, /* imag input array */
		      float  *RealOut, /* real output array */ 
		      float  *ImagOut  /* imag output array */
		      ) {
   unsigned NumBits;    /* Number of bits needed to store indices */
   unsigned i, j, k, n;
   unsigned BlockSize, BlockEnd;

   double angle_numerator = 2.0 * M_PI;
   double delta_angle;
   double alpha, beta;  /* used in recurrence relation */
   double delta_ar;
   double tr, ti;     /* temp real, temp imaginary */
   double ar, ai;     /* angle vector real, angle vector imaginary */

   if ( !IsPowerOfTwo(NumSamples) ) {
     shErrStackPush("smFftFloat:  size must be a power of two");
     return SH_GENERIC_ERROR;
   }

   if ( InverseTransform )
   {
      angle_numerator = -angle_numerator;
   }

   if (RealIn == NULL) {
     shErrStackPush("smFftFloat:  RealIn is NULL");
     return SH_GENERIC_ERROR;
   }
   if (RealOut == NULL) {
     shErrStackPush("smFftFloat:  RealIn is NULL");
     return SH_GENERIC_ERROR;
   }
   if (ImagOut == NULL) {
     shErrStackPush("smFftFloat:  RealIn is NULL");
     return SH_GENERIC_ERROR;
   }

   NumBits = NumberOfBitsNeeded ( NumSamples );
   if (NumBits == 0) {
     return SH_GENERIC_ERROR;
   }
   /*
   **   Do simultaneous data copy and bit-reversal ordering into outputs...
   */

   for ( i=0; i < NumSamples; i++ )
   {
      j = ReverseBits ( i, NumBits );

      RealOut[j] = RealIn[i];
      ImagOut[j] = (ImagIn == NULL) ? 0.0 : ImagIn[i];
   }

   /*
   **   Do the FFT itself...
   */

   BlockEnd = 1;
   for ( BlockSize = 2; BlockSize <= NumSamples; BlockSize <<= 1 )
   {
      delta_angle = angle_numerator / (double)BlockSize;
      alpha = sin ( 0.5 * delta_angle );
      alpha = 2.0 * alpha * alpha;
      beta = sin ( delta_angle );

      for ( i=0; i < NumSamples; i += BlockSize )
      {
         ar = 1.0;   /* cos(0) */
         ai = 0.0;   /* sin(0) */

         for ( j=i, n=0; n < BlockEnd; j++, n++ )
         {
            k = j + BlockEnd;
            tr = ar*RealOut[k] - ai*ImagOut[k];
            ti = ar*ImagOut[k] + ai*RealOut[k];

            RealOut[k] = RealOut[j] - tr;
            ImagOut[k] = ImagOut[j] - ti;

            RealOut[j] += tr;
            ImagOut[j] += ti;

            delta_ar = alpha*ar + beta*ai;
            ai -= (alpha*ai - beta*ar);
            ar -= delta_ar;
         }
      }

      BlockEnd = BlockSize;
   }

   /*
   **   Need to normalize if inverse transform...
   */

   if ( InverseTransform )
   {
      double denom = (double)NumSamples;

      for ( i=0; i < NumSamples; i++ )
      {
         RealOut[i] /= denom;
         ImagOut[i] /= denom;
      }
   }
   return SH_SUCCESS;
}


#define TRUE  1
#define FALSE 0

#define BITS_PER_WORD   (sizeof(unsigned) * 8)


int IsPowerOfTwo ( unsigned x )
{
   unsigned i, y;

   for ( i=1, y=2; i < BITS_PER_WORD; i++, y<<=1 )
   {
      if ( x == y ) return TRUE;
   }

   return FALSE;
}


unsigned NumberOfBitsNeeded ( unsigned PowerOfTwo )
{
   unsigned i;

   if ( PowerOfTwo < 2 )
   {
     shErrStackPush("NumberOfBitsNeeded:  PowerOfTwo < 2");
     return 0;
   }

   for ( i=0; ; i++ )
   {
      if ( PowerOfTwo & (1 << i) )
      {
         return i;
      }
   }
}



unsigned ReverseBits ( unsigned index, unsigned NumBits )
{
   unsigned i, rev;

   for ( i=rev=0; i < NumBits; i++ )
   {
      rev = (rev << 1) | (index & 1);
      index >>= 1;
   }

   return rev;
}


double Index_to_frequency ( unsigned NumSamples, unsigned Index )
{
   if ( Index >= NumSamples )
   {
      return 0.0;
   }
   else if ( Index <= NumSamples/2 )
   {
      return (double)Index / (double)NumSamples;
   }
   else
   {
      return -(double)(NumSamples-Index) / (double)NumSamples;
   }
}


/*<AUTO EXTRACT>
  Replace a region with its FFT (or inverse) using FFTW algorithms

Return:  SH_SUCCESS or SH_GENERIC_ERROR if direction is not +1 or -1,
or if reg->type != FL32

</AUTO>*/
RET_CODE smRegFftw(
		   REGION *rReg, /* the real part of the region to transform, 
				   of type FL32 */
		   REGION *iReg, /* the imaginary part of the region to
				   transform, of type FL32 */
		   int direction	/* +1 for forward; 0 for backwards */
		  ) {
  int i, irow, icol;

  fftwnd_plan plan;
  FFTW_REAL *array;
  
  /* sanity clauses */
  if ((direction!=+1) && (direction != 0)) {
    shErrStackPush("smRegFft:  direction must be +1 or 0");
    return SH_GENERIC_ERROR;
  }
  if (rReg->type != TYPE_FL32) {
    shErrStackPush("smRegFft:  I can only deal with FL32 regions");
    return SH_GENERIC_ERROR;
  }  
  if (iReg->type != TYPE_FL32) {
    shErrStackPush("smRegFft:  I can only deal with FL32 regions");
    return SH_GENERIC_ERROR;
  }
  if ((rReg->nrow != iReg->nrow) || (rReg->ncol != iReg->ncol)) {
    shErrStackPush("smRegFft:  rReg and iReg have to be the same size");
    return SH_GENERIC_ERROR;
  }

  if (direction == 1) {
    plan = 
      fftw2d_create_plan(rReg->ncol, rReg->nrow, FFTW_FORWARD, FFTW_IN_PLACE);
  } else {
    plan = 
      fftw2d_create_plan(rReg->ncol, rReg->nrow, FFTW_BACKWARD, FFTW_IN_PLACE);
  }

  array = shMalloc(rReg->ncol*rReg->nrow*sizeof(FFTW_REAL)*2);
  i = 0;
  for (irow=0; irow<rReg->nrow; irow++) {
    for (icol=0; icol<rReg->ncol; icol++) {
      array[2*i    ] = rReg->rows_fl32[irow][icol];
      array[2*i + 1] = iReg->rows_fl32[irow][icol];
      i++;
    }
  }

  fftwnd(plan, 1, (FFTW_COMPLEX *) array, 1, 
	 0, (FFTW_COMPLEX *) NULL, 0, 0);

  i = 0;
  for (irow=0; irow<rReg->nrow; irow++) {
    for (icol=0; icol<rReg->ncol; icol++) {
      rReg->rows_fl32[irow][icol] = array[2*i];
      iReg->rows_fl32[irow][icol] = array[2*i + 1];
      i++;
    }
  }

  /* clean up and get out of here */
  shFree(array);
  fftwnd_destroy_plan(plan);
  fftw_forget_wisdom();
  return SH_SUCCESS;

}






