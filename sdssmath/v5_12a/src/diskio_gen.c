/*
 * This file was machine generated from ../include/smaFunc.h 
 * on Tue Sep 27 18:29:13 2005
 *
 * Don't even think about editing it by hand
 */
#include <stdio.h>
#include <string.h>
#include "dervish_msg_c.h"
#include "smaFunc.h"
#include "shCSchema.h"
#include "shCUtils.h"
#include "shCDiskio.h"
#include "prvt/utils_p.h"

#define NSIZE 100		/* size to read strings*/

#define _STRING(S) #S
#define STRING(S) _STRING(S)		/* convert a cpp value to a string*/

static SCHEMA_ELEM schema_elems0[] = { /* SMA_FUNC */
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems1[] = { /* SMA_METH */
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems2[] = { /* FUNC */
   { "kind", "char", 0, 0, 0, STRING(FUNC_LABEL_SIZE), 0, offsetof(FUNC,kind) },
   { "nVector", "int", 0, 0, 0, NULL, 0, offsetof(FUNC,nVector) },
   { "va", "VECTOR", 0, 2, 0, NULL, 0, offsetof(FUNC,va) },
   { "cov", "VECTOR", 0, 1, 0, NULL, 0, offsetof(FUNC,cov) },
   { "sigma", "VECTOR", 0, 1, 0, NULL, 0, offsetof(FUNC,sigma) },
   { "chi2", "double", 0, 0, 0, NULL, 0, offsetof(FUNC,chi2) },
   { "fa", "SMA_FUNC", 0, 0, 0, NULL, 0, offsetof(FUNC,fa) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems3[] = { /* METH */
   { "kind", "char", 0, 0, 0, STRING(FUNC_LABEL_SIZE), 0, offsetof(METH,kind) },
   { "ma", "SMA_METH", 0, 0, 0, NULL, 0, offsetof(METH,ma) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems4[] = { /* TYPE */
   { "", "TYPE", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems5[] = { /* CHAR */
   { "", "char", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems6[] = { /* UCHAR */
   { "", "unsigned char", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems7[] = { /* INT */
   { "", "int", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems8[] = { /* UINT */
   { "", "unsigned int", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems9[] = { /* SHORT */
   { "", "short", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems10[] = { /* USHORT */
   { "", "unsigned short", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems11[] = { /* LONG */
   { "", "long", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems12[] = { /* ULONG */
   { "", "unsigned long", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems13[] = { /* FLOAT */
   { "", "float", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems14[] = { /* DOUBLE */
   { "", "double", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems15[] = { /* LOGICAL */
   { "", "logical", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems16[] = { /* STR */
   { "", "char", 0, 1, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems17[] = { /* PTR */
   { "", "ptr", 0, 1, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems18[] = { /* FUNCTION */
   { "", "function", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems19[] = { /* SCHAR */
   { "", "signed char", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA schema[] = {
   { "SMA_FUNC", PRIM, sizeof(SMA_FUNC), 0, schema_elems18,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "SMA_METH", PRIM, sizeof(SMA_METH), 0, schema_elems18,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "FUNC", STRUCT, sizeof(FUNC), 7, schema_elems2,
     (void *(*)(void))smFuncNew,
     (void *(*)(void *))smFuncDel,
     (RET_CODE(*)(FILE *,void *))shDumpGenericRead,
     (RET_CODE(*)(FILE *,void *))shDumpGenericWrite,
     0,
   },
   { "METH", STRUCT, sizeof(METH), 2, schema_elems3,
     (void *(*)(void))smMethNew,
     (void *(*)(void *))smMethDel,
     (RET_CODE(*)(FILE *,void *))shDumpGenericRead,
     (RET_CODE(*)(FILE *,void *))shDumpGenericWrite,
     0,
   },
   { "TYPE", PRIM, sizeof(TYPE), 1, schema_elems4,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpTypeRead,
     (RET_CODE(*)(FILE *,void *))shDumpTypeWrite,
     3,
   },
   { "CHAR", PRIM, sizeof(char), 1, schema_elems5,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpCharRead,
     (RET_CODE(*)(FILE *,void *))shDumpCharWrite,
     3,
   },
   { "UCHAR", PRIM, sizeof(unsigned char), 1, schema_elems6,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpUcharRead,
     (RET_CODE(*)(FILE *,void *))shDumpUcharWrite,
     3,
   },
   { "INT", PRIM, sizeof(int), 1, schema_elems7,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpIntRead,
     (RET_CODE(*)(FILE *,void *))shDumpIntWrite,
     3,
   },
   { "UINT", PRIM, sizeof(unsigned int), 1, schema_elems8,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpUintRead,
     (RET_CODE(*)(FILE *,void *))shDumpUintWrite,
     3,
   },
   { "SHORT", PRIM, sizeof(short), 1, schema_elems9,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpShortRead,
     (RET_CODE(*)(FILE *,void *))shDumpShortWrite,
     3,
   },
   { "USHORT", PRIM, sizeof(unsigned short), 1, schema_elems10,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpUshortRead,
     (RET_CODE(*)(FILE *,void *))shDumpUshortWrite,
     3,
   },
   { "LONG", PRIM, sizeof(long), 1, schema_elems11,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpLongRead,
     (RET_CODE(*)(FILE *,void *))shDumpLongWrite,
     3,
   },
   { "ULONG", PRIM, sizeof(unsigned long), 1, schema_elems12,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpUlongRead,
     (RET_CODE(*)(FILE *,void *))shDumpUlongWrite,
     3,
   },
   { "FLOAT", PRIM, sizeof(float), 1, schema_elems13,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpFloatRead,
     (RET_CODE(*)(FILE *,void *))shDumpFloatWrite,
     3,
   },
   { "DOUBLE", PRIM, sizeof(double), 1, schema_elems14,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpDoubleRead,
     (RET_CODE(*)(FILE *,void *))shDumpDoubleWrite,
     3,
   },
   { "LOGICAL", PRIM, sizeof(unsigned char), 1, schema_elems15,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpLogicalRead,
     (RET_CODE(*)(FILE *,void *))shDumpLogicalWrite,
     3,
   },
   { "STR", PRIM, sizeof(char *), 1, schema_elems16,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpStrRead,
     (RET_CODE(*)(FILE *,void *))shDumpStrWrite,
     3,
   },
   { "PTR", PRIM, sizeof(void *), 1, schema_elems17,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpPtrRead,
     (RET_CODE(*)(FILE *,void *))shDumpPtrWrite,
     3,
   },
   { "FUNCTION", PRIM, 0, 1, schema_elems18,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     3,
   },
   { "SCHAR", PRIM, sizeof(signed char), 1, schema_elems19,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpScharRead,
     (RET_CODE(*)(FILE *,void *))shDumpScharWrite,
     3,
   },
   { "", UNKNOWN, 0, 0,  NULL, NULL, NULL, NULL, NULL, SCHEMA_LOCK},
};

RET_CODE
smSchemaLoadFromSdssmath(void)
{
   return(p_shSchemaLoad(schema));
}

