/* WARNING -- this file was generated automatically */
/* by sdssmath/etc/makeTclBindings.pl */
/* <AUTO>
FILE: tcl_slatec.c
Bindings for SLATEC
</AUTO>
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include "dervish.h"
#include "sdssmath.h"
static char *tclFacil  = "slatec";
/*********************************************************
**
** ROUTINE tcl_d1mach 
**
**<AUTO EXTRACT>
** TCL VERB: d1mach
**</AUTO>
*/
char g_d1mach[] = "d1mach";
ftclArgvInfo g_d1machTbl[] = {
{NULL, FTCL_ARGV_HELP, NULL, NULL,
"Return floating point machine dependent constants -- double precision (double)\n"
"\n"
"   Assume double precision numbers are represented in the T-digit,\n"
"   base-B form\n"
"\n"
"              sign (B**E)*( (X(1)/B) + ... + (X(T)/B**T) )\n"
"\n"
"   where 0 .LE. X(I) .LT. B for I=1,...,T, 0 .LT. X(1), and\n"
"   EMIN .LE. E .LE. EMAX.\n"
"\n"
"   The values of B, T, EMIN and EMAX are provided in I1MACH as\n"
"\n"
"	i returns\n"
"	1 B**(EMIN-1), the smallest positive magnitude.\n"
"	2 B**EMAX*(1 - B**(-T)), the largest magnitude.\n"
"	3 B**(-T), the smallest relative spacing.\n"
"	4 B**(1-T), the largest relative spacing.\n"
"	5 LOG10(B)\n"
"Returns the value requested by the input paramter\n"
},
{"<i>", FTCL_ARGV_INT,NULL, NULL, "input parameter"},
{(char *) NULL, FTCL_ARGV_END, NULL, NULL, (char *) NULL}
};
static int
tcl_d1mach(ClientData clientData, 
Tcl_Interp *interp, int argc, char **argv) {
int rstat;
char tempString[100];
int i = 0; 
double con;
g_d1machTbl[1].dst = &i;
if ((rstat = shTclParseArgv(interp, &argc, argv,g_d1machTbl, FTCL_ARGV_NO_LEFTOVERS, g_d1mach))!= FTCL_ARGV_SUCCESS) return rstat;
con = smaD1MACH(i);
sprintf(tempString, "%g", con);
Tcl_AppendElement(interp, tempString);
return TCL_OK;
}
/*********************************************************
**
** ROUTINE tcl_dbint4 
**
**<AUTO EXTRACT>
** TCL VERB: dbint4
**</AUTO>
*/
char g_dbint4[] = "dbint4";
ftclArgvInfo g_dbint4Tbl[] = {
{NULL, FTCL_ARGV_HELP, NULL, NULL,
"    dbint computes the B representation (T,BCOEF,N,K) of a\n"
"    cubic spline (K=4) which interpolates data (X(I)),Y(I))),\n"
"    I=1,NDATA.\n"
"Returns a FUNC of type bspline.\n"
},
{"<x>", FTCL_ARGV_STRING,NULL, NULL, "vector of abcissae of length ndata, distinct and in increasing order"},
{"<y>", FTCL_ARGV_STRING,NULL, NULL, "vector of ordinates length ndata."},
{"-ibcl", FTCL_ARGV_INT,NULL, NULL, "selection parameter for left boundary condition; 1 constrains first derivative to fbcl; 2 constrains second derivative to fbcl"},
{"-ibcr", FTCL_ARGV_INT,NULL, NULL, "selection parameter for right boundary condition; 1 constrains first derivative to fbcl; 2 constrains second derivative to fbcl"},
{"-fbcl", FTCL_ARGV_DOUBLE,NULL, NULL, "left boundary value governed by ibcl"},
{"-fbcr", FTCL_ARGV_DOUBLE,NULL, NULL, "right boundary value governed by ibcl"},
{"-kntopt", FTCL_ARGV_INT,NULL, NULL, "knot selection parameter; 1 sets knot multiplicity at t[3] and t[n] to 4; 2 sets a symmetric placement of knots about t[3] and t[n]; 3 sets knots based on w set by user"},
{"<t>", FTCL_ARGV_STRING,NULL, NULL, "knot array of length ndata+6"},
{"<w>", FTCL_ARGV_STRING,NULL, NULL, "work array of at length at least 5*(ndata+2); if kntopt=3 then w[0] w[1] w[2] are knot values to the left of x[0]; w[3] w[4] w[5] are to the right, in increasing order supplied by the user."},
{(char *) NULL, FTCL_ARGV_END, NULL, NULL, (char *) NULL}
};
static int
tcl_dbint4(ClientData clientData, 
Tcl_Interp *interp, int argc, char **argv) {
int rstat;
char tempString[100];
VECTOR* x = NULL; 
char* handle_x = NULL;
VECTOR* y = NULL; 
char* handle_y = NULL;
int ibcl = 2; 
int ibcr = 2; 
double fbcl = 0; 
double fbcr = 0; 
int kntopt = 1; 
VECTOR* t = NULL; 
char* handle_t = NULL;
VECTOR* w = NULL; 
char* handle_w = NULL;
int n,k;
VECTOR **va;
char handle_func[HANDLE_NAMELEN];
FUNC* func = NULL;
g_dbint4Tbl[1].dst = &handle_x;
g_dbint4Tbl[2].dst = &handle_y;
g_dbint4Tbl[3].dst = &ibcl;
g_dbint4Tbl[4].dst = &ibcr;
g_dbint4Tbl[5].dst = &fbcl;
g_dbint4Tbl[6].dst = &fbcr;
g_dbint4Tbl[7].dst = &kntopt;
g_dbint4Tbl[8].dst = &handle_t;
g_dbint4Tbl[9].dst = &handle_w;
if ((rstat = shTclParseArgv(interp, &argc, argv,g_dbint4Tbl, FTCL_ARGV_NO_LEFTOVERS, g_dbint4))!= FTCL_ARGV_SUCCESS) return rstat;
if (handle_x==NULL) {
x = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_x,
(void **) &x, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for x",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_y==NULL) {
y = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_y,
(void **) &y, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for y",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_t==NULL) {
t = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_t,
(void **) &t, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for t",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_w==NULL) {
w = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_w,
(void **) &w, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for w",
TCL_VOLATILE); return TCL_ERROR;}
}
va = (VECTOR **) shMalloc(6*sizeof(VECTOR *));

va[1] = shVectorNew(x->dimen+2);

smaDBINT4(x->vec, y->vec, x->dimen, ibcl, ibcr, fbcl, fbcr, kntopt, t->vec, va[1]->vec, &n, &k, w->vec);

printf("after call to smaDBINT2:  n=%d k=%d\n", n, k);

va[0] = t;
va[2] = shVectorNew(1); va[2]->vec[0] = k;
va[3] = shVectorNew(1); va[3]->vec[0] = 0;
va[4] = shVectorNew(1); va[4]->vec[0] = 1;
va[5] = shVectorNew(3*k);
func = smFuncNew("bspline", va, 6);
if (shTclHandleNew(interp, handle_func, "FUNC", func)
!= TCL_OK) { Tcl_SetResult(interp, 
"Error getting func handle",
TCL_VOLATILE); return TCL_ERROR;}
sprintf(tempString, "%s", handle_func);
Tcl_AppendElement(interp, tempString);
return TCL_OK;
}
/*********************************************************
**
** ROUTINE tcl_dbintk 
**
**<AUTO EXTRACT>
** TCL VERB: dbintk
**</AUTO>
*/
char g_dbintk[] = "dbintk";
ftclArgvInfo g_dbintkTbl[] = {
{NULL, FTCL_ARGV_HELP, NULL, NULL,
"    dbint computes the B representation (T,BCOEF,N,K) of a\n"
"    spline which interpolates data (X(I)),Y(I))),\n"
"    I=1,NDATA.\n"
"Returns a FUNC of type bspline.\n"
},
{"<x>", FTCL_ARGV_STRING,NULL, NULL, "vector of abcissae of length n, distinct"},
{"<y>", FTCL_ARGV_STRING,NULL, NULL, "vector of ordinates length n"},
{"-k", FTCL_ARGV_INT,NULL, NULL, "order of the spline (k=4 is cubic)"},
{(char *) NULL, FTCL_ARGV_END, NULL, NULL, (char *) NULL}
};
static int
tcl_dbintk(ClientData clientData, 
Tcl_Interp *interp, int argc, char **argv) {
int rstat;
char tempString[100];
VECTOR* x = NULL; 
char* handle_x = NULL;
VECTOR* y = NULL; 
char* handle_y = NULL;
int k = 4; 
int i,n;
int *iperm = NULL;
int ier;
VECTOR **va;
VECTOR *xSort = NULL;
VECTOR *ySort = NULL;
VECTOR *t = NULL;
double *q = NULL;
double *w = NULL;

char handle_func[HANDLE_NAMELEN];
FUNC* func = NULL;
g_dbintkTbl[1].dst = &handle_x;
g_dbintkTbl[2].dst = &handle_y;
g_dbintkTbl[3].dst = &k;
if ((rstat = shTclParseArgv(interp, &argc, argv,g_dbintkTbl, FTCL_ARGV_NO_LEFTOVERS, g_dbintk))!= FTCL_ARGV_SUCCESS) return rstat;
if (handle_x==NULL) {
x = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_x,
(void **) &x, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for x",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_y==NULL) {
y = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_y,
(void **) &y, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for y",
TCL_VOLATILE); return TCL_ERROR;}
}
/* create xSort, ySort, and the knot array */
iperm = shMalloc(x->dimen*sizeof(int));
smaDPSORT(x->vec, x->dimen, iperm, 1, &ier);
xSort = shVectorNew(x->dimen);
ySort = shVectorNew(x->dimen);
for (i=0; i<x->dimen; i++) {
	xSort->vec[i] = x->vec[iperm[i]];
	ySort->vec[i] = y->vec[iperm[i]];
}
t = shVectorNew(x->dimen+k);

/* do something clever here to FILL the t array */


/* allocate the space we will need */
va = (VECTOR **) shMalloc(6*sizeof(VECTOR *));
va[1] = shVectorNew(x->dimen);
q = shMalloc( (2*k-1)*x->dimen*sizeof(double) );
w = shMalloc( 2*k*sizeof(double) );

/* do the actual call */
smaDBINTK(xSort->vec, ySort->vec, t->vec, xSort->dimen, k, va[1]->vec, q, w);

/* put the answers where they belong */
va[0] = t;
va[2] = shVectorNew(1); va[2]->vec[0] = k;
va[3] = shVectorNew(1); va[3]->vec[0] = 0;
va[4] = shVectorNew(1); va[4]->vec[0] = 1;
va[5] = shVectorNew(3*k);
func = smFuncNew("bspline", va, 6);

/* carefully clean up */
shVectorDel(xSort);
shVectorDel(ySort);
shFree(q);
shFree(w);

if (shTclHandleNew(interp, handle_func, "FUNC", func)
!= TCL_OK) { Tcl_SetResult(interp, 
"Error getting func handle",
TCL_VOLATILE); return TCL_ERROR;}
sprintf(tempString, "%s", handle_func);
Tcl_AppendElement(interp, tempString);
return TCL_OK;
}
/*********************************************************
**
** ROUTINE tcl_dbvalu 
**
**<AUTO EXTRACT>
** TCL VERB: dbvalu
**</AUTO>
*/
char g_dbvalu[] = "dbvalu";
ftclArgvInfo g_dbvaluTbl[] = {
{NULL, FTCL_ARGV_HELP, NULL, NULL,
"         DBVALU evaluates the B-representation (T,A,N,K) of a B-spline\n"
"         at X for the function value on IDERIV=0 or any of its\n"
"         derivatives on IDERIV=1,2,...,K-1.  Right limiting values\n"
"         (right derivatives) are returned except at the right end\n"
"         point X=T[N] where left limiting values are computed.  The\n"
"         spline is defined on T[K] .LE. X .LE. T[N].  DBVALU returns\n"
"         a fatal error message when X is outside of this interval.\n"
"\n"
"         To compute left derivatives or left limiting values at a\n"
"         knot T[I], replace N by I-1 and set X=T[I], I=K,N.\n"
"\n"
"Keyed list of dbvalue (the value of the spline evaluated at x) and inbv, which must be entered as input for efficient processing after the initial call.  Distinct splines require distinct inbv parameters.\n"
},
{"<t>", FTCL_ARGV_STRING,NULL, NULL, "knot array of length n+4"},
{"<a>", FTCL_ARGV_STRING,NULL, NULL, "B-spline coefficient vector of length n"},
{"<k>", FTCL_ARGV_INT,NULL, NULL, "order of the B-spline, k >= 1"},
{"<ideriv>", FTCL_ARGV_INT,NULL, NULL, "order of the derivative 0 <= ideriv <= k-1."},
{"<x>", FTCL_ARGV_DOUBLE,NULL, NULL, "argument, t[k-1] <= x <= t[n]"},
{"-inbv", FTCL_ARGV_INT,NULL, NULL, "initialization that must be set to 1 the first time DBVALU is called.  Its return value is used as input on subsequent calls with the same spline for efficient processing."},
{"<work>", FTCL_ARGV_STRING,NULL, NULL, "work vector of length 3*k"},
{(char *) NULL, FTCL_ARGV_END, NULL, NULL, (char *) NULL}
};
static int
tcl_dbvalu(ClientData clientData, 
Tcl_Interp *interp, int argc, char **argv) {
int rstat;
char tempString[100];
VECTOR* t = NULL; 
char* handle_t = NULL;
VECTOR* a = NULL; 
char* handle_a = NULL;
int k = 0.0; 
int ideriv = 0; 
double x = 0; 
int inbv = 1; 
VECTOR* work = NULL; 
char* handle_work = NULL;
double dbvalu;
g_dbvaluTbl[1].dst = &handle_t;
g_dbvaluTbl[2].dst = &handle_a;
g_dbvaluTbl[3].dst = &k;
g_dbvaluTbl[4].dst = &ideriv;
g_dbvaluTbl[5].dst = &x;
g_dbvaluTbl[6].dst = &inbv;
g_dbvaluTbl[7].dst = &handle_work;
if ((rstat = shTclParseArgv(interp, &argc, argv,g_dbvaluTbl, FTCL_ARGV_NO_LEFTOVERS, g_dbvalu))!= FTCL_ARGV_SUCCESS) return rstat;
if (handle_t==NULL) {
t = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_t,
(void **) &t, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for t",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_a==NULL) {
a = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_a,
(void **) &a, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for a",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_work==NULL) {
work = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_work,
(void **) &work, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for work",
TCL_VOLATILE); return TCL_ERROR;}
}
dbvalu = smaDBVALU(t->vec, a->vec, a->dimen, k, ideriv, x, &inbv, work->vec);
sprintf(tempString, "dbvalu %g", dbvalu);
Tcl_AppendElement(interp, tempString);
sprintf(tempString, "inbv %d", inbv);
Tcl_AppendElement(interp, tempString);
return TCL_OK;
}
/*********************************************************
**
** ROUTINE tcl_defc 
**
**<AUTO EXTRACT>
** TCL VERB: defc
**</AUTO>
*/
char g_defc[] = "defc";
ftclArgvInfo g_defcTbl[] = {
{NULL, FTCL_ARGV_HELP, NULL, NULL,
"  Fit a piecewise polynomial curve to discrete data.\n"
"  The piecewise polynomials are represented as B-splines.\n"
"  The fitting is done in a weighted least squares sense.\n"
"  A FUNC of the type bspline\n"
},
{"<x>", FTCL_ARGV_STRING,NULL, NULL, "vector of abcissae"},
{"<y>", FTCL_ARGV_STRING,NULL, NULL, "vector of ordinates"},
{"<sd>", FTCL_ARGV_STRING,NULL, NULL, "y standard deviation.  0 in any entry will weight that data point as 1."},
{"<nord>", FTCL_ARGV_INT,NULL, NULL, "order of the spline,  4 for cubic spline.  Keep it between (1 .le. nord .le. 20) please."},
{"<bkpt>", FTCL_ARGV_STRING,NULL, NULL, "Break points"},
{(char *) NULL, FTCL_ARGV_END, NULL, NULL, (char *) NULL}
};
static int
tcl_defc(ClientData clientData, 
Tcl_Interp *interp, int argc, char **argv) {
int rstat;
char tempString[100];
VECTOR* x = NULL; 
char* handle_x = NULL;
VECTOR* y = NULL; 
char* handle_y = NULL;
VECTOR* sd = NULL; 
char* handle_sd = NULL;
int nord = 4; 
VECTOR* bkpt = NULL; 
char* handle_bkpt = NULL;
int ndata, nbkpt;
int mdein=1;
int mdeout=0;
int maxval;
int lw;
double *w;
VECTOR **va;
char handle_func[HANDLE_NAMELEN];
FUNC* func = NULL;
g_defcTbl[1].dst = &handle_x;
g_defcTbl[2].dst = &handle_y;
g_defcTbl[3].dst = &handle_sd;
g_defcTbl[4].dst = &nord;
g_defcTbl[5].dst = &handle_bkpt;
if ((rstat = shTclParseArgv(interp, &argc, argv,g_defcTbl, FTCL_ARGV_NO_LEFTOVERS, g_defc))!= FTCL_ARGV_SUCCESS) return rstat;
if (handle_x==NULL) {
x = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_x,
(void **) &x, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for x",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_y==NULL) {
y = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_y,
(void **) &y, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for y",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_sd==NULL) {
sd = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_sd,
(void **) &sd, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for sd",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_bkpt==NULL) {
bkpt = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_bkpt,
(void **) &bkpt, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for bkpt",
TCL_VOLATILE); return TCL_ERROR;}
}

if ( (nord < 1) || (nord > 20)) {	
	Tcl_SetResult(interp, "nord must be 1 <= nord <= 20", TCL_VOLATILE);
	return TCL_ERROR;
}

ndata = x->dimen;
if ( (ndata !=y->dimen) || (ndata != sd->dimen) ) {
	Tcl_SetResult(interp, "x, y, and sd must be same size", TCL_VOLATILE);
	return TCL_ERROR;
}

nbkpt = bkpt->dimen;
if (nbkpt < 2*nord) {
	Tcl_SetResult(interp, "dimension of bkpt must be > 2*nord", 
	TCL_VOLATILE);
	return TCL_ERROR;
}

maxval= (ndata > nbkpt) ? ndata : nbkpt;
lw = (nbkpt-nord+3)*(nord+1)+
	(nbkpt+1)*(nord+1)+
	2*maxval+nbkpt+pow(nord,2);
w = shMalloc(lw*sizeof(double));

va = (VECTOR **) shMalloc (6*sizeof(VECTOR *));

va[1] = shVectorNew(nbkpt-nord);

smaDEFC(ndata, x->vec, y->vec, sd->vec, nord, nbkpt, bkpt->vec, mdein,
	&mdeout, va[1]->vec, lw, w);

if (mdeout == -1) {
	shVectorDel(va[1]);
	shFree(va);
	Tcl_SetResult(interp, "usage error of DEFC", TCL_VOLATILE);
	return TCL_ERROR;
}

if (mdeout == 2) {
	shVectorDel(va[1]);
	shFree(va);	
	Tcl_SetResult(interp, "Not enough data has been processed", 
	TCL_VOLATILE);
	return TCL_ERROR;
}

va[0] = bkpt;
va[2] = shVectorNew(1); va[2]->vec[0] = nord;
va[3] = shVectorNew(1); va[3]->vec[0] = 0;
va[4] = shVectorNew(1); va[4]->vec[0] = 1;
va[5] = shVectorNew(3*nord);
func = smFuncNew("bspline", va, 6);
shFree(w);
if (shTclHandleNew(interp, handle_func, "FUNC", func)
!= TCL_OK) { Tcl_SetResult(interp, 
"Error getting func handle",
TCL_VOLATILE); return TCL_ERROR;}
sprintf(tempString, "%s", handle_func);
Tcl_AppendElement(interp, tempString);
return TCL_OK;
}
/*********************************************************
**
** ROUTINE tcl_dgefs 
**
**<AUTO EXTRACT>
** TCL VERB: dgefs
**</AUTO>
*/
char g_dgefs[] = "dgefs";
ftclArgvInfo g_dgefsTbl[] = {
{NULL, FTCL_ARGV_HELP, NULL, NULL,
"Subroutine DGEFS solves a general NxN system of double\n"
"precision linear equations using LINPACK subroutines DGECO\n"
"and DGESL.  That is, if A is an NxN double precision matrix\n"
"and if X and B are double precision N-vectors, then DGEFS\n"
"solves the equation\n"
"                        A*X=B.\n"
"The matrix A is first factored into upper and lower tri-\n"
"angular matrices U and L using partial pivoting.  These\n"
"factors and the pivoting information are used to find the\n"
"solution vector X.  An approximate condition number is\n"
"calculated to provide a rough estimate of the number of\n"
"digits of accuracy in the computed solution.\n"
"If the equation A*X=B is to be solved for more than one vector\n"
"B, the factoring of A does not need to be performed again and\n"
"the option to only solve (ITASK.GT.1) will be faster for\n"
"the succeeding solutions.  In this case, the contents of A,\n"
"LDA, N and IWORK must not have been altered by the user follow-\n"
"ing factorization (ITASK=1).  IND will not be changed by DGEFS\n"
"in this case.\n"
"\n"
"ind, a rough estimate of the number of digits of accuracy in the solution x,\n"
"or an error code if ind < 0.\n"
},
{"<a>", FTCL_ARGV_STRING,NULL, NULL, "on entry, the elements of a 2-d array of dimension n,lda (c-convention) which contains the coefficient matrix.  On return, an upper triangular matrix U ant the multipliers mecessary to construct a matrix L so that A*L=V"},
{"<lda>", FTCL_ARGV_INT,NULL, NULL, "the leading dimension of the array A"},
{"<n>", FTCL_ARGV_INT,NULL, NULL, "the order of the matrix A.  The first n elements are the elements of the first column."},
{"<v>", FTCL_ARGV_STRING,NULL, NULL, "on entry, the singly subscripted vector of dimension n containing the right hand side B of A*X=B.  On return, v contains the solution vector x."},
{"<itask>", FTCL_ARGV_INT,NULL, NULL, "1 factor A and solve; >1 solve using existing a and iwork"},
{"<work>", FTCL_ARGV_STRING,NULL, NULL, "work space of at least dimension n"},
{"<iwork>", FTCL_ARGV_STRING,NULL, NULL, "work space of at least dimension n"},
{(char *) NULL, FTCL_ARGV_END, NULL, NULL, (char *) NULL}
};
static int
tcl_dgefs(ClientData clientData, 
Tcl_Interp *interp, int argc, char **argv) {
int rstat;
char tempString[100];
VECTOR* a = NULL; 
char* handle_a = NULL;
int lda = 0; 
int n = 0; 
VECTOR* v = NULL; 
char* handle_v = NULL;
int itask = 0; 
VECTOR* work = NULL; 
char* handle_work = NULL;
VECTOR* iwork = NULL; 
char* handle_iwork = NULL;
int *tempIwork;
int i,ind;
g_dgefsTbl[1].dst = &handle_a;
g_dgefsTbl[2].dst = &lda;
g_dgefsTbl[3].dst = &n;
g_dgefsTbl[4].dst = &handle_v;
g_dgefsTbl[5].dst = &itask;
g_dgefsTbl[6].dst = &handle_work;
g_dgefsTbl[7].dst = &handle_iwork;
if ((rstat = shTclParseArgv(interp, &argc, argv,g_dgefsTbl, FTCL_ARGV_NO_LEFTOVERS, g_dgefs))!= FTCL_ARGV_SUCCESS) return rstat;
if (handle_a==NULL) {
a = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_a,
(void **) &a, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for a",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_v==NULL) {
v = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_v,
(void **) &v, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for v",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_work==NULL) {
work = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_work,
(void **) &work, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for work",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_iwork==NULL) {
iwork = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_iwork,
(void **) &iwork, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for iwork",
TCL_VOLATILE); return TCL_ERROR;}
}
tempIwork=shMalloc(n*sizeof(int));
if (itask > 1) {
	for(i=0; i<n; i++) tempIwork[i]=(int)work->vec[i];
}
smaDGEFS(a->vec, lda,  n, v->vec, itask, &ind, work->vec, tempIwork);
for(i=0; i<n; i++) work->vec[i] = (double) tempIwork[i];
sprintf(tempString, "%d", ind);
Tcl_AppendElement(interp, tempString);
return TCL_OK;
}
/*********************************************************
**
** ROUTINE tcl_dp1vlu 
**
**<AUTO EXTRACT>
** TCL VERB: dp1vlu
**</AUTO>
*/
char g_dp1vlu[] = "dp1vlu";
ftclArgvInfo g_dp1vluTbl[] = {
{NULL, FTCL_ARGV_HELP, NULL, NULL,
"The subroutine  DP1VLU  uses the coefficients generated by  DPOLFT\n"
"to evaluate the polynomial fit of degree  L , along with the first\n"
"NDER  of its derivatives, at a specified point.  Computationally\n"
"stable recurrence relations are used to perform this task.\n"
"\n"
"The value of the fitting polynomial of degree l at x.  Also fills in yp.\n"
},
{"<l>", FTCL_ARGV_INT,NULL, NULL, "degree of the polynomial to be evaluated, less than or equal to ndeg provided by dpolft"},
{"<nder>", FTCL_ARGV_INT,NULL, NULL, "number of derivatives to be calculated"},
{"<x>", FTCL_ARGV_DOUBLE,NULL, NULL, "location to evaluate the polynomial and its derivatives"},
{"<yp>", FTCL_ARGV_STRING,NULL, NULL, "vector to be filled with nder derivatives"},
{"<func>", FTCL_ARGV_STRING,NULL, NULL, "FUNC from dpolft"},
{(char *) NULL, FTCL_ARGV_END, NULL, NULL, (char *) NULL}
};
static int
tcl_dp1vlu(ClientData clientData, 
Tcl_Interp *interp, int argc, char **argv) {
int rstat;
char tempString[100];
int l = 0; 
int nder = 0; 
double x = 0.0; 
VECTOR* yp = NULL; 
char* handle_yp = NULL;
FUNC* func = NULL; 
char* handle_func = NULL;
double yfit;
g_dp1vluTbl[1].dst = &l;
g_dp1vluTbl[2].dst = &nder;
g_dp1vluTbl[3].dst = &x;
g_dp1vluTbl[4].dst = &handle_yp;
g_dp1vluTbl[5].dst = &handle_func;
if ((rstat = shTclParseArgv(interp, &argc, argv,g_dp1vluTbl, FTCL_ARGV_NO_LEFTOVERS, g_dp1vlu))!= FTCL_ARGV_SUCCESS) return rstat;
if (handle_yp==NULL) {
yp = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_yp,
(void **) &yp, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for yp",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_func==NULL) {
func = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_func,
(void **) &func, "FUNC") != TCL_OK) {
Tcl_SetResult(interp, "Bad FUNC for func",
TCL_VOLATILE); return TCL_ERROR;}
}
smaDP1VLU(l, nder, x, &yfit, yp->vec, func->va[0]->vec);
sprintf(tempString, "%g", yfit);
Tcl_AppendElement(interp, tempString);
return TCL_OK;
}
/*********************************************************
**
** ROUTINE tcl_dpcoef 
**
**<AUTO EXTRACT>
** TCL VERB: dpcoef
**</AUTO>
*/
char g_dpcoef[] = "dpcoef";
ftclArgvInfo g_dpcoefTbl[] = {
{NULL, FTCL_ARGV_HELP, NULL, NULL,
"Return the taylor expansion of the results of dpolft around point c\n"
},
{"<func>", FTCL_ARGV_STRING,NULL, NULL, "The FUNC to evaluate"},
{"<l>", FTCL_ARGV_INT,NULL, NULL, "Degree of the Taylor expansion"},
{"-c", FTCL_ARGV_DOUBLE,NULL, NULL, "Center of the Taylor expansion"},
{(char *) NULL, FTCL_ARGV_END, NULL, NULL, (char *) NULL}
};
static int
tcl_dpcoef(ClientData clientData, 
Tcl_Interp *interp, int argc, char **argv) {
int rstat;
char tempString[100];
FUNC* func = NULL; 
char* handle_func = NULL;
int l = 0.0; 
double c = 0.0; 
int ndeg = 0;
char handle_out[HANDLE_NAMELEN];
VECTOR* out = NULL;
g_dpcoefTbl[1].dst = &handle_func;
g_dpcoefTbl[2].dst = &l;
g_dpcoefTbl[3].dst = &c;
if ((rstat = shTclParseArgv(interp, &argc, argv,g_dpcoefTbl, FTCL_ARGV_NO_LEFTOVERS, g_dpcoef))!= FTCL_ARGV_SUCCESS) return rstat;
if (handle_func==NULL) {
func = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_func,
(void **) &func, "FUNC") != TCL_OK) {
Tcl_SetResult(interp, "Bad FUNC for func",
TCL_VOLATILE); return TCL_ERROR;}
}
if (strncmp(func->kind,"dpol", 4) != 0) {
	Tcl_SetResult(interp, "dpcoef:  input func must be of type dpol", 
		TCL_VOLATILE);
	shTclInterpAppendWithErrStack(interp);
	return TCL_ERROR;
}
ndeg = func->va[1]->vec[0];
if (abs(l) > ndeg) {
	Tcl_SetResult(interp, "dpcoef:  abs(l) must be .LE. ndeg",
		TCL_VOLATILE);
	shTclInterpAppendWithErrStack(interp);
	return TCL_ERROR;
}
out = shVectorNew(abs(l)+1);
smaDPCOEF(l, c, out->vec, func->va[0]->vec);
if (shTclHandleNew(interp, handle_out, "VECTOR", out)
!= TCL_OK) { Tcl_SetResult(interp, 
"Error getting out handle",
TCL_VOLATILE); return TCL_ERROR;}
sprintf(tempString, "%s", handle_out);
Tcl_AppendElement(interp, tempString);
return TCL_OK;
}
/*********************************************************
**
** ROUTINE tcl_dpolft 
**
**<AUTO EXTRACT>
** TCL VERB: dpolft
**</AUTO>
*/
char g_dpolft[] = "dpolft";
ftclArgvInfo g_dpolftTbl[] = {
{NULL, FTCL_ARGV_HELP, NULL, NULL,
"Given a collection of points X(I) and a set of values Y(I) which\n"
"correspond to some function or measurement at each of the X(I),\n"
"subroutine  DPOLFT  computes the weighted least-squares polynomial\n"
"fits of all degrees up to some degree either specified by the user\n"
"or determined by the routine.  The fits thus obtained are in\n"
"orthogonal polynomial form.  Subroutine  DP1VLU  may then be\n"
"called to evaluate the fitted polynomials and any of their\n"
"derivatives at any point.  The subroutine  DPCOEF  may be used to\n"
"express the polynomial fits as powers of (X-C) for any specified\n"
"point C.\n"
"Returns a keyed list of ndeg (degree of the highest degree fit computed), eps (RMS error of the polynomial of degree ndeg), ierr (1 is normal), and r (vector containit the values of the fit of degree nfit at each x\n"
},
{"<x>", FTCL_ARGV_STRING,NULL, NULL, "vector of abcissae"},
{"<y>", FTCL_ARGV_STRING,NULL, NULL, "vector of ordinates"},
{"<w>", FTCL_ARGV_STRING,NULL, NULL, "vector of weights.  If w[0] is negative, all weights will be set to 1.0"},
{"<maxdeg>", FTCL_ARGV_INT,NULL, NULL, "maximum degree allowed for polynomial fit."},
{"-eps", FTCL_ARGV_DOUBLE,NULL, NULL, "if negative, use statistical F test to determine degree; if 0, compute all polynomials up to maxdeg; if positive, tolerence on RMS error"},
{(char *) NULL, FTCL_ARGV_END, NULL, NULL, (char *) NULL}
};
static int
tcl_dpolft(ClientData clientData, 
Tcl_Interp *interp, int argc, char **argv) {
int rstat;
char tempString[100];
VECTOR* x = NULL; 
char* handle_x = NULL;
VECTOR* y = NULL; 
char* handle_y = NULL;
VECTOR* w = NULL; 
char* handle_w = NULL;
int maxdeg = 0; 
double eps = 0; 
int ndeg, ierr, nValues;
VECTOR *a;
VECTOR **va;
char handle_r[HANDLE_NAMELEN];
VECTOR* r = NULL;
char handle_func[HANDLE_NAMELEN];
FUNC* func = NULL;
g_dpolftTbl[1].dst = &handle_x;
g_dpolftTbl[2].dst = &handle_y;
g_dpolftTbl[3].dst = &handle_w;
g_dpolftTbl[4].dst = &maxdeg;
g_dpolftTbl[5].dst = &eps;
if ((rstat = shTclParseArgv(interp, &argc, argv,g_dpolftTbl, FTCL_ARGV_NO_LEFTOVERS, g_dpolft))!= FTCL_ARGV_SUCCESS) return rstat;
if (handle_x==NULL) {
x = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_x,
(void **) &x, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for x",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_y==NULL) {
y = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_y,
(void **) &y, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for y",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_w==NULL) {
w = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_w,
(void **) &w, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for w",
TCL_VOLATILE); return TCL_ERROR;}
}

nValues = 3*x->dimen + 3*maxdeg + 3;
r = shVectorNew(x->dimen);
a = shVectorNew(nValues);
smaDPOLFT(x->dimen, x->vec, y->vec, w->vec, maxdeg, 
	&ndeg, &eps, r->vec, &ierr, a->vec);

va = (VECTOR **) shMalloc(2*sizeof(VECTOR *));
va[0] = a;
va[1] = shVectorNew(1); va[1]->vec[0] = ndeg;
func = smFuncNew("dpol", va, 2);
if (shTclHandleNew(interp, handle_r, "VECTOR", r)
!= TCL_OK) { Tcl_SetResult(interp, 
"Error getting r handle",
TCL_VOLATILE); return TCL_ERROR;}
if (shTclHandleNew(interp, handle_func, "FUNC", func)
!= TCL_OK) { Tcl_SetResult(interp, 
"Error getting func handle",
TCL_VOLATILE); return TCL_ERROR;}
sprintf(tempString, "ndeg %d", ndeg);
Tcl_AppendElement(interp, tempString);
sprintf(tempString, "eps %g", eps);
Tcl_AppendElement(interp, tempString);
sprintf(tempString, "ierr %d", ierr);
Tcl_AppendElement(interp, tempString);
sprintf(tempString, "r %s", handle_r);
Tcl_AppendElement(interp, tempString);
sprintf(tempString, "func %s", handle_func);
Tcl_AppendElement(interp, tempString);
return TCL_OK;
}
/*********************************************************
**
** ROUTINE tcl_vSort 
**
**<AUTO EXTRACT>
** TCL VERB: vSort
**</AUTO>
*/
char g_vSort[] = "vSort";
ftclArgvInfo g_vSortTbl[] = {
{NULL, FTCL_ARGV_HELP, NULL, NULL,
"	Return the permutation vector vPerm generated by sorting\n"
"	the vector vIn, and, optionally, rearrange the values in\n"
"	vIN.  Uses SLATEC subrouting dpsort\n"
"	The vector vPerm\n"
},
{"<vIn>", FTCL_ARGV_STRING,NULL, NULL, "The vector to be sorted"},
{"<kFlag>", FTCL_ARGV_INT,NULL, NULL, "2=increasing and sort vIn; 1=increasing and do not sort vIN; -1=decreasing and do not sort vIn; -2=decreasing and sort vIn"},
{(char *) NULL, FTCL_ARGV_END, NULL, NULL, (char *) NULL}
};
static int
tcl_vSort(ClientData clientData, 
Tcl_Interp *interp, int argc, char **argv) {
int rstat;
char tempString[100];
VECTOR* vIn = NULL; 
char* handle_vIn = NULL;
int kFlag = 2; 
int *iperm = NULL;
int ier = 0;
char handle_vPerm[HANDLE_NAMELEN];
VECTOR* vPerm = NULL;
g_vSortTbl[1].dst = &handle_vIn;
g_vSortTbl[2].dst = &kFlag;
if ((rstat = shTclParseArgv(interp, &argc, argv,g_vSortTbl, FTCL_ARGV_NO_LEFTOVERS, g_vSort))!= FTCL_ARGV_SUCCESS) return rstat;
if (handle_vIn==NULL) {
vIn = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_vIn,
(void **) &vIn, "VECTOR") != TCL_OK) {
Tcl_SetResult(interp, "Bad VECTOR for vIn",
TCL_VOLATILE); return TCL_ERROR;}
}
if ( (abs(kFlag) > 2) || kFlag==0) {
	Tcl_SetResult(interp, "vSort:  kFlag must be 2, 1, -1, or -2",
		TCL_VOLATILE);
	shTclInterpAppendWithErrStack(interp);
	return TCL_ERROR;
}
if (vIn->dimen <= 0) {
	Tcl_SetResult(interp, "vSort:  size of vIn must be > 0",
		TCL_VOLATILE);
	shTclInterpAppendWithErrStack(interp);
	return TCL_ERROR;
}
iperm = shMalloc(vIn->dimen*sizeof(int));
smaDPSORT(vIn->vec, vIn->dimen, iperm, kFlag, &ier);
vPerm = shVectorNew(vIn->dimen);
for (ier=0; ier<vIn->dimen; ier++) {
	vPerm->vec[ier] = iperm[ier]-1;
}
shFree(iperm);
if (shTclHandleNew(interp, handle_vPerm, "VECTOR", vPerm)
!= TCL_OK) { Tcl_SetResult(interp, 
"Error getting vPerm handle",
TCL_VOLATILE); return TCL_ERROR;}
sprintf(tempString, "%s", handle_vPerm);
Tcl_AppendElement(interp, tempString);
return TCL_OK;
}
/*********************************************************
**
** ROUTINE tcl_r1mach 
**
**<AUTO EXTRACT>
** TCL VERB: r1mach
**</AUTO>
*/
char g_r1mach[] = "r1mach";
ftclArgvInfo g_r1machTbl[] = {
{NULL, FTCL_ARGV_HELP, NULL, NULL,
"Return floating point machine dependent constants -- single precision (float)\n"
"\n"
"   Assume single precision numbers are represented in the T-digit,\n"
"   base-B form\n"
"\n"
"              sign (B**E)*( (X(1)/B) + ... + (X(T)/B**T) )\n"
"\n"
"   where 0 .LE. X(I) .LT. B for I=1,...,T, 0 .LT. X(1), and\n"
"   EMIN .LE. E .LE. EMAX.\n"
"\n"
"   The values of B, T, EMIN and EMAX are provided in I1MACH as\n"
"\n"
"	i returns\n"
"	1 B**(EMIN-1), the smallest positive magnitude.\n"
"	2 B**EMAX*(1 - B**(-T)), the largest magnitude.\n"
"	3 B**(-T), the smallest relative spacing.\n"
"	4 B**(1-T), the largest relative spacing.\n"
"	5 LOG10(B)\n"
"Returns the value requested by the input paramter\n"
},
{"<i>", FTCL_ARGV_INT,NULL, NULL, "input parameter"},
{(char *) NULL, FTCL_ARGV_END, NULL, NULL, (char *) NULL}
};
static int
tcl_r1mach(ClientData clientData, 
Tcl_Interp *interp, int argc, char **argv) {
int rstat;
char tempString[100];
int i = 0; 
float con;
g_r1machTbl[1].dst = &i;
if ((rstat = shTclParseArgv(interp, &argc, argv,g_r1machTbl, FTCL_ARGV_NO_LEFTOVERS, g_r1mach))!= FTCL_ARGV_SUCCESS) return rstat;
con = smaR1MACH(i);
sprintf(tempString, "%g", con);
Tcl_AppendElement(interp, tempString);
return TCL_OK;
}
/*********************************************************
**
** ROUTINE tcl_rand 
**
**<AUTO EXTRACT>
** TCL VERB: rand
**</AUTO>
*/
char g_rand[] = "rand";
ftclArgvInfo g_randTbl[] = {
{NULL, FTCL_ARGV_HELP, NULL, NULL,
"      This pseudo-random number generator is portable among a wide\n"
"variety of computers.  RAND undoubtedly is not as good as many\n"
"readily available installation dependent versions, and so this\n"
"routine is not recommended for widespread usage.  Its redeeming\n"
"feature is that the exact same random numbers (to within final round-\n"
"off error) can be generated from machine to machine.  Thus, programs\n"
"that make use of random numbers can be easily transported to and\n"
"checked in a new environment.\n"
"            Input Argument --\n"
"       If seed = 0., the next random number of the sequence is generated.\n"
"       If seed < 0., the last generated number will be returned for\n"
"         possible use in a restart procedure.\n"
"       If seed > 0., the sequence of random numbers will start with\n"
"         the seed mod 1.  This seed is also returned as the value of\n"
"         RAND provided the arithmetic is done exactly.\n"
"Returns a  pseudo-random number between 0. and 1.\n"
},
{"[seed]", FTCL_ARGV_DOUBLE,NULL, NULL, "input parameter"},
{(char *) NULL, FTCL_ARGV_END, NULL, NULL, (char *) NULL}
};
static int
tcl_rand(ClientData clientData, 
Tcl_Interp *interp, int argc, char **argv) {
int rstat;
char tempString[100];
double seed = 0; 
float random;
g_randTbl[1].dst = &seed;
if ((rstat = shTclParseArgv(interp, &argc, argv,g_randTbl, FTCL_ARGV_NO_LEFTOVERS, g_rand))!= FTCL_ARGV_SUCCESS) return rstat;
random = smaRAND((float) seed);
sprintf(tempString, "%11.9f", random);
Tcl_AppendElement(interp, tempString);
return TCL_OK;
}
/*********************************************************
**
** ROUTINE tcl_regFft 
**
**<AUTO EXTRACT>
** TCL VERB: regFft
**</AUTO>
*/
char g_regFft[] = "regFft";
ftclArgvInfo g_regFftTbl[] = {
{NULL, FTCL_ARGV_HELP, NULL, NULL,
"	replace the region with its fourier transform (or inverse fourier\n"
"	transform)\n"
"	error if the region is not FL32, is not square, or if direction\n"
"	is not +1 or -1\n"
},
{"<rReg>", FTCL_ARGV_STRING,NULL, NULL, "Real part of region to transform"},
{"<iReg>", FTCL_ARGV_STRING,NULL, NULL, "Imaginary part of region to transform"},
{"<direction>", FTCL_ARGV_INT,NULL, NULL, "+1 for fourier transform; 0 for inverse"},
{(char *) NULL, FTCL_ARGV_END, NULL, NULL, (char *) NULL}
};
static int
tcl_regFft(ClientData clientData, 
Tcl_Interp *interp, int argc, char **argv) {
int rstat;
REGION* rReg = NULL; 
char* handle_rReg = NULL;
REGION* iReg = NULL; 
char* handle_iReg = NULL;
int direction = 1; 
g_regFftTbl[1].dst = &handle_rReg;
g_regFftTbl[2].dst = &handle_iReg;
g_regFftTbl[3].dst = &direction;
if ((rstat = shTclParseArgv(interp, &argc, argv,g_regFftTbl, FTCL_ARGV_NO_LEFTOVERS, g_regFft))!= FTCL_ARGV_SUCCESS) return rstat;
if (handle_rReg==NULL) {
rReg = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_rReg,
(void **) &rReg, "REGION") != TCL_OK) {
Tcl_SetResult(interp, "Bad REGION for rReg",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_iReg==NULL) {
iReg = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_iReg,
(void **) &iReg, "REGION") != TCL_OK) {
Tcl_SetResult(interp, "Bad REGION for iReg",
TCL_VOLATILE); return TCL_ERROR;}
}
if (smRegFft(rReg, iReg, direction) != SH_SUCCESS) {
	Tcl_SetResult(interp, "error from smRegFft", TCL_VOLATILE);
	shTclInterpAppendWithErrStack(interp);
	return TCL_ERROR;
}
return TCL_OK;
}
/*********************************************************
**
** ROUTINE tcl_regFftw 
**
**<AUTO EXTRACT>
** TCL VERB: regFftw
**</AUTO>
*/
char g_regFftw[] = "regFftw";
ftclArgvInfo g_regFftwTbl[] = {
{NULL, FTCL_ARGV_HELP, NULL, NULL,
"	replace the region with its fourier transform (or inverse fourier\n"
"	transform) using the FFTW algorithms.\n"
"	error if the region is not FL32, is not square, or if direction\n"
"	is not +1 or -1\n"
},
{"<rReg>", FTCL_ARGV_STRING,NULL, NULL, "Real part of region to transform"},
{"<iReg>", FTCL_ARGV_STRING,NULL, NULL, "Imaginary part of region to transform"},
{"<direction>", FTCL_ARGV_INT,NULL, NULL, "+1 for fourier transform; 0 for inverse"},
{(char *) NULL, FTCL_ARGV_END, NULL, NULL, (char *) NULL}
};
static int
tcl_regFftw(ClientData clientData, 
Tcl_Interp *interp, int argc, char **argv) {
int rstat;
REGION* rReg = NULL; 
char* handle_rReg = NULL;
REGION* iReg = NULL; 
char* handle_iReg = NULL;
int direction = 1; 
g_regFftwTbl[1].dst = &handle_rReg;
g_regFftwTbl[2].dst = &handle_iReg;
g_regFftwTbl[3].dst = &direction;
if ((rstat = shTclParseArgv(interp, &argc, argv,g_regFftwTbl, FTCL_ARGV_NO_LEFTOVERS, g_regFftw))!= FTCL_ARGV_SUCCESS) return rstat;
if (handle_rReg==NULL) {
rReg = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_rReg,
(void **) &rReg, "REGION") != TCL_OK) {
Tcl_SetResult(interp, "Bad REGION for rReg",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_iReg==NULL) {
iReg = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_iReg,
(void **) &iReg, "REGION") != TCL_OK) {
Tcl_SetResult(interp, "Bad REGION for iReg",
TCL_VOLATILE); return TCL_ERROR;}
}
if (smRegFftw(rReg, iReg, direction) != SH_SUCCESS) {
	Tcl_SetResult(interp, "error from smRegFftw", TCL_VOLATILE);
	shTclInterpAppendWithErrStack(interp);
	return TCL_ERROR;
}

return TCL_OK;
}
/*********************************************************
**
** ROUTINE tcl_regElementMult 
**
**<AUTO EXTRACT>
** TCL VERB: regElementMult
**</AUTO>
*/
char g_regElementMult[] = "regElementMult";
ftclArgvInfo g_regElementMultTbl[] = {
{NULL, FTCL_ARGV_HELP, NULL, NULL,
"	replace reg1 with an element-by-element multiplication of reg1*reg2\n"
"	for FL32 regions only\n"
"	error if regions are not the same size or of type FL32\n"
},
{"<rReg1>", FTCL_ARGV_STRING,NULL, NULL, "real part of first region "},
{"<iReg1>", FTCL_ARGV_STRING,NULL, NULL, "imag part of first region "},
{"<rReg2>", FTCL_ARGV_STRING,NULL, NULL, "real part of second region "},
{"<iReg2>", FTCL_ARGV_STRING,NULL, NULL, "imag part of second region "},
{(char *) NULL, FTCL_ARGV_END, NULL, NULL, (char *) NULL}
};
static int
tcl_regElementMult(ClientData clientData, 
Tcl_Interp *interp, int argc, char **argv) {
int rstat;
REGION* rReg1 = NULL; 
char* handle_rReg1 = NULL;
REGION* iReg1 = NULL; 
char* handle_iReg1 = NULL;
REGION* rReg2 = NULL; 
char* handle_rReg2 = NULL;
REGION* iReg2 = NULL; 
char* handle_iReg2 = NULL;
g_regElementMultTbl[1].dst = &handle_rReg1;
g_regElementMultTbl[2].dst = &handle_iReg1;
g_regElementMultTbl[3].dst = &handle_rReg2;
g_regElementMultTbl[4].dst = &handle_iReg2;
if ((rstat = shTclParseArgv(interp, &argc, argv,g_regElementMultTbl, FTCL_ARGV_NO_LEFTOVERS, g_regElementMult))!= FTCL_ARGV_SUCCESS) return rstat;
if (handle_rReg1==NULL) {
rReg1 = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_rReg1,
(void **) &rReg1, "REGION") != TCL_OK) {
Tcl_SetResult(interp, "Bad REGION for rReg1",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_iReg1==NULL) {
iReg1 = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_iReg1,
(void **) &iReg1, "REGION") != TCL_OK) {
Tcl_SetResult(interp, "Bad REGION for iReg1",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_rReg2==NULL) {
rReg2 = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_rReg2,
(void **) &rReg2, "REGION") != TCL_OK) {
Tcl_SetResult(interp, "Bad REGION for rReg2",
TCL_VOLATILE); return TCL_ERROR;}
}
if (handle_iReg2==NULL) {
iReg2 = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_iReg2,
(void **) &iReg2, "REGION") != TCL_OK) {
Tcl_SetResult(interp, "Bad REGION for iReg2",
TCL_VOLATILE); return TCL_ERROR;}
}
if (smRegElementMult(rReg1, iReg1, rReg2, iReg2) != SH_SUCCESS) {
	Tcl_SetResult(interp, "error from smRegElementMult", TCL_VOLATILE);
	shTclInterpAppendWithErrStack(interp);
	return TCL_ERROR;
}
return TCL_OK;
}
/*********************************************************
**
** ROUTINE tcl_regWrapAround 
**
**<AUTO EXTRACT>
** TCL VERB: regWrapAround
**</AUTO>
*/
char g_regWrapAround[] = "regWrapAround";
ftclArgvInfo g_regWrapAroundTbl[] = {
{NULL, FTCL_ARGV_HELP, NULL, NULL,
"Shuffle around the values in the region to put them in wrap around\n"
"form so Press et al. can work their magic.  Do this to a region\n"
"which contains the PSF centered before FFT convolution.\n"
"\n"
"SH_SUCCESS or SH_GENERIC_ERROR if nrow, ncol are not multiples\n"
"of two or if it is not of type FL32\n"
"\n"
},
{"<reg>", FTCL_ARGV_STRING,NULL, NULL, "region to wrap around"},
{"<norm>", FTCL_ARGV_DOUBLE,NULL, NULL, "normalization  (nrow*ncol)/flux"},
{(char *) NULL, FTCL_ARGV_END, NULL, NULL, (char *) NULL}
};
static int
tcl_regWrapAround(ClientData clientData, 
Tcl_Interp *interp, int argc, char **argv) {
int rstat;
REGION* reg = NULL; 
char* handle_reg = NULL;
double norm = 1.0; 
g_regWrapAroundTbl[1].dst = &handle_reg;
g_regWrapAroundTbl[2].dst = &norm;
if ((rstat = shTclParseArgv(interp, &argc, argv,g_regWrapAroundTbl, FTCL_ARGV_NO_LEFTOVERS, g_regWrapAround))!= FTCL_ARGV_SUCCESS) return rstat;
if (handle_reg==NULL) {
reg = NULL;
} else {
if (shTclAddrGetFromName(interp, handle_reg,
(void **) &reg, "REGION") != TCL_OK) {
Tcl_SetResult(interp, "Bad REGION for reg",
TCL_VOLATILE); return TCL_ERROR;}
}
if (smRegWrapAround(reg, (float) norm) != SH_SUCCESS) {
	Tcl_SetResult(interp, "error from smRegFft", TCL_VOLATILE);
	shTclInterpAppendWithErrStack(interp);
	return TCL_ERROR;
}
return TCL_OK;
}
/*********************************************************
**
** ROUTINE tcl_xsetf 
**
**<AUTO EXTRACT>
** TCL VERB: xsetf
**</AUTO>
*/
char g_xsetf[] = "xsetf";
ftclArgvInfo g_xsetfTbl[] = {
{NULL, FTCL_ARGV_HELP, NULL, NULL,
"XSETF sets the error control flag value to KONTRL.\n"
"(KONTRL is an input parameter only.)\n"
"The following table shows how each message is treated,\n"
"depending on the values of KONTRL and LEVEL.  (See XERMSG\n"
"for description of LEVEL.)\n"
"\n"
"If KONTRL is zero or negative, no information other than the\n"
"message itself (including numeric values, if any) will be\n"
"printed.  If KONTRL is positive, introductory messages,\n"
"trace-backs, etc., will be printed in addition to the message.\n"
"\n"
"      ABS(KONTRL)\n"
"        LEVEL        0              1              2\n"
"        value\n"
"          2        fatal          fatal          fatal\n"
"\n"
"          1     not printed      printed         fatal\n"
"\n"
"          0     not printed      printed        printed\n"
"\n"
"         -1     not printed      printed        printed\n"
"                                  only           only\n"
"                                  once           once\n"
"The input parameter.\n"
},
{"<kontrl>", FTCL_ARGV_INT,NULL, NULL, "input parameter"},
{(char *) NULL, FTCL_ARGV_END, NULL, NULL, (char *) NULL}
};
static int
tcl_xsetf(ClientData clientData, 
Tcl_Interp *interp, int argc, char **argv) {
int rstat;
char tempString[100];
int kontrl = 0; 
g_xsetfTbl[1].dst = &kontrl;
if ((rstat = shTclParseArgv(interp, &argc, argv,g_xsetfTbl, FTCL_ARGV_NO_LEFTOVERS, g_xsetf))!= FTCL_ARGV_SUCCESS) return rstat;
smaXSETF(kontrl);
sprintf(tempString, "%d", kontrl);
Tcl_AppendElement(interp, tempString);
return TCL_OK;
}
/*******************************************************/
/*** declare these tcl verbs ***/
void tcl_slatec_declare(Tcl_Interp *interp) {
int flags = FTCL_ARGV_NO_LEFTOVERS;
shTclDeclare(interp, g_d1mach, (Tcl_CmdProc *) tcl_d1mach,
(ClientData) 0, (Tcl_CmdDeleteProc *)0, tclFacil,
shTclGetArgInfo(interp, g_d1machTbl, flags, g_d1mach),
shTclGetUsage(interp, g_d1machTbl, flags, g_d1mach));
shTclDeclare(interp, g_dbint4, (Tcl_CmdProc *) tcl_dbint4,
(ClientData) 0, (Tcl_CmdDeleteProc *)0, tclFacil,
shTclGetArgInfo(interp, g_dbint4Tbl, flags, g_dbint4),
shTclGetUsage(interp, g_dbint4Tbl, flags, g_dbint4));
shTclDeclare(interp, g_dbintk, (Tcl_CmdProc *) tcl_dbintk,
(ClientData) 0, (Tcl_CmdDeleteProc *)0, tclFacil,
shTclGetArgInfo(interp, g_dbintkTbl, flags, g_dbintk),
shTclGetUsage(interp, g_dbintkTbl, flags, g_dbintk));
shTclDeclare(interp, g_dbvalu, (Tcl_CmdProc *) tcl_dbvalu,
(ClientData) 0, (Tcl_CmdDeleteProc *)0, tclFacil,
shTclGetArgInfo(interp, g_dbvaluTbl, flags, g_dbvalu),
shTclGetUsage(interp, g_dbvaluTbl, flags, g_dbvalu));
shTclDeclare(interp, g_defc, (Tcl_CmdProc *) tcl_defc,
(ClientData) 0, (Tcl_CmdDeleteProc *)0, tclFacil,
shTclGetArgInfo(interp, g_defcTbl, flags, g_defc),
shTclGetUsage(interp, g_defcTbl, flags, g_defc));
shTclDeclare(interp, g_dgefs, (Tcl_CmdProc *) tcl_dgefs,
(ClientData) 0, (Tcl_CmdDeleteProc *)0, tclFacil,
shTclGetArgInfo(interp, g_dgefsTbl, flags, g_dgefs),
shTclGetUsage(interp, g_dgefsTbl, flags, g_dgefs));
shTclDeclare(interp, g_dp1vlu, (Tcl_CmdProc *) tcl_dp1vlu,
(ClientData) 0, (Tcl_CmdDeleteProc *)0, tclFacil,
shTclGetArgInfo(interp, g_dp1vluTbl, flags, g_dp1vlu),
shTclGetUsage(interp, g_dp1vluTbl, flags, g_dp1vlu));
shTclDeclare(interp, g_dpcoef, (Tcl_CmdProc *) tcl_dpcoef,
(ClientData) 0, (Tcl_CmdDeleteProc *)0, tclFacil,
shTclGetArgInfo(interp, g_dpcoefTbl, flags, g_dpcoef),
shTclGetUsage(interp, g_dpcoefTbl, flags, g_dpcoef));
shTclDeclare(interp, g_dpolft, (Tcl_CmdProc *) tcl_dpolft,
(ClientData) 0, (Tcl_CmdDeleteProc *)0, tclFacil,
shTclGetArgInfo(interp, g_dpolftTbl, flags, g_dpolft),
shTclGetUsage(interp, g_dpolftTbl, flags, g_dpolft));
shTclDeclare(interp, g_vSort, (Tcl_CmdProc *) tcl_vSort,
(ClientData) 0, (Tcl_CmdDeleteProc *)0, tclFacil,
shTclGetArgInfo(interp, g_vSortTbl, flags, g_vSort),
shTclGetUsage(interp, g_vSortTbl, flags, g_vSort));
shTclDeclare(interp, g_r1mach, (Tcl_CmdProc *) tcl_r1mach,
(ClientData) 0, (Tcl_CmdDeleteProc *)0, tclFacil,
shTclGetArgInfo(interp, g_r1machTbl, flags, g_r1mach),
shTclGetUsage(interp, g_r1machTbl, flags, g_r1mach));
shTclDeclare(interp, g_rand, (Tcl_CmdProc *) tcl_rand,
(ClientData) 0, (Tcl_CmdDeleteProc *)0, tclFacil,
shTclGetArgInfo(interp, g_randTbl, flags, g_rand),
shTclGetUsage(interp, g_randTbl, flags, g_rand));
shTclDeclare(interp, g_regFft, (Tcl_CmdProc *) tcl_regFft,
(ClientData) 0, (Tcl_CmdDeleteProc *)0, tclFacil,
shTclGetArgInfo(interp, g_regFftTbl, flags, g_regFft),
shTclGetUsage(interp, g_regFftTbl, flags, g_regFft));
shTclDeclare(interp, g_regFftw, (Tcl_CmdProc *) tcl_regFftw,
(ClientData) 0, (Tcl_CmdDeleteProc *)0, tclFacil,
shTclGetArgInfo(interp, g_regFftwTbl, flags, g_regFftw),
shTclGetUsage(interp, g_regFftwTbl, flags, g_regFftw));
shTclDeclare(interp, g_regElementMult, (Tcl_CmdProc *) tcl_regElementMult,
(ClientData) 0, (Tcl_CmdDeleteProc *)0, tclFacil,
shTclGetArgInfo(interp, g_regElementMultTbl, flags, g_regElementMult),
shTclGetUsage(interp, g_regElementMultTbl, flags, g_regElementMult));
shTclDeclare(interp, g_regWrapAround, (Tcl_CmdProc *) tcl_regWrapAround,
(ClientData) 0, (Tcl_CmdDeleteProc *)0, tclFacil,
shTclGetArgInfo(interp, g_regWrapAroundTbl, flags, g_regWrapAround),
shTclGetUsage(interp, g_regWrapAroundTbl, flags, g_regWrapAround));
shTclDeclare(interp, g_xsetf, (Tcl_CmdProc *) tcl_xsetf,
(ClientData) 0, (Tcl_CmdDeleteProc *)0, tclFacil,
shTclGetArgInfo(interp, g_xsetfTbl, flags, g_xsetf),
shTclGetUsage(interp, g_xsetfTbl, flags, g_xsetf));
}
