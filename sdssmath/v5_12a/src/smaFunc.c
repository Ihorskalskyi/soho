/* <AUTO>
   FILE: smaFunc.c
<HTML>
Interface to FUNC (function) and METH (method) structures in sdssmath
</HTML>
</AUTO> */

#include <string.h>
#include <math.h>
#include <smaValues.h>
#include <float.h>
#include "sdssmath.h"
#define isgood(x)   ( ( ((x) < FLT_MAX)) && ((x) > -FLT_MAX) ? (1) : (0))

/*<AUTO EXTRACT>
  Create a new FUNC
</AUTO>*/
FUNC *smFuncNew(
		char *name,	/* name of the function: gauss, dpol, bspline, 
				   lin */
		VECTOR **va,	/* array of vectors with parameter values */
		int nHandle	/* number of vectors in va */
		) {
  FUNC *func = (FUNC *)shMalloc(sizeof(FUNC));
  if ((func->fa = smaFuncAddrGetFromName(name, va, nHandle)) == NULL) {
    shFree(func);
    return NULL;
  }
  strncpy(func->kind, name, FUNC_LABEL_SIZE);
  func->nVector = nHandle;
  func->va = va;
  func->cov = NULL;
  func->sigma = NULL;
  func->chi2 = -1.0;
  return func;
}

/*<AUTO EXTRACT>
  Delete a FUNC
</AUTO>*/
RET_CODE smFuncDel(
		   FUNC *func	/* the FUNC to delete */
		   ) {
  int i;
  if (func != NULL) {
    for (i=0; i<func->nVector; i++) {
      shVectorDel(func->va[i]);
    }
    shFree(func->va);
    if (func->cov != NULL) shVectorDel(func->cov);
    if (func->sigma != NULL) shVectorDel(func->sigma);
    shFree(func);
  }
  return SH_SUCCESS;
}

/*<AUTO EXTRACT>
  Create a new METH
</AUTO>*/
METH *smMethNew(
		char *name	/* name of the method; chi2 */
		) {
  METH *meth = (METH *)shMalloc(sizeof(METH));
  if ((meth->ma = smaMethAddrGetFromName(name)) == NULL) {
    shFree(meth);
    return NULL;
  }
  strncpy(meth->kind, name, FUNC_LABEL_SIZE);
  return meth;
}

/*<AUTO EXTRACT>
  Delete a METH
</AUTO>*/
RET_CODE smMethDel(
		   METH *meth	/* the METH to delete */
		   ) {
  if (meth != NULL) shFree(meth);
  return SH_SUCCESS;
}

/*<AUTO EXTRACT>
  Evaluate at FUNC at one value of x.  Return SH_SUCCESS if the value is 
  valid, or SH_GENERIC_ERROR if there is trouble.
</AUTO>*/
RET_CODE smaFuncEval(
		     FUNC *func, /* the FUNC to evaluate */
		     double x,	/* where to evaluate it */
		     double *y	/* the return value */
		     ) {
  RET_CODE r;
  r = func->fa(func->nVector, func->va, NULL, x, y);
  return r;
}

static VECTOR *sma_global_x = NULL;
static VECTOR *sma_global_y = NULL;
static VECTOR *sma_global_w = NULL;
static VECTOR *sma_global_mask = NULL;
static FUNC   *sma_global_func = NULL;

/*<AUTO EXTRACT>
  Function definition for an "unknown" which only returns zeros
</AUTO>*/
RET_CODE smaFuncUnknown(
                        int nVector,
                        VECTOR **va,
                        double *param,
                        double x,
                        double *y
                        ) {
*y = 0;
return SH_SUCCESS;
}


/*<AUTO EXTRACT>
  Function definition for gauss
  One vector of dimen=3 in va with the position, sigma, and normalization.
</AUTO>*/
RET_CODE smaFuncGauss(
		      int nVector,
		      VECTOR **va,
		      double *param,
		      double x,
		      double *y
		      ) {
  double p0, p1, p2;
  if ( (nVector != 1) || (va[0]==NULL) ) {
    return SH_GENERIC_ERROR;
  }
  if (param == NULL) {
    p0 = va[0]->vec[0]; p1 = va[0]->vec[1]; p2 = va[0]->vec[2]; 
  } else {
    p0 = param[0]; p1 = param[1]; p2 = param[2];
  }
  *y = p2 * exp(-pow((x-p0),2) / (2 * pow(p1,2))) / (p1 * sqrt(2*M_PI));
  if (!isgood(*y)) *y=0.0;
  if (fabs(*y) < 1e-20) *y=0.0;
  return SH_SUCCESS;
}

/*<AUTO EXTRACT>
  Function definition for Hubble
  One vector of dimen=2 in va with I0 and a.
</AUTO>*/
RET_CODE smaFuncHubble(
		       int nVector,
		       VECTOR **va,
		       double *param,
		       double x,
		       double *y
		       ) {
  double p0, p1;
  if ( (nVector != 1) || (va[0]==NULL) ) {
    return SH_GENERIC_ERROR;
  }
  if (param == NULL) {
    p0 = va[0]->vec[0]; p1 = va[0]->vec[1]; 
  } else {
    p0 = param[0]; p1 = param[1]; 
  }
  *y = p0 / pow(1+(x/p1), 2);
  if (!isgood(*y)) *y=0.0;
  if (fabs(*y) < 1e-20) *y=0.0;
  return SH_SUCCESS;
}

/*<AUTO EXTRACT>
  Function definition for devauc
  One vector of dimen=2 in va with Ie and re.
</AUTO>*/
RET_CODE smaFuncDevauc(
		      int nVector,
		      VECTOR **va,
		      double *param,
		      double x,
		      double *y
		      ) {
  double p0, p1;
  if ( (nVector != 1) || (va[0]==NULL) ) {
    return SH_GENERIC_ERROR;
  }
  if (param == NULL) {
    p0 = va[0]->vec[0]; p1 = va[0]->vec[1]; 
  } else {
    p0 = param[0]; p1 = param[1]; 
  }
  *y = p0 * pow(10, -3.333*(pow(x/p1, 0.25)-1));
  if (!isgood(*y)) *y=0.0;
  if (fabs(*y) < 1e-20) *y=0.0;
  return SH_SUCCESS;
}

/*<AUTO EXTRACT>
  Function definition for Freeman
  One vector of dimen=2 in va with I0 and r0.
</AUTO>*/
RET_CODE smaFuncFreeman(
		      int nVector,
		      VECTOR **va,
		      double *param,
		      double x,
		      double *y
		      ) {
  double p0, p1;
  if ( (nVector != 1) || (va[0]==NULL) ) {
    return SH_GENERIC_ERROR;
  }
  if (param == NULL) {
    p0 = va[0]->vec[0]; p1 = va[0]->vec[1]; 
  } else {
    p0 = param[0]; p1 = param[1]; 
  }
  *y = p0 * exp(-x/p1);
  if (!isgood(*y)) *y=0.0;
  if (fabs(*y) < 1e-20) *y=0.0;
  return SH_SUCCESS;
}

/*<AUTO EXTRACT>
  Function definition for dpol
  We expect to get two vectors in the va:
  va[0] is returned from dpolft
  va[1] is a vector of length 1, containing the degree of the polynomial
</AUTO>*/
RET_CODE smaFuncDpol(
		      int nVector,
		      VECTOR **va,
		      double *param,
		      double x,
		      double *y
		      ) {
  int l;
  int nder=0;
  double *yp = NULL;

  if ( (nVector != 2) || (va[0]==NULL) || (va[1]==NULL)) {
    return SH_GENERIC_ERROR;
  }
  l = (int) va[1]->vec[0];
  smaDP1VLU(l, nder, x, y, yp, va[0]->vec);
  return SH_SUCCESS;
}

/*<AUTO EXTRACT>
  Function definition for bspline
We expect 6 vectors here:
     va[0] is the knot array of length n+4 (t)
     va[1] is the coefficient vector of length n (a)
     va[2] is the order of the fit spline k >= 1 (k)
     va[3] is the derivative to evaluate (0 for example)
     va[4] is the inbv parameter; set to 1 and retains state of v[5] (inbv)
     va[5] is a work array of length 3*k (work)
</AUTO>*/
RET_CODE smaFuncBspline (
		      int nVector,
		      VECTOR **va,
		      double *param,
		      double x,
		      double *y
		      ) {
  int n, k, ideriv, inbv;
  double xmin, xmax;

  if ( (nVector != 6) ) {
    shErrStackPush("smaFuncBspline:  nVector must be 6");
    return SH_GENERIC_ERROR;
  }
  n = va[0]->dimen-4; k=va[2]->vec[0]; 
  xmin = va[0]->vec[k-1];
  if (x <= xmin) {
    x = xmin;
  }
  xmax = va[0]->vec[n];
  if (x >= xmax) {
    x = xmax;
  }
  ideriv=va[3]->vec[0]; inbv=va[4]->vec[0];
  *y=smaDBVALU(va[0]->vec, va[1]->vec, n, k, ideriv, x, &inbv, va[5]->vec);
  va[4]->vec[0] = inbv;
  return SH_SUCCESS;
}

/*<AUTO EXTRACT>
  Function definition for lin
  One vector in va has intercept and slope of the straight line
</AUTO>*/
RET_CODE smaFuncLin(
		    int nVector,
		    VECTOR **va,
		    double *param,
		    double x,
		    double *y
		    ) {
  double p0, p1;
  if ( (nVector != 1) || (va[0]==NULL) ) {
    return SH_GENERIC_ERROR;
  }
  if (param == NULL) {
    p0 = va[0]->vec[0]; p1 = va[0]->vec[1];
  } else {
    p0 = param[0]; p1 = param[1];
  }
  *y = p0 + x*p1;
  return SH_SUCCESS;
}

/*<AUTO EXTRACT>
  Function definition for linMulti
  returned by vLinFit
  A linear equation with n terms and one unknown, 
       (i.e.,  Sigma_{0}^{i} a_i x_i = y, with one x_i unknown )
  va[0] = constants determined by linear fit
  va[1] = values for each x_i (include place holder value for unknown)
  va[2] = the y value in va[2]->vec[0] 
  x = index for the  x_i that is the unknown (zero based indexing)
  y = the returned value for the unknown x_i 
</AUTO>*/
RET_CODE smaFuncLinMulti( 
                          int nVector,
                          VECTOR **va,
                          double *param,
                          double x,
                          double *y
                         ) {
  int unknownIndex;
  int ct, nTerms;
  double sum, yTerm;

/* make sure all the inputs are well behaved */

  if ( nVector != 3 ) { 
    shErrStackPush("smaFuncLinMulti: need 3 vectors, found %d\n", nVector);
    return SH_GENERIC_ERROR;
  }

  if ( va[0]->dimen != va[1]->dimen ) {
    shErrStackPush("smaFuncLineMulti: vector with paramaters and variables must be equal\n");
    return SH_GENERIC_ERROR;
  }

/* solve for the unknown variable */

  unknownIndex = (int) x;

  if ( va[2] != NULL ) {
    yTerm = va[2]->vec[0];
  } else {
    yTerm = 0;
  }

  nTerms = va[0]->dimen;

  sum = 0;
  for ( ct=0; ct <= nTerms; ct++ ) {
    if ( ct != unknownIndex ) { 
      sum += va[0]->vec[ct] * va[1]->vec[ct];
    }
  }
  *y = ( yTerm - sum ) / va[0]->vec[unknownIndex];

  return SH_SUCCESS;
}

/********************************/

/*<AUTO EXTRACT>
  Set some global pointers
</AUTO>*/
void smaGlobalSet(VECTOR *x, VECTOR *y, VECTOR *w, FUNC *func, VECTOR *mask) {
  sma_global_x = x;
  sma_global_y = y;
  sma_global_w = w;
  sma_global_func = func;
  sma_global_mask = mask;
  return;
}

/*<AUTO EXTRACT>
  Method definition for Chi2
</AUTO>*/
void smaMethChi2 (
		  int *iflag, 
		  int *m,	 /* number of data points */
		  int *n,	 /* number of parameters */
		  double *param, /* input array of length n */
		  double *fvec,	 /* output array of length m */
		  double *fjac, 
		  int *ldfjac) {
  int i;
  double x, y, w, yfit;
  switch (*iflag) {
  case 0: /* print information */
    printf("in smaMethChi2 with func=%s\n", sma_global_func->kind);
    break;
  case 1: /* calculate the functions at x and return this vector in fvec */
    for (i=0; i<sma_global_x->dimen; i++) {
      if ( (sma_global_mask==NULL) || (sma_global_mask->vec[i]==1)) {
	x = sma_global_x->vec[i];
	sma_global_func->fa(sma_global_func->nVector, sma_global_func->va, 
			    param, x, &yfit);
	y = sma_global_y->vec[i];
	w = sma_global_w->vec[i];
	fvec[i] = w*pow(yfit-y,2);
      } else {
	fvec[i] = 0.0;
      }
      
    }
    /* printf("chi2 is %15.12g\n", smaDENORM(sma_global_x->dimen, fvec)); */
    break;
  case 2: /* calculate the full Jacobian */
    break;
  }
  return;
}

/*<AUTO EXTRACT>
  Get the address of the function given its name and do some sanity checks
</AUTO>*/
SMA_FUNC smaFuncAddrGetFromName(
				char *name, /* name of the function */
				VECTOR **va, /* the va to set */
				int nVector /* the number of vectors in va */
				) {
  if (!strcmp(name, "unknown")) {
    return smaFuncUnknown;
  }
  if (!strcmp(name, "gauss")) {
    if ((nVector != 1) || (va[0]->dimen != 3)) return NULL;
    return smaFuncGauss;
  }
  if (!strcmp(name, "hubble")) {
    if ((nVector != 1) || (va[0]->dimen != 2)) return NULL;
    return smaFuncHubble;
  }
  if (!strcmp(name, "devauc")) {
    if ((nVector != 1) || (va[0]->dimen != 2)) return NULL;
    return smaFuncDevauc;
  }
  if (!strcmp(name, "freeman")) {
    if ((nVector != 1) || (va[0]->dimen != 2)) return NULL;
    return smaFuncFreeman;
  }
  if (!strcmp(name, "dpol")) {
    if ((nVector != 2) || (va[1]->dimen != 1)) return NULL;
    return smaFuncDpol;
  }
  if (!strcmp(name, "bspline")) {
    if ((nVector != 6) || (va[2]->dimen != 1) || 
	(va[3]->dimen != 1) || (va[4]->dimen != 1)) return NULL;
    return smaFuncBspline;
  }
  if (!strcmp(name, "lin")) {
    if ((nVector != 1) || (va[0]->dimen != 2)) return NULL;
    return smaFuncLin;
  }
  if (!strcmp(name, "linMulti")) {
    if ( (nVector !=3) ) return NULL;
    return smaFuncLinMulti;
  }
  return NULL; 
}



/*<AUTO EXTRACT>
  Get the method address from the name
</AUTO>*/
SMA_METH smaMethAddrGetFromName(
				char *name /* the name of the method */
				) {
  if (!strcmp(name, "chi2")) {
    return  smaMethChi2;
  }
  return NULL; 
}

/*<AUTO EXTRACT>
  Fill in the covariance matrix 
</AUTO>*/
RET_CODE smaCovFromFit(
		       METH *meth, /* the method to use */
		       FUNC *func, /* the fit function; 
				      cov sigma and chi2 filled in here */
		       double epsfcn /* step size to use;
				      0.0 defaults to machine precision */
		       ) {
  int i, j;
  int iflag, m, n, ldfjac;
  double *param, *fvec, *fjac;
  double eps;
  double epsmch;
  double chi2[4], hi, hj;
  int *kpvt;
  int info = 0;
  double det[2];
  int inert[3];
  double *work;
  double t0, t2;

  epsmch = smaD1MACH(4);
  if (epsmch > epsfcn) eps = sqrt(epsmch); else eps = sqrt(epsfcn);

  iflag = 1;
  m = sma_global_x->dimen;
  n = func->va[0]->dimen;

  kpvt = (int *) shMalloc(n*sizeof(int));
  work = (double *) shMalloc(n*sizeof(double));


  ldfjac = 0;
  func->cov = shVectorNew(n*n);
  param = (double *) shMalloc(n*sizeof(double));
  for (i=0; i<n; i++) param[i] = func->va[0]->vec[i];
  fvec = (double *) shMalloc(m*sizeof(double));
  fjac = (double *) shMalloc(m*n*sizeof(double));

  ldfjac = m;
  meth->ma(&iflag, &m, &n, param, fvec, fjac, &ldfjac);
  chi2[0] = smaDENORM(m, fvec);
  func->chi2 = chi2[0];

  for (i=0; i<n; i++) {
    for (j=i; j<n; j++) {
      if (i==j) {
	hi = eps*fabs(param[i]);
	param[i] += hi;
	meth->ma(&iflag, &m, &n, param, fvec, fjac, &ldfjac);
	chi2[1] = smaDENORM(m, fvec);
	param[i] += hi;	
	meth->ma(&iflag, &m, &n, param, fvec, fjac, &ldfjac);
	chi2[2] = smaDENORM(m, fvec);
	t0 = (chi2[0]/(hi*hi)) - (chi2[1]/(hi*hi));
	t2 = (chi2[2]/(hi*hi)) - (chi2[1]/(hi*hi));
	func->cov->vec[i*n+j] = t0+t2;

#if 0
	printf("i=%d j=%d cov=%f\n", i, j, func->cov->vec[i*n+j]);
	printf(".....chi2[0]=%20.15g\n", chi2[0]);
	printf(".....chi2[1]=%20.15g\n", chi2[1]);
	printf(".....chi2[2]=%20.15g\n", chi2[2]);
	printf("t0=%20.15g\n", t0);
	printf("t2=%20.15g\n", t2);
#endif

	param[i] = func->va[0]->vec[i];
      } else {
	hi = eps*fabs(param[i]);
	hj = eps*fabs(param[j]);
	param[j] += hj;
	meth->ma(&iflag, &m, &n, param, fvec, fjac, &ldfjac);
	chi2[1] = smaDENORM(m, fvec);
	param[i] += hi;
	meth->ma(&iflag, &m, &n, param, fvec, fjac, &ldfjac);
	chi2[2] = smaDENORM(m, fvec);
	param[j] = func->va[0]->vec[j];
	meth->ma(&iflag, &m, &n, param, fvec, fjac, &ldfjac);
	chi2[3] = smaDENORM(m, fvec);
	func->cov->vec[i*n+j] = (chi2[2]+chi2[0]-chi2[1]-chi2[3])/(hi*hj);
	func->cov->vec[j*n+i] = func->cov->vec[i*n+j];
	param[i] = func->va[0]->vec[i];
      }
    }
  }
  if (strcmp(meth->kind,"chi2") == 0) {
    /* Don't forget the factor of 1/2 for the Chi2! */
    for (i=0; i<n*n; i++) func->cov->vec[i] *= 0.5;
  }

  smaDSIFA(func->cov->vec, n, n, kpvt, &info);

  if (info == 0) {
    smaDSIDI(func->cov->vec, n, n, kpvt, det, inert, work, 1);

    /* Set the _lower_ diag terms to be the same as the _upper_ diag terms */
    for (i=1; i<n; i++) {
      for (j=0; j<i; j++) {
	func->cov->vec[j*n+i] = func->cov->vec[i*n+j];
      }
    }
    func->sigma = shVectorNew(n);
    for (i=0; i<n; i++) func->sigma->vec[i] = func->cov->vec[i*n+i]; 
  } else {
    shVectorDel(func->cov);
    func->cov = NULL;
  }
  shFree(kpvt); 
  shFree(work);
  shFree(param);
  shFree(fvec);
  shFree(fjac);
  return SH_SUCCESS;
}

/*<AUTO EXTRACT>
  Plot a FUNC on a PGSTATE
</AUTO>*/
int smaFuncPlot(
		PGSTATE *pg,	/* where to plot and how to plot it */
		FUNC *func,	/* what to plot */
		FUNC *resFunc,  /* what to subtract at each point */
		double xmin,	/* beginning x */
		double xmax,	/* ending x */
		int npt		/* number of points to plot */
		) {
  double res;
  int i;
  int nptOut = 0;
  VECTOR *xpts, *ypts, *mask;
  xpts = shVectorNew(npt+1);
  ypts = shVectorNew(npt+1);
  mask = shVectorNew(npt+1);
  for (i=0; i<=npt; i++) {
    xpts->vec[i] = xmin + i*(xmax-xmin)/npt;
    if (resFunc == NULL) {
      if (smaFuncEval(func, xpts->vec[i], &ypts->vec[i]) == SH_SUCCESS) {
	mask->vec[i] = 1;
	nptOut++;
      } else {
	mask->vec[i] = 0;
      }
    } else {
      mask->vec[i] = 0;
      if (smaFuncEval(func, xpts->vec[i], &ypts->vec[i]) == SH_SUCCESS) {
	if (smaFuncEval(resFunc, xpts->vec[i], &res) == SH_SUCCESS) {
	  ypts->vec[i] -= res;
	  mask->vec[i] = 1;
	  nptOut++;
	}
      }
    }
  }
  if (nptOut > 0) {
    shVPlot(pg, xpts, NULL, ypts, NULL, mask, 
	    NULL, NULL, xmin, xmax, 
	    shVExtreme(ypts, mask, "min"),
	    shVExtreme(ypts, mask, "max"),
	    (XMINOPT | XMAXOPT | YMINOPT | YMAXOPT));
  }
  shVectorDel(xpts);
  shVectorDel(ypts);
  shVectorDel(mask);
  return nptOut;
}









