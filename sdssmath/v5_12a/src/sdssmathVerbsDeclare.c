#include <stdlib.h>
#include <stdio.h>
#include "dervish.h"
#include "sdssmath.h"

void smSchemaLoadFromSdssmath(void);


void smaVerbsDeclare(Tcl_Interp *interp) {

/* --- Place your module declarations here --- */
  (void)smSchemaLoadFromSdssmath();
  tcl_slatec_declare(interp);
  tcl_func_declare(interp);

  /* Tell the slatec routines to shut up and keep running if
     they get into a little trouble */
  smaXSETF(0);

  /* Use shMalloc and shFree memory management in FFTW calls */
  fftw_malloc_hook = shMalloc;
  fftw_free_hook   = shFree;

}
