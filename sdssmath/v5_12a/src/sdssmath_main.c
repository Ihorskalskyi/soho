/*****************************************************************************
 ******************************************************************************
 **
 ** FILE:
 **	sdssmath_main.c
 **
 ** ABSTRACT:
 **	This is the main program for minimal use of SDSS math tcl verbs
 **
 **	This product is built on top of DERVISH and FTCL.
 **
 ** ENVIRONMENT:
 **	ANSI C.
 **
 ******************************************************************************
 ******************************************************************************
 */
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include "dervish.h"
#include "tclMath.h"

#define	L_PHPROMPT	"sdssmath> "

int main(int argc, char *argv[])
{
   SHMAIN_HANDLE	handle;
   Tcl_Interp	*interp;
   
   /*
    ** INITIALIZE TO BEGIN ACCEPTING TCL COMMANDS FROM TERMINAL AND FSAOIMAGE 
    */
   if (shMainTcl_Init(&handle, &argc, argv, L_PHPROMPT) != SH_SUCCESS)
     return 1;
   
   interp = handle.interp;
   
   /*
    ** DECLARE TCL COMMANDS
    */
   ftclCreateCommands(interp);   /* Fermi enhancements to TCL   */
   ftclCreateHelp (interp);	/* Define help command         */
   ftclMsgDefine (interp);	/* Define help for tcl verbs   */

   /* --- DECLARE EAGPHOT COMMANDS --- */
   smaVerbsDeclare(interp);	/* sdssmath tcl declarations */

   /*
    ** OTHER INITIALIZATION 
    */
      
   /* --- PLACE YOUR FAVORITE INITIALIZATIONS HERE --- */
   ftclParseInit(interp);	/* Ftcl command parsing */

   /* 
     NOTE: shMainTcl_Declare is a comprehensive declaration routine for
     Dervish.  Included are TCL command declarations for region, peek, and
     FSAOimage.  If all commands are not desired, one can customize by
     replacing the call to this routine with individual calls to only the
     desired TCL command declaration routines. It runs the initialisation
     scripts, so must go after all of the initialisation routines have
     been called
     */
   shMainTcl_Declare(&handle);	/* Required Dervish TCL commands */ 

   /*
    *             DO NOT PUT ANYTHING BELOW THIS LINE.
    *
    * We will now enter the main loop of Tk. There is no successfull return
    * from this loop. Anything added below this line will never be reached,
    * NEVER!!
    */
   shTk_MainLoop(&handle);

   return 0;

}


