proc makeTclBindings {package} {
    set file [open tcl_$package.par r]

    set of [open ../src/tcl_$package.c w]

    puts $of "/* WARNING -- this file was generated automatically */"
    puts $of "/* by sdssmath/etc/makeTclBindings.tcl */ "
    puts $of "/* <AUTO>"
    puts $of "FILE: tcl_$package.c"
    gets $file line
    puts $of $line
    puts $of "</AUTO>"
    puts $of "*/"
    puts $of "\#include <stdlib.h>"
    puts $of "\#include <stdio.h>"
    puts $of "\#include <string.h>"
    puts $of "\#include <ctype.h>"
    puts $of "\#include <math.h>"
    puts $of "\#include \"dervish.h\""
    puts $of "\#include \"sdssmath.h\""
    puts $of "static char *tclFacil  = \"slatec\";"

    puts -nonewline "Now do top for "
    while {[gets $file line] > 0} {
	puts -nonewline [format " %s" $line]
	makeTclBindingTop $line $of
    }
    echo .
    close $file

    puts $of "/*******************************************************/"
    puts $of "/*** declare these tcl verbs ***/"
    puts $of "void tcl_[set package]_declare(Tcl_Interp *interp) \{"
    puts $of "int flags = FTCL_ARGV_NO_LEFTOVERS\;"

    set file [open tcl_$package.par r]
    gets $file line
    puts -nonewline "Now do bottom for "
    while {[gets $file line] > 0} {
	puts -nonewline [format " %s" $line]
	makeTclBindingBottom $line $of
    }
    echo .
    close $file

    puts $of "\}"
    close $of
}

proc makeTclBindingTop {name of} {
    global mtb np nd nr nc na nh no i line
    set file [open $name.par]
    set state none
    set states "DESCRIPTION RETURN PARAMETER CALL DECLARE NEWHANDLE OUTPUT"

    set np 0; set nd 0; set nr 0; set nc 0; set na 0; set nh 0; set no 0;

    while {[gets $file line] > -1} {
	set tempState [lindex $line 0]
	if {[lsearch $states $tempState] > -1} {
	    set state $tempState
	    if {$state == "PARAMETER"} {
		set mtb(p.$np) $line; incr np
	    }
	    if {$state == "NEWHANDLE"} {
		set mtb(h.$nh) $line; incr nh
	    }
	} else {
	    case $state in {
		DESCRIPTION {
		    set mtb(d.$nd) $line; incr nd
		}
		RETURN {
		    set mtb(r.$nr) $line; incr nr
		}
		CALL {
		    set mtb(c.$nc) $line; incr nc
		}
		DECLARE {
		    set mtb(a.$na) $line; incr na
		}
		OUTPUT {
		    if {[string length $line] > 0} {
			set mtb(o.$no) $line; incr no
		    }
		}
	    }
	}
    }
    close $file
    puts $of "/*********************************************************"
    puts $of "**"
    puts $of "** ROUTINE tcl_$name "
    puts $of "**"
    puts $of "**<AUTO EXTRACT>"
    puts $of "** TCL VERB: $name"
    puts $of "**</AUTO>"
    puts $of "*/"
    puts $of "char g_$name\[\] = \"$name\";"
    puts $of "ftclArgvInfo g_[set name]Tbl\[\] = \{"
    puts $of "\{NULL, FTCL_ARGV_HELP, NULL, NULL,"
    loop i 0 $nd {
	puts $of "\"$mtb(d.$i)\\n\""
    }
    loop i 0 $nr {
	puts $of "\"$mtb(r.$i)\\n\""
    }
    puts $of "\},"
    loop i 0 $np {
	puts -nonewline \
		$of "\{\"[lindex $mtb(p.$i) 1]\", [lindex $mtb(p.$i) 2],"
	puts $of "NULL, NULL, \"[lindex $mtb(p.$i) 4]\"\},"

    }
    puts $of "\{(char *) NULL, FTCL_ARGV_END, NULL, NULL, (char *) NULL\}"
    puts $of "\};"

    puts $of "static int"
    puts $of "tcl_[set name](ClientData clientData, "
    puts $of "Tcl_Interp *interp, int argc, char **argv) \{"

    # declare the variables 
    puts $of "int rstat\;"
    if {$no > 0} {
	puts $of "char tempString\[100\]\;"
    }
    loop i 0 $np {
	set type [lindex $mtb(p.$i) 3]
	set vname [string trim [lindex $mtb(p.$i) 1] <>-\[\]]
	set def  [lindex $mtb(p.$i) 5]
	puts $of "$type $vname = $def\; "
	if {[lindex $mtb(p.$i) 2] == "FTCL_ARGV_STRING"} {
	    if {$type != "char*"} {
		puts $of "char* handle_$vname = NULL\;"
	    }
	}
    }
    # these are listed in the .par file
    loop i 0 $na {
	puts $of $mtb(a.$i)
    }
    # for the handles
    loop i 0 $nh {
	set vname   [lindex $mtb(h.$i) 1]
	set type    [lindex $mtb(h.$i) 2]
	set default [lindex $mtb(h.$i) 3]
	puts $of "char handle_$vname\[HANDLE_NAMELEN\]\;"
	puts $of "$type $vname = $default\;"
    }

    # set up the g_*Tbl
    loop i 0 $np {
	set vname [string trim [lindex $mtb(p.$i) 1] <>-\[\]]
	set ip1 [expr $i+1]

	set t1 [lindex $mtb(p.$i) 2]
	set t2 [lindex $mtb(p.$i) 3]
	if {($t1 == "FTCL_ARGV_STRING") && ($t2 != "char*")} {
	    puts $of "g_[set name]Tbl\[$ip1\].dst = &handle_$vname\;"
	} else {
	    puts $of "g_[set name]Tbl\[$ip1\].dst = &$vname\;"
	}
    }

    # parse the command
    puts -nonewline $of \
	    "if ((rstat = shTclParseArgv(interp, &argc, argv,"
    puts -nonewline $of \
	    "g_[set name]Tbl, FTCL_ARGV_NO_LEFTOVERS, g_[set name]))"
    puts $of "!= FTCL_ARGV_SUCCESS) return rstat\;"

    # get addresses from handles
    loop i 0 $np {
	if {[lindex $mtb(p.$i) 2] == "FTCL_ARGV_STRING"} {
	    set type  [string trim [lindex $mtb(p.$i) 3] *]
	    if {$type != "char"} {
		set vname [string trim [lindex $mtb(p.$i) 1] <>-\[\]]
		puts $of "if (handle_$vname==NULL) \{"
		puts $of "$vname = NULL\;"
		puts $of "\} else \{"
		puts $of "if (shTclAddrGetFromName(interp, handle_$vname,"
		puts $of "(void **) &$vname, \"$type\") != TCL_OK) \{"
		puts $of "Tcl_SetResult(interp, \"Bad $type for $vname\","
		puts $of "TCL_VOLATILE)\; return TCL_ERROR\;\}"
		puts $of "\}"
	    }
	}
    }

    # call the actual code now
    loop i 0 $nc {
	puts $of "$mtb(c.$i)"
    }

    # make new handles 
    loop i 0 $nh {
	set vname   [lindex $mtb(h.$i) 1]
	set type    [string trim [lindex $mtb(h.$i) 2] *]
	puts $of "if (shTclHandleNew(interp, handle_$vname, \"$type\", $vname)"
	puts $of "!= TCL_OK) \{ Tcl_SetResult(interp, "
	puts $of "\"Error getting $vname handle\","
	puts $of "TCL_VOLATILE)\; return TCL_ERROR\;\}"
    }

    # build up the output string
    loop i 0 $no {
	if {[llength $mtb(o.$i)] == 2} {
	    set format [lindex $mtb(o.$i) 0]
	    set vname  [lindex $mtb(o.$i) 1]
	    puts $of "sprintf(tempString, \"$format\", $vname)\;"
	} else {
	    error "SORRY -- I don't know how to output anything complicated"
	}
	puts $of "Tcl_AppendElement(interp, tempString);"
    }

    # return
    puts $of "return TCL_OK\;"
    puts $of "\}"
}

proc makeTclBindingBottom {name of} {
    puts $of "shTclDeclare(interp, g_$name, (Tcl_CmdProc *) tcl_$name,"
    puts $of "(ClientData) 0, (Tcl_CmdDeleteProc *)0, tclFacil,"
    puts $of "shTclGetArgInfo(interp, g_[set name]Tbl, flags, g_$name),"
    puts $of "shTclGetUsage(interp, g_[set name]Tbl, flags, g_$name))\;"
}
