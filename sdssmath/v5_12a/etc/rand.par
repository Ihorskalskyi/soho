DESCRIPTION
      This pseudo-random number generator is portable among a wide
variety of computers.  RAND undoubtedly is not as good as many
readily available installation dependent versions, and so this
routine is not recommended for widespread usage.  Its redeeming
feature is that the exact same random numbers (to within final round-
off error) can be generated from machine to machine.  Thus, programs
that make use of random numbers can be easily transported to and
checked in a new environment.
            Input Argument --
       If seed = 0., the next random number of the sequence is generated.
       If seed < 0., the last generated number will be returned for
         possible use in a restart procedure.
       If seed > 0., the sequence of random numbers will start with
         the seed mod 1.  This seed is also returned as the value of
         RAND provided the arithmetic is done exactly.
RETURN
Returns a  pseudo-random number between 0. and 1.
PARAMETER [seed] FTCL_ARGV_DOUBLE double "input parameter" 0
DECLARE
float random;
CALL
random = smaRAND((float) seed);
OUTPUT
%11.9f random
