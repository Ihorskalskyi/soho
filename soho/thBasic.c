#include "thBasic.h"
#include "dervish.h"
#define SORTKEY_BIAS 16384
#define SORTKEY(X,Y) (((Y) << 16) | (SORTKEY_BIAS + X))


void thInsertStatus(RET_CODE *status, RET_CODE status2) {
if (status == NULL) return;
*status = status2;
return;
}


void thInsertInt(int *x, int y) {
if (x == NULL) return;
*x = y;
return;
}

int thErrorStatus(RET_CODE *status) {
if (status == NULL) return(0);
if (*status != SH_SUCCESS) return(1);
return(0);
}

OBJMASK *thObjmaskFromCircleInRect(int rowc, int colc, int r, 
					int row0, int col0, 
					int row1, int col1) {

OBJMASK *om;
if ((rowc + r < row0 || rowc - r > row1) && 
	(colc + r < col0 || colc -r > col1)) {
	om = phObjmaskNew(0);
	return(om);
	}

om = phObjmaskFromCircle(rowc, colc, r);
if ((colc + r <= col1) && (colc - r >= col0) 
	&& (rowc + r <= row1) && (rowc - r >= row0)) {
	return(om);
	}

static int init = 0;
static OBJMASK *rec_om;
static int ini_row0 = 0, ini_row1 = 0, ini_col0 = 0, ini_col1 = 0;

if (ini_row0 == row0 && ini_col0 == col0 && 
	ini_row1 == row1 && ini_col1 == col1 && init) {
	phObjmaskAndObjmask(om, rec_om);
	return(om);
}

if (rec_om != NULL) phObjmaskDel(rec_om);		
rec_om = phObjmaskFromRect(col0, row0, col1, row1);
ini_col0 = col0;
ini_col1 = col1;
ini_row0 = row0;
ini_row1 = row1;
init = 1;

phObjmaskAndObjmask(om, rec_om);
return(om);
}


/*****************************************************************************/
/*
 * And error messages
 *
 * function to call when science module receives a non-fatal error and
 * needs to tell framework. The message is printed, and the string is
 * pushed onto the error stack.
 *
 * Note that if the message ends in a newline it will be removed before
 * passing it on to the error stack
 */
void
my_thError(char *fmt, ...)
{
   va_list args;
   char buff[1000];
   int len;

   va_start(args,fmt);
   vsprintf(buff,fmt,args);
   va_end(args);

   if(buff[len = strlen(buff)] == '\n') {
      buff[len] = '\0';
   }
#if STACK_ERROR
   shErrStackPush("%s",buff);
#endif
#if RUNTIME_ERROR   
   fprintf(stderr,"%s\n",buff);
   fflush(stderr);
#endif
}

void
my_thWarn(char *fmt, ...)
{
   va_list args;
   char buff[1000];
   int len;

   va_start(args,fmt);
   vsprintf(buff,fmt,args);
   va_end(args);

   if(buff[len = strlen(buff)] == '\n') {
      buff[len] = '\0';
   }
#if STACK_WARNING
   shErrStackPush("%s",buff);
#endif
#if RUNTIME_WARNING  
   fprintf(stderr,"%s\n",buff);
   fflush(stderr);
#endif
}

/* centralize and decentralize region */
RET_CODE centralize_region(REGION *reg, int *row0, int *col0) {
char *name = "centralize_region";

shAssert(reg != NULL);
shAssert(row0 != NULL);
shAssert(col0 != NULL);
shAssert(row0 != &reg->row0)
shAssert(col0 != &reg->col0);

*row0 = reg->row0;
*col0 = reg->col0;

int nrow, ncol;
nrow = reg->nrow;
ncol = reg->ncol;

reg->row0 = -(nrow / 2);
reg->col0 = -(ncol / 2);

if (ncol % 2 == 0) {
	thError("%s: WARNING - even (nrow = %d) found", name, nrow);
}
if (ncol % 2 == 0) {
	thError("%s: WARNING - even (ncol = %d) found", name, ncol);
}

return(SH_SUCCESS);
}

RET_CODE decentralize_region(REGION *reg, int row0, int col0) {
shAssert(reg != NULL);
reg->row0 = row0;
reg->col0 = col0;
return(SH_SUCCESS);
}


RET_CODE thChainQsort(CHAIN *chain,
	     int (*compar)(const void *, const void *))
{
   char *name = "thChainQsort";
   void **arr;				/* unpack chain into this array */
   CHAIN_ELEM *elem;			/* a link of a chain */
   int i;
   int n;				/* length of chain */

   shAssert(chain != NULL && chain->type != shTypeGetFromName("GENERIC"));

   if((n = chain->nElements) <= 1) {		/* nothing to do */
      return(SH_SUCCESS);
   }
/*
 * extract chain into arr[]
 */
   arr = alloca(n*sizeof(void *));

   for(elem = chain->pFirst, i = 0; elem != NULL; elem = elem->pNext, i++) {
      arr[i] = elem->pElement;
   }
/*
 * call the system qsort to do the work
 */
   qsort(arr, n, sizeof(void *), compar);
/*
 * and rebuild the chain
 */
   char *varname = shNameGetFromType(shChainTypeGet(chain));
   for (i = n-1; i >= 0; i--) {
	shChainElementRemByPos(chain, TAIL);
}
   if (shChainSize(chain) != 0) {
	thError("%s: ERROR - could not empty chain before repopulating it", name);
  	return(SH_GENERIC_ERROR);
 }
   
   for (i = 0; i < n; i++) {
  	 shChainElementAddByPos(chain, arr[i], varname, TAIL, AFTER);
   }  
   return(SH_SUCCESS);
}


RET_CODE thCanonizeObjmask(
                  OBJMASK *sv,          /* OBJMASK to work on */
                  int nearly_sorted     /* is sv almost sorted already? */
                  ) {
char *name = "thCanonizeObjmask";
if (!(sv->rmin >= -SORTKEY_BIAS && sv->rmax < SORTKEY_BIAS &&
             sv->cmin >= -SORTKEY_BIAS && sv->cmax < SORTKEY_BIAS)) {
	int condition1 = (sv->rmin >= -SORTKEY_BIAS);
	int condition2 = (sv->rmax < SORTKEY_BIAS);
	int condition3 = (sv->cmin >= -SORTKEY_BIAS);
	int condition4 = (sv->cmax < SORTKEY_BIAS);
	
	if (!condition1) {
		thError("%s: ERROR - (sv, rmin = %d) does not pass SORTKEY_BIAS = %d test", name, (int) sv->rmin, (int) SORTKEY_BIAS);
	} else if (!condition2) {
		thError("%s: ERROR - (sv, rmax = %d) does not pass SORTKEY_BIAS = %d test", name, (int) sv->rmax, (int) SORTKEY_BIAS);
	} else if (!condition3) {
		thError("%s: ERROR - (sv, cmin = %d) does not pass SORTKEY_BIAS = %d test", name, (int) sv->cmin, (int) SORTKEY_BIAS);
	} else if (!condition4) {
		thError("%s: ERROR - (sv, cmax = %d) does not pass SORTKEY_BIAS = %d test", name, (int) sv->cmax, (int) SORTKEY_BIAS);
	} else {
		thError("%s: ERROR - binary issue and SORTKEY_BIAS problem", name);
	}
	return(SH_GENERIC_ERROR);
}

phCanonizeObjmask(sv, nearly_sorted);
return(SH_SUCCESS);
}


/*****************************************************************************/
/*
 *  * comparison function for qsort
 *   */
static int
compar(const void *pa, const void *pb)
{
   const OBJMASK *a = *(OBJMASK **)pa;
   const OBJMASK *b = *(OBJMASK **)pb;

   if(a->rmin < b->rmin) {
      return(-1);
   } else if(a->rmin == b->rmin) {
      return(a->cmin - b->cmin);
   } else {
      return(1);
   }
}

/*****************************************************************************/


RET_CODE
thCanonizeObjmaskChain(CHAIN *chain,    /* CHAIN of OBJMASKs to work on */
                       int canonize_objmasks, /* canonize each OBJMASK too? */
                       int nearly_sorted) /* are OBJMASKs almost sorted? */
{
   char *name = "thCanonizeObjmaskChain";
   CURSOR_T crsr;                       /* cursor for crChain */
   OBJMASK *om;                         /* an OBJMASK from the chain */
   RET_CODE status;  
   if (!(chain != NULL && chain->type == shTypeGetFromName("OBJMASK"))) {
	if (chain == NULL) {
		thError("%s: ERROR - null (chain) passed", name);
		return(SH_GENERIC_ERROR);
	} else if (chain->type != shTypeGetFromName("OBJMASK")) {
		thError("%s: ERROR - chain type is not 'OBJMASK'", name);
		return(SH_GENERIC_ERROR);
	} else {
		thError("%s: ERROR - binary and chain type error", name);
		return(SH_GENERIC_ERROR);
	}
   }
   if(canonize_objmasks) {
      crsr = shChainCursorNew(chain);
      while((om = shChainWalk(chain, crsr, NEXT)) != NULL) {
         status = thCanonizeObjmask(om, nearly_sorted);
	 if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not canonize (om)", name);
		return(status);
	}
      }
      shChainCursorDel(chain, crsr);
   }
/*
 *  * Sort by the lower-left corner of the bounding box, in Y then X
 *   */
   shChainQsort(chain, compar);
   return(SH_SUCCESS);
}



