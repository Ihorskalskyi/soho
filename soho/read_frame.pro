; source:
; http://www.sdss3.org/svn/repo/photoop/trunk/pro/image/read_frame.pro
;+
; NAME:
;   read_frame
; PURPOSE:
;   Read in corrected frame and apply attached calibrations
; CALLING SEQUENCE:
;   img= read_frame(run, camcol, field, rerun=, filter=, hdr=, $
;                   simg=, cimg=, /nosky, /nocalib)
;
; INPUTS:
;   run    - run number
;   camcol - camera column
;   field - field
;   rerun  - rerun name
;   filter - filter name ('u', 'g', 'r', 'i', 'z')
; OPTIONAL KEYWORDS:
;   nosky - restore the image to un-skysubtracted version
;   nocalib - restore the image to units of fpC (counts)
; OUTPUTS:
;   img - [2048, 1489] output image in nmgy per pixel, except
;         if /nocalib set, in which case of counts per pixel
;   hdr - header, including astrometry
;   simg - [2048, 1489] sky image (counts)
;   cimg - [2048, 1489] calibration image (nmgy/counts)
; COMMENTS:
;   Assumes that the appropriate image has been created by SDSS_FRAME, 
;     in the directory $BOSS_PHOTOOBJ/frames/[rerun]/[camcol]/[field]
;   Output image has calibrations, flats and sky-subtraction applied.
;   Note that these images have been
; REVISION HISTORY:
;   2009-Jun-03  Written by Mike Blanton, NYU
;
;----------------------------------------------------------------------
function read_frame, run, camcol, field, filter=filter, rerun=rerun, $
                     hdr=hdr, simg=simg, cimg=cimg, nosky=nosky, $
                     nocalib=nocalib

framename = (sdss_name('frame', run, camcol, field, $
                       filter=filternum(filter), rerun=rerun))[0]
framename= framename+'.bz2'

;; read in image
img= mrdfits(framename,0,hdr)
nrowc= (size(img,/dim))[1]

;; handle calibrations
if(keyword_set(nosky) gt 0 OR $
   arg_present(cimg) gt 0 OR $
   keyword_set(nocalib) gt 0) then begin

    ;; read in calibrations and restore
    calib= mrdfits(framename,1)
    cimg= calib#replicate(1.,nrowc)

    ;; restore to uncalibrated state if so desired
    if(keyword_set(nocalib) gt 0) then img= img/cimg
endif

;; handle sky
if(keyword_set(nosky) gt 0 OR $
   arg_present(simg) gt 0) then begin
    sky= mrdfits(framename,2)
    simg= interpolate(sky.allsky, sky.xinterp, sky.yinterp, /grid)
    if(keyword_set(nocalib) gt 0) then $
      img=img+simg $
    else $
      img=img+simg*cimg
endif

return, img

end


