
/* SDSS construction */
#include "dervish.h"
#include "phObjc.h"
#include "phPhotoFitsIO.h"
#include "phConsts.h"
/* SOHO Libs */
#include "sohoEnv.h"
#include "thDebug.h"

RET_CODE thCpFpObjc(char *infile, char *outfile);
CHAIN *thReadFpObjc(char *file, RET_CODE *status);

