#!/bin/bash

source "./write_condorjob.sh"
source "./write_condorshell_mledata.sh"
source "./write_parfile_mledata.sh"
source "./create_field_array.sh"

name="mle-data-script"
DATE=$(date +"%m-%d-%Y")
TIME=$(date +"%T") 


debug=no
verbose=no
multiband=no

lroot=no
lband=no
lcamcol=no
lrun=no
lrerun=no
lmodel=no
lcondordir=no
lsource_objcsdir=no
lsource_framesdir=no
lsource_psfdir=no
loutdir=no
lexecutable=no
lEXTRA_FLAGS=no
lcommandfile=no

droot="/u/khosrow/thesis/opt/soho"
dband="r"
dcamcol="4"
drun=$DEFAULT_SDSS_RUN
drerun=$DEFAULT_SDSS_RERUN
dmodel="deV"
dexecutable="do-mle"


while getopts ':-:' OPTION ; do
  case "$OPTION" in
    -  ) [ $OPTIND -ge 1 ] && optind=$(expr $OPTIND - 1 ) || optind=$OPTIND
         eval OPTION="\$$optind"
         OPTARG=$(echo $OPTION | cut -d'=' -f2)
         OPTION=$(echo $OPTION | cut -d'=' -f1)
         case $OPTION in
	     --debug     ) debug=yes			 ;;
     	     --verbose   ) verbose=yes			 ;;
	     --root      ) lroot=yes; root="$OPTARG"     ;;
     	     --run       ) lrun=yes; run="$OPTARG"	 ;;
	     --rerun     ) lrerun=yes; rerun="$OPTARG"    ;; 
	     --model     ) lmodel=yes; model="$OPTARG"    ;;
	     --camcol    ) lcamcol=yes; camcol="$OPTARG" ;;
	     --band      ) lband=yes; band="$OPTARG"	 ;;
             --condordir ) lcondordir=yes; condordir="$OPTARG" ;;     
	     --outdir	 ) loutdir=yes; outdir="$OPTARG" ;;
	     --source_objcsdir   ) lsource_objcsdir=yes; source_objcsdir="$OPTARG"     ;;
	     --source_framesdir  ) lsource_framesdir=yes; source_framesdir="$OPTARG";;
	     --source_psfdir	 ) lsource_psfdir=yes; source_psfdir="$OPTARG"	;;
	     --executable	) lexecutable=yes; executable="$OPTARG"		;;
	     --commandfile	) lcommandfile=yes; commandfile="$OPTARG"	;; 
	     --EXTRA_FLAGS	) lEXTRA_FLAGS=yes; EXTRA_FLAGS="$OPTARG"	;;
	     --multiband	) multiband=yes		;;  
	* )  echo "$name: invalide options ($OPTION)" ;;
         esac
       OPTIND=1
       shift
      ;;
    ? )  echo "$name: invalid options ($OPTION) "  ;;
  esac
done

if [ $lroot == no ]; then
	root=$droot
	echo "$name: argument (root)   set to '$root'"
fi
if [ $lrun == no ]; then
	run=$drun
	echo "$name: argument (run)    set to '$run'"
fi
if [ $lrerun == no ]; then
	rerun=$drerun;
	echo "$name: argument (rerun)  set to '$rerun'"
fi
if [ $lmodel == no ]; then
	model=$dmodel;
	echo "$name: argument (model) set to '$model'"
fi
if [ $lcamcol == no ]; then
	camcol=$dcamcol
	echo "$name: argument (camcol) set to '$camcol'"
fi
if [ $lband == no ]; then
	band=$dband
	echo "$name: argument (band)   set to '$band'"
fi
if [ $lsource_objcsdir == no ]; then
	echo "$name: argument (source_objcsdir) not provided"
	exit
fi
if [ $lsource_framesdir == no ]; then
	echo "$name: argument (source_framesdir) not provided"
	exit
fi
if [ $loutdir == no ]; then
	echo "$name: argument (outdir) not provided"
	exit
fi
if [ $lcondordir == no ]; then
	condordir="$outdir/condor"
	echo "$name: argument (condordir) set to '$condordir'"
fi
if [ $lexecutable == no ]; then
	echo "$name: argument (executable) was not passed"
	exit
fi
if [ $lcommandfile == yes ]; then
	if [[ -e $commandfile ]]; then
		echo "$name: WARNING - export file for commands '$commandfile' already exists. appending"
	fi
fi	
if [ $lEXTRA_FLAGS == no ]; then
	EXTRA_FLAGS=""
fi
multibandflag=""
if [ $multiband == yes ]; then
	multibandflag="--multibandmle"
fi

echo "$name: debug    = $debug"
echo "$name: verbose  = $verbose"
echo "$name EXTRA_FLAGS = '$EXTRA_FLAGS'"

commanddir="$outdir/cmd"
condordir="$outdir/condor"
clogdir="$condordir/log"
cerrordir="$condordir/error"
coutputdir="$condordir/output"
csubmitdir="$condordir/submit"
cshelldir="$condordir/shell"
objcdir="$outdir/objc"
parfiledir="$outdir/parfiles"
photodir="$outdir/photo"
tempdir="$outdir/temp"

if [ ! -d $commanddir ]; then
	echo "$name: making (commanddir) '$commanddir'"
	mkdir -p $commanddir
fi
if [ ! -d $condordir ]; then
	echo "$name: making (condordir) '$condordir'"
	mkdir -p $condordir
fi
if [ ! -d $clogdir ]; then
	echo "$name: making (clogdir) '$clogdir'"
	mkdir -p $clogdir
fi
if [ ! -d $cerrordir ]; then
	echo "$name: making (cerrordir) '$cerrordir'"
	mkdir -p $cerrordir
fi
if [ ! -d $coutputdir ]; then
	echo "$name: making (coutputdir) '$coutputdir'"
	mkdir -p $coutputdir
fi
if [ ! -d $csubmitdir ]; then
	echo "$name: making (csubmitdir) '$csubmitdir'"
	mkdir -p $csubmitdir
fi
if [ ! -d $cshelldir ]; then
	echo "$name: making (cshelldir) '$cshelldir'"
	mkdir -p $cshelldir
fi
if [ ! -d $objcdir ]; then
	echo "$name: making (objcdir) '$objcdir'"
	mkdir -p $objcdir
fi
if [ ! -d $parfiledir ]; then 
	echo "$name: making (parfiledir) '$parfiledir'"
	mkdir -p $parfiledir
fi
if [ ! -d $photodir ]; then
	echo "$name: making (photodir) '$photodir'"
	mkdir -p $photodir
fi
if [ ! -d $tempdir ]; then
	echo "$name: making (tempdir) '$tempdir'"
	mkdir -p $tempdir
fi
if [[ $unzip == yes && ! -d $unzipdir ]]; then 
	echo "$name: making (unzipdir) '$unzipdir'"
	mkdir -p $unzipdir
fi

tempfile="./temp-condor-submit.output"

ifield=$(( 10#$field0 ))
run=$(( 10#$run ))
runstr=`printf "%06d" $run`

#creating an array of available fields
field_array_string=$(create_field_array_mledata --datadir="$source_framesdir" --band=$band)
field_array=(${field_array_string//,/ })
nfield_array=${#field_array[@]}
if [[ $debug == alpha ]]; then  
	echo "$name: fields in string = $field_array_string"
	echo "$name: fields in array  = ${field_array[@]}"
fi
echo "$name: # of valid fields found = $nfield_array"

do_random_fields=false
njob=0
nbadjob=0
#while [[ $dataexists == yes ]] 
while [[ $nfield_array -gt 0 ]]
do
	#selecting a random field from the array of fields
	nfield_array=${#field_array[@]} 
	if [[ $nfield_array -gt 1 && "$do_random_fields" = true ]]; then 
		let nfield_array++
		irand=$(python -S -c "import random; print random.randrange(1,$nfield_array)")
		let nfield_array--
	else 
		irand=$nfield_array
	fi
	let irand--
	 ifield=${field_array[$irand]} #taking a random element from the field array
	 fieldstr=`printf "%04d" $ifield`
	if [ $multiband == yes ]; then 
	 parfilename="ff-$runstr-$camcol-$fieldstr.par"
	else
	 parfilename="ff-$runstr-$band$camcol-$fieldstr.par"
	fi
	 targetparfile="$outdir/parfiles/$parfilename"
	
	#condor job
	maxseed=4294967295 # the biggest unsigned int that can be passed as a seed to simulator 
	seed=$(python -S -c "import random; print(random.randrange(1,$maxseed))")
	 comment="'DATE: $DATE, TIME: $TIME, EXTRA_FLAGS: $EXTRA_FLAGS'"
	if [ $multiband == yes ]; then 
	 condorprefix="condor-mle-data-$model-$runstr-$camcol-$fieldstr"
	else
	 condorprefix="condor-mle-data-$model-$runstr-$band$camcol-$fieldstr"
	fi
	 condor_submitfile="$csubmitdir/$condorprefix.submit"
	 condor_shellfile="$cshelldir/$condorprefix.sh"
	 condor_errorfile="$cerrordir/$condorprefix.error"
	 condor_logfile="$clogdir/$condorprefix.log"
	 condor_outputfile="$coutputdir/$condorprefix.out"
	 condor_argument="--framefile $targetparfile -seed $seed $EXTRA_FLAGS"
	 condor_comment="mle-lrg data: date: $DATE, time: $TIME"

	 source_fpobjcdir="$source_psfdir"
	 source_fpBINdir="$source_psfdir"
	 source_photoobjcdir="$source_objcsdir"
	 target_objcdir=$objcdir
	 target_skydir=$objcdir
	 out_framesdir=$tempdir
	 out_fpBINdir=$tempdir
	 out_photoobjcdir=$tempdir
	 out_heasacdir=$tempdir

	#echo "$name: outdir = $outdir"
	#echo "$name: target_objcdir = $target_objcdir"
	#echo "$name: objcdir = $objcdir"
	
	write_parfile_mledata --framenew --source_framesdir="$out_framesdir" --source_psfdir="$source_psfdir" --source_fpobjcdir="$source_fpobjcdir" --source_photoobjcdir="$out_photoobjcdir" --source_fpBINdir="$out_fpBINdir" --source_fpMdir="$out_fpBINdir" --target_objcdir="$target_objcdir" --field=$ifield --run=$run --camcol=$camcol --band=$band --outdir="$outdir" --target_objcdir="$objcdir" --comment="$comment" --output="$targetparfile" $multibandflag	

	#echo "$name: (2)outdir = $outdir"
	#echo "$name: (2)target_objcdir = $target_objcdir"
	#echo "$name: (2)objcdir = $objcdir"

	write_condorshell_mledata --field=$ifield --run=$run --camcol=$camcol --shellfile=$condor_shellfile --comment="$condor_comment" --executable="$executable" --argument="$condor_argument" --inskydir="$source_fpBINdir" --outskydir="$out_fpBINdir"  --infpCDir="$source_framesdir" --outfpCDir="$out_framesdir" --incalibobjcdir="$source_photoobjcdir" --outcalibobjcdir="$out_photoobjcdir" --outheasacdir="$out_heasacdir" --outdir="$outdir" 

	#echo "$name: (3)outdir = $outdir"
	#echo "$name: (3)target_objcdir = $target_objcdir"
	#echo "$name: (3)objcdir = $objcdir"
	
	chmod u+x $condor_shellfile

	write_condorjob --restrict --submitfile=$condor_submitfile --errorfile=$condor_errorfile --logfile=$condor_logfile --outputfile=$condor_outputfile --executable="$condor_shellfile" --arguments="" --comment="$condor_comment"

	#echo "$name: (4)outdir = $outdir"
	#echo "$name: (4)target_objcdir = $target_objcdir"
	#echo "$name: (4)objcdir = $objcdir"
	

	if [ $debug == yes ]; then
		echo -n 'd'
	else 
 		if [ $lcommandfile == yes ]; then
			echo "condor_submit $condor_submitfile >> $tempfile" >> $commandfile
		else
			eval "condor_submit $condor_submitfile >> $tempfile"
		fi
		echo -n "."
	fi


	let njob++
	# now deleting the selected field from the list, and updating the array size, it is important to delete null array elements after deleting an element.
	unset field_array[$irand]
	field_array=(${field_array[@]})
	nfield_array=${#field_array[@]}
	
done

echo ""
echo "$name: $njob jobs submitted"
if [[ $nbadjob -gt 0 ]]; then
	echo "$name: $nbadjob cases of non-existent fpC / fpCC files found - jobs not submitted"
fi

