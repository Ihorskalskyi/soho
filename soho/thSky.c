#if THCLOCK  
#include "time.h"
#endif

#include "thSky.h"

RET_CODE thSkycoeffsFitSkybasisset(SKYCOEFFS *sc, /* sky basis coefficients and their covariance */ 
				   SKYBASISSET *sb,  /* sky function basis set */
				   REGION *wp, REGION *data, MASK *mask /* input poisson weight, image, mask */
				   )
  {
 
#if THCLOCK 
    clock_t clk1, clk2;
#endif

    char *name = "thSkycoeffsFitSkybasisset";
    RET_CODE thStatus;


    int bsize;

    if (sb == NULL) {
      thError("%s: No basis function supplied", name);
      return(SH_GENERIC_ERROR);
    }

    bsize = sb->bsize;

    if (bsize <= 0) {
      thError("%s: No basis function supplied", name);
      return(SH_GENERIC_ERROR);
    }

    if (sc == NULL) {
      sc = thSkycoeffsNew();
    }
    sc->bsize = bsize;
    
    THPIX **AMat, *cVec;
#if THCLOCK 
    printf("%s: <B_i | Wp | data> \n", name);
    clk1 = clock();
#endif
    /* obtain the constant c-vector */
    cVec = (THPIX *) thSkybasissetDotReg(sb, data, wp, mask, THSKYBIT);
    shAssert(cVec != NULL);
#if THCLOCK 
    clk2 = clock();
    printf("%s: operation completed in [%g] CPUs \n", 
	   name, ((float) (clk2 - clk1)) / CLOCKS_PER_SEC);
#endif
#if THCLOCK 
    printf("%s: <B_i | Wp | B_j > \n", name);
    clk1 = clock();
#endif
    /* obtain matrix A, the inverse covariance */
    AMat = (THPIX **) thSkybasissetMetric(sb, wp, mask);
    shAssert(AMat != NULL);
#if THCLOCK 
    clk2 = clock();
    printf("%s: operation completed in [%g] CPUs \n", 
	   name, ((float) (clk2 - clk1)) / CLOCKS_PER_SEC);
#endif
    /* copy the inverse of covarinace to sc */
    thStatus = thSkycoeffsCopy(sc, NULL, NULL, AMat, bsize);
    shAssert(thStatus == SH_SUCCESS);
    /* invert A-matrix */
    thStatus = thMathInit();
    shAssert(thStatus == SH_SUCCESS);
    thStatus = thMatrixInvert(AMat, cVec, bsize);
    shAssert (thStatus == SH_SUCCESS);
    /* copy maximum likelihood values and covariance to sc */
    thSkycoeffsPut(sc, cVec, AMat, NULL, NULL);

    return(SH_SUCCESS);

  }


    
RET_CODE thSkycoeffsRead(SKYCOEFFS *sc, PHFITSFILE *fits){
  /* this function reads the sc structure out of an already openned fits file 
     also ensures that the covariance matrix and its inverse are indeed related */

  char *name = "thSkycoeffsRead";

  thError("%s: Unsupported function", name);
  return(SH_GENERIC_ERROR);

}

RET_CODE thSkyCoeffWrite(PHFITSFILE *fits, SKYCOEFFS *sc){
  /* this function writes the given sc structure into an already openned fits file
   */

  char *name = "thSkycoeffsWrite";

  thError("%s: Unsupported function", name);
  return(SH_GENERIC_ERROR);
}




RET_CODE thSkymodelFit(SKYMODEL *sm, 
		       SKYBASISSET *sbs,
		       REGION *image, MASK *mask) {
 
  char *name = "thSkymodelFit";

  SKYCOEFFS *sc;
  RET_CODE thStatus;

  /* 
     poisson weight
     to be calculated based on data and gain info 
  */
  REGION *wp; 
  thStatus = thSkycoeffsFitSkybasisset(sc, sbs, 
				       wp, image, mask);
  if (thStatus != SH_SUCCESS) {
    thError("%s: Error calling thSkycoeffsFitSkybasis", name);
    return(thStatus);
  }
  thStatus = thSkymodelPut(sm, sbs, sc, NULL);
  if (thStatus != SH_SUCCESS) {
    thError("%s: Error calling thSkymodelPut", name);
  }
  return(thStatus);
}
  

RET_CODE thSkymodelReconstruct(SKYMODEL *sm,  /* structure containing the  Sky Model */
			       SKYBASISSET *sbs, /* structure containing the sky basis set */ 
			       SKYCOEFFS *sc /* structure containing the sky expansion coefficients 
							and their covarinace */
			       ) {

  char *name = "thSkymodelRecostruct";
  REGION *sky, *cbasis;
  SKYBASIS *basis;

  int i, bsize;
  int row0, col0, row1, col1, nrow, ncol;
  THPIX c;

  RET_CODE thStatus = SH_SUCCESS;

  bsize = sbs->bsize;
  nrow = sbs->nrow;
  ncol = sbs->ncol;

  if (sm->reg != NULL && sm->reg->nrow == nrow && sm->reg->ncol == ncol) {
    sky = sm->reg;
  } else if (sm->reg == NULL) {
    sky = shRegNew("reconstructed continuous sky", nrow, ncol, TYPE_THPIX);
    sm->reg = sky;
  } else if (sm->reg->nrow == 0  && sm->reg->ncol == 0) {
    sky = sm->reg;
    sky->rows_thpix = (THPIX **) thMalloc(nrow * sizeof(THPIX *));
    for (i = 0; i < ncol; i++) {
      sky->rows_thpix[i] = (THPIX *) thMalloc(ncol * sizeof(THPIX));
    }
  } else {
    thError("%s: (sms->reg) and (sbs) have non-mathcing (nrow = %d, %d), (ncol = %d, %d)",
		   name, sm->reg->nrow, nrow, sm->reg->ncol, ncol);
    return(SH_GENERIC_ERROR);
  }

  cbasis = shRegNew("c x basis", nrow, ncol, TYPE_THPIX);
  for (i = 0; i < bsize; i++) {
    basis = (SKYBASIS *) shChainElementGetByPos(sbs->basis, i);
    c = sc->c[i];

    if (basis->type == TYPE_FUNC) {
      thRegCrossScalar(c, ((FUNC *)basis->val)->reg, cbasis);
      thRegAddReg(cbasis, sky, sky);
    } else if (basis->type == TYPE_AMPL) {
      row0 = ((AMPL *)basis->val)->row0;
      row1 = ((AMPL *)basis->val)->row1;
      col0 = ((AMPL *)basis->val)->col0;
      col1 = ((AMPL *)basis->val)->col1;
      thRegAddScalar(c, sky, sky, row0, row1, col0, col1);
    } else {
      thError("%s: Unsupported Basis Type: %d", name, i);
      thStatus = SH_GENERIC_ERROR;
    }
  }
  
  shRegDel(cbasis);
  sm->reg = sky;
  
  return(thStatus);

}
  

RET_CODE thSkymodelRead(SKYMODEL *sm, PHFITSFILE *fits) {

  char *name = "thSkymodelRead";

  thError("%s: Unsupported function", name);

  return(SH_GENERIC_ERROR);
}

RET_CODE thSkymodelWrite(PHFITSFILE *fits, SKYMODEL *SkyModel) {

  char *name = "thSkymodelWrite";

  thError("%s: Unsupported funxction", name);

  return(SH_GENERIC_ERROR);

}

THPIX thSkybasisDotReg(SKYBASIS *b, REGION *reg, 
		       REGION *wp, MASK *mask, /* wp and mask are optional
						    will be 1 if NULL */
		       unsigned char maskbit) 
{
      
      char *name ="thSkybasisDotReg";
      
      shAssert(b != NULL && reg != NULL);
      
      THPIX res;
      
      if (b->type == TYPE_FUNC) {
	
	res = thRegDotReg(((FUNC*) b->val)->reg, reg, wp, mask, maskbit);
	
	return(res);
      } else if (b->type == TYPE_AMPL) {
	
	AMPL *ampl;
	ampl = (AMPL *) b->val;
	res = thRegSum(reg, 
		       ampl->row0, ampl->row1, ampl->col0, ampl->col1, 
		       wp, mask, maskbit);
      } else {
	
	thError("%s: Unknown basis type", name);
	res = (THPIX) 0.0;
      }
      
      return(res);
      
}



THPIX **thSkybasissetMetric(SKYBASISSET *sbs, /* input sky basis */ 
			   REGION *wp, MASK *mask /* poisson statistical weights and the mask */
			   ) 
{
    
  char *name = "thSkybasisMetric";
  int i, j, bsize;

  THPIX **metric = NULL;

  SKYBASIS *sb1, *sb2;

  if (sbs == NULL) {
    thError("%s: SKYBASISSET was NULL", name);
    return(metric);
  }
  bsize = sbs->bsize;
  
  metric = (THPIX **) thMalloc(bsize * sizeof(THPIX *));
  for (i = 0; i < bsize; i++){
    metric[i] = (THPIX *) thMalloc(bsize * sizeof(THPIX));
  }

  for(i = 0; i < bsize; i++) {
    sb1 = shChainElementGetByPos(sbs->basis, i);
    for (j = i; j < bsize; j++) {
      sb2 = (SKYBASIS *) shChainElementGetByPos(sbs->basis, j);
      metric[i][j] = thSkybasisDotSkyBasis(sb1, sb2, wp, mask, THSKYBIT);
      metric[j][i] = metric[i][j];  /* check if this need rearrangement to fit the cache */
    }
  }

  return(metric);
 }

 THPIX *thSkybasissetDotReg(SKYBASISSET *sb,
			     REGION *reg, 
			    REGION *wp, MASK *mask, unsigned char maskbit) {
   int i, bsize;
   SKYBASIS *b;

   bsize = sb->bsize;

   THPIX *vec = NULL;

   vec = (THPIX *) thMalloc(bsize * sizeof(THPIX));
   
   for (i = 0; i < bsize; i++) {
     b = (SKYBASIS *) shChainElementGetByPos(sb->basis, i);
     vec[i] = thSkybasisDotReg(b, reg, wp, mask, maskbit);
   }

   return(vec);

 }


THPIX thSkybasisDotSkyBasis(SKYBASIS *sb1, SKYBASIS *sb2, /* input sky basis */
			    REGION *wp, MASK *mask, /* input mask */
			    unsigned char maskbit)
{

  char *name = "thSkybasisDotSkyBasis";

  THPIX res;
  FUNC *func1, *func2;
  AMPL *ampl1, *ampl2;
  
  int row0, col0, row1, col1;

  shAssert((sb1 != NULL) && (sb2 != NULL));

  switch (sb1->type){
  case TYPE_FUNC:
    switch(sb2->type) {
    case TYPE_FUNC:

      /* both functions are provided in form of REGION */
      func1 = (FUNC *) (sb1->val);
      func2 = (FUNC *) (sb2->val);

      res = thRegDotReg(func1->reg, func2->reg, 
			wp, mask, maskbit);
      break;
    case TYPE_AMPL:
      /* the second basis function is an amplifier delta function */
      func1 = (FUNC *) (sb1->val);
      ampl2 = (AMPL *) (sb2->val);
      res = thRegSum(func1->reg, 
		     ampl2->row0, ampl2->row1, ampl2->col0, ampl2->col1,
		     wp, mask, maskbit); 
      break;
    default:
      thError ("%s: Unknown Sky Basis Type (sb2)", name);
    } 
    break;
  case TYPE_AMPL:
    ampl1 = (AMPL *) sb1->val;
    switch(sb2->type) {
    case TYPE_FUNC:
      func2 = (FUNC *) (sb2->val);
      res = thRegSum(func2->reg, 
		     ampl1->row0, ampl1->row1, ampl1->col0, ampl1->col1, 
		     wp, mask, maskbit); 
      break;
    case TYPE_AMPL:
      ampl2 = (AMPL *) sb2->val;
      row0 = MAX(ampl1->row0, ampl2->row0);
      row1 = MIN(ampl1->row1, ampl2->row1);

      col0 = MAX(ampl1->col0, ampl2->col0);
      col1 = MIN(ampl1->col1, ampl2->col1);
      
      
      /* both functions thCountMaskPix and shRegionSum 
	 return 0 if row > row1 or col0 > col1
      */
      if (wp == NULL) {
	res = (THPIX) thCountMaskPix(row0, row1, col0, col1, mask, maskbit);
      } else {
	res = thRegSum(wp, row0, row1, col0, col1, 
		       NULL, mask, maskbit);
      }
      break;
    default:
      thError ("%s: Unknown Sky Basis Type (sb2)", name);
    } 
    break;
  default:
    thError ("%s: Unknown Sky Basis Type (sb1)", name);
  }

  return(res);
}


 RET_CODE thSkybasissetAddFuncChain(SKYBASISSET *sbs, /* Basis Function Chain 
							 to be Altered */ 
				    CHAIN *func_chain /* Func Chain to be Added to 
							the Original Basis Function Chain */ 
				    ) 
 {

   char *name = "thSkybasissetAddFuncChain";

   if (func_chain == NULL) {
     thError("%s: func_chain is NULL - no change was made", name);
     return(SH_GENERIC_ERROR);
   }

   CHAIN *basis;
   SKYBASIS *basiselem;

   FUNC *func;
   int i, nfunc;

   if (sbs == NULL) {
     sbs = thSkybasissetNew();
   }
   if (sbs->basis == NULL) {
     sbs->basis = shChainNew("SKYBASIS");
   }

   basis = sbs->basis;
  
   nfunc = shChainSize(func_chain);

   for (i = 0; i < nfunc; i++) {
     func = (FUNC *) shChainElementGetByPos(func_chain, i);
     basiselem = thSkybasisGenFunc(func);
     if (basiselem != NULL) {
       shChainElementAddByPos(basis, basiselem, "SKYBASIS", TAIL, AFTER);
       sbs->bsize++;
     }
   }
       
     
   return (SH_SUCCESS);
 }
   
       
RET_CODE thSkybasissetAddFunc(SKYBASISSET *sbs, /* Basis Function Chain to be Altered */ 
			      FUNC *func /* Func Chain to be Added to 
					    the Original Basis Function Chain */ 
				    ) 
 {

   char *name = "thSkybasissetAddFunc";

   CHAIN *basis;
   SKYBASIS *basiselem;

   if (func != NULL) {

     if (sbs == NULL) {
       sbs = thSkybasissetNew();
     }
     if (sbs->basis == NULL) {
       sbs->basis = shChainNew("SKYBASIS");
     }
     
     basis = sbs->basis;
     
     basiselem = thSkybasisGenFunc(func);
     if (basiselem != NULL) {
       shChainElementAddByPos(basis, basiselem, "SKYBASIS", TAIL, AFTER);
       sbs->bsize++;
     } 
     return (SH_SUCCESS);
 
  } else {
     
     thError("%name: NULL passed as function, can't proceed", name);
     return (SH_GENERIC_ERROR);
   }
   
 }
 

RET_CODE thSkybasissetAddAmplChain(SKYBASISSET *sbs, /* Basis Function Set to be Altered */ 
				   CHAIN *ampl_chain /* Ampl Chain to be Added to 
						       the Original Basis Function Chain */ 
				    ) 
 {

   char *name = "thSkybasissetAddAmplChain";

   if (ampl_chain == NULL) {
     thError("%s: ampl_chain is NULL - no change will be made", name);
     return(SH_GENERIC_ERROR);
   }

   CHAIN *basis;
   SKYBASIS *basiselem;
   
   int i, nampl;
   AMPL *ampl;
   
   if (sbs == NULL) {
     sbs = thSkybasissetNew();
   }
   if (sbs->basis == NULL) {
     sbs->basis = shChainNew("SKYBASIS");
   }

   basis = sbs->basis;
  
   nampl = shChainSize(ampl_chain);

   for (i = 0; i < nampl; i++) {
     ampl = (AMPL *) shChainElementGetByPos(ampl_chain, i);
     basiselem = thSkybasisGenAmpl(ampl);
     if (basiselem != NULL) {
       shChainElementAddByPos(basis, basiselem, "SKYBASIS", TAIL, AFTER);
       sbs->bsize++;
     }
   }
       
     
   return (SH_SUCCESS);
 }
   
       
 RET_CODE thSkybasissetAddAmpl(SKYBASISSET *sbs, /* Basis Function Set to be Altered */ 
			       AMPL *ampl /* Amplifier region info to be Added to 
					     the Original Basis Function Set */ 
			       ) 
 {
   
   char *name = "thSkybasissetAddFunc";

   CHAIN *basis;
   SKYBASIS *basiselem;
   
   if (ampl != NULL) {
     
     if (sbs == NULL) {
       sbs = thSkybasissetNew();
     }
     if (sbs->basis == NULL) {
       sbs->basis = shChainNew("SKYBASIS");
     }
     
     basis = sbs->basis;
     
     basiselem = thSkybasisGenAmpl(ampl);
     if (basiselem != NULL) {
       shChainElementAddByPos(basis, basiselem, "SKYBASIS", TAIL, AFTER);
       sbs->bsize++;
     } 
     return (SH_SUCCESS);
     
   } else {
     
     thError("%s: NULL passed as function, can't proceed", name);
     return (SH_GENERIC_ERROR);
   }
   
 }
   
 SKYBASIS *thSkybasisGenAmpl(AMPL *Ampl) 
   {
     char *name = "thSkybasisGenAmpl";
 
     if (Ampl == NULL) {
       thError("%s: Warning (ampl) passed was null, returning null", name);
       return(NULL);
     }

     /* the following change is to adapt the 128 row overscan by SDSS
	note that the amplifiers in SDSS are fixed to columns rather than rows
	for a given column, all rows use the same amplifier

	one could solve for this using the fpc file image, but this makes the 
	whole process dependent on image and prohibits passing subregions
	(instead of full regions) as the image for processing

     */

     if (Ampl->row1 == RAWIMAGE_MAXROW) Ampl->row1 += PHOTO_OVERSCAN;

     SKYBASIS *b;
     
     b = thSkybasisNew();
     b->type = TYPE_AMPL;
     b->val = (void *) Ampl;

     return(b);
   }

 SKYBASIS *thSkybasisGenFunc(FUNC *func) 
   {
     char *name = "thSkybasisGenFunc";

     /* 
	since we cannot delete chain element without distrupting the
	chain structure, we can signal the code not to consider it 
	by nullifying the contents of a chain element. once the chain element
	is null, it is not going to be added to the skybasisset 
     */
     if (func == NULL) {
       thError("%s: Warning - (func) passed was null, returning null", name);
       return(NULL);
     }
     
     SKYBASIS *b;

     b = thSkybasisNew();
     b->type = TYPE_FUNC;
     b->val = (void *) func;
     
     return(b);
   }

RET_CODE thSse(REGION *model,
	       REGION *data, REGION *wp, MASK *mask,
	       unsigned char maskbit,
	       THPIX *res) {
  
  char *name = "thSse";

  if (model == NULL) {
    thError("%s: null (skymodel) passed", name);
    return(SH_GENERIC_ERROR);
  }

  if (data == NULL) {
    thError("%s: null (data) passed", name);
    return(SH_GENERIC_ERROR);
  }

  if ((model->nrow != data->nrow) || (model->ncol != data->ncol)) {
    thError("%s: data and model sizes do not match", name);
    return(SH_GENERIC_ERROR);
  }
  
  if (model->type != TYPE_THPIX) {
    thError("%s: unsupported pixel type in (model)", name);
    return(SH_GENERIC_ERROR);
  }

  if (data->type != TYPE_THPIX) {
    thError("%s, unsupported pixel type in (data)", name);
    return(SH_GENERIC_ERROR);
  }

  int nrow, ncol;
  nrow = data->nrow;
  ncol = data->ncol;
  
  REGION *error;
  error = shRegNew("error", nrow, ncol, TYPE_THPIX);


  if (thRegCrossScalar((THPIX) (-1.0), data, error) != SH_SUCCESS) {
    thError("%s: problem calculating the error terms", name);
    shRegDel(error);
    return(SH_GENERIC_ERROR);
  }

  if (thRegAddReg(error, model, error) != SH_SUCCESS) {
    thError("%s: problem subtracting model from data", name);
    shRegDel(error);
    return(SH_GENERIC_ERROR);
  }

  if (res == NULL) {
    thError("%s: memory needs to be allocated to (result)", name);
    shRegDel(error);
    return(SH_GENERIC_ERROR);
  }

  *res = thRegDotReg(error, error, wp, mask, maskbit);

  shRegDel(error);

  return(SH_SUCCESS);

}

BINREGION *phBinregionFromThBinregion(THBINREGION *source) {

char *name = "phBinregionFromThBinregion";
if (source == NULL) {
	thError("%s: ERROR - null input", name);
	return(NULL);
}

int nrow = NXNODE_THBINREGION;
int ncol = NYNODE_THBINREGION;
int bin = BINNODE_THBINREGION;
int sky_bit_shift = BITSHIFT_THBINREGION;
BINREGION *target = phBinregionNewFromConst(0.0, nrow, ncol, bin, bin, sky_bit_shift);

if (target == NULL) {
	thError("%s: ERROR - could not create a PHOTO binregion of approrpriate size", name);
	return(NULL);
}

int i, j;
for (i = 0; i < nrow; i++) {
	S32 *trows_i = target->reg->rows_s32[i];
	FL32 *srows_i = &(source->allsky[i][0]);
	for (j = 0; j < ncol; j++) {
		trows_i[j] = srows_i[j]*(1 << sky_bit_shift) + 0.5;
	}
}

return(target);
}

