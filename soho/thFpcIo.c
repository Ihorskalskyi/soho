
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "strings.h"
#include "dervish.h"
#include "sohoEnv.h"
#include "thDebug.h"

#define TYPE_FPC TYPE_U16
#define thCheckType 1 /* IN: Flag to signify if should match file and region data type ( = 1) */
#define thKeepType 0  /* IN: Flag to signify if new region should keep the type of the  
			 specified region and any read in pixels thould be converted to this type. */
#define FpcFileType STANDARD              /* FITS file Type: STANDARD, NONSTANDARD, IMAGE */
#define FpcFileDef  DEF_DEFAULT /* = DEF_NUM_OF_ELEMS    /* = DEF_NONE: thCFitsIo.h */
#define NAXIS_FPC 2
#define HDU_FPC 0
#define NROW_FPC 1489
#define NCOLUMN_FPC 2048

REGION *thRegReadFpcAll(const char *file,   
		     RET_CODE *thStatus);
REGION *thRegReadFpc(const char *file,  
		     const int row0,  const int column0,  
		     const int row1,  const int column1, 
		     RET_CODE *thStatus);

RET_CODE thWriteFpc(const char *file, 
		    const REGION *reg);

RET_CODE thCpFpc(const char *infile, const char *outfile, /* input and output filenames */
		       const int row0, const int column0, 
		       const int row1, const int column1 /* corners of the image in INFILE */);

/*****************************************************************************/

REGION *thRegReadFpcAll(const char *file,   
		     RET_CODE *thStatus)
{
  char *name = "thRegReadFpc";
  char *image="im";
  REGION *reg = NULL;
  
  /* dervish interfaces:
  RET_CODE shRegReadAsFits(REGION *a_regPtr, char *a_file, int a_checktype,
			   int a_keeptype, DEFDIRENUM a_FITSdef,
			   FILE *a_inPipePtr, int a_readtape, int naxis3_is_OK,
			   int hdu);
  RET_CODE thHdrReadAsFits(HDR *a_hdrPtr, char *a_file, DEFDIRENUM a_FITSdef,
			   FILE *a_inPipePtr, int a_readtape, int hdu);
  */
 
  //reg = shRegNew(image, nrow, ncolumn, TYPE_FPC);
 
  *thStatus = shRegReadAsFits(reg, file, thCheckType, thKeepType, FpcFileDef, NULL, 0, 0, HDU_FPC);
  if (*thStatus != SH_SUCCESS) {
    shRegDel(reg);
    shError("thRegReadFpc: Error %i ", *thStatus);
    shError("thRegReadFpc: cannot read the image in %s\n", file);
    *thStatus = SH_GENERIC_ERROR;
    return(NULL);
  } else {
    *thStatus = SH_SUCCESS;
    return(reg);
  }
  
}

REGION *thRegReadFpc(const char *file,  
		     const int row0,  const int column0,  
		     const int row1,  const int column1, 
		     RET_CODE *thStatus)
{
  char *name = "thRegReadFpc";
  char *image="im";
  int nrow,  ncolumn;
  REGION *sub = NULL, *fullreg = NULL, *reg = NULL;

  nrow = row1 - row0 + 1;
  ncolumn = column1 - column0 + 1;
  
  /* dervish interfaces:
  RET_CODE shRegReadAsFits(REGION *a_regPtr, char *a_file, int a_checktype,
			   int a_keeptype, DEFDIRENUM a_FITSdef,
			   FILE *a_inPipePtr, int a_readtape, int naxis3_is_OK,
			   int hdu);
  RET_CODE thHdrReadAsFits(HDR *a_hdrPtr, char *a_file, DEFDIRENUM a_FITSdef,
			   FILE *a_inPipePtr, int a_readtape, int hdu);
  */
 
  reg = shRegNew(image, nrow, ncolumn, TYPE_FPC);
  fullreg = shRegNew(image, NROW_FPC, NCOLUMN_FPC, TYPE_FPC);
 
  *thStatus = shRegReadAsFits(fullreg, file, thCheckType, thKeepType, FpcFileDef, NULL, 0, 0, HDU_FPC);
  if (*thStatus != SH_SUCCESS) {
    shRegDel(fullreg);
    shError("thRegReadFpc: Error %i ", *thStatus);
    shError("thRegReadFpc: cannot read the image in %s\n", file);
    *thStatus = SH_GENERIC_ERROR;
    return(NULL);
    }
  
  /* defining a sub-image and making a new, separate, image from it */
  if ((sub = shSubRegNew(image, fullreg, nrow, ncolumn, row0, column0, COPY_HEADER_DEEP)) == NULL){
    shError("thRegReadFpc: cannot trim region %s\n", file);
    *thStatus = SH_GENERIC_ERROR;
    return(NULL);
  }
  reg = shRegNewFromReg(sub, image, TYPE_FPC, COPY_HEADER_DEEP, thStatus);

  shRegDel(sub);
  shRegDel(fullreg);

  if (*thStatus != SH_SUCCESS) {
    return(NULL);
  } 
  
  /* successful run */
  *thStatus = SH_SUCCESS;
  return(reg);
}


RET_CODE thWriteFpc(const char *file, 
		    const REGION *reg)
{

  char *name = "thWriteFpc";
  RET_CODE thStatus;
  
  /* Write the REGION */

  if (reg != NULL){
    thStatus = shRegWriteAsFits (reg, file, FpcFileType, NAXIS_FPC, FpcFileDef, NULL, 0);
    if (thStatus != SH_SUCCESS) {
      return(thStatus);
    }
  }
       
  return(SH_SUCCESS);
}
  

RET_CODE thCpFpc(const char *infile, const char *outfile, /* input and output filenames */
		 const int row0, const int column0, 
		 const int row1, const int column1 /* corners of the image in INFILE */) 
{
  
  char *name = "thCpFpc_debug";
  REGION *reg = NULL;
  HDR *header = NULL;
  RET_CODE thStatus;

  reg = thRegReadFpc(infile, row0, column0, row1, column1, &thStatus);
  if (thStatus != SH_SUCCESS) {
    shRegDel(reg);
    return(thStatus);
  }
  thStatus = thWriteFpc(outfile, reg);

  if (header != NULL){
    shHdrDel(header);
  }
  if (reg != NULL) {
    shRegDel(reg);
  }
  return(thStatus);
}
