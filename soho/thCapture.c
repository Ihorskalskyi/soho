#include "time.h"
#include "phLibrary.h"
#include "phWallpaper.h"
#include "thObjcTypes.h"
#include "thCalib.h"
#include "thMap.h"
#include "thMle.h"
#include "thCapture.h"
#include "thDebug.h"

static RET_CODE  construct_file_fmt(char *dir, char *prefix, char *suffix, FRAMEID *id, char *fmt);

static RET_CODE construct_filename(char *dir, char *prefix, char *suffix, FRAMEID *id, char *file);

static RET_CODE capture_filename(char *fmt, int icapture, char *file);

static RET_CODE region_capture_stat(REGION *reg, CAPTURE_STAT *stat);

static RET_CODE export_capture_stat_chain(char *file, CHAIN *chain);

static RET_CODE export_pChainOfChains(char *file, CHAIN *pChainOfChains);

RET_CODE thCaptureInit(CAPTURE *capture, FRAME *f, int fitid, CAPTURE_MODE cmode) {
char *name = "thCaptureInit";
if (capture == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

if (f == NULL) {
	thError("%s: ERROR - cannot initialize (capture) with a null (lstruct)", name);
	return(SH_GENERIC_ERROR);
}

if (!(cmode & CAPTURE_ALL)) {
	thError("%s: ERROR - (cmode = '0x%8x' does not seem to include any of the capture modes", name, cmode);
	return(SH_GENERIC_ERROR);
}
#if DEBUG_CAPTURE
printf("%s: initiating (capture) \n", name);
#endif

RET_CODE status;
LSTRUCT *lstruct = NULL;
status = thFrameGetLstruct(f, &lstruct);

capture->lstruct=lstruct;
capture->frame = f;

FRAMEID *id = NULL;
status = thFrameGetFrameid(f, &id);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (id) from (frame)", name);
	return(status);
}
capture->id = id;

#if DEEP_DEBUG_CAPTURE
printf("%s: (capture) filenames and file formats \n", name);
#endif

FRAMEFILES *ff = NULL;
status = thFrameGetFramefiles(f, &ff);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (framefiles)", name);
	return(status);
}
char *outdir = NULL;
status = thFramefilesGetOutdir(ff, &outdir);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (outdir) from (frame)", name);
	return(status);
}

/* graphic file prefix */
char *gprefix[N_CAPTURE_SECTOR];
gprefix[CAPTURE_MODEL] = "mvModel";
gprefix[CAPTURE_SKY] = "mvSky";
gprefix[CAPTURE_OBJC] = "mvObjc";
gprefix[CAPTURE_RES] = "mvRes";
/* stat file prefix */
char *sprefix[N_CAPTURE_SECTOR];
sprefix[CAPTURE_RES] = "stRes";
sprefix[CAPTURE_MODEL] = "stModel";
sprefix[CAPTURE_SKY] = "stSky";
sprefix[CAPTURE_OBJC] = "stObjc";
/* par chain output file */
char *parprefix = "stP";
char *suffix = "fits";

CAPTURE_SECTOR sector;
for (sector = 0; sector < N_CAPTURE_SECTOR; sector++) {
	char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", sector);
	char *fmt = NULL;
	status = thCaptureGetFmtBySector(capture, sector, &fmt);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (fmt) string for (sector = '%s') in capture", name, sname);
		return(status);
	}
	status = construct_file_fmt(outdir, gprefix[sector], suffix, id, fmt);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not construct (fmt) for (sector = '%s') in capture", name, sname);
		return(status);
	}	
}
char *u8suffix = "pgm";
for (sector = 0; sector < N_CAPTURE_SECTOR; sector++) {
	char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", sector);
	char *u8fmt = NULL;
	status = thCaptureGetU8FmtBySector(capture, sector, &u8fmt);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (fmt) string for (sector = '%s') in capture", name, sname);
		return(status);
	}
	status = construct_file_fmt(outdir, gprefix[sector], u8suffix, id, u8fmt);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not construct (fmt) for (sector = '%s') in capture", name, sname);
		return(status);
	}	
}


suffix = "dat";
for (sector = 0; sector < N_CAPTURE_SECTOR; sector++) {
	char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", sector);
	char *file = NULL;
	status = thCaptureGetSFileBySector(capture, sector, &file);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (sfile) string for (sector = '%s') in (capture)", name, sname);
		return(status);
	}
	status = construct_filename(outdir, sprefix[sector], suffix, id, file);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not construct (sfilename) for (sector = '%s')", name, sname);
		return(status);
	}
}

status = construct_filename(outdir, parprefix, suffix, id, capture->fpar);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not construct (parameter chain) filename", name);
	return(status);
}

suffix = "sub";
for (sector = 0; sector < N_CAPTURE_SECTOR; sector++) {
	char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", sector);
	char *file = NULL;
	status = thCaptureGetSubfileBySector(capture, sector, &file);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (subfile) string for (sector = '%s') in (capture)", name, sname);
		return(status);
	}
	status = construct_filename(outdir, gprefix[sector], suffix, id, file);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not construct (subfilename) for (sector = '%s')", name, sname);
		return(status);
	}
}


#if VERBOSE_CAPTURE
printf("%s: outdir   = '%s' \n", name, outdir);
printf("%s: formats: \n", name);
int gcapture[N_CAPTURE_SECTOR];
gcapture[CAPTURE_RES] = GCAPTURE_RES;
gcapture[CAPTURE_OBJC] = GCAPTURE_OBJC;
gcapture[CAPTURE_MODEL] = GCAPTURE_MODEL;
gcapture[CAPTURE_SKY] = GCAPTURE_SKY;
for (sector = 0; sector < N_CAPTURE_SECTOR; sector++) {
	if (cmode & gcapture[sector]) {
		char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", sector);
		char *fmt = NULL;
		status = thCaptureGetFmtBySector(capture, sector, &fmt);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (fmt) string for (sector = '%s')", name, sname);
			return(status);
		}
		printf("%s: sector - '%s' ... '%s' \n", name, sname, fmt);
		FILE *stream; char *subfile;
		status = thCaptureGetSubfileBySector(capture, sector, &subfile);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get subtitle filename for (sector = '%s') in capture", name, sname);
			return(status);
		}
		stream = fopen(subfile, "w");
		status = thCapturePutSubStreamBySector(capture, sector, stream);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not put subtitle stream for (sector = '%s') in capture", name, sname);
			return(status);
		}	
	}
}
printf("%s: filenames \n", name);
int pcapture[N_CAPTURE_SECTOR];
pcapture[CAPTURE_RES] = PCAPTURE_RES;
pcapture[CAPTURE_OBJC] = PCAPTURE_OBJC;
pcapture[CAPTURE_MODEL] = PCAPTURE_MODEL;
pcapture[CAPTURE_SKY] = PCAPTURE_SKY;
for (sector = 0; sector < N_CAPTURE_SECTOR; sector++) {
	if (cmode & pcapture[sector]) {
		char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", sector);
		char *file = NULL;
		status = thCaptureGetSFileBySector(capture, sector, &file);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (file) string for (sector = '%s')", name, sname);
			return(status);
		}
		printf("%s: sector - '%s' ... '%s' \n", name, sname, file);
	}
}
if (cmode & PCAPTURE_PAR) {
	printf("%s: sector - 'PARS' ... '%s' \n", name, capture->fpar);
}
#endif

LINITMODE initmode = UNKNOWN_LINITMODE;
status = thLstructGetInitmode(lstruct, &initmode);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (initmode) from (lstruct)", name);
	return(status);
}
capture->initmode=initmode;

LIMAGEMODE imagemode = UNKNOWN_MLEIMAGE;
status = thLstructGetImagemode(lstruct, &imagemode);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (mleimagemode) from (lstruct)", name);
	return(status);
}
capture->imagemode=imagemode;

FRAMEDATA *fdata = NULL;
status = thFrameGetFramedata(f, &fdata);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (framedata) from (frame)", name);
	return(status);
}

REGION *fimage = NULL;
status = thFrameGetLdataPars(f, &fimage, NULL, NULL, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (flat image) from (framedata)", name);
	return(status);
}
if (fimage == NULL) {
	thError("%s: ERROR - null (data) image in (frame) - improperly allocated (frame)", name);
	return(SH_GENERIC_ERROR);
}
capture->fimage = fimage;
int nrow, ncol;
nrow = fimage->nrow;
ncol = fimage->ncol;

/* getting the header from frame */
HDR *hdr = NULL;
status = thFrameGetHdr(f, &hdr);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (hdr) from (frame)", name);
	return(status);
}


/* note that because of the statistical analysis always requires INT regions */
REGION *ireg = shRegNew("output region in U16 for (capture)", nrow, ncol, TYPE_U16);
capture->ireg = ireg;
status = shHdrCopy(hdr, &ireg->hdr);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not copy (hdr) from (frame) to (ireg)", name);
	return(status);
}
/* you need to add the proper soft bias keyword to header */
char *keyword = "SOFTBIAS";
char *comment = "software ""bias"" added to all DN";
status = shHdrInsertInt(hdr, keyword, SOFT_BIAS, comment);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not insert the value for keyword '%s' in (int reg) of (capture)", name, keyword);
	return(status);
}

int capture_reg[N_CAPTURE_SECTOR];
capture_reg[CAPTURE_SKY] = (GCAPTURE_SKY | PCAPTURE_SKY);
capture_reg[CAPTURE_OBJC] = (GCAPTURE_OBJC | PCAPTURE_OBJC);
capture_reg[CAPTURE_MODEL] = (GCAPTURE_MODEL | PCAPTURE_MODEL | GCAPTURE_RES | PCAPTURE_RES);
capture_reg[CAPTURE_RES] = (GCAPTURE_RES | PCAPTURE_RES);
/* initiating ldump for each of the sector */
for (sector = 0; sector < N_CAPTURE_SECTOR; sector++) {
	if (cmode & capture_reg[sector]) {
		char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", sector);
		LSTRUCT *ldump = NULL;
		status = thCaptureGetLBySector(capture, sector, &ldump);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (ldump) for (sector = '%s') in (capture)", name, sname);
			return(status);
		}
		switch (sector) {
		case CAPTURE_MODEL:
		case CAPTURE_SKY:
		case CAPTURE_OBJC:
			status = make_ldump_by_sector(lstruct, sector, ldump);	
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not make (ldump) for (sector = '%s')", name, sname);
				return(status);
			}
			break;
		case CAPTURE_RES:
			break;
		default:
			thError("%s: ERROR - unsupported (sector = '%s')", name, sname);
			return(SH_GENERIC_ERROR);
			break;
		}
		REGION *reg = NULL;
		if (sector != CAPTURE_RES) {
			status = thLstructGetMN(ldump, &reg);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get (mN) from (ldump) for (sector = '%s')", name, sname);
				return(status);
			}
		} else {
			reg = shRegNew("residual = data - model", nrow, ncol, TYPE_THPIX);
		}
		status = shHdrCopy(hdr, &reg->hdr);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not copy (hdr) to (reg) for (sector = '%s')", name, sname);
			return(status);
		}
		status = thCapturePutRegBySector(capture, sector, reg);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not put the (reg) for (sector = '%s') in place in (capture)", name, sname);
			return(status);
		}	
	}
}

/* graphical parameters */
capture->gbin = CAPTURE_BIN;
capture->growc = 0;
capture->gcolc = 0;
capture->gnrow = UNCHANGED_NROWCOL;
capture->gncol = UNCHANGED_NROWCOL;

/* should we set the chains to init values ? */

capture->cmode = cmode;
capture->ncapture=0;
capture->nmle=0;
capture->cflag = CAPTURE_INIT;

return(SH_SUCCESS);
}


RET_CODE thCapture(CAPTURE *capture) {
char *name = "thCapture";
if (capture == NULL) {
	thError("%s: ERROR - cannot capture with null input", name);
	return(SH_GENERIC_ERROR);
}
if (capture->cflag != CAPTURE_INIT) {
	thError("%s: ERROR - (capture) not properly initialized", name);
	return(SH_GENERIC_ERROR);
}

#if DEBUG_CAPTURE
printf("%s: capturing ... \n", name);
#endif

RET_CODE status;
CAPTURE_MODE cmode;
status = thCaptureGetCmode(capture, &cmode);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (capture_mode) from (capture)", name);
	return(status);
}
LSTRUCT *lstruct = NULL;
status = thCaptureGetLstruct(capture, &lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lstruct) from (capture)", name);
	return(status);
}

CHAIN *pchain = NULL;
status = thLstructCopyLastparchain(lstruct, &pchain, COPY_IOPROP);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not generate par chain from (lstruct)", name);
	return(status);
}
if (pchain == NULL) {
	thError("%s: ERROR - null parameter chain returned", name);
	return(SH_GENERIC_ERROR);
}
if (cmode & PCAPTURE_PAR) {
	CHAIN *pChainOfChain = NULL;
	status = thCaptureGetPChainOfChain(capture, &pChainOfChain);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (pChainOfChain) from (capture)", name);
		return(status);
	}
	if (pChainOfChain == NULL) {
		thError("%s: ERROR - null (pChainOfChain) - (capture) not properly initialized", name);
		return(SH_GENERIC_ERROR);
	}
	shChainElementAddByPos(pChainOfChain, pchain, "CHAIN", TAIL, AFTER);
}

/* 1. generate objects
   2. generate sky
   3. generate model
   4. generate diff
*/
/* create filenames */
int gcapture[N_CAPTURE_SECTOR];
gcapture[CAPTURE_MODEL] = GCAPTURE_MODEL;
gcapture[CAPTURE_OBJC] = GCAPTURE_OBJC;
gcapture[CAPTURE_SKY] = GCAPTURE_SKY;
gcapture[CAPTURE_RES] = GCAPTURE_RES;

int pcapture[N_CAPTURE_SECTOR];
pcapture[CAPTURE_OBJC] = PCAPTURE_OBJC;
pcapture[CAPTURE_SKY] = PCAPTURE_SKY;
pcapture[CAPTURE_MODEL] = PCAPTURE_MODEL;
pcapture[CAPTURE_RES] = PCAPTURE_RES;

int ncapture = capture->ncapture;
CAPTURE_SECTOR sector;
for (sector = 0; sector < N_CAPTURE_SECTOR; sector++) {
	if (cmode & gcapture[sector]) {
		char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", sector);
		char *filename = NULL, *fmt = NULL;
		status = thCaptureGetFmtBySector(capture, sector, &fmt);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (fmt) string for (sector = '%s') in (capture)", name, sname);
			return(status);
		}
		status = thCaptureGetRFileBySector(capture, sector, &filename);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (rfilename) string for (sector = '%s') in (capture)", name, sname);
			return(status);
		}
		status = capture_filename(fmt, ncapture, filename);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get create (rfilename) from (fmt) for (sector = '%s')", name, sname);
			return(status);
		}
	}
}



#if VERBOSE_CAPTURE
printf("%s: capturing run shot (%d) \n", name, capture->ncapture);
for (sector = 0; sector < N_CAPTURE_SECTOR; sector++) {
	if (gcapture[sector]  & cmode) {
		char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", sector);
		char *filename = NULL;
		status = thCaptureGetRFileBySector(capture, sector, &filename);
		printf("%s: '%s' ... %s \n", name, sname, filename);
	}
}
#endif

/* dump images */
/* create object
1. get lstruct in capture for non-linear objects
2. make model
3. copy model
*/	
int capture_reg[N_CAPTURE_SECTOR];
capture_reg[CAPTURE_SKY] = (GCAPTURE_SKY | PCAPTURE_SKY);
capture_reg[CAPTURE_OBJC] = (GCAPTURE_OBJC | PCAPTURE_OBJC);
capture_reg[CAPTURE_MODEL] = (GCAPTURE_MODEL | PCAPTURE_MODEL | GCAPTURE_RES | PCAPTURE_RES);
capture_reg[CAPTURE_RES] = (GCAPTURE_RES | PCAPTURE_RES);

status = thLDumpPar(lstruct, MODELTOIO);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump (par) from (model) onto (io) for (lstruct)", name);
	return(status);
}
status = thLDumpAmp(lstruct, XTOIO);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump (amp) from (x) onto (io) for (lstruct)", name);
	return(status);
}
status = thLDumpPar(lstruct, XNTOIO);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump (par) from (xN) onto (io) for (lstruct)", name);
	return(status);
}

/* 
status = thLDumpAmp(lstruct, XNTOMODEL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump (amp) from (xN) onto (model) for (lstruct)", name);
	return(status);
}
status = thLDumpPar(lstruct, XNTOMODEL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump (par) from (xN) onto (model) for (lstruct)", name);
	return(status);
}
*/
status = thLDumpCountsIo(lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (counts) in (ioprops) in (lstruct)", name);
	return(status);
}
status = thLDumpCov(lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump (variance) onto (io) for (lstruct)", name);
	return(status);
}

for (sector = 0; sector < N_CAPTURE_SECTOR; sector++) {
	char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", sector);
	if (cmode & capture_reg[sector]) {
		if (sector != CAPTURE_RES) {
			status = thCaptureMakeSectorModel(capture, sector);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not generate image for (sector = '%s')", name, sname);
				return(status);
			}
		} else {	
			REGION *data = NULL;
			status = thCaptureGetData(capture, &data);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get (data) region from (capture)", name);
				return(status);
			}
			REGION *res = NULL;
			status = thCaptureGetRegionBySector(capture, CAPTURE_RES, &res);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get (residual reg) from (capture)", name);
				return(status);
			}
			REGION *model = NULL;
			status = thCaptureGetRegionBySector(capture, CAPTURE_MODEL, &model);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get (model reg) from (capture)", name);
				return(status);
			}
			int nrow, ncol;
			nrow = data->nrow;
			ncol = data->ncol;
			if (nrow != model->nrow || ncol != model->ncol) {
				thError("%s: ERROR - (data: %d, %d) and (model: %d, %d) have different sizes", name, nrow, ncol, model->nrow, model->ncol);
				return(SH_GENERIC_ERROR);
			}
			if (nrow != res->nrow || ncol != res->ncol) {
				thError("%s: ERROR - (data: %d, %d) and (res: %d, %d) have different sizes", name, nrow, ncol, res->nrow, res->ncol);
				return(SH_GENERIC_ERROR);
			}
			shRegSub(data, model, res);
		}
	}
}
#if DEEP_DEBUG_CAPTURE
printf("%s: exporting images in (region)s to files \n", name);
#endif

REGION *ireg = NULL;
status = thCaptureGetIntReg(capture, &ireg); /* make sure the header is proper */
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (integer reg) from (capture)", name);
	return(status);
}
for (sector = 0; sector < N_CAPTURE_SECTOR; sector++) {
	if (cmode & gcapture[sector]) {	
		char *sname = shEnumNameGetFromValue("CAPTURE_STAT", sector);
		/* make the filename */
		char *fmt = NULL, *file = NULL;
		status = thCaptureGetFmtBySector(capture, sector, &fmt);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (fmt) string for (sector = '%s') from (capture)", name, sname);
			return(status);
		}
		status = thCaptureGetRFileBySector(capture, sector, &file);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (rfile) string for (sector = '%s') from (capture)", name, sname);
			return(status);
		}
		status = capture_filename(fmt, ncapture, file);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not create (rfilename) for (sector = '%s') in (icapture = %d)", name, sname, ncapture);
			return(status);
		}
		/* get the reg - remember the proper header should be copied during initiation */
		REGION *reg = NULL;
		status = thCaptureGetRegionBySector(capture, sector, &reg);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (region) for (sector = '%s') in (capture)", name, sname);
			return(status);
		}
		/* write reg into file with proper header */
		/* note that it might be necessary to write the region as integer */
		REGION *outreg = NULL;
		#if CAPTURE_INT
		if (sector == CAPTURE_RES) {
			shRegMultWithDbl((double) CAPTURE_BMAX, reg, reg);
			status = shRegIntCopy(ireg, reg);	
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not copy (reg) into (int reg) for (sector = '%s')", name, sname);
				return(status);
			}
			shRegMultWithDbl((double) 1.0 / CAPTURE_BMAX, reg, reg);
		} else {
			status = shRegIntCopy(ireg, reg);	
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not copy (reg) into (int reg) for (sector = '%s')", name, sname);
				return(status);
			}
		}
		outreg = ireg;
		#else
		outreg = reg;
		#endif
		status = shRegWriteAsFits(outreg, file, STANDARD, 2, DEF_NONE, NULL, 0);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not write (reg = '%s') onto (file = '%s') for (sector = '%s')", name, reg->name, file, sname);
			return(status);
		}
		#if CAPTURE_GRAPHICAL
		char *u8file = NULL, *u8fmt = NULL;
		status = thCaptureGetU8FileBySector(capture, sector, &u8file);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (u8file string) from (capture) for (sector = '%s')", name, sname);
			return(status);
		}
		status = thCaptureGetU8FmtBySector(capture, sector, &u8fmt);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (u8format) from (capture) for (sector = '%s')", name, sname);
			return(status);
		}
		status = capture_filename(u8fmt, ncapture, u8file);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not make (u8filename) for (sector = '%s')", name, sname);
			return(status);		
		}
		#if CAPTURE_INT
		#else	
		if (sector == CAPTURE_RES) {
			shRegMultWithDbl((double) CAPTURE_BMAX, reg, reg);
			status = shRegIntCopy(ireg, reg);	
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not copy (reg) into (int reg) for (sector = '%s')", name, sname);
				return(status);
			}
			shRegMultWithDbl((double) 1.0 / CAPTURE_BMAX, reg, reg);
		} else {
			status = shRegIntCopy(ireg, reg);	
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not copy (reg) into (int reg) for (sector = '%s')", name, sname);
				return(status);
			}
		}
		#endif
		REGION *u8reg = NULL;
		#if CAPTURE_USE_LUT	
		REGION *lut = NULL;
		status = thCaptureGetLUTBySector(capture, sector, &lut);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (lut map) from (capture) for (sector = '%s') at (ncapture = %d)", name, sname, ncapture);
			return(status);		
		}
		if (ncapture % CAPTURE_NLUT == 1) {
			status = thU16RegMakeLUT(ireg, lut);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not make (lut) from (ireg) for (sector = '%s')", name, sname);
				return(status);
			}
		}
		u8reg = phRegIntToU8LUT(ireg,lut);
		#else
		int base = -1, top = -1;
		if (ncapture > 0) {
			status = thCaptureGetBaseTopBySector(capture, sector, &base, &top);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get (base, top) from (capture) for (sector = '%s') at (ncapture = %d)", name, sname, ncapture);
				return(status);
			}
		} else if (ncapture == 0) {
			status = thU16RegGetBaseTop(ireg, &base, &top);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get (base, top) from (ireg) for (sector = '%s')", name, sname);
				return(status);
			}
			status = thCapturePutBaseTopBySector(capture, sector, base, top);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not put (base, top) back in (capture) for (sector = '%s')", name, sname);
				return(status);
			}
		}
		u8reg = phRegIntToU8Linear(ireg, base, top);	
		#endif
		int gbin, growc, gcolc, gnrow, gncol;
		status = thCaptureGetGInfo(capture, &gbin, &growc, &gcolc, &gnrow, &gncol);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get information for graphical output from (capture)", name);
			return(SH_GENERIC_ERROR);
		}
		REGION *bu8reg = NULL;
		status = thCaptureBinU8Reg(u8reg, gbin, growc, gcolc, gnrow, gncol, &bu8reg);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not bin graphical output", name);
			return(status);
		}
		if (phOnecolorPGMWrite(u8file, bu8reg) != 0) {
			thError("%s: ERROR - could not write image to graphic file '%s'", name, u8file);
			return(SH_GENERIC_ERROR);
		} 
		if (bu8reg != u8reg) {
			shRegDel(bu8reg);
		}
		shRegDel(u8reg);
		u8reg = NULL;
		#endif

	}
}

/* this is the object that should be used for creating the susbtitle */
LMACHINE *map = NULL;
status = thLstructGetLmachine(lstruct, &map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (map) from (lstruct)", name);
	return(status);
}
THOBJC *oneobjc = NULL, *skyobjc = NULL, **objcarr;
int nobjc = 0;
status = thMapGetObjcArr(map, NONLINEAR_MOBJCS, &objcarr, &nobjc);		
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get object array for non-linear fit", name);
	return(status);
}	
if (nobjc > 0 && objcarr != NULL) {
	oneobjc = objcarr[0];
}
status = thMapGetObjcArr(map, ALL_MSKY, &objcarr, &nobjc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get object array for sky", name);
	return(status);
}
if (nobjc > 0 && objcarr != NULL) {
	skyobjc = objcarr[0];
}

for (sector = 0; sector < N_CAPTURE_SECTOR; sector++) {
	if (cmode & pcapture[sector]) {	
		char *sname = shEnumNameGetFromValue("CAPTURE_STAT", sector);
		/* get the reg - remember the proper header should be copied during initiation */
		REGION *reg = NULL;
		status = thCaptureGetRegionBySector(capture, sector, &reg);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (region) for (sector = '%s') in (capture)", name, sname);
			return(status);
		}
		THPIX bmax = 1.0;	
		if (sector == CAPTURE_RES) {
			bmax = CAPTURE_BMAX;
			shRegMultWithDbl((double) CAPTURE_BMAX, reg, reg);
			status = shRegIntCopy(ireg, reg);	
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not copy (reg) into (int reg) for (sector = '%s')", name, sname);
				return(status);
			}	
			shRegMultWithDbl((double) 1.0 / CAPTURE_BMAX, reg, reg);
		} else {
			status = shRegIntCopy(ireg, reg);	
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not copy (reg) into (int reg) for (sector = '%s')", name, sname);
				return(status);
			}	
		}
		CHAIN *chain = NULL;
		status = thCaptureGetStatChainBySector(capture, sector, &chain);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (StatChain) for (sector = '%s') from (capture)", name, sname);
			return(status);
		}
		CAPTURE_STAT *stat = thCaptureStatNew();
		status = region_capture_stat(ireg, stat);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not extract (capture_stat) from (region) for (sector = '%s')", name, sname);
			return(status);
		}
		stat->bmax = bmax;
		shChainElementAddByPos(chain, stat, "CAPTURE_STAT", TAIL, AFTER);
		FILE *subfil = NULL;
		status = thCaptureGetSubStreamBySector(capture, sector, &subfil);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get subtitle stream for (sector = '%s')", name, sname);
			return(status);
		}
		THOBJC *subtitleobjc = NULL;
		if (sector ==  ALL_MSKY) {
			subtitleobjc = skyobjc;
		} else {
			subtitleobjc = oneobjc;
		}
		status = thCaptureWriteSubtitle(subfil, capture->ncapture, stat, subtitleobjc);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not write subtitle for (sector = '%s')", name, sname);
			return(status);
		}

	}
}

#if VERBOSE_CAPTURE
printf("%s: end of capture (%d) \n", name, capture->ncapture);
#endif

capture->ncapture++;
return(SH_SUCCESS);
}


RET_CODE thCaptureFini(CAPTURE *capture) {
char *name = "thCaptureFini";
if (capture == NULL) {
	thError("%s: ERROR - cannot finalize null input", name);
	return(SH_GENERIC_ERROR);
}
#if DEBUG_CAPTURE
printf("%s: finilizing (capture) \n", name);
#endif

RET_CODE status;
LSTRUCT *lstruct = NULL;
status = thCaptureGetLstruct(capture, &lstruct);

int nmle = -1;
CONVERGENCE convergence = UNKNOWN_CONVERGENCE;
status = thLstructGetConvergence(lstruct, &nmle, &convergence, NULL);

int i;
for (i = 0; i < N_CAPTURE_SECTOR; i++) {
	FILE *stream = NULL;	
	char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", i);
	status = thCaptureGetSubStreamBySector(capture, i, &stream);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get subtitle stream for (sector = '%s')", name, sname);
		return(status);
	}
	if (stream != NULL) fclose(stream);
	status = thCapturePutSubStreamBySector(capture, i, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not free subtitle stream for (sector = '%s')", name, sname);
		return(status);
	}
}

capture->convergence = convergence;
capture->nmle = nmle;
capture->cflag = CAPTURE_FINI;
return(SH_SUCCESS);
}

RET_CODE thCaptureExport(CAPTURE *capture) {
char *name = "thCaptureExport";
if (capture == NULL) {
	thError("%s: ERROR - cannot export null input", name);
	return(SH_GENERIC_ERROR);
}

if (capture->cflag != CAPTURE_FINI) {
	thError("%s: ERROR - cannot export if (capture) is not finalized", name);
	return(SH_GENERIC_ERROR);
}
#if DEBUG_CAPTURE
printf("%s: exporting (capture) \n", name);
#endif

RET_CODE status;
CAPTURE_MODE cmode;
status = thCaptureGetCmode(capture, &cmode);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (cmode) from (capture)", name);
	return(status);
}

/* export rStat's */

int pcapture[N_CAPTURE_SECTOR];
pcapture[CAPTURE_MODEL] = PCAPTURE_MODEL;
pcapture[CAPTURE_OBJC] = PCAPTURE_OBJC;
pcapture[CAPTURE_RES] = PCAPTURE_RES;
pcapture[CAPTURE_SKY] = PCAPTURE_SKY;

CAPTURE_SECTOR sector;
for (sector = 0; sector < N_CAPTURE_SECTOR; sector++) {
	if (cmode & pcapture[sector]) {
		char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", sector);
		char *file = NULL;
		status = thCaptureGetSFileBySector(capture, sector, &file);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (sfilename) for (sector = '%s') from (capture)", name, sname);
			return(status);
		}
		CHAIN *chain = NULL;
		status = thCaptureGetStatChainBySector(capture, sector, &chain);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (stat chain) for (sector = '%s') from (capture)", name, sname);
			return(status);
		}	
		#if DEEP_DEBUG_CAPTURE
		printf("%s: exporting statistics for (sector = '%s') to file '%s' \n", name, sname, file);
		#endif
		status = export_capture_stat_chain(file, chain);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not export (stat chain) for (sector = '%s') to (file = '%s')", name, sname, file);
			return(status);
		}
	}
}

/* export parameter list */

if (cmode & PCAPTURE_PAR) {
	char *fpar = capture->fpar;
	#if DEEP_DEBUG_CAPTURE
	printf("%s: exporting (par) chain to file '%s' \n", name, fpar);
	#endif
	CHAIN *pChainOfChains = NULL;
	status = thCaptureGetPChainOfChain(capture, &pChainOfChains);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (pChainOfChain) from (capture)", name);
		return(status);
	}
	status = export_pChainOfChains(fpar, pChainOfChains);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not export (pChainOfChain) to (file = '%s')", name, fpar);
		return(status);
	}
}

capture->cflag=CAPTURE_DONE;
return(SH_SUCCESS);
}


RET_CODE construct_file_fmt(char *dir, char *prefix, char *suffix, FRAMEID *id, char *fmt) {
char *name = "construct_file_fmt";
if (dir == NULL) {
	dir = ".";
	thError("%s: WARNING - (dir) was set to (null) - default is '.'", name);
}
shAssert(prefix != NULL);
shAssert(suffix != NULL);
shAssert(strlen(prefix) != 0);
shAssert(id != NULL);
shAssert(fmt != NULL);
int run, camcol, field, filter;
char band = '\0';
run=id->run;
camcol=id->camcol;
field=id->field;
filter=id->filter;
fmt[0]='\0';
RET_CODE status;
status= thNameGetFromBand(filter, &band);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (band) character for filter (%d)", name, filter);
	return(status);
}
sprintf(fmt, "%s/%s-%04d-%c%1d-%04d-X%%04d.%s", dir, prefix, run, band, camcol, field, suffix);
shAssert(strlen(fmt) != 0);
return(SH_SUCCESS);
}


RET_CODE construct_filename(char *dir, char *prefix, char *suffix, FRAMEID *id, char *file) {
char *name = "construct_filename";
if (dir == NULL) {
	dir = ".";
	thError("%s: WARNING - (dir) was set to (null) - default is '.'", name);
}
shAssert(prefix != NULL);
shAssert(suffix != NULL);
shAssert(strlen(prefix) != 0);
shAssert(id != NULL);
shAssert(file != NULL);
int run, camcol, field, filter;
char band = '\0';
run=id->run;
camcol=id->camcol;
filter=id->filter;
field=id->field;
file[0]='\0';
RET_CODE status;
status= thNameGetFromBand(filter, &band);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (band) character for filter (%d)", name, filter);
	return(status);
}
sprintf(file, "%s/%s-%04d-%c%1d-%04d.%s", dir, prefix, run, band, camcol, field, suffix);

shAssert(strlen(file) != 0);
return(SH_SUCCESS);
}


RET_CODE capture_filename(char *fmt, int icapture, char *file) {
char *name = "capture_filename";
shAssert(fmt != NULL);
shAssert(icapture >= 0);
shAssert(file != NULL)
if (fmt == NULL) {
	thError("%s: ERROR - (format) is null", name);
	return(SH_GENERIC_ERROR);
}
if (icapture < 0) {
	thError("%s: ERROR - (icapture = %d) should be zero or positive", name, icapture);
	return(SH_GENERIC_ERROR);
}
if (file == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
sprintf(file, fmt, icapture);
return(SH_SUCCESS);
}

RET_CODE region_capture_stat(REGION *reg, CAPTURE_STAT *stat) {
char *name = "region_capture_stat";
if (stat == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (reg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (reg->type != TYPE_U16) {
	thError("%s: ERROR - cannot handle non-U16 regions", name);
	return(SH_GENERIC_ERROR);
}

double nsigma = 1000.0;
float mean = 0.0, mode = 0.0, sigma = 0.0, quartiles[3];
memset(quartiles, '\0', 5 * sizeof(float));

RET_CODE status;

int nrow, ncol;
nrow = reg->nrow;
ncol = reg->ncol;

#if DEEP_DEBUG_CAPTURE
printf("%s: reg '%s', nrow = %d, ncol = %d \n", name, reg->name, nrow, ncol);
#endif
status = phRegStats(reg, nsigma, NULL, quartiles, &mean, &mode, &sigma);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - 'phRegStats' returned with error code (0x%010X) while expecting (0x%010X) and not (0x%010X)", name, status, SH_SUCCESS, SH_GENERIC_ERROR);
	return(SH_GENERIC_ERROR);
}

int i, j;
THPIX min, max;
min = (THPIX) reg->rows_u16[0][0];
max = min;
for (i = 0; i < nrow; i++) {
	U16 *row = reg->rows_u16[i];
	for (j = 0; j < ncol; j++) {
		min = MIN((THPIX) row[j], min);
		max = MAX((THPIX) row[j], max);
	}
}

int n;
n = nrow * ncol;

stat->n = n;
stat->nrow = nrow;
stat->ncol = ncol;
stat->min = min - (THPIX) SOFT_BIAS;
stat->max = max - (THPIX) SOFT_BIAS;
stat->median = (THPIX) quartiles[1] - (THPIX) SOFT_BIAS;
stat->q1 = (THPIX) quartiles[0] - (THPIX) SOFT_BIAS;
stat->q3 = (THPIX) quartiles[2] - (THPIX) SOFT_BIAS;
stat->iqr = stat->q3 - stat->q1;
stat->mean = (THPIX) mean - (THPIX) SOFT_BIAS;
stat->mode = (THPIX) mode - (THPIX) SOFT_BIAS;
stat->sigma = (THPIX) sigma;
stat->mode = (THPIX) mode - (THPIX) SOFT_BIAS;
stat->nsigma = (THPIX) nsigma;

strcpy(stat->title, reg->name);

return(SH_SUCCESS);
}

RET_CODE thCaptureStatWrite(FILE *fil, CAPTURE_STAT *stat) {
char *name = "thCaptureStatWrite";
if (fil == NULL) {
	thError("%s: ERROR - null output", name);
	return(SH_GENERIC_ERROR);
}
if (stat == NULL) { /* write the header */
	time_t     now;
	struct tm *ts;
	char       buf[80];
 
    /* Get the current time */
	now = time(NULL);
 
    /* Format and print the time, "ddd yyyy-mm-dd hh:mm:ss zzz" */
	ts = localtime(&now);
	strftime(buf, sizeof(buf), "%a %Y-%m-%d %H:%M:%S %Z", ts);
	fprintf(fil, "%s \n", buf);
/* 
char *hdr = "n, nrow, ncol, min, max, median, mean, mode, sigma, iqr, q1, q3, nsigma, title"
*/
	char *fmt = "%10s, %6s, %6s, %6s, %6s, %6s, %6s, %6s, %6s, %6s, %6s, %6s, %6s, %10s \n";
	fprintf(fil, fmt, "n", "nrow", "ncol", "min", "max", "median", "mean", "mode", "sigma", "iqr", "q1", "q3", "nsigma", "title");
	return(SH_SUCCESS);
}

char *fmt = "%10d, %6d, %6d, %6.2g, %6.2g, %6.2g, %6.2g, %6.2g, %6.2g, %6.2g, %6.2g, %6.2g, %6.2g, %30s \n";
fprintf(fil, fmt, stat->n, stat->nrow, stat->ncol, (float) stat->min, (float) stat->max, (float) stat->median, (float) stat->mean, (float) stat->mode, (float) stat->sigma, (float) stat->iqr, (float) stat->q1, (float) stat->q3, (float) stat->nsigma, stat->title);
return(SH_SUCCESS);

}

RET_CODE export_capture_stat_chain(char *file, CHAIN *chain) {
char *name = "export_capture_stat_chain";
if (file == NULL || strlen(file) == 0) {
	thError("%s: ERROR - null or empty filename", name);
	return(SH_GENERIC_ERROR);
}
if (chain == NULL) {
	thError("%s: ERROR - can't export null chain", name);
	return(SH_GENERIC_ERROR);
}
FILE *fil=fopen(file, "w");
if (fil == NULL) {
	thError("%s: ERROR - could not open file '%s' for output", name, file);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = thCaptureStatWrite(fil, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not write the header to file '%s'", name, file);
	fclose(fil);
	return(status);
}
int i, n;
n = shChainSize(chain);
if (n == 0) {
	thError("%s: WARNING - empty chain passed, file '%s' only contains header", name, file);
}
#if DEEP_DEBUG_CAPTURE
printf("%s: exporting (capture_stat) chain of size (n = %d) to file '%s' \n", name, n, file);
#endif
for (i = 0; i < n; i++) {
	CAPTURE_STAT *stat = shChainElementGetByPos(chain, i);
	status = thCaptureStatWrite(fil, stat);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not write (capture_stat) at position (i = %d) to file '%s'", name, i, file);
		fclose(fil);
	}
}
fclose(fil);
return(SH_SUCCESS);
}

RET_CODE pChainWrite(FILE *fil, CHAIN *chain) {
char *name = "pChainWrite";
if (fil == NULL) {
	thError("%s: ERROR - null output file", name);
	return(SH_GENERIC_ERROR);
}
if (chain == NULL) {
	thError("%s: ERROR - cannot export null chain", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
int i, n;
n = shChainSize(chain);
if (n == 0) {
	thError("%s: WARNING - empty chain", name);
}
#if DEEP_DEBUG_CAPTURE
printf("%s: export (pChain) of (size = %d) \n", name, n);
#endif
for (i = 0; i < n; i++) {
	THOBJC *objc = shChainElementGetByPos(chain, i);
	status = thObjcWrite(fil, objc);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not write (thobjc) at position (i = %d)", name, i);
		return(status);
	}
}
return(SH_SUCCESS);
}

RET_CODE export_pChainOfChains(char *file, CHAIN *pChainOfChains) {
char *name = "export_pChainOfChains";
if (file == NULL || strlen(file) == 0) {
	thError("%s: ERROR - null or empty file name", name);
	return(SH_GENERIC_ERROR);
}
if (pChainOfChains == NULL) {
	thError("%s: ERROR - cannot export null (pChainOfChains)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
FILE *fil = fopen(file, "w");
if (fil == NULL) {
	thError("%s: ERROR - failed to open file '%s' for output", name, file);
	return(SH_GENERIC_ERROR);
}

#if 0 /* not sure if a header is needed */
status = pChainWrite(fil, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not write header to file '%s'", name, file);
	fclose(fil);
	return(status);
}
#endif
int i, n;
n = shChainSize(pChainOfChains);
if (n == 0) {
	thError("%s: WARNING - empty (pChainOfChains) - only header was written to file '%s'", name, file);
}
#if DEEP_DEBUG_CAPTURE
printf("%s: exporting (pChainOfChains) of (size = %d) \n", name, n);
#endif
for (i = 0; i < n; i++) {
	status = thEmptyLineWrite(fil, "=");
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not write the separation line in file '%s'", name, file);
		fclose(fil);
		return(status);
	}
	CHAIN *chain = shChainElementGetByPos(pChainOfChains, i);
	status = pChainWrite(fil, chain);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not export (pChain) at position (i = %d) to file '%s'", name, i, file);
		fclose(fil);
		return(status);
	}
}

fclose(fil);
return(SH_SUCCESS);
}
RET_CODE thCaptureMakeSectorModel(CAPTURE *capture, CAPTURE_SECTOR sector) {
char *name = "thCaptureMakeSectorModel";

char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", sector);
if (sname == NULL || strlen(sname) == 0) {
	thError("%s: ERROR - (sector = %d) doesn't have an associated name in 'CAPTURE_SECTOR'", name,(int) sector);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
LSTRUCT *ldump = NULL;
REGION *smodel = NULL;
status = thCaptureGetLBySector(capture, sector, &ldump);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lstruct) for (sector = '%s') from (capture)", 
		name, shEnumNameGetFromValue("CAPTURE_SECTOR", sector));
	return(status);
}
status = thLDumpPar(ldump, IOTOXN);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not copy from (io) to (pN)", name);
	return(status);
}
status = thLDumpAmp(ldump, IOTOX);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not copy from (io) to (aN)", name);
	return(status);
}
/* 
status = thLDumpPar(ldump, MODELTOXN);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not copy from (model) to (pN)", name);
	return(status);
}
status = thLDumpAmp(ldump, MODELTOXN);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not copy from (model) to (aN)", name);
	return(status);
}
*/
status = thCaptureGetRegionBySector(capture, sector, &smodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (subregion) for (sector = '%s') from (capture)", name, sname);
	return(status);
}
shRegClear(smodel); /* without clearing it will keep up adding models */	
ALGORITHM *alg = NULL;
status = thLstructGetAlgByStage(ldump, SIMSTAGE, &alg);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (alg) to simulate (sector = '%s')", name, sname);
	return(status);
}
status = thAlgorithmRun(alg, ldump);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run (alg) to simulate (sector = '%s')", name, sname);
	return(status);
}

#if 0 /* note that the regions in capture should point to the same location as mN's in ldump's - no need to copy pixels */
status = thLstructGetMN(ldump, &mN);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (mN) generated for (sector = '%s') from (capture)", name, sname);
	return(status);
}
status = thRegCopyToSubReg(mN, smodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not copy (mN) into subregion for (sector = '%s')", name, sname);
	return(status);
}
#endif
return(SH_SUCCESS);
}


RET_CODE make_ldump_by_sector(LSTRUCT *l, CAPTURE_SECTOR sector, LSTRUCT *ldump) {
char *name = "make_ldump_by_sector";
if (l == NULL || ldump == NULL) {
	thError("%s: ERROR - null input or output", name);
	return(SH_GENERIC_ERROR);
}
char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", sector);
if (sector == CAPTURE_RES) {
	thError("%s: WARNING - no (ldump) needed for (sector = '%s')", sname);
	return(SH_SUCCESS);
} else if (sector >= N_CAPTURE_SECTOR) {
	thError("%s: ERROR - (sector = '%s') not supported", name, sname);
	return(SH_SUCCESS);
}

RET_CODE status;
status = thLstructCopy(l, ldump, LCOPY_SOURCE);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not copy (lstruct) onto (ldump) for (sector = '%s')", name, sname);
	return(status);
}
THOBJC **objcarr = NULL;
int nobjc = 0;
LMACHINE *map = NULL;
MOBJC_SECTOR objc_sector = UNKNOWN_MOBJC_SECTOR;
switch (sector) {
	case CAPTURE_MODEL:
		objc_sector = ALL_MOBJCS;
		break;
	case CAPTURE_OBJC:
		objc_sector = NONLINEAR_MOBJCS;
		break;
	case CAPTURE_SKY:
		objc_sector = ALL_MSKY;
		break;
	default:
		thError("%s: ERROR - (sector = '%s') not supported", name, sname);
		return(SH_GENERIC_ERROR);
		break;
	}
#if DEBUG_CAPTURE
printf("%s: sector = '%s', object-sector set to '%s', %d \n", name, sname, shEnumNameGetFromValue("MOBJC_SECTOR", objc_sector), objc_sector);
#endif

int band = UNKNOWN_BAND;
FRAME *f = NULL;
status = thLstructGetSource(l, (void **) &f);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (source frame) for (lstruct)", name);
	return(status);
}
status = thFrameGetBand(f, &band);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (band) from (frame)", name);
	return(status);
}
LMACHINE *lmap = NULL;
status = thLstructGetLmachine(l, &lmap);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (map) from the main (lstruct)", name);
	return(status);
}
LMODEL *lmodel = NULL;
status = thLstructGetLmodel(l, &lmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmodel) from (lstruct)", name);
	return(status);
}
status = thMapGetObjcArr(lmap, objc_sector, &objcarr, &nobjc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (objcarr) for (sector = '%s')", name, sname);
	return(status);
}
#if DEBUG_CAPTURE
printf("%s: sector = '%s', objc-array = %p, nobjc = %d \n", name, sname, objcarr, nobjc);
#endif
int i;
if (nobjc <= 0) {
	thError("%s: WARNING - expeced some (objc)s but received (nobjc = %d) for (sector = '%s')", name, nobjc, sname);
}
if (nobjc > 0 &&  objcarr == NULL) {
	thError("%s: ERROR - null (objc arr) while (nobjc = %d)", name, nobjc);
	return(SH_GENERIC_ERROR);
}
map = thMapmachineNew();
for (i = 0; i < nobjc; i++) {
	THOBJC *objc = objcarr[i];
	status = thMAddObjcAllModels(map, objc, band, SEPARATE);	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add (objc) at (i = %d) to (map)", name, i);
		return(status);
	}
}
status = thMCompile(map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not compile (map) for (sector = '%s')", name, sname);
	return(status);
}
int nm = -1, np = -1, npp = -1, nc = -1, nacc = -1, nj = -1;
status = thMapGetLpars(map, &nm, &np, &npp, &nc, &nacc, &nj);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lpars) from (map)", name);
	return(status);
}
int nrow = -1, ncol = -1;
LDATA *ldata = NULL;
status = thLstructGetLdata(l, &ldata);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (ldata) from (source)", name);
	return(status);
}
status = thLdataGetLpars(ldata, &nrow, &ncol);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (nrow, ncol) from (ldata) in (source)", name);
	return(status);
}
LWORK *dwork = NULL;
status = thLstructGetLwork(ldump, &dwork);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwork) for (ldump)", name);
	return(status);
}
status = thLworkAlloc(dwork, lmodel, map, nm, np, npp, nc, nacc, nj, nrow, ncol);	
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not allocate (lwork) in (ldump)", name);
	return(status);
}
LMODEL *dmodel = NULL;
status = thLstructGetLmodel(ldump, &dmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmodel) for (ldump)", name);
	return(status);
}
PSFMODEL *psf = NULL;
status = thLdataGet(ldata, NULL, NULL, NULL, NULL, &psf);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (psf_model) from (ldata) in (source)", name);
	return(status);
}
status = thLworkPut(dwork, psf);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not put (psf_model) in (lwork) in (ldump)", name);
	return(status);
}
status = thLmodelAlloc(dmodel, nm, np);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not allocate memory for (lmodel) in (ldump)", name);
	return(status);
}
status = thLstructPutLmachine(ldump, map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not put (map) for (sector = '%s') in (ldump)", name, sname);
	return(status);
}
/* have not yet put (psf) and (lmethod) in (ldump) */
status = MakeImageAndMatrices(ldump, MEMORY_ESTIMATE);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not estimate the memory for the model elements", name);
	return(status);
}

status = thLCompile(ldump, (MEMDBLE) CAPTURE_MEMORY, SIMFINALFIT);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not compile (ldump) for (sector = '%s')", name, sname);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE thU16RegMakeLUT(REGION *ireg, REGION *lut) {
char *name = "thU16RegMakeLUT";
if (ireg == NULL) {
	thError("%s: ERROR - null (U16) input reg", name);
	return(SH_GENERIC_ERROR);
}
if (ireg->type != TYPE_U16) {
	thError("%s: ERROR - improper type for (ireg)", name);
	return(SH_GENERIC_ERROR);
}
if (lut == NULL) {
	thError("%s: ERROR - null (U8) lut reg", name);
	return(SH_GENERIC_ERROR);
}
if (lut->type != TYPE_U8 || lut->nrow != 1 || lut->ncol != MAX_U16 + 1) {
	thError("%s: ERROR - improper (lut) passed", name);
	return(SH_GENERIC_ERROR);
}
int nrow = ireg->nrow;
int ncol = ireg->ncol;
if (nrow == 0 || ncol == 0) {
	thError("%s: ERROR - unsupported (ireg: nrow = %d, ncol = %d)", name, nrow, ncol);
	return(SH_GENERIC_ERROR);
}

/* centering zero - an correcting the width of the lut so that it contains possible values to come up in future runs */
U16 u16max = 0, u16min = MAX_U16;
THPIX middle, biasmax, biasmin;
int i;
#if CAPTURE_MINMAX
int j;
for (i = 0; i < nrow; i++) {
	U16 *rowi = ireg->rows_u16[i]; 
	for (j = 0; j < ncol; j++) {
		u16max = MAX(rowi[j], u16max);
		u16min = MIN(rowi[j], u16min);
	}
}
if (u16min >= u16max) {
	thError("%s: WARNING - uniform (ireg, min = %d, max = %d)", name, (int) u16min, (int) u16max);
	u16min = 0;
	u16max = MAX_U16;	
}
middle = 0.5 * ((THPIX) u16min + (THPIX) u16max);
#else
double nsigma = 1000.0;
float mean = 0.0, mode = 0.0, sigma = 0.0, quartiles[3];
memset(quartiles, '\0', 5 * sizeof(float));
RET_CODE status;
status = phRegStats(ireg, nsigma, NULL, quartiles, &mean, &mode, &sigma);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - 'phRegStats' returned with error code (0x%010X) while expecting (0x%010X) and not (0x%010X)", name, status, SH_SUCCESS, SH_GENERIC_ERROR);
	return(SH_GENERIC_ERROR);
}
middle = mean;
u16max = (U16) (quartiles[2] + 0.5);
u16min = (U16) (quartiles[0] + 0.5);
#endif

biasmax = thabs((THPIX) u16max - middle);
biasmin = thabs((THPIX) u16min - middle);
THPIX bias = MAX(biasmax, biasmin) * CAPTURE_LUT_RATIO;
biasmax = middle + bias;
biasmin = middle - bias;
if (biasmin < 0.0) {
	biasmin = 0.0;
}
if (biasmax > MAX_U16) {
	biasmax = (THPIX) MAX_U16;
}
u16min = (U16) biasmin;
u16max = (U16) biasmax;
#if DEBUG_CAPTURE
printf("%s: middle = %g, bias   = %g, biasmax = %g, biasmin = %g \n", name, middle, bias, biasmax, biasmin);
printf("%s: u16min = %d, u16max = %g \n", name, (int) u16min, (int) u16max);
#endif
/* now making the lookup table */
U8 *lookup = lut->rows_u8[0];
for (i = 0; i < (int) u16min; i++) {
	lookup[i] = 0;
}
for (i = (int) u16max + 1; i < (int) MAX_U16 + 1; i++) {
	lookup[i] = MAX_U8;
}
THPIX ratio = ((THPIX) MAX_U8 - 2) / ((THPIX) u16max - (THPIX) u16min);
for (i = (int) u16min; i <= (int) u16max; i++) {
	THPIX value = (i - (THPIX) u16min) * ratio + (THPIX) 1.0;
	shAssert(value < (THPIX) MAX_U8);
	shAssert(value > (THPIX) 0.0);
	lookup[i] = (U8) value;
}
return(SH_SUCCESS);
}

RET_CODE thCaptureWriteSubtitle(FILE *fil, int frame, CAPTURE_STAT *stat, THOBJC *objc) {
char *name = "thCaptureWriteSubtitle";
if (fil == NULL) {
	thError("%s: ERROR - null output file", name);	
	return(SH_GENERIC_ERROR);
}
THPIX min, max;
if (stat == NULL) {
	min = THNAN;
	max = THNAN;
} else {
	min = stat->min;
	max = stat->max;
}
THPIX bmax = stat->bmax;
if (bmax == 0.0) {
	bmax = 1.0;
}
min = min / bmax;
max = max / bmax;

TYPE iot = objc->iotype;
fprintf(fil, "{%d}{%d} %05d min=%9.3g, max=%9.3g", frame, frame, frame, (float) min, (float) max);
#if VERBOSE_CAPTURE
printf("{%d}{%d} %05d min=%9.3g, max=%9.3g", frame, frame, frame, (float) min, (float) max);
#endif
if (objc != NULL) {
	char *enames[100];
	int jj = 0;
	enames[jj++] = "counts"; 
	enames[jj++] = "re"; enames[jj++] = "e"; enames[jj++] = "phi"; 
	enames[jj++] = "I"; enames[jj++] = "mcount";
	enames[jj++] = "ue", enames[jj++] = "V"; enames[jj++] = "W";
	int nelem = 6;
	CHAIN *thprop=objc->thprop;
	int n = shChainSize(thprop);
	int i, j;
	for (i = 0; i < n; i++) {
		THPROP *prop = shChainElementGetByPos(thprop, i);
		char *mname = prop->mname; char *pname = prop->pname;
		fprintf(fil, "| %s - ", mname);
		#if VERBOSE_CAPTURE 
		printf("| %s - ", mname);
		#endif
		TYPE t = shTypeGetFromName(pname);
		/* 
		const SCHEMA *s = shSchemaGetFromType(t);
		*/
		for (j = 0; j < nelem; j++) {	
			THPIX *x = NULL;
			void *value = NULL;
			#if 0
			char *ename = enames[j];
			const SCHEMA_ELEM *se = shSchemaElemGetFromType(t, ename);
			value = prop->value;
			#else
			char ename[MX_STRING_LEN];
			memset(ename, '\0', MX_STRING_LEN * sizeof(char));
			sprintf(ename, "%s_%s", enames[j], mname);
			const SCHEMA_ELEM *se = shSchemaElemGetFromType(iot, ename);
			value = objc->ioprop;
			#endif
			if (se != NULL) x = shElemGet(value, se, NULL);
			if (x != NULL) {
				fprintf(fil,"%s=%9.3f ", enames[j], (float) *x);
				#if VERBOSE_CAPTURE
				printf("%s=%9.3g ", enames[j], (float) *x);
				#endif
			}

		}
	}
}		

fprintf(fil, "\n");
#if VERBOSE_CAPTURE
printf("\n");
#endif
return(SH_SUCCESS);
}


RET_CODE thCaptureFlush(CAPTURE *cap) {
char *name = "thCaptureFlush";
if (cap == NULL) {
	thError("%s: ERROR - null input (capture)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
FILE *stream;
int i;
for (i = 0; i < N_CAPTURE_SECTOR; i++) {
	char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", i);
	status = thCaptureGetSubStreamBySector(cap, i, &stream);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get subtitle stream for (sector = '%s')", name, sname);
		return(status);
	}
	fflush(stream);
}

fflush(stdout);
return(SH_SUCCESS);
}

RET_CODE thCaptureBinU8Reg(REGION *reg, int gbin, int growc, int gcolc, int gnrow, int gncol, REGION **outreg) {
char *name = "thCaptureBinU8Reg";
if (reg == NULL) {
	thError("%s: ERROR - null input (region)", name);
	return(SH_GENERIC_ERROR);
}
if (gbin <= 0) {
	thError("%s: ERROR - negative value for (gbin)", name);
	return(SH_GENERIC_ERROR);
}
if (growc < 0 || gcolc < 0) {
	thError("%s: ERROR - negative value for (growc, gcolc = %d, %d)", name, growc, gcolc);
	return(SH_GENERIC_ERROR);
}
if ((gnrow < gbin && gnrow != UNCHANGED_NROWCOL) || (gncol < gbin && gncol != UNCHANGED_NROWCOL)) {
	thError("%s: ERROR - graphical output too small (gnrow, gncol = %d, %d) while (gbin = %d)", name, gnrow, gncol, gbin);
	return(SH_GENERIC_ERROR);
}
if (reg->type != TYPE_U8) {
	thError("%s: ERROR - expected (region) of type 'U8'", name);
	return(SH_GENERIC_ERROR);
}
int nrow, ncol;
nrow = reg->nrow;
ncol = reg->ncol;
if (gnrow == UNCHANGED_NROWCOL) {
	gnrow = nrow;
}
if (gncol == UNCHANGED_NROWCOL) {
	gncol = ncol;
}
if (gbin == 1) {
	REGION *binreg = shSubRegNew(reg->name, reg, gnrow, gncol, growc, gcolc, NO_FLAGS);
	if (binreg == NULL) {
		thError("%s: ERROR - check graphical output values - null subregion", name);
		*outreg = NULL;
		return(SH_GENERIC_ERROR);
	}
	*outreg = binreg; 
	return(SH_SUCCESS);
} else {
	int bnrow, bncol, ggnrow, ggncol;
	bnrow = gnrow / gbin;
	bncol = gncol / gbin;
	ggnrow = bnrow * gbin;
	ggncol = bncol * gbin;
	REGION *chopped = shSubRegNew("chopped image", reg, ggnrow, ggncol, growc, gcolc, NO_FLAGS);
	if (chopped == NULL) {
		thError("%s: ERROR - wrong graphical output values - null subregion", name);
		*outreg = NULL;
		return(SH_GENERIC_ERROR);
	}
	U8 **rows_u8 = chopped->rows_u8;
	REGION *fbin = shRegNew("scratch space for binning", bnrow, bncol, TYPE_FL32);
	shRegClear(fbin);
	FL32 **rows_fl32 = fbin->rows_fl32;
	int i, j;
	for (i = 0; i < ggnrow; i++) {
		U8 *rows_i = rows_u8[i];
		FL32 *brows_i = rows_fl32[i/gbin];
		for (j = 0; j < ggncol; j++) {
			brows_i[j/gbin] += (FL32) rows_i[j];
		}	
	}
	shRegDel(chopped);
	REGION *binreg = shRegNew(reg->name, bnrow, bncol, TYPE_U8);
	rows_u8 = binreg->rows_u8;
	FL32 binsq = (FL32) gbin * (FL32) gbin;
	for (i = 0; i < bnrow; i++) {
		FL32 *frow_i = rows_fl32[i];
		U8 *brow_i = rows_u8[i];
		for (j = 0; j < bncol; j++) {
			brow_i[j] = (U8) (frow_i[j] / binsq + 0.5);
		}
	}
	shRegDel(fbin);
	*outreg = binreg;
	return(SH_SUCCESS);
}

}
		
