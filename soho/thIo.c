#include "thIo.h"

#define HEAPCHARSIZE 100

static int check_for_exception(char *in, char **except, int nexcept);

RET_CODE thInitIo() {

  char *name = "thInitIo";
  
  /* setup the schema's needed to read and writes from fits files */
  shSchemaLoadFromDervish();
  
  phSchemaLoadFromPhoto();

  thSchemaLoadFromSoho();

/* NOTE: NTYPE = 20 is set in file photoFitsIO.c is the maximum number of schemas that can be specified */

/* 
  thSchemaGetFunc();
*/

  thSchemaGetReg();

#if READ_CALIBRATED_FRAME
  thSchemaGetBinregion();
#endif
 
  thSchemaGetObjmask();
  
  thSchemaGetObject1();

  thSchemaGetObjc();

#if USE_FNAL_OBJC_IO
  thSchemaGetFnalObjcIo(PHOTO_INPUT_MODE);
#else
  thSchemaGetObjcIo(PHOTO_INPUT_MODE);
#endif

  thSchemaGetCalibPhObjcIo(PHOTO_INPUT_MODE);

  thSchemaGetAtlasImage();

  thSchemaGetGalacticCalib();
 
  thSchemaGetPhCalib();

  thSchemaGetPsfKLComp();
  
  thSchemaGetCalibIo();

  thSchemaGetCrudeCalib();
  
  thSchemeGetPsfwing();

  thSchemaGetSkycoeffs();

  thSchemaGetSkymodel();

  thSchemaGetSkymodelIo();

  thSchemaGetExtregion();

/* these were used when function values were stored 

  thSchemaGetFmhdu();

  thSchemaGetParelemio();
*/

  thSchemaGetObjcpars();

  thSchemaGetLmethod ();
  thSchemaGetLprofile ();

/* PSF report */

  thSchemaGetPsfReport();

/* no need for IO
  thSchemaGetBasicStat();
*/
  thSchemaGetBriefPsfReport();


  return(SH_SUCCESS);
}

SCHEMATRANS *thSchemaGetAtlasImage() {

SCHEMATRANS *mytrans = shSchemaTransNew();

int i, j, n = 6;
char **ttypes, *ttype, *TTYPE, *tform;
ttypes = shCalloc(n, sizeof(char *));
ttypes[0] = shCalloc(n * MX_STRING_LEN, sizeof(char));
for (i = 0; i < n; i++) {
	ttypes[i] = ttypes[0] + i * MX_STRING_LEN;
}
i = 0;
strcpy(ttypes[i], "id"); i++;
strcpy(ttypes[i], "run"); i++;
strcpy(ttypes[i], "rerun"); i++;
strcpy(ttypes[i], "camCol"); i++;
strcpy(ttypes[i], "field"); i++;
strcpy(ttypes[i], "parent"); i++;
TTYPE = shCalloc(MX_STRING_LEN, sizeof(char));
tform = shCalloc(MX_STRING_LEN, sizeof(char));
for (i = 0; i < n; i++) {
	char *ttype = ttypes[i];
	for (j = 0; j < strlen(ttype); j++) TTYPE[j] = toupper(ttype[j]);	
	shSchemaTransEntryAdd(mytrans, CONVERSION_BY_TYPE, TTYPE, ttype, "", NULL, NULL, NULL, NULL, 0.0, -1);
}
ttype = ttypes[0];
strcpy(ttype, "ncolor");
strcpy(tform, "uchar");
for (j = 0; j < strlen(ttype); j++) TTYPE[j] = toupper(ttype[j]);
shSchemaTransEntryAdd(mytrans, CONVERSION_BY_TYPE, TTYPE, ttype, "heap", tform, "-1", NULL, NULL, 0.0, -1);

thFree(ttypes[0]);
thFree(ttypes);
thFree(TTYPE);
thFree(tform);

phFitsBinDeclareSchemaTrans(mytrans, "ATLAS_IMAGE");

#if PRINT_IO_SCHEMA_TRANS
shSchemaTransEntryShow(mytrans, 0, n - 1);
#endif

return(mytrans);
}

SCHEMATRANS *thSchemaGetLprofile() {

SCHEMATRANS *mytrans = shSchemaTransNew();
SCHEMA *s = shSchemaGet("LPROFILE");
int n = s->nelem;
char *TTYPE = thCalloc(MX_STRING_LEN, sizeof(char));

int i, j; 
for (i = 0; i < n; i++) {
	SCHEMA_ELEM *se = s->elems + i;
	char *ttype = se->name;
	for (j = 0; j < strlen(ttype); j++) {
		TTYPE[j] = toupper(ttype[j]);
	}
	TTYPE[strlen(ttype)] = '\0';
	/* should this be added to schema trans ? depends whether we have decided to output the standard errors as part of this */
	int addthis = ((strstr(ttype, "Err")) == NULL);		
	#if IO_STD_ERRORS
		addthis = addthis || 1;
	#endif
	if (addthis) {
		shSchemaTransEntryAdd(mytrans, CONVERSION_BY_TYPE, TTYPE, ttype, "", 
				NULL, NULL, NULL, NULL, 0.0, -1);
	}
}
thFree(TTYPE);
phFitsBinDeclareSchemaTrans(mytrans, "LPROFILE");

#if PRINT_IO_SCHEMA_TRANS
shSchemaTransEntryShow(mytrans, 0, n - 1);
#endif

return(mytrans);
}



SCHEMATRANS *thSchemaGetObjcpars() {

char *name = "thSchemaGetObjcpars";
SCHEMATRANS *mytrans = shSchemaTransNew();
SCHEMA *s = shSchemaGet("OBJCPARS");
if (s == NULL) {
	thError("%s: ERROR - type 'OBJCPARS' not supported by schema", name);
	return(NULL);
}

int n = s->nelem;
char *TTYPE = thCalloc(MX_STRING_LEN, sizeof(char));

int i, j; 
for (i = 0; i < n; i++) {
	SCHEMA_ELEM *se = s->elems + i;
	char *ttype = se->name;
	for (j = 0; j < strlen(ttype); j++) {
		TTYPE[j] = toupper(ttype[j]);
	}
	TTYPE[strlen(ttype)] = '\0';
	/* should this be added to schema trans ? depends whether we have decided to output the standard errors as part of this */
	int addthis = ((strstr(ttype, "Err")) == NULL);		
	#if IO_STD_ERRORS
		addthis = addthis || 1;
	#endif
	if (addthis) {
		shSchemaTransEntryAdd(mytrans, CONVERSION_BY_TYPE, TTYPE, ttype, "", 
				NULL, NULL, NULL, NULL, 0.0, -1);
	}
}
thFree(TTYPE);
phFitsBinDeclareSchemaTrans(mytrans, "OBJCPARS");

#if PRINT_IO_SCHEMA_TRANS
shSchemaTransEntryShow(mytrans, 0, n - 1);
#endif

return(mytrans);
}


SCHEMATRANS *thSchemaGetFunc() {
  
  SCHEMATRANS *func_trans;
  func_trans = shSchemaTransNew();

  /* 
     Now adding schema elements
  */

  shSchemaTransEntryAdd(func_trans,CONVERSION_BY_TYPE,"NAME","name",
			"heap","char","-1",NULL,NULL,0.0,-1);
  shSchemaTransEntryAdd(func_trans,CONVERSION_BY_TYPE,"ROW0","row0",
			"",NULL,NULL,NULL,NULL,0.0,-1);
  shSchemaTransEntryAdd(func_trans,CONVERSION_BY_TYPE,"COL0","col0",
			"",NULL,NULL,NULL,NULL,0.0,-1);
  shSchemaTransEntryAdd(func_trans,CONVERSION_BY_TYPE,"ROWC","rowc",
			 "",NULL,NULL,NULL,NULL,0.0,-1);
  shSchemaTransEntryAdd(func_trans,CONVERSION_BY_TYPE,"COLC","colc",
			"",NULL,NULL,NULL,NULL,0.0,-1);
  shSchemaTransEntryAdd(func_trans,CONVERSION_BY_TYPE,"REG","reg",
			"",NULL,NULL,NULL,NULL,0.0,-1);
  /* 
     I don't understand why I have to pass type "char" as the type of 
     INDICES and PARS. Perhaps because its size is used to represent the size
     of the pointer

     Error: Unable to read/write heap data for FUNC->indices
     Error: Unable to read/write heap data for FUNC->pars

  
  char *heapsize;
  heapsize = (char *) thCalloc(HEAPCHARSIZE, sizeof(char));
  
  
  sprintf(heapsize, "%d", FUNC_N_INDICES);
  shSchemaTransEntryAdd(func_trans,CONVERSION_BY_TYPE,"INDICES","indices",
			"heap","int";, "-1", NULL,heapsize,0.0,-1);
  
  sprintf(heapsize, "%d", FUNC_N_PARS);
  shSchemaTransEntryAdd(func_trans,CONVERSION_BY_TYPE,"PARS","pars",
			"heap","float";, heapsize, NULL,NULL,0.0,-1);

  */

  char *src, *dst;
  int i;

  src =  thCalloc(MX_STRING_LEN, sizeof(char));
  dst =  thCalloc(MX_STRING_LEN, sizeof(char));

  for (i = 0; i < FUNC_N_INDICES; i++) {

    sprintf(src,"INDEX%d", i);
    sprintf(dst, "index%d", i);
    shSchemaTransEntryAdd(func_trans,CONVERSION_BY_TYPE, src, dst,
			  "",NULL,NULL,NULL,NULL,0.0,-1);
  }
    
  for (i = 0; i < FUNC_N_PARS; i++) {

    sprintf(src,"PAR%d", i);
    sprintf(dst, "par%d", i);
    shSchemaTransEntryAdd(func_trans,CONVERSION_BY_TYPE, src, dst,
			  "",NULL,NULL,NULL,NULL,0.0,-1);
  }

  thFree(src);
  thFree(dst);

  /* Now declaring the schema */

  phFitsBinDeclareSchemaTrans(func_trans,"FUNC");

  return(func_trans);

}

SCHEMATRANS *thSchemaGetReg() {
	

  char *name = "thSchemaGetReg";
 
  SCHEMATRANS *region_trans;

  region_trans = shSchemaTransNew();

  //shSchemaTransEntryAdd(region_trans,CONVERSION_BY_TYPE,"RNAME","name",
  // "heap","char","-1",NULL,NULL,0.0,-1);
  shSchemaTransEntryAdd(region_trans,CONVERSION_BY_TYPE,"RNROW","nrow",
			"",NULL,NULL,NULL,NULL,0.0,-1);
  shSchemaTransEntryAdd(region_trans,CONVERSION_BY_TYPE,"RNCOL","ncol",
			"",NULL,NULL,NULL,NULL,0.0,-1);
  shSchemaTransEntryAdd(region_trans,CONVERSION_BY_TYPE,"RTYPE","type",
			"",NULL,NULL,NULL,NULL,0.0,-1);
  /* 
     how do you define a schema trans element for REGION such that 
     it is usable for all region types.
  */

  char *io_rows, *io_type;
  #if IO_COMPRESSED 
  #ifdef THIOPIXTYPE
  if (THIOPIXTYPE == TYPE_FL32) { 
  io_rows = "rows_fl32";
  io_type = "float";
  } else if (THIOPIXTYPE == TYPE_S8) {
  io_rows = "rows_s8";
  if (sizeof(char) == sizeof(THIOTYPE)) {
	io_type = "char";
  } else if (sizeof(short int) == sizeof(THIOTYPE)) {
	io_type = "short int";
  } else if (sizeof(int) == sizeof(THIOTYPE)) {
	io_type = "int";
  } else if (sizeof(long int) == sizeof(THIOTYPE)) {
	io_type = "long int";
  } else if (sizeof(long long int) == sizeof(THIOTYPE)) {
	io_type = "long long int";
  } else {
	thError("%s: ERROR - unable to find an appropriate I/O type", name);
  }
  } else if (THIOPIXTYPE == TYPE_S16) {
  io_rows = "rows_s16";
 if (sizeof(short int) == sizeof(THIOTYPE)) {
	io_type = "short int";
  } else if (sizeof(int) == sizeof(THIOTYPE)) {
	io_type = "int";
  } else if (sizeof(long int) == sizeof(THIOTYPE)) {
	io_type = "long int";
  } else if (sizeof(long long int) == sizeof(THIOTYPE)) {
	io_type = "long long int";
  } else {
	thError("%s: ERROR - unable to find an appropriate I/O type", name);
  }
  } else if (THIOPIXTYPE == TYPE_U8) {
  io_rows = "rows_u8"; 
 if (sizeof(unsigned char) == sizeof(THIOTYPE)) {
	io_type = "unsigned char";
  } else if (sizeof(unsigned short int) == sizeof(THIOTYPE)) {
	io_type = "unsigned short int";
  } else if (sizeof(unsigned int) == sizeof(THIOTYPE)) {
	io_type = "unsigned int";
  } else if (sizeof(unsigned long int) == sizeof(THIOTYPE)) {
	io_type = "unsigned long int";
  } else if (sizeof(unsigned long long int) == sizeof(THIOTYPE)) {
	io_type = "unsigned long long int";
  } else {
	thError("%s: ERROR - unable to find an appropriate I/O type", name);
  }
  } else if (THIOPIXTYPE == TYPE_U16) {
  io_rows = "rows_u16";
 if (sizeof(unsigned char) == sizeof(THIOTYPE)) {
	io_type = "unsigned char";
  } else if (sizeof(unsigned short int) == sizeof(THIOTYPE)) {
	io_type = "unsigned short int";
  } else if (sizeof(unsigned int) == sizeof(THIOTYPE)) {
	io_type = "unsigned int";
  } else if (sizeof(unsigned long int) == sizeof(THIOTYPE)) {
	io_type = "unsigned long int";
  } else if (sizeof(unsigned long long int) == sizeof(THIOTYPE)) {
	io_type = "unsigned long long int";
  } else {
	thError("%s: ERROR - unable to find an appropriate I/O type", name);
  }
  } else if (THIOPIXTYPE == TYPE_S32) {
  io_rows = "rows_s32";
 if (sizeof(short int) == sizeof(THIOTYPE)) {
	io_type = "short int";
  } else if (sizeof(int) == sizeof(THIOTYPE)) {
	io_type = "int";
  } else if (sizeof(long int) == sizeof(THIOTYPE)) {
	io_type = "long int";
  } else if (sizeof(long long int) == sizeof(THIOTYPE)) {
	io_type = "long long int";
  } else {
	thError("%s: ERROR - unable to find an appropriate I/O type", name);
  }
  } else if (THIOPIXTYPE == TYPE_U32) {
   io_rows = "rows_u32";
 if (sizeof(unsigned char) == sizeof(THIOTYPE)) {
	io_type = "unsigned char";
  } else if (sizeof(unsigned short int) == sizeof(THIOTYPE)) {
	io_type = "unsigned short int";
  } else if (sizeof(unsigned int) == sizeof(THIOTYPE)) {
	io_type = "unsigned int";
  } else if (sizeof(unsigned long int) == sizeof(THIOTYPE)) {
	io_type = "unsigned long int";
  } else if (sizeof(unsigned long long int) == sizeof(THIOTYPE)) {
	io_type = "unsigned long long int";
  } else {
	thError("%s: ERROR - unable to find an appropriate I/O type", name);
  }
  } else {
	thError("%s: ERROR - unsupported IO type", name);
  }
  #endif
  #else 
  #ifdef TYPE_THPIX
  if (TYPE_THPIX == TYPE_FL32) { 
  io_rows = "rows_fl32";
  io_type = "float";
  } else {
	thError("%s: ERROR - unsupported IO type", name);
  }
  #endif
  #endif

  shSchemaTransEntryAdd(region_trans,CONVERSION_BY_TYPE,"RROWS",
			io_rows, 
			"heap", io_type,"-1",NULL,NULL,0.0,-1);
 
  shSchemaTransEntryAdd(region_trans,CONVERSION_BY_TYPE,"RROW0","row0",
			"",NULL,NULL,NULL,NULL,0.0,-1);
  shSchemaTransEntryAdd(region_trans,CONVERSION_BY_TYPE,"RCOL0","col0",
			"",NULL,NULL,NULL,NULL,0.0,-1);

  phFitsBinDeclareSchemaTrans(region_trans,"REGION");
  
  return(region_trans);
}

SCHEMATRANS *thSchemaGetObjmask() {

  SCHEMATRANS *om_trans;
  om_trans = shSchemaTransNew();
  shSchemaTransEntryAdd(om_trans, CONVERSION_BY_TYPE, "REFCNTR", "refcntr", 
			"", NULL, NULL, NULL, NULL, 0.0, -1);
  shSchemaTransEntryAdd(om_trans, CONVERSION_BY_TYPE, "NSPAN", "nspan", 
			"", NULL, NULL, NULL, NULL, 0.0, -1);
  shSchemaTransEntryAdd(om_trans, CONVERSION_BY_TYPE, "ROW0", "row0", 
			"", NULL, NULL, NULL, NULL, 0.0, -1);
  shSchemaTransEntryAdd(om_trans, CONVERSION_BY_TYPE, "COL0", "col0", 
			"", NULL, NULL, NULL, NULL, 0.0, -1);
  shSchemaTransEntryAdd(om_trans, CONVERSION_BY_TYPE, "RMIN", "rmin", 
			"", NULL, NULL, NULL, NULL, 0.0, -1);
  shSchemaTransEntryAdd(om_trans, CONVERSION_BY_TYPE, "RMAX", "rmax", 
			"", NULL, NULL, NULL, NULL, 0.0, -1);
  shSchemaTransEntryAdd(om_trans, CONVERSION_BY_TYPE, "CMIN", "cmin", 
			"", NULL, NULL, NULL, NULL, 0.0, -1);
  shSchemaTransEntryAdd(om_trans, CONVERSION_BY_TYPE, "CMAX", "cmax", 
			"", NULL, NULL, NULL, NULL, 0.0, -1);
  shSchemaTransEntryAdd(om_trans, CONVERSION_BY_TYPE, "NPIX", "npix", 
			"", NULL, NULL, NULL, NULL, 0.0, -1);
  shSchemaTransEntryAdd(om_trans, CONVERSION_BY_TYPE, "S", "s", 
			"heap", "SPAN", "-1", NULL, NULL, 0.0, -1);
  shSchemaTransEntryAdd(om_trans, CONVERSION_BY_TYPE, "L9_S", "l9_s", 
			"", NULL, NULL, NULL, NULL, 0.0, -1);
  phFitsBinDeclareSchemaTrans(om_trans, "OBJMASK");

  return(om_trans);

}

SCHEMATRANS *thSchemaGetObject1 () {
  
  char *name = "thSchemaGetObject1";

  SCHEMATRANS *object1_trans;
  object1_trans = shSchemaTransNew();
  
  char **tform, **ttype, *TTYPE;
  
  int i, j;
  int n1 = 17; /* index of the first heap field */
  int n2 = 99; /* 1 + index of the last heap field */
  
  tform = (char **) thCalloc(n2, sizeof(char *));
  ttype = (char **) thCalloc(n2, sizeof(char *));
  
  for (i = n1; i < n2; i++) {
    tform[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
    ttype[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }
  
  TTYPE = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  
  /* 
     Unpacked OBJECT1s
  */
  
  i = n1;

  strcpy(tform[i], "float");                                                   
  strcpy(ttype[i], "rowc");//rowc         
  i++;

  strcpy(tform[i], "float");                                                 
  strcpy(ttype[i], "rowcErr");//rowcErr                                             
  i++;

  strcpy(tform[i], "float");                                           
  strcpy(ttype[i], "colc");//colc
  i++;

  strcpy(tform[i], "float");                                          
  strcpy(ttype[i], "colcErr");//colcErr     
  i++;

  strcpy(tform[i], "float");                    
  strcpy(ttype[i], "sky");//sky        
  i++;

  strcpy(tform[i], "float");                                                 
  strcpy(ttype[i], "skyErr");//skyErr        
  i++;

  /*
    PSF and aperture fits / magnitude
  */

  strcpy(tform[23], "float");                                               
  strcpy(ttype[23], "psfCounts");//psfCounts                                                

  strcpy(tform[24], "float");                                                 
  strcpy(ttype[24], "psfCountsErr");//psfCountsErr                                          

  strcpy(tform[25], "float");                                             
  strcpy(ttype[25], "fiberCounts");//fiberCounts                                            

  strcpy(tform[26], "float");                                              
  strcpy(ttype[26], "fiberCountsErr");//fiberCountsErr                                      

  strcpy(tform[27], "float");                                                
  strcpy(ttype[27], "petroCounts");//petroCounts                                            

  strcpy(tform[28], "float");                                              
  strcpy(ttype[28], "petroCountsErr");//petroCountsErr                                      

  strcpy(tform[29], "float");                                               
  strcpy(ttype[29], "petroRad");//petroRad                                                  

  strcpy(tform[30], "float");                                                
  strcpy(ttype[30], "petroRadErr");//petroRadErr                                            

  strcpy(tform[31], "float");                                              
  strcpy(ttype[31], "petroR50");//petroR50                                                  

  strcpy(tform[32], "float");                                             
  strcpy(ttype[32], "petroR50Err");//petroR50Err                                            

  strcpy(tform[33], "float");                                             
  strcpy(ttype[33], "petroR90");//petroR90                                                  

  strcpy(tform[34], "float");                                             
  strcpy(ttype[34], "petroR90Err");//petroR90Err                                            

  /* 
     Shape of object
  */

  strcpy(tform[35], "float");                                              
  strcpy(ttype[35], "Q ");//Q                                                         

  strcpy(tform[36], "float");                                                
  strcpy(ttype[36], "QErr");//QErr                                                      

  strcpy(tform[37], "float");                                                
  strcpy(ttype[37], "U ");//U                                                         

  strcpy(tform[38], "float");                                                
  strcpy(ttype[38], "UErr");//UErr                                                      

  strcpy(tform[39], "float");                                                 
  strcpy(ttype[39], "M_e1");//M_e1                                                      

  strcpy(tform[40], "float");                                              
  strcpy(ttype[40], "M_e2");//M_e2                                                      

  strcpy(tform[41], "float");                                               
  strcpy(ttype[41], "M_e1e1Err");//M_e1e1Err                                                

  strcpy(tform[42], "float");                                                 
  strcpy(ttype[42], "M_e1e2Err");//M_e1e2Err                                                

  strcpy(tform[43], "float");                                                
  strcpy(ttype[43], "M_e2e2Err");//M_e2e2Err                                                

  strcpy(tform[44], "float");                                                 
  strcpy(ttype[44], "M_rr_cc");//M_rr_cc                                                   

  strcpy(tform[45], "float");                                                
  strcpy(ttype[45], "M_rr_ccErr");//M_rr_ccErr                                              

  strcpy(tform[46], "float");                                                
  strcpy(ttype[46], "M_cr4");//M_cr4                                                     

  strcpy(tform[47], "float");                                               
  strcpy(ttype[47], "M_e1_psf");//M_e1_psf                                                  

  strcpy(tform[48], "float");                                                 
  strcpy(ttype[48], "M_e2_psf");//M_e2_psf                                                  

  strcpy(tform[49], "float");                                               
  strcpy(ttype[49], "M_rr_cc_psf");//M_rr_cc_psf                                            

  strcpy(tform[50], "float");                                                 
  strcpy(ttype[50], "M_cr4_psf");//M_cr4_psf                                                

  /* 
     Properties of certain isophote 
   */

  strcpy(tform[51], "float");                                                 
  strcpy(ttype[51], "iso_rowc");//iso_rowc                                                  

  strcpy(tform[52], "float");                                                 
  strcpy(ttype[52], "iso_rowcErr");//iso_rowcErr                                            

  strcpy(tform[53], "float");                                                 
  strcpy(ttype[53], "iso_rowcGrad");//iso_rowcGrad                                          

  strcpy(tform[54], "float");                                                 
  strcpy(ttype[54], "iso_colc");//iso_colc                                                  

  strcpy(tform[55], "float");                                                
  strcpy(ttype[55], "iso_colcErr");//iso_colcErr                                            

  strcpy(tform[56], "float");                                               
  strcpy(ttype[56], "iso_colcGrad");//iso_colcGrad                                          

  strcpy(tform[57], "float");                                              
  strcpy(ttype[57], "iso_a");//iso_a                                                     

  strcpy(tform[58], "float");                                                
  strcpy(ttype[58], "iso_aErr");//iso_aErr                                                  

  strcpy(tform[59], "float");                                                
  strcpy(ttype[59], "iso_aGrad");//iso_aGrad                                                

  strcpy(tform[60], "float");                                              
  strcpy(ttype[60], "iso_b");//iso_b                                                     

  strcpy(tform[61], "float");                                               
  strcpy(ttype[61], "iso_bErr");//iso_bErr                                                  

  strcpy(tform[62], "float");                                               
  strcpy(ttype[62], "iso_bGrad");//iso_bGrad                                                

  strcpy(tform[63], "float");                                             
  strcpy(ttype[63], "iso_phi");//iso_phi                                                   

  strcpy(tform[64], "float");                                              
  strcpy(ttype[64], "iso_phiErr");//iso_phiErr                                              

  strcpy(tform[65], "float");                                               
  strcpy(ttype[65], "iso_phiGrad");//iso_phiGrad                                            

  /* 
     model parameters for deV and exponential models
  */

  strcpy(tform[66], "float");                                               
  strcpy(ttype[66], "r_deV");//r_deV                                                     

  strcpy(tform[67], "float");                                               
  strcpy(ttype[67], "r_deVErr");//r_deVErr                                                  

  strcpy(tform[68], "float");                                               
  strcpy(ttype[68], "ab_deV");//ab_deV                                                    

  strcpy(tform[69], "float");                                                 
  strcpy(ttype[69], "ab_deVErr");//ab_deVErr                                                

  strcpy(tform[70], "float");                                                 
  strcpy(ttype[70], "phi_deV");//phi_deV                                                   

  strcpy(tform[71], "float");                                              
  strcpy(ttype[71], "phi_deVErr");//phi_deVErr                                              

  strcpy(tform[72], "float");                                               
  strcpy(ttype[72], "counts_deV");//counts_deV                                              

  strcpy(tform[73], "float");                                             
  strcpy(ttype[73], "counts_deVErr");//counts_deVErr                                        

  strcpy(tform[74], "float");                                           
  strcpy(ttype[74], "r_exp");//r_exp                                                     

  strcpy(tform[75], "float");                                       
  strcpy(ttype[75], "r_expErr");//r_expErr                                                 

  strcpy(tform[76], "float");                                                  
  strcpy(ttype[76], "ab_exp");//ab_exp                                                    

  strcpy(tform[77], "float");                                                  
  strcpy(ttype[77], "ab_expErr");//ab_expErr                                                

  strcpy(tform[78], "float");                                                  
  strcpy(ttype[78], "phi_exp");//phi_exp                                                   

  strcpy(tform[79], "float");                                                  
  strcpy(ttype[79], "phi_expErr");//phi_expErr                                              

  strcpy(tform[80], "float");                                                  
  strcpy(ttype[80], "counts_exp");//counts_exp                                              

  strcpy(tform[81], "float");                                                  
  strcpy(ttype[81], "counts_expErr");//counts_expErr                                        

  strcpy(tform[82], "float");                                                     
  strcpy(ttype[82], "counts_model");//counts_model                                          

  strcpy(tform[83], "float");                                                     
  strcpy(ttype[83], "counts_modelErr");//counts_modelErr                                    

  
  /* 
     Measures of image structure 
   */

  strcpy(tform[84], "float");                                                     
  strcpy(ttype[84], "texture");//texture                                                   

  /* 
     Classification information
  */
  
  strcpy(tform[85], "float");                                                     
  strcpy(ttype[85], "star_L");//star_L                                                    

  strcpy(tform[86], "float");                                                     
  strcpy(ttype[86], "star_lnL");//star_lnL                                                  

  strcpy(tform[87], "float");                                                     
  strcpy(ttype[87], "exp_L");//exp_L                                                     

  strcpy(tform[88], "float");                                                     
  strcpy(ttype[88], "exp_lnL");//exp_lnL                                                   

  strcpy(tform[89], "float");                                                     
  strcpy(ttype[89], "deV_L");//deV_L                                                     

  strcpy(tform[90], "float");                                                     
  strcpy(ttype[90], "deV_lnL");//deV_lnL                                                   

  strcpy(tform[91], "float");                                                     
  strcpy(ttype[91], "fracPSF");//fracPSF                                                   

  strcpy(tform[92], "int");                                                       
  strcpy(ttype[92], "flags");//flags                                                     

  strcpy(tform[93], "int");                                                       
  strcpy(ttype[93], "flags2");//flags2                                                    

  strcpy(tform[94], "OBJ_TYPE"); //OBJ_TYPE                                                  
  strcpy(ttype[94], "type");//type                                                      

  strcpy(tform[95], "float");                                                     
  strcpy(ttype[95], "prob_psf");//prob_psf                                                  

  /* 
     Profile and extent of object
  */

  strcpy(tform[96], "int");                                                       
  strcpy(ttype[96], "nprof");//nprof                                                     

  strcpy(tform[97], "float");                                                     
  strcpy(ttype[97], "profMean");//profMean                                                  
  
  /* 
     you don't really need the size in schematrans 
     TDIM[97], "(15,5)"  ;                           
  */

  strcpy(tform[98], "float");                                                     
  strcpy(ttype[98], "profErr");//profErr                                                 
  /* 
     you don't really need the size in schematrans
     TDIM[98] = "(15,5)";                
  */

    for (i = n1; i < n2; i++) {
    /* 
       TTYPE is the upper cased ttype 
       this is how the fields are put in the
       binary tables 
    */
    TTYPE[strlen(ttype[i])] = '\0'; 
    for(j = 0; j < strlen(ttype[i]); j++) {
      TTYPE[j] = toupper(ttype[i][j]);
    }
    /*
    shSchemaTransEntryAdd(object1_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			  "heap", tform[i], "-1", NULL, NULL, 0.0, -1);
    */
    /* based on the sample given in photoFitsIo.c */
    shSchemaTransEntryAdd(object1_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			  "", NULL,NULL, NULL, NULL, 0.0, -1);
  }


    phFitsBinDeclareSchemaTrans(object1_trans, "OBJECT1");

  return(object1_trans);
  
}

SCHEMATRANS *thSchemaGetObjc() {

  SCHEMATRANS *objc_trans;
  objc_trans = shSchemaTransNew();

  char **tform, **ttype, *TTYPE;

  int i, j;
  int n1 = 17; /* index of the first heap field */
  int n2 = 99; /* 1 + index of the last heap field */

  tform = (char **) thCalloc(n2, sizeof(char *));
  ttype = (char **) thCalloc(n2, sizeof(char *));

  for (i = 0; i < n2; i++) {
    tform[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
    ttype[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }
  
  TTYPE = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  
  tform[1] = "int";                                                       
  ttype[1] = "id" ;//id      
  
  tform[2] = "int";                                                       
  ttype[2] = "parent" ;//parent  
  
  tform[3] = "int";                                                       
  ttype[3] = "nchild" ;//nchild
                                                    
  tform[4] = "OBJ_TYPE"; //OBJ_TYPE                                                  
  ttype[4] = "objc_type" ;//objc_type                                                

  tform[5] = "float";                                                     
  ttype[5] = "objc_prob_psf" ;//objc_prob_psf                                        

  tform[6] = "int";                                                       
  ttype[6] = "catID" ;//catID                                                     

  tform[7] = "int";                                                       
  ttype[7] = "objc_flags" ;//objc_flags                                              

  tform[8] = "int";                                                       
  ttype[8] = "objc_flags2" ;//objc_flags2                                            

  tform[9] = "float";                                                     
  ttype[9] = "objc_rowc" ;//objc_rowc                                                

  tform[10] = "float";                                                     
  ttype[10] = "objc_rowcErr" ;//objc_rowcErr                                          

  tform[11] = "float";                                                     
  ttype[11] = "objc_colc" ;//objc_colc     

  tform[12] = "float";                                                     
  ttype[12] = "objc_colcErr" ;//objc_colcErr                                          

  tform[13] = "float";                                                     
  ttype[13] = "rowv" ;//rowv                                                      

  tform[14] = "float";                                                     
  ttype[14] = "rowvErr" ;//rowvErr                                                   

  tform[15] = "float";                                                     
  ttype[15] = "colv" ;//colv                                                      

  tform[16] = "float";                                                     
  ttype[16] = "colvErr" ;//colvErr   

 
  for (i = 1; i < n1; i++) {
    /* 
       TTYPE is the upper cased ttype 
       this is how the fields are put in the
       binary tables 
    */
    TTYPE[strlen(ttype[i])] = '\0'; 
    for(j = 0; j < strlen(ttype[i]); j++) {
      TTYPE[j] = toupper(ttype[i][j]);
    }

    /*
      only one of the following should be used 
      shSchemaTransEntryAdd(objc_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
      "", NULL, NULL, NULL, NULL, 0.0, -1);
    */
    shSchemaTransEntryAdd(objc_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			  "", tform[i], NULL, NULL, NULL, 0.0, -1);
  }

  shSchemaTransEntryAdd(objc_trans, CONVERSION_BY_TYPE, "COLOR", "color", 
			"", NULL, NULL, NULL, NULL, 0.0, -1);

  phFitsBinDeclareSchemaTrans(objc_trans, "OBJC");
  
  return(objc_trans);
  
}


int check_for_exception(char *in, char **except, int nexcept) {
int res = 0;
shAssert(in != NULL);
if (except == NULL) return(res);
int i;
for (i = 0; i < nexcept; i++) {
	char *item = except[i];
	if (item != NULL && strlen(item) != 0) {
		res = res || !strcmp(in, item);
	}
}
return(res);
}


SCHEMATRANS *thSchemaGetGalacticCalib() {

  char *name = "thSchemaGetGalacticCalib";
  SCHEMATRANS *gc_trans;
  gc_trans = shSchemaTransNew();
  char *type = "GALACTIC_CALIB";
  TYPE t = shTypeGetFromName(type);
  SCHEMA *schema;
  schema = shSchemaGetFromType(t);
  if (schema == NULL) {
	thError("%s: ERROR - could not get schema for '%s'", name, type);
	return(NULL);
 }
int nelem = schema->nelem;
SCHEMA_ELEM *els = schema->elems;
char *ENAME = thCalloc(MX_STRING_LEN, sizeof(char)); 
int i, added = 0;
for (i = 0; i < nelem; i++) {
	SCHEMA_ELEM *el = els + i;
	char *ename = el->name;
	ENAME[strlen(ename)] = '\0';
	int j;
	for (j = 0; j < strlen(ename); j++) {
		ENAME[j] = toupper(ename[j]);
		}
	added++;
	shSchemaTransEntryAdd(gc_trans, CONVERSION_BY_TYPE, ENAME, ename, "", NULL, NULL, NULL, NULL, 0.0, -1);	
}
thFree(ENAME);
phFitsBinDeclareSchemaTrans(gc_trans, type);	
#if PRINT_IO_SCHEMA_TRANS   
shSchemaTransEntryShow(gc_trans, 0, added - 1);  
#endif
return(gc_trans);

}

SCHEMATRANS *thSchemaGetCalibPhObjcIo(IO_MODE mode) {

  char *name = "thSchemaGetCalibPhObjcIo";
  SCHEMATRANS *objc_trans1, *objc_trans2;
  objc_trans1 = shSchemaTransNew();
  objc_trans2 = shSchemaTransNew();
  shAssert(mode == PHOTO_INPUT_MODE || mode == DUMP_MODE);

	int nexcept = 0;
	if (mode == PHOTO_INPUT_MODE) nexcept++;
	char **except = NULL;
	if (nexcept != 0) except = thCalloc(nexcept, sizeof(char *));
	if (mode == PHOTO_INPUT_MODE) except[0] = "flagLRG";



   	#if CALIB_PHOBJC_IO_CHAR_ARRAY
	int nheap = 0;
	char **heap = NULL;
	#else
	int nheap = 4;
	char **heap = thCalloc(nheap, sizeof(char *));
	heap[0] = "objid";
	heap[1] = "parentid";
	heap[2] = "fieldid";
	heap[3] = "rerun";
	#endif

	char *type = "CALIB_PHOBJC_IO";
	TYPE t = shTypeGetFromName(type);
	SCHEMA *schema;
	schema = shSchemaGetFromType(t);
	if (schema == NULL) {
		thError("%s: ERROR - could not get schema for '%s'", name, type);
		return(NULL);
	}
	int nelem = schema->nelem;
	SCHEMA_ELEM *els = schema->elems;
	char *ENAME = thCalloc(MX_STRING_LEN, sizeof(char)); 
	int i, added = 0;
	for (i = 0; i < nelem; i++) {
		SCHEMA_ELEM *el = els + i;
		char *etype = el->type;
		char *ename = el->name;
		ENAME[strlen(ename)] = '\0';
		int j;
		for (j = 0; j < strlen(ename); j++) {
			ENAME[j] = toupper(ename[j]);
		}
		if (check_for_exception(ename, except, nexcept) == 0) {
			added++;
			int is_this_heap = check_for_exception(ename, heap, nheap);	
	/* Examples 
	* not a heap 
      shSchemaTransEntryAdd(klc_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			    "", NULL, NULL, NULL, NULL, 0.0, -1);
	 * heap *
      shSchemaTransEntryAdd(klc_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			    "", tform[i], NULL, NULL, NULL, 0.0, -1);
	*/
		        if (is_this_heap == 1) {
			shSchemaTransEntryAdd(objc_trans1, CONVERSION_BY_TYPE, 
			ENAME, ename, "",etype,NULL,NULL,NULL,0.0,-1);
			#if CALIB_PHOBJC_IO_USE_ALTERED_FITS_PACKAGE
			shSchemaTransEntryAdd(objc_trans2, CONVERSION_BY_TYPE, 
			ENAME, ename, "",etype,NULL,NULL,NULL,0.0,-1);
			#endif
			} else {
		 	shSchemaTransEntryAdd(objc_trans1, CONVERSION_BY_TYPE, 
			ENAME, ename, "", NULL, NULL, NULL, NULL, 0.0, -1);	
			#if CALIB_PHOBJC_IO_USE_ALTERED_FITS_PACKAGE
		  	shSchemaTransEntryAdd(objc_trans2, CONVERSION_BY_TYPE, 
			ENAME, ename, "", NULL, NULL, NULL, NULL, 0.0, -1);
			#endif
			}
			#if PRINT_IO_SCHEMA_TRANS   
			printf("%s: %d - %s, %s, %s \n", name, added, ENAME, ename, etype);
			#endif
 		}
	}
	thFree(ENAME);
	#if CALIB_PHOBJC_IO_USE_ALTERED_FITS_PACKAGE
	phFitsBinDeclareSchemaTrans(objc_trans1, type); 	
	thFitsBinDeclareSchemaTrans(objc_trans2, type);	
	#else
	phFitsBinDeclareSchemaTrans(objc_trans1, type);
	#endif
	#if PRINT_IO_SCHEMA_TRANS  
	shSchemaTransEntryShow(objc_trans1, 0, added - 1);  
	#endif
  	return(objc_trans1);

}



SCHEMATRANS *thSchemaGetBinregion() {

  char *name = "thSchemaGetBinregion";
  SCHEMATRANS *binregion_trans;
  binregion_trans = shSchemaTransNew();


	char *type = "THBINREGION";
	TYPE t = shTypeGetFromName(type);
	SCHEMA *schema;
	schema = shSchemaGetFromType(t);
	if (schema == NULL) {
		thError("%s: ERROR - could not get schema for '%s'", name, type);
		return(NULL);
	}
	int nelem = schema->nelem;
	SCHEMA_ELEM *els = schema->elems;
	char *ENAME = thCalloc(MX_STRING_LEN, sizeof(char)); 
	int i, added = 0;
	for (i = 0; i < nelem; i++) {
		SCHEMA_ELEM *el = els + i;
		char *etype = el->type;
		char *ename = el->name;
		ENAME[strlen(ename)] = '\0';
		int j;
		for (j = 0; j < strlen(ename); j++) {
			ENAME[j] = toupper(ename[j]);
		}
		added++;	
		 shSchemaTransEntryAdd(binregion_trans, CONVERSION_BY_TYPE, ENAME, ename, "", etype, NULL, NULL, NULL, 0.0, -1);
		#if PRINT_IO_SCHEMA_TRANS   
		printf("%s: %d - %s, %s, %s \n", name, added, ENAME, ename, etype);
		#endif
	}
	thFree(ENAME);
	phFitsBinDeclareSchemaTrans(binregion_trans, type);
	#if PRINT_IO_SCHEMA_TRANS  
	shSchemaTransEntryShow(binregion_trans, 0, added - 1);  
	#endif
  	return(binregion_trans);

}




SCHEMATRANS *thSchemaGetFnalObjcIo(IO_MODE mode) {

  char *name = "thSchemaGetFnalObjcIo";
  SCHEMATRANS *objc_trans;
  objc_trans = shSchemaTransNew();
  shAssert(mode == DUMP_MODE || mode == PHOTO_INPUT_MODE)


	int nexcept = 4;
	if (mode == PHOTO_INPUT_MODE) nexcept = 49;
	char **except = thCalloc(nexcept, sizeof(char *));
	except[0] = "ncolor";
	except[1] = "aimage";
	except[2] = "test";
	except[3] = "dgpsf";
	if (mode == PHOTO_INPUT_MODE) {
		except[4] = "flagLRG";
		except[5] = "r_deV2";
		except[6] = "r_deV2Err";
		except[7] = "ab_deV2";
		except[8] = "ab_deV2Err";
		except[9] = "phi_deV2";
		except[10] = "phi_deV2Err";
		except[11] = "counts_deV2";
		except[12] = "counts_deV2Err";
		except[13] = "r_exp2";
		except[14] = "r_exp2Err";
		except[15] = "ab_exp2";
		except[16] = "ab_exp2Err";
		except[17] = "phi_exp2";
		except[18] = "phi_exp2Err";
		except[19] = "counts_exp2";
		except[20] = "counts_exp2Err";
		except[21] = "counts_model2";
		except[22] = "counts_model2Err";
		except[23] = "r_pl";
		except[24] = "r_plErr";
		except[25] = "ab_pl";
		except[26] = "ab_plErr";
		except[27] = "phi_pl";
		except[28] = "phi_plErr";
		except[29] = "counts_pl";
		except[30] = "counts_plErr";
		except[31] = "n_pl";
		except[32] = "n_plErr";
		except[33] = "r_csersic";
		except[34] = "r_csersicErr";
		except[35] = "ab_csersic";
		except[36] = "ab_csersicErr";
		except[37] = "phi_csersic";
		except[38] = "phi_csersicErr";
		except[39] = "counts_csersic";
		except[40] = "counts_csersicErr";
		except[41] = "n_csersic";
		except[42] = "n_csersicErr";
		except[43] = "d_csersic";
		except[44] = "d_csersicErr";
		except[45] = "g_csersic";
		except[46] = "g_csersicErr";
		except[47] = "rb_csersic";
		except[48] = "rb_csersicErr";

	}

	char *type = "FNAL_OBJC_IO";
	TYPE t = shTypeGetFromName(type);
	SCHEMA *schema;
	schema = shSchemaGetFromType(t);
	if (schema == NULL) {
		thError("%s: ERROR - could not get schema for '%s'", name, type);
		return(NULL);
	}
	int nelem = schema->nelem;
	SCHEMA_ELEM *els = schema->elems;
	char *ENAME = thCalloc(MX_STRING_LEN, sizeof(char)); 
	int i, added = 0;
	for (i = 0; i < nelem; i++) {
		SCHEMA_ELEM *el = els + i;
		char *etype = el->type;
		char *ename = el->name;
		ENAME[strlen(ename)] = '\0';
		int j;
		for (j = 0; j < strlen(ename); j++) {
			ENAME[j] = toupper(ename[j]);
		}
		if (check_for_exception(ename, except, nexcept) == 0) {
			added++;	
		  	shSchemaTransEntryAdd(objc_trans, CONVERSION_BY_TYPE, 
			ENAME, ename, "", etype, NULL, NULL, NULL, 0.0, -1);
			#if PRINT_IO_SCHEMA_TRANS   
			printf("%s: %d - %s, %s, %s \n", name, added, ENAME, ename, etype);
			#endif
 		}
	}
	thFree(ENAME);
	phFitsBinDeclareSchemaTrans(objc_trans, type);
	#if PRINT_IO_SCHEMA_TRANS  
	shSchemaTransEntryShow(objc_trans, 0, added - 1);  
	#endif
  	return(objc_trans);

}

SCHEMATRANS *thSchemaGetObjcIo(IO_MODE mode) {

  char *name = "thSchemaGetObjcIo";
  SCHEMATRANS *objc_trans;
  objc_trans = shSchemaTransNew();
  shAssert(mode == DUMP_MODE || mode == PHOTO_INPUT_MODE);


	int nexcept = 6;
	if (mode == PHOTO_INPUT_MODE) nexcept++;
	char **except = thCalloc(nexcept, sizeof(char *));
	except[0] = "ncolor";
	except[1] = "aimage";
	except[2] = "test";
	except[3] = "dgpsf";
	except[4] = "fiber2Counts";
	except[5] = "fiber2CountsErr";
	if (mode == PHOTO_INPUT_MODE) except[6] = "flagLRG";
	char *type = "OBJC_IO";
	TYPE t = shTypeGetFromName(type);
	SCHEMA *schema;
	schema = shSchemaGetFromType(t);
	if (schema == NULL) {
		thError("%s: ERROR - could not get schema for '%s'", name, type);
		return(NULL);
	}
	int nelem = schema->nelem;
	SCHEMA_ELEM *els = schema->elems;
	char *ENAME = thCalloc(MX_STRING_LEN, sizeof(char)); 
	int i, added = 0;
	for (i = 0; i < nelem; i++) {
		SCHEMA_ELEM *el = els + i;
		char *etype = el->type;
		char *ename = el->name;
		ENAME[strlen(ename)] = '\0';
		int j;
		for (j = 0; j < strlen(ename); j++) {
			ENAME[j] = toupper(ename[j]);
		}
		if (check_for_exception(ename, except, nexcept) == 0) {
			added++;	
		  	shSchemaTransEntryAdd(objc_trans, CONVERSION_BY_TYPE, 
			ENAME, ename, "", etype, NULL, NULL, NULL, 0.0, -1);
			#if PRINT_IO_SCHEMA_TRANS   
			printf("%s: %d - %s, %s, %s \n", name, added, ENAME, ename, etype);
			#endif
 		}
	}
	thFree(ENAME);
	phFitsBinDeclareSchemaTrans(objc_trans, type);
	#if PRINT_IO_SCHEMA_TRANS  
	shSchemaTransEntryShow(objc_trans, 0, added - 1);  
	#endif
  	return(objc_trans);

#if 0	
  char **tform, **ttype, *TTYPE;

  int i, j;
  int n1 = 17; /* index of the first heap field */
  int n2 = 99; /* 1 + index of the last heap field */

  tform = (char **) thCalloc(n2, sizeof(char *));
  ttype = (char **) thCalloc(n2, sizeof(char *));
  #if 0
  tform[0] = (char *) thCalloc(n2 * MX_STRING_LEN, sizeof(char));
  ttype[0] = (char *) thCalloc(n2 * MX_STRING_LEN, sizeof(char));

  for (i = 1; i < n2; i++) {
    tform[i] = tform[0] + i * MX_STRING_LEN;
    ttype[i] = ttype[0] + i * MX_STRING_LEN;
  }
  #endif

  TTYPE = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  i = 0;
  tform[i] = "int";                                                       
  ttype[i] = "id" ;//id      
  i++;
 
  tform[i] = "int";                                                       
  ttype[i] = "parent" ;//parent  
  i++;

  tform[i] = "int";
  ttype[i] = "ncolor";
  i++;
 
  tform[i] = "int";                                                       
  ttype[i] = "nchild" ;//nchild
  i++;
                                              
  tform[i] = "OBJ_TYPE"; //OBJ_TYPE                     
  ttype[i] = "objc_type" ;//objc_type                                               
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "objc_prob_psf" ;//objc_prob_psf                                        
  i++;

  tform[i] = "int";                                                       
  ttype[i] = "catID" ;//catID                                                     
  i++;

  tform[i] = "int";                                                       
  ttype[i] = "objc_flags" ;//objc_flags              
  i++;

  tform[i] = "int";                                                       
  ttype[i] = "objc_flags2" ;//objc_flags2                                            
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "objc_rowc" ;//objc_rowc                                                
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "objc_rowcErr" ;//objc_rowcErr
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "objc_colc" ;//objc_colc     
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "objc_colcErr" ;//objc_colcErr 
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "rowv" ;//rowv   
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "rowvErr" ;//rowvErr         
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "colv" ;//colv                                                      
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "colvErr" ;//colvErr   
  i++;
  /* from now on we are dealing with heaps */

    /* 
     Unpacked OBJECT1s
  */

  tform[i] = "float";                                                     
  ttype[i] = "rowc" ;//rowc                                                      
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "rowcErr" ;//rowcErr                                                   
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "colc" ;//colc                                                      
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "colcErr" ;//colcErr
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "sky" ;//sky
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "skyErr" ;//skyErr                                                    
  i++;
  /*
    PSF and aperture fits / magnitude
  */

  tform[i] = "float";                                                     
  ttype[i] = "psfCounts" ;//psfCounts                                                
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "psfCountsErr" ;//psfCountsErr                                          
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "fiberCounts" ;//fiberCounts                                            
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "fiberCountsErr" ;//fiberCountsErr                                      
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "petroCounts" ;//petroCounts                                            
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "petroCountsErr" ;//petroCountsErr                                      
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "petroRad" ;//petroRad                                                  
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "petroRadErr" ;//petroRadErr                                            
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "petroR50" ;//petroR50                                                  
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "petroR50Err" ;//petroR50Err                                            
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "petroR90" ;//petroR90                                                  
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "petroR90Err" ;//petroR90Err                                            
  i++;
  /* 
     Shape of object
  */

  tform[i] = "float";                                                     
  ttype[i] = "Q " ;//Q                                                         
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "QErr" ;//QErr                                                      
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "U " ;//U                                                         
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "UErr" ;//UErr                                                      
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "M_e1" ;//M_e1                                                      
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "M_e2" ;//M_e2                                                      
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "M_e1e1Err" ;//M_e1e1Err                                                
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "M_e1e2Err" ;//M_e1e2Err                                                
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "M_e2e2Err" ;//M_e2e2Err                                                
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "M_rr_cc" ;//M_rr_cc                                                   
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "M_rr_ccErr" ;//M_rr_ccErr                                              
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "M_cr4" ;//M_cr4                                                     
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "M_e1_psf" ;//M_e1_psf                                                  
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "M_e2_psf" ;//M_e2_psf                                                  
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "M_rr_cc_psf" ;//M_rr_cc_psf                                            
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "M_cr4_psf" ;//M_cr4_psf                                                
  i++;
  /* 
     Properties of certain isophote 
   */

  tform[i] = "float";                                                     
  ttype[i] = "iso_rowc" ;//iso_rowc                                                  
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "iso_rowcErr" ;//iso_rowcErr                                            
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "iso_rowcGrad" ;//iso_rowcGrad                                          
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "iso_colc" ;//iso_colc                                                  
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "iso_colcErr" ;//iso_colcErr                                            
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "iso_colcGrad" ;//iso_colcGrad                                          
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "iso_a" ;//iso_a                                                     
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "iso_aErr" ;//iso_aErr                                                  
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "iso_aGrad" ;//iso_aGrad                                                
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "iso_b" ;//iso_b                                                     
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "iso_bErr" ;//iso_bErr                                                  
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "iso_bGrad" ;//iso_bGrad                                                
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "iso_phi" ;//iso_phi                                                   
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "iso_phiErr" ;//iso_phiErr                                              
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "iso_phiGrad" ;//iso_phiGrad                                            
  i++;
  /* 
     model parameters for deV and exponential models
  */

  tform[i] = "float";                                                     
  ttype[i] = "r_deV" ;//r_deV                                                     
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "r_deVErr" ;//r_deVErr                                                  
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "ab_deV" ;//ab_deV                                                    
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "ab_deVErr" ;//ab_deVErr                                                
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "phi_deV" ;//phi_deV                                                   
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "phi_deVErr" ;//phi_deVErr                                              
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "counts_deV" ;//counts_deV                                              
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "counts_deVErr" ;//counts_deVErr                                        
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "r_exp" ;//r_exp                                                     
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "r_expErr" ;//r_expErr                                                  
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "ab_exp" ;//ab_exp                                                    
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "ab_expErr" ;//ab_expErr                                                
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "phi_exp" ;//phi_exp                                                   
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "phi_expErr" ;//phi_expErr                                              
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "counts_exp" ;//counts_exp                                              
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "counts_expErr" ;//counts_expErr                                        
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "counts_model" ;//counts_model                                          
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "counts_modelErr" ;//counts_modelErr                                    
  i++;
  
  /* 
     Measures of image structure 
   */

  tform[i] = "float";                                                     
  ttype[i] = "texture" ;//texture                                                   
  i++;
  /* 
     Classification information
  */
  
  tform[i] = "float";                                                     
  ttype[i] = "star_L" ;//star_L                                                    
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "star_lnL" ;//star_lnL                                                  
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "exp_L" ;//exp_L                                                     
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "exp_lnL" ;//exp_lnL                                                   
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "deV_L" ;//deV_L                                                     
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "deV_lnL" ;//deV_lnL                                                   
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "fracPSF" ;//fracPSF                                                   
  i++;

  tform[i] = "int";                                                       
  ttype[i] = "flags" ;//flags                                                     
  i++;

  tform[i] = "int";                                                       
  ttype[i] = "flags2" ;//flags2                                                    
  i++;

  tform[i] = "OBJ_TYPE"; //OBJ_TYPE
  ttype[i] = "type" ;//type                                                      
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "prob_psf" ;//prob_psf                                                  
  i++;
  /* 
     Profile and extent of object
  */

  tform[i] = "int";                                                       
  ttype[i] = "nprof" ;//nprof                                                     
  i++;

  tform[i] = "float";                                                     
  ttype[i] = "profMean" ;//profMean                                                  
  i++; 
  /* 
     you don't really need the size in schematrans 
     TDIM[97] = "(15,5)"  ;                           
  */

  tform[i] = "float";                                                     
  ttype[i] = "profErr" ;//profErr                                                 
  i++;
  /* 
     you don't really need the size in schematrans
     TDIM[98] = "(15,5)";                
  */

  if (i != n2) {
	thError("%s: (%d) schema elements added while expected (%d)", name, i, n1);
	return(NULL);
	}

  for (i = 0; i < n1; i++) {
    /* 
       TTYPE is the upper cased ttype 
       this is how the fields are put in the
       binary tables 
    */
    TTYPE[strlen(ttype[i])] = '\0'; 
    for(j = 0; j < strlen(ttype[i]); j++) {
      TTYPE[j] = toupper(ttype[i][j]);
    }

    /*
      only one of the following should be used 
      shSchemaTransEntryAdd(objc_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
      "", NULL, NULL, NULL, NULL, 0.0, -1);
    */
    shSchemaTransEntryAdd(objc_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			  "", tform[i], NULL, NULL, NULL, 0.0, -1);
  }

  for (i = n1; i < n2; i++) {
    
    TTYPE[strlen(ttype[i])] = '\0'; 
    for(j = 0; j < strlen(ttype[i]); j++) {
      TTYPE[j] = toupper(ttype[i][j]);
    }
    shSchemaTransEntryAdd(objc_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			  "", NULL, NULL, NULL, NULL, 0.0, -1);
    
  }
   
  #if 0
  thFree(tform[0]);
  thFree(ttype[0]);
  #endif
  thFree(tform);
  thFree(ttype);
  thFree(TTYPE);

  phFitsBinDeclareSchemaTrans(objc_trans, "OBJC_IO");
#if PRINT_IO_SCHEMA_TRANS  
  shSchemaTransEntryShow(objc_trans, 0, n2 - 1);  
#endif
  return(objc_trans);
#endif
  
}

SCHEMATRANS *thSchemaGetPhCalib() {
  char *name = "thSchemaGetPhCalib";

  SCHEMATRANS *phcalib_trans;
  phcalib_trans = shSchemaTransNew();

  char **tform, **ttype, *TTYPE;

  int i, j;
  int n1, n2, n = 16;

  tform = (char **) thMalloc(n * sizeof(char *));
  ttype = (char **) thMalloc(n * sizeof(char *));

  for (i = 0; i < n; i++) {
    tform[i] = (char *) thMalloc(MX_STRING_LEN * sizeof(char));
    ttype[i] = (char *) thMalloc(MX_STRING_LEN * sizeof(char));
  }
  
	TTYPE = thCalloc(MX_STRING_LEN, sizeof(char));

i = 0;
strcpy(tform[i], "int"); 
strcpy(ttype[i], "run"); /* (int32): drift scan run number */
i++; 

strcpy(tform[i], "char");
strcpy(ttype[i], "rerun"); /* (string): reprocessing rerun number */
i++;

strcpy(tform[i], "int");
strcpy(ttype[i], "camcol"); /* (int32): drift scan camcol number*/
i++;

strcpy(tform[i], "int");
strcpy(ttype[i], "field"); /* (int32): drift scan field number */
i++;
n1 = i;
   
strcpy(tform[i], "int");
strcpy(ttype[i], "image_status"); /* NCOLOR]; (int32): bitmask describing image status in each band */
i++;
   
strcpy(tform[i], "int");
strcpy(ttype[i], "calib_status"); /* [NCOLOR]; (int32): bitmask describing calibration status in each band */
i++;
  
strcpy(tform[i],  "double");
strcpy(ttype[i], "tai"); /* [NCOLOR]; (float64): TAI-format time each band was observed in (ugriz) */
i++;
  
strcpy(tform[i], "double");
strcpy(ttype[i], "ref_tai"); /* [NCOLOR]; (float64): reference time in TAI-format for time variation (kdot) */
i++;
  
strcpy(tform[i],  "float");
strcpy(ttype[i], "airmass"); /* [NCOLOR]; (float32): airmass at time of observation in each batch */
i++;
   
strcpy(tform[i], "float");
strcpy(ttype[i], "aterm"); /* [NCOLOR]; (float32): "a-term" in calibration equations in each band */
i++;
  
strcpy(tform[i],  "float");
strcpy(ttype[i], "kterm"); /* NCOLOR]; (float32): "k-term" in calibration equations in each band (at reference time) */
i++;
   
strcpy(tform[i], "float");
strcpy(ttype[i], "kdot"); /* [NCOLOR]; (float32): time variation of "k-term" in calibration equations in each band */
i++;
  
strcpy(tform[i],  "int");
strcpy(ttype[i], "nstars_offset"); /* [NCOLOR]; (int32): number of stars used in calculating field offset */
i++;

strcpy(tform[i], "float");
strcpy(ttype[i], "field_offset"); /* [NCOLOR]; (float32): magnitude offset of ubercalibration relative to input (???) */
i++;
   
strcpy(tform[i], "float");
strcpy(ttype[i], "nmgypercount"); /* [NCOLOR]; (float32): nanomaggies per count in each band */
i++;
  
strcpy(tform[i],  "float");
strcpy(ttype[i], "nmgypercount_ivar"); /* [NCOLOR]; (float32): inverse variance of nanomaggies per count in each band */
i++;
n2 = i;
n1 = 1;
  for (i = 0; i < n1; i++) {
    /* 
       TTYPE is the upper cased ttype 
       this is how the fields are put in the
       binary tables 
    */
    TTYPE[strlen(ttype[i])] = '\0'; 
    for(j = 0; j < strlen(ttype[i]); j++) {
      TTYPE[j] = toupper(ttype[i][j]);
    }

    /*
      only one of the following should be used
	*/

	/*  
      shSchemaTransEntryAdd(phcalib_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
      "", NULL, NULL, NULL, NULL, 0.0, -1);
    */
	shSchemaTransEntryAdd(phcalib_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			  "", tform[i], NULL, NULL, NULL, 0.0, -1);
}

/* 
  for (i = n1; i < n2; i++) {
    
    TTYPE[strlen(ttype[i])] = '\0'; 
    for(j = 0; j < strlen(ttype[i]); j++) {
      TTYPE[j] = toupper(ttype[i][j]);
    }
    shSchemaTransEntryAdd(phcalib_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			  "", NULL, NULL, NULL, NULL, 0.0, -1);
    
  }
   */
	/*  
    if (strcmp(tform[i], "char")) {
      shSchemaTransEntryAdd(sm_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			    "", NULL, NULL, NULL, NULL, 0.0, -1);
    } else {
      shSchemaTransEntryAdd(sm_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			    "heap", tform[i], "-1", NULL, NULL, 0.0, -1);
    }
 */
  phFitsBinDeclareSchemaTrans(phcalib_trans, "PHCALIB");
  
  return(phcalib_trans);
  
}

SCHEMATRANS *thSchemaGetPsfKLComp() {

  char *name = "thSchemaGetPsfKLComp";

  SCHEMATRANS *klc_trans;
  klc_trans = shSchemaTransNew();

  char **tform, **ttype, *TTYPE;

  int i, j;
  int n1, n = 6;

  tform = (char **) thMalloc(n * sizeof(char *));
  ttype = (char **) thMalloc(n * sizeof(char *));

  for (i = 0; i < n; i++) {
    tform[i] = (char *) thMalloc(MX_STRING_LEN * sizeof(char));
    ttype[i] = (char *) thMalloc(MX_STRING_LEN * sizeof(char));
  }

  i = 0;
  strcpy(tform[i], "int");
  strcpy(ttype[i], "nrow_b");
  i++;
  strcpy(tform[i], "int");
  strcpy(ttype[i], "ncol_b");
  i++;
  /* array */
  n1 = i;
  strcpy(tform[i], "float");
  strcpy(ttype[i], "c");
  i++;
  strcpy(tform[i], "float");
  strcpy(ttype[i], "lambda");		
  i++;
  strcpy(tform[i], "REGION");
  strcpy(ttype[i], "reg");			
  i++;
  strcpy(tform[i], "float");
  strcpy(ttype[i], "counts");
  i++;


  if (i != n) {
    shError("%s: problem indexing schema elements", name);
  }

  TTYPE = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  
  for (i = 0; i < n; i++) {
    /* 
       TTYPE is the upper cased ttype 
       this is how the fields are put in the
       binary tables 
    */
    TTYPE[strlen(ttype[i])] = '\0'; 
    for(j = 0; j < strlen(ttype[i]); j++) {
      TTYPE[j] = toupper(ttype[i][j]);
    }
    
    /*
      only one of the following should be used 

    */
    if (i != n1) {
	/* not a heap */
      shSchemaTransEntryAdd(klc_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			    "", NULL, NULL, NULL, NULL, 0.0, -1);
    } else {
	/* heap */
      shSchemaTransEntryAdd(klc_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			    "", tform[i], NULL, NULL, NULL, 0.0, -1);
    }
  }
  
  phFitsBinDeclareSchemaTrans(klc_trans, "PSF_KL_COMP");

  /* cleaning up */
  for (i = 0; i < n; i++) {
    thFree(ttype[i]);
    thFree(tform[i]);
  }
  thFree(TTYPE);
  thFree(tform);
  thFree(ttype);

  /* returning */
  return(klc_trans);
  
}

SCHEMATRANS *thSchemaGetCalibIo() {

  char *name = "thSchemaGetCalibIo";

  SCHEMATRANS *calibio_trans;
  calibio_trans = shSchemaTransNew();
  
  char **tform, **ttype, *TTYPE;
  
  int i, j;
  int n1, n = 29;
  
  TTYPE = (char *) thCalloc(MX_STRING_LEN, sizeof(char));

  tform = (char **) thCalloc(n, sizeof(char *));
  ttype = (char **) thCalloc(n, sizeof(char *));
  
  for (i = 0; i < n; i++) {
    tform[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
    ttype[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }
  
  i = 0;
  /* 
     strcpy(tform[i], "int"); strcpy(ttype[i], "ncolor"); i++; 
  */
  strcpy(tform[i], "int"); strcpy(ttype[i], "field"); i++;
  /* 
     tform[i] = "double"; strcpy(ttype[i], "mjd"; i++; 
  */
  /* the following is replaced by proper type definition as INT */
  strcpy(tform[i], "PSP_STATUS"); strcpy(ttype[i], "psp_status");
  strcpy(tform[i], "int"); strcpy(ttype[i], "psp_status"); i++;
  n1 = i;
  /* 
     strcpy(tform[i] = "float"); strcpy(ttype[i], "node"); i++;
     strcpy(tform[i] = "float"); strcpy(ttype[i], "incl"); i++;
  */
  /* the following is replaced by proper type definition as INT */
  strcpy(tform[i],  "PSP_STATUS"); strcpy(ttype[i], "status");
  strcpy(tform[i],  "int"); strcpy(ttype[i], "status"); i++;
  
  strcpy(tform[i],  "float"); strcpy(ttype[i], "sky"); i++;
  strcpy(tform[i],  "float"); strcpy(ttype[i], "skysig"); i++;
  strcpy(tform[i],  "float"); strcpy(ttype[i], "skyerr"); i++;
  strcpy(tform[i],  "float"); strcpy(ttype[i], "skyslope"); i++;
  strcpy(tform[i],  "float"); strcpy(ttype[i], "lbias"); i++;
  strcpy(tform[i],  "float"); strcpy(ttype[i], "rbias"); i++;
  /* 
     strcpy(tform[i],  "float"); strcpy(ttype[i], "flux20"); i++;
     strcpy(tform[i],  "float"); strcpy(ttype[i], "flux20Err"); i++;
  */
  strcpy(tform[i],  "int"); strcpy(ttype[i], "psf_nstar"); i++;
  strcpy(tform[i],  "float"); strcpy(ttype[i], "psf_ap_correctionErr"); i++;
  /*
    strcpy(tform[i],  "int"); strcpy(ttype[i], "nann_ap_frame"); i++;
    strcpy(tform[i],  "int"); strcpy(ttype[i], "nann_ap_run"); i++;
    strcpy(tform[i],  "float"); strcpy(ttype[i], "ap_corr_run"); i++;
    strcpy(tform[i],  "float"); strcpy(ttype[i], "ap_corr_runErr"); i++;
  */
  strcpy(tform[i],  "float"); strcpy(ttype[i], "psf_sigma1"); i++;
  strcpy(tform[i],  "float"); strcpy(ttype[i], "psf_sigma2"); i++;
  /*
    strcpy(tform[i],  "float"); strcpy(ttype[i], "psf_sigmax1"); i++
    strcpy(tform[i],  "float"); strcpy(ttype[i], "psf_sigmax2"); i++
    strcpy(tform[i],  "float"); strcpy(ttype[i], "psf_sigmay1"); i++
    strcpy(tform[i],  "float"); strcpy(ttype[i], "psf_sigmay2"); i++;
  */
  strcpy(tform[i],  "float"); strcpy(ttype[i], "psf_b"); i++;
  strcpy(tform[i],  "float"); strcpy(ttype[i], "psf_p0"); i++;
  strcpy(tform[i],  "float"); strcpy(ttype[i], "psf_beta"); i++;
  strcpy(tform[i],  "float"); strcpy(ttype[i], "psf_sigmap"); i++;
  strcpy(tform[i],  "float"); strcpy(ttype[i], "psf_width"); i++;
  strcpy(tform[i],  "float"); strcpy(ttype[i], "psf_psfCounts"); i++;
  strcpy(tform[i],  "float"); strcpy(ttype[i], "psf_sigma1_2G"); i++;
  strcpy(tform[i],  "float"); strcpy(ttype[i], "psf_sigma2_2G"); i++;
  strcpy(tform[i],  "float"); strcpy(ttype[i], "psf_b_2G"); i++;
  strcpy(tform[i],  "float"); strcpy(ttype[i], "psfCounts"); i++;	     
  strcpy(tform[i],  "int"); strcpy(ttype[i], "prof_nprof"); i++;	     
  strcpy(tform[i],  "float"); strcpy(ttype[i], "prof_mean"); i++;	       
  strcpy(tform[i],  "float"); strcpy(ttype[i], "prof_med"); i++;	 
  strcpy(tform[i],  "float"); strcpy(ttype[i], "prof_sig"); i++;	       
  strcpy(tform[i],  "float"); strcpy(ttype[i], "gain"); i++;
  strcpy(tform[i],  "float"); strcpy(ttype[i], "dark_variance"); i++;
  if (i != n) {
    shError("%s: problem indexing schema elements", name);
  }

  for (i = 0; i < n1; i++) {
    /* 
       TTYPE is the upper cased ttype 
       this is how the fields are put in the
       binary tables 
    */
    TTYPE[strlen(ttype[i])] = '\0'; 
    for(j = 0; j < strlen(ttype[i]); j++) {
      TTYPE[j] = toupper(ttype[i][j]);
    }
    
    /*
      only one of the following should be used 
      shSchemaTransEntryAdd(objc_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
      "", NULL, NULL, NULL, NULL, 0.0, -1);
    */
    shSchemaTransEntryAdd(calibio_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			  "", tform[i], NULL, NULL, NULL, 0.0, -1);
  }
  
  for (i = n1; i < n; i++) {
    
    TTYPE[strlen(ttype[i])] = '\0'; 
    for(j = 0; j < strlen(ttype[i]); j++) {
      TTYPE[j] = toupper(ttype[i][j]);
    }
    shSchemaTransEntryAdd(calibio_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			  "", NULL, NULL, NULL, NULL, 0.0, -1);
    
  }
  
  thFree(TTYPE);
  for (i = 0; i < n; i++) {
    thFree(ttype[i]);
    thFree(tform[i]);
  }
  thFree(ttype);
  thFree(tform);

  phFitsBinDeclareSchemaTrans(calibio_trans, "CALIB_IO");
  
  return(calibio_trans);
  
}

SCHEMATRANS *thSchemaGetCrudeCalib() {
SCHEMATRANS *cc_trans = shSchemaTransNew();

  char **tform, **ttype, *TTYPE;
   
  int i, j;
  int n = 3;

  tform = (char **) thCalloc(n, sizeof(char *));
  ttype = (char **) thCalloc(n, sizeof(char *));

  for (i = 0; i < n; i++) {
    tform[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
    ttype[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }
  i = 0;

  tform[i] = "float"; ttype[i] = "flux20"; i++;
  tform[i] = "float"; ttype[i] = "flux20Err"; i++;
  tform[i] = "char"; ttype[i] = "filter"; i++;


  TTYPE = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  
  for (i = 0; i < n; i++) {
    
    TTYPE[strlen(ttype[i])] = '\0'; 
    for(j = 0; j < strlen(ttype[i]); j++) {
      TTYPE[j] = toupper(ttype[i][j]);
    }

    shSchemaTransEntryAdd(cc_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			  "", NULL, NULL, NULL, NULL, 0.0, -1);
    
  }
 
  phFitsBinDeclareSchemaTrans(cc_trans, "CRUDE_CALIB"); 
  return(cc_trans);
}

SCHEMATRANS *thSchemeGetPsfwing() {

  SCHEMATRANS *w_trans;
  w_trans = shSchemaTransNew();

  char **tform, **ttype, *TTYPE;
   
  int i, j;
  int n = 5;

  tform = (char **) thCalloc(n, sizeof(char *));
  ttype = (char **) thCalloc(n, sizeof(char *));

  for (i = 0; i < n; i++) {
    tform[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
    ttype[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }

  i = 0;

  tform[i] = "float"; ttype[i] = "a"; i++;
  tform[i] = "float"; ttype[i] = "rad"; i++;
  tform[i] = "int"; ttype[i] = "nrad"; i++;

  tform[i] = "REGION"; ttype[i] = "reg"; i++;
  tform[i] = "float"; ttype[i] = "totlum"; i++;

  TTYPE = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  
  for (i = 0; i < n; i++) {
    
    TTYPE[strlen(ttype[i])] = '\0'; 
    for(j = 0; j < strlen(ttype[i]); j++) {
      TTYPE[j] = toupper(ttype[i][j]);
    }

    shSchemaTransEntryAdd(w_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			  "", NULL, NULL, NULL, NULL, 0.0, -1);
    
  }
  
  phFitsBinDeclareSchemaTrans(w_trans, "PSFWING");
  
  return(w_trans);

}



SCHEMATRANS *thSchemaGetSkycoeffs() {

  SCHEMATRANS *sc_trans;
  sc_trans = shSchemaTransNew();

  char **tform, **ttype, *TTYPE;

  int i, j;
  int n = 4;

  tform = (char **) thCalloc(n, sizeof(char *));
  ttype = (char **) thCalloc(n, sizeof(char *));

  for (i = 0; i < n; i++) {
    tform[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
    ttype[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }

  i = 0;

  tform[i] = "int"; 
  ttype[i] = "bsize";
  i++;

  tform[i] = "float";
  ttype[i] = "c";
  i++;

  tform[i] = "float";
  ttype[i] = "cov";
  i++;
  
  tform[i] = "float";
  ttype[i] = "invcov";
  i++;

 

  TTYPE = (char *) thCalloc(MX_STRING_LEN, sizeof(char));

  for (i = 0; i < n; i++) {

    TTYPE[strlen(ttype[i])] = '\0'; 
    for(j = 0; j < strlen(ttype[i]); j++) {
      TTYPE[j] = toupper(ttype[i][j]);
    }

    shSchemaTransEntryAdd(sc_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			  "", NULL, NULL, NULL, NULL, 0.0, -1);
    
  }

  phFitsBinDeclareSchemaTrans(sc_trans, "SKYCOEFFS");
  
  return(sc_trans);

}

SCHEMATRANS *thSchemaGetSkymodel () {
  
  SCHEMATRANS *sm_trans;
  
  sm_trans = shSchemaTransNew();

  shSchemaTransEntryAdd(sm_trans,CONVERSION_BY_TYPE,"SC","sc",
			"",NULL,NULL,NULL,NULL,0.0,-1);
  
#if SKYMODEL_IO_REGION
  shSchemaTransEntryAdd(sm_trans,CONVERSION_BY_TYPE,"REG","reg",
			"",NULL,NULL,NULL,NULL,0.0,-1);
#endif

  shSchemaTransEntryAdd(sm_trans,CONVERSION_BY_TYPE,
			"LOGLIKELIHOOD","loglikelihood",
			"",NULL,NULL,NULL,NULL,0.0,-1);

  shSchemaTransEntryAdd(sm_trans,CONVERSION_BY_TYPE,"NPIXEL","npixel",
			"",NULL,NULL,NULL,NULL,0.0,-1);

  phFitsBinDeclareSchemaTrans(sm_trans, "SKYMODEL");
  
  return(sm_trans);

}

SCHEMATRANS *thSchemaGetSkymodelIo () {

  SCHEMATRANS *sm_trans;

  sm_trans = shSchemaTransNew();

  int i, j, n = 16;

  char **tform, **ttype, *TTYPE;

  tform = (char **) thCalloc(n, sizeof(char *));
  ttype = (char **) thCalloc(n, sizeof(char *));

  for (i = 0; i < n; i++) {
    tform[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
    ttype[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }

  i = 0;

  tform[i] = "int";
  ttype[i] = "run";
  i++;

  tform[i] = "int";
  ttype[i] = "rerun";
  i++;

  tform[i] = "int";
  ttype[i] = "field";
  i++;

  tform[i] = "int";
  ttype[i] = "camcol";
  i++;

  tform[i] = "int";
  ttype[i] = "camrow";
  i++;

  tform[i] = "int";
  ttype[i] = "filter";
  i++;

  tform[i] = "int";
  ttype[i] = "mjd";
  i++;


  tform[i] = "int"; 
  ttype[i] = "bsize";
  i++;

  tform[i] = "float";
  ttype[i] = "c";
  i++;

  tform[i] = "float";
  ttype[i] = "cov";
  i++;
  
  tform[i] = "float";
  ttype[i] = "invcov";
  i++;

#if SKYMODEL_IO_REGION
  tform[i] = "REGION";
  ttype[i] = "reg";
  i++;
#endif

  tform[i] = "float";
  ttype[i] = "loglikelihood";
  i++;

  tform[i] = "int";
  ttype[i] = "npixel";
  i++;
  

  tform[i] = "char";
  ttype[i] = "thmaskselfile";
  i++;
  
  tform[i] = "char";
  ttype[i] = "thobjcselfile";
  i++;

  n = i;
  
  TTYPE = (char *) thCalloc(MX_STRING_LEN, sizeof(char));

  for (i = 0; i < n; i++) {
    
    TTYPE[strlen(ttype[i])] = '\0'; 
    for(j = 0; j < strlen(ttype[i]); j++) {
      TTYPE[j] = toupper(ttype[i][j]);
    }
    
    if (strcmp(tform[i], "char")) {
      shSchemaTransEntryAdd(sm_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			    "", NULL, NULL, NULL, NULL, 0.0, -1);
    } else {
      shSchemaTransEntryAdd(sm_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			    "heap", tform[i], "-1", NULL, NULL, 0.0, -1);
    }
  }

   shSchemaTransEntryAdd(sm_trans,CONVERSION_BY_TYPE,"SC","sc",
			"",NULL,NULL,NULL,NULL,0.0,-1);
  
#if SKYMODEL_IO_REGION
  shSchemaTransEntryAdd(sm_trans,CONVERSION_BY_TYPE,"REG","reg",
			"",NULL,NULL,NULL,NULL,0.0,-1);
#endif

  shSchemaTransEntryAdd(sm_trans,CONVERSION_BY_TYPE,
			"LOGLIKELIHOOD","loglikelihood",
			"",NULL,NULL,NULL,NULL,0.0,-1);

  shSchemaTransEntryAdd(sm_trans,CONVERSION_BY_TYPE,"NPIXEL","npixel",
			"",NULL,NULL,NULL,NULL,0.0,-1);

  phFitsBinDeclareSchemaTrans(sm_trans, "SKYMODELIO");
  
  return(sm_trans);

}

SCHEMATRANS *thSchemaGetExtregion () {
  
  SCHEMATRANS *sm_trans;

  sm_trans = shSchemaTransNew();

  int i, j, n = 4, m = 16;

  char **tform, **ttype, *TTYPE;

  tform = (char **) thCalloc(n, sizeof(char *));
  ttype = (char **) thCalloc(n, sizeof(char *));

  for (i = 0; i < m; i++) {
    tform[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
    ttype[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }

  i = 0;

  strcpy(ttype[i], "reg");
  strcpy(tform[i], "REGION");
  i++;
  strcpy(ttype[i], "bzero");
  strcpy(tform[i], "double");
  i++;
  strcpy(ttype[i], "bscale");
  strcpy(tform[i], "double"); /* "long"; */
  i++;
  strcpy(ttype[i], "conversion");
  strcpy(tform[i], "int");
  i++;
  
  sm_trans = shSchemaTransNew();
  n = i;
  TTYPE = (char *) thCalloc(MX_STRING_LEN, sizeof(char));

for (i = 0; i < n; i++) {
    
    TTYPE[strlen(ttype[i])] = '\0'; 
    for(j = 0; j < strlen(ttype[i]); j++) {
      TTYPE[j] = toupper(ttype[i][j]);
    }
     if (strcmp(tform[i], "char")) {
      shSchemaTransEntryAdd(sm_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			    "", NULL, NULL, NULL, NULL, 0.0, -1);
    } else {
      shSchemaTransEntryAdd(sm_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			    "heap", tform[i], "-1", NULL, NULL, 0.0, -1);
    }
  }

  phFitsBinDeclareSchemaTrans(sm_trans, "EXTREGION");
 
  for (i = 0; i < m; i++) {
    thFree(tform[i]);
    thFree(ttype[i]);
  }
  thFree(tform); thFree(ttype); thFree(TTYPE);
  return(sm_trans);

}



SCHEMATRANS *thSchemaGetFmhdu () {
  
  SCHEMATRANS *sm_trans;

  int i, j, n = 7;

  char **tform, **ttype, *TTYPE;

  tform = (char **) thCalloc(n, sizeof(char *));
  ttype = (char **) thCalloc(n, sizeof(char *));

  for (i = 0; i < n; i++) {
    tform[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
    ttype[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }

  i = 0;

  ttype[i] = "funcid";
  tform[i] = "char";
  i++;
  ttype[i] = "funcname";
  tform[i] = "char";
  i++;
  ttype[i] = "stat";
  tform[i] = "int"; /* "long"; */
  i++;
  ttype[i] = "ncomp";
  tform[i] = "int";
  i++;
  ttype[i] = "compindex";
  tform[i] = "int";
  i++;
  ttype[i] = "firstrow";
  tform[i] = "int";
  i++;
  ttype[i] = "pthdu";
  tform[i] = "int";
  i++;

  sm_trans = shSchemaTransNew();
  n = i;
  TTYPE = (char *) thCalloc(MX_STRING_LEN, sizeof(char));

for (i = 0; i < n; i++) {
    
    TTYPE[strlen(ttype[i])] = '\0'; 
    for(j = 0; j < strlen(ttype[i]); j++) {
      TTYPE[j] = toupper(ttype[i][j]);
    }
     if (strcmp(tform[i], "char")) {
      shSchemaTransEntryAdd(sm_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			    "", NULL, NULL, NULL, NULL, 0.0, -1);
    } else {
      shSchemaTransEntryAdd(sm_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			    "heap", tform[i], "-1", NULL, NULL, 0.0, -1);
    }
  }

  phFitsBinDeclareSchemaTrans(sm_trans, "FMHDU");
 
/* 
  for (i = 0; i < m; i++) {
    thFree(tform[i]);
    thFree(ttype[i]);
  }
  thFree(tform); thFree(ttype); thFree(TTYPE);
*/
  return(sm_trans);

}

SCHEMATRANS *thSchemaGetParelemio () {
  
  SCHEMATRANS *sm_trans;

  int i, j, n = 16;
  char **tform, **ttype, *TTYPE;
  tform = (char **) thCalloc(n, sizeof(char *));
  ttype = (char **) thCalloc(n, sizeof(char *));
  for (i = 0; i < n; i++) {
    tform[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
    ttype[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }

  i = 0;

  tform[i] = "int";
  ttype[i] = "index";
  i++;
   tform[i] = "char";
  ttype[i] = "pname";
  i++;
   tform[i] = "float";
  ttype[i] = "pdiv1";
  i++;
 tform[i] = "int";
  ttype[i] = "pdiv2";
  i++;
 tform[i] = "THPARDIV_TYPE";
  ttype[i] = "divtype";
  i++;
 tform[i] = "THPAR_TYPE";
  ttype[i] = "ptype";
  i++;
 tform[i] = "int";
  ttype[i] = "n";
  i++;
 tform[i] = "int";
  ttype[i] = "npdiv";
  i++;

  sm_trans = shSchemaTransNew();
  n = i;
  TTYPE = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  char *size_str;
  size_str = (char *) thCalloc(MX_STRING_LEN, sizeof(char));


for (i = 0; i < n; i++) {
    
    TTYPE[strlen(ttype[i])] = '\0'; 
    for(j = 0; j < strlen(ttype[i]); j++) {
      TTYPE[j] = toupper(ttype[i][j]);
    }
     if (strcmp(tform[i], "char")) {

      shSchemaTransEntryAdd(sm_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			    "", NULL, NULL, NULL, NULL, 0.0, -1);

    } else if (!strcmp(ttype[i], "compindex")) {

	sprintf(size_str, "%d", N_FUNCMODEL_COMP_INDEX);
	shSchemaTransEntryAdd(sm_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			    "", NULL, NULL, NULL, NULL, 0.0, -1);
    } else {	

      shSchemaTransEntryAdd(sm_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			    "heap", tform[i], "-1", NULL, NULL, 0.0, -1);
    }
  }

  thFree(size_str); thFree(TTYPE);
  phFitsBinDeclareSchemaTrans(sm_trans, "PARELEMIO");
  
  return(sm_trans);

}

SCHEMATRANS *thSchemaGetLmethod () {
  
  SCHEMATRANS *lm_trans;

  int i, j, n = 17;
  char **tform, **ttype, *TTYPE;
  tform = (char **) thCalloc(n, sizeof(char *));
  ttype = (char **) thCalloc(n, sizeof(char *));
  for (i = 0; i < n; i++) {
    tform[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
    ttype[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }

  i = 0;

  tform[i] = "LMETHODTYPE";
  ttype[i] = "type";
  i++;
   tform[i] = "CONVERGENCE";
  ttype[i] = "cflag";
  i++;
   tform[i] = "double";
  ttype[i] = "lambda";
  i++;
 tform[i] = "double";
  ttype[i] = "nu";
  i++;
 tform[i] = "double";
  ttype[i] = "gamma";
  i++;
 tform[i] = "double";
  ttype[i] = "beta";
  i++;
 tform[i] = "double";
  ttype[i] = "rhobar";
  i++;
 tform[i] = "double";
  ttype[i] = "tau";
  i++;
 tform[i] = "int";
  ttype[i] = "q";
  i++;
 tform[i] = "double";
  ttype[i] = "e1";
  i++;
 tform[i] = "double";
  ttype[i] = "e2";
  i++;
 tform[i] = "int";
  ttype[i] = "k";
  i++;
 tform[i] = "int";
  ttype[i] = "kmax";
  i++;
 tform[i] = "double"; 
  ttype[i] = "chisq";
  i++;
 tform[i] = "double";
  ttype[i] = "cost";
  i++;
 tform[i] = "double";
  ttype[i] = "pcost";
  i++;
 tform[i] = "int";
  ttype[i] = "status";
  i++;

  lm_trans = shSchemaTransNew();
  n = i;
  TTYPE = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  char *size_str;
  size_str = (char *) thCalloc(MX_STRING_LEN, sizeof(char));


for (i = 0; i < n; i++) {
    
    TTYPE[strlen(ttype[i])] = '\0'; 
    for(j = 0; j < strlen(ttype[i]); j++) {
      TTYPE[j] = toupper(ttype[i][j]);
    }
     if (strcmp(tform[i], "char")) {

      shSchemaTransEntryAdd(lm_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			    "", NULL, NULL, NULL, NULL, 0.0, -1);

    } else {	

      shSchemaTransEntryAdd(lm_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			    "heap", tform[i], "-1", NULL, NULL, 0.0, -1);
    }
  }

  thFree(size_str); thFree(TTYPE);
  phFitsBinDeclareSchemaTrans(lm_trans, "LMETHOD");
  
  return(lm_trans);

}

SCHEMATRANS *thSchemaGetPsfReport () {
  
  SCHEMATRANS *report_trans;

  int i, j, n = 22;
  char **tform, **ttype, *TTYPE;
  tform = (char **) thCalloc(n, sizeof(char *));
  ttype = (char **) thCalloc(n, sizeof(char *));
  for (i = 0; i < n; i++) {
    tform[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
    ttype[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }

  i = 0;

  tform[i] = "float";
  ttype[i] = "xc";
  i++;
  tform[i] = "float";
  ttype[i] = "yc";
  i++;
  tform[i] = "int";
  ttype[i] = "inboundary";
  i++;
  tform[i] = "int";
  ttype[i] = "outboundary";
  i++;
  tform[i] = "float";
  ttype[i] = "background";
  i++;
  tform[i] = "float";
  ttype[i] = "amplitude";
  i++;
  tform[i] = "float";
  ttype[i] = "lostlight";
  i++;
  tform[i] = "float";
  ttype[i] = "halofrac";
  i++;
  tform[i] = "heap";
  ttype[i] = "sigma";
  i++;
  tform[i] = "heap";
  ttype[i] = "sigma1";
  i++;
  tform[i] = "heap";
  ttype[i] = "sigma2";
  i++;
  tform[i] = "heap";
  ttype[i] = "FWHM";
  i++;
  tform[i] = "heap";
  ttype[i] = "sum";
  i++;
#if DUMP_PSF_REPORT_RADIAL_PROFILE
  tform[i] = "heap";
  ttype[i] = "rad";
  i++;
  tform[i] = "heap";
  ttype[i] = "average";
  i++;
  tform[i] = "int";
  ttype[i] = "nrad";
  i++;
#endif
#if DUMP_PSF_REPORT_REGION 
  tform[i] = "float";
  ttype[i] = "bzero";
  i++;
  tform[i] = "float";
  ttype[i] = "bscale";
  i++;
  tform[i] = "REGION";
  ttype[i] = "image";
  i++;
#endif

  report_trans = shSchemaTransNew();
  n = i;
  TTYPE = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  char *size_str;
  size_str = (char *) thCalloc(MX_STRING_LEN, sizeof(char));


for (i = 0; i < n; i++) {
    
    TTYPE[strlen(ttype[i])] = '\0'; 
    for(j = 0; j < strlen(ttype[i]); j++) {
      TTYPE[j] = toupper(ttype[i][j]);
    }
     if (strcmp(tform[i], "char")) {

      shSchemaTransEntryAdd(report_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			    "", NULL, NULL, NULL, NULL, 0.0, -1);

    } else {	

      shSchemaTransEntryAdd(report_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			    "heap", tform[i], "-1", NULL, NULL, 0.0, -1);
    }
  }

  thFree(size_str); thFree(TTYPE);
#if PRINT_IO_SCHEMA_TRANS
   shSchemaTransEntryShow(report_trans, 0, n - 1);
#endif
  phFitsBinDeclareSchemaTrans(report_trans, "PSFREPORT");
  return(report_trans);

}

SCHEMATRANS *thSchemaGetBasicStat () {
  
  SCHEMATRANS *stat_trans;
  stat_trans = shSchemaTransNew();

  int i, j, n = 10;
  char **tform, **ttype, *TTYPE;
  tform = (char **) thCalloc(n, sizeof(char *));
  ttype = (char **) thCalloc(n, sizeof(char *));
  for (i = 0; i < n; i++) {
    tform[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
    ttype[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }

  i = 0;

  tform[i] = "int";
  ttype[i] = "n";
  i++;
  tform[i] = "float";
  ttype[i] = "mean";
  i++;
  tform[i] = "float";
  ttype[i] = "s";
  i++;
  tform[i] = "float";
  ttype[i] = "median";
  i++;
  tform[i] = "float";
  ttype[i] = "iqr";
  i++;
  tform[i] = "float";
  ttype[i] = "q1";
  i++;
  tform[i] = "float";
  ttype[i] = "q3";
  i++;
  tform[i] = "float";
  ttype[i] = "min";
  i++;
  tform[i] = "float";
  ttype[i] = "max";
  i++;
  tform[i] = "float";
  ttype[i] = "range";
  i++;

  n = i;
  TTYPE = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  char *size_str;
  size_str = (char *) thCalloc(MX_STRING_LEN, sizeof(char));


for (i = 0; i < n; i++) {
    
    TTYPE[strlen(ttype[i])] = '\0'; 
    for(j = 0; j < strlen(ttype[i]); j++) {
      TTYPE[j] = toupper(ttype[i][j]);
    }
     if (strcmp(tform[i], "char")) {

      shSchemaTransEntryAdd(stat_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			    "", NULL, NULL, NULL, NULL, 0.0, -1);

    } else {	

	shSchemaTransEntryAdd(stat_trans,CONVERSION_BY_TYPE,TTYPE,ttype[i],
        	              "",NULL,NULL,NULL,NULL,0.0,-1);
/* 
      shSchemaTransEntryAdd(stat_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			    "heap", tform[i], "-1", NULL, NULL, 0.0, -1);
*/
    }
  }

  thFree(size_str); thFree(TTYPE);
#if PRINT_IO_SCHEMA_TRANS 
  shSchemaTransEntryShow(stat_trans, 0, n - 1);
#endif
  phFitsBinDeclareSchemaTrans(stat_trans, "BASIC_STAT");
  
  return(stat_trans);

}

SCHEMATRANS *thSchemaGetBriefPsfReport () {
  
  SCHEMATRANS *report_trans;

  int i, j, n = 51;
  char **tform, **ttype, *TTYPE;
  tform = (char **) thCalloc(n, sizeof(char *));
  ttype = (char **) thCalloc(n, sizeof(char *));
  for (i = 0; i < n; i++) {
    tform[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
    ttype[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }

  i = 0;

  tform[i] = "int";
  ttype[i] = "inboundary";
  i++;
  tform[i] = "int";
  ttype[i] = "outboundary";
  i++; 
  tform[i] = "int";
  ttype[i] = "n";
  i++;
  tform[i] = "float";
  ttype[i] = "inboundary_c1";
  i++;
  tform[i] = "float";
  ttype[i] = "inboundary_w1";
  i++;
  tform[i] = "float";
  ttype[i] = "inboundary_c2";
  i++;
  tform[i] = "float";
  ttype[i] = "inboundary_w2";
  i++;
  tform[i] = "float";
  ttype[i] = "outboundary_c1";
  i++;
  tform[i] = "float";
  ttype[i] = "outboundary_w1";
  i++;
  tform[i] = "float";
  ttype[i] = "outboundary_c2";
  i++;
  tform[i] = "float";
  ttype[i] = "outboundary_w2";
  i++;
  tform[i] = "float";
  ttype[i] = "background_c1";
  i++;
  tform[i] = "float";
  ttype[i] = "background_c2";
  i++;
   tform[i] = "float";
  ttype[i] = "background_w1";
  i++;
  tform[i] = "float";
  ttype[i] = "background_w2";
  i++;
  tform[i] = "float";
  ttype[i] = "amplitude_c1";
  i++;
   tform[i] = "float";
  ttype[i] = "amplitude_c2";
  i++;
   tform[i] = "float";
  ttype[i] = "amplitude_w1";
  i++;
   tform[i] = "float";
  ttype[i] = "amplitude_w2";
  i++;
  tform[i] = "float";
  ttype[i] = "lostlight_c1";
  i++;
   tform[i] = "float";
  ttype[i] = "lostlight_c2";
  i++;
   tform[i] = "float";
  ttype[i] = "lostlight_w1";
  i++;
  tform[i] = "float";
  ttype[i] = "lostlight_w2";
  i++;
  tform[i] = "float";
  ttype[i] = "halofrac_c1";
  i++;
   tform[i] = "float";
  ttype[i] = "halofrac_c2";
  i++;
   tform[i] = "float";
  ttype[i] = "halofrac_w1";
  i++;
  tform[i] = "float";
  ttype[i] = "halofrac_w2";
  i++;
  tform[i] = "float";
  ttype[i] = "sigma_c1";
  i++;
   tform[i] = "float";
  ttype[i] = "sigma_c2";
  i++; 
  tform[i] = "float";
  ttype[i] = "sigma_w1";
  i++;
   tform[i] = "float";
  ttype[i] = "sigma_w2";
  i++;
 
  tform[i] = "float";
  ttype[i] = "sigma1_c1";
  i++;
   tform[i] = "float";
  ttype[i] = "sigma1_c2";
  i++; 
  tform[i] = "float";
  ttype[i] = "sigma1_w1";
  i++;
   tform[i] = "float";
  ttype[i] = "sigma1_w2";
  i++;
 

  tform[i] = "float";
  ttype[i] = "sigma2_c1";
  i++;
   tform[i] = "float";
  ttype[i] = "sigma2_c2";
  i++; 
  tform[i] = "float";
  ttype[i] = "sigma2_w1";
  i++;
   tform[i] = "float";
  ttype[i] = "sigma2_w2";
  i++;
 
  tform[i] = "float";
  ttype[i] = "FWHM_c1";
  i++;
   tform[i] = "float";
  ttype[i] = "FWHM_c2";
  i++; 
  tform[i] = "float";
  ttype[i] = "FWHM_w1";
  i++;
   tform[i] = "float";
  ttype[i] = "FWHM_w2";
  i++;
 
  tform[i] = "float";
  ttype[i] = "sum_c1";
  i++;
   tform[i] = "float";
  ttype[i] = "sum_c2";
  i++; 
  tform[i] = "float";
  ttype[i] = "sum_w1";
  i++;
   tform[i] = "float";
  ttype[i] = "sum_w2";
  i++;
 

  tform[i] = "char";
  ttype[i] = "name_c1";
  i++;
   tform[i] = "char";
  ttype[i] = "name_c2";
  i++; 
  tform[i] = "char";
  ttype[i] = "name_w1";
  i++;
   tform[i] = "char";
  ttype[i] = "name_w2";
  i++;
 

  report_trans = shSchemaTransNew();
  n = i;
  TTYPE = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  char *size_str;
  size_str = (char *) thCalloc(MX_STRING_LEN, sizeof(char));


for (i = 0; i < n; i++) {
    
    TTYPE[strlen(ttype[i])] = '\0'; 
    for(j = 0; j < strlen(ttype[i]); j++) {
      TTYPE[j] = toupper(ttype[i][j]);
    }
     if (strcmp(tform[i], "char")) {

      shSchemaTransEntryAdd(report_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			    "", NULL, NULL, NULL, NULL, 0.0, -1);

    } else {	


/* 
	shSchemaTransEntryAdd(report_trans,CONVERSION_BY_TYPE,TTYPE,ttype[i], "",NULL,NULL,NULL,NULL,0.0,-1);
*/
      shSchemaTransEntryAdd(report_trans, CONVERSION_BY_TYPE, TTYPE, ttype[i], 
			    "heap", tform[i], "-1", NULL, NULL, 0.0, -1);
    }
  }

  thFree(size_str); thFree(TTYPE);
#if PRINT_IO_SCHEMA_TRANS
  shSchemaTransEntryShow(report_trans, 0, n - 1);
#endif
 phFitsBinDeclareSchemaTrans(report_trans, "BRIEFPSFREPORT");
  
  return(report_trans);

}

THBINREGION *thFitsBinregionRead(PHFITSFILE *fits, RET_CODE *status) {
  
  char *name = "thFitsBinregionRead";
  
  if (fits == NULL) {
    thError("%s: null (fits) file", name);
    return(NULL);
  }

  int nfitsrow = 0, quiet = 0, phstatus;
  /* reading the extension header and evaluating the number of rows */
  phstatus = phFitsBinTblHdrRead(fits, "THBINREGION", NULL, &nfitsrow, quiet);

  if (nfitsrow > 1) {
    thError("%s: too many (%d) rows for THBINREGION", 
		   name, nfitsrow);
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
  }

  if (phstatus < 0 || nfitsrow == 0) {
    thError("%s: could no read the hdr for the fits file", name);
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
  }

  THBINREGION *simage = NULL;
  phstatus = phFitsBinTblRowRead(fits, simage);
  
  if (phstatus < 0) {
    thError("%s: could not read the contents of the binary table", 
		   name);
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
  }

  if (status != NULL) *status = SH_SUCCESS;
  return(simage);

}


CHAIN *thFitsFuncRead(PHFITSFILE *fits, RET_CODE *status) {

  char *name = "thFitsFuncRead";
  *status = SH_SUCCESS;

  int nfitsrow, phstatus, quiet = 1; 
  /* reading the extension header and evaluating the number of rows */
  nfitsrow = 0;
  phstatus = phFitsBinTblHdrRead(fits,"FUNC", NULL, &nfitsrow, quiet);

  /* if the nrow is successfully read */
  if (nfitsrow > 0 && phstatus >= 0) {
    
    CHAIN *funcChain;
    funcChain = shChainNew("FUNC");

    FUNC *func;
    int j;
    for (j = 0; j < nfitsrow; j++) {
      func = thFuncNew();
      func->reg = shRegNew("", 0, 0, TYPE_THPIX);
      phstatus = phFitsBinTblRowRead(fits,func);
      if ( phstatus < 0){
    
	thError("%s: problem reading fits file at row %d", 
		       name, j);
	if (status != NULL) *status = SH_GENERIC_ERROR;    
	return(NULL);

        } else {

	/* add it to chain */
	shChainElementAddByPos(funcChain, func,"FUNC", TAIL, AFTER);
        }

    }
    
    return (funcChain);

  } else {
    
    thError("%s: problem reading fits file - zero number of rows or unexisting extension", 
		   name);
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);

  }
  
}


SPANMASK *thFitsSpanmaskRead(PHFITSFILE *fits, 
			     int nrow, int ncol, 
			     RET_CODE *status) {

  char *name = "thFitsSpanmaskRead";

  if (status != NULL) *status = SH_SUCCESS;

  SPANMASK *sm;
  sm = phSpanmaskNew(nrow, ncol);

  int phstatus = 0, quiet = 1;
  
  /* no need to read the extension = 0, since Binary handlers in PHOTO
     naturally place the pointer at the first binary table available 
  */

  int nfitsrow, imask, extno;
  int nspans, j; 
  OBJMASK *om;

  for (imask = 0;  imask < NMASK_TYPES; imask++) {
    
    /* reading the extension header and evaluating the number of rows */
    nfitsrow = 0;
    phstatus = phFitsBinTblHdrRead(fits,"OBJMASK", NULL, &nfitsrow, quiet);

    /* if the nrow is successfully read */
    if ((nfitsrow > 0) & (phstatus >= 0)) {
      
      /* determine the real nspan */
      nspans = 0; // 10000;
      om = phObjmaskNew(nspans);
      phstatus = phFitsBinTblRowRead(fits, om);
      /* nspans  from om */
      nspans = om->nspan;
      
      phstatus = phFitsBinTblRowUnread(fits); 
      phObjmaskDel(om);
      
      for (j = 0; j < nfitsrow; j++) {
	om = phObjmaskNew(nspans); 
	/* read  om */
	
	phstatus = phFitsBinTblRowRead(fits, om);
	if ( phstatus < 0){
	  extno = imask + 1;
	  thError("%s: ERROR - problem reading fits file [%d] at row %d", name, extno, j);
	  if (status != NULL) *status = SH_GENERIC_ERROR;    
	  return(NULL);
	} else {
	  /* add it to spanmask */
	  if (om != NULL && sm != NULL && imask < S_NMASK_TYPES) {
	  	phObjmaskAddToSpanmask(om, sm, imask);
	  } else if (imask >= (int) S_NMASK_TYPES) {
		thError("%s: ERROR - found (imask = %d) while (S_NMASK_TYPES = %d)", name, imask, (int) S_NMASK_TYPES);
		if (status != NULL) *status = SH_GENERIC_ERROR;
		return(NULL);
	  } else if (sm == NULL) {
		thError("%s: WARNING - found (smask = NULL) at (row = %d)", name, j);		
	  } else if (om == NULL) {
		thError("%s: ERROR - null object mask", name);
		if (status != NULL) *status = SH_GENERIC_ERROR;
		return(NULL);
  	  }
	  
	}
      }
    }
    /* closing the current extension and going to the next one */
    phstatus = phFitsBinTblEnd(fits);
    if ( phstatus < 0){
      extno = imask + 1;
      thError("%s: unabel to close extension %d", name, extno);
      if (status != NULL) *status = SH_GENERIC_ERROR;
      return(NULL);
    }
    
  }

  if (sm != NULL) {
    for (imask = 0; imask < NMASK_TYPES; imask++) {
      if (sm->masks[imask] != NULL) {
	shChainTypeSet(sm->masks[imask], "OBJMASK");
      }
    }
  }

  return(sm);

}
  
CHAIN *thFitsAtlasRead(PHFITSFILE *fits, RET_CODE *status) {
char *name = "thFitsAtlasRead";

if (fits == NULL){
	thError("%s: ERROR - null fits file pointer", name);
	if (status != NULL) *status =  SH_GENERIC_ERROR;
	return(NULL);
}

int nfitsrow = 0, quiet = 0, phstatus;
/* reading the extension header and evaluating the number of rows */
phstatus = phFitsBinTblHdrRead(fits, "ATLAS_IMAGE", NULL, &nfitsrow, quiet);
if (phstatus < 0) {
	thError("%s: ERROR - could not open the binary table as 'ATLAS_IMAGE'", name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
}

ATLAS_IMAGE *atlas_image;
CHAIN *atlas_chain = NULL;
int i;

if (nfitsrow > 0 && phstatus >= 0) {
	atlas_chain = shChainNew ("ATLAS_IMAGE");
	for (i = 0; i < nfitsrow; i++){
		/* read the whole obj list */
		atlas_image = phAtlasImageNew((int) NCOLOR);
		phstatus = phFitsBinTblRowRead(fits, atlas_image);
		if (phstatus < 0) {
			thError("%s: ERROR - problem reading fits file at row %d", name, i);
			if (status != NULL) *status = SH_GENERIC_ERROR;
			return(NULL);
		} else {
			shChainElementAddByPos(atlas_chain, atlas_image, "ATLAS_IMAGE", TAIL, AFTER);
      		}
    	}
} else {
	thError("%s: ERROR - too few rows (%d), or problematic hdr read", name, nfitsrow); 
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
}

if (status != NULL) *status = SH_SUCCESS;
return(atlas_chain);

}
 

   
CHAIN *thFitsObjcRead(PHFITSFILE *fits, RET_CODE *status) {
  
  char *name = "thFitsObjcRead";

  thError("%s: ERROR - code never tested", name);
  if (status != NULL) *status = SH_GENERIC_ERROR;
   return(NULL);

  if (fits == NULL){
    thError("%s: ERROR - fits file pointer is empty", name);
    if (status != NULL) *status =  SH_GENERIC_ERROR;
    return(NULL);
  }

  int nfitsrow = 0, quiet = 0, phstatus;
  /* reading the extension header and evaluating the number of rows */
  phstatus = phFitsBinTblHdrRead(fits, "OBJC_IO", NULL, &nfitsrow, quiet);

  OBJC *objc;

  CHAIN *ochain;
  int i, j, ncolor;
  if (nfitsrow > 0 && phstatus >= 0) {

    ochain = shChainNew ("OBJC");

    /* reading ncolor */ 
    objc = phObjcNew(NCOLOR);
    phFitsBinTblRowRead(fits, objc);
    ncolor = objc->ncolor;
    shErrStackClear();
    phFitsBinTblRowUnread(fits);
    /* the following was changes on oct 1st, 2011 deep = 0 */
    phObjcDel(objc, 0);
  
    for (i = 0; i < nfitsrow; i++){
      /* read the whole obj list */
      objc = phObjcNew(ncolor);
      /* 
	 the following is necessary for OBJC but not for OBJC_IO
	 because the fields are alread expanded in OBJC_IO;
      */
      for (j = 0; j < ncolor; j++){
	objc->color[j] = phObject1New();
	objc->color[j]->region = shRegNew("", 0, 0, TYPE_THPIX);
      }
     

      phstatus = phFitsBinTblRowRead(fits, objc);
      
      if (phstatus < 0) {
	thError("%s: ERROR - problem reading fits file at row %d", name, i);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
      } else {
	shChainElementAddByPos(ochain, objc, "OBJC", TAIL, AFTER);
      }
    }
  } else {

    thError("%s: ERROR - too few rows (%d), or problematic hdr read", name, nfitsrow); 
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);

  }
  
  if (status != NULL) *status = SH_SUCCESS;
  return(ochain);

}
 
CHAIN *thFitsWObjcIoRead(PHFITSFILE *fits, RET_CODE *status) {
  char *name = "thFitsWObjcIoRead";

  if (fits == NULL){
    thError("%s: ERROR - fits file pointer is empty", name);
    if (status != NULL) *status =  SH_GENERIC_ERROR;
    return(NULL);
  }
  int nfitsrow = 0, quiet = 0, phstatus;

#if USE_FNAL_OBJC_IO
char *wobjc_name = "FNAL_OBJC_IO";
#else
char *wobjc_name = "OBJC_IO";  
#endif

/* reading the extension header and evaluating the number of rows */
  phstatus = phFitsBinTblHdrRead(fits, wobjc_name, NULL, &nfitsrow, quiet);
  WOBJC_IO *objc;
  if (phstatus < 0) {
	thError("%s: ERROR - problematic header read (phstatus = %d)", name, phstatus);
  	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
	}
  if (nfitsrow == 0) {
	thError("%s: ERROR - too few rows (%d)", name, nfitsrow);
  	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
	}
  CHAIN *ochain;
  int i, ncolor;

    /* reading ncolor */
    objc = thWObjcIoNew(NCOLOR);
    if (phFitsBinTblRowRead(fits, objc) != 0) {
	thError("%s: ERROR - could not read the first row from the fits file", name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
	}
    ncolor = objc->ncolor;
    shErrStackClear();
    if (phFitsBinTblRowUnread(fits) != 0) {
	thError("%s: ERROR - could not unread the first row from the fits file", name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	}
    /* the following was changes on oct 1st, 2011 deep = 0 */
	thWObjcIoDel(objc, 0);
    #if CHAIN_BLOCK_GENERATION 
    ochain = thChainObjcIoNewBlock(ncolor, nfitsrow);
    #else 
	ochain = shChainNew(wobjc_name);
    #endif

    if (ochain == NULL) {
	thError("%s: ERROR - could not generate CHAIN of type '%s'", name, wobjc_name);
	return(NULL);
    }

    for (i = 0; i < nfitsrow; i++){
      /* read the whole obj list */
	WOBJC_IO *objc1;
      #if CHAIN_BLOCK_GENERATION
      objc1 = shChainElementGetByPos(ochain, i);
      #else
	objc1 = thWObjcIoNew(ncolor);
      #endif
	phstatus = phFitsBinTblRowRead(fits, objc1);
	#if DEBUG_IO_OBJC_IO
	output_phobjc_checklist(name, objc1);
      	#endif

      if (phstatus < 0) {
	thError("%s: problem reading fits file at row %d", name, i);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
      } else {
	#if CHAIN_BLOCK_GENERATION
	#else
	shChainElementAddByPos(ochain, objc1, wobjc_name, TAIL, AFTER);
	#endif
      }
    }

  if (status != NULL) *status = SH_SUCCESS;
  return(ochain);

}

#if CALIB_PHOBJC_IO_USE_ALTERED_FITS_PACKAGE
CHAIN *thFitsCalibPhObjcIoRead(THFITSFILE *fits, RET_CODE *status) {
#else
CHAIN *thFitsCalibPhObjcIoRead(PHFITSFILE *fits, RET_CODE *status) {
#endif
  char *name = "thFitsCalibPhObjcIoRead";

  if (fits == NULL){
    thError("%s: ERROR - fits file pointer is empty", name);
    if (status != NULL) *status =  SH_GENERIC_ERROR;
    return(NULL);
  }
  int nfitsrow = 0, quiet = 0, phstatus;

char *objc_name = "CALIB_PHOBJC_IO";  

/* reading the extension header and evaluating the number of rows */
#if CALIB_PHOBJC_IO_USE_ALTERED_FITS_PACKAGE
  phstatus = thFitsBinTblHdrRead(fits, objc_name, NULL, &nfitsrow, quiet);
#else  
  phstatus = phFitsBinTblHdrRead(fits, objc_name, NULL, &nfitsrow, quiet);
#endif
    if (phstatus < 0) {
	thError("%s: ERROR - problematic header read (phstatus = %d)", name, phstatus);
  	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
	}
    if (nfitsrow == 0) {
	thError("%s: ERROR - too few rows (%d)", name, nfitsrow);
  	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
	}
  CHAIN *ochain;
  ochain = shChainNew(objc_name);
  if (ochain == NULL) {
	thError("%s: ERROR - could not generate CHAIN of type '%s'", name, objc_name);
	return(NULL);
    }

    int i;
    for (i = 0; i < nfitsrow; i++){
      /* read the whole obj list */
	CALIB_PHOBJC_IO *objc1;
	objc1 = thCalibPhObjcIoNew(NCOLOR);
#if CALIB_PHOBJC_IO_USE_ALTERED_FITS_PACKAGE
	phstatus = thFitsBinTblRowRead(fits, objc1);	
#else
	phstatus = phFitsBinTblRowRead(fits, objc1);	
#endif
	#if DEBUG_IO_CALIB_PHOBJC_IO
	output_calib_phobjc_checklist(name, objc1);
     	#endif 
      if (phstatus < 0) {
	thError("%s: ERROR - problem reading fits file at row %d", name, i);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
      } else {
	shChainElementAddByPos(ochain, objc1, objc_name, TAIL, AFTER);
      }
    }

  if (status != NULL) *status = SH_SUCCESS;
  return(ochain);

}

PHCALIB *thFitsPhCalibRead(PHFITSFILE *fits, int field, RET_CODE *status) {
  
  char *name = "thFitsPhCalibRead";

  if (fits == NULL){
    thError("%s: ERROR - fits file pointer is empty", name);
    if (status != NULL) *status =  SH_GENERIC_ERROR;
    return(NULL);
  }

  int nfitsrow = 0, quiet = 1, phstatus;
  /* reading the extension header and evaluating the number of rows */
  TYPE t;
  t = shTypeGetFromName("PHCALIB");
  if (t == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - relevant type for 'PHCALIB' not defined", name);
  	return(NULL);
  } else {
	printf("%s: CHECK - 'PHCALIB' has an associated schema \n", name);
  }
  phstatus = phFitsBinTblHdrRead(fits, "PHCALIB", NULL, &nfitsrow, quiet);

  PHCALIB *phcalib;

  int i;
  if (nfitsrow > 0 && phstatus >= 0) {

    int field0, field1, nrow;
    phcalib = phCalibNew();
    phFitsBinTblRowRead(fits, phcalib);
    shErrStackClear();
    phFitsBinTblRowUnread(fits);
    field0 = phcalib->field;
    field1 = field0 + nfitsrow - 1;
    if (field < field0 || field > field1) {
	thError("%s: WARNING - (field = %d) requested is out of range [%d, %d]", 
		name, field, field0, field1);
	if (status != NULL) *status = SH_SUCCESS;
	phCalibDel(phcalib);
	return(NULL);
	}
    if (field == field0) {
	if (status != NULL) *status = SH_SUCCESS;
	return(phcalib);
	}
    phCalibDel(phcalib); 
    nrow = field - field0; 
    for (i = 0; i < nrow; i++){
	phstatus = phFitsBinTblRowRead(fits, NULL); 
      if (phstatus < 0) {
	thError("%s: ERROR - problem reading fits file at row %d", name, i);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
      }
    }
    phcalib = phCalibNew();
    phstatus = phFitsBinTblRowRead(fits, phcalib); 
    if (phstatus < 0) {
	thError("%s: ERROR - problem reading fits file at row %d", name, i);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	phCalibDel(phcalib);
	return(NULL);
      }
  if (phcalib->field != field) {
	thError("%s: ERROR - looking for (field = %d) at (row = %d) but found (%d) instead", 
	name, field, i, phcalib->field);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	phCalibDel(phcalib);
	return(NULL);
	}
	
if (status != NULL) *status = SH_SUCCESS;
return(phcalib);

} else {

    thError("%s: ERROR - too few rows (%d), or problematic hdr read", name, nfitsrow); 
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);

  }

thError("%s: ERROR - source code problem", name);
if (status != NULL) *status = SH_GENERIC_ERROR;
return(NULL);
  
}


CHAIN *thFitsSkymodelRead(PHFITSFILE *fits, RET_CODE *status) {

  char *name = "thFitsSkymodelRead";
  
  if (fits == NULL){
    thError("%s: fits file pointer is empty", name);
    if (status != NULL) *status =  SH_GENERIC_ERROR;
    return(NULL);
  }
  
  int nfitsrow = 0, quiet = 0, phstatus;
  /* reading the extension header and evaluating the number of rows */
  phstatus = phFitsBinTblHdrRead(fits, "SKYMODEL", NULL, &nfitsrow, quiet);

  SKYMODEL *sm;

  CHAIN *smchain;
  int i, bsize;
  if (nfitsrow > 0 && phstatus >= 0) {

    smchain = shChainNew ("SKYMODEL");

    /* reading bsize */ 
    sm = thSkymodelNew(SKYBSIZE, 0, 0);
    phFitsBinTblRowRead(fits, sm);
    bsize = sm->sc->bsize;
    shErrStackClear();
    phFitsBinTblRowUnread(fits);
    thSkymodelDel(sm);
  
    for (i = 0; i < nfitsrow; i++){
      /* read the whole obj list */
      sm = thSkymodelNew(bsize, 0, 0);
      
      phstatus = phFitsBinTblRowRead(fits, sm);
      
      if (phstatus < 0) {
	thError("%s: problem reading fits file at row %d", name, i);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
      } else {
	shChainElementAddByPos(smchain, sm, "SKYMODEL", TAIL, AFTER);
      }
    }

  } else {
    
    thError("%s: too few rows (%d), or problematic hdr read", name, nfitsrow); 
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
    
  }
  
  if (status != NULL) *status = SH_SUCCESS;
  return(smchain);
  
}

CHAIN *thFitsSkymodelIoRead(PHFITSFILE *fits, RET_CODE *status) {

  char *name = "thFitsSkymodelIoRead";
  
  if (fits == NULL){
    thError("%s: fits file pointer is (null)", name);
    if (status != NULL) *status =  SH_GENERIC_ERROR;
    return(NULL);
  }
  
  int nfitsrow = 0, quiet = 0, phstatus;
  /* reading the extension header and evaluating the number of rows */
  phstatus = phFitsBinTblHdrRead(fits, "SKYMODEL", NULL, &nfitsrow, quiet);

  SKYMODELIO *sm;

  CHAIN *smchain;
  int i;
  if (nfitsrow > 0 && phstatus >= 0) {

    smchain = shChainNew ("SKYMODELIO");

    for (i = 0; i < nfitsrow; i++){
      /* read the whole obj list */
      sm = thSkymodelioNew(-1, -1);

      phstatus = phFitsBinTblRowRead(fits, sm);
      
      if (phstatus < 0) {
	thError("%s: problem reading fits file at row %d", name, i);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
      } else {
	shChainElementAddByPos(smchain, sm, "SKYMODELIO", TAIL, AFTER);
      }
    }

  } else {
    
    thError("%s: too few rows (%d), or problematic hdr read", name, nfitsrow); 
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
    
  }
  
  if (status != NULL) *status = SH_SUCCESS;
  return(smchain);
  
}


PSF_BASIS *thFitsPhPsfBasisRead(PHFITSFILE *fits, RET_CODE *status) {
  
  char *name = "thFitsPhPsfBasisRead";
  
  if (fits == NULL) {
    thError("%s: null (fits) file", name);
    return(NULL);
  }
  
  int ncomp = 0, quiet = 0, phstatus;
  /* reading the extension header and evaluating the number of rows */
  phstatus = phFitsBinTblHdrRead(fits, "PSF_KL_COMP", NULL, &ncomp, quiet);

  if (phstatus < 0) {
    thError("%s: could not properly read (PSF_KL_COMP) header from the fits binary table", name);
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
  }

  PSF_BASIS *basis = NULL;
  PSF_KERNEL *kern = NULL;
  PSF_KL_COMP *klc = NULL;
  REGION *reg;

  int i;
  if (ncomp > 0 && phstatus >= 0) {

    for (i = 0; i < ncomp; i++) {
      
      reg = shRegNew("klreg", 0, 0, TYPE_FL32);
      klc = phPsfKLCompNew(reg);
      phstatus = phFitsBinTblRowRead(fits, klc);

      if (i == 0) {
	kern = phPsfKernelNew(ncomp - 1, 1, 1, 
			      klc->nrow_b, klc->ncol_b);
	basis = phPsfBasisNew(kern, KL_BASIS, 
			      -1, -1, TYPE_FL32);
	phPsfKernelDel(kern);
      }
      
      phPsfBasisSetFromKLComp(basis, i, klc, 0);
      
      klc->reg = NULL;
      phPsfKLCompDel(klc);
    }

    if (status != NULL) *status = SH_SUCCESS;
    return(basis);

  } else {
    thError("%s: could not read the first extension", name);
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
  }

}
    
    
PHPSF_WING *thFitsPhPsfWingRead(PHFITSFILE *fits, RET_CODE *status) {
  
  char *name = "thFitsPhPsfWingRead";
  
  if (fits == NULL) {
    thError("%s: null (fits) file", name);
    return(NULL);
  }

  int nfitsrow = 0, quiet = 0, phstatus;
  /* reading the extension header and evaluating the number of rows */
  phstatus = phFitsBinTblHdrRead(fits, "CALIB_IO", NULL, &nfitsrow, quiet);

  if (nfitsrow > 1) {
    thError("%s: too many (%d) rows for PSF wings", 
		   name, nfitsrow);
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
  }

  if (phstatus < 0 || nfitsrow == 0) {
    thError("%s: could no read the hdr for the fits file", name);
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
  }

  PHPSF_WING *wing;
  wing = phPsfWingNew(NCOLOR);
  phstatus = phFitsBinTblRowRead(fits, wing);
  
  if (phstatus < 0) {
    thError("%s: could not read the contents of the binary table", 
		   name);
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
  }

  if (status != NULL) *status = SH_SUCCESS;
  return(wing);

}

PSFWING *thFitsPsfWingRead(PHFITSFILE *fits, RET_CODE *status) {
  
  char *name = "thFitsThPsfWingRead";
  
  if (fits == NULL) {
    thError("%s: null (fits) file", name);
    return(NULL);
  }

  int nfitsrow = 0, quiet = 0, phstatus;
  /* reading the extension header and evaluating the number of rows */
  phstatus = phFitsBinTblHdrRead(fits, "PSFWING", NULL, &nfitsrow, quiet);

  if (nfitsrow > 1) {
    thError("%s: too many (%d) rows for PSF wings", 
		   name, nfitsrow);
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
  }

  if (phstatus < 0 || nfitsrow == 0) {
    thError("%s: could no read the hdr for the fits file", name);
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
  }

  PSFWING *wing = NULL;
  phstatus = phFitsBinTblRowRead(fits, wing);
  
  if (phstatus < 0) {
    thError("%s: could not read the contents of the binary table", 
		   name);
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
  }

  if (status != NULL) *status = SH_SUCCESS;
  return(wing);

}


/* ====================== Dumpers ============================== */

/* file dumpers */
RET_CODE thFitsDumpChain(PHFITSFILE *fits, CHAIN *chain, char *ioname) {
char *name = "thFitsDumpChain";
thError("%s: ERROR - unsupported function", name);
return(SH_GENERIC_ERROR);
}

RET_CODE thAsciiDumpStru(FILE *file, void *stru, char *type, char **rnames, char **comments, int nrecord, char *hdr) {
char *name = "thAsciiDumpStru";
thError("%s: ERROR - unsupported function", name);
return(SH_GENERIC_ERROR);
}

/* generate the output header */
RET_CODE thFrameIoHdr(FRAME *f) {
char *name = "thFrameIoHdr";
thError("%s: ERROR - unsupported function", name);
return(SH_GENERIC_ERROR);
}

RET_CODE thFrameIoStamp(FRAME *f) {
char *name = "thFrameIoStamp";
thError("%s: ERROR - unsupported function", name);
return(SH_GENERIC_ERROR);
}

/* frame dumpers */

RET_CODE thFrameDump(FRAME *f) {
char *name = "thFrameDump";
thError("%s: ERROR - unsupported function", name);
return(SH_GENERIC_ERROR);
}

