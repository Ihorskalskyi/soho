#include "dervish.h"
#include "sohoEnv.h"

REGION *shRegReadFpcAll(const char *file,   
		     RET_CODE *shStatus);
REGION *shRegReadFpc(const char *file,  
		     const int row0,  const int column0,  
		     const int row1,  const int column1, 
		     RET_CODE *shStatus);

RET_CODE shWriteFpc(const char *file, 
		    const REGION *reg);

RET_CODE shCpFpc(const char *infile, const char *outfile, /* input and output filenames */
		       const int row0, const int column0, 
		       const int row1, const int column1 /* corners of the image in INFILE */);
