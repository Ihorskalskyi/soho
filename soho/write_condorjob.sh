#!/bin/bash

function write_condorjob {

local name="write_condorjob"

local lsubmitfile=no
local lexecutable=no
local larguments=no
local loutputfile=no
local lerrorfile=no
local lcomment=no
local restrict=no

local submitfile=empty
local executable=empty
local arguments=""
local outputfile=""
local errorfile=""
local comment=""

while getopts ':-:' OPTION ; do
  case "$OPTION" in
    -  ) [ $OPTIND -ge 1 ] && optind=$(expr $OPTIND - 1 ) || optind=$OPTIND
         eval OPTION="\$$optind"
         OPTARG=$(echo $OPTION | cut -d'=' -f2)
         OPTION=$(echo $OPTION | cut -d'=' -f1)
         case $OPTION in
             --submitfile    ) lsubmitfile=yes; submitfile="$OPTARG"    ;;
	     --executable    ) lexecutable=yes; executable="$OPTARG"   ;;
	     --arguments     ) larguments=yes; arguments="$OPTARG"     ;;
	     --logfile       ) llogfile=yes; logfile="$OPTARG"         ;;
	     --outputfile    ) loutputfile=yes; outputfile="$OPTARG"   ;;
	     --errorfile     ) lerrorfile=yes; errorfile="$OPTARG"     ;;
	     --comment	     ) lcomment=yes; comment="$OPTARG"         ;;
	     --restrict      ) restrict=yes	;;
             * )  echo "$name: invalide options ($OPTION)" ;;
         esac
       OPTIND=1
       shift
      ;;
    ? )  echo "$name: invalid options (short) "  ;;
  esac
done

if [ $lexecutable == no ]; then
	echo "$name: (executable) should be provided"
	exit
fi
if [ $lsubmitfile == no ]; then
	echo "$name: (condor_jobfile) should be provided"
	exit
fi

local condor_job_contents=empty
read -r -d '' condor_job_contents <<-END_PROC_JOB
	#$comment
	Executable = $executable
	Universe = vanilla
	Arguments = $arguments
	Output = $outputfile
	error = $errorfile
	Log = $logfile
 	notification = never
	
	Getenv = True
	image_size = 2G
	request_memory = 2G
	Requirements  = (OpSysAndVer == "RedHat6") 
	#&&
	#(Memory > 3000) && 
	#(VirtualMemory >= ImageSize) && (Memory =!= Undefined)

	#Requirements  = ((Arch == "INTEL" && OpSys == "LINUX") || \\
	#(Arch =="X86_64" && OpSys == "LINUX")) && \\
	#(machine != "glenn.astro.Princeton.EDU") && \\
	#(machine != "osca.astro.Princeton.EDU") 
	
	# suzuki and triump are Springdale 7 machines that do not support libX11 in 32 bits
	# glenn belongs to Yanfei Jiang
	# osca belongs to Fergal Mullaly

	#PeriodicHold = (ServerTime - JobCurrentStartDate) >= 60
	#PeriodicRelease = (ServerTime - JobCurrentStartDate) >= 120
	
	Queue $nprocs 

	#end of the job submission script

END_PROC_JOB

echo -e "${condor_job_contents}" >$submitfile
}


