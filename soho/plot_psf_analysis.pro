pro move_forward
n1 = !mywindow[1]
n2 = !mywindow[2]
n = !mywindow[1] * !mywindow[2]
if (n eq 0) then return
pos = !mywindow[0]
if ((pos mod n) eq (n - 1)) then begin
	!mywindow[0] = 0
	multiplot, [n1, n2], xgap = !my_xgap, ygap = !my_ygap, $
	mTitSize = !my_font_size, mxTitSize = !my_font_size, myTitSize = !my_font_size, $
	 /rowmajor
endif else  begin
	multiplot
	!mywindow[0] = pos + 1
endelse
return
end

pro move_to_window, pos
n = !mywindow[1] * !mywindow[2]
if (n eq 0) then return
pos0 = !mywindow[0]
dp = (pos - pos0 + n) mod n
if (dp gt 0) then begin
	for step = 1, dp, 1 do begin
		move_forward
	endfor
endif
return
end



function make_list_psf_files, dir = dir, prefix = prefix, band = band, camcol = camcol, run = run, rerun = rerun, field = field, count = count, lsize = lsize
name = "make_list_psf_stat_file"

if (n_elements(dir) eq 0) then dir = "./single-galaxy/images/psf-analysis"
if (n_elements(band) eq 0) then band_str = "?" else band_str = band
if (n_elements(camcol) eq 0) then camcol_str = "?" else camcol_str = string(camcol, format = "(I1.1)")
if (n_elements(run) eq 0) then run_str = "??????" else run_str = string(run, format = "(I6.6)")
if (n_elements(run) eq 0) then rundir = "*" else rundir = strtrim(string(run, format = "(I)"), 2)
if (n_elements(field) eq 0) then field_str = "????" else field_str = string(field, format = "(I4.4)")
if (n_elements(lsize) ne 1) then lsize = 0
if (lsize le 0.0) then checksize = 0 else checksize = 1

extension = ".fit"

filecount = 0
searchpath = dir+"/"+rerun+"/"+rundir+"/"+camcol_str+"/objc/"+prefix+"-"+run_str+"-"+band_str+camcol_str+"-"+field_str+extension
;; print, name, ": search path = ", searchpath
filelist = file_search(searchpath, count = filecount)
if (checksize and filecount gt 0) then begin
	sizecheck = bytarr(filecount)
	sizecheck[*] = 0
	bigfilecount = 0
	for ifile = 0, filecount - 1L,  1L do begin
		filename = filelist[ifile]
		filesize = (file_info(filename)).size
		if (filesize ge lsize) then begin
			sizecheck[ifile] = 1 
			bigfilecount++
		endif
	endfor
	if (bigfilecount ne 0) then begin
		bigindex = where(sizecheck ne 0)
		filelist = filelist[bigindex]
		filecount = bigfilecount
	endif else begin
		filelist = [-1]
		filecount = 0
	endelse
endif	

if (n_elements(count) ne 0) then count = filecount

return, filelist
end


pro plot_psf_bkgrnd_amplitude_stat, fname, darray = darray, barray = barray, $
	doerror = doerror, doc2w2 = doc2w2, $
	dobkgrnd = dobkgrnd, doamplitude = doamplitude, dolostlight = dolostlight, $
	dohalofrac = dohalofrac, dohldiff = dohldiff, $
	arcsec = arcsec, $ 
	overplot = overplot, xrange = xrange, yrange = yrange, xlog = xlog, ylog = ylog, $
	arange = arange, brange = brange, lrange = lrange, hrange = hrange, drange = drange, $
	title = title, seed = seed, error = error, $
	debug = debug, verbose = verbose, logfile = logfile

name = "plot_psf_bkgrnd_amplitude_stat"

;; if ((keyword_set(doamplitude) + keyword_set(dobkgrnd) + keyword_set(dolostlight) + keyword_set(dohalofrac) + keyword_set(dohldiff)) ne 1) then begin
;; 	print, name, ": ERROR - exactly one of the keywords 'doamplitude', 'dobkgrnd' should be set"
;;	return
;; endif

if (n_elements(title) eq 0) then title = ""
dodebug = keyword_set(debug)
doverbose = keyword_set(verbose)

stat = mrdfits(fname, 1, hdr, /silent)
;;determining which version of data it is
tags = strupcase(tag_names(stat[0]))
search_tag = strupcase("lostlight_c1")
nmatch = 0
lostlight_index = WHERE(STRMATCH(tags, search_tag, /FOLD_CASE) EQ 1, nmatch)
lostlight_exists = (nmatch gt 0)

search_tag = strupcase("halofrac_c1")
nmatch = 0
halofrac_index = WHERE(STRMATCH(tags, search_tag, /FOLD_CASE) EQ 1, nmatch)
halofrac_exists = (nmatch gt 0)

if (keyword_set(dohalofrac) and (halofrac_exists eq 0)) then begin
	error = -2
	print, name, ": ERROR - 'halofrac' not available in the stat file"
	return
endif
if (keyword_set(dohldiff) and (halofrac_exists eq 0)) then begin
	error = -2
	print, name, ": ERROR - 'halofrac' not available in the stat file"
	return
endif
inbound = stat.inboundary
if (keyword_set(arcsec)) then inbound = inbound * !PIXELSIZE
if (keyword_set(doc2w2)) then begin
	bkgrnd = stat.background_c2
	amplitude = stat.amplitude_c2
	bkgrnd_err = stat.background_w2
	amplitude_err = stat.amplitude_w2
	if (lostlight_exists) then begin
		lostlight = stat.lostlight_c2
		lostlight_err = stat.lostlight_w2
	endif
	if (halofrac_exists) then begin
	halofrac = stat.halofrac_c2
	halofrac_err = stat.halofrac_w2
	endif
endif else begin
	bkgrnd = stat.background_c1
	amplitude = stat.amplitude_c1
	bkgrnd_err = stat.background_w1
	amplitude_err = stat.amplitude_w1

	if (lostlight_exists) then begin
	lostlight = stat.lostlight_c1
	lostlight_err = stat.lostlight_w1
	endif
	if (halofrac_exists) then begin
	halofrac = stat.halofrac_c1
	halofrac_err = stat.halofrac_w1
	endif
endelse
if (not lostlight_exists) then begin
	print, format = "('l', $)"
	npixel = 31.0D0 * 31.0D0
	lostlight = 1.0D0 - amplitude * (1.0D0 + npixel * bkgrnd)
	lostlight_err = sqrt(amplitude_err ^ 2 + npixel ^ 2 * bkgrnd_err ^ 2)
endif
;; correcting the background becuase background in file is the one which is applied before amplitude correction, the one plotted by script should be the one the would be added before amplitude correction
bkgrnd = bkgrnd * amplitude
bkgrnd_err = bkgrnd_err * abs(amplitude)


if (halofrac_exists) then begin
	hldiff = abs(halofrac - lostlight)
	hldiff_err = sqrt(halofrac_err ^ 2 + lostlight_err ^ 2)
endif

;; analyzing halo frac and lost light for determining error
if (lostlight_exists) then begin
	nbad_l = 0
	bad_l = where(lostlight gt 1.0 or lostlight lt 0.0, nbad_l)
	if (nbad_l * 2 gt n_elements(lostlight)) then begin
		if (dodebug or doverbose) then	print, name, ": WARNING - suspicious file content (L < 0 | L > 1) - ", fname else print, format = "('L', $)"
		if (n_elements(logfile) ne 0) then printf, logfile, name, ": WARNING - suspicious file content (L < 0 | L > 1) - ", fname
	endif
endif
if (halofrac_exists) then begin
	nbad_h = 0
	bad_h = where(halofrac gt 1.0 or halofrac lt 0.0, nbad_h)
	if (nbad_h * 2 gt n_elements(halofrac)) then begin
		if (dodebug or doverbose) then print, name, ": WARNING - suspicious file content  (H < 0 | H > 1) - ", fname else print, format = "('H', $)"
		if (n_elements(logfile) ne 0) then printf, logfile, name, ": WARNING - suspicious file content  (H < 0 | H > 1) - ", fname
	endif
endif

xtickformat = "(i7)"
xtitle="r_{in}"
if (keyword_set(arcsec)) then xtitle = xtitle+" (arcsec)" else xtitle = xtitle + " (pixel)"
xtitle = textoidl(xtitle)

npoints = n_elements(inbound);
if (keyword_set(doerror)) then begin
	dither = randomu(seed, npoints, /uniform)
	dither = 0.1 * (dither - 0.5)
	x = inbound + dither
endif else begin
	x = inbound
endelse
;; exit if not enough points are available
if (npoints le 5) then begin
	error = 3
	return
endif

linestyle = 0
psym = 4
color = fix(256 * randomu(seed))

if (keyword_set(doamplitude)) then begin
	this_title = "amplitude"
	ytickformat = '(f9.2)'
	ytitle = "amplitude"
	y = amplitude
	yerr = amplitude_err

	if (keyword_set(doc2w2)) then $
	this_title = "median "+this_title+" "+title $
	else $
	this_title = "mean "+this_title+" "+title

	if (n_elements(arange) eq 2) then this_yrange = arange else this_yrange = yrange
	if (not keyword_set(overplot)) then $
	plot, [0], [0], xrange = xrange, yrange = this_yrange, xstyle = 1, ystyle = 1, $
	xtitle = xtitle, ytitle = ytitle, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
	xtickformat = xtickformat, ytickformat = ytickformat, $
	title = this_title, color = 0, /nodata
	if (keyword_set(doerror)) then begin
		ploterror, x, y, yerr, linestyle = linestyle, color = color, $
		xrange = xrange, yrange = yrange, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
		xstyle = 5, ystyle = 5
	endif else begin
		plot, x, y, linestyle = linestyle, color = color, $
		xrange = xrange, yrange = this_yrange, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
		xstyle = 5, ystyle = 5
	endelse
	;; multiplot
	move_forward
endif
if (keyword_set(dobkgrnd)) then begin
	this_title = "background"
	ytickformat = '(e9.1)'
	ytitle = "background"
	y = bkgrnd
	yerr = bkgrnd_err
	if (keyword_set(doc2w2)) then $
	this_title = "median "+this_title+" "+title $
	else $
	this_title = "mean "+this_title+" "+title

	if (n_elements(brange) eq 2) then this_yrange = brange else this_yrange = yrange
	if (not keyword_set(overplot)) then $
	plot, [0], [0], xrange = xrange, yrange = this_yrange, xstyle = 1, ystyle = 1, $
	xtitle = xtitle, ytitle = ytitle, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
	xtickformat = xtickformat, ytickformat = ytickformat, $
	title = this_title, color = 0, /nodata
	if (keyword_set(doerror)) then begin
		ploterror, x, y, yerr, linestyle = linestyle, color = color, $
		xrange = xrange, yrange = yrange, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
		xstyle = 5, ystyle = 5
	endif else begin
		plot, x, y, linestyle = linestyle, color = color, $
		xrange = xrange, yrange = this_yrange, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
		xstyle = 5, ystyle = 5
	endelse
	;; multiplot
	move_forward

endif
if (keyword_set(dolostlight)) then begin
	this_title = "lost light"
	ytickformat = '(f9.2)'
	ytitle = "lost light"
	y = lostlight
	yerr = lostlight_err
	if (keyword_set(doc2w2)) then $
	this_title = "median "+this_title+" "+title $
	else $
	this_title = "mean "+this_title+" "+title

	if (n_elements(lrange) eq 2) then this_yrange = lrange else this_yrange = yrange
	if (not keyword_set(overplot)) then $
	plot, [0], [0], xrange = xrange, yrange = this_yrange, xstyle = 1, ystyle = 1, $
	xtitle = xtitle, ytitle = ytitle, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
	xtickformat = xtickformat, ytickformat = ytickformat, $
	title = this_title, color = 0, /nodata
	if (keyword_set(doerror)) then begin
		ploterror, x, y, yerr, linestyle = linestyle, color = color, $
		xrange = xrange, yrange = yrange, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
		xstyle = 5, ystyle = 5
	endif else begin
		plot, x, y, linestyle = linestyle, color = color, $
		xrange = xrange, yrange = this_yrange, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
		xstyle = 5, ystyle = 5
	endelse
	;; multiplot
	move_forward

endif
if (keyword_set(dohalofrac)) then begin
	this_title = "halo fraction"
	ytickformat = '(f9.2)'
	ytitle = "halo fraction"
	y = halofrac
	yerr = halofrac_err
	if (keyword_set(doc2w2)) then $
	this_title = "median "+this_title+" "+title $
	else $
	this_title = "mean "+this_title+" "+title

	if (n_elements(hrange) eq 2) then this_yrange = hrange else this_yrange = yrange
	if (not keyword_set(overplot)) then $
	plot, [0], [0], xrange = xrange, yrange = this_yrange, xstyle = 1, ystyle = 1, $
	xtitle = xtitle, ytitle = ytitle, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
	xtickformat = xtickformat, ytickformat = ytickformat, $
	title = this_title, color = 0, /nodata
	if (keyword_set(doerror)) then begin
		ploterror, x, y, yerr, linestyle = linestyle, color = color, $
		xrange = xrange, yrange = yrange, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
		xstyle = 5, ystyle = 5
	endif else begin
		plot, x, y, linestyle = linestyle, color = color, $
		xrange = xrange, yrange = this_yrange, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
		xstyle = 5, ystyle = 5
	endelse
	;; multiplot
	move_forward
endif
if (keyword_set(dohldiff)) then begin
	this_title = "halofrac - lostlight"
	ytickformat = "(f9.2)"
	ytitle = "halofrac - lostlight"
	y = hldiff
	yerr = hldiff_err
	if (keyword_set(doc2w2)) then $
	this_title = "median "+this_title+" "+title $
	else $
	this_title = "mean "+this_title+" "+title

	if (n_elements(drange) eq 2) then this_yrange = drange else this_yrange = yrange
	if (not keyword_set(overplot)) then $
	plot, [0], [0], xrange = xrange, yrange = this_yrange, xstyle = 1, ystyle = 1, $
	xtitle = xtitle, ytitle = ytitle, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
	xtickformat = xtickformat, ytickformat = ytickformat, $
	title = this_title, color = 0, /nodata
	if (keyword_set(doerror)) then begin
		ploterror, x, y, yerr, linestyle = linestyle, color = color, $
		xrange = xrange, yrange = yrange, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
		xstyle = 5, ystyle = 5
	endif else begin
		plot, x, y, linestyle = linestyle, color = color, $
		xrange = xrange, yrange = this_yrange, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
		xstyle = 5, ystyle = 5
	endelse
	;; multiplot
	move_forward
endif

if (n_elements(darray) ne 0) then darray = y
if (n_elements(barray) ne 0) then barray = x	
error = 0
return
end

function create_bundle_filename, file
name = "create_bundle_filename"
if (n_elements(file) eq 0) then begin
	print, name, ": ERROR - 'file' string should not be empty"
	return, -1
endif

pos=strpos(file, '.', /REVERSE_SEARCH)
file_no_extension = file
extension = ""
if (pos ne -1 and pos ne 0) then begin
	len = strlen(file)
	len_extension = len - pos
	file_no_extension= strmid(file, 0, pos)
	extension = strmid(file, pos, len_extension)
endif
if (pos eq 0) then begin
	file_no_extension = ""
	extension = file
endif
suffix = "-Bundle"
efile = file_no_extension+suffix+extension
return, efile
end


function get_mfile, nlist, index, doeqcurves = doeqcurves
name = "get_mfile"

if (index ge n_elements(nlist)) then begin
	print, name, ": ERROR - (index) beyond (nlist) size"
	return, [-1]
endif
if (not keyword_set(doeqcurves)) then return, nlist[index]

min_nlist = min(nlist)
min_nlist2 = min_nlist
ngood = 0
goodindex = where(nlist ne 0, ngood)
if (ngood ne 0) then min_nlist2 = min(nlist[goodindex])

if ((min_nlist2 gt 0) and (nlist[index] gt 0)) then begin
	return, min_nlist2
endif

return, 0
end


pro plot_psf_analysis, dir_list = dir_list, epsfile = epsfile, $
	camcol_list = camcol_list, band_list = band_list, run_list = run_list, $
	rerun = rerun, $
	doerror = doerror, doc2w2 = doc2w2, dobundle = dobundle, $
	stats = stats, stat_lines = stat_lines, $
	dobkgrnd = dobkgrnd, doamplitude = doamplitude, dolostlight = dolostlight, $
	dohalofrac = dohalofrac, dohldiff = dohldiff, $
	arcsec = arcsec, $
	yrange = yrange, xrange = xrange, xlog = xlog, ylog = ylog, $
	arange = arange, brange = brange, lrange = lrange, hrange = hrange, drange = drange, $
	lsize = lsize, $
	doeqcurves = doeqcurves, dosinglepanel = dosinglepanel, $
	seed = seed, debug = debug, verbose = verbose, logfile = logfile

name = "plot_psf_analysis"
if (n_elements(camcol_list) eq 0) then begin
	camcol_list = [1, 2, 3, 4]
	print, name, ": setting (camcol_list) to default value = ", camcol_list
endif
if (n_elements(band_list) eq 0) then begin
	band_list = ["r", "g", "i"]
	print, name, ": setting (band_list) to default value = ", band_list
endif
if (n_elements(run_list) eq 0) then begin 
	run_list = [4797]
	print, name, ": setting (run_list) to default value = ", run_list
endif
if (n_elements(rerun) eq 0) then begin
	rerun='301'
	print, name, ": setting (rerun) to default value = ", rerun
endif
ndir = n_elements(dir_list)
if (ndir eq 0) then begin
	print, name, ": ERROR - dir_list should be provided"
	return
endif

nrad = 25
nfile = 300

ncamcol = n_elements(camcol_list)
nband = n_elements(band_list)
nrun = n_elements(run_list)

lygap = 0.09 * 21.0 / 4

DEFSYSV, '!MY_XSIZE', 21.0
DEFSYSV, '!MY_YSIZE', 10.3
DEFSYSV, '!MY_XYRATIO', 1.67

DEFSYSV, '!MY_XGAP', 0.12
DEFSYSV, '!MY_YGAP', 0.09
DEFSYSV, '!MY_FONT_SIZE', 10

!Y.MARGIN = [2, 2]

if (n_elements(seed) eq 0) then seed = 0;

n1 = nband
if (keyword_set(dosinglepanel)) then begin
	n2 = 1 
	singlepanel = 1
	doequalcurves = 1
endif else begin 
	n2 = ncamcol
	singlepanel = 0
	doequalcurves = keyword_set(doeqcurves)
endelse
n2 = n2 * ndir

xtickformat = "(i7)"
xtitle="r_{in}"
if (keyword_set(arcsec)) then xtitle = xtitle + " (arcsec)" else xtitle = xtitle + " (pixel)"
xtitle = textoidl(xtitle)

if (keyword_set(doamplitude)) then begin
	this_title = "amplitude"
	ytickformat = '(f7.2)'
	ytitle = "amplitude"
endif
if (keyword_set(dobkgrnd)) then begin
	this_title = "background"
	ytickformat = '(e9.1)'
	ytitle = "background"
endif
if (keyword_set(dolostlight)) then begin
	this_title = "lost light"
	ytickformat = '(f7.2)'
	ytitle = "lost light"
endif
if (keyword_set(dohalofrac)) then begin
	this_title = "halo fraction"
	ytickformat = '(f7.2)'
	ytitle = "halo fraction"
endif
if (keyword_set(dohldiff)) then begin
	this_title = "halofrac - lostlight"
	ytickformat = "(f7.2)"
	ytitle = "halofrac - lostlight"
endif
nrecord = keyword_set(doamplitude) + keyword_set(dobkgrnd) + keyword_set(dolostlight) + $
	keyword_set(dohalofrac) + keyword_set(dohldiff)

if (keyword_set(doc2w2)) then $
	this_title = "median "$
	else $
	this_title = "mean "
if (n_elements(logfile) ne 0) then begin
	get_lun, ulogfile
	openu, ulogfile, logfile, /append
	printf, ulogfile, "==========================================================="
	printf, ulogfile, name
	printf, ulogfile, systime()
	printf, ulogfile, " "
endif else begin
	ulogfile = !NULL
endelse

if (singlepanel eq 1) then begin
	nplot_camcol = 0 
	nplot_dir = nrecord
endif else begin
	nplot_camcol = nrecord
	nplot_dir = nrecord * ncamcol
endelse
nplot_column = ndir * nplot_dir
n2 = nplot_column
print, name, ": writing to file = ", epsfile
olddevice = !d.name ; store the current device name in a variable
set_plot, 'PS' ; switch to the postscript device
Device, DECOMPOSED=decomposed, COLOR=1, BITS_PER_PIXEL=8
device, /encapsulated, file=epsfile, font_size = !my_font_size ; specify some details, give the file a name
ct = 13
loadct, ct, file = "./colors/colors1.tbl"

;; size of the region
ymargin_size = (!Y.Margin[0] + !Y.Margin[1]) * !D.Y_CH_SIZE/ !D.Y_PX_CM 
!my_xgap = !my_xgap / (n1)
!my_ygap = !my_ygap / (n2)
xsize = !my_xsize
ysize = (!my_xsize * (1.0 - (n1) * !my_xgap) * n2) / (n1 * !my_xyratio) + lygap * (n2) + ymargin_size

!my_ygap = lygap / ysize

print, name, ": setting up graphic device, xsize = ", xsize, ", ysize = ", ysize, ", [n1, n2] = ", n1, n2
device, xsize=xsize, ysize=ysize
multiplot, /reset
multiplot, [n1, n2], xgap = !my_xgap, ygap = !my_ygap, $
	mTitSize = !my_font_size, mxTitSize = !my_font_size, myTitSize = !my_font_size, $
	 /rowmajor
DEFSYSV, "!MYWINDOW", [0, n1, n2]

npix = ndir * nband * ncamcol * nrun * nfile * nrad
data = replicate(0.0D0, npix)
data = reform(data, [ndir, nband, ncamcol, nrun, nfile, nrad])

ndata = replicate(0.0D0, ndir * nband * ncamcol * nrun, nfile)
ndata = reform(ndata, [ndir, nband, ncamcol, nrun, nfile])

nrad_array = replicate(0, ndir * nband * ncamcol * nrun * nfile)
nrad_array = reform(nrad_array, [ndir, nband, ncamcol, nrun, nfile])

num_files = replicate(0, ndir * nband * ncamcol * nrun)
num_files = reform(num_files, [ndir, nband, ncamcol, nrun])

for idir = 0, ndir - 1L, 1L do begin
for irun = 0, nrun - 1L, 1L do begin
for iband = 0, nband - 1L, 1L do begin
for icamcol = 0, ncamcol - 1L, 1L do begin
	dir = dir_list[idir]
	band = band_list[iband]
	camcol = camcol_list[icamcol]
	run = run_list[irun]
	band_str = band
	camcol_str = string(camcol, "(I1.1)")
	mfile = 0
	prefix = "psfS"
	title = "("+band_str+camcol_str+")"
	file_list_bc = make_list_psf_files(dir = dir, prefix = prefix, band = band, camcol = camcol, count = mfile, rerun = rerun, run = run, lsize = lsize)
	num_files[idir, iband, icamcol, irun] = mfile
endfor
endfor
endfor
endfor
file_list_bc = 0

for iband = 0, nband - 1L, 1L do begin

seed0 = seed
for idir = 0, ndir - 1L, 1L do begin
all_title = ""

for icamcol = 0, ncamcol - 1L, 1L do begin
print, format = "('/c',$)" 

dir = dir_list[idir]
band = band_list[iband]
camcol = camcol_list[icamcol]
band_str = band
camcol_str = string(camcol, "(I1.1)")
	
c_title = "("+band_str+camcol_str+")"
all_title = all_title+c_title	

pmulti0 = iband * nplot_column + idir * nplot_dir + icamcol * nplot_camcol
move_to_window, pmulti0
for irun = 0, nrun - 1L, 1L do begin

	print, format = "('r', $)"
	run = run_list[irun]
	;; print, run, format = "(I5, $)"
	;; determine mfile for this particular camcol
	mfile_camcol = get_mfile(num_files[idir, iband, *, irun], (icamcol), $
				doeqcurves = doequalcurves)

	mfile = 0
	prefix = "psfS"
	title = "("+band_str+camcol_str+")"
	if (singlepanel) then title = ""
	file_list = make_list_psf_files(dir = dir, prefix = prefix, $
	band = band, camcol = camcol, run = run, rerun = rerun, count = mfile, lsize = lsize)

	idata = 0
	if (mfile_camcol * mfile gt 0) then begin

	rfndex = random_indices(mfile, mfile, seed = seed)
	mrfile = n_elements(rfndex)

	;; saving the position of the plot so that we start from the right position each time inside the file look
	move_to_window, pmulti0
	for irfile = 0, mrfile -1L, 1L do begin
		ifile = rfndex[irfile]
		print, format = "('.', $)"
		dooverplot = (irfile gt 0) or (singlepanel)
		file = file_list[ifile]
		error = 0
		darray = [-1.0]
		barray = [-1.0]
		move_to_window, pmulti0
		plot_psf_bkgrnd_amplitude_stat, file, darray = darray, barray = barray, $
		title = title, doerror = keyword_set(doerror), doc2w2 = keyword_set(doc2w2), $
		dobkgrnd = keyword_set(dobkgrnd), doamplitude = keyword_set(doamplitude), $
		dolostlight = keyword_set(dolostlight), dohalofrac = keyword_set(dohalofrac), $
		dohldiff = keyword_set(dohldiff), $
		arcsec = keyword_set(arcsec), $
		overplot = dooverplot, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
		xrange = xrange, yrange = yrange, $
		arange = arange, brange = brange, lrange = lrange, hrange = hrange, drange = drange, $
		seed = seed0, error = error, $
		verbose = keyword_set(verbose), debug = keyword_set(debug), logfile = ulogfile
		if (error eq 0 and idata lt nfile and keyword_set(dobundle)) then begin
			copy_nrad = min([n_elements(darray), nrad])
			copy_index = lindgen(copy_nrad)
			data[idir, iband, icamcol, irun, idata, copy_index] = darray[copy_index]	
			nrad_array[idir, iband, icamcol, irun, idata] = copy_nrad
			idata++
		endif
	endfor ;; file loop
	move_to_window, pmulti0

	endif
	ndata[idir, iband, icamcol, irun] = idata
	
endfor ;; run loop

	print, format = "('\', $)"
	if ((not singlepanel) or (singlepanel and (icamcol eq ncamcol - 1L))) then begin

		move_to_window, pmulti0	
		if keyword_set(doamplitude) then begin

			this_title = "amplitude"
			ytickformat = '(f7.2)'
			ytitle = "amplitude"

			this_all_title = ""
			if (not singlepanel) then this_all_title = all_title
			if (singlepanel and (!mywindow[0] mod n2 eq 0)) then this_all_title = all_title else this_all_title = ""
			this_xtitle = ""
			if ((!mywindow[0] mod n2) eq (n2 - 1)) then this_xtitle = xtitle
			if (n_elements(arange) eq 2) then this_yrange = arange else this_yrange = yrange
			if (iband eq 0) then this_ytitle = ytitle else this_ytitle = ""
			plot, [0], [0], xrange = xrange, yrange = this_yrange, xstyle = 1, ystyle = 1, $
			xtitle = this_xtitle, ytitle = this_ytitle, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
			xtickformat = xtickformat, ytickformat = ytickformat, $
			title = this_all_title, color = 0, /nodata
			;;multiplot
			move_forward
		endif
		if keyword_set(dobkgrnd) then begin

			this_title = "background"
			ytickformat = '(e9.1)'
			ytitle = "background"

			this_all_title = ""
			if (not singlepanel) then this_all_title = all_title
			if (singlepanel and (!mywindow[0] mod n2 eq 0)) then this_all_title = all_title else this_all_title = ""
			this_xtitle = ""
			if ((!mywindow[0] mod n2) eq (n2 - 1)) then this_xtitle = xtitle
	
			if (n_elements(brange) eq 2) then this_yrange = brange else this_yrange = yrange
			if (iband eq 0) then this_ytitle = ytitle else this_ytitle = ""
			plot, [0], [0], xrange = xrange, yrange = this_yrange, xstyle = 1, ystyle = 1, $
			xtitle = this_xtitle, ytitle = this_ytitle, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
			xtickformat = xtickformat, ytickformat = ytickformat, $
			title = this_all_title, color = 0, /nodata
			;; multiplot
			move_forward
		endif
		if keyword_set(dolostlight) then begin

			this_title = "lost light"
			ytickformat = '(f7.2)'
			ytitle = "lost light"
			
			this_all_title = ""
			if (not singlepanel) then this_all_title = all_title
			if (singlepanel and (!mywindow[0] mod n2 eq 0)) then this_all_title = all_title else this_all_title = ""
			this_xtitle = ""
			if ((!mywindow[0] mod n2) eq (n2 - 1)) then this_xtitle = xtitle
	
			if (n_elements(lrange) eq 2) then this_yrange = lrange else this_yrange = yrange
			if (iband eq 0) then this_ytitle = ytitle else this_ytitle = ""
			plot, [0], [0], xrange = xrange, yrange = this_yrange, xstyle = 1, ystyle = 1, $
			xtitle = this_xtitle, ytitle = this_ytitle, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
			xtickformat = xtickformat, ytickformat = ytickformat, $
			title = this_all_title, color = 0, /nodata
			;; multiplot
			move_forward
		endif
		if keyword_set(dohalofrac) then begin

			this_title = "halo fraction"
			ytickformat = '(f7.2)'
			ytitle = "halo fraction"

			this_all_title = ""
			if (not singlepanel) then this_all_title = all_title
			if (singlepanel and (!mywindow[0] mod n2 eq 0)) then this_all_title = all_title else this_all_title = ""
			this_xtitle = ""
			if ((!mywindow[0] mod n2) eq (n2 - 1)) then this_xtitle = xtitle
	
			if (n_elements(hrange) eq 2) then this_yrange = hrange else this_yrange = yrange
			if (iband eq 0) then this_ytitle = ytitle else this_ytitle = ""
			plot, [0], [0], xrange = xrange, yrange = this_yrange, xstyle = 1, ystyle = 1, $
			xtitle = this_xtitle, ytitle = this_ytitle, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
			xtickformat = xtickformat, ytickformat = ytickformat, $
			title = this_all_title, color = 0, /nodata
			;; multiplot
			move_forward
		endif
		if keyword_set(dohldiff) then begin

			this_title = "halofrac - lostlight"
			ytickformat = "(f7.2)"
			ytitle = "halofrac - lostlight"

			this_all_title = ""
			if (not singlepanel) then this_all_title = all_title
			if (singlepanel and (!mywindow[0] mod n2 eq 0)) then this_all_title = all_title else this_all_title = ""
			this_xtitle = ""
			if ((!mywindow[0] mod n2) eq (n2 - 1)) then this_xtitle = xtitle
	
			if (n_elements(drange) eq 2) then this_yrange = drange else this_yrange = yrange
			if (iband eq 0) then this_ytitle = ytitle else this_ytitle = ""
			plot, [0], [0], xrange = xrange, yrange = this_yrange, xstyle = 1, ystyle = 1, $
			xtitle = this_xtitle, ytitle = this_ytitle, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
			xtickformat = xtickformat, ytickformat = ytickformat, $
			title = this_all_title, color = 0, /nodata
			;; multiplot
			move_forward
		endif
		move_to_window, pmulti0
	endif


endfor ;; icamcol loop

	if (singlepanel and 0) then begin
		if (iband eq 0) then this_ytitle = ytitle else this_ytitle = ""
		plot, [0], [0], xrange = xrange, yrange = yrange, xstyle = 1, ystyle = 1, $
		xtitle = xtitle, ytitle = this_ytitle, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
		xtickformat = xtickformat, ytickformat = ytickformat, $
		title = this_title + all_title, color = 0, /nodata
		multiplot
	endif

endfor ;; idir loop

endfor ;; iband loop

print
print, name, ": closing device"
device, /close ; close the file
set_plot, olddevice ; go back to the graphics device
command = "epstopdf "+epsfile
spawn, command
extpos = STRPOS(epsfile, '.', /REVERSE_SEARCH)
jpegfile = STRMID(epsfile, 0, extpos)+".jpg"
command = "convert " + epsfile + " " +  jpegfile
spawn, command

if (keyword_set(dobundle)) then begin

print, name, ": plotting the bundle curves"
bundlefile = create_bundle_filename(epsfile)
if (not singlepanel) then n2++
xsize = !my_xsize
ysize= (!my_xsize * n2) / (n1 * !my_xyratio)
print, name, ": setting up graphic device, xsize = ", xsize, ", ysize = ", ysize, ", [n1, n2] = ", n1, n2
print, name, ": writing to file = ", bundlefile
olddevice = !d.name ; store the current device name in a variable
set_plot, 'PS' ; switch to the postscript device
Device, DECOMPOSED=decomposed, COLOR=1, BITS_PER_PIXEL=8
device, /encapsulated, file=bundlefile, font_size = !my_font_size ; specify some details, give the file a name
ct = 0
loadct, ct, file = "./colors/colors1.tbl"
device, xsize=xsize, ysize=ysize
multiplot, /reset
multiplot, [n1, n2], xgap = !my_xgap, ygap = !my_ygap, $
	mTitSize = !my_font_size, mxTitSize = !my_font_size, myTitSize = !my_font_size, $
	/rowmajor


if (n_elements(stats) eq 0) then stats = [0.25, 0.50, 0.75]
nstat = n_elements(stats)
if (n_elements(stat_lines) eq 0) then stat_lines = replicate(0, nstat)
desc_array = dblArr(nstat, nrad)
for iband = 0, nband - 1L, 1L do begin

	for idir = 0, ndir - 1L, 1L do begin
	all_title = ""
	
	for icamcol = 0, ncamcol - 1L, 1L do begin
	;; for irun = 0, nrun - 1L, 1L do begin
	
	dir = dir_list[idir]
	band = band_list[iband]
	camcol = camcol_list[icamcol]
	;; run = run_list[irun]

	band_str = band
	camcol_str = string(camcol, "(I1.1)")
	error = 0
	extract_descriptive_curve, curve_array = data[idir, iband, icamcol, *, *, *], $
	nrad_array = nrad_array[idir, iband, icamcol, *, *], ndata = ndata[idir, iband, icamcol, *], $
	nrun = nrun, nfile = nfile, desc_array = desc_array, stats = stats, $
	doc2w2 = keyword_set(doc2w2), error = error

	max_nrad_array = max(nrad_array[idir, iband, icamcol, *, *])

	if (max_nrad_array ne 0) then begin

		c_title = "("+band_str+camcol_str+")"
		all_title = all_title+c_title
		max_radius = min([max(nrad_array[idir, iband, icamcol, *, *]), nrad])
		x = lindgen(max_radius)
	
		if (not singlepanel) then begin
			if (iband eq 0) then this_ytitle = ytitle else this_ytitle = ""
			plot, [0], [0], xrange = xrange, yrange = yrange, xstyle = 1, ystyle = 1, $
			xtitle = xtitle, ytitle = this_ytitle, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
			xtickformat = xtickformat, ytickformat = ytickformat, $
			title = this_title + c_title, color = 0, /nodata
		
			for istat = 0L, nstat - 1L, 1 do begin
				y = desc_array[istat, 0:max_radius - 1L]
				plot, x, y, xrange = xrange, yrange = yrange, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
				xstyle = 5, ystyle = 5, linestyle = stat_lines[istat]
			endfor
		endif

	endif

	;; endfor ;; end of run loop
	if (not singlepanel) then multiplot

	endfor ;; end of camcol loop
	
	
	error = 0
	bundle_descriptive_curve, curve_array = data[idir, iband, *, *, *, *], $
	nrad_array = nrad_array[idir, iband, *, *, *], ndata_array = ndata[idir, iband, *, * ], $ 
	ncamcol = ncamcol, nrun = nrun, nfile = nfile, $
	desc_array = desc_array, stats = stats, $
	doc2w2 = keyword_set(doc2w2), seed = seed, error = error


	max_nrad_array = max(nrad_array[idir, iband, *, *])
	if (max_nrad_array gt 0) then begin
	
		max_radius = min([max(nrad_array[idir, iband, *, *]), nrad])
		x = lindgen(max_radius)
		if keyword_set(arcsec) then x = x * !PIXELSIZE
		if (iband eq 0) then this_ytitle = ytitle else this_ytitle = ""
		plot, [0], [0], xrange = xrange, yrange = yrange, xstyle = 1, ystyle = 1, $
		xtitle = xtitle, ytitle = this_ytitle, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
		xtickformat = xtickformat, ytickformat = ytickformat, $
		title = this_title + all_title, color = 0, /nodata
		for istat = 0L, nstat - 1L, 1 do begin
			y = desc_array[istat, 0:max_radius- 1L]
			plot, x, y, xrange = xrange, yrange = yrange, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
			xstyle = 5, ystyle = 5, linestyle = stat_lines[istat]
		endfor

	endif

	multiplot
	endfor ;; idir loop
	
endfor

print, name, ": closing device"
device, /close ; close the file
set_plot, olddevice ; go back to the graphics device

command = "epstopdf "+bundlefile
spawn, command
extpos = STRPOS(bundlefile, '.', /REVERSE_SEARCH)
jpegfile = STRMID(bundlefile, 0, extpos)+".jpg"
command = "convert " + epsfile + " " +  jpegfile
spawn, command


endif

if (n_elements(ulogfile) ne 0) then begin
	close, ulogfile
	free_lun, ulogfile
endif

return
end

pro create_annotation_from_filename, filename, run_str = run_str, field_str = field_str, cband_str = cband_str, $
	bannonate = bannonate

pos = STRPOS(filename, 'psfR', /REVERSE_SEARCH)
if (pos lt 0) then begin
	run_str = ""
	field_str = ""
	cband_str = ""
	return
endif

file = strmid(filename, pos)
run_str = "run: " + strtrim(string(fix(strmid(file, 5, 6))), 2)
if (keyword_set(bannonate)) then  $
	cband_str = "camcol: " + strmid(file, 12, 2) $
	else $
	cband_str = "camcol: " + strmid(file, 13, 1)

field_str = "frame: "+ strtrim(string(fix(strmid(file, 15, 4))), 2)
;; print, run_str, " ", cband_str, " ", field_str
return
end


pro graph_radial_profile, pndex = pndex, rad = rad, nrad = nrad, average = average, $
	xlog = xlog, ylog = ylog, xrange = xrange, yrange = yrange, arcsec = arcsec, $
	colors = colors, linestyles = linestyles, thicks = thicks, error = error

name = "graph_radial_profile"

if (n_elements(nrad) eq 0 or n_elements(rad) eq 0 or n_elements(average) eq 0) then begin
	print, name, ": ERROR - (nrad, rad, average) should all be provided"
	error = -1
	return
endif
if (pndex ge 3) then begin
	print, name, ": ERROR - pndex = ", pndex, " is out of bounds, nrad[", n_elements(nrad), "]"
	error = -1
	return
endif
if (n_elements(nrad) eq 0) then begin
	print, name, ": ERROR - nrad should be passed"
	error = -1
	return
endif
if (n_elements(nrad) eq 1) then nrad_pndex = nrad else nrad_pndex = nrad[pndex]
if (nrad_pndex le 0) then begin
	print, name, ": ERROR - nrad = ", nrad, " not supported"
	error = -1
	return
endif
if (n_elements(colors) eq 0) then colors = [0, 1, 2]
if (n_elements(linestyles) eq 0) then linestyles = [0, 1, 2]
if (n_elements(thicks) eq 0) then thicks = [1, 1, 1]
rad_pndex = rad[0:nrad_pndex-1L]
if (keyword_set(arcsec)) then rad_pndex = rad_pndex * !PIXELSIZE

average_pndex = average[0:nrad_pndex-1L, pndex]
t0 = max(average_pndex)
average_pndex = average_pndex / t0

reverse_index = nrad_pndex - 1L - lindgen(nrad_pndex) 

;; extending to negative values of r for probable symmetric plots

average_pndex_neg = average_pndex[reverse_index]
rad_pndex_neg = -rad_pndex[reverse_index]
ext_rad_pndex = [rad_pndex_neg, rad_pndex]
ext_average_pndex = [average_pndex_neg, average_pndex]

plot, ext_rad_pndex, ext_average_pndex, xrange = xrange, yrange = yrange, $
	xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
	xstyle = 5, ystyle = 5, $
	color = colors[pndex], linestyle = linestyles[pndex], thick = thicks[pndex]
	
error = 0
return
end

function get_image_pointer_array_from_starinfo, starinfo = starinfo, error = error
name = "get_image_pointer_array_from_starinfo"

if (n_elements(starinfo) ne 1) then begin
	print, name, ": ERROR - 'starinfo' should be supplied"
	error = -1
endif

bzero = starinfo.bzero
bscale = starinfo.bscale

nrow0 = starinfo.rnrow
ncol0 = starinfo.rncol
image0 = double(reform((starinfo.rrows), [nrow0, ncol0]))
image0 = (image0 - bzero[0]) / bscale[0]
t0 = max(image0)
image0 = image0 / t0

nrow1 = starinfo.rnrow2
ncol1 = starinfo.rncol2
image1 = double(reform((starinfo.rrows2), [nrow1, ncol1]))
image1 = (image1 - bzero[1]) / bscale[1]
t1 = max(image1)
image1 = image1 / t1

nrow2 = starinfo.rnrow3
ncol2 = starinfo.rncol3
image2 = double(reform((starinfo.rrows3), [nrow2, ncol2]))
image2 = (image2 - bzero[2]) / bscale[2]
t2 = max(image2)
image2 = image2 / t2

image_array =  PTRARR(3, /ALLOCATE_HEAP)
*(image_array[0]) = image0
*(image_array[1]) = image1
*(image_array[2]) = image2

if (keyword_set(debug)) then begin
	print, name, ": min(rrows0) = ", min(starinfo.rrows)
	print, name, ": min(rrows1) = ", min(starinfo.rrows2)
	print, name, ": min(rrows2) = ", min(starinfo.rrows3)

	print, name, ": max(rrows0) = ", max(starinfo.rrows)
	print, name, ": max(rrows1) = ", max(starinfo.rrows2)
	print, name, ": max(rrows2) = ", max(starinfo.rrows3)

	print, name, ": bzero  = ", bzero
	print, name, ": bscale = ", bscale
endif

error = 0
return, image_array
end

pro graph_pixel_profile, pndex = pndex, image = image, $
		xlog = xlog, ylog = ylog, xrange = xrange, yrange = yrange, $
		arcsec = arcsec, sectional = sectional, colors = colors, psyms = psyms, thicks = thicks, error = error
	
name = "graph_pixel_profile"

if (n_elements(pndex) ne 1) then begin
	print, name, ": ERROR - 'pndex' should be provided"
	error = -1
	return
endif
if (pndex ge 3) then begin
	print, name, ": ERROR - pndex = ", pndex, " is out of bounds, nrad[", n_elements(nrad), "]"
	error = -1
	return
endif
if (n_elements(colors) eq 0) then colors = [0, 1, 2]
if (n_elements(psyms) eq 0) then psyms = [3, 3, 3]
if (n_elements(thicks) eq 0) then thicks = [1, 1, 1]
if (n_elements(image) le pndex) then begin
	print, name, ": ERROR - number of 'image' pointer should be greater than 'pndex'"
	error = -2
	return
endif

pixels = *(image[pndex])
dims = size(pixels, /dimensions)
nrow = dims[0]
ncol = dims[1]
x = dindgen(nrow) - (nrow - 1.0) / 2.0
x = rebin(x, [nrow, ncol])
y = dindgen(ncol) - (ncol - 1.0) / 2.0
y = reform(y, [1, ncol])
y = rebin(y, [nrow, ncol])
r = sqrt(x ^ 2 + y ^ 2)
if (keyword_set(arcsec)) then r = r * !PIXELSIZE

xx = [x, x]
yy = [y, y]
rr = [r, -r]
pp = [pixels, pixels]
t1 = tan(30 * !pi / 180.0)
t2 = tan(60 * !pi / 180.0)

if (keyword_set(sectional)) then index = where(pp gt 0.0D0 and yy gt xx * t1 and yy lt xx * t2) else index = where(pp gt 0.0D0)

plot, rr[index], pp[index], xrange = xrange, yrange = yrange, $
	xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
	xstyle = 5, ystyle = 5, $
	color = colors[pndex], psym = psyms[pndex]

if (keyword_set(debug)) then begin
	print, name, ": pndex, nrow, ncol = ", pndex, nrow, ncol
	print, name, ": min(r), max(r) = ", min(r), max(r)
	print, name, ": min(psf), max(psf) = ", min(pixels), max(pixels)
	;; help, r
	;; help, pixels
	;; help, rr
	;; help, pp
endif

error = 0
return
end

pro draw_psf_profile, filename, starno = starno, boundary = boundary, $ 
	klprof = klprof, extprof = extprof, gluedprof = gluedprof, arcsec = arcsec, $ 
	xrange = xrange, yrange = yrange, xlog = xlog, ylog = ylog, $
	run_str = run_str, field_str = field_str, cband_str = cband_str, $ ;; information to posted next to the plot
	colors = colors, thicks = thicks, linestyles = linestyles, psyms = psyms, $
	pixelized = pixelized, azimuthal = azimuthal, sectional = sectional, $
	title = title, overplot = overplot, bannonate = bannonate, $ 
	seed = seed, verbose = verbose, debug = debug, error = error, logfile = logfile

name = "draw_psf_profile"
error = 0
doklprof = keyword_set(klprof)
dogluedprof = keyword_set(gluedprof)
doextprof = keyword_set(extprof)

if (keyword_set(klprof) + keyword_set(gluedprof) + keyword_set(extprof) eq 0) then begin
	doklprof = 1
	dogluedprof = 1
	doextprof = 1
endif
if (n_elements(filename) eq 0) then begin
	print, name, ": ERROR - filename should be passed"
	error = -1
	return
endif
if (n_elements(boundary) ne 2) then begin
	print, name, ": ERROR - both boundaries should be provided"
	error = -2
	return
endif
status = 0
columns = ["XC", "YC", "INBOUNDARY", "OUTBOUNDARY", "RAD", "AVERAGE", "NRAD"]
columns2 = ["BZERO", "BSCALE", "RROWS", "RNROW", "RNCOL", "RROWS2", "RNROW2", "RNCOL2", "RROWS3", "RNROW3", "RNCOL3"]
info = mrdfits(filename, 1, hdr, columns = columns, status = status, /silent)
if (status ne 0) then begin
	print, name, ": ERROR - could not read file '", filename, "'"
	error = -1
	return
endif
if (size(info[0], /type) ne 8) then begin
	print, name, ": WARNING -  empty extension at file '", filename, "'"
	error = 0
	return
endif
if (n_elements(starno) eq 0) then starno = 0
if (n_elements(colors) ne 3) then colors = [0, 1, 2]
pos = string(info.xc, format="(F10.4)")+","+string(info.yc, format = "(F10.4)")
;; pos = pos[sort(pos)]
mystarpos = pos[starno]
ngood = 0
;; print, name, ": starpos = ", mystarpos
boundary0 = min(boundary)
boundary1 = max(boundary)
index = where(pos eq mystarpos and info.inboundary eq boundary0 and info.outboundary eq boundary1, ngood)
if (ngood eq 0) then begin
	print, name, ": ERROR - could not find any information for star = ", starno, ", boundary = ", boundary
	error = -2
	return
endif
if (ngood gt 1) then begin
	if (keyword_set(verbose)) then $
	print, name, ": WARNING - more than one match found for star = ", starno, ", boundary = ", boundary $
	else $
	print, format = "('W', $)"
endif

index = index[0]
;; PSF_CONVOLUTION_TYPE conv_types[N_PSF_REPORT_TYPE] = {CENTRAL_AND_WINGS_PSF, CENTRAL_PSF, RADIAL_PSF}; 

if (keyword_set(pixelized)) then begin

	starinfo = mrdfits(filename, 1, hdr, columns = columns2, rows = [index], status = status, /silent)
	image = get_image_pointer_array_from_starinfo(starinfo = starinfo, error = error)
	starinfo = -1 ;; freeing the memory
	if (error ne 0) then begin
		print, name, ": ERROR - could not create image pointer array from starinfo"
		return
	endif
	if (dogluedprof and not keyword_set(azimuthal)) then begin
		pndex = 0
		graph_pixel_profile, pndex = pndex, image = image, $
		xlog = keyword_set(xlog), ylog = keyword_set(ylog), xrange = xrange, yrange = yrange, $
		arcsec = keyword_set(arcsec), sectional = keyword_set(sectional), colors = colors, psyms = psyms, thicks = thicks, error = error
	endif
	if (doklprof) then begin
		pndex = 1
		graph_pixel_profile, pndex = pndex, image = image, $
		xlog = keyword_set(xlog), ylog = keyword_set(ylog), xrange = xrange, yrange = yrange, $
		arcsec = keyword_set(arcsec), sectional = keyword_set(sectional), colors = colors, psyms = psyms, thicks = thicks, error = error
	endif
	if (doextprof and not keyword_set(azimuthal)) then begin
		pndex = 2
		graph_pixel_profile, pndex = pndex, image = image, $
		xlog = keyword_set(xlog), ylog = keyword_set(ylog), xrange = xrange, yrange = yrange, $
		arcsec = keyword_set(arcsec), sectional = keyword_set(sectional), colors = colors, psyms = psyms, thicks = thicks, error = error
	endif

endif

if (keyword_set(azimuthal)) then begin

	starinfo = info[index]
	rad = starinfo.rad
	nrad = starinfo.nrad
	average = starinfo.average

	if (dogluedprof) then begin
		pndex = 0
		graph_radial_profile, pndex = pndex, rad = rad, nrad = nrad, average = average, $
		xlog = keyword_set(xlog), ylog = keyword_set(ylog), xrange = xrange, yrange = yrange, $
		arcsec = keyword_set(arcsec), colors = colors, linestyles = linestyles, thicks = thicks, error = error
	endif
	if (doklprof) then begin
		pndex = 1
		graph_radial_profile, pndex = pndex, rad = rad, nrad = nrad, average = average, $
		xlog = keyword_set(xlog), ylog = keyword_set(ylog), xrange = xrange, yrange = yrange, $
		arcsec = keyword_set(arcsec), colors = colors, linestyles = linestyles, thicks = thicks, error = error
	endif
	if (doextprof) then begin
		pndex = 2
		graph_radial_profile, pndex = pndex, rad = rad, nrad = nrad, average = average, $
		xlog = keyword_set(xlog), ylog = keyword_set(ylog), xrange = xrange, yrange = yrange, $
		arcsec = keyword_set(arcsec), colors = colors, linestyles = linestyles, thicks = thicks, error = error
	endif

endif

plot, [0], [0], xrange = xrange, yrange = yrange, $
	xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
	title = title, $
	xstyle = 1, ystyle = 1, /nodata

create_annotation_from_filename, filename, run_str = run_str, field_str = field_str, cband_str = cband_str, $
	bannonate = keyword_set(bannonate)
plot, [0], [0], xrange = [0.0, 1.0], yrange = [0.0, 1.0], xstyle = 5, ystyle = 5, /nodata
xyouts, 0.6, 0.85, run_str
xyouts, 0.6, 0.75, field_str
xyouts, 0.6, 0.65, cband_str

return
end


pro graph_psf_image, pndex = pndex, image = image, rmax = rmax, title = title, $
	min = min, max = max, logscale = logscale, arcsec = arcsec, colors = colors, error = error

name = "graph_psf_image"
;; nrowcol = max([nrow, ncol])
;; xrange = [-nrowcol / 2.0, nrowcol / 2.0]
;; yrange = xrange

xrange = [-abs(rmax), abs(rmax)]
if (keyword_set(arcsec)) then xrange = xrange / !PIXELSIZE
yrange = xrange

plot, [0], [0], xrange = xrange, yrange = yrange, xstyle = 5, ystyle = 5, /nodata
dims = size(image, /dimensions)
nrow0 = dims[0]
ncol0 = dims[1]
;; determining the x and y values
xcorners = [-nrow0 / 2.0, nrow0 / 2.0, -nrow0 / 2.0, nrow0 / 2.0]
ycorners = [-ncol0 / 2.0, -ncol0 / 2.0, ncol0 / 2.0, ncol0 / 2.0]

d_coords = convert_coord(xcorners, ycorners, /data, /to_device)
x0 = d_coords[0, 0] / !d.X_PX_CM
y0 = d_coords[1, 0] / !d.Y_PX_CM
lx = (d_coords[0, 1] - d_coords[0, 0]) / !d.X_PX_CM
ly = (d_coords[1, 2] - d_coords[1, 1]) / !d.Y_PX_CM

if (keyword_set(debug)) then begin
	print, name, ", nrow  = ", nrow
	print, name, ", ncol  = ", ncol
	print, name, ", nrow0 = ", nrow0
	print, name, ", ncol0 = ", ncol0
	print, name, ", xcorners  = ", xcorners
	print, name, ", ycorners  = ", ycorners
	print, name, ", device xc = ", d_coords[0, *]
	print, name, ", device yc = ", d_coords[1, *]
	print, name, ", !d.x_px_cm = ", !d.x_px_cm
	print, name, ", !d.y_px_cm = ", !d.y_px_cm
	print, name, ", x0 = ", x0, ", y0 = ", y0
	print, name, ", lx = ", lx, ", ly = ", ly
endif

if (keyword_set(logscale)) then begin

	positive_index = where(image gt 0.0)
	negative_index = where(image le 0.0)
	if (n_elements(min) ne 0) then alog_min = alog10(min) else alog_min = alog10(min(image[positive_index]))
	if (n_elements(max) ne 0) then alog_max = alog10(max) else alog_max = alog10(max(image[positive_index]))
	if (keyword_set(debug)) then begin
		print, name, ", min(***) = ", alog_min, ", max(***) = ", alog_max
		print, name, ", min(psf) = ", alog10(min(image[positive_index])), ", max(psf) = ", alog10(max(image[positive_index]))
	endif
	log_image = dblarr(size(image, /dimensions))
	log_image[positive_index] = alog10(image[positive_index])
	log_image[negative_index] = alog_min - 10.0
	;; normalize image values
	bmage = bytscl(log_image, min = alog_min, max = alog_max) 
	log_image = [-1]

endif else begin

	if (keyword_set(debug)) then begin
		print, name, ", min(***) = ", min, ", max(***) = ", max
		print, name, ", min(psf) = ", min(image), ", max(psf) = ", max(image)
	endif
	;; normalize image values
	bmage = bytscl(image, min = min, max = max) 


endelse

tv, bmage, x0, y0, xsize = lx, ysize = ly, /centimeter 

return
end

pro normalize, data0 = data0, data1 = data1, data2 = data2, bzero = bzero, bscale = bscale
data0 = (data0 - bzero[0]) / bscale[0]
data1 = (data1 - bzero[1]) / bscale[1]
data2 = (data2 - bzero[2]) / bscale[2]
sum0 = total(data0)
sum1 = total(data1)
sum2 = total(data2)
data0 = data0 / sum0
data1 = data1 / sum1
data2 = data2 / sum2

new_bzero = max(bzero)
new_bscale = max(bscale)

data0 = data0 * new_bscale + new_bzero
data1 = data1 * new_bscale + new_bzero
data2 = data2 * new_bscale + new_bzero

return
end

pro draw_psf_image, filename, starno = starno, boundary = boundary, rmax = rmax,  title = title, $ 
	klprof = klprof, extprof = extprof, gluedprof = gluedprof, arcsec = arcsec, logscale = logscale, $
	error = error, debug = debug, verbose = verbose, logfile = logfile
name = "draw_psf_image"

error = 0
if (keyword_set(klprof) + keyword_set(gluedprof) + keyword_set(extprof) eq 0 and keyword_set(verbose) ) then begin
	print, name, ": WARNING - at least one PSF profiles should be drawn"
endif
if (n_elements(boundary) ne 2) then begin	
	print, name, ": ERROR - both boundaries should be provided"
	error = -2
	return
endif

status = 0
columns = ["XC", "YC", "INBOUNDARY", "OUTBOUNDARY"]
columns2 = ["RROWS", "RNROW", "RNCOL", "BZERO", "BSCALE", $
	"RROWS2", "RNROW2", "RNCOL2", $
	"RROWS3", "RNROW3", "RNCOL3"]
info = mrdfits(filename, 1, hdr, columns = columns, status = status, /silent)
if (status ne 0) then begin
	print, name, ": ERROR - could not read file '", filename, "'"
	error = -1
	return
endif
info_type = size(info[0], /type)
if (info_type ne 8) then begin 
	print, name, ": WARNING - empty extension at file '", filename, "'"
	error = 0
	return
endif
if (n_elements(starno) eq 0) then starno = 0
if (n_elements(colors) ne 3) then colors = ["BLUE", "RED", "GREEN"]
xc = info.xc
yc = info.yc
inboundary = info.inboundary
outboundary = info.outboundary
boundary0 = min(boundary)
boundary1 = max(boundary)
pos = string(xc, format="(F10.4)")+","+string(yc, format = "(F10.4)")
mystarpos = pos[starno]
ngood = 0
index = where(pos eq mystarpos and inboundary eq boundary0 and outboundary eq boundary1, ngood)
if (ngood eq 0) then begin
	print, name, ": ERROR - could not find any information for star = ", starno, ", boundary = ", boundary, " in file ", filename
	error = -2
	return
endif
if (ngood ne 1) then begin
	if (keyword_set(verbose)) then $
		print, name, ": WARNING - more than one match found for star = ", starno, $
		", boundary = ", boundary, ", ngood = ", ngood $
	else $
		print, format = "('W', $)"	
endif

index = index[0]
starinfo = mrdfits(filename, 1, hdr, columns = columns2, rows = [index], status = status, /silent)
;; starinfo = info[index]

bzero_array = starinfo.bzero
bscale_array = starinfo.bscale

rrows0 = double((starinfo.rrows))
nrow0 = starinfo.rnrow
ncol0 = starinfo.rncol
t0 = max(rrows0)
rrows0 = rrows0 / t0

rrows1 = double((starinfo.rrows2))
nrow1 = starinfo.rnrow2
ncol1 = starinfo.rncol2
t1 = max(rrows1)
rrows1 = rrows1 / t1

rrows2 = double((starinfo.rrows3))
nrow2 = starinfo.rnrow3
ncol2 = starinfo.rncol3
t2 = max(rrows2)
rrows2 = rrows2 / t2

;; freeing memory
starinfo = -1

;; normalize, data0 = rrows0, data1 = rrows1, data2 = rrows2, $
;;  	bzero = bzero_array, bscale = bscale_array
if (keyword_set(logscale)) then begin
	all_rows = [rrows0, rrows1, rrows2]
	positive_index = where(all_rows gt 0.0)
	min_psf = min(all_rows[positive_index])
	max_psf = max(all_rows[positive_index])
	all_rows = [-1]
	positive_index = [-1]
endif else begin
	min_psf = percentiles([rrows0, rrows1, rrows2], value = 0.01)
	max_psf = percentiles([rrows0, rrows1, rrows2], value = 0.98)
endelse

nrow = max([nrow0, nrow1, nrow2])
ncol = max([ncol0, ncol1, ncol2])

if (keyword_set(debug)) then $
	print, name, ": min(psf) = ", min_psf, ", max(psf) = ", max_psf

;; PSF_CONVOLUTION_TYPE conv_types[N_PSF_REPORT_TYPE] = {CENTRAL_AND_WINGS_PSF, CENTRAL_PSF, RADIAL_PSF}; 
if (keyword_set(extprof)) then begin
	pndex = 2
	bzero = bzero_array[pndex]	
	bscale = bscale_array[pndex]
	;; image = ((1.0D0 * reform(uint(rrows2), nrow2, ncol2)) - bzero) / bscale
	image = reform(rrows2, [nrow2, ncol2])
	graph_psf_image, pndex = pndex, image = image, rmax = rmax, title = title, $
	arcsec = keyword_set(arcsec), logscale = keyword_set(logscale), min = min_psf, max = max_psf, colors = colors, error = error
	multiplot
endif
if (keyword_set(klprof)) then begin
	pndex = 1
	bzero = bzero_array[pndex]	
	bscale = bscale_array[pndex]
	;; image = ((1.0D0 * reform(uint(rrows1), nrow1, ncol1)) - bzero) / bscale
	image = reform(rrows1, [nrow1, ncol1])
	graph_psf_image, pndex = pndex, image = image, rmax = rmax, title = title, $
	arcsec = keyword_set(arcsec), logscale = keyword_set(logscale), min = min_psf, max = max_psf, colors = colors, error = error
	multiplot
endif
if (keyword_set(gluedprof)) then begin
	pndex = 0
	bzero = bzero_array[pndex]	
	bscale = bscale_array[pndex]
	;; image = ((1.0D0 * reform(uint(rrows0), nrow0, ncol0)) - bzero) / bscale
	image = reform(rrows0, [nrow0, ncol0])
	graph_psf_image, pndex = pndex, image = image, rmax = rmax, title = title, $
	arcsec = keyword_set(arcsec), logscale = keyword_set(logscale), min = min_psf, max = max_psf, colors = colors, error = error
	multiplot
endif
return
end

pro table_draw_psf_image, dir = dir, epsfile = epsfile, mtitle = mtitle, $
	camcol_list = camcol_list, band_list = band_list, run_list = run_list, rerun = rerun, $
	xrange = xrange, yrange = yrange, xlog = xlog, ylog = ylog, nprof = nprof, $
	boundary = boundary, gluedprof = gluedprof, klprof = klprof, extprof = extprof, $
	arcsec = arcsec, doprofile = doprofile, pixelized = pixelized, azimuthal = azimuthal, sectional = sectional, logscale = logscale, $
	seed = seed, debug = debug, verbose = verbose, logfile = logfile

name = "table_draw_psf_image"
if (n_elements(dir) eq 0) then begin
	dir = "./single-galaxy/images/psf-analysis"
	print, name, ": setting (dir) to default value = ", dir
endif
if (n_elements(camcol_list) eq 0) then begin
	camcol_list = [1, 2, 3, 4]
	print, name, ": setting (camcol_list) to default value = ", camcol_list
endif
if (n_elements(band_list) eq 0) then begin
	band_list = ["r", "g", "i"]
	print, name, ": setting (band_list) to default value = ", band_list
endif
if (n_elements(run_list) eq 0) then begin 
	run_list = [4797]
	print, name, ": setting (run_list) to default value = ", run_list
endif
if (n_elements(rerun) eq 0) then begin
	rerun='301'
	print, name, ": setting (rerun) to default value = ", rerun
endif
if (n_elements(nprof) eq 0) then nprof = 10
if (nprof le 0) then begin
	print, name, ": ERROR - cannot draw (nprof = ", nprof, ") profiles"
	error = -2
	return
endif

lower_size_limit = 2L ^ 21 ;; 2 MB the size to tell apart the psfR files containing PSF images

lower_size_limit = 2L ;; 2 MB the size to tell apart the psfR files containing PSF images

nrad = 25
nfile = 300

ncamcol = n_elements(camcol_list)
nband = n_elements(band_list)
nrun = n_elements(run_list)

if (n_elements(seed) eq 0) then seed = 0;

n1 = nband
n2prime = keyword_set(doprofile) + keyword_set(klprof) + keyword_set(gluedprof) + keyword_set(extprof)

num_files = replicate(0, nband * ncamcol * nrun)
num_files = reform(num_files, [nband, ncamcol * nrun])
num_files_band = replicate(0L, nband)
;; counting the available files
for iband = 0, nband - 1L, 1L do begin
itable=0
num_files_band[iband] = 0
for icamcol = 0, ncamcol - 1L, 1L do begin
for irun = 0, nrun - 1L, 1L do begin
	band = band_list[iband]
	camcol = camcol_list[icamcol]
	run = run_list[irun]
	band_str = band
	camcol_str = string(camcol, "(I1.1)")
	mfile = 0
	prefix = "psfR"
	title = "("+band_str+camcol_str+")"
	file_list_rc = make_list_psf_files(dir = dir, prefix = prefix, band = band, camcol = camcol, count = mfile, rerun = rerun, run = run, lsize = lower_size_limit)
	num_files[iband, itable] = mfile
	num_files_band[iband] += mfile
	itable++
endfor
endfor
endfor
file_list_bc = 0

if (nprof gt max(num_files_band)) then begin
	print, name, ": WARNING - nprof corrected to (", max(num_files_band), ") from (", nprof, ")"
	nprof = max(num_files_band)
endif
n2 = n2prime * nprof

xtickformat = "(i7)"
xtitle="inner radius of boundary region"
if keyword_set(arcsec) then xtitle = xtitle + " (arcsec)" else xtitle = xtitle + " (pixel)"
if (n_elements(logfile) ne 0) then begin
	get_lun, ulogfile
	openu, ulogfile, logfile, /append
	printf, ulogfile, "==========================================================="
	printf, ulogfile, name
	printf, ulogfile, systime()
	printf, ulogfile, " "
endif else begin
	ulogfile = !NULL
endelse

DEFSYSV, '!MY_XSIZE', 16.7
DEFSYSV, '!MY_YSIZE', 10.3
DEFSYSV, '!MY_XYRATIO', 1.00

DEFSYSV, '!MY_XGAP', 0.001 / 4
DEFSYSV, '!MY_YGAP', 0.001
DEFSYSV, '!MY_FONT_SIZE', 11


olddevice = !d.name ; store the current device name in a variable
set_plot, 'PS' ; switch to the postscript device
Device, DECOMPOSED=decomposed, COLOR=1, BITS_PER_PIXEL=8
device, /encapsulated, file=epsfile, font_size = !my_font_size ; specify some details, give the file a name

!Y.Margin = [3, 1]
!Y.OMargin = !Y.Margin

;; size of the region
lygap = 0.001 * 16.7 / 4
ymargin_size = (!Y.Margin[0] + !Y.Margin[1]) * !D.Y_CH_SIZE/ !D.Y_PX_CM 
xmargin_size = (!X.Margin[0] + !X.Margin[1]) * !D.X_CH_SIZE/ !D.X_PX_CM 
xsize = !my_xsize
ysize = ((!my_xsize * (1.0 - n1 * !my_xgap)  - xmargin_size) * n2) / (n1 * !my_xyratio) + lygap * (n2) + ymargin_size
!my_ygap = lygap / ysize
!my_xgap = lygap / xsize

print, name, ": setting up graphic device, xsize = ", xsize, ", ysize = ", ysize, ", [n1, n2] = ", n1, n2
print, name, ": writing to file = ", epsfile
loadct, 39
device, xsize=xsize, ysize=ysize
xformat = "(I10)"
yformat = "(E10.1)"

;; setting the x-title
xtitle = "r (pixels, distance from center of the PSF)"
if (keyword_set(arcsec)) then xtitle = "r (arcsec, distance from center of the PSF field)"

multiplot, /reset
multiplot, [n1, n2], $
	xgap = !my_xgap, ygap = !my_ygap, $
	xtickformat = xformat, ytickformat = yformat, $
	/rowmajor, /square, $
	mtitle = mtitle, mTitOffset = 0.0, mxtitle = xtitle

;; logfile
if (n_elements(ulogfile) ne 0) then begin
	printf, ulogfile, "*** in-boundary = ", boundary
	printf, ulogfile, " "
endif

for iband = 0, nband - 1L, 1L do begin
print, format = "('/b',$)" 
band = band_list[iband]
;; logfile
if (n_elements(ulogfile) ne 0) then printf, ulogfile, "** band = ", band

rndex = random_indices(num_files_band[iband], min([nprof, num_files_band[iband]]), seed = seed, error = error)
rndex = rndex[sort(rndex)]
all_title = ""
previous_mfiles = 0
itable = 0
ibfile = 0

for icamcol = 0, ncamcol - 1L, 1L do begin

camcol = camcol_list[icamcol]
band_str = band
camcol_str = string(camcol, "(I1.1)")
	
c_title = "("+band_str+")"	
for irun = 0, nrun - 1L, 1L do begin
	
	run = run_list[irun]
	mfile_table = num_files[iband, itable]
	next_mfiles = previous_mfiles + mfile_table
	rndex_table = where(rndex ge previous_mfiles and rndex lt next_mfiles, rcount)

	if (rcount gt 0) then begin

		if (n_elements(ulogfile) ne 0) then $
		printf, ulogfile, "band = ", band, "camcol = ", camcol, ", run = ", run, ", field count = ", rcount
		mfile = 0
		prefix = "psfR"
		file_list = make_list_psf_files(dir = dir, prefix = prefix, $
		band = band, camcol = camcol, run = run, rerun = rerun, count = mfile, lsize = lower_size_limit)

		idata = 0

		if (mfile gt 0) then begin

		rfndex = random_indices(mfile, mfile, seed = seed)
		mrfile = n_elements(rfndex)
		for irfile = 0, rcount -1L, 1L do begin
			ifile = rndex[rndex_table[irfile]] - previous_mfiles
			print, format = "('.', $)"
			dooverplot = (irfile gt 0)
			file = file_list[ifile]
			error = 0
			colors = [80, 150, 250]
			thicks = [8, 5, 5]
			linestyles = [0, 0, 0]
			if (ibfile eq 0) then title = "("+band_str+")" else title = ""
			;; printing to logfile
			if (n_elements(ulogfile) ne 0) then printf, ulogfile, file
			draw_psf_image, file, boundary = boundary, $
			klprof = keyword_set(klprof), gluedprof = keyword_set(gluedprof), extprof = keyword_set(extprof), $
			rmax = max(xrange), title = title, arcsec = keyword_set(arcsec), logscale = keyword_set(logscale), $
			error = error, verbose = keyword_set(verbose), debug = keyword_set(debug), logfile = ulogfile	
			if (keyword_set(doprofile)) then begin
				draw_psf_profile, file, starno = starno, boundary = boundary, $
				klprof = keyword_set(klprof), gluedprof = keyword_set(gluedprof), extprof = keyword_set(extprof), $
				overplot = dooverplot, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
				xrange = xrange, yrange = yrange, arcsec = keyword_set(arcsec), /bannonate, seed = seed, $
				colors = colors, linestyles = linestyles, thicks = thicks, error = error, $
				pixelized = keyword_set(pixelized), azimuthal = keyword_set(azimuthal), $
				verbose = keyword_set(verbose), debug = keyword_set(debug), $
				logfile = ulogfile
				;; inserting the x-tick marks and x-title
				;; suppress y-axis but do not suppress x-axis
				plot, [0], [0], xrange = xrange, yrange = yrange, xstyle = 1, ystyle = 5, /nodata
				multiplot
			endif	
			ibfile++
		endfor

		endif
	endif
	previous_mfiles = next_mfiles
	itable++
	
endfor
endfor
print, format = "('\',$)" 
endfor

print
print, name, ": closing device"
device, /close ; close the file
set_plot, olddevice ; go back to the graphics device
command = "epstopdf "+epsfile
spawn, command
extpos = STRPOS(epsfile, '.', /REVERSE_SEARCH)
jpegfile = STRMID(epsfile, 0, extpos)+".jpg"
command = "convert " + epsfile  + " " + jpegfile
spawn, command


if (n_elements(ulogfile) ne 0) then begin
	close, ulogfile
	free_lun, ulogfile
endif

return
end



pro table_draw_psf_profle, dir = dir, epsfile = epsfile, mtitle = mtitle, $
	camcol_list = camcol_list, band_list = band_list, run_list = run_list, rerun = rerun, $
	yrange = yrange, xrange = xrange, xlog = xlog, ylog = ylog, nprof = nprof, $
	boundary = boundary, gluedprof = gluedprof, klprof = klprof, extprof = extprof, $
	arcsec = arcsec, pixelized = pixelized, azimuthal = azimuthal, sectional = sectional, lsize = lsize, $ 
	seed = seed, debug = debug, verbose = verbose, logfile = logfile

name = "table_draw_psf_profle"
if (n_elements(dir) eq 0) then begin
	dir = "./single-galaxy/images/psf-analysis"
	print, name, ": setting (dir) to default value = ", dir
endif
if (n_elements(camcol_list) eq 0) then begin
	camcol_list = [1, 2, 3, 4]
	print, name, ": setting (camcol_list) to default value = ", camcol_list
endif
if (n_elements(band_list) eq 0) then begin
	band_list = ["r", "g", "i"]
	print, name, ": setting (band_list) to default value = ", band_list
endif
if (n_elements(run_list) eq 0) then begin 
	run_list = [4797]
	print, name, ": setting (run_list) to default value = ", run_list
endif
if (n_elements(rerun) eq 0) then begin
	rerun='301'
	print, name, ": setting (rerun) to default value = ", rerun
endif
if (n_elements(nprof) eq 0) then nprof = 10
if (nprof le 0) then begin
	print, name, ": ERROR - cannot draw (nprof = ", nprof, ") profiles"
	error = -2
	return
endif

nrad = 25
nfile = 300

ncamcol = n_elements(camcol_list)
nband = n_elements(band_list)
nrun = n_elements(run_list)

DEFSYSV, '!MY_XSIZE', 21.0
DEFSYSV, '!MY_YSIZE', 10.3
DEFSYSV, '!MY_XYRATIO', 1.55

DEFSYSV, '!MY_XGAP', 0.00
DEFSYSV, '!MY_YGAP', 0.00
DEFSYSV, '!MY_FONT_SIZE', 11
if (n_elements(seed) eq 0) then seed = 0;

n1 = nband
n2 = nprof

xtickformat = "(i7)"
xtitle="inner radius of boundary region"
if keyword_set(arcsec) then xtitle = xtitle + " (arcsec)" else xtitle = xtitle + " (pixel)"
if (n_elements(logfile) ne 0) then begin
	get_lun, ulogfile
	openu, ulogfile, logfile, /append
	printf, ulogfile, "==========================================================="
	printf, ulogfile, name
	printf, ulogfile, systime()
	printf, ulogfile, " "
endif else begin
	ulogfile = !NULL
endelse


xsize= !my_xsize ;;  * n1 / 2.0 ;; 45
ysize= (!my_xsize * n2) / (n1 * !my_xyratio) ;; 45

print, name, ": setting up graphic device, xsize = ", xsize, ", ysize = ", ysize, ", [n1, n2] = ", n1, n2
print, name, ": writing to file = ", epsfile
olddevice = !d.name ; store the current device name in a variable
set_plot, 'PS' ; switch to the postscript device
Device, DECOMPOSED=decomposed, COLOR=1, BITS_PER_PIXEL=8
device, /encapsulated, file=epsfile, font_size = !my_font_size ; specify some details, give the file a name
loadct, 40
device, xsize=xsize, ysize=ysize
xformat = "(I10)"
yformat = "(E10.1)"

;; setting the x-title
xtitle = "r (pixels, distance from center of the PSF)"
if (keyword_set(arcsec)) then xtitle = "r (arcsec, distance from center of the PSF field)"

!Y.Margin = [4, 4]
!Y.OMargin = !Y.Margin
multiplot, /reset
multiplot, [n1, n2], $
	xgap = !my_xgap, ygap = !my_ygap, $
	xtickformat = xformat, ytickformat = yformat, $
	mtitle = mtitle, mTitOffset = 2.0, mxtitle = xtitle, $
	/rowmajor
num_files = replicate(0, nband * ncamcol * nrun)
num_files = reform(num_files, [nband, ncamcol * nrun])
num_files_band = replicate(0L, nband)

for iband = 0, nband - 1L, 1L do begin
itable=0
num_files_band[iband] = 0
for icamcol = 0, ncamcol - 1L, 1L do begin
for irun = 0, nrun - 1L, 1L do begin
	band = band_list[iband]
	camcol = camcol_list[icamcol]
	run = run_list[irun]
	band_str = band
	camcol_str = string(camcol, "(I1.1)")
	mfile = 0
	prefix = "psfR"
	title = "("+band_str+camcol_str+")"
	file_list_rc = make_list_psf_files(dir = dir, prefix = prefix, band = band, camcol = camcol, count = mfile, rerun = rerun, run = run, lsize = lsize)
	num_files[iband, itable] = mfile
	num_files_band[iband] += mfile
	itable++
endfor
endfor
endfor
file_list_bc = 0


if (nprof gt max(num_files_band)) then begin
	print, name, ": WARNING - nprof corrected to (", max(num_files_band), ") from (", nprof, ")"
	nprof = max(num_files_band)
endif

;; logfile
if (n_elements(ulogfile) ne 0) then begin
	printf, ulogfile, "*** in-boundary = ", boundary
	printf, ulogfile, " "
endif

for iband = 0, nband - 1L, 1L do begin
print, format = "('/b',$)" 
band = band_list[iband]
;; logfile
if (n_elements(ulogfile) ne 0) then printf, ulogfile, "** band = ", band

nselect = min([nprof, num_files_band[iband]])
error = 0
rndex = random_indices(num_files_band[iband], nselect, seed = seed, error = error)
if (error ne 0) then begin
	print, name, ": ERROR - could not get a list of random indices for (iband = ", iband, "), num files = ", num_files_band[iband], ", nprof = ", nprof
	return
endif

rndex = rndex[sort(rndex)]
all_title = ""
previous_mfiles = 0
itable = 0
ibfile = 0

for icamcol = 0, ncamcol - 1L, 1L do begin

camcol = camcol_list[icamcol]
band_str = band
camcol_str = string(camcol, "(I1.1)")
	
c_title = "("+band_str+")"	
for irun = 0, nrun - 1L, 1L do begin
	
	run = run_list[irun]
	mfile_table = num_files[iband, itable]
	next_mfiles = previous_mfiles + mfile_table
	rndex_table = where(rndex ge previous_mfiles and rndex lt next_mfiles, rcount)

	if (rcount gt 0) then begin

		if (n_elements(ulogfile) ne 0) then $
		printf, ulogfile, "band = ", band, "camcol = ", camcol, ", run = ", run, ", field count = ", rcount
		mfile = 0
		prefix = "psfR"
		if (ibfile eq 0) then title = "("+band_str+")" else title = ""
		file_list = make_list_psf_files(dir = dir, prefix = prefix, $
		band = band, camcol = camcol, run = run, rerun = rerun, count = mfile, lsize = lsize)

		idata = 0

		if (mfile gt 0) then begin

		rfndex = random_indices(mfile, mfile, seed = seed)
		mrfile = n_elements(rfndex)
		for irfile = 0, rcount -1L, 1L do begin
			ifile = rndex[rndex_table[irfile]] - previous_mfiles
			print, format = "('.', $)"
			dooverplot = (irfile gt 0)
			file = file_list[ifile]
			error = 0
			colors = [80, 150, 250]
			thicks = [8, 5, 5]
			linestyles = [0, 0, 0]
			;; printing to logfile
			if (n_elements(ulogfile) ne 0) then printf, ulogfile, file
			draw_psf_profile, file, starno = starno, boundary = boundary, $
			klprof = keyword_set(klprof), gluedprof = keyword_set(gluedprof), extprof = keyword_set(extprof), $
			title = title, overplot = dooverplot, xlog = keyword_set(xlog), ylog = keyword_set(ylog), $
			xrange = xrange, yrange = yrange, arcsec = keyword_set(arcsec), $
			pixelized = keyword_set(pixelized), azimuthal = keyword_set(azimuthal), sectional = keyword_set(sectional), $
			seed = seed, colors = colors, linestyles = linestyles, thicks = thicks, error = error, $
			verbose = keyword_set(verbose), debug = keyword_set(debug), logfile = ulogfile
			ibfile++
			multiplot
		endfor

		endif
	endif
	previous_mfiles = next_mfiles
	itable++
	
endfor
endfor
print, format = "('\',$)" 
endfor

print
print, name, ": closing device"
device, /close ; close the file
set_plot, olddevice ; go back to the graphics device
command = "epstopdf "+epsfile
spawn, command
extpos = STRPOS(epsfile, '.', /REVERSE_SEARCH)
jpegfile = STRMID(epsfile, 0, extpos)+".jpg"
command = "convert " + epsfile + " " + jpegfile
spawn, command


if (n_elements(ulogfile) ne 0) then begin
	close, ulogfile
	free_lun, ulogfile
endif

return
end







pro extract_descriptive_curve, curve_array = curve_array, nrad_array = nrad_array, ndata = ndata, $
	stats = stats, desc_array = desc_array, $
	nfile = nfile, nrun = nrun, $
	doc2w2 = doc2w2, error = error
name = "extract_descriptive_curve"

if (n_elements(stats) eq 0) then stats = [0.5]
nstat = n_elements(stats)

ncurves = n_elements(curve_array)
if ((ncurves mod nfile * nrun) ne 0) then begin
	print, name, ": ERROR - size of (curve array) should be divisble by [nrun, nfile]"
	error = -1
	return
endif else begin
	nrad = ncurves / (nfile * nrun)
	curve_array = reform(curve_array, [nrun, nfile, nrad])	
endelse

nnrad = n_elements(nrad_array)
if (nnrad ne nfile * nrun) then begin
	print, name, ": ERROR - size of (nrad array) should be [nrun, nfile] array"
	error = -1
	return
endif else begin
	nrad_array = reform(nrad_array, [nrun, nfile])
endelse

if (n_elements(ndata) ne nrun) then begin
	print, name, ": ERROR - (ndata) should be of size [nrun]"
	error = -1
	return
endif

nbad_data = 0
bad_ndata = where(ndata lt 0 or ndata gt nfile, nbad_ndata)
if (nbad_ndata gt 0) then begin
	print, name, ": ERROR - (ndata) elements should be greater than 0 but less than or equal to (nfile)"
	error = -1
	return
endif
ndesc = n_elements(desc_array)
if (ndesc ne (nstat * nrad)) then begin
	print, name, ": ERROR - (desc array) should be an [nstat, nrad] array"
	error = -1
	return
endif else begin 
	desc_array = reform(desc_array, [nstat, nrad])
endelse

ngood = 0
gndex = where(nrad_array gt 0, ngood)
if (ngood eq 0) then begin
	print, name, ": WARNING - no good curve was found" 
	error = 0
	return
endif

choice_array = intArr(nrun, nfile)
sndex = lindgen(nstat)
desc_stats = dblArr(nstat)
for jndex = 0, nrad - 1L, 1L do begin

	total_jcount = 0
	for irun = 0, nrun - 1L, 1L do begin

	jcount = 0
	rndex = where(nrad_array[irun, *] gt jndex, jcount)
	total_jcount += jcount
	if (jcount gt 0) then begin
		error = 0
		choice_array[irun, *] = 0
		choice_array[irun, rndex] = 1
	endif
	
	endfor ;; run loop
	if (total_jcount gt 0) then begin
		extract_descriptive, curve_array[*, *, jndex], choice_array = choice_array, desc_stats = desc_stats, stats = stats, $
		doc2w2 = keyword_set(doc2w2), error = error
		desc_array[sndex, jndex] = desc_stats[sndex]
	endif


endfor

error = 0
return
end

	
pro bundle_descriptive_curve, curve_array = curve_array, nrad_array = nrad_array, ndata_array = ndata_array, $
	stats = stats, desc_array = desc_array, $
	ncamcol = ncamcol, nrun = nrun, nfile = nfile, $
	doc2w2 = doc2w2, seed = seed, error = error
name = "bundle_descriptive_curve"

if (n_elements(ncamcol) eq 0) then begin
	print, name, ": ERROR - (ncamcol) should be passed"
	error = -1
	return
endif
if (n_elements(nrun) eq 0) then begin
	print, name, ": ERROR - (nrun) should be passed"
	error = -1
	return
endif
if (n_elements(nfile) eq 0) then begin
	print, name, ": ERROR - (nfile) should be passed"
	error = -1
	return
endif
if (n_elements(stats) eq 0) then stats = [0.5]
nstat = n_elements(stats)

ncurve = size(curve_array, /N_ELEMENTS)
if ((ncurve mod (ncamcol * nrun * nfile)) ne 0) then begin
	print, name, ": ERROR - (curve array) should be a [ncamcol, nfile, nrad] array"
	error = -1
	return
endif else begin
	nrad = ncurve / (ncamcol * nfile * nrun)	
	curve_array = reform(curve_array, [ncamcol, nrun, nfile, nrad])
endelse

nnrad = size(nrad_array, /N_ELEMENTS)
if (nnrad ne (ncamcol * nrun * nfile)) then begin
	print, name, ": ERROR - (nrad array) should be an [ncamcol, nrun, nfile] array"
	error = -1
	return
endif else begin
	nrad_array = reform(nrad_array, [ncamcol, nrun, nfile])
endelse

nndata = size(ndata_array, /N_ELEMENTS)
if (nndata ne ncamcol * nrun) then begin
	print, name, ": ERROR - (ndata array) should be an [ncamcol, nrun] array"
	error = -1
	return
endif else begin
	ndata_array = reform(ndata_array, [ncamcol, nrun])
endelse

ndesc = size(desc_array, /N_ELEMENTS)
if (ndesc ne nstat * nrad) then begin
	print, name, ": ERROR - (desc array) should be an [nstat, nrad] array"
	error = -1
	return
endif else begin
	desc_array = reform(desc_array, [nstat, nrad])
endelse

ngood = 0
gndex = where(ndata_array gt 0, ngood)
if (ngood eq 0) then begin
	print, name, ": WARNING - no good curve was found" 
	error = 0
	return
endif

max_sim = max(ndata_array[*, *])
choice_array = replicate(0, ncamcol * nrun * max_sim)
choice_array = reform(choice_array, [ncamcol, nrun, max_sim])
choice_array[*, *] = 0

for icamcol = 0, ncamcol - 1L, 1L do begin
for irun = 0, nrun - 1L, 1L do begin

	nsim = min(ndata_array[*, irun])

	ndata_camcol_run = ndata_array[icamcol, irun]
	;; rndex = choose_random_indices(ndata_camcol, nrandom = nsim)	
	if (ndata_camcol_run gt 0 and nsim gt 0) then begin
		rndex = random_indices(ndata_camcol_run, nsim, seed = seed)
		choice_array[icamcol, irun, rndex] = 1
	endif

endfor
endfor
	
desc_stats = dblArr(nstat)
sndex = lindgen(nstat)
for jndex = 0, nrad - 1L, 1L do begin
	;; print, name, ": jndex = ", jndex
	rndex = where(nrad_array gt jndex)
	error = 0
	;; print, name, ": curve_array = ", size(curve_array, /DIMENSIONS)
	extract_descriptive, curve_array[*, *, *, jndex], choice_array = choice_array, desc_stats = desc_stats, $
	stats = stats, doc2w2 = keyword_set(doc2w2), error = error
	desc_array[sndex, jndex] = desc_stats[sndex]
endfor

error = 0
return
end

pro extract_descriptive, data_array, choice_array = choice_array, desc_stats = desc_stats, stats = stats, doc2w2 = doc2w2, $
	error = error, debug = debug, verbose = verbose

name = "extract_descriptive"
if (n_elements(data_array) eq 0) then begin
	print, name, ": ERROR - empty data_array"
	error = -1
	return
endif
dodebug = keyword_set(debug)
doverbose = keyword_set(verbose)

if (n_elements(stats) eq 0) then stats = [0.5]
nstat = n_elements(stats)

if (n_elements(desc_stats) ne nstat) then begin
	print, name, ": ERROR - (desc_stats) should be an [nstat] array"
	error = -1
	return
endif

ndata = n_elements(data_array)
if (n_elements(choice_array) eq 0) then begin
	choice_array = replicate(1, ndata)
endif
nchoice = 0
index = where(choice_array eq 1, nchoice)
if (nchoice eq 0) then begin
	print, name, ": ERROR - all elements of choice array are '0'"
	error = -1
	return
endif
;; print, name, ": nchoice = ", nchoice
ngood = 0
goodindex = where(index lt ndata, ngood)
if (ngood eq 0) then begin
	print, name, ": ERROR - all (choice_array) elements are beyond (data_array) size"
	error = -1
	return
endif
;; print, name, ": ngood = ", ngood
;; print, name, ": ndata = ", ndata
;; print, name, ": good index = ", min(index[goodindex]), max(index[goodindex])

index = index[goodindex]
range = [min(data_array[index]), max(data_array[index])]
if (keyword_set(doc2w2)) then begin
	desc_stats = quantile(stats, data_array[index], range=range)
	error = 0
	return
endif else begin
	xbar = mean(data_array[index])	
	s = sqrt(variance(data_array[index]))
	df = n_elements(index) - 1
	if (df gt 0) then begin
		for istat = 0L, nstat - 1L, 1L do begin	
			p = 1.0 - stats[istat]
			t = t_cvf(p, df)
			desc_stats[istat] = xbar + s * t
		endfor
		error = 0
	endif else  begin
		if (dodebug or doverbose) then print, name, ": ERROR - positive (df) expected, received df = ", df
		error = -1
	endelse
	return
endelse

error = -2	
return
end

