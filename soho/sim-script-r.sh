#!/bin/bash

export band="r"
export b=2
export camcol=4
export sky1=160
export sky2=160

export f1=9999
export f2=9999
export command="nohup do-sim-w21-w301-$f1-$f2 $b $sky1 $sky2 testff2.par ./simfit/sim-sdss-w21-w301-$f1-$f2-$band$camcol.fits ./simfit/sim-poisson-sdss-w21-w301-$f1-$f2-$band$camcol.fits > nohup.do-sim-w21-w301-$f1-$f2-$band$camcol.out"
eval $command

export f1=999
export f2=999
export command="nohup do-sim-w21-w301-$f1-$f2 $b $sky1 $sky2 testff2.par ./simfit/sim-sdss-w21-w301-$f1-$f2-$band$camcol.fits ./simfit/sim-poisson-sdss-w21-w301-$f1-$f2-$band$camcol.fits > nohup.do-sim-w21-w301-$f1-$f2-$band$camcol.out"
eval $command

export f1=99
export f2=99
export command="nohup do-sim-w21-w301-$f1-$f2 $b $sky1 $sky2 testff2.par ./simfit/sim-sdss-w21-w301-$f1-$f2-$band$camcol.fits ./simfit/sim-poisson-sdss-w21-w301-$f1-$f2-$band$camcol.fits > nohup.do-sim-w21-w301-$f1-$f2-$band$camcol.out"
eval $command

export f1=90
export f2=90 
export command="nohup do-sim-w21-w301-$f1-$f2 $b $sky1 $sky2 testff2.par ./simfit/sim-sdss-w21-w301-$f1-$f2-$band$camcol.fits ./simfit/sim-poisson-sdss-w21-w301-$f1-$f2-$band$camcol.fits > nohup.do-sim-w21-w301-$f1-$f2-$band$camcol.out"
eval $command

export f1=80
export f2=80 
export command="nohup do-sim-w21-w301-$f1-$f2 $b $sky1 $sky2 testff2.par ./simfit/sim-sdss-w21-w301-$f1-$f2-$band$camcol.fits ./simfit/sim-poisson-sdss-w21-w301-$f1-$f2-$band$camcol.fits > nohup.do-sim-w21-w301-$f1-$f2-$band$camcol.out"
eval $command

export f1=70
export f2=70 
export command="nohup do-sim-w21-w301-$f1-$f2 $b $sky1 $sky2 testff2.par ./simfit/sim-sdss-w21-w301-$f1-$f2-$band$camcol.fits ./simfit/sim-poisson-sdss-w21-w301-$f1-$f2-$band$camcol.fits > nohup.do-sim-w21-w301-$f1-$f2-$band$camcol.out"
eval $command

export f1=60
export f2=60 
export command="nohup do-sim-w21-w301-$f1-$f2 $b $sky1 $sky2 testff2.par ./simfit/sim-sdss-w21-w301-$f1-$f2-$band$camcol.fits ./simfit/sim-poisson-sdss-w21-w301-$f1-$f2-$band$camcol.fits > nohup.do-sim-w21-w301-$f1-$f2-$band$camcol.out"
eval $command

export f1=50
export f2=50 
export command="nohup do-sim-w21-w301-$f1-$f2 $b $sky1 $sky2 testff2.par ./simfit/sim-sdss-w21-w301-$f1-$f2-$band$camcol.fits ./simfit/sim-poisson-sdss-w21-w301-$f1-$f2-$band$camcol.fits > nohup.do-sim-w21-w301-$f1-$f2-$band$camcol.out"
eval $command

export f1=40
export f2=40 
export command="nohup do-sim-w21-w301-$f1-$f2 $b $sky1 $sky2 testff2.par ./simfit/sim-sdss-w21-w301-$f1-$f2-$band$camcol.fits ./simfit/sim-poisson-sdss-w21-w301-$f1-$f2-$band$camcol.fits > nohup.do-sim-w21-w301-$f1-$f2-$band$camcol.out"
eval $command

export f1=30
export f2=30 
export command="nohup do-sim-w21-w301-$f1-$f2 $b $sky1 $sky2 testff2.par ./simfit/sim-sdss-w21-w301-$f1-$f2-$band$camcol.fits ./simfit/sim-poisson-sdss-w21-w301-$f1-$f2-$band$camcol.fits > nohup.do-sim-w21-w301-$f1-$f2-$band$camcol.out"
eval $command

export f1=20
export f2=20 
export command="nohup do-sim-w21-w301-$f1-$f2 $b $sky1 $sky2 testff2.par ./simfit/sim-sdss-w21-w301-$f1-$f2-$band$camcol.fits ./simfit/sim-poisson-sdss-w21-w301-$f1-$f2-$band$camcol.fits > nohup.do-sim-w21-w301-$f1-$f2-$band$camcol.out"
eval $command

export f1=10
export f2=10 
export command="nohup do-sim-w21-w301-$f1-$f2 $b $sky1 $sky2 testff2.par ./simfit/sim-sdss-w21-w301-$f1-$f2-$band$camcol.fits ./simfit/sim-poisson-sdss-w21-w301-$f1-$f2-$band$camcol.fits > nohup.do-sim-w21-w301-$f1-$f2-$band$camcol.out"
eval $command

