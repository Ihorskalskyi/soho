#ifndef THPARSETYPES_H
#define THPARSETYPES_H
 
#include <unistd.h> 
#include <getopt.h>  
#include <stdio.h>
#include <string.h>
#include "thModelTypes.h"
#include "thObjcTypes.h"
#include "thMSkyTypes.h"
#include "thMGalaxyTypes.h"
#include "thProcTypes.h"
#include "thCTransformTypes.h"

typedef struct mgalaxy_parsed_input {
	THPIX N_SERSIC_1;
	THPIX N_SERSIC_2;
	THPIX N_SERSIC1_1;
	THPIX N_SERSIC1_2;
	THPIX N_SERSIC2_1;
	THPIX N_SERSIC2_2;

	THPIX N_SERSIC_DRAW_1;
	THPIX N_SERSIC_DRAW_2;

	THPIX N_SERSIC_DRAW1_1;
	THPIX N_SERSIC_DRAW1_2;

	THPIX N_SERSIC_DRAW2_1;
	THPIX N_SERSIC_DRAW2_2;

	THPIX N_CORESERSIC_1;
	THPIX N_CORESERSIC_2;
	THPIX N_CORESERSIC;

	THPIX DELTA_CORESERSIC_1;
	THPIX DELTA_CORESERSIC_2;
	THPIX DELTA_CORESERSIC;

	THPIX GAMMA_CORESERSIC_1;
	THPIX GAMMA_CORESERSIC_2;
	THPIX GAMMA_CORESERSIC;
	
	THPIX N_CORESERSIC_DRAW_1;
	THPIX N_CORESERSIC_DRAW_2;

	THPIX DELTA_CORESERSIC_DRAW_1;
	THPIX DELTA_CORESERSIC_DRAW_2;

	THPIX GAMMA_CORESERSIC_DRAW_1;
	THPIX GAMMA_CORESERSIC_DRAW_2;
	
	int lN_SERSIC_1;
	int lN_SERSIC_2;

	int lN_SERSIC1_1;
	int lN_SERSIC1_2;

	int lN_SERSIC2_1;
	int lN_SERSIC2_2;

	int lN_SERSIC_DRAW_1;
	int lN_SERSIC_DRAW_2;

	int lN_SERSIC_DRAW1_1;
	int lN_SERSIC_DRAW1_2;

	int lN_SERSIC_DRAW2_1;
	int lN_SERSIC_DRAW2_2;

	int lN_CORESERSIC_1;
	int lN_CORESERSIC_2;
	int lN_CORESERSIC;

	int lDELTA_CORESERSIC_1;
	int lDELTA_CORESERSIC_2;
	int lDELTA_CORESERSIC;
	
	int lGAMMA_CORESERSIC_1;
	int lGAMMA_CORESERSIC_2;
	int lGAMMA_CORESERSIC;

	int lN_CORESERSIC_DRAW_1;
	int lN_CORESERSIC_DRAW_2;

	int lDELTA_CORESERSIC_DRAW_1;
	int lDELTA_CORESERSIC_DRAW_2;
	
	int lGAMMA_CORESERSIC_DRAW_1;
	int lGAMMA_CORESERSIC_DRAW_2;

} MGALAXY_PARSED_INPUT; /* pragma SCHEMA */

typedef struct ctransform_parsed_input {
	THPIX SERSIC_INDEX_SMALL;
	THPIX CORESERSIC_INDEX_SMALL;
	THPIX SERSIC1_INDEX_SMALL;
	THPIX SERSIC2_INDEX_SMALL;
	THPIX SERSIC_INDEX_LARGE;
	THPIX CORESERSIC_INDEX_LARGE;
	THPIX SERSIC1_INDEX_LARGE;
	THPIX SERSIC2_INDEX_LARGE;
	
	int lSERSIC_INDEX_SMALL;
	int lCORESERSIC_INDEX_SMALL;
	int lSERSIC1_INDEX_SMALL;
	int lSERSIC2_INDEX_SMALL;
	int lSERSIC_INDEX_LARGE;
	int lCORESERSIC_INDEX_LARGE;
	int lSERSIC1_INDEX_LARGE;
	int lSERSIC2_INDEX_LARGE;
} CTRANSFORM_PARSED_INPUT; /*pragma SCHEMA */
	


typedef struct sim_parsed_input {
	char *setupfile; 
	int nrow; /* used for simulations */
	int ncol; /* used for simulations */
	int camcol;
	int band;
	int run;
	int rerun;
	int stripe;
	int field;
	char *objcfile;
	int readobjcfile;
	int readskyfile; /* added on Aug 11, 2015 */
	char *framefile;
	FRAMEFILES *ff;
	int smearobjc;
	float smearf;
	SKYPARS *sky;
	SKYPARS **skylist;
	CHAIN *objclist;
	int demand;
	unsigned int seed;
	/* added on April 27, 2015 */
	THPIX exp_inner_lumcut, exp_outer_lumcut;
	THPIX dev_inner_lumcut, dev_outer_lumcut;
	/* added on June 11, 2016 */
	int nstar; 
	int ngalaxy;
	int nlrg;
	float star_threshold;
	float galaxy_threshold;
	float deV_purity_threshold;
	float exp_purity_threshold;
	/* Added on August 20, 2016 */
	GC_SOURCE gcsource;
	int do_cD_classify;
	GCLASS cdclass;
	PSF_CONVOLUTION_TYPE galaxypsf;
	int fpC_do_float;
	float gaussian_noise_dn; 
} SIM_PARSED_INPUT; /* pragma IGNORE */

typedef struct mle_parsed_input {
	/* input */
	char *framefile;
	FRAMEFILES *ff;
	/* shaping the parameteres for fit */
	int dostar;
	GCLASS gclass;
	CDESCRIPTION csystem;
	int fitshape;
	int fitindex;
	int fitsize;
	int fitcenter;
	int lockcenter;
	int lockre;
	int nobjc;
	int nfit;
	/* algorithm specification */
	MEMDBLE memory;
	LINITMODE init;
	float error;
	int fit2fpCC;
	unsigned int seed;
	/* added in Sep 2016 */
	char *setupfile; 
	int nrow; /* used for simulations */
	int ncol; /* used for simulations */
	int camcol;
	int band;
	int run;
	int rerun;
	int stripe;
	int field;
	GC_SOURCE gcsource;
	int do_cD_classify;
	int force_cD_classify;
	int do_fit_sky; 
	int multistage;
	/* the following is using for passing arguments within the code and cannot be set by the user */
	GCLASS cdclass, galaxyclass;
	DUMP_WRITE_MODE dump_mode;

	/* the following is needed for mle on demand */
	CHAIN *objclist;
	SKYPARS **skylist;
	int demand;
	int initn;
	PSF_CONVOLUTION_TYPE galaxypsf;
	MGALAXY_PARSED_INPUT *mgalaxy_input;
	CTRANSFORM_PARSED_INPUT *ctransform_input;
} MLE_PARSED_INPUT; /* pragma IGNORE */

typedef struct psf_parsed_input {
	char *setupfile; 
	int camcol;
	int band;
	char bandname;
	int run;
	int rerun;
	int stripe;
	int field;

	char *objcfile;
	int readobjcfile;
	char *framefile;
	FRAMEFILES *ff;
	
	unsigned int seed;
	char *psfreportfile, *psfstatfile;
	int xcmin, xcmax, ycmin, ycmax;
	int npsfstar;
} PSF_PARSED_INPUT; /* pragma IGNORE */



SIM_PARSED_INPUT *thSimParsedInputNew(void);
void thSimParsedInputDel(SIM_PARSED_INPUT *x);

MLE_PARSED_INPUT *thMleParsedInputNew(void);
void thMleParsedInputDel(MLE_PARSED_INPUT *x);

PSF_PARSED_INPUT *thPsfParsedInputNew();
void thPsfParsedInputDel(PSF_PARSED_INPUT *x);

void delete_all_tokens(char **argv, int argc);
char **str_all_tokens(char *arg, char *delimiter, int *argc);

MGALAXY_PARSED_INPUT *thMGalaxyParsedInputNew();
void thMGalaxyParsedInputDel(MGALAXY_PARSED_INPUT *x);

CTRANSFORM_PARSED_INPUT *thCTransformParsedInputNew();
void thCTransformParsedInputDel(CTRANSFORM_PARSED_INPUT *x);

#endif
