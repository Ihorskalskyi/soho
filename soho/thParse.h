#ifndef THPARSE_H
#define THPARSE_H

#include "thParseTypes.h"
#include "thProcTypes.h"
#include "thMSky.h"

/* specific parsers */
RET_CODE simparser(int argc, char **argv, SIM_PARSED_INPUT *input);
RET_CODE mleparser(int argc, char **argv, MLE_PARSED_INPUT *input);
RET_CODE psfparser(int argc, char **argv, PSF_PARSED_INPUT *input);

/* generic parsers */
RET_CODE expparser(char *argv, WOBJC_IO **objc);
RET_CODE deVparser(char *argv, WOBJC_IO **objc);
RET_CODE starparser(char *argv, WOBJC_IO **objc);
RET_CODE skyparser(char *argv, SKYPARS **objc);
RET_CODE ffparser(char *arg, FRAMEFILES **ff);
RET_CODE mgalaxyparser(char *arg, MGALAXY_PARSED_INPUT *input);
RET_CODE ctransformparser(char *arg, CTRANSFORM_PARSED_INPUT *input);

/* parses demanded object by its code into a list of skies and objects */
RET_CODE demandparser(int demand, CHAIN *objclist_out, SKYPARS ***skylist_out, PSF_CONVOLUTION_TYPE *galaxy_psf_out, GCLASS *cdclass_out);
RET_CODE initdemandparser(int demand, CHAIN *objclist_out, SKYPARS ***skylist_out, PSF_CONVOLUTION_TYPE *galaxy_psf_out, GCLASS *cdclass_out);

#endif
