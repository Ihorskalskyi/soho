#!/bin/bash

source "./generate_temporary_file.bash"
source "./run_commands_in_file.bash"

name="mle-batch"
rerun=$DEFAULT_SDSS_RERUN

# generate a temporary command file
commandfile=$(generate_temporary_file --root="./" --prefix="temp-mle-submit-batch-" --suffix=".cmd")


declare -a camcol_a=("4" "3")
n0=2

declare -a source_model_a=("deV" "exp")
declare -a source_class_a=("DEV_GCLASS", "EXP_GCLASS")
n1=2

declare -a source_a=("fpC" "fpCC")
declare -a fit2fpCC_flag_a=("" "-fit2fpCC")
n2=1

declare -a target_model_a=("deV-exp" "deV" "exp")
declare -a target_class_a=("DEVEXP_GCLASS" "DEV_GCLASS" "EXP_GCLASS")
n3=3

declare -a target_model1_a=("deV" "deV-exp" "deV")
declare -a target_class1_a=("DEV_GCLASS" "DEVEXP_GCLASS" "DEV_GCLASS")
n31=1

declare -a target_model2_a=("deV-exp" "exp")
declare -a target_class2_a=("DEVEXP_GCLASS" "EXP_GCLASS")
n32=2

declare -a nobjc_flag_a=("-nobjc 1" "")
declare -a nobjcstr_a=("single" "multiple")
n4=1

#stripe 82
declare -a run_a=(4797 2700 2703 2708 2709 2960 3565 3434 3437 2968 4207 1742 4247 4252 4253 4263 4288 1863 1887 2570 2578 2579 109 125 211 240 241 250 251 256 259 273 287 297 307 94 5036 5042 5052 2873 21
24 1006 1009 1013 1033 1040 1055 1056 1057 4153 4157 4158 4184 4187 4188 4191 4192 2336 2886 2955 3325 3354 3355 3360 3362 3368 2385 2506 2728 4849 4858 4868 4874 4894 4895 4899 4905 4927 4930 4933 4948 2854 2855 2856
 2861 2662 3256 3313 3322 4073 4198 4203 3384 3388 3427 3430 4128 4136 4145 2738 2768 2820 1752 1755 1894 2583 2585 2589 2591 2649 2650 2659 2677 3438 3460 3458 3461 3465 7674 7183)
n5=120
n5=1


for (( i0=0; i0<$n0; i0++ ))
do

for (( i1=0; i1<$n1; i1++ ))
do 

for (( i2=0; i2<$n2; i2++ ))
do

for (( i3=0; i3<$n3; i3++ ))
do

for (( i4=0; i4<$n4; i4++ ))
do

for (( i5=0; i5<$n5; i5++ ))
do


camcol=${camcol_a[$i0]}

source_model=${source_model_a[$i1]}
source_class=${source_class_a[$i1]}

source=${source_a[$i2]}
fit2fpCC_flag=${fit2fpCC_flag_a[$i2]}

unset target_model_a
unset target_class_a

declare -a target_model_a=($source_model "deV-exp")
declare -a target_class_a=($source_class "DEVEXP_GCLASS")
n3=2

target_model=${target_model_a[$i3]}
target_class=${target_class_a[$i3]}

nobjc_flag=${nobjc_flag_a[$i4]}
nobjcstr=${nobjcstr_a[$i4]}

run=${run_a[$i5]}

root="/u/khosrow/thesis/opt/soho/single-galaxy/images"
outdir="$root/mle/$source_model/9999-9999/$target_model-$source-$nobjcstr/$rerun/$run/$camcol"
datadir="$root/sims/$source_model/$rerun/$run/$camcol"

#init_flag="SDSSpERROR_INIT -error 2"
init_flag="SDSS_INIT"
extra_flags="-fitsize -fitcenter -fitshape $nobjc_flag -gclass $target_class -init $init_flag $fit2fpCC_flag"

executable="do-mle"
mle-script.sh --debug --run=$run --rerun=$rerun --outdir=$outdir --datadir=$datadir --model=$source_model --fstep=1 --EXTRA_FLAGS="$extra_flags" --executable=$executable --field0=101 --field1=5000 --camcol=$camcol --commandfile=$commandfile

done
done
done
done
done
done

echo "$name: randomly running commands in file '$commandfile'"
run_commands_in_file --commandfile=$commandfile --random

echo "$name: deleting temp-file = $commandfile"
rm $commandfile

