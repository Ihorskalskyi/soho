#ifndef THKAHAN_H
#define THKAHAN_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ftcl.h"
#include "dervish.h"
#include "strings.h"

#include "thConsts.h"
#include "thMath.h"
#include "thDebug.h"

RET_CODE thKahanInit(int n);
RET_CODE thKahanEnd();
void thKahanInsertNext(KAHANFL x);
RET_CODE thKahanFree();
void thKahanAdd(KAHANFL x);
void thKahanZero();
void thKahanGet(KAHANFL *sum, KAHANFL *error, KAHANFL *total);

void thKahanAddVerbose(KAHANFL x, char *message);
void thKahanGetVerbose(KAHANFL *sum, KAHANFL *error, KAHANFL *total);

void thKahanAddDot2(KAHANFL a1, KAHANFL a2);
void thKahanAddDot3(KAHANFL a1, KAHANFL a2, KAHANFL a3);

#endif
