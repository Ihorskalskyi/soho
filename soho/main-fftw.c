/* PHOTO libraries */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ftcl.h"
#include "dervish.h"
#include "strings.h"
#include "time.h"

#include "phUtils.h"

#include "phPhotoFitsIO.h"
#include "phSpanUtil.h"

#include "thMath.h" 
#include "thDebug.h" 
#include "thIo.h" 
#include "thAscii.h" 
#include "thProc.h" 
#include "thMask.h" 
#include "thMSky.h" 
#include "thObjcTypes.h"
#include "thMGalaxy.h"
#include "thAlgorithm.h"
#include "thMap.h"
#include "thMle.h"
#include "thResidual.h"

typedef enum tweakflag {
	ABSOLUTE, FRACTIONAL, N_TWEAKFLAG, UNKNOWN_TWEAKFLAG
	} TWEAKFLAG;

#define SIM_SKY 1
#define ADD_POISSON_NOISE 1
#define DO_BKGRND 0
#define FIT_SHAPE  1
#define FIT_RE 1
#define FIT_E 0
#define FIT_PHI 0
#define FIT_CENTER 0 
#define LOCK_CENTER 1
#define FORCE_INIT_PAR 0
#define STAR_CLASSIFICATION 1 /* testing star classification */
#define GALAXY_CLASSIFICATION 1 /* testing galaxy classification */	
#define TABLEROW 5
#define N_RANDOM_FRAMES 2
#define N_OUTPUT_OBJC 5
#define FIXED_SKY 100.0
#define I_SKY 1.0
#define EllDesc SUPERCANONICAL
#define NSIMOBJC 3000
#define NFITOBJC 1
#define TWEAK 1
#define TWEAK_ALL 0
#define THIS_BAND 0
/* image clustering and memory constants */
#define THMEMORY 1300
#define DO_GREEDY 1
#define DO_ALGORITHM 1
#define ALGORITHM_MANUAL 0

#define DEV_ADJ_FACTOR 7
#define EXP_ADJ_FACTOR 4

typedef struct strategy {
	THPIX dev_adj_factor, exp_adj_factor;
	char *adj_method;
	int l_walktrap;
	int hierarchy_step;
	
	char *gfile_prefix, *clusterfile_prefix, *clusterlog_prefix;
	} STRATEGY;
 
int verbose = 0;

static int nrow = 1489;
static int ncol = 2048; 
static int band = 0;
static THPIX SKY_FRACTIONAL_TWEAK = 0.5;
#if FORCE_INIT_PAR
static THPIX I_TWEAK = 0.1; 
static THPIX CENTER_TWEAK = 1.0;
static THPIX SHAPE_TWEAK = 1.0;
static THPIX RE_TWEAK = 1.0;
static THPIX PHI_TWEAK = 1.0;
static THPIX E_TWEAK = 1.0;
#else 
static THPIX I_TWEAK = 1; 
static THPIX CENTER_TWEAK = 1.0;
static THPIX SHAPE_TWEAK = 1.0;
static THPIX RE_TWEAK = 1.0;
static THPIX PHI_TWEAK = 1.0;
static THPIX E_TWEAK = 1.0;
#endif

static THPIX XCENTER_INC = 0;
static THPIX YCENTER_INC = 0;
static THPIX BETA_SHAPE = 0.3;
static THPIX ELLIPTICITY = 0.7;
static THPIX PHI = 1.0;
static THPIX RE = 10.0;
static THPIX STRETCHINESS = 1.0;
static THPIX INTENSITY = 50;
static void usage(void);
void chain_model_output(char *job, CHAIN *objclist, THOBJCTYPE otype);
void model_output(char *job, THOBJC *objc);
int compare_re(const void *x1, const void *x2);
int compare_count(const void *x1, const void *x2);
void thChainQsort(CHAIN *chain,
             int (*compar)(const void *, const void *));
extern double ddot_(int *, double [], int *, double [], int *);
extern float sdot_(int *, float [], int *, float [], int *);

static RET_CODE tweak(void *p, THPIX e_tweak, char *pname, char **rnames, int rcount, CHAIN *chain, TWEAKFLAG tflag);
static RET_CODE add_poisson_noise_to_image(REGION *im, CHAIN *ampl);
static int fl_compare(const void *a, const void *b);
static int get_adjacency(void *a, void *b);
static RET_CODE get_objc_xy(THOBJC *objc, THPIX *xx, THPIX *yy);
static int pix_count(THOBJC *objc);	
static RET_CODE do_memory(CHAIN *objclist, STRATEGY *strategy);
static RET_CODE output_memory_stat(LSTRUCT *lstruct, char *fname1, char *fname2);
static RET_CODE output_reg_props(REGION *reg);
static RET_CODE tweak_init_pars(LSTRUCT *l, CHAIN *objclist);
static void my_holdup(void);
	
int
main(int ac, char *av[])
{

	int MEMORY_HOLDUP = 0;
  char *name = "do-model";
  /* Arguments */
  char *fname;

  char *simfile = "./fit/sim-model-poisson-sdss.fits";
  char *simodelfile = "./fit/sim-model-sdss.fits";
  char *fitfile = "./fit/fit-gaussian.fits";
  char *difffile = "./fit/diff-gaussian.fits";
  char *ifile = "./fit/graph-objc-overlap.bin";
  char *gfile = "./fit/graph-objc-overlap.gv";
  char *dndfile = "./fit/dnd-objc-overlap.gv";
  char *clusterfile = "./fit/cluster-objc-overlap.gv";
  char *clusterlog = "./fit/cluster-objc-overlap.log";

  while(ac > 2 && (av[1][0] == '-' || av[1][0] == '+')) {
    switch (av[1][1]) {
    case '?':
    case 'h':
      usage();
      exit(0);
      break;
    case 'v':
      verbose++;
      break;
    default:
      shError("Unknown option %s\n",av[1]);
      break;
    }
    ac--;
    av++;
  }
  if(ac <= 1) {
    shError("You must specify a process par file name. \n");
    exit(1);
  }
  fname = av[1]; 
  sscanf(av[2], "%d", &MEMORY_HOLDUP);	

int holdup = 0;
if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

#if THCLOCK 
clock_t clk1;
clk1 = clock();
#endif

printf("%s: initiating modules \n", name);

 printf("%s: initiating IO \n", name); 
 thInitIo();
 printf("%s: initiating Tank \n", name);
  thTankInit();
 printf("%s: initiating Ascii \n", name);
  thAsciiInit();  
 printf("%s: initiating SpanObjmask \n", name);
  initSpanObjmask();

 if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

printf("%s: reading framefile \n", name);

  /* get the related schema */
  ASCIISCHEMA *schema = NULL;
  char *strutype = "FRAMEFILES";	
  schema = thTankSchemaGetByType(strutype);

  if (schema == NULL) {
    printf("%s: No schema exists for structure %s", 
		   name, strutype);
  }

  /* get the structure constructor */
  void *(*strunew)();
  strunew = schema->constructor;
  if (strunew == NULL) {
    printf("%s: structure constructor for %s not available in tank", 
		   name, strutype);
  }

  /* construct a structure */
  void *stru;
  stru = (*strunew)();

  FRAMEFILES *ff;
  ff = (FRAMEFILES *) stru;

  FILE *fil;
  fil = fopen(fname, "r");
  
  CHAIN *fchain;
  fchain = thAsciiChainRead(fil);

  if (shChainSize(fchain) == 0) {
	thError("%s: no structures was properly read from the par file", name);
	}
  printf("%s: (%d) pieces of frame info read \n",
	 name, shChainSize(fchain));

if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

   
  ff = (FRAMEFILES *) shChainElementGetByPos(fchain, 0);
  
  //ff->phconfigfile[0] = 'X';
  
  /*
  size_t offset;
  offset = offsetof(FRAMEFILES, phconfigfile);

  void **phconfigfile;
  phconfigfile = (void **)(ff + offset);
  */

  if (ff == NULL) {
    thError("%s: NULL returned from chain - check source code", name);
  } else {    
    /* fields of the ff structure refer to outbound memory locations */
    printf("phconfigfile = %s \n", ff->phconfigfile);
    printf("phecalibfile = %s \n", ff->phecalibfile);
    printf("phflatframefile = %s \n", ff->phflatframefile);
    printf("phmaskfile = %s \n", ff->phmaskfile);
    printf("phsmfile = %s \n", ff->phsmfile);
    printf("phpsfile = %s \n", ff->phpsfile);
    printf("phobjcfile = %s \n", ff->phobjcfile);
    printf("thsbfile = %s \n", ff->thsbfile);
    printf("thobfile = %s \n", ff->thobfile);
    printf("thmakselfile = %s \n", ff->thmaskselfile);
    printf("thobjcselfile = %s \n", ff->thobjcselfile);
    printf("thsmfile = %s \n", ff->thsmfile);
    printf("thpsfile = %s \n", ff->thpsfile);
    printf("thobjcfile = %s \n", ff->thobjcfile);

  }

#if 1 /* debug nov 5 */ 
	printf("%s: initiating random numbers \n", name);
	RANDOM *rand;
	char *randstr;
	int nrandom;
	const float inorm = 1.0/(float)((1U<<(8*sizeof(int)-1)) - 1);
	randstr = thCalloc(MX_STRING_LEN, sizeof(char));
	nrandom = (int) (nrow * ncol * (float) N_RANDOM_FRAMES);
	sprintf(randstr, "%d:2", nrandom);
	printf("%s: generating %d random numbers (random frames= %g) \n", name, nrandom, (float) N_RANDOM_FRAMES);
	rand = phRandomNew(randstr, 1);

if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;
#endif

  printf("%s: process and frame \n", name);
  PROCESS *proc;
  proc = thProcessNew();

if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;
  int i, nframe = 1;
  int camrow, camcol;
  FRAME *frame;
  REGION *im;

  for (camrow = 1; camrow < 2;  camrow++) {
    for (camcol = 1; camcol < 2; camcol++) {

      frame = thFrameNew();
      if (frame->files != NULL) {
	thFramefilesDel(frame->files);
      }
      
      frame->files = ff; 
      frame->proc = proc;
      
#if 1 /* debug nov 5 */
      printf("%s: loading fpC file\n", name);
      thFrameLoadImage(frame);
 
if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;     
	im = frame->data->image;

      printf("%s: loading amplifier information\n", name);
      thFrameLoadAmpl(frame); 

if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

	/* no need to read the mask since we are gonna work with a fake sky only frame 
      printf("%s: loading PHOTO masks\n", name);
      thFrameLoadPhMask(frame);
	*/
#endif

#if 1 /* debug nov 5 */
	/* testing with PHOTO calibration data */
	printf("%s: loading PHCALIB \n", name);
	thFrameLoadCalib(frame);
	/* testing with SDSS objects */

if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;
#endif
      printf("%s: loading OBJC's \n", name);
      thFrameLoadPhObjc(frame);
	/* testing PSF at this stage */ 


if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;
#if 1 /* debugging */
      printf("%s: loading PHOTO psf \n", name);
      thFrameLoadPhPsf(frame);
      printf("%s: reshaping into SOHO psf \n", name);
      thFrameLoadThPsf(frame);

if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

      printf("%s: loading weight matrix -- space saver \n", name); 
      thFrameLoadWeight(frame);

if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

	/* mask is null at this point for this test */
      printf("%s: loading SOHO masks \n", name);
      thFrameLoadThMask(frame);

if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;
#endif	
	/* PERFORMING FIT TO THE FAKE FRAME */
	RET_CODE status;
	/* initializing model and object bank */
	thModelInit();

if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

	/* introducing sky to the object bank */
	printf("%s: initiating all models and object types  ... \n", name);
	#if 1 /* debug nov 5, 2012 */
	printf("%s: initiating sky model and object type\n", name);
	thSkyObjcInit(nrow, ncol, frame->data->ampl, &g0sky);

if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;
	#endif
	printf("%s: initiating other models \n", name);
	status = MGModelsInit();
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object models", name);
		return(-1);
	}

if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;
	int nm_max = 2;
	char *mname, **mnames = thCalloc(nm_max, sizeof(char *));
	for (i = 0; i < nm_max; i++) {
		mnames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
	} 
	mname = mnames[0];
	/* 
	strcpy(mname, "gaussian");
	printf("%s: initiating object type 'GAUSSIAN' \n", name);
	printf("%s: adding model '%s' \n", name, mname);
	status = MGaussianObjcInit(&mname, 1);	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object", name);
		return(-1);
	}
	*/
	/* 
	strcpy(mname, "deV");
	printf("%s: initiating object type 'DEVGALAXY' \n", name);
	printf("%s: adding  model '%s' \n", name, mname);
	status = MdeVGalaxyObjcInit(&mname, 1);	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object type", name);
		return(-1);
	}
	*/

	/* 	
	strcpy(mname, "Exp");
	printf("%s: initiating object type 'EXPGALAXY' \n", name);
	printf("%s: adding  model '%s' \n", name, mname);
	status = MExpGalaxyObjcInit(&mname, 1);	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object type", name);
		return(-1);
	}
	*/
	#if STAR_CLASSIFICATION
	strcpy(mnames[0], "star");
	printf("%s: initiating object type 'STAR' \n", name);
	status = MStarObjcInit(mnames[0]);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object type", name);
		return(-1);
	}
	#endif 

	#if CDCANDIDATE_CLASSIFICATION 
	strcpy(mnames[0], "deV");
	strcpy(mnames[1], "Exp2");
	printf("%s: initiating object type 'CDCANDIDATE' \n", name);
	printf("%s: adding two models '%s', '%s' \n", name, mnames[0], mnames[1]);
	status = McDCandidateObjcInit(mnames, 2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object type", name);
		return(-1);
	}
	#endif

	#if GALAXY_CLASSIFICATION

	strcpy(mnames[0], "deV");
	strcpy(mnames[1], "Exp");
	printf("%s: initiating object type 'GALAXY' \n", name);
	printf("%s: adding two models '%s', '%s' \n", name, mnames[0], mnames[1]);
	status = MGalaxyObjcInit(mnames, 2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object type", name);
		return(-1);
	}

	#endif

	int nsimobjc = 0;

	printf("%s: models and object types initiated \n", name);	
	/* define THOBJC with type SKY
	   construct a MAP
	*/

if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

	THOBJCID id = 0;
	THOBJC *objc = NULL;
	CHAIN *objclist = NULL;
	objclist = shChainNew("THOBJC");

	OBJC_IO *phobjc;
	CHAIN *phobjcs;
	int iobjc, nobjc;
	phobjcs = frame->data->phobjc;
	nobjc = shChainSize(phobjcs);
	printf("%s: (%d) objects read from SDSS file \n", name, nobjc);


	MAPMACHINE *map;
#if 1 /* debug nov 5, 2012 */
	printf("%s: loading models into (mapmachine) \n", name);
	map = thMapmachineNew();
	/* inserting map into frame */
	frame->work->map = map;

	/* add all the models in the object to the map */
	char **rnames, **iornames;
	int nrname = 10;
	rnames = thCalloc(nrname, sizeof(char*));
	iornames = thCalloc(nrname, sizeof(char *));
	for (i = 0; i < nrname; i++) {
		rnames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
		iornames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
	}
	LSTRUCT *l;
	LWORK *lwork;

	REGION *simreg = NULL, *bkgrnd = NULL;

	int pos = 0;
	char *ioname;
	TYPE iotype;
	SKYPARS *ps;

	
if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

	/* introducing sky to the simulated set of objects  */
	#define DO_SKY 1
	#if DO_SKY
	printf("%s: creating sky object \n", name);
	objc = thSkyObjcNew();
	if (objc == NULL) {
		thError("%s: ERROR - could no create object sample", name);
		return(-1);
	}
	printf("%s: loading sky object onto (mapmachine) \n", name);
	thMAddObjcAllModels(map, objc, SEPARATE);
	objc->thid = id++;
	shChainElementAddByPos(objclist, objc, "THOBJC", TAIL, AFTER);

	#if SIM_SKY

	printf("%s: uploading sky simulation parameters \n", name);
	status = thMCompile(map);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not compile the map", name);
		return(-1);
	}	
	/* constructing LSTRUCT */
	printf("%s: constructing likelihood data structure (lstruct) \n", name);
 	status = thFrameLoadLstruct(frame, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not load (lstruct)", name);
		return(-1);
	}
	l = frame->work->lstruct;
	l->lmodel->lmachine = map;
	lwork = l->lwork;

if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

	/* simulating the sky */
	printf("%s: sky simulation \n", name); 
	objc = shChainElementGetByPos(objclist, pos);
	thObjcGetIopar(objc, (void **) &ps, &iotype);
	ioname = shNameGetFromType(iotype);	
	if (ps != NULL && !strcmp(ioname, "SKYPARS")) {	
	printf("%s: setting simulation values for sky in io-par \n", name);
	ps->I_z00 = I_SKY * 10.00;
	ps->I_s00 = I_SKY * 5.00;
	ps->I_s01 = I_SKY * -5.00;
	ps->I_s10 = I_SKY * (-7.00);
        ps->I_s11 = I_SKY * 6.00;
	ps->I_s12 = I_SKY * 0.00;
	ps->I_s21 = I_SKY * 0.00;
	ps->I_s32 = I_SKY * 2.00;
	ps->I_s43 = I_SKY * 0.00;
	} else {
		printf("%s: WARNING - null parameter in object (objc: %d) \n", name, objc->thid);
	}

	thLDumpAmp(l, IOTOX);
	thLDumpPar(l, IOTOX);
	thLDumpPar(l, IOTOXN);

	thLDumpAmp(l, IOTOMODEL);
	thLDumpPar(l, IOTOMODEL);
	thLDumpPar(l, IOTOMODEL);

	#if DO_BKGRND

	printf("%s: simulating sky and saving as backgroundi \n", name);
	printf("%s: 1. memory estimates \n", name); 
	status = MakeImageAndMatrices(l, MEMORYESTIMATE);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get memory estimate for the model", name);
		return(-1);
	}
	printf("%s: 2. init images \n", name);
	status = MakeImageAndMatrices(l, INITIMAGE); /* MODELONLY */
   	if (status != SH_SUCCESS) {
		thError("%s: could not simulate the basis models for sky", name);
		return(-1);
	}
	status = thMakeM(l);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add up models to create sky", name);
		return(-1);
	}
	simreg = l->lwork->mN;
	bkgrnd = shRegNew("background image - sky", simreg->nrow, simreg->ncol, simreg->type);
	thRegPixCopy(simreg, bkgrnd);

	printf("%s: sky constructed and stored", name);
	#endif

	#endif

	#endif
if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;
#endif

	printf("%s: proceeding to the rest of the objects ... \n", name);
	phobjcs = frame->data->phobjc;
	nobjc = shChainSize(phobjcs);
	printf("%s: (%d) objects read from SDSS file \n", name, nobjc);

	/* introducing a galaxy */

	/* 
	printf("%s: simulating objects as '%s' \n", name, sim_name);
	*/
	printf("%s: sorting objects \n", name);
	shChainQsort(phobjcs, &compare_re);	

	for (iobjc = 0; iobjc < nobjc; iobjc++) {

		/* initiating the object */
		phobjc = shChainElementGetByPos(phobjcs, iobjc);
		PHPROPS *phprop = phPropsNew();
		PHCALIB *calibData;
		calibData = frame->data->phcalib;
		status = thPhpropsPut(phprop, phobjc, NULL, NULL);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not put object information in 'PHPROP'", name);
			return(-1);
		} 
		objc = thObjcNewFromPhprop(phprop, CLASSIFY, NULL);
		if (objc == NULL) {
			thError("%s: ERROR - could not create object for object (%d) in the sdss list", name, iobjc);
			return(-1);
		}
		objc->thid = (THOBJCID) phobjc->id;
		THOBJCTYPE objctype;
		char *objcname;
		objctype = objc->thobjctype;
		status = thObjcNameGetFromType(objctype, &objcname);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get type name from object type (iobjc = %d)", name, iobjc);
			return(-1);
		}
		if (objctype != SKY_OBJC && objctype != PHOTO_OBJC) {
			/* one should create the ioprop of the object after it is properly classified */
 	
			status = thObjcInitMpars(objc);
			if (status != SH_SUCCESS) {
				char *tname = NULL;
				thObjcNameGetFromType(objc->thobjctype, &tname);
				thError("%s: ERROR - could not initiate model parameters for (objc: %d) of class '%s'", 
				name, objc->thid, tname);
				return(-1);
			}

			shChainElementAddByPos(objclist, objc, "THOBJC", TAIL, AFTER);		
			thMAddObjcAllModels(map, objc, SEPARATE);
			nsimobjc++;
			if (nsimobjc == NSIMOBJC) break;
		
		} else if (objctype == PHOTO_OBJC) {
			thObjcDel(objc);
		}	


	}


	nobjc = shChainSize(objclist);
	printf("%s: total of (%d) objects added to (mapmachine) \n", name, nobjc);
	if (nobjc <= 1) {
		thError("%s: ERROR - no objects loaded from SDSS", name);
		return(-1);
	}

if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

	int ii, j = 0;
	ii = j;
	strcpy(rnames[j++], "I");
	#if FIT_SHAPE 
	int ishape, jshape, nshape;
	ishape = j;
	if (EllDesc == ALGEBRAIC) {
		strcpy(rnames[j++], "a");
 		strcpy(rnames[j++], "b");
		strcpy(rnames[j++], "c");
	} else if (EllDesc == CANONICAL) {
		#if FIT_RE
		strcpy(rnames[j++], "re");
		#endif
		#if FIT_E
		strcpy(rnames[j++], "e");
		#endif
		#if FIT_PHI
		strcpy(rnames[j++], "phi");
		#endif
	} else if (EllDesc == SUPERCANONICAL || EllDesc == UBERCANONICAL) {
		#if FIT_RE
		strcpy(rnames[j++], "re"); 
		#endif
		#if FIT_E
		strcpy(rnames[j++], "E");
		#endif
		#if FIT_PHI
		strcpy(rnames[j++], "phi");
		#endif
	}	
	jshape = j;
	nshape = j;
	#endif
	#if FIT_CENTER
	int icenter, jcenter, ncenter;
	icenter = j;
	strcpy(rnames[j++], "xc");  
	strcpy(rnames[j++], "yc");
	jcenter = j;
	ncenter = (jcenter - icenter);
	#endif
	
	int k, kmax, kfit = 0;
	if (j > 1) { 
	printf("%s: adding variables to nonlinear fit (variables/model = %d)\n", 
		name, j - 1);
	kmax = MIN(NFITOBJC, shChainSize(objclist));
	printf("%s: number of objects for nonlinear fit = %d \n", name, kmax); 
	for (k = 0; k < shChainSize(objclist); k++) {

	if (kfit == kmax) break;
	THOBJCTYPE objctype;
	char *objcname;
	objc = shChainElementGetByPos(objclist, k);
	objctype = objc->thobjctype;
	thObjcNameGetFromType(objctype, &objcname);
	if (!strcmp(objcname, "DEVGALAXY") || 
		!strcmp(objcname, "EXPGALAXY") || 
		!strcmp(objcname, "GAUSSIAN") || 
		!strcmp(objcname, "GALAXY") || 
		!strcmp(objcname, "CDCANDIDATE")) {
	
		kfit++;	
		if (!strcmp(objcname, "DEVGALAXY")) strcpy(mnames[0], "deV");
		if (!strcmp(objcname, "EXPGALAXY")) strcpy(mnames[0], "Exp");
		if (!strcmp(objcname, "GAUSSIAN")) strcpy(mnames[0], "gaussian");
		if (!strcmp(objcname, "GALAXY")) {
			strcpy(mnames[0], "deV");
			strcpy(mnames[1], "Exp");
		}
		if (!strcmp(objcname, "CDCANDIDATE")) {
			strcpy(mnames[0], "deV");
			strcpy(mnames[1], "Exp2");
		}	
		
		if (j > 1) {
		mname = mnames[0];	
		status = thMClaimObjcVar(map, objc, mname, rnames + 1, j - 1);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not add model '%s' in (objc: %d) to fit", 
			name, mname, objc->thid);
			return(-1);
		}
		mname = mnames[1];
		if (!strcmp(objcname, "GALAXY") || !strcmp(objcname, "CDCANDIDATE")) {
		status = thMClaimObjcVar(map, objc, mname, rnames + 1, j - 1);	
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not add model '%s' in (objc: %d) to fit", 
			name, mname, objc->thid);
			return(-1);
		}
		}
		#if FIT_CENTER
		#if LOCK_CENTER
		if (!strcmp(objcname, "GALAXY") || !strcmp(objcname, "CDCANDIDATE")) {
			status = thMEquivObjcVars(map, objc, mnames[0], rnames + icenter,
				objc, mnames[1], rnames + icenter, ncenter);
		}	
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not claim centers as equiv", name);
			return(-1);
		}
		#endif
		#endif
		}
		printf("%s: (objc: %d) ", name, objc->thid);
		for (i = 0; i < j; i++) {
			printf(" '%s' ", rnames[i]);
		}
		printf("\n");
	}
	}
	} else {
		printf("%s: no nonlinear component to the fit", name);
	}

if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

	printf("%s: skipping igraph tools \n", name);
	/* the following is igraph tools which should be skipped 
	printf("%s: preparing strategy to pass to graph tool \n", name);
	int n_walktrap = N_WALKTRAP;
	int n_hierarchy_step = N_HIERARCHY_STEP;
	int n_adj_method = N_ADJ_METHOD;
	int l_walktrap[6] = {2, 3, 5, 10, 20, 30};
	int hierarchy_step[4] = {3, 5, 6, 10};
	char **adj_method = thCalloc(n_adj_method, sizeof(char *));
	for (i = 0; i < n_adj_method; i++) {
		adj_method[i] = thCalloc(MX_STRING_LEN, sizeof(char));
	}
	#if SQUARE_OVERLAP
	strcpy(adj_method[0], "SQUARE");
	#else
	strcpy(adj_method[0], "CIRCLE");
	#endif

	STRATEGY *strategy = thCalloc(1, sizeof(STRATEGY));
	strategy->dev_adj_factor = (THPIX) DEV_ADJ_FACTOR;
	strategy->exp_adj_factor = (THPIX) EXP_ADJ_FACTOR;
	#if WEIGHTED_GRAPH
	strategy->gfile_prefix = "./fit/graph-objc-overlap-PSF-WEIGHTED";
	strategy->clusterfile_prefix = "./fit/cluster-objc-overlap-PSF-WEIGHTED";
	strategy->clusterlog_prefix = "./fit/cluster-objc-overlap-PSF-WEIGHTED";
	#else
	strategy->gfile_prefix = "./fit/graph-objc-overlap-PSF";
	strategy->clusterfile_prefix = "./fit/cluster-objc-overlap-PSF";
	strategy->clusterlog_prefix = "./fit/cluster-objc-overlap-PSF";
	#endif

	printf("%s: looping over strategy graph tool \n", name);

	int i_walktrap, i_adj_method, i_hierarchy_step;	
	for (i_walktrap = 0; i_walktrap < n_walktrap; i_walktrap++) {
		for (i_adj_method = 0; i_adj_method < n_adj_method; i_adj_method++) {
			for (i_hierarchy_step = 0; i_hierarchy_step < n_hierarchy_step; i_hierarchy_step++) {
				strategy->l_walktrap = l_walktrap[i_walktrap];
				strategy->hierarchy_step = hierarchy_step[i_hierarchy_step];
				strategy->adj_method = adj_method[i_adj_method];
				do_memory(objclist, strategy);
			}
		}
	}					

	*/

	/* return at the end of memory algorithm designation */

	/*  	
	printf("%s: premature return due to user's request \n", name);	
	return(1);
	*/
	
if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

	/* compiling map */	
	printf("%s: compiling (mapmachine) \n", name);
	thMCompile(map);
 
	printf("%s: outputting (mapmachine) info \n", name);
	/* 
	thMInfoPrint(map, EXTENSIVE);	
	*/
	printf("%s: n(model) = %d, n(par) = %d, n(objc) = %d \n", name, map->namp, map->npar, map->nobjc);

if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

	/* constructing LSTRUCT */
	printf("%s: constructing likelihood data structure (lstruct) \n", name);
 	status = thFrameLoadLstruct(frame, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not load (lstruct)", name);
		return(-1);
	}
	status = thFrameGetLstruct(frame, &l);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (lstruct) out of (frame)", name);
		return(-1);
	}
	status = thLstructPutLmachine(l, map);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (lmachine) out of (lstruct)", name);
		return(-1);
	}
	status = thLstructGetLwork(l, &lwork);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (lwork) out of (lstruct)", name);
		return(-1);
	}

if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

	/* simulating */

	#if 0
	int pos = 0;
	/* simulating the sky */ 
	SKYPARS *ps;
	char *ioname;
	TYPE iotype;
	objc = shChainElementGetByPos(objclist, pos);
	thObjcGetIopar(objc, (void **) &ps, &iotype);
	ioname = shNameGetFromType(iotype);	
	if (ps != NULL && !strcmp(ioname, "SKYPARS")) {	
	printf("%s: setting simulation values for sky in io-par \n", name);
	ps->I_z00 = I_SKY * 10.00;
	ps->I_s00 = I_SKY * 20.00;
	ps->I_s01 = I_SKY * (-5.00);
	ps->I_s10 = I_SKY * (-7.00);
        ps->I_s11 = I_SKY * 6.00;
	ps->I_s12 = I_SKY * 0.00;
	ps->I_s21 = I_SKY * 0.00;
	ps->I_s32 = I_SKY * 2.00;
	ps->I_s43 = I_SKY * 0.00;
	} else {
		printf("%s: WARNING - null parameter in object (objc: %d) \n", name, objc->thid);
	}

	/* 
	thLDumpAmp(l, IOTOX);
	thLDumpPar(l, IOTOX);
	thLDumpPar(l, IOTOXN);

	thLDumpAmp(l, IOTOMODEL);
	thLDumpPar(l, IOTOMODEL);
	thLDumpPar(l, IOTOMODEL);
	*/
	#endif
	
	/* simulating the galaxy */ 
	pos++;	
	OBJCPARS *pg;
	#if FORCE_INIT_PAR	
	printf("%s: setting random simulation values for objects \n", name);
	for (pos = 0; pos < shChainSize(objclist); pos++) {

	THOBJCTYPE objctype;
	char *objcname;
	objc = shChainElementGetByPos(objclist, pos);
	objctype = objc->thobjctype;
	thObjcNameGetFromType(objctype, &objcname);
	if (!strcmp(objcname, "GALAXY") || !strcmp(objcname, "DEVGALAXY") || !strcmp(objcname, "EXPGALAXY") || !strcmp(objcname, "GAUSSIAN")) {
	thObjcGetIopar(objc, (void **) &pg, NULL);
	if (pg != NULL) {
	if (!strcmp(objcname, "GAUSSIAN")) {
		pg->re_gaussian = RE;
		pg->I_gaussian= INTENSITY;
		pg->xc_gaussian = nrow / 2.0 + nrow /4.0 * phRandom();
		pg->yc_gaussian = ncol / 2.0 + ncol / 4.0 * phRandom();
		pg->a_gaussian = 0.5 / pow(pg->re_gaussian, 2);
		pg->c_gaussian = 0.5 / pow(pg->re_gaussian, 2);
		pg->b_gaussian = sqrt(pg->a_gaussian * pg->c_gaussian) * BETA_SHAPE;
		pg->e_gaussian = ELLIPTICITY;
		pg->phi_gaussian = PHI;
		pg->E_gaussian = STRETCHINESS;
	} else if (!strcmp(objcname, "DEVGALAXY")){
		printf("%s: randomly defining DEVGALAXY\n", name);
		pg->re_deV = RE * fabs((FL32) phGaussdev());
		pg->I_deV = INTENSITY * fabs((FL32) phGaussdev());
		pg->xc_deV= nrow / 2.0 * (1.0 + inorm * (FL32) phRandom());
		pg->yc_deV = ncol / 2.0 * (1.0 + inorm * (FL32) phRandom());
		/* 
		pg->a_deV = 0.5 / pow(pg->re_deV, 2);
		pg->c_deV = 0.5 / pow(pg->re_deV, 2);
		pg->b_deV = sqrt(pg->a_deV * pg->c_deV) * BETA_SHAPE * phGaussdev();
		*/ 
		pg->e_deV = ELLIPTICITY * fabs((FL32) phGaussdev());;
		pg->phi_deV = PI / 2.0 * (FL32) phGaussdev();
		pg->E_deV = STRETCHINESS * fabs((FL32) phGaussdev());
	}  else if (!strcmp(objcname, "EXPGALAXY")){
		printf("%s: randomly defining EXPGALAXY \n", name);
		pg->re_Exp = RE * fabs((THPIX) phGaussdev());
		pg->I_Exp = INTENSITY * fabs((THPIX) phGaussdev());
		pg->xc_Exp= nrow / 2.0 * (1.0 + inorm * (FL32) phRandom());
		pg->yc_Exp = ncol / 2.0 * (1.0 + inorm * (FL32) phRandom());
		/* 
		pg->a_deV = 0.5 / pow(pg->re_deV, 2);
		pg->c_deV = 0.5 / pow(pg->re_deV, 2);
		pg->b_deV = sqrt(pg->a_deV * pg->c_deV) * BETA_SHAPE * phGaussdev();
		*/ 
		pg->e_Exp = ELLIPTICITY * (1.0 + inorm * (FL32) phRandom()) / 2.0;
		pg->phi_Exp = PI / 2.0 * (THPIX) phGaussdev();
		pg->E_Exp = STRETCHINESS * fabs((THPIX) phGaussdev());
	} else if (!strcmp(objcname, "GALAXY")) {
		printf("%s: randomly defining GALAXY\n", name);
		pg->re_deV = RE * fabs((FL32) phGaussdev());
		pg->I_deV = INTENSITY * fabs((FL32) phGaussdev());
		pg->xc_deV= nrow / 2.0 * (1.0 + inorm * (FL32) phRandom());
		pg->yc_deV = ncol / 2.0 * (1.0 + inorm * (FL32) phRandom());
		/* 
		pg->a_deV = 0.5 / pow(pg->re_deV, 2);
		pg->c_deV = 0.5 / pow(pg->re_deV, 2);
		pg->b_deV = sqrt(pg->a_deV * pg->c_deV) * BETA_SHAPE * phGaussdev();
		*/ 
		pg->e_deV = ELLIPTICITY * (1.0 + inorm * (FL32) phRandom()) / 2.0;
		pg->phi_deV = PI / 2.0 * (FL32) phGaussdev();
		pg->E_deV = STRETCHINESS * fabs((FL32) phGaussdev());
		
		pg->re_Exp = RE * fabs((THPIX) phGaussdev());
		pg->I_Exp = INTENSITY * fabs((THPIX) phGaussdev());
		pg->xc_Exp= pg->xc_deV;
		pg->yc_Exp = pg->yc_deV;
		/* 
		pg->a_deV = 0.5 / pow(pg->re_deV, 2);
		pg->c_deV = 0.5 / pow(pg->re_deV, 2);
		pg->b_deV = sqrt(pg->a_deV * pg->c_deV) * BETA_SHAPE * phGaussdev();
		*/ 
		pg->e_Exp = ELLIPTICITY * (1.0 + inorm * (FL32) phRandom()) / 2.0;
		pg->phi_Exp = PI / 2.0 * (THPIX) phGaussdev();
		pg->E_Exp = STRETCHINESS * fabs((THPIX) phGaussdev());

	}
	} else {
		printf("%s: WARNING - null parameter in object (objc: %d) \n", name, objc->thid);
	}

	}
	}
	/* dumping parameters into their respective location */ 
	printf("%s: assigning parameter and amplitude arrays in (mapmachine) from IO parameters \n", name);
	
	thLDumpAmp(l, IOTOMODEL);
	thLDumpPar(l, IOTOMODEL); 
	
	thLDumpAmp(l, IOTOX);
	thLDumpPar(l, IOTOX);
	thLDumpPar(l, IOTOXN);
	
	#else 
	
	printf("%s: assigining parameters and amplitude arrays in (mapmachine) from SDSS model parameters\n", 
	name);

	  
	thLDumpAmp(l, MODELTOIO); 
	thLDumpPar(l, MODELTOIO);
 
	thLDumpAmp(l, MODELTOX);
	thLDumpPar(l, MODELTOX);
	thLDumpPar(l, MODELTOXN);

	#endif

	/* showing the statistics */
	chain_model_output("sim parameters: ", objclist, UNKNOWN_OBJC);

	/* calculation needs proper initiation of quadrature methods and elliptical description */
	status = MGInitProfile(EllDesc);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate profile package", name);
		return(-1);
	}

	/* creating the simulated / fake image */
	printf("%s: simulating the frame: \n", name);
	printf("%s: * memory estimates \n", name);
	status = MakeImageAndMatrices(l, MEMORY_ESTIMATE);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not estimate the memory for the model elements", name);
		return(-1);
	}

status = output_memory_stat(l, "./memory-stat.txt", NULL);

if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

	printf("%s: * algorithm design \n", name);
	ADJ_MATRIX *adj = thAdjMatrixNew();
	status = thCreateAdjacencyMatrix(adj, l);
 	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not create the adjacency matrix for the current likelihood structure", name);
		return(-1);
	}
	ALGORITHM *alg_cr = thAlgorithmNew();
	alg_cr->memory_total = (MEMDBLE) THMEMORY * (MEMDBLE) MEGABYTE;	
	status = design_algorithm(adj, alg_cr, CREATE_MODEL, INITRUN);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not design the algorithm", name);
		return(-1);
	} 
	output_algorithm(alg_cr, adj);
if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

	printf("%s: * algorithm run \n", name);
	status = thAlgorithmRun(alg_cr, l);	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not run the algorithm", name);
		return(-1);
	}
if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

	printf("%s: * copying simulated image onto data \n", name);	
	thRegPixCopy(l->lwork->mN, im);
	#if ADD_POISSON_NOISE
	printf("%s: outputting continuous model onto file '%s' \n", name, simodelfile);
	shRegWriteAsFits(im, simodelfile, STANDARD, 2, DEF_NONE, NULL, 0);
	printf("%s: simulating poisson noise \n", name);
	status = add_poisson_noise_to_image(im, frame->data->ampl);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add poisson noise to (image) in (lstruct)", name);
		return(-1);
	}
	#else
	printf("%s: poisson noise _not_ simulated \n", name);
	#endif
	printf("%s: outputting simulated model onto file '%s' \n", name, simfile);
	shRegWriteAsFits(im, simfile, STANDARD, 2, DEF_NONE, NULL, 0);

	/* re-creating the poisson weights */
	printf("%s: loading poisson weights \n", name);
      	thFrameLoadWeight(frame);
	printf("%s: loading SOHO masks \n", name);
      	thFrameLoadThMask(frame);
	
	printf("%s: number of objects = %d \n", name, shChainSize(objclist));
	#if TWEAK
	printf("%s: tweaking parameters to get initial guess ... \n", name);
	status = tweak_init_pars(l, objclist);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get the initial guess by tweaking", name);
		return(-1);
	}
	#endif
	/* outputting the tweaked parameters */
	chain_model_output("initial guess parameters (tweaked): ", objclist, UNKNOWN_OBJC);

#if DO_ALGORITHM
	clock_t clk1, clk2;
	time_t time1, time2, dif;
	printf("%s: * algorithm \n", name);

	#if ALGORITHM_MANUAL

	printf("%s: i. adjacency matrix \n", name);
	ADJ_MATRIX *adj = thAdjMatrixNew();
	status = thCreateAdjacencyMatrix(adj, l);
 	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not create the adjacency matrix for the current likelihood structure", name);
		return(-1);
	}

	/* 
	output_adj_matrix(adj);
	output_reg_props(l->lwork->mN);
	*/

if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

	/* 
	output_reg_props(l->lwork->mN);
	*/
	printf("%s: ii. algorithm design (manual) \n", name);	
	printf("%s: ii.a. initial run \n", name);
	clk1 = clock();
	ALGORITHM *alg = thAlgorithmNew();
	alg->memory_total = (MEMDBLE) THMEMORY * (MEMDBLE) MEGABYTE;	
	status = design_algorithm(adj, alg, CREATE_MODEL | INNER_PRODUCTS, INITRUN);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not design the algorithm", name);
		return(-1);
	}
	clk2 = clock();
	/* 
	output_algorithm(alg, adj);
	output_reg_props(l->lwork->mN);
	*/
	printf("%s: design time: %g sec \n", name, ((float) (clk2 - clk1)) / (float) CLOCKS_PER_SEC);
	printf("%s: ii.b. middle run \n", name);
	clk1 = clock();
	ALGORITHM *alg2 = thAlgorithmNew();
	alg2->memory_total = (MEMDBLE) THMEMORY * (MEMDBLE) MEGABYTE;	
	
	status = design_algorithm(adj, alg2, INNER_PRODUCTS, MIDDLERUN);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not design the algorithm", name);
		return(-1);
	}
	clk2 = clock();
	printf("%s: design time: %g sec \n", name, ((float) (clk2 - clk1)) / (float) CLOCKS_PER_SEC);
	/* 	
	output_algorithm(alg2, adj);
	output_reg_props(l->lwork->mN);
	*/
if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

	/* 
	output_reg_props(l->lwork->mN);
	*/
	printf("%s: iii. running the algorithm (manual) \n", name);
	printf("%s: iii.a. initial run \n", name);
	time(&time1);
	status = thAlgorithmRun(alg, l);	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not run the algorithm", name);
		return(-1);
	}
	time(&time2);
	dif = difftime(time2, time1);	
	printf("%s: run time: %g sec \n", name, (float) dif);
	/* 
	output_reg_props(l->lwork->mN);
	*/
if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;
	
	printf("%s: iii.b. middle run \n", name);
	time(&time1);
	status = thAlgorithmRun(alg2, l);	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not run the algorithm", name);
		return(-1);
	}
	time(&time2);
	dif = difftime(time2, time1);
	printf("%s: run time: %g sec \n", name, (float) dif);
if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

	#else

	MEMDBLE memory = (MEMDBLE) THMEMORY * MEGABYTE;	
	printf("%s: i. l compilation (automatic) \n", name);	
	clk1 = clock();
	status = thLCompile(l, memory, SIMFINALFIT);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not compile (l)", name);
		return(-1);
	}
	clk2 = clock();
	printf("%s: compile time: %g sec \n", name, ((float) (clk2 - clk1)) / (float) CLOCKS_PER_SEC);

	#if LRUN_MANUAL	
	printf("%s: ii.a. l run (manual) \n", name);
	ALGORITHM *alg;
	printf("%s: ii.a.i. INITSTAGE \n", name);
	alg = l->lalg->algs[INITSTAGE];
	status = thAlgorithmRun(alg, l);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not run 'INITSTAGE' alg", name);
		return(-1);
	}
	int iter;
	for (iter = 0; iter < 5; iter++) {
	printf("%s: ii.a.ii.  MIDDLESTAGE (iteration = %d) \n", name, iter);
	alg = l->lalg->algs[MIDDLESTAGE];
	status = thAlgorithmRun(alg, l);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not run 'MIDDLESTAGE' alg", name);
		return(-1);
	}
	}
	printf("%s: ii.a.iii. ENDSTAGE \n", name);
	alg = l->lalg->algs[ENDSTAGE];
	status = thAlgorithmRun(alg, l);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not run 'ENDSTAGE' alg", name);
		return(-1);
	}
	#else
	printf("%s: ii.a. l run (manual) - skipping \n", name);
	printf("%s: ii.b. l run (automatic) \n", name);
	time(&time1);
	status = thLRun(l);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not run (l)", name);
		return(-1);
	}
	time(&time2);
	dif = difftime(time2, time1);	
	printf("%s: run time: %g sec \n", name, (float) dif);


if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
	#endif
holdup++;

	#endif

return(1);
#else /* classic run */

	/*
	output_reg_props(l->lwork->mN);
	*/
	printf("%s: * init images (not the model) \n", name);
	status = MakeImageAndMatrices(l, INITIMAGES);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not init images", name);
		return(-1);
	}
	/* 
	output_reg_props(l->lwork->mN);
	*/
if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

	printf("%s: * making the model \n", name);
	status = thMakeM(l);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not make model for (lstruct)", name);
		return(-1);
	}
#endif

	/* 
	output_reg_props(l->lwork->mN);
	*/
	printf("%s: copying simulated image onto a new region \n", name);
	simreg = l->lwork->mN;
	thRegPixCopy(simreg, im);

if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;
	/* 
	output_reg_props(im);
	*/
	#if ADD_POISSON_NOISE
	/* adding poisson noise to the image */
	printf("%s: simulating poisson noise \n", name);
	status = add_poisson_noise_to_image(im, frame->data->ampl);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add poisson noise to (image) in (lstruct)", name);
		return(-1);
	}
if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

	/* 
	output_reg_props(im);
	*/
	#endif
	/* dumping created model into an image file */
	printf("%s: outputting the simulated image onto '%s' \n", name, simfile);
	shRegWriteAsFits(im, simfile, 
			STANDARD, 2, DEF_NONE, NULL, 0);

if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

	/*
	output_reg_props(l->lwork->mN);
	*/
/* doing residual reports */

	REGION *diff;
	diff = shRegNew("residual image", simreg->nrow, simreg->ncol, simreg->type);
	thPutModelComponent(diff, simreg, 1.0); /* sim reg is the product of simulation */
	thAddModelComponent(diff, im, -1.0); /* im is the data  */	

	int rbin1 = 1, cbin1 = 1, rbin2 = 20, cbin2 = 20;
	printf("%s: iv. esidual reports \n", name);
	printf("%s: iv.a. creating report for (rbin, cbin) = (%d, %d) \n", name, rbin1, cbin1);
	RESIDUAL_REPORT *res = NULL;
	status = thCreateResidualReport(diff, NULL, rbin1, cbin1, &res);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not create residual report", name);
		return(-1);
	}
	printf("%s: iv.a. compiling report \n", name);
	status = thCompileResidualReport(res);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not compile residual report", name);
		return(-1);
	}
	
if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

	printf("%s: iv.b. creating chain of reports (rbin = %d, %d), (cbin = %d, %d) \n", name, rbin1, rbin2, cbin1, cbin2);
	CHAIN *reschain;
	reschain = shChainNew("RESIDUAL_REPORT");
	status = thCreateResidualReportChain(diff, NULL, rbin1, rbin2, cbin1, cbin2, reschain);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not create residual report chain", name);
		return(-1);
	}
	printf("%s: iv.b. compiling residual report chain \n", name);
	status = thCompileResidualReportChain(reschain);	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not compile residual report chain", name);
		return(-1);
	}

if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

	status = output_residual_report_chain(reschain);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not output (residual report) chain", name);
		return(-1);
	}

if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

printf("%s: return requested by user \n", name);
return(1);

	/* re-creating the poisson weights */
	printf("%s: loading poisson weights \n", name);
      	thFrameLoadWeight(frame);
	printf("%s: loading SOHO masks \n", name);
      	thFrameLoadThMask(frame);

	/* 
		outputting simulation paramters 
	*/
	/* 
		Tweaking Initial Parameters a bit 
	*/

if (MEMORY_HOLDUP == holdup) {
	int iii;
	printf("%s: staying alive for memory holdup stage (%d) \n", name, holdup);
	while (1 != 0) {
		iii++;
	}
}
holdup++;

	printf("%s: tweaking parameters to get initial guess ... \n", name);
	status = tweak_init_pars(l, objclist);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get the initial guess by tweaking", name);
		return(-1);
	}
	/* outputting the tweaked parameters */
	chain_model_output("initial guess parameters (tweaked): ", objclist, UNKNOWN_OBJC);

	/*
	   do LM-ext fit
	*/
	printf("%s: conducting the extended LM fit \n", name);
	status = thModelExtLMFitSimpleRun(l);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not conduct an extended LM fit", name);
		return(-1);
	}
	/* 
	   put fit parameter values and amplitudes in the THOBJC data structure 
	*/
	#if 1
	printf("%s: dumping fit amplitudes from X-array onto models \n", name);
	thLDumpAmp(l, XTOMODEL);
	printf("%s: dumping fit amplitudes and parameters in IO records \n", name);
	/* thLDumpAmp(l, MODELTOIO); */
	thLDumpPar(l, MODELTOIO);
	#endif
	/*
           print out fit results
	*/
	printf("%s: printing fit results \n", name);	
	chain_model_output("fit results", objclist, UNKNOWN_OBJC);
	
	REGION *fitim;
	fitim = l->lwork->mN;
	printf("%s: outputting fitted image in '%s' \n", name, fitfile);
	shRegWriteAsFits(fitim, fitfile, 
			STANDARD, 2, DEF_NONE, NULL, 0);
	thAddModelComponent(im, fitim, -1.0);
	printf("%s: outputting residual image in '%s' \n", name, difffile);
	shRegWriteAsFits(im, difffile, 
			STANDARD, 2, DEF_NONE, NULL, 0);
	
	
	printf("%s: end of the program \n", name);

	}
    }

  thErrShowStack();
  /* for unknown reason, this returns an error - distribution of the variates were checked and the results produced satisfy the 
	randomness condition.
  phRandomDel(rand);
	*/

  /* successfull run */
  return(0);
}


/*****************************************************************************/

static void
usage(void)
{
   char **line;

   static char *msg[] = {
      "Usage: read_atlas_image [options] output-file n_tr n_tc",
      "Your options are:",
      "       -?      This message",
      "       -h      This message",
      "       -i      Print an ID string and exit",
      "       -v      Turn up verbosity (repeat flag for more chatter)",
      NULL,
   };

   for(line = msg;*line != NULL;line++) {
      fprintf(stderr,"%s\n",*line);
   }
}	

void chain_model_output(char *job, CHAIN *objclist, THOBJCTYPE otype) {

THOBJC *objc;
int iobjc, nobjc;
nobjc = shChainSize(objclist);
for (iobjc = 0; iobjc < MIN(nobjc, N_OUTPUT_OBJC); iobjc++) {
	objc = shChainElementGetByPos(objclist, iobjc);
	model_output(job, objc);
}
printf("--- %s: end of output --- \n", job);
return;
}


void model_output(char *job, THOBJC *objc) {

CHAIN *thprops;
int nmodel, imodel;
static char *line = NULL;
if (line == NULL) line = thCalloc(MX_STRING_LEN, sizeof(char));

thprops = objc->thprop;
nmodel = shChainSize(thprops);
for (imodel = 0; imodel < nmodel; imodel++) {
	SCHEMA *s;
	SCHEMA_ELEM *se;
	int n, i;
	char *rname;
	THPIX *value;
	THPROP *prop;
	prop = shChainElementGetByPos(thprops, imodel);	 		
	s = shSchemaGetFromType(shTypeGetFromName(prop->pname));
	n = s->nelem;
	printf("--- %s parameters for (objc: %d) (model: '%s') --- \n", 
		job, objc->thid, prop->mname);
	for  (i = 0; i < n; i++) {
		se = s->elems + i;
		rname = se->name;
		value = shElemGet(prop->value, se, NULL);
		if (strlen(line) != 0) {
			sprintf(line, "%s, %15s = %.5e", line, rname, *value);
		} else {
			sprintf(line,"%15s = %.5e", rname, *value);
		}
		if (((i + 1)%TABLEROW == 0) || i == n - 1) {
			printf("%s\n", line);
			strcpy(line, "");
		}
	}
}
return;
}

RET_CODE tweak(void *p, THPIX e_tweak, char *pname, char **rnames, int rcount, CHAIN *chain,
		TWEAKFLAG tflag) {
char *name = "tweak";

/* this assumes that all records (schema_elems) are of type THPIX */

SCHEMA *s;
SCHEMA_ELEM *se;

if (p == NULL || pname == NULL || strlen(pname) == 0) {
	thError("%s: ERROR - void or unknown input parameter cannot be tweaked", name);
	return(SH_GENERIC_ERROR);
}
TYPE ptype;
ptype = shTypeGetFromName(pname);
s = shSchemaGetFromType(ptype);
if (s == NULL) {
	thError("%s: ERROR - unknown parameter type '%s'", name, pname);
	return(SH_GENERIC_ERROR);
}
if (rcount < 0) {
	thError("%s: ERROR - negative record count (%d)", name, rcount);
	return(SH_GENERIC_ERROR);
}
if (rcount > 0 && rnames == NULL) {
	thError("%s: ERROR - null record name, while record count is positive (%d)", name, rcount);
	return(SH_GENERIC_ERROR);
}


RET_CODE status;
int i;
void *x;
#define YTYPE FL32
YTYPE y;
char *ytype = "FL32";

if (rcount == 0) {
	int n; 
	int onlypos = 0;
	n = s->nelem;
	for (i = 0; i < n; i++) {
		se = s->elems + i;
		if (se == NULL) {
			thError("%s: WARNING - could not find %d-th record in '%s'", name, i, pname);
		} else {
			onlypos = !strcmp(se->name, "re");	
			x = shElemGet(p, se, NULL);
			status = thqqTrans(x, se->type, &y, ytype);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not read record '%s' of type '%s' in '%s' into a double variable", name, se->name, se->type, pname);
				return(status);
			}
			int poscheck = 1;
			while (poscheck) {
				if (tflag == FRACTIONAL) {
					y *= ((YTYPE) 1.00 + ((YTYPE) phGaussdev()) * (YTYPE) e_tweak);
				} else if (tflag == ABSOLUTE) {
					y += ((YTYPE) phGaussdev()) * (YTYPE) e_tweak;
				}
				poscheck = onlypos && (y > (YTYPE) 0);
			}
			status = thqqTrans(&y, ytype, x, se->type);
 			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not transform '%s' data types into '%s' for record '%s' of '%s'", name, ytype, se->type, se->name, pname);
				return(status);
			}
		}
	}
} else {	
	char *rname;
	int onlypos = 0;
	for (i = 0; i < rcount; i++) {
		rname = rnames[i];
		onlypos = (!strcmp(rname, "re"));
		if (rname != NULL && strlen(rname) != 0) {
			se = shSchemaElemGetFromType(ptype, rname);
			if (se == NULL) {
				thError("%s: WARNING - could not find record '%s' in '%s'", name, rname, pname);
			} else {
				x = shElemGet(p, se, NULL);
				status = thqqTrans(x, se->type, &y, ytype);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not read record '%s' of type '%s' in '%s' into a working variable of type '%s'", name, se->name, se->type, pname, ytype);
					return(status);
				}
				int poscheck = 1;
				while (poscheck) {
					if (tflag == FRACTIONAL) {
						y *= ((YTYPE) 1.00 + ((YTYPE) phGaussdev()) * (YTYPE) e_tweak);
					} else if (tflag == ABSOLUTE) {
						y += ((YTYPE) phGaussdev()) * (YTYPE) e_tweak;
					}
					poscheck = onlypos && (y > (YTYPE) 0);
				}
				status = thqqTrans(&y, ytype, x, se->type);
 				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not transform '%s' data type into '%s' for record '%s' of '%s'", name, ytype, se->type, se->name, pname);
					return(status);
				}

		}
		}
	}
}
#undef YTYPE 
return(SH_SUCCESS);
}

RET_CODE add_poisson_noise_to_image(REGION *im, CHAIN *ampl) {
char *name = "add_poisson_noise_to_image";

if (im == NULL) {
	thError("%s: ERROR - null input image", name);
	return(SH_GENERIC_ERROR);
}

if (ampl == NULL) {
	thError("%s: ERROR - null amplifier information", name);
	return(SH_GENERIC_ERROR);
}

int nrow, ncol, i, j, n0 = 0, n_ = 0, nplus = 0;
THPIX *row;
FL32 y;

nrow = im->nrow;
ncol = im->ncol;

int ia, na;
na = shChainSize(ampl);
for (ia = 0; ia < na; ia++) {

AMPL *a;
int row0, row1, col0, col1;
THPIX g;
FL32 mu;
a = shChainElementGetByPos(ampl, ia);
if (a == NULL) {
	thError("%s: WARNING - null (AMPL) structure discovered in (ampl) chain", name);
} else {
	row0 = a->row0;
	row1 = a->row1;
	col0 = a->col0;
	col1 = a->col1;
	g = a->gain;	
	printf("%s: amplifier (%d) - (gain = %g)\n", name, ia, g);
	for (i = MAX(row0, 0); i < MIN(nrow, row1); i++) {
		row = im->rows_thpix[i];
		for (j = MAX(0, col0); j < MIN(ncol, col1); j++) {
		y = (FL32) (row[j]);
		if (y > (FL32) 0.0) {
			mu = g * y;
			y = phPoissondev(mu) / g;
			/* 
			y += sqrt(y / g) * ((FL32) phGaussdev());
			*/
			row[j] = y;
			nplus++;
		} else if (y == (THPIX) 0.0) {
			n0++;
		} else {
			n_++;
		}
		}
	}
}
}

if (n0 > 0) printf("%s: (zero pixels: %d) \n", name, n0);
if (n_ > 0) printf("%s: (negative pixels: %d) \n", name, n_);
if (nplus > 0) printf("%s: (positive pixels: %d) \n", name, nplus);

return(SH_SUCCESS);
}

int compare_re(const void *x1, const void *x2) {
	OBJC_IO *y1, *y2;
	shAssert(x1 != NULL);
	shAssert(x2 != NULL);
	y1 = *(OBJC_IO **) x1;
	y2 = *(OBJC_IO **) x2;
	float z1, z2;
	z1 = (float) y1->r_deV[THIS_BAND];
	z2 = (float) y2->r_deV[THIS_BAND];
	int res;
	if (z1 > z2) {
		res = -1;
	} else if (z1 < z2) {
		res = 1;
	} else {
		res = 0;
	}
	return(res);
}

int compare_count(const void *x1, const void *x2) {
	OBJC_IO *y1, *y2;
	shAssert(x1 != NULL);
	shAssert(x2 != NULL);
	y1 = *(OBJC_IO **) x1;
	y2 = *(OBJC_IO **) x2;
	int res;
	if (y1->type == OBJ_GALAXY && y2->type == OBJ_GALAXY) {
		float z1, z2;
		z1 = y1->counts_exp[THIS_BAND] + y1->counts_deV[THIS_BAND];	
		z2 = y2->counts_exp[THIS_BAND] + y2->counts_deV[THIS_BAND];
		if (z1 > z2) {
			res = -1;
		} else if (z1 < z2) {
			res = 1;
		} else {
			res = 0;
		}
	} else if (y1->type == OBJ_GALAXY) {
		res = -1;
	} else if (y2->type == OBJ_GALAXY) {
		res = 1;
	} else {
		res = 0;
	}
	return(res);
}


int fl_compare(const void *a, const void *b) {
	float aa, bb;
	aa = *(float *) a;
	bb = *(float *) b;
	if (aa > bb) return(1);
	if (aa < bb) return(-1);
	if (aa == bb) return(0);
}

int get_adjacency(void *a, void *b) {
	#if RANDOM_ADJ

	int res;
	res = ((phRandom()) % GRAPH_EDGE_CONST == 0);
	return(res);

	#else 

	THOBJC *objc1, *objc2;
	objc1 = (THOBJC *) a;	
	objc2 = (THOBJC *) b;
	THPROP *pa, *pb;
	CHAIN *pas, *pbs;
	pas = objc1->thprop;
	pbs = objc2->thprop;
	int na, nb;
	na = shChainSize(pas);
	nb = shChainSize(pbs);
	char *ma, *aname, *mb, *bname;
	void *va, *vb;
	THPIX xa, ya, ra, xb, yb, rb, dx, dy, d, d2, r;
	#if ADJ_PIXCOUNT
	THPIX ra2, rb2, ca, cb, ta, tb, sa, sb, s, sm = -1.0;
	#endif
	static void **x = NULL, **y = NULL;
	if (x == NULL) x = (void *) thCalloc(4, sizeof(float *));
	if (y == NULL) y = (void *) thCalloc(4, sizeof(float *));	
	int i, j;
	for (i = 0; i < na; i++) {
		int badpa = 0;
		pa = shChainElementGetByPos(pas, i);
		thPropGet(pa, &ma, &aname, &va);
		if (!strcmp(aname, "DEVPARS")) {
			xa = ((DEVPARS *)va)->xc;
			ya = ((DEVPARS *)va)->yc;
			ra = DEV_ADJ_FACTOR * ((DEVPARS *)va)->re + (THPIX) DEV_PSF_MARGIN;
		} else if (!strcmp(aname, "EXPPARS")) {
			xa = ((EXPPARS *)va)->xc;
			ya = ((EXPPARS *)va)->yc;
			ra = EXP_ADJ_FACTOR * ((EXPPARS *)va)->re + (THPIX) EXP_PSF_MARGIN;
		} else if (!strcmp(aname, "STARPARS")) {
			xa = ((STARPARS *) va)->xc;
			ya = ((STARPARS *) va)->yc;
			ra = (THPIX) STAR_PSF_MARGIN;
		} else 	{
			badpa = 1;
		}
		if (!badpa) {	
		for (j = 0; j < nb; j++) {
			int badpb = 0;
			pb = shChainElementGetByPos(pbs, j);
			thPropGet(pb, &mb, &bname, &vb);	
			if (!strcmp(bname, "DEVPARS")) {
				xb = ((DEVPARS *)vb)->xc;
				yb = ((DEVPARS *)vb)->yc;
				rb = DEV_ADJ_FACTOR * ((DEVPARS *)vb)->re + (THPIX) DEV_PSF_MARGIN;
			} else if (!strcmp(bname, "EXPPARS")) {
				xb = ((EXPPARS *)vb)->xc;
				yb = ((EXPPARS *)vb)->yc;
				rb = EXP_ADJ_FACTOR * ((EXPPARS *)vb)->re + (THPIX) EXP_PSF_MARGIN;

			} else if (!strcmp(aname, "STARPARS")) {
				xb = ((STARPARS *) vb)->xc;
				yb = ((STARPARS *) vb)->yc;
				rb = (THPIX) STAR_PSF_MARGIN;
			} else {
				badpb = 1;
			}	
			if (!badpb) {
				#if SQUARE_OVERLAP

				dx = xa - xb;
				dy = ya - yb;
				r = ra + rb;	
				#if ADJ_PIXCOUNT
				if (fabs((float) dx) > r || fabs((float) dy) > r) {
					s = -1.0;
				} else {
			
					float xa1, xa2, xb1, xb2, ya1, ya2, yb1, yb2;
	
					xa1 = xa - ra;
					xa2 = xa + ra;
					xb1 = xb - rb;
					xb2 = xb + rb;
				
					ya1 = ya - ra;
					ya2 = ya + ra;
					yb1 = yb - rb;
					yb2 = yb + rb;
	
					x[0] = &xa1;
					x[1] = &xa2;
					x[3] = &xb1;
					x[4] = &xb2;
				

					y[0] = &ya1;
					y[1] = &ya2;
					y[2] = &yb1;
					y[3] = &yb2;

					qsort(x, 4, sizeof(float), &fl_compare);
					qsort(y, 4, sizeof(float), &fl_compare);

					s = fabs((x[2] - x[1]) * (y[2] - y[1]));
				}
				if (s > 0.0) sm += s;
				#else
					if (fabs((float) dx) > r || fabs((float) dy) > r) return(1);
				#endif




				#else


				dx = xa - xb;
				dy = ya - yb;
				d2 = pow(dx, 2.0) + pow(dy, 2.0);
				d = pow(d2, 0.5);
				r = ra + rb;	
				#if ADJ_PIXCOUNT
				ra2 = pow(ra, 2.0);
				rb2 = pow(rb, 2.0);
				ca = (ra2 + d2 - rb2) / (2.0 * ra * d);
				cb = (rb2 + d2 - ra2) / (2.0 * rb * d);
				if (d > r) {
					s = -1.0;
				} else if (fabs((float) ca) <= 1.0 && fabs((float) cb) <= 1.0) {			
					ta = acos(ca);
					tb = acos(cb);
					sa = ra2 * (ta - sin(ta) * ca);
					sb = rb2 * (tb - sin(tb) * cb);
					s = sa + sb;
				} else if (ra <= rb) {
					s = PI * ra2;
				} else if (ra > rb) {
					s = PI * rb2;
				}
				if (s > 0.0) sm += s;
				#else
					if (d < r) return(1);
				#endif

				#endif
			}	
		}
		}		
	}

	#if ADJ_PIXCOUNT
	return((int) (sm + 1.0));
	#else
	return(0);
	#endif

	#endif	
	
}

RET_CODE get_objc_xy(THOBJC *objc, THPIX *xx, THPIX *yy) {
	char *procname = "get_objc_xy";
	THPROP *p;
	CHAIN *ps;
	ps = objc->thprop;
	int n;
	n = shChainSize(ps);
	char *m, *name;
	void *v;
	THPIX x, y;
	int i;
	for (i = 0; i < n; i++) {
		int badpa = 0;
		p = shChainElementGetByPos(ps, i);
		thPropGet(p, &m, &name, &v);
		if (!strcmp(name, "DEVPARS")) {
			x = ((DEVPARS *)v)->xc;
			y = ((DEVPARS *)v)->yc;
		} else if (!strcmp(name, "EXPPARS")) {
			x = ((EXPPARS *)v)->xc;
			y = ((EXPPARS *)v)->yc;
		} else {
			badpa = 1;
		}
		if (!badpa) {
			if (xx != NULL) *xx = x;
			if (yy != NULL) *yy = y;
			return(SH_SUCCESS);
		}

	}			

thError("%s: ERROR - does not recognize variable of type '%s' for model '%s'", procname, name, m);
return(SH_GENERIC_ERROR);

}



int pix_count(THOBJC *objc) {	
	THPROP *p;
	CHAIN *ps;
	ps = objc->thprop;
	int n;
	n = shChainSize(ps);
	char *m, *name;
	void *v;
	THPIX x, y, r, s = -1.0, sm = -1.0;
	int i;
	for (i = 0; i < n; i++) {
		int badpa = 0;
		p = shChainElementGetByPos(ps, i);
		thPropGet(p, &m, &name, &v);
		if (!strcmp(name, "DEVPARS")) {
			x = ((DEVPARS *)v)->xc;
			y = ((DEVPARS *)v)->yc;
			r = DEV_ADJ_FACTOR * ((DEVPARS *)v)->re + (THPIX) DEV_PSF_MARGIN;
		} else if (!strcmp(name, "EXPPARS")) {
			x = ((EXPPARS *)v)->xc;
			y = ((EXPPARS *)v)->yc;
			r = EXP_ADJ_FACTOR * ((EXPPARS *)v)->re + (THPIX) EXP_PSF_MARGIN;
		} else if (!strcmp(name, "STARPARS")) {
			x = ((STARPARS *) v)->xc;
			y = ((STARPARS *) v)->yc;
			r = (THPIX) INNER_PSF_SIZE;
		} else {
			badpa = 1;
		}
		if (!badpa) {
			#if SQUARE_OVERLAP
			s = 4.0 * pow(r, 2.0);
			#else
			s = PI * pow(r, 2.0);
			#endif
		} else {
			s = -1.0;
		}
		if (s > 0.0) sm += s;
	}

return((int) (sm + 1.0));
}

RET_CODE do_memory(CHAIN *objclist, STRATEGY *strategy) {

	char *name = "do-memory";
	if (objclist == NULL || strategy == NULL) {
		thError("%s: ERROR - insufficient input", name);
		return(SH_GENERIC_ERROR);
	}
	int nobjc = shChainSize(objclist);
	if (nobjc <= 1) {
		thError("%s: WARNING - memory strategy can be planned only if (nobjc > 1) found (nobj = %d) instead", name, nobjc);
		return(SH_SUCCESS);
	}
	int nnode = nobjc - 1;

	#if DO_GREEDY
	/* Setting up the adjacency matrix in MEMFL for algorithm package */
	/* --- */
	RET_CODE status;
	ADJ_MATRIX *adj2 = thAdjMatrixNew();
	status = thAdjMatrixRenew(adj2, nnode);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not renew (adj_matrix)", name);
		return(-1);
	}
	MEMFL **alg_adj = adj2->matrix;
	int i, j;
	for (i = 0; i < nnode; i++) {
		THOBJC *objc_i;
		MEMFL *alg_adj_i = alg_adj[i];
		objc_i = shChainElementGetByPos(objclist, i + 1);
		alg_adj_i[i] = (MEMFL) pix_count(objc_i); 
		for (j = 0; j < i; j++) {
			MEMFL *alg_adj_j = alg_adj[j];
			THOBJC *objc_j;
			objc_j = shChainElementGetByPos(objclist, j + 1);
			MEMFL e_ij = (MEMFL) get_adjacency(objc_i, objc_j); 
			alg_adj_i[j] = e_ij;
			alg_adj_j[i] = e_ij;
		}
	}

	/* 
	status = output_adj_matrix(adj2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not output adjacency matrix", name);
		return(status);
	}
	*/
	
	ALGORITHM *alg;
	alg = thAlgorithmNew();
	alg->memory_total = ((MEMDBLE) THMEMORY * (MEMDBLE) MEGABYTE) / (MEMDBLE) sizeof(THPIX);
	/* designing the algorithm */
	printf("%s: using the greedy algorithm to plan order of loading objects \n", 
		name);
	status = design_algorithm(adj2, alg, INNER_PRODUCTS, INITRUN);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not design the algorithm properly", name);
		return(status);
	}
	/* 
	status = output_algorithm(alg, adj2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not output algorithm properly", name);
		return(status);
	}
	*/
	/* --- */
	#endif


	/* name of the output files */
	char *adj_method = strategy->adj_method;
	char *gfile_prefix = strategy->gfile_prefix;
	char *clusterfile_prefix = strategy->clusterfile_prefix;
	char *clusterlog_prefix = strategy->clusterfile_prefix;

	char *gfile = thCalloc(MX_STRING_LEN, sizeof(char));
	char *clusterfile = thCalloc(MX_STRING_LEN, sizeof(char));
	char *clusterlog = thCalloc(MX_STRING_LEN, sizeof(char));

	return(SH_SUCCESS);	
}


RET_CODE output_memory_stat(LSTRUCT *lstruct, char *fname1, char *fname2) {
char *name = "output_memory_stat";

static LIMPACK *impack = NULL;
if (impack == NULL) impack = thLimpackNew();

int i;
FILE *file;
THOBJCTYPE objctype;
char *objcname;
MEMFL memory;
THOBJCID objcid;
THPIX *xx = NULL, re = 0.0, e = 0.0, phi = 0.0;

LMACHINE *map = lstruct->lmodel->lmachine;
char **mnames = map->mnames;
char **pnames = map->pnames;
void **ps = map->ps;
THOBJC **objcs = (THOBJC **) map->objcs;
int nmodel = map->namp;
if (fname1 != NULL && strlen(fname1) != 0) {
	printf("%s: outputting memory statistics for models to '%s' \n", name, fname1);
	file = fopen(fname1, "w");
	fprintf(file, "%10s %10s %10s %10s %10s %10s %10s %10s \n", 
		"memory", "model", "objcname", "objctype", "objcid", "re", "e", "phi");
	for (i = 0; i < nmodel; i++) {
		char *mname = mnames[i];
		char *pname = pnames[i];
		void *p = ps[i];
		re = VALUE_IS_BAD;
		e = VALUE_IS_BAD;
		phi = VALUE_IS_BAD;
		TYPE t = shTypeGetFromName(pname);
		SCHEMA_ELEM *se = shSchemaElemGetFromType(t, "re");
		if (se != NULL) {
			xx = shElemGet(p, se, NULL);
			if (xx != NULL) re = *xx;
		} 
		se = shSchemaElemGetFromType(t, "e");
		if (se != NULL) {
			xx = shElemGet(p, se, NULL);
			if (xx != NULL) e = *xx;
		}
		se = shSchemaElemGetFromType(t, "phi");
		if (se != NULL) {
			xx = shElemGet(p, se, NULL);
			if (xx != NULL) phi = *xx;
		}
		THOBJC *objc = objcs[i];
		objctype = objc->thobjctype;
		RET_CODE status = thObjcNameGetFromType(objctype, &objcname);		
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (objcname) for (objctype)", name);
			return(status);
		}
		THOBJCID objcid = objc->thid;
		status = thConstructImagePack(lstruct, i, impack);	
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not construct impack for model (%d) '%s'", name, i, mname);
			return(status);
		}
		status = thLimpackGetMemory(impack, &memory);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (impack) memory for model (%d) '%s'", name, i, mname);
			return(status);
		}
		fprintf(file, "%10.4g %10s %10s %10d %10d %10.4g %10.4g %10.4g \n", 
			(float) memory, mname, objcname, (int) objctype, (int) objcid, re, e, phi);
	}
	fclose(file);
}

int nobjc;
if (fname2 != NULL && strlen(fname2) != 0) {
	printf("%s: outputying memory statistics for objects to '%s' \n", name, fname2);
	file = fopen(fname2, "w");
	for (i = 0; i < nobjc; i++) {
		fprintf(file, "%g, %s, %d, %d \n", (float) memory, objcname, (int) objctype, (int) objcid);
	}
	fclose(file);
}
return(SH_SUCCESS);
}

RET_CODE output_reg_props(REGION *reg) {
#if OUTPUT_REG_PROPS
char *name = "output_reg_props";
if (reg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
THPIX **rows, *row_i;
rows = reg->rows_thpix;
int i, j;
int nplus = 0, nminus = 0, nzero = 0;
for (i = 0; i < reg->nrow; i++) {
	row_i = rows[i];
	for (j = 0; j < reg->ncol; j++) {
		if (row_i[j] == (THPIX) 0.0) {
			nzero++;
		} else if (row_i[j] > (THPIX) 0.0) {
			nplus++;
		} else {
			nminus++;
		}
	}
}

int npix = reg->nrow * reg->ncol;
printf("%s: properties for region '%s' \n", name, reg->name);
printf("%s: n(0) = %d (%6.2f %%) \n", name, nzero, 100 * (float) nzero / (float) npix);
printf("%s: n(+) = %d (%6.2f %%) \n", name, nplus, 100 * (float) nplus / (float) npix);
printf("%s: n(-) = %d (%6.2f %%) \n", name, nminus, 100 * (float) nminus / (float) npix);
printf("%s: n pixel = %d \n", name, npix);
#endif
return(SH_SUCCESS);

}

RET_CODE tweak_init_pars(LSTRUCT *l, CHAIN *objclist) {
char *name = "tweak_init_pars";

/* add all the models in the object to the map */
static char **rnames = NULL, **iornames = NULL, **mnames = NULL;
char *mname;
int nrname = 10;
int nm_max = 2;
int i;
if (rnames == NULL) {
	mnames = thCalloc(nm_max, sizeof(char *));
	rnames = thCalloc(nrname, sizeof(char*));
	iornames = thCalloc(nrname, sizeof(char *));
	for (i = 0; i < nrname; i++) {
		mnames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
		rnames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
		iornames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
	}
}

int pos = 0;
SKYPARS *ps;
THOBJC *objc = NULL;
TYPE ptype;
char *pname;
OBJCPARS *pg;

int ii, j = 0;
ii = j;
strcpy(rnames[j++], "I");
#if FIT_SHAPE 
int ishape, jshape, nshape;
ishape = j;
if (EllDesc == ALGEBRAIC) {
	strcpy(rnames[j++], "a");
 	strcpy(rnames[j++], "b");
	strcpy(rnames[j++], "c");
} else if (EllDesc == CANONICAL) {
	#if FIT_RE
	strcpy(rnames[j++], "re");
	#endif
	#if FIT_E
	strcpy(rnames[j++], "e");
	#endif
	#if FIT_PHI
	strcpy(rnames[j++], "phi");
	#endif
} else if (EllDesc == SUPERCANONICAL || EllDesc == UBERCANONICAL) {
	#if FIT_RE
	strcpy(rnames[j++], "re"); 
	#endif
	#if FIT_E
	strcpy(rnames[j++], "E");
	#endif
	#if FIT_PHI
	strcpy(rnames[j++], "phi");
	#endif
}	
jshape = j;
nshape = j;
#endif
#if FIT_CENTER
int icenter, jcenter, ncenter;
icenter = j;
strcpy(rnames[j++], "xc");  
strcpy(rnames[j++], "yc");
jcenter = j;
ncenter = (jcenter - icenter);
#endif

int ntweak = 0;
if (TWEAK_ALL) {
	ntweak = shChainSize(objclist);
} else {
	ntweak = MIN(1+ NFITOBJC, shChainSize(objclist));
}
RET_CODE status;
for (pos = 0; pos < ntweak; pos++) {

	THOBJCTYPE objctype;
	char *objcname;
	objc = shChainElementGetByPos(objclist, pos);
	objctype = objc->thobjctype;
	thObjcNameGetFromType(objctype, &objcname);
	if (objctype == SKY_OBJC) {

	/* sky */ 
	thObjcGetIopar(objc, (void **) &ps, &ptype);
	pname = shNameGetFromType(ptype);	
	if (ps != NULL) {
	status = tweak((void *) ps, SKY_FRACTIONAL_TWEAK, pname, NULL, 0, objclist, ABSOLUTE);
	if (status != SH_SUCCESS) {
		printf("%s: ERROR - could not tweak '%s' \n", name, pname);
		return(-1);
	}
	} else {
		printf("%s: WARNING - null parameter '%s' in object (objc: %d) - no tweaking \n", name, pname, objc->thid);
	}
	} else if (!strcmp(objcname, "DEVGALAXY") || !strcmp(objcname, "EXPGALAXY") || !strcmp(objcname, "GAUSSIAN") || (!strcmp(objcname, "GALAXY"))) {

	/* galaxy */
	OBJC_IO *ph;
	ph = ((PHPROPS *) objc->phprop)->phObjc;
	thObjcGetIopar(objc, (void **) &pg, &ptype);
	pname = shNameGetFromType(ptype);

	mname = mnames[0];
	if (!strcmp(objcname, "DEVGALAXY")) strcpy(mname, "deV");
	if (!strcmp(objcname, "EXPGALAXY")) strcpy(mname, "Exp");
	if (!strcmp(objcname, "GAUSSIAN")) strcpy(mname, "gaussian");
	if (!strcmp(objcname, "GALAXY"))  {
		strcpy(mnames[0], "deV");
		strcpy(mnames[1], "Exp");
	}
	if (!strcmp(objcname, "CDCANDIDATE")) {
		strcpy(mnames[0], "deV");
		strcpy(mnames[1], "Exp2");
	}

	if (pos < N_OUTPUT_OBJC) {
	printf("%s: tweaking (objc: %d): ", name, objc->thid);
	for (i = 0; i < j; i++) {
		sprintf(iornames[i], "%s_%s", rnames[i], mname);
		printf("'%s' ", iornames[i]);
	}
	printf("\n");
	}
	if (pg != NULL) {

	THPIX e_tweak;
	#if FORCE_INIT_PAR
	e_tweak = I_TWEAK;
	status = tweak((void *)pg, e_tweak, pname, iornames + ii, 
			1, objclist, FRACTIONAL);
	#else
	e_tweak = 0.0;
	if (!strcmp(mname, "deV")) {
		e_tweak = I_TWEAK * ph->counts_deVErr[band] / PI / pow(DEV_RE_SMALL + ph->r_deV[band], 2);
		if (pos < N_OUTPUT_OBJC) printf("%s: i_tweak('%s') = %g; countErr = %g, rErr = %g \n", 
		name, mname, e_tweak, ph->counts_deVErr[band], ph->r_deV[band]);} 
	else if (!strcmp(mname, "Exp")) {
		e_tweak = I_TWEAK * ph->counts_expErr[band] / PI / pow(EXP_RE_SMALL + ph->r_exp[band], 2);
		
		if (pos < N_OUTPUT_OBJC) printf("%s: i_tweak('%s') = %g; countErr = %g, rErr = %g \n", 
		name, mname, e_tweak, ph->counts_expErr[band], ph->r_exp[band]);
	}
	status = tweak((void *)pg, e_tweak, pname, iornames + ii, 
			1, objclist, ABSOLUTE);
	#endif
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not tweak '%s'", name, pname);
		return(status);
	} 
	#if FIT_SHAPE
	if (EllDesc == SUPERCANONICAL || EllDesc == UBERCANONICAL) {
		status = tweak((void *) pg, SHAPE_TWEAK, pname, iornames + ishape, 
		nshape, objclist, FRACTIONAL);
	} else {
		status = tweak((void *) pg, SHAPE_TWEAK, pname, iornames + ishape, 
		nshape, objclist, FRACTIONAL);
	}
	if (EllDesc == ALGEBRAIC) {
		pg->b_gaussian *= pow(1.0 - BETA_SHAPE, 2);
	}
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not tweak '%s'", name, pname);
		return(status);
	} 

	#endif
	#if FIT_CENTER
	for (i = 0; i < ncenter; i++) {
		e_tweak = 0.0;
		#if FORCE_INIT_PAR
		e_tweak = CENTER_TWEAK;
		#else 
		if (!strcmp(rnames[i + icenter], "xc")) {
			e_tweak = CENTER_TWEAK * ph->rowcErr[band];
		} else if (!strcmp(rnames[i + icenter], "yc")) {
			e_tweak = CENTER_TWEAK * ph->colcErr[band];
		}
		#endif
		status = tweak((void *) pg, e_tweak, pname, iornames + icenter + i, 
				1, objclist, ABSOLUTE);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not tweak '%s'", name, pname);
			return(status);
		}
	} 
	#endif
	
	if (!strcmp(objcname, "GALAXY")) {

	mname = mnames[1];
	if (pos < N_OUTPUT_OBJC) {
	printf("%s: tweaking (objc: %d): ", name, objc->thid);	
	for (i = 0; i < j; i++) {
		sprintf(iornames[i], "%s_%s", rnames[i], mname);
		printf("'%s' ", iornames[i]);
	}
	printf("\n");
	}

	e_tweak = 0.0;
	#if FORCE_INIT_PAR
	e_tweak = I_TWEAK;
	status = tweak((void *)pg, e_tweak, pname, iornames + ii, 
		1, objclist, FRACTIONAL);
	#else
	if (!strcmp(mname, "deV")) {
		e_tweak = I_TWEAK * ph->counts_deVErr[band] / PI / pow(DEV_RE_SMALL + ph->r_deV[band], 2);	
		if (pos < N_OUTPUT_OBJC) printf("%s: i_tweak('%s') = %g; countErr = %g, rErr = %g \n", 
		name, mname, (float) e_tweak, (float) ph->counts_deVErr[band], (float) ph->r_deV[band]);
	} else if (!strcmp(mname, "Exp")) {
		e_tweak = I_TWEAK * ph->counts_expErr[band] / PI / pow(EXP_RE_SMALL + ph->r_exp[band], 2);
		if (pos < N_OUTPUT_OBJC) printf("%s: i_tweak('%s') = %g; countErr = %g, rErr = %g \n", 
		name, mname, (float) e_tweak, (float) ph->counts_expErr[band], (float) ph->r_exp[band]);
	}
	status = tweak((void *)pg, e_tweak, pname, iornames + ii, 1, objclist, ABSOLUTE);
	#endif
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not tweak '%s'", name, pname);
		return(status);
	} 
	#if FIT_SHAPE
	if (EllDesc == SUPERCANONICAL || EllDesc == UBERCANONICAL) {
		status = tweak((void *) pg, SHAPE_TWEAK, pname, iornames + ishape, nshape, objclist, FRACTIONAL);
	} else {
		status = tweak((void *) pg, SHAPE_TWEAK, pname, iornames + ishape, nshape, objclist, FRACTIONAL);
	}
	if (EllDesc == ALGEBRAIC) {
		pg->b_gaussian *= pow(1.0 - BETA_SHAPE, 2);
	}
	#endif
	#if FIT_CENTER
	for (i = 0; i < ncenter; i++) {
		e_tweak = 0.0;
		#if FORCE_INIT_PAR
		e_tweak = CENTER_TWEAK;
		#else
		if (!strcmp(rnames[i + icenter], "xc")) {
			e_tweak = CENTER_TWEAK * ph->rowcErr[band];
		} else if (!strcmp(rnames[i + icenter], "yc")) {
			e_tweak = CENTER_TWEAK * ph->colcErr[band];
		}
		#endif
		status = tweak((void *) pg, e_tweak, pname, iornames + icenter + i, 
				1, objclist, ABSOLUTE);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not tweak '%s'", name, pname);
			return(status);
		}
	} 
	#endif

	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not tweak '%s'", name, pname);
		return(status);
	}

	}	
	} else {
		printf("%s: WARNING - null parameter '%s' in object (objc: %d) - no tweaking \n", name, pname, objc->thid);
	}

	}
	}
	/* dumping parameters into their respective location */ 
 
/* 	
	#if FORCE_INIT_PAR 
*/
	printf("%s: assigning parameter and amplitude arrays in (mapmachine) from tweaked IO parameters \n", name);
	thLDumpAmp(l, IOTOMODEL);
	thLDumpPar(l, IOTOMODEL);

	thLDumpAmp(l, IOTOX);
	thLDumpPar(l, IOTOX);
	thLDumpPar(l, IOTOXN);
/* 
	#else

	printf("%s: assigning SDSS parameters to (mapmachine) arrays", name);
	thLDumpAmp(l, MODELTOX);
	thLDumpPar(l, MODELTOX);

	#endif
*/


	printf("%s: assigning values to (a, p) from updated (aN, pN) \n", name);
	status = UpdateXFromXN(l, APSECTOR);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update parameters to the tweaked values", name);
		return(status);
	}

return(SH_SUCCESS);
}


void my_holdup(void) {
	int i;
	while (1 == 1) {
		i++;
	}
return;
}

