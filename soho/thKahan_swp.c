
#include "thKahan.h"

static KAHANFL *kahan_arr = NULL, *kahan_arr2 = NULL;
static int kahan_n = 0, kahan_index = 0;
static int init = 0;

static KAHANFL kahan_sum = 0.0, kahan_error = 0.0;
static KAHANFL kahan_c = 0.0, kahan_cc = 0.0, kahan_cs = 0.0, kahan_ccs = 0.0;

RET_CODE thKahanInit(int n) {
char *name = "thKahanInit";
if (init > 0) {
	thError("%s: ERROR - Kahan package already initiated", name);
	return(SH_GENERIC_ERROR);
} else if (kahan_n > 0) {
	thError("%s: ERROR - Kahan packaged not initiated but found (kahan_n = %d)", name, kahan_n);
	return(SH_GENERIC_ERROR);
} else if (kahan_arr != NULL || kahan_arr2 != NULL) {
	thError("%s: ERROR - Kahan package not initiated but found (kahan_arr != NULL)", name);
	return(SH_GENERIC_ERROR);
}
if (n <= 0) {
	thError("%s: ERROR - unable to support initiation with (n = %d)", name, n);
	return(SH_GENERIC_ERROR);
}
kahan_arr = shCalloc(n, sizeof(KAHANFL));
kahan_arr2 = shCalloc(n, sizeof(KAHANFL));
kahan_n = 0;
kahan_index = 0;
init = 1;

return(SH_SUCCESS);
}

RET_CODE thKahanEnd() {
char *name = "thKahanEnd";
if (init == 0) {
	thError("%s: ERROR - Kahan package not initiated", name);
	return(SH_GENERIC_ERROR);
} else if (kahan_n <= 0 || kahan_index < 0) {
	thError("%s: ERROR - Kahan package initiated improperly (n, index) = (%d, %d)", name, kahan_n, kahan_index);
	return(SH_GENERIC_ERROR);
} else if (kahan_arr == NULL || kahan_arr2 == NULL) {
	thError("%s: ERROR - Kahan package initiated improperly (arr, arr2) = (%p, %p)", name, kahan_arr, kahan_arr2);
	return(SH_GENERIC_ERROR);
} 

shFree(kahan_arr);
shFree(kahan_arr2);

kahan_arr = NULL;
kahan_arr2 = NULL;
kahan_n = 0;
kahan_index = 0;
init = 0;

return(SH_SUCCESS);
}

void thKahanInsertNext(KAHANFL x) {
shAssert(kahan_index < kahan_n);
shAssert(kahan_arr != NULL);
kahan_arr[kahan_index] = x;
kahan_index++;
}

RET_CODE thKahanFree() {
char *name = "thKahanFree";
if (init == 0) {
	thError("%s: ERROR - Kahan package not initiated", name);
	return(SH_GENERIC_ERROR);
} else if (kahan_n <= 0) {
	thError("%s: ERROR - Kahan package not initiated properly (n = %d)", name, kahan_n);
	return(SH_GENERIC_ERROR);
} else if (kahan_arr == NULL || kahan_arr2 == NULL) {
	thError("%s: ERROR - Kahan package not initiated properly (arr, arr2) = (%p, %p)", name, kahan_arr, kahan_arr2);
	return(SH_GENERIC_ERROR);
}

memset(kahan_arr, '\0', kahan_n * sizeof(KAHANFL));
memset(kahan_arr2, '\0', kahan_n * sizeof(KAHANFL));
kahan_index = 0;

return(SH_SUCCESS);
}

void thKahanAdd(KAHANFL x) {

/* source:
 * A generalized Kahan-Babuˇska-Summation-Algorithm
 * Andreas Klein April 21, 2005
 */

#if 0
/* this is from the paper above */
/*
t = kahan_sum + x;
if (fabs(kahan_sum) >= fabs(x)) {
	KAHANFL y = (kahan_sum - t);
	shAssert(!isinf(y) && !isnan(y));
	KAHANFL z = y + x;
	shAssert(!isnan(z) && !isinf(z));
	kahan_c = kahan_c + z;
} else {
	KAHANFL y = (x - t);
	shAssert(!isinf(y) && !isnan(y));
	z = y + kahan_sum;
	shAssert(!isnan(z) && !isnan(z));
	kahan_c = kahan_c + z;
}
kahan_sum = t;
*/

/* this is from wikipedia */
 
KAHANFL t = kahan_sum + x;
if (fabs(kahan_sum) >= fabs(x)) {
	KAHANFL negative_x = (kahan_sum - t);
	shAssert(!isinf(negative_x) && !isnan(negative_x));
	kahan_error += negative_x + x;
} else {
	KAHANFL negative_sum = (x - t);
	shAssert(!isinf(negative_sum) && !isnan(negative_sum));
	kahan_error += negative_sum + kahan_sum;
}       

kahan_sum = t;

#else

KAHANFL t = kahan_sum + x;
if (fabs(kahan_sum) >= fabs(x)) {
	KAHANFL y = (kahan_sum - t);
	shAssert(!isinf(y) && !isnan(y));
	kahan_c = y + x;
} else {
	KAHANFL y = (x - t);
	shAssert(!isinf(y) && !isnan(y));	
	kahan_c = y + kahan_sum;
}

kahan_sum = t;
t = kahan_cs + kahan_c;
if (fabs(kahan_cs) >= fabs(kahan_c)) {
	KAHANFL y = (kahan_cs - t);
	shAssert(!isinf(y) && !isnan(y));
	kahan_cc = y + kahan_c;
} else {
	KAHANFL y = kahan_c - t;
	shAssert(!isinf(y) && !isnan(y));
      	kahan_cc = y + kahan_cs;
}
kahan_cs = t;
kahan_ccs = kahan_ccs + kahan_cc;
#endif

return;
}

void thKahanAddVerbose(KAHANFL x, char *message) {

/* source:
 * A generalized Kahan-Babuˇska-Summation-Algorithm
 * Andreas Klein April 21, 2005
 */

#if 0
/* this is from the paper above */
/*
t = kahan_sum + x;
if (fabs(kahan_sum) >= fabs(x)) {
	KAHANFL y = (kahan_sum - t);
	shAssert(!isinf(y) && !isnan(y));
	KAHANFL z = y + x;
	shAssert(!isnan(z) && !isinf(z));
	kahan_c = kahan_c + z;
} else {
	KAHANFL y = (x - t);
	shAssert(!isinf(y) && !isnan(y));
	z = y + kahan_sum;
	shAssert(!isnan(z) && !isnan(z));
	kahan_c = kahan_c + z;
}
kahan_sum = t;
*/

/* this is from wikipedia */
 
KAHANFL t = kahan_sum + x;
if (fabs(kahan_sum) >= fabs(x)) {
	KAHANFL negative_x = (kahan_sum - t);
	shAssert(!isinf(negative_x) && !isnan(negative_x));
	kahan_error += negative_x + x;
} else {
	KAHANFL negative_sum = (x - t);
	shAssert(!isinf(negative_sum) && !isnan(negative_sum));
	kahan_error += negative_sum + kahan_sum;
}       

kahan_sum = t;

#else

printf("KAHAN - before adding x = %50.25Lg, kahan_sum = %50.25Lg, kahan_c = %50.25Lg, kahan_cc = %50.25Lg, kahan_cs = %50.25Lg, kahan_ccs = %50.25Lg \n, message = %s",
	(long double) x, (long double) kahan_sum, (long double) kahan_c, (long double) kahan_cc, (long double) kahan_cs, (long double) kahan_ccs, message);



KAHANFL t = kahan_sum + x;
if (fabs(kahan_sum) >= fabs(x)) {
	KAHANFL y = (kahan_sum - t);
	shAssert(!isinf(y) && !isnan(y));
	kahan_c = y + x;
} else {
	KAHANFL y = (x - t);
	shAssert(!isinf(y) && !isnan(y));	
	kahan_c = y + kahan_sum;
}

kahan_sum = t;
t = kahan_cs + kahan_c;
if (fabs(kahan_cs) >= fabs(kahan_c)) {
	KAHANFL y = (kahan_cs - t);
	shAssert(!isinf(y) && !isnan(y));
	kahan_cc = y + kahan_c;
} else {
	KAHANFL y = kahan_c - t;
	shAssert(!isinf(y) && !isnan(y));
      	kahan_cc = y + kahan_cs;
}
kahan_cs = t;
kahan_ccs = kahan_ccs + kahan_cc;

printf("KAHAN - after adding x = %50.25Lg, kahan_sum = %50.25Lg, kahan_c = %50.25Lg, kahan_cc = %50.25Lg, kahan_cs = %50.25Lg, kahan_ccs = %50.25Lg, message = %s \n",
	(long double) x, (long double) kahan_sum, (long double) kahan_c, (long double) kahan_cc, (long double) kahan_cs, (long double) kahan_ccs, message);

#endif

return;
}



void thKahanZero() {
kahan_sum = (KAHANFL) 0;
kahan_error = (KAHANFL) 0;
kahan_c = (KAHANFL) 0;
kahan_cs = (KAHANFL) 0;
kahan_ccs = (KAHANFL) 0;
return;
}

void thKahanGet(KAHANFL *sum, KAHANFL *error, KAHANFL *total) {
if (sum != NULL) *sum = kahan_sum;
if (error != NULL) *error = kahan_error + kahan_cs + kahan_ccs;
if (total != NULL) *total = (kahan_sum + kahan_error + kahan_cs + kahan_ccs);
return;
}

void thKahanGetVerbose(KAHANFL *sum, KAHANFL *error, KAHANFL *total) {
KAHANFL kahan_total = (kahan_sum + kahan_error + kahan_cs + kahan_ccs);
if (sum != NULL) *sum = kahan_sum;
if (error != NULL) *error = kahan_error + kahan_cs + kahan_ccs;
if (total != NULL) *total = (kahan_sum + kahan_error + kahan_cs + kahan_ccs);
printf("KAHAN - outcomes: total = %50.25Lg, kahan_sum = %50.25Lg, kahan_cs = %50.25Lg, kahan_ccs = %50.25Lg \n", (long double) kahan_total, (long double) kahan_sum, (long double) kahan_cs, (long double) kahan_ccs);
return;
}

void thKahanAddProduct2(KAHANFL a, KAHANFL b) {





}
void thKahanAddProduct3(KAHANFL a1, KAHANFL a2, KAHANFL a3) {

KAHANFL factor = pow((KAHANFL) 2.0, (KAHANFL) SPLIT_EXPONENT) + (KAHANFL) 1.0;

KAHANFL p1 = a1;
KAHANFL e1 = 0

p2 = p1 · a2

C = (factor · p1)
p11 = (C − (C − p1))
p12 = (p1 − p11)

C = factor · a2
a21 = (C − (C − a2))
a22 = (a2 − a21)

q2 = (p12 * a22 −(((p2−p11 * a21)−p12 * a21)−p11 * a22))

e2 = (e1 * a2 + q2)

/* calculating [p3, q3] */
p3 = p2 · a3

C = (factor · p2)
p21 = (C − (C − p2))
p22 = (p2 − p21)

C = factor · a3
a31 = (C − (C − a3))
a32 = (a3 − a31)

q3 = (p22 * a32 −(((p3−p21 * a31)−p22 * a31)−p21 * a32))


e3 = (e2 * a3 + q3) 

res = fl(pn + en)



}
