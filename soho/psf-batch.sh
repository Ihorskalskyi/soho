#!/bin/bash

source "./generate_temporary_file.bash"
source "./run_commands_in_file.bash"

name="psf-batch"

rootdir="/u/khosrow/thesis/opt/soho/single-galaxy/images"
ppsf=0.001
run=$DEFAULT_SDSS_RUN
rerun=$DEFAULT_SDSS_RERUN

# generate a temporary command file
commandfile=$(generate_temporary_file --root="./" --prefix="temp-psf-submit-batch-" --suffix=".cmd")


declare -a camcol_a=("1" "2" "3" "4")
n0=4

declare -a band_a=(g r i u z)
n1=3

declare -a exec_a=("do-psf-norm-2" "do-psf-norm-3")
declare -a dest_a=("psf-analysis-norm-2" "psf-analysis-norm-3")
n2=2

#stripe 82
declare -a run_a=(4797 2700 2703 2708 2709 2960 3565 3434 3437 2968 4207 1742 4247 4252 4253 4263 4288 1863 1887 2570 2578 2579 109 125 211 240 241 250 251 256 259 273 287 297 307 94 5036 5042 5052 2873 21
24 1006 1009 1013 1033 1040 1055 1056 1057 4153 4157 4158 4184 4187 4188 4191 4192 2336 2886 2955 3325 3354 3355 3360 3362 3368 2385 2506 2728 4849 4858 4868 4874 4894 4895 4899 4905 4927 4930 4933 4948 2854 2855 2856
 2861 2662 3256 3313 3322 4073 4198 4203 3384 3388 3427 3430 4128 4136 4145 2738 2768 2820 1752 1755 1894 2583 2585 2589 2591 2649 2650 2659 2677 3438 3460 3458 3461 3465 7674 7183)
n3=120

for (( i0=0; i0<$n0; i0++ ))
do
camcol=${camcol_a[$i0]}

for (( i1=0; i1<$n1; i1++ ))
do 

for (( i2=0; i2<$n2; i2++ ))
do

for (( i3=0; i3<$n3; i3++ ))
do

band=${band_a[$i1]}
destination=${dest_a[$i2]}
executable=${exec_a[$i2]}
run=${run_a[$i3]}

echo "$name: band = $band, camcol = $camcol, destination: $destination"
workdir="$rootdir/$destination"
datadir="$workdir/$rerun/$run/$camcol/photo"
outdir="$workdir/$rerun/$run/$camcol"	
psf-script.sh --executable=$executable --camcol=$camcol --band=$band --run=$run --rerun=$rerun --commandfile=$commandfile --docondor --datadir=$datadir --outdir=$outdir --ppsf=$ppsf


done
done
done
done

echo
echo "$name: randomly running commands in file '$commandfile'"
run_commands_in_file --commandfile=$commandfile --random

echo
echo "$name: not deleting temp-file = $commandfile"
#rm $commandfile
