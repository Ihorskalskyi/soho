function write_condorshell_mledata {
local name="write_condorshell_mledata"

local Options=$@
local Optnum=$#

local lfield=no
local lpsffield=no
local lrun=no
local lcamcol=no
local lmleparfile=no
local loutdir=no
local lobjcdir=no
local lcomment=no
local copypsf=no
local simulator=no
local linobjcdir=no
local lincalibobjcdir=no
local loutcalibobjcdir=no
local linskydir=no
local loutskydir=no
local loutheasacdir=no
local lexecutable=no
local lphotofile=no
local largument=no
local linfpCDir=no
local loutfpCDir=no
local zipfpC=no
local zipfpCC=no

local field=empty
local psffield=empty
local run=empty
local camcol=empty
local mleparfile=empty
local outdir=empty
local comment=empty
local inobjcdir=empty
local inskydir=empty
local outskydir=empty
local heasacdir=empty
local executable=empty
local photofile=empty
local argument=empty
local infpCDir=empty
local outfpCDir=empty
local incalibobjcdir=empty
local outcalibobjcdir=empty


while getopts ':-:' OPTION ; do
  case "$OPTION" in
    -  ) [ $OPTIND -ge 1 ] && optind=$(expr $OPTIND - 1 ) || optind=$OPTIND
         eval OPTION="\$$optind"
         OPTARG=$(echo $OPTION | cut -d'=' -f2)
         OPTION=$(echo $OPTION | cut -d'=' -f1)
         case $OPTION in
	     --field	 ) lfield=yes; field="$OPTARG"   ;;
	     --field0    ) lfield0=yes; field0="$OPTARG" ;;
	     --psffield  ) lpsffield=yes; psffield="$OPTARG"     ;;
	     --run       ) lrun=yes; run="$OPTARG"	 ;;
	     --camcol    ) lcamcol=yes; camcol="$OPTARG" ;;
	     --output    ) lmleparfile=yes; mleparfile="$OPTARG" 	;;
	     --outdir    ) loutdir=yes; outdir="$OPTARG"	;;
	     --objcdir   ) lobjcdir=yes; objcdir="$OPTARG"	;; 
	     --copypsf   ) copypsf=yes				;;
	     --comment   ) lcomment=yes; comment="$OPTARG"	;;
	     --simulator ) simulator=yes				;;
	     --sdsssimulator ) sdsssimulator=yes		;;
	     --inobjcdir ) linobjcdir=yes; inobjcdir="$OPTARG"	;; #for sdss based simulation
	     --incalibobjcdir ) lincalibobjcdir=yes; incalibobjcdir="$OPTARG" ;; # for sdss based simulation
	     --outcalibobjcdir ) loutcalibobjcdir=yes; outcalibobjcdir="$OPTARG" ;;
	     --inskydir  ) linskydir=yes; inskydir="$OPTARG"	;; #for sdss based simulation
		--outskydir    ) loutskydir=yes; outskydir="$OPTARG"	;; #for outputinng simulation parameter
		--outheasacdir ) loutheasacdir=yes; outheasacdir="$OPTARG"	;; #for outputting photoObj files after changing the header to BITPIX = 8
		--executable ) lexecutable=yes; executable="$OPTARG"	;;
		--photofile  ) lphotofile=yes; photofile="$OPTARG"	;;
		--argument   ) largument=yes; argument="$OPTARG"	;;
		--shellfile  ) lshellfile=yes; shellfile="$OPTARG"	;;
		--zipfpCC    ) zipfpCC=yes	;;
		--zipfpC     ) zipfpC=yes	;;	
		--infpCDir    ) linfpCDir=yes; infpCDir="$OPTARG"	;;
		--outfpCDir     ) loutfpCDir=yes; outfpCDir="$OPTARG"	;;
	     * )  echo "$name: invalid options ($OPTION)" ;;
         esac
       OPTIND=1
       shift
      ;;
    ? )  echo "$name: invalid options (short) "  ;;
  esac
done

#echo "$name: root = $root, field = $field, field0 = $field0, run = $run, camcol = $camcol, band = $band, output = $mleparfile"

if [ $lfield == no ] || [ $lrun == no ] || [ $lcamcol == no ] || [ $lexecutable == no ] || [ $largument == no ]; then
	echo "$name: not enough arguments set"
	exit
fi
if [ $field == empty ] || [ $run == empty ] || [ $camcol == empty ]; then
	echo "$name: some arguments are invoked but not set"
	exit
fi
if [ $loutdir == no ]; then
	echo "$name: argument (outdir) was not passed"
	exit
fi
if [ $lobjcdir == no ]; then
	objcdir="$outdir/objc"
fi
if [ $linobjcdir == no ]; then 
	inobjcdir="$datadir"
fi
if [ $linskydir == no ]; then
	inskydir="$datadir"
fi
if [ $loutskydir == no ]; then
	outskydir="$PHOTO_TEMP"
fi
if [ $linfpCDir == no ]; then
	infpCdir="$CPHOTO_FRAMES/$DEFAULT_SDSS_RERUN/$run/$camcol"
fi
if [ $loutfpCDir == no ]; then
	outfpCdir="$outskydir"
fi
if [ $lincalibobjcdir == no ]; then
	incalibobjcdir="$CPHOTO_OBJCS/$DEFAULT_SDSS_RERUN/$run/$camcol"
fi
if [ $loutcalibobjcdir == no ]; then
	outcalibobjcdir="$outskydir"
fi
if [ $loutheasacdir == no ]; then
	outheasacdir="$outskydir"
fi

#frame-r-001000-1-0095.fits.bz2	
local fpCprefix="frame"
local psFieldprefix="psField"
local fpObjcprefix="fpObjc"
local fpBINprefix="fpBIN"
local fpMprefix="fpM"
local gpObjcprefix="gpObjc"
local lMprefix="lM"
local psfRprefix="psfR"
local psfSprefix="psfS"
local photoObjcprefix="photoObj"

runstr=$(( 10#$run ))
field=$(( 10#$field ))

runstr=`printf "%06d" $run`
fieldstr=`printf "%04d" $field`

skyobjc_datadir="$datadir"
skyobjc_datadir2="$datadir2"


local band="u"
local fpBIN_ufile="fpBIN-$runstr-$band$camcol-$fieldstr.fit"
local fpM_ufile="fpM-$runstr-$band$camcol-$fieldstr.fit"
local fpC_ufile="frame-$band-$runstr-$camcol-$fieldstr.fits"
band="g"
local fpBIN_gfile="fpBIN-$runstr-$band$camcol-$fieldstr.fit"
local fpM_gfile="fpM-$runstr-$band$camcol-$fieldstr.fit"
local fpC_gfile="frame-$band-$runstr-$camcol-$fieldstr.fits"
band="r"
local fpBIN_rfile="fpBIN-$runstr-$band$camcol-$fieldstr.fit"
local fpM_rfile="fpM-$runstr-$band$camcol-$fieldstr.fit"
local fpC_rfile="frame-$band-$runstr-$camcol-$fieldstr.fits"
band="i"
local fpBIN_ifile="fpBIN-$runstr-$band$camcol-$fieldstr.fit"
local fpM_ifile="fpM-$runstr-$band$camcol-$fieldstr.fit"
local fpC_ifile="frame-$band-$runstr-$camcol-$fieldstr.fits"
band="z"
local fpBIN_zfile="fpBIN-$runstr-$band$camcol-$fieldstr.fit"
local fpM_zfile="fpM-$runstr-$band$camcol-$fieldstr.fit"
local fpC_zfile="frame-$band-$runstr-$camcol-$fieldstr.fits"

local photoObjc_file="photoObj-$runstr-$camcol-$fieldstr.fits"
local heasac_modification_file="$outheasacdir/heasac-mod.txt"

if [ ! -e $heasac_modification_file ]; then

local heasac_modification_string=empty
read -r -d '' heasac_modification_string <<-END_PROC_HEASAC
BITPIX=8 /
END_PROC_HEASAC
echo -e "${heasac_modification_string}" >$heasac_modification_file

fi

modify=/u/khosrow/thesis/opt/soho/modify.py

local condor_shell_contents=empty
read -r -d '' condor_shell_contents <<-END_PROC_SHELL

#!/bin/bash
#$comment
name=$shellfile

idl_batch_file=/u/khosrow/thesis/opt/soho/convert_frame_to_fpC_batch.pro 
python_batch_file=/u/khosrow/thesis/opt/soho/read_frame_altered.py

uzipfile=$inskydir/$fpBIN_ufile.gz
gzipfile=$inskydir/$fpBIN_gfile.gz
rzipfile=$inskydir/$fpBIN_rfile.gz
izipfile=$inskydir/$fpBIN_ifile.gz
zzipfile=$inskydir/$fpBIN_zfile.gz

ufile=$outskydir/$fpBIN_ufile
gfile=$outskydir/$fpBIN_gfile
rfile=$outskydir/$fpBIN_rfile
ifile=$outskydir/$fpBIN_ifile
zfile=$outskydir/$fpBIN_zfile

declare -a zipfiles=(\$uzipfile \$gzipfile \$rzipfile \$izipfile \$zzipfile)
declare -a outfiles=(\$ufile \$gfile \$rfile \$ifile \$zfile)
declare -a inflated=(no no no no no)
declare -a existed=(no no no no no)
declare -a deleted_unzipped=(no no no no no)

uzipfile2=$infpCDir/$fpC_ufile.bz2
gzipfile2=$infpCDir/$fpC_gfile.bz2
rzipfile2=$infpCDir/$fpC_rfile.bz2
izipfile2=$infpCDir/$fpC_ifile.bz2
zzipfile2=$infpCDir/$fpC_zfile.bz2

ufile2=$outfpCDir/$fpC_ufile
gfile2=$outfpCDir/$fpC_gfile
rfile2=$outfpCDir/$fpC_rfile
ifile2=$outfpCDir/$fpC_ifile
zfile2=$outfpCDir/$fpC_zfile

declare -a zipfiles2=(\$uzipfile2 \$gzipfile2 \$rzipfile2 \$izipfile2 \$zzipfile2)
declare -a outfiles2=(\$ufile2 \$gfile2 \$rfile2 \$ifile2 \$zfile2)
declare -a inflated2=(no no no no no)
declare -a existed2=(no no no no no)
declare -a deleted_unzipped2=(no no no no no)

uzipfile3=$inskydir/$fpM_ufile.gz
gzipfile3=$inskydir/$fpM_gfile.gz
rzipfile3=$inskydir/$fpM_rfile.gz
izipfile3=$inskydir/$fpM_ifile.gz
zzipfile3=$inskydir/$fpM_zfile.gz

ufile3=$outskydir/$fpM_ufile
gfile3=$outskydir/$fpM_gfile
rfile3=$outskydir/$fpM_rfile
ifile3=$outskydir/$fpM_ifile
zfile3=$outskydir/$fpM_zfile

declare -a zipfiles3=(\$uzipfile3 \$gzipfile3 \$rzipfile3 \$izipfile3 \$zzipfile3)
declare -a outfiles3=(\$ufile3 \$gfile3 \$rfile3 \$ifile3 \$zfile3)
declare -a inflated3=(no no no no no)
declare -a existed3=(no no no no no)
declare -a deleted_unzipped3=(no no no no no)



in_photoobjcfile=$incalibobjcdir/$photoObjc_file
in_incalibobjcfile=$incalibobjcdir/$photoObjc_file
out_photoobjcfile=$outcalibobjcdir/$photoObjc_file
out_incalibobjcfile=$outcalibobjcdir/$photoObjc_file

declare -a inheasac_files=(\$in_photoobjcfile \$in_incalibobjcfile)
declare -a outheasac_files=(\$out_photoobjcfile \$out_incalibobjcfile)
declare -a existed_heasac=(no no)
declare -a deleted_heasac=(no no)

nheasac=2

nband=5

echo "\$name: evaluating if files needs to be expanded"

for (( i=0; i<\$nband; i++ ))
do

	zipfile=\${zipfiles[\$i]}
	destination=\${outfiles[\$i]}

	if [ -e \$destination ]; then 
		existed[\$i]=yes
		rm -f \$destination
	fi
	if [[ ! -e \$destination  && -e \$zipfile ]]; then
		gunzip < \$zipfile > \$destination
		inflated[\$i]=yes
	fi

	zipfile2=\${zipfiles2[\$i]}
	destination2=\${outfiles2[\$i]}

	if [ -e \$destination2 ]; then 
		existed2[\$i]=yes
		rm -f \$destination2
	fi
	#if [[ ! -e \$destination2  && -e \$zipfile2 ]]; then
	#	bzip2 -ckd \$zipfile2 > \$destination2 
	#	inflated2[\$i]=yes
	#fi

	# problem with IDL licensing
	#idl -e @\$idl_batch_file -args "\$zipfile2" "\$destination2"
	bzip2 -ckd \$zipfile2 > \$destination2
	python \$python_batch_file "\$destination2" "\$destination2"

	zipfile3=\${zipfiles3[\$i]}
	destination3=\${outfiles3[\$i]}

	if [ -e \$destination3 ]; then 
		existed3[\$i]=yes
		rm -f \$destination3
	fi
	if [[ ! -e \$destination3  && -e \$zipfile3 ]]; then
		gunzip < \$zipfile3 > \$destination3
		inflated3[\$i]=yes
	fi



done

echo "\$name: inflated (1) status: "
echo "\$name: \${inflated[*]}"

echo "\$name: inflated (2) status: "
echo "\$name: \${inflated2[*]}"

echo "\$name: inflated (3) status: "
echo "\$name: \${inflated3[*]}"


echo "\$name: evaluating if fits file headers need to be modified"


for (( i=0; i<\$nheasac; i++ ))
do
	source=\${inheasac_files[\$i]}
	destination=\${outheasac_files[\$i]}

	if [ -e \$destination ]; then 
		existed_heasac[\$i]=yes
		rm -f \$destination
	fi

done

for (( i=0; i<\$nheasac; i++ ))
do

	source=\${inheasac_files[\$i]}
	destination=\${outheasac_files[\$i]}

	if [[ -e \$source && ! -e \$destination ]]; then
		$modify \$source \$destination
		#cp \$source  \$destination
		#fmodhead \$destination\[0\] $heasac_modification_file
		#fthedit \$destination\[1\] @/u/khosrow/thesis/opt/soho/heasac-mod-1.txt 
		processed_heasac[\$i]=yes
	fi

done


echo "\$name: processed status: "
echo "\$name: \${processed_heasac[*]}"


echo "\$name: executing the binary"
echo "\$name: $executable $argument"

$executable $argument

echo "\$name: deleting heasarc processed files"

for (( i=0; i<\$nheasac; i++ ))
do
	destination=\${outheasac_files[\$i]}
	if [ -e \$destination ]; then 
		deleted_heasac[\$i]=yes
		rm -f \$destination
	fi

done


echo "\$name: deletion status: "
echo "\$name: \${deleted_heasac[*]}"

echo "\$name: deleting unzipped files"

for (( i=0; i<\$nband; i++ ))
do

	zipfile=\${zipfiles[\$i]}
	destination=\${outfiles[\$i]}

	if [[ \${existed[\$i]} == no && \${inflated[\$i]} == yes ]]; then
		deleted_unzipped[\$i]=yes	
		rm -f \$destination
	fi

	zipfile2=\${zipfiles2[\$i]}
	destination2=\${outfiles2[\$i]}

	if [[ \${existed2[\$i]} == no && \${inflated2[\$i]} == yes ]]; then
		deleted_unzipped2[\$i]=yes	
		rm -f \$destination2
	fi

done

echo "\$name: deletion (1-fpBIN) status: "
echo "\$name: \${deleted_unzipped[*]}"

echo "\$name: deletion (2-frame) status: "
echo "\$name: \${deleted_unzipped2[*]}"

#end of condor shell script

END_PROC_SHELL

echo -e "${condor_shell_contents}" >$shellfile
}





