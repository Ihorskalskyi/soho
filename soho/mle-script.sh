#!/bin/bash

source "./write_condorjob.sh"
source "./write_parfile.sh"
source "./create_field_array.sh"

name="mle-script"
DATE=$(date +"%m-%d-%Y")
TIME=$(date +"%T") 


debug=no
verbose=no
fit2fpCC=no
multiband=no

lroot=no
lband=no
lcamcol=no
lrun=no
lrerun=no
lmodel=no
lpsf=no
lfstep=no
lfield0=no
lfield1=no
lcondordir=no
ldatadir=no
loutdir=no
lexecutable=no
lEXTRA_FLAGS=no
lexcept=no
lcommandfile=no
linner_lumcut=no
louter_lumcut=no

droot="/u/khosrow/thesis/opt/soho"
dband="r"
dcamcol="4"
drun=$DEFAULT_SDSS_RUN
drerun=$DEFAULT_SDSS_RERUN
dmodel="deV"
dpsf="9999-9999"
dfstep=68
dfield0=101
dfield1=4290
dexecutable="do-mle"
dexcept=0
dinner_lumcut="0.0001"
douter_lumcut="0.0001"


while getopts ':-:' OPTION ; do
  case "$OPTION" in
    -  ) [ $OPTIND -ge 1 ] && optind=$(expr $OPTIND - 1 ) || optind=$OPTIND
         eval OPTION="\$$optind"
         OPTARG=$(echo $OPTION | cut -d'=' -f2)
         OPTION=$(echo $OPTION | cut -d'=' -f1)
         case $OPTION in
	     --debug     ) debug=yes			 ;;
     	     --verbose   ) verbose=yes			 ;;
	     --fit2fpCC  ) fit2fpCC=yes		         ;;
	     --root      ) lroot=yes; root="$OPTARG"     ;;
	     --field0	 ) lfield0=yes; field0="$OPTARG"   ;;
	     --field1    ) lfield1=yes; field1="$OPTARG" ;;
	     --fstep     ) lfstep=yes; fstep="$OPTARG" ;;
	     --psf       ) lpsf=yes; psf="$OPTARG" ;;
     	     --run       ) lrun=yes; run="$OPTARG"	 ;;
	     --rerun     ) lrerun=yes; rerun="$OPTARG"    ;; 
	     --model     ) lmodel=yes; model="$OPTARG"    ;;
	     --camcol    ) lcamcol=yes; camcol="$OPTARG" ;;
	     --band      ) lband=yes; band="$OPTARG"	 ;;
             --condordir ) lcondordir=yes; condordir="$OPTARG" ;;     
	     --outdir	 ) loutdir=yes; outdir="$OPTARG" ;;
	     --datadir   ) ldatadir=yes; datadir="$OPTARG"     ;;
	     --executable	) lexecutable=yes; executable="$OPTARG"		;;
	     --except           ) lexcept=yes; except="$OPTARG"			;;
	     --commandfile	) lcommandfile=yes; commandfile="$OPTARG"	;; 
	     --EXTRA_FLAGS	) lEXTRA_FLAGS=yes; EXTRA_FLAGS="$OPTARG"	;;
	     --inner_lumcut     ) linner_lumcut=yes; inner_lumcut="$OPTARG"     ;;	
	     --outer_lumcut     ) louter_lumcut=yes; outer_lumcut="$OPTARG"     ;; 
	     --multiband	) multiband=yes		;;  
	* )  echo "$name: invalide options (long)" ;;
         esac
       OPTIND=1
       shift
      ;;
    ? )  echo "$name: invalid options (short) "  ;;
  esac
done

if [ $lroot == no ]; then
	root=$droot
	echo "$name: argument (root)   set to '$root'"
fi
if [ $lfield0 == no ]; then
	field0=$dfield0
	echo "$name: argument (field0)  set to '$field0'"
fi
if [ $lfield1 == no ]; then
	field1=$dfield1
	echo "$name: argument (field1) set to '$field1'"
fi
if [ $lfstep == no ]; then
	fstep=$dfstep
	echo "$name: argument (fstep)  set to '$fstep'"
fi
if [ $lpsf == no ]; then
	psf=$dpsf
	echo "$name: argument (psf) set to '$psf'"
fi
if [ $lrun == no ]; then
	run=$drun
	echo "$name: argument (run)    set to '$run'"
fi
if [ $lrerun == no ]; then
	rerun=$drerun;
	echo "$name: argument (rerun)  set to '$rerun'"
fi
if [ $lmodel == no ]; then
	model=$dmodel;
	echo "$name: argument (model) set to '$model'"
fi
if [ $lcamcol == no ]; then
	camcol=$dcamcol
	echo "$name: argument (camcol) set to '$camcol'"
fi
if [ $lband == no ]; then
	band=$dband
	echo "$name: argument (band)   set to '$band'"
fi
if [ $ldatadir == no ]; then
	datadir="$root/single-galaxy/images/sims/$model/$rerun/$run"
	echo "$name: argument (datadir) set to '$datadir'"
fi
if [ $loutdir == no ]; then
	outdir=$datadir;
	echo "$name: argument (outdir) set to '$outdir'"
fi
if [ $lcondordir == no ]; then
	condordir="$outdir/condor"
	echo "$name: argument (condordir) set to '$condordir'"
fi
if [ $lexecutable == no ]; then
	executable="$root/$dexecutable-$psf"
	echo "$name: argument (executable) set to '$executable'"
fi
if [ $lexcept == no ]; then
	except=$dexcept
	echo "$name: argument (except) set to '$except'"
fi
if [ $lcommandfile == yes ]; then
	if [[ -e $commandfile ]]; then
		echo "$name: WARNING - export file for commands '$commandfile' already exists. appending"
	fi
fi	
if [ $lEXTRA_FLAGS == no ]; then
	EXTRA_FLAGS=""
fi
if [ $linner_lumcut == no ]; then
	inner_lumcut=$dinner_lumcut
	echo "$name: argument (inner_lumcut) set to '$inner_lumcut'"
fi
if [ $louter_lumcut == no ]; then
	outer_lumcut=$douter_lumcut
	echo "$name: argument (outer_lumcut) set to '$outer_lumcut'"
fi
multibandflag=""
if [ $multiband == yes ]; then
	multibandflag="--multibandmle"
fi

echo "$name: debug    = $debug"
echo "$name: verbose  = $verbose"
echo "$name: fit2fpCC = $fit2fpCC"
echo "$name: *** fit2fpCC should be set in EXTRA_FLAGS if desired"
echo "$name EXTRA_FLAGS = '$EXTRA_FLAGS'"

clogdir="$condordir/log"
cerrordir="$condordir/error"
coutputdir="$condordir/output"
csubmitdir="$condordir/submit"
if [ ! -d $condordir ]; then
	echo "$name: making (condordir) '$condordir'"
	mkdir -p $condordir
fi
if [ ! -d $clogdir ]; then
	echo "$name: making (clogdir) '$clogdir'"
	mkdir -p $clogdir
fi
if [ ! -d $cerrordir ]; then
	echo "$name: making (cerrordir) '$cerrordir'"
	mkdir -p $cerrordir
fi
if [ ! -d $coutputdir ]; then
	echo "$name: making (coutputdir) '$coutputdir'"
	mkdir -p $coutputdir
fi
if [ ! -d $csubmitdir ]; then
	echo "$name: making (csubmitdir) '$csubmitdir'"
	mkdir -p $csubmitdir
fi
if [ ! -d $outdir ]; then
	echo "$name: making (outdir) '$outdir'"
	mkdir -p $outdir
fi
objcdir="$outdir/objc"
parfiledir="$outdir/parfiles"
photodir="$outdir/photo"
if [ ! -d $objcdir ]; then
	echo "$name: making (objcdir) '$objcdir'"
	mkdir -p $objcdir
fi
if [ ! -d $parfiledir ]; then 
	echo "$name: making (parfiledir) '$parfiledir'"
	mkdir -p $parfiledir
fi
if [ ! -d $photodir ]; then
	echo "$name: making (photodir) '$photodir'"
	mkdir -p $photodir
fi


tempfile="./temp-condor-submit.output"
fpCprefix="fpC"
fpCCprefix="fpCC"

ifield=$(( 10#$field0 ))
run=$(( 10#$run ))
runstr=`printf "%06d" $run`

#creating an array of available fields
if [[ $debug == yes ]]; then
	echo "$name: create_field_array --datadir=$datadir"
fi
field_array_string=$(create_field_array --datadir="$datadir" --field0="$field0" --run=$run --camcol="$camcol" --field1=$field1 --band=$band --fstep=$fstep)
field_array=(${field_array_string//,/ })
nfield_array=${#field_array[@]}
if [[ $debug == alpha ]]; then  
	echo "$name: fields in string = $field_array_string"
	echo "$name: fields in array  = ${field_array[@]}"
fi
echo "$name: # of valid fields found = $nfield_array"

njob=0
nbadjob=0
#while [[ $dataexists == yes ]] 
while [[ $nfield_array -gt 0 ]]
do
	#selecting a random field from the array of fields
	nfield_array=${#field_array[@]} 
	if [[ $nfield_array -gt 1 ]]; then 
		let nfield_array++
		irand=$(python -S -c "import random; print(random.randrange(1,$nfield_array))")
		let nfield_array--
	else 
		irand=$nfield_array
	fi
	let irand--
	ifield=${field_array[$irand]} #taking a random element from the field array
	fieldstr=`printf "%04d" $ifield`
	if [ $multiband == yes ]; then 
	parfilename="ff-$runstr-$camcol-$fieldstr.par"
	else
	parfilename="ff-$runstr-$band$camcol-$fieldstr.par"
	fi
	srcparfile="$datadir/parfiles/$parfilename"
	targetparfile="$outdir/parfiles/$parfilename"
	
	if [[ $debug == alpha ]]; then 	
		echo "$name: remaining field array = ${field_array[@]}"
		echo "$name: irand = $irand, nfield_array = $nfield_array, ifield = $ifield, fieldstr = $fieldstr"
	fi	
	#condor job
	#making new par file based on the old one and altering the outdir
	if [[ $debug == alpha ]]; then 
		echo "$name: write_parfile"
	fi
	maxseed=4294967295 # the biggest unsigned int that can be passed as a seed to simulator 
	seed=$(python -S -c "import random; print(random.randrange(1,$maxseed))")
	comment="'DATE: $DATE, TIME: $TIME, EXTRA_FLAGS: $EXTRA_FLAGS'"
	write_parfile --copypsf --datadir="$datadir/photo" --field=$ifield --psffield=$ifield --run=$run --camcol=$camcol --band=$band --outdir="$outdir" --comment="$comment" --output="$targetparfile" $multibandflag	
	echo -n "~"
	if [ $multiband == yes ]; then 
	condorprefix="condor-mle-$model-$runstr-$camcol-$fieldstr"
	else
	condorprefix="condor-mle-$model-$runstr-$band$camcol-$fieldstr"
	fi
	condor_submitfile="$csubmitdir/$condorprefix.submit"
	condor_errorfile="$cerrordir/$condorprefix.error"
	condor_logfile="$clogdir/$condorprefix.log"
	condor_outputfile="$coutputdir/$condorprefix.out"
	condor_argument="--framefile $targetparfile -seed $seed $EXTRA_FLAGS"
	condor_comment="date: $DATE, time: $TIME"
	#writing condor job
	if [[ $debug == alpha ]]; then
		echo "$name: write_condorjob"
	fi
	write_condorjob --submitfile=$condor_submitfile --errorfile=$condor_errorfile --logfile=$condor_logfile --outputfile=$condor_outputfile --executable="$executable" --arguments="$condor_argument" --comment="$condor_comment"
	if [[ $debug == alpha ]]; then 
		echo "$name: $condor_submitfile"
	else 
		if [ $multiband == yes ]; then
		fpCfile="$datadir/photo/$fpCprefix-$runstr-*$camcol-$fieldstr.fit"	
		fpCCfile="$datadir/photo/$fpCCprefix-$runstr-*$camcol-$fieldstr.fit"
		else
		fpCfile="$datadir/photo/$fpCprefix-$runstr-$band$camcol-$fieldstr.fit"	
		fpCCfile="$datadir/photo/$fpCCprefix-$runstr-$band$camcol-$fieldstr.fit"
		fi
		if [[ $verbose == yes ]]; then 
			echo "$name: fpC = $fpCfile, fpCC = $fpCCfile"
		fi
		if [[ -e "$fpCfile.gz" ]]; then 
			gzcommand="gunzip $fpCfile.gz"
			eval $gzcommand
		fi
		if [[ -e "$fpCCfile.gz" ]]; then
			gzcommand="gunzip $fpCCfile.gz"
			eval $gzcommand
		fi
		if [ $debug == yes ]; then
			echo -n 'e'
		else  
			if [ $lcommandfile == yes ]; then
				echo "condor_submit $condor_submitfile >> $tempfile" >> $commandfile
			else
				eval "condor_submit $condor_submitfile >> $tempfile"
			fi
			echo -n "."
		fi
	fi
	let njob++
	# now deleting the selected field from the list, and updating the array size, it is important to delete null array elements after deleting an element.
	unset field_array[$irand]
	field_array=(${field_array[@]})
	nfield_array=${#field_array[@]}
	#if the next field is out of the range then stop processing it#
	# let "ifield+=$fstep"
	#if [[ $ifield -gt $field1 ]]; then 
	#	dataexists=no
	#fi
done

echo ""
echo "$name: $njob jobs submitted"
if [[ $nbadjob -gt 0 ]]; then
	echo "$name: $nbadjob cases of non-existent fpC / fpCC files found - jobs not submitted"
fi

