#include "thRegionTypes.h"

static const SCHEMA *schema_fabrication_flag = NULL;
static const SCHEMA *schema_production_flag = NULL;
static const SCHEMA *schema_thregion_type = NULL;
static const SCHEMA *schema_psf_convolution_type = NULL;
static char *static_temp_string = NULL;

static void init_static_vars(void);
void init_static_vars(void) {
	TYPE t;
	t = shTypeGetFromName("FABRICATION_FLAG");
	schema_fabrication_flag = shSchemaGetFromType(t);
	shAssert(schema_fabrication_flag != NULL);
	t = shTypeGetFromName("PRODUCTION_FLAG");
	schema_production_flag = shSchemaGetFromType(t);
	shAssert(schema_production_flag != NULL);
	t = shTypeGetFromName("THREGION_TYPE");
	schema_thregion_type = shSchemaGetFromType(t);
	shAssert(schema_thregion_type != NULL);
	t = shTypeGetFromName("PSF_CONVOLUTION_TYPE");
	schema_psf_convolution_type = shSchemaGetFromType(t);
	shAssert(schema_psf_convolution_type != NULL);
	if (static_temp_string != NULL) thFree(static_temp_string);
	static_temp_string = thCalloc(MX_STRING_LEN, sizeof(char));
	return;
}
	

void region_type_free_static_memory (void) {
	if (schema_fabrication_flag != NULL) schema_fabrication_flag = NULL;
	if (schema_production_flag != NULL) schema_production_flag = NULL;
	if (static_temp_string != NULL) {
		thFree(static_temp_string);
		static_temp_string = NULL;
	}
	return;
}

RET_CODE thRegDelByProduction(REGION *reg, PRODUCTION_FLAG production) {
char *name = "thRegDelByProduction";
	if (reg == NULL) return(SH_SUCCESS);
	if (production == RUNTIME) {
		shRegDel(reg); 
		return(SH_SUCCESS);
	} else if (production == SAVED) {
		if (reg->type != TYPE_THPIX) {
			thError("%ERROR - limited type support for region handling", name);
			return(SH_GENERIC_ERROR);
		}
		reg->rows_thpix = NULL;
		reg->nrow = 0;
		reg->ncol = 0;	
		shRegDel(reg);
		return(SH_SUCCESS);
	} else {
		if (reg->nrow == 0 && reg->ncol == 0 && reg->rows_thpix == NULL) {
			shRegDel(reg);
			return(SH_SUCCESS);
		} 
		if (schema_production_flag == NULL) init_static_vars();
		thError("%s: ERROR - unsupported production flag '%s'", 
			name, (schema_production_flag->elems[production]).name);
		return(SH_GENERIC_ERROR);
	}
}

RET_CODE thRegFreeRowsByProduction(REGION *reg, PRODUCTION_FLAG production) {
char *name = "thRegFreeRowsByProduction";

	if (reg == NULL) return(SH_SUCCESS);
	if (production == RUNTIME || production == SAVED) {
		if (reg->type != TYPE_THPIX) {
			thError("%ERROR - limited type support for region handling", name);
			return(SH_GENERIC_ERROR);
		}
		if (production == RUNTIME) {
			/* old school, oct 16, 2012 */
			#if WISE_REGION_ALLOCATION
			p_shRegRowsFree(reg);
			#if PREFAB_ROWS
			#else
			p_shRegVectorFree(reg);
			#endif
			#else
			if (reg->rows_thpix != NULL &* reg->rows_thpix[0] != NULL) thFree(reg->rows_thpix[0])
			if (reg->rows_thpix != NULL) thFree(reg->rows_thpix);
			#endif
		} else {
			reg->rows_thpix = NULL;
		}
		reg->nrow = 0;
		reg->ncol = 0;
		reg->row0 = 0;
		reg->col0 = 0;		
		return(SH_SUCCESS);
	} else { 
		if (schema_production_flag == NULL) init_static_vars();
		char *flag_name = (schema_production_flag->elems[production]).name;
		thError("%s: ERROR - unsupported production flag '%s'", name, flag_name);
		return(SH_GENERIC_ERROR);
	}
}

	
THREGION1 *thRegion1New(char *name, TYPE type) {
	THREGION1 *reg1 = (THREGION1 *) thCalloc(1, sizeof(THREGION1));
	reg1->reg = shRegNew(name, 0, 0, type);
	reg1->production = UNKNOWN_PRODUCTION_FLAG;
	reg1->fab = INIT;
	return(reg1);
}

void thRegion1Del(THREGION1 *reg1) {
char *name = "thRegion1Del";
	if (reg1 == NULL) return;
	RET_CODE status;
	status = thRegDelByProduction(reg1->reg, reg1->production);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not delete the dervish region", name);
	}
	thFree(reg1);
	return;
}
THREGION2 *thRegion2New(char *name, TYPE type) {
	THREGION2 *reg2 = (THREGION2 *) thCalloc(1, sizeof(THREGION2));
	int i;
	if (static_temp_string == NULL) static_temp_string = thCalloc(MX_STRING_LEN, sizeof(char));
	for (i = 0; i < N_PSF_COMPONENT; i++) {
		sprintf(static_temp_string, "%s - %dth PSF component", name, i);
		reg2->reg[i] = shRegNew(static_temp_string, 0, 0, type);
		reg2->production[i] = UNKNOWN_PRODUCTION_FLAG;
		reg2->fab_i[i] = INIT;
	}
	reg2->fab = INIT;
	return(reg2);
}

void thRegion2Del(THREGION2 *reg2) {
char *name = "thRegion2Del";
	if (reg2 == NULL) return;
	int i;
	for (i = 0; i < N_PSF_COMPONENT; i++) {	
		REGION *reg = reg2->reg[i];
		PRODUCTION_FLAG production = reg2->production[i];
		RET_CODE status;
		status = thRegDelByProduction(reg, production);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not delete dervish region", name);
		}
	}
	thFree(reg2);
	return;
}
	
THREGION3 *thRegion3New(char *name, TYPE type) {
	char *proc = "thRegion3New";
	if (type != TYPE_THPIX) {
		thError("%s: ERROR - unsupported type for pixel", proc);
		return(NULL);
	}
	THREGION3 *reg3 = (THREGION3 *) thCalloc(1, sizeof(THREGION3));
	reg3->name = thCalloc(MX_STRING_LEN, sizeof(char));
	if (name != NULL) strcpy(reg3->name, name);
	reg3->production = UNKNOWN_PRODUCTION_FLAG;
	reg3->fab = INIT;
	return(reg3);
}


void thRegion3Del(THREGION3 *reg3) {
char *name = "thRegion3Del";
	if (reg3 == NULL) return;
	if (reg3->name != NULL) {
		thFree(reg3->name);
	} else {
		thError("%s: WARNING - null (name) string discovered in 'THREGION3' - improper allocation possible", name);
	}
	if (reg3->frow != NULL) {
		if (reg3->nrow <= 0) {
			thError("%s: ERROR - non-null (frow) while (nrow = %d)", name, reg3->nrow);
			return;
		}
		thFree(reg3->frow);
	} else if (reg3->nrow != 0) {
		#if WARN_NULL_REG3
		thError("%s: WARNING - null (frow) while (nrow = %d)", name, reg3->nrow);
		#endif
	}
	if (reg3->fcol != NULL) {
		if (reg3->ncol <= 0) {
			thError("%s: ERROR - non-null (frow) while (ncol = %d)", name, reg3->ncol);
			return;
		}
		thFree(reg3->fcol);
	} else if (reg3->ncol != 0) {
	#if WARN_NULL_REG3	
	thError("%s: WARNING - null (frow) while (ncol = %d)", name, reg3->ncol);
	#endif
	}
	thFree(reg3);
	return;
}


THREGION *thRegNew(char *name, TYPE type, THREGION_TYPE regtype) {
	char *proc = "thRegNew";
	if (regtype == THREGION1_TYPE) {
		THREGION *threg = thCalloc(1, sizeof(THREGION));
		threg->threg = thRegion1New(name, type);
		threg->type = regtype;
		return(threg);
	} else if (regtype == THREGION2_TYPE) {
		THREGION *threg = thCalloc(1, sizeof(THREGION));
		threg->threg = thRegion2New(name, type);
		threg->type = regtype;
		return(threg);
	} else if (regtype == THREGION3_TYPE) {
		THREGION *threg = thCalloc(1, sizeof(THREGION));
		threg->threg = thRegion3New(name, type);
		threg->type = regtype;
		return(threg);
	} else if (regtype == UNKNOWN_THREGION_TYPE) {
		THREGION *threg = thCalloc(1, sizeof(THREGION));
		threg->type = regtype;
		return(threg);
	} else {
		thError("%s: ERROR - unrecognized (regtype) THREGION_TYPE", proc);
		return(NULL);
	}
}

void thRegDel(THREGION *threg) {
	char *name = "thRegDel";
	if (threg == NULL) return;
	if (threg->threg == NULL) {
		thFree(threg);
		return;
	}
	if (threg->type == THREGION1_TYPE) {
		thRegion1Del(threg->threg);
		thFree(threg);
		return;
	} else if (threg->type == THREGION2_TYPE) {
		thRegion2Del(threg->threg);
		thFree(threg);
	} else if (threg->type == THREGION3_TYPE) {
		thRegion3Del(threg->threg);
		thFree(threg);
	} else {
		thError("%s: ERROR - unsupported (regtype) THREGION_TYPE", name);
		return;
	}
}

RET_CODE thRegRenew(THREGION *threg, char *name, TYPE type, THREGION_TYPE regtype) {
char *proc = "thRegRenew";
if (threg == NULL) {
	thError("%s: ERROR - cannot renew null (thregion)", proc);
	return(SH_GENERIC_ERROR);
}
if (threg->threg != NULL) {
	if (threg->type == THREGION1_TYPE) {
		thRegion1Del((THREGION1 *) threg->threg);
		threg->threg = NULL;
	} else if (threg->type == THREGION2_TYPE) {
		thRegion2Del((THREGION2 *) threg->threg);
		threg->threg = NULL;
	} else if (threg->type == THREGION3_TYPE) {
		thRegion3Del((THREGION3 *) threg->threg);
		threg->threg = NULL;
	} else {
		if (schema_thregion_type == NULL) init_static_vars ();
		thError("%s: ERROR - non-null (thregion) for type '%s' not supported", 
			proc, (schema_thregion_type->elems[threg->type]).name);
		return(SH_GENERIC_ERROR);
	}
}
if (regtype == THREGION1_TYPE) {
	threg->threg = thRegion1New(name, type);
	threg->type = regtype;
	return(SH_SUCCESS);
} else if (regtype == THREGION2_TYPE) {
	threg->threg = thRegion2New(name, type);
	threg->type = regtype;
	return(SH_SUCCESS);
} else if (regtype == THREGION3_TYPE) {
	threg->threg = thRegion3New(name, type);
	threg->type = regtype;
	return(SH_SUCCESS);
} else if (regtype == UNKNOWN_THREGION_TYPE) {
	threg->type = regtype;
	return(SH_SUCCESS);
} else {
	if (schema_thregion_type == NULL) init_static_vars ();
	thError("%s: ERROR - unsupported (regtype) '%s' for renewed (thregion)", 
		proc, (schema_thregion_type->elems[threg->type]).name);
	return(SH_GENERIC_ERROR);
}

}

RET_CODE thReg1Reinit(THREGION1 *threg) {
char *name = "thReg1Reinit";
if (threg == NULL) {
	thError("%s: WARNING - null (threg1)", name);
	return(SH_SUCCESS);
}
FABRICATION_FLAG fab = threg->fab;
if (fab != PREFAB) {
	init_static_vars ();
	thError("%s: ERROR - unsupported fabrication flag (%d, '%s')", name, 
	fab, shEnumNameGetFromValue("FABRICATION_FLAG", fab));
	return(SH_GENERIC_ERROR);
}
threg->fab = INIT;
threg->row0 = 0;
threg->col0 = 0;
threg->nrow = 0;
threg->ncol = 0;

REGION *reg = threg->reg;
if (reg == NULL) {
	thError("%s: ERROR - null (region) in (threg1): improperly allocated (threg1)", name);
	return(SH_GENERIC_ERROR);
}

#if PREFAB_ROWS
if (threg->production == RUNTIME) {
	p_shRegVectorFree(reg);
}
#endif
if (threg->production == SAVED) {
	reg->rows_thpix = NULL;
}
reg->nrow = 0;
reg->ncol = 0;
reg->row0 = 0;
reg->col0 = 0;


return(SH_SUCCESS);
}


RET_CODE thReg2Reinit(THREGION2 *threg) {
char *name = "thReg2Reinit";
if (threg == NULL) {
	thError("%s: WARNING - null (threg2)", name);
	return(SH_SUCCESS);
}
FABRICATION_FLAG fab = threg->fab;
if (fab != PREFAB && fab != INIT) {
	init_static_vars ();
	thError("%s: ERROR - unsupported fabrication flag (%d, '%s')", name, 
	fab, shEnumNameGetFromValue("FABRICATION_FLAG", fab));
	return(SH_GENERIC_ERROR);
}

int i;
for (i = 0; i < N_PSF_COMPONENT; i++) {	
	REGION *reg = threg->reg[i];
	if (reg != NULL) {
		#if PREFAB_ROWS
		if (threg->production[i] == RUNTIME) {
			p_shRegVectorFree(reg);
		}
		#endif
		if (threg->production[i] == SAVED) {
			reg->rows_thpix = NULL;
		}
		reg->nrow = 0;
		reg->ncol = 0;
		reg->row0 = 0;
		reg->col0 = 0;
	}

	threg->fab_i[i] = INIT;
	threg->nrow[i] = 0;
	threg->ncol[i] = 0;
	threg->row0[i] = 0;
	threg->col0[i] = 0;
}

threg->fab = INIT;

return(SH_SUCCESS);
}

RET_CODE thReg3Reinit(THREGION3 *threg) {
char *name = "thReg3Reinit";
if (threg == NULL) {
	thError("%s: WARNING - null (threg3)", name);
	return(SH_SUCCESS);
}
FABRICATION_FLAG fab = threg->fab;
if (fab != PREFAB) {
	init_static_vars ();
	thError("%s: ERROR - fabrication flag '%s' not supported", name, schema_fabrication_flag->elems[fab].name);
	return(SH_GENERIC_ERROR);
}
threg->frow = NULL;
threg->fcol = NULL;
threg->nrow = 0;
threg->ncol = 0;
threg->row0 = 0;
threg->col0 = 0;
threg->fab = INIT;

return(SH_SUCCESS);
}

RET_CODE thRegReinit(THREGION *threg) {
char *name = "thRegReinit";
if (threg == NULL) {
	thError("%s: WARNING - null input", name);
	return(SH_SUCCESS);
}
RET_CODE status;
THREGION_TYPE type = threg->type;
if (type == THREGION1_TYPE) {
	status = thReg1Reinit(threg->threg);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not re-init (threg1)", name);
		return(status);
	}
	return(SH_SUCCESS);
} else if (type == THREGION2_TYPE) {
	status = thReg2Reinit(threg->threg);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not re-init (threg2)", name);
		return(status);
	}
	return(SH_SUCCESS);
} else if (type == THREGION3_TYPE) {
	status = thReg3Reinit(threg->threg);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not re-init (threg3)", name);
		return(status);
	}
	return(SH_SUCCESS);
} else {
	init_static_vars ();
	thError("%s: ERROR - unsupported thregion (type) = '%s'", name, schema_thregion_type->elems[type].name);
	return(SH_GENERIC_ERROR);
}
}



RET_CODE thReg1Prefab(THREGION1 *threg, int row0, int col0, 
			int nrow, int ncol, PRODUCTION_FLAG production) {
char *name = "thReg1Prefab";
if (threg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (nrow < 0 || ncol < 0) {
	thError("%s: ERROR - unacceptable (nrow, ncol) = (%d, %d)", name, nrow, ncol);
	return(SH_GENERIC_ERROR);	
	}

if (production != RUNTIME && production != SAVED) {
	if (schema_production_flag == NULL) init_static_vars();
	thError("%s: ERROR - unsupported production flag '%s'", 
		name, (schema_production_flag->elems[production]).name);
	return(SH_GENERIC_ERROR);
}

FABRICATION_FLAG flag;
if ((flag = threg->fab) != INIT) {
	if (schema_fabrication_flag == NULL) init_static_vars();
	thError("%s: ERROR - fabrication flag '%s' not supported, expected 'INIT'", 
		name, (schema_fabrication_flag->elems[flag]).name);
	return(SH_GENERIC_ERROR);
}
#if PREFAB_ROWS 
if (production == RUNTIME && nrow > 0) {
	REGION *reg = threg->reg;
	if (p_shRegVectorGet(reg, nrow, TYPE_THPIX) == (int) NULL) {
		thError("%s: ERROR - could not generate the (row) array for region '%s' (nrow = %d)", name, reg->name, nrow);
	}
}
#endif

threg->row0 = row0;
threg->col0 = col0;
threg->nrow = nrow;
threg->ncol = ncol;
threg->production = production;
threg->fab = PREFAB;
return(SH_SUCCESS);
}

RET_CODE thReg2Prefab(THREGION2 *threg, int row0, int col0, int nrow, int ncol, PRODUCTION_FLAG production, int i_psf_component) {
char *name = "thReg2Prefab";
if (threg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (nrow <= 0 || ncol <= 0) {
	thError("%s: ERROR - unacceptable (nrow, ncol) = (%d, %d)", name, nrow, ncol);
	return(SH_GENERIC_ERROR);	
	}
if (i_psf_component < 0 || i_psf_component >= N_PSF_COMPONENT) {
	thError("%s: ERROR - unsupported (i_psf_component): %d", name, i_psf_component);
	return(SH_GENERIC_ERROR);
}
if (production != SAVED && production != RUNTIME) {
	if (schema_production_flag == NULL) init_static_vars();
	thError("%s: ERROR - unsupported production flag '%s'", 
		name, (schema_production_flag->elems[production]).name);
	return(SH_GENERIC_ERROR);
}
FABRICATION_FLAG flag;
if ((flag = threg->fab_i[i_psf_component]) != INIT) {
	if (schema_fabrication_flag == NULL) init_static_vars();
	thError("%s: ERROR - fabrication flag '%s' not supported, expected 'INIT'", 
		name, (schema_fabrication_flag->elems[flag]).name);
	return(SH_GENERIC_ERROR);
}

#if PREFAB_ROWS 
if (production == RUNTIME) {
	REGION *reg = threg->reg[i_psf_component];
	if (p_shRegVectorGet(reg, nrow, TYPE_THPIX) == (int) NULL) {
		thError("%s: ERROR - could not generate the (row) array for region '%s' (nrow = %d)", name, reg->name, nrow);
	}
}
#endif
threg->row0[i_psf_component] = row0;
threg->col0[i_psf_component] = col0;
threg->nrow[i_psf_component] = nrow;
threg->ncol[i_psf_component] = ncol;
threg->fab_i[i_psf_component] = PREFAB;
threg->production[i_psf_component] = production;

return(SH_SUCCESS);
}
RET_CODE thReg3Prefab(THREGION3 *threg, int row0, int col0, 
			int nrow, int ncol, PRODUCTION_FLAG production) {
char *name = "thReg3Prefab";
if (threg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (nrow <= 0 || ncol <= 0) {
	thError("%s: ERROR - unacceptable (nrow, ncol) = (%d, %d)", name, nrow, ncol);
	return(SH_GENERIC_ERROR);	
	}

if (production != RUNTIME && production != SAVED) {
	if (schema_production_flag == NULL) init_static_vars();
	thError("%s: ERROR - unsupported production flag '%s'", 
		name, (schema_production_flag->elems[production]).name);
	return(SH_GENERIC_ERROR);
}

FABRICATION_FLAG flag;
if ((flag = threg->fab) != INIT) {
	if (schema_fabrication_flag == NULL) init_static_vars();
	thError("%s: ERROR - fabrication flag '%s' not supported, expected 'INIT'", 
		name, (schema_fabrication_flag->elems[flag]).name);
	return(SH_GENERIC_ERROR);
}

threg->row0 = row0;
threg->col0 = col0;
threg->nrow = nrow;
threg->ncol = ncol;
threg->production = production;
threg->fab = PREFAB;

return(SH_SUCCESS);
}



RET_CODE thRegPrefab(THREGION *threg, int row0, int col0, int nrow, int ncol, PRODUCTION_FLAG production, int i_psf_component) {
char *name = "thRegPrefab";
if (threg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
THREGION_TYPE regtype = threg->type;
if (regtype == THREGION1_TYPE) {
	if (i_psf_component != 0) {
		thError("%s: ERROR - 'THREGION' is of the first kind - has only one component - unacceptable (i_psf = %d)", 
		name, i_psf_component);
		return(SH_GENERIC_ERROR);
	}
	THREGION1 *threg1 = threg->threg;
	status = thReg1Prefab(threg1, row0, col0, nrow, ncol, production);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not prefabricate (threg)", name);
		return(status);
	}
} else if (regtype == THREGION2_TYPE) {
	THREGION2 *threg2 = threg->threg;
	status = thReg2Prefab(threg2, row0, col0, nrow, ncol, production, i_psf_component);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not prefabricate (threg)", name);
		return(status);
	}
} else if (regtype == THREGION3_TYPE) {
	if (i_psf_component != 0) {
		thError("%s: ERROR - 'THREGION' is of the 3rd kind - has only one component - unacceptable (i_psf = %d)", 
		name, i_psf_component);
		return(SH_GENERIC_ERROR);
	}
	THREGION3 *threg3 = threg->threg;
	status = thReg3Prefab(threg3, row0, col0, nrow, ncol, production);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not prefabricate (threg)", name);
		return(status);
	}
} else {
	thError("%s: ERROR - unsupported (type) of THREGION", name);
	return(SH_GENERIC_ERROR);
}
	
return(SH_SUCCESS);
}

RET_CODE thReg1PrefabFinalize(THREGION1 *threg1) {
char *name = "thReg1PrefabFinalize";
if (threg1 == NULL) {
	thError("%s: WARNING - null input: no (thregion1) to prefabricate", name);
	return(SH_SUCCESS);
}


if (threg1->fab != PREFAB) {
	if (schema_fabrication_flag == NULL) init_static_vars ();
	thError("%s: ERROR - for 'THREGION1' only PREFAB-ed regions can be finalized, received '%s' instead", 
	name, (schema_fabrication_flag->elems[threg1->fab]).name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
PSF_CONVOLUTION_TYPE psf_conv_type;
status = thPsfConvInfoGetPsfConvType(threg1->psf_conv_info, &psf_conv_type);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (psf_conv_type) from (psf_conv_info) in THREGION1", name);
	return(status);
}
if (threg1->production == SAVED && psf_conv_type == NO_PSF_CONVOLUTION) {
	threg1->memory = 0;
} else {
	threg1->memory = (MEMFL) (sizeof(THPIX)) * ((MEMFL) threg1->nrow) * ((MEMFL) threg1->ncol);
}

return(SH_SUCCESS);
}

RET_CODE thReg2PrefabFinalize(THREGION2 *threg2) {
char *name = "thReg2PrefabFinalize";
if (threg2 == NULL) {
	thError("%s: WARNING - null input: no (thregion2) to prefabricate", name);
	return(SH_SUCCESS);
}

RET_CODE status;
int n;
status = thReg2GetNComponent(threg2, &n); /* this utilizes thPsfConvInfoGetNComponent 
				to obtain the default values for n if needed */
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get number of components for this PSF", name);
	return(status);
}
if (n > N_PSF_COMPONENT) {
	thError("%s: ERROR - (n) for psf method is larger than maximum allowed. recompile the code (%d, %d)", name, n, N_PSF_COMPONENT);
	return(SH_GENERIC_ERROR);
}

FABRICATION_FLAG fab = threg2->fab; 
if (fab != INIT) {
	thError("%s: ERROR - fabrication flag '%s' not supported, expected 'INIT'", 
			name, (schema_fabrication_flag->elems[fab]).name);
	return(SH_GENERIC_ERROR);
}
int i;
fab = PREFAB;
for (i = 0; i < n; i++) {
	FABRICATION_FLAG fab_i;
	fab_i = threg2->fab_i[i];
	if (fab_i != INIT && fab != PREFAB) {
		if (schema_fabrication_flag == NULL) init_static_vars();
		thError("%s: ERROR - fabrication flag '%s' found in component (%d) not supported, expected 'INIT'", 
			name, (schema_fabrication_flag->elems[fab_i]).name, i);
		return(SH_GENERIC_ERROR);
	}
	if (fab_i == INIT) fab = INIT;
}
threg2->fab = fab;
if (fab != PREFAB) {
	thError("%s: ERROR - not all components are prefabricated, cannot finalize prefabrication", name);
	return(SH_GENERIC_ERROR);
}

/* doing memory specifications */
MEMFL memory = 0, memory_ii, *memory_i;
memory_i = threg2->memory_i;
PRODUCTION_FLAG *production;
production = threg2->production;
int *nrow, *ncol;
nrow = threg2->nrow;
ncol = threg2->ncol;
PSF_CONVOLUTION_TYPE psf_conv_type;
status = thPsfConvInfoGetPsfConvType(threg2->psf_conv_info, &psf_conv_type);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (psf_conv_type) from (psf_conv_info) in THREGION1", name);
	return(status);	
}

for (i = 0; i < n; i++) {
	PRODUCTION_FLAG production_i = production[i];
	if (production_i == RUNTIME || 
	(production_i == SAVED && psf_conv_type != NO_PSF_CONVOLUTION)) {
		memory_ii = (MEMFL) (sizeof(THPIX)) * (MEMFL) nrow[i] * (MEMFL) ncol[i];
		memory += memory_ii;
	} else if (production_i == SAVED) {
		memory_ii = 0;
	} else {
		if (schema_production_flag == NULL) init_static_vars();
		thError("%s: ERROR - production flag '%s' found in component (%d) is not supported", name, (schema_production_flag->elems[production_i]).name, i);	
		return(SH_GENERIC_ERROR);
	}	
	memory_i[i] = memory_ii;
	}
for (i = n; i < N_PSF_COMPONENT; i++) {
	memory_i[i] = 0;
}	

threg2->memory = memory;	 
return(SH_SUCCESS);
}

RET_CODE thReg3PrefabFinalize(THREGION3 *threg3) {
char *name = "thReg3PrefabFinalize";
if (threg3 == NULL) {
	thError("%s: WARNING - null input: no (thregion3) to prefabricate", name);
	return(SH_SUCCESS);
}


if (threg3->fab != PREFAB) {
	if (schema_fabrication_flag == NULL) init_static_vars ();
	thError("%s: ERROR - for 'THREGION3' only PREFAB-ed regions can be finalized, received '%s' instead", 
	name, (schema_fabrication_flag->elems[threg3->fab]).name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
PSF_CONVOLUTION_TYPE psf_conv_type;
status = thPsfConvInfoGetPsfConvType(threg3->psf_conv_info, &psf_conv_type);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (psf_conv_type) from (psf_conv_info) in THREGION1", name);
	return(status);
}
if (threg3->production == SAVED && psf_conv_type == NO_PSF_CONVOLUTION) {
	threg3->memory = 0;
} else {
	threg3->memory = (MEMFL) (sizeof(THPIX) * (threg3->nrow + threg3->ncol));
}

return(SH_SUCCESS);
}



RET_CODE thRegPrefabFinalize(THREGION *threg) {
char *name = "thRegPrefabFinalize";
if (threg == NULL) {
	thError("%s: WARNING - null input: no (thregion) to prefabricate", name);
	return(SH_SUCCESS);
}
THREGION_TYPE regtype = threg->type;
if (regtype == THREGION2_TYPE) {
	THREGION2 *threg2 = threg->threg;
	RET_CODE status;
	status = thReg2PrefabFinalize(threg2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not finalize prefabrication of (thregion)", name);
		return(status);
	}
	return(SH_SUCCESS);
} else if (regtype == THREGION1_TYPE) {
	THREGION1 *threg1 = threg->threg;
	RET_CODE status;
	status = thReg1PrefabFinalize(threg1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not finalize prefabrication of (thregion)", name);
		return(status);
	}
	return(SH_SUCCESS);
} else if (regtype == THREGION3_TYPE) {
	THREGION3 *threg3 = threg->threg;
	RET_CODE status;
	status = thReg3PrefabFinalize(threg3);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not finalize prefabrication of (thregion)", name);
		return(status);
	}
	return(SH_SUCCESS);
} else {
	if (schema_thregion_type == NULL) init_static_vars();
	thError("%s: ERROR - regtype of type '%s' not supported", 
		name, (schema_thregion_type->elems[regtype]).name);
	return(SH_GENERIC_ERROR);
}	

}

RET_CODE thReg1Fab(THREGION1 *threg, REGION *source) {
char *name = "thReg1Fab";
if (threg == NULL && source == NULL) {
	thError("%s: WARNING - null input", name);
	return(SH_SUCCESS);
	}
if (threg == NULL && source != NULL) {
	thError("%s: ERROR - cannot put any source region in a null THREGION1", name);
	return(SH_SUCCESS);
}
if (threg->fab != PREFAB) {
	if (schema_fabrication_flag == NULL) init_static_vars();
	thError("%s: ERROR - (threg) should be PREFAB-ed - current fabrication flag is '%s'", 
		name, (schema_fabrication_flag->elems[threg->fab]).name);  
	return(SH_GENERIC_ERROR);
}
if (threg->production != SAVED && source != NULL) {
	if (schema_production_flag == NULL) init_static_vars();
	thError("%s: ERROR - a source region provided while production type is '%s' - requires 'SAVED' production type", 
		name, (schema_production_flag->elems[threg->production]).name);
	return(SH_GENERIC_ERROR);
}

if (threg->production == SAVED && source == NULL) {
	if (schema_production_flag == NULL) init_static_vars();
	thError("%s: ERROR - a (threg) of production type 'SAVED' cannot be fabricated without source region", name);
	return(SH_GENERIC_ERROR);
}

int nrow = threg->nrow;
int ncol = threg->ncol;
int row0 = threg->row0;
int col0 = threg->col0;

if (threg->production == SAVED) {
	if (threg->nrow != source->nrow || threg->ncol != source->ncol) {
		thError("%s: ERROR - prefab'ed (threg) contains (nrow = %d, ncol = %d), found (%d, %d) instead", name, threg->nrow, threg->ncol, source->nrow, source->ncol);
		return(SH_GENERIC_ERROR);
	}
	REGION *reg = threg->reg;
	if (reg->type != source->type) {
		thError("%s: ERROR - prefab'ed region and source should have the same pixel type", name);
		return(SH_GENERIC_ERROR);
	}
	if (reg->type != TYPE_THPIX) {
		thError("%s: ERROR - limited pixel type support", name);
		return(SH_GENERIC_ERROR);
	}
	reg->rows_thpix = source->rows_thpix; 
	reg->nrow = nrow;
	reg->ncol = ncol;
	reg->row0 = row0;
	reg->col0 = col0;
	threg->fab = FAB;
	return(SH_SUCCESS);
} else if (threg->production == RUNTIME) {
	REGION *reg = threg->reg;
	if (reg->type != TYPE_THPIX) {
		thError("%s: ERROR - limited pixel type support", name);
		return(SH_GENERIC_ERROR);
	}
	
	/* old school oct 16, 2012 */
	if (nrow < 0 || ncol < 0) {
		thError("%s: ERROR - unsupported (nrow, ncol) = (%d, %d)", name, nrow, ncol);
		return(SH_GENERIC_ERROR);
	}
	#if WISE_REGION_ALLOCATION
	#if PREFAB_ROWS
	#else
	if (nrow > 0 && p_shRegVectorGet(reg, nrow, TYPE_THPIX) == (int) NULL) {
		thError("%s: ERROR - could not allocate (row) addresses in (reg)", name);
		return(SH_GENERIC_ERROR);
	}	
	#endif
	if (nrow > 0 && ncol > 0 && p_shRegRowsGet(reg, nrow, ncol, TYPE_THPIX) == (int) NULL) {
		thError("%s: ERROR - could not allocated (rows) in (reg)", name);
		return(SH_GENERIC_ERROR);
	}
	#else
	if (nrow > 0) reg->rows_thpix = thCalloc(nrow, sizeof(THPIX *));
	if (nrow * ncol > 0) reg->rows_thpix[0] = thCalloc(nrow * ncol, sizeof(THPIX));
	int i;
	for (i = 0; i < nrow; i++) {
		reg->rows_thpix[i] = reg->rows_thpix[0] + ncol; 
	}
	reg->nrow = nrow;
	reg->ncol = ncol;
	#endif

	#if WARN_HUGE_REGION
	if (threg->nrow >= MAXNROW || threg->ncol >= MAXNCOL) {
		thError("%s: WARNING - encountered a huge region (%s)", name, reg->name);
	}
	#endif

	reg->row0 = threg->row0;
	reg->col0 = threg->col0;
	threg->fab = FAB;

	return(SH_SUCCESS);
} else {
	if (schema_production_flag == NULL) init_static_vars();
	thError("%s: ERROR - unsupported production type '%s'", name, (schema_production_flag->elems[threg->production]).name);
	return(SH_GENERIC_ERROR);
}	

}

RET_CODE thReg2Fab(THREGION2 *threg, REGION *source, int i_psf_component) {
char *name = "thReg2Fab";
if (threg == NULL) {
	thError("%s: ERROR - null (threg)", name);
	return(SH_GENERIC_ERROR);
}
if (threg->fab != PREFAB) {
	if (schema_fabrication_flag == NULL) init_static_vars();
	thError("%s: ERROR - (threg) should be PREFAB-ed - current overall fabrication flag is '%s'", 
		name, (schema_fabrication_flag->elems[threg->fab]).name);  
	return(SH_GENERIC_ERROR);
}
int n;
RET_CODE status; 
status = thReg2GetNComponent(threg, &n); /* this utilizes thPsfConvInfoGetNComponent and changes for the default value if needed */
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the number of components for the specific PSF convolution scheme in (threg)", name);
	return(status);
}
if (i_psf_component < 0 || i_psf_component >= n) {
	thError("%s: ERROR - PSF convolution scheme only supports (%d) components - unable to operate on component (%d)", 
	name, n, i_psf_component);
	return(SH_GENERIC_ERROR);
}

if (threg->production[i_psf_component] != SAVED && source != NULL) {
	if (schema_production_flag == NULL) init_static_vars();
	thError("%s: ERROR - a source region provided while production type for component (%d) is '%s' - requires 'SAVED' production type", 
		name, i_psf_component, (schema_production_flag->elems[threg->production[i_psf_component]]).name);
	return(SH_GENERIC_ERROR);
}

if (threg->production[i_psf_component] == SAVED && source == NULL) {
	if (schema_production_flag == NULL) init_static_vars();
	thError("%s: ERROR - a (threg) of production type 'SAVED' cannot be fabricated without source region", name);
	return(SH_GENERIC_ERROR);
}

int nrow = threg->nrow[i_psf_component];
int ncol = threg->ncol[i_psf_component];
int row0 = threg->row0[i_psf_component];
int col0 = threg->col0[i_psf_component];

if (threg->production[i_psf_component] == SAVED) {
	if (nrow != source->nrow || ncol != source->ncol) {
		thError("%s: ERROR - prefab'ed (threg:%d) contain (nrow= %d, ncol= %d), but (%d, %d) in source", name, i_psf_component, nrow, ncol, source->nrow, source->ncol);
		return(SH_GENERIC_ERROR);
	}
	REGION *reg = threg->reg[i_psf_component];
	if (reg->type != source->type) {
		thError("%s: ERROR - prefab'ed region and source should have the same pixel type", name);
		return(SH_GENERIC_ERROR);
	}
	if (reg->type != TYPE_THPIX) {
		thError("%s: ERROR - limited pixel type support", name);
		return(SH_GENERIC_ERROR);
	}
	reg->rows_thpix = source->rows_thpix;
	reg->nrow = nrow;
	reg->ncol = ncol;
	reg->row0 = row0;
	reg->col0 = col0;
	threg->fab_i[i_psf_component] = FAB;
	return(SH_SUCCESS);
} else if (threg->production[i_psf_component] == RUNTIME) {
	REGION *reg = threg->reg[i_psf_component];
	if (reg->type != TYPE_THPIX) {
		thError("%s: ERROR - limited pixel type support", name);
		return(SH_GENERIC_ERROR);
	}
	#if PREFAB_ROWS
	#else
	if (p_shRegVectorGet(reg, nrow, TYPE_THPIX) == (int) NULL) {
		thError("%s: ERROR - could not allocate (row) addresses in (reg) for component (%d)", name, i_psf_component);
		return(SH_GENERIC_ERROR);
	}
	#endif	
	if (p_shRegRowsGet(reg, nrow, ncol, TYPE_THPIX) == (int) NULL) {
		thError("%s: ERROR - could not allocated (rows) in (reg) for component (%d)", name, i_psf_component);
		return(SH_GENERIC_ERROR);
	}
	reg->row0 = row0;
	reg->col0 = col0;
	threg->fab_i[i_psf_component] = FAB;

	return(SH_SUCCESS);
} else {
	if (schema_production_flag == NULL) init_static_vars();
	thError("%s: ERROR - unsupported production type '%s'", 
	name, (schema_production_flag->elems[threg->production[i_psf_component]]).name);
	return(SH_GENERIC_ERROR);
}	
}

RET_CODE thReg3Fab(THREGION3 *threg, void *rsource, void *csource) {
char *name = "thReg3Fab";
if (threg == NULL && rsource == NULL && csource == NULL) {
	thError("%s: WARNING - null input", name);
	return(SH_SUCCESS);
	}
if (threg == NULL && (rsource != NULL || csource != NULL)) {
	thError("%s: ERROR - cannot put any source region in a null THREGION3", name);
	return(SH_GENERIC_ERROR);
}
if (threg->fab != PREFAB) {
	if (schema_fabrication_flag == NULL) init_static_vars();
	thError("%s: ERROR - (threg) should be PREFAB-ed - current fabrication flag is '%s'", 
		name, (schema_fabrication_flag->elems[threg->fab]).name);  
	return(SH_GENERIC_ERROR);
}
if (threg->production != SAVED && (rsource != NULL || csource != NULL)) {
	if (schema_production_flag == NULL) init_static_vars();
	thError("%s: ERROR - a source region provided while production type is '%s' - requires 'SAVED' production type", 
		name, (schema_production_flag->elems[threg->production]).name);
	return(SH_GENERIC_ERROR);
}

#if WARN_NULL_REG3
if (threg->production == SAVED && (rsource == NULL || csource == NULL)) {
	if (schema_production_flag == NULL) init_static_vars();
	if (threg->name == NULL) {
		thError("%s: WARNING - a (threg) of production type 'SAVED' is being fabricated with null source", name);
	} else {
		thError("%s: WARNING - (threg - '%s') of production type 'SAVED' is being fabricated with null source", name, threg->name);
	}
}
#endif

if (threg->production == SAVED) {
	threg->frow = rsource;
	threg->fcol = csource;
	threg->fab = FAB;
	return(SH_SUCCESS);
} else if (threg->production == RUNTIME) {
	if (threg->frow != NULL || threg->fcol != NULL) {
		thError("%s: ERROR - non-null (frow) and (fcol) found in PREFAB'ed 'THREGION3'", name);
		return(SH_GENERIC_ERROR);
	}
	threg->frow = thCalloc(threg->nrow, sizeof(THPIX));
	threg->fcol = thCalloc(threg->ncol, sizeof(THPIX));
	#if WARN_HUGE_REGION
	if (threg->nrow >= MAXNROW || threg->ncol >= MAXNCOL) {
		thError("%s: WARNING - encountered a huge region (%s)", name, threg->name);
	}
	#endif

	threg->fab = FAB;

	return(SH_SUCCESS);
} else {
	if (schema_production_flag == NULL) init_static_vars();
	thError("%s: ERROR - unsupported production type '%s'", name, (schema_production_flag->elems[threg->production]).name);
	return(SH_GENERIC_ERROR);
}	

}



RET_CODE thRegFab(THREGION *threg, REGION *source, void *rsource, void *csource, int i_psf_component) {

char *name = "thRegFab";
if (threg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
THREGION_TYPE regtype = threg->type;

if (regtype == THREGION1_TYPE) {
	if (rsource != NULL || csource != NULL) {
		thError("%s: ERROR - vector source data not supported for (thregion) of the 1st kind", name);
		return(SH_GENERIC_ERROR);
	}
	if (i_psf_component != 0) {
		thError("%s: ERROR - 'THREGION' is of the first kind - has only one component - unacceptable (i_psf = %d)", 
		name, i_psf_component);
		return(SH_GENERIC_ERROR);
	}
	THREGION1 *threg1 = threg->threg;
	status = thReg1Fab(threg1, source);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not prefabricate (threg)", name);
		return(status);
	}
} else if (regtype == THREGION2_TYPE) {
	if (rsource != NULL || csource != NULL) {
		thError("%s: ERROR - vector source data not supported for (thregion) of the 2nd kind", name);
		return(SH_GENERIC_ERROR);
	}
	THREGION2 *threg2 = threg->threg;
	status = thReg2Fab(threg2, source, i_psf_component);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not prefabricate (threg)", name);
		return(status);
	}
} else if (regtype == THREGION3_TYPE) {
	if (source != NULL) {
		thError("%s: ERROR - region source data not supported for the 3rd kind", name);
		return(SH_GENERIC_ERROR);
	}
	if (i_psf_component != 0) {
		thError("%s: ERROR - 'THREGION' is of the 3rd kind - has only one component - unacceptable (i_psf = %d)", 
		name, i_psf_component);
		return(SH_GENERIC_ERROR);
	}

	THREGION3 *threg3 = threg->threg;
	status = thReg3Fab(threg3, rsource, csource);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not prefabricate (threg)", name);
		return(status);
	}
} else {
	thError("%s: ERROR - unsupported (type) of THREGION", name);
	return(SH_GENERIC_ERROR);
}
	
return(SH_SUCCESS);
}

RET_CODE thRegFabFinalize(THREGION *threg) {
char *name = "thRegFabFinalize";
if (threg == NULL) {
	thError("%s: WARNING - null input", name);
	return(SH_SUCCESS);
}

THREGION_TYPE regtype = threg->type;
if (regtype == THREGION2_TYPE) {
	THREGION2 *threg2 = threg->threg;
	int n;
	RET_CODE status;
	status = thReg2GetNComponent(threg2, &n);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get number of components for this PSF", name);
		return(status);
	}
	if (n > N_PSF_COMPONENT) {
		thError("%s: ERROR - (n) for psf method is larger than maximum allowed. recompile the code (%d, %d)", 
		name, n, N_PSF_COMPONENT);
		return(SH_GENERIC_ERROR);
	}
	FABRICATION_FLAG fab = threg2->fab; 
	if (fab != PREFAB) {
		thError("%s: ERROR - fabrication flag '%s' not supported, expected 'PREFAB'", 
			name, (schema_fabrication_flag->elems[fab]).name);
		return(SH_GENERIC_ERROR);
	}
	int i;
	fab = FAB;
	for (i = 0; i < n; i++) {
		FABRICATION_FLAG fab_i;
		fab_i = threg2->fab_i[i];
		if (fab_i != PREFAB && fab != FAB) {
			if (schema_fabrication_flag == NULL) init_static_vars();
			thError("%s: ERROR - fabrication flag '%s' found in component (%d) not supported", 
			name, (schema_fabrication_flag->elems[fab_i]).name, i);
			return(SH_GENERIC_ERROR);
		}
		if (fab_i == PREFAB) fab = PREFAB;
	}
	threg2->fab = fab;
	if (fab != FAB) {
		thError("%s: ERROR - not all components are fabricated, cannot finalize fabrication", name);
		return(SH_GENERIC_ERROR);
	}
	return(SH_SUCCESS);

} else if (regtype == THREGION1_TYPE) {
	return(SH_SUCCESS);
} else if (regtype == THREGION3_TYPE) {
	return(SH_SUCCESS);
} else {
	if (schema_thregion_type == NULL) init_static_vars();
	thError("%s: ERROR - regtype of type '%s' not supported", 
		name, (schema_thregion_type->elems[regtype]).name);
	return(SH_GENERIC_ERROR);
}	

}

RET_CODE thReg1FreeRows(THREGION1 *threg) {
char *name = "thReg1FreeRows";
if (threg == NULL) {
	thError("%s: WARNING - null input", name);
	return(SH_SUCCESS);
}
if (threg->fab != FAB && threg->fab != PSFCONVOLVED) {
	thError("%s: ERROR - (threg) should be FAB'ed found (fab = '%s')", name, shEnumNameGetFromValue("FABRICATION_FLAG", threg->fab));
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = thRegFreeRowsByProduction(threg->reg, threg->production);
if (status != SH_SUCCESS) {
	thError("%s:ERROR - could not free rows in (reg) of (thregion)", name);
	return(status);
}
threg->fab = PREFAB;
return(SH_SUCCESS);
}

RET_CODE thReg2FreeRows(THREGION2 *threg) {
char *name = "thReg2FreeRows";
if (threg == NULL) {
	thError("%s: WARNING - null input", name);
	return(SH_SUCCESS);
}
if (threg->fab != FAB && threg->fab != PSFCONVOLVED) {
	thError("%s: ERROR - (threg) should be fully FAB'ed", name);
	return(SH_GENERIC_ERROR);
}
int n;
RET_CODE status;
status = thReg2GetNComponent(threg, &n);
if (status != SH_SUCCESS) {
	thError("%s:ERROR - could not get the number of component from (psf_conv_info)", name);
	return(status);
} 
if (n > N_PSF_COMPONENT) {
	thError("%s: ERROR - (n) for psf method is larger than maximum allowed. recompile the code (%d, %d)", 
		name, n, N_PSF_COMPONENT);
		return(SH_GENERIC_ERROR);
}
int i;
for (i = 0; i < n; i++) {
	REGION *reg = threg->reg[i];
	PRODUCTION_FLAG production = threg->production[i];
	status = thRegFreeRowsByProduction(reg, production);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not free rows in component (%d) of (threg)", name, i);
		return(status);
	}
	threg->fab_i[i] = PREFAB;
}
threg->fab = PREFAB;
return(SH_SUCCESS);
}

RET_CODE thReg3FreeRows(THREGION3 *threg) {
char *name = "thReg3FreeRows";
if (threg == NULL) {
	thError("%s: WARNING - null input", name);
	return(SH_SUCCESS);
}
if (threg->fab != FAB && threg->fab != PSFCONVOLVED) {
	thError("%s: ERROR - (threg) should be FAB'ed, found (fab = '%s')", name, shEnumNameGetFromValue("FABRICATION_FLAG", threg->fab));
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = thRegFreeVectorByProduction(threg->frow, threg->production);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not free data vector (frow) of (thregion)", name);
	return(status);
}
threg->frow = NULL;
status = thRegFreeVectorByProduction(threg->fcol, threg->production);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not free data vector (fcol) of (thregion)", name);
	return(status);
}
threg->fcol = NULL;
threg->fab = PREFAB;
return(SH_SUCCESS);
}


RET_CODE thRegFreeRows(THREGION *threg) {
char *name = "thRegFreeRows";
if (threg == NULL) {
	thError("%s: WARNING - null input", name);
	return(SH_SUCCESS);
}

RET_CODE status;
THREGION_TYPE regtype = threg->type;
if (regtype == THREGION1_TYPE) {
	THREGION1 *threg1 = threg->threg;
	status = thReg1FreeRows(threg1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not prefabricate (threg)", name);
		return(status);
	}
} else if (regtype == THREGION2_TYPE) {
	THREGION2 *threg2 = threg->threg;
	status = thReg2FreeRows(threg2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not prefabricate (threg)", name);
		return(status);
	}
} else if (regtype == THREGION3_TYPE) {
	THREGION3 *threg3 = threg->threg;
	status = thReg3FreeRows(threg3);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not prefabricate (threg)", name);
		return(status);
	}
} else {
	thError("%s: ERROR - unsupported (type) of THREGION", name);
	return(SH_GENERIC_ERROR);
}
	
return(SH_SUCCESS);
}

RET_CODE thRegFreeVectorByProduction(THPIX *fvector, PRODUCTION_FLAG production) {
if (fvector == NULL) return(SH_SUCCESS);
if (production == RUNTIME) thFree(fvector);
return(SH_SUCCESS);
}

RET_CODE thRegion1Get(THREGION1 *threg, 
		FABRICATION_FLAG *fab, 
		PSF_CONVOLUTION_INFO **psf_info, 
		REGION **reg, NAIVE_MASK **mask, int i) {
char *name = "thRegion1Get";
if (threg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

if (fab != NULL) *fab = threg->fab;
if (psf_info != NULL) *psf_info = threg->psf_conv_info;

if (i != BAD_PSF_INDEX) {
	int n;
	RET_CODE status;
	status = thReg1GetNComponent(threg, &n);
	if (status == SH_SUCCESS && (i >= n || i < 0)) {
		thError("%s: WARNING - requested component (%d) is beyond the allowed psf index (%d)", name, i, n);
		if (reg != NULL) *reg = NULL;
		if (mask != NULL) *mask = NULL;
		return(SH_SUCCESS);
	} else if (status == SH_SUCCESS) {
		if (reg != NULL) *reg = threg->reg;
		if (mask != NULL) {
			*mask = &(threg->mask);
		}
	} else if (status != SH_SUCCESS && (i < N_PSF_COMPONENT && i >= 0)) {
		thError("%s: WARNING - improper (psf_info) - crude fetching", name);
		if (reg != NULL) *reg = threg->reg;
		if (mask != NULL) { 
			*mask = &(threg->mask);
		}
	} else if (status != SH_SUCCESS) {
		thError("%s: ERROR - requested component out of memory range", name);
		if (reg != NULL) *reg = NULL;
		if (mask != NULL) *mask = NULL;
		return(SH_GENERIC_ERROR);
	} else {
		thError("%s: ERROR - source code probelm - debug suggested", name);
		return(SH_GENERIC_ERROR);
	}
} else {
	if (reg != NULL) *reg = NULL;
	if (mask != NULL) *mask = NULL;
}

return(SH_SUCCESS);

}

RET_CODE thRegion2Get(THREGION2 *threg, 
		FABRICATION_FLAG *fab, 
		PSF_CONVOLUTION_INFO **psf_info, 
		REGION **reg, NAIVE_MASK **mask, THPIX *a, int i) {
char *name = "thRegion2Get";
if (threg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (psf_info != NULL) *psf_info = threg->psf_conv_info;
if (i != BAD_PSF_INDEX) {
	int n;
	RET_CODE status;
	status = thReg2GetNComponent(threg, &n);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get number of components for (thregion2)",
			 name);
		if (reg != NULL) *reg = NULL;
		if (mask != NULL) *mask = NULL;
		if (a != NULL) *a = THNAN;
		if (fab != NULL) *fab = UNKNOWN_FABRICATION_FLAG;
		return(SH_GENERIC_ERROR);
	} 
	if (i >= n || i < 0) {
		thError("%s: WARNING - requested component (%d) is beyond the allowed psf index (%d)", name, i, n);
		if (reg != NULL) *reg = NULL;
		if (mask != NULL) *mask = NULL;
		if (a != NULL) *a = 0.0;
		if (fab != NULL) *fab = UNKNOWN_FABRICATION_FLAG;
		return(SH_SUCCESS);
	} else {
		if (reg != NULL) *reg = threg->reg[i];
		if (mask != NULL) {
			*mask = threg->mask + i;
		}
		if (a != NULL) *a = threg->a[i];
		if (fab != NULL) *fab = threg->fab_i[i];
	} 

}else {
	if (reg != NULL) *reg = NULL;
	if (mask != NULL) *mask = NULL;
	if (a != NULL) *a = THNAN;
	if (fab != NULL) *fab = threg->fab;
}

return(SH_SUCCESS);

}

RET_CODE thRegion3Get(THREGION3 *threg, 
		FABRICATION_FLAG *fab, 
		PSF_CONVOLUTION_INFO **psf_info, 
		void **frow, void **fcol, int i) {
char *name = "thRegion3Get";
if (threg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

if (fab != NULL) *fab = threg->fab;
if (psf_info != NULL) *psf_info = threg->psf_conv_info;

if (i != BAD_PSF_INDEX && i != 0) {
	thError("%s: ERROR - could not find component (%d) of (thregion3)", name, i);
	return(SH_GENERIC_ERROR);
} else {
	if (frow != NULL) *frow = threg->frow;
	if (fcol != NULL) *fcol = threg->fcol;
}

return(SH_SUCCESS);

}

RET_CODE thRegGet(THREGION *threg, 
	FABRICATION_FLAG *fab,
	PSF_CONVOLUTION_INFO **psf_info,
	REGION **reg, void **frow, void **fcol, 
	NAIVE_MASK **mask, THPIX *a, int i) {
char *name = "thRegGet";
if (threg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
THREGION_TYPE type = threg->type;
RET_CODE status;
if (type == THREGION1_TYPE) {
	THREGION1 *threg1 = threg->threg;
	status = thRegion1Get(threg1, fab, psf_info, reg, mask, i);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not retrieve the requested data from (threg1)", name);
		return(status);
	}
	if (a != NULL) *a = 1.0; /* amplifier default value when it is not defined */	
	if (frow != NULL) *frow = NULL;
	if (fcol != NULL) *fcol = NULL;
	return(SH_SUCCESS);
} else if (type == THREGION2_TYPE) {
	THREGION2 *threg2 = threg->threg;
	status = thRegion2Get(threg2, fab, psf_info, reg, mask, a, i);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not retrieve the requested data from (threg2)", name);
		return(status);
	}
	if (frow != NULL) *frow = NULL;
	if (fcol != NULL) *fcol = NULL;
	return(SH_SUCCESS);
} else if (type == THREGION3_TYPE) {
	THREGION3 *threg3 = threg->threg;
	status = thRegion3Get(threg3, fab, psf_info, frow, fcol, i);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not retrieve the requested data from (threg1)", name);
		return(status);
	}
	if (a != NULL) *a = 1.0; /* amplifier default value when it is not defined */	
	if (reg != NULL) *reg = NULL;
	if (mask != NULL) *mask = NULL;
	return(SH_SUCCESS);
} else {
	if (schema_thregion_type == NULL) init_static_vars();
	thError("%s: ERROR - unsupported (regtype) '%s' in (threg)", 
		name, (schema_thregion_type->elems[type]).name);
	return(SH_GENERIC_ERROR);
} 

thError("%s: ERROR - source code problem - debug suggested", name);
return(SH_GENERIC_ERROR);
}


RET_CODE thRegGetFabFlag(THREGION *threg, FABRICATION_FLAG *fab) {
char *name = "thRegGetFabFlag";
RET_CODE status;
status = thRegGet(threg, fab, NULL, NULL, NULL, NULL, NULL, NULL, BAD_PSF_INDEX);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not retrieve fabrication flag", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE thRegion1GetProduction(THREGION1 *threg, PRODUCTION_FLAG *production) {
char *name = "thRegion1GetProduction";
shAssert(threg != NULL && production != NULL);
*production = threg->production;
return(SH_SUCCESS);
}

RET_CODE thRegion2GetProduction(THREGION2 *threg, PRODUCTION_FLAG *production, int index) {
char *name = "thRegion2GetProduction";
shAssert(threg != NULL && production != NULL);
if (index < 0 || index >= N_PSF_COMPONENT) {
	*production = UNKNOWN_PRODUCTION_FLAG;
	thError("%s: ERROR - index (%d) out of acceptable range", name, index);
	return(SH_GENERIC_ERROR);
}
*production = threg->production[index];
return(SH_SUCCESS);
}

RET_CODE thRegion3GetProduction(THREGION3 *threg, PRODUCTION_FLAG *production) {
char *name = "thRegion3GetProduction";
shAssert(threg != NULL && production != NULL);
*production = threg->production;
return(SH_SUCCESS);
}

RET_CODE thRegGetProduction(THREGION *threg, PRODUCTION_FLAG *production, int index) {
char *name = "thRegGetProduction";
shAssert(threg != NULL && production != NULL);
THREGION_TYPE type = threg->type;
void *reg = threg->threg;
RET_CODE status;
if (type == THREGION1_TYPE) {
	if (index != 0) {
		thError("%s: ERROR - unsupported index (%d) for 'THREGION1'", name, index);
		*production = UNKNOWN_PRODUCTION_FLAG;
		return(SH_GENERIC_ERROR);
	}
	status = thRegion1GetProduction(reg, production);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get production flag for 'THREGION1'", name);
		return(status);
	}
	return(SH_SUCCESS);
} else if (type == THREGION2_TYPE) {
	status = thRegion2GetProduction(reg, production, index);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get production flag for 'THREGION2' in index (%d)", name, index);
		return(status);
	}
	return(SH_SUCCESS);
} else if (type == THREGION3_TYPE) {
	if (index != 0) {
		thError("%s: ERROR - unsupported index (%d) for 'THREGION3'", name, index);
		*production = UNKNOWN_PRODUCTION_FLAG;
		return(SH_GENERIC_ERROR);
	}
	status = thRegion3GetProduction(reg, production);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get production flag for 'THREGION3'", name);
		return(status);
	}
	return(SH_SUCCESS);
} else {
	thError("%s: ERROR - unsupported (thregion) type '%s'", name, shEnumNameGetFromValue("THREGION_TYPE", type));
	*production = UNKNOWN_PRODUCTION_FLAG;
	return(SH_GENERIC_ERROR);
}

thError("%s: ERROR - source code error", name);
return(SH_GENERIC_ERROR);
}

RET_CODE thRegion1GetPsfConvInfo(THREGION1 *threg, PSF_CONVOLUTION_INFO **psf_info) {
char *name = "thReg1GetPsfConvInfo";
RET_CODE status;
status = thRegion1Get(threg, NULL, psf_info, NULL, NULL, BAD_PSF_INDEX);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not retrive (psf_info) from (threg)", name);
	return(status);
}

return(SH_SUCCESS);
}


RET_CODE thRegion2GetPsfConvInfo(THREGION2 *threg, PSF_CONVOLUTION_INFO **psf_info) {
char *name = "thReg2GetPsfConvInfo";
RET_CODE status;
status = thRegion2Get(threg, NULL, psf_info, NULL, NULL, NULL, BAD_PSF_INDEX);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not retrive (psf_info) from (threg)", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE thRegion3GetPsfConvInfo(THREGION3 *threg, PSF_CONVOLUTION_INFO **psf_info) {
char *name = "thReg3GetPsfConvInfo";
RET_CODE status;
status = thRegion3Get(threg, NULL, psf_info, NULL, NULL, BAD_PSF_INDEX);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not retrive (psf_info) from (threg)", name);
	return(status);
}

return(SH_SUCCESS);
}



RET_CODE thRegGetPsfConvInfo(THREGION *threg, PSF_CONVOLUTION_INFO **psf_info) {
char *name = "thRegGetPsfConvInfo";
RET_CODE status;
status = thRegGet(threg, NULL, psf_info, NULL, NULL, NULL, NULL, NULL, BAD_PSF_INDEX);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not retrive (psf_info) from (threg)", name);
	return(status);
}

return(SH_SUCCESS);
}

/* note that the following put commands do not kill the psf_info already in the reg */
RET_CODE thRegion1PutPsfConvInfo(THREGION1 *reg, PSF_CONVOLUTION_INFO *psf_info) {
char *name = "thRegion1PutPsfConvInfo";
if (reg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (psf_info == NULL) {
	thError("%s: WARNING - placing (null) in (psf_info)", name);
}
reg->psf_conv_info = psf_info;
return(SH_SUCCESS);
}

RET_CODE thRegion2PutPsfConvInfo(THREGION2 *reg, PSF_CONVOLUTION_INFO *psf_info) {
char *name = "thRegion3PutPsfConvInfo";
if (reg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (psf_info == NULL) {
	thError("%s: WARNING - placing (null) in (psf_info)", name);
}
reg->psf_conv_info = psf_info;
return(SH_SUCCESS);
}

RET_CODE thRegion3PutPsfConvInfo(THREGION3 *reg, PSF_CONVOLUTION_INFO *psf_info) {
char *name = "thRegion3PsfConvInfo";
if (reg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

if (psf_info == NULL) {
	thError("%s: WARNING - placing (null) in (psf_info)", name);
}
reg->psf_conv_info = psf_info;
return(SH_SUCCESS);
}



RET_CODE thRegPutPsfConvInfo(THREGION *threg, PSF_CONVOLUTION_INFO *psf_info) {
char *name = "thRegPutPsfConvInfo";
RET_CODE status;
THREGION_TYPE type;
type = threg->type;
void *reg = threg->threg;
if (type == THREGION1_TYPE) {
	status = thRegion1PutPsfConvInfo(reg, psf_info);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put (psf_info) in (thregion1)", name);
		return(status);
	}
	return(SH_SUCCESS);
} else if (type == THREGION2_TYPE) {
	status = thRegion2PutPsfConvInfo(reg, psf_info);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put (psf_info) in (thregion2)", name);
		return(status);
	}
	return(SH_SUCCESS);
} else if (type == THREGION3_TYPE) {
	status = thRegion3PutPsfConvInfo(reg, psf_info);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put (psf_info) in (thregion3)", name);
		return(status);
	}
	return(SH_SUCCESS);
} else {
	thError("%s: ERROR - (thregion) type is not supported", name);
	return(SH_GENERIC_ERROR);
}

}

RET_CODE thRegGetRegMaskA(THREGION *threg, 
			REGION **reg, NAIVE_MASK **mask, THPIX *a,
			FABRICATION_FLAG *fab,  
			int i) {
char *name = "thRegGetRegMaskA";
RET_CODE status;
status = thRegGet(threg, fab, NULL, reg, NULL, NULL, mask, a, i);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not retrieve (reg, mask, a) for component (%d) in (threg_", 
	name, i);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE thReg1GetNComponent(THREGION1 *threg, int *nc) {
char *name = "thReg1GetNComponent";
if (threg == NULL && nc == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
}
if (threg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (nc == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
RET_CODE status;
PSF_CONVOLUTION_INFO *psf_info;
status = thRegion1GetPsfConvInfo(threg, &psf_info);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (psf_info)", name);
	return(status);
}
int n;
status = thPsfConvInfoGetNComponent(psf_info, &n);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (ncomponent) from (psf_info)", name);
	return(status);
}
PSF_CONVOLUTION_TYPE type;
status = thPsfConvInfoGetPsfConvType(psf_info, &type);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (conv_type) for (psf_info)", name);
	return(status);
}
if (n == DEFAULT_NCOMPONENT) {
	if (type == NO_PSF_CONVOLUTION) {
		*nc = 1;
		return(SH_SUCCESS);
	 } else if (type == CENTRAL_PSF) {
		*nc = 1;
		return(SH_SUCCESS);
	} else if (type == CENTRAL_AND_WINGS_PSF || type == STAR_CENTRAL_AND_WINGS) {
		*nc = 2;
		return(SH_SUCCESS);
	}
	if (schema_psf_convolution_type == NULL) init_static_vars ();
	thError("%s: ERROR - (conv_type) '%s' is not supported", name, (schema_psf_convolution_type->elems[type]).name);
	return(SH_GENERIC_ERROR);
}
if (n <= 0) {
	thError("%s: ERROR - unacceptable (ncomponent) (%d)", name, n);
	return(SH_GENERIC_ERROR);
}
if (n > N_PSF_COMPONENT) {
	thError("%s: ERROR - (ncomponent) (%d) larger than N_PSF_COMPONENT (%d)", name, n, N_PSF_COMPONENT);
	return(SH_GENERIC_ERROR);
}
*nc = n;
return(SH_SUCCESS);
}

RET_CODE thReg2GetNComponent(THREGION2 *threg, int *nc) {
char *name = "thReg2GetNComponent";
if (threg == NULL && nc == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
}
if (threg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (nc == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
RET_CODE status;
PSF_CONVOLUTION_INFO *psf_info;
status = thRegion2GetPsfConvInfo(threg, &psf_info);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (psf_info)", name);
	return(status);
}
int n;
status = thPsfConvInfoGetNComponent(psf_info, &n);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (ncomponent) from (psf_info)", name);
	return(status);
}
PSF_CONVOLUTION_TYPE type;
status = thPsfConvInfoGetPsfConvType(psf_info, &type);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (conv_type) for (psf_info)", name);
	return(status);
}
if (n == DEFAULT_NCOMPONENT) {
	if (type == NO_PSF_CONVOLUTION) {
		*nc = N_PSF_COMPONENT;
		return(SH_SUCCESS);
	}
	if (schema_psf_convolution_type == NULL) init_static_vars ();
	thError("%s: ERROR - (conv_type) '%s' is not supported", name, (schema_psf_convolution_type->elems[type]).name);
	return(SH_GENERIC_ERROR);
}
if (n <= 0) {
	thError("%s: ERROR - unacceptable (ncomponent) (%d)", name, n);
	return(SH_GENERIC_ERROR);
}
if (n > N_PSF_COMPONENT) {
	thError("%s: ERROR - (ncomponent) (%d) larger than N_PSF_COMPONENT (%d)", name, n, N_PSF_COMPONENT);
	return(SH_GENERIC_ERROR);
}
*nc = n;
return(SH_SUCCESS);
}

RET_CODE thReg3GetNComponent(THREGION3 *threg, int *nc) {
char *name = "thReg3GetNComponent";
if (threg == NULL && nc == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
}
if (threg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (nc == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
RET_CODE status;
PSF_CONVOLUTION_INFO *psf_info;
status = thRegion3GetPsfConvInfo(threg, &psf_info);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (psf_info)", name);
	return(status);
}
int n;
status = thPsfConvInfoGetNComponent(psf_info, &n);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (ncomponent) from (psf_info)", name);
	return(status);
}
PSF_CONVOLUTION_TYPE type;
status = thPsfConvInfoGetPsfConvType(psf_info, &type);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (conv_type) for (psf_info)", name);
	return(status);
}
if (n == DEFAULT_NCOMPONENT) {
	if (type == NO_PSF_CONVOLUTION) {
		*nc = 1;
		return(SH_SUCCESS);
	}
	if (schema_psf_convolution_type == NULL) init_static_vars ();
	thError("%s: ERROR - (conv_type) '%s' is not supported for (thregion) of 3rd kind", 
		name, (schema_psf_convolution_type->elems[type]).name);
	return(SH_GENERIC_ERROR);
}
if (n <= 0) {
	thError("%s: ERROR - unacceptable (ncomponent) (%d)", name, n);
	return(SH_GENERIC_ERROR);
}
if (n > N_PSF_COMPONENT) {
	thError("%s: ERROR - (ncomponent) (%d) larger than N_PSF_COMPONENT (%d)", name, n, N_PSF_COMPONENT);
	return(SH_GENERIC_ERROR);
}
*nc = n;
return(SH_SUCCESS);
}

RET_CODE thRegGetNComponent(THREGION *threg, int *nc) {
char *name = "thRegGetNComponent";
if (threg == NULL && nc == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
}
if (threg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (nc == NULL) {
	thError("%s: WARNING - null output placeholder", name);	
	return(SH_SUCCESS);
}
RET_CODE status;
if (threg->type == THREGION1_TYPE) {
	/* 
	status = thReg1GetNComponent(threg->threg, nc);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (ncomponent) for (threg1)", name);
		return(status);
	}
	*/
	*nc = 1;
	return(SH_SUCCESS);
}  else if (threg->type == THREGION3_TYPE) {
	status = thReg3GetNComponent(threg->threg, nc);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (ncomponent) for (threg3)", name);
		return(status);
	}
	return(SH_SUCCESS);
} else if (threg->type == THREGION2_TYPE) {
	status = thReg2GetNComponent(threg->threg, nc);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (ncomponent) for (threg2)", name);
		return(status);
	}
	return(SH_SUCCESS);
}

if (schema_thregion_type == NULL) init_static_vars();
thError("%s: ERROR - unsupported type of (thregion) '%s'", 
	name, (schema_thregion_type->elems[threg->type]).name);
return(SH_GENERIC_ERROR);
}

RET_CODE thRegion1GetMemory(THREGION1 *threg, MEMFL *memory) {
char *name = "thRegion1GetMemory";
if (threg == NULL && memory == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
}
if (memory == NULL) {
	thError("%s: WARNING - null output placeholder", name);	
	return(SH_SUCCESS);
}
if (threg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
FABRICATION_FLAG fab = threg->fab;
if (fab != PREFAB && fab != FAB && fab != PSFCONVOLVED) {
	thError("%s: ERROR - unsupported fabrication flag (%d, '%s')", name, 
	fab, shEnumNameGetFromValue("FABRICATION_FLAG", fab));
	return(SH_GENERIC_ERROR);
}
if (fab == PREFAB) {
	RET_CODE status = thReg1PrefabFinalize(threg);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not finalize prefabrication of (thregion1)", name);
		return(status);
	}
}
*memory = threg->memory;
return(SH_SUCCESS);
} 

RET_CODE thRegion2GetMemory(THREGION2 *threg, MEMFL *memory) {
char *name = "thRegion2GetMemory";
if (threg == NULL && memory == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
}
if (memory == NULL) {
	thError("%s: WARNING - null output placeholder", name);	
	return(SH_SUCCESS);
}
if (threg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
FABRICATION_FLAG fab = threg->fab;
if (fab != PREFAB && fab != FAB && fab != PSFCONVOLVED) {
	thError("%s: ERROR - unsupported fabrication flag (%d, '%s')", name, 
	fab, shEnumNameGetFromValue("FABRICATION_FLAG", fab));
	return(SH_GENERIC_ERROR);
}
/* we assume that all thregions are finalized 
if (fab == PREFAB) {
	RET_CODE status = thReg2PrefabFinalize(threg);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not finalize prefabrication of (thregion2)", name);
		return(status);
	}
}
*/
*memory = threg->memory;
return(SH_SUCCESS);
} 

RET_CODE thRegion3GetMemory(THREGION3 *threg, MEMFL *memory) {
char *name = "thRegion1GetMemory";
if (threg == NULL && memory == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
}
if (memory == NULL) {
	thError("%s: WARNING - null output placeholder", name);	
	return(SH_SUCCESS);
}
if (threg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
FABRICATION_FLAG fab = threg->fab;
if (fab != PREFAB && fab != FAB && fab != PSFCONVOLVED) {
	thError("%s: ERROR - unsupported fabrication flag (%d, '%s')", name, 
	fab, shEnumNameGetFromValue("FABRICATION_FLAG", fab));
	return(SH_GENERIC_ERROR);
}
if (fab == PREFAB) {
	RET_CODE status = thReg3PrefabFinalize(threg);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not finalize prefabrication of (thregion1)", name);
		return(status);
	}
}
*memory = threg->memory;
return(SH_SUCCESS);
} 

RET_CODE thRegGetMemory(THREGION *threg, MEMFL *memory) {
char *name = "thRegGetMemory";
if (threg == NULL && memory == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
}
if (threg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (memory == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
RET_CODE status;
THREGION_TYPE regtype = threg->type;
if (regtype == THREGION1_TYPE) {
	status = thRegion1GetMemory(threg->threg, memory);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not retrieve memory for (thregion1)", name);
		return(status);
	}
} else if (regtype == THREGION2_TYPE) {
	status = thRegion2GetMemory(threg->threg, memory);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not retrieve memory form (thregion2)", name);
		return(status);
	}
} else if (regtype == THREGION3_TYPE) {
	status = thRegion3GetMemory(threg->threg, memory);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not retrieve memory form (thregion2)", name);
		return(status);
	}
} else {
	thError("%s: ERROR - unsupported (thregion_type) '%s'", 
		name, (schema_thregion_type->elems[regtype]).name); 
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE thRegGetType(THREGION *threg, THREGION_TYPE *type) {
char *name = "thRegGetType";
if (threg == NULL && type == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
}
if (threg == NULL) {
	*type = UNKNOWN_THREGION_TYPE;
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (type == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
*type = threg->type;
return(SH_SUCCESS);
}

RET_CODE thRegGetTypeAndReg(THREGION *threg, THREGION_TYPE *type, void **reg) {
char *name = "thRegGetType";
if (threg == NULL && type == NULL && reg == NULL) {
	thError("%s: WARNING - null input and output placeholder", name);
	return(SH_SUCCESS);
}
if (threg == NULL) {
	*type = UNKNOWN_THREGION_TYPE;
	*reg = NULL;
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (type == NULL && reg == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}

if (type != NULL) *type = threg->type;
if (reg != NULL) *reg = threg->threg;

return(SH_SUCCESS);
}

