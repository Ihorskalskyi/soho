pro gen_overlapfile,  run,  camcol,  field0,  field1,  filename,  nf =  nf

print,  "* generating overlapfile for "
print,  "* run = ",  run,  ", camcol = ", camcol
print,  "* field0 = ",  field0,  ", field1 = ",  field1
if (n_elements(nf) ne  0) then begin
    nf =  nf[0]
endif else begin
    nf =  10
endelse

if (nf le 0) then begin
    nf =  10
endif

print,  "* packaging into ", nf,  "fields per process"

get_lun,  lun
openw,  lun,  filename

runstr =  strtrim(run,  2)
camcolstr =  strtrim(camcol,  2)
nfields =  field1 - field0 + 1

for i =  1,  nfields,  1 do begin

if (i eq 1) then begin
    printf,  lun,  "{ Process #",  (i / nf + 1)
endif

field =  i - 1 + field0
fieldstr =  strtrim(field,  2)

printf,  lun,  'Run: ',  runstr, ' Camcol: ',  camcolstr,  ' Field: ',  fieldstr

if (((i mod nf) eq 0) && (i ne 1) && (i ne nfields)) then begin
    printf,  lun,  "}"
    printf,  lun,  "{ Process #",  (i / nf + 1)
endif

if ((i eq nfields)) then begin
    printf,  lun,  "}"
endif

endfor

free_lun,  lun

END

