
#ifndef THPAR_H
#define THPAR_H

#include <stdio.h>
#include <string.h>
#include "dervish.h"
#include "thParTypes.h"


#ifndef MAX
#define MAX(a,b) \
       ({ typeof (a) _a = (a); \
           typeof (b) _b = (b); \
         _a > _b ? _a : _b; })
#endif
#ifndef MIN
#define MIN(a,b) \
       ({ typeof (a) _a = (a); \
           typeof (b) _b = (b); \
         _a < _b ? _a : _b; })
#endif

typedef enum {
  PAR_EOSTRUCT, PAR_EOF, 
  PAR_MOSTRUCT, PAR_HDR,
  PAR_DOSTRUCT, PAR_BOSTRUCT
} PAR_STATUS;

typedef struct par_fil {
  char *name;
  FILE  *fil;
  int hdr_len;
  char **hdr;
} PAR_FILE;

typedef struct par_data {
  CHAIN *ecalib;
  CHAIN *ccdconfig;
} PAR_DATA;

PAR_STATUS check_ln_delimiter(FILE *fil);

char **read_line(FILE *fil, const int n);
void skip_line(FILE *fil, const int n);
void skip_empty_space(FILE *fil);
void read_i(FILE *fil, void *var) ;
void read_l(FILE *fil, void *var) ;
void read_ll(FILE *fil, void  *var) ;
void read_f(FILE *fil, void *var) ;
void read_d(FILE *fil, void *var) ;
void read_ld(FILE *fil, void *var) ;
void read_s(FILE *fil, void *var) ;
void read_i_heap(FILE *fil, void *var, const int n) ;
void read_l_heap(FILE *fil, void *var, const int n) ;
void read_ll_heap(FILE *fil, void  *var, const int n) ;
void read_f_heap(FILE *fil, void *var, const int n) ;
void read_d_heap(FILE *fil, void *var, const int n) ;
void read_ld_heap(FILE *fil, void *var, const int n) ;
void read_s_heap(FILE *fil, void **var, const int n) ;
void read_enum(FILE *fil, int *var, char **enum_str_list, const int n) ;
void skip_var(FILE *fil);
void skip_vars(FILE *fil, const int n);
void read_FRAMEINFO(FILE *fil, FRAMEINFO *var /* output */); 
void read_RUNMARK(FILE *fil, RUNMARK *var /* output */);
void read_IMAGINGRUN(FILE *fil, IMAGINGRUN *var /* output */); 
void read_DFTYPE(FILE *fil, DFTYPE*var /* output */);
void read_CCDBC(FILE *fil, CCDBC*var /* output */); 
void read_CCDGEOMETRY(FILE *fil, CCDGEOMETRY*var /* output */); 
void read_DEWARGEOMETRY(FILE *fil,/* input */ DEWARGEOMETRY* var);
void read_CCDCONFIG(FILE *fil,CCDCONFIG*var /* output */);
void read_LINEARITY_TYPE(FILE *fil, LINEARITY_TYPE*var /* output */); 
void read_ECALIB(FILE *fil, ECALIB*var /* output */);
void read_RUNLIST(FILE *fil, RUNLIST *var /* output */); 

PAR_FILE *ini_par_file(char *fname, int hdr_len);
RET_CODE fini_par_file(PAR_FILE *fil);

RET_CODE read_stru_chain(PAR_FILE *fil, 
			 void read_stru(FILE *fil, void *stru), 
			 char *stru_name, int stru_size, 
			 int l, CHAIN *stru_chain); 


/* 
   searching for specific camcol and camrow in 
   information read from par files 
*/

ECALIB *get_ecalib_from_chain(CHAIN *chain, 
			      int camrow, int camcol);
CCDCONFIG *get_ccdconfig_from_chain(CHAIN *chain, 
				    int camrow, int camcol);

/* 
   conversion of PARDARA into field specific AMPL info 
*/

CHAIN *thAmplChainGenFromPardata(PAR_DATA *pardata, 
				 int camrow, int camcol, RET_CODE *status);

CHAIN *thParReadCcdconfig(char *file, RET_CODE *status);
CHAIN *thParReadEcalib(char *file, RET_CODE *status);

#endif
