#!/bin/bash

source "./generate_temporary_file.bash"
source "./run_commands_in_file.bash"

name="mle-lrg"
rerun=$DEFAULT_SDSS_RERUN

# generate a temporary command file
commandfile=$(generate_temporary_file --root="./" --prefix="temp-mle-lrg-submit-batch-" --suffix=".cmd")

declare -a camcol_a=("2" "4" "5" "6")
n0=1

declare -a source_model_a=("sdss-g0-s0-lrg1-aligned-6" "sdss-g0-s0-cd1-aligned-2" "sdss-gA-sA-cd1-aligned-2" "sdss-g0-s0-cd1" "sdss-gA-sA-cd1")
declare -a source_class_a=("DEVEXP_GCLASS" "DEVEXP_GCLASS" "DEVEXP_GCLASS" "DEVEXP_GCLASS")
n1=1

declare -a source_a=("fpC" "fpCC")
declare -a fit2fpCC_flag_a=("" "-fit2fpCC")
n2=1

declare -a target_model_a=("deV-exp" "deV" "exp")
declare -a target_class_a=("DEVEXP_GCLASS" "DEV_GCLASS" "EXP_GCLASS")
n3=1

declare -a nobjc_flag_a=("" "" "" "-nobjc 1" "-nobjc 1")
declare -a nobjcstr_a=("" "multiple-init-6-pcost-8-DD1-Rlog-stage-1" "multiple-init-6-pcost-8-DD1-Rlog-stage-2")
declare -a multistageflag_a=("" "" "-multistage")
n4=1

#stripe 82
#4797
declare -a run_a=(4207 4797 2700 2703 2708 2709 2960 3565 3434 3437 2968 1742 4247 4252 4253 4263 4288 1863 1887 2570 2578 2579 109 125 211 240 241 250 251 256 259 273 287 297 307 94 5036 5042 5052 2873 21
24 1006 1009 1013 1033 1040 1055 1056 1057 4153 4157 4158 4184 4187 4188 4191 4192 2336 2886 2955 3325 3354 3355 3360 3362 3368 2385 2506 2728 4849 4858 4868 4874 4894 4895 4899 4905 4927 4930 4933 4948 2854 2855 2856
 2861 2662 3256 3313 3322 4073 4198 4203 3384 3388 3427 3430 4128 4136 4145 2738 2768 2820 1752 1755 1894 2583 2585 2589 2591 2649 2650 2659 2677 3438 3460 3458 3461 3465 7674 7183)
n5=120
n5=12
n5=12
n5=1

#-fitsize -fitcenter -fitshape 
# code=5 was used for unconstrained MLE on demand 
#executable="do-mle-lrg-demand-pcost-multi-init-50-nstep-60"
#code=8 
#executable="do-mle-lrg-multi-init-15-nstep-150"
#code=9

declare -a executable_a=("do-mle-lrg-unconstrained-multi-init-1-nstep-9000-nearby-10-full-sky-apsector-hqn" "do-mle-lrg-unconstrained-multi-init-1-nstep-9000-nearby-10-full-sky-apsector-hqn" "do-mle-lrg-multi-init-1-nstep-0-nearby-10" "do-mle-lrg-multi-init-1-nstep-9000")
declare -a fitflags_a=("-fitsize" "-fitsize -fixsky " "-fitsize" "-fitsize" "-fitsize" "-fitsize" "-fitsize")
declare -a dirflags_a=("flag-fitsize-unconstrained-init-1-nstep-9000-nearby-10-full-sky-apsector-hqn" "flag-fitsize-unconstrained-init-1-nstep-9000-nearby-10-sky-subtracted-apsector-hqn" "flag-amplitude-unconstrained-init-1-nstep-9000-nearby-10-full-sky" "flag-fitsize-unconstrained-init-1-nstep-9000-nearby-10-full-sky" "flag-fitsize-init-1-nstep-0-nearby-10" "flag-fitsize-init-1-nstep-9000" "flag-fitsize-init-1-nstep-1-nearby-25"  "flag-fitsize-init-1-nstep-9000-nearby-25" "flag-fitsize-10" "flag-fitsize-11" "flag-fitsize-12" "flag-fitsize-13")
n6=2

#declare -a fitflags_a=("-fitsize" "" "-fitsize" "" "-fitsize" "-fitcenter" "-fitshape" "-fitsize -fitcenter" "-fitsize -fitshape" "-fitcenter -fitshape" "-fitsize -fitcenter -fitshape")
#declare -a dirflags_a=("flag-fitsize-$code" "flag-amplitude-$code" "flag-fitsize-$code" "flag-amplitude-$code" "flag-fitsize-$code" "flag-fitcenter-$code" "flag-fitshape-$code" "flag-fitsize-fitcenter-$code" "flag-fitsize-fitshape-$code" "flag-fitcenter-fitshape-$code" "flag-fitsize-fitcenter-fitshape-$code")
#n6=1

#declare -a fitflags_a=("-fitcenter" "-fitshape" "-fitsize -fitcenter" "-fitsize -fitshape" "-fitcenter -fitshape" "-fitsize -fitcenter -fitshape")
#declare -a dirflags_a=("flag-fitcenter-$code" "flag-fitshape-$code" "flag-fitsize-fitcenter-$code" "flag-fitsize-fitshape-$code" "flag-fitcenter-fitshape-$code" "flag-fitsize-fitcenter-fitshape-$code")
#n6=6


declare -a cd_model_a=("deV-deV" "deV")
declare -a cd_class_a=("DEVDEV_GCLASS" "DEV_GCLASS")
n7=1

#declare -a sim_demand_code_a=(1 2 3 4 11 12 13 14 6 7 8 9 16 17 18 19)
#declare -a sim_demand_code_a=(21 22 23 24 31 32 33 34 26 27 28 29 36 37 38 39)
#declare -a sim_demand_code_a=(41 42 43 44 51 52 53 54 46 47 48 49 56 57 58 59 61 62 63 64 71 72 73 74 66 67 68 69 76 77 78 79)
#n8=32

demand_suffix="a"
declare -a sim_demand_code_a=(52 57 52 57 52 72 77)
n8=1
 
declare -a mle_demand_alteration_a=(60 100 0 100 200 300 400 500 600) #(0 100)
n9=1

declare -a mixed_noise_dir_a=("" "multi-init" "mixed-noise" "")
n10=1

for (( i10=0; i10<$n10; i10++ ))
do

for (( i0=0; i0<$n0; i0++ ))
do

for (( i1=0; i1<$n1; i1++ ))
do

for (( i2=0; i2<$n2; i2++ ))
do

for (( i3=0; i3<$n3; i3++ ))
do

for (( i4=0; i4<$n4; i4++ ))
do

for (( i5=0; i5<$n5; i5++ ))
do

for (( i6=0; i6<$n6; i6++ ))
do

for (( i7=0; i7<$n7; i7++ ))
do

for (( i8=0; i8<$n8; i8++ ))
do

for (( i9=0; i9<$n9; i9++ ))
do

camcol=${camcol_a[$i0]}

source_model=${source_model_a[$i1]}

source=${source_a[$i2]}
fit2fpCC_flag=${fit2fpCC_flag_a[$i2]}

target_model=${target_model_a[$i3]}
target_class=${target_class_a[$i3]}

nobjc_flag=${nobjc_flag_a[$i4]}
nobjcstr=${nobjcstr_a[$i4]}
multistage_flag=${multistageflag_a[$i4]}

run=${run_a[$i5]}

fitflag=${fitflags_a[$i6]}
dirflag=${dirflags_a[$i6]}
executable=${executable_a[$i6]} # for optimization of multi-init runs

cdclass=${cd_class_a[$i7]}

sim_demand_code=${sim_demand_code_a[$i8]}
mle_demand_extra=${mle_demand_alteration_a[$i9]}
mle_demand_code=$((sim_demand_code + mle_demand_extra))
mle_demand_flag="-demand $mle_demand_code"
sim_demand_dir="demand-$sim_demand_code$demand_suffix"
mle_demand_dir="mle-demand-$mle_demand_code"
demand_model="sim-$sim_demand_dir-$mle_demand_dir"

mixed_noise_dir=${mixed_noise_dir_a[$i10]}

	root="/u/khosrow/thesis/opt/soho/multi-object/images"
	#outdir="$root/mle/$source_model/$target_model-$source-$nobjcstr/$dirflag/$rerun/$run/$camcol"
	#outdir="$root/mle/$source_model/sim-$sim_demand_dir/$mle_demand_dir/$dirflag/$rerun/$run/$camcol"
	#outdir="$root/mle/sim-$sim_demand_dir/$mle_demand_dir/$dirflag/$rerun/$run/$camcol"
	#datadir="$root/sims/$sim_demand_dir/$rerun/$run/$camcol"
	outdir="$root/mle/float/$mixed_noise_dir/sim-$sim_demand_dir/$mle_demand_dir/$dirflag/$rerun/$run/$camcol"
	datadir="$root/sims/float/$mixed_noise_dir/$sim_demand_dir/$rerun/$run/$camcol"
	field0="0001"
	field1="9999"

	#init_flag="SDSSpERROR_INIT -error 2"
	init_flag="SDSS_INIT"
	maxseed=4294967295 # the biggest unsigned int that can be passed as a seed to simulator 
	seed=$(python -S -c "import random; print(random.randrange(1,$maxseed))")

	#extra_flags="-fitsize -fitcenter -fitshape $nobjc_flag $multistage_flag -gclass $target_class -init $init_flag $fit2fpCC_flag -cD_classify -cD_force -seed $seed"
	#extra_flags="$fitflag $nobjc_flag $multistage_flag -gclass $target_class -init $init_flag $fit2fpCC_flag -cD_classify -cD_force -cdclass $cdclass -seed $seed"
	extra_flags="$fitflag $mle_demand_flag -gclass $target_class -init $init_flag $fit2fpCC_flag -cD_classify -cD_force -cdclass $cdclass"

	#executable="do-mle-lrg-2"
	#mle-script.sh --run=$run --rerun=$rerun --outdir=$outdir --datadir=$datadir --model=$source_model --fstep=1 --EXTRA_FLAGS="$extra_flags" --executable=$executable --field0=$field0 --field1=$field1 --camcol=$camcol --commandfile=$commandfile --multiband

	#executable="do-mle-lrg-demand-pcost-multi-init-50-nstep-60"
	#executable="do-mle-lrg-multi-init-15-nstep-150" refer above for optimization of multiinit runs
	mle-script.sh --run=$run --rerun=$rerun --outdir=$outdir --datadir=$datadir --model=$demand_model --fstep=1 --EXTRA_FLAGS="$extra_flags" --executable=$executable --field0=$field0 --field1=$field1 --camcol=$camcol --commandfile=$commandfile --multiband


done
done
done
done
done
done
done
done
done
done
done


echo "$name: randomly running commands in file '$commandfile'"
#run_commands_in_file --commandfile=$commandfile --random

echo "$name: not deleting temp-file = $commandfile"
#rm $commandfile

