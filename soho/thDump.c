#include "thDump.h"
#include "thStat.h"
#include "thFitmask.h"
#include "thMGalaxy.h"

static int dump_write_flag = DEFAULT_DUMP_WRITE_FLAG;

static int compare_frame_io(const void *v1, const void *v2);

static RET_CODE ZscoreMakeFromResidual(REGION *res, LWMASK *lwmask, REGION *z_image, THPIX badpixel);

static RET_CODE print_chisq_stats_for_various_run(CHAIN *io_chain);

RET_CODE thDumpInit(DUMP_WRITE_MODE mode) {
char *name = "thDumpInit";
int target_flag = DEFAULT_DUMP_WRITE_FLAG;
if (mode == DUMP_DEFAULT) {
	target_flag = DEFAULT_DUMP_WRITE_FLAG;
} else if (mode == DUMP_OVERWRITE) {
	target_flag = DUMP_OVERWRITE_FLAG;
} else if (mode == DUMP_APPEND) {
	target_flag = DUMP_APPEND_FLAG;
} else {
	thError("%s: ERROR - dump mode ('%s') not recognized", name, shEnumNameGetFromValue("DUMP_WRITE_MODE", mode));
	return(SH_GENERIC_ERROR);
}
if (dump_write_flag == target_flag) {
	thError("%s: WARNING - requested dump mode to change into (%d) while already set to (%d)", name, target_flag, dump_write_flag);
	return(SH_SUCCESS);
}
printf("%s: requested dump mode to change into ('%s', %d), previously set to (%d) \n", name, shEnumNameGetFromValue("DUMP_WRITE_MODE", mode), target_flag, dump_write_flag);
dump_write_flag = target_flag;
return(SH_SUCCESS);
}

int thDumpGetFlag() {
return(dump_write_flag);
}

RET_CODE thFrameInitLogDump(FRAME *f) {
char *name = "thFrameInitLogDump";
if (f == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

FRAMEIO *fio = NULL;
LOGDUMP *log = NULL;

RET_CODE status;
status = thFrameGetFrameio(f, &fio);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (frameio) from (frame)", name);
	return(status);
}
FRAMEFILES *ff = NULL;
status = thFrameGetFramefiles(f, &ff);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (framefiles)", name);
	return(status);
}
status = thFrameioGetLogDump(fio, &log);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (logdump) from (frameio)", name);
	return(status);
}
if (log == NULL || ff == NULL || fio == NULL) {
	thError("%s: ERROR - imperoperly allocated frame", name);
	return(SH_GENERIC_ERROR);
}

/* setting up the filename */
strcpy(log->filename, ff->thlogfile);
status = thFrameGetStamp(f, log->stamp);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not generate frame stamp", name);
	return(status);
}
/* setting substructures */
status = thFrameInitCcdinfo(f, log->ccdinfo);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not init (ccdinfo)", name);
	return(status);
}
status = thFrameInitRuninfo(f, log->runinfo);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not init (runinfo)", name);
	return(status);
}

status = thFrameInitAlginfo(f, log->alginfo);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not init (alginfo)", name);
	return(status);
}

status = thFrameInitFieldinfo(f, log->fieldinfo);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not init (fieldinfo)", name);
	return(status);
}

status = thFrameInitIoinfo(f, log->ioinfo);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not init (ioinfo)", name);
	return(status);
}

return(SH_SUCCESS);
}


 
RET_CODE thFrameFiniLogDump(FRAME *f) {
char *name = "thFrameFiniLogDump";
if (f == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

FRAMEIO *fio = NULL;
LOGDUMP *log = NULL;
RET_CODE status;
status = thFrameGetFrameio(f, &fio);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (frameio) from (frame)", name);
	return(status);
}
status = thFrameioGetLogDump(fio, &log);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (logdump) from (frameio)", name);
	return(status);
}

status = thFrameFiniRuninfo(f, log->runinfo);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not init (runinfo)", name);
	return(status);
}

status = thFrameFiniAlginfo(f, log->alginfo);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not init (alginfo)", name);
	return(status);
}

status = thFrameFiniFieldinfo(f, log->fieldinfo);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not init (fieldinfo)", name);
	return(status);
}

return(SH_SUCCESS);
}



RET_CODE thFrameInitSkyDump(FRAME *f) {
char *name = "thFrameInitSkyDump"; 
if (f == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

FRAMEIO *fio = NULL;
SKYDUMP *sky = NULL;
RET_CODE status;
status = thFrameGetFrameio(f, &fio);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (frameio) from (frame)", name);
	return(status);
}
FRAMEFILES *ff = NULL;
status = thFrameGetFramefiles(f, &ff);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (framefiles)", name);
	return(status);
}
status = thFrameioGetSkyDump(fio, &sky);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (logdump) from (frameio)", name);
	return(status);
}
if (sky == NULL || ff == NULL || fio == NULL) {
	thError("%s: ERROR - imperoperly allocated frame", name);
	return(SH_GENERIC_ERROR);
}

/* setting up the filename */
strcpy(sky->filename, ff->thsmfile);
return(SH_SUCCESS);
}

RET_CODE thFrameFiniSkyDump(FRAME *f) {
char *name = "thFrameFiniSkyDump";
if (f == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
FRAMEIO *fio = NULL;
status = thFrameGetFrameio(f, &fio);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (frameio)", name);
	return(SH_GENERIC_ERROR);
}
SKYDUMP *sky;
status = thFrameioGetSkyDump(fio, &sky);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (skydump)", name);
	return(status);
}

THOBJC *skyobjc = NULL;
status = thFrameGetSkyobjc(f, &skyobjc);
shAssert(skyobjc != NULL);
TYPE iotype = UNKNOWN_SCHEMA;
void *iopar = NULL;
status = thObjcGetIopar(skyobjc, &iopar, &iotype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (iopar) for (sky) object", name);
	return(status);
}
shAssert(iotype != UNKNOWN_SCHEMA);
shAssert(iopar != NULL);
char *ioname = shNameGetFromType(iotype);

status = thqqTrans(iopar, ioname, sky->skypars, "SKYPARS");
if (status != SH_SUCCESS) {
	thError("%s: ERROR - failed to transfor onto (skypars)", name);
	return(status);
}

REGION *reg = sky->reg;
shAssert(reg != NULL);
status = thObjcMakeImage(skyobjc, reg);
return(SH_SUCCESS);
}

RET_CODE thFrameInitObjcDump(FRAME *f) {
char *name = "thFrameInitObjcDump";
if (f == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
FRAMEIO *fio = NULL;
OBJCDUMP *objc = NULL;
RET_CODE status;
status = thFrameGetFrameio(f, &fio);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (frameio) from (frame)", name);
	return(status);
}
FRAMEFILES *ff = NULL;
status = thFrameGetFramefiles(f, &ff);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (framefiles)", name);
	return(status);
}
status = thFrameioGetObjcDump(fio, &objc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (logdump) from (frameio)", name);
	return(status);
}
if (objc == NULL || ff == NULL || fio == NULL) {
	thError("%s: ERROR - imperoperly allocated frame", name);
	return(SH_GENERIC_ERROR);
}
HDR *hdr = NULL;
status = thFrameGetHdr(f, &hdr);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get main (hdr) from (frame)", name);
	return(status);
}
objc->hdr = hdr;
strcpy(objc->filename, ff->thobjcfile);
#if DUMP_SDSSOBJC
status = thFrameGetSdssObjc(f, objc->sdssobjc, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (sdssobjc) from (frame)", name);
	return(status);
}
#endif
#if DUMP_SDSSOBJC2
status = thFrameGetSdssObjc(f, NULL, objc->sdssobjc2);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (sdssobjc2) from (frame)", name);
	return(status);
}
#endif
#if DUMP_INITOBJC
status = thFrameGetFIObjc(f, objc->initobjc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (initobjc) from (frame)", name);
	return(status);
}
#endif

return(SH_SUCCESS);
}

RET_CODE thFrameFiniObjcDump(FRAME *f) {
char *name = "thFrameFiniObjcDump";
if (f == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
RUN_MODE run_mode = UNKNOWN_RUN_MODE;
status = thMGalaxyModeGet(&run_mode);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the (run_mod) from (MGalaxy) module", name);
	return(status);
}
FRAMEIO *fio = NULL;
OBJCDUMP *objc = NULL;
status = thFrameGetFrameio(f, &fio);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (frameio) from (frame)", name);
	return(status);
}
FRAMEFILES *ff = NULL;
status = thFrameGetFramefiles(f, &ff);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (framefiles)", name);
	return(status);
}
status = thFrameioGetObjcDump(fio, &objc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (logdump) from (frameio)", name);
	return(status);
}
if (objc == NULL || ff == NULL || fio == NULL) {
	thError("%s: ERROR - imperoperly allocated frame", name);
	return(SH_GENERIC_ERROR);
}
#if DUMP_FITOBJC
status = thFrameUpdateFitmask(f);
if (status != SH_SUCCESS && run_mode != MLE_ON_DEMAND_MODE) {
	thError("%s: ERROR - could not update (fitmask) for (frame)", name);
	return(status);
} else if (status != SH_SUCCESS && run_mode == MLE_ON_DEMAND_MODE) {
	thError("%s: WARNING - could not update (fitmask) for (frame) while (run_model = '%s')", name, shEnumNameGetFromValue("RUN_MODE", run_mode));
}
status = thFrameGetFIObjc(f, objc->fitobjc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (fitobjc) from (frame)", name);
	return(status);
}
#endif
#if DUMP_PHOBJC
status = thFrameGetPhObjc(f, objc->phobjc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (phobjc) from (frame)", name);
	return(status)
}
#endif
#if DUMP_LM_INTO_GPOBJC
CHAIN *lmchain = NULL;
status = thObjcDumpGetLMChain(objc, &lmchain);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmchain) from (objcdump)", name);
	return(status);
}
if (lmchain == NULL) {
	thError("%s: ERROR - null (lmchain) - improperly allocated (objcdump)", name);
	return(SH_GENERIC_ERROR);
}
if (shChainSize(lmchain) != 0) {
	thError("%s: ERROR - filled (lmchain) - improperly allocated (objcdump)", name);
	return(SH_GENERIC_ERROR);
}
LMETHOD *lm_init = NULL, *lm_fini = NULL;
status = thFrameGetInitFiniLM(f, &lm_init, &lm_fini);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (init and fini LMs) from (frame)", name);
	return(status);
}

LMETHOD *lm_init_copy = thLmethodNew();
LMETHOD *lm_fini_copy = thLmethodNew();
status = thLmethodCopy(lm_init, lm_init_copy, 1);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not copy (lm_init)", name);
	return(status);
}
status = thLmethodCopy(lm_fini, lm_fini_copy, 1);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not copy (lm_fini)", name);
	return(status);
}
shChainElementAddByPos(lmchain, lm_init_copy, "LMETHOD", TAIL, AFTER);
shChainElementAddByPos(lmchain, lm_fini_copy, "LMETHOD", TAIL, AFTER);
#endif
#if DUMP_PROF_INTO_GPOBJC
CHAIN *prof_init = NULL, *prof_fit = NULL;
status = thObjcDumpGetLPChain(objc, &prof_init, &prof_fit);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (profchains) from (objcdump)", name);
	return(status);
}
if (prof_init == NULL || prof_fit == NULL ) {
	thError("%s: ERROR - null (profchains) from (objcdump)", name);
	return(SH_GENERIC_ERROR);
}
if (shChainSize(prof_init) != 0 || shChainSize(prof_fit) != 0) {
	thError("%s: ERROR - filled (profchains) - improperly llocated (objcdump)", name);
	return(SH_GENERIC_ERROR);
}
CHAIN *lprof_init = NULL, *lprof_fini = NULL;
status = thFrameGetInitFiniLP(f, &lprof_init, &lprof_fini);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lprof chains) from (frame)", name);
	return(status);
}
int i, n;
void *prof = NULL;
n = shChainSize(lprof_init);
for (i = 0; i < n; i++) {
	prof = shChainElementGetByPos(lprof_init, i);
	shChainElementAddByPos(prof, prof_init, "LPROF", TAIL, AFTER);
}
n = shChainSize(lprof_fini);
for (i = 0; i < n; i++) {
	prof = shChainElementGetByPos(lprof_fini, i);
	shChainElementAddByPos(prof, prof_fit, "LPROF", TAIL, AFTER);
}

#endif

return(SH_SUCCESS);
}

RET_CODE thFrameInitLMDump(FRAME *f) {
char *name = "thFrameInitLMDump";
if (f == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
FRAMEIO *fio = NULL;
LMDUMP *lm = NULL;
RET_CODE status;
status = thFrameGetFrameio(f, &fio);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (frameio) from (frame)", name);
	return(status);
}
FRAMEFILES *ff = NULL;
status = thFrameGetFramefiles(f, &ff);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (framefiles)", name);
	return(status);
}
status = thFrameioGetLMDump(fio, &lm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (LMdump) from (frameio)", name);
	return(status);
}
if (lm == NULL || ff == NULL || fio == NULL) {
	thError("%s: ERROR - imperoperly allocated frame", name);
	return(SH_GENERIC_ERROR);
}
if (strlen(ff->thlmfile) == 0) {
	thError("%s: WARNING - empty string found for (lmfile) in (framefiles)", name);
}
HDR *hdr = NULL;
status = thFrameGetHdr(f, &hdr);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (hdr) from (frame)", name);
	return(status);
}
if (hdr == NULL) {
	thError("%s: WARNING - null (hdr) found in (frame)", name);
	return(SH_GENERIC_ERROR);
}


strcpy(lm->filename, ff->thlmfile);
lm->hdr = hdr;
return(SH_SUCCESS);
}

RET_CODE thFrameFiniLMDump(FRAME *f) {
char *name = "thFrameFiniLMDump";
if (f == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
FRAMEIO *fio = NULL;
LMDUMP *lmdump = NULL;
RET_CODE status;
status = thFrameGetFrameio(f, &fio);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (frameio) from (frame)", name);
	return(status);
}
status = thFrameioGetLMDump(fio, &lmdump);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (LMdump) from (frameio)", name);
	return(status);
}
LSTRUCT *l = NULL;
status = thFrameGetLstruct(f, &l);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lstruct) from (frame)", name);
	return(status);
}
LFIT *lf = NULL;
status = thLstructGetLfit(l, &lf);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lfit) from (lstruct)", name);
	return(status);
}
if (lmdump == NULL || fio == NULL || l == NULL || lf == NULL) {
	thError("%s: ERROR - imperoperly allocated frame", name);
	return(SH_GENERIC_ERROR);
}
CHAIN *lm_chain = lmdump->lm;
int i, n;
n = lf->nlm;
lmdump->doflag = (lf->rtype & LM_RECORD_ANY);
LMETHOD *lm_array = lf->lms;
if (n < 0) {
	thError("%s: ERROR - (nlm = %d < 0) found in lfit", name, n);
	return(SH_GENERIC_ERROR);
} else if (n == 0) {
	thError("%s: WARNING - (nlm = %d) found in (lfit)", name, n);
	thError("%s: WARNING - placing out of bond chisq in the LMETHOD to be exported to (lm_chain)", name);
	LMETHOD *lm_copy = thLmethodNew();
	int err = 0;
	if (thLmethodPutCflag(lm_copy, UNKNOWN_CONVERGENCE) != SH_SUCCESS) err++;
	if (thLmethodPutChisq(lm_copy, CHISQ_IS_BAD, CHISQ_IS_BAD) != SH_SUCCESS) err++;
	if (thLmethodPutStatus(lm_copy, UNKNOWN_LM_STEP_STATUS, 0, 0) != SH_SUCCESS) err++;
	if (thLmethodPutStepCount(lm_copy, 0, 0)  != SH_SUCCESS) err++;
	if (thLmethodPutDeltaQn(lm_copy, (MLEFL) 0.0) != SH_SUCCESS) err++;
	if (thLmethodPutType(lm_copy, UNKNOWN_LMETHODTYPE, UNKNOWN_LMETHODSUBTYPE) != SH_SUCCESS) err++;
	if (err > 0) {
		thError("%s: ERROR - could not update fake (lm_copy) in [%d] instances", name, err);
		return(SH_GENERIC_ERROR);
	}
	shChainElementAddByPos(lm_chain, lm_copy, "LMETHOD", TAIL, AFTER);
}
for (i = 0; i <  n; i++) {
	LMETHOD *lm = lm_array + i;
	LMETHOD *lm_copy = thLmethodNew();
	status = thLmethodCopy(lm, lm_copy, 1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not make a copy of (lmethod) in (lm_array[%d])", name, i);
		return(status);
	}
	shChainElementAddByPos(lm_chain, lm_copy, "LMETHOD", TAIL, AFTER);
}

return(SH_SUCCESS);
}

RET_CODE thFrameInitLPDump(FRAME *f) {
char *name = "thFrameInitLPDump";
if (f == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
FRAMEIO *fio = NULL;
LPDUMP *lp = NULL;
RET_CODE status;
status = thFrameGetFrameio(f, &fio);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (frameio) from (frame)", name);
	return(status);
}
FRAMEFILES *ff = NULL;
status = thFrameGetFramefiles(f, &ff);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (framefiles)", name);
	return(status);
}
status = thFrameioGetLPDump(fio, &lp);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (LPdump) from (frameio)", name);
	return(status);
}
if (lp == NULL || ff == NULL || fio == NULL) {
	thError("%s: ERROR - imperoperly allocated frame", name);
	return(SH_GENERIC_ERROR);
}
HDR *hdr = NULL;
status = thFrameGetHdr(f, &hdr);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (hdr) from (frame)", name);
	return(status);
}
if (hdr == NULL) {
	thError("%s: WARNING - null (hdr) found in (frame)", name);
	return(SH_GENERIC_ERROR);
}

strcpy(lp->filename, ff->thlpfile);
lp->hdr = hdr;
return(SH_SUCCESS);
}

RET_CODE thFrameFiniLPDump(FRAME *f) {
char *name = "thFrameFiniLPDump";
if (f == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
FRAMEIO *fio = NULL;
LPDUMP *lpdump = NULL;
RET_CODE status;
status = thFrameGetFrameio(f, &fio);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (frameio) from (frame)", name);
	return(status);
}
status = thFrameioGetLPDump(fio, &lpdump);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (LPdump) from (frameio)", name);
	return(status);
}
LSTRUCT *l = NULL;
status = thFrameGetLstruct(f, &l);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lstruct) from (frame)", name);
	return(status);
}
LFIT *lf = NULL;
status = thLstructGetLfit(l, &lf);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lfit) from (lstruct)", name);
	return(status);
}
if (lpdump == NULL || fio == NULL || l == NULL || lf == NULL) {
	thError("%s: ERROR - imperoperly allocated frame", name);
	return(SH_GENERIC_ERROR);
}
CHAIN *lp_chain = lpdump->lp;
int i, n, nmax;
status = thLfitGetNLprofile(lf, &n, &nmax);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (n_lprofile) from (lfit)", name);
	return(status);
}
lpdump->doflag = (lf->rtype & LP_RECORD_ANY) || (lf->rtype & LP_RECORD_LASTOFBEST);
LPROFILE *lp_array = lf->lps;
for (i = 0; i < n; i++) {
	LPROFILE *lp = lp_array + i;
	LPROFILE *lp_copy = thLprofileNew();
	status = thLprofileCopy(lp, lp_copy, 1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy (lprofile) from (lp_array[%d]", name, i);
		return(status);
	}
	shChainElementAddByPos(lp_chain, lp, "LPROFILE", TAIL, AFTER);
}

return(SH_SUCCESS);
}

RET_CODE thFrameInitLRZDump(FRAME *f) {
char *name = "thFrameInitLRZDump";
if (f == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
FRAMEIO *fio = NULL;
LRZDUMP *lrz = NULL;
RET_CODE status;
status = thFrameGetFrameio(f, &fio);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (frameio) from (frame)", name);
	return(status);
}
FRAMEFILES *ff = NULL;
status = thFrameGetFramefiles(f, &ff);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (framefiles)", name);
	return(status);
}
status = thFrameioGetLRZDump(fio, &lrz);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (LPdump) from (frameio)", name);
	return(status);
}
if (lrz == NULL || ff == NULL || fio == NULL) {
	thError("%s: ERROR - imperoperly allocated frame", name);
	return(SH_GENERIC_ERROR);
}
HDR *hdr = NULL;
status = thFrameGetHdr(f, &hdr);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (hdr) from (frame)", name);
	return(status);
}
if (hdr == NULL) {
	thError("%s: WARNING - null (hdr) found in (frame)", name);
	return(SH_GENERIC_ERROR);
}
strcpy(lrz->rfilename, ff->thlrfile);
strcpy(lrz->zfilename, ff->thlzfile);
strcpy(lrz->wfilename, ff->thlwfile);
lrz->hdr = hdr;
#if DUMP_LRZ
lrz->doflag = 1;
#else
lrz->doflag = 0;
#endif
return(SH_SUCCESS);
}

RET_CODE thFrameFiniLRZDump(FRAME *f) {
char *name = "thFrameFiniLRZDump";
if (f == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
FRAMEIO *fio = NULL;
LRZDUMP *lrzdump = NULL;
RET_CODE status;
status = thFrameGetFrameio(f, &fio);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (frameio) from (frame)", name);
	return(status);
}
status = thFrameioGetLRZDump(fio, &lrzdump);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (LPdump) from (frameio)", name);
	return(status);
}
LSTRUCT *l = NULL;
status = thFrameGetLstruct(f, &l);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lstruct) from (frame)", name);
	return(status);
}
LFIT *lf = NULL;
status = thLstructGetLfit(l, &lf);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lfit) from (lstruct)", name);
	return(status);
}
if (lrzdump == NULL || fio == NULL || l == NULL || lf == NULL) {
	thError("%s: ERROR - imperoperly allocated frame", name);
	return(SH_GENERIC_ERROR);
}

int doflag = lrzdump->doflag;
if (doflag == 0) {
	return(SH_SUCCESS);
} else if (doflag != 1) {
	thError("%s: ERROR - unacceptable value for (doflag = %d)", name, doflag);
	return(SH_GENERIC_ERROR);
}

status = thMakeMAlgRun(l);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make (model) image", name);
	return(status);
}
REGION *r_image = lrzdump->r_image;
REGION *z_image = lrzdump->z_image;
REGION *w_image = lrzdump->w_image;
if (r_image == NULL) {
	thError("%s: ERROR - null (r-image) - improperly allocated (lrzdump)", name);
	return(SH_GENERIC_ERROR);
}
if (z_image == NULL) {
	thError("%s: ERROR - null (z-image) - improperly allocated (lrzdump)", name);
	return(SH_GENERIC_ERROR);
}
if (w_image == NULL) {
	thError("%s: ERROR - null (w-image) - improperly allocated (lrzdump)", name);
	return(SH_GENERIC_ERROR);
}

REGION *dmm = NULL;
status = thExtractDmm(l, &dmm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract | m - d >", name);
	return(status);
}
if (dmm == NULL) {
	thError("%s: ERROR - null (d - m) region - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}

int nrow, ncol;
nrow = dmm->nrow;
ncol = dmm->ncol;
if (nrow == 0 || ncol == 0) {
	thError("%s: ERROR - (d - m) matrix of unacceptable size (nrow = %d, ncol = %d)", name, nrow, ncol);
	return(SH_GENERIC_ERROR);
}
int tnrow, tncol;
tnrow = r_image->nrow;
tncol = r_image->ncol;
if (tnrow != nrow || tncol != ncol) {
	status = shRegReshape(r_image, nrow, ncol);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not reshape (r-image)", name);
		return(status);
	}
}
shRegPixCopy(dmm, r_image);

LWMASK *lwmask = NULL;
status = thLstructGetLwmask (l, &lwmask);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwmask) from (lstruct)", name);
	return(status);
}

status = ZscoreMakeFromResidual(dmm, lwmask, z_image, (THPIX) BAD_Z_SCORE); 
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make (z-image) from (r-image) and (lwmask)", name);
	return(status);
}

tnrow = w_image->nrow;
tncol = w_image->ncol;
if (tnrow != nrow || tncol != ncol) {
	status = shRegReshape(w_image, nrow, ncol);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not reshape (w-image)", name);
		return(status);
	}
}
REGION *W = NULL;
status = thLwmaskGetW(lwmask, &W);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (W) from (lwmask)", name);
	return(status);
}
shRegPixCopy(W, w_image);


return(SH_SUCCESS);
}

RET_CODE thFrameInitIo(FRAME *f) /* does the objcs ini, log init, ccd info, run info, stamp, file info */ {
char *name = "thFrameInitIo";
RET_CODE status;

FRAMEIO *io = thFrameioNew("OBJCPARS");
status = thFramePutFrameio(f, io);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not put (frameio) in place", name);
	return(status);
}

#if DUMP_LOG
status = thFrameInitLogDump(f);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not init (logdump)", name);
	return(status);
}
#endif
#if DUMP_SKY
status = thFrameInitSkyDump(f);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not init (skydump)", name);
	return(status);
}
#endif
#if DUMP_OBJC
status = thFrameInitObjcDump(f);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not (init) (objcdump)", name);
	return(status);
}
#endif
#if DUMP_LM
status = thFrameInitLMDump(f);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not (init) (lmdump)", name);
	return(status);
}
#endif
#if DUMP_PROF
status = thFrameInitLPDump(f);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not (init) (lprof)", name);
	return(status);
}
#endif
#if DUMP_LRZ
status = thFrameInitLRZDump(f);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not (init) (lrzdump)", name);
	return(status);
}
#endif
return(SH_SUCCESS);
}


RET_CODE thFrameFiniIo(FRAME *f) /* does the objcs, maps, log fini, sky */
{
char *name = "thFrameFiniIo";
RET_CODE status;

#if DUMP_LOG
status = thFrameFiniLogDump(f);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not finilize (logdump)", name);
	return(status);
}
#endif
#if DUMP_LRZ
status = thFrameFiniLRZDump(f);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not finalize (lrzdump)", name);
	return(status);
}
#endif
#if DUMP_SKY
status = thFrameFiniSkyDump(f);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not finalize (skydump)", name);
	return(status);
}
#endif
#if DUMP_OBJC
status = thFrameFiniObjcDump(f);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not finalize (objcdump)", name);
	return(status);
}
#endif
#if DUMP_LM
status = thFrameFiniLMDump(f);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not finalize (lmdump)", name);
	return(status);
}
#endif
#if DUMP_PROF
status = thFrameFiniLPDump(f);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not finalize (lprof)", name);
	return(status);
}
#endif

return(SH_SUCCESS);
}

RET_CODE thDumpLog(LOGDUMP *log) {
char *name = "thDumpLog";

RET_CODE status;

FILE *file = NULL;
char *hdr = NULL, *filename = log->filename;
if (filename == NULL || strlen(filename) == 0) {
	thError("%s: WARNING - empty filename discovered, dump failed", name);
	return(SH_SUCCESS);
};

file = fopen(filename, "w");

/* write stamp first */
fprintf(file, "%s", log->stamp);

hdr = "ccd info";
status = thAsciiDumpStru(file, log->ccdinfo, "CCDINFO", NULL, NULL, ALL_RECORDS, hdr);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump (ccdinfo) in (logfile)", name);
	return(status);
}
hdr = "run info";
status = thAsciiDumpStru(file, log->runinfo, "RUNINFO", NULL, NULL, ALL_RECORDS, hdr);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump (runinfo) in (logfile)", name);
	return(status);
}
hdr = "algorithm info";
status = thAsciiDumpStru(file, log->alginfo, "ALGINFO", NULL, NULL, ALL_RECORDS, hdr);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump (alginfo) in (logfile)", name);
	return(status);
}
hdr = "field info";
status = thAsciiDumpStru(file, log->fieldinfo, "FIELDINFO", NULL, NULL, ALL_RECORDS, hdr);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump (fieldinfo) in (logfile)", name);
	return(status);
}
hdr = "fit info";
status = thAsciiDumpStru(file, log->fitinfo, "FITINFO", NULL, NULL, ALL_RECORDS, hdr);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump (fitinfo) in (logfile)", name);
	return(status);
}

fclose(file);

return(SH_SUCCESS);
}

RET_CODE thDumpSky(SKYDUMP *sky) {
char *name = "thDumpSky";
if  (sky == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

PHFITSFILE *fd;
char *filename = sky->filename;
if (filename == NULL || strlen(filename) == 0) {
	thError("%s: WARNING - empty filename discovered, dump failed", name);
	return(SH_SUCCESS);
}

HDR *hdr = sky->hdr;
fd = phFitsBinTblOpen(filename, dump_write_flag, hdr);
if (fd == NULL) {
	thError("%s: ERROR - could not open (skyfile) '%s'", name, filename);
	return(SH_GENERIC_ERROR);
}

char *type = "SKYDUMP";
if (phFitsBinTblHdrWrite(fd, type, NULL) < 0) {
	thError("%s: ERROR - could not write schema header for type '%s'", name, type);
	return(SH_GENERIC_ERROR);
}
if (phFitsBinTblRowWrite(fd, sky) < 0) {
	thError("%s: ERROR - could not write sky information", name);
	return(SH_GENERIC_ERROR);
}
phFitsBinTblEnd(fd);
if (phFitsBinTblClose(fd) < 0) {
	thError("%s: ERROR - could not close sky file", name);
	return(SH_GENERIC_ERROR);
}
return(SH_SUCCESS);
}

RET_CODE thDumpObjc(OBJCDUMP *objc) {
char *name = "thDumpObjc";
if (objc == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

PHFITSFILE *fd;
char *filename = objc->filename;
if (filename == NULL || strlen(filename) == 0) {
	thError("%s: WARNING - empty filename discovered, dump failed", name);
	return(SH_SUCCESS);
}
	
HDR *hdr = objc->hdr;
if (hdr == NULL) {
	thError("%s: WARNING - null (hdr) discovered in (objcdump)", name);
}
fd = phFitsBinTblOpen(filename, dump_write_flag, hdr);
if (fd == NULL) {
	thError("%s: ERROR - could not open (objcfile), '%s'", name, filename);
	return(SH_GENERIC_ERROR);
}
char *type;

#if DUMP_SDSSOBJC
#if USE_FNAL_OBJC_IO
type = "FNAL_OBJC_IO";
phFitsBinForgetSchemaTrans(type);
thSchemaGetFnalObjcIo(DUMP_MODE);
#else
type = "OBJC_IO";
#endif
if (phFitsBinTblHdrWrite(fd, type, NULL) < 0) {
	thError("%s: ERROR - could not write header information for type '%s'", name, type);
	return(SH_GENERIC_ERROR);
}
if (phFitsBinTblChainWrite(fd, objc->sdssobjc) < 0) {
	thError("%s: ERROR - could not write chain of '%s'", name, type);
	return(SH_GENERIC_ERROR);
}
if (shChainSize(objc->sdssobjc) == 0) {
	thError("%s: WARNING - empty (sdssobjc)", name);
}
phFitsBinTblEnd(fd);
#if USE_FNAL_OBJC_IO
type = "FNAL_OBJC_IO";
phFitsBinForgetSchemaTrans(type);
thSchemaGetFnalObjcIo(PHOTO_INPUT_MODE);
#endif

#endif

#if DUMP_SDSSOBJC2
type = "CALIB_PHOBJC_IO";
if (phFitsBinTblHdrWrite(fd, type, NULL) < 0) {
	thError("%s: ERROR - could not write header information for type '%s'", name, type);
	return(SH_GENERIC_ERROR);
}
if (phFitsBinTblChainWrite(fd, objc->sdssobjc2) < 0) {
	thError("%s: ERROR - could not write chain of '%s'", name, type);
	return(SH_GENERIC_ERROR);
}
if (shChainSize(objc->sdssobjc2) == 0) {
	thError("%s: WARNING - empty (sdssobjc2)", name);
}
phFitsBinTblEnd(fd);
#endif

#if DUMP_INITOBJC
type = "OBJCPARS";
if (phFitsBinTblHdrWrite(fd, type, NULL) < 0) {
	thError("%s: ERROR - could not write header information for type '%s' (initobjc)", name, type);
	return(SH_GENERIC_ERROR);
}
if (phFitsBinTblChainWrite(fd, objc->initobjc) < 0) {
	thError("%s: ERROR - could not write chain of '%s' (initobjc)", name, type);
	return(SH_GENERIC_ERROR);
}
if (shChainSize(objc->initobjc) == 0) {
	thError("%s: WARNING - empty (initobjc)", name);
}
phFitsBinTblEnd(fd);
#endif
#if DUMP_FITOBJC
type = "OBJCPARS";
if (phFitsBinTblHdrWrite(fd, type, NULL) < 0) {
	thError("%s: ERROR - could not write header information for type '%s' (fitobjc)", name, type);
	return(SH_GENERIC_ERROR);
}
if (phFitsBinTblChainWrite(fd, objc->fitobjc) < 0) {
	thError("%s: ERROR - could not write chain of '%s' (fitobjc)", name, type);
	return(SH_GENERIC_ERROR);
}
if (shChainSize(objc->fitobjc) == 0) {
	thError("%s: WARNING - empty (fitobjc)", name);
} 
phFitsBinTblEnd(fd);
#endif
#if DUMP_LM_INTO_GPOBJC
type = "LMETHOD";
if (phFitsBinTblHdrWrite(fd, type, NULL) < 0) {
	thError("%s: ERROR - could not write header information for type '%s' (LM)", name, type);
	return(SH_GENERIC_ERROR);
}
if (phFitsBinTblChainWrite(fd, objc->lm) < 0) {
	thError("%s: ERROR - could not write '%s' (LM)", name, type);
	return(SH_GENERIC_ERROR);
}
if (shChainSize(objc->lm) == 0) {
	thError("%s: WARNING - empty (lmchain)", name);
}
phFitsBinTblEnd(fd);
#endif
#if DUMP_PROF_INTO_GPOBJC
	type = "LPROF";
	#if DUMP_PROF_INIT
	if (phFitsBinTblHdrWrite(fd, type, NULL) < 0) {
		thError("%s: ERROR - could not write header information for type '%s' (LPROF)", name, type);
		return(SH_GENERIC_ERROR);
	}
	if (phFitsBinTblChainWrite(fd, objc->lprof_init) < 0) {
		thError("%s: ERROR - could not write '%s' (LPROF _init)", name, type);
		return(SH_GENERIC_ERROR);
	}
	if (shChainSize(objc->lprof_init== 0) {
		thError("%s: WARNING - empty (lprof_init)", name);
	}
	phFitsBinTblEnd(fd);
	#endif
	#if DUMP_PROF_FIT
	if (phFitsBinTblHdrWrite(fd, type, NULL) < 0) {
		thError("%s: ERROR - could not write header information for type '%s' (LPROF)", name, type);
		return(SH_GENERIC_ERROR);
	}
	if (phFitsBinTblChainWrite(fd, objc->lprof_fit) < 0) {
		thError("%s: ERROR - could not write '%s' (LPROF _fit)", name, type);
		return(SH_GENERIC_ERROR);
	}
	if (shChainSize(objc->lprof_fit == 0) {
		thError("%s: WARNING - empty (lprof_fit)", name);
	}
	phFitsBinTblEnd(fd);
	#endif
#endif
if (phFitsBinTblClose(fd) < 0) {
	thError("%s: ERROR - could not close binary table", name);
	return(SH_GENERIC_ERROR);
}
return(SH_SUCCESS);
}

RET_CODE thFrameDumpIo(FRAME *f, int load_best) {
char *name = "thFrameDumpIo";
if (f == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

if (load_best != 0 && load_best != 1) {
	thError("%s: (load_best) flag can only accept (0, 1), received (%d)", name, load_best);
	return(SH_GENERIC_ERROR);
}

FRAMEIO *fio = NULL;
RET_CODE status;
#if (DUMP_PROF || DUMP_LM) 
if (load_best == 1) {
	/* the following selects the best run in terms of chi-squared value */
	status = thFrameOrderIo(f);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not load (io) for the best run", name);
		return(status);
	}
	#if DUMP_PROF
	status = thFrameAddLprofileLastOfBest(f);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add last of best (lprofile) to (frameio)", name);
		return(status);
	}
	#endif
} else {
	status = thFrameOrderIo(f);
	if (status != SH_SUCCESS) {
		thError("%s: WARNING - could not order (io_chain) in (frame) - proceeding", name);
	}
	thError("%s: ERROR - (JoinIo) not currently supported", name);
	return(SH_GENERIC_ERROR);

	#if 0
	status = thFrameJoinIo(f);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not join all (frameio) in (frame)", name);
		return(status);
	}
	#endif
}
#endif

status = thFrameGetFrameio(f, &fio);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (frameio)", name);
	return(status);
}
#if DUMP_LOG
LOGDUMP *log = NULL;
status = thFrameioGetLogDump(fio, &log);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (logdump)", name);
	return(status);
}
status = thDumpLog(log);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump (log)", name);
	return(status);
}

#endif
#if DUMP_SKY
SKYDUMP *sky = NULL;
status = thFrameioGetSkyDump(fio, &sky);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (skydump)", name);
	return(status);
}
status = thDumpSky(sky);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump (sky)", name);
	return(status);
}
#endif
#if DUMP_OBJC
OBJCDUMP *objc = NULL;
status = thFrameioGetObjcDump(fio, &objc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (objcdump)", name);
	return(status);
}
status = thDumpObjc(objc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump (objc)", name);
	return(status);
}
#endif
#if DUMP_LM
LMDUMP *lm = NULL;
status = thFrameioGetLMDump(fio, &lm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmdimp)", name);
	return(status);
}
status = thDumpLM(lm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump (LM)", name);
	return(status);
}
#endif
#if DUMP_PROF
LPDUMP *lp = NULL;
status = thFrameioGetLPDump(fio, &lp);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lpdump)", name);
	return(status);
}
status = thDumpLP(lp);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump (LPROF)", name);
	return(status);
}
#endif
#if DUMP_LRZ
LRZDUMP *lrz = NULL;
status = thFrameioGetLRZDump(fio, &lrz);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lrzdump)", name);
	return(status);
}
status = thDumpLRZ(lrz);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump (LRZ)", name);
	return(status);
}
#endif

return(SH_SUCCESS);
}

RET_CODE thFrameGetSkyobjc(FRAME *f, THOBJC **objc) {
char *name = "thFrameGetSkyobjc";
thError("%s: ERROR - unsupported function", name);
return(SH_GENERIC_ERROR);
}

RET_CODE thFrameGetSdssObjc(FRAME *f, CHAIN *sdssobjc, CHAIN *sdssobjc2) {
char *name = "thFrameGetSdssObjc";
if (f == NULL) {
	thError("%s: ERROR - null frame", name);
	return(SH_GENERIC_ERROR);
}
if (sdssobjc != NULL && shChainSize(sdssobjc) != 0) {
	thError("%s: ERROR - expected a chain of length zero for 'sdssobjc'", name);
	return(SH_GENERIC_ERROR);
}
if (sdssobjc2 != NULL && shChainSize(sdssobjc2) != 0) {
	thError("%s: ERROR - expected a chain of length zero for 'sdssobjc2'", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
LSTRUCT *l = NULL;
status = thFrameGetLstruct(f, &l);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lstruct) from (frame)", name);
	return(status);
}
MAPMACHINE *map = NULL;
status = thLstructGetLmachine(l, &map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (mapmachine) from (lstruct)", name);
	return(status);
}
MCFLAG cflag = ALTERED;
status = thMapmachineGetCflag(map, &cflag);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract compilation flag from (map)", name);
	return(status);
} else if (cflag != COMPILED) {
	thError("%s: ERROR - (map) needs to be compiled", name);
	return(SH_GENERIC_ERROR);
}

/* taking list of objects from map */
int nfitobjc = 0; 
void **objcs = NULL;
int nobjc = -1;
#if DUMP_AMPOBJC
status = thMapmachineGetUObjcs(map, &objcs, &nobjc);
#else
status = thMapmachineGetUPObjcs(map, &objcs, &nobjc);
#endif
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (object list) from (map)", name);
	return(status);
}
if (nobjc == 0) {
	thError("%s: WARNING - object list contains (0) elements", name);
	return(SH_SUCCESS);
}
#if USE_FNAL_OBJC_IO
char *target_pname = "FNAL_OBJC_IO";
#else
char *target_pname = "OBJC_IO";
#endif
TYPE ptype = shTypeGetFromName(target_pname); 
if (sdssobjc != NULL) shChainTypeGet(sdssobjc);
char *pname = shNameGetFromType(ptype);
if (ptype == UNKNOWN_SCHEMA || pname == NULL || strlen(pname) == 0) {
	thError("%s: ERROR - unknown io type for fit objects", name);
	return(SH_GENERIC_ERROR);
}
if (strcmp(target_pname, pname) != 0) {
	thError("%s: ERROR - expected a chain of type '%s' but received '%s'", name, target_pname, pname);
	return(SH_GENERIC_ERROR);
}

char *target_pname2 = "CALIB_PHOBJC_IO";
TYPE ptype2 = shTypeGetFromName(target_pname2);
if (sdssobjc2 != NULL) ptype2 = shChainTypeGet(sdssobjc2);
char *pname2 = shNameGetFromType(ptype2);
if (ptype2 == UNKNOWN_SCHEMA || pname2 == NULL || strlen(pname2) == 0) {
	thError("%s: ERROR - unknown io type for fit objects", name);
	return(SH_GENERIC_ERROR);
}
if (strcmp(target_pname2, pname2) != 0) {
	thError("%s: ERROR - expected a chain of type '%s' but received '%s'", name, target_pname2, pname2);
	return(SH_GENERIC_ERROR);
}


SCHEMA *schema = shSchemaGetFromType(ptype);
void * (*construct)(void);
construct = schema->construct;
size_t copy_size = schema->size;

SCHEMA *schema2 = shSchemaGetFromType(ptype2);
void * (*construct2)(void);
construct2 = schema2->construct;
size_t copy_size2 = schema2->size;

/* determining which one is a fit object */
int i;
for (i = 0; i < nobjc; i++) {
	THOBJC *myobjc = objcs[i];

	/* sdss properties of the object */
	char *phname = myobjc->phname;	
	/* 
	TYPE phtype = myobjc->phtype;
	*/
	void *phprop = myobjc->phprop;
	/* void *p = construct(); */
	/* object type according to soho rules */
	THOBJCTYPE thtype = myobjc->thobjctype;
	THOBJCTYPE SKY_OBJC = UNKNOWN_OBJC;
	MSkyGetType(&SKY_OBJC);
	
	if (thtype != SKY_OBJC) {

	void *p = thMalloc(copy_size);
	void *p2 = thMalloc(copy_size2);

	/* status = thqqTrans(iop, ioname, p, pname); */
	if (phprop == NULL) {
		thError("%s: ERROR - found a null 'phprop' (sdss object) in 'thobjc (%s)'", name, phname);
		return(SH_GENERIC_ERROR);
	}
	WOBJC_IO *phObjc = NULL;
	CALIB_PHOBJC_IO *cphobjc = NULL;
	status =  thPhpropsGet(phprop, &phObjc, NULL, NULL, &cphobjc, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get 'phObjc' from 'phprops'", name);
		return(status);
	}
	if (phObjc == NULL) {
		thError("%s: ERROR - found a null 'phObjc' in 'phprops'", name);
		return(SH_GENERIC_ERROR);
	}
	if (cphobjc == NULL) {
		thError("%s: ERROR - found a null 'cphobjc' in 'phprops'", name);
		return(SH_GENERIC_ERROR);
	}
	memcpy(p, phObjc, copy_size);
	memcpy(p2, cphobjc, copy_size2);
	
	if (sdssobjc != NULL) shChainElementAddByPos(sdssobjc, p, pname, TAIL, AFTER);	
	if (sdssobjc2 != NULL) shChainElementAddByPos(sdssobjc2, p2, pname2, TAIL, AFTER);	
	
	nfitobjc++;
	
	}

}

return(SH_SUCCESS);
}

RET_CODE thFrameGetFIObjc(FRAME *f, CHAIN *fitobjc) {
char *name = "thFrameGetFIObjc";
if (fitobjc == NULL || f == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (shChainSize(fitobjc) != 0) {
	thError("%s: ERROR - expected a chain of length zero", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
LSTRUCT *l = NULL;
status = thFrameGetLstruct(f, &l);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lstruct) from (frame)", name);
	return(status);
}
MAPMACHINE *map = NULL;
status = thLstructGetLmachine(l, &map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (mapmachine) from (lstruct)", name);
	return(status);
}
MCFLAG cflag = ALTERED;
status = thMapmachineGetCflag(map, &cflag);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract compilation flag from (map)", name);
	return(status);
} else if (cflag != COMPILED) {
	thError("%s: ERROR - (map) needs to be compiled", name);
	return(SH_GENERIC_ERROR);
}

/* taking list of objects from map */
int nfitobjc = 0; 
void **objcs = NULL;
int nobjc = -1;

#if DUMP_AMPOBJC
status = thMapmachineGetUObjcs(map, &objcs, &nobjc);
#else
status = thMapmachineGetUPObjcs(map, &objcs, &nobjc);
#endif
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (object list) from (map)", name);
	return(status);
}
if (nobjc == 0) {
	thError("%s: WARNING - object list contains (0) elements", name);
	return(SH_SUCCESS);
}
TYPE ptype = shChainTypeGet(fitobjc);
char *pname = shNameGetFromType(ptype);
if (ptype == UNKNOWN_SCHEMA || pname == NULL || strlen(pname) == 0) {
	thError("%s: ERROR - unknown io type for fit objects", name);
	return(SH_GENERIC_ERROR);
}
SCHEMA *schema = shSchemaGetFromType(ptype);
void * (*construct)(void);
construct = schema->construct;

/* determining which one is a fit object */
int i;
for (i = 0; i < nobjc; i++) {
	THOBJC *myobjc = objcs[i];
	THOBJCTYPE thtype = myobjc->thobjctype;
	THOBJCTYPE SKY_OBJC = UNKNOWN_OBJC;
	MSkyGetType(&SKY_OBJC);	

	if (thtype != SKY_OBJC) {

	char *ioname = myobjc->ioname;	
	/* 
	TYPE iotype = myobjc->iotype;
	*/
	void *iop = myobjc->ioprop;
	void *p = construct();
	if (p == NULL) {
		thError("%s: ERROR - could not construct variable of type '%s'", name, pname);
		return(SH_GENERIC_ERROR);
	}
	status = thqqTrans(iop, ioname, p, pname);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not transform '%s' onto '%s'", name, ioname, pname);
		return(status);
	}
	FITMASK *fitmask = NULL;
	status = thObjcGetFitmask(myobjc, &fitmask);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (fitmask) from (objc)", name);
		return(status);
	}
	status = thFitmaskTrans(fitmask, p, pname);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not translate (fitmask) records into '%s' records", name, pname);
		return(status);
	}
	shChainElementAddByPos(fitobjc, p, pname, TAIL, AFTER);	
	nfitobjc++;
	
	}
}

return(SH_SUCCESS);
}

/* initiators and finilizers */
RET_CODE thFrameInitCcdinfo(FRAME *f, CCDINFO *ccdinfo) {
char *name = "thFrameInitCcdinfo";
thError("%s: ERROR - unsupported function", name);
return(SH_GENERIC_ERROR);
}

RET_CODE thFrameInitRuninfo(FRAME *f, RUNINFO *runinfo) {
char *name = "thFrameInitRuninfo";
thError("%s: ERROR - unsupported function", name);
return(SH_GENERIC_ERROR);
}

RET_CODE thFrameInitAlginfo(FRAME *f, ALGINFO *alginfo) {
char *name = "thFrameInitAlginfo";
thError("%s: ERROR - unsupported function", name);
return(SH_GENERIC_ERROR);
}

RET_CODE thFrameInitFieldinfo(FRAME *f, FIELDINFO *finfo) {
char *name = "thFrameInitFieldinfo";
thError("%s: ERROR - unsupported function", name);
return(SH_GENERIC_ERROR);
}
RET_CODE thFrameInitIoinfo(FRAME *f, IOINFO *ioinfo) {
char *name = "thFrameInitIoinfo";
thError("%s: ERROR - unsupported function", name);
return(SH_GENERIC_ERROR);
}
RET_CODE thFrameFiniRuninfo(FRAME *f, RUNINFO *runinfo) {
char *name = "thFrameFiniRuninfo";
thError("%s: ERROR - unsupported function", name);
return(SH_GENERIC_ERROR);
}

RET_CODE thFrameFiniAlginfo(FRAME *f, ALGINFO *alginfo) {
char *name = "thFrameFiniAlginfo";
thError("%s: ERROR - unsupported function", name);
return(SH_GENERIC_ERROR);
}

RET_CODE thFrameFiniFieldinfo(FRAME *f, FIELDINFO *finfo) {
char *name = "thFrameFiniFieldinfo";
thError("%s: ERROR - unsupported function", name);
return(SH_GENERIC_ERROR);
}

/* new dumpers Nov 13, 2013 */
RET_CODE thDumpLM(LMDUMP *lm) {
char *name = "thDumpLM";
if (lm == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

int doflag = lm->doflag;
char *filename = lm->filename;
if (!doflag) {
	if (filename != NULL && strlen(filename) != 0) {
		thError("%s: WARNING - an output filename discovered while (doflag = 0)", name);
	}
	return(SH_SUCCESS);
}

PHFITSFILE *fd;
HDR *hdr = lm->hdr;
fd = phFitsBinTblOpen(filename, dump_write_flag, hdr);
if (fd == NULL) {
	thError("%s: ERROR - could not open (lmfile) '%s'", name, filename);
	return(SH_GENERIC_ERROR);
}
char *type = "LMETHOD";
if (phFitsBinTblHdrWrite(fd, type, NULL) < 0) {
	thError("%s: ERROR - could not write header information for type '%s'", name, type);
	return(SH_GENERIC_ERROR);
}
if (phFitsBinTblChainWrite(fd, lm->lm) < 0) {
	thError("%s: ERROR - could not write chain of '%s'", name, type);
	return(SH_GENERIC_ERROR);
}
if (shChainSize(lm->lm) == 0) {
	thError("%s: WARNING - empty (lmchain)", name);
}
phFitsBinTblEnd(fd);

if (phFitsBinTblClose(fd) < 0) {
	thError("%s: ERROR - could not close binary table", name);
	return(SH_GENERIC_ERROR);
}
return(SH_SUCCESS);
}

RET_CODE thDumpLP(LPDUMP *lp) {
char *name = "thDumpLP";
if (lp == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

int doflag = lp->doflag;
char *filename = lp->filename;
if (!doflag) {
	if (filename != NULL && strlen(filename) != 0) {
		thError("%s: WARNING - an output filename discovered while (doflag = 0)", name);
	}
	return(SH_SUCCESS);
}

PHFITSFILE *fd;
HDR *hdr = lp->hdr;
fd = phFitsBinTblOpen(filename, dump_write_flag, hdr);
if (fd == NULL) {
	thError("%s: ERROR - could not open (lpfile) '%s'", name, filename);
	return(SH_GENERIC_ERROR);
}

#if (DUMP_PROF_INIT || DUMP_PROF_FIT || DUMP_PROF)
char *type = "LPROFILE"; 
CHAIN *lp_output = NULL;
#endif

#if DUMP_PROF
lp_output = lp->lp;
if (phFitsBinTblHdrWrite(fd, type, NULL) < 0) {
	thError("%s: ERROR - could not write header information for type '%s'", name, type);
	return(SH_GENERIC_ERROR);
}
if (phFitsBinTblChainWrite(fd, lp_output) < 0) {
	thError("%s: ERROR - could not write chain of '%s'", name, type);
	return(SH_GENERIC_ERROR);
}
if (shChainSize(lp_output) == 0) {
	thError("%s: WARNING - empty (lpchain)", name);
}
phFitsBinTblEnd(fd);
#endif

#if DUMP_PROF_INIT
lp_output = lp->lp_init;
if (phFitsBinTblHdrWrite(fd, type, NULL) < 0) {
	thError("%s: ERROR - could not write header information for type '%s'", name, type);
	return(SH_GENERIC_ERROR);
}
if (phFitsBinTblChainWrite(fd, lp_output) < 0) {
	thError("%s: ERROR - could not write chain of '%s'", name, type);
	return(SH_GENERIC_ERROR);
}
if (shChainSize(lp->lp_init) == 0) {
	thError("%s: WARNING - empty (lpchain)", name);
}
phFitsBinTblEnd(fd);
#endif

#if DUMP_PROF_FIT
lp_output = lp->lp_fit;
if (phFitsBinTblHdrWrite(fd, type, NULL) < 0) {
	thError("%s: ERROR - could not write header information for type '%s'", name, type);
	return(SH_GENERIC_ERROR);
}
if (phFitsBinTblChainWrite(fd, lp_output) < 0) {
	thError("%s: ERROR - could not write chain of '%s'", name, type);
	return(SH_GENERIC_ERROR);
}
if (shChainSize(lp->lp_init) == 0) {
	thError("%s: WARNING - empty (lpchain)", name);
}
phFitsBinTblEnd(fd);
#endif

if (phFitsBinTblClose(fd) < 0) {
	thError("%s: ERROR - could not close binary table", name);
	return(SH_GENERIC_ERROR);
}
return(SH_SUCCESS);
}

RET_CODE thDumpLRZ(LRZDUMP *lrz) {
char *name = "thDumpLRZ";
if (lrz == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

int doflag = lrz->doflag;
char *filename = lrz->rfilename;
REGION *image = lrz->r_image;
HDR *hdr = lrz->hdr;
if (hdr == NULL) {
	thError("%s: ERROR - null header found - improperly initiated (lrz)", name);
	return(SH_GENERIC_ERROR);
}

int nrow = 0, ncol = 0;
if (image != NULL) {
	nrow = image->nrow;
	ncol = image->ncol;
}
if (!doflag) {
	if (filename != NULL && strlen(filename) != 0) {
		thError("%s: WARNING - an output r-filename discovered while (doflag = 0)", name);
	}
} else if (image == NULL) {
	thError("%s: ERROR - null (r-image) found while (doflag == %d)", name, doflag);
	return(SH_GENERIC_ERROR);
} else if (nrow <= 0 || ncol <= 0) {
	thError("%s: ERROR - found (nrow = %d, ncol = %d) in (r-image) while (doflag = %d)", name, nrow, ncol, doflag);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
if (doflag) {
	if (hdr != NULL) shHdrCopy(hdr, &(image->hdr));
	status = shRegWriteAsFits(image, filename, STANDARD, 2, DEF_NONE, NULL, 0);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not write (r-image) onto (file = '%s')", name, filename);
		return(status);
	}
}

doflag = lrz->doflag;
filename = lrz->zfilename;
image = lrz->z_image;
nrow = 0;
ncol = 0;
if (image != NULL) {
	nrow = image->nrow;
	ncol = image->ncol;
}
if (!doflag) {
	if (filename != NULL && strlen(filename) != 0) {
		thError("%s: WARNING - an output z-filename discovered while (doflag = 0)", name);
	}
} else if (image == NULL) {
	thError("%s: ERROR - null (z-image) found while (doflag == %d)", name, doflag);
	return(SH_GENERIC_ERROR);
} else if (nrow <= 0 || ncol <= 0) {
	thError("%s: ERROR - found (nrow = %d, ncol = %d) in (z-image) while (doflag = %d)", name, nrow, ncol, doflag);
	return(SH_GENERIC_ERROR);
}


if (doflag) {
	if (hdr != NULL) shHdrCopy(hdr, &(image->hdr));
	status = shRegWriteAsFits(image, filename, STANDARD, 2, DEF_NONE, NULL, 0);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not write (z-image) onto (file = '%s')", name, filename);
		return(status);
	}
}

doflag = lrz->doflag;
filename = lrz->wfilename;
image = lrz->w_image;
nrow = 0;
ncol = 0;
if (image != NULL) {
	nrow = image->nrow;
	ncol = image->ncol;
}
if (!doflag) {
	if (filename != NULL && strlen(filename) != 0) {
		thError("%s: WARNING - an output w-filename discovered while (doflag = 0)", name);
	}
} else if (image == NULL) {
	thError("%s: ERROR - null (w-image) found while (doflag == %d)", name, doflag);
	return(SH_GENERIC_ERROR);
} else if (nrow <= 0 || ncol <= 0) {
	thError("%s: ERROR - found (nrow = %d, ncol = %d) in (w-image) while (doflag = %d)", name, nrow, ncol, doflag);
	return(SH_GENERIC_ERROR);
}


if (doflag) {
	if (hdr != NULL) shHdrCopy(hdr, &(image->hdr));
	status = shRegWriteAsFits(image, filename, STANDARD, 2, DEF_NONE, NULL, 0);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not write (w-image) onto (file = '%s')", name, filename);
		return(status);
	}
}



return(SH_SUCCESS);
}



RET_CODE thDumpPsf(PSFDUMP *dump) {
char *name = "thDumpPsf";
if (dump == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
char *reportfile = NULL, *statfile = NULL;
status = thPsfDumpGetFilename(dump, &reportfile, &statfile);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get filenames from dump", name);
	return(status);
}
CHAIN  *reportchain = NULL, *statchain = NULL;
status = thPsfDumpGetData(dump, &reportchain, &statchain);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get data chains from dump", name);
	return(status);
}
HDR *hdr = NULL;
status = thPsfDumpGetHdr(dump, &hdr);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the header from dump", name);
	return(status);
}
if (hdr == NULL) {
	thError("%s: WARNING - null (hdr) discovered in (dump)", name);
}

#if DUMP_PSF_REPORT
int n;
PHFITSFILE *fd;
char *type;
n = shChainSize(reportchain);
#if VERBOSE_DUMP
printf("%s: writing (report chain: %d) to '%s' \n", name, n, reportfile);
#endif
fd = phFitsBinTblOpen(reportfile, dump_write_flag, hdr);
if (fd == NULL) {
	thError("%s: ERROR - could not open (reportfile), '%s'", name, reportfile);
	return(SH_GENERIC_ERROR);
}
type = "PSFREPORT";
if (shTypeGetFromName(type) == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - type '%s' not supported by a schema", name, type);
	return(SH_GENERIC_ERROR);
}
if (phFitsBinTblHdrWrite(fd, type, NULL) < 0) {
	thError("%s: ERROR - could not write header information for type '%s' to file '%s'", name, type, reportfile);
	return(SH_GENERIC_ERROR);
}
if (reportchain == NULL) {
	thError("%s: WARNING - null (report chain) found in (psf dump)", name);
} else {
	if (shChainSize(reportchain) != n) {
		thError("%s: ERROR - memory issue: size of (report chain) has changed from (%d) to (%d)", name, n, shChainSize(reportchain));
		return(SH_GENERIC_ERROR);
	}
	if (phFitsBinTblChainWrite(fd, reportchain) < 0) {
		thError("%s: ERROR - could not write chain of '%s' to '%s'", name, type, reportfile);
		return(SH_GENERIC_ERROR);
	}
}
if (shChainSize(reportchain) == 0) {
	thError("%s: WARNING - empty (psf report)", name);
}
phFitsBinTblEnd(fd);
#endif

#if DUMP_PSF_STAT
n = shChainSize(statchain);
#if VERBOSE_DUMP
printf("%s: writing (stat chain: %d) to '%s' \n", name, n, statfile);
#endif
fd = phFitsBinTblOpen(statfile, dump_write_flag, hdr);
if (fd == NULL) {
	thError("%s: ERROR - could not open (statfile), '%s'", name, statfile);
	return(SH_GENERIC_ERROR);
}
type = "BRIEFPSFREPORT";
if (shTypeGetFromName(type) == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - type '%s' not supported by a schema", name, type);
	return(SH_GENERIC_ERROR);
}
if (phFitsBinTblHdrWrite(fd, type, NULL) < 0) {
	thError("%s: ERROR - could not write header information for type '%s' to '%s'", name, type, statfile);
	return(SH_GENERIC_ERROR);
}
if (statchain == NULL) {
	thError("%s: WARNING - null (stat chain) found in (psf dump)", name);
} else {
	if (shChainSize(statchain) != n) {
		thError("%s: ERROR - memory issue: size of (stat chain) has changed from (%d) to (%d)", name, n, shChainSize(statchain));
		return(SH_GENERIC_ERROR);
	}
	if (phFitsBinTblChainWrite(fd, statchain) < 0) {
		thError("%s: ERROR - could not write chain of '%s' to '%s'", name, type, statfile);
		return(SH_GENERIC_ERROR);
	}
}
if (shChainSize(statchain) == 0) {
	thError("%s: WARNING - empty (psf report)", name);
}
phFitsBinTblEnd(fd);
#endif

return(SH_SUCCESS);
}

int compare_frame_io(const void *v1, const void *v2) {
char *name = "compare_frame_io";

FRAMEIO *io1 = (FRAMEIO *) v1;
FRAMEIO *io2 = (FRAMEIO *) v2;

RET_CODE status;
int bad_value = COMPARE_FRAMEIO_BAD_VALUE;

if (io1 != NULL && io2 != NULL) {
	CHISQFL chisq1, chisq2;
	LMDUMP *lmdump1 = NULL, *lmdump2 = NULL;
	status = thFrameioGetLMDump(io1, &lmdump1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (lmdump) from (io1)", name);
		return(bad_value);
	}
	status = thFrameioGetLMDump(io2, &lmdump2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (lmdump) from (io2)", name);
		return(-bad_value);
	}
	status = thLMDumpGetMinChisq(lmdump1, &chisq1, NULL, NULL, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get minimum (chisq) value from (lmdump1)", name);
		return(bad_value);
	}
	status = thLMDumpGetMinChisq(lmdump2, &chisq2, NULL, NULL, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get minimum (chisq) value from (lmdump2)", name);
		return(-bad_value);
	}
	if (chisq1 != CHISQ_IS_BAD && chisq2 != CHISQ_IS_BAD) {
		CHISQFL diff = chisq1 - chisq2;
		int int_diff = (int) (diff * (CHISQFL) CHISQ_DIFF_SCALE);
		return(int_diff);
	} else if (chisq1 == CHISQ_IS_BAD && chisq2 == CHISQ_IS_BAD) {
		return((int) 0);
	} else if (chisq1 == CHISQ_IS_BAD) {
		CHISQFL diff = 2.0 * fabs(chisq2) + 1.0;
		int int_diff = (int) (diff * (CHISQFL) CHISQ_DIFF_SCALE);
                return(int_diff);
	} else if (chisq2 == CHISQ_IS_BAD) {
		CHISQFL diff = - 2.0 * fabs(chisq1) - 1.0;
		int int_diff = (int) (diff * (CHISQFL) CHISQ_DIFF_SCALE);
                return(int_diff);
	} else {
		thError("%s: ERROR - binary error", name);
		return(-bad_value);
	}
} else if (io1 == NULL) {
	thError("%s: ERROR - null (io1) passed", name);
	return(bad_value);
} else if (io2 == NULL) {
	thError("%s: ERROR - null (io2) passed", name);
	return(-bad_value);
} else {
	thError("%s: ERROR - binary problem", name);
	return(bad_value);
}

thError("%s: ERROR - binary problem", name);
return(bad_value);
}

RET_CODE thLMDumpGetMinChisq(LMDUMP *lmdump, CHISQFL *chisq, int *nstep, int *nstep_all, CHISQFL *pcost) {
char *name = "thLMDumpGetMinChisq";
if (lmdump == NULL) {
	thError("%s: ERROR - null (lmdump) passed", name);
	return(SH_GENERIC_ERROR);
}
if (chisq == NULL || nstep == NULL || nstep_all == NULL || pcost == NULL) {
	thError("%s: ERROR - null output place holder", name);
	return(SH_GENERIC_ERROR);
}
CHAIN *lm_chain = lmdump->lm;
if (lm_chain == NULL) { 
	thError("%s: ERROR - null (lm_chain) found in (lmdump)", name);
	*chisq = CHISQ_IS_BAD;
	*nstep = (int) VALUE_IS_BAD;
	*nstep_all = (int) VALUE_IS_BAD;
	*pcost = 0.0;
	return(SH_GENERIC_ERROR);
} else if (shChainSize(lm_chain) == 0) {
	thError("%s: WARNING - empty (lm_chain) found in (lmdump)", name);
	*chisq = CHISQ_IS_BAD;
	*nstep = (int) VALUE_IS_BAD;
	*nstep_all = (int) VALUE_IS_BAD;
	*pcost = 0.0;
	return(SH_SUCCESS);
}
RET_CODE status;
int found = -1, i, count = 0, n = shChainSize(lm_chain);
CHISQFL cost_value = -1.0, cost_value_min = -1.0, chisq_value = -1.0, chisq_value_min = -1.0;
int nstep_min = -1, nstep_total = -1;
for (i = 0; i < n; i++) {
	LMETHOD *lm = shChainElementGetByPos(lm_chain, i);
	if (lm != NULL) {
		status = thLmethodGetChisq(lm, &chisq_value, &cost_value);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (chisq) from (lmethod, i = %d)", name, i);
			return(status);
		}
		if (cost_value > (CHISQFL) 0.0) {
			if (count == 0 || cost_value < cost_value_min) {
				cost_value_min = cost_value;
				chisq_value_min = chisq_value;
				nstep_min = lm->k;
				nstep_total = nstep_min;
				found = i;
			 } else if (cost_value == cost_value_min && chisq_value < chisq_value_min) {
				cost_value_min = cost_value;
				chisq_value_min = chisq_value;
				nstep_min = lm->k;
				nstep_total = nstep_min;
				found = i;
			}
			count++;
		}
		if ((found >= 0) && (lm->k >= nstep_min)) {
			nstep_total = lm->k;
		}
	}
}
if (count != n) {
	thError("%s: WARNING - (count = %d) (lmethod, cost > 0) found in (lmdump) but chain (n = %d)", name, count, n);
}

#if DEBUG_DUMP_LM
printf("%s: (lmdump) has a (chisq = %20.10G) while (size = %d, found_index = %d), (nstep_min = %d, nstep_total = %d) \n", name, (double) chisq_value, n, found, nstep_min, nstep_total);
#endif

if (chisq != NULL) *chisq = chisq_value_min;
if (pcost != NULL) *pcost = cost_value_min - chisq_value_min;
if (nstep != NULL) *nstep = nstep_min;
if (nstep_all != NULL) *nstep_all = nstep_total;
return(SH_SUCCESS);
}

RET_CODE print_chisq_stats_for_various_run(CHAIN *io_chain) {
char *name = "print_chisq_stats_for_various_run";
if (io_chain == NULL || shChainSize(io_chain) == 0) {
		thError("%s: ERROR - null or empty input (io_chain)", name);
		return(SH_GENERIC_ERROR);
}

RET_CODE status;
int count = 0, i, n = shChainSize(io_chain);
CHISQFL chisq_array[n];
float chisq_array_float[n], da[n];
for (i = 0; i < n; i++) {
	FRAMEIO *io = shChainElementGetByPos(io_chain, i);
	if (io != NULL) {	
		LMDUMP *lmdump = NULL;
		status = thFrameioGetLMDump(io, &lmdump);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (lmdump) from (frameio)", name);
			return(status);
		}
		if (lmdump != NULL) {
			CHISQFL chisq = 0.0;
			int nstep = -1, nstep_all = -1;
			CHISQFL pcost = -1.0;
			status = thLMDumpGetMinChisq(lmdump, &chisq, &nstep, &nstep_all, &pcost);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get min (chisq) from (lmdump)", name);
				return(status);
			}
			if (chisq != CHISQ_IS_BAD) {
				chisq_array[count] = chisq;
				chisq_array_float[count] = (float) (chisq / (CHISQFL) (2048 * 1489));
				da[count] = 1.0E-9;
				count++;
			}
		}
	}
}

printf("%s: found (%d) (frameio) pieces with (chisq) info \n", name, count);
if (count == 0) {
	thError("%s: WARNING - no valid (chisq) info found in (io_chain)", name);
	return(SH_SUCCESS);
}
printf("%s: chisq = ", name);
for (i = 0; i < count; i++) {
	if (i % 5 == 0) printf("\n");
	printf("%20.10G ", (double) chisq_array[i]);
	if (i < n-1) printf(","); 
}
printf("\n");


printf("%s: reduced chisq = ", name);
for (i = 0; i < count; i++) {
	if (i % 5 == 0) printf("\n");
	printf("%20.10G ", (double) (chisq_array[i] / (double) (2048.00 * 1489.00)));
	if (i < n-1) printf(","); 
}
printf("\n");

BASIC_STAT *stat = thBasicStatNew();
status = thBasicStatFromArray(chisq_array_float, da, stat, count);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not produce statistics from (reduced chisq array)", name);
	return(status);
}
printf("%s: reduced chi-squared: mean = %20.10G, median = %20.10G, Q1 = %20.10G, Q3 = %20.10G \n", name, (double) stat->mean, (double) stat->median, (double) stat->q1, (double) stat->q3);
printf("%s: reduced chi-squared: IQR = %20.10G, STDEV = %20.10G \n", name, (double) stat->iqr, (double) stat->s);
/* 
printf("%s: reduced chi-squared: min = %20.10G, max = %20.10G \n", name, (double) stat->min, (double) stat->max);
*/

thBasicStatDel(stat);

return(SH_SUCCESS);
}

RET_CODE thFrameOrderIo(FRAME *f) {
char *name = "thFrameOrderIo";
if (f == NULL) {
	thError("%s: ERROR - null (frame)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
CHAIN *io_chain = NULL;
status = thFrameGetFrameioChain(f, &io_chain);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (io_chain) from (frame)", name);
	return(status);
}

status = print_chisq_stats_for_various_run(io_chain);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not print (chisq) stats from (io_chain)", name);
	return(status);
}


/* 
shChainQsort(io_chain, &compare_frame_io);
*/

FRAMEIO *io = NULL;
int n = shChainSize(io_chain);
if (n == 0) {
	thError("%s: WARNING - empty chain cannot be ordered", name);
	status = thFrameGetFrameio(f, &io);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - cannot get (frameio) from (frame)", name);
		return(status);
	}
	if (io != NULL) {
		thError("%s: ERROR - found non-null (io) in (frame) while (io_chain) is empty", name);
		return(SH_GENERIC_ERROR);
	}
	return(SH_SUCCESS);
} 

int found = -1, i, count = 0, nstep = -1, nstep_all = -1, nstep_min = -1, nstep_min_all = -1;
FRAMEIO *io_min;
CHISQFL chisq_min = CHISQ_IS_BAD, chisq = CHISQ_IS_BAD, pcost = 0.0, pcost_min;
for (i = 0; i < n; i++) {
	io = shChainElementGetByPos(io_chain, i);
	LMDUMP *lmdump = NULL;
	status = thFrameioGetLMDump(io, &lmdump);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (lmdump) from (frameio)", name);
		return(status);
	}
	status = thLMDumpGetMinChisq(lmdump, &chisq, &nstep, &nstep_all, &pcost);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (chisq) from (lmdump)", name);
		return(status);
	}
	if (chisq != CHISQ_IS_BAD && chisq > (CHISQFL) 0.0) {
		if (count == 0 || chisq <= chisq_min) {
			found = i;
			chisq_min = chisq;
			nstep_min = nstep;
			nstep_min_all = nstep_all;
			pcost_min = pcost;
			io_min = io;
			count++;
		}
	} 
}

if (chisq_min == CHISQ_IS_BAD) {
	thError("%s: ERROR - minimum (chisq) is bad", name);
	return(SH_GENERIC_ERROR);
} else if (chisq_min < (CHISQFL) 0.0) {
	thError("%s: ERROR - found (chisq_min = %G < 0) while negative values were screened", name, (double) chisq_min);
	return(SH_GENERIC_ERROR);
}

double stdev_theory = 2.0 / sqrt((double) 1489.0 * (double) 2048.0);
printf("%s: min (chisq = %20.10G, reduced chisq = %20.10G, theoretical stdev = %20.10G) (nstep_found = %d, nstep_all = %d, pcost = %20.10G) found in random initiation (%d) \n", name, (double) chisq_min, (double) chisq_min / (double) (2048 * 1489), (double) stdev_theory, nstep_min, nstep_min_all, (double) pcost_min, found);
status = thFramePutFrameio(f, io_min);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update (frameio) at (frame)", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE thFrameRevertObjcDump(FRAME *f) {
char *name = "thFrameRevertObjcDump";
if (f == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
FRAMEIO *fio = NULL;
OBJCDUMP *objc = NULL;
RET_CODE status;
status = thFrameGetFrameio(f, &fio);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (frameio) from (frame)", name);
	return(status);
}
FRAMEFILES *ff = NULL;
status = thFrameGetFramefiles(f, &ff);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (framefiles)", name);
	return(status);
}
status = thFrameioGetObjcDump(fio, &objc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (logdump) from (frameio)", name);
	return(status);
}
if (objc == NULL || ff == NULL || fio == NULL) {
	thError("%s: ERROR - imperoperly allocated frame", name);
	return(SH_GENERIC_ERROR);
}
#if DUMP_FITOBJC
status = thFrameRevertFIObjc(f, objc->fitobjc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not revert (fitobjc) onto (frame)", name);
	return(status);
}
#endif
#if DUMP_PHOBJC
status = thFrameRevertPhObjc(f, objc->phobjc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not revert (phobjc) onto (frame)", name);
	return(status)
}
#endif
return(SH_SUCCESS);
}

RET_CODE thFrameRevertFIObjc(FRAME *f, CHAIN *fitobjc) {
char *name = "thFrameRevertFIObjc";
if (fitobjc == NULL || f == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
int n_fitobjc = shChainSize(fitobjc);
RET_CODE status;
LSTRUCT *l = NULL;
status = thFrameGetLstruct(f, &l);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lstruct) from (frame)", name);
	return(status);
}
MAPMACHINE *map = NULL;
status = thLstructGetLmachine(l, &map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (mapmachine) from (lstruct)", name);
	return(status);
}
MCFLAG cflag = ALTERED;
status = thMapmachineGetCflag(map, &cflag);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract compilation flag from (map)", name);
	return(status);
} else if (cflag != COMPILED) {
	thError("%s: ERROR - (map) needs to be compiled", name);
	return(SH_GENERIC_ERROR);
}

/* taking list of objects from map */
void **objcs = NULL;
int nobjc = -1;

#if DUMP_AMPOBJC
status = thMapmachineGetUObjcs(map, &objcs, &nobjc);
#else
status = thMapmachineGetUPObjcs(map, &objcs, &nobjc);
#endif
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (object list) from (map)", name);
	return(status);
}
if ((nobjc != n_fitobjc) && (nobjc != n_fitobjc + 1)) {
	thError("%s: ERROR - (nobjc = %d) in (map) deos not match (n_fitobjc = %d) in input", name, nobjc, n_fitobjc);
	return(SH_GENERIC_ERROR);
}
if (nobjc == 0) {
	thError("%s: WARNING - object list contains (0) elements", name);
	return(SH_SUCCESS);
}
TYPE ptype = shChainTypeGet(fitobjc);
char *pname = shNameGetFromType(ptype);
if (ptype == UNKNOWN_SCHEMA || pname == NULL || strlen(pname) == 0) {
	thError("%s: ERROR - unknown io type for fit objects", name);
	return(SH_GENERIC_ERROR);
}
/* determining which one is a fit object */
int i,  i_fitobjc = 0;
for (i = 0; i < nobjc; i++) {
	THOBJC *myobjc = objcs[i];
	THOBJCTYPE thtype = myobjc->thobjctype;
	THOBJCTYPE SKY_OBJC = UNKNOWN_OBJC;
	MSkyGetType(&SKY_OBJC);	

	if (thtype != SKY_OBJC) {

	char *ioname = myobjc->ioname;	
	/* 
	TYPE iotype = myobjc->iotype;
	*/
	void *iop = myobjc->ioprop;
	void *p = shChainElementGetByPos(fitobjc, i_fitobjc);
	if (p == NULL) {
		thError("%s: ERROR - null element found in fitobjc[%d] of type '%s'", name, i, pname);
		return(SH_GENERIC_ERROR);
	}
	status = thqqTrans(p, pname, iop, ioname);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not transform '%s' onto '%s'", name, ioname, pname);
		return(status);
	}
	i_fitobjc++;
	
	}
}

if (i_fitobjc != n_fitobjc) {
	thError("%s: ERROR - only (%d) objects passed in (fitobjc) chain (n_fitobjc = %d) could be matched", name, i_fitobjc, n_fitobjc);
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}


RET_CODE thFrameAddLprofileLastOfBest(FRAME *f) {
char *name = "thFrameAddLprofileLastOfBest";

RET_CODE status;
LSTRUCT *l = NULL;
status = thFrameGetLstruct(f, &l);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lstruct) from (frame)", name);
	return(status);
}

LFIT *lf = NULL;
status = thLstructGetLfit(l, &lf);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lfit) from (lstruct)", name);
	return(status);
}
LFITRECORDTYPE rtype_old = NULL_LFITRECORD, rtype_new = NULL_LFITRECORD;
status = thLfitGetRtype(lf, &rtype_old);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (rtype) from (lfit)", name);
	return(status);
}
if (rtype_old & LP_RECORD_LASTOFBEST) {
	if (rtype_old & LP_RECORD_NOW) {
		thError("%s: ERROR - (rtype) found with 'LPRECORD_NOW' manually set, cannot proceed", name);
		return(SH_GENERIC_ERROR);
	}
	rtype_new = LP_RECORD_LASTOFBEST | LP_RECORD_NOW;
	status = thLfitPutRtype(lf, rtype_new);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - failed to update (rtype) temporarily", name);
		return(status);
	}
	status = thFrameRevertObjcDump(f); 
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not reserve update (l) object-io parameters from (frameio)", name);
		return(status);
	}
	status = thLDumpPar(l, IOTOMODEL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump par 'IOTOMODEL'", name);
		return(status);
	}
	status = thLDumpAmp(l, IOTOMODEL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump amp 'IOTOMODEL'", name);
		return(status);
	}
	status = thLDumpPar(l, IOTOX);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump par 'IOTOX'", name);
		return(status);
	}
	status = thLDumpAmp(l, IOTOX);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump amp 'IOTOX'", name);
		return(status);
	}
	status = thLDumpPar(l, IOTOXN);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump par 'IOTOXN'", name);
		return(status);
	}
	status = thLDumpAmp(l, IOTOX);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump amp 'IOTOXN'", name);
		return(status);
	}
	XUPDATE fake_update = UNKNOWN_XUPDATE;
	status = thLfitAddLprofileFromLstruct(lf, l, fake_update);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add (lprofile) to (lstruct)", name);
		return(status);
	}
	status = thFrameFiniLPDump(f);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not finalize (lpdump) after creating (lprofile) for the best run", name);
		return(status);
	}
	status = thLfitPutRtype(lf, rtype_old);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - failed to revert (rtype) to its old value", name);
		return(status);
	}
}

return(SH_SUCCESS);
}

RET_CODE ZscoreMakeFromResidual(REGION *res, LWMASK *lwmask, REGION *z_image, THPIX badpixel) {
char *name = "ZscoreMakeFromResidual";
if (res == NULL) {
	thError("%s: ERROR - null (r-image) passed", name);
	return(SH_GENERIC_ERROR);
}
if (lwmask == NULL) {
	thError("%s: WARNING - null (lwmask) passed", name);
}
if (z_image == NULL) {
	thError("%s: ERROR - null output (z-image) passed", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
REGION *W = NULL;
status = thLwmaskGetW(lwmask, &W);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (W-image) from (lwmask)", name);
	return(status);
}

if (W == NULL) {
	thError("%s: ERROR - null (W-image) found - improperly allocated (lwmask)", name);
	return(SH_GENERIC_ERROR);
}

if (res->type != TYPE_THPIX) {
	thError("%s: ERROR - expected (pixel type = thpix) for (r-image)", name);
	return(SH_GENERIC_ERROR);
} else if (W->type != TYPE_THPIX) {
	thError("%s: ERROR - expected (pixel type = thpix) for (W)", name);
	return(SH_GENERIC_ERROR);
} else if (z_image->type != TYPE_THPIX) {
	thError("%s: ERROR - expected (pixel type = thpix) for (z-image)", name);
	return(SH_GENERIC_ERROR);
}

int nrow = res->nrow;
int ncol = res->ncol;
int wnrow = W->nrow;
int wncol = W->ncol;
if (wnrow != nrow || wncol != ncol) {
	thError("%s: ERROR - size mistamcth (W: nrow = %d, ncol = %d) found while (r-image: nrow = %d, ncol = %d)", name, wnrow, wncol, nrow, ncol);
	return(SH_GENERIC_ERROR);
}
if (nrow != z_image->nrow || ncol != z_image->ncol) {
	status = shRegReshape(z_image, nrow, ncol);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not reshape (z-image) from (r-image)", name);
		return(status);
	}
}
shRegClear(z_image);
int i, j;
for (i = 0; i < nrow; i++) {
	THPIX *W_i = W->rows_thpix[i];
	THPIX *res_i = res->rows_thpix[i];
	THPIX *z_i = z_image->rows_thpix[i];
	for (j = 0; j < ncol; j++) {
		z_i[j] = res_i[j] * sqrt(W_i[j]);
	}
}

MASKBIT maskbit = 0x0;
status = thLwmaskGetMaskbit(lwmask, &maskbit);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (maskbit) from (lwmask)", name);
	return(status);
}
THMASK *mask = NULL;
status = thLwmaskGetMask(lwmask, &mask);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (mask) from (lwmask)", name);
	return(status);
}
MASKBIT **mask_rows = NULL;
if (mask != NULL) {
	int mnrow, mncol;
	mnrow = mask->nrow;
	mncol = mask->ncol;
	if (mnrow != nrow || mncol != ncol) {
		thError("%s: ERROR - size mismatch (mask: nrow = %d, ncol = %d) while (r-image: nrow = %d, ncol = %d)", name, wnrow, wncol, nrow, ncol);
		return(SH_GENERIC_ERROR);
	}
	#if (THMASKisMASK | THMASKisSUPERMASK)
	mask_rows = mask->rows_mask;
	#endif
	if (mask_rows == NULL) {
		thError("%s: WARNING - null (mask rows) found in (mask)", name);
	} else {
		int n_nann = 0;
		for (i = 0; i < nrow; i++) {
			MASKBIT *mask_i = mask_rows[i];
			THPIX *z_i = z_image->rows_thpix[i];
			for (j = 0; j < ncol; j++) {
				if (!(mask_i[j] & maskbit)) {
					z_i[j] = badpixel;
				} else if (isnan(z_i[j]) || isinf(z_i[j])) {
					n_nann++;
				}
			}
		}
		if (n_nann != 0) {
			thError("%s: WARNING - found (%d) instances of NAN / INF in acceptable parts of (z-image)", name, n_nann);
		}
	}
} else {
	thError("%s: WARNING - null mask found in (lwmask) - acceptable during simulations only", name);
}	
return(SH_SUCCESS);
}

RET_CODE thFrameUpdateFitmask(FRAME *f) {
char *name = "thFrameUpdateFitmask";
if (f == NULL) {
	thError("%s: ERROR - null (frame) placeholder", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
LSTRUCT *l = NULL;
status = thFrameGetLstruct(f, &l);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lstruct) from (frame)", name);
	return(status);
}
MAPMACHINE *map = NULL;
status = thLstructGetLmachine(l, &map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (mapmachine) from (lstruct)", name);
	return(status);
}
MCFLAG cflag = ALTERED;
status = thMapmachineGetCflag(map, &cflag);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract compilation flag from (map)", name);
	return(status);
} else if (cflag != COMPILED) {
	thError("%s: ERROR - (map) needs to be compiled", name);
	return(SH_GENERIC_ERROR);
}

/* taking list of objects from map */
void **objcs = NULL, **objcs_list = NULL;
int nobjc = -1, nobjc_list = -1;

status = thMapmachineGetUObjcs(map, &objcs_list, &nobjc_list);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (objc_list) from (map)", name);
	return(status);
}

#if DUMP_AMPOBJC
status = thMapmachineGetUObjcs(map, &objcs, &nobjc);
#else
status = thMapmachineGetUPObjcs(map, &objcs, &nobjc);
#endif
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (object list) from (map)", name);
	return(status);
}
if (nobjc == 0) {
	thError("%s: WARNING - object list contains (0) elements", name);
	return(SH_SUCCESS);
}
OBJMASK *goodmask = NULL;
SPANMASK *thsm = NULL;
FRAMEDATA *fd = NULL;

status = thFrameGetFramedata(f, &fd);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (framedata) from (frame)", name);
	return(status);
}
status = thFramedataGetThSpanmask(fd, &thsm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (th-spanmask) from (frame)", name);
	return(status);
}
if (thsm == NULL) {
	thError("%s: ERROR - null (th-spanmask)", name);
	return(SH_GENERIC_ERROR);
}
CHAIN *objmask_chain = thsm->masks[TH_MASK_GOOD];
if (objmask_chain == NULL || shChainSize(objmask_chain) == 0) {
	thError("%s: ERROR - null or empty (objmask_chain) recovered from (th-spanmask)", name);
	return(SH_GENERIC_ERROR);
} else if (shChainSize(objmask_chain) > 1) {
	thError("%s: ERROR - discovered multiple (goodmask) in (spanmask)", name);
	return(SH_GENERIC_ERROR);
}

goodmask = shChainElementGetByPos(objmask_chain, 0);
if (goodmask == NULL) {
	thError("%s: ERROR - null goodmask discovered in (th-spanmask)", name);
	return(SH_GENERIC_ERROR);
}

FRAMEIO *fio = NULL;
status = thFrameGetFrameio(f, &fio);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (frameio) from (frame)", name);
	return(status);
}

LRZDUMP *lrzdump = NULL;
status = thFrameioGetLRZDump(fio, &lrzdump);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lrzdump) from (frame)", name);
	return(status);
}
REGION *r_image = NULL, *z_image = NULL;
status = thLRZDumpGetRZWImage(lrzdump, &r_image, &z_image, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (r_image) and (z_image) from (lrzdump)", name);
	return(status);
}
int i;
for (i = 0; i < nobjc; i++) {
	THOBJC *objc = objcs[i];
	status = thObjcMakeFitmask(objc, goodmask, (THOBJC **) objcs_list, nobjc_list);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not make (fitmask) for (objc) [%d]", name, i);
		return(status);
	}
	status = thObjcUpdateFitMask(objc, r_image, z_image);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update (fitmask) for (objc) [%d]", name, i);
		return(status);
	}
}

return(SH_SUCCESS);
}




