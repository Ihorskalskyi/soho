#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "strings.h"
#include "dervish.h"

#include "thCalibTypes.h"
#include "phPhotoFitsIO.h"
#include "sohoEnv.h"

RET_CODE thIoFpcDebug(char *name, HDR *header, REGION *reg);
RET_CODE thPrintHdr(HDR *header);
RET_CODE thPrintReg(REGION *reg);
RET_CODE thErrShowStack();

/* **************************************************** */
RET_CODE thIoFpcDebug(char *name, 
		      HDR *header, REGION *reg)
{ RET_CODE thStatus;

  printf("**** %s **** \n", name);
  thStatus = thPrintHdr(header);
  thStatus = thPrintReg(reg);
  
  return(thStatus);
}
    

RET_CODE thPrintHdr(HDR *header)
{
  int LineNo;
  RET_CODE thStatus;
  char *Line;

  if (header == NULL) {
    printf("HEADER Pointer is NULL \n");
    return(SH_SUCCESS);
  } else {
    printf("HEADER = %d \n", (int) sizeof(*header));
    
    thStatus = SH_SUCCESS;
    LineNo = 1;
    while (thStatus == SH_SUCCESS) {
      thStatus = shHdrGetLineCont (header, &LineNo, Line);
      if (thStatus == SH_SUCCESS) {
	printf("%d: %s \n", LineNo, Line);
	LineNo++;
      } else {
	LineNo--;
      }
      if (thStatus == SH_HEADER_IS_NULL) {
	printf("%d: Header is Null \n", LineNo);
      }
    }
    printf("Number of Lines in the header: %d \n", LineNo);
    return(thStatus);
  }
}
  
RET_CODE thPrintReg(REGION *reg)
{
  RET_CODE thStatus;

    // printing the header information
  // Program received signal SIGSEGV, Segmentation fault. //

  if (reg == NULL) {
    printf("REGION pointer is NULL \n");
    return(SH_SUCCESS);
  } else {
    printf("REGION = %d \n", (int) sizeof(*reg));
    thStatus = SH_SUCCESS;
    if (reg != NULL) {
      printf("REG: (NROW, NCOLUMN), NTYPE: (%d, %d), %d \n", reg->nrow, reg->ncol, reg->type);
      thPrintHdr(&reg->hdr);
      return(SH_SUCCESS);
    } else {
      printf("REG: Null \n");
      return(SH_GENERIC_ERROR);
    }}
  
}
  
RET_CODE thErrShowStack() {
  
  char *name = "thErrShowStack";
  char *errormessage;
  int i = 0;
 
  printf("%s: showing the errors stored in stack during last run \n", name);
  errormessage = shErrStackGetEarliest();

  while (errormessage != NULL) {
    printf("%3d : %s ", i, errormessage);
    errormessage = shErrStackGetNext();
    i++;
  }
  printf("%s: end of error stack \n", name);

}
   

RET_CODE thDebugWriteCalibPhObjcIo(char *filename, HDR *hdr) {
char *name = "thDebugWriteCalibPhObjcIo";
shAssert(filename != NULL);
shAssert(strlen(filename) != 0); 

printf("%s: writing debug Calib-PhObjc-IO onto '%s' \n", name, filename);

if (hdr == NULL) {
	shError("%s: WARNING - null (hdr) passed", name);
}

PHFITSFILE *fd;
fd = phFitsBinTblOpen(filename, 1, hdr);
if (fd == NULL) {
	shError("%s: ERROR - could not open (debug dump file), '%s'", name, filename);
	return(SH_GENERIC_ERROR);
}
char *type = "CALIB_PHOBJC_IO";
if (phFitsBinTblHdrWrite(fd, type, NULL) < 0) {
	shError("%s: ERROR - could not write header information for type '%s'", name, type);
	return(SH_GENERIC_ERROR);
}
CHAIN *debugchain = shChainNew(type);
CALIB_PHOBJC_IO *objc = thCalibPhObjcIoNew(NCOLOR);
shChainElementAddByPos(debugchain, objc, type, TAIL, AFTER);

if (phFitsBinTblChainWrite(fd, debugchain) < 0) {
	shError("%s: ERROR - could not write chain of '%s'", name, type);
	return(SH_GENERIC_ERROR);
}
if (shChainSize(debugchain) == 0) {
	shError("%s: WARNING - empty (debug chain)", name);
}
phFitsBinTblEnd(fd);
printf("%s: debug fits file successfully written \n", name);
return(SH_SUCCESS);
}
