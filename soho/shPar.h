#include <stdio.h>
#include <string.h>
#include "dervish.h"
#include "shParTypes.h"


#ifndef MAX
#define MAX(a,b) \
       ({ typeof (a) _a = (a); \
           typeof (b) _b = (b); \
         _a > _b ? _a : _b; })
#endif
#ifndef MIN
#define MIN(a,b) \
       ({ typeof (a) _a = (a); \
           typeof (b) _b = (b); \
         _a < _b ? _a : _b; })
#endif

typedef enum {
  PAR_EOSTRUCT, PAR_EOF, 
  PAR_MOSTRUCT, PAR_HDR,
  PAR_DOSTRUCT, PAR_BOSTRUCT
} PAR_STATUS;



char **read_line(FILE *fil, const int n);
void skip_line(FILE *fil, const int n);
void read_i(FILE *fil, int *var) ;
void read_l(FILE *fil, long *var) ;
void read_ll(FILE *fil, long long  *var) ;
void read_f(FILE *fil, float *var) ;
void read_d(FILE *fil, double *var) ;
void read_ld(FILE *fil, long double *var) ;
void read_s(FILE *fil, char *var) ;
void read_i_heap(FILE *fil, int *var, const int n) ;
void read_l_heap(FILE *fil, long *var, const int n) ;
void read_ll_heap(FILE *fil, long long  *var, const int n) ;
void read_f_heap(FILE *fil, float *var, const int n) ;
void read_d_heap(FILE *fil, double *var, const int n) ;
void read_ld_heap(FILE *fil, long double *var, const int n) ;
void read_s_heap(FILE *fil, char **var, const int n) ;
void read_enum(FILE *fil, int *var, char **enum_str_list, const int n) ;
void skip_var(FILE *fil);
void skip_vars(FILE *fil, const int n);
void read_FRAMEINFO(FILE *fil, FRAMEINFO *var /* output */); 
void read_RUNMARK(FILE *fil, RUNMARK *var /* output */);
void read_IMAGINGRUN(FILE *fil, IMAGINGRUN *var /* output */); 
void read_DFTYPE(FILE *fil, DFTYPE*var /* output */);
void read_CCDBC(FILE *fil, CCDBC*var /* output */); 
void read_CCDGEOMETRY(FILE *fil, CCDGEOMETRY*var /* output */); 
void read_DEWARGEOMETRY(FILE *fil,/* input */ DEWARGEOMETRY* var);
void read_CCDCONFIG(FILE *fil,CCDCONFIG*var /* output */);
void read_LINEARITY_TYPE(FILE *fil, LINEARITY_TYPE*var /* output */); 
void read_ECALIB(FILE *fil, ECALIB*var /* output */);
void read_RUNLIST(FILE *fil, RUNLIST *var /* output */); 
