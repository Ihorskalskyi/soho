(int) shReadFcpTable(const fitsfile *fptr, const long *rows
		     OBJ_IO *Arr, int* status);
{
  /* determine the permissible ROWs to be read, 
     if ROWS in NULL, then READ all the rows in the table */
  /* get a list of all records in the structure as a CHAR array */

  /* --- */
  /* Do the following for every RECORD */ 

  /* determine if the RECORD exists as a ROW in FITS TABLE

  /* if it does */
     /* determine its data type in the FITS binary file */
     /* or determine the data type of the RECORD in the structure */

     /* Read the appropriate column in the FITS Bin Table 
        from the requested Rows*/

     /* Save the column in Arr */

  /* if it doesn't */
     /* save the column as the null value given in NULLSTRUCT */

  /* EndDo */
  /* --- */

/*
 * Here is the internal photo representation of an object in all bands
 */
typedef struct objc {
   const int id;			/* id number for this objc */
   const int ncolor;			/* number of colours */
   OBJECT1 **color;			/* obj1 information from each band */
   OBJ_TYPE type;			/* overall classification */
   float prob_psf;			/* Bayesian probability of being PSF*/
   ATLAS_IMAGE *aimage;			/* atlas image for this object */
   struct test_info *test;		/* information for testers */
   float rowc, rowcErr;			/* row position and error of centre */
   float colc, colcErr;			/* column position and error */
   float rowv, rowvErr;			/* row velocity, in pixels/frame */
   float colv, colvErr;			/* col velocity, in pixels/frame */

   int catID;			        /* catalog id number */
   int flags;				/* c.f. OBJECT1->flags */
   int flags2;				/* c.f. OBJECT1->flags2 */
   int flags3;				/* photo book-keeping; Not Saved */
   PEAKS *peaks;			/* merged peak list */
   int nchild;				/* number of children */
   struct objc *parent;			/* parent if blended */
   struct objc *sibbs;			/* list of siblings */
   struct objc *children;		/* list of children */
} OBJC;					/* pragma SCHEMA */

/*
 * And here is an OBJC reorganised for output, including an Atlas Image
 */
typedef struct {
   int id;				/* id number for this objc */
   int parent;			        /* id of parent for deblends */
   const int ncolor;			/* number of colours */
   int nchild;				/* number of children */
   OBJ_TYPE objc_type;			/* overall classification */
   float objc_prob_psf;			/* Bayesian probability of being PSF */
   int catID;			        /* catalog id number */
   int objc_flags;			/* flags from OBJC */
   int objc_flags2;			/* flags2 from OBJC */
   float objc_rowc, objc_rowcErr;	/* row position and error of centre */
   float objc_colc, objc_colcErr;	/* column position and error */
   float rowv, rowvErr;			/* row velocity, in pixels/frame (NS)*/
   float colv, colvErr;			/* col velocity, in pixels/frame (NS)*/

   ATLAS_IMAGE *aimage;			/* the atlas image */
   struct test_info *test;		/* information for testers */
/*
 * Unpacked OBJECT1s
 */
   float rowc[NCOLOR], rowcErr[NCOLOR];
   float colc[NCOLOR], colcErr[NCOLOR];
   float sky[NCOLOR], skyErr[NCOLOR];
/*
 * PSF and aperture fits/magnitudes
 */
   float psfCounts[NCOLOR], psfCountsErr[NCOLOR];
   float fiberCounts[NCOLOR], fiberCountsErr[NCOLOR];
   float petroCounts[NCOLOR], petroCountsErr[NCOLOR];
   float petroRad[NCOLOR], petroRadErr[NCOLOR];
   float petroR50[NCOLOR], petroR50Err[NCOLOR];
   float petroR90[NCOLOR], petroR90Err[NCOLOR];
/*
 * Shape of object
 */
   float Q[NCOLOR], QErr[NCOLOR], U[NCOLOR], UErr[NCOLOR];

   float M_e1[NCOLOR], M_e2[NCOLOR];
   float M_e1e1Err[NCOLOR], M_e1e2Err[NCOLOR], M_e2e2Err[NCOLOR];
   float M_rr_cc[NCOLOR], M_rr_ccErr[NCOLOR];
   float M_cr4[NCOLOR];
   float M_e1_psf[NCOLOR], M_e2_psf[NCOLOR];
   float M_rr_cc_psf[NCOLOR];
   float M_cr4_psf[NCOLOR];
/*
 * Properties of a certain isophote.
 */
   float iso_rowc[NCOLOR], iso_rowcErr[NCOLOR], iso_rowcGrad[NCOLOR];
   float iso_colc[NCOLOR], iso_colcErr[NCOLOR], iso_colcGrad[NCOLOR];
   float iso_a[NCOLOR], iso_aErr[NCOLOR], iso_aGrad[NCOLOR];
   float iso_b[NCOLOR], iso_bErr[NCOLOR], iso_bGrad[NCOLOR];
   float iso_phi[NCOLOR], iso_phiErr[NCOLOR], iso_phiGrad[NCOLOR];
/*
 * Model parameters for deV and exponential models
 */
   float r_deV[NCOLOR], r_deVErr[NCOLOR];
   float ab_deV[NCOLOR], ab_deVErr[NCOLOR];
   float phi_deV[NCOLOR], phi_deVErr[NCOLOR];
   float counts_deV[NCOLOR], counts_deVErr[NCOLOR];
   float r_exp[NCOLOR], r_expErr[NCOLOR];
   float ab_exp[NCOLOR], ab_expErr[NCOLOR];
   float phi_exp[NCOLOR], phi_expErr[NCOLOR];
   float counts_exp[NCOLOR], counts_expErr[NCOLOR];

   float counts_model[NCOLOR], counts_modelErr[NCOLOR];
/*
 * Measures of image structure
 */
   float texture[NCOLOR];
/*
 * Classification information
 */
   float star_L[NCOLOR], star_lnL[NCOLOR];
   float exp_L[NCOLOR], exp_lnL[NCOLOR];
   float deV_L[NCOLOR], deV_lnL[NCOLOR];
   float fracPSF[NCOLOR];

   int flags[NCOLOR];
   int flags2[NCOLOR];

   OBJ_TYPE type[NCOLOR];
   float prob_psf[NCOLOR];
/*
 * Profile and extent of object
 */
   int nprof[NCOLOR];
   float profMean[NCOLOR][NANN];
   float profErr[NCOLOR][NANN];
} OBJC_IO;				/* pragma SCHEMA */


