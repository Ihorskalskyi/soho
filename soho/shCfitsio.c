#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dervish.h"
#include "phFits.h"
#include "phConsts.h"
#include "fitsio.h"

void*
shCfitsioError(int *status)
{
  char *err_text;

  (void) fits_get_errstatus(*status, err_text);
  if (err_text != '\0') {
    shError(err_text);
    return(status);
  } else {
    return(NULL);
  }
}
