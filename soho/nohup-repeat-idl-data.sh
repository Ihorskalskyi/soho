#!/bin/bash
pcost_report_file="./pcost_report_file.txt"
camcol_array=( 2 4 5 6 )
for i in `seq 1 100`;
do

	#rm -r -f $pcost_report_file
	#for camcol in "${camcol_array[@]}";
	#do
	#	echo "$i : camcol = $camcol"
	#	address="/u/khosrow/thesis/opt/soho/multi-object/images/mle/sdss-g0-s0-cd1/deV-exp-fpC-multiple-init-3-pcost-5/301/*/$camcol/condor/output/*.out"
	#	grep -e 'cD_pcost_generic' $address >> $pcost_report_file
	#	address="/u/khosrow/thesis/opt/soho/multi-object/images/mle/sdss-g0-s0-cd1-extended/deV-exp-fpC-multiple-init-3-pcost-5/301/*/$camcol/condor/output/*.out"
	#	grep -e 'cD_pcost_generic' $address >> $pcost_report_file
	#done
	
	echo "$i: plot attempt begins"
	date "+%Y-%m-%d %H:%M:%S"
	outputfile="./nohup.idl.data.out"
	echo "$i: track plotting progress in $outputfile"
	#idl plot_nohup_sim_photo_match.pro > $outputfile
	idl plot_nohup_data_mle_match.pro > $outputfile
	echo "$i: plot attempt finished"
	date "+%Y-%m-%d %H:%M:%S"
	sleep 30m
done    



