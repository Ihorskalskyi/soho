#include "thParseTypes.h"

SIM_PARSED_INPUT *thSimParsedInputNew(void) {
	SIM_PARSED_INPUT *x = thCalloc(1, sizeof(SIM_PARSED_INPUT));
	return(x);
}

void thSimParsedInputDel(SIM_PARSED_INPUT *x) {
	if (x == NULL) return;
	thSkyparsDel(x->sky);
	if (x->skylist != NULL) {
		int i;
		for (i = 0; i < NCOLOR; i++) thSkyparsDel(x->skylist[i]);
		
	}
	if (x->objclist != NULL) shChainDel(x->objclist);
	thFree(x);
	return;
}

MLE_PARSED_INPUT *thMleParsedInputNew(void) {
	MLE_PARSED_INPUT *x = thCalloc(1, sizeof(MLE_PARSED_INPUT));
	return(x);
}

void thMleParsedInputDel(MLE_PARSED_INPUT *x) {
	if (x == NULL) return;
	if (x->mgalaxy_input != NULL) thMGalaxyParsedInputDel(x->mgalaxy_input);
	thFree(x);
	return;
}

char **str_all_tokens(char *arg, char *delimiter, int *argc) {
	shAssert(arg != NULL);
	shAssert(argc != NULL);
	shAssert(delimiter != NULL);
	if (strlen(arg) == 0) {
		*argc = 0;
		return(NULL);
	}
	int ntoken = 0;
	char **argv = thCalloc(strlen(arg), sizeof(char *));
  	argv[ntoken] = "mymacro";
	ntoken++;
	char *pch = strtok(arg, delimiter);
	while (pch != NULL)	{
		argv[ntoken] = pch;
		ntoken++;
    		pch = strtok (NULL, delimiter); 
 	}
	*argc = ntoken;
	if (ntoken == 0) {
		thFree(argv);
		return(NULL);
	}
	return(argv);
}

void delete_all_tokens(char **argv, int argc){
	shAssert(argc >= 0);
	if (argc == 0) {
		shAssert(argv == NULL);
		return;
	}
	shAssert(argv != NULL);
	thFree(argv);
	return;
}

MGALAXY_PARSED_INPUT *thMGalaxyParsedInputNew() {
	MGALAXY_PARSED_INPUT *x = thCalloc(1, sizeof(MGALAXY_PARSED_INPUT));
return(x);
}

void thMGalaxyParsedInputDel(MGALAXY_PARSED_INPUT *x) {
	if (x == NULL) return;
	thFree(x);
	return;
}

CTRANSFORM_PARSED_INPUT *thCTransformParsedInputNew() {
	CTRANSFORM_PARSED_INPUT *x = thCalloc(1, sizeof(CTRANSFORM_PARSED_INPUT));
	return(x);
}

void thCTransformParsedInputDel(CTRANSFORM_PARSED_INPUT *x) {
	if (x == NULL) return;
	thFree(x);
	return;
}

