#ifndef THSKYTYPES_H
#define THSKYTYPES_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dervish.h"
#include "phPhotoFitsIO.h"
#include "phSpanUtil.h"
#include "phUtils.h"
#include "phObjc.h"
#include "phDataIo.h"
#include "prvt/region_p.h"

#include "thConsts.h"
#include "thMathTypes.h"
#include "thMaskTypes.h"

#ifndef MAX
#define MAX(a,b) \
       ({ typeof (a) _a = (a); \
           typeof (b) _b = (b); \
         _a > _b ? _a : _b; })
#endif
#ifndef MIN
#define MIN(a,b) \
       ({ typeof (a) _a = (a); \
           typeof (b) _b = (b); \
         _a < _b ? _a : _b; })
#endif


/* sky structure handling */

typedef struct Skycoeffs {
  int bsize; /* number of variables */

  /*
    
  THPIX *c;
  THPIX **cov, **invcov;
  
  */
  
  THPIX c[SKYBSIZE]; /* coefficients for the most likely sky */
  THPIX cov[SKYBSIZE][SKYBSIZE], invcov[SKYBSIZE][SKYBSIZE]; 
  /* 
     covariance and inverse covariance 
     of c-variables 
  */

} SKYCOEFFS; /* pragma SCHEMA */

typedef struct Skybasisset {
  int bsize; /* number of basis functions */
  int nrow;
  int ncol;  /*size of the image this basis set thould be used for */
  CHAIN *basis; /* a chain of sky_basis elements of length bsize */
} SKYBASISSET; /* pragma SCHEMA */


typedef enum {
  TYPE_UNKNOWN, TYPE_FUNC, TYPE_AMPL
} TYPE_SKYBASIS; /* pragma SCHEMA */

typedef struct Skybasis {
  TYPE_SKYBASIS type; /* type of the sky_basis, 
			  is it a function or an amplifier region */
  void *val;
} SKYBASIS; /* pragma SCHEMA */


typedef struct Ampl {
  THPIX gain; /* gain of the amplifier */

  int row0, row1;
  int col0, col1; /* coordinates of the two corners 
		     of the amplifier region */
  THPIX readnoise; /* read noise */

  int amplno;

  /* 
  ECONFIG *econfig;
  CCDCONFIG *ccdconfig;
  */
  
} AMPL; /* pragma SCHEMA */


typedef struct Skymodel {
  SKYBASISSET *basis; /* the set of basis function used */
  SKYCOEFFS *sc; /* the coefficients and their covariance */
  REGION *reg; /* the region which includes the accurate realization 
		  of the sky obtained from c-values and the form of 
		  the basis functions 
	       */
  THPIX loglikelihood;
  int npixel;

} SKYMODEL; /* pragma SCHEMA */

/* 

The contents of the third (ext[2]) of the frame model files 
shows the sky meta data used to correct the images

ALLSKY	float32[256,ny]	the binned sky values, in units of counts
XINTERP	float32[2048]	X pixel-values to interpolate "allsky" into to produce a full image of the sky (bilinear interpolation is fine)
YINTERP	float32[1489]	Y pixel-values to interpolate "allsky" into to produce a full image of the sky (bilinear interpolation is fine)

*/

typedef struct thbinregion {
	FL32 allsky[NXNODE_THBINREGION][NYNODE_THBINREGION];
	FL32 xinterp[NX_THBINREGION];
	FL32 yinterp[NY_THBINREGION];
} THBINREGION; /* pragma SCHEMA */
  
  
SKYCOEFFS *thSkycoeffsNew();
SKYCOEFFS *thSkycoeffsMake(const int bsize);

RET_CODE thSkycoeffsDel(SKYCOEFFS *sc);
				   
RET_CODE thSkycoeffsCopy(SKYCOEFFS *sc, 
			 THPIX  *c, THPIX ** cov, THPIX ** invcov,
			 int bsize);
RET_CODE thSkycoeffsPut(SKYCOEFFS *sc, 
			THPIX *c, THPIX **cov, THPIX **invcov,
			int *bsize);

SKYMODEL *thSkymodelNew(const int bsize, /* number of basis functions */ 
			 const int nrow, const int ncol /* size of the image region */
			 );
RET_CODE thSkymodelDel(SKYMODEL *sm);

RET_CODE thSkymodelPut(SKYMODEL *model,
		       SKYBASISSET *basis,
		       SKYCOEFFS *sc,
		       REGION *reg);



RET_CODE thSkybasissetDel(SKYBASISSET *sbs);
SKYBASISSET *thSkybasissetNew();
SKYBASIS *thSkybasisNew();
RET_CODE thSkybasisDel(SKYBASIS *sb);

AMPL *thAmplNew();
RET_CODE thAmplDel(AMPL *ampl);

THBINREGION *thBinregionNew();
void thBinregionDel(THBINREGION *x);

#endif
