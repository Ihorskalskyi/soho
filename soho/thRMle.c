#include "thMle.h"
#include "thRMle.h"

RET_CODE thLSetInitialGuess(LSTRUCT *l) {
char *name = "thLSetInitialGuess";
if (l == NULL) {
	thError("%s: WARNING - null (lstrut) placeholder", name);
	return(SH_SUCCESS);
}

status = copy_parameters_to_initial_guess(l);

status = make_initial_model(l);

status = make_reduced_data(l);

CHISQFL chisq = 0.0;
status = make_chisq_from_reduced_data(l, &chisq); /* calculate value of chisq as < delta | W | delta >  where | delta > is the reduced data = | data - m_init > */

status = put_value_in_init_chisq(l, chisq); /* also print a comment on the change and how much change there was */

