contour_map="/u/khosrow/thesis/opt/soho/plots/plot-contour-sim-demand-52a-mle-demand-112-full-sky-CHISQ-all.png"
tracedir="/u/khosrow/thesis/opt/soho/multi-object/images/mle/float/sim-demand-52a//mle-demand-112/flag-fitsize-unconstrained-init-1-nstep-9000-nearby-10-full-sky-apsector/301/4207/2/plots/"
prefix="lC"

for (( ifield=1; ifield<9999; ifield++ )) do

fieldstr=`printf "%04d" $ifield`
tracefile="lT-sim-mle-gal-all-004207-2-$fieldstr.png"
trace_add="$tracedir$tracefile"
trace_overlay="$tracedir$prefix$tracefile"

#echo "source file1 = $trace_add"
#echo "source file2 = $contour_map"
#echo "destin file  = $trace_overlay"
if [[ -e $trace_overlay ]]; then
	rm $trace_overlay
fi

if [[ -e $contour_map && -e $trace_add ]]; then
	composite $contour_map $trace_add $trace_overlay
fi

done

contour_map="/u/khosrow/thesis/opt/soho/plots/plot-contour-sim-demand-52a-mle-demand-112-full-sky-CHISQ-all.png"
tracedir="/u/khosrow/thesis/opt/soho/multi-object/images/mle/float/sim-demand-52a//mle-demand-112/flag-fitsize-unconstrained-init-1-nstep-9000-nearby-10-full-sky-apsector-hqn/301/4207/2/plots/"
prefix="lC"

for (( ifield=1; ifield<9999; ifield++ )) do

fieldstr=`printf "%04d" $ifield`
tracefile="lT-sim-mle-gal-all-004207-2-$fieldstr.png"
trace_add="$tracedir$tracefile"
trace_overlay="$tracedir$prefix$tracefile"

#echo "source file1 = $trace_add"
#echo "source file2 = $contour_map"
#echo "destin file  = $trace_overlay"
if [[ -e $trace_overlay ]]; then
	rm $trace_overlay
fi

if [[ -e $contour_map && -e $trace_add ]]; then
	composite $contour_map $trace_add $trace_overlay
fi

done
