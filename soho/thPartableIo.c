#include "thPartableIo.h"

const char  DB_INIT_CHAR = '{';
const char  DB_STRUCT_HDR_CHAR = ':';
const char *const DB_STRUCT_HDR_FMT = "%s : %s";
const char  DB_EOL_CHAR = '\n';
const char  DB_LINE_TRANS_CHAR = '\\';
const char  DB_EODB_CHAR = '}';

DBFILE *thDbfileNew() {
DBFILE *f;
f = thCalloc(1, sizeof(DBFILE));
f->filename = thCalloc(MX_STRING_LEN, sizeof(char));
f->status = DB_UNKNOWN_STATUS;
f->mode = thCalloc(3, sizeof(char));
return(f);
}

void thDbfileDel(DBFILE *f) {
if (f == NULL) return;
if (f->filename != NULL) thFree(f->filename);
thFree(f);
}

DBFILE *OpenDBFile(char *filename, char *mode, RET_CODE *status) {
char *name = "OpenDBFile";
if (filename == NULL || strlen(filename) == 0 || mode == NULL || strlen(mode) == 0) {
	thError("%s: ERROR - null or empty filename", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(NULL);
	}
if (strcmp(mode, "r") && strcmp(mode, "w") && strcmp(mode, "a") && strcmp(mode, "r+") && strcmp(mode, "w+") && strcmp(mode, "a+")) {
	thError("%s: ERROR - unknown IO mode %s", name, mode);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(NULL);
}
DBFILE *f;
f = thDbfileNew();
f->f = fopen(filename, mode);
strcpy(f->mode, mode);
f->status = DB_HOF;
thInsertStatus(status, SH_SUCCESS);
return(f);
}

void CloseDBFile(DBFILE *f, RET_CODE *status) {
if (f == NULL) {
	thInsertStatus(status, SH_SUCCESS);
	return;
}
if (f->f == NULL) {
	thInsertStatus(status, SH_SUCCESS);
	return;
}
char *name = "CloseDBFile";
int i;
if ((i = fclose(f->f)) == 0) {
	thError("%s: ERROR - could not close file (error code = %d)", name, i);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return;
}
f->f = NULL;
thInsertStatus(status, SH_SUCCESS);
return;
}

void SeekInitDB(DBFILE *f, char *filename, RET_CODE *status) {
char *name = "SeekInitDB";
if (f == NULL || f->f == NULL || filename == NULL) {
	thError("%s: ERROR - null input or output", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return;
}

RET_CODE status2;
SeekCharDB(f, DB_INIT_CHAR, &status2);

if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not locate %c", name, DB_INIT_CHAR);
	thInsertStatus(status, status2);
	return;
}

if (fscanf(f->f, "%s", filename) != 1 || strlen(filename) == 0) {
	thError("%s: ERROR - could not locate a string for filename", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return;
	}
if (!feof(f->f)) {
	f->status = DB_HOS;
	} else {
	f->status = DB_EOF;
	}
thInsertStatus(status, SH_SUCCESS);
return;
}

void SeekHdrDB(DBFILE *f, char *funcname, char *type, RET_CODE *status) {
char *name = "SeekHdr";
if (f == NULL || f->f == NULL || funcname == NULL || type == NULL) {
	thError("%s: ERROR - null input or output", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return;
	}

if (f->status != DB_HOS && f->status != DB_EOS) {
	thError("%s: ERROR - STRUCTUE header not expected", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return;
	}
RET_CODE status2;
SeekCharDB(f, DB_STRUCT_HDR_CHAR, &status2);
if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not locate a header", name);
	thInsertStatus(status, status2);
	return;
	}
if (fscanf(f->f, DB_STRUCT_HDR_FMT, funcname, type) != 2 || strlen(type) == 0 || strlen(funcname) == 0) {
	thError("%s: ERROR - could not locate a proper filename or typename", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return;
}
SeekLineDB(f, NULL, NULL);
if (f->status != DB_EOF && f->status != DB_EODB) f->status = DB_MOS;
thInsertStatus(status, SH_SUCCESS);
return;
}

void SeekLineDB(DBFILE *f, char *line, RET_CODE *status) {
char *name = "SeekLine";
if (f == NULL || f->f == NULL) {
	thError("%s: ERROR - null input", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return;
	}

char *c;
if (line != NULL) {
	c = line;
	} else {
	c = thCalloc(1, sizeof(char));
	}

char prevc = '\0';
int eofline = 0;
int linetransfer = 0;
while (!eofline && !feof(f->f)) {
	*c = fgetc(f->f);
	if (*c == DB_EOL_CHAR && prevc == DB_LINE_TRANS_CHAR) {linetransfer = 1;} 
	else if (*c == DB_EOL_CHAR && prevc != DB_LINE_TRANS_CHAR) {eofline = 1; *c = '\0';}
	else if (*c == DB_LINE_TRANS_CHAR) {linetransfer = 1;}
	else if (*c == DB_EODB_CHAR) {eofline = 1; *c = '\0'; f->status = DB_EODB;} 
	prevc = *c;
	if (line != NULL && !linetransfer) c++;
	linetransfer = 0;
	}
if (feof(f->f)) {
	f->status = DB_EOF;
	*c = '\0';
	}
thInsertStatus(status, SH_SUCCESS);
return;
}

PARELEM *ReadParelemDB(char *line, char *type, RET_CODE *status) {
char *name = "ReadParelem";
if (line == NULL) {
	thError("%s: ERROR - null input", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(NULL);
}
if (strlen(line) == 0) return(NULL);
TYPE qtype = UNKNOWN_SCHEMA;
if (type != NULL && (qtype = shTypeGetFromName(type)) == UNKNOWN_SCHEMA) {
	thError("%s: WARNING - unknown type: %s", name, type);
	}
SCHEMA *s;
if (qtype != UNKNOWN_SCHEMA) {
	s = shSchemaGetFromType(qtype);
}
PARELEM *pe;
pe = thParelemNew();

char *rtype, *dtype, *dtypex;
rtype = thCalloc(MX_STRING_LEN, sizeof(char));
dtype = thCalloc(MX_STRING_LEN, sizeof(char));

int m;
FILE *stream;
stream = fmemopen(line, strlen(line), "r");

if ((m = fscanf(stream, "%s %s %s %d %d", pe->pname, rtype, dtype, &(pe->npdiv), &(pe->n))) != 5) {
	thError("%s: ERROR - expected (%d) values, found (%d) instead", name, 5, m);
}
/* now make sure that the division type is understood */
dtypex = shValueGetFromEnumName("THPARDIV_TYPE", dtype, (int *) &(pe->divtype));
if (dtypex == NULL) {
	thError("%s: WARNING - unknown division type: %s for record %s", name, dtype, pe->pname);
	thParelemDel(pe);
	thFree(rtype);
	thFree(dtype);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(NULL);
}

SCHEMA_ELEM *se;
se = shSchemaElemGetFromType(qtype, pe->pname);

int i;
char *format;
format = thCalloc(MX_STRING_LEN, sizeof(char));
if (!strcmp(rtype, "int") || !strcmp(rtype, "INT")) {

	pe->ptype = INTEGER_PARTYPE;
	if (se != NULL && 	
	((!strcmp(se->type, "S16") && sizeof(int) != sizeof(S16)) ||  
	(!strcmp(se->type, "S32") && sizeof(int) != sizeof(S32)))) {
		thError("%s: WARNING - mismatch between element type definition (%s) and schema type info (%s)", name, rtype, se->type);
	}
	pe->pdiv = thCalloc(pe->n, sizeof(int));
	strcpy(format, "%d");
	for (i = 0; i < pe->n; i++) {
		if (fscanf(stream, format, ((int *) pe->pdiv) + i) != 1) {
			thError("%s: ERROR - was expecting (%d) division specifiers, found (%d) instead", 
			name, pe->n, i);
			thInsertStatus(status, SH_GENERIC_ERROR);
			break;
		}
	}		
} else if (!strcmp(rtype, "float") || !strcmp(rtype, "FLOAT")) {

        pe->ptype = PIX_PARTYPE;
        if (se != NULL && (
	(!strcmp(se->type, "FL32") && sizeof(THPIX) != sizeof(FL32)) ||  
	(!strcmp(se->type, "DOUBLE") && sizeof(THPIX) != sizeof(double)))) {
                thError("%s: WARNING - mismatch between element type definition (%s) and schema type info (%s)", name, rtype, se->type);
        } 
	pe->pdiv = thCalloc(pe->n, sizeof(THPIX));
	if (sizeof(THPIX) == sizeof(float)) {
		strcpy(format, "%g");
	} else if (sizeof(THPIX) == sizeof(double)) {
		strcpy(format, "%lg");
	} else if (sizeof(THPIX) == sizeof(long double)) {
		strcpy(format, "%Lg");
	}
	for (i = 0; i < pe->n; i++) {
		if (fscanf(stream, format, ((THPIX *) pe->pdiv) + i) != 1) {
			thError("%s: ERROR - was expecting (%d) division specifiers, found (%d) instead", 
			name, pe->n, i);
			thInsertStatus(status, SH_GENERIC_ERROR);
			break;
		}
	}
} else if (qtype == UNKNOWN_SCHEMA || se == NULL) {
	thError("%s: ERROR - unknown type definition and element not existing", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	thParelemDel(pe); thFree(rtype); fclose(stream);
	return(NULL);
} else if (!strcmp(se->type, "SHORT") || !strcmp(se->type, "INT") || 
	!strcmp(se->type, "USHORT") || !strcmp(se->type, "UINT")) {
	pe->ptype = INTEGER_PARTYPE;
	pe->pdiv = thCalloc(pe->n, sizeof(int));
	strcpy(format, "%d");
	for (i = 0; i < pe->n; i++) {
		if (fscanf(stream, format, ((int *) pe->pdiv) + i) != 1) {
			thError("%s: ERROR - was expecting (%d) division specifiers, found (%d) instead", 
			name, pe->n, i);
			thInsertStatus(status, SH_GENERIC_ERROR);
			break;
		}
	}
} else if (!strcmp(se->type, "FLOAT") || !strcmp(se->type, "DOUBLE")) {
	pe->ptype = PIX_PARTYPE;
	pe->pdiv = thCalloc(pe->n, sizeof(THPIX));
	if (sizeof(THPIX) == sizeof(float)) {
		strcpy(format, "%g");
	} else if (sizeof(THPIX) == sizeof(double)) {
		strcpy(format, "%lg");
	} else if (sizeof(THPIX) == sizeof (long double)){
		strcpy(format, "%Lg");
	}
	for (i = 0; i < pe->n; i++) {
		if (fscanf(stream, format, ((THPIX *) pe->pdiv) + i) != 1) {
			thError("%s: ERROR - was expecting (%d) division specifiers, found (%d) instead", 
			name, pe->n, i);
			thInsertStatus(status, SH_GENERIC_ERROR);
			break;
		}
	}	
} else {
	thError("%s: ERROR - unsupported type for record %s", name, pe->pname);
	thInsertStatus(status, SH_GENERIC_ERROR);
	thParelemDel(pe); thFree(rtype);
	fclose(stream);
	return(NULL);
}

fclose(stream);
thFree(rtype);
return(pe);
}

PARTABLE *ReadPartableDB(DBFILE *f, RET_CODE *status) {
char *name = "ReadPartableDB";
if (f == NULL || f->f == NULL) {
	thError("%s: ERROR - null input", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(NULL);
	}

PARTABLE *pt;
pt = thPartableNew();

RET_CODE status2;
SeekHdrDB(f, pt->funcname, pt->tablename, &status2);
if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not read the header of partable in file %s", name, f->filename);
	thInsertStatus(status, status2);
	thPartableDel(pt);
	return(NULL);
	}

PARELEM *pe;
char *line;
line = thCalloc(MX_STRING_LEN, sizeof(char));

int pos;
char *wcolon, *tstr;
tstr = thCalloc(MX_STRING_LEN, sizeof(char));
while(f->status == DB_MOS) {
	pos = ftell(f->f);
	SeekLineDB(f, line, &status2);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not read line", name);
		thInsertStatus(status, status2);
		thPartableDestroy(pt);
		return(NULL);
	}
	wcolon = strchr(line, DB_STRUCT_HDR_CHAR);
	if (wcolon != NULL) {
		*wcolon = '\0';
		if (sscanf(line, "%s", tstr) == 0 || strlen(tstr) == 0) {
			*wcolon = DB_STRUCT_HDR_CHAR;
			fseek(f->f, pos, SEEK_SET);
			f->status = DB_HOS;
			} else {
			*wcolon = DB_STRUCT_HDR_CHAR;
			}
	}
	if (f->status == DB_MOS) {
	pe = ReadParelemDB(line, pt->tablename, &status2);
	if (status2 != SH_SUCCESS) {
		thError("%s: WARNING - could not convert line to parelem:%s", name, line);
		/*
		thInsertStatus(status, status2);
		thPartableDestroy(pt);
		return(NULL);
		*/
	}
	shChainElementAddByPos(pt->table, pe, "PARELEM", TAIL, AFTER);
	}
}

thFree(line); thFree(tstr);
thInsertStatus(status, SH_SUCCESS);
return(pt);
}

CHAIN *ReadPartableChainDB(DBFILE *f, char *filename, RET_CODE *status) {
char *name = "ReadPartableChain";
if (f == NULL || f->f == NULL) {
	thError("%s: ERROR - null input", name);
	return(NULL);
}

RET_CODE status2;
SeekInitDB(f, filename, &status2);
if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could no read the filename for DB", name);
	thInsertStatus(status, status2);
	return(NULL);
}

PARTABLE *pt;
CHAIN *pt_chain;
pt_chain = shChainNew("PARTABLE");
while (f->status == DB_HOS) {
	pt = ReadPartableDB(f, &status2);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - unable to read partable", name);
		shChainDestroy(pt_chain, &thPartableDestroy);
		return(NULL);
	}
	shChainElementAddByPos(pt_chain, pt, "PARTABLE", TAIL, AFTER);
}

thInsertStatus(status, SH_SUCCESS);
return(pt_chain);
}

void SeekCharDB(DBFILE *f, char c, RET_CODE *status) {
char *name = "SeekCharDB";
if (f == NULL || f->f == NULL) {
	thError("%s: ERROR - null input", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return;
	}

char x = '\0';
while(!feof(f->f) && x != c) x = fgetc(f->f);
if (feof(f->f)) f->status = DB_EOF;
if (x == c) {
	thInsertStatus(status, SH_SUCCESS);
	} else {
	thInsertStatus(status, SH_GENERIC_ERROR);
	}
return;
}
