#ifndef THPROFILE_H
#define THPROFILE_H

#include "dervish.h"
#include "thConsts.h"
#include "thBasic.h"
#include "thMath.h"
#include "thProfileTypes.h"
#include "thNaiveMaskTypes.h"

#define CreateProfile SquareCreateProfile

RET_CODE InitQuad();

RET_CODE CircleCreateProfile(THPIX (*f)(PIXPOS *, void *), void *q, char *type,  
			REGION *reg, 
			int idr, int idc,
			QUADRATURE **Q, int nQ);
RET_CODE SquareCreateProfile(THPIX (*f)(PIXPOS *, void *), void *q, char *type,  
			REGION *reg, 
			int idr, int idc,
			QUADRATURE **Q, int nQ);
QUADRATURE **SetProfileQuadrature(int *nq);

void thQuadPix(THPIX (*f)(PIXPOS *, void *), 
		PIXPOS *pixpos1, PIXPOS *pixpos2, 
		void *q, QUADRATURE *Q);
void thQuadOnePix(THPIX (*f)(PIXPOS *, void *), 
	PIXPOS *pixpos1, 
	void *q, QUADRATURE *Q); 

int IsQuadNeeded_CR(OBJMASK *om, QUADRATURE *QE);
QUAD_CONDITION IsQuadNeeded_SQ(NAIVE_MASK *om, QUADRATURE *QE);

void ChooseWorseQuad(QUADRATURE **tQ, QUADRATURE **sQ, int nQ);
int ChooseWorstAcceptableQuad(QUADRATURE **sQ, int nQ);
int ChooseBestQuad(QUADRATURE **Q, int nQ);

void fMaskGetUnionNext(OBJMASK *om, int nrow, int ncol);

void thQuadCopyArray(QUADRATURE **tQ, QUADRATURE **sQ, int nQ);
THPIX thGetNextQuadRadius(THPIX r, int m, THPIX nu, THPIX c);
RET_CODE thVariableGetCenter(void *q, char *type, THPIX *rowc, THPIX *colc);

#endif
