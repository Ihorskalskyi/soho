#!/bin/bash

source "./generate_temporary_file.bash"
source "./run_commands_in_file.bash"

name="sim-batch"

rootdir="/u/khosrow/thesis/opt/soho/single-galaxy/images/sims/star"
ppsf=0.001
run=$DEFAULT_SDSS_RUN
rerun=$DEFAULT_SDSS_RERUN

# generate a temporary command file
commandfile=$(generate_temporary_file --root="./" --prefix="temp-sim-single-submit-batch-" --suffix=".cmd")

bands="gri"
camcols="123456"
nfield=1000

declare -a exec_a=("do-sim" "do-sim" "do-sim")
declare -a dest_a=("star" "exp" "deV")
declare -a model_a=("star" "exp" "deV" "sersic")
n2=3

declare -a psf_a=("9999")
declare -a psfdir_a=("9999-9999")
declare -a inner_lumcut_a=("0.0001")
declare -a outer_lumcut_a=("0.0001")
n3=1

#stripe 82
#4797
declare -a run_a=(4797 2700 2703 2708 2709 2960 3565 3434 3437 2968 4207 1742 4247 4252 4253 4263 4288 1863 1887 2570 2578 2579 109 125 211 240 241 250 251 256 259 273 287 297 307 94 5036 5042 5052 2873 21
24 1006 1009 1013 1033 1040 1055 1056 1057 4153 4157 4158 4184 4187 4188 4191 4192 2336 2886 2955 3325 3354 3355 3360 3362 3368 2385 2506 2728 4849 4858 4868 4874 4894 4895 4899 4905 4927 4930 4933 4948 2854 2855 2856
 2861 2662 3256 3313 3322 4073 4198 4203 3384 3388 3427 3430 4128 4136 4145 2738 2768 2820 1752 1755 1894 2583 2585 2589 2591 2649 2650 2659 2677 3438 3460 3458 3461 3465 7674 7183)
n4=10
n4=${#run_a[@]}
n4=1

for (( i2=0; i2<$n2; i2++ ))
do

for (( i3=0; i3<$n3; i3++ ))
do

for (( i4=0; i4<$n4; i4++ ))
do


model=${model_a[$i2]}
destination=${dest_a[$i2]}
executable=${exec_a[$i2]}

fpsf=${psf_a[$i3]}
psfdir=${psfdir_a[$i3]}
inner_lumcut=${inner_lumcut_a[$i3]}
outer_lumcut=${outer_lumcut_a[$i3]}

run=${run_a[$i4]}

echo "$name: model = $band, camcol = $camcol, destination: $destination"


if [ "$model" = "star" ]; then 

	nfield=200
	rootdir="/u/khosrow/thesis/opt/soho/single-galaxy/images/sims"
	workdir="$rootdir/$destination"
	outdir="$workdir/$rerun/$run"
	sim-single-script.sh --circular --executable=$executable --bands=$bands --camcols=$camcols --nfield=$nfield --run=$run --rerun=$rerun --commandfile=$commandfile --docondor  --model=$model --outdir=$outdir --inner_lumcut=$inner_lumcut --outer_lumcut=$outer_lumcut

else

	nfield=1100
	rootdir="/u/khosrow/thesis/opt/soho/single-galaxy/images/sims/circular"
	workdir="$rootdir/$destination"
	outdir="$workdir/$rerun/$run"
	sim-single-script.sh --circular --executable=$executable --bands=$bands --camcols=$camcols --nfield=$nfield --run=$run --rerun=$rerun --commandfile=$commandfile --docondor  --model=$model --outdir=$outdir --inner_lumcut=$inner_lumcut --outer_lumcut=$outer_lumcut

	nfield=5000
	rootdir="/u/khosrow/thesis/opt/soho/single-galaxy/images/sims/all"
	workdir="$rootdir/$destination"
	outdir="$workdir/$rerun/$run"
	sim-single-script.sh --executable=$executable --bands=$bands --camcols=$camcols --nfield=$nfield --run=$run --rerun=$rerun --commandfile=$commandfile --docondor  --model=$model --outdir=$outdir --inner_lumcut=$inner_lumcut --outer_lumcut=$outer_lumcut

fi

done
done
done 

echo
echo "$name: randomly running commands in file '$commandfile'"
run_commands_in_file --commandfile=$commandfile --random

echo
echo "$name: not deleting temp-file = $commandfile"
#rm $commandfile
