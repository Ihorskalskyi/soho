#ifndef THMGALAXY_H
#define THMGALAXY_H

#include "thLRG.h"
#include "thMGalaxyTypes.h"
#include "thModelTypes.h"
#include "thCTransform.h"
#include "thMGProfile.h"
#include "thPCost.h"

RET_CODE thMGalaxyModeSet(RUN_MODE mode);
RET_CODE thMGalaxyModeGet(RUN_MODE *mode);
RET_CODE thMGalaxyPsfSet(PSF_CONVOLUTION_TYPE psf_convolution_type);
RET_CODE thMGalaxyPsfGet(PSF_CONVOLUTION_TYPE *psf_convolution_type);

/* generic object definition tool */
RET_CODE MGObjcInit(char *tname, char *pname, 
		THOBJCTYPE ctype, char *cname, 
		void *classifier, void *compactor, 
		char **mnames, int nm);
/* definition of object as galaxy and cD-candidate, star-galaxy */
RET_CODE MGalaxyObjcInit(char **mnames, int nm);
RET_CODE McDCandidateObjcInit(char **mnames, int nm); 
RET_CODE MdeVGalaxyObjcInit();
RET_CODE MExpGalaxyObjcInit();
RET_CODE MSersicGalaxyObjcInit();
RET_CODE MSersicExpGalaxyObjcInit();
RET_CODE MSersicSersicGalaxyObjcInit(char **mnames, int nm);
RET_CODE MCoresersicGalaxyObjcInit(char **mnames, int nm);
RET_CODE MStarObjcInit(char *mname);
RET_CODE MStarGalaxyObjcInit(char **mnames, int nm);
RET_CODE MGaussianObjcInit(char **mnames, int nm);

/* initiating the models elements used for definiing the galaxy */
MODEL_ELEM *thStarModelElemNewInBank(char *mname, void *f0, char *pname, 
				void *init, void *h0, RET_CODE *status);
MODEL_ELEM *thGalaxyModelElemNewInBank(char *mname, void *f0, char *pname, 
				void *init, void *h0, void *k0, RET_CODE *status);
RET_CODE MGModelsInit();
/* object classifiers - returns 1 if object belongs to that type */

int MSuperLRGClassifier(PHPROPS *p); 
int MGalaxyClassifier(PHPROPS *p);
int McDCandidateClassifier(PHPROPS *p);
int MdeVGalaxyClassifier(PHPROPS *p);
int MExpGalaxyClassifier(PHPROPS *p);
int MSersicGalaxyClassifier(PHPROPS *p);
int MSersicExpGalaxyClassifier(PHPROPS *p);
int MSersicSersicGalaxyClassifier(PHPROPS *p);
int MCoresersicGalaxyClassifier(PHPROPS *p);
int MStarClassifier(PHPROPS *p);
int MStarGalaxyClassifier(PHPROPS *p);
int MIdentityClassifier(PHPROPS *p);

int PHGalaxyClassifier(PHPROPS *p);
/* classifying if a cD candidate is indeed a cD galaxy */

/*
int McDGalaxyClassify(void *p);
*/

/* initializers for different model variables based on SDSS parameters */
 
RET_CODE MGInitStar(PHPROPS *p, void *q, int band);
RET_CODE MGInitExp(PHPROPS *p, void *q, int band);
RET_CODE MGInitExp1(PHPROPS *p, void *q, int band);
RET_CODE MGInitExp2(PHPROPS *p, void *q, int band);
RET_CODE MGInitGaussian(PHPROPS *p, void *q, int band);
RET_CODE MGInitDeV(PHPROPS *p, void *q, int band);
RET_CODE MGInitDeV1(PHPROPS *p, void *q, int band);
RET_CODE MGInitDeV2(PHPROPS *p, void *q, int band);
RET_CODE MGInitPl(PHPROPS *p, void *q, int band);
RET_CODE MGInitSersic(PHPROPS *p, void *q, int band);
RET_CODE MGInitSersic1(PHPROPS *p, void *q, int band);
RET_CODE MGInitSersic2(PHPROPS *p, void *q, int band);
RET_CODE MGInitCoresersic(PHPROPS *p, void *q, int band);

RET_CODE thMGInitAllObjectTypes(int classflag, GCLASS cdclass, GCLASS galaxyclass, RUN_MODE runmode);

RET_CODE SetMGalaxyStaticPars(char *pname, THPIX value);
RET_CODE initMGalaxy(void *input);
#endif
