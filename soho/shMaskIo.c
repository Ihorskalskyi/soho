
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "strings.h"
#include "dervish.h"
#include "sohoEnv.h"
#include "shDebug.h"

#define FpmFileType STANDARD              /* FITS file Type: STANDARD, NONSTANDARD, IMAGE */
#define FpmFileDef  DEF_DEFAULT /* = DEF_NUM_OF_ELEMS    /* = DEF_NONE: shCFitsIo.h */
#define NROW_FPM 1489
#define NCOLUMN_FPM 2048
#define REGION_FLAGS_FPM NO_FLAGS

MASK *shMaskReadFpm(char *file,  
		     const int row0,  const int column0,  
		     const int row1,  const int column1, 
		     RET_CODE *shStatus);
RET_CODE shWriteFpm(char *file, 
		    MASK *mask);

RET_CODE shCpFpm(char *infile, char *outfile, /* input and output filenames */
		       const int row0, const int column0, 
		       const int row1, const int column1 /* corners of the image in INFILE */);

/*****************************************************************************/

MASK *shMaskReadFpm(char *file,  
		   const int row0,  const int column0,  
		   const int row1,  const int column1, 
		   RET_CODE *shStatus)
{
  char *name = "shMaskReadFpm";
  char *image="im";
  int  nrow,  ncolumn;
  MASK *sub = NULL, *fullmask = NULL, *mask = NULL;

  nrow = row1 - row0 + 1;
  ncolumn = column1 - column0 + 1;

  /*RET_CODE shMaskReadAsFits
    (
    MASK   *a_maskPtr,       // IN: Pointer to mask //
    char   *a_file,          // IN: Name of FITS file //
    DEFDIRENUM a_FITSdef,    // IN: Directory/file extension default type //
    FILE   *maskFilePtr,     // IN:  If != NULL then is a pipe to read from //
    int     a_readtape       // IN:  Flag to indicate that we must fork/exec an
    instance of "dd" to read a tape drive //
    ) */
  mask = shMaskNew(image, nrow, ncolumn);
  fullmask = shMaskNew(image, NROW_FPM, NCOLUMN_FPM);

  *shStatus = shMaskReadAsFits(fullmask, file, FpmFileDef, NULL, 0);
  if (*shStatus != SH_SUCCESS) {
    shMaskDel(mask);
    shMaskDel(fullmask);
    shErro/u/khosrow/lib/Linux/photo/src/phSpanUtil.cr("%s: %d \n", name, *shStatus);
    shError("%s: cannot read the MASK in %s\n", name, file);
    *shStatus = SH_GENERIC_ERROR;
    return(NULL);
    }
  
  /*C SYNTAX: 
    REGION * shSubMaskNew ( char *name,  IN : Address of name 
    MASK *mask // IN : Address of parent region/ 
    int nrow, // IN : Number of rows in image /
    int ncol, // IN : Number of columns in image /
    int row0, // IN : smallest row number in parent /
    int col0, // IN : smallest col number in parent /
    REGION_FLAGS flags // IN : Described above/ )
  */
    /* defining a sub-image and making a new, separate, image from it */
  if ((sub = shSubMaskNew(image, fullmask, nrow, ncolumn, row0, column0, REGION_FLAGS_FPM)) == NULL){
    shError("%s: cannot trim region %s\n", name, file);
    *shStatus = SH_GENERIC_ERROR;
    return(NULL);
  }
  shMaskCopy(sub, mask); 

  if (sub != NULL) {shMaskDel(sub);}
  if (fullmask != NULL) {shMaskDel(fullmask);}

  if (*shStatus != SH_SUCCESS) {
    return(NULL);
  } 
  
  /* successful run */
  *shStatus = SH_SUCCESS;
  return(mask);
}


RET_CODE shWriteFpm(char *file, 
		    MASK *mask)
{

  char *name = "shWriteFpc";
  RET_CODE shStatus;
  
  /* Write the MASK */

  /*
    RET_CODE shMaskWriteAsFits
    (
    MASK   *a_maskPtr,       // IN: Pointer to mask //
    char   *a_file,          // IN: Name of FITS file //
    DEFDIRENUM a_FITSdef,    // IN: Directory/file extension default type //
    FILE   *maskFilePtr,     // IN:  If != NULL then is a pipe to read from //
    int     a_writetape      // IN:  Flag to indicate that we must fork/exec an
    instance of "dd" to write a tape drive //
				      )
  */
  if (mask != NULL){
    shStatus = shMaskWriteAsFits (mask, file, FpmFileDef, NULL, 0);
  }
  return(shStatus);
}
  

RET_CODE shCpFpm(char *infile, char *outfile, /* input and output filenames */
		 const int row0, const int column0, 
		 const int row1, const int column1 /* corners of the image in INFILE */) 
{
  
  char *name = "shCpFpm";
  MASK *mask = NULL;
  RET_CODE shStatus;

  mask = shMaskReadFpm(infile, row0, column0, row1, column1, &shStatus);
  if (shStatus != SH_SUCCESS) {
    shMaskDel(mask);
    return(shStatus);
  }
  shStatus = shWriteFpm(outfile, mask);

  if (mask != NULL) {
    shMaskDel(mask);
  }
  return(shStatus);
}
