pro plot_batch_psf_analysis, arcsec = arcsec, $
	debug = debug, verbose = verbose, logfile = logfile

DEFSYSV, '!PIXELSIZE', 0.396, 1 ;; pixel size in arcsec

;; dir = "./psf-glue-norm-1-correct-binary"
;; srcdir="/u/khosrow/thesis/opt/soho/single-galaxy/images/psf-analysis-norm-1-correct-binary"

;; dir = "./psf-glue-norm-0"
;; srcdir="/u/khosrow/thesis/opt/soho/single-galaxy/images/psf-analysis-norm-0"


run_list=[4797, 2700, 2703, 2708, 2709, 2960, 3565, 3434, 3437, 2968, 4207, 1742, 4247, 4252, 4253, 4263, 4288, 1863, 1887, 2570, 2578, 2579, 109, 125, 211, 240, 241, 250, 251, 256, 259, 273, 287, 297, 307, 94, 5036, 5042, 5052, 2873, 21, 24, 1006, 1009, 1013, 1033, 1040, 1055, 1056, 1057, 4153, 4157, 4158, 4184, 4187, 4188, 4191, 4192, 2336, 2886, 2955, 3325, 3354, 3355, 3360, 3362, 3368, 2385, 2506, 2728, 4849, 4858, 4868, 4874, 4894, 4895, 4899, 4905, 4927, 4930, 4933, 4948, 2854, 2855, 2856, 2861, 2662, 3256, 3313, 3322, 4073, 4198, 4203, 3384, 3388, 3427, 3430, 4128, 4136, 4145, 2738, 2768, 2820, 1752, 1755, 1894, 2583, 2585, 2589, 2591, 2649, 2650, 2659, 2677, 3438, 3460, 3458, 3461, 3465, 7674, 7183]
;;nrun = n_elements(run_list)
;; run_list = run_list[1L:nrun-1L]
nrun = n_elements(run_list)

root="."
srcroot = "/u/khosrow/thesis/opt/soho/single-galaxy/images"

dir_array = ["psf-glue-norm-0", "psf-glue-norm-1"]
srcdir_array = ["psf-analysis-norm-0", "psf-analysis-norm-1"]
suffix_array = ["-norm-0", "-norm-1"]
ndir = n_elements(dir_array)

if (n_elements(logfile) eq 0) then logfile = "./plot-batch-psf.log"
ulogfile = logfile

srcdir_list = srcroot + "/" + srcdir_array
suffix = ""
dir = "./psf-glue"

;; for idir = 0, ndir-1L, 1 do begin
;; dir=root+"/"+dir_array[idir]
;; srcdir=srcroot+"/"+srcdir_array[idir]
;; suffix = suffix_array[idir]

dobundle = 1
doerror = 0
dosinglepanel = 1
doc2w2 = 0 ;; pushes mean to be median
stats = [0.025, 0.5, 0.975]
stat_lines = [3, 0, 3]
camcol_list = [1, 2, 3, 4]
seed0 = total(10 ^ (lindgen(6) * 2) * bin_date(systime(0)))


xrange = [-1, 21]
if (keyword_set(xrange)) then xrange = xrange * !PIXELSIZE
;; background
seed = seed0
yrange = [0.9E-6, 1.1E-1]
doxlog = 0
doylog = 1
yrange = [-2.0E-4, 2.0E-4]
doylog = 0
filename = "psf-analysis-background-mean" + suffix + ".eps"
epsfile = dir + "/" + filename
plot_psf_analysis, dir_list = srcdir_list, epsfile = epsfile, /dobkgrnd, $
	doerror = doerror, dobundle = dobundle, $
	stats = stats, stat_lines = stat_lines, doc2w2 = doc2w2, $
	camcol_list = camcol_list, run_list = run_list, $
	yrange = yrange, xrange = xrange, xlog = doxlog, ylog = doylog, $
	dosinglepanel = dosinglepanel, seed = seed, $
	arcsec = keyword_set(arcsec), $
	debug = keyword_set(debug), verbose = keyword_set(verbose), logfile = ulogfile

seed = seed0
filename = "psf-analysis-background-median" + suffix + ".eps"
epsfile = dir + "/" + filename
plot_psf_analysis, dir_list = srcdir_list, epsfile = epsfile, /dobkgrnd, $
	doerror = doerror, /doc2w2, dobundle = dobundle, $
	stats = stats, stat_lines = stat_lines, run_list = run_list, $
	camcol_list = camcol_list, $
	yrange = yrange, xrange = xrange, xlog = doxlog, ylog = doylog, $
	dosinglepanel = dosinglepanel, seed = seed, $
	arcsec = keyword_set(arcsec), $
	debug = keyword_set(debug), verbose = keyword_set(verbose), logfile = ulogfile

;; amplitude
yrange = [-0.51, 1.06]
doxlog = 0
doylog = 0
seed = seed0
filename = "psf-analysis-amplitude-mean" + suffix + ".eps"
epsfile = dir + "/" + filename
plot_psf_analysis, dir_list = srcdir_list, epsfile = epsfile, /doamplitude, $
	doerror = doerror, dobundle = dobundle, $
	stats = stats, stat_lines = stat_lines, doc2w2 = doc2w2, $
	camcol_list = camcol_list, run_list = run_list, $
	yrange = yrange, xrange = xrange, xlog = doxlog, ylog = doylog, $
	dosinglepanel = dosinglepanel, seed = seed, $
	arcsec = keyword_set(arcsec), $
	debug = keyword_set(debug), verbose = keyword_set(verbose), logfile = ulogfile

seed = seed0
filename = "psf-analysis-amplitude-median" + suffix + ".eps"
epsfile = dir + "/" + filename
plot_psf_analysis, dir_list = srcdir_list, epsfile = epsfile, /doamplitude, $
	doerror = doerror, /doc2w2, dobundle = dobundle, $
	stats = stats, stat_lines = stat_lines, $
	camcol_list = camcol_list, run_list = run_list, $
	yrange = yrange, xrange = xrange, xlog = doxlog, ylog = doylog, $
	dosinglepanel = dosinglepanel, seed = seed, $
	arcsec = keyword_set(arcsec), $
	debug = keyword_set(debug), verbose = keyword_set(verbose), logfile = ulogfile

;; lost light
yrange = [0.9E-3, 1.1E0]
yrange = [-1.1E-2, 1.1E-1]
doxlog = 0
doylog = 1
doylog = 0
seed = seed0
filename = "psf-analysis-lostlight-mean" + suffix + ".eps"
epsfile = dir + "/" + filename
plot_psf_analysis, dir_list = srcdir_list, epsfile = epsfile, /dolostlight, $
	doerror = doerror, dobundle = dobundle, $
	stats = stats, stat_lines = stat_lines, doc2w2 = doc2w2, $
	camcol_list = camcol_list, run_list = run_list, $
	yrange = yrange, xrange = xrange, xlog = doxlog, ylog = doylog, $
	dosinglepanel = dosinglepanel, seed = seed, $
	arcsec = keyword_set(arcsec), $
	debug = keyword_set(debug), verbose = keyword_set(verbose), logfile = ulogfile

seed = seed0
filename = "psf-analysis-lostlight-median" + suffix + ".eps"
epsfile = dir + "/" + filename
plot_psf_analysis, dir_list = srcdir_list, epsfile = epsfile, /dolostlight, $
	doerror = doerror, /doc2w2, dobundle = dobundle, $
	stats = stats, stat_lines = stat_lines, $
	camcol_list = camcol_list, run_list = run_list, $
	yrange = yrange, xrange = xrange, xlog = doxlog, ylog = doylog, $
	dosinglepanel = dosinglepanel, seed = seed, $
	arcsec = keyword_set(arcsec), $
	debug = keyword_set(debug), verbose = keyword_set(verbose), logfile = ulogfile

;; lost light
yrange = [0.9E-3, 1.1E0]
yrange = [-0.11E-1, 0.11E0]
doxlog = 0
doylog = 1
doylog = 0
seed = seed0
filename = "psf-analysis-halofrac-mean" + suffix + ".eps"
epsfile = dir + "/" + filename
plot_psf_analysis, dir_list = srcdir_list, epsfile = epsfile, /dohalofrac, $
	doerror = doerror, dobundle = dobundle, $
	stats = stats, stat_lines = stat_lines, doc2w2 = doc2w2, $
	camcol_list = camcol_list, run_list = run_list, $
	yrange = yrange, xrange = xrange, xlog = doxlog, ylog = doylog, $
	dosinglepanel = dosinglepanel, seed = seed, $
	arcsec = keyword_set(arcsec), $
	debug = keyword_set(debug), verbose = keyword_set(verbose), logfile = ulogfile

seed = seed0
filename = "psf-analysis-halofrac-median" + suffix + ".eps"
epsfile = dir + "/" + filename
plot_psf_analysis, dir_list = srcdir_list, epsfile = epsfile, /dohalofrac, $
	doerror = doerror, /doc2w2, dobundle = dobundle, $
	stats = stats, stat_lines = stat_lines, $
	camcol_list = camcol_list, run_list = run_list, $
	yrange = yrange, xrange = xrange, xlog = doxlog, ylog = doylog, $
	dosinglepanel = dosinglepanel, seed = seed, $
	arcsec = keyword_set(arcsec), $
	debug = keyword_set(debug), verbose = keyword_set(verbose), logfile = ulogfile

;; halo frac and lost light diff
yrange = [0.9E-6, 1.1E5]
doxlog = 0
doylog = 1
yrange = [-1.1E-2, 1.1E-1]
doylog = 0
seed = seed0
filename = "psf-analysis-hldiff-mean" + suffix + ".eps"
epsfile = dir + "/" + filename
plot_psf_analysis, dir_list = srcdir_list, epsfile = epsfile, /dohldiff, $
	doerror = doerror, dobundle = dobundle, $
	stats = stats, stat_lines = stat_lines, doc2w2 = doc2w2, $
	camcol_list = camcol_list, run_list = run_list, $
	yrange = yrange, xrange = xrange, xlog = doxlog, ylog = doylog, $
	dosinglepanel = dosinglepanel, seed = seed, $
	arcsec = keyword_set(arcsec), $
	debug = keyword_set(debug), verbose = keyword_set(verbose), logfile = ulogfile

seed = seed0
filename = "psf-analysis-hldiff-median" + suffix + ".eps"
epsfile = dir + "/" + filename
plot_psf_analysis, dir_list = srcdir_list, epsfile = epsfile, /dohldiff, $
	doerror = doerror, /doc2w2, dobundle = dobundle, $
	stats = stats, stat_lines = stat_lines, $
	camcol_list = camcol_list, run_list = run_list, $
	yrange = yrange, xrange = xrange, xlog = doxlog, ylog = doylog, $
	dosinglepanel = dosinglepanel, seed = seed, $
	arcsec = keyword_set(arcsec), $
	debug = keyword_set(debug), verbose = keyword_set(verbose), logfile = ulogfile

;; endfor ;; end fo for loop for directoy

return
end
