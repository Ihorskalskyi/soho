#ifndef THFITMASKTYPES_H
#define THFITMASKTYPES_H

#include "thConsts.h"
#include "phSpanUtil.h"

typedef struct fitmask {

	int nmask; /* number of masks initiated */
	int npar; /* number of object parameters */
	int npix_mask[NMASK_FITOBJC];
	FL32 radius_mask[NMASK_FITOBJC];
	/* radius of each mask */
	FL32 delta_rms_mask[NMASK_FITOBJC]; 
	/* rms count deviation as defined by Seiger, Graham, Jeijer 2007 */
	FL32 chisq_mask[NMASK_FITOBJC]; 
	/* portion of chi-squared value calculated in the respective mask */
	OBJMASK *span_mask[NMASK_FITOBJC];

	/* chosen object specific chi-squared information */
	FL32 rPetro_objc, rmask_objc, rPetroCoeff_objc;
	int indexPetro_objc, npix_objc;
	FL32 chisq_objc, chisq_nu_objc, delta_rms_objc;

} FITMASK; /* pragma SCHEMA */

FITMASK *thFitmaskNew();
void thFitmaskDel(FITMASK *fitmask);

RET_CODE thFitmaskGetNmask(FITMASK *fitmask, int *nmask);
RET_CODE thFitmaskPutNmask(FITMASK *mask, int nmask);
RET_CODE thFitmaskGetSpanmaskByPos(FITMASK *fitmask, int imask, OBJMASK **sp);
RET_CODE thFitmaskGetRadiusByPos(FITMASK *fitmask, int imask, FL32 *radius);
RET_CODE thFitmaskPutSpanmaskByPos(FITMASK *fitmask, int imask, OBJMASK *sp);
RET_CODE thFitmaskPutDeltaRmsByPos(FITMASK *mask, int imask, MLEFL ss);
RET_CODE thFitmaskPutChisqByPos(FITMASK *mask, int imask, MLEFL ss);
RET_CODE thFitmaskPutNpixByPos(FITMASK *mask, int imask, int npix);

RET_CODE thFitmaskPutRadiusByPos(FITMASK *mask, int imask, FL32 radius);
RET_CODE thFitmaskPutObjcPetro(FITMASK *mask, FL32 rPetro_objc, FL32 rmask_objc, FL32 rPetroCoeff_objc, int indexPetro_objc, int npix_objc, FL32 chisq_objc, FL32 chisq_nu_objc, FL32 delta_rms_objc);

#endif
