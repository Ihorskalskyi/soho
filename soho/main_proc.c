/*
 * Read fpC images (infile), and copy its content to Outfile.
 */
/* PHOTO libraries */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ftcl.h"
#include "dervish.h"
#include "strings.h"

#include "phUtils.h"

#include "phPhotoFitsIO.h"
#include "phSpanUtil.h"

#include "thMath.h"
#include "thDebug.h"
#include "thIo.h"
#include "thAscii.h"
#include "thProc.h"
#include "thMask.h"

int verbose = 0;

static int nrow = 1489;
static int ncol = 2048; 

static void usage(void);


int
main(int ac, char *av[])
{

  char *name = "read_proc";
  /* Arguments */
  char *fname;

  while(ac > 1 && (av[1][0] == '-' || av[1][0] == '+')) {
    switch (av[1][1]) {
    case '?':
    case 'h':
      usage();
      exit(0);
      break;
    case 'v':
      verbose++;
      break;
    default:
      shError("Unknown option %s\n",av[1]);
      break;
    }
    ac--;
    av++;
  }
  if(ac <= 1) {
    shError("You must specify a process par file name. \n");
    exit(1);
  }
  fname = av[1]; 


  thInitIo();
  thTankInit();
  thAsciiInit();
  

  /* get the related schema */
  ASCIISCHEMA *schema = NULL;
  char *strutype = "FRAMEFILES";	
  schema = thTankSchemaGetByType(strutype);

  if (schema == NULL) {
    printf("%s: No schema exists for structure %s", 
		   name, strutype);
  }

  /* get the structure constructor */
  void *(*strunew)();
  strunew = schema->constructor;
  if (strunew == NULL) {
    printf("%s: structure constructor for %s not available in tank", 
		   name, strutype);
  }

  /* construct a structure */
  void *stru;
  stru = (*strunew)();

  FRAMEFILES *ff;
  ff = (FRAMEFILES *) stru;

  FILE *fil;
  fil = fopen(fname, "r");
  
  CHAIN *fchain;
  fchain = thAsciiChainRead(fil);

  if (shChainSize(fchain) == 0)thError("%s: no structures was properly read from the par file", name);

  printf("%s: (%d) pieces of frame info read \n",
	 name, shChainSize(fchain));
   
  ff = (FRAMEFILES *) shChainElementGetByPos(fchain, 0);
  
  //ff->phconfigfile[0] = 'X';
  
  size_t offset;
  offset = offsetof(FRAMEFILES, phconfigfile);

  void **phconfigfile;
  phconfigfile = (void **)(ff + offset);

  if (ff == NULL) {
    thError("%s: NULL returned from chain - check source code", name);
  } else {    
    /* fields of the ff structure refer to outbound memory locations */
    printf("phconfigfile = %s \n", ff->phconfigfile);
    printf("phecalibfile = %s \n", ff->phecalibfile);
    printf("phflatframefile = %s \n", ff->phflatframefile);
    printf("phmaskfile = %s \n", ff->phmaskfile);
    printf("phsmfile = %s \n", ff->phsmfile);
    printf("phpsfile = %s \n", ff->phpsfile);
    printf("phobjcfile = %s \n", ff->phobjcfile);
    printf("thsbfile = %s \n", ff->thsbfile);
    printf("thobfile = %s \n", ff->thobfile);
    printf("thmakselfile = %s \n", ff->thmaskselfile);
    //printf("thpsmodelfile = %s \n", ff->thpsmodelfile);
    printf("thobjcselfile = %s \n", ff->thobjcselfile);
    printf("thsmfile = %s \n", ff->thsmfile);
    printf("thpsfile = %s \n", ff->thpsfile);
    printf("thobjcfile = %s \n", ff->thobjcfile);

  }

  
  /* initiates the Spanmask module */
  thInitIo();
  initSpanObjmask();

  PROCESS *proc;
  proc = thProcessNew();

  int i, nframe = 1;
  int camrow, camcol;

  for (camrow = 1; camrow < 2;  camrow++) {
    for (camcol = 1; camcol < 2; camcol++) {

      FRAME *frame;
      frame = thFrameNew();
      if (frame->files != NULL) {
	thFramefilesDel(frame->files);
      }
      
      frame->files = ff;
      
      frame->proc = proc;
      
      printf("%s: loading fpC file\n", name);
      thFrameLoadImage(frame);

      /* for debugging puporsed only
	 frame->id->camcol = camcol;
	 frame->id->camrow = camrow;
	 
	 printf("%s: camrow = %d, camcol = %d \n", 
	 name, camrow, camcol);
	 
      */

      printf("%s: loading amplifier information\n", name);
      thFrameLoadAmpl(frame); 
      printf("%s: loading PHOTO masks\n", name);
      thFrameLoadPhMask(frame);
      printf("%s: loading sky basis set \n", name);
      thFrameLoadSkybasisset(frame);
      printf("%s: loading OBJC's \n", name);
      thFrameLoadPhObjc(frame);
      printf("%s: loading PHOTO psf \n", name);
      thFrameLoadPhPsf(frame);
      printf("%s: loading poisson weights \n", name);
      thFrameLoadWeight(frame);
      printf("%s: loading SOHO masks \n", name);
      thFrameLoadThMask(frame);
      printf("%s: fitting sky \n", name);
      thFrameFitSky(frame);


    }
  }
  
  thErrShowStack();

  /* successfull run */
  return(0);
}

/*****************************************************************************/

static void
usage(void)
{
   char **line;

   static char *msg[] = {
      "Usage: read_atlas_image [options] output-file n_tr n_tc",
      "Your options are:",
      "       -?      This message",
      "       -h      This message",
      "       -i      Print an ID string and exit",
      "       -v      Turn up verbosity (repeat flag for more chatter)",
      NULL,
   };

   for(line = msg;*line != NULL;line++) {
      fprintf(stderr,"%s\n",*line);
   }
}

