#!/bin/bash

function check_data_exists_for_mle {

local name="check_data_exists_for_mle"
DATE=$(date +"%m-%d-%Y")
TIME=$(date +"%T") 

lband=no
lcamcol=no
lrun=no
lrerun=no
ldatadir=no

fit2fpCC=no
dband="r"
dcamcol="4"
drun=$DEFAULT_SDSS_RUN
drerun="137"

while getopts ':-:' OPTION ; do
  case "$OPTION" in
    -  ) [ $OPTIND -ge 1 ] && optind=$(expr $OPTIND - 1 ) || optind=$OPTIND
         eval OPTION="\$$optind"
         OPTARG=$(echo $OPTION | cut -d'=' -f2)
         OPTION=$(echo $OPTION | cut -d'=' -f1)
         case $OPTION in
	     --fit2fpCC  ) fit2fpCC=yes		         ;;
     	     --run       ) lrun=yes; run="$OPTARG"	 ;;
	     --rerun     ) lrerun=yes; rerun="$OPTARG"    ;; 
	     --camcol    ) lcamcol=yes; camcol="$OPTARG" ;;
	     --band      ) lband=yes; band="$OPTARG"	 ;;
	     --datadir   ) ldatadir=yes; datadir="$OPTARG"     ;;
	* )  echo "$name: invalid options (long) - optind = $OPTARG, option = $OPTION" ;;
         esac
       OPTIND=1
       shift
      ;;
    ? )  echo "$name: invalid options (short) "  ;;
  esac
done

if [ $ldatadir == no ]; then
	echo "$name: ERROR - argument (datadir) should be set"
	exit
fi
if [ $lrun == no ]; then
	run=$drun
	echo "$name: argument (run)    set to '$run'"
fi
if [ $lrerun == no ]; then
	rerun=$drerun;
	echo "$name: argument (rerun)  set to '$rerun'"
fi
if [ $lcamcol == no ]; then
	camcol=$dcamcol
	echo "$name: argument (camcol) set to '$camcol'"
fi
if [ $lband == no ]; then
	band=$dband
	echo "$name: argument (band)   set to '$band'"
fi

fieldstr=`printf "%04d" $field`
runstr=`printf "%06d" $run`

srcparfile="$datadir/ff-$runstr-$fieldstr-$band$camcol.par"
fpCfile="$datadir/$fpCprefix-$runstr-$band$camcol-$fieldstr.fit"	
fpCCfile="$datadir/$fpCCprefix-$runstr-$band$camcol-$fieldstr.fit"

if [[ $fit2fpCC == yes ]]; then
	desire_fpCfile=$fpCCfile
else
	desired_fpCCfile=$fpCfile
fi
desired_fpCfile_gz="$desired_fpCfile.gz"
if [[ -e $srcparfile ]]; then 
#  && [[ [ -e $desired_fpCfile] || [ -e $desired_fpCfile_gz ] ]] ]]; then
	exists=1
else
	exists=0
fi
#since the value is an integer you can return it using the returned command
return exists

}
