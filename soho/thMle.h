#ifndef THMLE_H
#define THMLE_H

#include "thMleTypes.h"
#include "thMath.h"
#include "thProfile.h"
#include "thAlgorithm.h"

RET_CODE thLRun(LSTRUCT *lstruct);
RET_CODE thModelExtLMFitSimpleRun(LSTRUCT *lstruct);
RET_CODE thModelExtLMFitAlgRun(LSTRUCT *lstruct);

RET_CODE DoLMFitSimpleRun(LSTRUCT *lstruct);
RET_CODE DoRSVFitAlgRun(LSTRUCT *lstruct, LSECTOR sector);
RET_CODE DoPGDFitAlgRun(LSTRUCT *lstruct, LSECTOR sector);
RET_CODE DoNAGFitAlgRun(LSTRUCT *lstruct, LSECTOR sector);
RET_CODE DoMGDFitAlgRun(LSTRUCT *lstruct, LSECTOR sector);
RET_CODE DoGDFitAlgRun(LSTRUCT *lstruct, LSECTOR sector);
RET_CODE DoQNFitAlgRun(LSTRUCT *lstruct, LSECTOR sector);
RET_CODE DoLMFitAlgRun(LSTRUCT *lstruct, LSECTOR sector);
RET_CODE DoAmplitudeFit(LSTRUCT *lstruct);

RET_CODE MakeImageAndMatrices(LSTRUCT *lstruct, RUNFLAG rflag);

RET_CODE thMakeM(LSTRUCT *lstruct);
RET_CODE thMakeMAlgRun(LSTRUCT *lstruct);
RET_CODE thMakeJMatricesSimpleRun(LSTRUCT *lstruct);
RET_CODE thMakeJMatricesAlgRun(LSTRUCT *lstruct);

CONVERGENCE CheckLMConvergence(LSTRUCT *lstruct, LSECTOR sector, RET_CODE *status);


RET_CODE thLCalcChisqNSimpleRun(LSTRUCT *lstruct);
RET_CODE thLCalcChisqNAlgRun(LSTRUCT *lstruct);
RET_CODE thLCalcCovap(LSTRUCT *lstruct, int *singular);

int IsRecursionNeeded(LSTRUCT *lstruct, RET_CODE *status);

/* to be written */
RET_CODE DoLMFit(LSTRUCT *lstruct);
RET_CODE GetAmpParCov(LSTRUCT *lstruct);
RET_CODE GetLS(LSTRUCT *lstruct, MLEFL ***mimj, MLEFL **mid, 
		THPIX **amp, MLEFL ***cov, int *namp);

RET_CODE ExtractLSMatrices(LSTRUCT *lstruct, MLEFL **dmi, MLEFL ***mimj, 
			MLEFL **amp, MLEFL **ampT, MLEFL ***cov, MLEFL ***covT, int *namp);
RET_CODE ExtractLMCurrentMatrices(LSTRUCT *lstruct, LSECTOR sector,  
				MLEFL ***jtj, MLEFL ***jtjpdiag, MLEFL ***covT, MLEFL **jtdmm, 
				MLEFL **dX, MLEFL **dxT, CHISQFL *chisq, CHISQFL *cost, int *npar);

RET_CODE ExtractLMNewChisq(LSTRUCT *lstruct, CHISQFL *Nchisq);

/* initializing the L-method */
RET_CODE thInitLmethod(LSTRUCT *lstruct);
RET_CODE thLGetLambda0(MLEFL **Jtj, int np, LMETHOD *lm);
RET_CODE thBuildImage(void *fparam, char *mname, char **rnames, int nmpar, LIMPACK *impack, RUNFLAG rflag);

/* dummy */
RET_CODE thGenericPixelIntegrator (void * q, void *f, REGION *reg, THPIX *mcount);

/* Algorithm handling */
RET_CODE UploadModelToMemory(int imodel, LSTRUCT *lstruct);
RET_CODE KillAllModelsInMemory(LSTRUCT *lstruct);
RET_CODE KillModelInMemory(int imodel, LSTRUCT *lstruct);
RET_CODE DoInnerProduct(int imodel1, int imodel2, LSTRUCT *lstruct);
RET_CODE DoMMInnerProduct(int imodel1, int imodel2, LSTRUCT *lstruct);
RET_CODE DoMDInnerProduct(int imodel1, LSTRUCT *lstruct);
RET_CODE DoDDInnerProduct(LSTRUCT *lstruct);


RET_CODE DoAddToModel(int imodel, LSTRUCT *lstruct);
RET_CODE DoInitModel(LSTRUCT *lstruct);

RET_CODE thAlgorithmStepPerform(ALGORITHM_STEP *step, LSTRUCT *lstruct);
RET_CODE thAlgorithmRun(ALGORITHM *alg, LSTRUCT *lstruct);	

RET_CODE thSizeOfImpack(LIMPACK *impack, MEMFL *z);
RET_CODE thOverlapBetweenImpacks(LIMPACK *impack1, LIMPACK *impack2, FITSTAT fitstat1, FITSTAT fitstat2, MEMFL *z);
RET_CODE thCreateAdjacencyMatrix(ADJ_MATRIX *matrix, LSTRUCT *lstruct);

RET_CODE thLCompile(LSTRUCT *l, MEMDBLE memory, SIMFLAG simflag);

/* MISC */
RET_CODE thObjcMakeImage(THOBJC *objc, REGION *reg);

/* create psf image in a location without the need to create a star */
RET_CODE thLstructGenPsf(LSTRUCT *l, REGION *reg, PSF_CONVOLUTION_REPORT *report, THPIX xc, THPIX yc, PSF_CONVOLUTION_TYPE convtype);

RET_CODE thLCrudeCalib(LSTRUCT *l, CRUDE_CALIB *cc);
RET_CODE thObjcCrudeCalib(CRUDE_CALIB *cc, THOBJC *objc);
RET_CODE thLCalib(LSTRUCT *lstruct);
RET_CODE thLRecordLfit(LSTRUCT *lstruct);
RET_CODE thLDumpIo(LSTRUCT *l);
RET_CODE thLWriteMemoryStat(LSTRUCT *lstruct, char *fname1, char *fname2);

RET_CODE DoCost(LSTRUCT *lstruct);
RET_CODE thLObjcCalcPcost(THOBJC *objc, 
	CHISQFL *pcost, CHISQFL *dpcost, CHISQFL **ddpcost, int npar,  
	char **rnames, int *pndexmap, int nrnames, int add);
RET_CODE thLstructManipulateWeight(LSTRUCT *lstruct, char **classnames, int nclass);
RET_CODE thLfitAddLprofileFromLstruct(LFIT *lfit, LSTRUCT *l, XUPDATE update);
RET_CODE thLMakeModelReg(THPROP *model, REGION *reg, THPIX *amp, LSTRUCT *l);
RET_CODE thLProfileAddModelProfile(LSTRUCT *l, LPROFILE *lprofile, int i, THPROP *model, PSFWING *wpsfwing);

RET_CODE thLprofileUpdateFromCalib(LPROFILE *lprofile, CALIB_WOBJC_IO *cwphobjc, CALIB_PHOBJC_IO *cphobjc);
RET_CODE thLprofileUpdateFromWObjcIo(LPROFILE *lprofile, WOBJC_IO *wobjcio);	

RET_CODE thLUPObjcRenewFromPhprop(LSTRUCT *l);
RET_CODE thObjcTweakWobjc(THOBJC *objc, int fitflag);

RET_CODE thLRefresh(LSTRUCT *l);
RET_CODE thLworkUpdateBqn(LWORK *lwork, LSECTOR sector);

RET_CODE thLmethodDecideWorkType(LSTRUCT *lstruct, LMETHOD *lmethod, LWORK *lwork);
RET_CODE thLworkUpdateAdaDeltaGD(LWORK *lwork, LWMASK *lwmask, LMETHOD *lm, LSECTOR sector);

#endif
