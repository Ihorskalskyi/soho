/* deallocating quasi-newton B-matrix (inverse of Hessian) */
if (lwork->Hqnap != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (Hqnap) allocated while (nap = %d)", name, lwork->nap);
	return(SH_GENERIC_ERROR);
}

if (lwork->Hqn != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (Hqn) allocated while (np = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}

if (lwork->Hqnap != NULL) {
	 if (lwork->Hqnap[0] != NULL) thFree(lwork->Hqnap[0]);
	thFree(lwork->Hqnap);
	lwork->Hqnap = NULL;
}

if (lwork->Hqn != NULL) {
	thFree(lwork->Hqn);
	lwork->Hqn = NULL;
}

if (lwork->Hqnap_prev != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (Hqnap_prev) allocated while (nap = %d)", name, lwork->nap);
	return(SH_GENERIC_ERROR);
}

if (lwork->Hqn_prev != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (Hqn_prev) allocated while (np = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}

if (lwork->Hqnap_prev != NULL) {
	 if (lwork->Hqnap_prev[0] != NULL) thFree(lwork->Hqnap_prev[0]);
	thFree(lwork->Hqnap_prev);
	lwork->Hqnap_prev = NULL;
}

if (lwork->Hqn_prev != NULL) {
	thFree(lwork->Hqn_prev);
	lwork->Hqn_prev = NULL;
}


