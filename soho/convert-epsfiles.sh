!#/bin/bash
declare -a dir_a=("./Sim-Demand-Analysis/Mixed-Noise-Demand-11" "./Sim-Demand-Analysis/Mixed-Noise-Demand-16" "./Sim-Demand-Analysis/Sim-Demand-11" "./Sim-Demand-Analysis/Sim-Demand-16")
declare -a ext_a=("png" "eps" "ps" "jpeg")
for dir in dir_a; do
for f in ./$dir/*.pdf; do
  echo $f
  for ext in ext_a; do
  	convert "$f" "${f%.pdf}.$ext"
  done
done
done


