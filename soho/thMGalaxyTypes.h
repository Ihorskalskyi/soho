#ifndef THMGALAXYTYPES_H
#define THMGALAXYTYPES_H

#include "dervish.h"
#include "phObjc.h"
#include "thConsts.h"

typedef enum gclass {
	DEV_GCLASS, EXP_GCLASS, 
	DEVEXP_GCLASS, DEVDEV_GCLASS, DEVPL_GCLASS,  
	EXPDEV_GCLASS, EXPEXP_GCLASS,
	SERSIC_GCLASS, SERSICPL_GCLASS, SERSICEXP_GCLASS, SERSICSERSIC_GCLASS, CORESERSIC_GCLASS,  
	STAR_GCLASS, /* I suggest that STAR_GCLASS should not be supported in the final binary */
	N_GCLASS, UNKNOWN_GCLASS
} GCLASS;

/* generic model parameter type */
typedef struct gmodelpars {
	THPIX xc, yc; /* center location */
	THPIX re, ue; /* radial shape parameters */
	THPIX e, phi, E; /* canonical shape parameters */
	THPIX v, w; /* hyperbolic shape parameters */
	THPIX V, W; /* klog-hyperbolic shape paremeters */
	THPIX n, n1, n2; /* auxiliary shape parameters */
	THPIX a, b, c;   /* auxiliary shape parameters */
	THPIX sigma, sigma1, sigma2; /* auxiliary shape parameters */
	THPIX aphi, bphi, cphi;
	THPIX ae, be, ce;
	THPIX aE, bE, cE, eE, eEE;
	THPIX av, bv, cv;
	THPIX aw, bw, cw;
	THPIX aV, bV, cV;
	THPIX aW, bW, cW;
	THPIX reUe, reUeUe;
	THPIX alpha, beta, s2phi, c2phi;
	int band;	
	/* added on April 27, 2015 */
	THPIX lumcut, inner_lumcut, outer_lumcut;
} GMODELPARS;

typedef struct starpars {
	THPIX I; 
	THPIX xc, yc;
	THPIX mcount, counts;
	int band;
	/* added on April 27, 2015 */
	THPIX lumcut, inner_lumcut, outer_lumcut;
} STARPARS;

typedef struct devpars {
	THPIX I;
	THPIX xc, yc;
	THPIX re, ue;
	THPIX e, phi, E;
	THPIX v, w;
	THPIX V, W;
	THPIX a, b, c;
	THPIX aphi, bphi, cphi;
	THPIX ae, be, ce;
	THPIX aE, bE, cE;
	THPIX av, bv, cv;
	THPIX aw, bw, cw;
	THPIX aV, bV, cV;
	THPIX aW, bW, cW;
	THPIX reUe, reUeUe, eE, eEE;
	THPIX alpha, beta, s2phi, c2phi;
	THPIX mcount, counts;
	int band;
	/* added on April 27, 2015 */
	THPIX lumcut, inner_lumcut, outer_lumcut;
} DEVPARS;

typedef struct exppars {
	THPIX I;
	THPIX xc, yc;
	THPIX re, ue;
	THPIX e, phi, E;
	THPIX v, w;
	THPIX V, W;
	THPIX a, b, c;
	THPIX aphi, bphi, cphi;
	THPIX ae, be, ce;
	THPIX aE, bE, cE;
	THPIX av, bv, cv;
	THPIX aw, bw, cw;
	THPIX aV, bV, cV;
	THPIX aW, bW, cW;
	THPIX reUe, reUeUe, eE, eEE;
	THPIX alpha, beta, s2phi, c2phi;
	THPIX mcount, counts;
	int band;
	/* added on April 27, 2015 */
	THPIX lumcut, inner_lumcut, outer_lumcut;
} EXPPARS;

typedef struct sersicpars {
	THPIX I;
	THPIX xc, yc;
	THPIX re, ue;
	THPIX e, phi, E;
	THPIX n, N, k, inverse_2n, gamma_2n, ln_gamma_2n;
	THPIX v, w;
	THPIX V, W;
	THPIX a, b, c;
	THPIX aphi, bphi, cphi;
	THPIX ae, be, ce;
	THPIX aE, bE, cE;
	THPIX av, bv, cv;
	THPIX aw, bw, cw;
	THPIX aV, bV, cV;
	THPIX aW, bW, cW;
	THPIX reUe, reUeUe, eE, eEE, kn, nN;
	THPIX alpha, beta, s2phi, c2phi;
	THPIX mcount, counts;
	int band;
	/* added on April 27, 2015 */
	THPIX lumcut, inner_lumcut, outer_lumcut;
	int prv_cid;
} SERSICPARS;

typedef struct coresersicpars {
	THPIX I;
	THPIX xc, yc;
	THPIX re, ue;
	THPIX rb, ub;
	THPIX e, phi, E;
	THPIX n, N, k, inverse_2n, gamma_2n, ln_gamma_2n;
	THPIX delta, D, gamma, G;
	THPIX v, w;
	THPIX V, W;
	THPIX a, b, c;
	THPIX aphi, bphi, cphi;
	THPIX ae, be, ce;
	THPIX aE, bE, cE;
	THPIX av, bv, cv;
	THPIX aw, bw, cw;
	THPIX aV, bV, cV;
	THPIX aW, bW, cW;
	THPIX reUe, reUeUe, eE, eEE, kn, nN;
	THPIX rbUb, rbUbUb; 
	THPIX dD, gG;
	THPIX alpha, beta, s2phi, c2phi;
	THPIX mcount, counts;
	THPIX r50, k50; 
	int band;
	/* added on April 27, 2015 */
	THPIX lumcut, inner_lumcut, outer_lumcut;
} CORESERSICPARS;




typedef struct powerlawpars {
	THPIX I;
	THPIX xc, yc;
	THPIX re, ue;
	THPIX e, phi, E;
	THPIX v, w;
	THPIX V, W;
	THPIX a, b, c;
	THPIX aphi, bphi, cphi;
	THPIX ae, be, ce;
	THPIX aE, bE, cE;
	THPIX av, bv, cv;
	THPIX aw, bw, cw;
	THPIX aV, bV, cV;
	THPIX aW, bW, cW;
	THPIX reUe, reUeUe, eE, eEE;
	THPIX alpha, beta, s2phi, c2phi;
	THPIX n; /* index */
	THPIX mcount, counts;
	int band;
	/* added on April 27, 2015 */
	THPIX lumcut, inner_lumcut, outer_lumcut;
} POWERLAWPARS;

typedef struct gaussianpars {
	THPIX xc, yc;	
	THPIX a, b, c;	
	THPIX re, ue;
	THPIX phi, e, E;
	THPIX v, w;
	THPIX V, W;
	THPIX sigmax, sigmay; /* sigmax > sigmay */
	THPIX aphi, bphi, cphi; /* derivatives w.r.t. phi */
	THPIX ae, be, ce; /* derivatives w.r.t. e */
	THPIX aE, bE, cE, eE, eEE; /* derivatives w.r.t. E */
	THPIX av, bv, cv; /* derivatives w.r.t. shape parameters v, w */
	THPIX aw, bw, cw;
	THPIX aV, bV, cV; /* derivatives w.r.t. shape parameters V, W */
	THPIX aW, bW, cW;
	THPIX reUe, reUeUe; /* derivative w.r.t. super-logarithmic radial scale */
	THPIX alpha, beta;
	THPIX c2phi, s2phi;
	THPIX mcount, counts;
	int band;
	/* added on April 27, 2015 */
	THPIX lumcut, inner_lumcut, outer_lumcut;
} GAUSSIANPARS;

typedef struct objcpars {

	/* star component */
	THPIX I_star, J_star;
	THPIX xc_star, yc_star;
 	THPIX mcount_star;
	THPIX counts_star;
	THPIX flux_star, mag_star;

	/* standard errors */
	THPIX IErr_star, JErr_star;
	THPIX xcErr_star, ycErr_star;
	THPIX countsErr_star;
	THPIX fluxErr_star;
	THPIX magErr_star;

	/* accumulated galaxy models */
	THPIX counts_model;
	THPIX xc_model, yc_model;	
	THPIX flux_model, mag_model;
	THPIX chisq_model;
	int namp_model;

	/*standard errors */
	THPIX countsErr_model;
	THPIX xcErr_model, ycErr_model;	
	THPIX fluxErr_model, magErr_model;

	/* deV component */
	THPIX I_deV, J_deV;
	THPIX xc_deV, yc_deV;
	THPIX re_deV, e_deV, phi_deV;
	THPIX a_deV, b_deV, c_deV;
	THPIX ue_deV, E_deV;
	THPIX v_deV, w_deV;
	THPIX V_deV, W_deV;
	THPIX mcount_deV;
	THPIX counts_deV;
	THPIX flux_deV, mag_deV;

	/* standard errors */
	THPIX IErr_deV, JErr_deV;
	THPIX xcErr_deV, ycErr_deV;
	THPIX reErr_deV, eErr_deV, phiErr_deV;
	THPIX aErr_deV, bErr_deV, cErr_deV;
	THPIX ueErr_deV, EErr_deV;
	THPIX vErr_deV, wErr_deV;
	THPIX VErr_deV, WErr_deV;
	THPIX countsErr_deV;
	THPIX fluxErr_deV, magErr_deV;

	/* first (inner) deV component */
	THPIX I_deV1, J_deV1;
	THPIX xc_deV1, yc_deV1;
	THPIX re_deV1, e_deV1, phi_deV1;
	THPIX a_deV1, b_deV1, c_deV1;
	THPIX ue_deV1, E_deV1;
	THPIX v_deV1, w_deV1;
	THPIX V_deV1, W_deV1;
	THPIX mcount_deV1;
	THPIX counts_deV1;
	THPIX flux_deV1, mag_deV1;
	
	/* standard errors */
	THPIX IErr_deV1, JErr_deV1;
	THPIX xcErr_deV1, ycErr_deV1;
	THPIX reErr_deV1, eErr_deV1, phiErr_deV1;
	THPIX aErr_deV1, bErr_deV1, cErr_deV1;
	THPIX ueErr_deV1, EErr_deV1;
	THPIX vErr_deV1, wErr_deV1;
	THPIX VErr_deV1, WErr_deV1;
	THPIX countsErr_deV1;
	THPIX fluxErr_deV1, magErr_deV1;

	/* second (outer) deV component */
	THPIX I_deV2, J_deV2;
	THPIX xc_deV2, yc_deV2;
	THPIX re_deV2, e_deV2, phi_deV2;
	THPIX a_deV2, b_deV2, c_deV2;
	THPIX ue_deV2, E_deV2;
	THPIX v_deV2, w_deV2;
	THPIX V_deV2, W_deV2;
	THPIX mcount_deV2;
	THPIX counts_deV2;
	THPIX flux_deV2, mag_deV2;
	
	/* standard errors */
	THPIX IErr_deV2, JErr_deV2;
	THPIX xcErr_deV2, ycErr_deV2;
	THPIX reErr_deV2, eErr_deV2, phiErr_deV2;
	THPIX aErr_deV2, bErr_deV2, cErr_deV2;
	THPIX ueErr_deV2, EErr_deV2;
	THPIX vErr_deV2, wErr_deV2;
	THPIX VErr_deV2, WErr_deV2;
	THPIX countsErr_deV2;
	THPIX fluxErr_deV2, magErr_deV2;

	/* exponential component */
	THPIX I_Exp, J_Exp;
	THPIX xc_Exp, yc_Exp;
	THPIX re_Exp, e_Exp, phi_Exp;
	THPIX a_Exp, b_Exp, c_Exp;
	THPIX ue_Exp, E_Exp;
	THPIX v_Exp, w_Exp;
	THPIX V_Exp, W_Exp;
	THPIX mcount_Exp;
	THPIX counts_Exp;
	THPIX flux_Exp, mag_Exp;

	/* standard errors */
	THPIX IErr_Exp, JErr_Exp;
	THPIX xcErr_Exp, ycErr_Exp;
	THPIX reErr_Exp, eErr_Exp, phiErr_Exp;
	THPIX aErr_Exp, bErr_Exp, cErr_Exp;
	THPIX ueErr_Exp, EErr_Exp;
	THPIX vErr_Exp, wErr_Exp;
	THPIX VErr_Exp, WErr_Exp;
	THPIX countsErr_Exp;	
	THPIX fluxErr_Exp, magErr_Exp;

	/* first (inner) exponential component */

	THPIX I_Exp1, J_Exp1;
	THPIX xc_Exp1, yc_Exp1;
	THPIX re_Exp1, e_Exp1, phi_Exp1;
	THPIX a_Exp1, b_Exp1, c_Exp1;
	THPIX ue_Exp1, E_Exp1;
	THPIX v_Exp1, w_Exp1;
	THPIX V_Exp1, W_Exp1;
	THPIX mcount_Exp1;
	THPIX counts_Exp1;
	THPIX flux_Exp1, mag_Exp1;

	/* standard errors */
	THPIX IErr_Exp1, JErr_Exp1;
	THPIX xcErr_Exp1, ycErr_Exp1;
	THPIX reErr_Exp1, eErr_Exp1, phiErr_Exp1;
	THPIX aErr_Exp1, bErr_Exp1, cErr_Exp1;
	THPIX ueErr_Exp1, EErr_Exp1;
	THPIX vErr_Exp1, wErr_Exp1;
	THPIX VErr_Exp1, WErr_Exp1;
	THPIX countsErr_Exp1;
	THPIX fluxErr_Exp1, magErr_Exp1;

	/* second (outer) exponential component */

	THPIX I_Exp2, J_Exp2;
	THPIX xc_Exp2, yc_Exp2;
	THPIX re_Exp2, e_Exp2, phi_Exp2;
	THPIX a_Exp2, b_Exp2, c_Exp2;
	THPIX ue_Exp2, E_Exp2;
	THPIX v_Exp2, w_Exp2;
	THPIX V_Exp2, W_Exp2;
	THPIX mcount_Exp2;
	THPIX counts_Exp2;
	THPIX flux_Exp2, mag_Exp2;

	/* standard errors */
	THPIX IErr_Exp2, JErr_Exp2;
	THPIX xcErr_Exp2, ycErr_Exp2;
	THPIX reErr_Exp2, eErr_Exp2, phiErr_Exp2;
	THPIX aErr_Exp2, bErr_Exp2, cErr_Exp2;
	THPIX ueErr_Exp2, EErr_Exp2;
	THPIX vErr_Exp2, wErr_Exp2;
	THPIX VErr_Exp2, WErr_Exp2;
	THPIX countsErr_Exp2;
	THPIX fluxErr_Exp2, magErr_Exp2;

	/* powerlaw component */
/* 
	THPIX I_Pl, J_Pl;
	THPIX xc_Pl, yc_Pl;
	THPIX re_Pl, e_Pl, phi_Pl, n_Pl;
	THPIX a_Pl, b_Pl, c_Pl;
	THPIX ue_Pl, E_Pl;
	THPIX v_Pl, w_Pl;
	THPIX V_Pl, W_Pl;
	THPIX mcount_Pl;
	THPIX counts_Pl;
	THPIX flux_Pl, mag_Pl;
*/
	/* standard errors */
/* 
	THPIX IErr_Pl, JErr_Pl;
	THPIX xcErr_Pl, ycErr_Pl;
	THPIX reErr_Pl, eErr_Pl, phiErr_Pl, nErr_Pl;
	THPIX aErr_Pl, bErr_Pl, cErr_Pl;
	THPIX ueErr_Pl, EErr_Pl;
	THPIX vErr_Pl, wErr_Pl;
	THPIX VErr_Pl, WErr_Pl;
	THPIX countsErr_Pl;
	THPIX fluxErr_Pl, magErr_Pl;
*/
	/* sersic component */
	THPIX I_sersic, J_sersic;
	THPIX xc_sersic, yc_sersic;
	THPIX re_sersic, e_sersic, phi_sersic;
	THPIX k_sersic, n_sersic, N_sersic;
	THPIX a_sersic, b_sersic, c_sersic;
	THPIX ue_sersic, E_sersic;
	THPIX v_sersic, w_sersic;
	THPIX V_sersic, W_sersic;
	THPIX mcount_sersic;
	THPIX counts_sersic;
	THPIX flux_sersic, mag_sersic;

	/* standard errors */
	THPIX IErr_sersic, JErr_sersic;
	THPIX xcErr_sersic, ycErr_sersic;
	THPIX reErr_sersic, eErr_sersic, phiErr_sersic;
	THPIX kErr_sersic, nErr_sersic, NErr_sersic;
	THPIX aErr_sersic, bErr_sersic, cErr_sersic;
	THPIX ueErr_sersic, EErr_sersic;
	THPIX vErr_sersic, wErr_sersic;
	THPIX VErr_sersic, WErr_sersic;
	THPIX countsErr_sersic;
	THPIX fluxErr_sersic, magErr_sersic;

	/* first (inner) sersic component */
	THPIX I_sersic1, J_sersic1;
	THPIX xc_sersic1, yc_sersic1;
	THPIX re_sersic1, e_sersic1, phi_sersic1;
	THPIX k_sersic1, n_sersic1, N_sersic1;
	THPIX a_sersic1, b_sersic1, c_sersic1;
	THPIX ue_sersic1, E_sersic1;
	THPIX v_sersic1, w_sersic1;
	THPIX V_sersic1, W_sersic1;
	THPIX mcount_sersic1;
	THPIX counts_sersic1;
	THPIX flux_sersic1, mag_sersic1;

	/* standard errors */
	THPIX IErr_sersic1, JErr_sersic1;
	THPIX xcErr_sersic1, ycErr_sersic1;
	THPIX reErr_sersic1, eErr_sersic1, phiErr_sersic1;
	THPIX kErr_sersic1, nErr_sersic1, NErr_sersic1;
	THPIX aErr_sersic1, bErr_sersic1, cErr_sersic1;
	THPIX ueErr_sersic1, EErr_sersic1;
	THPIX vErr_sersic1, wErr_sersic1;
	THPIX VErr_sersic1, WErr_sersic1;
	THPIX countsErr_sersic1;
	THPIX fluxErr_sersic1, magErr_sersic1;

	
	/* second (outer) sersic component */
	THPIX I_sersic2, J_sersic2;
	THPIX xc_sersic2, yc_sersic2;
	THPIX re_sersic2, e_sersic2, phi_sersic2;
	THPIX k_sersic2, n_sersic2, N_sersic2;
	THPIX a_sersic2, b_sersic2, c_sersic2;
	THPIX ue_sersic2, E_sersic2;
	THPIX v_sersic2, w_sersic2;
	THPIX V_sersic2, W_sersic2;
	THPIX mcount_sersic2;
	THPIX counts_sersic2;
	THPIX flux_sersic2, mag_sersic2;

	/* standard errors */
	THPIX IErr_sersic2, JErr_sersic2;
	THPIX xcErr_sersic2, ycErr_sersic2;
	THPIX reErr_sersic2, eErr_sersic2, phiErr_sersic2;
	THPIX kErr_sersic2, nErr_sersic2, NErr_sersic2;
	THPIX aErr_sersic2, bErr_sersic2, cErr_sersic2;
	THPIX ueErr_sersic2, EErr_sersic2;
	THPIX vErr_sersic2, wErr_sersic2;
	THPIX VErr_sersic2, WErr_sersic2;
	THPIX countsErr_sersic2;
	THPIX fluxErr_sersic2, magErr_sersic2;

	/* coresersic component */
 
	THPIX I_coresersic, J_coresersic;
	THPIX xc_coresersic, yc_coresersic;
	THPIX re_coresersic, rb_coresersic;
	THPIX e_coresersic, phi_coresersic;
	THPIX k_coresersic, n_coresersic, N_coresersic;
	THPIX gamma_coresersic, delta_coresersic;
	THPIX G_coresersic, D_coresersic;
	THPIX a_coresersic, b_coresersic, c_coresersic;
	THPIX ue_coresersic, ub_coresersic, E_coresersic;
	THPIX v_coresersic, w_coresersic;
	THPIX V_coresersic, W_coresersic;
	THPIX mcount_coresersic;
	THPIX counts_coresersic;
	THPIX r50_coresersic, k50_coresersic; 
	THPIX flux_coresersic, mag_coresersic;

	/* standard errors */
	THPIX IErr_coresersic, JErr_coresersic;
	THPIX xcErr_coresersic, ycErr_coresersic;
	THPIX reErr_coresersic, rbErr_corsersic;
	THPIX eErr_coresersic, phiErr_coresersic;
	THPIX kErr_coresersic, nErr_coresersic, NErr_coresersic;
	THPIX gammaErr_coresersic, deltaErr_coresersic;
	THPIX GErr_coresersic, DErr_coresersic; 
	THPIX aErr_coresersic, bErr_coresersic, cErr_coresersic;
	THPIX ueErr_coresersic, ubErr_coresersic, EErr_coresersic;
	THPIX vErr_coresersic, wErr_coresersic;
	THPIX VErr_coresersic, WErr_coresersic;
	THPIX countsErr_coresersic;
	THPIX r50Err_coresersic, k50Err_coresersic; 
	THPIX fluxErr_coresersic, magErr_coresersic;
	
	/* second (outer) power-law component */
	/* 
	THPIX I_pl2, J_pl2;
	THPIX xc_pl2, yc_pl2;
	THPIX n_pl2;
	THPIX mcount_pl2;

	*/
	/* gaussian */

/* 
	THPIX I_gaussian, J_gaussian;
	THPIX a_gaussian, b_gaussian, c_gaussian;
	THPIX xc_gaussian, yc_gaussian;
	THPIX re_gaussian, e_gaussian, phi_gaussian;
	THPIX E_gaussian; 
	THPIX v_gaussian, w_gaussian;
	THPIX V_gaussian, W_gaussian;
	THPIX mcount_gaussian;
	THPIX counts_gaussian;
	THPIX flux_gaussian, mag_gaussian;
*/
	/* standard errors */	
 
/* 
	THPIX IErr_gaussian, JErr_gaussian;
	THPIX aErr_gaussian, bErr_gaussian, cErr_gaussian;
	THPIX xcErr_gaussian, ycErr_gaussian;
	THPIX reErr_gaussian, eErr_gaussian, phiErr_gaussian;
	THPIX EErr_gaussian; 
	THPIX vErr_gaussian, wErr_gaussian;
	THPIX VErr_gaussian, WErr_gaussian;
	THPIX countsErr_gaussian;
	THPIX fluxErr_gaussian, magErr_gaussian;
*/
	/* information for all moderls */
	/* accurate and crude calibration info
	THPIX kk, aa;
	THPIX flux20; 
	*/

	/* goodness of fit and deviation masures */
	int npar; /* number of object parameters */
	/*
  	number of masks initiated
	number of pixels used in each mask
  	radius of each mask 
  	rms count deviation as defined by Seiger, Graham, Jeijer 2007
  	portion of chi-squared value calculated in the respective mask 
	*/
 
	int nmask; 
	int npix_mask[NMASK_FITOBJC];
	THPIX radius_mask[NMASK_FITOBJC];
	THPIX delta_rms_mask[NMASK_FITOBJC]; 
	THPIX chisq_mask[NMASK_FITOBJC]; 

	/* chosen object specific chi-squared information */

	/* 
	THPIX rPetro_objc, rmask_objc;
	int npix_objc;
	THPIX chisq_objc, delta_rms_objc;
	*/

	/* 
	THPIX rPetroCoeff_objc;
	int indexPetro_objc;
	THPIX chisq_nu_objc;
	*/

	int  band;
} OBJCPARS;

GMODELPARS *thGmodelparsNew();
void thGmodelparsDel(GMODELPARS *q);
	
STARPARS *thStarparsNew();
void thStarparsDel(STARPARS *q);

DEVPARS *thDevparsNew();
void thDevparsDel(DEVPARS *dp);

EXPPARS *thExpparsNew();
void thExpparsDel(EXPPARS *q);

SERSICPARS *thSersicparsNew();
void thSersicparsDel(SERSICPARS *q);

CORESERSICPARS *thCoresersicparsNew();
void thCoresersicparsDel(CORESERSICPARS *q);

POWERLAWPARS *thPowerlawparsNew();
void thPowerlawparsDel(POWERLAWPARS *q);

GAUSSIANPARS *thGaussianparsNew();
void thGaussianparsDel(GAUSSIANPARS *q);

OBJCPARS *thObjcparsNew();
void thObjcparsDel(OBJCPARS *gp);

RET_CODE thDumpObjcparsWrite(FILE *fil, OBJCPARS *p, OBJ_TYPE sdsstype);
void simple_print_model_pars(void *q, char *qname);

#endif
