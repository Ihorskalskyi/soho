#include "thDebug.h"
#include "thProfile.h"

#include<math.h>
#include <stdlib.h>

extern double ddot_(int *, double [], int *, double [], int *);
extern float sdot_(int *, float [], int *, float [], int *);

static PROFILEFL **QuadNodes = NULL;
static PROFILEFL **QuadWeights = NULL;
static int mQuad = 0;

static int init_QOP = 0, init_mrow_QOP = 0, init_mcol_QOP = 0, init_m_QOP;
static PROFILEFL ***ww_QOP = NULL;
static PIXPOS *pixpos_QOP = NULL;

static RET_CODE thInitQuadOnePix(void);
static RET_CODE thDeInitQuadOnePix(void);

RET_CODE InitQuad() {

char *name = "InitQuad";
static int init = 0;

if (init) {
	thError("%s: WARNING - quadrature structures already initiated for (mQuad = %d)",
		name, mQuad);
	return(SH_SUCCESS);
}

mQuad = 30;
QuadNodes = thCalloc(mQuad, sizeof(PROFILEFL *));
QuadWeights = thCalloc(mQuad, sizeof(PROFILEFL *));
int i;
for (i = 0; i < mQuad; i++) {
	QuadNodes[i] = thCalloc(mQuad, sizeof(PROFILEFL));
	QuadWeights[i] = thCalloc(mQuad, sizeof(PROFILEFL));
	}

int n;
PROFILEFL *x;

n = 1; x = QuadNodes[n-1]  + (n-1) / 2;
x[0] = 0.0;

n = 2;  x = QuadNodes[n-1] + (n-1) / 2;
x[1] = 0.577350269189626;

n = 3;  x = QuadNodes[n-1] + (n-1) / 2;
x[0] = 0.000000000000000;
x[1] = 0.774596669241483; 

n = 4;  x = QuadNodes[n-1] + (n-1) / 2;
x[1] = 0.339981043584856;
x[2] = 0.861136311594053;

n = 5;  x = QuadNodes[n-1] + (n-1) / 2;
x[0] = 0.000000000000000; 
x[1] = 0.538469310105683; 
x[2] = 0.906179845938664;

n = 6;  x = QuadNodes[n-1] + (n-1) / 2;
x[1] = 0.238619186083197;
x[2] = 0.661209386466265;
x[3] = 0.932469514203152;

n = 7;  x = QuadNodes[n-1] + (n-1) / 2;
x[0] = 0.000000000000000;
x[1] = 0.405845151377397;
x[2] = 0.741531185599394;
x[3] = 0.949107912342759;

n = 8;  x = QuadNodes[n-1] + (n-1) / 2;
x[1] = 0.183434642495650;
x[2] = 0.525532409916329; 
x[3] = 0.796666477413627; 
x[4] = 0.960289856497536;

n = 9;  x = QuadNodes[n-1] + (n-1) / 2; 
x[0] = 0.000000000000000; 
x[1] = 0.324253423403809; 
x[2] = 0.613371432700590; 
x[3] = 0.836031107326636; 
x[4] = 0.968160239507626;

n = 10;  x = QuadNodes[n-1] + (n-1) / 2;
x[1] = 0.148874338981631; 
x[2] = 0.433395394129247; 
x[3] = 0.679409568299024; 
x[4] = 0.865063366688985; 
x[5] = 0.973906528517172;

n = 11;  x = QuadNodes[n-1] + (n-1) / 2;
x[0] = 0.000000000000000;
x[1] = 0.269543155952345; 
x[2] = 0.519096129110681; 
x[3] = 0.730152005574049;
x[4] = 0.887062599768095; 
x[5] = 0.978228658146057;

n = 12;  x = QuadNodes[n-1] + (n-1) / 2;
x[1] = 0.125333408511469; 
x[2] = 0.367831498918180; 
x[3] = 0.587317954286617; 
x[4] = 0.769902674194305; 
x[5] = 0.904117256370475; 
x[6] = 0.981560634246719;

n = 13;  x = QuadNodes[n-1] + (n-1) / 2;
x[0] = 0.000000000000000; 
x[1] = 0.230458315955135; 
x[2] = 0.448492751036447; 
x[3] = 0.642349339440340;
x[4] = 0.801578090733310; 
x[5] = 0.917598399222978; 
x[6] = 0.984183054718588;

n = 14;  x = QuadNodes[n-1] + (n-1) / 2;
x[1] = 0.108054948707344; 
x[2] = 0.319112368927890; 
x[3] = 0.515248636358154;
x[4] = 0.687292904811685; 
x[5] = 0.827201315069765; 
x[6] = 0.928434883663574; 
x[7] = 0.986283808696812;

n = 15;  x = QuadNodes[n-1] + (n-1) / 2;
x[0] = 0.000000000000000; 
x[1] = 0.201194093997435; 
x[2] = 0.394151347077563;
x[3] = 0.570972172608539; 
x[4] = 0.724417731360170;
x[5] = 0.848206583410427; 
x[6] = 0.937273392400706; 
x[7] = 0.987992518020485;

n = 16;  x = QuadNodes[n-1] + (n-1) / 2;
x[1] = 0.095012509837637; 
x[2] = 0.281603550779259; 
x[3] = 0.458016777657227;
x[4] = 0.617876244402644; 
x[5] = 0.755404408355003; 
x[6] = 0.865631202387832;
x[7] = 0.944575023073233; 
x[8] = 0.989400934991650;

PROFILEFL *a;
n = 1;  a = QuadWeights[n-1] + (n-1) / 2;
a[0] = 2.0;

n = 2;  a = QuadWeights[n-1] + (n-1) / 2;
a[1] = 1.000000000000000;

n = 3;  a = QuadWeights[n-1] + (n-1) / 2;
a[0] = 0.888888888888889;
a[1] = 0.555555555555556;

n = 4;  a = QuadWeights[n-1] + (n-1) / 2;
a[1] = 0.652145154862546;
a[2] = 0.347854845137454;

n = 5;  a = QuadWeights[n-1] + (n-1) / 2;
a[0] = 0.568888888888889;
a[1] = 0.478628670499366; 
a[2] = 0.236926885056189;

n = 6;  a = QuadWeights[n-1] + (n-1) / 2;
a[1] = 0.467913934572691; 
a[2] = 0.360761573048139; 
a[3] = 0.171324492379170;

n = 7;  a = QuadWeights[n-1] + (n-1) / 2;
a[0] = 0.417959183673469;
a[1] = 0.381830050505119;
a[2] = 0.279705391489277; 
a[3] = 0.129484966168870;

n = 8;  a = QuadWeights[n-1] + (n-1) / 2;
a[1] = 0.362683783378362;
a[2] = 0.313706645877887;
a[3] = 0.222381034453374; 
a[4] = 0.101228536290376;

n = 9;  a = QuadWeights[n-1] + (n-1) / 2;
a[0] = 0.330239355001260; 
a[1] = 0.312347077040003; 
a[2] = 0.260610696402935; 
a[3] = 0.180648160694857; 
a[4] = 0.081274388361574;

n = 10;  a = QuadWeights[n-1] + (n-1) / 2;
a[1] = 0.295524224714753;
a[2] = 0.269266719309996;
a[3] = 0.219086362515982;
a[4] = 0.149451349150581;
a[5] = 0.066671344308688;

n = 11;  a = QuadWeights[n-1] + (n-1) / 2;
a[0] = 0.272925086777901;
a[1] = 0.262804544510247;
a[2] = 0.233193764591990;
a[3] = 0.186290210927734;
a[4] = 0.125580369464905;
a[5] = 0.055668567116174;

n = 12;  a = QuadWeights[n-1] + (n-1) / 2;
a[1] = 0.249147045813403;
a[2] = 0.233492536538355; 
a[3] = 0.203167426723066; 
a[4] = 0.160078328543346; 
a[5] = 0.106939325995318;
a[6] = 0.047175336386512;


n = 13;  a = QuadWeights[n-1] + (n-1) / 2;
a[0] = 0.232551553230874;
a[1] = 0.226283180262897;
a[2] = 0.207816047536889;
a[3] = 0.178145980761946;
a[4] = 0.138873510219787;
a[5] = 0.092121499837728;
a[6] = 0.040484004765316;

n = 14;  a = QuadWeights[n-1] + (n-1) / 2;
a[1] = 0.215263853463158;
a[2] = 0.205198463721290; 
a[3] = 0.185538397477938; 
a[4] = 0.157203167158194;
a[5] = 0.121518570687903;
a[6] = 0.080158087159760;
a[7] = 0.035119460331752;

n = 15;  a = QuadWeights[n-1] + (n-1) / 2;
a[0] = 0.202578241925561; 
a[1] = 0.198431485327111;
a[2] = 0.186161000015562; 
a[3] = 0.166269205816994;
a[4] = 0.139570677926154; 
a[5] = 0.107159220467172; 
a[6] = 0.070366047488108; 
a[7] = 0.030753241996117;

n = 16;  a = QuadWeights[n-1] + (n-1) / 2;
a[1] = 0.189450610455069;
a[2] = 0.182603415044924;
a[3] = 0.169156519395003;
a[4] = 0.149595988816577;
a[5] = 0.124628971255534;
a[6] = 0.095158511682493;
a[7] = 0.062253523938648;
a[8] = 0.027152459411754;


n = 17;  
x = QuadNodes[n-1] + (n-1) / 2;
a = QuadWeights[n-1] + (n-1) / 2;
x[0] =	0;
a[0] =	0.1794464703562065254583;
x[1] =	0.1784841814958478558507;
a[1] =	0.1765627053669926463253;
x[2] =	0.3512317634538763152972;
a[2] =	0.16800410215645004451;
x[3] =	0.5126905370864769678863;
a[3] =	0.1540457610768102880814;
x[4] =	0.6576711592166907658503;
a[4] =	0.1351363684685254732863;
x[5] =	0.7815140038968014069252;
a[5] =	0.111883847193403971095;
x[6] =	0.880239153726985902123;
a[6] =	0.0850361483171791808835;
x[7] =	0.9506755217687677612227;
a[7] =	0.055459529373987201129;
x[8] =	0.9905754753144173356754;
a[8] =	0.0241483028685479319601;


n = 18;  
x = QuadNodes[n-1] + (n-1) / 2;
a = QuadWeights[n-1] + (n-1) / 2;

x[1] = 0.0847750130417353012423;
x[2] = 0.251886225691505509589;
x[3] = 0.4117511614628426460359;
x[4] = 0.5597708310739475346079;
x[5] = 0.6916870430603532078749;
x[6] = 0.803704958972523115682;
x[7] = 0.8926024664975557392061;
x[8] = 0.9558239495713977551812;
x[9] = 0.99156516842093094673;

a[1] = 0.169142382963143591841;
a[2] = 0.164276483745832722986;
a[3] = 0.1546846751262652449254;
a[4] = 0.140642914670650651205;
a[5] = 0.1225552067114784601845;
a[6] = 0.100942044106287165563;
a[7] = 0.0764257302548890565291;
a[8] = 0.0497145488949697964533;
a[9] = 0.0216160135264833103133;


n = 19;  
x = QuadNodes[n-1] + (n-1) / 2;
a = QuadWeights[n-1] + (n-1) / 2;


x[0] = 0;
x[1] = 0.1603586456402253758681;
x[2] = 0.3165640999636298319901;
x[3] = 0.4645707413759609457173;
x[4] = 0.6005453046616810234696;
x[5] = 0.7209661773352293786171;
x[6] = 0.8227146565371428249789;
x[7] = 0.9031559036148179016427;
x[8] = 0.9602081521348300308528;
x[9] = 0.992406843843584403189;


a[0] = 0.1610544498487836959792;
a[1] = 0.15896884339395434765;
a[2] = 0.1527660420658596667789;
a[3] = 0.1426067021736066117758;
a[4] = 0.1287539625393362276755;
a[5] = 0.111566645547333994716;
a[6] = 0.091490021622449999464;
a[7] = 0.0690445427376412265807;
a[8] = 0.0448142267656996003328;
a[9] = 0.0194617882297264770363;

n = 20;  
x = QuadNodes[n-1] + (n-1) / 2;
a = QuadWeights[n-1] + (n-1) / 2;


x[1] = 0.0765265211334973337546;
x[2] = 0.2277858511416450780805;
x[3] = 0.3737060887154195606726;
x[4] = 0.5108670019508270980044;
x[5] = 0.6360536807265150254528;
x[6] = 0.7463319064601507926143;
x[7] = 0.8391169718222188233945;
x[8] = 0.9122344282513259058678;
x[9] = 0.9639719272779137912677;
x[10] = 0.9931285991850949247861;


a[1] = 0.152753387130725850698;
a[2] = 0.149172986472603746788;
a[3] = 0.142096109318382051329;
a[4] = 0.1316886384491766268985;
a[5] = 0.1181945319615184173124;
a[6] = 0.101930119817240435037;
a[7] = 0.083276741576704748725;
a[8] = 0.0626720483341090635695;
a[9] = 0.040601429800386941331;
a[10] = 0.0176140071391521183119;


n = 21;  
x = QuadNodes[n-1] + (n-1) / 2;
a = QuadWeights[n-1] + (n-1) / 2;

x[0] = 0;
x[1] = 0.145561854160895090937;
x[2] = 0.2880213168024010966008;
x[3] = 0.4243421202074387835737;
x[4] = 0.551618835887219807059;
x[5] = 0.667138804197412319306;
x[6] = 0.7684399634756779086159;
x[7] = 0.8533633645833172836473;
x[8] = 0.9200993341504008287902;
x[9] = 0.9672268385663062943166;
x[10] = 0.9937521706203895002602;


a[0] = 0.146081133649690427192;
a[1] = 0.1445244039899700590638;
a[2] = 0.1398873947910731547221;
a[3] = 0.1322689386333374617811;
a[4] = 0.1218314160537285341954;
a[5] = 0.108797299167148377663;
a[6] = 0.0934444234560338615533;
a[7] = 0.0761001136283793020171;
a[8] = 0.057134425426857208284;
a[9] = 0.0369537897708524938;
a[10] = 0.016017228257774333324;

n = 22;  
x = QuadNodes[n-1] + (n-1) / 2;
a = QuadWeights[n-1] + (n-1) / 2;

x[1] = 0.0697392733197222212138;
x[2] = 0.2078604266882212854789;
x[3] = 0.341935820892084225158;
x[4] = 0.4693558379867570264063;
x[5] = 0.587640403506911592959;
x[6] = 0.6944872631866827800507;
x[7] = 0.7878168059792081620043;
x[8] = 0.8658125777203001365364;
x[9] = 0.9269567721871740005207;
x[10] = 0.970060497835428727124;
x[11] = 0.994294585482399292073;


a[1] = 0.139251872855631993375;
a[2] = 0.136541498346015171353;
a[3] = 0.131173504787062370733;
a[4] = 0.1232523768105124242856;
a[5] = 0.112932296080539218393;
a[6] = 0.1004141444428809649321;
a[7] = 0.0859416062170677274144;
a[8] = 0.069796468424520488095;
a[9] = 0.0522933351526832859403;
a[10] = 0.0337749015848141547933;
a[11] = 0.014627995298272200685;

n = 23;  
x = QuadNodes[n-1] + (n-1) / 2;
a = QuadWeights[n-1] + (n-1) / 2;

x[0] = 0;
x[1] = 0.1332568242984661109317;
x[2] = 0.2641356809703449305339;
x[3] = 0.3903010380302908314215;
x[4] = 0.50950147784600754969;
x[5] = 0.619609875763646156385;
x[6] = 0.7186613631319501944616;
x[7] = 0.8048884016188398921511;
x[8] = 0.8767523582704416673782;
x[9] = 0.9329710868260161023492;
x[10] = 0.972542471218115231956;
x[11] = 0.9947693349975521235239;

a[0] = 0.1336545721861061753515;
a[1] = 0.132462039404696617372;
a[2] = 0.1289057221880821499786;
a[3] = 0.1230490843067295304676;
a[4] = 0.1149966402224113649416;
a[5] = 0.104892091464541410074;
a[6] = 0.092915766060035147477;
a[7] = 0.0792814117767189549229;
a[8] = 0.064232421408525852127;
a[9] = 0.048037671731084668572;
a[10] = 0.0309880058569794443107;
a[11] = 0.013411859487141772081;

n = 24;  
x = QuadNodes[n-1] + (n-1) / 2;
a = QuadWeights[n-1] + (n-1) / 2;


x[1] = 	0.06405689286260560000;
x[2] = 	0.19111886747361600000;
x[3] = 	0.31504267969616300000;
x[4] = 	0.43379350762604500000;
x[5] = 	0.54542147138883900000;
x[6] = 	0.64809365193697500000;
x[7] = 	0.74012419157855400000;
x[8] = 	0.82000198597390200000;
x[9] = 	0.88641552700440100000;
x[10] = 	0.93827455200273200000;
x[11] = 	0.97472855597130900000;
x[12] = 	0.99518721999702100000;



a[1] = 	0.12793819534675200000;
a[2] = 	0.12583745634682800000;
a[3] = 	0.12167047292780300000;
a[4] = 	0.11550566805372500000;
a[5] = 	0.10744427011596500000;
a[6] = 	0.09761865210411380000;
a[7] = 	0.08619016153195320000;
a[8] = 	0.07334648141108030000;
a[9] = 	0.05929858491543670000;
a[10] =	0.04427743881741980000;
a[11] = 	0.02853138862893360000;
a[12] = 	0.01234122979998710000;

n = 25;
x = QuadNodes[n-1] + (n-1) / 2;
a = QuadWeights[n-1] + (n-1) / 2;

x[0] = 	0;
x[1] = 	0.12286469261071000000;
x[2] = 	0.24386688372098800000;
x[3] = 	0.36117230580938700000;
x[4] = 	0.47300273144571400000;
x[5] = 	0.57766293024122200000;
x[6] = 	0.67356636847346800000;
x[7] = 	0.75925926303735700000;
x[8] = 	0.83344262876083400000;
x[9] = 	0.89499199787827500000;
x[10] = 	0.94297457122897400000;
x[11] = 	0.97666392145951700000;
x[12] = 	0.99555696979049800000;


a[0] = 	0.12317605372671500000;
a[1] = 	0.12224244299031000000;
a[2] = 	0.11945576353578400000;
a[3] = 	0.11485825914571100000;
a[4] = 	0.10851962447426300000;
a[5] = 	0.10053594906705000000;
a[6] = 	0.09102826198296360000;
a[7] = 	0.08014070033500100000;
a[8] = 	0.06803833381235690000;
a[9] = 	0.05490469597583510000;
a[10] =	0.04093915670130630000;
a[11] = 	0.02635498661503210000;
a[12] = 	0.01139379850102620000;

n = 26;
x = QuadNodes[n-1] + (n-1) / 2;
a = QuadWeights[n-1] + (n-1) / 2;

x[1] = 	0.05923009342931320000;
x[2] = 	0.17685882035689000000;
x[3] = 	0.29200483948595600000;
x[4] = 	0.40305175512348600000;
x[5] = 	0.50844071482450500000;
x[6] = 	0.60669229301761800000;
x[7] = 	0.69642726041995700000;
x[8] = 	0.77638594882067800000;
x[9] = 	0.84544594278849800000;
x[10] = 	0.90263786198430700000;
x[11] = 	0.94715906666171400000;
x[12] = 	0.97838544595647000000;
x[13] = 	0.99588570114561600000;

a[1] = 	0.11832141527926200000;
a[2] = 	0.11666044348529600000;
a[3] = 	0.11336181654631900000;
a[4] = 	0.10847184052857600000;
a[5] = 	0.10205916109442500000;
a[6] = 	0.09421380035591410000;
a[7] = 	0.08504589431348520000;
a[8] = 	0.07468414976565970000;
a[9] = 	0.06327404632957480000;
a[10] =	0.05097582529714780000;
a[11] = 	0.03796238329436270000;
a[12] = 	0.02441785109263190000;
a[13] = 	0.01055137261734300000;

n = 27;
x = QuadNodes[n-1] + (n-1) / 2;
a = QuadWeights[n-1] + (n-1) / 2;

x[0] = 	0;
x[1] = 	0.11397258560952900000;
x[2] = 	0.22645936543953600000;
x[3] = 	0.33599390363850800000;
x[4] = 	0.44114825175002600000;
x[5] = 	0.54055156457945600000;
x[6] = 	0.63290797194649500000;
x[7] = 	0.71701347373942300000;
x[8] = 	0.79177163907050800000;
x[9] = 	0.85620790801829400000;
x[10] = 	0.90948232067749100000;
x[11] = 	0.95090055781470500000;
x[12] = 	0.97992347596150100000;
x[13] = 	0.99617926288898800000;

a[0] = 	0.11422086737895600000;
a[1] = 	0.11347634610896500000;
a[2] = 	0.11125248835684500000;
a[3] = 	0.10757828578853300000;
a[4] = 	0.10250163781774500000;
a[5] = 	0.09608872737002850000;
a[6] = 	0.08842315854375690000;
a[7] = 	0.07960486777305770000;
a[8] = 	0.06974882376624550000;
a[9] = 	0.05898353685983350000;
a[10] =	0.04744941252061500000;
a[11] = 	0.03529705375741970000;
a[12] = 	0.02268623159618060000;
a[13] = 	0.00979899605129430000;

n = 28;
x = QuadNodes[n-1] + (n-1) / 2;
a = QuadWeights[n-1] + (n-1) / 2;

x[1] = 	0.05507928988403420000;
x[2] = 	0.16456928213338000000;
x[3] = 	0.27206162763517800000;
x[4] = 	0.37625151608907800000;
x[5] = 	0.47587422495511800000;
x[6] = 	0.56972047181140100000;
x[7] = 	0.65665109403886400000;
x[8] = 	0.73561087801363100000;
x[9] = 	0.80564137091717900000;
x[10] = 	0.86589252257439500000;
x[11] = 	0.91563302639213200000;
x[12] = 	0.95425928062893800000;
x[13] = 	0.98130316537087200000;
x[14] = 	0.99644249757395400000;

a[1] = 	0.11004701301647500000;
a[2] = 	0.10871119225829400000;
a[3] = 	0.10605576592284600000;
a[4] = 	0.10211296757806000000;
a[5] = 	0.09693065799792990000;
a[6] = 	0.09057174439303280000;
a[7] = 	0.08311341722890120000;
a[8] = 	0.07464621423456870000;
a[9] = 	0.06527292396699950000;
a[10] =	0.05510734567571670000;
a[11] = 	0.04427293475900420000;
a[12] = 	0.03290142778230430000;
a[13] = 	0.02113211259277120000;
a[14] = 	0.00912428259309451000;


n = 29;
x = QuadNodes[n-1] + (n-1) / 2;
a = QuadWeights[n-1] + (n-1) / 2;

x[0] = 	0;
x[1] = 	0.10627823013267900000;
x[2] = 	0.21135228616600100000;
x[3] = 	0.31403163786763900000;
x[4] = 	0.41315288817400800000;
x[5] = 	0.50759295512422700000;
x[6] = 	0.59628179713822700000;
x[7] = 	0.67821453760268600000;
x[8] = 	0.75246285173447700000;
x[9] = 	0.81818548761525200000;
x[10] = 	0.87463780492010200000;
x[11] = 	0.92118023295305800000;
x[12] = 	0.95728559577808700000;
x[13] = 	0.98254550526141300000;
x[14] = 	0.99667944226059600000;

a[0] = 	0.10647938171831400000;
a[1] = 	0.10587615509732000000;
a[2] = 	0.10407331007772900000;
a[3] = 	0.10109127375991400000;
a[4] = 	0.09696383409440860000;
a[5] = 	0.09173775713925870000;
a[6] = 	0.08547225736617250000;
a[7] = 	0.07823832713576370000;
a[8] = 	0.07011793325505120000;
a[9] = 	0.06120309065707910000;
a[10] =	0.05159482690249790000;
a[11] = 	0.04140206251868280000;
a[12] = 	0.03074049220209360000;
a[13] = 	0.01973208505612270000;
a[14] = 	0.00851690387874640000;

n = 30;
x = QuadNodes[n-1] + (n-1) / 2;
a = QuadWeights[n-1] + (n-1) / 2;

x[1] = 	0.05147184255531760000;
x[2] = 	0.15386991360858300000;
x[3] = 	0.25463692616788900000;
x[4] = 	0.35270472553087800000;
x[5] = 	0.44703376953808900000;
x[6] = 	0.53662414814201900000;
x[7] = 	0.62052618298924200000;
x[8] = 	0.69785049479331500000;
x[9] = 	0.76777743210482600000;
x[10] = 	0.82956576238276800000;
x[11] = 	0.88256053579205200000;
x[12] = 	0.92620004742927400000;
x[13] = 	0.96002186496830700000;
x[14] = 	0.98366812327974700000;
x[15] = 	0.99689348407464900000;

a[1] = 	0.10285265289355800000;
a[2] = 	0.10176238974840500000;
a[3] = 	0.09959342058679520000;
a[4] = 	0.09636873717464420000;
a[5] = 	0.09212252223778610000;
a[6] = 	0.08689978720108290000;
a[7] = 	0.08075589522942020000;
a[8] = 	0.07375597473770520000;
a[9] = 	0.06597422988218040000;
a[10] =	0.05749315621761900000;
a[11] = 	0.04840267283059400000;
a[12] = 	0.03879919256962700000;
a[13] = 	0.02878470788332330000;
a[14] = 	0.01846646831109090000;
a[15] = 	0.00796819249616660000;

int j;
for (i = 0; i < mQuad; i++) {
	a = QuadWeights[i];
	x = QuadNodes[i];
	for (j = 0; j < (i + 1) / 2; j++) {
		a[j] =  a[i - j];
		x[j] = -x[i - j];
	}
}

/* 2D quadrature */
RET_CODE status;
status = thInitQuadOnePix();
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not initiate 'QuadOnePix'", name);
	return(status);
}

init = 1;
printf("%s: quadrature structure initiated for (mQuad = %d) \n", name, mQuad);

return(SH_SUCCESS);

}

RET_CODE SquareCreateProfile(THPIX (*f)(PIXPOS *, void *), void *q, char *type,  
			REGION *reg, 
			int idr, int idc,
			QUADRATURE **Q, int nQ) {
char *name = "SquareCreateProfile";

if (f == NULL) {
	thError("%s: ERROR - null pixel functions", name);
	return(SH_GENERIC_ERROR);
	}
if (reg == NULL) {
	thError("%s: ERROR - null placeholder for (reg)", name);
	return(SH_GENERIC_ERROR);
}
if (reg->nrow == 0 || reg->ncol == 0) {
	thError("%s: ERROR - empty region (nrow, ncol) = (%d, %d)", 
	name, reg->nrow, reg->ncol);
	return(SH_GENERIC_ERROR);
}

/* setting up the order of quadrature to be done */
static int snQE = 0;
static QUADRATURE **swQE = NULL, **sQE = NULL;

int nQE = 0;
QUADRATURE **QE = NULL, **wQE = NULL;
if (nQ <= 0 || Q == NULL) {
	if (sQE == NULL) {
		InitQuad();
		sQE = SetProfileQuadrature(&snQE);
	}
	QE = sQE;
	nQE = snQE;
} else {
  	QE = Q;
  	nQE = nQ;     
}
if (swQE != QE || snQE != nQE) {
	if (swQE != NULL) {
		int i;	
		for (i = 0; i < snQE; i++) {
			thQuadratureDel(swQE[i]);
		}
		 thFree(swQE);
	}
	swQE = NULL;
	snQE = 0;
}

if (swQE == NULL) {
	/* copy QE onto sQE */
	swQE = thCalloc(nQE, sizeof(QUADRATURE*));
	thQuadCopyArray(swQE, QE, nQE);
	snQE = nQE;
} 
wQE = swQE;

/* setting up the null rectangle and the variables to be used later */

#if DEBUG_PROFILE
int npix = 0;
#endif

int nrow, ncol;
nrow = reg->nrow;
ncol = reg->ncol;

static PIXPOS *pixpos = NULL, *pixpos2 = NULL;
if (pixpos == NULL) pixpos = thPixposNew();
if (pixpos2 == NULL) pixpos2 = thPixposNew();

static NAIVE_MASK *om = NULL, *rec_om = NULL, *n_square_om = NULL, *old_square_om = NULL;
if (om == NULL) om = thNaiveMaskNew();
if (rec_om == NULL) rec_om = thNaiveMaskNew();
if (n_square_om == NULL) n_square_om = thNaiveMaskNew();
if (old_square_om == NULL) old_square_om = thNaiveMaskNew();

/* reseting static masks in case they are used before*/
thNaiveMaskReset(om);
thNaiveMaskReset(n_square_om);
thNaiveMaskReset(old_square_om);
thNaiveMaskReset(rec_om);

int l = 0;
THPIX radius;
RET_CODE status;
status = thNaiveMaskFromRect(rec_om, 0, ncol - 1, 0, nrow - 1);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not set the master rectangle mask", name);
	return(status);
}

radius = (THPIX) PROFILE_INIT_RADIUS;
radius = (int) radius + 0.5;
THPIX rowc, colc;
status  = thVariableGetCenter(q, type, &rowc, &colc);
if (status != SH_SUCCESS) {
	thError("%s: WARNING - (rowc) and (colc) could not be calculated for object (%s)", name, type); 
	colc = (THPIX) 0.0;
	rowc = (THPIX) 0.0;
}
status = thNaiveMaskFromRect(om, (int) (colc - radius) + idc, 
			(int) (colc + radius) + idc, 
			(int) (rowc - radius) + idr, 
			(int) (rowc + radius) + idr);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not set the inner rectangle mask for (l = %d)",
		name, l);
	return(status);
}
status = thNaiveMaskAndNaiveMask(om, rec_om);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the intersection between inner rectangle mask at (l = %d) and the master rectangle", name, l);
	return(status);
}
status = thNaiveMaskCopy(n_square_om, om);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make a copy of the integration rectangle at (l = %d)", name, l);
	return(status);
}

int nqe;
int iqe = 0, s, i, i1, i2, j, j1, j2, r, m;
QUAD_CONDITION qneeded;
THPIX *row;
/* moved to the inside of the loop */
nqe = nQE;

#if DEEP_DEBUG_PROFILE
printf("%s: nspan = ", name);
#endif

while ((qneeded = IsQuadNeeded_SQ(om, QE[0])) == CONTINUE_QUADRATURE) {

	nqe = nQE;

	int nspan;
	status = thNaiveMaskGetNspan(om, &nspan);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get the number of span rectangles for integeration region at (l = %d)", name, l);
		return(status);
	}
	#if DEEP_DEBUG_PROFILE 
	printf("%d, ", nspan);
	#endif
	
	/* lqe - limits the number of Q-calcs to less than nqe */
	int lqe;
	/* 
	lqe = (nqe + 1) - MAX((794 * (nqe + 1)) / 1000, 1);
	*/
	lqe = nqe;
	/* loop over pixels */
	for (s = 0; s < nspan; s++) {
	status = thNaiveMaskGetElement(om, s, &j1, &j2, &i1, &i2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get the ring element corrdinates for span (%d) of integration region (l = %d)", name, s, l);
		return(status);
	}
	for (i = i1; i <= i2; i++) {
	row = reg->rows_thpix[i];
	for (j = j1; j <= j2; j++) {
			#if DEBUG_PROFILE
			npix++;
			#endif
			/* loop over quadratures */
			for (r = 0; r < lqe; r++) {
			thPixposSet(pixpos, (THPIX) (i - idr), (THPIX) (j - idc));	
			thQuadOnePix(f, pixpos, q, QE[r]);	
			}

			iqe = ChooseWorstAcceptableQuad(QE, lqe);	
			/* this seems to be source of a problem which affects the time profiling
			Feb 7, 2012
			nqe -= iqe;
			*/
			lqe = iqe + 1;
			nqe = iqe + 1;
			row[j] = QE[0]->val;

		}
		}
	}
	/* now decide what is the next quadrature rule that should be used */
	//iqe = ChooseBestQuad(wQE, nqe);
	
	nQE -= iqe;
	wQE += iqe;
	QE  += iqe;
        /* this seems to be causing the problem
	nqe = MIN(nqe, nQE);
	*/
	status = thNaiveMaskReset(om);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not reset integration region's mask", name);
		return(status);
	}
	status = thNaiveMaskCopy(old_square_om, n_square_om);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not make copy of new mask onto old mask", 
			name);	
		return(status);
	}
	/* creating the next circle of integration */
	m = MIN(wQE[0]->mrow, wQE[0]->mcol);
	radius = thGetNextQuadRadius(radius, m, 0.0, PROFILE_RATIO);
	radius = (THPIX) ((int) (radius + 0.5));
	status = thNaiveMaskFromRect(n_square_om, (int) (colc - radius) + idc, 
				(int) (colc + radius) + idc, 
				(int) (rowc - radius) + idr, 
				(int) (rowc + radius) + idr);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not generate new rectangle after integrating region (l = %d)", name, l);
		return(status);
	}
	status = thNaiveMaskNotIntersectionNaiveMask(om, n_square_om, old_square_om);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get the ring for integration after region (l = %d)", name, l);
		return(status);
	}
	status = thNaiveMaskAndNaiveMask(om, rec_om);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not calculate the intersection of new integration ring and master rectangle after (l = %d)", name, l);
		return(status);
	}
	l++;
}

#if DEEP_DEBUG_PROFILE
printf("\n");
#endif

if (qneeded == UNKNOWN_QUAD_CONDITION) {
	thError("%s: ERROR - returned the Q-loop with unknown exit condition", name);
	return(SH_GENERIC_ERROR);
}
if (qneeded == NO_QUADRATURE_NEEDED) {
/* now filling the rest of the image */
	int nspan;
	status = thNaiveMaskAndNotNaiveMask(rec_om, old_square_om);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not define exterior to quadrature region", name);
		return(status);
	}
	status = thNaiveMaskGetNspan(rec_om, &nspan);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get the number of span rectangles for integeration region at (l = %d)", name, l);
		return(status);
	}
	/* loop over pixels */
	for (s = 0; s < nspan; s++) {
		/* the following assumes that the spanmask has no parent */
		status = thNaiveMaskGetElement(rec_om, s, &j1, &j2, &i1, &i2);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get the ring element corrdinates for span (%d) of integration region (l = %d)", name, s, l);
			return(status);
		}
		for (i = i1; i <= i2; i++) {
		row = reg->rows_thpix[i];
			for (j = j1; j <= j2; j++) {
			#if DEBUG_PROFILE
			npix++;
			#endif
			thPixposSet(pixpos, 
				(THPIX) (i - idr + 0.5), (THPIX) (j - idc + 0.5));
			row[j] = f(pixpos, q);
			}
		}
	}
}

/* setting up the location of the integrated region with respect to the main image*/
reg->row0 = -idr;
reg->col0 = -idc;

#if DEEP_DEBUG_PROFILE
printf("%s: integrated (%d) pixels out of (%d) -- f = %g \n", name, npix, nrow * ncol, (float) npix / (float) (nrow * ncol));
printf("%s: nrow = %d, r_f = %g \n", name, nrow, radius);
#endif

/* returning success */
return(SH_SUCCESS);
}

RET_CODE CircleCreateProfile(THPIX (*f)(PIXPOS *, void *), void *q, char *type,  
			REGION *reg, 
			int idr, int idc,
			QUADRATURE **Q, int nQ) {
char *name = "CircleCreateProfile";

if (f == NULL) {
	thError("%s: ERROR - null pixel functions", name);
	return(SH_GENERIC_ERROR);
	}
if (reg == NULL) {
	thError("%s: ERROR - null placeholder for (reg)", name);
	return(SH_GENERIC_ERROR);
}
if (reg->nrow == 0 || reg->ncol == 0) {
	thError("%s: ERROR - empty region (nrow, ncol) = (%d, %d)", 
	name, reg->nrow, reg->ncol);
	return(SH_GENERIC_ERROR);
}

/* setting up the order of quadrature to be done */
static int snQE = 0;
static QUADRATURE **swQE = NULL, **sQE = NULL;

int nQE = 0;
QUADRATURE **QE = NULL, **wQE = NULL;
if (nQ <= 0 || Q == NULL) {
	if (sQE == NULL) {
		InitQuad();
		sQE = SetProfileQuadrature(&snQE);
	}
	QE = sQE;
	nQE = snQE;
} else {
  	QE = Q;
  	nQE = nQ;     
}
if (swQE != QE || snQE != nQE) {
	if (swQE != NULL) {
		int i;	
		for (i = 0; i < snQE; i++) {
			thQuadratureDel(swQE[i]);
		}
		 thFree(swQE);
	}
	swQE = NULL;
	snQE = 0;
}

if (swQE == NULL) {
	/* copy QE onto sQE */
	swQE = thCalloc(nQE, sizeof(QUADRATURE*));
	thQuadCopyArray(swQE, QE, nQE);
	snQE = nQE;
} 
wQE = swQE;

/* setting up the null rectangle and the variables to be used later */


int nrow, ncol;
nrow = reg->nrow;
ncol = reg->ncol;

static PIXPOS *pixpos = NULL, *pixpos2 = NULL;
if (pixpos == NULL) pixpos = thPixposNew();
if (pixpos2 == NULL) pixpos2 = thPixposNew();

OBJMASK *om, *rec_om, *n_circle_om = NULL, *old_circle_om = NULL;

int l = 0;
THPIX radius;
rec_om = phObjmaskFromRect(0, 0, ncol - 1, nrow - 1);
radius = (THPIX) PROFILE_INIT_RADIUS;
radius = (int) radius + 0.5;
THPIX rowc, colc;
RET_CODE status;
status  = thVariableGetCenter(q, type, &rowc, &colc);
if (status != SH_SUCCESS) {
	thError("%s: WARNING - (rowc) and (colc) could not be calculated for object (%s)", name, type); 
	colc = (THPIX) 0.0;
	rowc = (THPIX) 0.0;
}
om = phObjmaskFromCircle((int) colc + idc, (int) rowc + idr, (int) radius);
phObjmaskAndObjmask(om, rec_om);
n_circle_om = phObjmaskCopy(om, 0, 0);

int nqe;
int qneeded, iqe = 0, s, i, j, j1, j2, r, m;
THPIX *row;
/* moved to the inside of the loop */
nqe = nQE;

#if DEBUG_PROFILE
printf("%s: ", name);
#endif

while ((qneeded = IsQuadNeeded_CR(om, QE[0])) == 1) {

	nqe = nQE;

	/* loop over pixels */
	for (s = 0; s < om->nspan; s++) {
	/* the following assumes that the spanmask has no parent */
	i = (int) om->s[s].y; 
	j1 = (int) om->s[s].x1;
	j2 = (int) om->s[s].x2; 	
	row = reg->rows_thpix[i];
	for (j = j1; j <= j2; j++) {

			/* loop over quadratures */
			for (r = 0; r < nqe; r++) {
			thPixposSet(pixpos, (THPIX) (i - idr), (THPIX) (j - idc));
			thQuadOnePix(f, pixpos, q, QE[r]);	
			}

			iqe = ChooseWorstAcceptableQuad(QE, nqe);	
			/* this seems to be source of a problem which affects the time profiling
			Feb 7, 2012
			nqe -= iqe;
			*/
			nqe = iqe + 1;
			row[j] = QE[0]->val;

		}
	}
	/* now decide what is the next quadrature rule that should be used */
	//iqe = ChooseBestQuad(wQE, nqe);
	
	#if DEBUG_PROFILE
	printf(" (nQE=%d, iqe=%d, %g) ", nQE, iqe, radius);
	#endif
	
	nQE -= iqe;
	wQE += iqe;
	QE  += iqe;
        /* this seems to be causing the problem
	nqe = MIN(nqe, nQE);
	*/
	phObjmaskDel(om);
	if (old_circle_om != NULL) phObjmaskDel(old_circle_om);	
	old_circle_om = n_circle_om;
	/* creating the next circle of integration */
	m = MIN(wQE[0]->mrow, wQE[0]->mcol);
	radius = thGetNextQuadRadius(radius, m, 0.0, PROFILE_RATIO);
	radius = (THPIX) ((int) (radius + 0.5));
	n_circle_om = phObjmaskFromCircle((int) colc + idc, (int) rowc + idr, 
					  (int) radius);
	om = phObjmaskNotIntersectionObjmask(n_circle_om, old_circle_om);
	phObjmaskAndObjmask(om, rec_om);
	l++;
}

#if DEBUG_PROFILE
printf("\n");
#endif

phObjmaskDel(om);
phObjmaskDel(n_circle_om);

/* now filling the rest of the image */

phObjmaskAndNotObjmask(rec_om, old_circle_om);
phObjmaskDel(old_circle_om);

for (s = 0; s < rec_om->nspan; s++) {
	i = rec_om->s[s].y; /* you should make sure that the spanmask used has no parent */
	j1 = rec_om->s[s].x1;
	j2 = rec_om->s[s].x2; 	
	row = reg->rows_thpix[i];
	for (j = j1; j <= j2; j++) {
		thPixposSet(pixpos, 
		(THPIX) (i - idr + 0.5), (THPIX) (j - idc + 0.5));
		row[j] = f(pixpos, q);
	}
}

/* freeing the possibly used memory */
phObjmaskDel(rec_om);

reg->row0 = -idr;
reg->col0 = -idc;

/* returning success */
return(SH_SUCCESS);
}


QUADRATURE **SetProfileQuadrature(int *nq) {
char *name = "SetProfileQuadrature";
static QUADRATURE **q = NULL;

if (nq == NULL) {	
	thError("%s: ERROR - should be able to export the size of the QUAD array", name);
	return(NULL);
	}

static const int m = PROFILE_INIT_QUAD;
if (q == NULL) {
	q = thCalloc(m, sizeof(QUADRATURE*));
	int i;
	for (i = 0; i < m; i++) {
		q[i] = thQuadratureNew();
		q[i]->mrow = m - i;
		q[i]->mcol = m - i;
		}
}
*nq = m;
return(q);
}


void thQuadPix(THPIX (*f)(PIXPOS *, void *), 
	PIXPOS *pixpos1, PIXPOS *pixpos2, 
	void *q, QUADRATURE *Q) {
char *name = "thQuadPix";


static PIXPOS *pixpos = NULL;
static PROFILEFL vv, v, *w = NULL, ***ww = NULL;
static int init = 0, init_mrow = 0, init_mcol = 0, init_m = 0;

if (f == NULL && pixpos1 == NULL && pixpos2 == NULL && 
	q == NULL && Q == NULL) {
	int i, j;
	for (i = 0; i < init_mrow; i++) {
		PROFILEFL **wwi;
		wwi = ww[i];
		for (j = 0; j < init_mcol; j++) {
			if (wwi[j] != NULL) thFree(wwi[j]); /* static array - checked for fragmentation */
		}
		if (wwi != NULL) thFree(wwi); /* static array - checked for fragmentation */
	}
	if (ww != NULL) {
		thFree(ww); /* static array - checked for fragmentation */
		ww = NULL;
	}
	if (pixpos != NULL) {
		thFree(pixpos); /* static array - checked for fragmentation */
		pixpos = NULL;
	}
	init_mrow = 0;
	init_mcol = 0;
	init_m = 0;
	init = 0;
	thError("%s: WARNING - should re-initiate the function", name);
	return;
}

if (f == NULL) {
	thError("%s: ERROR - null function", name);
	return;
}
if (pixpos1 == NULL || pixpos2 == NULL) {
	thError("%s: ERROR - null (pixpos)", name);
	return;
}

if (q == NULL) {
	thError("%s: ERROR - null function parameter (q)", name);
	return;
}
if (Q == NULL) {
	thError("%s: ERROR - null quadrature package (Q)", name);
	return;
	}


int m, mrow, mcol;
mrow = Q->mrow;
mcol = Q->mcol;
m = mrow * mcol;


if (init && (mrow > init_mrow || mcol > init_mcol)) {
	thError("%s: ERROR - quadrature not supported", name);
	return;
}

int r, i, j;

if (!init) {	

	init_m = (int) pow(PROFILE_INIT_QUAD, 2);
	init_mrow = (int) PROFILE_INIT_QUAD;
	init_mcol = (int) PROFILE_INIT_QUAD;	

	int ii, jj;
	PROFILEFL **wwi;
	PROFILEFL *rw, *cw;
	pixpos = (PIXPOS *) thCalloc(init_m, sizeof(PIXPOS));
	ww = (PROFILEFL ***) thCalloc(init_mrow, sizeof(PROFILEFL **));
	for (i = 0; i < init_mrow; i++) {
		ww[i] = (PROFILEFL **) thCalloc(init_mcol, sizeof(PROFILEFL *));
		wwi = ww[i];
		for (j = 0; j < init_mcol; j++) {
			m = (i + 1) * (j + 1);
			wwi[j] = (PROFILEFL *) thCalloc(m, sizeof(PROFILEFL));
			w = wwi[j];
			rw = QuadWeights[i];
			cw = QuadWeights[j];

			for (r = 0; r < m; r++) {
				ii = r % (i + 1);
				jj = r / (i + 1);
				w[r] = rw[ii] * cw[jj];
			}
							

		}
	}
		
	init = 1;
}

THPIX x1, x2, y1, y2, x12, y12, dx, dy;
thPixposGet(pixpos1, &x1, &y1);
thPixposGet(pixpos2, &x2, &y2);
x12 = 0.5 * (x1 + x2);
y12 = 0.5 * (y1 + y2);
dx = 0.5 * (x2 - x1);
dy = 0.5 * (y2 - y1);

if (dx == (THPIX) 0.0 || dy == (THPIX) 0.0) {
	Q->val = (THPIX) 0.0;
}


static PROFILEFL *rn;
static PROFILEFL *cn;

w = ww[mrow - 1][mcol - 1];	
rn = QuadNodes[mrow - 1];
cn = QuadNodes[mcol - 1];

v = 0.0; vv = 0.0;
for (r = 0; r < m; r++) {

	i = r % mrow;
	j = r / mrow;
	thPixposSet(pixpos + r, 
	(THPIX) (x12 + dx * rn[i]), 
	(THPIX) (y12 + dy * cn[j]));
	v = (PROFILEFL) f(pixpos + r, q);
	vv += v * w[r];


}

Q->val = (THPIX) (vv / (PROFILEFL) 4.0);

return;
}

RET_CODE thInitQuadOnePix(void) {
char *name = "thInitQuadOnePix";

if (!init_QOP) {	

	init_m_QOP = (int) pow(PROFILE_INIT_QUAD, 2);
	init_mrow_QOP = (int) PROFILE_INIT_QUAD;
	init_mcol_QOP = (int) PROFILE_INIT_QUAD;	

	int i, j, m, r, ii, jj;
	PROFILEFL **wwi;
	PROFILEFL *rw, *cw;
	pixpos_QOP = (PIXPOS *) thCalloc(init_m_QOP, sizeof(PIXPOS)); 
	ww_QOP = (PROFILEFL ***) thCalloc(init_mrow_QOP, sizeof(PROFILEFL **)); 
	for (i = 0; i < init_mrow_QOP; i++) {
		ww_QOP[i] = (PROFILEFL **) thCalloc(init_mcol_QOP, sizeof(PROFILEFL *)); 
		wwi = ww_QOP[i];
		for (j = 0; j < init_mcol_QOP; j++) {
			m = (i + 1) * (j + 1);
			wwi[j] = (PROFILEFL *) thCalloc(m, sizeof(PROFILEFL)); 
			PROFILEFL *w = wwi[j];
			rw = QuadWeights[i];
			cw = QuadWeights[j];
			for (r = 0; r < m; r++) {
				ii = r % (i + 1);
				jj = r / (i + 1);
				w[r] = rw[ii] * cw[jj];
			}
							

		}
	}
	init_QOP = 1;
}

return(SH_SUCCESS);
}

RET_CODE thDeInitQuadOnePix(void) {
char *name = "thDeInitQuadOnePix";	
if (init_QOP) {
	int i, j;
	for (i = 0; i < init_mrow_QOP; i++) {
		PROFILEFL **wwi;
		wwi = ww_QOP[i];
		for (j = 0; j < init_mcol_QOP; j++) {
			if (wwi[j] != NULL) thFree(wwi[j]); /* static array - checked for fragmentation */
		}
		if (wwi != NULL) thFree(wwi); /* static array - checked for fragmentation */
	}
	if (ww_QOP != NULL) {
		thFree(ww_QOP); /* static array - checked for fragmentation */
		ww_QOP = NULL;
	}
	if (pixpos_QOP != NULL) {
		thFree(pixpos_QOP); /* static array - checked for fragmentation */
		pixpos_QOP = NULL;
	}
	init_mrow_QOP = 0;
	init_mcol_QOP = 0;
	init_m_QOP = 0;
	init_QOP = 0;
	thError("%s: WARNING - should re-initiate 'QuadOnePix'", name);
	return(SH_SUCCESS);

} else {
	thError("%s: WARNING - 'QuadOnePix' was never initiated", name);
	return(SH_SUCCESS);
}
}

void thQuadOnePix(THPIX (*f)(PIXPOS *, void *), 
	PIXPOS *pixpos1, 
	void *q, QUADRATURE *Q) {
char *name = "thQuadOnePix";

shAssert(f != NULL);
shAssert(pixpos1 != NULL);
shAssert(q != NULL);
shAssert(Q != NULL);
shAssert(init_QOP == 1);

int m, mrow, mcol;
mrow = Q->mrow;
mcol = Q->mcol;
m = mrow * mcol;

if (mrow > init_mrow_QOP || mcol > init_mcol_QOP) {
	thError("%s: ERROR - quadrature not supported", name);
	return;
}

int r;
/* OPTIMIZE QUAD_PIX */
register PROFILEFL *rn, *cn;
rn = QuadNodes[mrow - 1];
cn = QuadNodes[mcol - 1];

register THPIX x1, y1;
x1 = pixpos1->row;
y1 = pixpos1->col;

register PIXPOS *pixpos_temp = pixpos_QOP;
for (r = 0; r + 3 < m; r = r + 4) {
	register int i1, i2, i3, i4;
	 
	i1 = r % mrow;
	i2 = (r + 1) % mrow;
	i3 = (r + 2) % mrow;
	i4 = (r + 3) % mrow;
	(pixpos_temp + r)->row = (THPIX) (x1 + 0.5 + rn[i1]);
	(pixpos_temp + r + 1)->row = (THPIX) (x1 + 0.5 + rn[i2]);
	(pixpos_temp + r + 2)->row = (THPIX) (x1 + 0.5 + rn[i3]);
	(pixpos_temp + r + 3)->row = (THPIX) (x1 + 0.5 + rn[i4]);
	i1 = r / mrow;
	i2 = (r + 1) / mrow;
	i3 = (r + 2) / mrow;
	i4 = (r + 3) / mrow;
	(pixpos_temp + r)->col = (THPIX) (y1 + 0.5 + cn[i1]);
	(pixpos_temp + r + 1)->col = (THPIX) (y1 + 0.5 + cn[i2]);
	(pixpos_temp + r + 2)->col = (THPIX) (y1 + 0.5 + cn[i3]);
	(pixpos_temp + r + 3)->col = (THPIX) (y1 + 0.5 + cn[i4]);

}
for (r = (m / 4) * 4; r < m; r++) {
	register int i;
	i = r % mrow;
	(pixpos_temp + r)->row = (THPIX) (x1 + 0.5 + rn[i]);
	i = r / mrow;
	(pixpos_temp + r)->col = (THPIX) (y1 + 0.5 + cn[i]); 
}
	
/*  OPTIMIZE_QUADPIX */
register PROFILEFL *w, vw;
w = ww_QOP[mrow - 1][mcol - 1];
vw = (PROFILEFL) 0.0;

for (r = 0; r+3 < m; r=r+4) {
	register PROFILEFL vv1, vv2, vv3, vv4;
	vv1 = (PROFILEFL) f(pixpos_temp + r, q);
	vv2 = (PROFILEFL) f(pixpos_temp + r + 1, q);
	vw += vv1 * w[r];
	vw += vv2 * w[r+1];
	
	vv3 = (PROFILEFL) f(pixpos_temp + r + 2, q);
	vv4 = (PROFILEFL) f(pixpos_temp + r + 3, q);
	vw += vv3 * w[r+2];
	vw += vv4 * w[r+3];
}
for (r = (m / 4) * 4; r < m; r++) {
	register PROFILEFL vv;
	vv = (PROFILEFL) f(pixpos_temp + r, q);
	vw += vv * w[r];
}

/* the following does not work properly for readons i don't understand. 
	it could be because of the size of the arrays involved or the value of &inc
        Discovered on Feb 7, 2012. Originally included in October following DNS's suggestions
#if PROFILEFL == FL64
	int inc = 1;
	vw = ddot_(&m, vv, &inc, w, &inc);
#else 
#if PROFILEFL == FL32 
	vw = sdot_(&m, vv, &inc, w, &inc);
#else
vw = (PROFILEFL) THNAN;
#endif
#endif
*/

Q->val = (THPIX) (vw / (PROFILEFL) 4.0);

return;
}

QUAD_CONDITION IsQuadNeeded_SQ(NAIVE_MASK *om, QUADRATURE *QE) {
char *name = "IsQuadNeeded_SQ";

if (om == NULL || QE == NULL) {
	thError("%s: WARNING - null input", name);
	return(UNKNOWN_QUAD_CONDITION);
}

/* if the spanmask belongs to an empty space it means that you have already integrated over all pixels */
if (om->nspan == 0) {
	return(INTEGRATED_ALL);
}

/* if quadrature gives you m = 1, then you indeed don't need quadrature procedure, you can simply use the middle point */

if (QE->mrow == 1 && QE->mcol == 1) {
	return(NO_QUADRATURE_NEEDED);
}

return(CONTINUE_QUADRATURE);
}



int IsQuadNeeded_CR(OBJMASK *om, QUADRATURE *QE) {

/* if the spanmask belongs to an empty space it means that you have already integrated over all pixels */
if (om->nspan == 0) {
	return(0);
}

/* if quadrature gives you m = 1, then you indeed don't need quadrature procedure, you can simply use the middle point */

if (QE->mrow == 1 && QE->mcol == 1) {
	return(0);
}

return(1);
}

void ChooseWorseQuad(QUADRATURE **tQ, QUADRATURE **sQ, int nQ) {
/* this function should not be used */
int i;
THPIX v;
v = sQ[0]->val;
for (i = 1; i < nQ; i++) {
	tQ[i]->val = MAX(tQ[i]->val, fabs((sQ[i]->val - v) / v));
}
return;
}

int ChooseWorstAcceptableQuad(QUADRATURE **sQ, int nQ) {
int i = 0;
THPIX v, z = 0.0;
#if VERY_DEEP_DEBUG_PROFILE
static int n_invoked = 0;
n_invoked++;
#endif
v = sQ[0]->val;
for (i = nQ - 1; i > 0; i--) {
	z = (sQ[i]->val - v) / v;
	#if VERY_DEEP_DEBUG_PROFILE
	if (n_invoked < 40) printf(".. %g ..", fabs(z));
	#endif
	if (fabs(z) < QUAD_TOLERANCE) break;
}
#if VERY_DEEP_DEBUG_PROFILE
if (n_invoked < 40) printf(" (z=%g, i=%d, nqe=%d) ", z, i, nQ);
#endif
return(i);
}

int ChooseBestQuad(QUADRATURE **Q, int nQ) {
int i;
THPIX bval;
int r = 0;
bval = Q[0]->val;
for (i = 1; i < nQ; i++) {
	 if (Q[i]->val < bval || Q[i]->val < QUAD_TOLERANCE)  {
		r = i;
		bval = Q[i]->val;
		}
}
return(r);
}


/* the following function merges the om with the region on the image that is *** OUTSIDE ****
of the mask */

void fMaskGetUnionNext(OBJMASK *om, int nrow, int ncol) {
if (om == NULL) return;

int i = 0;
SPAN *s;
s = om->s;
int nspan, npix;
nspan = om->nspan;
OBJMASK *om1, *om2;
while (i < nspan) {
	if (i < nspan - 1 && s->y == (s + 1)->y) {
		if (s->x1 > (s + 1)->x1) {
			npix += ncol - s->x2;
			s->x2 = ncol;
		} else {
			npix += s->x1;	
			s->x1 = 0;
		}
	} else if (i > 0 && s->y == (s - 1)->y) {
		if (s->x1 > (s + 1)->x1) {
			npix += ncol - s->x2;
			s->x2 = ncol;
		} else {
			npix += s->x1;	
			s->x1 = 0;
		}
	} else {
		npix += s->x1 + ncol - s->x2;	
		s->x1 = 0;
		s->x2 = ncol;
	}
	i++; s++;
	}
om->npix += npix;

if (om->rmin > 0) {
	om1 = phObjmaskFromRect(0, 0, ncol - 1, om->rmin - 1);
	phObjmaskMerge(om, om1, 0, 0);
}

if (om->rmax < ncol - 1) {
	om2 = phObjmaskFromRect(0, om->rmax + 1, ncol - 1, nrow - 1);
	phObjmaskMerge(om, om2, 0, 0);
}

return;
}

void thQuadCopyArray(QUADRATURE **tQ, QUADRATURE **sQ, int nQ) {

if (tQ == NULL || sQ == NULL || nQ <= 0) return;
static size_t QUAD_SIZE = sizeof(QUADRATURE);
int i;
for (i = 0; i < nQ; i++) {
	if (sQ[i] != NULL) {
		if  (tQ[i] == NULL) tQ[i] = thQuadratureNew();
		memcpy(tQ[i], sQ[i], QUAD_SIZE);
	}
}
return;
}

THPIX thGetNextQuadRadius(THPIX r, int m, THPIX nu, THPIX c) {
/* this is a function that gives the next m-th radius for the quadrature
   on a function ~ exp(-r ^ nu)
*/
THPIX rr;
if (r <= 0.0 && m <= 0 && c <= 0) return((THPIX) -1.0);
rr = r * pow(c, 1.0 / (2.0 * m + nu));
if (rr <= r) return((THPIX) -1.0);
if (rr < r + 1.0) return(r + 1.0);
return(rr);
}

RET_CODE thVariableGetCenter(void *q, char *type, THPIX *rowc, THPIX *colc) {
char *name = "thVariableGetCenter";
if (q == NULL || type == NULL || strlen(type) == 0) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (rowc == NULL && colc == NULL) {
	thError("%s: WARNING - null output", name);
	return(SH_SUCCESS);
}

int i;
static int init = 0;
static char **rowc_name = NULL, **colc_name = NULL;
static int n_rowc_names = 0, n_colc_names = 0;
static char *rowc_type = NULL, *colc_type = NULL;
if (!init) {
	if (rowc_name != NULL) {
		for (i = 0; i < N_ROWC_NAMES; i++) {
			if (rowc_name[i] != NULL) thFree(rowc_name[i]); /* static array - checked for fragmentation */
		}
	thFree(rowc_name); /* static array - checked for fragmentation */
	rowc_name = NULL;
	} 
 	if (colc_name != NULL) {
		for (i = 0; i < N_COLC_NAMES; i++) {
			if (colc_name[i] != NULL) thFree(colc_name[i]); /* static array - checked for fragmentation */
		}
	thFree(colc_name); /* static array - checked for fragmentation */
	colc_name = NULL;
	} 
	rowc_name = thCalloc(N_ROWC_NAMES, sizeof(char *)); /* static array - checked for fragmentation */
	colc_name = thCalloc(N_COLC_NAMES, sizeof(char *)); /* static array - checked for fragmentation */
	for (i = 0; i < N_ROWC_NAMES; i++) {
		rowc_name[i] = thCalloc(SIZE, sizeof(char)); /* static array - checked for fragmentation */
	}
	for (i = 0; i < N_COLC_NAMES; i++) {
		colc_name[i] = thCalloc(SIZE, sizeof(char)); /* static array - checked for fragmentation */
	}

	i = 0;
	strcpy(rowc_name[i], "rowc");
	i++;
	strcpy(rowc_name[i], "ROWC");
	i++;
	strcpy(rowc_name[i], "xc");
	i++;
	strcpy(rowc_name[i], "XC");
	i++;
	strcpy(rowc_name[i], "x_c");
	i++;
	strcpy(rowc_name[i], "X_C");
	i++;
	strcpy(rowc_name[i], "row_c");
	i++;
	strcpy(rowc_name[i], "ROW_c");
	i++;
	strcpy(rowc_name[i], "crow");
	i++;
	strcpy(rowc_name[i], "CROW");
	i++;
	n_rowc_names = i;

	i = 0;
	strcpy(colc_name[i], "colc");
	i++;
	strcpy(colc_name[i], "COLC");
	i++;
	strcpy(colc_name[i], "yc");
	i++;
	strcpy(colc_name[i], "YC");
	i++;
	strcpy(colc_name[i], "y_c");
	i++;
	strcpy(colc_name[i], "Y_C");
	i++;
	strcpy(colc_name[i], "col_c");
	i++;
	strcpy(colc_name[i], "COL_c");
	i++;
	strcpy(colc_name[i], "ccol");
	i++;
	strcpy(colc_name[i], "CCOL");
	i++;
	n_colc_names = i;

	if (rowc_type != NULL) thFree(rowc_type); /* static string - checked for fragmentation */
	if (colc_type != NULL) thFree(colc_type); /* static string - checked for fragmentation */
	rowc_type = thCalloc(SIZE, sizeof(char)); /* static string - checked for fragmentation */
	colc_type = thCalloc(SIZE, sizeof(char)); /* static string - checked for fragmentation */

	
 	if (sizeof(THPIX) == sizeof(FL32)) {	
		strcpy(rowc_type, "FL32");
		strcpy(colc_type, "FL32");
	} else if (sizeof(THPIX) == sizeof(FL64)) {
		strcpy(rowc_type, "FL64");
		strcpy(colc_type, "FL64");
	} else {
		thError("%s: ERROR - THPIX is not registered", name);
		return(SH_GENERIC_ERROR);
	}
	
init = 1;
}

TYPE qtype;
qtype = shTypeGetFromName(type);
if (qtype == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - unregistered type (%s)", name, type);
	return(SH_GENERIC_ERROR);
	}

SCHEMA_ELEM *se;
if (rowc != NULL) {
	i = 0;
	while ((i < n_rowc_names) && 
	((se = shSchemaElemGetFromType(qtype, rowc_name[i])) == NULL || 
	strcmp(se->type, rowc_type))) i++;
	if (se != NULL && i < n_rowc_names) {
		*rowc = *(THPIX *) shElemGet(q, se, NULL);
	} else {
 		thError("%s: ERROR - could not locate proper 'rowc' record in variable %s", name, type);
		return(SH_GENERIC_ERROR);
	}
}
if (colc != NULL) {
	i = 0; se = NULL;
	while ((i < n_colc_names) && 
	((se = shSchemaElemGetFromType(qtype, colc_name[i])) == NULL || 
		strcmp(se->type, colc_type))) i++;
	if (se != NULL && !strcmp(se->type, colc_type)) {
		*colc = *(THPIX *) shElemGet(q, se, NULL);
	} else {
 		thError("%s: ERROR - could not locate proper 'colc' record in variable %s", name, type);
		return(SH_GENERIC_ERROR);
	}
}
return(SH_SUCCESS);
}
