function basic_draw_matched_pairs, list1 = list1, list2 = list2, list3 = list3, liststar2 = liststar2, $
	sky1 = sky1, sky2 = sky2, sky3= sky3, $
	color1 = color1, color2 = color2, color3 = color3, $
	boxplots = boxplots, galaxy = galaxy, star = star, LRG = LRG, flagLRG = flagLRG, $
	fake_cd2 = fake_cd2, $
	locmap = locmap, calib_info = calib_info, galactic_calib = galactic_calib, bands = bands, mag_thresh = mag_thresh, $
	matchlog1 = matchlog1, matchlog2 = matchlog2, sample_report = sample_report, distance_match = distance_match

name = "draw_matched_pairs"
verbose = !verbose_deep

if (n_elements(sample_report) eq 0) then begin
	print, name, ": ERROR - no sample report passed"
	return, [-1L]
endif
bad_report = sample_report
status = -1
if (n_elements(sky1) ne 1) then sky1 = !value_is_bad
if (n_elements(sky2) ne 1) then sky2 = !value_is_bad
if (n_elements(sky3) ne 1) then sky3 = !value_is_bad

if (n_elements(color1) eq 0 or n_elements(color2) eq 0 or n_elements(color3) eq 0) then begin
	print, name, ": ERROR - all three colors should be set"
	return, bad_report
endif
if (keyword_set(galaxy) + keyword_set(star) + keyword_set(LRG) ne 1) then begin
	print, name, ": ERROR - one of the object type keywords should be set"
	return, bad_report
endif

if (keyword_set(galaxy) or keyword_set(LRG)) then objc_type = 3
if (keyword_set(star)) then objc_type = 6

n1 = n_elements(list1)
n2 = n_elements(list2)
n3 = n_elements(list3)
if (n1 eq 0) then begin
	print, name, ": ERROR - empty list1"
	return, bad_report
endif
list2_flag = 0
if (n2 gt 0 and size(list2[0], /type) eq 8) then list2_flag = 1;

if (n3 ne 0) then begin
	if  (size(list3[0], /type) ne 8) then n3 = 0
endif
if (n3 ne 0) then print, '£', format='(A, $)'

if (n_elements(calib_info) eq 0) then begin
	print, name, ": ERROR - empty calib_info"
	return, bad_report
endif
if (n_elements(flagLRG) eq 0) then this_flagLRG = -1 else this_flagLRG = flagLRG
status = -1
if (size(list1[0], /type) ne 8) then begin
	if (verbose) then print, name, ": ERROR - non-structure list1"
	return, bad_report
endif
if (size(list2[0], /type) ne 8) then begin
	;; if (verbose) then print, name, ": ERROR - non-structure list2"
	;; print, "L2", format='(A, $)'
	;; return, bad_report
	list2_flag = 0
endif else begin
	list2_flag = 1
endelse

if (size(calib_info[0], /type) ne 8) then begin
	if (verbose) then print, name, ": ERROR - non-structure calib_info"
	return, bad_report
endif
if (n_elements(bands) eq 0) then bands = ['r'] ;; ['u', 'g', 'r', 'i', 'z']
if (n_elements(mag_thresh) eq 0) then mag_thresh = 100


count_match2 = 0
count_match1 = 0L
count_non_match2 = 0
count_non_match1 = 0
match_count = 0
unmatch_count = 0


do_cd2 = 0
do_cd2_new_type = 0
do_fake_cd2 = 0
do_star2 = 0

do_cd1 = tag_exist(list1[0], "R_DEV2")
do_star1 = tag_exist(list1[0], "COUNTS_STAR")

if (list2_flag eq 1) then begin
	do_cd2 = tag_exist(list2[0], "I_DEV2")
	do_cd2_new_type = tag_exist(list2[0], "I_DEV1");
	do_fake_cd2 = keyword_set(fake_cd2)
	do_star2 = tag_exist(list2[0], "COUNTS_STAR")
endif

status = 0

count1 = 0
count2 = 0

counts_new_cD = 0
nstar2i = 0
nstar2f = 0
ngalaxy2i = 0
ngalaxy2f = 0

status = 0
if (tag_exist(list1[0], "FLAGLRG") eq 1) then flagLRG1 = list1.flagLRG $
else flagLRG1 = create_flagLRG(list1, galactic_calib = galactic_calib, calib_info = calib_info, status = status)
if (status ne 0) then begin
	print, name, ": ERROR - could not create flagLRG for list1 (size = ", n_elements(list1), ")"
	return, bad_report
endif

if (keyword_set(galaxy) or keyword_set(star)) then begin
	index1 = where(list1.objc_type eq objc_type and list1.nchild eq 0, count1)
endif else begin
	if (this_flagLRG eq -1) then $
		index1 = where(list1.objc_type eq objc_type and flagLRG1 ne 0 and list1.nchild eq 0, count1) $
	else $
		index1 = where(list1.objc_type eq objc_type and flagLRG1 eq flagLRG and list1.nchild eq 0, count1)
	index1 = where(list1.objc_type eq objc_type and list1.nchild eq 0, count1)
endelse

if (n3 gt 0) then begin
	counts_new_cD = 0
	new_index = where(list3.I_deV1 ne 0.0, counts_new_cD)
	if (counts_new_cD gt 0) then begin
	list3[new_index].I_deV = list3[new_index].I_deV1
	list3[new_index].IErr_deV = list3[new_index].IErr_deV1
	list3[new_index].counts_deV = list3[new_index].counts_deV1
	list3[new_index].re_deV = list3[new_index].re_deV1
	list3[new_index].mcount_deV = list3[new_index].mcount_deV1
	list3[new_index].e_deV = list3[new_index].e_deV1
	list3[new_index].phi_deV = list3[new_index].phi_deV1
	list3[new_index].mag_deV = list3[new_index].mag_deV1
	list3[new_index].xc_deV = list3[new_index].xc_deV1
	list3[new_index].yc_deV = list3[new_index].yc_deV1

	list3[new_index].I_deV1 = 0.0
	list3[new_index].IErr_deV1 = 0.0
	list3[new_index].counts_deV1 = 0.0
	list3[new_index].re_deV1 = 0.0;
	list3[new_index].mcount_deV1 = 0.0
	list3[new_index].e_deV1 = 0.0	
	list3[new_index].phi_deV1 = 0.0
	list3[new_index].mag_deV1 = 0.0
	
	endif

	if (tag_exist(list3[0], "I_PL")) then begin

		counts_new_cD = 0
		new_index = where(list3.I_pl ne 0.0, counts_new_cD)
		if (counts_new_cD gt 0) then begin
		list3[new_index].I_deV2 = list3[new_index].I_pl
		list3[new_index].IErr_deV2 = list3[new_index].IErr_pl
		list3[new_index].counts_deV2 = list3[new_index].counts_pl
		list3[new_index].re_deV2 = list3[new_index].re_pl
		list3[new_index].mcount_deV2 = list3[new_index].mcount_pl
		list3[new_index].e_deV2 = list3[new_index].e_pl
		list3[new_index].phi_deV2 = list3[new_index].phi_pl
		list3[new_index].mag_deV2 = list3[new_index].mag_pl
		list3[new_index].xc_deV2 = list3[new_index].xc_pl
		list3[new_index].yc_deV2 = list3[new_index].yc_pl

		list3[new_index].I_pl = 0.0
		list3[new_index].IErr_pl = 0.0
		list3[new_index].counts_pl = 0.0
		list3[new_index].re_pl = 0.0;
		list3[new_index].mcount_pl = 0.0
		list3[new_index].e_pl = 0.0	
		list3[new_index].phi_pl = 0.0
		list3[new_index].mag_pl = 0.0
	
		endif

	endif
	
endif


if (keyword_set(galaxy) or keyword_set(star) or keyword_set(lrg)) then begin
	if (do_cd2) then begin
		if (do_cd2_new_type) then begin
			counts_new_cD = 0
			new_index = where(list2.I_deV1 ne 0.0, counts_new_cD)
			
			if (counts_new_cD gt 0) then begin
			list2[new_index].I_deV = list2[new_index].I_deV1
			list2[new_index].IErr_deV = list2[new_index].IErr_deV1
			list2[new_index].counts_deV = list2[new_index].counts_deV1
			list2[new_index].re_deV = list2[new_index].re_deV1
			list2[new_index].mcount_deV = list2[new_index].mcount_deV1
			list2[new_index].e_deV = list2[new_index].e_deV1
			list2[new_index].phi_deV = list2[new_index].phi_deV1
			list2[new_index].mag_deV = list2[new_index].mag_deV1
			list2[new_index].xc_deV = list2[new_index].xc_deV1
			list2[new_index].yc_deV = list2[new_index].yc_deV1

			list2[new_index].I_deV1 = 0.0
			list2[new_index].IErr_deV1 = 0.0
			list2[new_index].counts_deV1 = 0.0
			list2[new_index].re_deV1 = 0.0;
			list2[new_index].mcount_deV1 = 0.0
			list2[new_index].e_deV1 = 0.0	
			list2[new_index].phi_deV1 = 0.0
			list2[new_index].mag_deV1 = 0.0

			endif

			if (tag_exist(list2[0], "I_PL") and counts_new_cD eq 0) then begin

			counts_new_cD = 0
			new_index = where(list2.I_pl ne 0.0, counts_new_cD)
			if (counts_new_cD gt 0) then begin
			list2[new_index].I_deV2 = list2[new_index].I_pl
			list2[new_index].IErr_deV2 = list2[new_index].IErr_pl
			list2[new_index].counts_deV2 = list2[new_index].counts_pl
			list2[new_index].re_deV2 = list2[new_index].re_pl
			list2[new_index].mcount_deV2 = list2[new_index].mcount_pl
			list2[new_index].e_deV2 = list2[new_index].e_pl
			list2[new_index].phi_deV2 = list2[new_index].phi_pl
			list2[new_index].mag_deV2 = list2[new_index].mag_pl
			list2[new_index].xc_deV2 = list2[new_index].xc_pl
			list2[new_index].yc_deV2 = list2[new_index].yc_pl
	
			list2[new_index].I_pl = 0.0
			list2[new_index].IErr_pl = 0.0
			list2[new_index].counts_pl = 0.0
			list2[new_index].re_pl = 0.0;
			list2[new_index].mcount_pl = 0.0
			list2[new_index].e_pl = 0.0	
			list2[new_index].phi_pl = 0.0
			list2[new_index].mag_pl = 0.0
			endif

			if (tag_exist(list2[0], "I_CORESERSIC") and counts_new_cD eq 0) then begin

			counts_new_cD = 0
			new_index = where(list2.I_coresersic ne 0.0, counts_new_cD)
			;if (counts_new_cD gt 0) then begin
			
			;list2[new_index].I_deV1 = list2[new_index].I_coresersic
			;list2[new_index].IErr_deV1 = list2[new_index].IErr_coresersic
			;list2[new_index].counts_deV1 = list2[new_index].counts_coresersic
			;list2[new_index].re_deV1 = list2[new_index].rb_coresersic
			;list2[new_index].mcount_deV1 = list2[new_index].mcount_coresersic
			;list2[new_index].e_deV1 = list2[new_index].e_coresersic
			;list2[new_index].phi_deV1 = list2[new_index].phi_coresersic
			;list2[new_index].mag_deV1 = list2[new_index].mag_coresersic
			;list2[new_index].xc_deV1 = list2[new_index].xc_coresersic
			;list2[new_index].yc_deV1 = list2[new_index].yc_coresersic


			;list2[new_index].I_deV2 = list2[new_index].I_coresersic
			;list2[new_index].IErr_deV2 = list2[new_index].IErr_coresersic
			;list2[new_index].counts_deV2 = list2[new_index].counts_coresersic
			;list2[new_index].re_deV2 = list2[new_index].re_coresersic
			;list2[new_index].mcount_deV2 = list2[new_index].mcount_coresersic
			;list2[new_index].e_deV2 = list2[new_index].e_coresersic
			;list2[new_index].phi_deV2 = list2[new_index].phi_coresersic
			;list2[new_index].mag_deV2 = list2[new_index].mag_coresersic
			;list2[new_index].xc_deV2 = list2[new_index].xc_coresersic
			;list2[new_index].yc_deV2 = list2[new_index].yc_coresersic
	
			;list2[new_index].I_pl = 0.0
			;list2[new_index].IErr_pl = 0.0
			;list2[new_index].counts_pl = 0.0
			;list2[new_index].re_pl = 0.0;
			;list2[new_index].mcount_pl = 0.0
			;list2[new_index].e_pl = 0.0	
			;list2[new_index].phi_pl = 0.0
			;list2[new_index].mag_pl = 0.0
			;endif

			endif

			if (tag_exist(list2[0], "I_SERSIC") and counts_new_cD eq 0) then begin
				counts_new_cD = 0
				new_index = where(list2.I_deV2 ne 0.0 or list2.I_pl ne 0.0 or list2.I_sersic ne 0.0, counts_new_cD)
				if (tag_exist(list2[0], "I_SERSIC1")) then begin
					counts_new_cD = 0
					new_index = where(list2.I_deV2 ne 0.0 or list2.I_pl ne 0.0 or list2.I_sersic ne 0.0 or list2.I_sersic1 ne 0.0, counts_new_cD)
					if (tag_exist(list2[0], "I_SERSIC2")) then begin
						counts_new_cD = 0
						new_index = where(list2.I_deV2 ne 0.0 or list2.I_pl ne 0.0 or list2.I_sersic ne 0.0 or list2.I_sersic1 ne 0.0 or list2.I_sersic2, counts_new_cD)
					endif
				endif
			endif
			


			endif
		
		endif
 	
		count2 = 0
		flagLRG2 = replicate(0L, n_elements(list2))
		if (keyword_set(lrg) or keyword_set(galaxy)) then begin
			nflag_lrg2 = 0	
			if (tag_exist(list2[0],"I_CORESERSIC") or tag_exist(list2[0],"I_SERSIC")) then begin
				if (tag_exist(list2[0], "I_SERSIC1")) then begin
					if (tag_exist(list2[0], "I_CORESERSIC")) then begin
						flag_lrg_index2 = where(list2.I_deV2 ne 0.0 or list2.I_exp2 ne 0.0 or list2.I_coresersic ne 0.0 or $
						list2.I_sersic ne 0.0 or list2.I_sersic1 ne 0.0 or list2.I_sersic2 ne 0.0, nflag_lrg2)
					endif else begin
						flag_lrg_index2 = where(list2.I_deV2 ne 0.0 or list2.I_exp2 ne 0.0 or list2.I_sersic2 ne 0.0 or $
						list2.I_sersic ne 0.0 or list2.I_sersic1 ne 0.0, nflag_lrg2)
					endelse
				endif else begin	
				flag_lrg_index2 = where(list2.I_deV2 ne 0.0 or list2.I_exp2 ne 0.0 or list2.I_coresersic ne 0.0 or list2.I_sersic ne 0.0, nflag_lrg2)
				endelse
			endif else begin	
				flag_lrg_index2 = where(list2.I_deV2 ne 0.0 or list2.I_exp2 ne 0.0, nflag_lrg2)
			endelse
			if (nflag_lrg2 ne 0) then flagLRG2[flag_lrg_index2] = -1L
			;; print, nflag_lrg2, format='(I, $)' 	
			if (tag_exist(list2[0], "I_CORESERSIC") or tag_exist(list2[0],"I_SERSIC")) then begin
				if (tag_exist(list2[0], "I_SERSIC1")) then begin
					if (tag_exist(list2[0], "I_SERSIC2")) then begin
						if (tag_exist(list2[0], "I_CORESERSIC")) then begin
							index2 = where(list2.I_deV ne 0 or list2.I_deV2 ne 0.0 or list2.I_exp ne 0.0 or list2.I_exp2 ne 0.0 or $
				 			list2.I_sersic ne 0.0 or list2.I_sersic1 ne 0.0 or list2.I_sersic2 ne 0.0 or list2.I_coresersic ne 0.0, count2)
						endif else begin
							index2 = where(list2.I_deV ne 0 or list2.I_deV2 ne 0.0 or list2.I_exp ne 0.0 or list2.I_exp2 ne 0.0 or $
				 			list2.I_sersic ne 0.0 or list2.I_sersic1 ne 0.0 or list2.I_sersic2 ne 0.0, count2)
						endelse
					endif else begin
						index2 = where(list2.I_deV ne 0 or list2.I_deV2 ne 0.0 or list2.I_exp ne 0.0 or list2.I_exp2 ne 0.0 or $
				 		list2.I_sersic ne 0.0 or list2.I_sersic1 ne 0.0 or list2.I_coresersic ne 0.0, count2)
					endelse
				endif else begin	
				index2 = where(list2.I_deV ne 0 or list2.I_deV2 ne 0.0 or list2.I_exp ne 0.0 or list2.I_exp2 ne 0.0 or list2.I_sersic ne 0.0, count2)
				endelse
			endif else begin	
				index2 = where(list2.I_deV ne 0 or list2.I_deV2 ne 0.0 or list2.I_exp ne 0.0 or list2.I_exp2 ne 0.0 or list2.I_coresersic, count2)
			endelse
		endif else begin
			index2 = where(list2.I_star ne 0.0, count2)
			;; print, "SNDEX(min, max), SNDEX_count = ", min(index2), max(index2), count2  
			;; print, "I_star (min, max) = ", min(list2[index2].I_star), max(list2[index2].I_star)
		endelse
		nstar2f = 0
		star_index2 = where(list2.I_star ne 0.0, nstar2f)	
		if (nstar2f ne 0) then begin
			star_list2f = list2[star_index2]
		endif
		ngalaxy2f = 0
		galaxy_index2 = where(list2.I_star eq 0.0, ngalaxy2f)	
		if (ngalaxy2f ne 0) then begin
			galaxy_list2f = list2[galaxy_index2]
		endif
	endif else begin
		count2 = 0
		nstar2f = 0
		ngalaxy2f = 0
		index2 = [-1L]
		star_index2f = [-1L]
		galaxy_index2f = [-1L]
		
		if (list2_flag eq 1) then begin
			index2 = where(list2.objc_type eq objc_type and list2.nchild eq 0, count2)
			nstar2f = 0
			star_index2f = where(list2.objc_type eq 6 and list2.nchild eq 0, nstar2f)	
			if (nstar2f ne 0) then star_list2f = list2[star_index2f]
			ngalaxy2f = 0
			galaxy_index2f = where(list2.objc_type eq 3 and list2.nchild eq 0, ngalaxy2f)	
			if (ngalaxy2f ne 0) then galaxy_list2f = list2[galaxy_index2f]
		endif
	endelse
endif else begin
	status = 0
	if (tag_exist(list2[0], "FLAGLRG") eq 1) then flagLRG2 = list2.flagLRG $
	else flagLRG2 = create_flagLRG(list2, galactic_calib = galactic_calib, calib_info = calib_info, status = status)
	if (status ne 0) then begin
		print, name, ": ERROR - could not create flagLRG for list2 (size = ", n_elements(list2), ")"
		return, bad_report
	endif
	if (do_cd2) then begin
		count2 = n_elements(list2)
		index2 = lindgen(count2)
	endif else begin
		if (this_flagLRG eq -1) then $
		index2 = where(list2.objc_type eq objc_type and flagLRG2 ne 0 and list2.nchild eq 0, count2) $
		else $
		index2 = where(list2.objc_type eq objc_type and flagLRG2 eq flagLRG and list2.nchild eq 0, count2)
	endelse

endelse
nstar2i = 0
ngalaxy2i = 0
star_index2i  = [-1L]
galaxy_index2i  = [-1L]

if (n_elements(liststar2) ne 0 and size(liststar2[0], /type) eq 8) then begin
	nstar2i = 0
	star_index2i = where(liststar2.objc_type eq 6 and liststar2.nchild eq 0, nstar2i)
	if (nstar2i gt 0) then star_list2i = liststar2[star_index2i]		
	ngalaxy2i = 0
	galaxy_index2i = where(liststar2.objc_type eq 3 and liststar2.nchild eq 0, ngalaxy2i)
	if (ngalaxy2i gt 0) then galaxy_list2i = liststar2[galaxy_index2i]		

endif

mag_range = [12.0, 27.0]

xrange = [0.0, 2048.0]
yrange = [0.0, 1489.0]
xstyle = 7
ystyle = 7
psym1 = 1
psym2 = 4

all_reports = [bad_report]

if (count1 ne 0) then begin
x1 = list1[index1].objc_colc
y1 = list1[index1].objc_rowc
x1 = reform(x1, count1);
y1 = reform(y1, count1);
;;print, "SLOC1: x (mean), y (mean) = ", mean(x1), mean(y1)

endif
if (list2_flag eq 1 and count2 ne 0) then begin
	if (do_cd2) then begin
		if (keyword_set(lrg) or keyword_set(galaxy)) then begin
			y2 = replicate(!value_is_bad, count2)
			x2 = replicate(!value_is_bad, count2)
			if (tag_exist(list2[0], "I_DEV")) then begin
				aux_count = 0
				aux_index = where(list2[index2].I_deV ne 0.0, aux_count)
				if (aux_count gt 0) then begin
					y2[aux_index] = list2[index2[aux_index]].xc_deV
					x2[aux_index] = list2[index2[aux_index]].yc_deV
				endif
			endif		
			if (tag_exist(list2[0], "I_DEV1")) then begin
				aux_count = 0
				aux_index = where(list2[index2].I_deV1 ne 0.0, aux_count)
				if (aux_count gt 0) then begin
					y2[aux_index] = list2[index2[aux_index]].xc_deV1
					x2[aux_index] = list2[index2[aux_index]].yc_deV1
				endif
			endif	
			if (tag_exist(list2[0], "I_DEV2")) then begin
				aux_count = 0
				aux_index = where(list2[index2].I_deV2 ne 0.0, aux_count)
				if (aux_count gt 0) then begin
					y2[aux_index] = list2[index2[aux_index]].xc_deV2
					x2[aux_index] = list2[index2[aux_index]].yc_deV2
				endif
			endif	
			if (tag_exist(list2[0], "I_EXP")) then begin
				aux_count = 0
				aux_index = where(list2[index2].I_exp ne 0.0, aux_count)
				if (aux_count gt 0) then begin
					y2[aux_index] = list2[index2[aux_index]].xc_exp
					x2[aux_index] = list2[index2[aux_index]].yc_exp
				endif
			endif			
			if (tag_exist(list2[0], "I_EXP1")) then begin
				aux_count = 0
				aux_index = where(list2[index2].I_exp1 ne 0.0, aux_count)
				if (aux_count gt 0) then begin
					y2[aux_index] = list2[index2[aux_index]].xc_exp1
					x2[aux_index] = list2[index2[aux_index]].yc_exp1
				endif
			endif			
			if (tag_exist(list2[0], "I_EXP2")) then begin
				aux_count = 0
				aux_index = where(list2[index2].I_exp2 ne 0.0, aux_count)
				if (aux_count gt 0) then begin
					y2[aux_index] = list2[index2[aux_index]].xc_exp2
					x2[aux_index] = list2[index2[aux_index]].yc_exp2
				endif
			endif		
			if (tag_exist(list2[0], "I_SERSIC")) then begin
				aux_count = 0
				aux_index = where(list2[index2].I_sersic ne 0.0, aux_count)
				if (aux_count gt 0) then begin
					y2[aux_index] = list2[index2[aux_index]].xc_sersic
					x2[aux_index] = list2[index2[aux_index]].yc_sersic
				endif
			endif			
			if (tag_exist(list2[0], "I_SERSIC1")) then begin
				aux_count = 0
				aux_index = where(list2[index2].I_sersic1 ne 0.0, aux_count)
				if (aux_count gt 0) then begin
					y2[aux_index] = list2[index2[aux_index]].xc_sersic1
					x2[aux_index] = list2[index2[aux_index]].yc_sersic1
				endif
			endif			
			if (tag_exist(list2[0], "I_SERSIC2")) then begin
				aux_count = 0
				aux_index = where(list2[index2].I_sersic2 ne 0.0, aux_count)
				if (aux_count gt 0) then begin
					y2[aux_index] = list2[index2[aux_index]].xc_sersic2
					x2[aux_index] = list2[index2[aux_index]].yc_sersic2
				endif
			endif
			if (tag_exist(list2[0], "I_PL")) then begin
				aux_count = 0
				aux_index = where(list2[index2].I_pl ne 0.0, aux_count)
				if (aux_count gt 0) then begin
					y2[aux_index] = list2[index2[aux_index]].xc_pl
					x2[aux_index] = list2[index2[aux_index]].yc_pl
				endif
			endif			
			if (tag_exist(list2[0], "I_PL1")) then begin
				aux_count = 0
				aux_index = where(list2[index2].I_pl1 ne 0.0, aux_count)
				if (aux_count gt 0) then begin
					y2[aux_index] = list2[index2[aux_index]].xc_pl1
					x2[aux_index] = list2[index2[aux_index]].yc_pl1
				endif
			endif			
			if (tag_exist(list2[0], "I_PL2")) then begin
				aux_count = 0
				aux_index = where(list2[index2].I_pl2 ne 0.0, aux_count)
				if (aux_count gt 0) then begin
					y2[aux_index] = list2[index2[aux_index]].xc_pl2
					x2[aux_index] = list2[index2[aux_index]].yc_pl2
				endif
			endif
			if (tag_exist(list2[0], "I_CORESERSIC")) then begin
				aux_count = 0
				aux_index = where(list2[index2].I_coresersic ne 0.0, aux_count)
				if (aux_count gt 0) then begin
					y2[aux_index] = list2[index2[aux_index]].xc_coresersic
					x2[aux_index] = list2[index2[aux_index]].yc_coresersic
				endif
			endif

			;; print, x2, format="(F,$)"
			;; print, y2, format="(F,$)"			
		endif else begin
			y2 = list2[index2].xc_star
			x2 = list2[index2].yc_star
			;; print, "SLOC2: x (mean), y (mean) = ", mean(x2), mean(y2)
		endelse
	endif else begin
		x2 = list2[index2].objc_colc
		y2 = list2[index2].objc_rowc
	endelse
	x2 = reform(x2, count2)
	y2 = reform(y2, count2)

endif

if (count1 ne 0 and count2 ne 0 and list2_flag eq 1) then begin

match_index = [-1]
max_delta = -1.0
match_two_lists, list1 = list1[index1], list2 = list2[index2], match_index = match_index, max_delta = max_delta, distance_match = distance_match, $
	star = keyword_set(star), galaxy = keyword_set(galaxy), lrg = keyword_set(lrg), $
	flagLRG1 = flagLRG1[index1], flagLRG2 = flagLRG2[index2]

;; print, match_index, format="(I,$)"
x1prime = x1[match_index]
y1prime = y1[match_index]

endif

if (keyword_set(locmap) and !graphical_output) then begin

if (count1 ne 0) then begin
	if (!do_plots) then plot,x1,y1, psym=psym1,color = color1,  xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle
endif
if (count2 ne 0 and list2_flag eq 1) then begin
	if (!do_plots) then plot,x2,y2,psym=psym2,color=color2,  xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle
endif

endif

if (count1 eq 0 or count2 eq 0 and list2_flag eq 0) then begin
	if (verbose) then print, name, ": WARNING - no object found in inlist or outlist matching object type = ", objc_type
	if (verbose) then print, name, ": WARNING - count1 = ", count1, ", count2 = ", count2
	;; return, bad_report
endif

if (verbose) then print, "m", format='(A, $)'

if (count1 ne 0 and count2 ne 0 and list2_flag eq 1) then begin


if (n1 gt !shaky_n) then begin
print, name, ": WARNING - shaky algorithm"
distance = sqrt((x1prime - x2) ^ 2 + (y1prime - y2) ^ 2)
subindex = [-1]
mean_distance = -1.0
sigma_distance = -1.0
MEANCLIP, distance, mean_distance, sigma_distance, clipsig = 3.0, subs = subindex
clipped_match_index = replicate(-1L, count2)
clipped_match_index[subindex] = match_index[subindex]

non_repeated_values = clipped_match_index[uniq(clipped_match_index, sort(clipped_match_index))]
count_unique = -1
goodindex = where(non_repeated_values ne -1L, count_unique)
non_repeated_values = non_repeated_values[goodindex]
for i = 0, count_unique - 1, 1 do begin
	this_index = non_repeated_values[i]
	jcount = -1
	jndex = where(clipped_match_index eq this_index, jcount)
	if (jcount gt 1) then begin
		this_distance = distance[jndex]
		min_distance = min(this_distance, min_index)
		min_jndex = jndex[min_index]
		bad_jndex = jndex[where(jndex ne min_jndex)]
		clipped_match_index[bad_jndex] = -1L
	endif
endfor

endif else begin

clipped_match_index = match_index

endelse

;; the following information can be used to create box plot and stats for non matched and matched pairs

count_match2 = 0
match_index2 = where(clipped_match_index ne -1L, count_match2)

if (count_match2 gt 0) then begin
	match_index1 = clipped_match_index[match_index2]
	count_match1 = n_elements(match_index1)
endif else begin
	match_index1 = [-1L]
	count_match1 = 0L
endelse

count_non_match2 = 0
non_match_index2 = where(clipped_match_index eq -1L, count_non_match2)

bad_match_index1 = lindgen(count1)
if (count_match1 ne 0) then bad_match_index1[match_index1] = -1L
count_non_match1 = 0
non_match_index1 = where(bad_match_index1 ne -1L, count_non_match1)

if (keyword_set(locmap) and !graphical_output) then begin

	for i = 0, count_match2 -1 , 1L do begin

	j2 = match_index2[i]
	j1 = clipped_match_index[j2]
	x = [x1[j1], x2[j2]]
	y = [y1[j1], y2[j2]]
	if (!do_plots) then plot, x, y, xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle, color = color3

	endfor

	if (!do_plots) then multiplot

endif

endif ;; end of count check for existing objects 


;; if (keyword_set(boxplots)) then begin

report_exists = 0L
report_all = sample_report

bfilter = [1.4E-10, 0.9E-10, 1.2E-10, 1.8E-10, 7.4E-10]
for iband = 0L, n_elements(bands) - 1L, 1L do begin

band = bands[iband]
bcount = -1
bndex = where(calib_info.filter eq band, bcount)

if (bcount eq 0) then begin
	status = -1
	if (verbose) then print, name, ": ERROR - band could not be found in calib_info"
	return, bad_report
endif
bndex = bndex[0]

if (keyword_set(galaxy) or keyword_set(LRG)) then begin 

	if (count1 ne 0) then begin		
	counts1 = list1[index1].counts_model[bndex]
	frac1 = list1[index1].fracpsf[bndex]
	counts_deV1 = list1[index1].counts_deV[bndex]
	counts_exp1 = list1[index1].counts_exp[bndex]
	counts1 = counts_deV1 * frac1 + counts_exp1 * (1.0 - frac1)
		if (do_cd1) then begin
		;; frac_halo1 = list1[index1].fracpsf2[bndex]
		counts_halo_deV1 = list1[index1].counts_deV2[bndex] + list1[index1].counts_pl[bndex]
		;; counts_halo_exp1 = list1[index1].counts_exp2[bndex]
		;; counts_halo1 = counts_halo_deV1 * frac_halo1 + counts_halo_exp1 * (1.0 - frac_halo1)
		counts_halo1 = counts_halo_deV1
		endif else begin
		counts_halo1 = replicate(0.0, count1)
		endelse
	counts_sersic1 = replicate(0.0, count1)
	counts_exp_outer1 = replicate(0.0, count1)
	counts_sersic_inner1 = replicate(0.0, count1)
	counts_sersic_outer1 = replicate(0.0, count1)
	counts_csersic_inner1 = replicate(0.0, count1)
	counts_csersic_outer1 = replicate(0.0, count1)
	counts_total1 = counts_sersic1 + counts_csersic_inner1 + counts_csersic_outer1 + counts_sersic_inner1 + counts_sersic_outer1 + counts_exp_outer1 + counts_halo1 + counts1
	endif
	
	if (count2 ne 0) then begin
		if (do_cd2) then begin
			counts_deV2 = list2[index2].counts_deV
			counts_exp2 = list2[index2].counts_exp
			counts_sersic2 = list2[index2].counts_sersic
			if (tag_exist(list2[0], "I_SERSIC1")) then begin
			counts_sersic_inner2 = list2[index2].counts_sersic1
			endif else begin
			counts_sersic_inner2 = replicate(0.0, count2)
			endelse
			if (tag_exist(list2[0], "I_SERSIC2")) then begin
			counts_sersic_outer2 = list2[index2].counts_sersic2
			endif else begin
			counts_sersic_outer2 = replicate(0.0, count2)
			endelse
			if (tag_exist(list2[0], "I_CORESERSIC")) then begin
			counts_csersic_inner2 = 0.5 * list2[index2].counts_coresersic
			counts_csersic_outer2 = 0.5 * list2[index2].counts_coresersic
			endif else begin
			counts_csersic_inner2 = replicate(0.0, count2)
			counts_csersic_outer2 = replicate(0.0, count2)
			endelse

			counts_halo_deV2 = list2[index2].counts_deV2
			counts_halo_exp2 = list2[index2].counts_exp2
			counts2 = counts_deV2 + counts_exp2
			counts_halo2 = counts_halo_deV2 + counts_halo_exp2
			counts_total2 = counts2 + counts_halo2 + counts_sersic2 + counts_sersic_inner2 + counts_sersic_outer2 + $
			counts_csersic_inner2 + counts_csersic_outer2 
 
			I_deV2 = list2[index2].I_deV
			I_exp2 = list2[index2].I_exp
			I_halo_deV2 = list2[index2].I_deV2
			I_halo_exp2 = list2[index2].I_exp2
			I_sersic2 = list2[index2].I_sersic	
			if (tag_exist(list2[0], "I_SERSIC1")) then begin
			I_sersic_inner2 = list2[index2].I_sersic1
			endif else begin
			I_sersic_inner2 = replicate(0.0, count2)
			endelse
			if (tag_exist(list2[0], "I_SERSIC2")) then begin
			I_sersic_outer2 = list2[index2].I_sersic2
			endif else begin
			I_sersic_outer2 = replicate(0.0, count2)
			endelse
			if (tag_exist(list2[0], "I_CORESERSIC")) then begin
			I_csersic_inner2 = list2[index2].I_coresersic
			I_csersic_outer2 = list2[index2].I_coresersic
			endif else begin
			I_csersic_inner2 = replicate(0.0, count2)
			I_csersic_outer2 = replicate(0.0, count2)
			endelse


			IErr_deV2 = list2[index2].IErr_deV
			IErr_exp2 = list2[index2].IErr_exp
			IErr_halo_deV2 = list2[index2].IErr_deV2
			IErr_halo_exp2 = list2[index2].IErr_exp2
			IErr_sersic2 = list2[index2].IErr_sersic
			if (tag_exist(list2[0], "I_SERSIC1")) then begin
			IErr_sersic_inner2 = list2[index2].IErr_sersic1
			endif else begin
			IErr_sersic_inner2 = replicate(!value_is_bad, count2)
			endelse
			if (tag_exist(list2[0], "I_SERSIC2")) then begin
			IErr_sersic_outer2 = list2[index2].IErr_sersic2
			endif else begin
			IErr_sersic_outer2 = replicate(!value_is_bad, count2)
			endelse

			;;print, name, ": sersic amplitudes ", I_sersic2
			if (n3 gt 0) then begin
				counts_deV3 = list3[index2].counts_deV
				counts_exp3 = list3[index2].counts_exp
				counts3 = counts_deV3 + counts_exp3
				counts_halo_deV3 = list3[index2].counts_deV2
				counts_halo_exp3 = list3[index2].counts_exp2
				counts_halo3 = counts_halo_deV3 + counts_halo_exp3
				counts_total3 = counts3 + counts_halo3

				I_deV3 = list3[index2].I_deV
				I_exp3 = list3[index2].I_exp
				I_halo_deV3 = list3[index2].I_deV2
				I_halo_exp3 = list3[index2].counts_exp2

				IErr_deV3 = list3[index2].IErr_deV
				IErr_exp3 = list3[index2].IErr_exp
				IErr_halo_deV3 = list3[index2].IErr_deV2
				IErr_halo_exp3 = list3[index2].IErr_exp2

			endif		
		endif else begin
			counts2 = list2[index2].counts_model[bndex]
			frac2 = list2[index2].fracpsf[bndex]
			counts_deV2 = list2[index2].counts_deV[bndex]
			counts_exp2 = list2[index2].counts_exp[bndex]
			if (do_fake_cd2) then begin
					
				counts2 = replicate(0.0, count2)
				counts_halo2 = replicate(0.0, count2)
				r_deV2 = list2[index2].r_deV[bndex]
				r_exp2 = list2[index2].r_exp[bndex]
				nhalo_deV_index = 0
				ncore_deV_index = 0
				halo_deV_index = where(r_deV2 gt r_exp2, nhalo_deV_index)
				core_deV_index = where(r_deV2 le r_exp2, ncore_deV_index)
				
				if (nhalo_deV_index gt 0) then begin
					counts_halo2[halo_deV_index] = counts_deV2[halo_deV_index] * frac2[halo_deV_index]
					counts2[halo_deV_index] = counts_exp2[halo_deV_index] * (1.0 - frac2[halo_deV_index])
				endif 
				if (ncore_deV_index gt 0) then begin
					counts_halo2[core_deV_index] = counts_exp2[core_deV_index] * (1.0 - frac2[core_deV_index])
					counts2[core_deV_index] = counts_deV2[core_deV_index] * frac2[core_deV_index]
				endif
	
			endif else begin
				counts2 = counts_deV2 * frac2 + counts_exp2 * (1.0 - frac2)
				counts_halo2 = replicate(0.0, count2)
			endelse
			counts_total2 = counts2 + counts_halo2
		endelse
	endif
	if (nstar2f gt 0) then begin
		if (do_cd2) then begin
			counts_star2f = star_list2f[*].counts_star
		endif else begin
			counts_star2f = star_list2f[*].PsfCounts[bndex]
		endelse
	endif
	if (nstar2i gt 0) then begin
		counts_star2i = star_list2i[*].PsfCounts[bndex]
	endif
	if (ngalaxy2f gt 0) then begin
		if (do_cd2) then begin
			counts_galaxy2f = galaxy_list2f[*].counts_model
		endif else begin
			counts_galaxy2f = galaxy_list2f[*].counts_deV[bndex] * galaxy_list2f[*].fracPSF[bndex] + galaxy_list2f[*].counts_exp[bndex] * (1.0 - galaxy_list2f[*].fracPSF[bndex])
		endelse
	endif
	if (ngalaxy2i gt 0) then begin
		counts_galaxy2i = galaxy_list2i[*].counts_deV[bndex] * galaxy_list2i[*].fracPSF[bndex] + $
		galaxy_list2i[*].counts_exp[bndex] * (1.0 - galaxy_list2i[*].fracPSF[bndex])
	endif

endif else begin

	if (count1 ne 0) then begin
		if (do_star1) then begin
			counts1 = list1[index1].counts_star
		endif else begin
			counts1 = list1[index1].psfcounts[bndex]
		endelse
		counts_halo1 = replicate(0.0, count1)
		counts_sersic1 = replicate(0.0, count1)
		counts_sersic_inner1 = replicate(0.0, count1)
		counts_sersic_outer1 = replicate(0.0, count1)
		counts_csersic_inner1 = replicate(0.0, count1)
		counts_csersic_outer1 = replicate(0.0, count1)
		counts_total1 = counts1
	endif
	if (count2 ne 0) then begin
		if (do_star2) then begin
			counts2 = list2[index2].counts_star
			n_good_star_counts2 = 0
			good_star_counts_index = where(counts2 ne 0.0, n_good_star_counts2)
			;; print, "S, SGOOD = ",n_elements(index2), n_good_star_counts2 
		endif else begin
			counts2 = list2[index2].psfcounts[bndex]
		endelse
		if (n3 gt 0) then begin
			counts3 = list3[index2].counts_star
			counts_halo3 = replicate(0.0, count2)
			counts_total3 = counts3
		endif
		counts_halo2 = replicate(0.0, count2)	
		counts_total2 = counts2
	endif

endelse

A = 1.0E8 * calib_info[bndex].flux20;
B = 2.0 * bfilter[bndex]
C = 2.5 / alog(10.0);
D = alog(bfilter[bndex]);

if (count1 gt 0) then begin
	mag1 = photo_calibrate(counts1, A, B, C, D)
	mag_halo1 = photo_calibrate(counts_halo1, A, B, C, D)
	mag_sersic1 = photo_calibrate(counts_sersic1, A, B, C, D)
	mag_sersic_inner1 = photo_calibrate(counts_sersic_inner1, A, B, C, D)
	mag_sersic_outer1 = photo_calibrate(counts_sersic_outer1, A, B, C, D)
	mag_csersic_inner1 = photo_calibrate(counts_csersic_inner1, A, B, C, D)
	mag_csersic_outer1 = photo_calibrate(counts_csersic_outer1, A, B, C, D)
	mag_total1 = photo_calibrate(counts_total1, A, B, C, D)
endif

if (count2 gt 0) then begin

	mag2 = photo_calibrate(counts2, A, B, C, D)
	mag_halo2 = photo_calibrate(counts_halo2, A, B, C, D)
	mag_sersic2 = photo_calibrate(counts_sersic2, A, B, C, D)
	mag_sersic_inner2 = photo_calibrate(counts_sersic_inner2, A, B, C, D)
	mag_sersic_outer2 = photo_calibrate(counts_sersic_outer2, A, B, C, D)
	mag_csersic_inner2 = photo_calibrate(counts_csersic_inner2, A, B, C, D)
	mag_csersic_outer2 = photo_calibrate(counts_csersic_outer2, A, B, C, D)	
	mag_total2 = photo_calibrate(counts_total2, A, B, C, D)

	if (n3 gt 0) then begin
		mag3 = photo_calibrate(counts3, A, B, C, D)
		mag_halo3 = photo_calibrate(counts_halo3, A, B, C, D)
		mag_total3 = photo_calibrate(counts_total3, A, B, C, D)
	endif

endif

X_mag_star1i = !value_is_bad
X_mag_star2i = !value_is_bad
X_mag_star3i = !value_is_bad
X_mag_star1f = !value_is_bad
X_mag_star2f = !value_is_bad
X_mag_star3f = !value_is_bad
X_mag_galaxy1i = !value_is_bad
X_mag_galaxy2i = !value_is_bad
X_mag_galaxy3i = !value_is_bad
X_mag_galaxy1f = !value_is_bad
X_mag_galaxy2f = !value_is_bad
X_mag_galaxy3f = !value_is_bad

if (count1 gt 0) then begin
delta_star1i = replicate(!value_is_bad, count1)
delta_star2i = replicate(!value_is_bad, count1)
delta_star3i = replicate(!value_is_bad, count1)
delta_galaxy1i = replicate(!value_is_bad, count1)
delta_galaxy2i = replicate(!value_is_bad, count1)
delta_galaxy3i = replicate(!value_is_bad, count1)
endif

if (count2 gt 0) then begin
delta_star1f = replicate(!value_is_bad, count2)
delta_star2f = replicate(!value_is_bad, count2)
delta_star3f = replicate(!value_is_bad, count2)
delta_galaxy1f = replicate(!value_is_bad, count2)
delta_galaxy2f = replicate(!value_is_bad, count2)
delta_galaxy3f = replicate(!value_is_bad, count2)
endif

nstar2f = n_elements(counts_star2f)
if (isa(nstar2f) and nstar2f gt 0) then begin
	mag_star2f = photo_calibrate(counts_star2f, A, B, C, D)
	sorted_index_star2f = sort(mag_star2f)
	mag_star2f = mag_star2f[sorted_index_star2f]
	star_list2f = star_list2f[sorted_index_star2f]
	X_mag_star1f = mag_star2f[0]
	if (nstar2f gt 1) then X_mag_star2f = mag_star2f[1]
	if (nstar2f gt 2) then X_mag_star3f = mag_star2f[2] 
	delta_star1f = calculate_distance(list2[index2], star_list2f[0])
	if (nstar2f gt 1) then delta_star2f = calculate_distance(list2[index2], star_list2f[1])
	if (nstar2f gt 2) then delta_star3f = calculate_distance(list2[index2], star_list2f[2])
endif else begin
	nstar2f = 0
endelse
nstar2i = n_elements(counts_star2i)
if (isa(nstar2i) and nstar2i gt 0) then begin
	mag_star2i = photo_calibrate(counts_star2i, A, B, C, D)
	sorted_index_star2i = sort(mag_star2i)
	mag_star2i = mag_star2i[sorted_index_star2i]
	star_list2i = star_list2i[sorted_index_star2i]
	X_mag_star1i = mag_star2i[0]
	if (nstar2i gt 1) then X_mag_star2i = mag_star2i[1]
	if (nstar2i gt 2) then X_mag_star3i = mag_star2i[2] 
	delta_star1i = calculate_distance(list1[index1], star_list2i[0])
	if (nstar2i gt 1) then delta_star2i = calculate_distance(list1[index1], star_list2i[1])
	if (nstar2i gt 2) then delta_star3i = calculate_distance(list1[index1], star_list2i[2])

endif else begin
	nstar2i = 0
endelse
ngalaxy2f = n_elements(counts_galaxy2f)
if (isa(ngalaxy2f) and ngalaxy2f gt 0) then begin
	mag_galaxy2f = photo_calibrate(counts_galaxy2f, A, B, C, D)
	sorted_index_galaxy2f = sort(mag_galaxy2f)
	mag_galaxy2f = mag_galaxy2f[sorted_index_galaxy2f]
	galaxy_list2f = galaxy_list2f[sorted_index_galaxy2f]
	X_mag_galaxy1f = mag_galaxy2f[0]
	if (ngalaxy2f gt 1) then X_mag_galaxy2f = mag_galaxy2f[1]
	if (ngalaxy2f gt 2) then X_mag_galaxy3f = mag_galaxy2f[2] 
	delta_galaxy1f = calculate_distance(list2[index2], galaxy_list2f[0])
	if (ngalaxy2f gt 1) then delta_galaxy2f = calculate_distance(list2[index2], galaxy_list2f[1])
	if (ngalaxy2f gt 2) then delta_galaxy3f = calculate_distance(list2[index2], galaxy_list2f[2])
endif else begin
	ngalaxy2f = 0
endelse
ngalaxy2i = n_elements(counts_galaxy2i)
if (isa(ngalaxy2i) and ngalaxy2i gt 0) then begin
	mag_galaxy2i = photo_calibrate(counts_galaxy2i, A, B, C, D)
	sorted_index_galaxy2i = sort(mag_galaxy2i)
	mag_galaxy2i = mag_galaxy2i[sorted_index_galaxy2i]
	galaxy_list2i = galaxy_list2i[sorted_index_galaxy2i]
	X_mag_galaxy1i = mag_galaxy2i[0]
	if (ngalaxy2i gt 1) then X_mag_galaxy2i = mag_galaxy2i[1]
	if (ngalaxy2i gt 2) then X_mag_galaxy3i = mag_galaxy2i[2] 
	delta_galaxy1i = calculate_distance(list1[index1], galaxy_list2i[0])
	if (ngalaxy2i gt 1) then delta_galaxy2i = calculate_distance(list1[index1], galaxy_list2i[1])
	if (ngalaxy2i gt 2) then delta_galaxy3i = calculate_distance(list1[index1], galaxy_list2i[2])

endif else begin
	ngalaxy2i = 0
endelse




if (keyword_set(galaxy) or keyword_set(LRG)) then begin

if (count1 ne 0) then begin 
r_dev_1 = list1[index1].r_dev[bndex]
r_exp_1 = list1[index1].r_exp[bndex]

I_dev_1 = replicate(!value_is_bad, n_elements(index1))
I_exp_1 = replicate(!value_is_bad, n_elements(index1))
IErr_dev_1 = replicate(!value_is_bad, n_elements(index1))
IErr_exp_1 = replicate(!value_is_bad, n_elements(index1))

ab_dev_1 = list1[index1].ab_dev[bndex]
ab_exp_1 = list1[index1].ab_exp[bndex]

phi_dev_1 = list1[index1].phi_dev[bndex]
phi_exp_1 = list1[index1].phi_exp[bndex]

n_small_ab = 0
small_ab_index = where(ab_exp_1 lt 1.0, n_small_ab)
if (n_small_ab gt 0) then ab_exp_1[small_ab_index] = 1.0 / ab_exp_1[small_ab_index]
n_small_ab = 0
small_ab_index = where(ab_dev_1 lt 1.0, n_small_ab)
if (n_small_ab gt 0) then ab_dev_1[small_ab_index] = 1.0 / ab_dev_1[small_ab_index]

e_dev_1 = sqrt(ab_dev_1 ^ 2 - 1.0) / ab_dev_1
e_exp_1 = sqrt(ab_exp_1 ^ 2 - 1.0) / ab_exp_1

scount = 0
sndex = where(r_dev_1 / sqrt(ab_dev_1) lt !DEV_RE_SMALL, scount)
if (scount gt 0) then r_dev_1[sndex] = !DEV_RE_SMALL * sqrt(ab_dev_1[sndex])
scount = 0
sndex = where(r_exp_1 / sqrt(ab_exp_1) lt !EXP_RE_SMALL, scount)
if (scount gt 0) then r_exp_1[sndex] = !EXP_RE_SMALL * sqrt(ab_exp_1[sndex])

npix_dev_1 = !pi * r_dev_1 ^ 2 / ab_dev_1
npix_exp_1 = !pi * r_exp_1 ^ 2 / ab_exp_1

	if (do_cd1) then begin

	r_halo_dev_1 = list1[index1].r_dev2[bndex]
	r_halo_exp_1 = list1[index1].r_exp2[bndex]
	ab_halo_dev_1 = list1[index1].ab_dev2[bndex]
	ab_halo_exp_1 = list1[index1].ab_exp2[bndex]

	phi_halo_dev_1 = list1[index1].phi_dev2[bndex]
	phi_halo_exp_1 = list1[index1].phi_exp2[bndex]

	if (tag_exist(list1[0], "COUNTS_PL")) then begin
		r_halo_dev_1 = list1[index1].r_pl[bndex]
		ab_halo_dev_1 = list1[index1].ab_pl[bndex]
		phi_halo_dev_1 = list1[index1].phi_pl[bndex]
	endif

	I_halo_dev_1 = replicate(!value_is_bad, n_elements(index1))
	I_halo_exp_1 = replicate(!value_is_bad, n_elements(index1))
	IErr_halo_dev_1 = replicate(!value_is_bad, n_elements(index1))
	IErr_halo_exp_1 = replicate(!value_is_bad, n_elements(index1))

	n_small_ab = 0
	small_ab_index = where(ab_halo_exp_1 lt 1.0, n_small_ab)
	if (n_small_ab gt 0) then ab_halo_exp_1[small_ab_index] = 1.0 / ab_halo_exp_1[small_ab_index]
	n_small_ab = 0
	small_ab_index = where(ab_halo_dev_1 lt 1.0, n_small_ab)
	if (n_small_ab gt 0) then ab_halo_dev_1[small_ab_index] = 1.0 / ab_halo_dev_1[small_ab_index]

	e_halo_dev_1 = sqrt(ab_halo_dev_1 ^ 2 - 1.0) / ab_halo_dev_1
	e_halo_exp_1 = sqrt(ab_halo_exp_1 ^ 2 - 1.0) / ab_halo_exp_1
	
	scount = 0
	sndex = where(r_halo_dev_1 / sqrt(ab_halo_dev_1) lt !DEV_RE_SMALL2, scount)
	if (scount gt 0) then r_halo_dev_1[sndex] = !DEV_RE_SMALL2 * sqrt(ab_halo_dev_1[sndex])
	scount = 0
	sndex = where(r_halo_exp_1 / sqrt(ab_halo_exp_1) lt !EXP_RE_SMALL2, scount)
	if (scount gt 0) then r_halo_exp_1[sndex] = !EXP_RE_SMALL2 * sqrt(ab_halo_exp_1[sndex])
 
	npix_halo_dev_1 = !pi * r_halo_dev_1 ^ 2 / ab_halo_dev_1
	npix_halo_exp_1 = !pi * r_halo_exp_1 ^ 2 / ab_halo_exp_1

	endif

endif

if (count2 ne 0) then begin

	if (do_cd2) then begin
		
	e_dev_2 = list2[index2].e_dev
	e_exp_2 = list2[index2].e_exp
	e_halo_dev_2 = list2[index2].e_dev2
	e_halo_exp_2 = list2[index2].e_exp2
	e_sersic_2 = list2[index2].e_sersic
	if (tag_exist(list2[0], "I_SERSIC1")) then begin
	e_sersic_inner_2 = list2[index2].e_sersic1
	endif else begin
	e_sersic_inner_2 = replicate(!value_is_bad, count2)
	endelse
	if (tag_exist(list2[0], "I_SERSIC2")) then begin
	e_sersic_outer_2 = list2[index2].e_sersic2
	endif else begin
	e_sersic_outer_2 = replicate(!value_is_bad, count2)
	endelse

	if (tag_exist(list2[0], "I_CORESERSIC")) then begin
	e_csersic_inner_2 = list2[index2].e_coresersic
	e_csersic_outer_2 = list2[index2].e_coresersic
	endif else begin
	e_csersic_inner_2 = replicate(!value_is_bad, count2)
	e_csersic_outer_2 = replicate(!value_is_bad, count2)
	endelse


	ab_dev_2 = 1 / sqrt(1.0 - e_dev_2 ^ 2)
	ab_exp_2 = 1 / sqrt(1.0 - e_exp_2 ^ 2)
	ab_halo_dev_2 = 1 / sqrt(1.0 - e_halo_dev_2 ^ 2)
	ab_halo_exp_2 = 1 / sqrt(1.0 - e_halo_exp_2 ^ 2)
	ab_sersic_2 = 1 / sqrt(1.0 - e_sersic_2 ^ 2)
	ab_sersic_inner_2 = 1 / sqrt(1.0 - e_sersic_inner_2 ^ 2)
	ab_sersic_outer_2 = 1 / sqrt(1.0 - e_sersic_outer_2 ^ 2)
	ab_csersic_inner_2 = 1 / sqrt(1.0 - e_csersic_inner_2 ^ 2)
	ab_csersic_outer_2 = 1 / sqrt(1.0 - e_csersic_outer_2 ^ 2)
	
	r_dev_2 = list2[index2].re_dev * sqrt(ab_dev_2)
	r_exp_2 = list2[index2].re_exp * sqrt(ab_exp_2)
	r_halo_dev_2 = list2[index2].re_dev2 * sqrt(ab_halo_dev_2)
	r_halo_exp_2 = list2[index2].re_exp2 * sqrt(ab_halo_exp_2)	
	r_sersic_2 = list2[index2].re_sersic * sqrt(ab_sersic_2)
	n_sersic_2 = list2[index2].n_sersic

	if (tag_exist(list2[0], "I_SERSIC1")) then begin
	r_sersic_inner_2 = list2[index2].re_sersic1 * sqrt(ab_sersic_inner_2)
	n_sersic_inner_2 = list2[index2].n_sersic1
	endif else begin
	r_sersic_inner_2 = replicate(!value_is_bad, count2)
	n_sersic_inner_2 = replicate(!value_is_bad, count2)
	endelse
	if (tag_exist(list2[0], "I_SERSIC2")) then begin
	r_sersic_outer_2 = list2[index2].re_sersic2 * sqrt(ab_sersic_outer_2)
	n_sersic_outer_2 = list2[index2].n_sersic2
	endif else begin
	r_sersic_outer_2 = replicate(!value_is_bad, count2)
	n_sersic_outer_2 = replicate(!value_is_bad, count2)
	endelse
	
	if (tag_exist(list2[0], "I_CORESERSIC")) then begin
	r_csersic_inner_2 = list2[index2].rb_coresersic * sqrt(ab_csersic_inner_2)
	n_csersic_inner_2 = list2[index2].gamma_coresersic
	r_csersic_outer_2 = list2[index2].re_coresersic * sqrt(ab_csersic_outer_2)
	n_csersic_outer_2 = list2[index2].n_coresersic
	alpha_csersic_2 = list2[index2].delta_coresersic
	endif else begin
	r_csersic_inner_2 = replicate(!value_is_bad, count2) 
	n_csersic_inner_2 = replicate(!value_is_bad, count2)
	r_csersic_outer_2 = replicate(!value_is_bad, count2)
	n_csersic_outer_2 = replicate(!value_is_bad, count2)
	alpha_csersic_2 = replicate(!value_is_bad, count2)
	endelse


	I_dev_2 = list2[index2].I_dev 
	I_exp_2 = list2[index2].I_exp 
	I_halo_dev_2 = list2[index2].I_dev2
	I_halo_exp_2 = list2[index2].I_exp2
	I_sersic_2 = list2[index2].I_sersic
	if (tag_exist(list2[0], "I_SERSIC1")) then begin
	I_sersic_inner_2 = list2[index2].I_sersic1
	endif else begin
	I_sersic_inner_2 = replicate(0.0, count2)
	endelse	
	if (tag_exist(list2[0], "I_SERSIC2")) then begin
	I_sersic_outer_2 = list2[index2].I_sersic2
	endif else begin
	I_sersic_outer_2 = replicate(0.0, count2)
	endelse	
	if (tag_exist(list2[0], "I_CORESERSIC")) then begin
	I_csersic_inner_2 = list2[index2].I_coresersic
	I_csersic_outer_2 = list2[index2].I_coresersic
	endif else begin
	I_csersic_inner_2 = replicate(0.0, count2)
	I_csersic_outer_2 = replicate(0.0, count2)
	endelse	


	IErr_dev_2 = list2[index2].IErr_dev 
	IErr_exp_2 = list2[index2].IErr_exp 
	IErr_halo_dev_2 = list2[index2].IErr_dev2
	IErr_halo_exp_2 = list2[index2].IErr_exp2
	IErr_sersic_2 = list2[index2].IErr_sersic
	
	if (tag_exist(list2[0], "I_SERSIC1")) then begin
	IErr_sersic_inner_2 = list2[index2].IErr_sersic1
	endif else begin
	IErr_sersic_inner_2 = replicate(0.0, count2)
	endelse
	if (tag_exist(list2[0], "I_SERSIC2")) then begin
	IErr_sersic_outer_2 = list2[index2].IErr_sersic2
	endif else begin
	IErr_sersic_outer_2 = replicate(0.0, count2)
	endelse
	if (tag_exist(list2[0], "I_CORESERSIC")) then begin
	IErr_csersic_inner_2 = list2[index2].IErr_coresersic
	IErr_csersic_outer_2 = list2[index2].IErr_coresersic
	endif else begin
	IErr_csersic_inner_2 = replicate(0.0, count2)
	IErr_csersic_outer_2 = replicate(0.0, count2)
	endelse


	bad_angle_count = 0
	bad_angle_index = where(list2[index2].phi_deV eq !value_is_bad, bad_angle_count)
	phi_dev_2 = 180.00 / !pi * atan(tan(list2[index2].phi_dev))
	if (bad_angle_count gt 0) then phi_dev_2[index2[bad_angle_index]] = !value_is_bad

	bad_angle_count = 0
	bad_angle_index = where(list2[index2].phi_exp eq !value_is_bad, bad_angle_count)	
	phi_exp_2 = 180.00 / !pi * atan(tan(list2[index2].phi_exp))
	if (bad_angle_count gt 0) then phi_exp_2[index2[bad_angle_index]] = !value_is_bad

	bad_angle_count = 0
	bad_angle_index = where(list2[index2].phi_dev2 eq !value_is_bad, bad_angle_count)		
	phi_halo_dev_2 = 180.0 / !pi * atan(tan(list2[index2].phi_dev2))
	if (bad_angle_count gt 0) then phi_halo_dev_2[index2[bad_angle_index]] =!value_is_bad

	bad_angle_count = 0
	bad_angle_index = where(list2[index2].phi_exp2 eq !value_is_bad, bad_angle_count)			
	phi_halo_exp_2 = 180.0 / !pi * atan(tan(list2[index2].phi_exp2))
	if (bad_angle_count gt 0) then phi_halo_exp_2[index2[bad_angle_index]] =!value_is_bad

	bad_angle_count = 0
	bad_angle_index = where(list2[index2].phi_sersic eq !value_is_bad, bad_angle_count)			
	phi_sersic_2 = 180.0 / !pi * atan(tan(list2[index2].phi_sersic))
	if (bad_angle_count gt 0) then phi_sersic_2[index2[bad_angle_index]] =!value_is_bad

	if (tag_exist(list2, "I_SERSIC1")) then begin

		bad_angle_count = 0
		bad_angle_index = where(list2[index2].phi_sersic1 eq !value_is_bad, bad_angle_count)			
		phi_sersic_inner_2 = 180.0 / !pi * atan(tan(list2[index2].phi_sersic1))
		if (bad_angle_count gt 0) then phi_sersic_inner_2[index2[bad_angle_index]] =!value_is_bad

	endif else begin
		phi_sersic_inner_2 = replicate(!value_is_bad, count2) 
	endelse

	if (tag_exist(list2, "I_SERSIC2")) then begin

		bad_angle_count = 0
		bad_angle_index = where(list2[index2].phi_sersic2 eq !value_is_bad, bad_angle_count)			
		phi_sersic_outer_2 = 180.0 / !pi * atan(tan(list2[index2].phi_sersic2))
		if (bad_angle_count gt 0) then phi_sersic_outer_2[index2[bad_angle_index]] =!value_is_bad

	endif else begin
		phi_sersic_outer_2 = replicate(!value_is_bad, count2) 
	endelse
	if (tag_exist(list2, "I_CORESERSIC")) then begin

		bad_angle_count = 0
		bad_angle_index = where(list2[index2].phi_coresersic eq !value_is_bad, bad_angle_count)			
		phi_csersic_outer_2 = 180.0 / !pi * atan(tan(list2[index2].phi_coresersic))
		if (bad_angle_count gt 0) then phi_csersic_outer_2[index2[bad_angle_index]] =!value_is_bad
		
		bad_angle_count = 0
		bad_angle_index = where(list2[index2].phi_coresersic eq !value_is_bad, bad_angle_count)			
		phi_csersic_inner_2 = 180.0 / !pi * atan(tan(list2[index2].phi_coresersic))
		if (bad_angle_count gt 0) then phi_csersic_inner_2[index2[bad_angle_index]] =!value_is_bad

	endif else begin
		phi_csersic_inner_2 = replicate(!value_is_bad, count2) 
		phi_csersic_outer_2 = replicate(!value_is_bad, count2) 
	endelse
	


	npix_dev_2 = !pi * r_dev_2 ^ 2 / ab_dev_2
	npix_exp_2 = !pi * r_exp_2 ^ 2 / ab_exp_2
	npix_halo_dev_2 = !pi * r_halo_dev_2 ^ 2 / ab_halo_dev_2
	npix_halo_exp_2 = !pi * r_halo_exp_2 ^ 2 / ab_halo_exp_2
	npix_sersic_2 = !pi * r_sersic_2 ^ 2 / ab_sersic_2
	npix_sersic_inner_2 = !pi * r_sersic_inner_2 ^ 2 / ab_sersic_inner_2
	npix_sersic_outer_2 = !pi * r_sersic_outer_2 ^ 2 / ab_sersic_outer_2
	npix_csersic_inner_2 = !pi * r_csersic_inner_2 ^ 2 / ab_csersic_inner_2
	npix_csersic_outer_2 = !pi * r_csersic_outer_2 ^ 2 / ab_csersic_outer_2

	if (n3 gt 0) then begin
	e_dev_3 = list3[index2].e_dev
	e_exp_3 = list3[index2].e_exp
	e_halo_dev_3 = list3[index2].e_dev2
	e_halo_exp_3 = list3[index2].e_exp2
	
	ab_dev_3 = 1 / sqrt(1.0 - e_dev_3 ^ 2)
	ab_exp_3 = 1 / sqrt(1.0 - e_exp_3 ^ 2)
	ab_halo_dev_3 = 1 / sqrt(1.0 - e_halo_dev_3 ^ 2)
	ab_halo_exp_3 = 1 / sqrt(1.0 - e_halo_exp_3 ^ 2)
	
	r_dev_3 = list3[index2].re_dev * sqrt(ab_dev_3)
	r_exp_3 = list3[index2].re_exp * sqrt(ab_exp_3)
	r_halo_dev_3 = list3[index2].re_dev2 * sqrt(ab_halo_dev_3)
	r_halo_exp_3 = list3[index2].re_exp2 * sqrt(ab_halo_exp_3)	

	I_dev_3 = list3[index2].I_dev
	I_exp_3 = list3[index2].I_exp
	I_halo_dev_3 = list3[index2].I_dev2 
	I_halo_exp_3 = list3[index2].I_exp2
	IErr_dev_3 = list3[index2].IErr_dev
	IErr_exp_3 = list3[index2].IErr_exp
	IErr_halo_dev_3 = list3[index2].IErr_dev2 
	IErr_halo_exp_3 = list3[index2].IErr_exp2


	phi_dev_3 = 180.00 / !pi * atan(tan(list3[index2].phi_dev))
	phi_exp_3 = 180.00 / !pi * atan(tan(list3[index2].phi_exp))
	phi_halo_dev_3 = 180.0 / !pi * atan(tan(list3[index2].phi_dev2))
	phi_halo_exp_3 = 180.0 / !pi * atan(tan(list3[index2].phi_exp2))

	npix_dev_3 = !pi * r_dev_3 ^ 2 / ab_dev_3
	npix_exp_3 = !pi * r_exp_3 ^ 2 / ab_exp_3
	npix_halo_dev_3 = !pi * r_halo_dev_3 ^ 2 / ab_halo_dev_3
	npix_halo_exp_3 = !pi * r_halo_exp_3 ^ 2 / ab_halo_exp_3

	endif

	endif else begin

	r_dev_2 = list2[index2].r_dev[bndex]
	r_exp_2 = list2[index2].r_exp[bndex]
	ab_dev_2 = list2[index2].ab_dev[bndex]
	ab_exp_2 = list2[index2].ab_exp[bndex]

	scount = 0;
	sndex = where(ab_dev_2 lt 1.0, scount)
	if (scount gt 0) then ab_dev_2[sndex] = 1.0 / ab_dev_2[sndex]
	scount = 0;
	sndex = where(ab_exp_2 lt 1.0, scount)
	if (scount gt 0) then ab_exp_2[sndex] = 1.0 / ab_exp_2[sndex]

	e_dev_2 = sqrt(ab_dev_2 ^ 2 - 1.0) / ab_dev_2
	e_exp_2 = sqrt(ab_exp_2 ^ 2 - 1.0) / ab_exp_2

	phi_dev_2 = list2[index2].phi_dev[bndex]
	phi_exp_2 = list2[index2].phi_exp[bndex]

	I_dev_2 = replicate(!value_is_bad, n_elements(index2))
	I_exp_2 = replicate(!value_is_bad, n_elements(index2))
	IErr_dev_2 = replicate(!value_is_bad, n_elements(index2))
	IErr_exp_2 = replicate(!value_is_bad, n_elements(index2))

	npix_dev_2 = !pi * r_dev_2 ^ 2 / ab_dev_2
	npix_exp_2 = !pi * r_exp_2 ^ 2 / ab_exp_2

	endelse

endif
;;scount = 0
;;sndex = where(r_dev_2 lt !DEV_RE_SMALL, scount)
;;if (scount gt 0) then r_dev_2[sndex] = !DEV_RE_SMALL
;;scount = 0
;;sndex = where(r_exp_2 lt !EXP_RE_SMALL, scount)
;;if (scount gt 0) then r_exp_2[sndex] = !EXP_RE_SMALL

if (count1 ne 0) then begin

	dev_frac_1 = list1[index1].fracpsf[bndex]
	r1 = (counts_deV1 * dev_frac_1 * r_dev_1 + counts_exp1 * (1.0-dev_frac_1) * r_exp_1) / counts1
	rr1 = r1
	r1 *= !PIXELSIZE
	r1 = alog10(r1)
	ab1 = (counts_deV1 * dev_frac_1 * ab_dev_1 + counts_exp1 * (1.0-dev_frac_1) * ab_exp_1) / counts1
	e1 = (counts_deV1 * dev_frac_1 * e_dev_1 + counts_exp1 * (1.0-dev_frac_1) * e_exp_1) / counts1
	phi1 = (counts_deV1 * dev_frac_1 * phi_dev_1 + counts_exp1 * (1.0-dev_frac_1) * phi_exp_1) / counts1
	fracPSF1 = dev_frac_1

	npix1 = (counts_deV1 * dev_frac_1 * npix_dev_1 + counts_exp1 * (1.0-dev_frac_1) * npix_exp_1) / counts1
	fraction_halo_in_core1 = replicate(0.0, count1)
	if (sky1 ne !value_is_bad) then begin
		SN1 = (counts1 * 0.5) / sqrt(counts1 * 0.5 + sky1 * npix1 + counts_halo1 * fraction_halo_in_core1)
	endif else begin
		SN1 = replicate(!value_is_bad, count1)
	endelse

	II1 = (dev_frac_1 * I_dev_1 + (1.0-dev_frac_1) * I_exp_1)
	IErr1 = (dev_frac_1 * IErr_dev_1 + (1.0-dev_frac_1) * IErr_exp_1)

	
	nan_count = 0
	nan_index = where(finite(r1) eq 0, nan_count)
	if (nan_count gt 0) then begin
		r1[nan_index] = !value_is_bad
		ab1[nan_index] = !value_is_bad
		e1[nan_index] = !value_is_bad
		phi1[nan_index] = !value_is_bad
		SN1[nan_index] = !value_is_bad
	endif

	if (do_cd1) then begin

		;; dev_halo_frac_1 = list1[index1].fracpsf2[bndex]
		;; r1 = (counts_halo_deV1 * dev_halo_frac_1 * r_halo_dev_1 + counts_halo_exp1 * (1.0-dev_halo_frac_1) * r_halo_exp_1) / counts_halo1
		r_halo1 = r_halo_dev_1
		rr_halo1 = r_halo1
		r_halo1 *= !PIXELSIZE
		r_halo1 = alog10(r_halo1)
		ab_halo1 = ab_halo_dev_1
		e_halo1 = e_halo_dev_1
		phi_halo1 = phi_halo_dev_1
		npix_halo1 = npix_halo_dev_1

		I_halo1 = I_halo_dev_1
		IErr_halo1 = IErr_halo_dev_1

		fraction_halo_in_core1 = find_fraction_prof_in_prof(r1 = rr_halo1, r2 = rr1, n1 = 4.0, k1 = !k_dev)
		fraction_core_in_halo1 = find_fraction_prof_in_prof(r1 = rr1, r2 = rr_halo1, n1 = 4.0, k1 = !k_dev)
		if (sky1 ne !value_is_bad) then begin
			SN1 = (counts1 * 0.5) / sqrt(counts1 * 0.5 + sky1 * npix1 + counts_halo1 * fraction_halo_in_core1)
			SN_halo1 = (counts_halo1 * 0.5) / sqrt(counts_halo1 * 0.5 + sky1 * npix_halo1 + counts1 * fraction_core_in_halo1)
			nancount = 0
			nandex = where(fraction_halo_in_core1 lt 0.0 or fraction_core_in_halo1 lt 0.0, nancount)
			if (nancount gt 0) then begin
				SN1[nandex] = !value_is_bad
				SN_halo1[nandex] = !value_is_bad
			endif
		endif else begin
			SN1 = replicate(!value_is_bad, count1)
			SN_halo1 = replicate(!value_is_bad, count1)
		endelse
		
		nan_count = 0
		nan_index = where(finite(r_halo1) eq 0, nan_count)
        	if (nan_count gt 0) then begin
			r_halo1[nan_index] = !value_is_bad
			ab_halo1[nan_index] = !value_is_bad
			e_halo1[nan_index] = !value_is_bad
			phi_halo1[nan_index] = !value_is_bad
			SN_halo1[nan_index] = !value_is_bad
		endif
	
		r_sersic1 = replicate(!VALUE_IS_BAD, count1)
		ab_sersic1 = replicate(!VALUE_IS_BAD, count1)
		e_sersic1 = replicate(!VALUE_IS_BAD, count1)
		phi_sersic1 = replicate(!VALUE_IS_BAD, count1)
		SN_sersic1 = replicate(!VALUE_IS_BAD, count1)
		I_sersic1 = replicate(!VALUE_IS_BAD, count1)
		IErr_sersic1 = replicate(!VALUE_IS_BAD, count1)	

		r_sersic_inner1 = replicate(!VALUE_IS_BAD, count1)
		ab_sersic_inner1 = replicate(!VALUE_IS_BAD, count1)
		e_sersic_inner1 = replicate(!VALUE_IS_BAD, count1)
		phi_sersic_inner1 = replicate(!VALUE_IS_BAD, count1)
		SN_sersic_inner1 = replicate(!VALUE_IS_BAD, count1)
		I_sersic_inner1 = replicate(!VALUE_IS_BAD, count1)
		IErr_sersic_inner1 = replicate(!VALUE_IS_BAD, count1)	

		r_sersic_outer1 = replicate(!VALUE_IS_BAD, count1)
		ab_sersic_outer1 = replicate(!VALUE_IS_BAD, count1)
		e_sersic_outer1 = replicate(!VALUE_IS_BAD, count1)
		phi_sersic_outer1 = replicate(!VALUE_IS_BAD, count1)
		SN_sersic_outer1 = replicate(!VALUE_IS_BAD, count1)
		I_sersic_outer1 = replicate(!VALUE_IS_BAD, count1)
		IErr_sersic_outer1 = replicate(!VALUE_IS_BAD, count1)	

		r_csersic_inner1 = replicate(!VALUE_IS_BAD, count1)
		ab_csersic_inner1 = replicate(!VALUE_IS_BAD, count1)
		e_csersic_inner1 = replicate(!VALUE_IS_BAD, count1)
		phi_csersic_inner1 = replicate(!VALUE_IS_BAD, count1)
		SN_csersic_inner1 = replicate(!VALUE_IS_BAD, count1)
		I_csersic_inner1 = replicate(!VALUE_IS_BAD, count1)
		IErr_csersic_inner1 = replicate(!VALUE_IS_BAD, count1)	

		r_csersic_outer1 = replicate(!VALUE_IS_BAD, count1)
		ab_csersic_outer1 = replicate(!VALUE_IS_BAD, count1)
		e_csersic_outer1 = replicate(!VALUE_IS_BAD, count1)
		phi_csersic_outer1 = replicate(!VALUE_IS_BAD, count1)
		SN_csersic_outer1 = replicate(!VALUE_IS_BAD, count1)
		I_csersic_outer1 = replicate(!VALUE_IS_BAD, count1)
		IErr_csersic_outer1 = replicate(!VALUE_IS_BAD, count1)	

	endif else begin

		r_halo1 = replicate(!VALUE_IS_BAD, count1)
		ab_halo1 = replicate(!VALUE_IS_BAD, count1)
		e_halo1 = replicate(!VALUE_IS_BAD, count1)
		phi_halo1 = replicate(!VALUE_IS_BAD, count1)
		SN_halo1 = replicate(!VALUE_IS_BAD, count1)
		I_halo1 = replicate(!VALUE_IS_BAD, count1)
		IErr_halo1 = replicate(!VALUE_IS_BAD, count1)	
	
		r_sersic1 = replicate(!VALUE_IS_BAD, count1)
		ab_sersic1 = replicate(!VALUE_IS_BAD, count1)
		e_sersic1 = replicate(!VALUE_IS_BAD, count1)
		phi_sersic1 = replicate(!VALUE_IS_BAD, count1)
		SN_sersic1 = replicate(!VALUE_IS_BAD, count1)
		I_sersic1 = replicate(!VALUE_IS_BAD, count1)
		IErr_sersic1 = replicate(!VALUE_IS_BAD, count1)	

		r_sersic_inner1 = replicate(!VALUE_IS_BAD, count1)
		ab_sersic_inner1 = replicate(!VALUE_IS_BAD, count1)
		e_sersic_inner1 = replicate(!VALUE_IS_BAD, count1)
		phi_sersic_inner1 = replicate(!VALUE_IS_BAD, count1)
		SN_sersic_inner1 = replicate(!VALUE_IS_BAD, count1)
		I_sersic_inner1 = replicate(!VALUE_IS_BAD, count1)
		IErr_sersic_inner1 = replicate(!VALUE_IS_BAD, count1)	

		r_sersic_outer1 = replicate(!VALUE_IS_BAD, count1)
		ab_sersic_outer1 = replicate(!VALUE_IS_BAD, count1)
		e_sersic_outer1 = replicate(!VALUE_IS_BAD, count1)
		phi_sersic_outer1 = replicate(!VALUE_IS_BAD, count1)
		SN_sersic_outer1 = replicate(!VALUE_IS_BAD, count1)
		I_sersic_outer1 = replicate(!VALUE_IS_BAD, count1)
		IErr_sersic_outer1 = replicate(!VALUE_IS_BAD, count1)	
		
		r_csersic_inner1 = replicate(!VALUE_IS_BAD, count1)
		ab_csersic_inner1 = replicate(!VALUE_IS_BAD, count1)
		e_csersic_inner1 = replicate(!VALUE_IS_BAD, count1)
		phi_csersic_inner1 = replicate(!VALUE_IS_BAD, count1)
		SN_csersic_inner1 = replicate(!VALUE_IS_BAD, count1)
		I_csersic_inner1 = replicate(!VALUE_IS_BAD, count1)
		IErr_csersic_inner1 = replicate(!VALUE_IS_BAD, count1)	

		r_csersic_outer1 = replicate(!VALUE_IS_BAD, count1)
		ab_csersic_outer1 = replicate(!VALUE_IS_BAD, count1)
		e_csersic_outer1 = replicate(!VALUE_IS_BAD, count1)
		phi_csersic_outer1 = replicate(!VALUE_IS_BAD, count1)
		SN_csersic_outer1 = replicate(!VALUE_IS_BAD, count1)
		I_csersic_outer1 = replicate(!VALUE_IS_BAD, count1)
		IErr_csersic_outer1 = replicate(!VALUE_IS_BAD, count1)	

	endelse
endif
if (count2 ne 0) then begin
	if (do_cd2) then begin
		;; print, 'ç', format='(A, $)'
		dev_frac_2 = replicate(1.0, count2)
		r2 = (counts_deV2 * r_dev_2 + counts_exp2 * r_exp_2) / (counts2)
		;;r2 *= !PIXELSIZE
		ab2 = (counts_deV2 * ab_dev_2 + counts_exp2 * ab_exp_2) / (counts2)
		e2 = (counts_deV2 * e_dev_2 + counts_exp2 * e_exp_2) / (counts2)
		phi2 = (counts_deV2 * phi_dev_2 + counts_exp2 * phi_exp_2) / (counts2)
		;; this should work with zero and non-zero
		I2 = (counts_deV2 * I_dev_2 + counts_exp2 * I_exp_2) / (counts2)
		IErr2 = (counts_deV2 * IErr_dev_2 + counts_exp2 * IErr_exp_2) / (counts2)

		npix2 = (counts_deV2 * dev_frac_2 * npix_dev_2 + counts_exp2 * (1.0-dev_frac_2) * npix_exp_2) / counts2
		if (sky2 ne !value_is_bad) then begin
			SN2 = (counts2 * 0.5) / sqrt(counts2 * 0.5 + sky2 * npix2 + counts_halo2 * fraction_halo_in_core2)
		endif else begin
			SN2 = replicate(!value_is_bad, count2)
		endelse

	
		r_halo2 = (counts_halo_deV2 * r_halo_dev_2 + counts_halo_exp2 * r_halo_exp_2) / (counts_halo2)
		r_halo2 *= !PIXELSIZE
		r_halo2 = alog10(r_halo2)
		ab_halo2 = (counts_halo_deV2 * ab_halo_dev_2 + counts_halo_exp2 * ab_halo_exp_2) / (counts_halo2)
		e_halo2 = (counts_halo_deV2 * e_halo_dev_2 + counts_halo_exp2 * e_halo_exp_2) / (counts_halo2)
		phi_halo2 = (counts_halo_deV2 * phi_halo_dev_2 + counts_halo_exp2 * phi_halo_exp_2) / (counts_halo2)
		;; this should work with zero and non-zero pair	
		I_halo2 = (counts_halo_deV2 * I_halo_dev_2 + counts_halo_exp2 * I_halo_exp_2) / (counts_halo2)
		IErr_halo2 = (counts_halo_deV2 * IErr_halo_dev_2 + counts_halo_exp2 * IErr_halo_exp_2) / (counts_halo2)
		
		npix_halo2 = (counts_halo_deV2 * dev_frac_2 * npix_dev_2 + counts_halo_exp2 * (1.0-dev_frac_2) * npix_exp_2) / counts_halo2
		if (sky2 ne !value_is_bad) then begin
			SN_halo2 = (counts_halo2 * 0.5) / sqrt(counts_halo2 * 0.5 + sky2 * npix_halo2 + counts2 * fraction_core_in_halo2)
		endif else begin
			SN_halo2 = replicate(!value_is_bad, count2)
		endelse


		r_sersic_2 *= !pixelsize
		r_sersic_2 = alog10(r_sersic_2)
		r_sersic_inner_2 *= !pixelsize
		r_sersic_inner_2 = alog10(r_sersic_inner_2)
		r_sersic_outer_2 *= !pixelsize
		r_sersic_outer_2 = alog10(r_sersic_outer_2)
		r_csersic_inner_2 *= !pixelsize
		r_csersic_inner_2 = alog10(r_csersic_inner_2)
		r_csersic_outer_2 *= !pixelsize
		r_csersic_outer_2 = alog10(r_csersic_outer_2)


		nan_count = 0
		nan_index = where(finite(r_halo2) eq 0, nan_count)
	
		;; print, '˜', format='(A, $)'
		;; print, nan_count, format='(I, $)'
		;; print, [min(IErr_halo2), max(IErr_halo2)], format='(2F, $)'
		if (nan_count gt 0) then begin
			r_halo2[nan_index] = !value_is_bad
			ab_halo2[nan_index] = !value_is_bad
			e_halo2[nan_index] = !value_is_bad
			phi_halo2[nan_index] = !value_is_bad
			SN_halo2[nan_index] = !value_is_bad
			I_halo2[nan_index] = !value_is_bad
			IErr_halo2[nan_index] = !value_is_bad
		endif
		if (n3 gt 0) then begin
	
		dev_frac_3 = replicate(1.0, count2)
		r3 = (counts_deV3 * r_dev_3 + counts_exp3 * r_exp_3) / (counts3)
		r3 *= !PIXELSIZE
		r3 = alog10(r3)
		ab3 = (counts_deV3 * ab_dev_3 + counts_exp3 * ab_exp_3) / (counts3)
		e3 = (counts_deV3 * e_dev_3 + counts_exp3 * e_exp_3) / (counts3)
		phi3 = (counts_deV3 * phi_dev_3 + counts_exp3 * phi_exp_3) / (counts3)
		
		I3 = (counts_deV3 * I_dev_3 + counts_exp3 * I_exp_3) / (counts3)
		IErr3 = (counts_deV3 * IErr_dev_3 + counts_exp3 * IErr_exp_3) / (counts3)

		npix3 = (counts_deV3 * npix_dev_3 + counts_exp3 * npix_exp_3) / counts3
		if (sky3 ne !value_is_bad) then begin
			SN3 = (counts3 * 0.5) / sqrt(counts3 * 0.5 + sky3 * npix3 + counts_halo3 * fraction_halo_in_core3)
		endif else begin
			SN3 = replicate(!value_is_bad, count2)
		endelse


		r_halo3 = (counts_halo_deV3 * r_halo_dev_3 + counts_halo_exp3 * r_halo_exp_3) / (counts_halo3)
		r_halo3 *= !PIXELSIZE
		r_halo3 = alog10(r_halo3)
		ab_halo3 = (counts_halo_deV3 * ab_halo_dev_3 + counts_halo_exp3 * ab_halo_exp_3) / (counts_halo3)
		e_halo3 = (counts_halo_deV3 * e_halo_dev_3 + counts_halo_exp3 * e_halo_exp_3) / (counts_halo3)
		phi_halo3 = (counts_halo_deV3 * phi_halo_dev_3 + counts_halo_exp3 * phi_halo_exp_3) / (counts_halo3)
	
		I_halo3 = (counts_halo_deV3 * I_halo_dev_3 + counts_halo_exp3 * I_halo_exp_3) / (counts_halo3)
		IErr_halo3 = (counts_halo_deV3 * IErr_halo_dev_3 + counts_halo_exp3 * IErr_halo_exp_3) / (counts_halo3)

		npix_halo3 = (counts_halo_deV3 * npix_halo_dev_3 + counts_halo_exp3 * npix_halo_exp_3) / counts_halo3
		if (sky3 ne !value_is_bad) then begin
			SN_halo3 = (counts_halo3 * 0.5) / sqrt(counts_halo3 * 0.5 + sky3 * npix_halo3 + counts3 * fraction_core_in_halo3)
		endif else begin
			SN_halo3 = replicate(!value_is_bad, count2)
		endelse


		nan_count = 0
		nan_index = where(finite(r_halo3) eq 0, nan_count)
		if (nan_count gt 0) then begin
			r_halo3[nan_index] = !value_is_bad
			ab_halo3[nan_index] = !value_is_bad
			e_halo3[nan_index] = !value_is_bad
			phi_halo3[nan_index] = !value_is_bad
			SN_halo3[nan_index] = !value_is_bad
		endif

		endif

	endif else begin
		dev_frac_2 = list2[index2].fracpsf[bndex]
		if (do_fake_cd2) then begin	
			print, '∂', format='(A, $)'
			r_halo2 = replicate(!VALUE_IS_BAD, count2)
			r2 = replicate(!VALUE_IS_BAD, count2)
			ab2 = replicate(!value_is_bad, count2)
			ab_halo2 = replicate(!value_is_bad, count2)
			e2 = replicate(!value_is_bad, count2)
			e_halo2 = replicate(!value_is_bad, count2)
			phi2 = replicate(!value_is_bad, count2)
			phi_halo2 = replicate(!value_is_bad, count2)
			SN_halo2 = replicate(!value_is_bad, count2)
			I_halo2 = replicate(!VALUE_IS_BAD, count2)
			I2 = replicate(!VALUE_IS_BAD, count2)
			IErr_halo2 = replicate(!VALUE_IS_BAD, count2)
			IErr2 = replicate(!VALUE_IS_BAD, count2)
		
			nhalo_deV_index = 0
			ncore_deV_index = 0
			halo_deV_index = where(r_deV_2 gt r_exp_2, nhalo_deV_index)
			core_deV_index = where(r_deV_2 le r_exp_2, ncore_deV_index)
			if (nhalo_deV_index gt 0) then begin
				r_halo2[halo_deV_index] = r_deV_2[halo_deV_index] 
				r2[halo_deV_index] = r_exp2[halo_deV_index]
				ab_halo2[halo_deV_index] = ab_deV_2[halo_deV_index] 
				ab2[halo_deV_index] = ab_exp_2[halo_deV_index]
				e_halo2[halo_deV_index] = e_deV_2[halo_deV_index] 
				e2[halo_deV_index] = e_exp_2[halo_deV_index]
				phi_halo2[halo_deV_index] = phi_deV_2[halo_deV_index] 
				phi2[halo_deV_index] = phi_exp_2[halo_deV_index]
				SN_halo2[halo_deV_index] = SN_deV_2[halo_deV_index]
				SN2[[halo_deV_index]] = SN_exp_2[halo_deV_index]

				I_halo2[halo_deV_index] = I_deV_2[halo_deV_index] 
				I2[halo_deV_index] = I_exp2[halo_deV_index]	
				IErr_halo2[halo_deV_index] = IErr_deV_2[halo_deV_index] 
				IErr2[halo_deV_index] = IErr_exp2[halo_deV_index]



			endif 
			if (ncore_deV_index gt 0) then begin
				r_halo2[core_deV_index] = r_exp_2[core_deV_index] 
				r2[core_deV_index] = r_deV_2[core_deV_index]
				ab_halo2[core_deV_index] = ab_exp_2[core_deV_index] 
				ab2[core_deV_index] = ab_deV_2[core_deV_index]
				e_halo2[core_deV_index] = e_exp_2[core_deV_index] 
				e2[core_deV_index] = e_deV_2[core_deV_index]
				phi_halo2[core_deV_index] = phi_exp_2[core_deV_index] 
				phi2[core_deV_index] = phi_deV_2[core_deV_index]
				SN_halo2[core_deV_index] = SN_exp_2[core_deV_index] 
				SN2[core_deV_index] = SN_deV_2[core_deV_index]

				I_halo2[core_deV_index] = I_exp_2[core_deV_index] 
				I2[core_deV_index] = I_deV_2[core_deV_index]
				IErr_halo2[core_deV_index] = IErr_exp_2[core_deV_index] 
				IErr2[core_deV_index] = IErr_deV_2[core_deV_index]

			endif
			r_halo2 *= !PIXELSIZE
			r_halo2 = alog10(r_halo2)

		endif else begin
		r2 = (counts_deV2 * dev_frac_2 * r_dev_2 + counts_exp2 * (1.0-dev_frac_2) * r_exp_2) / counts2
		ab2 = (counts_deV2 * dev_frac_2 * ab_dev_2 + counts_exp2 * (1.0-dev_frac_2) * ab_exp_2) / counts2
		e2 = (counts_deV2 * dev_frac_2 * e_dev_2 + counts_exp2 * (1.0-dev_frac_2) * e_exp_2) / counts2
		phi2 = (counts_deV2 * dev_frac_2 * phi_dev_2 + counts_exp2 * (1.0-dev_frac_2) * phi_exp_2) / counts2


		I2 = (dev_frac_2 * I_dev_2 + (1.0-dev_frac_2) * I_exp_2)
		IErr2 = (dev_frac_2 * IErr_dev_2 + (1.0-dev_frac_2) * IErr_exp_2)

		npix2 = (counts_deV2 * dev_frac_2 * npix_dev_2 + counts_exp2 * (1.0-dev_frac_2) * npix_exp_2) / counts2
		if (sky2 ne !value_is_bad) then begin
			SN2 = (counts2 * 0.5) / sqrt(counts2 * 0.5 + sky2 * npix2 + counts_halo2 * fraction_halo_in_core2)
		endif else begin
			SN2 = replicate(!value_is_bad, count2)
		endelse
	
		r_halo2 = replicate(!VALUE_IS_BAD, count2)
		ab_halo2 = replicate(!VALUE_IS_BAD, count2)
		e_halo2 = replicate(!VALUE_IS_BAD, count2)
		phi_halo2 = replicate(!VALUE_IS_BAD, count2)
		SN_halo2 = replicate(!VALUE_IS_BAD, count2)

		I_halo2 = replicate(!VALUE_IS_BAD, count2)
		IErr_halo2 = replicate(!VALUE_IS_BAD, count2)
		endelse
		nan_count = 0
		nan_index = where(finite(r_halo2) eq 0, nan_count)
		if (nan_count gt 0) then begin
			r_halo2[nan_index] = !value_is_bad
			ab_halo2[nan_index] = !value_is_bad
			e_halo2[nan_index] = !value_is_bad
			phi_halo2[nan_index] = !value_is_bad
			SN_halo2[nan_index] = !value_is_bad
			
		endif
	endelse

	r2 *= !PIXELSIZE
	r2 = alog10(r2)
	nan_count = 0
	nan_index = where(finite(r2) eq 0, nan_count)
	if (nan_count gt 0) then begin
		r2[nan_index] = !value_is_bad
		e2[nan_index] = !value_is_bad
		ab2[nan_index] = !value_is_bad
		phi2[nan_index] = !value_is_bad
		SN2[nan_index] = !value_is_bad
	endif
	cdmatch = replicate(0L, count2)
	good_value_count = 0
	good_value_index = where(r2 ne !value_is_bad and r_halo2 ne !value_is_bad, good_value_count);
	if (good_value_count gt 0) then begin
		cdmatch[good_value_index] = 1L
	endif	
endif

endif
if (keyword_set(boxplots) and !graphical_output) then begin

;; now create the boxplots


mag1_bp = CREATEBOXPLOTDATA(mag1)
mag2_bp = CREATEBOXPLOTDATA(mag2)
match1_mag_bp = CREATEBOXPLOTDATA(mag1[match_index1])
non_match1_mag_bp = CREATEBOXPLOTDATA(mag1[non_match_index1])
match2_mag_bp = CREATEBOXPLOTDATA(mag2[match_index2])
non_match2_mag_bp = CREATEBOXPLOTDATA(mag2[non_match_index2])
error_mag = mag2[match_index2] - mag1[clipped_match_index[match_index2]]
error_mag_bp = CREATEBOXPLOTDATA(error_mag)

if (keyword_set(galaxy) or keyword_set(LRG)) then begin 
r1_bp = CREATEBOXPLOTDATA(r1)
r2_bp = CREATEBOXPLOTDATA(r2)
match1_r_bp = CREATEBOXPLOTDATA(r1[match_index1])
non_match1_r_bp = CREATEBOXPLOTDATA(r1[non_match_index1])
match2_r_bp = CREATEBOXPLOTDATA(r2[match_index2])
non_match2_r_bp = CREATEBOXPLOTDATA(r2[non_match_index2])
error_r = (r2[match_index2]) - (r1[clipped_match_index[match_index2]])
error_r_bp = CREATEBOXPLOTDATA(error_r)
endif


;; box plot info for all categories
mag_bp_arr = [[mag1_bp], [mag2_bp], [match1_mag_bp], [match2_mag_bp], [non_match1_mag_bp], [non_match2_mag_bp]] ;;, [error_bp]]
mag_bp_arr = transpose(mag_bp_arr)
labels = ['sim', 'PH', 'match', 'PH', 'false', 'PH']
yrange = mag_range
ytitle = "mag "+bands[iband]
if (!do_plots) then BOXPLOT, mag_bp_arr, yrange = yrange, labels = labels, ytitle = ytitle ;; , XTITLE="Category", YTITLE="data unit (unit)", $
if (!do_plots) then multiplot

if (keyword_set(galaxy) or keyword_set(lrg)) then begin
;; box plot info for all categories
r_bp_arr = [[r1_bp], [r2_bp], [match1_r_bp], [match2_r_bp], [non_match1_r_bp], [non_match2_r_bp]] ;;, [error_bp]]
r_bp_arr = transpose(r_bp_arr)
labels = ['sim', 'PH', 'match', 'PH', 'false', 'PH']
yrange = [9.0, 36.0]
ytitle = "r_eff "+bands[iband]
if (!do_plots) then BOXPLOT, r_bp_arr, labels = labels, ytitle = ytitle ;; , XTITLE="Category", YTITLE="data unit (unit)"
if (!do_plots) then multiplot
endif

if (iband eq 0) then error_mag_bp_arr = [error_mag_bp] else error_mag_bp_arr = [[error_mag_bp_arr], [error_mag_bp]]
if (keyword_set(galaxy) or keyword_set(lrg)) then begin
	if (iband eq 0) then error_r_bp_arr = [error_r_bp] else error_r_bp_arr = [[error_r_bp_arr], [error_r_bp]]
endif

endif ;; boxplot condition


if (!graphical_output) then begin

xtitle = ""
ytitle = ""
xstyle = 1
ystyle = 1

;; draw selection probabilities
;; simulation input and PHOTO output in object parameter space
p1 = 1.0
;; matching draw fraction
p2 = 1.0
;; error distribution
p3 = 1.0
xrange = mag_range
yrange = [-2.0, 2.0]
;; print, "xrange = ", xrange, ", yrange = ", yrange
if (1 eq 1) then begin
seed = total(10 ^ (lindgen(6) * 2) * bin_date(systime(0)))

colorx = 120
p = p1
nsimulation = n_elements(non_match_index1) + n_elements(match_index1)
n = n_elements(non_match_index1)
;; temp = where(non_match_index1 ne -1, n)
n1 = 0
n2 = 0
rndex = [-1L]
if (n_elements(match_index1) gt 0) then temp = where(match_index1 ne -1, n1)
if (n_elements(non_match_index1) gt 0) then temp = where(non_match_index1 ne -1, n2)
if (n gt 0) then rndex = get_random_index(n, p, seed = seed)

if (keyword_set(lrg) or keyword_set(galaxy)) then begin

ytitle = "sim"
title1 = "sim input, p = "+strtrim(STRING(p, FORMAT='(F5.2)'))+", n1 = "+strtrim(string(n1, format="(I5)"))+", n2 = "+strtrim(string(n2, format="(I5)"))

if (!do_plots) then plot, mag1[non_match_index1[rndex]], r1[non_match_index1[rndex]], psym = psym1, color = colorx, xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle, ytitle = ytitle

rndex = [-1L]
n = n_elements(match_index1)
if (n gt 0) then rndex = get_random_index(n, p, seed = seed)

if (!do_plots) then plot, mag1[match_index1[rndex]], r1[match_index1[rndex]], psym = psym1, color = color1, xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle, xtitle = xtitle
if (!do_plots) then plot, [0], [0], /nodata, xrange = xrange, yrange = yrange, xstyle =  1, ystyle = 1, color = 1, title = title1
if (!do_plots) then multiplot, /doxaxis, /doyaxis

nphoto = n_elements(non_match_index2) + n_elements(match_index2)
n = n_elements(non_match_index2)
;; temp = where(non_match_index2 ne -1, n)
n1 = 0
n2 = 0
if (n_elements(match_index2) gt 0) then temp = where(match_index2 ne -1, n1)
if (n_elements(non_match_index2) gt 0) then temp = where(non_match_index2 ne -1, n2)

rndex = [-1L]
if (n gt 0) then rndex = get_random_index(n, p, seed = seed)

ytitle = "PHOTO"
title2 = "PHOTO output, p = "+STRING(p, FORMAT='(F5.2)')+", n1 = "+strtrim(string(n1, format="(I5)"))+", n2 = "+strtrim(string(n2, format="(I5)"))
if (!do_plots) then plot, mag2[non_match_index2[rndex]], r2[non_match_index2[rndex]], psym = psym2, color = colorx, xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle, ytitle = ytitle
n = n_elements(match_index2)
rndex = [-1L]
if (n gt 0) then rndex = get_random_index(n, p, seed = seed)
if (!do_plots) then plot, mag2[match_index2[rndex]], r2[match_index2[rndex]], psym = psym2, color = color2, xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle
if (!do_plots) then plot, [0], [0], /nodata, xrange = xrange, yrange = yrange, xstyle =  1, ystyle = 1, color = 1, title = title2
if (!do_plots) then multiplot, /doxaxis, /doyaxis

index_bright = [-1L]
if (n_elements(match_index2) gt 0 and n_elements(clipped_match_index) gt 0) then index_bright = where(mag1[clipped_match_index[match_index2]] lt mag_thresh)
p = p2
xstyle = 1
ystyle = 1
ytitle = "all"

rndex = [-1L]
if (n_elements(non_match_index1) gt 0) then rndex = get_random_index(n_elements(non_match_index1), p, seed = seed)
if (!do_plots) then plot, mag1[non_match_index1[rndex]],r1[non_match_index1[rndex]], psym=psym1,color = color1,  xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle, ytitle = ytitle
if (n_elements(non_match_index2) gt 0) then rndex = get_random_index(n_elements(non_match_index2), p, seed = seed)
if (!do_plots) then plot, mag2[non_match_index2[rndex]],r2[non_match_index2[rndex]], psym=psym2,color=color2,  xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle, ytitle = ytitle

;; choosing bright objects only
rndex = [-1L]
brndex = [-1L]
if (n_elements(match_index1) gt 0 and n_elements(index_bright) gt 0) then begin
	rndex = get_random_index(n_elements(match_index1), p, seed = seed)
	brndex = get_random_index(n_elements(index_bright), p, seed = seed)
	rndex = index_bright[brndex]
endif

if (!do_plots) then plot, mag1[clipped_match_index[match_index2[rndex]]],r1[clipped_match_index[match_index2[rndex]]], psym=psym1,color = color1,  $
 	xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle, ytitle = ytitle
if (!do_plots) then plot, mag2[match_index2[rndex]],r2[match_index2[rndex]], psym=psym2,color = color2,  $
	xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle, ytitle = ytitle
xstyle = 5
ystyle = 5
if (!do_plots) then begin
for i = 0, n_elements(rndex) -1L , 1L do begin

	j2 = match_index2[rndex[i]]
	j1 = clipped_match_index[j2]
	x = [mag1[j1], mag2[j2]]
	y = [r1[j1], r2[j2]]
	color3 = 120
	;; print, "x, y = ", x, y, xrange, yrange
	if (!do_plots) then plot, x, y, xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle, color = color3

endfor
endif

endif ;; keyword_set(lrg || galaxy)

endif ;; graphical_output

mag_offset = 2.50 * alog10(2 * !pi)

if (verbose) then print, "R", format='(A, $)'
if (count1 gt 0) then begin

	; make reports
	reports = replicate(sample_report, count1)
	reports[*].band = iband
	reports[*].bname = band
	reports[*].x1 = x1[*]
	reports[*].y1 = y1[*]
	reports[*].mag1 = mag1[*]
	reports[*].counts1 = counts1[*]

	if (keyword_set(lrg) or keyword_set(galaxy)) then begin
	reports[*].ab1 = ab1[*]
	reports[*].e1 = e1[*]

	bad_angle_count = 0
	bad_angle_index = where(phi1 eq !value_is_bad, bad_angle_count)
	reports[*].phi1 = 180.0 / !pi * atan(tan(!pi / 180.0 * phi1[*]))
	if (bad_angle_count ne 0) then reports[bad_angle_index].phi1 = !value_is_bad	

	reports[*].a1 = r1[*]
	reports[*].b1 = r1[*] - alog10(ab1[*])
	reports[*].r1 = r1[*] - 0.5 * alog10(ab1[*])
	reports[*].mu1 = reports[*].mag1 + 5.0 * reports[*].r1 + mag_offset
	reports[*].fracPSF1 = fracPSF1[*]
	reports[*].SN1 = SN1[*]
	reports[*].I1 = II1[*]
	reports[*].IErr1 = IErr1[*]
	good_IErr_index = where(reports[*].IErr1 gt 0.0)
	reports[good_IErr_index].z1 = reports[good_IErr_index].I1 / reports[good_IErr_index].IErr1
	endif 

	reports[*].mag_halo1 = mag_halo1[*]
	reports[*].counts_halo1 = counts_halo1[*]

	reports[*].mag_sersic1 = mag_sersic1[*]
	reports[*].counts_sersic1 = counts_sersic1[*]
	reports[*].mag_sersic_inner1 = mag_sersic_inner1[*]
	reports[*].counts_sersic_inner1 = counts_sersic_inner1[*]
	reports[*].mag_sersic_outer1 = mag_sersic_outer1[*]
	reports[*].counts_sersic_outer1 = counts_sersic_outer1[*]
	reports[*].mag_csersic_inner1 = mag_csersic_inner1[*]
	reports[*].counts_csersic_inner1 = counts_csersic_inner1[*]
	reports[*].mag_csersic_outer1 = mag_csersic_outer1[*]
	reports[*].counts_csersic_outer1 = counts_csersic_outer1[*]

	if (keyword_set(galaxy) or keyword_set(lrg)) then begin
	reports[*].ab_halo1 = ab_halo1[*]
	reports[*].e_halo1 = e_halo1[*]

	bad_angle_count = 0
	bad_angle_index = where(phi_halo1 eq !value_is_bad, bad_angle_count)
	reports[*].phi_halo1 = 180.0 / !pi * atan(tan(!pi / 180.0 * phi_halo1[*]))
	if (bad_angle_count gt 0) then reports[bad_angle_index].phi_halo1 = !value_is_bad

	reports[*].a_halo1 = r_halo1[*]
	reports[*].b_halo1 = r_halo1[*] - alog10(ab_halo1[*])
	reports[*].r_halo1 = r_halo1[*] - alog10(sqrt(ab_halo1[*]))
	reports[*].mu_halo1 = reports[*].mag_halo1 + 5.0 * reports[*].r_halo1 + mag_offset
	reports[*].SN_halo1 = SN_halo1[*]
	reports[*].I_halo1 = I_halo1[*]
	reports[*].IErr_halo1 = IErr_halo1[*]
	good_IErr_index = where(reports[*].IErr_halo1 gt 0.0)
	reports[good_IErr_index].z_halo1 = reports[good_IErr_index].I_halo1 / reports[good_IErr_index].IErr_halo1
	

	reports[*].ab_sersic1 = ab_sersic1[*]
	reports[*].e_sersic1 = e_sersic1[*]
	reports[*].ab_sersic_inner1 = ab_sersic_inner1[*]
	reports[*].e_sersic_inner1 = e_sersic_inner1[*]
	reports[*].ab_sersic_outer1 = ab_sersic_outer1[*]
	reports[*].e_sersic_outer1 = e_sersic_outer1[*]
	reports[*].ab_csersic_inner1 = ab_csersic_inner1[*]
	reports[*].e_csersic_inner1 = e_csersic_inner1[*]
	reports[*].ab_csersic_outer1 = ab_csersic_outer1[*]
	reports[*].e_csersic_outer1 = e_csersic_outer1[*]


	bad_angle_count = 0
	bad_angle_index = where(phi_sersic1 eq !value_is_bad, bad_angle_count)
	reports[*].phi_sersic1 = 180.0 / !pi * atan(tan(!pi / 180.0 * phi_sersic1[*]))
	if (bad_angle_count gt 0) then reports[bad_angle_index].phi_sersic1 = !value_is_bad
	bad_angle_count = 0
	bad_angle_index = where(phi_sersic_inner1 eq !value_is_bad, bad_angle_count)
	reports[*].phi_sersic_inner1 = 180.0 / !pi * atan(tan(!pi / 180.0 * phi_sersic_inner1[*]))
	if (bad_angle_count gt 0) then reports[bad_angle_index].phi_sersic_inner1 = !value_is_bad
	bad_angle_count = 0
	bad_angle_index = where(phi_sersic_outer1 eq !value_is_bad, bad_angle_count)
	reports[*].phi_sersic_outer1 = 180.0 / !pi * atan(tan(!pi / 180.0 * phi_sersic_outer1[*]))
	if (bad_angle_count gt 0) then reports[bad_angle_index].phi_sersic_outer1 = !value_is_bad
	bad_angle_count = 0
	bad_angle_index = where(phi_csersic_inner1 eq !value_is_bad, bad_angle_count)
	reports[*].phi_csersic_inner1 = 180.0 / !pi * atan(tan(!pi / 180.0 * phi_csersic_inner1[*]))
	if (bad_angle_count gt 0) then reports[bad_angle_index].phi_csersic_inner1 = !value_is_bad
	bad_angle_count = 0
	bad_angle_index = where(phi_csersic_outer1 eq !value_is_bad, bad_angle_count)
	reports[*].phi_csersic_outer1 = 180.0 / !pi * atan(tan(!pi / 180.0 * phi_csersic_outer1[*]))
	if (bad_angle_count gt 0) then reports[bad_angle_index].phi_csersic_outer1 = !value_is_bad

	reports[*].a_sersic1 = r_halo1[*]
	reports[*].b_sersic1 = r_halo1[*] - alog10(ab_halo1[*])
	reports[*].r_sersic1 = r_halo1[*] - alog10(sqrt(ab_halo1[*]))
	reports[*].mu_sersic1 = reports[*].mag_sersic1 + 5.0 * reports[*].r_sersic1 + mag_offset
	reports[*].SN_sersic1 = SN_sersic1[*]
	reports[*].I_sersic1 = I_sersic1[*]
	reports[*].IErr_sersic1 = IErr_sersic1[*]
	good_IErr_index = where(reports[*].IErr_sersic1 gt 0.0)
	reports[good_IErr_index].z_sersic1 = reports[good_IErr_index].I_sersic1 / reports[good_IErr_index].IErr_sersic1
	
	endif

	reports[*].counts_total1 = counts_total1[*]
	reports[*].mag_total1 = mag_total1[*]

	reports[*].match = 0
	reports[*].ncd = counts_new_cD
	reports[*].nstari = nstar2i
	reports[*].nstarf = nstar2f
	reports[*].mag_star1f = X_mag_star1f
	reports[*].mag_star2f = X_mag_star2f
	reports[*].mag_star3f = X_mag_star3f
	reports[*].mag_star1i = X_mag_star1i
	reports[*].mag_star2i = X_mag_star2i
	reports[*].mag_star3i = X_mag_star3i
	reports[*].ngalaxyi = ngalaxy2i
	reports[*].ngalaxyf = ngalaxy2f
	reports[*].mag_galaxy1f = X_mag_galaxy1f
	reports[*].mag_galaxy2f = X_mag_galaxy2f
	reports[*].mag_galaxy3f = X_mag_galaxy3f
	reports[*].mag_galaxy1i = X_mag_galaxy1i
	reports[*].mag_galaxy2i = X_mag_galaxy2i
	reports[*].mag_galaxy3i = X_mag_galaxy3i

	if (n_elements(match_index1) ne n_elements(match_index2)) then begin
	print, name, ": ERROR - match_index_1 and match_index_2 have different sizes: ", $
	n_elements(match_index1), n_elements(match_index2)
	endif

	if (n_elements(match_index1) gt 0 and n_elements(match_index2) gt 0) then begin
	reports[match_index1].delta_star1f = delta_star1f[match_index2]
	reports[match_index1].delta_star2f = delta_star2f[match_index2]
	reports[match_index1].delta_star3f = delta_star3f[match_index2]
	reports[match_index1].delta_galaxy1f = delta_galaxy1f[match_index2]
	reports[match_index1].delta_galaxy2f = delta_galaxy2f[match_index2]
	reports[match_index1].delta_galaxy3f = delta_galaxy3f[match_index2]
	endif

	reports[*].delta_star1i = delta_star1i[*]
	reports[*].delta_star2i = delta_star2i[*]
	reports[*].delta_star3i = delta_star3i[*]
	reports[*].delta_galaxy1i = delta_galaxy1i[*]
	reports[*].delta_galaxy2i = delta_galaxy2i[*]
	reports[*].delta_galaxy3i = delta_galaxy3i[*]


	max_delta2 = -1.0
	if (count_match2 gt 0) then begin
	reports[match_index1].x2 = x2[match_index2]
	reports[match_index1].y2 = y2[match_index2]
	reports[match_index1].delta = $
	SQRT((reports[match_index1].x1 - reports[match_index1].x2) ^ 2 + (reports[match_index1].y1 - reports[match_index1].y2) ^ 2)
	
	reports[match_index1].counts2 = counts2[match_index2]
	reports[match_index1].mag2 = mag2[match_index2]

		if (keyword_set(galaxy) or keyword_set(lrg)) then begin
		reports[match_index1].ab2 = ab2[match_index2]
		reports[match_index1].e2 = e2[match_index2]

		bad_angle_count = 0
		bad_angle_index = where(phi2[match_index2] eq !value_is_bad, bad_angle_count)
		reports[match_index1].phi2 = 180.0 / !pi * atan(tan(!pi / 180.0 * phi2[match_index2]))
		if (bad_angle_count gt 0) then reports[match_index1[bad_angle_index]].phi2 = !value_is_bad

		reports[match_index1].a2 = r2[match_index2]
		reports[match_index1].b2 = r2[match_index2] - alog10(ab2[match_index2])
		reports[match_index1].r2 = r2[match_index2] - alog10(sqrt(ab2[match_index2]))
		reports[match_index1].mu2 = reports[match_index1].mag2 + 5.0 * reports[match_index1].r2 + mag_offset
		reports[match_index1].SN2 = SN2[match_index2]
		reports[match_index1].I2 = I2[match_index2]
		reports[match_index1].IErr2 = IErr2[match_index2]
		good_IErr_index = where(reports[*].IErr2 gt 0.0)
		reports[good_IErr_index].z2 = reports[good_IErr_index].I2 / reports[good_IErr_index].IErr2
		endif

		reports[match_index1].counts_halo2 = counts_halo2[match_index2]
		reports[match_index1].mag_halo2 = mag_halo2[match_index2]
		reports[match_index1].counts_sersic2 = counts_sersic2[match_index2]
		reports[match_index1].mag_sersic2 = mag_sersic2[match_index2]
		reports[match_index1].counts_sersic_inner2 = counts_sersic_inner2[match_index2]
		reports[match_index1].mag_sersic_inner2 = mag_sersic_inner2[match_index2]
		reports[match_index1].counts_sersic_outer2 = counts_sersic_outer2[match_index2]
		reports[match_index1].mag_sersic_outer2 = mag_sersic_outer2[match_index2]
		reports[match_index1].counts_csersic_inner2 = counts_csersic_inner2[match_index2]
		reports[match_index1].mag_csersic_inner2 = mag_csersic_inner2[match_index2]
		reports[match_index1].counts_csersic_outer2 = counts_csersic_outer2[match_index2]
		reports[match_index1].mag_csersic_outer2 = mag_csersic_outer2[match_index2]

		if (keyword_set(galaxy) or keyword_set(lrg)) then begin
		reports[match_index1].ab_halo2 = ab_halo2[match_index2]
		reports[match_index1].e_halo2 = e_halo2[match_index2]

		bad_angle_count = 0
		bad_angle_index = where(phi_halo2[match_index2] eq !value_is_bad, bad_angle_count)
		reports[match_index1].phi_halo2 = 180.0 / !pi * atan(tan(!pi / 180.0 * phi_halo2[match_index2]))
		if (bad_angle_count gt 0) then reports[match_index1[bad_angle_index]].phi_halo2 = !value_is_bad 

		reports[match_index1].a_halo2 = r_halo2[match_index2]
		reports[match_index1].b_halo2 = r_halo2[match_index2] - alog10(ab_halo2[match_index2])
		reports[match_index1].r_halo2 = r_halo2[match_index2] - alog10(sqrt(ab_halo2[match_index2]))
		reports[match_index1].mu_halo2 = reports[match_index1].mag_halo2 + 5.0 * reports[match_index1].r_halo2 + mag_offset
		reports[match_index1].SN_halo2 = SN_halo2[match_index2]
		reports[match_index1].I_halo2 = I_halo2[match_index2]
		reports[match_index1].IErr_halo2 = IErr_halo2[match_index2]
		good_IErr_index = where(reports[*].IErr_halo2 gt 0.0)
		reports[good_IErr_index].z_halo2 = reports[good_IErr_index].I_halo2 / reports[good_IErr_index].IErr_halo2

		reports[match_index1].ab_sersic2 = ab_sersic_2[match_index2]
		reports[match_index1].e_sersic2 = e_sersic_2[match_index2]

		bad_angle_count = 0
		bad_angle_index = where(phi_sersic_2[match_index2] eq !value_is_bad, bad_angle_count)
		reports[match_index1].phi_sersic2 = 180.0 / !pi * atan(tan(!pi / 180.0 * phi_sersic_2[match_index2]))
		if (bad_angle_count gt 0) then reports[match_index1[bad_angle_index]].phi_sersic2 = !value_is_bad 

		reports[match_index1].a_sersic2 = r_sersic_2[match_index2]
		reports[match_index1].b_sersic2 = r_sersic_2[match_index2] - alog10(ab_sersic_2[match_index2])
		reports[match_index1].r_sersic2 = r_sersic_2[match_index2] - alog10(sqrt(ab_sersic_2[match_index2]))
		reports[match_index1].n_sersic2 = n_sersic_2[match_index2]
		reports[match_index1].mu_sersic2 = reports[match_index1].mag_sersic2 + 5.0 * reports[match_index1].r_sersic2 + mag_offset
		;; reports[match_index1].SN_sersic2 = SN_sersic_2[match_index2]
		reports[match_index1].I_sersic2 = I_sersic_2[match_index2]
		reports[match_index1].IErr_sersic2 = IErr_sersic_2[match_index2]
		good_IErr_index = where(reports[*].IErr_sersic2 gt 0.0)
		reports[good_IErr_index].z_sersic2 = reports[good_IErr_index].I_sersic2 / reports[good_IErr_index].IErr_sersic2


		reports[match_index1].ab_sersic_inner2 = ab_sersic_inner_2[match_index2]
		reports[match_index1].e_sersic_inner2 = e_sersic_inner_2[match_index2]
		reports[match_index1].e_csersic_inner2 = e_csersic_inner_2[match_index2]

		bad_angle_count = 0
		bad_angle_index = where(phi_sersic_inner_2[match_index2] eq !value_is_bad, bad_angle_count)
		reports[match_index1].phi_sersic_inner2 = 180.0 / !pi * atan(tan(!pi / 180.0 * phi_sersic_inner_2[match_index2]))
		if (bad_angle_count gt 0) then reports[match_index1[bad_angle_index]].phi_sersic_inner2 = !value_is_bad 

		reports[match_index1].a_sersic_inner2 = r_sersic_inner_2[match_index2]
		reports[match_index1].b_sersic_inner2 = r_sersic_inner_2[match_index2] - alog10(ab_sersic_inner_2[match_index2])
		reports[match_index1].r_sersic_inner2 = r_sersic_inner_2[match_index2] - alog10(sqrt(ab_sersic_inner_2[match_index2]))

		reports[match_index1].a_csersic_inner2 = r_csersic_inner_2[match_index2]
		reports[match_index1].b_csersic_inner2 = r_csersic_inner_2[match_index2] - alog10(ab_csersic_inner_2[match_index2])
		reports[match_index1].r_csersic_inner2 = r_csersic_inner_2[match_index2] - alog10(sqrt(ab_csersic_inner_2[match_index2]))
	

		reports[match_index1].n_sersic_inner2 = n_sersic_inner_2[match_index2]
		reports[match_index1].n_csersic_inner2 = n_csersic_inner_2[match_index2]
		reports[match_index1].mu_sersic_inner2 = reports[match_index1].mag_sersic_inner2 + 5.0 * reports[match_index1].r_sersic_inner2 + mag_offset
		reports[match_index1].mu_csersic_inner2 = reports[match_index1].mag_csersic_inner2 + 5.0 * reports[match_index1].r_csersic_inner2 + mag_offset
		;; reports[match_index1].SN_sersic_inner2 = SN_sersic_inner_2[match_index2]
		reports[match_index1].I_sersic_inner2 = I_sersic_inner_2[match_index2]
		reports[match_index1].I_csersic_inner2 = I_csersic_inner_2[match_index2]
		reports[match_index1].IErr_sersic_inner2 = IErr_sersic_inner_2[match_index2]
		reports[match_index1].IErr_csersic_inner2 = IErr_csersic_inner_2[match_index2]
		good_IErr_index = where(reports[*].IErr_sersic_inner2 gt 0.0D0)
		reports[good_IErr_index].z_sersic_inner2 = reports[good_IErr_index].I_sersic_inner2 / reports[good_IErr_index].IErr_sersic_inner2
		good_IErr_index = where(reports[*].IErr_csersic_inner2 gt 0.0D0)
		reports[good_IErr_index].z_csersic_inner2 = reports[good_IErr_index].I_csersic_inner2 / reports[good_IErr_index].IErr_csersic_inner2
		nan_ierr_count = 0
		nan_ierr_index = where(not finite(reports[*].IERR_CSERSIC_INNER2), nan_ierr_count)
		if (nan_ierr_count gt 0) then reports[nan_ierr_index].ierr_csersic_inner2 = !value_is_bad
	
		reports[match_index1].ab_sersic_outer2 = ab_sersic_outer_2[match_index2]
		reports[match_index1].e_sersic_outer2 = e_sersic_outer_2[match_index2]
		reports[match_index1].e_csersic_outer2 = e_csersic_outer_2[match_index2]
		
		bad_angle_count = 0
		bad_angle_index = where(phi_sersic_outer_2[match_index2] eq !value_is_bad, bad_angle_count)
		reports[match_index1].phi_sersic_outer2 = 180.0 / !pi * atan(tan(!pi / 180.0 * phi_sersic_outer_2[match_index2]))
		if (bad_angle_count gt 0) then reports[match_index1[bad_angle_index]].phi_sersic_outer2 = !value_is_bad 

		reports[match_index1].a_sersic_outer2 = r_sersic_outer_2[match_index2]
		reports[match_index1].b_sersic_outer2 = r_sersic_outer_2[match_index2] - alog10(ab_sersic_outer_2[match_index2])
		reports[match_index1].r_sersic_outer2 = r_sersic_outer_2[match_index2] - alog10(sqrt(ab_sersic_outer_2[match_index2]))
		reports[match_index1].a_csersic_outer2 = r_csersic_outer_2[match_index2]
		reports[match_index1].b_csersic_outer2 = r_csersic_outer_2[match_index2] - alog10(ab_csersic_outer_2[match_index2])
		reports[match_index1].r_csersic_outer2 = r_csersic_outer_2[match_index2] - alog10(sqrt(ab_csersic_outer_2[match_index2]))
		reports[match_index1].n_sersic_outer2 = n_sersic_outer_2[match_index2]
		reports[match_index1].n_csersic_outer2 = n_csersic_outer_2[match_index2]
		reports[match_index1].alpha_csersic2 = alpha_csersic_2[match_index2]
		reports[match_index1].mu_sersic_outer2 = reports[match_index1].mag_sersic_outer2 + 5.0 * reports[match_index1].r_sersic_outer2 + mag_offset
		reports[match_index1].mu_csersic_outer2 = reports[match_index1].mag_csersic_outer2 + 5.0 * reports[match_index1].r_csersic_outer2 + mag_offset
		;; reports[match_index1].SN_sersic_outer2 = SN_sersic_outer_2[match_index2]
		reports[match_index1].I_sersic_outer2 = I_sersic_outer_2[match_index2]
		reports[match_index1].IErr_sersic_outer2 = IErr_sersic_outer_2[match_index2]
		good_IErr_index = where(reports[*].IErr_sersic_outer2 gt 0.0D0)
		reports[good_IErr_index].z_sersic_outer2 = reports[good_IErr_index].I_sersic_outer2 / reports[good_IErr_index].IErr_sersic_outer2
		reports[match_index1].I_csersic_outer2 = I_csersic_outer_2[match_index2]
		reports[match_index1].IErr_csersic_outer2 = IErr_csersic_outer_2[match_index2]
		good_IErr_index = where(reports[*].IErr_csersic_outer2 gt 0.0D0)
		reports[good_IErr_index].z_csersic_outer2 = reports[good_IErr_index].I_csersic_outer2 / reports[good_IErr_index].IErr_csersic_outer2	
		nan_ierr_count = 0
		nan_ierr_index = where(not finite(reports[*].IERR_CSERSIC_OUTER2), nan_ierr_count)
		if (nan_ierr_count gt 0) then reports[nan_ierr_index].ierr_csersic_outer2 = !value_is_bad

		;; insert_objc_chisq_in_report, reports, list1 = list1, list2 = list2, match_index1 = match_index1, match_index2 = match_index2
		if (tag_exist(list2[0], "CHISQ_MASK")) then begin
			reports[match_index1].chisq2 = list2[match_index2].chisq_mask[7]
			reports[match_index1].chisq2_nu = list2[match_index2].chisq_mask[7]/ list2[match_index2].npix_mask[7]
			reports[match_index1].npix2 = list2[match_index2].npix_mask[7]	
		endif else begin
			if (tag_exist(list2[0], "CHISQ_OBJCSS")) then begin
				reports[match_index1].chisq2 = list2[match_index2].chisq_objc
				reports[match_index1].chisq2_nu = list2[match_index2].chisq_objc / list2[match_index2].npix_objc
				reports[match_index1].npix2 = list2[match_index2].npix_objc	
			endif
		endelse


		endif
	
	reports[match_index1].counts_total2 = counts_total2[match_index2]
	reports[match_index1].mag_total2 = mag_total2[match_index2]
	reports[match_index1].dmag = reports[match_index1].mag2 - reports[match_index1].mag1
	reports[match_index1].dmag_total = reports[match_index1].mag_total2 - reports[match_index1].mag_total1
	reports[match_index1].dmag_halo = reports[match_index1].mag_halo2 - reports[match_index1].mag_halo1

		if (keyword_set(galaxy) or keyword_set(lrg)) then begin
		reports[match_index1].dr = reports[match_index1].r2 - reports[match_index1].r1 
		reports[match_index1].dr_halo = reports[match_index1].r_halo2 - reports[match_index1].r_halo1 
		reports[match_index1].dmu = reports[match_index1].mu2 - reports[match_index1].mu1 
		reports[match_index1].dmu_halo = reports[match_index1].mu_halo2 - reports[match_index1].mu_halo1 

		reports[match_index1].dphi = reports[match_index1].phi2 - reports[match_index1].phi1
		reports[match_index1].dphi_halo = reports[match_index1].phi_halo2 - reports[match_index1].phi_halo1
		reports[match_index1].dphi = 180.00 / !pi * atan(tan(!pi / 180.00 * reports[match_index1].dphi))
		reports[match_index1].dphi_halo = 180.00 / !pi * atan(tan(!pi / 180.00 * reports[match_index1].dphi_halo))
		
		bad_angle_count = 0
		bad_angle_index = where(reports[match_index1].phi2 eq !value_is_bad or reports[match_index1].phi1 eq !value_is_bad)
		if (bad_angle_count gt 0) then reports[match_index1[bad_angle_index]].dphi = !value_is_bad
		bad_angle_count = 0
		bad_angle_index = where(reports[match_index1].phi_halo1 eq !value_is_bad or reports[match_index1].phi_halo2 eq !value_is_bad)
		if (bad_angle_count gt 0) then reports[match_index1[bad_angle_index]].dphi_halo = !value_is_bad

		endif

		if (n3 gt 0) then begin
	
		reports[match_index1].counts3 = counts3[match_index2]
		reports[match_index1].mag3 = mag3[match_index2]
		
			if (keyword_set(galaxy) or keyword_set(lrg)) then begin
			reports[match_index1].ab3 = ab3[match_index2]
			reports[match_index1].e3 = e3[match_index2]

			bad_angle_count = 0
			bad_angle_index = where(phi3[match_index2] eq !value_is_bad, bad_angle_count)
			reports[match_index1].phi3 = 180.0 / !pi * atan(tan(!pi / 180.0 * phi3[match_index2]))
			if (bad_angle_count gt 0) then reports[match_index1[bad_angle_index]].phi3 = !value_is_bad

			reports[match_index1].a3 = r3[match_index2]
			reports[match_index1].b3 = r3[match_index2] - alog10(ab3[match_index2])
			reports[match_index1].r3 = r3[match_index2] - alog10(sqrt(ab3[match_index2]))
			reports[match_index1].mu3 = reports[match_index1].mag3 + 5.0 * reports[match_index1].r3 + mag_offset
			reports[match_index1].SN3 = SN3[match_index2]
			reports[match_index1].I3 = I3[match_index2]
			reports[match_index1].IErr3 = IErr3[match_index2]
			good_IErr_index = where(reports[*].IErr3 gt 0.0)
			reports[good_IErr_index].z3 = reports[good_IErr_index].I3 / reports[good_IErr_index].IErr3
			endif

		reports[match_index1].counts_halo3 = counts_halo2[match_index2]
		reports[match_index1].mag_halo3 = mag_halo3[match_index2]

			if (keyword_set(lrg) or keyword_set(galaxy)) then begin
			reports[match_index1].ab_halo3 = ab_halo3[match_index2]
			reports[match_index1].e_halo3 = e_halo3[match_index2]
			
			bad_angle_count = 0
			bad_angle_index = where(phi_halo3[match_index2] eq !value_is_bad, bad_angle_count)
			reports[match_index1].phi_halo3 = 180.0 / !pi * atan(tan(!pi / 180.0 * phi_halo3[match_index2]))
			if (bad_angle_count gt 0) then	reports[match_index1[bad_angle_index]].phi_halo3 = !value_is_bad

			reports[match_index1].a_halo3 = r_halo3[match_index2]
			reports[match_index1].b_halo3 = r_halo3[match_index2] - alog10(ab_halo3[match_index2])
			reports[match_index1].r_halo3 = r_halo3[match_index2] - alog10(sqrt(ab_halo3[match_index2]))
			reports[match_index1].mu_halo3 = reports[match_index1].mag_halo3 + 5.0 * reports[match_index1].r_halo3 + mag_offset
			reports[match_index1].SN_halo3 = SN_halo3[match_index2]
			reports[match_index1].I_halo3 = I_halo3[match_index2]
			reports[match_index1].IErr_halo3 = IErr_halo3[match_index2]
			good_IErr_index = where(reports[*].IErr_halo3 gt 0.0)
			reports[good_IErr_index].z_halo3 = reports[good_IErr_index].I_halo3 / reports[good_IErr_index].IErr_halo3
			endif

		reports[match_index1].mag_total3 = mag_total3[match_index2]
		reports[match_index1].dmag3 = reports[match_index1].mag3 - reports[match_index1].mag1
		reports[match_index1].dmag_total3 = reports[match_index1].mag_total3 - reports[match_index1].mag_total1
		reports[match_index1].dmag_halo3 = reports[match_index1].mag_halo3 - reports[match_index1].mag_halo1

			if (keyword_set(lrg) or keyword_set(galaxy)) then begin
			reports[match_index1].dr3 = reports[match_index1].r3 - reports[match_index1].r1 
			reports[match_index1].dr_halo3 = reports[match_index1].r_halo3 - reports[match_index1].r_halo1 
			reports[match_index1].dmu3 = reports[match_index1].mu3 - reports[match_index1].mu1 
				reports[match_index1].dmu_halo3 = reports[match_index1].mu_halo3 - reports[match_index1].mu_halo1 
			reports[match_index1].dphi3 = reports[match_index1].phi3 - reports[match_index1].phi1
			reports[match_index1].dphi_halo3 = reports[match_index1].phi_halo3 - reports[match_index1].phi_halo1
			reports[match_index1].dphi3 = 180.00 / !pi * atan(tan(!pi / 180.00 * reports[match_index1].dphi3))
			reports[match_index1].dphi_halo3 = 180.00 / !pi * atan(tan(!pi / 180.00 * reports[match_index1].dphi_halo3))

			bad_angle_count = 0
			bad_angle_index = where(reports[match_index1].phi3 eq !value_is_bad or reports[match_index1].phi1 eq !value_is_bad, bad_angle_count)
			if (bad_angle_count gt 0) then reports[match_index1[bad_angle_index]].dphi3 = !value_is_bad
			bad_angle_count = 0
			bad_angle_index = where(reports[match_index1].phi_halo3 eq !value_is_bad or reports[match_index1].phi_halo1 eq !value_is_bad, bad_angle_count)
			if (bad_angle_count gt 0) then reports[match_index1[bad_angle_index]].dphi_halo3 = !value_is_bad

			endif

		endif ;; if n3 > 0

	reports[match_index1].match = 1L
	if (keyword_set(lrg) or keyword_set(galaxy)) then reports[match_index1].cdmatch = cdmatch[match_index2]
	max_delta2 = max(reports[match_index1].delta)

	endif ;; if count_match2 > 0

;; print, "max_delta1 = ", max_delta, ", max_delta2 = ", max_delta2

	if (report_exists eq 0) then begin
		all_reports = [reports] 
		report_exists = 1
	endif else begin
		all_reports = [reports, all_reports]
	endelse

;; print, name, ": reports - ", n_elements(reports)
;; print, name, ": all_reports - ", n_elements(all_reports)

endif else begin
	print, '0', format='(A, $)' ;; count1 == 0 
endelse



endif else begin
	;; print, "d0", format='(A, $)'
endelse


if (!graphical_output) then begin

if (keyword_set(lrg) or keyword_set(galaxy)) then begin

titlex="input <---> output, p = "+string(p, FORMAT="(F5.2)")
if (!do_plots) then plot, [0], [0], /nodata, xrange = xrange, yrange = yrange, xstyle =  1, ystyle = 1, color = 1, title = titlex
if (!do_plots) then multiplot, /doxaxis, /doyaxis

p = p3
n = n_elements(match_index2)
rndex = [-1L]
if (n gt 0 and n_elements(index_bright) gt 0) then begin
rndex = get_random_index(n, p, seed = seed)
brndex = get_random_index(n_elements(index_bright), p, seed = seed)
rndex = index_bright[brndex]

mu = 5 * r1[clipped_match_index[match_index2[*]]] + mag1[clipped_match_index[match_index2[*]]] 
dmag = mag2[match_index2[*]] - mag1[clipped_match_index[match_index2[*]]] 
dr = r2[match_index2[*]] - r1[clipped_match_index[match_index2[*]]] 
eta = dmag / 4.0 + dr / 1.0
epsilon = dmag / 1.0 - dr / 4.0
m = n_elements(rndex)

dmag_plot = dmag[rndex] + 0.1 * randomu(seed, m)
dr_plot = dr[rndex] + 0.1 * randomu(seed, m)
eta_plot = eta[rndex] + 0.1 * randomu(seed, m)
epsilon_plot = epsilon[rndex] + 0.1 * randomu(seed, m)
mag_plot =  mag1[clipped_match_index[match_index2[rndex]]] 
r_plot =  r1[clipped_match_index[match_index2[rndex]]]
 
mu_plot = mu[rndex]

endif

colorx = 200

xrange = [-5.0, 5.0]
yrange = [-3.0, 3.0]
psymx = 5
xstyle = 1
ystyle = 1
xtitle = "mag error"
ytitle = "r error"
dtitle = "matching error, p = "+string(p, FORMAT="(F5.2)")
if (!do_plots) then plot, dmag_plot, dr_plot, xrange = xrange, yrange = yrange, /nodata, xstyle = xstyle, ystyle = ystyle, color = 1, ytitle = ytitle, xtitle = xtitle, title = dtitle
xstyle = 5
ystyle = 5
if (!do_plots) then plot, dmag_plot, dr_plot, xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle, color = colorx, psym = psymx
if (!do_plots) then multiplot, /doxaxis, /doyaxis

k = 0.0
xtitle = "mag"
ytitle = "mag error"
xrange = mag_range
yrange = [-5.0, 5.0]
xstyle = 1
ystyle = 1
dtitle = "mag error vs. mag, p = "+string(p, format="(F5.2)")
if (!do_plots) then plot, mag_plot, dmag_plot, xrange = xrange, yrange = yrange, /nodata, $
	xstyle = xstyle, ystyle = ystyle, color = 1, ytitle = ytitle, xtitle = xtitle, title = dtitle
xstyle = 5
ystyle = 5
if (!do_plots) then plot, mag_plot, dmag_plot, xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle, color = colorx, psym = psymx
if (!do_plots) then multiplot, /doxaxis, /doyaxis

xtitle = "r"
ytitle = "mag error"
xrange = [-2.0, 2.0]
yrange = [-5.0, 5.0]
xstyle = 1
ystyle = 1
dtitle="mag error vs. r_eff, p = "+string(p, format="(F5.2)")
if (!do_plots) then plot, r_plot, dmag_plot, xrange = xrange, yrange = yrange, /nodata, $
	xstyle = xstyle, ystyle = ystyle, color = 1, ytitle = ytitle, xtitle = xtitle, title = dtitle
xstyle = 5
ystyle = 5
if (!do_plots) then plot, r_plot, dmag_plot, xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle, color = colorx, psym = psymx
if (!do_plots) then multiplot, /doxaxis, /doyaxis

n1 = n_elements(x1)
n2 = n_elements(x2)

if (n_elements(r1) gt 0) then r1_pixel = (10.0 ^ r1) / !PIXELSIZE
if (n_elements(r2) gt 0) then r2_pixel = (10.0 ^ r2) / !PIXELSIZE

if (n1 gt 0) then begin
match_array1 = replicate(0.0, n1)
epsilon_array1 = replicate(!VALUE_IS_BAD, n1)
dmag1_array = replicate(!VALUE_IS_BAD, n1)
dr1_array = replicate(!VALUE_IS_BAD, n1)
endif

if (n_elements(match_index2) gt 0 and n_elements(clipped_match_index) gt 0) then begin
match_array1[clipped_match_index[match_index2]] = 1.0
epsilon_array1[clipped_match_index[match_index2]] = epsilon
dmag1_array[clipped_match_index[match_index2]] = dmag
dr1_array[clipped_match_index[match_index2]] = dr / alog(10)
endif

if (n2 gt 0) then begin
match_array2 = replicate(0.0, n2)
epsilon_array2 = replicate(!VALUE_IS_BAD, n2)
dmag2_array = replicate(!VALUE_IS_BAD, n2)
dr2_array = replicate(!VALUE_IS_BAD, n2)
r1_for_2 = replicate(!VALUE_IS_BAD, n2)
mag1_for_2 = replicate(!VALUE_IS_BAD, n2)
endif
if (n_elements(match_index2) gt 0) then begin
match_array2[match_index2] = 1.0
epsilon_array2[match_index2] = epsilon
dmag2_array[match_index2] = dmag
dr2_array[match_index2] = dr / alog(10)
endif

if (n_elements(match_index2) gt 0 and n_elements(clipped_match_index) gt 0) then begin
r1_for_2[match_index2] = r1_pixel[clipped_match_index[match_index2]] 
mag1_for_2[match_index2] = mag1[clipped_match_index[match_index2]] 
endif

if (!do_plots) then begin
header1 = ["rowc_sim", "colc_sim", "mag_sim", "r_sim", "match_status", "epsilon", "dmag", "dr/r", "r_dev_sim(pixel)", "r_exp_sim(pixel)", "dev_frac_sim"]
header2 = ["rowc_ph", "colc_ph", "mag_ph", "r_ph(pixel)", "mag_sim", "r_sim(pixel)", "match_status", "epsilon", "dmag", "dr/r", "r_dev_ph(pixel)", "r_exp_ph(pixel)", "dev_frac_ph"]
csv_array1 = [[y1], [x1], [mag1], [r1_pixel], [match_array1], [epsilon_array1], [dmag1_array], [dr1_array], [r_dev_1], [r_exp_1], [dev_frac_1]]
csv_array2 = [[y2], [x2], [mag2], [r2_pixel], [mag1_for_2], [r1_for_2], [match_array2], [epsilon_array2], [dmag2_array], [dr2_array], [r_dev_2], [r_exp_2], [dev_frac_2]]
csv_array1 = transpose(csv_array1)
csv_array2 = transpose(csv_array2)
table_header1 = ["simulation input with match status"]
table_header2 = ["PHOTO results with match status"]

ttt1 = n_elements(csv_array1);
if (count1 eq 1) then csv_array1 = reform(csv_array1, ttt1, count1);
ttt2 = n_elements(csv_array2);
if (count2 eq 1) then csv_array2 = reform(csv_array2, ttt2, count2)

if (n_elements(matchlog1) ne 0) then write_csv, matchlog1, csv_array1, header = header1, table_header = table_header1
if (n_elements(matchlog2) ne 0) then write_csv, matchlog2, csv_array2, header = header2, table_header = table_header2
endif ;; end of !do_plots 

endif

endif

endfor ;; band loop

if (keyword_set(boxplots) and !graphical_output) then begin

	error_mag_bp_arr = transpose(error_mag_bp_arr)
	labels = bands
	yrange = [-13.0, 13.0]
	ytitle = "mag error"
	if (!do_plots) then BOXPLOT, error_mag_bp_arr, yrange = yrange, labels = labels, ytitle = ytitle
	if (!do_plots) then multiplot

	if (keyword_set(galaxy) and 1 eq 0) then begin

	error_r_bp_arr = transpose(error_r_bp_arr)
	labels = bands
	yrange = [-13.0, 13.0]
	ytitle = "reff error"
	if (!do_plots) then BOXPLOT, error_r_bp_arr, labels = labels, ytitle = ytitle
	if (!do_plots) then multiplot
endif

endif

;; print, name, ": all_reports - ", n_elements(all_reports)
if (n_elements(all_reports) eq 0) then begin
	print, name, ": ERROR - all_reports undefined"
	if (n_elements(bad_report) ne 0) then return, [bad_report]
	print, name, ": ERROR - bad report undefined"
	return, [-1L]
endif
return, all_reports
end


