
#define MX_HDR_LINE_LEN 1000
#define MX_STRING_LEN 1000

#include "phCcdpars.h"

/* 
   The following structure type definitions are borrowed from
   par file headers.

   These structures are used in the par file readers to facilitate the reading from
   file
*/

/* idFrameLog file */

typedef struct {
    int       frame;          
    double    mjd;            
    double    ra;             
    double    dec;            
    float     spa;            
    float     ipa;            
    float     ipaRate;        
    double    az;             
    double    alt;            
} FRAMEINFO;

/* idReport file */

typedef enum {
         START,
         END
} RUNMARK;

typedef struct {
         char    program[20];
         int       stripe;
         char      strip[20];
         int       run;
         char      flavor[20];
         char      sys_scn[20];
         double    eqnx_scn;
         double    node;
         double    incl;
         double    muStart;
         double    muEnd;
         int       lastFrame;
         double    xBore;
         double    yBore;
         char      system[20];
         double    equinox;
         double    c_obs;
         double    tracking;
         char      quality[20];
         double    mjd;
         RUNMARK   mark;
} IMAGINGRUN;

/* idWeather files */

typedef struct { double thisiscrap; } MARMOTWEATHER;

/* opBC files */

/* 
this enum is already defined in phCcdpars.h

typedef enum {
	 DRKCUR, 		 // low level dark current 
	 HOTCOL, 		 // Hot column - bad dark current 
	 DEPCOL, 		 // Depressed column
	 CTECOL, 		 // Bad charge transfer efficiency 
	 BLKCOL, 		 // blocked column - fix with flatfield 
	 BADBLK, 		 // badly blocked col - just fill in
	 HOTSPOT, 		 // a single hot pixel 
	 NOISY, 		 // a noisy column 
	 TRAP  		 // location of trap head 
} DFTYPE;

*/

typedef struct {
	 char program[40]; 	 // program name 'stare_0123'
	 int camRow;      	 // camRow mt=0 spec=0 dsc=9
	 int camCol;        	 // camCol mt=0 spec=1234 dsc=9
	 int dfcol0;        	 // starting column of this defect (trimmed image)
	 int dfncol;        	 // number of columns in this defect
	 int dfrow0;        	 // starting row of this defect (trimmed image)
	 int dfnrow;        	 // number of rows in this defect
	 DFTYPE dftype;     	 // defect type
	 int astroline;     	 // 1 if astroline to pay attention, 0 else 
} CCDBC;

/* opCamera files */

typedef struct {
  int dewarID;
  int camRow;
  int camCol;
  double rowRef;
  double colRef;
  double xc;
  double yc;
  double theta;
  double sfactc;
  double pscale;
  double xmag;
  int rowOffset;
  int frameOffset;
  double dRow0;
  double dRow1;
  double dRow2;
  double dRow3;
  double dCol0;
  double dCol1;
  double dCol2;
  double dCol3;
} CCDGEOMETRY;

typedef struct {
  int dewarID;
  double xb;
  double yb;
  double thetai;
  double sfacti;
} DEWARGEOMETRY;

/* opConfig files */

typedef struct {
	 char program[40];
	 int camRow;
	 int camCol;
	 int rowBinning;
	 int colBinning;
	 int amp0;
	 int amp1;
	 int amp2;
	 int amp3;
	 int nrows;
	 int ncols;
	 int sPreBias0 ;
	 int nPreBias0 ;
	 int sPostBias0 ;
	 int nPostBias0 ;
	 int sOverScan0 ;
	 int nOverScan0 ;
	 int sMapOverScan0 ;
	 int nMapOverScan0 ;
	 int sOverScanRows0 ;
	 int nOverScanRows0 ;
	 int sDataSec0 ;
	 int nDataSec0 ;
	 int sDataRow0 ;
	 int nDataRow0 ;
	 int sCCDRowSec0 ;
	 int sCCDColSec0 ;
	 int sPreBias1 ;
	 int nPreBias1 ;
	 int sPostBias1 ;
	 int nPostBias1 ;
	 int sOverScan1 ;
	 int nOverScan1 ;
	 int sMapOverScan1 ;
	 int nMapOverScan1 ;
	 int sOverScanRows1 ;
	 int nOverScanRows1 ;
	 int sDataSec1 ;
	 int nDataSec1 ;
	 int sDataRow1 ;
	 int nDataRow1 ;
	 int sCCDRowSec1 ;
	 int sCCDColSec1 ;
	 int sPreBias2 ;
	 int nPreBias2 ;
	 int sPostBias2 ;
	 int nPostBias2 ;
	 int sOverScan2 ;
	 int nOverScan2 ;
	 int sMapOverScan2 ;
	 int nMapOverScan2 ;
	 int sOverScanRows2 ;
	 int nOverScanRows2 ;
	 int sDataSec2 ;
	 int nDataSec2 ;
	 int sDataRow2 ;
	 int nDataRow2 ;
	 int sCCDRowSec2 ;
	 int sCCDColSec2 ;
	 int sPreBias3 ;
	 int nPreBias3 ;
	 int sPostBias3 ;
	 int nPostBias3 ;
	 int sOverScan3 ;
	 int nOverScan3 ;
	 int sMapOverScan3 ;
	 int nMapOverScan3 ;
	 int sOverScanRows3 ;
	 int nOverScanRows3 ;
	 int sDataSec3 ;
	 int nDataSec3 ;
	 int sDataRow3 ;
	 int nDataRow3 ;
	 int sCCDRowSec3 ;
	 int sCCDColSec3 ;
	 int sPreBias0good ;
	 int nPreBias0good ;
	 int sPostBias0good ;
	 int nPostBias0good ;
	 int sOverScan0good ;
	 int nOverScan0good ;
	 int sMapOverScan0good ;
	 int nMapOverScan0good ;
	 int sOverScanRows0good ;
	 int nOverScanRows0good ;
	 int sDataSec0good ;
	 int nDataSec0good ;
	 int sDataRow0good ;
	 int nDataRow0good ;
	 int sCCDRowSec0good ;
	 int sCCDColSec0good ;
	 int sPreBias1good ;
	 int nPreBias1good ;
	 int sPostBias1good ;
	 int nPostBias1good ;
	 int sOverScan1good ;
	 int nOverScan1good ;
	 int sMapOverScan1good ;
	 int nMapOverScan1good ;
	 int sOverScanRows1good ;
	 int nOverScanRows1good ;
	 int sDataSec1good ;
	 int nDataSec1good ;
	 int sDataRow1good ;
	 int nDataRow1good ;
	 int sCCDRowSec1good ;
	 int sCCDColSec1good ;
	 int sPreBias2good ;
	 int nPreBias2good ;
	 int sPostBias2good ;
	 int nPostBias2good ;
	 int sOverScan2good ;
	 int nOverScan2good ;
	 int sMapOverScan2good ;
	 int nMapOverScan2good ;
	 int sOverScanRows2good ;
	 int nOverScanRows2good ;
	 int sDataSec2good ;
	 int nDataSec2good ;
	 int sDataRow2good ;
	 int nDataRow2good ;
	 int sCCDRowSec2good ;
	 int sCCDColSec2good ;
	 int sPreBias3good ;
	 int nPreBias3good ;
	 int sPostBias3good ;
	 int nPostBias3good ;
	 int sOverScan3good ;
	 int nOverScan3good ;
	 int sMapOverScan3good ;
	 int nMapOverScan3good ;
	 int sOverScanRows3good ;
	 int nOverScanRows3good ;
	 int sDataSec3good ;
	 int nDataSec3good ;
	 int sDataRow3good ;
	 int nDataRow3good ;
	 int sCCDRowSec3good ;
	 int sCCDColSec3good ;
 } CCDCONFIG;

/* opECalib files */

/* this enum is defined in phCcdpars.h
typedef enum {
  LINEAR_ILLEGAL, //= 666,  // illegal type; this amp isn't used
  LINEAR_NONE, // = 1,      // C/R = 1
  LINEAR_QUADRATIC,     // C/R = 1 + c1 R
  LINEAR_LOG,           // C/R = 1 + c1 lg(R)
  LINEAR_THRESHLOG,     // C/R = 1 + (R<c2 ? 0 : c1 lg(R/c2))
  LINEAR_NTYPE          // number of functional forms
} LINEARITY_TYPE;

*/
typedef struct {
  char program[40];
  int camRow;
  int camCol;
  float readNoiseDN0;
  float fullWellDN0;
  float gain0;
  float biasLevel0;
  float DN0[13];
  float linearity0[13];
  float readNoiseDN1;
  float fullWellDN1;
  float gain1;
  float biasLevel1;
  float DN1[13];
  float linearity1[13];
  float readNoiseDN2;
  float fullWellDN2;
  float gain2;
  float biasLevel2;
  float DN2[13];
  float linearity2[13];
  float readNoiseDN3;
  float fullWellDN3;
  float gain3;
  float biasLevel3;
  float DN3[13];
  float linearity3[13];
} ECALIB;

/* opRunlist files */

typedef struct {
  int   run;
  int   mjd;
  char  datestring[MX_STRING_LEN];
  int   stripe;
                    // Stripe number, 0 if not on a survey stripe
  char  strip[MX_STRING_LEN];
                    // N=north, S=south, O=overlap
  double xbore;
                    // Boresight offset perpendicular to great circle in degrees
  int   field_ref;
                    // Field reference for mu_ref,mjd_ref
  int   lastfield;
                    // Last field number
  char  flavor[MX_STRING_LEN];
                    // science, engineering, bias
  int   xbin;
                    // CCD binning in X (perpendicular to scan direction)
  int   ybin;
                    // CCD binning in Y (scan direction)
  double mjd_ref;
                    // Fractional MJD at row 0 of reference frame
  double mu_ref;
                    // Mu position at reference field number
  int   linestart;
                    // Linestart rate in microsec between each (binned) row
  double tracking;
                    // Tracking rate in arcsec/sec
  double node;
                    // Node of great circle (RA on the J2000 equator)
  double incl;
                    // Inclination of great circle (relative to J2000 equator)
  char  comments[MX_STRING_LEN];
  float qterm;
                    // Quadratic term for astrometric solution in arcsec/hr^2
  float maxmuresid;
                    // Max arcsec residual from great circle in scan direction
  float maxnuresid;
                    // Max arcsec residual from great circle in x-scan direction
} RUNLIST;
