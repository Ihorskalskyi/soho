#ifndef THMSKY_H
#define THMSKY_H

#include <stdio.h>
#include "thMSkyTypes.h"
#include "thModelTypes.h"
#include "thMath.h"

#define thSkyObjcInit MSkyObjcInit
#define thSkyFuncInit sky_init

extern THOBJCTYPE SKY_OBJC;

RET_CODE sky_init(SBINDEX *sb, REGION *reg);
void MSkyGetType(THOBJCTYPE *type);

RET_CODE MSkyObjcInit(int nrow, int ncol, CHAIN *ampl, void *f0);
RET_CODE MSkyObjcFini(void);

RET_CODE MSkyPutAmpCharInIndex(SBINDEX *sbindex, CHAIN *amp);

RET_CODE MSkyInitAmp1(SBINDEX *sbindex);
RET_CODE MSkyInitAmp2(SBINDEX *sbindex);

RET_CODE MSkyInit00(SBINDEX *sbindex);
RET_CODE MSkyInit10(SBINDEX *sbindex);
RET_CODE MSkyInit20(SBINDEX *sbindex);
RET_CODE MSkyInit30(SBINDEX *sbindex);
RET_CODE MSkyInit40(SBINDEX *sbindex);
RET_CODE MSkyInit50(SBINDEX *sbindex);
RET_CODE MSkyInit60(SBINDEX *sbindex);

RET_CODE MSkyInit01(SBINDEX *sbindex);
RET_CODE MSkyInit11(SBINDEX *sbindex);
RET_CODE MSkyInit21(SBINDEX *sbindex);
RET_CODE MSkyInit31(SBINDEX *sbindex);
RET_CODE MSkyInit41(SBINDEX *sbindex);
RET_CODE MSkyInit51(SBINDEX *sbindex);
RET_CODE MSkyInit61(SBINDEX *sbindex);

RET_CODE MSkyInit02(SBINDEX *sbindex);
RET_CODE MSkyInit12(SBINDEX *sbindex);
RET_CODE MSkyInit22(SBINDEX *sbindex);
RET_CODE MSkyInit32(SBINDEX *sbindex);
RET_CODE MSkyInit42(SBINDEX *sbindex);
RET_CODE MSkyInit52(SBINDEX *sbindex);
RET_CODE MSkyInit62(SBINDEX *sbindex);

RET_CODE MSkyInit03(SBINDEX *sbindex);
RET_CODE MSkyInit13(SBINDEX *sbindex);
RET_CODE MSkyInit23(SBINDEX *sbindex);
RET_CODE MSkyInit33(SBINDEX *sbindex);
RET_CODE MSkyInit43(SBINDEX *sbindex);
RET_CODE MSkyInit53(SBINDEX *sbindex);
RET_CODE MSkyInit63(SBINDEX *sbindex);


RET_CODE MSkyInit04(SBINDEX *sbindex);
RET_CODE MSkyInit14(SBINDEX *sbindex);
RET_CODE MSkyInit24(SBINDEX *sbindex);
RET_CODE MSkyInit34(SBINDEX *sbindex);
RET_CODE MSkyInit44(SBINDEX *sbindex);
RET_CODE MSkyInit54(SBINDEX *sbindex);
RET_CODE MSkyInit64(SBINDEX *sbindex);

RET_CODE MSkyInit05( SBINDEX *sbindex);
RET_CODE MSkyInit15( SBINDEX *sbindex);
RET_CODE MSkyInit25( SBINDEX *sbindex);
RET_CODE MSkyInit35( SBINDEX *sbindex);
RET_CODE MSkyInit45( SBINDEX *sbindex);
RET_CODE MSkyInit55( SBINDEX *sbindex);
RET_CODE MSkyInit65( SBINDEX *sbindex);

RET_CODE MSkyInit06( SBINDEX *sbindex);
RET_CODE MSkyInit16( SBINDEX *sbindex);
RET_CODE MSkyInit26( SBINDEX *sbindex);
RET_CODE MSkyInit36( SBINDEX *sbindex);
RET_CODE MSkyInit46( SBINDEX *sbindex);
RET_CODE MSkyInit56( SBINDEX *sbindex);
RET_CODE MSkyInit66( SBINDEX *sbindex);

RET_CODE f0sky(void *p, REGION *reg);
RET_CODE g0sky(void *p, THREGION *threg, THPIX *mcount, int index);

THOBJC *thSkyObjcNew();

RET_CODE SkySpecifyRegion(void *q, PSF_CONVOLUTION_INFO *psf_info, THREGION *threg, int index, int reprefab);
RET_CODE SkySpecifyRegion_old(void *q, PSF_CONVOLUTION_INFO *psf_info, THREGION *threg, int index, int reprefab);
#endif

