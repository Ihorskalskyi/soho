#include "thDebug.h"
#include "thMle.h"
#include "thCalib.h"
#include "thMGProfile.h"
#include "thKahan.h"

#if DO_CAPTURE /* extensive debug mode, movie and step by step statistics */
#include "thCapture.h"
#endif

#define CONVERGENCE_SECTOR PSECTOR

static LSECTOR wsector = PSECTOR; /* the dispatched value is LM_DEFAULT_SECTOR = APSECTOR, other values such as PSECTOR and ASECTOR may be used for debugging purposes only */

static const SCHEMA *schema_algorithm_action = NULL;

static void init_static_vars (void);

static RET_CODE find_point_in_direction(MLEFL *y, MLEFL *x, MLEFL *x0, 
					MLEFL *D, MLEFL r, int n);

static MLEFL RELDX(MLEFL *x, MLEFL *y, MLEFL *D, int n, RET_CODE *status);

static MLEFL qk(MLEFL *x, MLEFL *xk, MLEFL fk, 
		MLEFL *jtdmm, MLEFL **jtj, MLEFL *w, int n, 
		RET_CODE *status);
static RET_CODE do_gauss_newton(MLEFL *y, MLEFL *x, 
				MLEFL **H, MLEFL *mDf, 
				MLEFL **wH, 
				int n, int *singular);

static RET_CODE is_derivative_too_small_for_gauss_newton(MLEFL *Fprime, CHISQFL F, int n, int *small_flag);

static RET_CODE thMultiplyVariable(THPIX *x, THPIX y);
static RET_CODE LMultiplyVariable(MLEFL *x, MLEFL y);

static int check_string_in_string_list(char *string, char **list, int n);

static RET_CODE tweak_wobjc_io(WOBJC_IO *objc, float tweakf, int fitflag);

static RET_CODE do_lfit_record_insert_chisq_in_LM(LSTRUCT *lstruct);

static MLEFL mag_mlefl_vector(MLEFL *a, MLEFL **metric, int n, int inf);
static THPIX mag_thpix_vector(THPIX *a, THPIX **metric, int n, int inf);
static THPIX mag_thpix_mlefl_vector(THPIX *a, MLEFL **metric, int n, int inf);

static CONVERGENCE test_QN_dx_convergence(LMETHOD *lmethod, MLEFL *dx, MLEFL *x, MLEFL **metric, int n);
static CONVERGENCE test_QN_Fprime_convergence(LMETHOD *lmethod, MLEFL *Fprime, int n);
static RET_CODE update_QN_delta(LMETHOD *lmethod, MLEFL delta, MLEFL rho, MLEFL mag_dx, MLEFL *delta_new);
static RET_CODE initiate_QN_delta(LSTRUCT *lstruct, LMETHOD *lmethod, LWORK *lwork, LSECTOR sector);

static CONVERGENCE test_GD_dx_convergence(LMETHOD *lmethod, LWMASK *lwmask, MLEFL *dx, MLEFL *x, MLEFL **metric, int n);
static CONVERGENCE test_GD_Fprime_convergence(LMETHOD *lmethod, LWMASK *lwmask, MLEFL *Fprime, int n);
static RET_CODE update_PGD_eta_max(LMETHOD *lmethod, MLEFL eta, MLEFL rho, MLEFL mag_dx, MLEFL cost, MLEFL costN, MLEFL *eta_new);
static RET_CODE update_GD_eta_max(LMETHOD *lmethod, MLEFL eta, MLEFL rho, MLEFL mag_dx, MLEFL cost, MLEFL costN, MLEFL *eta_new);

static void output_mlefl_sqr_matrix(MLEFL **A, int n);

static RET_CODE randomize_amplitude(LSTRUCT *lstruct);

static RET_CODE thMathMultiplySquareMatColumn(MLEFL **A, MLEFL *C, MLEFL *wC, int n);


static RET_CODE thMathMultiplyInverseSquareMatGeneral(MLEFL **A, MLEFL **B, MLEFL **wA, MLEFL **wB, MAT *wQ1, MAT *wQ2, VEC *wx, int n, int m); 

static RET_CODE thMathMultiplyInverseSquareMat(MLEFL **A, MLEFL **B, MLEFL **wA, MLEFL **wB, MAT *wQ1, MAT *wQ2, VEC *wx, int n);

static RET_CODE thMathMultiplyInverseSquareMatColumn(MLEFL **A, MLEFL *C, MLEFL **wA, MLEFL *wC, MAT *wQ1, MAT *wQ2, VEC *wx, int n);

static RET_CODE random_ball(MLEFL *x, int n, MLEFL r);

static RET_CODE randomize_mlefl_array(MLEFL *x, int n, MLEFL x1, MLEFL x2, int flatten_tanh);

static int check_for_edge(MLEFL *x, int n, int *flag);

void init_static_vars (void) {

	TYPE t = shTypeGetFromName("ALGORITHM_ACTION");
	schema_algorithm_action = shSchemaGetFromType(t);
	shAssert(schema_algorithm_action != NULL);
	
	return;
}

#if DEEP_DEBUG_MLE || DEBUG_CHISQ
static int dbgX(LSTRUCT *lstruct, char *fname, LSECTOR xsector, char *comment);
static int dbgCov(MLEFL **cov, int n, char *fname, char *comment);
static int dbgInvCov(MLEFL **invcov, int n, char *fname, char *comment);
static int dbgJtdmm(MLEFL *jtdmm, int n, char *fname, char *comment);
static int dbgJtj(MLEFL **jtj, int n, char *fname, char *comment);
static int dbgMimj(MLEFL **mimj, int n, char *fname, char *comment);
static int dbgDmi(MLEFL *dmi, int n, char *fname, char *comment);
static int dbg2DArray(MLEFL **A, int n, int m, char *fname, char *comment);
static int dbg1DArray(MLEFL *A, int n, char *fname, char *comment);

int dbgX(LSTRUCT *lstruct, char *fname, LSECTOR xsector, char *comment) {
char *name = "dbgX";

MLEFL *dbgx, *dbgxT, *dbgy, *dbgz;
int i, n = 0;

printf("%s: redirected from '%s': %s \n", name, fname, comment);

RET_CODE status;	
status = ExtractXxNdX(lstruct, xsector, &dbgx, &dbgxT, &dbgy, &dbgz, NULL, &n);

printf("%s: %s: x_old = ", name, comment);
for (i = 0; i < n; i++) {		
	if (i < n - 1) {
		printf("%20.10g, ", (float) dbgx[i]);
	} else {
		printf("%20.10g", (float) dbgx[i]);
	}
}
printf("\n");

printf("%s: %s: x_temp = ", name, comment);
for (i = 0; i < n; i++) {		
	if (i < n - 1) {
		printf("%20.10g, ", (float) dbgxT[i]);
	} else {
		printf("%20.10g", (float) dbgxT[i]);
	}
}
printf("\n");



printf("%s: %s: x_current = ", name, comment);
for (i = 0; i < n; i++) {		
	if (i < n - 1) {
		printf("%20.10g, ", (float) dbgy[i]);
	} else {
		printf("%20.10g", (float) dbgy[i]);
	}
}
printf("\n");

printf("%s: %s: x_new = ", name, comment);
for (i = 0; i < n; i++) {		
	if (i < n - 1) {
		printf("%20.10g, ", (float) dbgz[i]);
	} else {
		printf("%20.10g", (float) dbgz[i]);
	}
}
printf("\n");


return(0);
}

int dbgCov(MLEFL **cov, int n, char *fname, char *comment) {
char *name = "dbgCov";

int i, j;
printf("%s: Cov values - redirected from '%s': %s \n", name, fname, comment);

for (i = 0; i < n; i++) {
	printf("Cov = ");
	for (j = 0; j < n; j++) {
		if (j < n - 1) {
			printf("%10.5G, ", (double) cov[i][j]);
		} else {
			printf("%10.5G", (double) cov[i][j]);
		}
	}
	printf("\n");
}

return(0);
}

int dbgInvCov(MLEFL **invcov, int n, char *fname, char *comment) {
char *name = "dbgInvCov";

int i, j;
printf("%s: InvCov values - redirected from '%s': %s \n", name, fname, comment);

for (i = 0; i < n; i++) {
	printf("InvCov = ");
	for (j = 0; j < n; j++) {
		if (j < n - 1) {
			printf("%10.5G, ", (double) invcov[i][j]);
		} else {
			printf("%10.5G", (double) invcov[i][j]);
		}
	}
	printf("\n");
}

return(0);
}

int dbgJtdmm(MLEFL *jtdmm, int n, char *fname, char *comment) {
char *name = "dbgJtdmm";

int i;
printf("%s: Jtdmm values - redirected from '%s': %s \n", name, fname, comment);

for (i = 0; i < n; i++) {
	printf("%s: %s: Jt | D - M > = ", name, comment);
	if (i < n - 1) {
		printf("%8.4g, ", (float) jtdmm[i]);
	} else {
		printf("%8.4g", (float) jtdmm[i]);
	}
	printf("\n");
}

return(0);
}



int dbgJtj(MLEFL **jtj, int n, char *fname, char *comment) {
char *name = "dbgJtj";

int i, j;
printf("%s: Jtj values - redirected from '%s': %s \n", name, fname, comment);

for (i = 0; i < n; i++) {
	printf("JtJ = ");
	for (j = 0; j < n; j++) {
		if (j < n - 1) {
			printf("%8.4g, ", (float) jtj[i][j]);
		} else {
			printf("%8.4g", (float) jtj[i][j]);
		}
	}
	printf("\n");
}

return(0);
}
int dbgMimj(MLEFL **mimj, int n, char *fname, char *comment) {
char *name = "dbgMimj";

int i, j;
printf("%s: < mi | mj > values - redirected from '%s': %s \n", name, fname, comment);

for (i = 0; i < n; i++) {
	printf("MiMj = ");
	for (j = 0; j < n; j++) {
		if (j < n - 1) {
			printf("%20.10g, ", (float) mimj[i][j]);
		} else {
			printf("%20.10g", (float) mimj[i][j]);
		}
	}
	printf("\n");
}

return(0);
}

int dbg2DArray(MLEFL **A, int n, int m, char *fname, char *comment) {
char *name = "dbg2DArray";

int i, j;
printf("%s: A[i][j] values - redirected from '%s': %s \n", name, fname, comment);

for (i = 0; i < n; i++) {
	printf("A[i][j] = ");
	for (j = 0; j < m; j++) {
		if (j < m - 1) {
			printf("%20.10g, ", (float) A[i][j]);
		} else {
			printf("%20.10g", (float) A[i][j]);
		}
	}
	printf("\n");
}

return(0);
}

int dbg1DArray(MLEFL *A, int n, char *fname, char *comment) {
char *name = "dbg1DArray";

int i;
printf("%s: ARR[i] values - redirected from '%s': %s \n", name, fname, comment);

for (i = 0; i < n; i++) {
	printf("ARR[i] = ");
	if (i < n - 1) {
		printf("%20.10g, ", (float) A[i]);
	} else {
		printf("%20.10g", (float) A[i]);
	}
	printf("\n");
}

return(0);
}



int dbgDmi(MLEFL *dmi, int n, char *fname, char *comment) {
char *name = "dbgDmi";

int j;
printf("%s: <d | mi> values - redirected from '%s': %s \n", name, fname, comment);

printf("DMi = ");
for (j = 0; j < n; j++) {
	if (j < n - 1) {
		printf("%20.10g, ", (float) dmi[j]);
	} else {
		printf("%20.10g", (float) dmi[j]);
	}
}
printf("\n");

return(0);
}


#endif

int check_string_in_string_list(char *string, char **list, int n) {
shAssert(n >= 0);
shAssert(n == 0 || list != NULL);
int res = 0;
int i;
for (i = 0; i < n; i++) {
	shAssert(list[i] != NULL);
	res = res || (!strcmp(list[i], string));
}
return(res);
}

/* At this point this package is only written to perform extended-LM maximization of the likelihood
   written in the form < m - d | W | m - d >. 

   The extended-LM maximization has two major steps:
   1. Maximize linearly over amplitudes
   2. Maximize by derivative (LM) over non-linear parameters

   If the amplitude for each object model is shows as a_i and the model parameters are called p_j, then 
   we have the following relationship for the first order and the second order derivatives of the likelihood 
   function

   L                 = < a_k m_k - d | W | a_l m_l - d >

   DL / Da_i         =         < m_i | W | a_l m_l - d > + cnjg.
   DL / Dp_j         =   < a_k m_k_j | W | a_l m_l - d > + cnjg.

   DDL / (Da_i Da_j) =         < m_i | W | m_j >         + cnjg.
   DDL / (Da_i Dp_j) =       < m_i_j | W | a_l m_l - d > + cnjg.
                             + < m_i | W | a_l m_l_j >   + cnjg.
   DDK / (Dp_i Dp_j) =  < a_k m_k_ij | W | a_l m_l - d > + cnjg
                       + < a_k m_k_i | W | a_l m_l_j >   + cnjg. 


  On the light of this formulae the following matrices / images are referred to often:
  
  Matrices in parameter / amplitude space:

     < m_i_j | W | m - d >   	for DL / Dp, DDL / (Da Dp) 
       < m_i | W | m_j >        for DDL / (Da Da)
     < m_k_i | W | m_l_j >      for DDL / (Dp Dp)
       < m_i | W | m_k_j >      for DDL / (Da Dp)

    < m_k_ij | W | m - d >      for DDL / (Dp Dp) 

  Given that DDL / (Dp Dp) will be calculated during LM, only the first four  matrices are needed.

    
  Images:

       | m - d >
       | m_i >
       | m_i_j >

     W | m_i >
     W | m_i_j >
  
  The latter two images are only added two save time in case there is enough memory space. I speculate that because their inclusion doubles the space needed they will impose a hefty price on timing.

*/      

RET_CODE randomize_mlefl_array(MLEFL *x, int n, MLEFL x1, MLEFL x2, int flatten_tanh) {
char *name = "randomize_mlefl_array";
if (x == NULL || n <= 0) {
	thError("%s: ERROR - null array or (n < 0) passed", name);
	return(SH_GENERIC_ERROR);
}
if (flatten_tanh != 0 && flatten_tanh != 1) {
	thError("%s: ERROR - expected boolean flag for (flatten_tanh), received (%d)", name, flatten_tanh);
	return(SH_GENERIC_ERROR);
}

int i = 0;

if (flatten_tanh) {
	MLEFL t1 = tanh(x1);
	MLEFL t2 = tanh(x2);
	for (i = 0; i < n; i++) {
		MLEFL t = t1 + (t2 - t1) * (MLEFL) phRandomUniformdev();		
		x[i] = (MLEFL) atanh(t);
	}
} else {
	for (i = 0; i < n; i++) {
		x[i] = x1 + (x2 - x1) * (MLEFL) phRandomUniformdev();		
	}
}
return(SH_SUCCESS);
}

int check_for_edge(MLEFL *x, int n, int *flag) {
char *name = "check_for_edge";
if (x == NULL && n != 0) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (flag == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (n == 0) {
	thError("%s: WARNING - input size is passed as (n = 0)", name);
	*flag = 0;
	return(SH_SUCCESS);
}
if (n < 0) {
	thError("%s: ERROR - negative input size is passed (n = %d)", name, n);
	return(SH_GENERIC_ERROR);
}
int i, count = 0;
for (i = 0; i < n; i++) {
	if ((x[i] - (MLEFL) LM_NAG_EDGE_VAL_THRESH1) * (x[i] - (MLEFL) LM_NAG_EDGE_VAL_THRESH2) > (MLEFL) 0.0) count++;
}
if ((count >= 2) || (2 * count >= n)) {
	*flag = 1;
} else {
	*flag = 0;
} 

return(SH_SUCCESS);
}

RET_CODE thLRun(LSTRUCT *lstruct) {
char *name = "thLRun";
if (lstruct == NULL || lstruct->lalg == NULL) {
	thError("%s: ERROR - null or improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;

#if DEBUG_MEMORY
printf("%s: memory statistics before defragmenting memory", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#endif

int ret, free_to_os = FREE_TO_OS;
ret = shMemDefragment(free_to_os);
if (ret != 0) {
	thError("%s: ERROR - problem defragmenting memory", name);
	return(SH_GENERIC_ERROR);
}

#if DEBUG_MEMORY
printf("%s: memory statistics after defragmenting memory", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#endif

#if DO_CAPTURE /* extensive debug mode, movie and step by step statistics */
CAPTURE *capture = thCaptureNew();
FRAME *f = NULL;
status = thLstructGetSource(lstruct, (void **) &f);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the (frame) which is source of (lstruct)", name);
	return(status);
}
status = thCaptureInit(capture, f, MY_CAPTURE_MODE);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not initiate (capture) for (lstruct)", name);
	return(status);
}
status = thLstructPutPrivate(lstruct, capture, "CAPTURE");
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not put (capture) in (lprivate)", name);
	return(status);
}
#endif
RUNTYPE runtype = lstruct->lalg->runtype;
if (runtype == SIMPLERUN) {
	status = thModelExtLMFitSimpleRun(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not conclude the simple run", name);
		return(status);
	}
} else if (runtype == ALGRUN) {
	status = thModelExtLMFitAlgRun(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not conclude algorithm run", name);
		return(status);
	}
} else {
	thError("%s: ERROR - unsupported (runtype)", name);
	return(SH_GENERIC_ERROR);
}
#if 1
dbgX(lstruct, name, PSECTOR, "PSECTOR, end of nonlinear fit - caller: thLRun");
dbgX(lstruct, name, ASECTOR, "ASECTOR, end of nonlinear fit - caller: thLRun");
#endif
status = thLCalib(lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calibrate (lstruct)", name);
	return(status);
}

#if DO_CAPTURE /* extensive debug mode, movie and step by step statistics */
status = thCaptureFini(capture);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not finalize (capture) for (lstruct)", name);
	return(status);
}
status = thCaptureExport(capture);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not export (capture) for (lstruct)", name);
	return(status);
}
thCaptureDel(capture);
status = thLstructPutPrivate(lstruct, NULL, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not clean (lprivate) from (capture)", name);
	return(status);
}
#endif
status = thLDumpIo(lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump (fit) parameters onto (IO)", name);
	return(status);
}

return(SH_SUCCESS);
}



RET_CODE thModelExtLMFitSimpleRun(LSTRUCT *lstruct) {

char *name = "thModelExtLMFitSimpleRun";
RET_CODE status = SH_SUCCESS;	
/* check the input */

/* now recursively fit on amplitudes and non-linear params */
CONVERGENCE flag;
/* 
int recursion = 1;
*/
/* debug */
#if DEEP_DEBUG_MLE 
	dbgX(lstruct, name, PSECTOR, "upon start");
#endif
/* end of debug */

/* initiating all images (models and their derivatives) and the matrices */
status = MakeImageAndMatrices(lstruct, INITIMAGES_AND_DM);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract model images and matrices", name);
	return(status);
}

status = DoAmplitudeFit(lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not perform the amplitude fit", name);
	fflush(stderr);
	fflush(stdout);
	return(status);
}
flag = UNKNOWN_CONVERGENCE;
status = thLstructGetConvergence(lstruct, NULL, &flag, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get convergence flag from (lstruct)", name);
	return(status);
}
if (flag & GENERAL_CONVERGENCE) {
	thError("%s: WARNING - convergence flag '%s' raised by amplitude fit", name, shEnumNameGetFromValue("CONVERGENCE", flag));
}
/* debug */
#if DEEP_DEBUG_MLE 
dbgX(lstruct, name, PSECTOR, "after the first amplitude fit - caller: thModelExtLMFitSimpleRun");
#endif 
/* end of debug */		

status = thMakeM(lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calculate (m) and (d - m)", name);
	return(status);
}
/* J-matrices are not calculated until this point - see if the derivatives are calculated before invoking this fucntion */
status = thMakeJMatricesSimpleRun(lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calculate J matrices", name);
	return(status);
	}
status = thInitLmethod(lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not initialize (lmethod) based on initial (Jtj) matrix", name);
	return(status);
}
if (flag & GENERAL_CONVERGENCE) {
	status = UpdateLMConvergence(lstruct, flag);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update (convergence flag) in (lstruct)", name);
		return(status);
	}
}
status = thLCalcChisqNSimpleRun(lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calculate (chisq)", name);
	return(status);
}
status = UpdateChisq(lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update calculated value for (chisq)", name);
	return(status);
}

int np;
status = thLstructGetNPar(lstruct, &np);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (np) from (lstruct)", name);
	return(status);
}
if (!(flag & GENERAL_CONVERGENCE)) {
	if (np > 0) {
		flag = CONTINUE;
	} else if (np == 0) {
		/* purely linear fit */
		flag = CONVERGED;
	} else {
		thError("%s: ERROR - negative (np = %d) discovered in (lstruct)", name, np);
		return(status);
	}
}
/*
recursion = IsRecursionNeeded(lstruct, &status);

if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not check recursion criteria", name);
	return(status);
}
*/

/* make sure about the image and matrix construction */

XUPDATE update;

int loop_started = 1;
while (flag == CONTINUE) {

	status = thLstructGetXupdate(lstruct, &update, PSECTOR);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not obtain (apupdate) flag", name);
		return(status);
	}
	/* notice this uses new parameters */
	if (update == UPDATED || loop_started) {

		loop_started = 0; /* this flag is there only to ensure calculation 
		of J-matrices at the first passing within this while loop */

	/* debug */
	#if DEEP_DEBUG_MLE 
		dbgX(lstruct, name, PSECTOR, "middle of nonlinear fit - caller: thModelExtLMFitSimpleRun");
	#endif
	/* E of D */		
		status = thMakeJMatricesSimpleRun(lstruct);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not calculate J matrices", name);
			return(status);
			}
		}
	status = DoLMFitSimpleRun(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not perform nonlinear fit", name);
		return(status);
	}

	status = thLstructGetXupdate(lstruct, &update, PSECTOR);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not obtain (apupdate) flag", name);
		return(status);
	}
	/* notice this uses new parameters */
	if (update == UPDATED) {
		/* this is already calculated in DoLMFit
		status = MakeImageAndMatrices(lstruct, DM);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not extract model images and matrices", name);
			return(status);
		}
		*/
		status = DoAmplitudeFit(lstruct);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not perform the amplitude fit", name);
			fflush(stderr);
			fflush(stdout);
			return(status);
		}
		status = thLstructGetConvergence(lstruct, NULL, &flag, NULL);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get convergence flag from (lstruct)", name);
			return(status);
		}
		if (flag & GENERAL_CONVERGENCE) {
			thError("%s: WARNING - convergence flag '%s' raised during amplitude fit", name, shEnumNameGetFromValue("CONVERGENCE", flag));
		}
		status = thMakeM(lstruct); /* this step is needed to calculate ChisqN */
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not calculate (m) and (d - m)", name);
			return(status);
		}
	}
	if (!(flag & GENERAL_CONVERGENCE)) {
		flag = CheckLMConvergence(lstruct, CONVERGENCE_SECTOR, &status);
		/*
		recursion = IsRecursionNeeded(lstruct, &status);
		*/
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not check recursion criteria", name);
			return(status);
		}
	}
        }	
if (np == 0) {
	status = thMakeJMatricesSimpleRun(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not calculate JpJ matrix while (np = %d)", name, np);
		return(status);
	}
}

int singular = 0;
status = thLCalcCovap(lstruct, &singular);
if (singular) {
	#if DEBUG_MLE_SINGULAR
	thError("%s: WARNING - singularity detected when finding covaraince matrix", name);
	#endif
	fflush(stderr);
	fflush(stdout);
} else if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calculate amplitude-parameter covariance", name);
	fflush(stderr);
	fflush(stdout);
	return(status);
	}
#if THCLOCK
clk2 = clock();
printf("%s: CLOCKING - [%g] CPUs \n", 
	   name, ((float) (clk2 - clk1)) / CLOCKS_PER_SEC);
#endif
	

return(status);
}

RET_CODE thModelExtLMFitAlgRun(LSTRUCT *lstruct) {
char *name = "thModelExtLMFitAlgRun";

if (lstruct == NULL || lstruct->lalg == NULL) {
	thError("%s: ERROR - null or improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}

LSECTOR sector = wsector;

RET_CODE status = SH_SUCCESS;	
#if DO_CAPTURE /* extensive debug mode, movie and step by step statistics */
CAPTURE *capture = NULL;
char *captype = NULL, *mycaptype = "CAPTURE";
status = thLstructGetPrivate(lstruct, (void **) &capture, &captype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (capture) stored in (lprivate)", name);
	return(status);
}
if (strcmp(captype, mycaptype)) {
	thError("%s: ERROR - the structure stored in (lprivate) seems to be of type '%s' which is not expected ('%s')", name, captype, mycaptype);
	return(SH_GENERIC_ERROR);
}
#endif

/* this is needed to insert CFLAG in LMETHOD */
LMETHOD *lm = NULL;
status = thLstructGetLmethod(lstruct, &lm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmethod) from (lstruct)", name);
	return(status);
}


CONVERGENCE flag;
#if DEEP_DEBUG_MLE 
dbgX(lstruct, name, sector, "upon start");
dbgX(lstruct, name, ASECTOR, "A-sector, upon start");
dbgX(lstruct, name, PSECTOR, "P-sector, upon start");
#endif

ALGORITHM *alg;
/* initiating the dot products of 
all images (models and their derivatives) and the matrices */
status = thLstructGetAlgByStage(lstruct, INITSTAGE, &alg);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the algorithm for initial stage", name);
	return(status);
}
status = thAlgorithmRun(alg, lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run the init stage algorithm", name);
	return(status);
}

/*
 * this should be included do not comment out 
 */
#if START_WITH_AMPFIT

status = DoAmplitudeFit(lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not perform the amplitude fit before entering the loop", name);
	fflush(stderr);
	fflush(stdout);
	return(status);
}
status = randomize_amplitude(lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not randomize amplitudes", name);
	return(status);
}

flag = UNKNOWN_CONVERGENCE;
status = thLstructGetConvergence(lstruct, NULL, &flag, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get convergence flag from (lstruct)", name);
	return(status);
}
if (flag & GENERAL_CONVERGENCE) {
	thError("%s: WARNING - convergence flag '%s' raised by initial amplitude fit", name, shEnumNameGetFromValue("CONVERGENCE", flag));
}

#endif

status = thMakeJMatricesAlgRun(lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update J-matrices after initial a-fit", name);
	return(status);
}


#if DEEP_DEBUG_MLE 
dbgX(lstruct, name, sector, "W-sector - after the first amplitude fit - caller: thModelExtLMFitAlgRun");
dbgX(lstruct, name, ASECTOR, "A-sector - after the first amplitude fit - caller: thModelExtLMFitAlgRun");
dbgX(lstruct, name, PSECTOR, "P-sector - after the first amplitude fit - caller: thModelExtLMFitAlgRun");
#endif 

status = thInitLmethod(lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not initialize (lmethod) based on initial (Jtj) matrix", name);
	return(status);
}
if (flag & GENERAL_CONVERGENCE) {
	status = UpdateLMConvergence(lstruct, flag);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update (convergence flag) in (lstruct)", name);
		return(status);
	}
}
status = thLCalcChisqNAlgRun(lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calculate (chisq)", name);
	return(status);
}
status = CopyChisqNChisq(lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not initiate with calculated value for (chisqN)", name);
	return(status);
}

int nx = -1;
status = thLstructGetNX(lstruct, sector, &nx);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (np) from (lstruct)", name);
	return(status);
}
if (nx > 0) {
	if (!(flag & GENERAL_CONVERGENCE)) {
		flag = CheckLMConvergence(lstruct, sector, &status);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not check recursion criteria", name);
			return(status);
		}
	}
	#if DO_LFIT_RECORD
	status = do_lfit_record_insert_chisq_in_LM(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not insert (chisq) infoin (lmethod)", name);
		return(status);
	}
	status = thLRecordLfit(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not record 'lmethod' at this step", name);
		return(status);
	}
	#endif
} else if (nx == 0) {
	/* purely linear fit */
	if (!(flag & GENERAL_CONVERGENCE)) flag = CONTINUE;
	/* covariance calculation is usually done in the LM-Amp 
	 * 	loop below but for linear fits only this loop is not executed
 	 * 	so we need to calculate covariance separately */	
	status = thMakeJMatricesAlgRun(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not calculate JtJ matrix while (nx = %d) for (sector = '%s')", name, nx, shEnumNameGetFromValue("LSECTOR", sector));
		return(status);
	}
	int singular = 0;
	status = thLCalcCovap(lstruct, &singular);
	if (singular) {
		#if DEBUG_MLE_SINGULAR
		thError("%s: WARNING - singularity flag raised amplitude-parameter covariance", name);
		#endif
		fflush(stderr);
		fflush(stdout);
	} else if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not calculate the AP-sector covariance", name);
		return(status);
	}
	#if DO_LFIT_RECORD
	status = thLRecordLfit(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not record 'lmethod' at this step", name);
		return(status);
	}
	#endif
	if (!(flag & GENERAL_CONVERGENCE)) flag = CheckLMConvergence(lstruct, sector, &status);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not check recursion criteria", name);
		return(status);
	}
} else {
	thError("%s: ERROR - negative (nx = %d, sector = '%s') discovered in (lstruct)", name, nx, shEnumNameGetFromValue("LSECTOR", sector));
	return(status);
}

LMETHOD *lmethod = NULL;
status = thLstructGetLmethod(lstruct, &lmethod);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmethod) from (lstruct)", name);
	return(status);
}
LMETHODTYPE method_type = UNKNOWN_LMETHODTYPE;
status = thLmethodGetType(lmethod, &method_type, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (method_type) from (lmethod)", name);
	return(status);
}
if (method_type == SGD) {
	LWMASK *lwmask = NULL;
	status = thLstructGetLwmask(lstruct, &lwmask);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (lwmask) from (lstruct)", name);
		return(status);
	}
	printf("%s: sampleing (lwmask) for 'SGD' \n", name);
	#if 0
	status = thLwmaskSample(lwmask, DEFAULT_LWMASK_NSAMPLE, DEFAULT_LWMASK_NMINI);
	#else
	status = thLwmaskSample(lwmask, DEFAULT_LWMASK_NSAMPLE);
	#endif
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not sample (lwmask) for 'SGD'", name);
		return(status);
	}
}
 
alg = NULL;
XUPDATE update;

int iter = 0, loop_started = 1;
while (flag == CONTINUE) {

	iter++;
	status = thLstructGetXupdate(lstruct, &update, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not obtain (apupdate) flag", name);
		return(status);
	}
	/* notice this uses new parameters */
	/* if (update == UPDATED || loop_started) {
	*/

	if (loop_started) {
		loop_started = 0; /* this flag is there only to ensure calculation 
		of J-matrices at the first passing within this while loop */

	#if DEEP_DEBUG_MLE 
		if (loop_started) {
			dbgX(lstruct, name, sector, "w-sector, middle of nonlinear fit - caller: thModelExtLMFitAlgRun");
		} else {	
			dbgX(lstruct, name, sector, "w-sector, start of nonlinear fit - caller: thModelExtLMFitAlgRun");
		}
	#endif	
		status = thMakeJMatricesAlgRun(lstruct);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not update J-matrices after a-fit", name);
			return(status);
		}

		/* covariance and all - should this covariance calculation be here at all */
		int singular = 0;
		status = thLCalcCovap(lstruct, &singular);
		if (singular) {
			#if DEBUG_MLE_SINGULAR
			thError("%s: WARNING - singular amplitude-parameter covariance", name);
			fflush(stderr);
			fflush(stdout);
			#endif
		} else if (status != SH_SUCCESS) {
			thError("%s: WARNING - could not calculate AP-sector covariance", name);
			/* 
			return(status);
			*/
		}


	#if DO_CAPTURE /* extensive debug mode, movie and step by step statistics */
		status = thCapture(capture);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not capture at (iter = %d)", name, iter);
			return(status);
		}
		status = thCaptureFlush(capture);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not flush (capture) related files", name);
			return(status);
		}
		fflush(stdout); /* this is so that the output to nohup.out file is uptodate */
	#endif
	}

	LMETHOD *lmethod = NULL;
	status = thLstructGetLmethod(lstruct, &lmethod);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (lmethod) from (lstruct)", name);
		return(status);
	}
	LMETHODTYPE method_type = UNKNOWN_LMETHODTYPE;
	status = thLmethodGetType(lmethod, &method_type, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (method_type) from (lmethod)", name);
		return(status);
	}
	LMETHODSUBTYPE wmethod_type = UNKNOWN_LMETHODSUBTYPE;
	LWORK *lwork = NULL;
	status = thLstructGetLwork(lstruct, &lwork);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (lwork) from (lstruct)", name);
		return(status);
	}
	status = thLmethodDecideWorkType(lstruct, lmethod, lwork);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not determine working formalism for method = '%s'",
		name, shEnumNameGetFromValue("LMETHODTYPE", method_type));
		return(status);
	}
	status = thLmethodGetType(lmethod, NULL, &wmethod_type);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (work_type) for (lmethod)", name);
		return(status);
	}
	LWMASK *lwmask = NULL;
	status = thLstructGetLwmask(lstruct, &lwmask);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (lwmask) from (lstruct)", name);
		return(status);
	}
	/* take the nonlinear step */
	if (wmethod_type == LEVENBERGMARQUARDT) {

		status = DoLMFitAlgRun(lstruct, sector);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not perform nonlinear fit (iteration = %d), (wmethod = '%s')", name, iter, shEnumNameGetFromValue("LMETHODSUBTYPE", wmethod_type));

			#if 0
			/* end stage algorithm -- emptying the remaining space */
			thError("%s: WARNING - running algorithm 'ENDSTAGE' prematurely \n", name);
			status = thLstructGetAlgByStage(lstruct, ENDSTAGE, &alg);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get the algorithm 'ENDSTAGE'", name);
				return(status);
			}
			status = thAlgorithmRun(alg, lstruct);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not run the algorithm 'ENDSTAGE'", name);
				return(status);
			}
	
			return(status);
			#else
			status = KillAllModelsInMemory(lstruct);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not kill all models in memory", name);
				return(status);
			}
			return(SH_GENERIC_ERROR);
			#endif
			}
	
	} else if (wmethod_type == QUASINEWTON) {
		
		status = DoQNFitAlgRun(lstruct, sector);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not perform nonlinear fit for (iteration = %d), (wmethod = '%s')", name, iter, shEnumNameGetFromValue("LMETHODSUBTYPE", wmethod_type));
			return(status);
		}

	} else if (wmethod_type == STEEPESTDESCENT) {
		status = DoGDFitAlgRun(lstruct, sector);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not perform nonlinear fit for (iteration = %d), (wmethod = '%s')", name, iter, shEnumNameGetFromValue("LMETHODSUBTYPE", wmethod_type));
			return(status);
		}
		if (method_type == SGD) {
			status = thLwmaskNextActive(lwmask);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not activate the next (mask)", name);
				return(status);
			}
			int index_active = -1;
			int mini_index_active = -1;
			status = thLwmaskGetIndexActive(lwmask, &index_active, &mini_index_active);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get (index_active) from (lwmask)", name);
				return(status);	
			}
			printf("%s: next mask (index = %d, mini_index = %d) is now activated for 'SGD' \n", name, index_active, mini_index_active); 
			if (index_active == 0 && mini_index_active == 0) {
				printf("%s: shuffling masks for 'SGD' \n", name);
				status = thLwmaskShuffle(lwmask);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not shuffle (lwmask)", name);
					return(status);	
				}	
				printf("%s: sampling (lwmask) for 'SGD' \n", name);
				#if 0
				status = thLwmaskSample(lwmask, DEFAULT_LWMASK_NSAMPLE, DEFAULT_LWMASK_NMINI);
				#else
				status = thLwmaskSample(lwmask, DEFAULT_LWMASK_NSAMPLE);
				#endif
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not sample (lwmask) for 'SGD'", name);
					return(status);
				}
			}

		}
	} else if (wmethod_type == PGDPURE) {

		if (method_type != PGD) {
			thError("%s: ERROR - expcted (method = 'PGD') but received '%s' when (work_type = '%s')", name, shEnumNameGetFromValue("LMETHODTYPE", method_type), shEnumNameGetFromValue("LMETHODSUBTYPE", wmethod_type));
			return(SH_GENERIC_ERROR);
		}
		status = DoPGDFitAlgRun(lstruct, sector);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not perform nonlinear fit for (iteration = %d), (wmethod = '%s')", name, iter, shEnumNameGetFromValue("LMETHODSUBTYPE", wmethod_type));
			return(status);
		}
	
	} else if (wmethod_type == MGDPURE) {

		if (method_type != MGD) {
			thError("%s: ERROR - expcted (method = 'MGD') but received '%s' when (work_type = '%s')", name, shEnumNameGetFromValue("LMETHODTYPE", method_type), shEnumNameGetFromValue("LMETHODSUBTYPE", wmethod_type));
			return(SH_GENERIC_ERROR);
		}
		status = DoMGDFitAlgRun(lstruct, sector);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not perform nonlinear fit for (iteration = %d), (wmethod = '%s')", name, iter, shEnumNameGetFromValue("LMETHODSUBTYPE", wmethod_type));
			return(status);
		}
	
	} else if (wmethod_type == NAGPURE) {

		if (method_type != NAG) {
			thError("%s: ERROR - expcted (method = 'NAG') but received '%s' when (work_type = '%s')", name, shEnumNameGetFromValue("LMETHODTYPE", method_type), shEnumNameGetFromValue("LMETHODSUBTYPE", wmethod_type));
			return(SH_GENERIC_ERROR);
		}
		status = DoNAGFitAlgRun(lstruct, sector);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not perform nonlinear fit for (iteration = %d), (wmethod = '%s')", name, iter, shEnumNameGetFromValue("LMETHODSUBTYPE", wmethod_type));
			return(status);
		}

	} else if (wmethod_type == RSVPURE) {

		if (method_type != RSV) {
			thError("%s: ERROR - expcted (method = 'RSV') but received '%s' when (work_type = '%s')", name, shEnumNameGetFromValue("LMETHODTYPE", method_type), shEnumNameGetFromValue("LMETHODSUBTYPE", wmethod_type));
			return(SH_GENERIC_ERROR);
		}
		status = DoRSVFitAlgRun(lstruct, sector);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not perform nonlinear fit for (iteration = %d), (wmethod = '%s')", name, iter, shEnumNameGetFromValue("LMETHODSUBTYPE", wmethod_type));
			return(status);
		}
	
	} else {
		
		thError("%s: ERROR - (work_type = '%s') not supported", name, shEnumNameGetFromValue("LMETHODSUBTYPE", wmethod_type));
		return(SH_GENERIC_ERROR);
	}	

	status = thLstructGetConvergence(lstruct, NULL, &flag, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get convergence flag from (lstruct)", name);
		return(status);
	}
	if (flag & GENERAL_CONVERGENCE) {
		thError("%s: WARNING - convergence flag '%s' raised after working method '%s' on sector '%s'", name, 
		shEnumNameGetFromValue("CONVERGENCE", flag), shEnumNameGetFromValue("LMETHODSUBTYPE", wmethod_type), shEnumNameGetFromValue("LSECTOR", sector));
	}	
	status = thLstructGetXupdate(lstruct, &update, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not obtain (apupdate) flag", name);
		return(status);
	}
	/* update the inverse Hessian matrix B */
	if (method_type == HYBRIDLM && update == UPDATED) {
		status = thLworkUpdateBqn(lwork, sector);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not update inverse Hessian (B_k)", name);
			return(status);
		}
	} 
	/* notice this uses new parameters */
	if (update == UPDATED) {
		#if DEEP_DEBUG_MLE 
		if (loop_started) {
			dbgX(lstruct, name, sector, "w-sector, middle of nonlinear fit");
		} else {	
			dbgX(lstruct, name, sector, "w-sector, start of nonlinear fit");
		}
		#endif	

		/* 
		status = thMakeJMatricesAlgRun(lstruct);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not update J-matricesi before a-fit", name);
			return(status);
		}
		*/
		/* trial */
		if (sector == PSECTOR || (LM_CONDUCT_AMPFIT && (float) phRandomUniformdev() < (float) LM_AMPFIT_CHANCE)) {
			status = DoAmplitudeFit(lstruct);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not perform the amplitude fit while (iter = %d) in the loop", name, iter);
				fflush(stderr);
				fflush(stdout);
				return(status);
			}
			status = thLstructGetConvergence(lstruct, NULL, &flag, NULL);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get convergence flag from (lstruct)", name);
				return(status);
			}
			if (flag & GENERAL_CONVERGENCE) {
				thError("%s: WARNING - convergence flag '%s' raised during amplitude fit", name, shEnumNameGetFromValue("CONVERGENCE", flag));
			}	
		}
		/* chi-squared value might slightly change after amplitude fitting */
		status = thLCalcChisqNAlgRun(lstruct);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not calculate (chisq)", name);
			return(status);
		}
		
		status = UpdateChisq(lstruct);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not update the (chisq) value", name);
			return(status);
		}

		/* covariance and all - should this covariance calculation be here at all */
		int singular = 0;
		status = thLCalcCovap(lstruct, &singular);
		if (singular) {
			#if DEBUG_MLE_SINGULAR
			thError("%s: WARNING - singularity in AP-sector covariance", name);
			fflush(stderr);
			fflush(stdout);
			#endif
		} else if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not calculate amplitude-parameter covariance", name);
			fflush(stderr);
			fflush(stdout);
			return(status);
		}

	#if DO_CAPTURE /* extensive debug mode, movie and step by step statistics */
		status = thCapture(capture);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not capture at (iter = %d)", name, iter);
			return(status);
		}
		status = thCaptureFlush(capture);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not flush (capture) related files", name);
			return(status);
		}
		fflush(stdout); /* this is so that the output to nohup.out file is uptodate */
	#endif

		
	}
	if (flag == CONTINUE) {
		if (wmethod_type == LEVENBERGMARQUARDT) {
			flag = CheckLMConvergence(lstruct, sector, &status);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not check recursion criteria", name);
				return(status);
			}
		} else if (wmethod_type == QUASINEWTON) {
			status = thLstructGetConvergence(lstruct, NULL, &flag, NULL);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get (convergnce) flag from (lmethod)", name);
				return(status);
			}
		} else if (wmethod_type == STEEPESTDESCENT) {
			status = thLstructGetConvergence(lstruct, NULL, &flag, NULL);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get (convergnce) flag from (lmethod)", name);
				return(status);
			}
		} else if (wmethod_type == PGDPURE || wmethod_type == MGDPURE || wmethod_type == NAGPURE || wmethod_type == RSVPURE) {
			status = thLstructGetConvergence(lstruct, NULL, &flag, NULL);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get (converence) flag from (lmethod)", name);
				return(status);
			}
		} else {
			thError("%s: ERROR - does not support (work type = '%s')", name, shEnumNameGetFromValue("LMETHODSUBTYPE", wmethod_type));
			return(SH_GENERIC_ERROR);
		}
	}

	#if DO_LFIT_RECORD
	status = do_lfit_record_insert_chisq_in_LM(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not insert (chisq) info in (lmethod)", name);
		return(status);
	}
	status = thLRecordLfit(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not record 'lmethod' at this step", name);
		return(status);
	}
	#endif

}
/* convergence criteia */
#if DEBUG_MLE
printf("%s: '%s' convergence flag raised \n", name, shEnumNameGetFromValue("CONVERGENCE", flag));
#endif

/* end stage algorithm -- emptying the remaining space */
printf("%s: running algorithm 'ENDSTAGE' \n", name);

status = thLstructGetAlgByStage(lstruct, ENDSTAGE, &alg);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the algorithm for the end stage", name);
	return(status);
}
status = thAlgorithmRun(alg, lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run the algorithm for the final stage", name);
	return(status);
}

/* there is no need to recalculate the Jtj matrix for linear fits only it has been calculated once before. 
 * the only use we had for it was the Covariance calculation which is done before this stage for linear fits
if (np == 0) {
	status = thMakeJMatricesAlgRun(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not calculate JpJ matrix while (np = %d)", name, np);
		return(status);
	}
}
*/
/* covariance and all */
/* covarinace is calculated within the LM-Amp loop with a correct value of JtJ matrix 
status = thLCalcCovap(lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calculate amplitude-parameter covariance", name);
	return(status);
	}
*/ 
return(SH_SUCCESS);
}



RET_CODE MakeImageAndMatrices(LSTRUCT *lstruct, RUNFLAG rflag) {

char *name = "MakeImageAndMatrices";
RET_CODE status = SH_SUCCESS;

#if THCLOCK
clock_t clk1, clk2;
clk1 = clock();
#endif

/* note: this function takes new parameter list as input */

/* first checking if the following has been allocated 
 | m - d > 
 | m_i > 
 | m_i_j > 
*/



/* 
   Making Images and Their Derivatives:

   1. construct an image package for each object / model / amplitude {m_i, m_i_j} 
	- have in mind to only include parameters which are relevant to the model 
   2. submit IMAGE PACKAGE along with the PAR MACHINE and the FUNCTION PACKAGE to the builder
*/

int nmodel, nparam;
status = thLstructGetNModel(lstruct, &nmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the number of models", name);
	return(status);
}
status = thLstructGetNPar(lstruct, &nparam);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the number of parameters for the fit", name);
	return(status);
}
/* now loop over models and build the images and the derivatives */
LIMPACK **impacks;
status = thLstructGetImpacks(lstruct, &impacks); /* should check that they are not null */
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get list of (impack)s", name);
	return(status);
}
/* old school - creates fragmentation oct 17, 2012
if (impack == NULL) impack = thLimpackNew();
*/
void *fparam;

/* get the new parameters */
MLEFL *p;
status = thLstructGetPN(lstruct, &p);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not retrieve list of new parameters from (lstruct)", name);
	return(status);
}
LMACHINE *lmachine;
status = thLstructGetLmachine(lstruct, &lmachine);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - coul not retrieve (lmachine) from (lstruct)", name);
	return(status);
}
int i, nmpar;
char *mname, **rnames;
char **mnames, ***enames;
int *nmpars;

mnames = lmachine->mnames;
enames = lmachine->enames;
nmpars = lmachine->nmpar;

/* dump pN values in model variables  */
status = thLDumpPar(lstruct, XNTOMODEL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump from (pN) onto (model)", name);
	return(status);
}

char *rname = shEnumNameGetFromValue("RUNFLAG", rflag);
/* instead of a loop over models, it should be a loop over algorithm elements */
LIMPACK *impack;
impack = impacks[0];
for (i = 0; i < nmodel; i++) {
	status = thConstructImagePack(lstruct, i, impack);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not construct the image pack for model %d", name, i);
		return(status);
	}
	/* get fparam 
	   this used to be construction process 
	   but now it simply derives what is already saved under object  
	*/

	/* deriving mname, rnames */
	mname = mnames[i];
	rnames = enames[i];
	nmpar = nmpars[i];

	status = thGetFParam(lmachine, i, &fparam);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not construct a parameter of the type needed for model (%d) '%s'", name, i, mname);		
		return(status);
	}
	status = thBuildImage(fparam, mname, rnames, nmpar, impack, rflag);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not build the image(s) for model (%d, '%s') (rflag = '%s', %d)", 
			name, i, mname, rname, (int) rflag);
		return(status);
	}
}

if (rflag == MEMORY_ESTIMATE) return(SH_SUCCESS);

/* is the calculation is done to compute the model, e.g. at the beginning of the simulation, 
   there is no need to calculate the derivatives and their brackets */
if (rflag == IMAGES) return(SH_SUCCESS);
if (rflag == INITIMAGES) return(SH_SUCCESS); /* this return was added on june 2012 */

/* now that the images and the derivatives are calculated, calculate the matrices */

MLEFL ****mijmkl, ***mimjk, **mimj, *mid;

/* extracting in the following order:
<m_i | W | d >
<m_i | W | m - d >
<m_i | W | m_j >
<m_i | W | m_j_k > 
<m_ij| W | m_k_l>

*/
status =  thExtractBrackets(lstruct, &mid, NULL, &mimj, NULL, &mimjk, &mijmkl);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract the matrices properly", name);
	return(status);
}
THREGION *a, *b;

/*
     < m_i_j | W | m - d >   	for DL / Dp, DDL / (Da Dp)  --- TO BE BUILT LATER 
       < m_i | W | m_j >        for DDL / (Da Da)
     < m_k_i | W | m_l_j >      for DDL / (Dp Dp)
       < m_i | W | m_k_j >      for DDL / (Da Dp)
*/

LWMASK *lwmask = NULL;
status = thLstructGetLwmask(lstruct, &lwmask);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwmask) from (lstruct)", name);
	return(status);
}

int j;
for (i = 0; i < nmodel; i++) {

	a = NULL;
	status = thExtractMi(lstruct, i, &a);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract model %d of %d", name, i, nmodel);
		return(status);
		}
	if (rflag == INITIMAGES_AND_DM || nmpars[i] != 0) { 
		MLEFL z = thRegBracket(a, lwmask, a, &status);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not calculate the image bracket for <m_%d|W|m_%d>", name, i, i);
			return(status);
		}
		mimj[i][i] = z;
		status = thLMijMklPut(lstruct, i, -1, i, -1, z);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not insert (mijmkl) for (%d, %d), (%d, %d)", name, i, -1, i, -1);
			return(status);
		}

		#if MLE_CATCH_NAN
		if (isnan(z) || isinf(z)) {
			thError("%s: ERROR - (NAN) value at <m_%d | m_%d>", name, i, i);
			return(SH_GENERIC_ERROR);
		}
		#endif
	}
	for (j = 0; j < i; j++) {
		if (rflag == INITIMAGES_AND_DM || 
			nmpars[i] != 0 || nmpars[j] != 0) {   /* if either of the models i or j has a non-linear fit component
								 then the dot product is expected to change between the steps */
			b = NULL;
			status = thExtractMi(lstruct, j, &b);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not extract model %d of %d", name, j, nmodel);
				return(status);
			}
			MLEFL z = thRegBracket(a, lwmask, b, &status);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not calculate the image bracket for <m_%d|W|m_%d>", name, i, j);
				return(status);
			}
			mimj[i][j] = z;
			mimj[j][i] = z;
			status = thLMijMklPut(lstruct, i, -1, j, -1, z);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not insert (mijmkl) for (%d, %d), (%d, %d)", name, i, -1, j, -1);
				return(status);
			}
			status = thLMijMklPut(lstruct, j, -1, i, -1, z);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not insert (mijmkl) for (%d, %d), (%d, %d)", name, j, -1, i, -1);
				return(status);
			}

			#if MLE_CATCH_NAN
			if (isnan(z) || isinf(z)) {
				thError("%s: ERROR - (NAN) value at <m_%d|m_%d>", name, i, j);
				return(SH_GENERIC_ERROR);
			}
			#endif
		}
	}
}

int nmp, ai, bi, k, l;
nmp = nmodel * nparam;
for (ai = 0; ai < nmp; ai++) {
	j = ai % nparam;
	i = ai / nparam;
	status = thExtractMij(lstruct, i, j, &a);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract m_%d_%d", name, i, j);
		return(status);
		}
	for (bi = 0; bi <= ai; bi++) {
		l = bi % nparam;
		k = bi / nparam;
		status = thExtractMij(lstruct, k, l, &b);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not extract m_%d_%d", name, k, l);
			return(status);
		}
		MLEFL z = 0.0;
		if (a != NULL && b != NULL) {
			z = thRegBracket(a, lwmask, b, &status);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not calculate the image bracket for <m_%d_%d|W|m_%d_%d>", name, i, j, k, l);
				return(status);
			}
		}
		#if MLE_CATCH_NAN
		if (isnan(z) || isinf(z)) {
			thError("%s: ERROR - (NAN) value at <m_%d_%d | m_%d_%d>",
				name, i, j, k, l);
			return(SH_GENERIC_ERROR);
		}
		#endif

		#if OLD_MAP
		mijmkl[i][j][k][l] = z;
		mijmkl[k][l][i][j] = z;
		#else
		status = thLMijMklPut(lstruct, i, j, k, l, z);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not insert (mijmkl) for (%d, %d), (%d, %d)", name, i, j, k, l);
			return(status);
		}
		status = thLMijMklPut(lstruct, k, l, i, j, z);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not insert (mklmij) for (%d, %d), (%d, %d)", name, k, l, i, j);
			return(status);
		}
		#endif
		}
}

/* NOTE - Sep 12, 2011:

   Matrix [mimjk] is only needed to calculate cross elements of Jt J between a-p sectors.
   If the LS / LM calculation is done separately and the convergence criteria is tested 
   such that there is no need for cross-elements of Jt J, a lot of time, 
   t = nc x nm x t(bracket), will be saved by dropping the calculation of [mimjk] inside 
   the main recursion loop and instead calculating it after exiting the loop before 
   calculating covariance

*/

for (i = 0; i < nmodel; i++) {
	status = thExtractMi(lstruct, i, &a);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract model %d of %d", name, j, nmodel);
		return(status);
		}
	for (j = 0; j < nmodel; j++) {
	for (k = 0; k < nparam; k++) {
		status = thExtractMij(lstruct, j, k, &b);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not extract m_%d_%d", name, k, l);
			return(status);
		}
	MLEFL z = 0.0;	
	if (a != NULL && b != NULL) {
		z = thRegBracket(a, lwmask, b, &status);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not calculate the image bracket for <m_%d_%d|W|m_%d_%d>", name, i, j, k, l);
			return(status);
		}
	}
	#if MLE_CATCH_NAN
	if (isnan(z) || isinf(z)) {
		thError("%s: ERROR - (NAN) value at <m_%d | m_%d_%d>", name, i, j, k);
		return(SH_GENERIC_ERROR);
	}
	#endif

	#if OLD_MAP
		mimjk[i][j][k] = z;
	#else
		status = thLMijMkPut(lstruct, j, k, i, z);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not put value for (mimjk) for (%d), (%d, %d)", name, i, j, k);
			return(status);
		}
	#endif
	}
	}
}

REGION *c;
status = thExtractD(lstruct, &c);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract the data image", name);
	return(status);
	}
for (i = 0; i < nmodel; i++) {
	if (rflag == INITIMAGES_AND_DM || nmpars[i] != 0) {
		status = thExtractMi(lstruct, i, &b);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not extract model %d of %d", name, j, nmodel);
			return(status);
		}
		mid[i] = thRegHalfBracket(c, lwmask, b, &status);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not calculate the image bracket for <m_%d|W|d>", name, i);
		}
		#if MLE_CATCH_NAN
		if (isnan(mid[i]) || isinf(mid[i])) {
			thError("%s: ERROR - (NAN) value at <m_%d | D>", name, i);
			return(SH_GENERIC_ERROR);
		}
		#endif
		if (mid[i] < (MLEFL) 0.0) {
			thError("%s: WARNING - found < m[%d] | D > = %20.10G < 0, model = '%s', nmpars = %d", i, (double) mid[i], mnames[i], nmpars[i]);
		}
	}
}
/* <m_i_j | W | m - d > is postponed to the time amp's are calculated */

return(SH_SUCCESS);
}	

RET_CODE DoAmplitudeFit(LSTRUCT *lstruct) {

char *name = "DoAmplitudeFit";

#if DEBUG_MEMORY_VERBOSE
printf("%s: memory statistics before conducting (A-fit)", name);
shMemStatsPrint();
fflush(stdout);
fflush(stderr);
#endif


RET_CODE status = SH_SUCCESS;

MLEFL *dmi, **mimj; 
MLEFL *aN, **cov;
MLEFL *aT, **covT;
int namp;

/* extract the relevant matrices from within the data structure */

status = ExtractLSMatrices(lstruct, &dmi, &mimj, &aN, &aT, &cov, &covT, &namp);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the relevant matrices", name);
	fflush(stdout);
	fflush(stderr);
	return(status);
	}

if (aT == NULL || covT == NULL) {
	thError("%s: ERROR - null (aT) or (covT) array in (lstruct)", name);
	return(SH_GENERIC_ERROR);
}

LWORK *lwork = NULL;
status = thLstructGetLwork(lstruct, &lwork);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwork) from (lstruct)", name);
	return(status);
}
LSECTOR sector = ASECTOR;
MAT *wMatA = NULL, *wMatQ = NULL;
VEC **wVecs = NULL;
status = thLworkGetWMatVec(lwork, sector, &wMatA, &wMatQ, &wVecs);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get working Mat, Vec for '%s'", 
	name, shEnumNameGetFromValue("LSECTOR", sector));
	return(status);
}	

int i, j;
memcpy(aT, aN, namp * sizeof(MLEFL));
for (i = 0; i < namp; i++) {
	for (j = 0; j < namp; j++) {
		covT[i][j] = cov[i][j];
	}
}

for (i = 0; i < namp; i++) {
	aN[i] = dmi[i];
	for (j = 0; j < namp; j++) {
		cov[i][j] = (MLEFL) mimj[i][j];
	}
	}

#if MLE_CATCH_NAN
int err_nan = 0;
for (i = 0; i < namp; i++) {
	if (isnan(dmi[i]) || isinf(dmi[i])) {
		thError("%s: ERROR - (NAN) detected in < d | m_%d >", name, i);
		err_nan++;
	}
	for (j = 0; j < namp; j++) {
		if (isinf(mimj[i][j]) || isnan(mimj[i][j])) {
			thError("%s: ERROR - (NAN) detected in < m_%d | m_%d >", name, i, j);
			err_nan++;
		}
	}
}
if (err_nan != 0) {
	thError("%s: ERROR - (NAN) entries detected in (%d) occasions", name, err_nan);
	fflush(stderr);
	fflush(stdout);
	return(SH_GENERIC_ERROR);
}
#endif

#if (DEBUG_MLE_COV)
if (dbgInvCov(cov, namp, name, "- ASECTOR") != 0) {
	thError("%s: ERROR - could not print out elements of covariance matrix", name);
	fflush(stderr);
	fflush(stdout);
	return(SH_GENERIC_ERROR);
}
#endif	

#if DEEP_DEBUG_MLE
dbgX(lstruct, name, ASECTOR, "A-sector - before inversion");
dbgX(lstruct, name, PSECTOR, "P-sector - before inversion");
#endif

int singular = 0;
status = LMatrixInvert(cov, dmi, aN, namp, &singular);

#if (DEBUG_MLE_COV)
if (dbgCov(cov, namp, name, "- ASECTOR") != 0) {
	thError("%s: ERROR - could not print out elements of inverse covaraince matrix", name);
	fflush(stderr);
	fflush(stdout);
	return(SH_GENERIC_ERROR);
}
#endif

#if DEEP_DEBUG_MLE
dbgX(lstruct, name, ASECTOR, "A-sector - after inversion");
dbgX(lstruct, name, PSECTOR, "P-sector - after inversion");
#endif

if (singular) {
	thError("%s: WARNING - possible degeneracy discovered in linear fit [n_amp = %d]", name, namp);
	#if PSEUDO_INVERSE_FOR_SINGULAR
	for (i = 0; i < namp; i++) {
		aN[i] = dmi[i];
		for (j = 0; j < namp; j++) {
			cov[i][j] = (MLEFL) mimj[i][j];
		}
	}
	#if DEEP_DEBUG_MLE
	dbgX(lstruct, name, ASECTOR, "A-sector - before SVD inversion");
	#endif
	status = LMatrixSolveSVD(cov, dmi, aN, namp, wMatA, wMatQ, wVecs);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not use SVD to calculate derivate based step", name);
		return(status);
	}

	#if DEEP_DEBUG_MLE	
	dbgX(lstruct, name, ASECTOR, "A-sector - after SVD inversion");
	#endif
	#else

	memcpy(aN, aT, namp * sizeof(MLEFL));
	for (i = 0; i < namp; i++) {
		for (j = 0; j < namp; j++) {
		 	cov[i][j] = covT[i][j];
		}
	}

	status = UpdateLMConvergence(lstruct, SINGULAR_CONVERGENCE);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not place convergence flag in (lstruct)", name);
		return(status);
	}
	return(SH_SUCCESS);
	#endif
} else if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not conduct matrix inversion for linear fit", name);
	return(status);
}

#if MLE_CATCH_NAN
LMACHINE *map = NULL;
status = thLstructGetLmachine(lstruct, &map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (map) from (lstruct)", name);
	return(status);
}
for (i = 0; i < namp; i++) {
	char *mname_i = NULL;
	void *objc_i = NULL;
	void *p_i = NULL;
	status = thMapmachineGetModelInfo(map,  i, &mname_i, &p_i, &objc_i);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get info for (imodel = %d) from (map)", name, i);
		return(status);
	}
	if (isnan(aN[i]) || isinf(aN[i])) {
		thError("%s: ERROR - (NAN) detected in aN[%d]", name, i);
		err_nan++;
	}
	#if DEBUG_MLE_PRINT_MIMJ
	if (!strcmp(mname_i, "Exp")) {
		EXPPARS *p_ii = p_i;
		printf("%s: i = %d, model = '%s', m_diag[%d] = %g, re = %g, xc = %g, yc = %g \n", name, i, mname_i, i, (float) mimj[i][i], p_ii->re, p_ii->xc, p_ii->yc);
	} else if (!strcmp(mname_i, "deV")) {
		DEVPARS *p_ii = p_i;
		printf("%s: i = %d, model = '%s', m_diag[%d] = %g, re = %g, xc = %g, yc = %g \n", name, i, mname_i, i, (float) mimj[i][i], p_ii->re, p_ii->xc, p_ii->yc);
	} else if (!strcmp(mname_i, "Pl")) {
		POWERLAWPARS *p_ii = p_i;
		printf("%s: i = %d, model = '%s', m_diag[%d] = %g, re = %g, xc = %g, yc = %g \n", name, i, mname_i, i, (float) mimj[i][i], p_ii->re, p_ii->xc, p_ii->yc);
	} else {
		printf("%s: i = %d, model = '%s', m_diag[%d] = %g, re = N/A, xc = N/A, yc = N/A \n", name, i, mname_i, i, (float) mimj[i][i]);
	}
	#endif
	for (j = 0; j < namp; j++) { 
		char *mname_j = NULL;
		void *objc_j = NULL;
		void *p_j = NULL;
		status = thMapmachineGetModelInfo(map, j, &mname_j, &p_j, &objc_j);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get info or (imodel = %d) from (map)", name, j);
			return(status);
		}
		if (isnan(cov[i][j]) || isinf(cov[i][j])) {
			thError("%s: ERROR - (NAN) detected in cov[%d][%d] model, objc[%d]='%s', %p; model, objc[%d]='%s', %p", 
			name, i, j, i, mname_i, (void *) objc_i, j, mname_j, (void *) objc_j);
			err_nan++;
		}
	}
}
if (err_nan != 0) {
	thError("%s: ERROR - (NAN) detected in (%d) occasions", name, err_nan);
	fflush(stderr);
	fflush(stdout);
	return(SH_GENERIC_ERROR);
}
#endif

#if DEEP_DEBUG_MLE
dbgX(lstruct, name, ASECTOR, "A-sector - before X-update");
#endif
status = UpdateXFromXN(lstruct, ASECTOR);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update the (a) values", name);
	fflush(stderr);
	fflush(stdout);
	return(status);
}
#if DEEP_DEBUG_MLE
dbgX(lstruct, name, ASECTOR, "A-sector - after X-update");
#endif

#if DEBUG_MEMORY_VERBOSE
printf("%s: memory statistics after conducting (A-fit)", name);
shMemStatsPrint();
fflush(stdout);
fflush(stderr);
#endif

return(SH_SUCCESS);
}


RET_CODE thMakeM(LSTRUCT *lstruct) {

/* IMPORTANT NOTE - Sep 9, 2011:

This function cannot tell the difference between new and old parameters. It used the brackets calculated last.
Because the LWORK structure has only one place holder for brackets (new and old) one should make sure that 
this function is not called once more after rejecting a new step

*/

char *name = "thMakeM";
RET_CODE status = SH_SUCCESS;

#if THCLOCK
clock_t clk1, clk2;
clk1 = clock();
#endif

if (lstruct == NULL) {
	thError("%s: ERROR - null or improperly allocated LSTRUCT", name);
	return(status);
	}

/* now first calculate < m_i_j | W | m - d > */

REGION *m;
int nmodel;

status = thExtractM(lstruct, &m);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract model image", name);
	return(status);
	}

shRegClear(m);

status = thLstructGetNModel(lstruct, &nmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract number of models", name);
	return(status);
}
MLEFL *amp;
status = thLstructGetAmp(lstruct, &amp);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract amplitudes", name);
	return(status);
}

int i;
THREGION *mi;
for (i = 0; i < nmodel; i++) {
	status = thExtractMi(lstruct, i, &mi);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract m_%d", name, i);
		return(status);
	}
	if (i == 0) {
		status = thRegPutModelComponent(m, mi, amp[i]);
	} else {
		status = thRegAddModelComponent(m, mi, amp[i]);
	}
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put the model component %d in the final image", name, i);
		return(status);
	}
}	


REGION *dmm, *d;
status = thExtractD(lstruct, &d);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract | d >", name);
	return(status);
}
status = thExtractDmm(lstruct, &dmm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract | m - d >", name);
	return(status);
}
/* changing M - D convention to D - M to save space on jtj and mijdmm similarities for amplitudes */
status = thRegMinusReg(dmm, d, m);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not subtract |m> from |d>", name);
	return(status);
}

#if THCLOCK
clk2 = clock();
printf("%s: CLOCKING - [%g] CPUs \n", name, ((float) (clk2 - clk1)) / CLOCKS_PER_SEC);
#endif

return(SH_SUCCESS);
}

RET_CODE thMakeMAlgRun(LSTRUCT *lstruct) {

char *name = "thMakeMAlgRun";
RET_CODE status = SH_SUCCESS;

if (lstruct == NULL) {
	thError("%s: ERROR - null or improperly allocated LSTRUCT", name);
	return(status);
	}

/* now first calculate < m_i_j | W | m - d > */

ALGORITHM *alg = NULL;
status = thLstructGetAlgByStage(lstruct, SIMSTAGE, &alg);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get algorithm for 'SIMSTAGE'", name);
	return(status);
}
if (alg == NULL) {
	thError("%s: ERROR - null algorithm found for 'SIMSTAGE'", name);
	return(SH_GENERIC_ERROR);
}


REGION *m = NULL;
status = thExtractM(lstruct, &m);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract model image", name);
	return(status);
	}

shRegClear(m);

/* this is possibly source of error when degenracy happens and the code exits with zero amplitude */
status = thLDumpX(lstruct, XTOMODEL, wsector);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump (x = [a, p]) onto model", name);
	return(status);
}

status = thAlgorithmRun(alg, lstruct); 
REGION *dmm = NULL, *d = NULL;
status = thExtractD(lstruct, &d);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract | d >", name);
	return(status);
}

status = thExtractDmm(lstruct, &dmm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract | m - d >", name);
	return(status);
}

status = thRegMinusReg(dmm, d, m);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not subtract |m> from |d>", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE thMakeJMatricesSimpleRun(LSTRUCT *lstruct) {

/* IMPORTANT NOTE - May 14, 2011:

This function cannot tell the difference between new and old parameters. It used the brackets calculated last.
Because the LWORK structure has only one place holder for brackets (new and old) one should make sure that 
this function is not called once more after rejecting a new step

*/

char *name = "thMakeJMatricesSimpleRun";
RET_CODE status = SH_SUCCESS;

#if QUASI_NEWTON
thError("%s: ERROR - this function in not supported in quasi-newton mode", name);
return(SH_GENERIC_ERROR);
#endif

#if THCLOCK
clock_t clk1, clk2;
clk1 = clock();
#endif

if (lstruct == NULL) {
	thError("%s: ERROR - null or improperly allocated LSTRUCT", name);
	return(status);
	}

/* assume that | m >, |d - m > are claculated before calling this function (by thMakeM) */
 
/* now first calculate < m_i_j | W | m - d > */

REGION *m;
int nmodel;

status = thExtractM(lstruct, &m);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract model image", name);
	return(status);
	}
status = thLstructGetNModel(lstruct, &nmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract number of models", name);
	return(status);
}
MLEFL *amp;
status = thLstructGetAmp(lstruct, &amp);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract amplitudes", name);
	return(status);
}

int i, j;

REGION *dmm, *d;
status = thExtractD(lstruct, &d);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract | d >", name);
	return(status);
}
status = thExtractDmm(lstruct, &dmm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract | d - m >", name);
	return(status);
}

/* this step must have already been taken in thExtractM
status = thRegMinusReg(dmm, d, m);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not subtract |m> from |d>", name);
	return(status);
}
*/

/* extract the matrix placeholder */
#if OLD_MAP
MLEFL **mijdmm, ****mijmkl;
status =  thExtractBrackets(lstruct, NULL, NULL, NULL, &mijdmm, NULL ,&mijmkl);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract the place holder for < m_i_j | W | m - d >, or < m_i_j | W | m_k_l > ", name);
	return(status);
}
#endif

int nparam;
THREGION *mij;
status = thLstructGetNPar(lstruct, &nparam);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract number of parameters", name);
	return(status);
	}

LWMASK *lwmask = NULL;
status = thLstructGetLwmask(lstruct, &lwmask);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwmask) from (lstruct)", name);
	return(status);
}

/* now do the bracketing */
for (i = 0; i < nmodel; i++) {
	for (j = 0; j < nparam; j++) {
		status = thExtractMij(lstruct, i, j, &mij);
		if (status != SH_SUCCESS)  {
			thError("%s: ERROR - could not extract m_%d_%d", i, j);
			return(status);
		}
		MLEFL z = 0.0;
		if (mij != NULL && dmm != NULL) {
			z = thRegHalfBracket(dmm, lwmask, mij, &status);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not calculate < m_%d_%d | W | m - d >", name, i, j);
				return(status);
			}
		}
		#if OLD_MAP
		mijdmm[i][j] = z;
		#else 
		status = thLMijDmmPut(lstruct, i, j, z);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not place element in MijDmm for (%d, %d)", name, i, j);
			return(status);
		}
		#endif
		#if MLE_CATCH_NAN
		if (isnan(z) || isinf(z)) {
			thError("%s: ERROR - (NAN) detected in < M_%d_%d | D - M >", name, i, j);
			return(SH_GENERIC_ERROR);
		}
		#endif
	}
}


#if MLE_DO_JTJ_APSECTOR
MLEFL **jtjap, *jtapdmm;
status = thExtractJtj(lstruct, APSECTOR, &jtjap);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract JtJ placeholder", name);
	return(status);
	}
status = thExtractJtdmm(lstruct, APSECTOR, &jtapdmm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract Jt (d - m) placeholder", name);
	return(status);
}

MLEFL **MM = NULL;
int nmm = -1;
status = thLworkGetMM(lstruct->lwork, &MM, &nmm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get <M_i | W | M_j> matrix", name);
	return(status);
}
for (i = 0; i < nmodel; i++) {
	for (j = 0; j < nmodel; j++) {
		jtjap[i][j] = MM[i][j];
	}
} 		

for (i = 0; i < nmodel; i++) {
	int k;
	for (k = 0; k < nparam; k++) {
		MLEDBLE x = (MLEDBLE) 0.0;
		for (j = 0; j < nmodel; j++) {
			MLEFL z = (MLEFL) 0.0;
			status = thLMijMkGet(lstruct, j, k, i, &z);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get <m_%d | W | D m_%d / Dx_%d>", name, i, j, k);
				return(status);
			}
			x += (MLEDBLE) z * (MLEDBLE) amp[j];
		}
		jtjap[i][k + nmodel] = (MLEFL) x;
		jtjap[k + nmodel][i] = (MLEFL) x;
	}
} 


MLEFL *MDmm = NULL;
status = thLworkGetMDmm(lstruct->lwork, &MDmm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get < M_i | D - M > vector", name);
	return(status);
}
for (i = 0; i < nmodel; i++) {
	jtapdmm[i] = MDmm[i];
	if (isnan(MDmm[i]) | isinf(MDmm[i])) {
		thError("%s: ERROR - (NAN) found in < M[%d] | D - M >", name, i);
		return(SH_GENERIC_ERROR);
	}
}	


#endif


/* extracting place holders for j-matrices */
MLEFL **jtj, *jtdmm;
status = thExtractJtj(lstruct, PSECTOR, &jtj);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract JtJ placeholder", name);
	return(status);
	}
status = thExtractJtdmm(lstruct, PSECTOR, &jtdmm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract Jt (d - m) placeholder", name);
	return(status);
}
/* now calculate the J matrices */
MLEDBLE x;
int k, l; 

#if MLE_CATCH_NAN
int err_nan = 0;

for (i = 0; i < nparam; i++) {
	for (j = 0; j <= i; j++) {
		for (k = 0; k < nmodel; k++) {
		for (l = 0; l < nmodel; l++) {
			MLEFL z = 0.0;
			#if OLD_MAP
			z = (mijmkl[k][i][l][j]);
			#else 
			status = thLMijMklGet(lstruct, k, i, l, j, &z);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get value for (mijmkl) for (%d, %d) (%d, %d)", 
				name, k, i, l, j);
				return(status);
			}
			#endif	
			if (isinf(z) || isnan(z)) {
				thError("%s: ERROR - (NAN) detected in < M_%d_%d | M_%d_%d >", name, k, i, l, j);
				err_nan++;
			}
		}
		}
	}		
}

for (i = 0; i < nparam; i++) {
	for (j = 0; j < nmodel; j++) {
		MLEFL z = 0.0;
		#if OLD_MAP
		z = mijdmm[j][i];
		#else
		status = thLMijDmmGet(lstruct, j, i, &z);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get element of (mijdmm) for (%d, %d)", name, j, i);
			return(status);
		}
		#endif	
		/* again this has to be replaced with the positive sign due to a change from M - D convention to D - M */
		if (isinf(z) || isnan(z)) {
			thError("%s: ERROR - (NAN) detected in < M_%d_%d | D - M >", name, j, i);
			err_nan++;
		}
	}
}

for (i = 0; i < nmodel; i++) {
	if (isnan(amp[i]) ||  isinf(amp[i])) {
		thError("%s: ERROR - (NAN) detected in amplitude_%d", name, i);
		err_nan++;
	}
}
if (err_nan != 0) {
	thError("%s: ERROR - (NAN) detected in (%d) occasions", name, err_nan);
	fflush(stderr);
	fflush(stdout);
	return(SH_GENERIC_ERROR);
}

#endif

for (i = 0; i < nparam; i++) {
	for (j = 0; j <= i; j++) {
		x = (MLEDBLE) 0.0;
		for (k = 0; k < nmodel; k++) {
		for (l = 0; l < nmodel; l++) {
			MLEFL z = 0.0;
			#if OLD_MAP
			z = (mijmkl[k][i][l][j]);
			#else 
			status = thLMijMklGet(lstruct, k, i, l, j, &z);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get value for (mijmkl) for (%d, %d) (%d, %d)", 
				name, k, i, l, j);
				return(status);
			}
			#endif	
			x += ((MLEDBLE) amp[k]) * ((MLEDBLE) amp[l]) * (MLEDBLE) z;
		}
		}
		jtj[i][j] = (MLEFL) x;
		if (i != j) jtj[j][i] = x;
	}		
}


for (i = 0; i < nparam; i++) {
	x = (MLEDBLE) 0.0;
	for (j = 0; j < nmodel; j++) {
		MLEFL z = 0.0;
		#if OLD_MAP
		z = mijdmm[j][i];
		#else
		status = thLMijDmmGet(lstruct, j, i, &z);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get element of (mijdmm) for (%d, %d)", name, j, i);
			return(status);
		}
		#endif	
		/* again this has to be replaced with the positive sign due to a change from M - D convention to D - M */
		if (isnan(z) || isinf(z)) {
			thError("%s: ERROR - (NAN) found in < M[%d, %d] | D - M >", name, j, i);
			err_nan++;
		}
		x += ((MLEDBLE) amp[j]) * (MLEDBLE) z;
	}
	jtdmm[i] = (MLEFL) x;
	if (isnan(x) || isinf(x)) {
		thError("%s: ERROR - (NAN) found in Jt[p] | D - M >, i_p = %d", name, i);
		err_nan++;
	} 
}

#if MLE_CATCH_NAN
for (i = 0; i < nparam; i++) {
	for (j = 0; j < nparam; j++) {
		if (jtj[i][j] != jtj[i][j]) {
			thError("%s: ERROR - (NAN) detected in JtJ[%d][%d]", name, i, j);
			err_nan++;
		}
	}
}
for (i = 0; i < nparam; i++) {
	if (jtdmm[i] != jtdmm[i]) {
		thError("%s: ERROR - (NAN) detected in Jt [ D-M ][%d]", name, i);
		err_nan++;
	}
}
if (err_nan != 0) {
	thError("%s: ERROR - (NAN) detected in (%d) occasions", name, err_nan);
	return(SH_GENERIC_ERROR);
}
#endif
 

#if THCLOCK
clk2 = clock();
printf("%s: CLOCKING - [%g] CPUs \n", name, ((float) (clk2 - clk1)) / CLOCKS_PER_SEC);
#endif

/* J matrices are calculated */
return(status);
}

RET_CODE thMakeJMatricesAlgRun(LSTRUCT *lstruct) {

/* IMPORTANT NOTE - May 14, 2011:

This function cannot tell the difference between new and old parameters. It used the brackets calculated last.
Because the LWORK structure has only one place holder for brackets (new and old) one should make sure that 
this function is not called once more after rejecting a new step

*/

char *name = "thMakeJMatricesAlgRun";
#if DEBUG_MEMORY_VERBOSE
printf("%s: memory statistics before making J-matrices", name);
shMemStatsPrint();
fflush(stdout);
fflush(stderr);
#endif

LSECTOR my_wsector = wsector;
if (my_wsector == PSECTOR) my_wsector = APSECTOR;

RET_CODE status = SH_SUCCESS;

#if THCLOCK
clock_t clk1, clk2;
clk1 = clock();
#endif

if (lstruct == NULL) {
	thError("%s: ERROR - null or improperly allocated LSTRUCT", name);
	return(status);
	}

LMETHOD *lm = NULL;
status = thLstructGetLmethod(lstruct, &lm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - coud not get (lmethod) from (lstruct)", name);
	return(status);
}

#if QUASI_NEWTON
LMETHODTYPE lmtype = UNKNOWN_LMETHODTYPE;
status = thLmethodGetType(lm, &lmtype, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmtype) from (lmethod)", name);
	return(status);
}
int doDeriv = 1;
if (lmtype == RSV) {
	doDeriv = 0;
	thError("%s: WARNING - all derivatives will be avoided for (lmtype = '%s')", name, shEnumNameGetFromValue("LMETHODTYPE", lmtype));
}

if (lmtype == HYBRIDLM || lmtype == NEWTON) {
	LM_STEP_STATUS step_status = UNKNOWN_LM_STEP_STATUS;
	status = thLstructGetConvergence(lstruct, NULL, NULL, &step_status);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (step_status)", name);
		return(status);
	}

	XUPDATE update = UNKNOWN_XUPDATE;
	status = thLstructGetXupdate(lstruct, &update, my_wsector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (xupdate) flag from (lstruct)", name);
		return(status);
	}
	if (update == UPDATED || step_status == LM_STEP_ACCEPTED) {
		printf("%s: updating QN matrices for (update = '%s', step_status = '%s') \n", 
		name, shEnumNameGetFromValue("XUPDATE", update), 
		shEnumNameGetFromValue("LM_STEP_STATUS", step_status));
		status = thLworkCopyJBqnPrev(lstruct->lwork);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not copy old (Jt | d - m > and B_qn) matrices", name);
			return(status);
		}
	} else if (update == OUTDATED || step_status == LM_STEP_REJECTED) {
		printf("%s: will not update QN matrices because (update = '%s', step_status = '%s') \n", name, shEnumNameGetFromValue("XUPDATE", update), shEnumNameGetFromValue("LM_STEP_STATUS", step_status));
	} else {
		thError("%s: ERROR - unsupported (update) flag = '%s'", name, shEnumNameGetFromValue("XUPDATE", update));
		return(SH_GENERIC_ERROR);
	}	
} else if (lmtype == GD || lmtype == SGD || lmtype == PGD || lmtype == MGD || lmtype == NAG || lmtype == RSV) {
	LM_STEP_STATUS step_status = UNKNOWN_LM_STEP_STATUS;
	status = thLstructGetConvergence(lstruct, NULL, NULL, &step_status);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (step_status)", name);
		return(status);
	}

	XUPDATE update = UNKNOWN_XUPDATE;
	status = thLstructGetXupdate(lstruct, &update, my_wsector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (xupdate) flag from (lstruct)", name);
		return(status);
	}
	if (update == UPDATED || step_status == LM_STEP_ACCEPTED) {
		printf("%s: updating PGD / SGD / GD / AdeDelta  matrices for (update = '%s', step_status = '%s') \n", 
		name, shEnumNameGetFromValue("XUPDATE", update), 
		shEnumNameGetFromValue("LM_STEP_STATUS", step_status));
		status = thLworkCopyJAdaDeltaPrev(lstruct->lwork, lstruct->lmethod);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not copy old (Jt | d - m > and AdaDelta) matrices", name);
			return(status);
		}
	} else if (update == OUTDATED || step_status == LM_STEP_REJECTED) {
		printf("%s: will not update PGD / SGD / GD / AdaDelta matrices because (update = '%s', step_status = '%s') \n", name, shEnumNameGetFromValue("XUPDATE", update), shEnumNameGetFromValue("LM_STEP_STATUS", step_status));
	} else {
		thError("%s: ERROR - unsupported (update) flag = '%s'", name, shEnumNameGetFromValue("XUPDATE", update));
		return(SH_GENERIC_ERROR);
	}	
} else {
	thError("%s: WARNING - will not update lwork matrices for (lm_type = '%s')", name, shEnumNameGetFromValue("LMETHODTYPE", lmtype));
}
#endif
/* assume that | m >, |d - m > are claculated before calling this function (by thMakeM) */
 
/* now first calculate < m_i_j | W | m - d > */

int nmodel;
int i, j;

status = thLstructGetNModel(lstruct, &nmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract number of models", name);
	return(status);
}
MLEFL *amp;
status = thLstructGetAmp(lstruct, &amp);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract amplitudes", name);
	return(status);
}

#if MLE_CATCH_NAN
int err_nan = 0;
for (i = 0; i < nmodel; i++) {
	if (isnan(amp[i]) || isinf(amp[i])) {
		thError("%s: ERROR - (NAN) detected at Amp[%d]", name, i);
		err_nan++;
	}
}
if (err_nan != 0) {
	thError("%s: ERROR - (NAN) detected (%d) times", name, err_nan);
	fflush(stderr);
	fflush(stdout);
	return(SH_GENERIC_ERROR);
}
#endif


REGION *d;
status = thExtractD(lstruct, &d);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract | d >", name);
	return(status);
}

int nparam;
status = thLstructGetNPar(lstruct, &nparam);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract number of parameters", name);
	return(status);
	}

LWMASK *lwmask = NULL;
status = thLstructGetLwmask(lstruct, &lwmask);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwmask) from (lstruct)", name);
	return(status);
}

/* now do the bracketing */
do {

if (!doDeriv) {
	thError("%s: WARNING - avoiding calculation of < M | M > brackets with derivatives", name);
	break;
}

for (i = 0; i < nmodel; i++) {
	for (j = 0; j < nparam; j++) {
		int k;
		MLEFL z;
		status = thLMijDGet(lstruct, i, j, &z);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get <m_%d_%d | W | D >", 
				name, i, j);
			return(status);
		}
		#if MLE_CATCH_NAN
		if (isnan(z) || isinf(z)) {
			thError("%s: ERROR - (NAN) detected in < m_%d_%d | D >", name, i, j);
			err_nan++;
		}
		#endif
		MLEDBLE z_dble = (MLEDBLE) z;
		for (k = 0; k < nmodel; k++) {
			MLEFL z2;
			status = thLMijMkGet(lstruct, i, j, k, &z2);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get <m_%d_%d | m_%d >", name, i, j, k);
				return(status);
			}
			z_dble -=  ((MLEDBLE) amp[k]) * ((MLEDBLE) z2);
			#if MLE_CATCH_NAN
			if (isnan(z2) ||  isinf(z2)) {
				thError("%s: ERROR - (NAN) detected in <m_%d_%d | m_%d>", name, i, j, k);
				err_nan++;
			}
			if (isnan(amp[k]) || isinf(amp[k])) {
				thError("%s: ERROR - (NAN) detected in Amp[%d]", name, k);
				err_nan++;
			}
			if (isnan(z_dble) || isinf(z_dble)) {
				thError("%s: ERROR - (NAN) detected in (Amp_%d)< m_%d_%d|m_%d> sum", name, k, i, j, k);
				err_nan++;
			}
			#endif
			#if VERY_DEEP_DEBUG_MLE
			printf("%s: (i = %2d, j = %2d, k = %2d), < m_i_j | m_k > = %g \n", name, i, j, k, (float) z2);
			#endif

		}
		z = (MLEFL) z_dble;
		#if VERY_DEEP_DEBUG_MLE
		printf("%s: (i = %2d, j = %2d), <m_i_j | d - m > = %g \n", name, i, j, (float) z); 
		#endif
		status = thLMijDmmPut(lstruct, i, j, z);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not place element in MijDmm for (%d, %d)", name, i, j);
			return(status);
		}
		#if MLE_CATCH_NAN
		if (isnan(z) || isinf(z)) {
			thError("%s: ERROR - (NAN) detected in < m_%d_%d | d - m >", name, i, j);
			err_nan++;
		}
		#endif
	}
}
} while (0);

MLEFL **MiMj = NULL;
status = thLExtractMiMj(lstruct, &MiMj);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get < Mi | Mj > matrix", name);
	return(status);
}
for (i = 0; i < nmodel; i++) {
	for (j = -1; j < 0; j++) {
		int k;
		MLEFL z;
		status = thLMijDGet(lstruct, i, j, &z);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get <m_%d_%d | W | D >", 
				name, i, j);
			return(status);
		}
		#if MLE_CATCH_NAN
		if (isnan(z) || isinf(z)) {
			thError("%s: ERROR - (NAN) detected in < m_%d_%d | D >", name, i, j);
			err_nan++;
		}
		#endif
		MLEDBLE z_dble = (MLEDBLE) z;
		for (k = 0; k < nmodel; k++) {
			MLEFL z2;
			z2 = MiMj[i][k];
			z_dble -=  ((MLEDBLE) amp[k]) * ((MLEDBLE) z2);
			#if MLE_CATCH_NAN
			if (isnan(z2) ||  isinf(z2)) {
				thError("%s: ERROR - (NAN) detected in <m_%d_%d | m_%d>", name, i, j, k);
				err_nan++;
			}
			if (isnan(amp[k]) || isinf(amp[k])) {
				thError("%s: ERROR - (NAN) detected in Amp[%d]", name, k);
				err_nan++;
			}
			if (isnan(z_dble) || isinf(z_dble)) {
				thError("%s: ERROR - (NAN) detected in (Amp_%d)< m_%d_%d|m_%d> sum", name, k, i, j, k);
				err_nan++;
			}
			#endif
			#if VERY_DEEP_DEBUG_MLE
			printf("%s: (i = %2d, j = %2d, k = %2d), < m_i_j | m_k > = %g \n", name, i, j, k, (float) z2);
			#endif

		}
		z = (MLEFL) z_dble;
		#if VERY_DEEP_DEBUG_MLE
		printf("%s: (i = %2d, j = %2d), <m_i_j | d - m > = %g \n", name, i, j, (float) z); 
		#endif
		status = thLMijDmmPut(lstruct, i, j, z);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not place element in MijDmm for (%d, %d)", name, i, j);
			return(status);
		}
		#if MLE_CATCH_NAN
		if (isnan(z) || isinf(z)) {
			thError("%s: ERROR - (NAN) detected in < m_%d_%d | d - m >", name, i, j);
			err_nan++;
		}
		#endif
	}
}



#if MLE_DO_JTJ_APSECTOR
MLEFL **jtjap, *jtapdmm;
status = thExtractJtj(lstruct, my_wsector, &jtjap);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract JtJ placeholder", name);
	return(status);
	}
status = thExtractJtdmm(lstruct, my_wsector, &jtapdmm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract Jt (d - m) placeholder", name);
	return(status);
}

MLEFL **mimj = NULL;
status = thExtractBrackets(lstruct, NULL, NULL, &mimj, NULL, NULL, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not extract necessary matrices", name);
        return(status);
}

for (i = 0; i < nmodel; i++) {
	for (j = 0; j < nmodel; j++) {
		jtjap[i][j] = mimj[i][j];
	}
} 		

if (doDeriv) {
	for (i = 0; i < nmodel; i++) {
		int k;
		for (k = 0; k < nparam; k++) {
			MLEDBLE x = (MLEDBLE) 0.0;
			for (j = 0; j < nmodel; j++) {
				MLEFL z = (MLEFL) 0.0;
				status = thLMijMkGet(lstruct, j, k, i, &z);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not get <m_%d | W | D m_%d / Dx_%d>", name, i, j, k);
					return(status);
				}
				x += (MLEDBLE) z * (MLEDBLE) amp[j];
			}
			jtjap[i][k + nmodel] = (MLEFL) x;
			jtjap[k + nmodel][i] = (MLEFL) x;
		}
	} 
} else {
	thError("%s: WARNING - avoiding calculation of (JtJ) for mixed (APSECTOR)", name);
}

MLEFL *MDmm = NULL;
status = thLworkGetMDmm(lstruct->lwork, &MDmm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get < M_i | D - M > vector", name);
	return(status);
}
for (i = 0; i < nmodel; i++) {
	MLEFL x_temp = 0.0;
	status = thLMijDmmGet(lstruct, i, -1, &x_temp);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (MiDmm) for (i = %d)", name, i);
		return(status);
	}
	jtapdmm[i] = x_temp;
	if (isinf(x_temp) || isnan(x_temp)) {
		thError("%s: ERROR - (NAN) found in jtdmm[%d] 'ASECTOR' = < M[%d] | D - M >", name, i, i);
		err_nan++;
	}
}	

#if DEEP_DEBUG_MLE
dbgJtdmm(jtapdmm, nmodel, name, "-right after calculation - ASECTOR");
#endif


#endif

/* extracting place holders for j-matrices */
MLEFL **jtj, *jtdmm;
status = thExtractJtj(lstruct, PSECTOR, &jtj);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract JtJ placeholder", name);
	return(status);
	}
status = thExtractJtdmm(lstruct, PSECTOR, &jtdmm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract Jt (d - m) placeholder", name);
	return(status);
}
/* now calculate the J matrices */

int k, l; 
do {

if (!doDeriv) {
	thError("%s: WARNING - avoiding calculation of (Jtj) for 'PSECTOR'", name);
	break;
}

MLEDBLE x;
for (i = 0; i < nparam; i++) {
	for (j = 0; j <= i; j++) {
		x = (MLEDBLE) 0.0;
		for (k = 0; k < nmodel; k++) {
		MLEDBLE amp_k = (MLEDBLE) amp[k];
		MLEDBLE y = (MLEDBLE) 0.0;
		for (l = 0; l < nmodel; l++) {
			MLEFL z = 0.0;
			status = thLMijMklGet(lstruct, k, i, l, j, &z);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get value for (mijmkl) for (%d, %d) (%d, %d)", 
				name, k, i, l, j);
				return(status);
			}
			y += ((MLEDBLE) amp[l]) * (MLEDBLE) z;
			#if MLE_CATCH_NAN
			if (isinf(z) || isnan(z)) {
				thError("%s: ERROR - (NAN) detected in <m_%d_%d | m_%d_%d>", name, k, i, l, j);
				err_nan++;
			}
			if (isnan(amp[l]) || isinf(amp[l])) {
				thError("%s: ERROR - (NAN) detected in Amp[%d]", name, l);
				err_nan++;
			}
			if (isnan(y) || isinf(y)) {
				thError("%s: ERROR - (NAN) detected in Amp[%d] <m_%d_%d | m_%d_%d> sum", name, l, k, i, l, j);
				err_nan++;
			}
			#endif


		}
		x += amp_k * y;
		#if MLE_CATCH_NAN
		if (isnan(x) || isinf(x)) {
			thError("%s: ERROR - (NAN) detected in the Amp[%d] [Y] sum", name, k);
			err_nan++;
		}
		#endif 
		}
		#if MLE_APPLY_CONSTRAINTS
		CHISQFL d2Cdalpha2 = 0.0;
		status = thLPiPjCGet(lstruct, i, j, &d2Cdalpha2);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get the constraint function derivatives for (i = %d, j = %d)", name, i, j);
			return(status);
		}
		x += (CHISQFL) 0.5 * d2Cdalpha2;
		#endif

		jtj[i][j] = (CHISQFL) x;
		if (i != j) jtj[j][i] = x;
		#if MLE_CATCH_NAN
		if (isnan(jtj[i][j]) || isinf(jtj[i][j])) {
			thError("%s: ERROR - (NAN) detected in JtJ[%d][%d]", name, i, j);
			err_nan++;
		}
		#endif
	}		
}


#if VERY_DEEP_DEBUG_MLE
dbgJtj(jtjap, nparam + nmodel, name, " - right after calculation");
#endif

for (i = 0; i < nparam; i++) {
	x = (MLEDBLE) 0.0;
	for (j = 0; j < nmodel; j++) {
		MLEFL z = 0.0;
		MLEDBLE amp_j = (MLEDBLE) amp[j];
		status = thLMijDmmGet(lstruct, j, i, &z);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get element of (mijdmm) for (%d, %d)", name, j, i);
			return(status);
		}
		x += (amp_j) * (MLEDBLE) z;
		#if MLE_CATCH_NAN
		if (isnan(z) || isinf(z)) {
			thError("%s: ERROR - (NAN) detected in <m_%d_%d|d-m>", name, j, i);
			err_nan++;
		}
		if (isnan(amp_j) ||  isinf(amp_j)) {
			thError("%s: ERROR - (NAN) detected in Amp[%d]", name,  j);
			err_nan++;
		}
		if (isnan(x) || isinf(x)) {
			thError("%s: ERROR - (NAN) detected in <Amp_%d|m_%d_%d> sum", name, j, j, i);
			err_nan++;
		}
		#endif

	/* 	
		MLEDBLE y = 0.0;
		for (k = 0; k < nmodel; k++) {
			status = thLMijMkGet(lstruct, j, i, k, &z);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get value for <m_%d_%d | m_%d>", name, j, i, k);
				return(status);
			}
			y +=  (MLEDBLE) amp[k] * (MLEDBLE) z;
		}
		x += y * amp_j;
	*/
	}
	#if MLE_APPLY_CONSTRAINTS
	CHISQFL dCdalpha = 0.0;
	status = thLPiCGet(lstruct, i, &dCdalpha);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get cost function derivative for constraints (i = %d)", name, i);
		return(status);
	}
	x -= dCdalpha;
	#endif

	jtdmm[i] = (MLEFL) x;
	#if MLE_CATCH_NAN
	if (isnan(x) || isinf(x)) {
		thError("%s: ERROR - (NAN) detected for precision placeholder for Jt[D-M][%d]", name, i);
		err_nan++;
	}
	if (isnan(jtdmm[i]) || isinf(jtdmm[i])) {
		thError("%s: ERROR - (NAN) detected in Jt[D-M][%d]", name, i);
		err_nan++;
	}
	#endif
}

#if MLE_CATCH_NAN
if (err_nan != 0) {
	thError("%s: ERROR - (NAN) detected in (%d) occasions", name, err_nan);
	return(SH_GENERIC_ERROR);
}
#endif

#if DEEP_DEBUG_MLE
dbgJtdmm(jtdmm, nparam, name, "-right after calculation - PSECTOR");
#endif

} while (0);

#if MLE_CONDUCT_HIGHER_ORDER
do {

if (!doDeriv) {
	thError("%s: WARNING - avoiding calculation of higher order derivative", name);
	break;
}

MLEFL *JtDDm = NULL;
status = thLExtractJtDDm(lstruct, PSECTOR, &JtDDm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extact (JtDDm) matrix from (lstruct)", name);
	return(status);
} 
if (JtDDm == NULL && nparam != 0) {
	thError("%s: ERROR - received a null (JtDDm) matrix while (nparam = %d)", name, nparam);
	return(SH_GENERIC_ERROR);
}

MLEFL  *p = NULL, *pN = NULL, *dp = NULL;
int np = -1;
status = ExtractXxNdX(lstruct, PSECTOR, &p, &pN, &dp, &np);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (p, pN, dp) vectors", name);
	return(status);
} else if (dp == NULL && np != 0) {
	thError("%s: ERROR - (dp = NULL, np = %d) was found in (lstruct)", name, np);
	return(SH_GENERIC_ERROR);
} else if (np != nparam) {
	thError("%s: ERROR - expectec (nparam = %d) found (np = %d)", name, nparam, np);
	return(SH_GENERIC_ERROR);
}

MLEFL  *a = NULL, *aN = NULL;
int nm = -1;
status = ExtractXxNdX(lstruct, ASECTOR, &a, &aN, NULL, &nm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (a, aN) vectors", name);
	return(status);
} else if (nm != nmodel) {
	thError("%s: ERROR - expectec (nmodel = %d) found (nm = %d)", name, nmodel, nm);
	return(SH_GENERIC_ERROR);
}

int i1, i2, j1, j2, k2;
for (j1 = 0; j1 < nparam; j1++) {

MLEFL z = 0.0;
for (i1 = 0; i1 < nmodel; i1++) {
for (i2 = 0; i2 < nmodel; i2++) {
for (j2 = 0; j2 < nparam; j2++) {
for (k2 = 0; k2 < nparam; k2++) {

MLEFL y = 0.0, a1 = 0.0, a2 = 0.0, dp1 = 0.0, dp2 = 0.0;
a1 = (MLEFL) a[i1];
a2 = (MLEFL) a[i2];
dp1 = (MLEFL) dp[j2];
dp2 = (MLEFL) dp[k2];
status = thLMijMklmGet(lstruct, i1, j1, i2, j2, k2, &y);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get < m_%d_%d | m_%d_%d_%d >", name, i1, j1, i2, j2, k2);
	return(status);
	}
#if MLE_CATCH_NAN
if (isnan(a1) || isinf(a1)) {
	thError("%s: ERROR - (NAN) detected in amp[%d]", name, i1);
	err_nan++;
}
if (isnan(a2) || isinf(a2)) {
	thError("%s: ERROR - (NAN) detected in amp[%d]", name, i2);
	err_nan++;
}
if (isnan(dp1) || isinf(dp1)) {
	thError("%s: ERROR - (NAN) detected in dp[%d]", name, j1);
	err_nan++;
}
if (isnan(dp2) || isinf(dp2)) {
	thError("%s: ERROR - (NAN) detected in dp[%d]", name, j2);
	err_nan++;
}
if (isnan(y) || isinf(y)) {
	thError("%s: ERROR - (NAN) found in < M[%d, %d] | M [%d, %d %d] >", name, i1, j1, i2, j2, k2);
	err_nan++;
}
#endif
z += y * a2 * a1 * dp2 * dp1;

}}}

#if MLE_CATCH_NAN
if (isnan(z) || isinf(z)) {
	thError("%s: ERROR - (NAN) detected in <m_%d_%d|d-m>", name, j, i);
	err_nan++;
}
#endif
	
JtDDm[j1] = z;

}
}
} while (0);
#endif

LWORK *lwork = NULL;
status = thLstructGetLwork(lstruct, &lwork);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwork) from (lstruct)", name);
	return(status);
}
#if DEEP_DEBUG_MLE
dbgJtdmm(jtapdmm, nparam + nmodel, name, "- right before 'PSECTOR' operations");
#endif

/* now calculate Da / Dp when only minimizing on PSECTOR */
do {
if (wsector == PSECTOR) {

	if (!doDeriv) {
		thError("%s: WARNING - avoiding derivative adjustment while (sector = '%s', lmtype = '%s')", 
			name, shEnumNameGetFromValue("LSECTOR", wsector), shEnumNameGetFromValue("LMETHODTYPE", lmtype));
		break;
	}
	#if DEBUG_MLE
	printf("%s: making adjustments for 'PSECTOR' \n", name);
	#endif
	MLEFL **DaDp = NULL, **DaDpT = NULL, **wN = NULL, **wA = NULL, **wwN = NULL, *wC = NULL;
	MAT *wQ1 = NULL, *wQ2 = NULL;
	VEC *wX = NULL;

	status = thLworkGetDaDp(lwork, &DaDp, &DaDpT, &wN, &wA, &wwN, &wC, &wQ1, &wQ2, &wX, &nmodel, &nparam);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get 'DaDp' and working space", name);
		return(status);
	}

	if (nmodel <= 0) {
		thError("%s: ERROR - found (nmodel = %d)", name, nmodel);
		return(SH_GENERIC_ERROR);
	}
	if (nparam < 0) {
		thError("%s: ERROR - found (nparam = %d)", name, nparam);
		return(SH_GENERIC_ERROR);
	} else if (nparam == 0) {
		thError("%s: WARNING - found (nparam = %d)", name, nparam);
	}

	if (nparam > 0) {
		int i_p = 0, nap = -1, nm = -1, np = -1;
		MLEFL **jtja = NULL, **jtjp = NULL, **jtjap = NULL, **wjtjap = NULL;
		#if (EXACT_DADP | 1)
		for (i = 0; i < nmodel; i++) {
			for (j = 0; j < nmodel; j++) {
				MLEFL x = 0.0;
				status = thLMijMkGet(lstruct, i, -1, j, &x);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not get < m[%d] | m[%d] >", name, i, j);
					return(status);
				}
				wN[i][j] = x;
				wN[i][j] = mimj[i][j];
			}
		}
		#if DEEP_DEBUG_MLE
		dbgMimj(wN, nmodel, name, "wN = ");
		#endif
		for (i_p = 0; i_p < nparam; i_p++) {
			MLEFL *Da = DaDpT[i_p];
			for (j = 0; j < nmodel; j++) {
				for (k = 0; k < nmodel; k++) {
					MLEFL x = 0.0, y = 0.0;
					status = thLMijMkGet(lstruct, j, i_p, k, &x);
					if (status != SH_SUCCESS) {
						thError("%s: ERROR - could not get < m[%d] | m[%d, %d] >", name, k, j, i_p);
						return(status);
					}
					status = thLMijMkGet(lstruct, k, i_p, j, &y);
					if (status != SH_SUCCESS) {
						thError("%s: ERROR - could not get < m[%d] | m[%d, %d] >", name, j, k, i_p);
						return(status);
					}
					wA[j][k] = -(x + y);
					#if VERY_DEEP_DEBUG_MLE
					if (((fabs(x) >= (MLEFL) 0.0E-5) | (fabs(y) > (MLEFL) 0.0E-5)) && k <= 5 && j <= 5) {
						printf("%s: wA[%d, %d] = %g, i_p = %d, (x, y) = (%G, %G), < m[%d] | m[%d] > = %G \n", 
							name, j, k, (float) wA[j][k], i_p, (double) x, (double) y, j, k, (double) mimj[j][k]);
					}
					#endif
				}
			}
			#if DEEP_DEBUG_MLE
			char wA_comment[1000];
			sprintf(wA_comment, "wA[i_p = %d] (before multiplication with wN =  < m[i] | m[j] > ^ -1", i_p);
			dbg2DArray(wA, nmodel, nmodel, name, wA_comment);
			#endif
			status = thLworkGetWJtj(lwork, APSECTOR, &wjtjap, &nap);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get (WJtj) for (sector = 'APSECTOR')", name);
				return(status);
			}
			if (nap != (nmodel + nparam)) {
				thError("%s: ERROR - mismatching (nap, nparam + nmodel) = (%d, %d)", name, nap, (nparam + nmodel));
				return(status);
			}
			status = thMathMultiplyInverseSquareMat(wN, wA, wwN, wjtjap, wQ1, wQ2, wX, nmodel);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not conduct matrix inversion and multiplication", name);
				return(status);
			}
			#if DEEP_DEBUG_MLE
			dbg2DArray(wN, nmodel, nmodel, name, "wN = < m[i] | m[j] >  ^ -1");	
			sprintf(wA_comment, "wA[i_p = %d] (after multiplication with wN = < m[i] | m[j] > ^ -1", i_p);
			dbg2DArray(wA, nmodel, nmodel, name, wA_comment);
			#endif
			for (i = 0; i < nmodel; i++) {
				wA[i][i] += (MLEFL) 1.0;
				status = thLMijDGet(lstruct, i, -1, &(Da[i]));
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not get < m[%d] | D >", name, i);
					return(status);
				}
			}
			#if DEEP_DEBUG_MLE
			dbgDmi(Da, nmodel, name, "< M | D > = ");
			#endif
			status = thMathMultiplyInverseSquareMatColumn(wN, Da, wwN, wC, wQ1, wQ2, wX, nmodel);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not conduct matrix inversion and multiplication", name);
				return(status);
			}
			status = thMathMultiplySquareMatColumn(wA, Da, wC, nmodel);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not condcut matrix multiplication", name);
				return(status);		
			}
		}
		#else
		status = thLworkGetJtj(lwork, ASECTOR, &jtja, &nm);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (Jtj) for (sector = 'ASECTOR')", name);
			return(status);
		}
		if (nm != nmodel) {
			thError("%s: ERROR - mistmatching (nm, nmodel) = (%d, %d)", name, nm, nmodel);
			return(SH_GENERIC_ERROR);
		} 
		status = thLworkGetJtj(lwork, APSECTOR, &jtjap, &nap);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (Jtj) for (sector = 'APSECTOR')", name);
			return(status);
		}
		if (nap != (nmodel + nparam)) {
			thError("%s: ERROR - mismatching (nap, nparam + nmodel) = (%d, %d)", name, nap, (nparam + nmodel));
			return(status);
		}
		status = thLworkGetWJtj(lwork, APSECTOR, &wjtjap, &nap);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (WJtj) for (sector = 'APSECTOR')", name);
			return(status);
		}
		if (nap != (nmodel + nparam)) {
			thError("%s: ERROR - mismatching (nap, nparam + nmodel) = (%d, %d)", name, nap, (nparam + nmodel));
			return(status);
		}
		status = thMathMultiplyInverseSquareMatGeneral(jtja, jtjap, wN, wjtjap, wQ1, wQ2, wX, nm, nap);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not conduct matrix multiplication of (Jtj[PSECTOR]) with inv(Jtj[ASECTOR])", name);
			return(status);
		}
		for (i = 0; i < nmodel; i++) {
			MLEFL *DaDp_i = DaDp[i];
			MLEFL *wjtjap_i = wjtjap[i];
			int alpha = 0;
			for (alpha = 0; alpha < nparam; alpha++) {
				MLEFL x = -(wjtjap_i[alpha + nmodel]);
				DaDp_i[alpha] = x;
				DaDpT[alpha][i] = x;
				if (isinf(x) || isnan(x)) {
					thError("%s: ERROR - (NAN) detected in DaDp[%d, %d]", name, i, alpha);
					err_nan++;
				}
			}
		}
		#endif

		#if DEEP_DEBUG_MLE
		dbg2DArray(jtjap, nmodel, nparam + nmodel, name, "Jtj[APSECTOR] = ");
		dbg2DArray(wjtjap, nmodel, nparam + nmodel, name, "WJtj[APSECTOR] = ");
		dbg2DArray(DaDp, nmodel, nparam, name, "DaDp = ");
		#endif		

		MLEFL *jtdmma = NULL, *jtdmmp = NULL, *jtdmmap = NULL;
		status = thLworkGetJtdmm(lwork, APSECTOR, &jtdmmap, &nap);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get Jt | D - m > for APSECTOR", name);
			return(status);
		}
		status = thLworkGetJtdmm(lwork, ASECTOR, &jtdmma, &nmodel);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get Jt | D - m > for amplitude sector", name);
			return(status);
		}
		status = thLworkGetJtdmm(lwork, PSECTOR, &jtdmmp, &nparam);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get Jt | D - m > for parameter sector", name);
			return(status);
		}
		if (nap != (nmodel + nparam)) {
			thError("%s: ERROR - mismatching (na, np, nap) = (%d, %d, %d)", name, nmodel, nparam, nap);
			return(SH_GENERIC_ERROR);
		}
		#if (DEEP_DEBUG_MLE)
		dbgJtdmm(jtdmmp, nparam, name, "- right before 'PSECTOR' update");
		#endif
		for (i_p = 0; i_p < nparam; i_p++) {
			MLEFL DX2Dp = 0.0;
			MLEFL *DX2Da = jtdmma;
			MLEFL *Da = DaDpT[i_p];
			for (i = 0; i < nmodel; i++) {
				DX2Dp += DX2Da[i] * Da[i];
				/* 
				printf("%s: DX2Da[i = %d] = %g, DaDp[i = %d, i_p = %d] = %g \n", name, i, (float) DX2Da[i], i, i_p, (float) Da[i]);
				*/
			}
			/* 
			printf("%s: Updated amount DX2Dp[i_p = %d] = %g \n", name, i_p, (float) DX2Dp);
			*/
			jtdmmp[i_p] += DX2Dp;
		}
		#if (DEEP_DEBUG_MLE)
		dbgJtdmm(jtdmmp, nparam, name, "- right after 'PSECTOR' update");
		#endif
		/* now updating JtJ */
		status = thLworkGetJtj(lwork, APSECTOR, &jtjap, &nap);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get JtJ_[ap] from (lwork)", name);
			return(status);
		}
		status = thLworkGetJtj(lwork, PSECTOR, &jtjp, &np);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get JtJ_[p] from (lwork)", name);
			return(status);
		}
		if (np != nparam) {
			thError("%s: ERROR - inconsistent (np, nparam) = (%d, %d)", name, np, nparam);
			return(SH_GENERIC_ERROR);
		}
		if (nap != (nparam + nmodel)) {
			thError("%s: ERROR - inconsistent (nap, nparam + nmodel) = (%d, %d)", name, nap, (nparam + nmodel));
			return(SH_GENERIC_ERROR);
		}
		#if (UPDATE_JTJ_FOR_PSECTOR | 1)
		#if DEEP_DEBUG_MLE
		printf("%s: updating Jtj matrix for 'PSECTOR' \n", name);
		dbgJtj(jtj, np, name, " - right before calculation");
		#endif
		int alpha, beta;
		for (alpha = 0; alpha < nparam; alpha++) {
			MLEFL *JtJ_alpha = jtjp[alpha];
			MLEFL *DaDpT_alpha = DaDpT[alpha];
			for (beta = 0; beta < nparam; beta++) {
				MLEFL *DaDpT_beta = DaDpT[beta];
				MLEFL x = 0.0;
				for (i = 0; i < nmodel; i++) {
					MLEFL *Jtj_i = jtjap[i];
					for (j = 0; j < nmodel; j++) {
						x += DaDpT_alpha[i] * DaDpT_beta[j] * Jtj_i[j];
					}
					x += Jtj_i[alpha + nmodel] * DaDpT_alpha[i];
					x += Jtj_i[beta + nmodel] * DaDpT_beta[i];
				}
				JtJ_alpha[beta] += x;
			}
		}		
		#if DEEP_DEBUG_MLE	
		dbgJtj(jtj, np, name, " - right after calculation");
		#endif
		#endif

	}
}
} while (0);

#if (ADADELTA)

/* this introduces some changes in Jt | D - M > matrix. This is a memory bug and it is perhaps due to a particular JtDmm being read into an array of different name by mistake */
status = thLstructGetLwmask(lstruct, &lwmask);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwmask) from (lstruct)", name);
	return(status);
}

if (1 == 0) {
	status = thLworkUpdateAdaDeltaGD(lwork, lwmask, lm, ASECTOR); 
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update AdaDelta for (sector = 'ASECTOR')", name);
		return(status);
	}
	status = thLworkUpdateAdaDeltaGD(lwork, lwmask, lm, PSECTOR); 
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update AdaDelta for (sector = 'PSECTOR')", name);
		return(status);
	}
	status = thLworkUpdateAdaDeltaGD(lwork, lwmask, lm, APSECTOR); 
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update AdaDelta for (sector = 'APSECTOR')", name);
		return(status);
	}
} else {
	status = thLworkUpdateAdaDeltaGD(lwork, lwmask, lm, wsector); 
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update AdaDelta for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", wsector));
		return(status);
	}
}
#endif

#if THCLOCK
clk2 = clock();
printf("%s: CLOCKING - [%g] CPUs \n", name, ((float) (clk2 - clk1)) / CLOCKS_PER_SEC);
#endif

#if DEBUG_MEMORY_VERBOSE
printf("%s: memory statistics after making J-matrices", name);
shMemStatsPrint();
fflush(stdout);
fflush(stderr);
#endif

/* J matrices are calculated */
return(SH_SUCCESS);
}


RET_CODE thLworkUpdateAdaDeltaGD(LWORK *lwork, LWMASK *lwmask, LMETHOD *lm, LSECTOR sector) {
char *name = "thLwmaskUpdateAdaDeltaGD";

RET_CODE status;
int mini_batch_size = -1;
status = thLwmaskGetMiniBatchSize(lwmask, &mini_batch_size);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get active (mini_batch_size) from (lwmask)", name);
	return(status);
}
if (mini_batch_size <= 0) {
	thError("%s: ERROR - found improper active (mini_batch_size = %d) in (lwmask)", name, mini_batch_size);
	return(SH_GENERIC_ERROR);
}

int nx = -1;
MLEFL *E_gg = NULL;
MLEFL *E_gg_prev = NULL;
status = thLworkGetEGgAdaDelta(lwork, sector, &E_gg, &E_gg_prev, &nx);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (E_gg_adadelta) from (lwork)", name);
	return(status);
}
MLEFL *rms_g = NULL;
MLEFL *rms_g_prev = NULL;
status = thLworkGetRmsGAdaDelta(lwork, sector, &rms_g, &rms_g_prev, &nx);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (RMS_gg_adadelta) from (lwork)", name);
	return(status);
}
MLEFL *jtdmmx = NULL;
status = thLworkGetJtdmm(lwork, sector, &jtdmmx, &nx);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (jtdmm) from (lwork)", name);
	return(status);
}
MLEFL rho_adadelta = MLEFLNAN;
MLEFL epsilon_adadelta_dx = MLEFLNAN, epsilon_adadelta_g = MLEFLNAN;
status = thLmethodGetRhoAdaDelta(lm, &rho_adadelta, &epsilon_adadelta_dx, &epsilon_adadelta_g);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (rho_adadelta) from (lwork)", name);
	return(status);
}

int i;
for (i = 0; i < nx; i++) {
	E_gg[i] = (rho_adadelta) * E_gg_prev[i] + (1.0 - rho_adadelta) * jtdmmx[i] * jtdmmx[i] / ((MLEFL) mini_batch_size * (MLEFL) mini_batch_size) / pow((MLEFL) LM_MINIBATCH_CORRECTION_ADADELTA, 2.0);
	rms_g[i] = pow(E_gg[i] + epsilon_adadelta_g, 0.5);
	E_gg_prev[i] = E_gg[i];	
}

MLEFL *E_dxdx = NULL, *E_dxdx_prev = NULL;
MLEFL *dx = NULL;
status = thLworkGetEDxdx(lwork, sector, &E_dxdx, &E_dxdx_prev, &nx);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get E_dxdx from (lwork)", name);
	return(status);
}
status = thLworkGetDx(lwork, sector, &dx, &nx);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (dx) from (lwork)", name);
	return(status);
}
MLEFL *rms_dx = NULL;
status = thLworkGetRmsDxAdaDelta(lwork, sector, &rms_dx);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (rms_dx) from (lwork)", name);
	return(status);
}
for (i = 0; i < nx; i++) {
	E_dxdx[i] = (rho_adadelta) * E_dxdx_prev[i] + (1.0 - rho_adadelta) * dx[i] * dx[i]; 	
	rms_dx[i] = pow(E_dxdx[i] + epsilon_adadelta_dx, 0.5);
	E_dxdx_prev[i] = E_dxdx[i];
}

int err_nan = 0;
for (i = 0; i < nx; i++) {
	if (isnan(dx[i]) || isinf(dx[i])) {
		thError("%s: ERROR - found (NAN) in dx[%d]", name, i);
		err_nan++;
	}
	if (isnan(E_dxdx[i]) || isinf(E_dxdx[i])) {
		thError("%s: ERROR - found (NAN) in E_dx[%d]", name, i);
		err_nan++;
	}
	if (isnan(E_dxdx_prev[i]) || isinf(E_dxdx_prev[i])) {
		thError("%s: ERROR - found (NAN) in E_dx_prev[%d]", name, i);
		err_nan++;
	}
	if (isnan(rms_dx[i]) || isinf(rms_dx[i])) {
		thError("%s: ERROR - found (NAN) in rms_dx[%d]", name, i);
		err_nan++;
	}
	if (isnan(jtdmmx[i]) || isinf(jtdmmx[i])) {
		thError("%s: ERROR - found (NAN) in Jt | D - M > [%d]", name, i);
		err_nan++;
	}
	if (isnan(E_gg[i]) || isinf(E_gg[i])) {
		thError("%s: ERROR - found (NAN) in E_g[%d]", name, i);
		err_nan++;
	}
	if (isnan(E_gg_prev[i]) || isinf(E_gg_prev[i])) {
		thError("%s: ERROR - found (NAN) in E_g_prev[%d]", name, i);
		err_nan++;
	}
	if (isnan(rms_g[i]) || isinf(rms_g[i])) {
		thError("%s: ERROR - found (NAN) in rms_g[%d]", name, i);
		err_nan++;
	}
}
if (err_nan > 0) {
	thError("%s: ERROR - found (%d) NAN's when updating for (sector = '%s', nx = %d)", name, err_nan, shEnumNameGetFromValue("LSECTOR", sector), nx);
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}



RET_CODE DoLMFitSimpleRun(LSTRUCT *lstruct) {

char *name = "DoLMFitSimpleRun";
#if MLE_CONDUCT_HIGHER_ORDER
thError("%s: ERROR - this function does not support higher order LM minimization", name);
return(SH_GENERIC_ERROR);
#endif

LSECTOR sector = PSECTOR;
RET_CODE status = SH_SUCCESS;

/* LM method is executed as follows 

   L = < m - d | W | m - d > 
   J = DL / Dp

   The new step is taken by Dp such that 

   (JT J + \lambda diag(JT J)) Dp = JT (d - m)

   Where the Jacobian J and the related square matrices are defined as follows 

   J = D | m > / Dp

   J_i = D | m > / Dp_i
   (JT J)_i_j       =  D < m | / Dp_i   W     D | m > / Dp_j
   (JT (d - m))_i =  D < m | / Dp_i   W   | d - m >

*/
#if THCLOCK
clock_t clk1, clk2;
clk1 = clock();
#endif

MLEFL lambda, beta, gamma, rhobar, nu;
int q;

status = ExtractLMDamping(lstruct, &lambda, &beta, &gamma, &rhobar, &nu, &q, NULL, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract damping parameters", name);
	return(status);
}   

MLEFL **jtj, *jtd;
MLEFL **jtjpdiag, **covT;
CHISQFL chisq = 0.0, cost = 0.0;
MLEFL *dp, *dxT;
int npar;
status = ExtractLMCurrentMatrices(lstruct, sector, &jtj, &jtjpdiag, &covT, &jtd, &dp, &dxT, &chisq, &cost, &npar);

if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract the Jacobian for Levenberg-Marquardt", name);
	return(status);
}   

int i, j;
for (i = 0; i < npar; i++) {
	for (j = 0; j < npar; j++) {
		covT[i][j] = jtjpdiag[i][j];
	}
}
memcpy(dxT, dp, npar * sizeof(MLEFL));

MLEFL onepluslambda;
onepluslambda = (MLEFL) 1.0 + (MLEFL) lambda;

for (i = 0; i < npar; i++) {
	for (j = 0; j < npar; j++) {
		jtjpdiag[i][j] = jtj[i][j];
	}
	}
for (i = 0; i < npar; i++) {
	jtjpdiag[i][i] *= onepluslambda;
	dp[i] = (MLEFL) jtd[i];
}

int singular = 0;
#if 0
status = thMatrixInvert(jtjpdiag, dp, npar); 
#else
status = LMatrixInvert(jtjpdiag, jtd, dp, npar, &singular);
#endif
if (singular) {
	thError("%s: WARNING - possibly degeneracy discovered while taking derivative-based steps", name);
	status = UpdateLMConvergence(lstruct, SINGULAR_CONVERGENCE);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not place convergence flag into (lstruct)", name);
		return(status);
	}
	for (i = 0; i < npar; i++) {
		for (j = 0; j < npar; j++) {
			jtjpdiag[i][j] = covT[i][j];
		}
	}
	memset(dp, '\0', npar * sizeof(MLEFL));
} else if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not invert the jtj(1 + lambda) matrix in P-sector", name);
	return(status);
}


/*
Choice of damping parameter

The choice of the damping parameter is based on the following algorithm: 

IMM DEPARTMENT OF MATHEMATICAL MODELLING	J. No. MARQ
8.4.1999 Technical University of Denmark	HBN/ms
DK-2800 Lyngby – Denmark
DAMPING PARAMETER IN MARQUARDT’S METHOD
Hans Bruun Nielsen
TECHNICAL REPORT IMM-REP-1999-05

*/

/* the following updates the par values but doesn't set any flags */
 
/* 
note that the dp used is indeed already in the work space so there
is absolutely no need to supply it explicitly, this incrementing is done
in new space  
*/
status = IncrementP(lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update the parameter values by the designated amount", name);
	return(status);
}

/* now trying to see if the new step is acceptable */

CHISQFL Nchisq, Ncost;
MLEFL dl, rho = -1.0;

/* at this point 
	MakeImageAndMatrices should only create model images (not derivatives) 
	for models which have a changing paramater (non-linear) list.
	The derivatives are calculated only after we make sure that the new step
	is an acceptable one  */
status = MakeImageAndMatrices(lstruct, IMAGES); /* IMAGES */
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make matrices and images for the new parameter", name);
	return(status);
}
status = thMakeM(lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not create (m) and (d-m)", name);
	return(status);
}
status = thLCalcChisqNSimpleRun(lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calculate (chisqN) for (pN)", name);
	return(status);
}
status = thLstructGetChisqN(lstruct, &Nchisq, &Ncost);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract derivative information for the new par value", name);
	return(status);
}   

if (cost >= Ncost) {

	/* now that we know this is an acceptable step, the derivatives should be
	calculated */
	status = MakeImageAndMatrices(lstruct, DM);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not make matrices and images for the new parameter", name);
		return(status);
	}

	dl = (MLEFL) 0.0;
	for (i = 0; i < npar; i++) {
		dl += ((MLEFL) 0.5) * (lambda * dp[i] * jtj[i][i] - jtd[i]) * dp[i];
	}

	/* updated to cost rather than chisq value to enable parameter restrictions */
	rho = (cost - Ncost) / dl;

	/* 
	we should decide whether during this update the derivative matrices are updated as well 
	and how the flags are set
	*/
	MLEFL x1, x2;
	if (rho >= 0) {
		status = UpdateXFromXN(lstruct, sector);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not update the parameter values", name);
			return(status);
		}
		/* this part seems to have been mistakenly omitted Feb 14, 2012 */  
		status = UpdateChisq(lstruct);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not update the (chisq) value", name);
			return(status);
		}
		/* */
		/* x1 and x2 are auxiliary paramters to make the code readable */
		x1 = ((MLEFL) 1.0) / gamma;
		x2 = (MLEFL) 1.0 - (beta - (MLEFL) 1.0) * pow((rho / rhobar - (MLEFL) 1.0), q);
		status = LMultiplyVariable(&lambda, MAX(x1, x2));
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not multiply (lambda= %g) with (MAX(x1,x2)= %g)", name, (float) lambda, (float) MAX(x1, x2));
			return(status);
		}
		nu = beta;
	} else {
		thError("%s: ERROR - algorithm problem, while X2 has decreased, (rho) has become negative", name);
		return(SH_GENERIC_ERROR);
	}

} else {
	status = thLstructSetXupdate(lstruct, OUTDATED, PSECTOR);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not set (pupdate) flag in (lstruct) to 'outdated'", name);
		return(status);
	}
	status = LMultiplyVariable(&lambda, nu);	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not multiply (lambda= %g) with (nu= %g)", name, lambda, nu);
		return(status);
	}
	status = LMultiplyVariable(&nu, (MLEFL) LM_NU_BOOST_FACTOR);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not multiply (nu= %g) with (2.0)", nu);
		return(status);
	}
}

#if DEBUG_MLE
if (rho >= 0) {
	printf("%s: rho= %G, lambda= %G, nu= %g, dl= %G, dCost = %G, Cost = %G, CostN = %G, dX2= %G, X2= %10.7E, X2_N= %10.7E \n",
		name, (double) rho, (double) lambda, (double) nu, (double) dl, 
	(double) cost, (double) cost - (double) Ncost, (double) Ncost, 
	(double) chisq - (double) Nchisq, (double) chisq, (double) Nchisq);
} else {
	printf("%s: rho= %G, lambda= %G, nu= %G, dl= %G, dCost = %G, Cost = %G, CostN = %G, dX2= %G, X2= %10.7E, X2_N= %10.7E \n",
		name, (double) rho, (double) lambda, (double) nu, (double) dl, 
		(double) cost, (double) cost - (double) Ncost, (double) Ncost, 	
		(double) chisq - (double) Nchisq, (double) chisq, (double) Nchisq);
}
#endif

/* update the damping parameters */
status = UpdateLMDamping(lstruct, lambda, nu);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update the damping parametes", name);
	return(status);
}

/* do we need convergence test at this stage at all:

CONVERGENCE flag;

flag = CheckLMConvergence(lstruct, PSECTOR, &status);
if (status != SH_SUCCESS) thError("%s: ERROR - could not perform convergence test", name);

UpdateLMConvergence(lstruct, flag);
return(status);
*/
#if THCLOCK
clk2 = clock();
printf("%s: CLOCKING - [%g] CPUs \n", name, ((float) (clk2 - clk1)) / CLOCKS_PER_SEC);
#endif

return(SH_SUCCESS);
}

/* random search */
RET_CODE DoRSVFitAlgRun(LSTRUCT *lstruct, LSECTOR sector) {
char *name = "DoRSVFitAlgRun";
RET_CODE status = SH_SUCCESS;

printf("%s: *** STARTING *** \n", name);
/* Solve B h_qn = -F'(x_old) */

if (sector != APSECTOR) {
	thError("%s: WARNING - working with (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
}

LWORK *lwork = NULL;
status = thLstructGetLwork(lstruct, &lwork);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwork) from (lstruct)", name);
	return(status);
}

LWMASK *lwmask = NULL;
status = thLstructGetLwmask(lstruct, &lwmask);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwmask) from (lstruct)", name);
	return(status);
}

int k, kmax;
status = ExtractLMStepNo(lstruct, &k, &kmax); 
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract convergence criteria", name);
	return(status);
}

int i, n;
CHISQFL chisq = 0.0, cost = 0.0;
MLEFL *dx, *x_prev, *x_next;
MLEFL *jtdmm_prev = NULL, *jtdmm_next = NULL;

status = thLstructGetChisq(lstruct, &chisq, &cost);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (chisq) and (cost) from (lstruct)", name);
	return(status);
}
status = thLworkGetX(lwork, &x_prev, &n, sector);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (x_prev) for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(status);
}
status = thLworkGetXN(lwork, &x_next, &n, sector);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (x_next) for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(status);
}

status = thLworkGetRSV(lwork, &jtdmm_prev, &jtdmm_next, &dx, &n, sector);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (PGD) workspace from (lwork)", name);
	return(status);
}

#if DEEP_DEBUG_MLE
printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
dbgX(lstruct, name, ASECTOR, "A-sector, upon start");
dbgX(lstruct, name, PSECTOR, "P-sector, upon start");
#endif

if (dx == NULL) {
	thError("%s: ERROR - (lwork) not allocated properly - null (dx) array among (GD) arrays", name);
	return(SH_GENERIC_ERROR);
} else if (jtdmm_prev == NULL) {
	thError("%s: ERROR - (lwork) not allocated properly - null (jtdmm_prev) array among (GD) arrays", name);
	return(SH_GENERIC_ERROR);
} 

#if DEEP_DEBUG_MLE
dbgJtdmm(jtdmm_prev, n, name, "Jtdmm_prev upon start");
dbgJtdmm(jtdmm_next, n, name, "Jtdmm_next upon start");
#endif

#if MLE_CATCH_NAN
int err_nan = 0;
for (i = 0; i < n; i++) {
	if (isnan(jtdmm_prev[i]) || isinf(jtdmm_prev[i])) {
		thError("%s: ERROR - (NAN) detected in Jt[D-M][%d]", name, i);
		err_nan++;
	}
}

if (err_nan > 0) {
	thError("%s: ERROR - (NAN) detected in (%d) occasions", name, err_nan);
	return(SH_GENERIC_ERROR);
}

#endif

LMETHOD *lmethod = NULL;
status = thLstructGetLmethod(lstruct, &lmethod);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmethod) from (lstruct)", name);
	return(status);
}

LMETHODSUBTYPE wmethod_type = UNKNOWN_LMETHODSUBTYPE;
status = thLmethodGetType(lmethod, NULL, &wmethod_type);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (wmethod_type) from (lmethod)", name);
	return(status);
}
if (wmethod_type != RSVPURE) {
	thError("%s: ERROR - expected (wmethod_type = 'RSVPURE') but received '%s'", name, shEnumNameGetFromValue("WMETHODTYPE", wmethod_type));
	return(SH_GENERIC_ERROR);
}

if (n > 0) {
	
	status = randomize_mlefl_array(x_next, n, (MLEFL) LM_RSV_XN_MIN_RANDOM, (MLEFL) LM_RSV_XN_MAX_RANDOM, 0);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not randomize (xN) array", name);
		return(status);
	}

	for (i = 0; i < n; i++) dx[i] = x_next[i] - x_prev[i];

} else {
	thError("%s: WARNING - discovered (n = %d) - skipping matrix inversion", name, n);
}

#if DEEP_DEBUG_MLE
sprintf(comment, "dx - sector = '%s' - prior to definition", shEnumNameGetFromValue("LSECTOR", sector));
dbg1DArray(dx, n, name, comment);
#endif

#if MLE_CATCH_NAN
for (i = 0; i < n; i++) {
	if (isnan(dx[i]) || isinf(dx[i])) {
		thError("%s: ERROR - (NAN) detected in dx[%d] = %g", 
			name, i, (float) dx[i]);
		err_nan++;
	}	
}
if (err_nan > 0) {
	thError("%s: ERROR - (NAN) detected in (%d) occasions", name, err_nan);
	return(SH_GENERIC_ERROR);
}
#endif

int step_count = 0, step_mode = 0;
status = thLmethodGetStepCount(lmethod, &step_count, &step_mode);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (step_count) from (lmethod)", name);
	return(status);
}

LM_STEP_STATUS step_status = UNKNOWN_LM_STEP_STATUS;

CHISQFL Nchisq, Ncost;
CONVERGENCE convergence = CONTINUE;
if (k >= kmax) {
	convergence = TOOMANYITER;
	/* there is no h-convergence test for Gradient Descent methods */
} 

if (convergence == CONTINUE) {
	/* now trying to see if the new step is acceptable */
	status = IncrementX(lstruct, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not increment the X-values for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(status);
	}

	#if (DEEP_DEBUG_MLE | 1)	
	printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
	/* 
	dbgX(lstruct, name, ASECTOR, "A-sector, after X-increment");
	*/
	dbgX(lstruct, name, PSECTOR, "P-sector, after X-increment");
	#endif

	status = thLDumpX(lstruct, XNTOMODEL, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump (pN) values onto model parameters", name);
		return(status);
	}

	ALGORITHM *alg;
	status = thLstructGetAlgByStage(lstruct, MIDDLESTAGE, &alg);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (algorithm) for 'MIDDLESTAGE'", name);
		return(status);
	}
	status = thAlgorithmRun(alg, lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get run (algorithm) for 'MIDDLESTAGE'", name);
		return(status);
	}
	if (sector == PSECTOR) {
		#if DEEP_DEBUG_MLE
		printf("%s: conducting amplitude fit after original dx ['PSECTOR'] \n", name);
		printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
		dbgX(lstruct, name, ASECTOR, "A-sector, before amplitude fit");
		dbgX(lstruct, name, PSECTOR, "P-sector, before amplitude fit");
		#endif
		status = DoAmplitudeFit(lstruct);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not perform the amplitude fit after parameter fitin the loop", name);
			fflush(stderr);
			fflush(stdout);
			return(status);
		}
		#if DEEP_DEBUG_MLE
		printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
		dbgX(lstruct, name, ASECTOR, "A-sector, after amplitude fit");
		dbgX(lstruct, name, PSECTOR, "P-sector, after amplitude fit");
		#endif
		status = thLDumpX(lstruct, XNTOMODEL, ASECTOR);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not dump (aN) onto (model)", name);
			return(SH_SUCCESS);
		}
	}
	/* should we do amplitude fit */
	status = thLCalcChisqNAlgRun(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not calculate (chisqN) for (xN)", name);
		return(status);
	}
	status = thMakeJMatricesAlgRun(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not make J-matrices", name);
		return(status);
	}
	status = thLstructGetChisqN(lstruct, &Nchisq, &Ncost);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract derivative information for the new par value", name);
		return(status);
	}  
	if (Ncost < Nchisq) {
		thError("%s: WARNING - found (chisqN = %g) > (costN = %g", name, (float) Nchisq, (float) Ncost);
	}	 
		
	printf("%s: (dX2 = %G, X2 = %20.10G, X2N = %20.10G\n", name, (double) (cost - Ncost), (double) cost, (double) Ncost);
	printf("%s: RSV does not have any step size hyperparameter \n", name);

	#if DO_LFIT_RECORD
	status = do_lfit_record_insert_chisq_in_LM(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not insert (chisq) in (lmethod)", name);
		return(status);
	}
	#endif
	step_status = LM_STEP_ACCEPTED;
	printf("%s: RSV does not have any rejection or acceptance criteria \n", name);
	status = UpdateLMConvergence(lstruct, convergence);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update (convergence flag) to '%s'", name, shEnumNameGetFromValue("CONVERGENCE", convergence));
		return(status);
	}

} else {
	step_count = 0;
	step_status = LM_STEP_ACCEPTED;
	status = UpdateLMConvergence(lstruct, convergence);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update (convergence flag) to '%s'", name, shEnumNameGetFromValue("CONVERGENCE", convergence));
		return(status);
	}	
}

if (step_status == LM_STEP_ACCEPTED) {
	#if DEEP_DEBUG_MLE
	printf("%s: step accepted, (chisq_old = %20.10G, chisq_new = %20.10G) \n", name, (double) cost, (double) Ncost);
	printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
	dbgX(lstruct, name, ASECTOR, "A-sector, before X-update");
	dbgX(lstruct, name, PSECTOR, "P-sector, before X-update");
	#endif
	status = UpdateXFromXN(lstruct, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update the parameter values for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(status);
	}
	#if DEEP_DEBUG_MLE
	printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
	dbgX(lstruct, name, ASECTOR, "A-sector, after X-update");
	dbgX(lstruct, name, PSECTOR, "P-sector, after X-update");
	#endif
	/* this part seems to have been mistakenly omitted Feb 14, 2012 */  
	status = UpdateChisq(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update the (chisq) value", name);
		return(status);
	}
} else if (step_status == LM_STEP_REJECTED) {
	printf("%s: step rejected, (chisq_old = %20.10G, chisq_new = %20.10G) \n", name, (double) cost, (double) Ncost);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not revert J-matrices due to rejection of step", name);
		return(status);
	}
	status = thLstructSetXupdate(lstruct, OUTDATED, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not set (pupdate) flag in (lstruct) to 'outdated'", name);
		return(status);
	}
} else {
	thError("%s: ERROR - unsupported (step_status = '%s')", name, shEnumNameGetFromValue("LM_STEP_STATUS", step_status));
	return(SH_GENERIC_ERROR);
}
/* should you not do an amplitude fit - one which is reversible to old values */

#if (DO_LFIT_RECORD || 1)
step_mode++;
status = thLmethodPutStatus(lmethod, step_status, step_count, step_mode);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update the acceptance status in (lmethod)", name);
	return(status);
}
#endif

status = thLmethodRSVPut(lmethod, wmethod_type);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update contentx of (lmethod)", name);
	return(status);
}

/* update the step number */
k++;
status = UpdateLMStepNo(lstruct, k);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update (step_no) in (lmethod)", name);
	return(status);
}

printf("%s: *** EXITING *** \n", name);
return(SH_SUCCESS);
}


RET_CODE DoPGDFitAlgRun(LSTRUCT *lstruct, LSECTOR sector) {
char *name = "DoPGDFitAlgRun";
RET_CODE status = SH_SUCCESS;

printf("%s: *** STARTING *** \n", name);
/* Solve B h_qn = -F'(x_old) */

if (sector != APSECTOR) {
	thError("%s: WARNING - working with (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
}

LWORK *lwork = NULL;
status = thLstructGetLwork(lstruct, &lwork);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwork) from (lstruct)", name);
	return(status);
}

LWMASK *lwmask = NULL;
status = thLstructGetLwmask(lstruct, &lwmask);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwmask) from (lstruct)", name);
	return(status);
}

int k, kmax;
status = ExtractLMStepNo(lstruct, &k, &kmax); 
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract convergence criteria", name);
	return(status);
}

int i, n;
CHISQFL chisq = 0.0, cost = 0.0;
MLEFL *dx, *x_prev, *x_next;
MLEFL *jtdmm_prev = NULL, *jtdmm_next = NULL;

status = thLstructGetChisq(lstruct, &chisq, &cost);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (chisq) and (cost) from (lstruct)", name);
	return(status);
}
status = thLworkGetX(lwork, &x_prev, &n, sector);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (x_prev) for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(status);
}
status = thLworkGetXN(lwork, &x_next, &n, sector);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (x_next) for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(status);
}

status = thLworkGetPGD(lwork, &jtdmm_prev, &jtdmm_next, &dx, &n, sector);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (PGD) workspace from (lwork)", name);
	return(status);
}

#if DEEP_DEBUG_MLE
printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
dbgX(lstruct, name, ASECTOR, "A-sector, upon start");
dbgX(lstruct, name, PSECTOR, "P-sector, upon start");
#endif

if (dx == NULL) {
	thError("%s: ERROR - (lwork) not allocated properly - null (dx) array among (GD) arrays", name);
	return(SH_GENERIC_ERROR);
} else if (jtdmm_prev == NULL) {
	thError("%s: ERROR - (lwork) not allocated properly - null (jtdmm_prev) array among (GD) arrays", name);
	return(SH_GENERIC_ERROR);
} 

#if DEEP_DEBUG_MLE
dbgJtdmm(jtdmm_prev, n, name, "Jtdmm_prev upon start");
dbgJtdmm(jtdmm_next, n, name, "Jtdmm_next upon start");
#endif

#if MLE_CATCH_NAN
int err_nan = 0;
for (i = 0; i < n; i++) {
	if (isnan(jtdmm_prev[i]) || isinf(jtdmm_prev[i])) {
		thError("%s: ERROR - (NAN) detected in Jt[D-M][%d]", name, i);
		err_nan++;
	}
}

if (err_nan > 0) {
	thError("%s: ERROR - (NAN) detected in (%d) occasions", name, err_nan);
	return(SH_GENERIC_ERROR);
}

#endif

LMETHOD *lmethod = NULL;
status = thLstructGetLmethod(lstruct, &lmethod);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmethod) from (lstruct)", name);
	return(status);
}

int perturbation_PGD_condition = 0, count_PGD_noise, count_PGD_thresh, k_PGD_noise = 0, k_PGD_thresh = 0;
MLEFL mag_Fprime_PGD_thresh = 0.0, perturbation_PGD_size = 0.0;
MLEFL cost_PGD_noise = 0.0, chisq_PGD_noise = 0.0, cost_PGD_thresh = 0.0;
LMETHODSUBTYPE wmethod_type = UNKNOWN_LMETHODSUBTYPE;
MLEFL eta_gd_max = 0.0;
#if (CONTROL_PGD_STEPSIZE || 1)
status = thLmethodGetPGD(lmethod, 
	&count_PGD_noise, &count_PGD_thresh, &k_PGD_noise, &k_PGD_thresh, 
	&perturbation_PGD_condition, &perturbation_PGD_size, &mag_Fprime_PGD_thresh, 
	&eta_gd_max, 
	&cost_PGD_noise, &chisq_PGD_noise, &cost_PGD_thresh, &wmethod_type);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (delta_qn) from (lmethod)", name);
	return(status);
}
#endif

if (wmethod_type != PGDPURE) {
	thError("%s: ERROR - expected (wmethod_type = 'PGDPURE') but received '%s'", name, shEnumNameGetFromValue("WMETHODTYPE", wmethod_type));
	return(SH_GENERIC_ERROR);
}

char comment[1000];
#if DEEP_DEBUG_MLE
sprintf(comment, "dX - sector = '%s' - prior to definition", shEnumNameGetFromValue("LSECTOR", sector));
dbg1DArray(dx, n, name, comment);
#endif

if (n > 0) {
	if (perturbation_PGD_condition & PERTURB_GENERAL) {
		int approached_edge_by_value = 0;
		status = check_for_edge(x_prev, n, &approached_edge_by_value);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not check for edge effects", name);
			return(status);
		}
		if (approached_edge_by_value) {
			
			printf("%s: perturb due to edge effect (PGD_cond = %d, edge_flag = %d, k = %d, dim = %d, min = %g, max = %g) \n", name, 
				perturbation_PGD_condition, approached_edge_by_value, k, n, (float) LM_PGD_XN_MIN_RANDOM, (float) LM_PGD_XN_MAX_RANDOM);
			sprintf(comment, "X  - sector = '%s' - prior to edge perturbation", shEnumNameGetFromValue("LSECTOR", sector));
			dbg1DArray(x_prev, n, name, comment);
			sprintf(comment, "XN - sector = '%s' - prior to edge perturbation", shEnumNameGetFromValue("LSECTOR", sector));
			dbg1DArray(x_next, n, name, comment);


			status = randomize_mlefl_array(x_next, n, (MLEFL) LM_PGD_XN_MIN_RANDOM, (MLEFL) LM_PGD_XN_MAX_RANDOM, 1);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not randomize (xN) array", name);
				return(status);
			}

			k_PGD_noise = k;
			for (i = 0; i < n; i++) dx[i] = x_next[i] - x_prev[i];

		} else {

			printf("%s: taking perturbation step (k = %d, dim = %d, r = %g) \n", name, k, n, (float) perturbation_PGD_size);
			status = random_ball(dx, n, perturbation_PGD_size);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not create random (dx)", name);
				return(status);
			}
			k_PGD_noise = k;
			eta_gd_max = (MLEFL) LM_ETA_MAX_DEFAULT;
			#if (DEEP_DEBUG_MLE || 1)
			sprintf(comment, "dX - sector = '%s' - assigned randomly", 
				shEnumNameGetFromValue("LSECTOR", sector));
			dbg1DArray(dx, n, name, comment);
			#endif

		}
	} else {
		#if (CONTROL_PGD_STEPSIZE || ETA_FIXED_PGD)
		MLEFL eta = eta_gd_max;
		printf("%s: taking GD (contolled) step (k = %d, dim = %d, eta = %g) \n", name, k, n, (float) eta);
		#else
		MLEFL **jtj = NULL; 
		status = thExtractJtj(lstruct, sector, &jtj);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (Jtj) for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
			return(status);
		}
		MLEFL eta1 = mag_mlefl_vector(jtdmm_prev, NULL, n, 0);
		MLEFL eta2 = mag_mlefl_vector(jtdmm_prev, jtj, n, 0);
		MLEFL eta = pow(eta1 / eta2, 2.0);
		printf("%s: taking GD (uncontrolled) step (k = %d, dim = %d, eta = %g, eta1 = %g, eta2 = %g) \n", name, k, n, (float) eta, (float) eta1, (float) eta2);
		#endif
		for (i = 0; i < n; i++) dx[i] = jtdmm_prev[i] * eta;
		#if (DEEP_DEBUG_MLE | 1)
		sprintf(comment, "dX - sector = '%s' - assigned in gradient step", shEnumNameGetFromValue("LSECTOR", sector));
		dbg1DArray(dx, n, name, comment);
		#endif

	}
} else {
	thError("%s: WARNING - discovered (n = %d) - skipping matrix inversion", name, n);
}

#if DEEP_DEBUG_MLE
sprintf(comment, "dx - sector = '%s' - prior to definition", shEnumNameGetFromValue("LSECTOR", sector));
dbg1DArray(dx, n, name, comment);
#endif

#if MLE_CATCH_NAN
for (i = 0; i < n; i++) {
	if (isnan(dx[i]) || isinf(dx[i])) {
		thError("%s: ERROR - (NAN) detected in dx[%d] = %g", 
			name, i, (float) dx[i]);
		err_nan++;
	}	
}
if (err_nan > 0) {
	thError("%s: ERROR - (NAN) detected in (%d) occasions", name, err_nan);
	return(SH_GENERIC_ERROR);
}
#endif

int step_count = 0, step_mode = 0;
status = thLmethodGetStepCount(lmethod, &step_count, &step_mode);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (step_count) from (lmethod)", name);
	return(status);
}

LM_STEP_STATUS step_status = UNKNOWN_LM_STEP_STATUS;

CHISQFL Nchisq, Ncost;
CONVERGENCE convergence = CONTINUE;
if (k >= kmax) {
	convergence = TOOMANYITER;
	/* there is no h-convergence test for Gradient Descent methods */
} 

if (convergence == CONTINUE) {
	/* now trying to see if the new step is acceptable */
	status = IncrementX(lstruct, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not increment the X-values for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(status);
	}

	#if (DEEP_DEBUG_MLE | 1)	
	printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
	/* 
	dbgX(lstruct, name, ASECTOR, "A-sector, after X-increment");
	*/
	dbgX(lstruct, name, PSECTOR, "P-sector, after X-increment");
	#endif

	status = thLDumpX(lstruct, XNTOMODEL, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump (pN) values onto model parameters", name);
		return(status);
	}

	ALGORITHM *alg;
	status = thLstructGetAlgByStage(lstruct, MIDDLESTAGE, &alg);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (algorithm) for 'MIDDLESTAGE'", name);
		return(status);
	}
	status = thAlgorithmRun(alg, lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get run (algorithm) for 'MIDDLESTAGE'", name);
		return(status);
	}
	if (sector == PSECTOR) {
		#if DEEP_DEBUG_MLE
		printf("%s: conducting amplitude fit after original dx ['PSECTOR'] \n", name);
		printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
		dbgX(lstruct, name, ASECTOR, "A-sector, before amplitude fit");
		dbgX(lstruct, name, PSECTOR, "P-sector, before amplitude fit");
		#endif
		status = DoAmplitudeFit(lstruct);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not perform the amplitude fit after parameter fitin the loop", name);
			fflush(stderr);
			fflush(stdout);
			return(status);
		}
		#if DEEP_DEBUG_MLE
		printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
		dbgX(lstruct, name, ASECTOR, "A-sector, after amplitude fit");
		dbgX(lstruct, name, PSECTOR, "P-sector, after amplitude fit");
		#endif
		status = thLDumpX(lstruct, XNTOMODEL, ASECTOR);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not dump (aN) onto (model)", name);
			return(SH_SUCCESS);
		}
	}
	/* should we do amplitude fit */
	status = thLCalcChisqNAlgRun(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not calculate (chisqN) for (xN)", name);
		return(status);
	}
	status = thMakeJMatricesAlgRun(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not make J-matrices", name);
		return(status);
	}
	status = thLstructGetChisqN(lstruct, &Nchisq, &Ncost);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract derivative information for the new par value", name);
		return(status);
	}  
	if (Ncost < Nchisq) {
		thError("%s: WARNING - found (chisqN = %g) > (costN = %g", name, (float) Nchisq, (float) Ncost);
	}	 
	if (perturbation_PGD_condition) {
		cost_PGD_noise = Ncost;
		chisq_PGD_noise = Nchisq;
	}
	
	/* problematic definition of dl and rho */ 
	MLEFL **jtj = NULL; 
	status = thExtractJtj(lstruct, sector, &jtj);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (Jtj) for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(status);
	}
	MLEFL mag_dx = mag_mlefl_vector(dx, jtj, n, 0);
	MLEFL dl = 0.5 * pow((MLEFL) mag_dx, 2.0);
	for (i = 0; i < n; i++) {
		dl -= (MLEFL) jtdmm_prev[i] * (MLEFL) dx[i];
	}
	MLEFL rho = (Ncost - cost) / dl;
	if (cost == Ncost) rho = 0.0;

	#if (CONTROL_PGD_STEPSIZE || ETA_FIXED_PGD)
	printf("%s: (rho = %G, dl = %G, eta_max = %G, || dx || = %G, dX2 = %G, X2 = %20.10G, X2N = %20.10G\n", 
	name, (double) rho, (double) dl, (double) eta_gd_max, (double) mag_dx, (double) (cost - Ncost), (double) cost, (double) Ncost);
	
	MLEFL eta_gd_max_new = eta_gd_max;
	status = update_PGD_eta_max(lmethod, eta_gd_max, rho, mag_dx, cost, Ncost, &eta_gd_max_new);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update (delta) value for (quasi-newtotnian) step", name);
		return(status);
	}
	eta_gd_max = eta_gd_max_new;
	printf("%s: eta_max_new = %G \n", name, (double) eta_gd_max_new);
	status = thLmethodPutEta(lmethod, eta_gd_max_new);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put (eta_gd_max = %g) in (lmethod)", name, (float) eta_gd_max_new);
		return(status);
	}
	#else
	printf("%s: (| dX | = %G, dX2 = %G, X2 = %20.10G, X2N = %20.10G\n", 
	name, (double) mag_dx, (double) (cost - Ncost), (double) cost, (double) Ncost);
	printf("%s: PGD does not have any step size hyperparameter \n", name);
	#endif

	#if DO_LFIT_RECORD
	status = do_lfit_record_insert_chisq_in_LM(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not insert (chisq) in (lmethod)", name);
		return(status);
	}
	#endif
	int approached_edge_by_value = 0;
	status = check_for_edge(x_next, n, &approached_edge_by_value);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - unable to check for edge effects", name);
		return(status);
	}

	if (approached_edge_by_value) perturbation_PGD_condition = PERTURB_DUE_TO_EDGE;

	if (convergence == CONTINUE) {
	
		MLEFL mag_Fprime = mag_mlefl_vector(jtdmm_next, NULL, n, 0);
		printf("%s: evaluating pertubation flag (k, k_noise, k_thresh) = (%d, %d, %d), (|F'|, |F'|_thresh) = (%g, %g) \n", 
			name, k, k_PGD_noise, k_PGD_thresh, (float) mag_Fprime, (float) mag_Fprime_PGD_thresh);
		if ((mag_Fprime < mag_Fprime_PGD_thresh) && ((k - k_PGD_noise) > k_PGD_thresh) && !approached_edge_by_value) {
			perturbation_PGD_condition = PERTURB_DUE_TO_CONVERGE;
		} else if ((k - k_PGD_noise) > 300 * k_PGD_thresh) {	
			perturbation_PGD_condition = PERTURB_DUE_TO_LATE;
		} else if (approached_edge_by_value) {
			perturbation_PGD_condition = PERTURB_DUE_TO_EDGE; 
		} else {
			perturbation_PGD_condition = NO_PERTURB;
		}
		#if CONTROL_PGD_STEPSIZE
		if ((Ncost < cost) || perturbation_PGD_condition) {
			step_status = LM_STEP_ACCEPTED;
		} else {
			step_status = LM_STEP_REJECTED;
		}
	 	
		#else
		step_status = LM_STEP_ACCEPTED;
		printf("%s: PGD does not have any rejection or acceptance criteria \n", name);
		#endif
		status = UpdateLMConvergence(lstruct, convergence);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not update (convergence flag) to '%s'", name, shEnumNameGetFromValue("CONVERGENCE", convergence));
			return(status);
		}

	} else {
		step_status = LM_STEP_ACCEPTED;
		status = UpdateLMConvergence(lstruct, convergence);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not update (convergence flag) to '%s'", name, shEnumNameGetFromValue("CONVERGENCE", convergence));
			return(status);
		}	
	}
} else {
	step_count = 0;
	step_status = LM_STEP_ACCEPTED;
	status = UpdateLMConvergence(lstruct, convergence);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update (convergence flag) to '%s'", name, shEnumNameGetFromValue("CONVERGENCE", convergence));
		return(status);
	}	
}

if (step_status == LM_STEP_ACCEPTED) {
	#if DEEP_DEBUG_MLE
	printf("%s: step accepted, (chisq_old = %20.10G, chisq_new = %20.10G) \n", name, (double) cost, (double) Ncost);
	printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
	dbgX(lstruct, name, ASECTOR, "A-sector, before X-update");
	dbgX(lstruct, name, PSECTOR, "P-sector, before X-update");
	#endif
	status = UpdateXFromXN(lstruct, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update the parameter values for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(status);
	}
	#if DEEP_DEBUG_MLE
	printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
	dbgX(lstruct, name, ASECTOR, "A-sector, after X-update");
	dbgX(lstruct, name, PSECTOR, "P-sector, after X-update");
	#endif
	/* this part seems to have been mistakenly omitted Feb 14, 2012 */  
	status = UpdateChisq(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update the (chisq) value", name);
		return(status);
	}
} else if (step_status == LM_STEP_REJECTED) {
	printf("%s: step rejected, (chisq_old = %20.10G, chisq_new = %20.10G) \n", name, (double) cost, (double) Ncost);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not revert J-matrices due to rejection of step", name);
		return(status);
	}
	status = thLstructSetXupdate(lstruct, OUTDATED, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not set (pupdate) flag in (lstruct) to 'outdated'", name);
		return(status);
	}
} else {
	thError("%s: ERROR - unsupported (step_status = '%s')", name, shEnumNameGetFromValue("LM_STEP_STATUS", step_status));
	return(SH_GENERIC_ERROR);
}
/* should you not do an amplitude fit - one which is reversible to old values */

#if (DO_LFIT_RECORD || 1)
step_mode++;
status = thLmethodPutStatus(lmethod, step_status, step_count, step_mode);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update the acceptance status in (lmethod)", name);
	return(status);
}
#endif

if (((k - k_PGD_noise) == k_PGD_thresh) && ((cost_PGD_noise - Ncost) > -cost_PGD_thresh) && (count_PGD_noise >= count_PGD_thresh)) {
	if (PGD_SECOND_PHASE == PGDPURE) {
		thError("%s: WARNING - second phase of (PGD) should not be the same as its primary phase", name);
	}
	wmethod_type = PGD_SECOND_PHASE;	
} else {
	printf("%s: DEBUG - count_PGD_noise = %d, count_PGD_thresh = %d \n", name, count_PGD_noise, count_PGD_thresh);
}
status = thLmethodPGDPut(lmethod, k_PGD_noise, perturbation_PGD_condition, eta_gd_max, cost_PGD_noise, chisq_PGD_noise, wmethod_type);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update contentx of (lmethod)", name);
	return(status);
}

/* update the step number */
k++;
status = UpdateLMStepNo(lstruct, k);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update (step_no) in (lmethod)", name);
	return(status);
}

printf("%s: *** EXITING *** \n", name);
return(SH_SUCCESS);
}

/* Nosterov Accelerated Gradient Method */
RET_CODE DoNAGFitAlgRun(LSTRUCT *lstruct, LSECTOR sector) {
char *name = "DoNAGFitAlgRun";
RET_CODE status = SH_SUCCESS;

printf("%s: *** STARTING *** \n", name);
/* Solve B h_qn = -F'(x_old) */

if (sector != APSECTOR) {
	thError("%s: WARNING - working with (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
}

LWORK *lwork = NULL;
status = thLstructGetLwork(lstruct, &lwork);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwork) from (lstruct)", name);
	return(status);
}

LWMASK *lwmask = NULL;
status = thLstructGetLwmask(lstruct, &lwmask);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwmask) from (lstruct)", name);
	return(status);
}

int i, n;
MLEFL *jtdmm_prev = NULL, *jtdmm_next = NULL;
CHISQFL chisq = 0.0, cost = 0.0;
MLEFL *dx = NULL, *x_prev = NULL, *x_next = NULL;
status = thLstructGetChisq(lstruct, &chisq, &cost);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (chisq) and (cost) from (lstruct)", name);
	return(status);
}
status = thLworkGetMGD(lwork, &jtdmm_prev, &jtdmm_next, &dx, &n, sector);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (gradient-descent) matrices", name);
	return(status);
}
status = thLworkGetX(lwork, &x_prev, &n, sector);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (x_prev) for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(status);
}
status = thLworkGetXN(lwork, &x_next, &n, sector);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (x_next) for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(status);
}


#if DEEP_DEBUG_MLE
printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
dbgX(lstruct, name, ASECTOR, "A-sector, upon start");
dbgX(lstruct, name, PSECTOR, "P-sector, upon start");
#endif

if (dx == NULL) {
	thError("%s: ERROR - (lwork) not allocated properly - null (dx) array among (NAG) arrays", name);
	return(SH_GENERIC_ERROR);
} else if (jtdmm_prev == NULL) {
	thError("%s: ERROR - (lwork) not allocated properly - null (jtdmm_prev) array among (NAG) arrays", name);
	return(SH_GENERIC_ERROR);
} else if (jtdmm_next == NULL) {
	thError("%s: ERROR - (lwork) not allocated properly - null (jtdmm_next) array among (NAG) arrays", name);
	return(SH_GENERIC_ERROR);
} 


#if DEEP_DEBUG_MLE
dbgJtdmm(jtdmm_prev, n, name, "Jtdmm_prev upon start");
dbgJtdmm(jtdmm_next, n, name, "Jtdmm_next upon start");
#endif

#if MLE_CATCH_NAN
int err_nan = 0;
for (i = 0; i < n; i++) {
	if (isnan(jtdmm_prev[i]) || isinf(jtdmm_prev[i])) {
		thError("%s: ERROR - (NAN) detected in Jt[D-M][%d]", name, i);
		err_nan++;
	}
}

if (err_nan > 0) {
	thError("%s: ERROR - (NAN) detected in (%d) occasions", name, err_nan);
	return(SH_GENERIC_ERROR);
}

#endif

CHISQFL Nchisq, Ncost;
LMETHOD *lmethod = NULL;
status = thLstructGetLmethod(lstruct, &lmethod);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmethod) from (lstruct)", name);
	return(status);
}

MLEFL lr_gd, momentum_gd;
status = thLmethodGetMGD(lmethod, &momentum_gd, &lr_gd, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (delta_qn) from (lmethod)", name);
	return(status);
}

int k, kmax;
status = ExtractLMStepNo(lstruct, &k, &kmax); 
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract convergence criteria", name);
	return(status);
}

char comment[1000];
#if DEEP_DEBUG_MLE
sprintf(comment, "dX - sector = '%s' - prior to definition", shEnumNameGetFromValue("LSECTOR", sector));
dbg1DArray(dx, n, name, comment);
#endif

if (n > 0) {
	int mini_batch_size = 0;
	status = thLwmaskGetMiniBatchSize(lwmask, &mini_batch_size);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get active (mini_batch_size) from (lwmask)", name);
		return(status);
	}
	if (mini_batch_size <= 0) {
		thError("%s: ERROR - found an improper active (mini_batch_size = %d) from (lwmask)", name, mini_batch_size);
		return(SH_GENERIC_ERROR);
	}

	/* the following x_new will be used to calculate the gradient */
	for (i = 0; i < n; i++) x_next[i] = x_prev[i] + momentum_gd * dx[i];
	status = thLDumpX(lstruct, XNTOMODEL, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump (pN) values onto model parameters", name);
		return(status);
	}

	ALGORITHM *alg;
	status = thLstructGetAlgByStage(lstruct, MIDDLESTAGE, &alg);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (algorithm) for 'MIDDLESTAGE'", name);
		return(status);
	}
	status = thAlgorithmRun(alg, lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get run (algorithm) for 'MIDDLESTAGE'", name);
		return(status);
	}
	if (sector == PSECTOR) {
		#if DEEP_DEBUG_MLE
		printf("%s: conducting amplitude fit after original dx ['PSECTOR'] \n", name);
		printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
		dbgX(lstruct, name, ASECTOR, "A-sector, before amplitude fit");
		dbgX(lstruct, name, PSECTOR, "P-sector, before amplitude fit");
		#endif
		status = DoAmplitudeFit(lstruct);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not perform the amplitude fit after parameter fitin the loop", name);
			fflush(stderr);
			fflush(stdout);
			return(status);
		}
		#if (DEEP_DEBUG_MLE)
		printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector)); 
		dbgX(lstruct, name, ASECTOR, "A-sector, after amplitude fit");
		dbgX(lstruct, name, PSECTOR, "P-sector, after amplitude fit");
		#endif
		status = thLDumpX(lstruct, XNTOMODEL, ASECTOR);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not dump (aN) onto (model)", name);
			return(SH_SUCCESS);
		}
	}
	/* should we do amplitude fit */
	status = thLCalcChisqNAlgRun(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not calculate (chisqN) for (xN)", name);
		return(status);
	}
	status = thMakeJMatricesAlgRun(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not make J-matrices", name);
		return(status);
	}
	status = thLstructGetChisqN(lstruct, &Nchisq, &Ncost);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract derivative information for the new par value", name);
		return(status);
	}  
	if (Ncost < Nchisq) {
		thError("%s: WARNING - found (chisqN = %g) > (costN = %g", name, (float) Nchisq, (float) Ncost);
	}	 

	printf("%s: momentum = %g, learning rate = %g \n", name, (float) momentum_gd, (float) lr_gd);
	sprintf(comment, "dx(PREV) - sector = '%s'", shEnumNameGetFromValue("LSECTOR", sector));
	dbg1DArray(dx, n, name, comment);
	for (i = 0; i < n; i++) dx[i] = momentum_gd * dx[i] + lr_gd * jtdmm_prev[i];
	if (k == 0) {
		MLEFL mag_dx = mag_mlefl_vector(dx, NULL, n, 0);	
		if (mag_dx < (MLEFL) LM_NAG_MIN_INIT_STEP) {
			printf("%s: adjusting dx size from (%g) to (%g) in the first step \n", name, (float) mag_dx, (float) LM_NAG_MIN_INIT_STEP);
			for (i = 0; i < n; i++) dx[i] *= ((MLEFL) LM_NAG_MIN_INIT_STEP) / mag_dx;	
		}
	}
	sprintf(comment, "dx(NEXT) - sector = '%s'", shEnumNameGetFromValue("LSECTOR", sector));
	dbg1DArray(dx, n, name, comment);

} else {
	thError("%s: WARNING - discovered (n = %d) - skipping matrix inversion", name, n);
}

#if DEEP_DEBUG_MLE
sprintf(comment, "dx - sector = '%s' - prior to definition", shEnumNameGetFromValue("LSECTOR", sector));
dbg1DArray(dx, n, name, comment);
#endif

#if MLE_CATCH_NAN
for (i = 0; i < n; i++) {
	if (isnan(dx[i]) || isinf(dx[i])) {
		thError("%s: ERROR - (NAN) detected in dx[%d] = %g, F'[%d] = %g", 
			name, i, (float) dx[i], i, (float) jtdmm_prev[i]);
		err_nan++;
	}	
}
if (err_nan > 0) {
	thError("%s: ERROR - (NAN) detected in (%d) occasions", name, err_nan);
	return(SH_GENERIC_ERROR);
}
#endif

int step_count = 0, step_mode = 0;
status = thLmethodGetStepCount(lmethod, &step_count, &step_mode);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (step_count) from (lmethod)", name);
	return(status);
}

LM_STEP_STATUS step_status = UNKNOWN_LM_STEP_STATUS;

CONVERGENCE convergence = CONTINUE;
if (k >= kmax) {
	convergence = TOOMANYITER;
	/* there is no h-convergence test for Gradient Descent methods */
} else {
	#if CHECK_NAG_STEPSIZE
	convergence = test_GD_dx_convergence(lmethod, dx, x_prev, NULL, n);
	printf("%s: tested for h-convergence (flag = '%s') \n", name, shEnumNameGetFromValue("CONVERGENCE", convergence));
	#else
	printf("%s: NAG does not have a step size convergence test \n", name);
	convergence = CONTINUE;
	#endif
}
if (convergence == CONTINUE) {
	/* now trying to see if the new step is acceptable */
	status = IncrementX(lstruct, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not increment the X-values for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(status);
	}

	#if (DEEP_DEBUG_MLE)	
	printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
	dbgX(lstruct, name, ASECTOR, "A-sector, after X-increment");
	dbgX(lstruct, name, PSECTOR, "P-sector, after X-increment");
	#endif

	status = thLDumpX(lstruct, XNTOMODEL, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump (pN) values onto model parameters", name);
		return(status);
	}

	ALGORITHM *alg;
	status = thLstructGetAlgByStage(lstruct, MIDDLESTAGE, &alg);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (algorithm) for 'MIDDLESTAGE'", name);
		return(status);
	}
	status = thAlgorithmRun(alg, lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get run (algorithm) for 'MIDDLESTAGE'", name);
		return(status);
	}
	if (sector == PSECTOR) {
		#if DEEP_DEBUG_MLE
		printf("%s: conducting amplitude fit after original dx ['PSECTOR'] \n", name);
		printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
		dbgX(lstruct, name, ASECTOR, "A-sector, before amplitude fit");
		dbgX(lstruct, name, PSECTOR, "P-sector, before amplitude fit");
		#endif
		status = DoAmplitudeFit(lstruct);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not perform the amplitude fit after parameter fitin the loop", name);
			fflush(stderr);
			fflush(stdout);
			return(status);
		}
		#if (DEEP_DEBUG_MLE | 1)
		printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
		/* 
		dbgX(lstruct, name, ASECTOR, "A-sector, after amplitude fit");
		*/
		dbgX(lstruct, name, PSECTOR, "P-sector, after amplitude fit");
		#endif
		status = thLDumpX(lstruct, XNTOMODEL, ASECTOR);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not dump (aN) onto (model)", name);
			return(SH_SUCCESS);
		}
	}
	/* should we do amplitude fit */
	status = thLCalcChisqNAlgRun(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not calculate (chisqN) for (xN)", name);
		return(status);
	}
	status = thMakeJMatricesAlgRun(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not make J-matrices", name);
		return(status);
	}
	status = thLstructGetChisqN(lstruct, &Nchisq, &Ncost);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract derivative information for the new par value", name);
		return(status);
	}  
	if (Ncost < Nchisq) {
		thError("%s: WARNING - found (chisqN = %g) > (costN = %g", name, (float) Nchisq, (float) Ncost);
	}	 
	
	/* problematic definition of dl and rho */
	MLEFL mag_dx = mag_mlefl_vector(dx, NULL, n, 0);
	MLEFL dl = 0.5 * pow((MLEFL) mag_dx, 2.0);
	for (i = 0; i < n; i++) {
		dl -= (MLEFL) jtdmm_prev[i] * (MLEFL) dx[i];
	}
	MLEFL rho = (Ncost - cost) / dl;
	if (cost == Ncost) rho = 0.0;

	#if CONTROL_NAG_STEPSIZE
	printf("%s: (rho = %G, dl = %G, eta_gd_max = %G, || dx || = %G, dX2 = %G, X2 = %20.10G, X2N = %20.10G\n", 
	name, (double) rho, (double) dl, (double) eta_gd_max, (double) mag_dx, (double) (cost - Ncost), (double) cost, (double) Ncost);
	
	MLEFL eta_gd_max_new = eta_gd_max;
	status = update_GD_eta_max(lmethod, eta_gd_max, rho, mag_dx, &eta_gd_max_new);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update (delta) value for (quasi-newtotnian) step", name);
		return(status);
	}
	status = thLmethodPutEta(lmethod, eta_gd_max_new);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put (eta_gd_max = %g) in (lmethod)", name, (float) eta_gd_max_new);
		return(status);
	}
	#else
	printf("%s: (| dX | = %G, dX2 = %G, X2 = %20.10G, X2N = %20.10G\n", 
	name, (double) mag_dx, (double) (cost - Ncost), (double) cost, (double) Ncost);
	printf("%s: NAG does not have any step size hyperparameter \n", name);
	#endif

	#if DO_LFIT_RECORD
	status = do_lfit_record_insert_chisq_in_LM(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not insert (chisq) in (lmethod)", name);
		return(status);
	}
	#endif

	convergence = test_GD_Fprime_convergence(lmethod, NULL, jtdmm_next, n);
	printf("%s: tested for F' convergence (flag = '%s') \n", name, shEnumNameGetFromValue("CONVERGENCE", convergence));
	MLEFL mag_Fprime = mag_mlefl_vector(jtdmm_next, NULL, n, 0);
	MLEFL mag_Fprime_prev = mag_mlefl_vector(jtdmm_prev, NULL, n, 0);
	printf("%s: |F'| = %20.10G, |F'N| = %20.10G, dim = %d \n", name, (double) mag_Fprime_prev, (double) mag_Fprime, n); 
	int k_fprime_convergence = 0;
	status = thLmethodGetFprimeIncidence(lmethod, &k_fprime_convergence);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (Fprime) incidence in (lmethod)", name);
		return(status);
	}
	int approached_edge_by_value = 0;
	status = check_for_edge(x_next, n, &approached_edge_by_value);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not check for edge effects", name);
		return(status);
	}
	int approached_edge_by_deriv = ((convergence == TOOSMALLDERIV) && ((k - k_fprime_convergence) < LM_NAG_RESTART_THRESH));
	int approached_edge = approached_edge_by_deriv || approached_edge_by_value;
	int not_enough_initiation = ((convergence == TOOSMALLDERIV) && (step_mode < LM_NAG_INIT_THRESH_LOW));
	if (approached_edge || not_enough_initiation) {
		do {
			if (step_mode >= LM_NAG_INIT_THRESH) {
				printf("%s: reached edge but too many initiations (step_mode = %d) have already been attemped \n", name, step_mode);
				convergence = EDGE_CONVERGENCE;
				break;
			}
			
			#if (DEEP_DEBUG_MLE | 1)
			printf("%s: re-initiating model parameters as edge was reached \n", name);
			#endif
			
			/* chi-squared value might slightly change after amplitude fitting */
			status = thLCalcChisqNAlgRun(lstruct);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not calculate (chisq)", name);
				return(status);
			}
			status = UpdateXFromXN(lstruct, sector);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not update the parameter values for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
				return(status);
			}
			status = UpdateChisq(lstruct);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not update the (chisq) value", name);
				return(status);
			}
			status = thLmethodPutFprimeIncidence(lmethod, k);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not update (Fprime) incidene in (lmethod)", name);
				return(status);
			}

			#if DO_CAPTURE /* extensive debug mode, movie and step by step statistics */
			status = thCapture(capture);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not capture at (iter = %d)", name, iter);
				return(status);
			}
			status = thCaptureFlush(capture);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not flush (capture) related files", name);
				return(status);
			}
			fflush(stdout); /* this is so that the output to nohup.out file is uptodate */
			#endif
			
			#if DO_LFIT_RECORD
			status = do_lfit_record_insert_chisq_in_LM(lstruct);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not insert (chisq) info in (lmethod)", name);
				return(status);
			}
			status = thLRecordLfit(lstruct);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not record 'lmethod' at this step", name);
				return(status);
			}
			#endif

			#if (DO_LFIT_RECORD || 1)
			step_mode++;
			step_status = LM_STEP_ACCEPTED;
			status = thLmethodPutStatus(lmethod, step_status, step_count, step_mode);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not update the acceptance status in (lmethod)", name);
				return(status);
			}
			step_status = UNKNOWN_LM_STEP_STATUS;
			#endif
			/* update the step number */
			k++;
			status = UpdateLMStepNo(lstruct, k);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not update (step_no) in (lmethod)", name);
				return(status);
			}

			status = randomize_mlefl_array(x_next, n, (MLEFL) LM_NAG_XN_MIN_RANDOM, (MLEFL) LM_NAG_XN_MAX_RANDOM, 1);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not randomize (xN) array", name);
				return(status);
			}
			for (i = 0; i < n; i++) dx[i] = (MLEFL) 0.0;
			status = thLDumpX(lstruct, XNTOMODEL, sector);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not dump (pN) values onto model parameters", name);
				return(status);
			}
			status = thAlgorithmRun(alg, lstruct);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get run (algorithm) for 'MIDDLESTAGE'", name);
				return(status);
			}
			if (sector == PSECTOR) {
				#if DEEP_DEBUG_MLE
				printf("%s: conducting amplitude fit after original dx ['PSECTOR'] \n", name);
				printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
				dbgX(lstruct, name, ASECTOR, "A-sector, before amplitude fit");
				dbgX(lstruct, name, PSECTOR, "P-sector, before amplitude fit");
				#endif
				status = DoAmplitudeFit(lstruct);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not perform the amplitude fit after parameter fitin the loop", name);
					fflush(stderr);
					fflush(stdout);
					return(status);
				}
				#if (DEEP_DEBUG_MLE | 1)
				printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
				/* 
				dbgX(lstruct, name, ASECTOR, "A-sector, after amplitude fit");
				*/
				dbgX(lstruct, name, PSECTOR, "P-sector, after amplitude fit");
				#endif
				status = thLDumpX(lstruct, XNTOMODEL, ASECTOR);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not dump (aN) onto (model)", name);
					return(SH_SUCCESS);
				}
			}
			/* should we do amplitude fit */
			status = thLCalcChisqNAlgRun(lstruct);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not calculate (chisqN) for (xN)", name);
				return(status);
			}
			status = thMakeJMatricesAlgRun(lstruct);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not make J-matrices", name);
				return(status);
			}
			status = thLstructGetChisqN(lstruct, &Nchisq, &Ncost);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not extract derivative information for the new par value", name);
				return(status);
			}  
			if (Ncost < Nchisq) {
				thError("%s: WARNING - found (chisqN = %g) > (costN = %g", name, (float) Nchisq, (float) Ncost);
			}	 
			
			#if DO_LFIT_RECORD
			status = do_lfit_record_insert_chisq_in_LM(lstruct);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not insert (chisq) in (lmethod)", name);
				return(status);
			}
			#endif
			convergence = CONTINUE;
		} while (0);
	}

	if (convergence == CONTINUE) {
		#if CONTROL_NAG_STEPSIZE
		if ((Ncost < cost)) {
			step_status = LM_STEP_ACCEPTED;
		} else if (mag_Fprime > mag_Fprime_prev) {
			step_status = LM_STEP_REJECTED;
		} else {
			step_status = LM_STEP_REJECTED;
		}
	 
		if (mag_Fprime >= mag_Fprime_prev) {
			step_count++;
		} else {
			step_count = 0;
			status = thLmethodPutStepCount(lmethod, 0, step_mode);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not place (step_count) in (lmethod)", name);
				return(status);
			}
		}
	
		#else
		step_status = LM_STEP_ACCEPTED;
		printf("%s: NAG does not have any rejection or acceptance criteria \n", name);
		#endif
		status = UpdateLMConvergence(lstruct, convergence);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not update (convergence flag) to '%s'", name, shEnumNameGetFromValue("CONVERGENCE", convergence));
			return(status);
		}

	} else {
		step_status = LM_STEP_ACCEPTED;
		status = UpdateLMConvergence(lstruct, convergence);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not update (convergence flag) to '%s'", name, shEnumNameGetFromValue("CONVERGENCE", convergence));
			return(status);
		}	
	}
} else {
	step_count = 0;
	step_status = LM_STEP_ACCEPTED;
	status = UpdateLMConvergence(lstruct, convergence);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update (convergence flag) to '%s'", name, shEnumNameGetFromValue("CONVERGENCE", convergence));
		return(status);
	}	
}

if (step_status == LM_STEP_ACCEPTED) {
	/* 
	status = UpdateEAdaDelta(lwork, lmethod);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update (E_$) in (lwork)", name);
		return(status);
	}
	*/
	#if DEEP_DEBUG_MLE
	printf("%s: step accepted, (chisq_old = %20.10G, chisq_new = %20.10G) \n", name, (double) cost, (double) Ncost);
	printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
	dbgX(lstruct, name, ASECTOR, "A-sector, before X-update");
	dbgX(lstruct, name, PSECTOR, "P-sector, before X-update");
	#endif
	status = UpdateXFromXN(lstruct, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update the parameter values for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(status);
	}
	#if DEEP_DEBUG_MLE
	printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
	dbgX(lstruct, name, ASECTOR, "A-sector, after X-update");
	dbgX(lstruct, name, PSECTOR, "P-sector, after X-update");
	#endif
	/* this part seems to have been mistakenly omitted Feb 14, 2012 */  
	status = UpdateChisq(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update the (chisq) value", name);
		return(status);
	}
} else if (step_status == LM_STEP_REJECTED) {
	printf("%s: step rejected, (chisq_old = %20.10G, chisq_new = %20.10G) \n", name, (double) cost, (double) Ncost);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not revert J-matrices due to rejection of step", name);
		return(status);
	}
	status = thLstructSetXupdate(lstruct, OUTDATED, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not set (pupdate) flag in (lstruct) to 'outdated'", name);
		return(status);
	}
} else {
	thError("%s: ERROR - unsupported (step_status = '%s')", name, shEnumNameGetFromValue("LM_STEP_STATUS", step_status));
	return(SH_GENERIC_ERROR);
}
/* should you not do an amplitude fit - one which is reversible to old values */

#if (DO_LFIT_RECORD || 1)
status = thLmethodPutStatus(lmethod, step_status, step_count, step_mode);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update the acceptance status in (lmethod)", name);
	return(status);
}
#endif

/* update the step number */
k++;
status = UpdateLMStepNo(lstruct, k);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update (step_no) in (lmethod)", name);
	return(status);
}

printf("%s: *** EXITING *** \n", name);
return(SH_SUCCESS);
}





/* gradient descent with momentum */
RET_CODE DoMGDFitAlgRun(LSTRUCT *lstruct, LSECTOR sector) {
char *name = "DoMGDFitAlgRun";
RET_CODE status = SH_SUCCESS;

printf("%s: *** STARTING *** \n", name);
/* Solve B h_qn = -F'(x_old) */

if (sector != APSECTOR) {
	thError("%s: WARNING - working with (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
}

LWORK *lwork = NULL;
status = thLstructGetLwork(lstruct, &lwork);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwork) from (lstruct)", name);
	return(status);
}

LWMASK *lwmask = NULL;
status = thLstructGetLwmask(lstruct, &lwmask);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwmask) from (lstruct)", name);
	return(status);
}

int i, n;
MLEFL *jtdmm_prev = NULL, *jtdmm_next = NULL;
CHISQFL chisq = 0.0, cost = 0.0;
MLEFL *dx, *x_prev;
status = thLstructGetChisq(lstruct, &chisq, &cost);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (chisq) and (cost) from (lstruct)", name);
	return(status);
}
status = thLworkGetMGD(lwork, &jtdmm_prev, &jtdmm_next, &dx, &n, sector);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (gradient-descent) matrices", name);
	return(status);
}
status = thLworkGetX(lwork, &x_prev, &n, sector);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (x_prev) for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(status);
}

#if DEEP_DEBUG_MLE
printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
dbgX(lstruct, name, ASECTOR, "A-sector, upon start");
dbgX(lstruct, name, PSECTOR, "P-sector, upon start");
#endif

if (dx == NULL) {
	thError("%s: ERROR - (lwork) not allocated properly - null (dx) array among (MGD) arrays", name);
	return(SH_GENERIC_ERROR);
} else if (jtdmm_prev == NULL) {
	thError("%s: ERROR - (lwork) not allocated properly - null (jtdmm_prev) array among (MGD) arrays", name);
	return(SH_GENERIC_ERROR);
} else if (jtdmm_next == NULL) {
	thError("%s: ERROR - (lwork) not allocated properly - null (jtdmm_next) array among (MGD) arrays", name);
	return(SH_GENERIC_ERROR);
} 


#if DEEP_DEBUG_MLE
dbgJtdmm(jtdmm_prev, n, name, "Jtdmm_prev upon start");
dbgJtdmm(jtdmm_next, n, name, "Jtdmm_next upon start");
#endif

#if MLE_CATCH_NAN
int err_nan = 0;
for (i = 0; i < n; i++) {
	if (isnan(jtdmm_prev[i]) || isinf(jtdmm_prev[i])) {
		thError("%s: ERROR - (NAN) detected in Jt[D-M][%d]", name, i);
		err_nan++;
	}
}

if (err_nan > 0) {
	thError("%s: ERROR - (NAN) detected in (%d) occasions", name, err_nan);
	return(SH_GENERIC_ERROR);
}

#endif

LMETHOD *lmethod = NULL;
status = thLstructGetLmethod(lstruct, &lmethod);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmethod) from (lstruct)", name);
	return(status);
}

MLEFL lr_gd, momentum_gd;
status = thLmethodGetMGD(lmethod, &momentum_gd, &lr_gd, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (delta_qn) from (lmethod)", name);
	return(status);
}

char comment[1000];
#if DEEP_DEBUG_MLE
sprintf(comment, "dX - sector = '%s' - prior to definition", shEnumNameGetFromValue("LSECTOR", sector));
dbg1DArray(dx, n, name, comment);
#endif

if (n > 0) {
	int mini_batch_size = 0;
	status = thLwmaskGetMiniBatchSize(lwmask, &mini_batch_size);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get active (mini_batch_size) from (lwmask)", name);
		return(status);
	}
	if (mini_batch_size <= 0) {
		thError("%s: ERROR - found an improper active (mini_batch_size = %d) from (lwmask)", name, mini_batch_size);
		return(SH_GENERIC_ERROR);
	}
	printf("%s: momentum = %g, learning rate = %g \n", name, (float) momentum_gd, (float) lr_gd);
	sprintf(comment, "dx(PREV) - sector = '%s'", shEnumNameGetFromValue("LSECTOR", sector));
	dbg1DArray(dx, n, name, comment);
	for (i = 0; i < n; i++) dx[i] = momentum_gd * dx[i] + lr_gd * jtdmm_prev[i];
	sprintf(comment, "dx(NEXT) - sector = '%s'", shEnumNameGetFromValue("LSECTOR", sector));
	dbg1DArray(dx, n, name, comment);

} else {
	thError("%s: WARNING - discovered (n = %d) - skipping matrix inversion", name, n);
}

#if DEEP_DEBUG_MLE
sprintf(comment, "dx - sector = '%s' - prior to definition", shEnumNameGetFromValue("LSECTOR", sector));
dbg1DArray(dx, n, name, comment);
#endif

#if MLE_CATCH_NAN
for (i = 0; i < n; i++) {
	if (isnan(dx[i]) || isinf(dx[i])) {
		thError("%s: ERROR - (NAN) detected in dx[%d] = %g, F'[%d] = %g", 
			name, i, (float) dx[i], i, (float) jtdmm_prev[i]);
		err_nan++;
	}	
}
if (err_nan > 0) {
	thError("%s: ERROR - (NAN) detected in (%d) occasions", name, err_nan);
	return(SH_GENERIC_ERROR);
}
#endif

int step_count = 0, step_mode = 0;
status = thLmethodGetStepCount(lmethod, &step_count, &step_mode);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (step_count) from (lmethod)", name);
	return(status);
}

LM_STEP_STATUS step_status = UNKNOWN_LM_STEP_STATUS;

int k, kmax;
status = ExtractLMStepNo(lstruct, &k, &kmax); 
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract convergence criteria", name);
	return(status);
}

CHISQFL Nchisq, Ncost;
CONVERGENCE convergence = CONTINUE;
if (k >= kmax) {
	convergence = TOOMANYITER;
	/* there is no h-convergence test for Gradient Descent methods */
} else {
	#if CHECK_MGD_STEPSIZE
	convergence = test_GD_dx_convergence(lmethod, dx, x_prev, NULL, n);
	printf("%s: tested for h-convergence (flag = '%s') \n", name, shEnumNameGetFromValue("CONVERGENCE", convergence));
	#else
	printf("%s: AdaDelta does not have a step size convergence test \n", name);
	convergence = CONTINUE;
	#endif
}
if (convergence == CONTINUE) {
	/* now trying to see if the new step is acceptable */
	status = IncrementX(lstruct, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not increment the X-values for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(status);
	}

	#if (DEEP_DEBUG_MLE)	
	printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
	dbgX(lstruct, name, ASECTOR, "A-sector, after X-increment");
	dbgX(lstruct, name, PSECTOR, "P-sector, after X-increment");
	#endif

	status = thLDumpX(lstruct, XNTOMODEL, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump (pN) values onto model parameters", name);
		return(status);
	}

	ALGORITHM *alg;
	status = thLstructGetAlgByStage(lstruct, MIDDLESTAGE, &alg);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (algorithm) for 'MIDDLESTAGE'", name);
		return(status);
	}
	status = thAlgorithmRun(alg, lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get run (algorithm) for 'MIDDLESTAGE'", name);
		return(status);
	}
	if (sector == PSECTOR) {
		#if DEEP_DEBUG_MLE
		printf("%s: conducting amplitude fit after original dx ['PSECTOR'] \n", name);
		printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
		dbgX(lstruct, name, ASECTOR, "A-sector, before amplitude fit");
		dbgX(lstruct, name, PSECTOR, "P-sector, before amplitude fit");
		#endif
		status = DoAmplitudeFit(lstruct);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not perform the amplitude fit after parameter fitin the loop", name);
			fflush(stderr);
			fflush(stdout);
			return(status);
		}
		#if (DEEP_DEBUG_MLE | 1)
		printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
		/* 
		dbgX(lstruct, name, ASECTOR, "A-sector, after amplitude fit");
		*/
		dbgX(lstruct, name, PSECTOR, "P-sector, after amplitude fit");
		#endif
		status = thLDumpX(lstruct, XNTOMODEL, ASECTOR);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not dump (aN) onto (model)", name);
			return(SH_SUCCESS);
		}
	}
	/* should we do amplitude fit */
	status = thLCalcChisqNAlgRun(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not calculate (chisqN) for (xN)", name);
		return(status);
	}
	status = thMakeJMatricesAlgRun(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not make J-matrices", name);
		return(status);
	}
	status = thLstructGetChisqN(lstruct, &Nchisq, &Ncost);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract derivative information for the new par value", name);
		return(status);
	}  
	if (Ncost < Nchisq) {
		thError("%s: WARNING - found (chisqN = %g) > (costN = %g", name, (float) Nchisq, (float) Ncost);
	}	 
	
	/* problematic definition of dl and rho */
	MLEFL mag_dx = mag_mlefl_vector(dx, NULL, n, 0);
	MLEFL dl = 0.5 * pow((MLEFL) mag_dx, 2.0);
	for (i = 0; i < n; i++) {
		dl -= (MLEFL) jtdmm_prev[i] * (MLEFL) dx[i];
	}
	MLEFL rho = (Ncost - cost) / dl;
	if (cost == Ncost) rho = 0.0;

	#if CONTROL_MGD_STEPSIZE
	printf("%s: (rho = %G, dl = %G, eta_gd_max = %G, || dx || = %G, dX2 = %G, X2 = %20.10G, X2N = %20.10G\n", 
	name, (double) rho, (double) dl, (double) eta_gd_max, (double) mag_dx, (double) (cost - Ncost), (double) cost, (double) Ncost);
	
	MLEFL eta_gd_max_new = eta_gd_max;
	status = update_GD_eta_max(lmethod, eta_gd_max, rho, mag_dx, &eta_gd_max_new);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update (delta) value for (quasi-newtotnian) step", name);
		return(status);
	}
	status = thLmethodPutEta(lmethod, eta_gd_max_new);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put (eta_gd_max = %g) in (lmethod)", name, (float) eta_gd_max_new);
		return(status);
	}
	#else
	printf("%s: (| dX | = %G, dX2 = %G, X2 = %20.10G, X2N = %20.10G\n", 
	name, (double) mag_dx, (double) (cost - Ncost), (double) cost, (double) Ncost);
	printf("%s: AdaDelta does not have any step size hyperparameter \n", name);
	#endif

	#if DO_LFIT_RECORD
	status = do_lfit_record_insert_chisq_in_LM(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not insert (chisq) in (lmethod)", name);
		return(status);
	}
	#endif

	convergence = test_GD_Fprime_convergence(lmethod, NULL, jtdmm_next, n);
	printf("%s: tested for F' convergence (flag = '%s') \n", name, shEnumNameGetFromValue("CONVERGENCE", convergence));
	MLEFL mag_Fprime = mag_mlefl_vector(jtdmm_next, NULL, n, 0);
	MLEFL mag_Fprime_prev = mag_mlefl_vector(jtdmm_prev, NULL, n, 0);
	printf("%s: |F'| = %20.10G, |F'N| = %20.10G, dim = %d \n", name, (double) mag_Fprime_prev, (double) mag_Fprime, n); 

	if (convergence == CONTINUE) {
		#if CONTROL_MGD_STEPSIZE
		if ((Ncost < cost)) {
		/*  || ((Ncost < ((CHISQFL) 1.0 + sqrt((CHISQFL) CHISQFL_EPSILON)) * cost) && (mag_Fprime < mag_Fprime_prev))) {
	*/
			step_status = LM_STEP_ACCEPTED;
		} else if (mag_Fprime > mag_Fprime_prev) {
			step_status = LM_STEP_REJECTED;
		} else {
			step_status = LM_STEP_REJECTED;
		}
	 
		if (mag_Fprime >= mag_Fprime_prev) {
			step_count++;
		} else {
			step_count = 0;
			status = thLmethodPutStepCount(lmethod, 0, step_mode);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not place (step_count) in (lmethod)", name);
				return(status);
			}
		}
	
		#else
		step_status = LM_STEP_ACCEPTED;
		printf("%s: AdaDelta does not have any rejection or acceptance criteria \n", name);
		#endif
		status = UpdateLMConvergence(lstruct, convergence);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not update (convergence flag) to '%s'", name, shEnumNameGetFromValue("CONVERGENCE", convergence));
			return(status);
		}

	} else {
		step_status = LM_STEP_ACCEPTED;
		status = UpdateLMConvergence(lstruct, convergence);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not update (convergence flag) to '%s'", name, shEnumNameGetFromValue("CONVERGENCE", convergence));
			return(status);
		}	
	}
} else {
	step_count = 0;
	step_status = LM_STEP_ACCEPTED;
	status = UpdateLMConvergence(lstruct, convergence);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update (convergence flag) to '%s'", name, shEnumNameGetFromValue("CONVERGENCE", convergence));
		return(status);
	}	
}

if (step_status == LM_STEP_ACCEPTED) {
	/* 
	status = UpdateEAdaDelta(lwork, lmethod);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update (E_$) in (lwork)", name);
		return(status);
	}
	*/
	#if DEEP_DEBUG_MLE
	printf("%s: step accepted, (chisq_old = %20.10G, chisq_new = %20.10G) \n", name, (double) cost, (double) Ncost);
	printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
	dbgX(lstruct, name, ASECTOR, "A-sector, before X-update");
	dbgX(lstruct, name, PSECTOR, "P-sector, before X-update");
	#endif
	status = UpdateXFromXN(lstruct, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update the parameter values for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(status);
	}
	#if DEEP_DEBUG_MLE
	printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
	dbgX(lstruct, name, ASECTOR, "A-sector, after X-update");
	dbgX(lstruct, name, PSECTOR, "P-sector, after X-update");
	#endif
	/* this part seems to have been mistakenly omitted Feb 14, 2012 */  
	status = UpdateChisq(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update the (chisq) value", name);
		return(status);
	}
} else if (step_status == LM_STEP_REJECTED) {
	printf("%s: step rejected, (chisq_old = %20.10G, chisq_new = %20.10G) \n", name, (double) cost, (double) Ncost);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not revert J-matrices due to rejection of step", name);
		return(status);
	}
	status = thLstructSetXupdate(lstruct, OUTDATED, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not set (pupdate) flag in (lstruct) to 'outdated'", name);
		return(status);
	}
} else {
	thError("%s: ERROR - unsupported (step_status = '%s')", name, shEnumNameGetFromValue("LM_STEP_STATUS", step_status));
	return(SH_GENERIC_ERROR);
}
/* should you not do an amplitude fit - one which is reversible to old values */

#if (DO_LFIT_RECORD || 1)
step_mode++;
status = thLmethodPutStatus(lmethod, step_status, step_count, step_mode);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update the acceptance status in (lmethod)", name);
	return(status);
}
#endif

/* update the step number */
k++;
status = UpdateLMStepNo(lstruct, k);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update (step_no) in (lmethod)", name);
	return(status);
}

printf("%s: *** EXITING *** \n", name);
return(SH_SUCCESS);
}

RET_CODE DoGDFitAlgRun(LSTRUCT *lstruct, LSECTOR sector) {
char *name = "DoGDFitAlgRun";
RET_CODE status = SH_SUCCESS;

printf("%s: *** STARTING *** \n", name);
/* Solve B h_qn = -F'(x_old) */

if (sector != APSECTOR) {
	thError("%s: WARNING - working with (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
}

LWORK *lwork = NULL;
status = thLstructGetLwork(lstruct, &lwork);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwork) from (lstruct)", name);
	return(status);
}

LWMASK *lwmask = NULL;
status = thLstructGetLwmask(lstruct, &lwmask);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwmask) from (lstruct)", name);
	return(status);
}

int i, n;
MLEFL *rms_g = NULL, *rms_ginv = NULL, *rms_dx = NULL, *jtdmm_prev = NULL, *jtdmm_next = NULL;
CHISQFL chisq = 0.0, cost = 0.0;
MLEFL *dx, *x_prev;
status = thLstructGetChisq(lstruct, &chisq, &cost);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (chisq) and (cost) from (lstruct)", name);
	return(status);
}
status = thLworkGetGD(lwork, &jtdmm_prev, &jtdmm_next, &rms_g, &rms_ginv, &rms_dx, &dx, &n, sector);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (gradient-descent) matrices", name);
	return(status);
}
status = thLworkGetX(lwork, &x_prev, &n, sector);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (x_prev) for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(status);
}

#if DEEP_DEBUG_MLE
printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
dbgX(lstruct, name, ASECTOR, "A-sector, upon start");
dbgX(lstruct, name, PSECTOR, "P-sector, upon start");
#endif

if (dx == NULL) {
	thError("%s: ERROR - (lwork) not allocated properly - null (dx) array among (GD) arrays", name);
	return(SH_GENERIC_ERROR);
} else if (jtdmm_prev == NULL) {
	thError("%s: ERROR - (lwork) not allocated properly - null (jtdmm_prev) array among (GD) arrays", name);
	return(SH_GENERIC_ERROR);
} else if (rms_g == NULL) {
	thError("%s: ERROR - (lwork) not allocated properly - null (rms_g) array among (GD) arrays", name);
} else if (rms_ginv == NULL) {
	thError("%s: ERROR - (lwork) not allocated properly - null (rms_ginv) array among (GD) arrays", name);
	return(SH_GENERIC_ERROR);
}

#if DEEP_DEBUG_MLE
dbgJtdmm(jtdmm_prev, n, name, "Jtdmm_prev upon start");
dbgJtdmm(jtdmm_next, n, name, "Jtdmm_next upon start");
#endif

#if MLE_CATCH_NAN
int err_nan = 0;
for (i = 0; i < n; i++) {
	if (isnan(jtdmm_prev[i]) || isinf(jtdmm_prev[i])) {
		thError("%s: ERROR - (NAN) detected in Jt[D-M][%d]", name, i);
		err_nan++;
	}
}

if (err_nan > 0) {
	thError("%s: ERROR - (NAN) detected in (%d) occasions", name, err_nan);
	return(SH_GENERIC_ERROR);
}

#endif

LMETHOD *lmethod = NULL;
status = thLstructGetLmethod(lstruct, &lmethod);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmethod) from (lstruct)", name);
	return(status);
}

#if CONTROL_GD_STEPSIZE
MLEFL eta_gd_max;
status = thLmethodGetEta(lmethod, &eta_gd_max);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (delta_qn) from (lmethod)", name);
	return(status);
}
#endif

#if DEEP_DEBUG_MLE
char comment[1000];
sprintf(comment, "dX - sector = '%s' - prior to definition", shEnumNameGetFromValue("LSECTOR", sector));
dbg1DArray(dx, n, name, comment);
sprintf(comment, "RMS[g] - sector = '%s' - prior to definition", shEnumNameGetFromValue("LSECTOR", sector));
dbg1DArray(rms_g, n, name, comment);
#endif

if (n > 0) {
	#if ADAPTIVE_GD
		int mini_batch_size = 0;
		status = thLwmaskGetMiniBatchSize(lwmask, &mini_batch_size);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get active (mini_batch_size) from (lwmask)", name);
			return(status);
		}
		if (mini_batch_size <= 0) {
			thError("%s: ERROR - found an improper active (mini_batch_size = %d) from (lwmask)", name, mini_batch_size);
			return(SH_GENERIC_ERROR);
		}
		#if CONTROL_GD_STEPSIZE
		for (i = 0; i < n; i++) dx[i] = jtdmm_prev[i] * eta_gd_max / rms_g[i] / ((MLEFL) mini_batch_size * (MLEFL) LM_MINIBATCH_CORRECTION_ADADELTA);
		#else	
		for (i = 0; i < n; i++) dx[i] = jtdmm_prev[i] * rms_dx[i] / rms_g[i] / ((MLEFL) mini_batch_size * (MLEFL) LM_MINIBATCH_CORRECTION_ADADELTA);
		#endif
	#else
	for (i = 0; i < n; i++) dx[i] = jtdmm_prev[i] * eta_gd_max; 
	#endif
} else {
	thError("%s: WARNING - discovered (n = %d) - skipping matrix inversion", name, n);
}

#if DEEP_DEBUG_MLE
sprintf(comment, "dx - sector = '%s' - prior to definition", shEnumNameGetFromValue("LSECTOR", sector));
dbg1DArray(dx, n, name, comment);
#endif

#if MLE_CATCH_NAN
for (i = 0; i < n; i++) {
	if (isnan(dx[i]) || isinf(dx[i])) {
		thError("%s: ERROR - (NAN) detected in dx[%d] = %g, rms_g[%d] = %g, rms_dx[%d] = %g", 
			name, i, (float) dx[i], i, (float) rms_g[i], i , (float) rms_dx[i]);
		err_nan++;
	}	
}
if (err_nan > 0) {
	thError("%s: ERROR - (NAN) detected in (%d) occasions", name, err_nan);
	return(SH_GENERIC_ERROR);
}
#endif

int step_count = 0, step_mode = 0;
status = thLmethodGetStepCount(lmethod, &step_count, &step_mode);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (step_count) from (lmethod)", name);
	return(status);
}

LM_STEP_STATUS step_status = UNKNOWN_LM_STEP_STATUS;

int k, kmax;
status = ExtractLMStepNo(lstruct, &k, &kmax); 
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract convergence criteria", name);
	return(status);
}

CHISQFL Nchisq, Ncost;
CONVERGENCE convergence = CONTINUE;
if (k >= kmax) {
	convergence = TOOMANYITER;
	/* there is no h-convergence test for Gradient Descent methods */
} else {
	#if CHECK_GD_STEPSIZE
	convergence = test_GD_dx_convergence(lmethod, lwmask, dx, x_prev, NULL, n);
	printf("%s: tested for h-convergence (flag = '%s') \n", name, shEnumNameGetFromValue("CONVERGENCE", convergence));
	#else
	printf("%s: AdaDelta does not have a step size convergence test \n", name);
	convergence = CONTINUE;
	#endif
}
if (convergence == CONTINUE) {
	#if 0
	MLEFL mag_dx = mag_mlefl_vector(dx, NULL, n, 0);
	if (mag_dx > delta_qn) {
		MLEFL correction_ratio = delta_qn / mag_dx;
		for (i = 0; i < n; i++) dx[i] *= correction_ratio;
		mag_dx = delta_qn;
	}
	#endif
	/* now trying to see if the new step is acceptable */


	status = IncrementX(lstruct, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not increment the X-values for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(status);
	}

	#if (DEEP_DEBUG_MLE | 1)
	printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
	/* dbgX(lstruct, name, ASECTOR, "A-sector, after X-increment"); */
	dbgX(lstruct, name, PSECTOR, "P-sector, after X-increment");
	#endif

	status = thLDumpX(lstruct, XNTOMODEL, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump (pN) values onto model parameters", name);
		return(status);
	}

	ALGORITHM *alg;
	status = thLstructGetAlgByStage(lstruct, MIDDLESTAGE, &alg);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (algorithm) for 'MIDDLESTAGE'", name);
		return(status);
	}
	status = thAlgorithmRun(alg, lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get run (algorithm) for 'MIDDLESTAGE'", name);
		return(status);
	}
	if (sector == PSECTOR) {
		#if DEEP_DEBUG_MLE
		printf("%s: conducting amplitude fit after original dx ['PSECTOR'] \n", name);
		printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
		dbgX(lstruct, name, ASECTOR, "A-sector, before amplitude fit");
		dbgX(lstruct, name, PSECTOR, "P-sector, before amplitude fit");
		#endif
		status = DoAmplitudeFit(lstruct);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not perform the amplitude fit after parameter fitin the loop", name);
			fflush(stderr);
			fflush(stdout);
			return(status);
		}
		#if DEEP_DEBUG_MLE
		printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
		dbgX(lstruct, name, ASECTOR, "A-sector, after amplitude fit");
		dbgX(lstruct, name, PSECTOR, "P-sector, after amplitude fit");
		#endif
		status = thLDumpX(lstruct, XNTOMODEL, ASECTOR);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not dump (aN) onto (model)", name);
			return(SH_SUCCESS);
		}
	}
	/* should we do amplitude fit */
	status = thLCalcChisqNAlgRun(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not calculate (chisqN) for (xN)", name);
		return(status);
	}
	status = thMakeJMatricesAlgRun(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not make J-matrices", name);
		return(status);
	}
	status = thLstructGetChisqN(lstruct, &Nchisq, &Ncost);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract derivative information for the new par value", name);
		return(status);
	}  
	if (Ncost < Nchisq) {
		thError("%s: WARNING - found (chisqN = %g) > (costN = %g", name, (float) Nchisq, (float) Ncost);
	}	 
	
	/* problematic definition of dl and rho */
	MLEFL mag_dx = mag_mlefl_vector(dx, NULL, n, 0);
	MLEFL dl = 0.5 * pow((MLEFL) mag_dx, 2.0);
	for (i = 0; i < n; i++) {
		dl -= (MLEFL) jtdmm_prev[i] * (MLEFL) dx[i];
	}
	MLEFL rho = (Ncost - cost) / dl;
	if (cost == Ncost) rho = 0.0;

	#if CONTROL_GD_STEPSIZE
	printf("%s: (rho = %G, dl = %G, eta_gd_max = %G, || dx || = %G, dX2 = %G, X2 = %20.10G, X2N = %20.10G\n", 
	name, (double) rho, (double) dl, (double) eta_gd_max, (double) mag_dx, (double) (cost - Ncost), (double) cost, (double) Ncost);
	
	MLEFL eta_gd_max_new = eta_gd_max;
	status = update_GD_eta_max(lmethod, eta_gd_max, rho, mag_dx, cost, Ncost, &eta_gd_max_new);

	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update (delta) value for (quasi-newtotnian) step", name);
		return(status);
	}
	status = thLmethodPutEta(lmethod, eta_gd_max_new);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put (eta_gd_max = %g) in (lmethod)", name, (float) eta_gd_max_new);
		return(status);
	}
	#else
	printf("%s: (| dX | = %G, dX2 = %G, X2 = %20.10G, X2N = %20.10G\n", 
	name, (double) mag_dx, (double) (cost - Ncost), (double) cost, (double) Ncost);
	printf("%s: AdaDelta does not have any step size hyperparameter \n", name);
	#endif

	#if DO_LFIT_RECORD
	status = do_lfit_record_insert_chisq_in_LM(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not insert (chisq) in (lmethod)", name);
		return(status);
	}
	#endif

	convergence = test_GD_Fprime_convergence(lmethod, NULL, jtdmm_next, n);
	printf("%s: tested for F' convergence (flag = '%s') \n", name, shEnumNameGetFromValue("CONVERGENCE", convergence));
	MLEFL mag_Fprime = mag_mlefl_vector(jtdmm_next, NULL, n, 0);
	MLEFL mag_Fprime_prev = mag_mlefl_vector(jtdmm_prev, NULL, n, 0);
	printf("%s: |F'| = %20.10G, |F'N| = %20.10G, dim = %d \n", name, (double) mag_Fprime_prev, (double) mag_Fprime, n); 
	
	#if CHECK_GD_STEPSIZE
	if (convergence == CONTINUE) {
		convergence = test_GD_dx_convergence(lmethod, lwmask, dx, x_prev, NULL, n);
		printf("%s: tested for h-convergence (flag = '%s') \n", name, shEnumNameGetFromValue("CONVERGENCE", convergence));
	}
	#else
	printf("%s: AdaDelta does not have a step size convergence test \n", name);
	#endif
	if (convergence == CONTINUE) {
		#if CONTROL_GD_STEPSIZE
		if ((Ncost < cost)) {
		/*  || ((Ncost < ((CHISQFL) 1.0 + sqrt((CHISQFL) CHISQFL_EPSILON)) * cost) && (mag_Fprime < mag_Fprime_prev))) {
	*/
			step_status = LM_STEP_ACCEPTED;
		} else if (mag_Fprime > mag_Fprime_prev) {
			step_status = LM_STEP_REJECTED;
		} else {
			step_status = LM_STEP_REJECTED;
		}
	 
		if (mag_Fprime >= mag_Fprime_prev) {
			step_count++;
		} else {
			step_count = 0;
			status = thLmethodPutStepCount(lmethod, 0, step_mode);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not place (step_count) in (lmethod)", name);
				return(status);
			}
		}
	
		#else
		step_status = LM_STEP_ACCEPTED;
		printf("%s: AdaDelta does not have any rejection or acceptance criteria \n", name);
		#endif
		status = UpdateLMConvergence(lstruct, convergence);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not update (convergence flag) to '%s'", name, shEnumNameGetFromValue("CONVERGENCE", convergence));
			return(status);
		}

	} else {
		step_status = LM_STEP_ACCEPTED;
		status = UpdateLMConvergence(lstruct, convergence);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not update (convergence flag) to '%s'", name, shEnumNameGetFromValue("CONVERGENCE", convergence));
			return(status);
		}	
	}
} else {
	step_count = 0;
	step_status = LM_STEP_ACCEPTED;
	status = UpdateLMConvergence(lstruct, convergence);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update (convergence flag) to '%s'", name, shEnumNameGetFromValue("CONVERGENCE", convergence));
		return(status);
	}	
}

if (step_status == LM_STEP_ACCEPTED) {
	/* 
	status = UpdateEAdaDelta(lwork, lmethod);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update (E_$) in (lwork)", name);
		return(status);
	}
	*/
	#if DEEP_DEBUG_MLE
	printf("%s: step accepted, (chisq_old = %20.10G, chisq_new = %20.10G) \n", name, (double) cost, (double) Ncost);
	printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
	dbgX(lstruct, name, ASECTOR, "A-sector, before X-update");
	dbgX(lstruct, name, PSECTOR, "P-sector, before X-update");
	#endif
	status = UpdateXFromXN(lstruct, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update the parameter values for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(status);
	}
	#if DEEP_DEBUG_MLE
	printf("%s: passed (sector = '%s') \n", name, shEnumNameGetFromValue("LSECTOR", sector));
	dbgX(lstruct, name, ASECTOR, "A-sector, after X-update");
	dbgX(lstruct, name, PSECTOR, "P-sector, after X-update");
	#endif
	/* this part seems to have been mistakenly omitted Feb 14, 2012 */  
	status = UpdateChisq(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update the (chisq) value", name);
		return(status);
	}
} else if (step_status == LM_STEP_REJECTED) {
	printf("%s: step rejected, (chisq_old = %20.10G, chisq_new = %20.10G) \n", name, (double) cost, (double) Ncost);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not revert J-matrices due to rejection of step", name);
		return(status);
	}
	status = thLstructSetXupdate(lstruct, OUTDATED, APSECTOR);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not set (pupdate) flag in (lstruct) to 'outdated'", name);
		return(status);
	}
} else {
	thError("%s: ERROR - unsupported (step_status = '%s')", name, shEnumNameGetFromValue("LM_STEP_STATUS", step_status));
	return(SH_GENERIC_ERROR);
}
/* should you not do an amplitude fit - one which is reversible to old values */

#if (DO_LFIT_RECORD || 1)
step_mode++;
status = thLmethodPutStatus(lmethod, step_status, step_count, step_mode);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update the acceptance status in (lmethod)", name);
	return(status);
}
#endif

/* update the step number */
k++;
status = UpdateLMStepNo(lstruct, k);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update (step_no) in (lmethod)", name);
	return(status);
}

printf("%s: *** EXITING *** \n", name);
return(SH_SUCCESS);
}

RET_CODE DoQNFitAlgRun(LSTRUCT *lstruct, LSECTOR sector) {
char *name = "DoQNFitAlgRun";
RET_CODE status = SH_SUCCESS;

/* Solve B h_qn = -F'(x_old) */

LWORK *lwork = NULL;
status = thLstructGetLwork(lstruct, &lwork);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwork) from (lstruct)", name);
	return(status);
}

int i, j, n;
MLEFL **Bk_prev, **Bk_next, **BBk_prev, **BBk_next, *jtdmm_prev, *jtdmm_next;
CHISQFL chisq = 0.0, cost = 0.0;
MLEFL *dx, *x_prev, **Hk_prev = NULL, **Hk_next = NULL, **HHk_prev = NULL, **HHk_next = NULL;

status = thLstructGetChisq(lstruct, &chisq, &cost);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (chisq) and (cost) from (lstruct)", name);
	return(status);
}
status = thLworkGetQN(lwork, 
	&Bk_prev, &Bk_next, &BBk_prev, &BBk_next, 
	&jtdmm_prev, &jtdmm_next, 
	&Hk_prev, &Hk_next, &HHk_prev, &HHk_next, 
	&dx, &n, sector);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (quasi-newtonian) matrices", name);
	return(status);
}
status = thLworkGetX(lwork, &x_prev, &n, sector);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (x_prev) for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(status);
}

if (dx == NULL) {
	thError("%s: ERROR - (lwork) not allocated properly - null (dx) array among (QN) arrays", name);
	return(SH_GENERIC_ERROR);
} else if (jtdmm_prev == NULL) {
	thError("%s: ERROR - (lwork) not allocated properly - null (jtdmm_prev) array among (QN) arrays", name);
	return(SH_GENERIC_ERROR);
} else if (Bk_prev == NULL) {
	thError("%s: ERROR - (lwork) not allocated properly - null (Bk_prev) array among (QN) arrays", name);
} else if (Hk_prev == NULL) {
	thError("%s: ERROR - (lwork) not allocated properly - null (Hk_prev) array among (QN) arrays", name);
	return(SH_GENERIC_ERROR);
} else if (BBk_prev == NULL) {
	thError("%s: ERROR - (lwork) not allocated properly - null (BBk_prev) array among (QN) arrays", name);
} else if (HHk_prev == NULL) {
	thError("%s: ERROR - (lwork) not allocated properly - null (HHk_prev) array among (QN) arrays", name);
	return(SH_GENERIC_ERROR);
}

for (i = 0; i < n; i++) {
	dx[i] = (MLEFL) jtdmm_prev[i];
}
for (i = 0; i < n; i++) {
	MLEFL *Bk_i = Bk_prev[i];
	MLEFL *Hk_i = Hk_prev[i];
	for (j = 0; j < n; j++) {
		Hk_i[j] = (MLEFL) Bk_i[j];
	}
}
for (i = 0; i < n; i++) {
	MLEFL *BBk_i = BBk_prev[i];
	MLEFL *HHk_i = HHk_prev[i];
	for (j = 0; j < n; j++) {
		HHk_i[j] = (MLEFL) BBk_i[j];
	}
}


#if MLE_CATCH_NAN
int err_nan = 0;
for (i = 0; i < n; i++) {
	if (isnan(dx[i]) || isinf(dx[i])) {
		thError("%s: ERROR - (NAN) detected in dx[%d]", name, i);
		err_nan++;
	}
	if (isnan(jtdmm_prev[i]) || isinf(jtdmm_prev[i])) {
		thError("%s: ERROR - (NAN) detected in Jt[D-M][%d]", name, i);
		err_nan++;
	}
}

if (err_nan > 0) {
	thError("%s: ERROR - (NAN) detected in (%d) occasions", name, err_nan);
	return(SH_GENERIC_ERROR);
}

#endif

if (n > 0) {
	int singular = 0;
	#if 0
	status = thMatrixInvert(Hk_prev, dx, n); 
	#else
	status = LMatrixInvert(HHk_prev, jtdmm_prev, dx, n, &singular);
	#endif
	if (singular) {
		thError("%s: WARNING - possibly degeneracy discovered while taking derivative-based steps", name);
		status = UpdateLMConvergence(lstruct, SINGULAR_CONVERGENCE);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not place convergence flag into (lstruct)", name);
			return(status);
		}
		/* 
		return(SH_SUCCESS);
		*/
	} else if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not inverse matrix to obtain Hessian matrix", name);
		return(status);
	}
	/* 
	for (i = 0; i < n; i++) dx[i] *= (MLEFL) -1.0;
	*/
} else {
	thError("%s: WARNING - discovered (n = %d) - skipping matrix inversion", name, n);
}

#if MLE_CATCH_NAN
for (i = 0; i < n; i++) {
	if (isnan(dx[i]) || isinf(dx[i])) {
		thError("%s: ERROR - (NAN) detected in dx[%d]", name, i);
		err_nan++;
	}
	MLEFL *Hk_prev_i = Hk_prev[i];
	for (j = 0; j < n; j++) {
		if (isnan(Hk_prev_i[j]) || isinf(Hk_prev_i[j])) {
			thError("%s: ERROR - (NAN) detected in Hessian[%d][%d]", name, i, j);
			err_nan++;
		}
	}
	MLEFL *HHk_prev_i = HHk_prev[i];
	for (j = 0; j < n; j++) {
		if (isnan(HHk_prev_i[j]) || isinf(HHk_prev_i[j])) {
			thError("%s: ERROR - (NAN) detected in HHessian[%d][%d]", name, i, j);
			err_nan++;
		}
	}

}
if (err_nan > 0) {
	thError("%s: ERROR - (NAN) detected in (%d) occasions", name, err_nan);
	return(SH_GENERIC_ERROR);
}
#endif

LMETHOD *lmethod = NULL;
status = thLstructGetLmethod(lstruct, &lmethod);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmethod) from (lstruct)", name);
	return(status);
}

int k, kmax;
status = ExtractLMStepNo(lstruct, &k, &kmax); 
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract convergence criteria", name);
	return(status);
}
if (k <= -1) {
	printf("%s: DEBUG - initiating (delta_qn) - k = %d", name, k);
	status = initiate_QN_delta(lstruct, lmethod, lwork, wsector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate (delta_qn)", name);
		return(status);
	}
}

LM_STEP_STATUS step_status = UNKNOWN_LM_STEP_STATUS;
CHISQFL Nchisq, Ncost;
CONVERGENCE convergence = CONTINUE;
LMCRIT criteria = UNKNOWN_LMCRIT;
MLEFL delta_qn;
int step_count = 0, step_mode = 0;

if (k >= kmax) {
	convergence = TOOMANYITER;
	/* there is no h-convergence test for Gradient Descent methods */
} else {
	status = thLmethodGetDeltaQn(lmethod, &delta_qn);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (delta_qn) from (lmethod)", name);
		return(status);
	}
	status = thLmethodGetStepCount(lmethod, &step_count, &step_mode);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (step_count) from (lmethod)", name);
		return(status);
	}
	convergence = test_QN_dx_convergence(lmethod, dx, x_prev, BBk_prev, n);
	printf("%s: tested for h-convergence (flag = '%s') \n", name, shEnumNameGetFromValue("CONVERGENCE", convergence));
}
if (convergence == CONTINUE) {
	MLEFL mag_dx = mag_mlefl_vector(dx, BBk_prev, n, 0);
	if (mag_dx > delta_qn) {
		MLEFL correction_ratio = delta_qn / mag_dx;
		for (i = 0; i < n; i++) dx[i] *= correction_ratio;
		mag_dx = delta_qn;
	}
	
	/* now trying to see if the new step is acceptable */


	status = IncrementX(lstruct, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not increment the X-values for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(status);
	}

	status = thLDumpX(lstruct, XNTOMODEL, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump (pN) values onto model parameters", name);
		return(status);
	}

	ALGORITHM *alg;
	status = thLstructGetAlgByStage(lstruct, MIDDLESTAGE, &alg);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (algorithm) for 'MIDDLESTAGE'", name);
		return(status);
	}
	status = thAlgorithmRun(alg, lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get run (algorithm) for 'MIDDLESTAGE'", name);
		return(status);
	}
	/* we should do amplitude fit if proper PSECTOR step is taken */
	if (sector == PSECTOR) {
		/* trying to avoid singularity in amplitude fit */
		status = DoAmplitudeFit(lstruct);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not conduct amplitude fit using new parameters", name);
			fflush(stderr);
			fflush(stdout);
			return(status);
		}
		CONVERGENCE flag = UNKNOWN_CONVERGENCE;
		status = thLstructGetConvergence(lstruct, NULL, &flag, NULL);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get convergence flag from (lstruct)", name);
			return(status);
		}
		if (flag & SINGULAR_CONVERGENCE) {
			thError("%s: WARNING - convergence flag '%s' raised after amplitude fit", name, shEnumNameGetFromValue("CONVERGENCE", flag));
			/* 
			singular2 = 1;
			*/
			flag = CONTINUE;
			status = UpdateLMConvergence(lstruct, flag);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not update (convergence flag) to '%s'", name, shEnumNameGetFromValue("CONVERGENCE", flag));
				return(status);
			}
		} else {
			status = thLstructAssignDa(lstruct);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not assign (da)", name);
				return(status);
			}
		}
	}

	status = thLCalcChisqNAlgRun(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not calculate (chisqN) for (xN)", name);
		return(status);
	}
	status = thMakeJMatricesAlgRun(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not make J-matrices", name);
		return(status);
	}
	status = thLstructGetChisqN(lstruct, &Nchisq, &Ncost);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract derivative information for the new par value", name);
		return(status);
	}  
	if (Ncost < Nchisq) {
		thError("%s: WARNING - found (chisqN = %g) > (costN = %g", name, (float) Nchisq, (float) Ncost);
	}	 
	
	/* problematic definition of dl and rho */
	MLEFL mag_dx_Bk = mag_mlefl_vector(dx, BBk_prev, n, 0);
	MLEFL dl = 0.5 * pow((MLEFL) mag_dx_Bk, 2.0);
	for (i = 0; i < n; i++) {
		dl -= (MLEFL) jtdmm_prev[i] * (MLEFL) dx[i];
	}
	MLEFL rho = (Ncost - cost) / dl;
	if (cost == Ncost) rho = 0.0;

	printf("%s: (rho = %G, dl = %G, delta_qn = %G, | dX | = %G, || dX ||_Bk = %G, dX2 = %G, X2 = %20.10G, X2N = %20.10G\n", 
	name, (double) rho, (double) dl, (double) delta_qn, (double) mag_dx, (double) mag_dx_Bk, (double) (cost - Ncost), (double) cost, (double) Ncost);

	status = update_QN_delta(lmethod, delta_qn, rho, mag_dx, &delta_qn);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update (delta) value for (quasi-newtotnian) step", name);
		return(status);
	}
	status = thLmethodPutDeltaQn(lmethod, delta_qn);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put (delta_qn = %g) in (lmethod)", name, (float) delta_qn);
		return(status);
	}
	/* 
	status = IncrementX(lstruct, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update the parameter values by the designated amount", name);
		return(status);
	}
	*/
	#if DO_LFIT_RECORD
	status = do_lfit_record_insert_chisq_in_LM(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not insert (chisq) in (lmethod)", name);
		return(status);
	}
	#endif

	convergence = test_QN_Fprime_convergence(lmethod, jtdmm_next, n);
	printf("%s: tested for F' convergence (flag = '%s') \n", name, shEnumNameGetFromValue("CONVERGENCE", convergence));
	MLEFL mag_Fprime = mag_mlefl_vector(jtdmm_next, NULL, n, 1);
	MLEFL mag_Fprime_prev = mag_mlefl_vector(jtdmm_prev, NULL, n, 1);
	printf("%s: |F'| = %20.10G, |F'N| = %20.10G \n", name, (double) mag_Fprime_prev, (double) mag_Fprime); 

	if (convergence == CONTINUE) {
		if (Ncost < cost) {
			step_status = LM_STEP_ACCEPTED;
			criteria = LM_COST_DECREASED_ACC;
		} else if (((Ncost - cost) < (sqrt((CHISQFL) CHISQFL_EPSILON)) * cost) && (mag_Fprime < mag_Fprime_prev)) {
			step_status = LM_STEP_ACCEPTED;
			criteria = LM_DERIV_IMPROVED_ACC;
		} else if (mag_Fprime > mag_Fprime_prev) {
			step_status = LM_STEP_REJECTED;
			criteria = LM_DERIV_INCREASED_REJ;
		} else {
			step_status = LM_STEP_REJECTED;
			criteria = LM_COST_INCREASED_REJ;
		}
		if (mag_Fprime >= mag_Fprime_prev) {
			step_count++;
		} else {
			step_count = 0;
			status = thLmethodPutStepCount(lmethod, 0, step_mode);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not place (step_count) in (lmethod)", name);
				return(status);
			}
		}
		status = UpdateLMConvergence(lstruct, convergence);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not update (convergence flag) to '%s'", name, shEnumNameGetFromValue("CONVERGENCE", convergence));
			return(status);
		}

	} else {
		step_status = LM_STEP_ACCEPTED;
		criteria = LM_CONVERGED_ACC;
		status = UpdateLMConvergence(lstruct, convergence);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not update (convergence flag) to '%s'", name, shEnumNameGetFromValue("CONVERGENCE", convergence));
			return(status);
		}	
	}
} else {
	step_count = 0;
	step_status = LM_STEP_ACCEPTED;
	criteria = LM_CONVERGED_ACC;
	status = UpdateLMConvergence(lstruct, convergence);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update (convergence flag) to '%s'", name, shEnumNameGetFromValue("CONVERGENCE", convergence));
		return(status);
	}	
}

if (step_status == LM_STEP_ACCEPTED) {
	printf("%s: step accepted, (chisq_old = %20.10G, chisq_new = %20.10G), criteria = '%s' \n", name, (double) cost, (double) Ncost, shEnumNameGetFromValue("LMCRIT", criteria));
	status = UpdateXFromXN(lstruct, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update the parameter values for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(status);
	}
	/* this part seems to have been mistakenly omitted Feb 14, 2012 */  
	status = UpdateChisq(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update the (chisq) value", name);
		return(status);
	}
	/* 	
	status = UpdateQnPrevFromQn(lstruct, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update (QN) matrices", name);
		return(status);
	}
	*/
} else if (step_status == LM_STEP_REJECTED) {
	printf("%s: step rejected, (chisq_old = %20.10G, chisq_new = %20.10G) \n", name, (double) cost, (double) Ncost);
	/* 
	status = thLworkRevertJqn(lwork);
	*/
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not revert J-matrices due to rejection of step", name);
		return(status);
	}
	status = thLstructSetXupdate(lstruct, OUTDATED, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not set (pupdate) flag in (lstruct) to 'outdated'", name);
		return(status);
	}
} else {
	thError("%s: ERROR - unsupported (step_status = '%s')", name, shEnumNameGetFromValue("LM_STEP_STATUS", step_status));
	return(SH_GENERIC_ERROR);
}

/* update the step number */
k++;
status = UpdateLMStepNo(lstruct, k);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update (step_no) in (lmethod)", name);
	return(status);
}

/* should you not do an amplitude fit - one which is reversible to old values */
#if (DO_LFIT_RECORD || 1)
step_mode++;
status = thLmethodPutStatus(lmethod, step_status, step_count, step_mode);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update the acceptance status in (lmethod)", name);
	return(status);
}
#endif

#if DEBUG_MLE
printf("%s: dX2 = %8.4G, X2 = %20.10G, X2_N = %20.10G \n", name, (double) (chisq - Nchisq), (double) chisq, (double) Nchisq);
printf("%s: dpcost= %8.4G, pcost = %20.10G, pcost_N = %20.10G \n", name, (double) ((cost - Ncost) - (chisq - Nchisq)), (double) (cost - chisq), (double) (Ncost - Nchisq));
#endif



return(SH_SUCCESS);
}

RET_CODE DoLMFitAlgRun(LSTRUCT *lstruct, LSECTOR sector) {
char *name = "DoLMFitAlgRun";
#if DEBUG_MEMORY_VERBOSE
printf("%s: memory statistics before conducting (LM-fit)", name);
shMemStatsPrint();
fflush(stdout);
fflush(stderr);
#endif


RET_CODE status = SH_SUCCESS;

/* LM method is executed as follows 

   L = < m - d | W | m - d > 
   J = DL / Dp

   The new step is taken by Dp such that 

   (JT J + \lambda diag(JT J)) Dp = JT (d - m)

   Where the Jacobian J and the related square matrices are defined as follows 

   J = D | m > / Dp

   J_i = D | m > / Dp_i
   (JT J)_i_j       =  D < m | / Dp_i   W     D | m > / Dp_j
   (JT (d - m))_i =  D < m | / Dp_i   W   | d - m >

*/
#if THCLOCK
clock_t clk1, clk2;
clk1 = clock();
#endif

MLEFL lambda, beta, gamma, rhobar, nu;
int q, step_count = 0, step_mode = 0;
status = ExtractLMDamping(lstruct, &lambda, &beta, &gamma, &rhobar, &nu, &q, &step_count, &step_mode);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract damping parameters", name);
	return(status);
}   

MLEFL **jtj, *jtdmm;
MLEFL **jtjpdiag = NULL, **covT = NULL;
CHISQFL chisq = 0.0, cost = 0.0;
MLEFL *dX = NULL, *dxT = NULL;
int nx;
status = ExtractLMCurrentMatrices(lstruct, sector, &jtj, &jtjpdiag, &covT, &jtdmm, &dX, &dxT, &chisq, &cost, &nx);

if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract the Jacobian for Levenberg-Marquardt", name);
	return(status);
}   

LWORK *lwork = NULL;
status = thLstructGetLwork(lstruct, &lwork);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwork) from (lstruct)", name);
	return(status);
}
MAT *wMatA = NULL, *wMatQ = NULL;
VEC **wVecs = NULL;
status = thLworkGetWMatVec(lwork, sector, &wMatA, &wMatQ, &wVecs);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get working Mat, Vec for '%s'", 
	name, shEnumNameGetFromValue("LSECTOR", sector));
	return(status);
}	

int i, j;
for (i = 0; i < nx; i++) {
	for (j = 0; j < nx; j++) {
		covT[i][j] = jtjpdiag[i][j];
	}
}
memcpy(dxT, dX, nx * sizeof(MLEFL));

MLEFL onepluslambda;
onepluslambda = (MLEFL) 1.0 + (MLEFL) lambda;


#if (ABS_JTJ | 1)
status = LMatrixAbsoluteValueSVD(jtj, jtjpdiag, nx, wMatA, wMatQ, wVecs);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calculate abs(JtJ)", name);
	return(status);
} 
#else
for (i = 0; i < nx; i++) {
	memcpy(jtjpdiag[i], jtj[i], nx * sizeof(MLEFL));
	}
#endif



for (i = 0; i < nx; i++) {
	jtjpdiag[i][i] *= onepluslambda;
	dX[i] = (MLEFL) jtdmm[i];
}

#if MLE_CATCH_NAN
int err_nan = 0;
for (i = 0; i < nx; i++) {
	for (j = 0; j < nx; j++) {
		if (isnan(jtj[i][j]) || isinf(jtj[i][j])) {
			thError("%s: ERROR - (NAN) detected in JtJ[%d][%d]", name, i, j);
			err_nan++;
		}
		if (isnan(jtjpdiag[i][j]) || isinf(jtjpdiag[i][j])) {
			thError("%s: ERROR - (NAN) detected in JtJ(1+LaMbDa)[%d][%d]", name, i, j);
			err_nan++;
		}

	}
	}
for (i = 0; i < nx; i++) {
	if (isnan(dX[i]) || isinf(dX[i])) {
		thError("%s: ERROR - (NAN) detected in dX[%d], sector = '%s'", name, i, shEnumNameGetFromValue("LSECTOR", sector));
		err_nan++;
	}
	if (isnan(jtdmm[i]) || isinf(jtdmm[i])) {
		thError("%s: ERROR - (NAN) detected in Jt[D-M][%d], sector = '%s'", name, i, shEnumNameGetFromValue("LSECTOR", sector));
		err_nan++;
	}
}

if (isinf(onepluslambda) || isnan(onepluslambda)) {
	thError("%s: ERROR - (NAN) detected at 1 + lambda", name);
	err_nan++;
}

if (err_nan > 0) {
	thError("%s: ERROR - (NAN) detected in (%d) occasions", name, err_nan);
	return(SH_GENERIC_ERROR);
}

#endif

int singular = 0;
if (nx > 0) {
	#if 1
	status = LMatrixInvert(jtjpdiag, jtdmm, dX, nx, &singular);
	if (singular) {
		/* in case of singularity redefine JtJ (1 + lambda) and pass it to SVD */
		#if DEBUG_MLE_SINGULAR
		thError("%s: WARNING - singularity in taking derivative-based steps (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		#endif

		#if PSEUDO_INVERSE_FOR_SINGULAR

		#if (ABS_JTJ | 1)
		status = LMatrixAbsoluteValueSVD(jtj, jtjpdiag, nx, wMatA, wMatQ, wVecs);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not calculate abs(JtJ)", name);
			return(status);
		} 
		#else
		for (i = 0; i < nx; i++) {
			memcpy(jtjpdiag[i], jtj[i], nx * sizeof(MLEFL));
			}
		#endif

		for (i = 0; i < nx; i++) {
			jtjpdiag[i][i] *= onepluslambda;
			dX[i] = (MLEFL) jtdmm[i];
		}

		/* SVD is a pseudo-inversion method that is meant to avoid degeneracy problem in the matrix */
		status = LMatrixSolveSVD(jtjpdiag, jtdmm, dX, nx, wMatA, wMatQ, wVecs);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not use SVD to calculate derivate based step", name);
			return(status);
		}
		#else
		for (i = 0; i < nx; i++) {
			for (j = 0; j < nx; j++) {
				jtjpdiag[i][j] = covT[i][j];
			}
		}
		memcpy(dX, dxT, nx * sizeof(MLEFL));
		memset(dX, '\0', nx * sizeof(MLEFL));
	
		status = UpdateLMConvergence(lstruct, SINGULAR_CONVERGENCE);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not place convergence flag into (lstruct)", name);
			return(status);
		}
		#endif
	} else if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not invert Jtj(1 + lambda) matrix", name);
		return(status);
	}
	#else
	/* SVD is a pseudo-inversion method that is meant to avoid degeneracy problem in the matrix */
	status = LMatrixSolveSVD(jtjpdiag, jtdmm, dX, nx, wMatA, wMatQ, wVecs);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not use SVD to calculate derivate based step", name);
		return(status);
	}
	#endif
} else {
	thError("%s: WARNING - discovered (nx = %d) for (sector = '%s') - skipping matrix inversion", name, nx, shEnumNameGetFromValue("LSECTOR", sector));
}

#if MLE_CATCH_NAN
for (i = 0; i < nx; i++) {
	if (isnan(dX[i]) || isinf(dX[i])) {
		thError("%s: ERROR - (NAN) detected in dx[%d]", name, i);
		err_nan++;
	}
	MLEFL *jtjpdiag_i = jtjpdiag[i];
	for (j = 0; j < nx; j++) {
		if (isnan(jtjpdiag_i[j]) || isinf(jtjpdiag_i[j])) {
			thError("%s: ERROR - (NAN) detected in inverse(JtJ + lambda DtD)[%d][%d]", name, i, j);
			err_nan++;
		}
	}
}
if (err_nan > 0) {
	thError("%s: ERROR - (NAN) detected in (%d) occasions", name, err_nan);
	return(SH_GENERIC_ERROR);
}
#endif

#if MLE_CONDUCT_HIGHER_ORDER

/* 
Improvements to the Levenberg-Marquardt algorithm for nonlinear least-squares minimization
Mark K. Transtruma, James P. Sethnaa
aLaboratory of Atomic and Solid State Physics, Cornell University, Ithaca, New York 14853, USA
*/

MLEFL *dp2 = NULL;
status = ExtractDX2(lstruct, PSECTOR, &dp2, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get 'dp2' from (lstruct)", name);
	return(status);
}
if (dp2 == NULL && npar > 0) {
	thError("%s: ERROR - obtained (dp2 = null) while (npar = %d)", name, npar);
	return(SH_GENERIC_ERROR);
}
MLEFL *JtDDm = NULL;
status = thLExtractJtDDm(lstruct, PSECTOR, &JtDDm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract 'JtDDm' matrix", name);
	return(status);
}
if (JtDDm == NULL && npar > 0) {
	thError("%s: ERROR - obtained (JtDDm = null) while (npar = %d)", name, npar);
	return(SH_GENERIC_ERROR);
}
for (i = 0; i < npar; i++) {
	MLEFL *jtjpdiag_i = jtjpdiag[i];
	MLEFL dot_product = 0.0;
	for (j = 0; j < npar; j++) {
		dot_product += jtjpdiag_i[j] * ((MLEFL) JtDDm[j]);
	}
	dp2[i] = -0.5 * dot_product;
}

MLEFL mag_dp1 = 0.0, mag_dp2 = 0.0;
for (i = 0; i < npar; i++) {
	mag_dp1 += pow(dp[i], 2.0);
	mag_dp2 += pow(dp2[i], 2.0);
}
mag_dp1 = pow(mag_dp1, 0.5);
mag_dp2 = pow(mag_dp2, 0.5);

#if MLE_CATCH_NAN
for (i = 0; i < npar; i++) {
	if (isnan(dp2[i]) || isinf(dp2[i])) {
		thError("%s: ERROR - (NAN) detected in dp2[%d]", name, i);
		err_nan++;
	}
}
if (isnan(mag_dp1) || isinf(mag_dp1)) {
	thError("%s: ERROR - (NAN) detected at |dp1|", name);
	err_nan++;
}
if (isnan(mag_dp2) || isinf(mag_dp2)) {
	thError("%s: ERROR - (NAN) detected at |dp2|", name);
	err_nan++;
}
if (err_nan > 0) {
	thError("%s: ERROR - (NAN) detected in (%d) occasions", name, err_nan);
	return(SH_GENERIC_ERROR);
}

#endif

if ((mag_dp1 > 0.0) && ((2.0 * mag_dp2 / mag_dp1) < (MLEFL) MLE_HIGHER_ORDER_ALPHA)) {
	#if DEBUG_MLE_HIGHER_ORDER
	printf("%s: DD-step accepted: |dp1| = %g, |dp2| = %g, |dp2| / |dp1| = %g, alpha = %g \n", name, 
	(float) mag_dp1, (float) mag_dp2, (float) (mag_dp2 / mag_dp1), (float) MLE_HIGHER_ORDER_ALPHA);
	#endif
	for (i = 0; i < npar; i++) {
		dp[i] += dp2[i];
	}
} else {
	#if DEBUG_MLE_HIGHER_ORDER
	printf("%s: DD-step rejected: |dp1| = %g, |dp2| = %g, |dp2| / |dp1| = %g, alpha = %g \n", name, 
	(float) mag_dp1, (float) mag_dp2, (float) (mag_dp2 / mag_dp1), (float) MLE_HIGHER_ORDER_ALPHA);
	#endif
}

#endif


/*
Choice of damping parameter

The choice of the damping parameter is based on the following algorithm: 

IMM DEPARTMENT OF MATHEMATICAL MODELLING	J. No. MARQ
8.4.1999 Technical University of Denmark	HBN/ms
DK-2800 Lyngby – Denmark
DAMPING PARAMETER IN MARQUARDT’S METHOD
Hans Bruun Nielsen
TECHNICAL REPORT IMM-REP-1999-05

*/

/* the following updates the par values but doesn't set any flags */
 
/* 
note that the dp used is indeed already in the work space so there
is absolutely no need to supply it explicitly, this incrementing is done
in new space  
*/
status = IncrementX(lstruct, sector);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update the parameter values by the designated amount", name);
	return(status);
}

/* now trying to see if the new step is acceptable */

CHISQFL Nchisq, Ncost;
MLEFL dl, rho = -1.0;

/* the following should be replaced with AlgorithmRun 
   you should be careful not to invoke J-creation functions before the new value is 
   confirmed by the way of ChiSq comparison */

/* at this point 
	MakeImageAndMatrices should only create model images (not derivatives) 
	for models which have a changing paramater (non-linear) list.
	The derivatives are calculated only after we make sure that the new step
	is an acceptable one 
status = MakeImageAndMatrices(lstruct, IMAGES);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make matrices and images for the new parameter", name);
	return(status);
}
status = thMakeM(lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not create (m) and (d-m)", name);
	return(status);
}
*/
status = thLDumpX(lstruct, XNTOMODEL, sector);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump (pN) values onto model parameters", name);
	return(status);
}

ALGORITHM *alg;
status = thLstructGetAlgByStage(lstruct, MIDDLESTAGE, &alg);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (algorithm) for 'MIDDLESTAGE'", name);
	return(status);
}
status = thAlgorithmRun(alg, lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run (algorithm) for 'MIDDLESTAGE'", name);
	return(status);
}

int singular2 = 0;
/* should you not do an amplitude fit - one which is reversible to old values */
if (sector == PSECTOR || (LM_CONDUCT_AMPFIT && (float) phRandomUniformdev() < (float) LM_AMPFIT_CHANCE)) {
	/* trying to avoid singularity in amplitude fit */
	status = DoAmplitudeFit(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not conduct amplitude fit using new parameters", name);
		fflush(stderr);
		fflush(stdout);
		return(status);
	}
	CONVERGENCE flag = UNKNOWN_CONVERGENCE;
	status = thLstructGetConvergence(lstruct, NULL, &flag, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get convergence flag from (lstruct)", name);
		return(status);
	}
	if (flag & SINGULAR_CONVERGENCE) {
		thError("%s: WARNING - convergence flag '%s' raised after amplitude fit", name, shEnumNameGetFromValue("CONVERGENCE", flag));
		singular2 = 1;
		flag = CONTINUE;
		status = UpdateLMConvergence(lstruct, flag);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not update (convergence flag) to '%s'", name, shEnumNameGetFromValue("CONVERGENCE", flag));
			return(status);
		}
	} else {
		status = thLstructAssignDa(lstruct);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not assign (da)", name);
			return(status);
		}
	}
}

/* there should be a new way of calculating Chisq */
status = thLCalcChisqNAlgRun(lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calculate (chisqN) for (pN)", name);
	return(status);
}
status = thLstructGetChisqN(lstruct, &Nchisq, &Ncost);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract derivative information for the new par value", name);
	return(status);
}  
if (Ncost < 0.0 || Nchisq < 0.0 || cost < 0.0 || chisq < 0.0) {
	thError("%s: ERROR - found some negative values in (X2 = %20.10G, X2N = %20.10G, C = %20.10G, CN = %20.10G)", name, (double) chisq, (double) Nchisq, (double) cost, (double) Ncost);
	return(SH_GENERIC_ERROR);
}
if (Ncost < Nchisq) {
	thError("%s: WARNING - found (chisqN = %g) > (costN = %g)", name, (float) Nchisq, (float) Ncost);
} 
#if DO_LFIT_RECORD
status = do_lfit_record_insert_chisq_in_LM(lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not insert (chisq) in (lmethod)", name);
	return(status);
}
#endif

CHISQFL Dchisq = chisq - Nchisq;

LM_STEP_STATUS step_status = UNKNOWN_LM_STEP_STATUS;

MLEFL x1 = 0.0, x2 = 0.0;
if (cost >= Ncost) {

	step_status = LM_STEP_ACCEPTED;
	if (singular || singular2) {
		step_count += HYBRID_LM_SINGULARITY_ALLOWANCE; 
	} else {
		int small_flag = 0;
		status = is_derivative_too_small_for_gauss_newton(jtdmm, Ncost, nx, &small_flag);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not evaluate if F' is too small for Gauss-Newton", name);
			return(status);
		}
		if (small_flag) {
			step_count++;
		} else {
			step_count = 0;
		}
	}
	/* in AlgRun derivatives are made whenever the images are made  -
	note - the J-matrix used below should not be the update version */
	/* now that we know this is an acceptable step, the derivatives should be
	calculated 
	status = MakeImageAndMatrices(lstruct, DM);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not make matrices and images for the new parameter", name);
		return(status);
	}
	*/
	
	dl = (MLEFL) 0.0;
	for (i = 0; i < nx; i++) {
		if (dX[i] != (MLEFL) 0.0) dl += ((MLEFL) 0.5) * (lambda * dX[i] * jtj[i][i] - jtdmm[i]) * dX[i];
	}

	rho = (cost - Ncost) / dl;
	if (cost == Ncost) rho = 0;
	/* 
	we should decide whether during this update the derivative matrices are updated as well 
	and how the flags are set
	*/
	if (nx == 0 || rho >= 0 || cost >= Ncost) {
		status = UpdateXFromXN(lstruct, sector);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not update the parameter values", name);
			return(status);
		}
		/* this part seems to have been mistakenly omitted Feb 14, 2012 */  
		status = UpdateChisq(lstruct);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not update the (chisq) value", name);
			return(status);
		}
		/* */
		/* x1 and x2 are auxiliary paramters to make the code readable */
		x1 = ((MLEFL) 1.0) / gamma;
		if (isnan (rho) || isinf(rho)) {
			x2 = -1;
		} else {
			x2 = (MLEFL) 1.0 - (beta - (MLEFL) 1.0) * pow((rho / rhobar - (MLEFL) 1.0), q);
		}
		status = LMultiplyVariable(&lambda, MAX(x1, x2));
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not multiply (lambda= %g) with (MAX(x1,x2)= %g)", name, (float) lambda, (float) MAX(x1, x2));
			return(status);
		}
		nu = beta;
	} else {
		thError("%s: ERROR - algorithm problem, Cost_new (= %g) < Cost_old (= %g), (rho = %g) < 0", name, (float) Ncost, (float) cost, (float) rho);
		return(SH_GENERIC_ERROR);
	}

} else {
	
	step_status = LM_STEP_REJECTED;
  	step_count = 0;

	status = thLstructSetXupdate(lstruct, OUTDATED, sector);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not set (Xupdate) flag in (lstruct) to 'outdated'", name);
		return(status);
	}
	status = LMultiplyVariable(&lambda, nu);	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not multiply (lambda= %g) with (nu= %g)", name, lambda, nu);
		return(status);
	}
	status = LMultiplyVariable(&nu, (MLEFL) LM_NU_BOOST_FACTOR);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not multiply (nu= %g) with (LM_NU_BOOST_FACTOR)", nu);
		return(status);
	}
}

#if (DO_LFIT_RECORD || 1)
	LMETHOD *lmethod = NULL;
	status = thLstructGetLmethod(lstruct, &lmethod); 
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (lmethod) from (lstruct)", name);
		return(status);
	}
	step_mode++;
	status = thLmethodPutStatus(lmethod, step_status, step_count, step_mode);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update the acceptance status in (lmethod)", name);
		return(status);
	}
#endif


#if DEBUG_MLE
printf("%s: rho= %8.4G, lambda= %8.4G, nu= %8.4G, (x1, x2) = (%8.4G, %8.4G) \n", name, (double) rho, (double) lambda, (double) nu, (double) x1, (double) x2);
printf("%s: dl = %8.4G, dCost = %20.10G, Cost = %20.10G, Cost_N = %20.10G \n", name, (double) dl, (double) (cost - Ncost), (double) cost, (double) Ncost);
printf("%s: dX2 = %8.4G, X2 = %20.10G, X2_N = %20.10G \n", name, (double) (chisq - Nchisq), (double) chisq, (double) Nchisq);
printf("%s: dpcost= %8.4G, pcost = %20.10G, pcost_N = %20.10G \n", name, (double) ((cost - Ncost) - (chisq - Nchisq)), (double) (cost - chisq), (double) (Ncost - Nchisq));
#endif

/* update the damping parameters */
status = UpdateLMDamping(lstruct, lambda, nu);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update the damping parametes", name);
	return(status);
}

status = UpdateLMSmallStepCount(lstruct, Dchisq);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get update (small step count) in (LM)", name);
	return(status);
}

/* do we need convergence test at this stage at all:

CONVERGENCE flag;

flag = CheckLMConvergence(lstruct, PSECTOR, &status);
if (status != SH_SUCCESS) thError("%s: ERROR - could not perform convergence test", name);

UpdateLMConvergence(lstruct, flag);
return(status);
*/
#if THCLOCK
clk2 = clock();
printf("%s: CLOCKING - [%g] CPUs \n", name, ((float) (clk2 - clk1)) / CLOCKS_PER_SEC);
#endif

#if DEBUG_MEMORY_VERBOSE
printf("%s: memory statistics after conducting (LM-fit)", name);
shMemStatsPrint();
fflush(stdout);
fflush(stderr);
#endif


return(SH_SUCCESS);
}

/* the following criteria is developed based on the article 

J. Dennis, D. Gay and R. Welsch, "An adaptive nonlinear least-squares algorithm", ACM Trans. Math. Software 7 (1981), 348-368.

1. Absolute function convergence:

  f(x_k) < epsilon_A

=============

I. Definition:

q_k(x) is the quadratic apporoximation based on the value of f(x_k) and J(x_k) for function f(x)

II. Condition for further tests:

f(x_k) - f(x_k+1) =< 2 [f(x_k) - q_k(x_k+1)]

III. Definition: 

RELDX(x, y, D) := max { |d_i (x_i - y_i) | } / max { d_j (|x_j| + |y_j|) }

Where D is the scale matrix diag(d_1_k, d_2_k, ... d_p_k) obtained from Hessian (Jt J)

============

2. X-convergence:

RELDX(x_k, x_k+1, D_k) =< epsilon_k

3. Relative function-convergence

H_k is positive definite &&

[f(x_k) - q_k(x_k - inv(H_k) grad(f(x_k))] / f(x_k) =< epsilon_R

4. Singular convergence:

None of (1, 2, 3) is satisfied &&

max{ f(x_k) - q_k(x): || D_k(x - x_k) ||_2 <= b0} =< epsilon_R f(x_k)

If necessary, the left hand side of the last inequality is evaluated by computing (but not trying) another step (x = x_k+1)

5. False convergence (convergence to a non-critical point)

None of the tests are satisfied, condition mentioned above doesn't hold &&

RELDX(x_k, x_k+1, D_k) < epsilon_F < epsilon_x

This may mean that the convergance tolerance in previous criteria are too small for the accuracy to which f and J are being computed, that there is an error in computing J, or that f or grad(f) is discontinuous near x_k.


===========

Default Values for Tolerance Levels

epsilon_A = 1.0E-20; 
epsilon_x = 1.49E-8; 
epsilon_R = 1.0E-10; 
epsilon_F = 2.22E-14

on a machine with a double precision round off error of 16 ^ -13 = 2.22E-16


*/

/* the following criteria is developed based on the article 

J. Dennis, D. Gay and R. Welsch, "An adaptive nonlinear least-squares algorithm", ACM Trans. Math. Software 7 (1981), 348-368.

1. Absolute function convergence:

  f(x_k) < epsilon_A

=============
*/



/* the following criteria is developed based on the article 

J. Dennis, D. Gay and R. Welsch, "An adaptive nonlinear least-squares algorithm", ACM Trans. Math. Software 7 (1981), 348-368.

1. Absolute function convergence:

  f(x_k) < epsilon_A

=============

I. Definition:

q_k(x) is the quadratic apporoximation based on the value of f(x_k) and J(x_k) for function f(x)

II. Condition for further tests:

f(x_k) - f(x_k+1) =< 2 [f(x_k) - q_k(x_k+1)]

III. Definition: 

RELDX(x, y, D) := max { |d_i (x_i - y_i) | } / max { d_j (|x_j| + |y_j|) }

Where D is the scale matrix diag(d_1_k, d_2_k, ... d_p_k) obtained from Hessian (Jt J)

============

2. X-convergence:

RELDX(x_k, x_k+1, D_k) =< epsilon_k

3. Relative function-convergence

H_k is positive definite &&

[f(x_k) - q_k(x_k - inv(H_k) grad(f(x_k))] / f(x_k) =< epsilon_R

4. Singular convergence:

None of (1, 2, 3) is satisfied &&

max{ f(x_k) - q_k(x): || D_k(x - x_k) ||_2 <= b0} =< epsilon_R f(x_k)

If necessary, the left hand side of the last inequality is evaluated by computing (but not trying) another step (x = x_k+1)

5. False convergence (convergence to a non-critical point)

None of the tests are satisfied, condition mentioned above doesn't hold &&

RELDX(x_k, x_k+1, D_k) < epsilon_F < epsilon_x

This may mean that the convergance tolerance in previous criteria are too small for the accuracy to which f and J are being computed, that there is an error in computing J, or that f or grad(f) is discontinuous near x_k.


===========

Default Values for Tolerance Levels

epsilon_A = 1.0E-20; 
epsilon_x = 1.49E-8; 
epsilon_R = 1.0E-10; 
epsilon_F = 2.22E-14

on a machine with a double precision round off error of 16 ^ -13 = 2.22E-16


*/

/* the following criteria is developed based on the article 

J. Dennis, D. Gay and R. Welsch, "An adaptive nonlinear least-squares algorithm", ACM Trans. Math. Software 7 (1981), 348-368.

1. Absolute function convergence:

  f(x_k) < epsilon_A

=============
*/

RET_CODE CheckDorrisConvergence(lstruct) {
char *name = "CheckDorrisConvergence";

thError("%s: ERROR - function not ready for dispatch", name);
return(SH_GENERIC_ERROR);

CONVERGENCE cflag;

CHISQFL chisq;
MLEFL eA;
if (chisq < eA) {
	cflag = ABSOLUTEF_CONVERGENCE;
	return(SH_SUCCESS);
}

/*
I. Definition:

q_k(x) is the quadratic apporoximation based on the value of f(x_k) and J(x_k) for function f(x)

II. Condition for further tests:

f(x_k) - f(x_k+1) =< 2 [f(x_k) - q_k(x_k+1)]

*/

MLEFL *apN, *ap;
CHISQFL chisqN;
MLEFL *jtdmmap, **jtjap;
MLEFL *wx;
int k, nap;

RET_CODE status;
MLEFL z;

z = qk(apN, ap, chisq, jtdmmap, jtjap, wx, nap, &status); 
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calculate (qk) for (p_(k+1)) (k = %d) -- cannot perform further convergence tests", name, k);
	cflag = UNKNOWN_CONVERGENCE;
	return(status);	
}
if ((chisq - chisqN) < 2.0 * (chisq - z)) {

/*
III. Definition: 

RELDX(x, y, D) := max { |d_i (x_i - y_i) | } / max { d_j (|x_j| + |y_j|) }

Where D is the scale matrix diag(d_1_k, d_2_k, ... d_p_k) obtained from Hessian (Jt J)

============

2. X-convergence:

RELDX(x_k, x_k+1, D_k) =< epsilon_k

*/

MLEFL *Dap, ek;
MLEFL y;

y = RELDX(ap, apN, Dap, nap, &status);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calculate the D-distance between (ap) and (apN) at step (k  = %d) - cannot perform 'X convergence' test", name, k);
	cflag = UNKNOWN_CONVERGENCE;
	return(SH_GENERIC_ERROR);
}
if (y <= ek) {
	cflag = X_CONVERGENCE;
	return(SH_SUCCESS);
}

/*
3. Relative function-convergence

H_k is positive definite &&

[f(x_k) - q_k(x_k - inv(H_k) grad(f(x_k))] / f(x_k) =< epsilon_R

*/

MLEFL eR;
MLEFL *wwx, *wwwx;

int singular = 0;
status = do_gauss_newton(wwx, ap, jtjap, jtdmmap, NULL, nap, &singular);
if (singular) {
	#if DEBUG_MLE_SINGULAR
	thError("%s: WARNING - singularity detected in gauss-newton step", name);
	fflush(stderr);
	fflush(stdout);
	#endif
	cflag = SINGULAR_CONVERGENCE;
	return(SH_SUCCESS);
} else if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not take Gauss-Newton step in (k = %d) -- cannot check for 'relative function convergence'", name, k);
	cflag = ERROR_CONVERGENCE;
	return(status);
}
y = qk(wwx, ap, chisq, jtdmmap, jtjap, wx, nap, &status);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calculate (qk) for the Guass-Newton step (k = %d) -- cannot check for 'relative function convergence'", name, k);
	cflag = UNKNOWN_CONVERGENCE;
	return(status);
}
if ((chisq - y) / chisq <= eR) {
	cflag = RELATIVEF_CONVERGENCE;
	return(SH_SUCCESS);
}

/*
4. Singular convergence:

None of (1, 2, 3) is satisfied &&

max{ f(x_k) - q_k(x): || D_k(x - x_k) ||_2 <= b0} =< epsilon_R f(x_k)

If necessary, the left hand side of the last inequality is evaluated by computing (but not trying) another step (x = x_k+1)

*/

MLEFL b0;

status = find_point_in_direction(wx, apN, ap, Dap, +b0, nap);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not find the point in direction of x_k+1 for step (k = %d)", name, k);
	cflag = UNKNOWN_CONVERGENCE;
	return(status);
}
status = find_point_in_direction(wwx, apN, ap, Dap, -b0, nap);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not find point in the opposite direction of x_k+1 for step (k = %d)", name);
	cflag = UNKNOWN_CONVERGENCE;
	return(status);
}


y = qk(wx, ap, chisq, jtdmmap, jtjap, wwwx, nap, &status);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calculate (qk) for extreme point I on ball || D (x - x_k) || < b0 in step (k = %d) -- cannot perform 'singular convergence' test'", name, k);
	cflag = UNKNOWN_CONVERGENCE;
	return(status);
}
z = qk(wwx, ap, chisq, jtdmmap, jtjap, wwwx, nap, &status);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calculate (qk) for extreme point II on ball || D (x - x_k) || < b0 in step (k = %d) -- cannot perform 'singular convergence' test'", name, k);
	cflag = UNKNOWN_CONVERGENCE;
	return(status);
}
if (((chisq - y) <= eR * chisq) && ((chisq - z) <= eR * chisq)) {
	cflag = SINGULAR_CONVERGENCE;
	return(SH_SUCCESS);
}

} else {

/* 

5. False convergence (convergence to a non-critical point)

None of the tests are satisfied, condition mentioned above doesn't hold &&

RELDX(x_k, x_k+1, D_k) < epsilon_F < epsilon_x

This may mean that the convergance tolerance in previous criteria are too small for the accuracy to which f and J are being computed, that there is an error in computing J, or that f or grad(f) is discontinuous near x_k.

*/

MLEFL eF, *Dap;
MLEFL y;
y = RELDX(ap, apN, Dap, nap, &status);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calculate distance (x, xN; D) for (k = %d) -- cannot check for 'false convergence'", name, k);
	cflag = UNKNOWN_CONVERGENCE;
	return(status);
}

if (y < eF) {
	cflag = FALSE_CONVERGENCE;
	return(SH_SUCCESS);
}
}

cflag = CONTINUE;
return(SH_SUCCESS);
}


/* sector is a flag that tells the machine if the convergence should be checked for a-p sector or for p sector only */
CONVERGENCE CheckLMConvergence(LSTRUCT *lstruct, LSECTOR sector, RET_CODE *status) {
char *name = "CheckLMConvergence";

printf("%s: DEBUG - convergence being evaluated in sector = '%s' \n", name, shEnumNameGetFromValue("LSECTOR", sector));
 
RET_CODE status2;
/* Convergence Test */
int k, kmax;
MLEFL e1, e2;
status2 = ExtractLMConvergence(lstruct, &k, &e1, &e2, &kmax); 
if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not extract convergence criteria", name);
	if (status != NULL) *status = status2;
	return(UNKNOWN_CONVERGENCE);
}

/* update the step number */
k++;
status2 = UpdateLMStepNo(lstruct, k);
if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not update the step number to (%d) in (lstruct)", name, k);
	if (status != NULL) *status = status2;
	return(UNKNOWN_CONVERGENCE);
}

/* if p is the same as before no need to go into the whole pain of calculating magnitudes and all */
XUPDATE update;
status2 = thLstructGetXupdate(lstruct, &update, PSECTOR);
if  (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not extract (pupdate) flag in (lstruct)", name);
	if (status != NULL) *status = status2;
	return(UNKNOWN_CONVERGENCE);
}
int i, n;
MLEFL *jtdmm;
MLEFL magFprime = 0.0;

if (update == UPDATED) {

	status2 = thExtractNx(lstruct, sector, &n);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not determine the size of parameter (x) (p or a-p)", name);
		if (status != NULL) *status = status2;
		return(UNKNOWN_CONVERGENCE);
	}
	status2 = thExtractJtdmm(lstruct, sector, &jtdmm);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not extract < Jt | W | d - m > vector", name);
		if (status != NULL) *status = status2;
		return(UNKNOWN_CONVERGENCE);
	}
	for (i = 0; i < n; i++) {
		magFprime += pow(jtdmm[i], 2);
		};
	magFprime = pow(magFprime, 0.5);
	if (magFprime <= e1 && k >= NSTEP_MIN_REQUIRED) {
		status2 = UpdateLMConvergence(lstruct, CONVERGED);
		if (status2 != SH_SUCCESS) {
			thError("%s: ERROR - could not update convergence flag", name);
			if (status != NULL) *status = status2;
			return(UNKNOWN_CONVERGENCE);
		} else {
			if (status != NULL) *status = SH_SUCCESS;
			return(CONVERGED);
		}
		}
}

MLEFL *x, *dx;
status2 = ExtractXxNdX(lstruct, sector, NULL, NULL, &x, NULL, &dx, &n);
if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not locate (x, xN, or n) in (lstruct)", 
		name);
	if (status != NULL) *status = status2;
	return(UNKNOWN_CONVERGENCE);
	}

#if 0
/* this the old metric used for the additive format, the new metric uses matrix JtJ */

MLEFL magDx = 0.0, magX = 0.0;
for (i = 0; i < n; i++) {
	magDx += pow(dx[i], 2);
	magX += pow(x[i], 2);
}

#else

MLEFL magDx = 0.0, magX = 0.0;
int j;
MLEFL **D, *Di;
MLEFL xi, dxi;
status2 = thExtractJtj(lstruct, sector, &D);
if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not extract metric properly", name);
	if (status != NULL) *status = status2;
	return(UNKNOWN_CONVERGENCE);
}

for (i = 0; i < n; i++) {
	dxi = dx[i];
	xi = x[i];
	Di = D[i];
	for (j = 0; j < n; j++) {
		magDx += dxi * Di[j] * dx[j];
		magX += xi * Di[j] * x[j];
	}
}
#endif 

magDx = pow(magDx, 0.5);
magX = pow(magX, 0.5);

if (magDx - e2 * magX < 0.0 && k >= NSTEP_MIN_REQUIRED) {
	status2 = UpdateLMConvergence(lstruct, TOOSMALLSTEP);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not update convergence flag", name);
		if (status != NULL) *status = status2;
		return(UNKNOWN_CONVERGENCE);
	} else {
		#if DEBUG_MLE
		printf("%s: convergence criteria 'TOOSMALLSTEP' satisified at (k = %d)\n", name, lstruct->lmethod->k);
		printf("%s: ||Dx|| = %.3e, ||x|| = %.3e , e2 = %.3e \n", name, (float) magDx, (float) magX, (float) e2);
		LMETHOD *lmethod = lstruct->lmethod;
		printf("%s: sector = '%s', type = '%s', wtype = '%s' \n", name, shEnumNameGetFromValue("LSECTOR", sector), shEnumNameGetFromValue("LMETHODTYPE", lmethod->type), shEnumNameGetFromValue("LMETHODSUBTYPE", lmethod->wtype));
		#endif
		if (status != NULL) *status = SH_SUCCESS;
		return(TOOSMALLSTEP);
	}
	}	
if (k >= kmax) {
	status2 = UpdateLMConvergence(lstruct, TOOMANYITER);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not update convergence flag", name);
		if (status != NULL) *status = status2;
		return(UNKNOWN_CONVERGENCE);
	} else {
		#if DEBUG_MLE
		printf("%s: convergence criteria 'TOOMANYITER' satisfied at (k = %d, kmax = %d)\n", name, lstruct->lmethod->k, lstruct->lmethod->kmax);
		#endif
		if (status != NULL) *status = SH_SUCCESS;
		return(TOOMANYITER);
	}
	}

status2 = UpdateLMConvergence(lstruct, CONTINUE);
if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not update convergence flag", name);
	if (status != NULL) *status = status2;
	return(UNKNOWN_CONVERGENCE);
}
if (status != NULL) *status = SH_SUCCESS;
return(CONTINUE);
}

static MLEFL qk(MLEFL*x, MLEFL *xk, MLEFL fk, 
		MLEFL *jtdmm, MLEFL **jtj, MLEFL *w, int n, 
		RET_CODE *status) {
char *name = "qk";

if (jtdmm == NULL || jtj == NULL) {
	thError("%s: ERROR - null (jtdmm) or (jtj) -- cannot calculate the quadratic approximation to function f", name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(THNAN);
}

if (n <= 0) {
	thError("%s: ERROR - unacceptable dimention (n = %d) - cannot do the quadratic approx to func f", name, n);
	if (status != NULL) *status = SH_GENERIC_ERROR;
        return(THNAN);
}

if (x == NULL || xk == NULL) {
	thError("%s: ERROR - null (x) or (xk) -- cannot calculate the quadratic approximation to function f", name);
        if (status != NULL) *status = SH_GENERIC_ERROR;
        return(THNAN);
}

int i, j;
MLEFL z, y = 0.0, *jtji;

if (w != NULL) {
	MLEFL *dx;
	dx = w;

	for (i = 0; i < n; i++) {
		dx[i] = x[i] - xk[i];
	}

	for (i = 0; i < n; i++) {
		/* 
		   J is defines as D |m > / Dp
		   therefore the first derivative of L is
		   Jt W |m - d > 
		   while jtdmm is Jt W |d - m >
		*/	
		z = -2.0 * jtdmm[i];
		jtji = jtj[i];
		if (jtji == NULL) {
			thError("%s: ERROR - misallocated JtJ matrix at row (i = %d)", name, i);
			if (status != NULL) *status = SH_GENERIC_ERROR;
			return(MLEFLNAN);
		}
		for (j = 0; j < n; j++) {
			z += (MLEFL) dx[j] * jtji[j];
		}
		y += 0.5 * z * (MLEFL) dx[i];
	}
} else {

	for (i = 0; i < n; i++) {
		z = 2.0 * jtdmm[i];
		jtji = jtj[i];
		if (jtji == NULL) {
			thError("%s: ERROR - misallocated JtJ matrix at row (i = %d)", name, i);
			if (status != NULL) *status = SH_GENERIC_ERROR;
			return(THNAN);
		}
		for (j = 0; j < n; j++) {
			z += (MLEFL) (x[j] - xk[j]) * jtji[j];
		}
		y += 0.5 * z * (MLEFL) (x[i] - xk[i]);
	}
}

y += (MLEFL) fk;
if (status != NULL) *status = SH_SUCCESS;
return(y);
}

static MLEFL RELDX(MLEFL *x, MLEFL *y, MLEFL *D, int n, RET_CODE *status) {
char *name = "RELDX";

/* 
RELDX(x, y, D) := max { |d_i (x_i - y_i) | } / max { d_j (|x_j| + |y_j|) }
*/

if (n <= 0) {
	thError("%s: ERROR - unacceptable array size (n = %d)", name, n);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(THNAN);
}

if (x == NULL || y == NULL) {
	thError("%s: ERROR - null (x) or (y) - cannot calculated relative DX", name);
        if (status != NULL) *status = SH_GENERIC_ERROR;
        return(THNAN);
}

/* does not check for the positiveness of D_k */

MLEFL d, d1, m1, m2;
int i;

if (D != NULL) {
	m1 = fabs(D[0] * (x[0] - y[0]));
	m2 = D[0] * (fabs(x[0]) + fabs(y[0]));
	for (i = 1; i < n; i++) {
		d1 = fabs(D[i] * (x[i] - y[i]));
		m1 = MAX(m1, d1);
		d1 = fabs(D[i]) * (fabs(x[i]) + fabs(y[i]));
		m2 = MAX(m2, d1);
	}
	
	d = m1 / m2;

} else {

	thError("%s: WARNING - null metrix (D) translates into diagonal {1}_(%d)", name, n);

	m1 = fabs((x[0] - y[0]));
	m2 = (fabs(x[0]) + fabs(y[0]));
	for (i = 1; i < n; i++) {
		d1 = fabs(x[i] - y[i]);
		m1 = MAX(m1, d1);
		d1 = fabs(x[i]) + fabs(y[i]);
		m2 = MAX(m2, d1);
	}
	
	d = m1 / m2;

}

if (status != NULL) *status = SH_SUCCESS;
return(d);
}

static RET_CODE find_point_in_direction(MLEFL *y, MLEFL *x, MLEFL *x0, MLEFL *D, MLEFL r, int n) {
char *name = "find_point_in_direction";

if (n <= 0) {
	thError("%s: ERROR - expected positive size of parameter while (n = %d)", name, n);
	return(SH_GENERIC_ERROR);
}

if (y == NULL) {
	thError("%s: ERROR - no placeholder for output point (y)", name);	
	return(SH_GENERIC_ERROR);
}
if (x == NULL) {
	thError("%s: ERROR - null directionality point (x)", name);
	return(SH_GENERIC_ERROR);
}
if (x0 == NULL) {
	thError("%s: ERROR - null origin (x0)", name);
	return(SH_GENERIC_ERROR);
}

int i;
if (r == (MLEFL) 0.0) {
	for (i = 0; i < n; i++) {
		y[i] = x0[i];
	}
	return(SH_SUCCESS);
}

MLEFL d = 0.0;
if (D != NULL) {
	for (i = 0; i < n; i++) {
		y[i] = x[i] - x0[i];
		d +=  y[i] * y[i] * D[i];
	}
} else {
	for (i = 0; i < n; i++) {
		y[i] = x[i] - x0[i];
		d +=  y[i] * y[i];
	}
}
d = sqrt(d);
if (d == (MLEFL) 0.0) {
	thError("%s: ERROR - (x) and (x0) coincide, cannot find the mirror image", name);
	return(SH_GENERIC_ERROR);
}
MLEFL lambda;
lambda = r / d;

for (i = 1; i < n; i++) {
	y[i] = lambda * y[i] + x0[i];
}

return(SH_SUCCESS);
}


static RET_CODE do_gauss_newton(MLEFL *y, MLEFL *x, 
				MLEFL **H, MLEFL *mDf, MLEFL **wH, int n, int *singular) {
char *name = "do_gauss_newton";

if (n <= 0) {
	thError("%s: ERROR - expected a positive dimension for parameter space (n = %d)", name, n);
	return(SH_GENERIC_ERROR);
}

if (x == NULL) {
	thError("%s: ERROR - null starting position (x)", name);
	return(SH_GENERIC_ERROR);
}
if (H == NULL) {
	thError("%s: ERROR - null Hessian matrix (H)", name);
	return(SH_GENERIC_ERROR);
}
if (mDf == NULL) {
	thError("%s: ERROR - null negative derivative (Df)", name);
	return(SH_GENERIC_ERROR);
}
if (y == NULL) {
	thError("%s: ERROR - null place holder for result (y)", name);
	return(SH_GENERIC_ERROR);
}
if (singular == NULL) {
	thError("%s: ERROR - null output placeholder for (singular)", name);
	return(SH_GENERIC_ERROR);
}
*singular = 0;
int i, j;
MLEFL **invH = NULL;
if (wH == NULL) {

	invH = thCalloc(n, sizeof(MLEFL *));
	invH[0] = shCalloc(n * n, sizeof(MLEFL));
	for (i = 0; i < n; i++) {
		invH[i] = invH[0] + i * n;
	}
	wH = invH;
}

MLEFL *wHrow;
MLEFL *Hrow;
for (i = 0; i < n; i++) {
	wHrow = wH[i];
	Hrow = H[i];
	if (Hrow == NULL) {
		thError("%s: ERROR - memory allocation problem with Hessian matrix (H) at row (i = %d)", name, i);
		return(SH_GENERIC_ERROR);
	}
	if (wHrow == NULL) {
		thError("%s: ERROR - memory allocation problem with (wH) at row (i = %d)", name, i);
		return(SH_GENERIC_ERROR);
	}
	for (j = 0; j < n; j++) {
		wHrow[j] = (MLEFL) Hrow[j];
	}
	y[i] = (MLEFL) mDf[i];
}

RET_CODE status;
int this_singular = 0;
#if 0
status = thMatrixInvert(wH, y, n); 
#else
status = LMatrixInvert(wH, mDf, y, n, &this_singular);
#endif
if (this_singular) {
	#if DEBUG_MLE_SINGULAR
	thError("%s: WARNING - found singularity while inverting a matrix to get Hessian matrix", name);
	fflush(stderr);
	fflush(stdout);
	#endif
	*singular = this_singular;
	if (invH != NULL) {
		thFree(invH[0]);
		thFree(invH);
	}
	return(SH_SUCCESS);
}
if (invH != NULL) {
	thFree(invH[0]);
	thFree(invH);
}
for (i = 0; i < n; i++) {
	y[i] += x[i];
	}
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not invert the Hessian matrix (H)", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE thLCalcChisqNSimpleRun(LSTRUCT *lstruct) {
char *name = "thLCalcChisqNSimpleRun";

if (lstruct == NULL) {
	thError("%s: ERROR - null (lstruct)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
LWORK *lwork = NULL;
LWMASK *lwmask = NULL;
status = thLstructGetLwork(lstruct, &lwork);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwork) from (lstruct)", name);
	return(status);
}
status = thLstructGetLwmask(lstruct, &lwmask);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwmask) from (lstruct)", name);
	return(status);
}
if (lwork == NULL || lwmask == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
REGION *dmm;
status = thExtractDmm(lstruct, &dmm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - couldn't extract |d - m >", name);
	return(status);
}

CHISQFL chisq;
chisq = (CHISQFL) (thBracket(dmm, lwmask, dmm, &status));
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calculate < d - m | W | d - m >", name);
	return(status);
}

lwork->chisqN = chisq;

#if MLE_APPLY_CONSTRAINTS
thError("%s: ERROR - this function does not support MLE_APPLY_CONSTRAINTS keyword", name);
return(SH_GENERIC_ERROR);

CHISQFL parameter_cost = 0.0;
status = thLCGet(lstruct, &parameter_cost);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get parameter cost function from (lstruct)", name);
	return(status);
	}
if (parameter_cost < (CHISQFL) 0.0) {
	thError("%s: WARNING - negative (pcost = %g) found in (lstruct)", name, (float) parameter_cost);
}
chisq += parameter_cost;
#endif
lwork->costN = chisq;

return(SH_SUCCESS);
}

RET_CODE thLCalcChisqNAlgRun(LSTRUCT *lstruct) {
char *name = "thLCalcChisqNAlgRun";

if (lstruct == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
LWORK *lwork = NULL;
LWMASK *lwmask = NULL;
status = thLstructGetLwork(lstruct, &lwork);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwork) from (lstruct)", name);
	return(status);
}
status = thLstructGetLwmask(lstruct, &lwmask);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwmask) from (lstruct)", name);
	return(status);
}
if (lwork == NULL || lwmask == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}

int nmodel;
status = thLstructGetNModel(lstruct, &nmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract number of models", name);
	return(status);
}
MLEFL *amp;
status = thLstructGetAmpN(lstruct, &amp);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract amplitudes", name);
	return(status);
}

MLEFL *dmi, **mimj;
status = thExtractBrackets(lstruct, &dmi, NULL, &mimj, NULL, NULL, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get <mi|d> and <mi|mj>", name);
	return(status);
}
#if VERY_DEEP_DEBUG_CHISQ
if (nmodel < 200) {
	dbgMimj(mimj, nmodel, name, "");
	dbgDmi(dmi, nmodel, name, "");
	MLEFL dddd = 0.0;
	status = thLDDGet(lstruct, &dddd);
	printf("%s: < D | W | D > = %20.10G \n", name, (double) dddd);
}
#endif
#if DEEP_DEBUG_CHISQ
if (nmodel < 200) {
	dbgX(lstruct, name, ASECTOR, "amplitudes - caller: thLCalcChisqNAlgRun");
	dbgX(lstruct, name, PSECTOR, "parameters - caller: thLCalcChisqNAlgRun");
}
#endif
int i, j;
MLEFL x;
#if DO_KAHAN_SUM
	#if USE_KAHAN_PACKAGE
	KAHANFL z_dble = (KAHANFL) 0.0;
	#else
	MLEDBLE z_dble = (MLEDBLE) 0.0;
	#endif
#else
MLEDBLE z_dble = (MLEDBLE) 0.0;
#endif

#if MLE_CATCH_NAN
int err_nan = 0;
for (i = 0; i < nmodel; i++) {
	MLEDBLE amp_i = (MLEDBLE) amp[i];
	if (isnan(amp_i) || isinf(amp_i)) {
		thError("%s: ERROR - (NAN) value in amplitude value at (i = %d)", name, i);
		err_nan++;
	}
	MLEFL *mimj_i = mimj[i];
	for (j = 0; j < nmodel; j++) {
		MLEDBLE mimj_ij = ((MLEDBLE) mimj_i[j]);
		if (isnan(mimj_ij) || isinf(mimj_ij)) {
			thError("%s: ERROR - (NAN) value in mimj matrix at (i = %d, j = %d)", name, i, j);
			err_nan++;
		}
		
	}
}
status = thLDDGet(lstruct, &x);
if (isnan(x) || isinf(x)) {
	thError("%s: ERROR - (NAN) value obtained for < data | data >", name);
	err_nan++;
}
if (err_nan != 0) {
	thError("%s: ERROR - (NAN) detected in (%d) occasions",  name, err_nan);
	fflush(stderr);
	fflush(stdout);
	return(SH_GENERIC_ERROR);
}

#endif

#if DO_KAHAN_SUM

#if USE_KAHAN_PACKAGE
thKahanZero();
KAHANFL zz_item = 0.0;
#else
CHISQFL x_error = (CHISQFL) 0.0;
MLEDBLE z_error = (MLEDBLE) 0.0;
MLEDBLE z_item, z_dble2 = (MLEDBLE) 0.0;
#endif


for (i = 0; i < nmodel; i++) {
	MLEDBLE amp_i = (MLEDBLE) amp[i];
	MLEFL *mimj_i = mimj[i];
	for (j = 0; j < nmodel; j++) {
		#if USE_KAHAN_PACKAGE
			#if USE_KAHAN_PACKAGE3
			thKahanAddDot3((KAHANFL) amp[j], (KAHANFL) mimj_i[j], (KAHANFL) amp_i);
			#else
			zz_item = ((KAHANFL) amp[j]) * ((KAHANFL) mimj_i[j]) * (KAHANFL) amp_i;
			char message[1000];
			sprintf(message, "< a[%d] | m[%d] m[%d] | a[%d] >, a[%d] = %50.25G, a[%d] = %50.25G, m[%d]m[%d] = %50.25G", 
				i, i, j, j, i, (double) amp[i], j, (double) amp[j], i, j, (double) mimj_i[j]);
			thKahanAddVerbose(zz_item, message);
			#endif
		#else
			z_item = ((MLEDBLE) amp[j]) * ((MLEDBLE) mimj_i[j]) * amp_i - z_error;
			z_dble2 = z_dble + z_item;
			z_error = (MLEDBLE) (z_dble2 - z_dble) - (MLEDBLE) z_item;
			z_dble = z_dble2;
			#if DEBUG_CHISQ_KAHAN
			printf("%s: i = %d, j = %d, z_item = %20.10G, z_error = %20.10G \n", name, i, j, (double) z_item, (double) z_error);
			#endif	
		#endif
	}
}

#if DEBUG_CHISQ_KAHAN
	#if USE_KAHAN_PACKAGE
	#else
	printf("%s: <amp | mimj | amp >   = %20.10G \n", name, (double) z_dble);
	printf("%s: < I | MM | I > error  = %20.10G \n", name, (double) z_error);
	#endif
#endif

if (nmodel > 0) {
	for (i = 0; i < nmodel; i++) {
		#if USE_KAHAN_PACKAGE
			#if USE_KAHAN_PACKAGE3
			thKahanAddDot3((KAHANFL) amp[i], (KAHANFL) -2.0, (KAHANFL) dmi[i]);
			#else
			zz_item =((KAHANFL) -2.0) * ((KAHANFL) amp[i]) * ((KAHANFL) dmi[i]);
			char message[1000];
			sprintf(message, "a[%d] < m[%d] | D >, a[%d] = %50.25G, m[%d]D = %50.25G", 
				i, i, i, (double) amp[i], i, (double) dmi[i]);
			thKahanAddVerbose(zz_item, message);
			#endif
		#else
			z_item = (((MLEDBLE) -2.0) * (MLEDBLE) amp[i]) * ((MLEDBLE) dmi[i]) - z_error;
			z_dble2 = z_dble + z_item;
			z_error = (MLEDBLE) (z_dble2 - z_dble) - (MLEDBLE) z_item;
			z_dble = z_dble2;
			#if DEBUG_CHISQ_KAHAN
			printf("%s: i = %d, j = data, z_item = %20.10G, z_error = %20.10G \n", name, i, (double) z_item, (double) z_error);
			#endif
		#endif		
	
	} 
}
#if USE_KAHAN_PACKAGE
#else
	#if (DEBUG_CHISQ_KAHAN)
	printf("%s: < I | < M | D > error = %20.10G \n", name, (double) z_error);
	#endif
#endif
status = thLDDGet(lstruct, &x);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not obtain the < D | D > value from lstruct", name);
	return(status);
}
if (x <= 1.0) {
	thError("%s: WARNING - expected a large value for < D | D > obtained (%g)", name, (float) x);
}

#if USE_KAHAN_PACKAGE
	#if USE_KAHAN_PACKAGE3
	thKahanAddDot2((KAHANFL) 1.0, (KAHANFL) x);
	#else
	zz_item = (KAHANFL) x;
	char message[1000];
	strcpy(message, "< D  | D >");
	thKahanAddVerbose(zz_item, message);
	#endif
	thKahanGetVerbose(NULL, NULL, &z_dble);	
	thKahanZero();
#else
	z_item = ((MLEDBLE) x) - z_error;
	z_dble2 = z_dble + z_item;
	z_error = (MLEDBLE) (z_dble2 - z_dble) - (MLEDBLE) z_item;
	z_dble = z_dble2;	

	#if DEBUG_CHISQ_KAHAN
	printf("%s: < D | D > value = %20.10G,  error       = %20.10G \n", name, (double) z_item, (double) z_error);
	#endif
	x_error = (CHISQFL) (z_error + ((MLEDBLE) ((CHISQFL) z_dble) - z_dble));
#endif

#if DEBUG_CHISQ_KAHAN
	#if USE_KAHAN_PACKAGE
	printf("%s: z_dble (MLEDBLE)      = %20.10G \n", name, (double) z_dble);
	printf("%s: x (CHISQFL)           = %20.10G \n", name, (double) ((CHISQFL) z_dble));
	#else
	printf("%s: x error               = %20.10G \n", name, (double) x_error);
	printf("%s: z_dble (MLEDBLE)      = %20.10G \n", name, (double) z_dble);
	printf("%s: x (CHISQFL)           = %20.10G \n", name, (double) ((CHISQFL) z_dble));
	#endif
#endif

#else

for (i = 0; i < nmodel; i++) {
	MLEDBLE amp_i = (MLEDBLE) amp[i];
	MLEFL *mimj_i = mimj[i];
	MLEDBLE y = 0;
	for (j = 0; j < nmodel; j++) {
		y += ((MLEDBLE) amp[j]) * ((MLEDBLE) mimj_i[j]);
		
	}
	#if DEEP_DEBUG_CHISQ
	printf("%s: | mimj | amp > = %20.10g \n", name, (float) y);
	#endif
	z_dble += amp_i * y;
}
#if DEEP_DEBUG_CHISQ
printf("%s: <amp | mimj | amp > = %20.10g \n", name, (float) z_dble);
#endif

if (nmodel > 0) {
	MLEDBLE y = 0;
	for (i = 0; i < nmodel; i++) {
		y += ((MLEDBLE) amp[i]) * ((MLEDBLE) dmi[i]);
	}
	#if DEEP_DEBUG_CHISQ
	printf("%s: < amp | dmi > = %20.10g \n", name, (float) y);
	#endif
 
	z_dble -= ((MLEDBLE) 2.0) * y;
}

status = thLDDGet(lstruct, &x);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not obtain the < D | D > value from lstruct", name);
	return(status);
}
if (x <= 1.0) {
	thError("%s: WARNING - expected a large value for < D | D > obtained (%g)", name, (float) x);
}

z_dble += (MLEDBLE) x;

#endif /* end of KAHAN sum */

lwork->chisqN = (CHISQFL) z_dble;

#if DEEP_DEBUG_CHISQ
printf("%s: < d | d > = %20.10G \n", name, (double) x);
#endif
#if DEBUG_CHISQ
printf("%s: chisq     = %20.10G \n", name, (double) z_dble);
#endif
 

#if MLE_APPLY_CONSTRAINTS
CHISQFL parameter_cost = 0.0;
status = thLCGet(lstruct, &parameter_cost);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get parameter cost from (lstruct)", name);
	return(status);
}
if (parameter_cost < (CHISQFL) 0.0) {
	thError("%s: WARNING - negative (pcost = %g) found in (lstruct)", name, (float) parameter_cost);
}
z_dble += (MLEDBLE) parameter_cost;
#endif
lwork->costN = (CHISQFL) z_dble;


return(SH_SUCCESS);
}



RET_CODE thLCalcCovap(LSTRUCT *lstruct, int *singular) {
char *name = "thLCalcCovap";

if (lstruct == NULL) {
	thError("%s: ERROR - null (lstruct) cannot be processed", name);
	return(SH_GENERIC_ERROR);
}

if (lstruct->lcov == NULL) {
	thError("%s: ERROR - null (lcov) in (lstruct) - cannot calculate a-p covariance matrix", name);
	return(SH_GENERIC_ERROR);
}
if (singular == NULL) {
	thError("%s: ERROR - null output placeholder for singularity indicator", name);
	fflush(stdout);
	fflush(stderr);
	return(SH_GENERIC_ERROR);
}

*singular = 0;

LMODVAR *lcov = NULL;
LWORK *lwork = NULL;
LDATA *ldata = NULL;
LWMASK *lwmask = NULL;

/* the possible formulas for covariance are availabke at Dennis et al

(Eqn 6.10) 	sigma_k = 2 f(x_k) / max{1, n - p}

Covariance can be calculated in three possible ways 

(Eqn 6.11)	sigma^2 H^-1 (JtJ) H^-1
(Eqn 6.12) 	sigma^2 H^-1
(Eqn 6.13) 	sigma^2 (JtJ)^-1

When (6.11) or (6.12) is specified, a symmetric finite difference Hessian approximation H is obtained at the solution x*. If H is positive definite (or J has full rank at x* for (6.13)), the specified covariance matrix is computed.

A detailed discussion of all three covariance forms is contained in [3]. The second form (6.12) is based on asymptotic maximum likelihood theory and is perhaps the most common form of estimated covariance matrix. We feel that (6.11), the default, is more useful for smaller sample sizes and in other cases where the conditions necessary for the asymptotic theory [38] may be violated. The third form assumes that the residuals at the solution are small and is therefore often highly suspect.

*/

RET_CODE status;
MLEFL sigma, sigmasqrd;
CHISQFL chisqap;
int npix, nap, i, j;

lcov = lstruct->lcov;
lwork = lstruct->lwork;
ldata = lstruct->ldata;
status = thLstructGetLwmask(lstruct, &lwmask);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwmask) from (lstruct)", name);
	return(status);
}
if (lwmask != NULL) {
	status = thLwmaskCountPix(lwmask, &npix);	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not count number of pixels from (lwmask) in (lstruct)", name);
		return(SH_GENERIC_ERROR);
	}
} else {
	int nrow, ncol;

	thError("%s: WARNING - no (lwmask) available - assuming no mask", name);

	if (ldata != NULL && ldata->image != NULL) {
		nrow = ldata->image->nrow;
		ncol = ldata->image->ncol;
	} else if (ldata != NULL && ldata->weight != NULL) {
		nrow = ldata->weight->nrow;
		ncol = ldata->weight->ncol;
	} else if (lwork != NULL && lwork->m != NULL) {
		nrow = lwork->m->nrow;
		ncol = lwork->m->ncol;
	} else if (lwork != NULL && lwork->dmm != NULL) {
		nrow = lwork->dmm->nrow;	
		ncol = lwork->dmm->ncol;
	} else {
		thError("%s: ERROR - improperly allocated (lstruct), cannot count pixels in the fit region", name);
		return(SH_GENERIC_ERROR);
	} 

	npix = nrow * ncol;
}
/* note - in the definition of chisq below there is no 1/2 factor */
status = thLstructGetChisq(lstruct, &chisqap, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calculate (chisq) for a-p mix", name);
	return(SH_GENERIC_ERROR);
}

nap = lwork->nap;

if (nap != lcov->nap) {
	thError("%s: ERROR - mismatch between size of (covap) in (lcov) and (jtj) in (lwork), (nap = %d, %d) respectively", 
	name, lcov->nap, nap);
	return(SH_GENERIC_ERROR);
}


sigma = chisqap / ((MLEFL) MAX(1, (npix - nap)));
if (isnan(sigma) || isinf(sigma)) {
	thError("%s: ERROR - (NAN) detected at (sigma) for the fit", name);
	return(SH_GENERIC_ERROR);
}
sigmasqrd = sigma * sigma;
MLEFL **jtjap, *jtjapi;
MLEFL **covap, **invcovap, *invcovapi, *covapi;
MLEFL *wx;


covap = lcov->covap;
invcovap = lcov->invcovap;
jtjap = lwork->jtjap;
wx = lwork->wx;

if (covap == NULL) {
	thError("%s: ERROR - misallocated (lcov) at (covap)", name);
	return(SH_GENERIC_ERROR);
} else if (invcovap == NULL) {
	thError("%s: ERROR - misallocated (lcov) at (invcovap)", name);
	return(SH_GENERIC_ERROR);
} else if (jtjap == NULL) {
	thError("%s: ERROR - misallocated (lwork) at (jtjap)", name);
	return(SH_GENERIC_ERROR);
}

int err_nan_count = 0;
for (i = 0; i < nap; i++) {
	jtjapi = jtjap[i];
	invcovapi = invcovap[i];
	covapi = covap[i];
	if (covapi == NULL || invcovapi == NULL) {
		thError("%s: ERROR - misallocated (covap) or (invcov) in (lcov) at row (i = %d)", name, i);
		return(SH_GENERIC_ERROR);
	} else if (jtjapi == NULL) {
		thError("%s: ERROR - misallocated (jtjap) in (lwork) at row (i = %d)", name, i);
		return(SH_GENERIC_ERROR);
	}
	for (j = 0; j < nap; j++) {
		covapi[j] = (MLEFL) jtjapi[j];
		invcovapi[j] = (MLEFL) (jtjapi[j] / sigmasqrd);
		if (isnan(covapi[j]) || isinf(covapi[j])) {
			if (err_nan_count < 5) printf("%s: (NAN) detected at (cov-a-p)[%d][%d] matrix \n", name, i, j);
			err_nan_count++;
		} else if (isnan(invcovapi[j]) || isinf(invcovapi[j])) {
			if (err_nan_count < 5) printf("%s: (NAN) detected at (inv-cov-a-p)[%d][%d] matrix \n", name, i, j);
			err_nan_count++;
		}		
	}
}
if (err_nan_count > 0) {
	thError("%s: WARNING - found (%d) instances of (NAN) in cov / inv-cov matrix", name, err_nan_count);
}
/* inverting invcov to get cov - note: wap is of no use */
if (nap != 0) {
	int this_singular = 0;
	status = LMatrixInvert(covap, wx, wx, nap, &this_singular);
	if (this_singular) {

		#if DEBUG_MLE_SINGULAR
		thError("%s: WARNING - singularity detected in inverse-cov matrix", name);
		fflush(stderr);
		fflush(stdout);
		#endif
		
		#if PSEUDO_INVERSE_FOR_SINGULAR
		MAT *wMatA = NULL, *wMatQ = NULL;
		VEC **wVecs = NULL;
		status = thLworkGetWMatVec(lwork, APSECTOR, &wMatA, &wMatQ, &wVecs); /* this should be APSECTOR because this function is particulalry designed for APSECTOR */
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get SVD work space from (lwork)", name);
			return(status);
		}
		for (i = 0; i < nap; i++) {
			for (j = 0; j < nap; j++) {
				covap[i][j] = (MLEFL) (MLEFL) jtjap[i][j];
			}
			wx[i] = (MLEFL) 0.0;
		}
		status = LMatrixSolveSVD(covap, wx, wx, nap, wMatA, wMatQ, wVecs);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not use SVD to calculate covariance matrix", name);
			return(status);
		}
			
		#else
		
		*singular = this_singular;
		for (i = 0; i < nap; i++) {
			covapi = covap[i];
			for (j = 0; j < nap; j++) {
				covapi[j] = NAN;
			}
		}
		return(SH_SUCCESS);
	
		#endif

	} else if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not invert matrix", name);
		fflush(stdout);
		fflush(stderr);
		return(SH_GENERIC_ERROR);
	}
} else {
	thError("%s: WARNING - covariance matrix size is zero", name);
}
/* multiplying with sigma squared */
for (i = 0; i < nap; i++) {
	covapi = covap[i];
	for (j = 0; j < nap; j++) {
		covapi[j] = sigmasqrd * covapi[j];
	}
}

#if DEBUG_MLE_COV
if (dbgCov(covap, nap, name, "- APSECTOR") != 0) {
	thError("%s: ERROR - could not print out elements of covariance matrix", name);
}
if (dbgInvCov(invcovap, nap, name, "- APSECTOR") != 0) {
	thError("%s: ERROR - could not print out elements of inverse covaraince matrix", name);
}
#endif	
return(SH_SUCCESS);
}

RET_CODE ExtractLSMatrices(LSTRUCT *lstruct, 
			MLEFL **dmi, MLEFL ***mimj, 
			MLEFL **amp, MLEFL **ampT, MLEFL ***cov, MLEFL ***covT,  
			int *namp) {
char *name = "ExtractLSMatrices";

if (lstruct == NULL) {
	thError("%s: ERROR - null (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
LWORK *lwork = lstruct->lwork;
LMODVAR *lcov = lstruct->lcov;
LMODEL *lmodel = lstruct->lmodel;
if (lwork == NULL) {
	thError("%s: ERROR - null (lwork) found in (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
if (lcov == NULL) {
	thError("%s: ERROR - null (lcov) found in (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
if (lmodel == NULL) {
	thError("%s: ERROR - null (lmodel) found in (lstruct)", name);
	return(SH_GENERIC_ERROR);
}

if (amp != NULL) *amp = lwork->aN;
if (cov != NULL) *cov = lcov->covap;
if (ampT != NULL) *ampT = lwork->aT;
if (covT != NULL) *covT = lwork->covaT;

if (dmi != NULL || mimj != NULL) {
	if (dmi != NULL) *dmi = lwork->mid;
	if (mimj != NULL) *mimj = lwork->mimj;
}

if (namp != NULL) {
	if (lmodel != NULL) { 
		*namp = lmodel->namp;
	} else if (lwork != NULL) {
		*namp = lwork->nm;
	} else if (lcov != NULL) {
		*namp = lcov->namp;
	} else {
		thError("%s: ERROR - null (lmodel), (lwork), and (lcov) in (lstruct) -- cannot find value for (namp)", name);
		return(SH_GENERIC_ERROR);
	}
}

return(SH_SUCCESS);
}

	

RET_CODE ExtractLMCurrentMatrices(LSTRUCT *lstruct, LSECTOR sector, 
				MLEFL ***jtj, MLEFL ***jtjpdiag, MLEFL ***covT, MLEFL **jtdmm, 
				MLEFL **dX, MLEFL **dxT, CHISQFL *chisq, CHISQFL *cost, int *nx) {

char *name = "ExtractLMCurrentMatrices";

if (lstruct == NULL) {
	thError("%s: ERROR - null (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
LWORK *lwork;
lwork = lstruct->lwork;
if (lwork == NULL) {
	thError("%s: ERROR - null (lwork) in (lstruct)", name);
	return(SH_GENERIC_ERROR);
	}

if (jtj == NULL && jtjpdiag == NULL && jtdmm == NULL && covT == NULL && 
	dX == NULL && dxT == NULL && chisq == NULL && cost == NULL && nx == NULL) {
	thError("%s: WARNING - null place holders, no output", name);
	return(SH_SUCCESS);
}

if (sector == PSECTOR) {
	if (jtj != NULL) *jtj = lwork->jtj;
	if (jtjpdiag != NULL) *jtjpdiag = lwork->jtjpdiag;
	if (jtdmm != NULL) *jtdmm = lwork->jtdmm;
	if (dX != NULL) *dX = lwork->dp;
	if (chisq != NULL) *chisq = lwork->chisq;
	if (cost != NULL) *cost = lwork->cost;
	if (nx != NULL) *nx = lwork->np;
	if (covT != NULL) *covT = lwork->covpT;
	if (dxT != NULL) *dxT = lwork->dpT;
} else if (sector == ASECTOR || sector == APSECTOR) {
	if (jtj != NULL) *jtj = lwork->jtjap;
	if (jtjpdiag != NULL) *jtjpdiag = lwork->jtjpdiagap;
	if (jtdmm != NULL) *jtdmm = lwork->jtdmmap;
	if (dX != NULL) *dX = lwork->da;
	if (chisq != NULL) *chisq = lwork->chisq;
	if (cost != NULL) *cost = lwork->cost;
	if (nx != NULL) {
		if (sector == ASECTOR) {
			*nx = lwork->nm;
		} else if (sector == APSECTOR) {
			*nx = lwork->nap;
		} else {
			thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
			return(SH_GENERIC_ERROR);
		}
	}
	if (dxT != NULL) *dxT = lwork->daT;
	if (covT != NULL) {
		if (sector == ASECTOR) {
			*covT = lwork->covaT;
		} else if (sector == APSECTOR) {
			*covT = lwork->covapT;
		} else {
			thError("%s: ERROR - unsupportd (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
			return(SH_GENERIC_ERROR);
		}
	}
} else {
	thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

int IsRecursionNeeded(LSTRUCT *lstruct, RET_CODE *status) {
char *name = "IsRecursionNeeded";
if (lstruct == NULL) {
	thError("%s: ERROR - null (lstruct)", name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(0);
}

CONVERGENCE flag;
RET_CODE status2;
flag = CheckLMConvergence(lstruct, CONVERGENCE_SECTOR, &status2);
if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not check convergence", name);
	if (status != NULL) *status = status2;
	return(0);
}
if (status != NULL) *status = SH_SUCCESS;

if (flag == CONTINUE) {
	return(1);
} else if (flag == UNKNOWN_CONVERGENCE || flag == N_CONVERGENCE) {
	if (status != NULL) *status = SH_GENERIC_ERROR;
}

return(0);
}


RET_CODE thInitLmethod(LSTRUCT *lstruct) {
char *name = "thInitLmethod";
LWORK *lwork;
LMETHOD *lmethod;
MLEFL **Jtj = NULL;
int np;
if (lstruct == NULL) { 
	thError("%s: ERROR - null (lstruct)", name);
	return(SH_GENERIC_ERROR);
	}
if ((lwork = lstruct->lwork) == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct) - null (lwork)", name);
	return(SH_GENERIC_ERROR);
	}
#if 0
if ((Jtj = lwork->jtj) == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct) - null (jtj) in (lwork)", name);
	return(SH_GENERIC_ERROR);
	}
#endif
if ((np = lwork->np) < 0) {
	thError("%s: ERROR - improperly allocated (lstruct) - (np = %d)", name, np);
	return(SH_GENERIC_ERROR);
	}
if ((lmethod = lstruct->lmethod) == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct) - null (lmethod)", name);
	return(SH_GENERIC_ERROR);
	}
RET_CODE status;
status = thLGetLambda0(Jtj, np, lmethod);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not initiate (lmethod)", name);
	return(status);
}

lmethod->type = LMETHOD_DEFAULT;
lmethod->cflag = CONTINUE;
lmethod->status = LM_STEP_ACCEPTED;
lmethod->wtype = LMETHODSUBTYPE_DEFAULT;

return(SH_SUCCESS);
}


RET_CODE thLGetLambda0(MLEFL **Jtj, int np, LMETHOD *lm) {

char *name = "thLGetLambda0";
if (np < 0) {
	thError("%s: ERROR - negative (np = %d)", name, np);
	return(SH_GENERIC_ERROR);
}
#if 0
if (Jtj == NULL || np <= 0) {
	thError("%s: ERROR - problematic (Jtj) or (np)", name);
	return(SH_GENERIC_ERROR);
	}
#endif

if (lm == NULL) {
	thError("%s: ERROR - null (lmethod)", name);
	return(SH_GENERIC_ERROR);
}

MLEFL tau;
tau = lm->tau;

if (tau <= 0) {
	thError("%s: ERROR - unacceptable value for (tau <= 0)", name);
	return(SH_GENERIC_ERROR);
}

#ifdef TAU_MIN

#ifdef TAU_MAX

if (tau < TAU_MIN || tau > TAU_MAX) {
	thWarn("%s: WARNING - (tau = %g) outside the suggested range (%g, %g)", name, tau, TAU_MIN, TAU_MAX);
	}

#else

if (tau < TAU_MIN) {
	thWarn("%s: WARNING - (tau = %g) outside of the suggested range (%g, INF)", name, tau, TAU_MIN);
	}

#endif

#else

#ifdef TAU_MAX

if (tau > TAU_MAX) {
	thWarn("%s: WARNING - (tau = %g) outside of the suggested range (0, %g)", name, tau, TAU_MAX);
	}

#endif
#endif

MLEFL l0;
#if 0

/* 
note that for this part to be effective, one needs to also generate J-matrices in the first call
*/
int i = 0;
MLEFL x, *jrow;
jrow = Jtj[i];
if (jrow == NULL) {
	thError("%s: ERROR - (Jtj) not allocated properly", name);
	return(SH_GENERIC_ERROR);
}
x = jrow[i];
if (x < (MLEFL) 0.0) {
	thError("%s: ERROR - negative diagonal value discovered in (Jtj[%d][%d] = %g)", name, i, i, (float) x);
	return(SH_GENERIC_ERROR);
}
l0 = x;

for (i = 1; i < np; i++) {
	jrow = Jtj[i];
	if (jrow == NULL) {
		thError("%s: ERROR - (Jtj) not allocated properly", name);
		return(SH_GENERIC_ERROR);
	}
	x = jrow[i];
	if (x < (MLEFL) 0.0) {
		thError("%s: ERROR - negative diagonal value discovered in (Jtj, i= %d)", name, i);
		return(SH_GENERIC_ERROR);
	}
	l0 = MAX(l0, x);
}

if (l0 <= (MLEFL) 0.0) {
	thError("%s: ERROR - unacceptable value in diagonal search of (Jtj)", name);
	return(SH_GENERIC_ERROR);
}
#endif

l0 = tau;
lm->lambda = l0;
return(SH_SUCCESS);
}

RET_CODE thMultiplyVariable(THPIX *x, THPIX y) {
char *name = "thMultiplyVariable";
if (x == NULL) {
	thError("%s: ERROR - null input, cannot multiply", name);
	return(SH_GENERIC_ERROR);
}
if (y <= (THPIX) 0.0) {
	thError("%s: ERROR - negative multiplier (y= %g)", name, (float) y);
	return(SH_GENERIC_ERROR);
}

THPIX xx;
xx = *x;
if (xx < (THPIX) 0.0) {
	thError("%s: ERROR - negative value for multiplier (x= %g)", name, (float) xx);
	return(SH_GENERIC_ERROR);
}

if (y > (THPIX) 1.0) {
	if ((double) THPIX_MAX / (double) y > (double) xx) {
		*x = (THPIX) ((double) xx * (double) y);
	} else {
		*x = THPIX_MAX;
	}
} else if (y < (THPIX) 1.0){ 
	if ((double) THPIX_MIN / (double) y < (double) xx) {
		*x = (THPIX) ((double) xx * (double) y);
	} else {
		*x = THPIX_MIN;
	}
}

return(SH_SUCCESS);
}

RET_CODE LMultiplyVariable(MLEFL *x, MLEFL y) {
char *name = "LMultiplyVariable";
if (x == NULL) {
	thError("%s: ERROR - null input, cannot multiply", name);
	return(SH_GENERIC_ERROR);
}
if (y <= (MLEFL) 0.0) {
	thError("%s: ERROR - negative multiplier (y= %g)", name, (float) y);
	return(SH_GENERIC_ERROR);
}

MLEFL xx;
xx = *x;
if (xx < (MLEFL) 0.0) {
	thError("%s: ERROR - negative value for multiplier (x= %g)", name, (float) xx);
	return(SH_GENERIC_ERROR);
}

if (y > (MLEFL) 1.0) {
	if ((double) MLEFL_MAX / (double) y > (double) xx) {
		*x = (MLEFL) ((double) xx * (double) y);
	} else {
		*x = MLEFL_MAX;
	}
} else if (y < (MLEFL) 1.0){ 
	if ((double) MLEFL_MIN / (double) y < (double) xx) {
		*x = (MLEFL) ((double) xx * (double) y);
	} else {
		*x = MLEFL_MIN;
	}
}

return(SH_SUCCESS);
}




RET_CODE thBuildImage(void *fparam, char *mname, char **rnames, int nmpar, LIMPACK *impack, RUNFLAG rflag) {

/* note: there should be a way to tell if a function is already calculated, especially for function which only take an amplitude and no nonlinear input parameter (like sky functions) this will be absolutely useful */

char *name = "thBuildImage";
#if THCLOCK
static int init_clk = 0;
static clock_t clk1, clk2;
if (!init_clk) clk1 = clock();
init_clk++;
#endif

if (mname == NULL || strlen(mname) == 0) {
	thError("%s: ERROR - no model name provided", name);
	return(SH_GENERIC_ERROR);
}
if (rnames == NULL && nmpar > 0) {
	thError("%s: ERROR - no (rnames) provided while (nmpar = %d)", name, nmpar);
	return(SH_GENERIC_ERROR);
}

if (impack == NULL) {
	thError("%s: WARNING - null (impack), no output to be generated", name);
	return(SH_SUCCESS);
	} 

if (rflag != MEMORY_ESTIMATE && rflag != INITIMAGES && rflag != INITIMAGES_AND_DM && 
	rflag != IMAGES && rflag != IMAGES_AND_DM && rflag != DM) {
	thError("%s: ERROR - unsupported (rflag) 'RUNFLAG'", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
int i;
THREGION *reg;
REGION *lreg;
void *ptr_f = NULL, *ptr_Df = NULL;

/* 
RET_CODE (*ptr_image_f) (void *, REGION *, THPIX *);
RET_CODE (*ptr_image_Df) (void *, REGION *);
*/

RET_CODE (*ptr_image_f) (void *, THREGION *, THPIX *, int) = NULL;
RET_CODE (*ptr_image_Df) (void *, THREGION *, int) = NULL;
RET_CODE (*specify_region) (void *, PSF_CONVOLUTION_INFO *, THREGION *, int, int) = NULL;
char *rname;

MODEL_ELEM *me;
status = thModelElemGetByName(mname, &me);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (model_elem) for '%s'", name, mname);
	return(status);
}

TYPE ptype;
char *pname;
void *init, *reg_generator, *psf_info_generator, *psf_mask_generator, *amplitude_specifier;
CONSTRUCTION_MODE cmode;
THREGION_TYPE regtype;
PSF_CONVOLUTION_TYPE convtype;
int ncomponent;

status = thModelElemGet(me, NULL, &ptype, &pname, 
		&init, 
		&reg_generator, 
		&psf_info_generator, 
		&psf_mask_generator, &amplitude_specifier, 
		&cmode, &regtype, &convtype, 
		&ncomponent); 

if (cmode != WHOLEREGION && cmode != PIXELINTEGRAL) {
	thError("%s: ERROR - construction mode not supported for model '%s'", name, mname);
	return(SH_GENERIC_ERROR);
}

if (reg_generator == NULL) {
	thError("%s: ERROR - null region generator and specifier for model '%s'", 
		name, mname);	
	return(SH_GENERIC_ERROR);
}

char *rflagname = shEnumNameGetFromValue("RUNFLAG", rflag);
/* note that because the ldump structure in capture package are only for model creation purposes and do not have any nonlinear terms, successive call of model simulation algorithm can result in mask - region discrepancy because the shape parameters of some of the objects which are fit nonlinearly elsewhere might change without ldump knowing about it */
#if DO_CAPTURE || DUMP_PROF_INTO_GPOBJC || DUMP_PROF
int reprefab = 1;
#else
int reprefab = (impack->nmpar > 0);
#endif
specify_region = reg_generator;

#if VERY_DEEP_DEBUG_MLE
printf("%s: working with model '%s' at (runflag = '%s') \n", name, mname, rflagname);
#endif

PSF_CONVOLUTION_INFO *psf_info = NULL;
FABRICATION_FLAG fab;
int index, ncomputation = 0;
for (index = 0; index < ncomponent; index++) { /* this for loop should be inserted inside
						 each of the following loops */
	status = thModelElemGetFunc(me, (void **) &ptr_f);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not find a function to generate model '%s'", 
		name, mname);
		return(status);
	}
	if (rflag == MEMORY_ESTIMATE ||rflag == INITIMAGES_AND_DM || rflag == INITIMAGES || 
		(rflag == IMAGES && reprefab)) {
		MLEFL *mcount;
		reg = impack->mreg[0];
		mcount = impack->mcount;
		/* the following gets psf info to use to it guess the margin needed to convolves with a proper PSF */	
		status = thRegGetPsfConvInfo(reg, &psf_info);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (psf_info) from (reg) in model '%s' at (runflag = '%s')", name, mname, rflagname);
			return(status);
		}
		RET_CODE (*psf_info_update)(void *, PSF_CONVOLUTION_INFO *);
		if (psf_info_generator != NULL) {
			psf_info_update = psf_info_generator;
			status = psf_info_update(fparam, psf_info);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not update (psf_info) for model '%s' at (runflag = '%s') ", name, mname, rflagname);
				return(status);
			}
		}
	
		THPIX mcount_thpix = VALUE_IS_BAD;
		if (cmode == WHOLEREGION) {	
			ptr_image_f = ptr_f;
			#if VERY_DEEP_DEBUG_MLE
			printf("%s: flagging image for model '%s' component (%d) at (runflag = '%s') \n", name, mname, index, rflagname);
			#endif
			status = specify_region(fparam, psf_info, reg, index, reprefab);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not flag the image for model '%s' component (%d) for (runflag = '%s')", name, mname, index, rflagname);
				return(status);
			}
			if (rflag != MEMORY_ESTIMATE) {
				status = thRegGetRegMaskA(reg, &lreg, NULL, NULL, &fab, index);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not get working region for index (%d) of model '%s' at (runflag = '%s')", 
						name, index, mname, rflagname);
					return(status);
				}	
				status = ptr_image_f(fparam, reg, &mcount_thpix, index);
				if (mcount != NULL) *mcount = (MLEFL) mcount_thpix;
				ncomputation++;
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - wholesale generation of image failed for component (%d) of model '%s' at (runflag = '%s')",
						name, index, mname, rflagname);
					return(status);
				}
			}
		} else if (cmode == PIXELINTEGRAL) {
			specify_region = reg_generator;
			if (specify_region == NULL) {
				thError("%s: ERROR - null region generator in creation of component (%s) of model '%s' via pixel integration",
					name, index, mname);
				return(SH_GENERIC_ERROR);
			}
			status = specify_region(fparam, psf_info, reg, index, reprefab);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not specify a region for pixel integration of component (%d) of model '%s'", 
				name, index, mname);
				return(status);
			}
			if (rflag != MEMORY_ESTIMATE) {
				status = thRegGetRegMaskA(reg, &lreg, NULL, NULL, &fab, index);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not get working region for index (%d) of model '%s'", 
						name, index, mname);
					return(status);
				} 
				status = thGenericPixelIntegrator(fparam, ptr_f, lreg, &mcount_thpix);
				if (mcount != NULL) *mcount = (MLEFL) mcount_thpix;
				ncomputation++;
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - pixel integration failed for component (%d) of model '%s'", 
						name, index, mname);
					return(status);
				}
			}	
		}
	}
}	

if (rflag == MEMORY_ESTIMATE) {
	status = thRegPrefabFinalize(reg);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not finalize PREFAB for main image in model '%s' at (runflag = '%s')", name, mname, rflagname);
		return(status);
	}
} else if (ncomputation != 0) {
	status = thRegFabFinalize(reg);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not finalize fabrication of (thregion) for model '%s' at (runflag = '%s')", name, mname, rflagname);
		return(status);
	}
	status = thRegGetPsfConvInfo(reg, &psf_info);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (psf_info) from (reg) in model '%s' at (runflag = '%s')", name, mname, rflagname);
		return(status);
	}
	RET_CODE (*psf_info_update)(void *, PSF_CONVOLUTION_INFO *);
	if (psf_info_generator != NULL) {
		psf_info_update = psf_info_generator;
		status = psf_info_update(fparam, psf_info);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not update (psf_info) for model '%s' at (runflag = '%s') ", name, mname, rflagname);
			return(status);
		}
	}
	status = thRegConvolveWithPsf(reg);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not convolve image for model '%s' with PSF at (runflag = '%s')", 
			name, mname, rflagname);
		return(status);
	}	
	ncomputation = 0;
}

if (rflag != IMAGES && rflag != INITIMAGES) {
	for (i = 0; i < nmpar; i++) {
		rname = rnames[i];
		if (rname == NULL) rname = "";
		status = thModelElemGetDeriv(me, rname, (void **) &ptr_Df);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not find the function to generate derivative of model '%s' component (%d) w.r.t. variable '%s'", 
			name, mname, index, rname);
			return(status);
		}
		if (ptr_Df == NULL) {
			thError("%s: ERROR - null derivative function for model '%s' component (%d) w.r.t. variable '%s'", name, mname, index, rname);
			return(SH_GENERIC_ERROR);
		}
		reg = impack->mreg[i + 1];
		status = thRegGetPsfConvInfo(reg, &psf_info);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (psf_info) from (reg) in model '%s' at (runflag = '%s')", name, mname, rflagname);
			return(status);
		}
		for (index = 0; index < ncomponent; index++) {
			if (cmode == WHOLEREGION) {	
				ptr_image_Df = ptr_Df;
				status = specify_region(fparam, psf_info, reg, index, reprefab);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not flag the image for derivative of model '%s' component (%d) w.r.t. variable '%s'", 
						name, mname, index, rname);
					return(status);
				}
				if (rflag != MEMORY_ESTIMATE) {
					status = thRegGetRegMaskA(reg, &lreg, NULL, NULL, &fab, index);
					if (status != SH_SUCCESS) {
						thError("%s: ERROR - could not get working region for index (%d) of derivative of  model '%s' w.r.t. '%s'", 
						name, index, mname, rname);
						return(status);
					} 
					status = ptr_image_Df(fparam, reg, index);
					if (status != SH_SUCCESS) {
						thError("%s: ERROR - wholesale generation of image for component (%d) of derivative of model '%s' w.r.t. '%s' failed",
							name, index, mname, rname);
						return(status);
					} 
					ncomputation++;
				}
			} else if (cmode == PIXELINTEGRAL) {
				specify_region = reg_generator;
				if (specify_region == NULL) {
					thError("%s: ERROR - null region generator in creation of component (%s) of model '%s' via pixel integration",
						name, index, mname);
					return(SH_GENERIC_ERROR);
				}
				status = specify_region(fparam, psf_info, reg, index, reprefab);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not specify a region for pixel integration of component (%d) of model '%s'", 
					name, index, mname);
					return(status);
				}
				if (rflag != MEMORY_ESTIMATE) {
					status = thRegGetRegMaskA(reg, &lreg, NULL, NULL, &fab, index);
					if (status != SH_SUCCESS) {
						thError("%s: ERROR - could not get working region for index (%d) of model '%s'", 
						name, index, mname);
						return(status);
					} 
					status = thGenericPixelIntegrator(fparam, ptr_Df, lreg, NULL);
					if (status != SH_SUCCESS) {
						thError("%s: ERROR - pixel integration failed for component (%d) of derivative for model '%s' w.r.t. '%s'", 
							name, index, mname, rname);
						return(status);
					}
					ncomputation++;
				}
			}
		}
		if (rflag == MEMORY_ESTIMATE) {
			status = thRegPrefabFinalize(reg);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not finalize PREFAB for derivative of model '%s' w.r.t. '%s'", 
					name, mname, rname);
				return(status);
			}
		} else if (ncomputation != 0) {
			status = thRegFabFinalize(reg);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not finalize fabrication of (thregion) for derivative of model '%s' w.r.t. '%s'", name, mname, rname);
				return(status);
			}
			status = thRegGetPsfConvInfo(reg, &psf_info);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get (psf_info) from (reg) in derivative of model '%s' w.r.t. '%s'", name, mname, rname);
				return(status);
			}
			if (psf_info_generator != NULL) {
				RET_CODE (*psf_info_update)(void *, PSF_CONVOLUTION_INFO *);
				psf_info_update = psf_info_generator;
				status = psf_info_update(fparam, psf_info);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not update (psf_info) for deriv. of model '%s' w.r.t. '%s'", name, mname, rname);
					return(status);
				}
			}
			status = thRegConvolveWithPsf(reg);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not convolved the image for the derivative of model '%s' w.r.t. '%s'", name, mname, rname);
				if (!strcmp(mname, "coresersic")) {
					simple_print_model_pars(fparam, "CORESERSICPARS");
				} else if (!strcmp(mname, "sersic") || !strcmp(mname, "sersic1") || !strcmp(mname, "sersic2")) {
					simple_print_model_pars(fparam, "SERSICPARS");
				} else if (!strcmp(mname, "deV") || !strcmp(mname, "deV1") || !strcmp(mname, "deV2")) {
					simple_print_model_pars(fparam, "DEVPARS");
				} else if (!strcmp(mname, "Exp") || !strcmp(mname, "Exp1") || !strcmp(mname, "Exp2")) {
					simple_print_model_pars(fparam, "EXPPARS");
				}
				return(status);
			} 
			ncomputation = 0;
		}
	}
}

#if MLE_CONDUCT_HIGHER_ORDER
void  *ptr_DDf = NULL;
RET_CODE (*ptr_image_DDf) (void *, THREGION *, int) = NULL;

if (rflag != IMAGES && rflag != INITIMAGES) {
	for (i = 0; i < nmpar; i++) {
	int j;
	for (j = 0; j <= i; j++) {

		reg = impack->DDmreg[i][j];
		status = thRegGetPsfConvInfo(reg, &psf_info);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (psf_info) from (reg) in model '%s' at (runflag = '%s')", name, mname, rflagname);
			return(status);
		}
		char *rname1 = rnames[i];
		char *rname2 = rnames[j];
		status = thModelElemGetDeriv2(me, rname1, rname2, (void **) &ptr_DDf);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not find DDf to generate '%s' comp(%d) wrt. vars ('%s','%s')", 
			name, mname, index, rname1, rname2);
			return(status);
		}
		if (ptr_DDf != NULL) {
		for (index = 0; index < ncomponent; index++) {
			if (cmode == WHOLEREGION) {	
				ptr_image_DDf = ptr_DDf;
				status = specify_region(fparam, psf_info, reg, index, reprefab);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not flag image for deriv. of model '%s' comp (%d) wrt. '%s', '%s'", 
						name, mname, index, rname1, rname2);
					return(status);
				}
				if (rflag != MEMORY_ESTIMATE) {
					status = thRegGetRegMaskA(reg, &lreg, NULL, NULL, &fab, index);
					if (status != SH_SUCCESS) {
						thError("%s: ERROR - could not get working region for index (%d) of deriv of model '%s' wrt. ('%s','%s')", 
						name, index, mname, rname1, rname2);
						return(status);
					} 
					status = ptr_image_DDf(fparam, reg, index);
					if (status != SH_SUCCESS) {
						thError("%s: ERROR - wholesale generation of image for comp (%d) of deriv of model '%s' wrt. ('%s', '%s') failed",
							name, index, mname, rname1, rname2);
						return(status);
					} 
					ncomputation++;
				}
			} else if (cmode == PIXELINTEGRAL) {
				specify_region = reg_generator;
				if (specify_region == NULL) {
					thError("%s: ERROR - null region generator in creation of component (%s) of model '%s' via pixel integration",
						name, index, mname);
					return(SH_GENERIC_ERROR);
				}
				status = specify_region(fparam, psf_info, reg, index, reprefab);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not specify a region for pixel integration of component (%d) of model '%s'", 
					name, index, mname);
					return(status);
				}
				if (rflag != MEMORY_ESTIMATE) {
					status = thRegGetRegMaskA(reg, &lreg, NULL, NULL, &fab, index);
					if (status != SH_SUCCESS) {
						thError("%s: ERROR - could not get working region for index (%d) of model '%s'", 
						name, index, mname);
						return(status);
					} 
					status = thGenericPixelIntegrator(fparam, ptr_DDf, lreg, NULL);
					if (status != SH_SUCCESS) {
						thError("%s: ERROR - pixel integration failed for component (%d) of derivative for model '%s' w.r.t. ('%s', '%s')", 
							name, index, mname, rname1, rname2);
						return(status);
					}
					ncomputation++;
				}
			} else {
				thError("%s: ERROR - unsupported (cmode = %d, '%s')", name, cmode, shEnumNameGetFromValue("CONSTRUCTION_MODE", cmode));
				return(SH_GENERIC_ERROR);
			}
		}
		} else {
		for (index = 0; index < ncomponent; index++) {
			if (cmode == WHOLEREGION || cmode == PIXELINTEGRAL) {	
				status = NullGalaxySpecifyRegion(fparam, psf_info, reg, index, reprefab);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not flag null image for deriv. of model '%s' comp (%d) wrt. '%s', '%s'", 
						name, mname, index, rname1, rname2);
					return(status);
				}
				if (rflag != MEMORY_ESTIMATE) {
					status = thRegGetRegMaskA(reg, &lreg, NULL, NULL, &fab, index);
					if (status != SH_SUCCESS) {
						thError("%s: ERROR - could not get working region for index (%d) of deriv of model '%s' wrt. ('%s','%s')", 
						name, index, mname, rname1, rname2);
						return(status);
					} 
					status = nullProf(fparam, reg, index);
					if (status != SH_SUCCESS) {
						thError("%s: ERROR - generation of null image for component (%d) of deriv. of model '%s' wrt. ('%s','%s') failed",
							name, index, mname, rname1, rname2);
						return(status);
					} 
					ncomputation++;
				}
			} else {
				thError("%s: ERROR - unsupported (cmode = %d, '%s')", name, cmode, shEnumNameGetFromValue("CONSTRUCTION_MODE", cmode));
				return(SH_GENERIC_ERROR);
			}
		}
		}
		if (rflag == MEMORY_ESTIMATE) {
			status = thRegPrefabFinalize(reg);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not finalize PREFAB for derivative of model '%s' w.r.t. ('%s', '%s')", 
					name, mname, rname1, rname2);
				return(status);
			}
		} else if (ncomputation != 0) {
			status = thRegFabFinalize(reg);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not finalize fabrication of (thregion) for derivative of model '%s' w.r.t. ('%s', '%s')", name, mname, rname1, rname2);
				return(status);
			}
			if (ptr_DDf != NULL) { /* do not convolve with PSF is second derivative image is not created */
				status = thRegGetPsfConvInfo(reg, &psf_info);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not get (psf_info) from (reg) in derivative of model '%s' w.r.t. ('%s', '%s')", name, mname, rname1, rname2);
					return(status);
				}	 
				if (psf_info_generator != NULL) {
					RET_CODE (*psf_info_update)(void *, PSF_CONVOLUTION_INFO *);
					psf_info_update = psf_info_generator;
					status = psf_info_update(fparam, psf_info);
					if (status != SH_SUCCESS) {
						thError("%s: ERROR - could not update (psf_info) for deriv. of model '%s' w.r.t. ('%s', '%s')", name, mname, rname1, rname2);
						return(status);
					}
				}
				status = thRegConvolveWithPsf(reg);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not convolved the image for the derivative of model '%s' w.r.t. ('%s', '%s')", name, mname, rname1, rname2);
					return(status);
				}	 
			} else {
				status = thRegConvolveWithNullPsf(reg);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not formally convolve deriv of model '%s' wrt. ('%s', '%s')", 
					name, mname, rname1, rname2);
					return(status);
				}
			}
			ncomputation = 0;
		}
	}
	}
}

#endif

if (rflag != MEMORY_ESTIMATE) {
	status = thPsfInfoCleanup(psf_info);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not clean-up (psf_info)", name);
		return(status);
	}
}

#if THCLOCK
clk2 = clock();
printf("%s: CLOCKING - [%g] CPUs \n", name, ((float) (clk2 - clk1)) / CLOCKS_PER_SEC);
#endif

#if DEBUG_MLE_ALGORITHM
FABRICATION_FLAG fab_out = UNKNOWN_FABRICATION_FLAG;
status = thRegGetFabFlag(impack->mreg[0], &fab_out);
printf("%s: fab flag of mreg[0] = '%s' \n", name, shEnumNameGetFromValue("FABRICATION_FLAG", fab_out));
#endif

return(SH_SUCCESS);
}

RET_CODE thGenericPixelIntegrator (void * q, void *f, REGION *reg, THPIX *mcount) {
char *name = "thGenericPixelIntegrator";
thError("%s: ERROR - pixel functions not supported at this stage", name);
return(SH_GENERIC_ERROR);
}


RET_CODE UploadModelToMemory(int imodel, LSTRUCT *lstruct) {

char *name = "UploadModelToMemory";
RET_CODE status = SH_SUCCESS;

/* first checking if the following has been allocated 
 | m_i > 
 | m_i_j > 
*/



/* 
   Making Images and Their Derivatives:

   1. construct an image package for each object / model / amplitude {m_i, m_i_j} 
	- have in mind to only include parameters which are relevant to the model 
   2. submit IMAGE PACKAGE along with the PAR MACHINE and the FUNCTION PACKAGE to the builder
*/

int nmodel;
status = thLstructGetNModel(lstruct, &nmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the number of models", name);
	return(status);
}
if (imodel >= nmodel || imodel < 0) {
	thError("%s: ERROR - unacceptable (imodel = %d) where (nmodel = %d)", 
		name, imodel, nmodel);
	return(SH_GENERIC_ERROR);
}
LMACHINE *lmachine;
status = thLstructGetLmachine(lstruct, &lmachine);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - coul not retrieve (lmachine) from (lstruct)", name);
	return(status);
}
int nmpar;
char *mname, **rnames;

mname = lmachine->mnames[imodel];
rnames = lmachine->enames[imodel];
nmpar = lmachine->nmpar[imodel];

/* dump pN values in model variables  */
/* the following should be done outside of this function, before executing the algorithn 
status = thLDumpPar(lstruct, XNTOMODEL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump (pN) onto (model)", name);
	return(status);
}
*/

/* instead of a loop over models, it should be a loop over algorithm elements */
LIMPACK **impacks = NULL;
status = thLstructGetImpacks(lstruct, &impacks);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get list of (impack)s", name);
	return(status);
}
LIMPACK *impack;
impack = impacks[0];
/* old school - contributes to fragmentation oct 17, 2012
if (impack == NULL) impack = thLimpackNew();
*/
status = thConstructImagePack(lstruct, imodel, impack);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not construct the image pack for model (%d) '%s'", 
		name, imodel, mname);	
	return(status);
}
void *fparam;
status = thGetFParam(lmachine, imodel, &fparam);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not construct a parameter of the type needed for model (%d) '%s'", name, imodel, mname);		
	return(status);
}
/* RFLAG in the following command is set such that the object is 
definitely generated */
status = thBuildImage(fparam, mname, rnames, nmpar, impack, INITIMAGES_AND_DM);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not build the image(s) for model (%d) '%s' (rflag = 'INIT_IMAGES_AND_DM')", name, imodel, mname);
	return(status);
}

int ret, free_to_os = 1;
ret = shMemDefragment(free_to_os);
if (ret != 0) {
	thError("%s: ERROR - problem defragmenting memory", name);
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE KillAllModelsInMemory(LSTRUCT *lstruct) {

char *name = "KillAllModelsInMemory";
RET_CODE status = SH_SUCCESS;

/* 
   Killing All Images and Their Derivatives in the memory

   1. construct an image package for each object / model / amplitude {m_i, m_i_j} 
	- have in mind to only include parameters which are relevant to the model 
   2. submit IMAGE PACKAGE along with the PAR MACHINE and the FUNCTION PACKAGE to the builder
*/

int nmodel;
status = thLstructGetNModel(lstruct, &nmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the number of models", name);
	return(status);
}

LMACHINE *lmachine;
status = thLstructGetLmachine(lstruct, &lmachine);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - coul not retrieve (lmachine) from (lstruct)", name);
	return(status);
}

int imodel;
for (imodel = 0; imodel < nmodel; imodel++) {

	char *mname;
	mname = lmachine->mnames[imodel];
	/* instead of a loop over models, it should be a loop over algorithm elements */
	LIMPACK **impacks = NULL;
	status = thLstructGetImpacks(lstruct, &impacks);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get list of (impack)s", name);
		return(status);
	}

	LIMPACK *impack;
	impack = impacks[0];

	status = thConstructImagePack(lstruct, imodel, impack);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not construct the image pack for model (%d) '%s'", 
			name, imodel, mname);	
		return(status);
	}

	status = thFreeImpackMemory(impack);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not free memory for model (%d) '%s'", 
			name, imodel, mname);
		return(status);
	}

}

int ret, free_to_os = 1;
ret = shMemDefragment(free_to_os);
if (ret != 0) {
	thError("%s: ERROR - problem defragmenting memory", name);
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE KillModelInMemory(int imodel, LSTRUCT *lstruct) {

char *name = "KillModelInMemory";
RET_CODE status = SH_SUCCESS;

/* 
   Killing Images and Their Derivatives in the memory

   1. construct an image package for each object / model / amplitude {m_i, m_i_j} 
	- have in mind to only include parameters which are relevant to the model 
   2. submit IMAGE PACKAGE along with the PAR MACHINE and the FUNCTION PACKAGE to the builder
*/

int nmodel;
status = thLstructGetNModel(lstruct, &nmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the number of models", name);
	return(status);
}
if (imodel >= nmodel || imodel < 0) {
	thError("%s: ERROR - unacceptable (imodel = %d) where (nmodel = %d)", 
		name, imodel, nmodel);
	return(SH_GENERIC_ERROR);
}

LMACHINE *lmachine;
status = thLstructGetLmachine(lstruct, &lmachine);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - coul not retrieve (lmachine) from (lstruct)", name);
	return(status);
}

char *mname;
mname = lmachine->mnames[imodel];
/* instead of a loop over models, it should be a loop over algorithm elements */
LIMPACK **impacks = NULL;
status = thLstructGetImpacks(lstruct, &impacks);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get list of (impack)s", name);
	return(status);
}
LIMPACK *impack;
impack = impacks[0];
/* old school - contributes to memory fragmentation - oct 17, 2012 
if (impack == NULL) impack = thLimpackNew();
*/
status = thConstructImagePack(lstruct, imodel, impack);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not construct the image pack for model (%d) '%s'", 
		name, imodel, mname);	
	return(status);
}

status = thFreeImpackMemory(impack);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not free memory for model (%d) '%s'", 
		name, imodel, mname);
	return(status);
}

int ret, free_to_os = 1;
ret = shMemDefragment(free_to_os);
if (ret != 0) {
	thError("%s: ERROR - problem defragmenting memory", name);
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE DoInnerProduct(int imodel1, int imodel2, LSTRUCT *lstruct) {
char *name = "DoInnerProduct";
RET_CODE status;
if (imodel1 == DATA_NODE && imodel2 == DATA_NODE) {
	status = DoDDInnerProduct(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not do the data-data inner product", name);
		return(status);
	}
	return(SH_SUCCESS);
} else if (imodel1 == DATA_NODE || imodel2 == DATA_NODE) {
	int regular_node;
	regular_node = imodel1;
	if (imodel1 == DATA_NODE) regular_node = imodel2;
	status = DoMDInnerProduct(regular_node, lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not perform data-model (%d) inner product", name, regular_node);
		return(status);
	}
	return(SH_SUCCESS);
} else {
	status = DoMMInnerProduct(imodel1, imodel2, lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not perform model(%d)-model(%d) inner product", name, imodel1, imodel2);
		return(status);
	}
	return(SH_SUCCESS);
}

}

RET_CODE DoMMInnerProduct(int imodel1, int imodel2, LSTRUCT *lstruct) {
char *name = "DoMMInnerProduct";

int nmodel, nparam;
RET_CODE status;
status = thLstructGetNModel(lstruct, &nmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the number of models", name);
	return(status);
}
status = thLstructGetNPar(lstruct, &nparam);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not extract (npar) from (lstruct)", name);
        return(status);
}
if (imodel1 >= nmodel || imodel1 < 0) {
	thError("%s: ERROR - unacceptable (imodel1 = %d) where (nmodel = %d)", 
		name, imodel1, nmodel);
	return(SH_GENERIC_ERROR);
}
if (imodel2 >= nmodel || imodel2 < 0) {
	thError("%s: ERROR - unacceptable (imodel2 = %d) where (nmodel = %d)", 
		name, imodel2, nmodel);
	return(SH_GENERIC_ERROR);
}

LMACHINE *lmachine;
status = thLstructGetLmachine(lstruct, &lmachine);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - coul not retrieve (lmachine) from (lstruct)", name);
	return(status);
}
int nmpar1, nmpar2;
char *mname1, *mname2, **rnames1, **rnames2;

mname1 = lmachine->mnames[imodel1];
rnames1 = lmachine->enames[imodel1];
nmpar1 = lmachine->nmpar[imodel1];

mname2 = lmachine->mnames[imodel2];
rnames2 = lmachine->enames[imodel2];
nmpar2 = lmachine->nmpar[imodel2];

int **invploc = lmachine->invploc;

MLEFL **mimj, ***mimjk, ****mijmkl;
status = thExtractBrackets(lstruct, NULL, NULL, &mimj, NULL, &mimjk, &mijmkl);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract necessary matrices", name);
	return(status);
}

THREGION *a1 = NULL, *a2 = NULL;

/*
       < m_i | W | m_j >        for DDL / (Da Da)
     < m_k_i | W | m_l_j >      for DDL / (Dp Dp)
       < m_i | W | m_k_j >      for DDL / (Da Dp)
*/

LWMASK *lwmask = NULL;
status = thLstructGetLwmask(lstruct, &lwmask);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwmask) from (lstruct)", name);
	return(status);
}

status = thExtractMi(lstruct, imodel1, &a1);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract model (%d) '%s' of %d", 
		name, imodel1, mname1, nmodel);
	return(status);
}
status = thExtractMi(lstruct, imodel2, &a2);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract model (%d) '%s' of %d", 
		name, imodel2, mname2, nmodel);
	return(status);
}
mimj[imodel1][imodel2] = thRegBracket(a1, lwmask, a2, &status);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calculate the image bracket for <m_%d|W|m_%d>", 
		name, imodel1, imodel2);
	return(status);
}
#if DEEP_DEBUG_MLE
if (imodel1 == imodel2 && imodel1 <= 10) {
	printf("%s: self-dot product (imodel = %d), < m | m > = %G \n", name, imodel1, (double) mimj[imodel1][imodel2]);
}
#endif

#if MLE_CATCH_NAN
if (isinf(mimj[imodel1][imodel2]) || isnan(mimj[imodel1][imodel2])) {
	thError("%s: ERROR - (NAN) value at < m_%d | m_%d > = < %s | %s >", name, imodel1, imodel2, mname1, mname2);
	return(SH_GENERIC_ERROR);
}
#endif

mimj[imodel2][imodel1] = mimj[imodel1][imodel2];

int nmp, j1, j2, k1, k2;
nmp = nmodel * nparam;

status = thExtractMi(lstruct, imodel1, &a1);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract model (%d) '%s' of %d", 
	name, imodel1, mname1, nmodel);
	return(status);
}
for (j2 = 0; j2 < nparam; j2++) {
	k2 = invploc[j2][imodel2];
	char *rname2 = NULL;
	if (k2 != BAD_INDEX) rname2 = rnames2[k2];
	status = thExtractMij(lstruct, imodel2, j2, &a2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract m_%d_%d", name, imodel2, j2);
		return(status);
	}
	MLEFL z = 0.0;	
	if (a1 != NULL && a2 != NULL) {
		z = thRegBracket(a1, lwmask, a2, &status);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not calculate the image bracket for <m_%d|W|m_%d_%d>", name, imodel1, imodel2, j2);
			return(status);
		}
	}
	#if MLE_CATCH_NAN
	if (isinf(z) || isnan(z)) {
		thError("%s: ERROR - (NAN) value at < m_%d | m_%d_%d > = < %s | D%s/D%s > ", name, imodel1, imodel2, j2, mname1, mname2, rname2);
		return(SH_GENERIC_ERROR);
	}
	#endif
	#if OLD_MAP
		mimjk[imodel1][imodel2][j2] = z;
	#else
	#if VERY_DEEP_DEBUG_MLE
	printf("%s: < m[%d][%d] | m[%d] > = %g \n", name, imodel2, j2, imodel1, (float) z);
	#endif
	status = thLMijMkPut(lstruct, imodel2, j2, imodel1, z);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put value for (mimjk) for (%d), (%d, %d)", name, imodel1, imodel2, j2);
		return(status);
	}
	#endif
}

if (imodel1 != imodel2) {

status = thExtractMi(lstruct, imodel2, &a2);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract model (%d) '%s' of %d", 
		name, imodel2, mname2, nmodel);
	return(status);
}
for (j1 = 0; j1 < nparam; j1++) {
	k1 = invploc[j1][imodel1];
	char *rname1 = NULL;
	if (k1 != BAD_INDEX) rname1 = rnames1[k1];
	status = thExtractMij(lstruct, imodel1, j1, &a1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract m_%d_%d", name, imodel1, j1);
		return(status);
	}
	MLEFL z = 0.0;	
	if (a1 != NULL && a2 != NULL) {
		z = thRegBracket(a1, lwmask, a2, &status);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not calculate the image bracket for <m_%d|W|m_%d_%d>", name, imodel2, imodel1, j1);
			return(status);
		}
	}
	#if MLE_CATCH_NAN
	if (isnan(z) || isinf(z)) {
		thError("%s: ERROR - (NAN) value at < m_%d | m_%d_%d > = < %s | D %s / D %s >", name, imodel2, imodel1, j1, mname2, mname1, rname1);
		return(SH_GENERIC_ERROR);
	}
	#endif
	#if OLD_MAP
		mimjk[imodel2][imodel1][j1] = z;
	#else
	
		#if VERY_DEEP_DEBUG_MLE	
		printf("%s: < m[%d][%d] | m[%d] > = %g \n", name, imodel1, j1, imodel2, (float) z);
		#endif
		status = thLMijMkPut(lstruct, imodel1, j1, imodel2, z);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not put value for (mimjk) for (%d), (%d, %d)", name, imodel2, imodel1, j1);
			return(status);
		}
	#endif
}

}

if (imodel1 != imodel2) {

for (j1 = 0; j1 < nparam; j1++) {
	k1 = invploc[j1][imodel1];
	char *rname1 = NULL;
	if (k1 >= 0) rname1 = rnames1[k1];
	status = thExtractMij(lstruct, imodel1, j1, &a1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract m_%d_%d", 
			name, imodel1, j1);
		return(status);
	}
	for (j2 = 0; j2 < nparam; j2++) {
		k2 = invploc[j2][imodel2];
		char *rname2 = NULL;
		if (k2 != BAD_INDEX) rname2 = rnames2[k2];
		status = thExtractMij(lstruct, imodel2, j2, &a2);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not extract m_%d_%d", 
				name, imodel2, j2);
			return(status);
		}
		MLEFL z = 0.0;
		if (a1 != NULL && a2 != NULL) {
			z = thRegBracket(a1, lwmask, a2, &status);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not calculate the image bracket for <m_%d_%d|W|m_%d_%d>", name, imodel1, j1, imodel2, j2);
				return(status);
			}
		}
		#if MLE_CATCH_NAN
		if (isinf(z) || isnan(z)) {
			thError("%s: ERROR - (NAN) value at < m_%d_%d | m_%d_%d > = < D %s / D %s | D %s / D %s > ",
				name, imodel1, j1, imodel2, j2, mname1, rname1, mname2, rname2);
			return(SH_GENERIC_ERROR);
		}
		#endif
		#if OLD_MAP
		mijmkl[imodel1][j1][imodel2][j2] = z;
		mijmkl[imdoel2][j2][imodel1][j1] = z;
		#else
		status = thLMijMklPut(lstruct, imodel1, j1, imodel2, j2, z);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not insert (mijmkl) for (%d, %d), (%d, %d)", name, imodel1, j1, imodel2, j2);
			return(status);
		}
		status = thLMijMklPut(lstruct, imodel2, j2, imodel1, j1, z);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not insert (mklmij) for (%d, %d), (%d, %d)", name, imodel2, j2, imodel1, j1);
			return(status);
		}
		#endif
	}
}

} else {

for (j1 = 0; j1 < nparam; j1++) {
	k1 = invploc[j1][imodel1];
	char *rname1 = NULL;
	if (k1 != BAD_INDEX) rname1 = rnames1[k1];
	status = thExtractMij(lstruct, imodel1, j1, &a1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract m_%d_%d", name, imodel1, j1);
		return(status);
	}
	for (j2 = 0; j2 <= j1; j2++) {
		k2 = invploc[j2][imodel2];
		char *rname2 = NULL;
		if (k2 != BAD_INDEX) rname2 = rnames2[k2];
		status = thExtractMij(lstruct, imodel2, j2, &a2);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not extract m_%d_%d", name, imodel2, j2);
			return(status);
		}
		MLEFL z = 0.0;
		if (a1 != NULL && a2 != NULL) {
			z = thRegBracket(a1, lwmask, a2, &status);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not calculate the image bracket for <m_%d_%d|W|m_%d_%d>", name, imodel1, j1, imodel2, j2);
				return(status);
			}
		}
		#if MLE_CATCH_NAN
		if (isnan(z) || isinf(z)) {
			thError("%s: ERROR - (NAN) value at <m_%d_%d | m_%d_%d> = < D %s / D %s | D %s / D %s > ",
				name, imodel1, j1, imodel2, j2, mname1, rname1, mname2, rname2);
			return(SH_GENERIC_ERROR);
		}
		#endif
		#if OLD_MAP
		mijmkl[imodel1][j1][imodel2][j2] = z;
		mijmkl[imdoel2][j2][imodel1][j1] = z;
		#else
		status = thLMijMklPut(lstruct, imodel1, j1, imodel2, j2, z);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not insert (mijmkl) for (%d, %d), (%d, %d)", name, imodel1, j1, imodel2, j2);
			return(status);
		}
		if (j1 != j2) {
			status = thLMijMklPut(lstruct, imodel2, j2, imodel1, j1, z);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not insert (mklmij) for (%d, %d), (%d, %d)", name, imodel2, j2, imodel1, j1);
				return(status);
			}
		}
		#endif
	}
}

}


#if MLE_CONDUCT_HIGHER_ORDER

{
int j1, j2, j3, k1, k2, k3;
for (j1 = 0; j1 < nparam; j1++) {
k1 = invploc[j1][imodel1];
char *rname1 = NULL;
if (k1 != BAD_INDEX) rname1 = rnames1[k1];

for (j2 = 0; j2 < nparam; j2++) {
k2 = invploc[j2][imodel2];
char *rname2 = NULL;
if (k2 != BAD_INDEX) rname2 = rnames2[k2];

for (j3 = 0; j3 < nparam; j3++) {
k3 = invploc[j3][imodel2];
char *rname3 = NULL;
if (k3 != BAD_INDEX) rname3 = rnames2[k3];

	THREGION *a2 = NULL, *a1 = NULL;
	status = thExtractMijk(lstruct, imodel2, j2, j3, &a2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not fetch | m_%d_%d_%d > = | DD m_%s / D %s D %s >", 
		name, imodel2, j2, j3, mname2, rname2, rname3);
		return(status);
	}
	status = thExtractMij(lstruct, imodel1, j1, &a1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not fetch | m_%d_%d > = | D m_%s / D %s >", 
		name, imodel1, j1, mname1, rname1);
		return(status);
	}

	MLEFL z = 0.0;
	if (a1 != NULL && a2 != NULL) {
		z = thRegBracket(a1, lwmask, a2, &status);
		#if MLE_CATCH_NAN
		if (isnan(z) || isinf(z)) {
			thError("%s: ERROR - (NAN) value at <m_%d_%d | m_%d_%d_%d> = < D %s / D %s | DD %s / (D %s D %s) > ",
			name, imodel1, j1, imodel2, j2, j3, mname1, rname1, mname2, rname2, rname3);
			return(SH_GENERIC_ERROR);
		}
		#endif
	}
	status = thLMijMklmPut(lstruct, imodel1, j1, imodel2, j2, j3, z);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put < m_%d_%d_%d | m_%d_%d > = < DD m_%s / D %s D %s | D m_%s / D %s >", 
		name, imodel2, j2, j3, imodel1, j1, mname2, rname2, rname2, mname1, rname1);
		return(status);
	}


}}}

if (imodel1 != imodel2) {

int jmodel1, jmodel2;
char *Mname1, *Mname2;
char **Rnames1, **Rnames2;
jmodel1 = imodel2;
jmodel2 = imodel1;
Mname1 = mname2;
Mname2 = mname1;
Rnames1 = rnames2;
Rnames2 = rnames1;

for (j1 = 0; j1 < nparam; j1++) {
k1 = invploc[j1][jmodel1];
char *rname1 = NULL;
if (k1 != BAD_INDEX) rname1 = Rnames1[k1];

for (j2 = 0; j2 < nparam; j2++) {
k2 = invploc[j2][jmodel2];
char *rname2 = NULL;
if (k2 != BAD_INDEX) rname2 = Rnames2[k2];

for (j3 = 0; j3 < nparam; j3++) {
k3 = invploc[j3][jmodel2];
char *rname3 = NULL;
if (k3 != BAD_INDEX) rname3 = Rnames2[k3];

	THREGION *a2 = NULL, *a1 = NULL;
	status = thExtractMijk(lstruct, jmodel2, j2, j3, &a2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not fetch | m_%d_%d_%d > = | DD m_%s / D %s D %s >", 
		name, jmodel2, j2, j3, Mname2, rname2, rname3);
		return(status);
	}

	status = thExtractMij(lstruct, jmodel1, j1, &a1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not fetch | m_%d_%d > = | D m_%s / D %s >", 
		name, jmodel1, j1, Mname1, rname1);
		return(status);
	}
	MLEFL z = 0.0;
	if (a1 != NULL && a2 != NULL) {
		z = thRegBracket(a1, lwmask, a2, &status);
		#if MLE_CATCH_NAN
		if (isnan(z) || isinf(z)) {
			thError("%s: ERROR - (NAN) value at <m_%d_%d | m_%d_%d_%d> = < D %s / D %s | DD %s / (D %s D %s) > ",
			name, jmodel1, j1, jmodel2, j2, k2, Mname1, rname1, Mname2, rname2, rname3);
			return(SH_GENERIC_ERROR);
		}
		#endif
	}
	status = thLMijMklmPut(lstruct, jmodel1, j1, jmodel2, j2, j3, z);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put < m_%d_%d_%d | m_%d_%d > = < DD m_%s / D %s D %s | D m_%s / D %s >", 
		name, jmodel2, j2, j3, jmodel1, j1, Mname2, rname2, rname2, Mname1, rname1);
		return(status);
	}

}}}

}

}
#endif


return(SH_SUCCESS);
}

RET_CODE DoMDInnerProduct(int imodel, LSTRUCT *lstruct) {
char *name = "DoMDInnerProduct";

RET_CODE status;
int nmodel, nparam;
status = thLstructGetNModel(lstruct, &nmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the number of models", name);
	return(status);
}
status = thLstructGetNPar(lstruct, &nparam);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not extract (npar) from (lstruct)", name);
        return(status);
}
if (imodel >= nmodel || imodel < 0) {
	thError("%s: ERROR - unacceptable (imodel1 = %d) where (nmodel = %d)", 
		name, imodel, nmodel);
	return(SH_GENERIC_ERROR);
}

LMACHINE *lmachine;
status = thLstructGetLmachine(lstruct, &lmachine);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - coul not retrieve (lmachine) from (lstruct)", name);
	return(status);
}
int nmpar;
char *mname, **rnames;

mname = lmachine->mnames[imodel];
rnames = lmachine->enames[imodel];
nmpar = lmachine->nmpar[imodel];

MLEFL *mid;
status =  thExtractBrackets(lstruct, &mid, NULL, NULL, NULL, NULL, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract (mid) from (lstruct)", name);
	return(status);
}

THREGION *a = NULL;

/*
       < m_i | W | d >      
     < m_i_j | W | d >    
*/

LWMASK *lwmask = NULL;
status = thLstructGetLwmask(lstruct, &lwmask);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwmask) from (lstruct)", name);
	return(status);
}

REGION *d = NULL;
status = thExtractD(lstruct, &d);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract the data image", name);
	return(status);
}

status = thExtractMi(lstruct, imodel, &a);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract model (%d) '%s' of %d", 
		name, imodel, mname, nmodel);
	return(status);
}

MLEFL z;
z = thRegHalfBracket(d, lwmask, a, &status);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calculate the image bracket for <m_%d|W|d>", 
		name, imodel);
}
mid[imodel] = z;
status = thLMijDPut(lstruct, imodel, -1, z);   
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not place < M_i | D > for (i = %d)", name, imodel);
	return(status);
}
int j;
for (j = 0; j < nparam; j++) {
	status = thExtractMij(lstruct, imodel, j, &a);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract |m_%d_%d> ", name, imodel, j);
		return(status);
	}
	if (a != NULL) {
		z = thRegHalfBracket(d, lwmask, a, &status);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not compute <m_%d_%d|W|d> ", 
				name, imodel, j);
			return(status);
		}
		#if OLD_MLE
		mijd[imodel][j] = z;
		#else
		status = thLMijDPut(lstruct, imodel, j, z);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not put value for (mijd) for (%d, %d)", name, imodel, j);
			return(status);
		}
		#endif
	}
}	
				
return(SH_SUCCESS);
}	



RET_CODE DoDDInnerProduct(LSTRUCT *lstruct) {
char *name = "DoDDInnerProduct";

RET_CODE status;
LWMASK *lwmask = NULL;
status = thLstructGetLwmask(lstruct, &lwmask);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwmask) from (lstruct)", name);
	return(status);
}

REGION *d = NULL;
status = thExtractD(lstruct, &d);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract data image", name);
	return(status);
}

MLEFL x = thBracket(d, lwmask, d, &status);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calculate data-data inner product", name);
	return(status);
}

status = thLDDPut(lstruct, x);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not put data-data inner product in place", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE DoCost(LSTRUCT *lstruct) {
char *name = "DoCost";
if (lstruct == NULL || lstruct->lwork == NULL) {
	thError("%s: ERROR - null or improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}

LWORK *lwork = lstruct->lwork;
CHISQFL *pcostN = &(lwork->pcostN);
CHISQFL *dpcostN = lwork->dpcostN;
CHISQFL **ddpcostN = lwork->ddpcostN;
int npar = lwork->np;

int i;
*pcostN = 0.0;
if (npar > 0) {
	if (dpcostN != NULL) memset(dpcostN, '\0', npar * sizeof(CHISQFL));
	if (ddpcostN != NULL) for (i = 0; i < npar; i++) memset(ddpcostN[i], '\0', npar * sizeof(CHISQFL));
}

RET_CODE status;

LMACHINE *lmachine;
status = thLstructGetLmachine(lstruct, &lmachine);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - coul not retrieve (lmachine) from (lstruct)", name);
	return(status);
}

int iobjc, nobjc = -1;
THOBJC **objcs = NULL; 
status = thMapmachineGetUPObjcs(lmachine, (void ***) &objcs, &nobjc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get unique list of objects from (mapmachine)", name);
	return(status);
}

for (iobjc = 0; iobjc < nobjc; iobjc++) {
	THOBJC *objc = objcs[iobjc];
	char **rnames = NULL;
	int *pndexmap = NULL;
	int nrnames = 0, npndexmap = 0;
 
	status = thLGetObjcRnamesPndex(lstruct, (void *) objc, &rnames, &nrnames, &pndexmap, &npndexmap);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract objc record names and their parameter map", name);
		return(status);
	}
	shAssert(nrnames == npndexmap);
	int add = 1;
	status = thLObjcCalcPcost(objc, pcostN, dpcostN, ddpcostN, npar, rnames, pndexmap, nrnames, add);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not calculate and add parameter cost function for (iobjc = %d)", iobjc);
		return(status);
	}
}

return(SH_SUCCESS);
}

RET_CODE thLObjcCalcPcost(THOBJC *objc, 
	CHISQFL *pcost, CHISQFL *dpcost, CHISQFL **ddpcost, int npar,  
	char **rnames, int *pndexmap, int nrnames, int add) {
char *name = "thLObjcCalcPcost";
int i;
if (add == 0) {
	if (pcost != NULL) *pcost = 0;
	if (dpcost != NULL) memset(dpcost, '\0', npar * sizeof(CHISQFL));
	if (ddpcost != NULL) for (i = 0; i < npar; i++)  memset(ddpcost[i], '\0', npar * sizeof(CHISQFL));
}
	
if (nrnames == 0) return(SH_SUCCESS);
shAssert(npar > 0);
RET_CODE (*cost_function)(THOBJC *, CHISQFL *);
RET_CODE (*dcost_function)(THOBJC *, CHISQFL *);
RET_CODE (*ddcost_function)(THOBJC *, CHISQFL *);

RET_CODE status;

if (pcost != NULL) {
	status = thObjcGetPCostFunction(objc, "", "", (void **) &cost_function); 
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get cost function for (objc = %p)", name, (void *) objc);
		return(SH_GENERIC_ERROR);
	}
	if (cost_function != NULL) {
		CHISQFL z = 0.0;
		status = cost_function(objc, &z);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not obtain parameter cost for (objc = %p)", name, (void *) objc);
			return(status);
		}
		*pcost += (CHISQFL) z;
	}		
}
int irecord, jrecord, iparam, jparam;
if (dpcost != NULL) {
	for (irecord = 0; irecord < nrnames; irecord++) {
		char *rname = rnames[irecord];
		iparam = pndexmap[irecord];
		if (iparam != BAD_INDEX) {
			status = thObjcGetPCostFunction(objc, rname, "", (void **) &dcost_function);	
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get deriv. of cost function for (objc = %p, variable = '%s')", name, (void *) objc, rname);	
  				return(status);
			}
			if (dcost_function != NULL) {
				CHISQFL z = 0.0;
				status = dcost_function(objc, &z);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not obtain deriv. parameter cost for (objc = %p, variable = '%s')", name, (void *) objc, rname);
					return(status);
				}
				dpcost[iparam] += (CHISQFL) z;
			}
		}
	}
}

if (ddpcost != NULL) {
	for (irecord = 0; irecord < nrnames; irecord++) {
	for (jrecord = 0; jrecord < nrnames; jrecord++) {

		char *rname1 = rnames[irecord];
		char *rname2 = rnames[jrecord];
		iparam = pndexmap[irecord];
		jparam = pndexmap[jrecord];
		if (iparam != BAD_INDEX && jparam != BAD_INDEX) {
			status = thObjcGetPCostFunction(objc, rname1, rname2, (void **) &ddcost_function);		
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get deriv. of cost function for (objc = %p, var1 = '%s', var2 = '%s')", name, (void *) objc, rname1, rname2);	
  				return(status);
			}
			if (ddcost_function != NULL) {
				CHISQFL z = 0.0;
				status = ddcost_function(objc, &z);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not obtain deriv. parameter cost for (objc = %p, var1 = '%s', var2 = '%s')", name, (void *) objc, rname1, rname2);
					return(status);
				}
				ddpcost[iparam][jparam] += (CHISQFL) z;
			}
		}
	}
	}
}

return(SH_SUCCESS);
}


RET_CODE DoProgressOutput(MLEFL f) {
char *name = "DoProgressOutput";
if (f < (MLEFL) 0.0) {
	thError("%s: ERROR - problematic progress (f = %g)", name, (float) f);
	return(SH_GENERIC_ERROR);
}
printf("%s: Algorithm Progress: %10.2g \n", name, (float) f);
return(SH_SUCCESS);
}


RET_CODE DoAddToModel(int imodel, LSTRUCT *lstruct) {
char *name = "DoAddToModel";

RET_CODE status;
if (lstruct == NULL) {
	thError("%s: ERROR - null or improperly allocated LSTRUCT", name);
	return(SH_GENERIC_ERROR);
}

/* now first calculate < m_i_j | W | m - d > */

REGION *m;
int nmodel;

status = thExtractM(lstruct, &m);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract model image", name);
	return(status);
	}
status = thLstructGetNModel(lstruct, &nmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract number of models", name);
	return(status);
}
if (imodel < 0 || imodel >= nmodel) {
	thError("%s: ERROR - model number (%d) out of range (nmodel = %d)", name, imodel, nmodel);
	return(SH_GENERIC_ERROR);
}
MLEFL *amp;
status = thLstructGetAmp(lstruct, &amp);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract amplitudes", name);
	return(status);
}

THREGION *mi;
status = thExtractMi(lstruct, imodel, &mi);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract m_%d", name, imodel);
	return(status);
}
status = thRegAddModelComponent(m, mi, amp[imodel]);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not put the model component %d in the final image", name, imodel);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE DoInitModel(LSTRUCT *lstruct) {
char *name = "DoInitModel";

RET_CODE status;
if (lstruct == NULL) {
	thError("%s: ERROR - null or improperly allocated LSTRUCT", name);
	return(SH_GENERIC_ERROR);
}

REGION *m;

status = thExtractM(lstruct, &m);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract model image", name);
	return(status);
}

if (m == NULL) {
	thError("%s: ERROR - null model image found, improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}

shRegClear(m);
thRegMakeZero(m);

return(SH_SUCCESS);
}



RET_CODE thAlgorithmStepPerform(ALGORITHM_STEP *step, LSTRUCT *lstruct) {
char *name = "thAlgorithmStepPerform";

if (step == NULL) {
	thError("%s: ERROR - null (algorithm_step) as input", name);
	return(SH_GENERIC_ERROR);
}
if (lstruct == NULL) {
	thError("%s: ERROR - null (lstruct) as input", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
int runtime = step->runtime;
ALGORITHM_ACTION action = step->action;
#if 0
#if DEEP_DEBUG_MLE
static SCHEMA *schema_alg_action = NULL;
if (schema_alg_action == NULL) {
	TYPE t;
	t = shTypeGetFromName("ALGORITHM_ACTION");
	schema_alg_action = shSchemaGetFromType(t);
}
printf("%s: step = '%s', runtime = %d, runtime0 = %d \n", 
	name, schema_alg_action->elems[action].name, runtime, step->runtime_source);
#endif  
#endif

if (runtime < 0 && runtime != INDEFINITE_TIMES) {
	thError("%s: ERROR - unsupported runtime (%d)", name, runtime);
	return(SH_GENERIC_ERROR);
} else if (runtime == 0) {
	return(SH_SUCCESS);
} else if (runtime > 0) {
	step->runtime--;
}

int node1 = (int) step->node1;
if (action == NODE_LOAD) {
	status = UploadModelToMemory(node1, lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not load node (%d) to memory", name, node1);
		return(status);
	}
} else if (action == NODE_UNLOAD) {
	status = KillModelInMemory(node1, lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not unload node (%d) from memory", name, node1);
		return(status);
	}
} else if (action == EDGE_COMPUTE) {
	int node2 = step->node2;
	status = DoInnerProduct(node1, node2, lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not do the edge computation between nodes (%d, %d)", name, node1, node2);
		return(status);
	}
} else if (action == COST_COMPUTE) {
	#if MLE_APPLY_CONSTRAINTS
	status = DoCost(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not calculate the parameter cost for (lstruct)", name);
		return(status);
	}
	#else
	thError("%s: MLE_APPLY_CONSTRAINTS keyword is off, should not expect to calculate parameter cost function", name);
	return(SH_GENERIC_ERROR);
	#endif
} else if (action == PRINT_PROGRESS) {
	int node2 = step->node2;
	status = DoProgressOutput((MLEFL) node1 / (MLEFL) node2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not output a progress report", name);
		return(status);
	}
} else if (action == INIT_MODEL) {
	status = DoInitModel(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initialize model region", name);
		return(status);
	}
} else if (action == NODE_ADDTOMODEL) {
	status = DoAddToModel(node1, lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add nodel (%d) to model image", name, node1);
		return(status);
	}
} else {
	if (schema_algorithm_action == NULL) init_static_vars();
	thError("%s: ERROR - unsupported action '%s' in (algorithm_elem)", name, (schema_algorithm_action->elems[action]).name);
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE thAlgorithmRun(ALGORITHM *alg, LSTRUCT *lstruct) {
char *name = "thAlgorithmRun";

if (alg == NULL) {
	thError("%s: ERROR - null (algorithm) as input", name);
	return(SH_GENERIC_ERROR);
}
if (lstruct == NULL) {
	thError("%s: ERROR - null (lstruct) as input", name);
	return(SH_GENERIC_ERROR);
}
#if DEBUG_MEMORY
printf("%s: memory statistics upon running the algorithm \n", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#endif

RET_CODE status;
int n;
status = thAlgorithmGetNsteps(alg, &n);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the size of (algorithm)", name);;
	return(status);
}
if (n == 0) {
	thError("%s: WARNING - (algorithm) has no steps", name);
	return(SH_SUCCESS);
}
int i;
for (i = 0; i < n; i++) {
	ALGORITHM_STEP *step = thAlgorithmStepGetByPos(alg, i, &status);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (algorithm_step) for (pos = %d)", name, i);
		return(status);
	}
	#if DEBUG_MLE_ALGORITHM
	printf("%s: [step%d]: (node1 = %d, node2 = %d), (action = '%s', runtime = %d, runtime0 = %d) \n", name, i, step->node1, step->node2, shEnumNameGetFromValue("ALGORITHM_ACTION", step->action), step->runtime, step->runtime_source); 
	#endif
	status = thAlgorithmStepPerform(step, lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not perform step (%d) of (algorithm)", name, i);
		return(status);
	}
}

#if DEBUG_MEMORY
printf("%s: memory statistics after running the algorithm \n", name);
shMemStatsPrint();
fflush(stdout);
fflush(stderr);
#endif
return(SH_SUCCESS);
}

RET_CODE thSizeOfImpack(LIMPACK *impack, MEMFL *z) {
char *name = "thSizeOfImpack";
if (impack == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (z == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
RET_CODE status;
int i, nparam;
nparam = impack->nmpar;
MEMFL zzz = 0, zz = 0;
THREGION **mreg;
mreg = impack->mreg;
for (i = 0; i < nparam + 1; i++) {
	status = thRegGetMemory(mreg[i], &zzz);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get the memory needed for component (%d) of the (image_pack)", name, i);
		return(status);
	}
	zz += zzz;
}

*z = zz;
return(SH_SUCCESS);
}

RET_CODE thOverlapBetweenImpacks(LIMPACK *impack1, LIMPACK *impack2, FITSTAT fitstat1, FITSTAT fitstat2, MEMFL *z) {
char *name = "thOverlapBetweenImpacks";

if (impack1 == NULL) {
	thError("%s: ERROR - null input (impack1)", name);
	return(SH_GENERIC_ERROR);
}
if (impack2 == NULL) {
	thError("%s: ERROR - null input (impack2)", name);
	return(SH_GENERIC_ERROR);
}
if (z == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}

int i1, i2, nparam1, nparam2;
MEMFL pix;
RET_CODE status;
THREGION **mreg1, **mreg2, *reg1, *reg2;
MEMFL zz = 0;
nparam1 = impack1->nmpar;
nparam2 = impack2->nmpar;
mreg1 = impack1->mreg;
mreg2 = impack2->mreg;
for (i1 = 0; i1 < nparam1 + 1; i1++) {
	reg1 = mreg1[i1];
	for (i2 = 0; i2 < nparam2 + 1; i2++) {
		reg2 = mreg2[i2];
		status = thRegOverlapPix(reg1, reg2, fitstat1, fitstat2, &pix);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get the pixel count for the overlap region between component (%d, %d) of the two (image_packs)", name, i1, i2);
			return(status);
		}
		zz += ((MEMFL) pix * ((MEMFL) sizeof(THPIX)));
	}
}

*z = zz;
return(SH_SUCCESS);
}

RET_CODE thCreateAdjacencyMatrix(ADJ_MATRIX *matrix, LSTRUCT *lstruct) {
char *name = "thCreateAdjacencyMatrix";

if (lstruct == NULL) {
	thError("%s: ERROR - null (lstruct) as input", name);
	return(SH_GENERIC_ERROR);
}
if (matrix == NULL) {
	thError("%s: ERROR - null placeholder for output (matrix)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
int nmodel;
status = thLstructGetNModel(lstruct, &nmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the number of models for (lstruct)", name);
	return(status);
}
if (nmodel < 0) {
	thError("%s: ERROR - does not support (nmodel = %d)", name, nmodel);
	return(SH_GENERIC_ERROR);
}
if (nmodel == 0) {
	thError("%s: WARNING - (lstruct) does not contain any models - (nmodel = %d)", name, nmodel);
}
int nmax = matrix->nmax;
status = thAdjMatrixRenew(matrix, nmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not renew adjacency matrix of size (%d) to (%d)", name, nmax, nmodel);
	return(status);
}
nmax = matrix->nmax;

LIMPACK **impacks;
status = thLstructGetImpacks(lstruct, &impacks);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get list of (impack)s", name);
	return(status);
}
LIMPACK *impack1, *impack2;
impack1 = impacks[1];
impack2 = impacks[2];
/* old school - contributes to memory fragmentation - oct 17, 2012 
if (impack1 == NULL) impack1 = thLimpackNew();
if (impack2 == NULL) impack2 = thLimpackNew();
*/

MEMFL **adj_matrix, *adj_row, *memspace;
adj_matrix = matrix->matrix;
memspace = matrix->memspace;
FITSTAT *fitstat = matrix->fitstat;
int n = matrix->n;
if (n != nmodel) {
	thError("%s: ERROR - adjacency matrix constructed with (n = %d) while (nmodel = %d)", name, n, nmodel);
	return(SH_GENERIC_ERROR);
}

int i1, i2;
for (i1 = 0; i1 < nmodel; i1++) {
	status = thConstructImagePack(lstruct, i1, impack1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not create (image_pack) for model (%d)", name, i1);
		return(status);
	}
	adj_row = adj_matrix[i1];

	status = thSizeOfImpack(impack1, adj_row + i1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not calculate the memory size of node (%d)", 
			name, i1);
		return(status);
	}

	int nmpar1 = impack1->nmpar;
	if (nmpar1 > 0) {
		fitstat[i1] = FITMODEL;
	} else {
		fitstat[i1] = GENERAL_LINEAR_MODEL;
	}
	FITSTAT fitstat1 = fitstat[i1];
	/* self-overlap */
	status = thOverlapBetweenImpacks(impack1, impack1, fitstat1, fitstat1, adj_row + i1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not find the self-overlap for model (%d)", name, i1);
		return(status);
	}
	#if DEEP_DEBUG_MLE
	if (i1 < 10) {
		printf("%s: found self-adjacency for (imodel = %d), overlap = %G \n", name, i1, (double) adj_row[i1]);
	}
	#endif
	status = thLimpackGetMemory(impack1, memspace + i1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (impack) memory for model (%d)", 
			name, i1);
		return(status);
	}

	/* overlap with other models */	
	for (i2 = 0; i2 < i1; i2++) {
		FITSTAT fitstat2 = fitstat[i2];
		status = thConstructImagePack(lstruct, i2, impack2);	
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not create (image_pack) for model (%d)", name, i2);
			return(status);
		}
		status = thOverlapBetweenImpacks(impack1, impack2, fitstat1, fitstat2, adj_row + i2);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not find the overlap between models (%d, %d)", name, i1, i2);
			return(status);
		}
	}
}

for (i1 = 0; i1 < nmodel; i1++) {
	adj_row = adj_matrix[i1];
	for (i2 = 0; i2 < i1; i2++) {
		adj_matrix[i2][i1] = adj_row[i2];
	}
} 

/* fixing fitstat for neighborhood */
for (i1 = 0; i1 < nmodel; i1++) {
	if (fitstat[i1] == GENERAL_LINEAR_MODEL) {
		adj_row = adj_matrix[i1];
		for (i2 = 0; i2 < nmodel; i2++) {
			if (fitstat[i2] == FITMODEL && adj_row[i2] != (MEMFL) 0) {
				fitstat[i1] = NEIGHBOR_TO_FITMODEL;
				break;
			}
		}
	}
}	

/* testing the adjacency matrix */
int neg_count = 0;
for (i1 = 0; i1 < nmodel; i1++) {
	adj_row = adj_matrix[i1];
	for (i2 = 0; i2 < nmodel; i2++) {
		MEMFL adj_value = adj_row[i2];
		if (adj_value < (MEMFL) 0) {
			neg_count++;
			#if DEEP_DEBUG_MLE
			thError("%s: ERROR - found adj[%d][%d] < 0", name, i1, i2);
			#endif
		}
	}
}
if (neg_count > 0) {
	thError("%s: ERROR - found (%d) counts of negative values in the adjacency matrix", name, neg_count);
}
int zero_count = 0;
for (i1 = 0; i1 < nmodel; i1++) {
	MEMFL adj_value = adj_matrix[i1][i1];
	if (adj_value == (MEMFL) 0) {
		zero_count++;
		#if DEEP_DEBUG_MLE
		thError("%s: ERROR - found adj[%d][%d] = 0", name, i1, i1);
		#endif
	}
}
if (zero_count > 0) {
	thError("%s: ERROR - found (%d) counts of zero diagonal values in the adjacency matrix", name, zero_count);
}
if (zero_count > 0 || neg_count > 0) {
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE thLCompile(LSTRUCT *l, MEMDBLE memory, SIMFLAG simflag) {
char *name = "thLCompile";
#if DEBUG_MEMORY
printf("%s: memory statistics before compiling (lstruct)", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#endif

if (l == NULL || l->lalg == NULL) {
	thError("%s: ERROR - null or improperly allocated (lstruct)", name);
	return(SH_SUCCESS);
}
if (memory > (MEMDBLE) 0.0) {
	LWORK *lwork = l->lwork;
	if (lwork == NULL) {
		thError("%s: ERROR - null (lwork) - improperly allocated (lstruct)", name);
		return(SH_GENERIC_ERROR);
	}
	MEMDBLE used_memory = lwork->memory;
	if (used_memory <= 0) {
		used_memory = 0;
		thError("%s: WARNING - (lwork) memory size not available - calculating", name);	
		RET_CODE status = thLworkGetMemSize(lwork, &used_memory);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not calculate memory size for (lwork)", name);
			return(status);
		}
		lwork->memory = used_memory;
	}
	MEMDBLE used_memory2 = (MEMDBLE) shMemBytesInUse();
	if (used_memory2 > used_memory) {
		printf("%s: nominal memory for (lwork: %-.1f MB, %-.1f%%), while actual memory used so far (%-.1f MB) \n", name, (float) ((double) used_memory / (double) MEGABYTE), (float) (100.0 * (double) used_memory / (double) used_memory2), (float) ((double) used_memory2 / (double) MEGABYTE));
		used_memory = used_memory2;
	}
	if (used_memory >= memory) {
		thError("%s: ERROR - (%-.1f MB) used by (lwork) while (%-.1f MB) of memory is available", name, (float) ((double) used_memory / (double) MEGABYTE), (float) ((double) memory / (double) MEGABYTE));
		return(SH_GENERIC_ERROR);
	}
	used_memory = (MEMDBLE) ((long double) MEMORY_FUDGE_FACTOR * (long double) used_memory);
	if (used_memory >= memory) {
		thError("%s: ERROR - (%-.1f MB) is predicted for (lstruct) while (%-.1f MB) of memory is available", name, (float) ((double) used_memory / (double) MEGABYTE), (float) ((double) memory / (double) MEGABYTE));
		return(SH_GENERIC_ERROR);
	}
	memory -= used_memory;
	if ((double) memory < (double) MEMORY_MIN) {
		thError("%s: ERROR - (%-.1f MB) of memory left for algorithm which is less than (%-.1f MB) minimum required", name, (float) ((double) memory / (double) MEGABYTE), (float) ((double) MEMORY_MIN / (double) MEGABYTE));
		return(SH_GENERIC_ERROR);
	}
	printf("%s: memory available for algorithm = %-.1f MB, memory available for misc operations = %-.1f MB \n", name, (float) ((double) memory / (double) MEGABYTE),  (float) ((double) used_memory / (double) MEGABYTE));
}
LALG *lalg = l->lalg;

if (memory < (MEMDBLE) 0) {
	lalg->runtype = SIMPLERUN;
} else {
	lalg->runtype = ALGRUN;
	int i;
	ALGORITHM **algs, *alg;
	if (lalg->algs == NULL) {
		thError("%s: WARNING - improperly allocated (lalg) detected, correcting", name);
		lalg->algs = thCalloc((int) N_RUNSTAGE, sizeof(ALGORITHM *));
		algs = lalg->algs;
		for (i = 0; i < (int) N_RUNSTAGE; i++) {
			algs[i] = thAlgorithmNew();
		}
	}
	algs = lalg->algs;
	for (i = 0; i < (int) N_RUNSTAGE; i++) {
		if (algs[i] == NULL) algs[i] = thAlgorithmNew();
	}
	RET_CODE status;

	/* creating the adjacency matrix */
	#if DEBUG_MEMORY
	printf("%s: memory statistics before allocating (Adj-matrix)", name);
	shMemStatsPrint();
	fflush(stderr);
	fflush(stdout);
	#endif
	ADJ_MATRIX *adj = thAdjMatrixNew();
	#if DEBUG_MEMORY
	printf("%s: memory statistics before compiling (Adj-matrix)", name);
	shMemStatsPrint();
	fflush(stderr);
	fflush(stdout);
	#endif

	status = thCreateAdjacencyMatrix(adj, l);
 	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not create the adjacency matrix for (lstruct)", name);
		return(status);
	}
	#if DEBUG_MEMORY
	printf("%s: memory statistics after compiling (Adj-matrix)", name);
	shMemStatsPrint();
	fflush(stderr);
	fflush(stdout);
	#endif

	#if DEBUG_MEMORY
	printf("%s: memory statistics before designing algorithm ('INITRUN')", name);
	shMemStatsPrint();
	fflush(stderr);
	fflush(stdout);
	#endif

	alg = algs[INITSTAGE];
	alg->memory_total = memory;
	status = design_algorithm(adj, alg, INNER_PRODUCTS, INITRUN);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not design algorithm for the initial stage", name);
		return(status);
	}

	#if DEBUG_MEMORY
	printf("%s: memory statistics after designing algorithm ('INITRUN')", name);
	shMemStatsPrint();
	fflush(stderr);
	fflush(stdout);
	#endif

	#if DEBUG_MLE_ALGORITHM
	printf("%s: algorithm for 'INITRUN' \n", name);
	output_algorithm(alg, adj);
	#endif

	#if DEBUG_MEMORY
	printf("%s: memory statistics before designing algorithm ('MIDDLERUN')", name);
	shMemStatsPrint();
	fflush(stderr);
	fflush(stdout);
	#endif

	alg = algs[MIDDLESTAGE];
	alg->memory_total = memory;
	status = design_algorithm(adj, alg, INNER_PRODUCTS, MIDDLERUN);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not design algorithm for the middle stage", name);
		return(status);
	}
	
	#if DEBUG_MEMORY
	printf("%s: memory statistics after designing algorithm ('MIDDLERUN')", name);
	shMemStatsPrint();
	fflush(stderr);
	fflush(stdout);
	#endif

	#if DEBUG_MLE_ALGORITHM
	printf("%s: algorithm for 'MIDDLESTAGE' \n", name);
	output_algorithm(alg, adj);
	#endif

	#if DEBUG_MEMORY
	printf("%s: memory statistics before designing algorithm ('ENDRUN')", name);
	shMemStatsPrint();
	fflush(stderr);
	fflush(stdout);
	#endif

	alg = algs[ENDSTAGE];
	alg->memory_total = memory;
	status = design_algorithm(adj, alg, INNER_PRODUCTS, ENDRUN);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not design algorithm for the end stage", name);
		return(status);
	}

	#if DEBUG_MEMORY
	printf("%s: memory statistics before designing algorithm ('ENDRUN')", name);
	shMemStatsPrint();
	fflush(stderr);
	fflush(stdout);
	#endif

	#if DEBUG_MLE_ALGORITHM
	printf("%s: algorithm for 'ENDSTAGE' \n", name);
	output_algorithm(alg, adj);
	#endif
	if (simflag == SIMFINALFIT) {
		
		#if DEBUG_MEMORY
		printf("%s: memory statistics before designing algorithm ('SIMSTAGE')", name);
		shMemStatsPrint();
		fflush(stderr);
		fflush(stdout);
		#endif

		alg = algs[SIMSTAGE];
		alg->memory_total = memory;
		status = design_algorithm(adj, alg, CREATE_MODEL, INITRUN);
		if (status != SH_SUCCESS) {	
			thError("%s: ERROR - could not design algorithm for simulation of the fitted model", name);
			return(status);
		}

		#if DEBUG_MEMORY
		printf("%s: memory statistics before designing algorithm ('SIMSTAGE')", name);
		shMemStatsPrint();
		fflush(stderr);
		fflush(stdout);
		#endif

	} else if (simflag != NOSIMFORFINALFIT) {
		thError("%s: ERROR - unsupported (simflag)", name);
		return(SH_GENERIC_ERROR);
	}
	#if DEBUG_MLE_ALGORITHM
	printf("%s: algorithm for 'SIMSTAGE' \n", name);
	output_algorithm(alg, adj);
	#endif

	/* clean the mess */
	thAdjMatrixDel(adj);
	#if DEBUG_MEMORY
	printf("%s: memory statistics after deleting (Adj-matrix)", name);
	shMemStatsPrint();
	fflush(stderr);
	fflush(stdout);
	#endif

}

#if DEBUG_MEMORY
printf("%s: memory statistics after compiling (lstruct)", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#endif

#if DEBUG_MEMORY
printf("%s: memory statistics before defragmenting memory", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#endif

int ret, free_to_os = FREE_TO_OS;
ret = shMemDefragment(free_to_os);
if (ret != 0) {
	thError("%s: ERROR - problem defragmenting memory", name);
	return(SH_GENERIC_ERROR);
}

#if DEBUG_MEMORY
printf("%s: memory statistics after defragmenting memory", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#endif



return(SH_SUCCESS);
}


/* MISC. */

RET_CODE thObjcMakeImage(THOBJC *objc, REGION *reg) {
char *name = "thObjcMakeImage";
thError("%s: ERROR - unuspported function", name);
return(SH_GENERIC_ERROR);
}

RET_CODE thLstructGenPsf(LSTRUCT *l, REGION *reg, PSF_CONVOLUTION_REPORT *report, THPIX xc, THPIX yc, PSF_CONVOLUTION_TYPE convtype) {
char *name = "thLstructGenPsf";

if (l == NULL || reg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
LWORK *lwork; 
if ((lwork = l->lwork) == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
WPSF *wpsf;
if ((wpsf = lwork->wpsf) == NULL) {
	thError("%s: ERROR - improperly allocated (lwork)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
PSF_CONVOLUTION_INFO *psf_info = NULL;
if ((psf_info = thPsfConvInfoNew(convtype, DEFAULT_NCOMPONENT)) == NULL) {
	thError("%s: ERROR - null (psf_info)", name);
	return(SH_GENERIC_ERROR);
}
psf_info->wpsf = wpsf;

status = thPsfInfoPutCenter(psf_info, xc, yc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update position for (psf_info)", name);
	return(status);
}
psf_info->conv_type = convtype;

status = thPsfInfoUpdateWpsf(psf_info);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update (wpsf) in (psf_info)", name);
	return(status);
}

FFTW_KERNEL *fftw_kern = NULL;
REGION *lreg = NULL;

int index, nindex = 0;
int nrow = 0, ncol = 0;

status = thPsfConvInfoGetNComponent(psf_info, &nindex);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (ncomponent) for (psf_info)", name);
	return(status);
}
if (nindex < 0) {
	thError("%s: ERROR - unsupported (ncomponent = %d) for (psf_info)", name, nindex);
	return(SH_GENERIC_ERROR);
}

#if DEBUG_MLE
printf("%s: conv '%s' ncomponent = %d \n", name, shEnumNameGetFromValue("PSF_CONVOLUTION_TYPE", convtype), nindex);
#endif
for (index = 0; index < nindex; index++) {
	status = thPsfInfoGetFftwKernel(psf_info, index, &fftw_kern);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (fftw_ken) from (psf_info, index: %d)", name, index);
		return(status);
	}
	status = thFftwKernelGetDirReg(fftw_kern, &lreg);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not fetch direct image from (fftw_kern)", name);
		return(status);
	}
	nrow = MAX(nrow, lreg->nrow);
	ncol = MAX(ncol, lreg->ncol);
}

int tnrow = (int) (0.5 + pow((THPIX) (nrow * nrow + ncol * ncol), 0.5));
if (tnrow%2 == 0) tnrow++;
int tncol = tnrow;
if (reg->nrow < tnrow || reg->ncol < tncol) {
	#if VERBOSE_MLE
	if (reg->nrow == 0 || reg->ncol == 0) {
		thError("%s: WARNING - region supplied has (nrow, ncol) of (%d, %d) while (%d, %d) is needed", name, reg->nrow, reg->ncol, tnrow, tncol);
		thError("%s: WARNING - region will be reshaped", name);
	}
	#endif
	shRegReshape(reg, tnrow, tncol);
}
nrow = reg->nrow;
ncol = reg->ncol;
if (reg->row0 != 0 || reg->col0 != 0) {
	thError("%s: WARNING - adjusting row0, col0 for output region", name);
	reg->row0 = 0;
	reg->col0 = 0;
}
shRegClear(reg);

for (index = 0; index < nindex; index++) {
	status = thPsfInfoGetFftwKernel(psf_info, index, &fftw_kern);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (fftw_ken) from (psf_info, index: %d)", name, index);
		return(status);
	}
	status = thFftwKernelGetDirReg(fftw_kern, &lreg);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not fetch direct image from (fftw_kern)", name);
		return(status);
	}

	/* inserting the component amplitude */
	THPIX a = fftw_kern->psf_a;
	/* fix the row0, col0 */
	int lnrow, lncol, lrow0, lcol0;
	lnrow = lreg->nrow;
	lncol = lreg->ncol;
	lrow0 = lreg->row0;
	lcol0 = lreg->col0;
	lreg->row0 = (nrow + 1) / 2 - (lnrow + 1) / 2;
	lreg->col0 = (ncol + 1) / 2 - (lncol + 1) / 2;	
	/* make sure that lreg has the hole value set to 0.0 */
	#if DEBUG_MLE
	printf("%s: adding psf component (%d), psf_a = %g, (nrow, ncol) = (%d, %d) \n", name, index, (float) a, lnrow, lncol);
	#endif	
	if (index == 0) {
		status = thPutModelComponent(reg, lreg, a);
	} else {
		status = thAddModelComponent(reg, lreg, a);
	}
	lreg->row0 = lrow0;
	lreg->col0 = lcol0;
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put / add psf component to image (index = %d) ", name, index);
		return(status);
	}
}

THPIX i, i_max, i_min;
int j, k; 
double total = 0.0;
for (j = 0; j < reg->nrow; j++) {
	THPIX *rows_j;
	rows_j = reg->rows_thpix[j];
	for (k = 0; k < reg->ncol; k++) {
		i = rows_j[k];	
		total += (double) i;	
		i_max = MAX(i_max, i);
		i_min = MIN(i_min, i);
	}
}

#if DEBUG_MLE
printf("%s: '%s' nrow = %d, ncol = %d \n", name, reg->name, reg->nrow, reg->ncol);
printf("%s: MIN: I (%12.2e) \n", name, i_min);
printf("%s: MAX: I (%12.2e) \n", name, i_max);
printf("%s: TOT: I (%12.2e) \n", name, (float) total);
#endif

if (report != NULL) {
	PSF_CONVOLUTION_REPORT *psf_conv_report = NULL;
	status = thPsfInfoGetPsfConvReport(psf_info, &psf_conv_report);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (psf_conv_report) from (psf_info)", name);
		return(status);
	}
	status = thPsfConvReportCopy(report, psf_conv_report);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy (psf_conv_report) onto (report)", name);
		return(status);
	}
}
psf_info->wpsf = NULL;
thPsfConvInfoDel(psf_info);

return(SH_SUCCESS);
}

RET_CODE thObjcCrudeCalib(CRUDE_CALIB *cc, THOBJC *objc) {
char *name = "thCrudeCalibObjc";
if (cc == NULL) {
	thError("%s: ERROR - null (crude_calib)", name);
	return(SH_GENERIC_ERROR);
}
if (objc == NULL) {
	thError("%s: ERROR - null (thobjc)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
void *iopar = NULL;
TYPE iotype = UNKNOWN_SCHEMA;
status = thObjcGetIopar(objc, &iopar, &iotype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (iopar) in (thobjc)", name);
	return(status);
}
status = thCrudeCalibVariable(cc, iopar, iotype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calibrate (iopar)", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE thLCrudeCalib(LSTRUCT *l, CRUDE_CALIB *cc) {
char *name = "thLCrudeCalib";
RET_CODE status;
if (l == NULL) {
	thError("%s: ERROR - null (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
if (cc == NULL) {
	status = thLGetCrudeCalib(l, &cc);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (crude_calib) from (lstruct)", name);
		return(status);
	}
	if (cc == NULL) {
		thError("%s: WARNING - null (crude_calib) found in (lstruct)", name);
	}
}
if (cc == NULL) {
	thError("%s: ERROR - null (crude_calib)", name);
	return(SH_GENERIC_ERROR);
}

LMACHINE *map = NULL;
status = thLstructGetLmachine(l, &map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (map) from (lstruct)", name);
	return(status);
}
THOBJC **objcs = NULL;
int nobjc = -1;
status = thMapmachineGetUObjcs(map, (void ***) &objcs, &nobjc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get list of (objc) from (lstruct)", name);
	return(status);
}
if (nobjc <= 0) {
	thError("%s: WARNING - no objects were found in (lstruct) - (nobjc = %d)", name, nobjc);
}
int i;
for (i = 0; i < nobjc; i++) {
	THOBJC *objc = objcs[i];
	status = thObjcCrudeCalib(cc, objc);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not calibrate (objc: 0x%x)", name, objc->thid);
		return(status);
	}
}
return(SH_SUCCESS);
}

RET_CODE thLCalib(LSTRUCT *lstruct) {
char *name = "thLCalib";
/* do the calibration of io-pars based on calibration type and structure available in lcalib */

RET_CODE status;
CALIBTYPE calibtype = UNKNOWN_CALIBTYPE;
LCALIB *lcalib = NULL;
status = thLstructGetLCalib(lstruct, &lcalib);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lcalib) from (lstruct)", name);
	return(status);
}
status = thLCalibGetType(lcalib, &calibtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (calibtype) from (lcalib)", name);
	return(status);
}

if (calibtype == CRUDECALIB) {
	status = thLCrudeCalib(lstruct, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not run crude_calib", name);
		return(status);	
	}
} else if (calibtype != NOCALIB) {
	thError("%s: ERROR - unsupported calibtype '%s'", name, shEnumNameGetFromValue("CALIBTYPE", calibtype));
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE thLRecordLfit(LSTRUCT *lstruct) {
char *name = "thLRecordLfit";

#if DEBUG_MEMORY_VERBOSE
printf("%s: memory statistics before recording (lfit)", name);
shMemStatsPrint();
fflush(stdout);
fflush(stderr);
#endif

RET_CODE status;
LFIT *lfit = NULL;
status = thLstructGetLfit(lstruct, &lfit);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lfit) from (lstruct)", name);
	return(status);
}
LMETHOD *lm = NULL;
status = thLstructGetLmethod(lstruct, &lm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmethod) from (lfit)", name);
	return(status);
}
XUPDATE update;
status = thLstructGetXupdate(lstruct, &update, PSECTOR);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (update) status from (lstruct)", name);
	return(status);
}
status = thLfitAddLmethod(lfit, lm, update);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add (lmethod) to (lfit)", name);
	return(status);
}
status = thLfitAddLprofileFromLstruct(lfit, lstruct, update);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add (lprofile) to (lfit)", name);
	return(status);
}

#if DEBUG_MEMORY_VERBOSE
printf("%s: memory statistics before recording (lfit)", name);
shMemStatsPrint();
fflush(stdout);
fflush(stderr);
#endif

return(SH_SUCCESS);
}

RET_CODE thLDumpIo(LSTRUCT *l) {
char *name = "thLDumpIo";
if (l == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;

#if 0 /* this was commented out on Sep 12, 2014 */
status = thLDumpAmp(l, XTOIO); 
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not map amplitude array (X) onto IO structures", name);
	return(status);
}
status = thLDumpPar(l, MODELTOIO);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not map parameters from (model) onto IO structures", name);
	return(status);
}
#else /* the above section was replaced by the following from Capture package */

status = thLDumpPar(l, MODELTOIO);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump (par) from (model) onto (io) for (lstruct)", name);
	return(status);
}
status = thLDumpAmp(l, XTOIO);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump (amp) from (x) onto (io) for (lstruct)", name);
	return(status);
}
status = thLDumpPar(l, XNTOIO);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump (par) from (xN) onto (io) for (lstruct)", name);
	return(status);
}
status = thLDumpCov(l);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump (variance) onto (io) for (lstruct)", name);
	return(status);
}
#endif


status = thLDumpCountsIo(l);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (counts) in (ioprops) in (l)", name);
	return(status);
}
status = thLCalib(l);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calibrate (l) with initial count data", name);
	return(-1);
}
return(SH_SUCCESS);
}

RET_CODE thLWriteMemoryStat(LSTRUCT *lstruct, char *fname1, char *fname2) {
char *name = "thLWriteMemoryStat";

static LIMPACK *impack = NULL;
if (impack == NULL) impack = thLimpackNew();

int i;
FILE *file;
THOBJCTYPE objctype;
char *objcname;
MEMFL memory;
THOBJCID objcid;
THPIX *xx = NULL, re = 0.0, e = 0.0, phi = 0.0;

LMACHINE *map = lstruct->lmodel->lmachine;
char **mnames = map->mnames;
char **pnames = map->pnames;
void **ps = map->ps;
THOBJC **objcs = (THOBJC **) map->objcs;
int nmodel = map->namp;
if (fname1 != NULL && strlen(fname1) != 0) {
	printf("%s: outputting memory statistics for models to '%s' \n", name, fname1);
	file = fopen(fname1, "w");
	fprintf(file, "%10s %10s %10s %10s %10s %10s %10s %10s \n", 
		"memory", "model", "objcname", "objctype", "objcid", "re", "e", "phi");
	for (i = 0; i < nmodel; i++) {
		char *mname = mnames[i];
		char *pname = pnames[i];
		void *p = ps[i];
		re = VALUE_IS_BAD;
		e = VALUE_IS_BAD;
		phi = VALUE_IS_BAD;
		TYPE t = shTypeGetFromName(pname);
		SCHEMA_ELEM *se = shSchemaElemGetFromType(t, "re");
		if (se != NULL) {
			xx = shElemGet(p, se, NULL);
			if (xx != NULL) re = *xx;
		} 
		se = shSchemaElemGetFromType(t, "e");
		if (se != NULL) {
			xx = shElemGet(p, se, NULL);
			if (xx != NULL) e = *xx;
		}
		se = shSchemaElemGetFromType(t, "phi");
		if (se != NULL) {
			xx = shElemGet(p, se, NULL);
			if (xx != NULL) phi = *xx;
		}
		THOBJC *objc = objcs[i];
		objctype = objc->thobjctype;
		RET_CODE status = thObjcNameGetFromType(objctype, &objcname);		
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (objcname) for (objctype)", name);
			return(status);
		}
		THOBJCID objcid = objc->thid;
		status = thConstructImagePack(lstruct, i, impack);	
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not construct impack for model (%d) '%s'", name, i, mname);
			return(status);
		}
		status = thLimpackGetMemory(impack, &memory);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (impack) memory for model (%d) '%s'", name, i, mname);
			return(status);
		}
		fprintf(file, "%10.4g %10s %10s %10d %10d %10.4g %10.4g %10.4g \n", 
			(float) memory, mname, objcname, (int) objctype, (int) objcid, re, e, phi);
	}
	fclose(file);
}

int nobjc;
if (fname2 != NULL && strlen(fname2) != 0) {
	printf("%s: outputying memory statistics for objects to '%s' \n", name, fname2);
	file = fopen(fname2, "w");
	for (i = 0; i < nobjc; i++) {
		fprintf(file, "%g, %s, %d, %d \n", (float) memory, objcname, (int) objctype, (int) objcid);
	}
	fclose(file);
}
return(SH_SUCCESS);
}


RET_CODE thLstructManipulateWeight(LSTRUCT *lstruct, char **classnames, int nclass) {
char *name = "thLstructManipulateWeight";
if (lstruct == NULL) {
	thError("%s: ERROR - null (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
if (nclass == 0 || classnames == NULL) {
	thError("%s: WARNING - no (classnames) passed (nclass = %d)", name, nclass);
	return(SH_SUCCESS);
}

RET_CODE status;
LDATA *ldata = NULL;
status = thLstructGetLdata(lstruct, &ldata);
if (status != SH_SUCCESS) {
	thError("%s: ERROR  - could not get (ldata) from (lstruct)", name);
	return(status);
}
if (ldata == NULL) {
	thError("%s: null (ldata) discovered", name);
	return(SH_GENERIC_ERROR);
}
REGION *weight = NULL;
status = thLdataGet(ldata, NULL, &weight, NULL, NULL, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (weight) from (ldata)", name);
	return(status);
}
if (weight == NULL) {
	thError("%s: ERROR - null (weight)", name);
	return(SH_GENERIC_ERROR);
}
LMACHINE *map = NULL;
status = thLstructGetLmachine(lstruct, &map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmachine)", name);
	return(status);
}
if (map == NULL) {
	thError("%s: ERROR - null (lmachine)", name);
	return(SH_GENERIC_ERROR);
}

THOBJC **objclist = NULL;
int nobjc = 0;
status = thMapmachineGetUObjcs(map, (void ***) &objclist, &nobjc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get object list from (lmachine)", name);
	return(status);
}

int i;
if (nobjc <= 0 || objclist == NULL) {
	thError("%s: WARNING - no objects loaded on the map. no change to (weight)", name);
	return(SH_SUCCESS);
}

int nmodel = 8;
char *mnames[nmodel];
mnames[0] = "deV";
mnames[1] = "deV1";
mnames[2] = "exp";
mnames[3] = "exp1";
mnames[4] = "sersic";
mnames[5] = "sersic1";
mnames[6] = "gaussian";
mnames[7] = "gaussian1";

void *fpixels[nmodel];
#if MLE_USE_STEP_FUNCTION
fpixels[0]= &deVStep;
fpixels[1]= &deVStep;
fpixels[2]= &ExpStep;
fpixels[3]= &ExpStep;
fpixels[4]= NULL;
fpixels[5]= NULL;
fpixels[6]= &gaussianStep;
fpixels[7]= &gaussianStep;
#else
fpixels[0]= &deV;
fpixels[1]= &deV;
fpixels[2]= &Exp;
fpixels[3]= &Exp;
fpixels[4]= NULL;
fpixels[5]= NULL;
fpixels[6]= &gaussian;
fpixels[7]= &gaussian;
#endif
LWORK *lwork = NULL;
status = thLstructGetLwork(lstruct, &lwork);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwork) from (lstruct)", name);
	return(status);
}

REGION *wreg = NULL, *wreg2 = NULL;
wreg = lwork->wreg;
wreg2 = lwork->wreg2;

int nrow, ncol;
nrow = weight->nrow;
ncol = weight->ncol;

if (wreg != NULL && (nrow != wreg->nrow || ncol != wreg->ncol)) {
	shRegDel(wreg);
	wreg = NULL;
	lwork->wreg = NULL;
}
if (wreg2 != NULL && (nrow != wreg2->nrow || ncol != wreg2->ncol)) {
	shRegDel(wreg2);
	wreg2 = NULL;
	lwork->wreg2 = NULL;
}
if (wreg == NULL) {
	wreg = shRegNew("work image", nrow, ncol, TYPE_THPIX);
	lwork->wreg = wreg;
}
if (wreg2 == NULL) {
	wreg2 = shRegNew("work image2", nrow, ncol, TYPE_THPIX);
	lwork->wreg2 = wreg2;
}

shRegClear(wreg2);
wreg2->row0 = 0;
wreg2->col0 = 0;

for (i = 0; i < nobjc; i++) {
	THOBJC *objc = objclist[i];
	THOBJCTYPE objctype = objc->thobjctype;
	char *objcname = NULL;
	status = thObjcNameGetFromType(objctype, &objcname); 
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (objcname) from (objctype)", name);
		return(status);
	}
	int match_fit_request = check_string_in_string_list(objcname, classnames, nclass);  
	if (match_fit_request) {
		int j;
		for (j = 0; j < nmodel; j++) {
			char *mname = mnames[j];
			THPIX (*fpixel)(PIXPOS *, void *);
			fpixel = fpixels[j];
			if (mname != NULL && strlen(mname) != 0 && thObjcHasModel(objc, mname)) {		
				THPROP *prop = NULL;
				status = thObjcGetPropByMname(objc, mname, &prop);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not get (thprop) for '%s'", name, mname);
					return(status);
				}
				char *mname3 = NULL, *pname2 = NULL;
				void *qmodel = NULL;
				status = thPropGet(prop, &mname3, &pname2, &qmodel);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not extract relevant info for '%s' from (thprop)", name, mname);
					return(status);
				}
				if (strcmp(mname, mname3)) {
					thError("%s: ERROR - found '%s' while looking for '%s' in (thrprop)", name, mname3, mname);
					return(SH_GENERIC_ERROR);
				}
				if (fpixel == NULL) {
					thError("%s: ERROR - no pixel function provided for '%s' found in (objc)", name, mname);
					return(SH_GENERIC_ERROR);
				}
				MODEL_ELEM *melem = NULL;
				status = thModelElemGetByName(mname, &melem);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not get (model_elem) for '%s'", name, mname);
					return(status);
				}
				char *mname2 = NULL;
				TYPE ptype= UNKNOWN_SCHEMA;
				char *pname = NULL;
				status = thModelElemGet(melem, &mname2, &ptype, &pname,  
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not get necessary information about '%s' from model_elem", 
					name, mname);
					return(status);
				}
				if (mname2 == NULL || strcmp(mname, mname2)) {
					thError("%s: ERROR - looking for '%s', found '%s'", name, mname, mname2);
					return(SH_GENERIC_ERROR);
				}
				if (strcmp(pname, pname2)) {
					thError("%s: ERROR - expecting '%s', found '%s' in (thprop)", name, pname, pname2);
					return(SH_GENERIC_ERROR);
				}
				int idr = 0, idc = 0;
				wreg->row0 = 0;
				wreg->col0 = 0;	
				shRegClear(wreg);
				status = CreateProfile(fpixel, qmodel, pname, wreg, idr, idc, NULL, -1);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not make the profile", name);
					return(status);
				}	
				THPIX I = THNAN;
				SCHEMA_ELEM *se = shSchemaElemGetFromType(ptype, "I");
				if (se == NULL) {
					thError("%s: ERROR - could not find (schema_elem) for 'I' in '%s'", name, pname);
					return(SH_GENERIC_ERROR);
				}
				THPIX *ptr_I = (THPIX *) shElemGet(qmodel, se, NULL);
				if (ptr_I == NULL) {
					thError("%s: ERROR - could not find record 'I' in '%s'", name, pname);
					return(SH_GENERIC_ERROR);
				}
				I = *ptr_I;
				double J = fabs((double) I / (double) MLE_SDSS_SN0);
				shRegMultWithDbl(J, wreg, wreg);
				shRegAdd(wreg, wreg2, wreg2);
				printf("%s: dampening core region signal (iobjc = %d, model = '%s'), (I = %g, SN0 = %g, I / SN0 = %g) \n", 
				name, i, mname, (float) I, (float) MLE_SDSS_SN0, (float) J);
				}
		}
	}
}

shAssert(nrow == wreg2->nrow);
shAssert(ncol == wreg2->ncol);

int j;
for (i = 0; i < nrow; i++) {
	THPIX *wrows_i = wreg2->rows_thpix[i];
	THPIX *rows_i = weight->rows_thpix[i];
	for (j = 0; j < ncol; j++) {
		rows_i[j] /= ((THPIX) 1.0 + wrows_i[j]);
	} 	
}

return(SH_SUCCESS);
}

RET_CODE thLprofileUpdateFromCalib(LPROFILE *lprofile, CALIB_WOBJC_IO *cwphobjc, CALIB_PHOBJC_IO *cphobjc) {
char *name = "thLprofileUpdateFromCalib";

if (cwphobjc == NULL && cphobjc == NULL) {
	thError("%s: WARNING - null input placeholder", name);
	return(SH_SUCCESS);
}
if (lprofile == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}

if (cphobjc != NULL) {
	memcpy(lprofile->objid, cphobjc->objid, 19 * sizeof(char));
	memcpy(lprofile->parentid, cphobjc->parentid, 19 * sizeof(char));
	memcpy(lprofile->fieldid, cphobjc->fieldid, 19 * sizeof(char));
	lprofile->skyversion = cphobjc->skyversion;
	lprofile->mode = cphobjc->mode;
	lprofile->clean = cphobjc->clean;
	lprofile->run = cphobjc->run;
	memcpy(lprofile->rerun, cphobjc->rerun, 3 * sizeof(char));
	lprofile->camcol = cphobjc->camcol;
	lprofile->field = cphobjc->field;
	lprofile->flagLRG = cphobjc->flagLRG;
} 

if (cwphobjc != NULL) {
	lprofile->id = cwphobjc->id;
	lprofile->parent = cwphobjc->parent;
	lprofile->nchild = cwphobjc->nchild;
	lprofile->objc_type = cwphobjc->objc_type;
	lprofile->objc_prob_psf = cwphobjc->objc_prob_psf;
	lprofile->catID = cwphobjc->catID;
	lprofile->objc_rowc = cwphobjc->objc_rowc;
	lprofile->objc_colc = cwphobjc->objc_colc;
	lprofile->flagLRG2 = cwphobjc->flagLRG;
}

return(SH_SUCCESS);
}


RET_CODE thLprofileUpdateFromWObjcIo(LPROFILE *lprofile, WOBJC_IO *wobjcio) {
char *name = "thLprofileUpdateFromWObjcIo";
if (wobjcio == NULL) {
	thError("%s: WARNING - null input", name);
	return(SH_SUCCESS);
}
if (lprofile == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
} 

lprofile->id = wobjcio->id;
lprofile->parent = wobjcio->parent;
lprofile->nchild = wobjcio->nchild;
lprofile->objc_type = wobjcio->objc_type;
lprofile->objc_prob_psf = wobjcio->objc_prob_psf;
lprofile->catID = wobjcio->catID;
lprofile->objc_rowc = wobjcio->objc_rowc;
lprofile->objc_colc = wobjcio->objc_colc;
#if USE_FNAL_OBJC_IO
lprofile->flagLRG3 = wobjcio->flagLRG;
#endif

return(SH_SUCCESS);
}

RET_CODE thLprofileMakeFromObjc(LPROFILE *lprofile, THOBJC *objc, LSTRUCT *l) {
char *name = "thLprofileMakeFromObjc";
if (lprofile == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (objc == NULL) {
	thError("%s: ERROR - null intput (objc) passed", name);
	return(SH_GENERIC_ERROR);
}
if (l == NULL) {
	thError("%s: ERROR - null lstruct passed", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;

PHPROPS *phprop = (PHPROPS *) objc->phprop;
CALIB_WOBJC_IO *cwphobjc = NULL;
CALIB_PHOBJC_IO *cphobjc = NULL;
WOBJC_IO *wobjcio = NULL;


status = thPhpropsGet(phprop, &wobjcio, NULL, &cwphobjc, &cphobjc, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get needed information from (phprop) in (objc = %p)", name, (void *) objc);
	return(status);
}
status = thLprofileUpdateFromCalib(lprofile, cwphobjc, cphobjc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update (lprofile) from calibrated object info", name);
	return(status);
}

status = thLprofileUpdateFromWObjcIo(lprofile, wobjcio);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update (lprofile) from (wobjcio)", name);
	return(status);
}

status = thTransModelPars(objc, "LPROFILE", lprofile, MODELTOIO);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not transform model parameter onto LPROFILE for (objc = %p)", name, (void *) objc);
	return(status);
}

LWORK *lwork = NULL;
status = thLstructGetLwork(l, &lwork);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwork) from (lstruct)", name);
	return(status);
}
/* 
REGION *wreg = NULL;
*/
PSFWING *wpsfwing = NULL; /* this is not really a psf */
status = thLworkGetWpsfwing(lwork, &wpsfwing);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (wpsfwing) from (lwork)", name);
	return(status);
}
int nmodel = -1;
status = thObjcGetNmodel(objc, &nmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (nmodel) from (objc)", name);
	return(status);
}

lprofile->nmodel = nmodel;

int i;
THPIX *rad = lprofile->rad;
for (i = 0; i < PROFILENRAD; i++) {
	rad[i] = i;
}

/* 
int anndex_ver0 = -1;
status = thPsfGetAnndexVer(&anndex_ver0);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (working anndex version)", name);
	return(status);
}
int anndex_ver = ANNULUS_UPDATED_GALAXY_VER;
status = thPsfSetAnndexVer(anndex_ver);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not set (anndex version) to (%d)", name, anndex_ver);
	return(status);
}
*/
for (i = 0; i < nmodel; i++) {
	THPROP *mprop = NULL;
	status = thObjcGetModelPropByIndex(objc, i, &mprop);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get model (%d) from (objc: %p)", name, i, (void *) objc);
		return(status);
	}
	status = thLProfileAddModelProfile(l, lprofile, i, mprop, wpsfwing); 
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add model info (%d) in objc (%p) to lprofile", name, i, (void *) objc);
		return(status);
	}
}
THPIX *profile = lprofile->profile;
memset(profile, '\0', PROFILENRAD * sizeof(THPIX));
for (i = 0; i < nmodel; i++) {
	THPIX *mprofile = lprofile->modelprofiles + PROFILENRAD * i;
	int j;
	for (j = 0; j < PROFILENRAD; j++) {
		profile[j] += mprofile[j];
	}	
}
/* 
status = thPsfSetAnndexVer(anndex_ver0);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not revert (working anndex ver) to (%d)", name, anndex_ver0);
	return(status);
} 
*/
lprofile->counts = 0.0;
for (i = 0; i < nmodel; i++) {
	lprofile->counts += lprofile->modelcounts[i];
}
return(SH_SUCCESS);
}


RET_CODE thLProfileAddModelProfile(LSTRUCT *l, LPROFILE *lprofile, int i, THPROP *model, PSFWING *wpsfwing) {

char *name = "thLprofileAddModelProfile";
if (l == NULL) {
	thError("%s: ERROR - null input (l)", name);
	return(SH_GENERIC_ERROR);
}
if (lprofile == NULL) {
	thError("%s: ERROR - null output placeholder (lprofile)", name);
	return(SH_GENERIC_ERROR);
}
if (model == NULL) {
	thError("%s: ERROR - null input (model)", name);
	return(SH_GENERIC_ERROR);
}
if (wpsfwing == NULL) {
	thError("%s: ERROR - null workspace (wpsfwing)", name);
	return(SH_GENERIC_ERROR);
}
if (i < 0 || i > NCOMPONENT) {
	thError("%s: ERROR - unacceptable value of model index (i = %d) while expecting a number between 0 and %d", name, i, (int) NCOMPONENT);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
char *mname = NULL;
status = thPropGet(model, &mname, NULL, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get name for model '%p'", name, (void *) model);
	return(status);
}
THPIX mag = VALUE_IS_BAD, counts = 0.0;
status = thPropGetCountMag(model, &counts, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (counts) and (mag) for model '%s'", name, mname);
	return(status);
}

#if LP_MAKE_1D_PROF

REGION *reg = wpsfwing->reg;
if (reg == NULL) {
	reg = shRegNew("model reg for LPROFILE in LFIT",
	(int) (2 * PROFILENRAD + 1), (int) (2 * PROFILENRAD), TYPE_THPIX);
	wpsfwing->reg = reg;
}
THPIX amp = 0.0;
status = thLMakeModelReg(model, reg, &amp, l);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make simple region for model '%s'", name, mname);
	return(status);
	}
status = thAnnulusGetFromReg(wpsfwing);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make annulus sums for model '%s'", name, mname);
	return(status);
}
status = thPsfRegGetFromAnnulus(wpsfwing);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not turn annulus sum into radial avg for model '%s'", name, mname);
	return(status);
}

int nrow = reg->nrow;
int i_center, n_center;
i_center = nrow / 2;
n_center = i_center + (nrow % 2);
THPIX *rad_profile = reg->rows_thpix[i_center] + i_center; /* this radial profile will be used to find FWHM */

int nrad = MIN(PROFILENRAD, n_center);
/* strcpy(lprofile->modelnames[i], mname);
*/
THPIX *mprofile = lprofile->modelprofiles + i * PROFILENRAD;
memset(mprofile, '\0', PROFILENRAD * sizeof(THPIX));
memcpy(mprofile, rad_profile, n_center * sizeof(THPIX));
for (i = 0; i < n_center; i++) {
	mprofile[i] *= amp;
}
#endif

lprofile->modelnames[i] = mname;
lprofile->modelcounts[i] = counts;
lprofile->modelmags[i] = mag;
return(SH_SUCCESS);
}


RET_CODE thLMakeModelReg(THPROP *model, REGION *reg, THPIX *amp, LSTRUCT *l) {
char *name = "thLMakeModelReg";
if (model == NULL) {
	thError("%s: ERROR - null input model", name);
	return(SH_GENERIC_ERROR);
	}
if (reg == NULL) {
	thError("%s: ERROR - null output reg", name);
	return(SH_GENERIC_ERROR);
}
if (l == NULL) {
	thError("%s: ERROR - null (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
if (amp == NULL) {
	thError("%s: ERROR - null output placeholder for (amp)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
int imodel = BAD_INDEX;
LIMPACK **impacks = NULL, *impack = NULL;
void *fparam = NULL;
char *mname = NULL, *pname = NULL;
char **rnames = NULL; 
int nmpar = 0;
MLEFL *amp_list = NULL;
THREGION *threg = NULL;
int nmodel = -1;

LMACHINE *map = NULL;
status = thLstructGetLmachine(l, &map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmachine) from (lstruct)", name);
	return(status);
}
status = thPropGet(model, &mname, &pname, &fparam);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get contents of (thprop)", name);
	return(status);
}
status = thMapmachineModelIndexGetFromThprop(map, model, &imodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get index for model (thprop = %p, model name = %s)", name, (void *) model, mname);
	return(status);
}
status = thLstructGetNModel(l, &nmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (nmodel) from (lstruct)", name);
	return(status);
}
if (imodel >= nmodel) {
	thError("%s: ERROR - (imodel = %d) while (nmode = %d)", name, imodel, nmodel);
	return(SH_GENERIC_ERROR);
} else if (imodel < 0) {
	thError("%s: ERROR - could not locate model in lstruct, imodel = %d", name, imodel);
	return(SH_GENERIC_ERROR);
}
/* 
status = UploadModelToMemory(imodel, l); should we not update the parameter list accordingly 
*/
status = thLstructGetImpacks(l, &impacks);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (impacks) from (lstruct)", name);
	return(status);
}
impack = impacks[LPIMPACKINDEX];
status = thConstructImagePack(l, imodel, impack);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not construct (impack)", name);
	return(status);
}

threg = impack->mreg[0];
FABRICATION_FLAG fab0 = UNKNOWN_FABRICATION_FLAG;
status = thRegGetFabFlag(threg, &fab0);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get fab flag in (threg = %p)", name, (void *) threg);
	return(status);
}
/* RFLAG in the following command is set such that the object is 
definitely generated but the derivatives are not */
status = thBuildImage(fparam, mname, rnames, nmpar, impack, IMAGES);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not build image for model '%s'", name, mname);
	return(status);
}

FABRICATION_FLAG fab = UNKNOWN_FABRICATION_FLAG;
status = thRegGetFabFlag(threg, &fab);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get fab flag in (threg = %p)", name, (void *) threg);
	return(status);
}
if (fab != FAB && fab != PSFCONVOLVED) {
	thError("%s: ERROR - (threg = %p) for (model = '%s') not properly fabricated (fab = '%s')", 
	name, (void *) threg, mname, shEnumNameGetFromValue("FABRICATION_FLAG", fab));
	return(SH_GENERIC_ERROR);
}

status = thLstructGetAmp(l, &amp_list);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (amp) from (lstruct)", name);
	return(status);
}
*amp = amp_list[imodel];
status = thRegionConvertToshReg(threg, reg);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not convert (thregion) to dervish style for model '%s'", name, mname);
	return(status);
}

if (fab0 == PREFAB) {
	status = thFreeImpackMemory(impack);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not free memory allocated to impack in (model = '%s')", name, mname);
		return(status);
	}
} else if (fab0 == FAB || fab0 ==  PSFCONVOLVED) {
	thError("%s: WARNING - (thregion) for model '%s' was found with (fab = '%s') prior to construction", name, mname, shEnumNameGetFromValue("FABRICATION_FLAG", fab0));
	} else {
	thError("%s: WARNING - model '%s' came with (fab = '%s') prior to build. expect error", name, mname, shEnumNameGetFromValue("FABRICATION_FLAG", fab0));
	}

int ret, free_to_os = 1;
ret = shMemDefragment(free_to_os);
if (ret != 0) {
	thError("%s: ERROR - problem defragmenting memory", name);
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE thLfitAddLprofileFromLstruct(LFIT *lfit, LSTRUCT *l, XUPDATE update) {
char *name = "thLfitAddLprofileFromLstruct";


RET_CODE status;
LFITRECORDTYPE rtype = NULL_LFITRECORD;
status = thLfitGetRtype(lfit, &rtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (rtype) from (lfit)", name);
	return(status);
}
if ((rtype & LP_RECORD_ANY) && 
	((rtype & LP_RECORD_NOW) && (rtype & LP_RECORD_LASTOFBEST)))        {
	thError("%s: ERROR - confusing (rflag = %d) 'LP_RECORD_ANY' 'LP_RECORD_NOW', 'LP_RECORD_LAST_OF_BEST' are all set", name, (int) rtype);
	return(SH_GENERIC_ERROR);
}
 
if (!(rtype & LP_RECORD_ANY) && 	
	!((rtype & LP_RECORD_NOW) && (rtype & LP_RECORD_LASTOFBEST))) return(SH_SUCCESS); /* no request for recording (lprofile) has been made */

int nlp = -1, nlpmax = -1;
status = thLfitGetNLprofile(lfit, &nlp, &nlpmax);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (nlp) from LFIT", name);
	return(status);
}
int nlp_old = nlp;
if (nlp >= nlpmax) {
	thError("%s: ERROR - not enough space in LFIT to record LPROFILE (nlp = %d, lp_nmax = %d)", name, nlp, nlpmax);
	return(SH_GENERIC_ERROR);
}

LMACHINE *map = NULL;
status = thLstructGetLmachine(l, &map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmachine) from (lstruct)", name);
	return(status);
}

LMETHOD *lmethod = NULL;
status = thLstructGetLmethod(l, &lmethod);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmethod) from (lstruct)", name);
	return(status);
}

CONVERGENCE convergence = UNKNOWN_CONVERGENCE;
LM_STEP_STATUS step_status = UNKNOWN_LM_STEP_STATUS;
int nsteps = -1;
status = thLstructGetConvergence(l, &nsteps, &convergence, &step_status);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (convergence flag) from (lstruct)", name);
	return(status);
}

int make_profile = ((rtype & (LP_RECORD_ALL)) ||
	((rtype & LP_RECORD_ALLGOOD) && (update == UPDATED)) ||
	((rtype & LP_RECORD_FIRSTANDLAST) && (update == UPDATED) && convergence != CONTINUE) || 
	((rtype & LP_RECORD_NOW) && (rtype & LP_RECORD_LASTOFBEST)));
if (make_profile) {
	#if DEBUG_MLE_LPROFILE
	printf("%s: creating LP structure for fit objects (nstep = %d), (convergence = '%s'), (step_status = '%s') \n", 
	name, nsteps, shEnumNameGetFromValue("CONVERGENCE", convergence), shEnumNameGetFromValue("LM_STEP_STATUS", step_status));
	#endif
	THOBJC **objclist = NULL;
	int nobjc = 0, npar = 0;
	status = thLstructGetNPar(l, &npar);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (npar) from (lstruct)", name);
		return(status);
	}
	if (npar > 0) {
		/* should be the unique list */
		status = thMapmachineGetUPObjcs(map, (void ***) &objclist, &nobjc);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get fit (objclist) from (map)", name);
			return(status);
		}
	} else {
		status = thMapmachineGetUObjcs(map, (void ***) &objclist, &nobjc);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get all (objclist) from (map)", name);
			return(status);
		}
	}
	if (nobjc <= 0) {
		thError("%s: WARNING - found no objects to record LPROFILE (nobjc = %d)", name, nobjc);
		return(SH_SUCCESS);
	}
	int i;
	for (i = 0; i < nobjc; i++) {
		if (nlp >= nlpmax) {
			thError("%s: ERROR - not enough space in LFIT for next LP (nlp = %d, nlp_max = %d) while (iobjc = %d)", name, nlp, nlpmax, i);
			status = thLfitPutNLprofile(lfit, nlp);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not update (nlp = %d) in (lfit) while (nlp_max = %d)", name, nlp, nlpmax);
				return(status);
			}
			return(SH_GENERIC_ERROR);
		}
		THOBJC *objc = objclist[i];
		char *classname = NULL;
		status = thObjcGetClassName(objc, &classname);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get classname for (objc = %p)", name, objc);
			return(status);
		}
		if (classname == NULL || strlen(classname) == 0) {
			thError("%s: ERROR - null or empty classname for (objc = %p)", name, objc);
			return(SH_GENERIC_ERROR);
		}
		if (strstr(classname, "SKY") == NULL && strstr(classname, "sky") == NULL) {				/* this is not a sky object */
			LPROFILE *lprofile = NULL;
			status = thLfitGetLprofileByIndex(lfit, nlp, &lprofile);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get LPROFILE at (index = %d) from LFIT", name, nlp);
				return(status);
			}
			status = thLprofileMakeFromObjc(lprofile, objc, l);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get make LPROFILE for (objc = %p, iobjc = %d)", name, (void *) objc, i);
				return(status);
			}
			status = thLprofileAddLmethodInfo(lprofile, lmethod);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not add (lmethod) info to (lprofile)", name);
				return(SH_GENERIC_ERROR);
			}
			nlp++;
		}	
	}
	if ((rtype & LP_RECORD_FIRSTANDLAST) && (nlp_old != 0)) {
		nlp = nlp_old;
	}
	status = thLfitPutNLprofile(lfit, nlp);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get put (nlp = %d) in LFIT", name, nlp);
		return(status);
	}
} else if (!(rtype & LP_RECORD_ANY)) {
	thError("%s: WARNING - no LP recording is requested (rtype = %x)", name, rtype);
}
 
return(SH_SUCCESS);
}

RET_CODE thLUPObjcRenewFromPhprop(LSTRUCT *l) {
char *name = "thLUPObjcRenewFromPhprop";
if (l == NULL) {
	thError("%s: ERROR - null (lstruct) passed", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
LMACHINE *map = NULL;
status = thLstructGetLmachine(l, &map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmachine) from (lstruct)", name);
	return(status);
}
if (map == NULL) {
	thError("%s: ERROR - null (lmachine) found in (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
int nupobjc = 0;
THOBJC **upobjcs = NULL;
status = thMapmachineGetUPObjcs(map, (void ***) &upobjcs,  &nupobjc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not obtain list of (UPObjcs) from (map)", name);
	return(status);
}
if ((nupobjc < 0 && upobjcs != NULL) || (nupobjc != 0 && upobjcs == NULL)) {
	thError("%s: ERROR - found (nupobjc = %d) while array (upobjc = %p)", name, nupobjc, (void *) upobjcs);
	return(SH_GENERIC_ERROR);
}
if (nupobjc == 0) {
	if (upobjcs != NULL && upobjcs[0] != NULL) {
		thError("%s: ERROR - found (nupobjcs = %d) while (UPobjcs[0] = %p)", name, nupobjc, upobjcs[0]);
		return(SH_GENERIC_ERROR);
	}
	thError("%s: WARNING - no (UPobjc) found in (map) - (nUPobjc = %d)", name, nupobjc);
	return(SH_SUCCESS);
}

int i;
for (i = 0; i < nupobjc; i++) {
	THOBJC *objc = upobjcs[i];
	status = thObjcRenewFromPhprop(objc);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not renew (UPobjc = %p, iUPobjc = %d) from (phprop)", name, (void *) objc, i);
		return(status);
	}
}

return(SH_SUCCESS);
}

RET_CODE thObjcTweakWobjc(THOBJC *objc, int fitflag) {
char *name = "thObjcTweakWobjc";
if (objc == NULL) {
	thError("%s: ERROR - null (objc)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
PHPROPS *phprop = NULL;
status = thObjcGetPhprops(objc, &phprop);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (phprop) from (objc = %p)", name, (void *) objc);
	return(status);
}
if (phprop == NULL) {
	thError("%s: ERROR - null (phprops) found in (objc = %p)", name, (void *) objc);
	return(SH_GENERIC_ERROR);
}
WOBJC_IO *wobjc_io = NULL;
status = thPhpropsGet(phprop, &wobjc_io, NULL, NULL, NULL, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (wobjc_io) for (phprop) obtained for (objc = %p)", name, (void *) objc);
	return(status);
}
float tweakf = (float) DEFAULT_PARAMETER_TWEAK_COEFF;
status = tweak_wobjc_io(wobjc_io, tweakf, fitflag);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not tweak (wobjc_io) found in (objc = %p)", name, (void *) objc);
	return(status);
}
return(SH_SUCCESS);
}



RET_CODE tweak_wobjc_io(WOBJC_IO *objc, float tweakf, int fitflag) {
char *name = "tweak_wobjc_io";
if (objc == NULL) {
	thError("%s: ERROR - null objc", name);
	return(SH_GENERIC_ERROR);
}

float epsilon1, epsilon2, epsilon3, epsilon4, epsilon5;
int bndex;
if (fitflag & (MFIT_RE | MFIT_RB | MFIT_RE1 | MFIT_RE2 | MFIT_RB1 | MFIT_RB2)) {
	int passed = 1;
	do  {
	passed = 1;
	epsilon1 = (float) parameter_epsilon_random_draw() * tweakf;
	epsilon2 = (float) parameter_epsilon_random_draw() * tweakf;
	epsilon3 = (float) parameter_epsilon_random_draw()* tweakf;
	epsilon4 = (float) parameter_epsilon_random_draw()* tweakf;
	epsilon5 = (float) parameter_epsilon_random_draw()* tweakf;
	
	for (bndex = 0; bndex < NCOLOR; bndex++) {
		if (objc->r_deV[bndex] != VALUE_IS_BAD && objc->r_deVErr[bndex] != VALUE_IS_BAD) {
			#if 1
			objc->r_deV[bndex] += objc->r_deVErr[bndex] * epsilon1;
			passed &= (objc->r_deV[bndex] >= 0.0);
			#else
			float x = 1.0 + objc->r_deVErr[bndex] * epsilon1 / objc->r_deV[bndex];
			passed &= (x >= 0.0 || isnan(x) || isinf(x));
			if (passed) {
				objc->r_deV[bndex] *= x;
				objc->counts_deV[bndex] *= pow(x, 2.0);
			}
			#endif
		}
		#if USE_FNAL_OBJC_IO
		
		#if 1
		if (objc->r_deV2[bndex] != VALUE_IS_BAD && objc->r_deV2Err[bndex] != VALUE_IS_BAD) {
		objc->r_deV2[bndex] += objc->r_deV2Err[bndex] * epsilon2;
		passed &= objc->r_deV2[bndex] >= 0.0;
		}
		if (objc->r_pl[bndex] != VALUE_IS_BAD && objc->r_plErr[bndex] != VALUE_IS_BAD) {
		objc->r_pl[bndex] += objc->r_plErr[bndex] * epsilon3;
		passed &= objc->r_pl[bndex] >= 0.0;
		}
		if (objc->r_csersic[bndex] != VALUE_IS_BAD && objc->r_csersicErr[bndex] != VALUE_IS_BAD) {
		objc->r_csersic[bndex] += objc->r_csersicErr[bndex] * epsilon4;
		passed &= objc->r_csersic[bndex] >= 0.0;
		}
		if (objc->rb_csersic[bndex] != VALUE_IS_BAD && objc->rb_csersicErr[bndex] != VALUE_IS_BAD) {
		objc->rb_csersic[bndex] += objc->rb_csersicErr[bndex] * epsilon5;
		passed &= objc->rb_csersic[bndex] >= 0.0;
		}


		#else
		if (objc->r_deV2[bndex] != VALUE_IS_BAD) { 
		float x = 1.0 + objc->r_deV2Err[bndex] * epsilon2 / objc->r_deV2[bndex];
		passed &= (x > 0.0 || isnan(x) || isinf(x));
		if (passed) {
			objc->r_deV2[bndex] *= x;
			objc->counts_deV2[bndex] *= pow(x, 2.0);
		}
		}
		if (objc->r_pl[bndex] != VALUE_IS_BAD) { 
		float x = 1.0 + objc->r_plErr[bndex] * epsilon3 / objc->r_pl[bndex];
		passed &= (x > 0.0 || isnan(x) || isinf(x));
		if (passed) {
			objc->r_pl[bndex] *= x;
			objc->counts_pl[bndex] *= pow(x, 2.0);
		}
		}
		#endif
		#endif
	}
	} while (passed == 0);
	do {
	passed = 1;
	epsilon2 = (float) parameter_epsilon_random_draw() * tweakf;
	epsilon1 = (float) parameter_epsilon_random_draw() * tweakf;
	for (bndex = 0; bndex < NCOLOR; bndex++) {
		if (objc->r_exp[bndex] != VALUE_IS_BAD && objc->r_expErr[bndex] != VALUE_IS_BAD) {
			objc->r_exp[bndex] += objc->r_expErr[bndex] * epsilon1;
			passed &= objc->r_exp[bndex] >= 0.0;
		}
		#if USE_FNAL_OBJC_IO
		if (objc->r_exp2[bndex] != VALUE_IS_BAD && objc->r_exp2Err[bndex] != VALUE_IS_BAD) {
			objc->r_exp2[bndex] += objc->r_exp2Err[bndex] * epsilon2;
			passed &= objc->r_exp2[bndex] >= 0.0;
		}
		#endif
	}
	} while (passed == 0);
}
if (fitflag & (MFIT_PHI | MFIT_PHI1 | MFIT_PHI2)) {
	epsilon1 = (float)  parameter_epsilon_random_draw() * tweakf;
	epsilon2 = (float)  parameter_epsilon_random_draw() * tweakf;
	epsilon3 = (float)  parameter_epsilon_random_draw() * tweakf;
	epsilon4 = (float)  parameter_epsilon_random_draw() * tweakf;
	for (bndex = 0; bndex < NCOLOR; bndex++) {
		objc->phi_deV[bndex] += objc->phi_deVErr[bndex] * epsilon1;	
		#if USE_FNAL_OBJC_IO
		objc->phi_deV2[bndex] += objc->phi_deV2Err[bndex] * epsilon2;
		objc->phi_pl[bndex] += objc->phi_plErr[bndex] * epsilon3;
		objc->phi_csersic[bndex] += objc->phi_csersicErr[bndex] * epsilon4;
		#endif
	}
	epsilon2 = (float)  parameter_epsilon_random_draw() * tweakf;
	epsilon1 = (float)  parameter_epsilon_random_draw() * tweakf;
	for (bndex = 0; bndex < NCOLOR; bndex++) {
		objc->phi_exp[bndex] += objc->phi_expErr[bndex] * epsilon1;	
		#if USE_FNAL_OBJC_IO
		objc->phi_exp2[bndex] += objc->phi_exp2Err[bndex] * epsilon2;
		#endif
	}
}
if (fitflag & (MFIT_CENTER | MFIT_CENTER1 | MFIT_CENTER2)) {
	epsilon1 = (float)  parameter_epsilon_random_draw() * tweakf;
	epsilon2 = (float)  parameter_epsilon_random_draw() * tweakf;
	objc->objc_rowc += objc->objc_rowcErr * epsilon1;
	objc->objc_colc += objc->objc_colcErr * epsilon2;
	for (bndex = 0; bndex < NCOLOR; bndex++) {
		objc->rowc[bndex] += objc->rowcErr[bndex] * epsilon1;
		objc->colc[bndex] += objc->colcErr[bndex] * epsilon2;
	}
}
if (fitflag & (MFIT_E | MFIT_E1 | MFIT_E2)) {
	int passed = 1;
	do {
	passed = 1;
	epsilon1 = (float)  parameter_epsilon_random_draw() * tweakf;
	epsilon2 = (float)  parameter_epsilon_random_draw() * tweakf;
	epsilon3 = (float)  parameter_epsilon_random_draw() * tweakf;
	epsilon4 = (float)  parameter_epsilon_random_draw() * tweakf;
	for (bndex = 0; bndex < NCOLOR; bndex++) {
		if (objc->ab_deV[bndex] != VALUE_IS_BAD && objc->ab_deVErr[bndex] != VALUE_IS_BAD) {
			objc->ab_deV[bndex] += objc->ab_deVErr[bndex] * epsilon1;	
			passed &= (objc->ab_deV[bndex] >= 0.0);
		}
		#if USE_FNAL_OBJC_IO
		if (objc->ab_deV2[bndex] != VALUE_IS_BAD && objc->ab_deV2Err[bndex] != VALUE_IS_BAD) {
			objc->ab_deV2[bndex] += objc->ab_deV2Err[bndex] * epsilon2;
			passed &= (objc->ab_deV2[bndex] >= 0.0);
		}
		if (objc->ab_pl[bndex] != VALUE_IS_BAD && objc->ab_plErr[bndex] != VALUE_IS_BAD) {
			objc->ab_pl[bndex] += objc->ab_plErr[bndex] * epsilon3;
			passed &= (objc->ab_pl[bndex] >= 0.0);
		}
		if (objc->ab_csersic[bndex] != VALUE_IS_BAD && objc->ab_csersicErr[bndex] != VALUE_IS_BAD) {
			objc->ab_csersic[bndex] += objc->ab_csersicErr[bndex] * epsilon4;
			passed &= (objc->ab_csersic[bndex] >= 0.0);
		}

		#endif
	}
	} while (passed == 0);
	do {
	passed = 1;
	epsilon2 = (float)  parameter_epsilon_random_draw() * tweakf;
	epsilon1 = (float)  parameter_epsilon_random_draw() * tweakf;
	for (bndex = 0; bndex < NCOLOR; bndex++) {
		if (objc->ab_exp[bndex] != VALUE_IS_BAD && objc->ab_expErr[bndex] != VALUE_IS_BAD) {
			objc->ab_exp[bndex] += objc->ab_expErr[bndex] * epsilon1;	
			passed &= (objc->ab_exp[bndex] >= 0.0);
		}
		#if USE_FNAL_OBJC_IO
		if (objc->ab_exp2[bndex] != VALUE_IS_BAD && objc->ab_exp2Err[bndex] != VALUE_IS_BAD) {
			objc->ab_exp2[bndex] += objc->ab_exp2Err[bndex] * epsilon2;
			passed &= (objc->ab_exp2[bndex] >= 0.0);
		}
		#endif
	}
	} while (passed == 0);
}
if (fitflag & (MFIT_INDEX | MFIT_INDEX1 | MFIT_INDEX2)) {
	int passed = 1;
	do {
	passed = 1;
	for (bndex = 0; bndex < NCOLOR; bndex++) {
	epsilon1 = (float)  parameter_epsilon_random_draw();
	epsilon2 = (float)  parameter_epsilon_random_draw();
	objc->fracPSF[bndex] = epsilon1;
	passed &= (epsilon1 >= 0.0 && epsilon1 <= 1.0);
	#if USE_FNAL_OBJC_IO && 0
	if (objc->fracPSF2[bndex] != VALUE_IS_BAD) {
		objc->fracPSF2[bndex] = epsilon2;
		passed &= (epsilon2 >= 0.0 && epsilon2 <= 1.0);
	}
	#endif
	}
	} while (passed == 0);
}
return(SH_SUCCESS);
}

RET_CODE thLRefresh(LSTRUCT *l) {
char *name = "thLRefresh";
if (l == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

LSTAGE stages[4] = {INITSTAGE, MIDDLESTAGE, ENDSTAGE, SIMSTAGE};
int i;
RET_CODE status;

for (i = 0; i < 4; i++) {

	LSTAGE stage = stages[i];
	ALGORITHM *alg = NULL;
	status = thLstructGetAlgByStage(l, stage, &alg);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (algorithm) for (stage = '%s')", name, shEnumNameGetFromValue("LSTAGE", stage));
		return(status);
	}
	status = thAlgorithmRefresh(alg);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not refresh (algorithm) for (stage = '%s')", name, shEnumNameGetFromValue("LSTAGE", stage));
		return(status);
	} 

}

LMETHOD *lmethod = NULL;
status = thLstructGetLmethod(l, &lmethod);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmethod) from (lstruct)", name);
	return(status);
}
status = thLmethodDefault(lmethod);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not turn (lmethod) into default value", name);
	return(status);
}

LFIT *lfit = NULL;
status = thLstructGetLfit(l, &lfit);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lfit) from (lstruct)", name);
	return(status);
}
status = thLfitRefresh(lfit);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not refresh (lfit)", name);
	return(status);
}

status = KillAllModelsInMemory(l);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not free memory in model images", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE do_lfit_record_insert_chisq_in_LM(LSTRUCT *lstruct) {
char *name = "do_lfit_record_insert_chisq_cost_in_lmethod";

RET_CODE status;
CHISQFL Nchisq, Ncost;
status = thLstructGetChisqN(lstruct, &Nchisq, &Ncost);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (chisqN), (costN) from (lstruct)", name);
	return(status);
}
CHISQFL chisq, cost;
status = thLstructGetChisq(lstruct, &chisq, &cost);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (chisq), (cost) from (lstruct)", name);
	return(status);
}
LMETHOD *lmethod = NULL;
status = thLstructGetLmethod(lstruct, &lmethod); 
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmethod) from (lstruct)", name);
	return(status);
}
LFIT *lf = NULL;
status = thLstructGetLfit(lstruct, &lf);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lfit) from (lstruct)", name);
	return(status);
}
LFITRECORDTYPE rtype;
status = thLfitGetRtype(lf, &rtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (rtype) from (lfit)", name);
	return(status);
}
/* note that unless rtype is set to RECORD_ALL, only good steps should be chosen but separating by the value of chisq is not suggested here since it colludes the algorithm check */
CHISQFL lm_chisq, lm_cost;

if (rtype & LM_RECORD_ALL) { 
	lm_chisq = Nchisq;
	lm_cost = Ncost;
} else {
	lm_chisq = MIN(chisq, Nchisq);
	lm_cost = MIN(cost, Ncost);
}

status = thLmethodPutChisq(lmethod, lm_chisq, lm_cost);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not insert (chisqN / chisq) in (lmethod)", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE thLworkUpdateBqn(LWORK *lwork, LSECTOR sector) {
char *name = "thLworkUpdateBqn";

printf("%s: DEBUG - working with sector = '%s' \n", name, shEnumNameGetFromValue("LSECTOR", sector));

if (lwork == NULL) {
	thError("%s: ERROR - null (lwork) passed", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
MLEFL **Bk = NULL, **Bkplus1 = NULL;
MLEFL **BBk = NULL, **BBkplus1 = NULL;
MLEFL *dfk = NULL, *dfkplus1 = NULL;
MLEFL *yk = NULL; 
MLEFL  *dxk = NULL;
MLEFL *zk = NULL;

int n = 0;
status = thLworkGetQN(lwork, 
	&Bk, &Bkplus1, &BBk, &BBkplus1, 
	&dfk, &dfkplus1, 
	NULL, NULL, NULL, NULL, &dxk, &n, sector);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (quasi-newtonian) matrices from (lwork)", name);
	return(status);
}
if (n == 0) {
	thError("%s: WARNING - linear fit - found (n = %d) while retrieving (quasi-newtonian) matrices in '%s'", name, n, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_SUCCESS);
} else if (n < 0) {	
	thError("%s: ERROR - found (n = %d) while retrieving (quasi-newtonian) matrices in '%s'", name, n, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}

if (Bk == NULL || Bkplus1 == NULL || BBk == NULL || BBkplus1 == NULL || dfk == NULL || dfkplus1 == NULL || dxk == NULL) {
	thError("%s: ERROR - found null (quasi-newtonian) matrices in (lwork) while (n = %d) in '%s'", name, n, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}
int n2 = 0;
status = thLworkGetQNWorkSpace(lwork, &yk, &zk, &n2, sector);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (quasi-newtonian) work space from (lwork) for '%s'", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(status);
}
if (n2 != n) {
	thError("%s: ERROR - found mismatching sizes in two sets of (quasi-newtonian) matrices (n1 = %d, n2 = %d) for '%s'", name, n, n2, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}
if (yk == NULL || zk == NULL) {
	thError("%s: ERROR - found null (quasi-newtonian) work space in (lwork) while (n = %d) in '%s'", name, n, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}
MLEFL ydotdx = 0.0, dxBdx = 0.0;
int i, j, nyk_zero = 0;
for (i = 0; i < n; i++) {
	yk[i] = -(dfkplus1[i] - dfk[i]);
	if (yk[i] == (MLEFL) 0) nyk_zero++;
}
if (nyk_zero == n) {
	thError("%s: WARNING - discovered (yk[:] = 0) or (f(k) = f(k+1))", name);
} else {
	printf("%s: found (%d out of %d) zeros in y(k) \n", name, nyk_zero, n);
}
for (i = 0; i < n; i++) {
	ydotdx += yk[i] * dxk[i];
}
if (ydotdx == (MLEFL) 0) {
	thError("%s: WARNING - discovered (y.dx = 0)", name);
}

for (i = 0; i < n; i++) {
	MLEFL *B_i = Bk[i];
	MLEFL z_temp = 0.0;
	for (j = 0; j < n; j++) {
		z_temp += B_i[j] * dxk[j];
	}
	zk[i] = z_temp;
}
for (i = 0; i < n; i++) {
	dxBdx += zk[i] * dxk[i];
}
if (dxBdx == (MLEFL) 0) {
	thError("%s: WARNING - discovered || dx || _B == 0", name);
}
for (i = 0; i < n; i++) {
	memcpy(Bkplus1[i], Bk[i], n * sizeof(MLEFL));
}
#if SYMMETRIC_RANK_ONE
MLEFL y_zdotdx = ydotdx - dxBdx;
if (fabs((THPIX) y_zdotdx) >= 1.0E-8) {
	for (i = 0; i < n; i++) {
		MLEFL *Bkplus1_i = Bkplus1[i];
		MLEFL yk_zk_i = yk[i] - zk[i];
		for (j = 0; j < n; j++) {
			Bkplus1_i[j] += (yk_zk_i)* (yk[j] - zk[j])/ y_zdotdx;
		}
	}		
} else {
	thError("%s: WARNING - (y - Bs).s = %G ~ 0, setting B(k+1) = B(k) in symmetric-rank-one update", name, (double) y_zdotdx);
}
#else
if (ydotdx >= (MLEFL) 0.0) {
	for (i = 0; i < n; i++) {
		MLEFL *Bkplus1_i = Bkplus1[i];
		MLEFL yk_i = yk[i];
		for (j = 0; j < n; j++) {
			Bkplus1_i[j] += yk_i * yk[j] / ydotdx;
		}
	}
	if  (fabs(dxBdx) > 1.0E-8) {		
		for (i = 0; i < n; i++) {
			MLEFL *Bkplus1_i = Bkplus1[i];
			MLEFL zk_i = zk[i];
			for (j = 0; j < n; j++) {
				Bkplus1_i[j] -= zk_i * zk[j] / dxBdx;
		}
	}
	} else {
		thError("%s: WARNING - || s || _B = %G ~ 0, overlooking second update step for BFGS", name, (double) dxBdx);
	}
} else {
	thError("%s: WARNING - BFGS convexity condition y .s > 0 is not met ( y . s = %G)", naem, ydotdx);
}
	
#endif
MAT *wMatA = NULL, *wMatQ = NULL;
VEC **wVecs = NULL;
status = thLworkGetWMatVec(lwork, sector, &wMatA, &wMatQ, &wVecs);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get working Mat, Vec for '%s'", 
	name, shEnumNameGetFromValue("LSECTOR", sector));
	return(status);
}	

status = LMatrixAbsoluteValueSVD(Bkplus1, BBkplus1, n, wMatA, wMatQ, wVecs);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make |B(k+1)| from B(k+1)", name);
	return(status);
}

int nan_count = 0;
for (i = 0; i < n; i++) {	
	MLEFL *Bkplus1_i = Bkplus1[i];
	for (j = 0; j < n; j++) {
		MLEFL x_temp = Bkplus1_i[j];
		if (isnan(x_temp) ||  isinf(x_temp)) {
			nan_count++;
		}
	}
}
if (nan_count > 0) {
	thError("%s: ERROR - found (%d) instances (out of %d) of (NaN or Inf) in new estimate of (Bqn)", name, nan_count, (int) (n*n));
	return(SH_GENERIC_ERROR);
}
nan_count = 0;
for (i = 0; i < n; i++) {	
	MLEFL *BBkplus1_i = BBkplus1[i];
	for (j = 0; j < n; j++) {
		MLEFL x_temp = BBkplus1_i[j];
		if (isnan(x_temp) ||  isinf(x_temp)) {
			nan_count++;
		}
	}
}
if (nan_count > 0) {
	thError("%s: ERROR - found (%d) instances (out of %d) of (NaN or Inf) in new estimate of (BBqn)", name, nan_count, (int) (n*n));
	return(SH_GENERIC_ERROR);
}

#if DEBUG_MLE_QN

printf("%s: B(k) = \n", name);
output_mlefl_sqr_matrix(Bk, n);
printf("%s: BB(k) = \n", name);
output_mlefl_sqr_matrix(BBk, n);

printf("%s: B(k+1) = \n", name);
output_mlefl_sqr_matrix(Bkplus1, n);
printf("%s: BB(k+1) = \n", name);
output_mlefl_sqr_matrix(BBkplus1, n);

#endif

return(SH_SUCCESS);
}

RET_CODE is_derivative_too_small_for_gauss_newton(MLEFL *Fprime, CHISQFL F, int n, int *small_flag) {
char *name = "is_derivative_too_small_for_gauss_newton";
shAssert(small_flag != NULL);
if (n <= 0) {
	thError("%s: ERROR - expect positive size, received (n = %d)", name, n);
	return(SH_GENERIC_ERROR);
}
if (F < (CHISQFL) 0.0) {
	thError("%s: ERROR - expected positive chisq value - received (F = %G)", name, (double) F);
	return(SH_GENERIC_ERROR);
}
if (Fprime == NULL) {
	thError("%s: ERROR - null (F') vector passed", name);
	return(SH_GENERIC_ERROR);
}
MLEFL abs_Fprime = 0.0;
int i;
for (i = 0; i < n; i++) {
	if (fabs(Fprime[i]) > abs_Fprime) abs_Fprime = fabs(Fprime[i]);
}
int res = (((CHISQFL) abs_Fprime) < (((CHISQFL) LM_GAUSS_NEWTON_THRESHOLD) * F));
*small_flag = res;
return(SH_SUCCESS);
}

RET_CODE thLmethodDecideWorkType(LSTRUCT *lstruct, LMETHOD *lmethod, LWORK *lwork) {
char *name = "thLmethodDecideWorkType";
if (lmethod == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
LMETHODTYPE type = UNKNOWN_LMETHODTYPE;
LMETHODSUBTYPE wtype = UNKNOWN_LMETHODSUBTYPE;

status = thLmethodGetType(lmethod, &type, &wtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (type) records from (lmethod)", name);
	return(status);
}
if (type == LM || type == EXTLM) {
	wtype = LEVENBERGMARQUARDT;
} else if (type == HYBRIDLM) {
	int step_count = 0, step_mode = 0;
	status = thLmethodGetStepCount(lmethod, &step_count, &step_mode);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (step_count) from (lmethod)", name);
		return(status);
	}
	if ((wtype == LEVENBERGMARQUARDT) && (step_count >= (int) HYBRID_LM_ALLOWANCE)) {
		wtype = QUASINEWTON;
		step_count = 0;
		step_mode = 0;
		status = initiate_QN_delta(lstruct, lmethod, lwork, wsector);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not initiate (delta_qn) for (quasi-newtonian) method", name);
			return(status);
		}
	} else if ((wtype == QUASINEWTON) && (step_count >= (int) HYBRID_QN_ALLOWANCE)) {
		wtype = LEVENBERGMARQUARDT;
		step_count = 0;
		step_mode = 0;
	} else if (wtype != LEVENBERGMARQUARDT && wtype != QUASINEWTON) {
		thError("%s: ERROR - unsupported (work method = '%s') found in (lmethod)", name, shEnumNameGetFromValue("LMETHODSUBTYPE", wtype));
		return(SH_GENERIC_ERROR);
	} 
	status = thLmethodPutStepCount(lmethod, step_count, step_mode);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put (step_count) in (lmethod)", name);
		return(status);
	}
} else if (type == GD || type == SGD) {
	wtype = STEEPESTDESCENT;
} else if (type == PGD || type == MGD || type == NAG || type == RSV) {
	
} else {
	thError("%s: ERROR - (method_type = '%s') not supported", name, shEnumNameGetFromValue("LMETHODTYPE", type));
	wtype = UNKNOWN_LMETHODSUBTYPE;
	status = thLmethodPutType(lmethod, type, wtype);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update (lmethod_type)", name);
		return(status);
	}
	return(SH_GENERIC_ERROR);
}

status = thLmethodPutType(lmethod, type, wtype);  
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update (lmethod_types)", name);
	return(status);
}
return(SH_SUCCESS);
}

MLEFL mag_mlefl_vector(MLEFL *a, MLEFL **metric, int n, int inf) {
char *name = "mag_mlefl_vector";
shAssert(a != NULL);
shAssert(n > 0);
shAssert(inf == 0 || inf == 1);
int i, j;
MLEFL magnitude = 0.0;
if (inf == 1) {
	for (i = 0; i < n; i++) {
		if (fabs(a[i]) > magnitude) magnitude = (MLEFL) fabs(a[i]);
	}
} else {
	if (metric == NULL) {
		for (i = 0; i < n; i++) {
			magnitude += pow(a[i], 2.0);
		}
		magnitude = pow(magnitude, 0.5);
	} else {
		for (i = 0; i < n; i++) {
			MLEFL a_i = a[i];
			MLEFL *metric_i = metric[i];
			for (j = 0; j < n; j++) {
				magnitude += a_i * a[j] * metric_i[j];
			}
		}
		if (magnitude < 0.0) {
			thError("%s: WARNING - metric is not positive definitive", name);
		}
		magnitude = pow(fabs(magnitude), 0.5);
	}
}
return(magnitude);
}

THPIX mag_thpix_vector(THPIX *a, THPIX **metric, int n, int inf) {
shAssert(a != NULL);
shAssert(n > 0);
shAssert(inf == 0 || inf == 1);
int i, j;
THPIX magnitude = 0.0;
if (inf == 1) {
	for (i = 0; i < n; i++) {
		if (fabs(a[i]) > magnitude) magnitude = (THPIX) fabs(a[i]);
	}
} else {
	if (metric == NULL) {
		for (i = 0; i < n; i++) {
			magnitude += pow(a[i], 2.0);
		}
		magnitude = pow(magnitude, 0.5);
	} else {
		for (i = 0; i < n; i++) {
			THPIX a_i = a[i];
			THPIX *metric_i = metric[i];
			for (j = 0; j < n; j++) {
				magnitude += a_i * a[j] * metric_i[j];
			}
		}
		magnitude = pow(magnitude, 0.5);
	}
}
return(magnitude);
}

THPIX mag_thpix_mlefl_vector(THPIX *a, MLEFL **metric, int n, int inf) {
shAssert(a != NULL);
shAssert(n > 0);
shAssert(inf == 0 || inf == 1);
int i, j;
THPIX magnitude = 0.0;
if (inf == 1) {
	for (i = 0; i < n; i++) {
		if (fabs(a[i]) > magnitude) magnitude = (THPIX) fabs(a[i]);
	}
} else {
	if (metric == NULL) {
		for (i = 0; i < n; i++) {
			magnitude += pow(a[i], 2.0);
		}
		magnitude = pow(magnitude, 0.5);
	} else {
		for (i = 0; i < n; i++) {
			THPIX a_i = a[i];
			MLEFL *metric_i = metric[i];
			for (j = 0; j < n; j++) {
				magnitude += a_i * a[j] * ((THPIX) metric_i[j]);
			}
		}
		magnitude = pow(magnitude, 0.5);
	}
}
return(magnitude);
}



CONVERGENCE test_QN_dx_convergence(LMETHOD *lmethod, MLEFL *dx, MLEFL *x, MLEFL **metric, int n) {
char *name = "test_QN_dx_convergence";
shAssert(n > 0);
shAssert(dx != NULL);
shAssert(x != NULL);
shAssert(lmethod != NULL);

if (metric == NULL) {
	thError("%s: WARNING - null metric passed", name);
}
#if 0
THPIX mag_dx = mag_thpix_mlefl_vector(dx, metric, n, 0);
THPIX mag_x = mag_thpix_mlefl_vector(x, metric, n, 0);
#else
MLEFL mag_dx = mag_mlefl_vector(dx, metric, n, 0);
MLEFL mag_x = mag_mlefl_vector(x, metric, n, 0);
#endif
MLEFL e2 = (MLEFL) lmethod->e2;
int k = lmethod->k;
int step_mode = lmethod->step_mode;

CONVERGENCE convergence;
printf("%s: testing for h-convergence: || dx || = %G, || x || = %G, e2 = %G \n", name, (double) mag_dx, (double) mag_x, (double) e2);
if (mag_dx <= e2 * (mag_x + e2) && k >= NSTEP_MIN_REQUIRED && step_mode > NSTEP_QN_MIN_REQUIRED) {
	convergence = TOOSMALLSTEP;
} else if ((isnan(mag_x) || isinf(mag_x)) &&  !(isnan(mag_dx) || isinf(mag_dx))) {
	convergence = UNABLETOASSESSSTEP;
} else {
	convergence = CONTINUE;
}
return(convergence);
}

CONVERGENCE test_QN_Fprime_convergence(LMETHOD *lmethod, MLEFL *Fprime, int n) {
shAssert(lmethod != NULL);
shAssert(Fprime != NULL);
shAssert(n > 0);
MLEFL e1 = (MLEFL) lmethod->e1;
int k = lmethod->k;
CONVERGENCE convergence;
MLEFL mag_Fprime = mag_mlefl_vector(Fprime, NULL, n, 1);
if (mag_Fprime <= e1 && k >= NSTEP_MIN_REQUIRED) {
	convergence = TOOSMALLDERIV;
} else {
	convergence = CONTINUE;
}
return(convergence);
}


RET_CODE update_QN_delta(LMETHOD *lmethod, MLEFL delta, MLEFL rho, MLEFL mag_dx, MLEFL *delta_new) {
char *name = "update_QN_delta";
shAssert(lmethod != NULL);
if (delta <= 0.0) {
	thError("%s: ERROR - (delta = %g) found in (lmethod)", name, (float) delta);
	return(SH_GENERIC_ERROR);
}

shAssert(delta_new != NULL);
MLEFL delta_out = delta;
if (rho < 0.25) {
	delta_out = delta / 2.0;
} else if (rho > 0.75) {
	delta_out = MAX(delta, (MLEFL) (3.0 * mag_dx));
	if (delta_out > (MLEFL) QN_DELTA_MAX) delta_out = (MLEFL) QN_DELTA_MAX;
}
*delta_new = delta_out;
return(SH_SUCCESS);
}

RET_CODE initiate_QN_delta(LSTRUCT *lstruct, LMETHOD *lmethod, LWORK *lwork, LSECTOR sector) {
char *name = "initiate_QN_delta";
if (lwork == NULL || lmethod == NULL || lstruct == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
LMETHODTYPE type;
LMETHODSUBTYPE wtype;
status = thLmethodGetType(lmethod, &type, &wtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (method_type) from (lmethod)", name);
	return(status);
}
if (wtype != LEVENBERGMARQUARDT) {
	thError("%s: WARNING - (delta_qn) should be initiating before starting (quasi-newtonian) steps", name);
}

MLEFL *x = NULL, *xN = NULL, *dx = NULL;
int n = -1;
status = ExtractXxNdX(lstruct, sector, NULL, NULL, &x, &xN, &dx, &n);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract (x, xN, dx, n) from (lstruct) for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(status);
}

MLEFL e2 = lmethod->e2;
MLEFL mag_x = 0.0, mag_xN = 0.0, mag_dx = 0.0;
MLEFL **Bk_prev = NULL, **BBk_prev = NULL;

status = thLworkGetQN(lwork, 
	&Bk_prev, NULL, &BBk_prev, NULL, 
	NULL, NULL, 
	NULL, NULL, NULL, NULL, 
	NULL, &n, sector);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (Bk_prev) from (lwork)", name);
	return(status);
}

mag_dx = mag_mlefl_vector(dx, BBk_prev, n, 0);
mag_x = mag_mlefl_vector(x, BBk_prev, n, 0);
mag_xN = mag_mlefl_vector(xN, BBk_prev, n, 0);

mag_x = MAX(mag_x, mag_xN);

MLEFL delta_qn = MAX((MLEFL) (1.5 * e2 * (mag_x + e2)), (MLEFL) (0.2 * mag_dx));
if (delta_qn <= 0.0) {
	thError("%s: WARNING - found (delta_qn = %g) upon initiation", name, (float) delta_qn);
} else if (delta_qn > (MLEFL) QN_DELTA_MAX) {
	delta_qn = (MLEFL) QN_DELTA_MAX;
}

status = thLmethodPutDeltaQn(lmethod, delta_qn);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not put (delta_qn) in (lmethod)", name);
	return(status);
}

return(SH_SUCCESS);
}


void output_mlefl_sqr_matrix(MLEFL **A, int n) {
shAssert(n > 0)
shAssert(A != NULL);
int p = 51, step = 5;
int m = MIN(p, n);
int i, j;

for (i = 0; i < m; i = i + step) {
	MLEFL *A_i = A[i];
	shAssert(A_i != NULL);
	for (j = 0; j < m; j = j + step){
		#if 0
		printf("[%2d][%2d] = %20.10g, ", i, j, (float) A_i[j]);
		#else	
		printf("%10.5f, ", (float) A_i[j]);
		#endif
	}
	printf("\n");
}

return;
}

CONVERGENCE test_GD_Fprime_convergence(LMETHOD *lmethod, LWMASK *lwmask, MLEFL *Fprime, int n) {
char *name = "test_GD_Fprime_convergence";
shAssert(lmethod != NULL);
shAssert(Fprime != NULL);
shAssert(n > 0);
MLEFL e1 = (MLEFL) lmethod->e1;
int k = lmethod->k;
CONVERGENCE convergence;
MLEFL mag_Fprime = mag_mlefl_vector(Fprime, NULL, n, 1);
printf("%s: comparing |F'| = %g to |F'_thresh| = %g \n", name, (float) mag_Fprime, (float) e1);

if (mag_Fprime <= e1 && k >= NSTEP_MIN_REQUIRED) {
	convergence = TOOSMALLDERIV;
	if (lwmask != NULL) {
		int index_active = 0, mini_index_active = 0;
		RET_CODE status;
		status = thLwmaskGetIndexActive(lwmask, &index_active, &mini_index_active);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (index_active) from (lwmask)", name);
			return(UNKNOWN_CONVERGENCE);
		}
		if (index_active != 0 || mini_index_active != 0) convergence = CONTINUE;
	}
} else {
	convergence = CONTINUE;
}
return(convergence);
}

RET_CODE update_PGD_eta_max(LMETHOD *lmethod, MLEFL eta, MLEFL rho, MLEFL mag_dx, MLEFL cost, MLEFL costN, MLEFL *eta_new) {
char *name = "update_GD_eta_max";
shAssert(lmethod != NULL);
if (eta <= 0.0) {
	thError("%s: ERROR - (eta = %g) found in (lmethod)", name, (float) eta);
	return(SH_GENERIC_ERROR);
}

#if ETA_FIXED_PGD
	shAssert(eta_new != NULL);
	MLEFL eta_out = eta;
#else
	#if ETA_UPDATE_CLASSICAL 
	shAssert(eta_new != NULL);
	MLEFL eta_max = lmethod->eta_max;
	MLEFL eta_out = eta;
	if (costN >= cost) {
		eta_out = eta_max / 2.0;
	} else {
		eta_out = MAX(1.5 * eta, eta_max);
	}
	if (eta_out > (MLEFL) LM_ETA_GD_MAX_LARGE) {
		eta_out = (MLEFL) LM_ETA_GD_MAX_LARGE;
	} else if (eta_out < (MLEFL) LM_ETA_GD_MAX_SMALL) {
		eta_out = (MLEFL) LM_ETA_GD_MAX_SMALL;
	}
	#else
	shAssert(eta_new != NULL);
	MLEFL eta_max = lmethod->eta_max;
	MLEFL eta_out = eta;
	if ((rho < 0.25) || (costN >= cost)) {
		eta_out = eta_max / 2.0;
	} else if (rho > 0.75) {
		eta_out = MAX(3.0 * eta, eta_max);
	}
	if (eta_out > (MLEFL) LM_ETA_GD_MAX_LARGE) {
		eta_out = (MLEFL) LM_ETA_GD_MAX_LARGE;
	} else if (eta_out < (MLEFL) LM_ETA_GD_MAX_SMALL) {
		eta_out = (MLEFL) LM_ETA_GD_MAX_SMALL;
	}
	#endif
#endif
*eta_new = eta_out;
return(SH_SUCCESS);
}


RET_CODE update_GD_eta_max(LMETHOD *lmethod, MLEFL eta, MLEFL rho, MLEFL mag_dx, MLEFL cost, MLEFL costN, MLEFL *eta_new) {
char *name = "update_GD_eta_max";
shAssert(lmethod != NULL);
if (eta <= 0.0) {
	thError("%s: ERROR - (eta = %g) found in (lmethod)", name, (float) eta);
	return(SH_GENERIC_ERROR);
}
#if ETA_UPDATE_CLASSICAL
shAssert(eta_new != NULL);
MLEFL eta_max = lmethod->eta_max;
MLEFL eta_out = eta;
if (costN >= cost) {
	eta_out = eta_max / 2.0;
} else {
	eta_out = MAX(1.5 * eta, eta_max);
}
if (eta_out > (MLEFL) LM_ETA_GD_MAX_LARGE) {
	eta_out = (MLEFL) LM_ETA_GD_MAX_LARGE;
} else if (eta_out < (MLEFL) LM_ETA_GD_MAX_SMALL) {
	eta_out = (MLEFL) LM_ETA_GD_MAX_SMALL;
}
#else
shAssert(eta_new != NULL);
MLEFL eta_max = lmethod->eta_max;
MLEFL eta_out = eta;
if ((rho < 0.25) || (costN >= cost)) {
	eta_out = eta_max / 2.0;
} else if (rho > 0.75) {
	eta_out = MAX(3.0 * eta, eta_max);
}
if (eta_out > (MLEFL) LM_ETA_GD_MAX_LARGE) {
	eta_out = (MLEFL) LM_ETA_GD_MAX_LARGE;
} else if (eta_out < (MLEFL) LM_ETA_GD_MAX_SMALL) {
	eta_out = (MLEFL) LM_ETA_GD_MAX_SMALL;
}
#endif
*eta_new = eta_out;
return(SH_SUCCESS);
}

CONVERGENCE test_GD_dx_convergence(LMETHOD *lmethod, LWMASK *lwmask, MLEFL *dx, MLEFL *x, MLEFL **metric, int n) {
char *name = "test_GD_dx_convergence";
shAssert(n > 0);
shAssert(dx != NULL);
shAssert(x != NULL);
shAssert(lmethod != NULL);

if (metric == NULL) {
	thError("%s: WARNING - null metric passed", name);
}

if (lwmask != NULL) {
	RET_CODE status;
	int index_active = 0, mini_index_active = 0;
	status = thLwmaskGetIndexActive(lwmask, &index_active, &mini_index_active);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (index_active) from (lwmask)", name);
		return(UNKNOWN_CONVERGENCE);
	}
	if (index_active != 0 || mini_index_active != 0) return(CONTINUE);
}
#if 0
THPIX mag_dx = mag_thpix_mlefl_vector(dx, metric, n, 0);
THPIX mag_x = mag_thpix_mlefl_vector(x, metric, n, 0);
#else
MLEFL mag_dx = mag_mlefl_vector(dx, metric, n, 0);
MLEFL mag_x = mag_mlefl_vector(x, metric, n, 0);
#endif
MLEFL e2 = (MLEFL) lmethod->e2;
int k = lmethod->k;

CONVERGENCE convergence;
printf("%s: testing for h-convergence: || dx || = %G, || x || = %G, e2 = %G , k = %d \n", name, (double) mag_dx, (double) mag_x, (double) e2, k);
if (mag_dx <= e2 * (mag_x + e2) && k >= NSTEP_MIN_REQUIRED) {
	convergence = TOOSMALLSTEP;
} else if ((isnan(mag_x) || isinf(mag_x)) &&  !(isnan(mag_dx) || isinf(mag_dx))) {
	convergence = UNABLETOASSESSSTEP;
} else {
	convergence = CONTINUE;
}
return(convergence);
}

RET_CODE randomize_amplitude(LSTRUCT *lstruct) {
char *name = "randomize_amplitude";
if (lstruct == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
LWORK *lwork = NULL;
status = thLstructGetLwork(lstruct, &lwork);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwork) from (lstruct)", name);
	return(status);
}

MLEFL *x = NULL, *xN = NULL;
int n = 0, nN = 0;
status = thLworkGetX(lwork, &x, &n, ASECTOR);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (x-array) for 'ASECTOR' from (lwork)", name);
	return(status);
}
status = thLworkGetXN(lwork, &xN, &nN, ASECTOR);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (xN-array) for 'ASECTOR' from (lwork)", name);
	return(status);
}
if (n != nN) {
	thError("%s: ERROR - found (x- and xN-array) of different sizes (%d, %d)", name, n, nN);
	return(SH_GENERIC_ERROR);
}
int i;
for (i = 0; i < n; i++) {
	x[i] *= (1.0 + ((MLEFL) phGaussdev()) * (MLEFL) AMPLITUDE_CV);
	xN[i] *= (1.0 + ((MLEFL) phGaussdev()) * (MLEFL) AMPLITUDE_CV);
}

return(SH_SUCCESS);
}


RET_CODE thMathMultiplySquareMatColumn(MLEFL **A, MLEFL *C, MLEFL *wC, int n) {
char *name = "thMathMultiplySquareMatColumn";
if (A == NULL || C == NULL || wC == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (n <= 0) {
	thError("%s: ERROR - found (n = %d)", name, n);
	return(SH_GENERIC_ERROR);
}
if (C == wC) {
	thError("%s: ERROR - workspace should be different", name);
	return(SH_GENERIC_ERROR);
}
int i, j;
for (i = 0; i < n; i++) {
	MLEFL x = 0.0;
	MLEFL *A_i = A[i];
	for (j = 0; j < n; j++) {
		x += A_i[j] * C[j];
	}
	wC[i] = x;
}
for (i = 0; i < n; i++) C[i] = wC[i];
memset(wC, '\0', n * sizeof(MLEFL));
return(SH_SUCCESS);
}


RET_CODE thMathMultiplyInverseSquareMatGeneral(MLEFL **A, MLEFL **B, MLEFL **wA, MLEFL **wB, MAT *wQ1, MAT *wQ2, VEC *wx, int n, int m) {

/* A[n, n], B[n, m], wA[n, n], wB[n, m], wC[m] */
/* wQ1[n, n], wQ2[n, n], wx[n] */
char *name = "thMathMultiplyInverseSquareMatGeneral";
if (A == NULL || B == NULL || wA == NULL || wB == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (n <= 0 || m <= 0) {
	thError("%s: ERROR - found (n, m) = (%d,  %d) <= 0", name, n, m);
	return(SH_GENERIC_ERROR);
}
if (A == wA || B == wA || A == wB || B == wB || wA == wB) {
	thError("%s: ERROR - matrices should be distinct", name);
	return(SH_GENERIC_ERROR);
}
if (wQ1 == NULL || wQ2 == NULL || wx == NULL) {
	thError("%s: ERROR - empty working space", name);
	return(SH_GENERIC_ERROR);
	}
if (wQ1->m != n || wQ2->m != n || wx->dim != n) {
	thError("%s: ERROR - mistamcthing size between work space and the input matrix", name);
	return(SH_GENERIC_ERROR);
}
int i;
for (i = 0; i < n; i++) {
	memcpy(wA[i], A[i], n * sizeof(MLEFL));
}
#if VERY_DEEP_DEBUG_MLE
dbg2DArray(wA, n, n, name, "A = ");
#endif

RET_CODE status;
status = LMatrixInvertSVD(wA, wQ1, wQ2, wx, n); 
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not invert matrix (A)", name);
	return(status);
}
status = LCopyMatOnto(wA, wQ1);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not copy (MAT) into array", name);
	return(status);
}
#if VERY_DEEP_DEBUG_MLE
dbg2DArray(wA, n, n, name, "inv(A) = ");
dbg2DArray(B, n, m, name, "B = ");
#endif
int j, k;
for (i = 0; i < n; i++) {
	MLEFL *wB_i = wB[i];
	MLEFL *wA_i = wA[i];
	for (j = 0; j < m; j++) {	
		MLEFL x = (MLEFL) 0.0;
		for (k = 0; k < n; k++) {
			x += wA_i[k] * B[k][j];
		}
		wB_i[j] = x;
	}
}
#if VERY_DEEP_DEBUG_MLE
dbg2DArray(wB, n, m, name, "inv(A) x B = ");
#endif

return(SH_SUCCESS);
}




RET_CODE thMathMultiplyInverseSquareMat(MLEFL **A, MLEFL **B, MLEFL **wA, MLEFL **wB, MAT *wQ1, MAT *wQ2, VEC *wx, int n) {
char *name = "thMathMultiplyInverseSquareMat";
if (A == NULL || B == NULL || wA == NULL || wB == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (n <= 0) {
	thError("%s: ERROR - found (n = %d)", name, n);
	return(SH_GENERIC_ERROR);
}
if (A == B || A == wA || B == wA) {
	thError("%s: ERROR - matrices should be distinct", name);
	return(SH_GENERIC_ERROR);
}
if (wQ1 == NULL || wQ2 == NULL || wx == NULL) {
	thError("%s: ERROR - empty working space", name);
	return(SH_GENERIC_ERROR);
	}
if (wQ1->m != n || wQ2->m != n || wx->dim != n) {
	thError("%s: ERROR - mistamcthing size between work space and the input matrix", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = thMathMultiplyInverseSquareMatGeneral(A, B, wA, wB, wQ1, wQ2, wx, n, n);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not conduct general multiplication", name);
	return(status);
}
return(status);

#if 0
int i;
for (i = 0; i < n; i++) {
	memcpy(wA[i], A[i], n * sizeof(MLEFL));
}
status = LMatrixInvertSVD(wA, wQ1, wQ2, wx, n); 
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not invert matrix (A)", name);
	return(status);
}
status = LCopyMatOnto(wA, wQ1);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not copy (MAT) into array", name);
	return(status);
}
for (i = 0; i < n; i++) {
	status = thMathMultiplySquareMatColumn(wA, B[i], wC, n);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - coud not multiply with the column (i = %d)", name, i);
		return(status);
	}
}

return(SH_SUCCESS);
#endif

}

RET_CODE thMathMultiplyInverseSquareMatColumn(MLEFL **A, MLEFL *C, MLEFL **wA, MLEFL *wC, MAT *wQ1, MAT *wQ2, VEC *wx, int n) {
char *name = "thMathMultiplyInverseSquareMatColumn";
if (A == NULL || C == NULL || wA == NULL || wC == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (n <= 0) {
	thError("%s: ERROR - found (n = %d)", name, n);
	return(SH_GENERIC_ERROR);
}
if (A == wA || C == wC) {
	thError("%s: ERROR - matrices should be distinct", name);
	return(SH_GENERIC_ERROR);
}
if (wQ1 == NULL || wQ2 == NULL || wx == NULL) {
	thError("%s: ERROR - empty working space", name);
	return(SH_GENERIC_ERROR);
	}
if (wQ1->m != n || wQ2->m != n || wx->dim != n) {
	thError("%s: ERROR - mistamcthing size between work space and the input matrix", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;

#if 0
MLEFL **CC;
CC = &C;

status = thMathMultiplyInverseSquareMatGeneral(A, CC, wA, &wC, wQ1, wQ2, wx, n, 1);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not conduct general multiplication", name);
	return(status);
}
return(status);

#else
int i;
for (i = 0; i < n; i++) {
	memcpy(wA[i], A[i], n * sizeof(MLEFL));
}
status = LMatrixInvertSVD(wA, wQ1, wQ2, wx, n); 
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not invert matrix (A)", name);
	return(status);
}
status = LCopyMatOnto(wA, wQ1);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not copy (MAT) into array", name);
	return(status);
}
status = thMathMultiplySquareMatColumn(wA, C, wC, n);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - coud not multiply", name);
	return(status);
}
#endif

return(SH_SUCCESS);
}

RET_CODE random_ball(MLEFL *x, int n, MLEFL r) {
char *name = "random_ball";
if (r <= (MLEFL) 0.0) {
	thError("%s: ERROR - unacceptable ball size (r = %g, n = %d)", name, (float) r, n);
	return(SH_GENERIC_ERROR);
} else if (n <= 0) {
	thError("%s: ERROR - unacceptable dimension for ball (n = %d)", name, n);
	return(SH_GENERIC_ERROR);
} else if (x == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}

int i;
x[0] = r * (MLEFL) (2.0 * phRandomUniformdev() - 1.0);
for (i = 0; i < n; i++) {
	MLEFL theta = (MLEFL) 2.0 * (MLEFL) PI * (MLEFL) phRandomUniformdev();	
	if ((i + 1) < n) {
		MLEFL c = cos(theta);
		MLEFL s = sin(theta);
		x[i+1] = x[i] * s;
		x[i] *= c;
	}
}

return(SH_SUCCESS);
}


