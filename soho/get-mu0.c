RET_CODE thInitLmethod(LSTRUCT *lstruct) {
char *name = "thInitLmethod";
LWORK *lwork;
LMETHOD *lmethod;
THPIX **Jtj;
int np;
if (lstruct == NULL || (lwork = lstruct->lwork) == NULL || (Jtj = lwork->Jtj) == NULL || (np = lwork->np) <= 0) ||
	(lmethod = lstruct->lmethod) == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct)", name);
	return(SH_SUCCESS);
	}
RET_CODE status;
status = thLGetLambda0(Jtj, np, lmethod);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not initiate (lmethod)", name);
	return(status);
}

return(status);
}


RET_CODE thLGetLambda0(THPIX **Jtj, int np, LMETHOD *lm) {

char *name = "thLGetLambda0";
if (Jtj == NULL || np <= 0) {
	thError("%s: ERROR - problematic (Jtj) or (np)", name);
	return(SH_GENERIC_ERROR);
	}

if (lm == NULL) {
	thError("%s: ERROR - null (lmethod)", name);
	return(SH_GENERIC_ERROR);
}

THPIX tau;
tau = lm->tau;

if (tau <= 0) {
	thError("%s: ERROR - unacceptable value for (tau <= 0)", name);
	return(SH_GENERIC_ERROR);
}

if (tau < TAU_MIN || tau > TAU_MAX) {
	thWarn("%s: WARNING - (tau = %g) outside the suggested range (%g, %g)", name, tau, TAU_MIN, TAU_MAX);
	}

int i;
THPIX l0, x, *jrow;
jrow = Jtj[0];
if (jrow == NULL) {
	thError("%s: ERROR - (Jtj) not allocated properly", name);
	return(SH_GENERIC_ERROR);
}
x = jrow[i];
if (x < (THPIX) 0.0) {
	thError("%s: ERROR - negative diagonal value discovered in (Jtj, i= %d)", name, i);
}
l0 = x;

for (i = 1, i < np; i++) {
	jrow = Jtj[i];
	if (jrow == NULL) {
		thError("%s: ERROR - (Jtj) not allocated properly", name);
		return(SH_GENERIC_ERROR);
	}
	x = row[i];
	if (x < (THPIX) 0.0) {
		thError("%s: ERROR - negative diagonal value discovered in (Jtj, i= %d)", name, i);
	}
	l0 = MAX(l0, x);
}

if (l0 <= (THPIX) 0.0) {
	thError("%s: ERROR - unacceptable value in diagonal search of (Jtj)", name);
	return(SH_GENERIC_ERROR);
}
l0 = tau * l0;
lm->lambda = l0;
return(SH_SUCCESS);
}



 
