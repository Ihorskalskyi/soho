#ifndef THCONSTS_H
#define THCONSTS_H

#include <math.h>

/* long double definition as a data type */
typedef long double FL96; /* pragma SCHEMA */
typedef struct ldouble {
	long double x;
} LDOUBLE; /* pragma SCHEMA */

#define THCLOCK 0 

/* PHOTO setting */
#define SCAN_OVERLAP 128 /* this is the overlap between two adjacent FPC fields */

#if 1 /* define float as the working type */

#define FL32 FL32 
#define THDBLE FL64
#define TYPE_FL32 TYPE_FL32  /* a type definition for later use */
#define rows_thpix rows_fl32 /* the rows in a region with type th_pix */
#define thabs fabs
#define STR_FL32 "float"
/* defining NaN */
#define THNAN (sqrt((FL32) -1.0))
#define FL32_MIN 1.0E-37
#define FL32_MAX 1.0E+37

#else  /* define ouble as the working type -- there is no FL64 type*/

#define FL32 FL64 
#define THDBLE FL64
#define TYPE_FL32 TYPE_FL64  /* a type definition for later use */
#define rows_thpix rows_fl64 /* the rows in a region with type th_pix */
#define thabs dabs
#define STR_FL32 "double"
/* defining NaN */
#define THNAN  (sqrt((FL64) -1.0))
#define FL32_MIN FL64_MIN
#define FL32_MAX FL64_MAX

#endif

/* odd number preferred as this is used in profile maker and sometimes referred to in LPROFILE handlers for 1D profiles */
#define MAXNROW 2047 /*  2048 */ /* 4096 */
#define MAXNCOL 2047 /* 2048 */ /* 4096 */
#define MINNROW 3
#define MINNCOL 3
#define DEFAULT_NROW 1489 /* SDSS fpC file size by default */ 
#define DEFAULT_NCOL 2048 /* SDSS fpC file size by default */
#define UNKNOWN_NROW -1
#define UNKNOWN_NCOL -1

/* fitting */

/* 1. mapping */

#define MAP_HAS_SCHEMA_INFO 1 /* performs the schema transformation once within the map, and not during each fitting step */
#define BAD_INDEX -1  /* bad value for index used in (accnmpar) in (mapmachine) */
#define OLD_MAP 0 /* old map contains a crude management of MM and MDmm matrices
			the improved format uses a new indexing system which saves a lot of space
			for 150 objects with 2 fit parameters for each:
			the old map required 7.8 GB of space for storage for MM matrices
			the new	map requires less than 0.5 MB of space for the same matrices (0.005%)
			*/

/* IO-lib */
#define IO_MODE int
#define DUMP_MODE 0
#define PHOTO_INPUT_MODE 1

#define CHAIN_BLOCK_GENERATION 0
#define IO_COMPRESSED 0   /* changed to no-compression on march 16, 2015 for temporary reasons */
#define THIOTYPE FL32
#define THIOPIXTYPE TYPE_FL32
#define rows_io rows_fl32
#define MAXIONROW 4096
#define MAXIONCOL 4096
#define IO_LOAD_PHCALIB 0 /* phCalib files were previously available in Princeton dispatch of DSS but no longer available after the FAHL crash */

#define SUPERMASK_PIX U16
#define TYPE_SUPERMASK_PIX TYPE_U16
#define rows_sm rows_u16 /* dara rows in a region with pixel->type supermask_pix */

#define PRINT_IO_SCHEMA_TRANS 0 /* outputs some important schema trans's that have created problem in the past when declaring them */
#define USE_FNAL_OBJC_IO 1 /* used after FAHL crash because a couple of records are discrepant 
	- should be set to 0 only during MLE runs on locally produced data 
	- should be set to 1 when producing simulations from downloaded data */
/* working OBJC_IO structure */
#if USE_FNAL_OBJC_IO
	#define WOBJC_IO FNAL_OBJC_IO
	#define thWObjcIoNew thFnal_objc_ioNew
	#define thWObjcIoDel thFnal_objc_ioDel
	#define DEFAULT_GC_SOURCE SWEEP_FILE_AND_WRITE
	#define READ_CALIBRATED_FRAME 0 /* use frame- files */
#else
	#define WOBJC_IO OBJC_IO
	#define thWObjcIoNew phObjcIoNew
	#define thWObjcIoDel phObjcIoDel
	#define DEFAULT_GC_SOURCE GC_FILE
	#define READ_CALIBRATED_FRAME 0 /* use fpC- images */
#endif

/* fit flags*/
#define MFIT_NULL   (int) 0x000000
#define MFIT_RE     (int) 0x000001
#define MFIT_INDEX  (int) 0x000002
#define MFIT_E	    (int) 0x000004
#define MFIT_PHI    (int) 0x000008
#define MFIT_CENTER (int) 0x000010
#define MFIT_LOCK_CENTER (int) 0x000020
#define MFIT_LOCK_RE     (int) 0x000040
#define MFIT_RB     (int) 0x000080 /* for core sersic model */
#define MFIT_SHAPE (MFIT_RE|MFIT_RB|MFIT_E|MFIT_PHI|MFIT_INDEX)
#define MFIT_SIZE (MFIT_RE|MFIT_RB)

#define MFIT_RE1     (int) 0x000100 
#define MFIT_INDEX1  (int) 0x000200
#define MFIT_E1	     (int) 0x000400
#define MFIT_PHI1    (int) 0x000800
#define MFIT_CENTER1   (int) 0x001000
#define MFIT_LOCK_RE1  (int) 0x002000
#define MFIT_RB1       (int) 0x004000 /* for core sersic model */
#define MFIT_SHAPE1 (MFIT_RE1|MFIT_RB1|MFIT_E1|MFIT_PHI1|MFIT_INDEX1)
#define MFIT_SIZE1 (MFIT_RE1|MFIT_RB1)

#define MFIT_RE2       (int) 0x008000 
#define MFIT_INDEX2    (int) 0x010000
#define MFIT_E2	       (int) 0x020000
#define MFIT_PHI2      (int) 0x040000
#define MFIT_CENTER2   (int) 0x080000
#define MFIT_LOCK_RE2  (int) 0x100000
#define MFIT_RB2       (int) 0x200000 /* for core sersic model */
#define MFIT_SHAPE2 (MFIT_RE2|MFIT_RB2|MFIT_E2|MFIT_PHI2|MFIT_INDEX2)
#define MFIT_SIZE2 (MFIT_RE2|MFIT_RB2)

#define DEBUG_NFIT 10000 /* used in thMap.c to restrain the number of objects fit to - should be set to a larged number if the dispatched binary */
 
/* dumping (output) */
#define DUMP_OVERWRITE_FLAG 1 /* not to change - flag used by photo FITS handler */
#define DUMP_APPEND_FLAG 2    /* not to change - flag used by photo FITS handler */
#define DEFAULT_DUMP_WRITE_FLAG DUMP_OVERWRITE_FLAG

#define DUMP_LOG 0 /* should a log be dumped */
#define DUMP_SKY 0 /* should sky be dumped */
#define DUMP_OBJC 1 /* should object information be dumped ( see the three following flags ) */
#define DUMP_SDSSOBJC 1 /* should sdss obj_io be dumped as part of objc dump */
#define DUMP_SDSSOBJC2 1 /* should sdss calib_phobjc_io be dumped as part of objc dump */
#define DUMP_INITOBJC 1 /* should init parameter for the fit be dumped */
#define DUMP_FITOBJC 1  /* should fit outcomes be dumped */
#define DUMP_AMPOBJC 0 /* the objects that are dumped, should they be amplitude fitted or parameter fitted only */
#define DUMP_LM_INTO_GPOBJC 0 /* should the initial and final LMETHOD be dumped at gpObjc */
#define DUMP_PROF_INTO_GPOBJC 0 /* should the 1D profiles be dumped in gpObjc files */
#define ALL_RECORDS -1 /* not to change - used in log dumper */
#define DUMP_SKY_BIN 2  /* size of bin pixel for sky output */
#define DUMP_LM 1      /* should LMETHOD chain be output onto a separate file */
#define DUMP_PROF 1 /* should LPROF chains be output onto a separate file */
#define DUMP_PROF_INIT 0 /* should the initial 1D profiles for MLE objects be dumped */
#define DUMP_PROF_FIT  0 /* should the final 1D profile for the fit result in MLE be dumped */
#define DUMP_PSF_REPORT 0 /* extensive PSF report for each random position with radial profile and the image of the PSF included for KL, wing, and glued PSF */
#define DUMP_PSF_REPORT_RADIAL_PROFILE 0 /* should the radial PSF profile in PSF_REPORT be dumped? */
#define DUMP_PSF_REPORT_REGION 0 /* should the PSF image available in PSF_REPORT be dumped */
#define DUMP_PSF_STAT 0   /* descriptibe statistics of the simulation from the extensive report, does not include the images or the radial profile */
#define DUMP_LRZ 1 /* should the residual image and their z-scores be dumped */
#define BAD_Z_SCORE VALUE_IS_BAD /* use this value for indicating the z-scores of the pixels that are considered bad */

#define CHISQ_DIFF_SCALE 1000 /* used in comparison within Dump package to scale up chi-squared difference to sort chain of LMDUMP's */
#define COMPARE_FRAMEIO_BAD_VALUE -999 /* used in comparison within Dump package: bad value returned when finding the chi-squared value from LMDUMP fails */

/* io-stuff not in io-lib */
#define EMPTY_LINE_LENGTH 10 /* the length of the separation line */
#define SET_DEFAULT_OUTDIR 1 /* sets the default outdir to "." in thProcTypes.h */

/* package (standard error) */
#define DO_STD_ERRORS 0 /* this also affects whether the std. errors of the mdoel variables are printed to file */
#define IO_STD_ERRORS 1

/* package (capture) */
#define DO_CAPTURE 0 /* turns on capture - extensive debugging, movie and statistical tracing */
#define CAPTURE_INT 1
#define CAPTURE_GRAPHICAL 1
#define CAPTURE_USE_LUT 1
#define CAPTURE_BIN 2
#define UNCHANGED_NROWCOL -1 /* for binning */
#define MY_CAPTURE_MODE (PCAPTURE_RES | GCAPTURE_RES | PCAPTURE_PAR)
#define CAPTURE_MEMORY ((MEMDBLE) 1 * GIGABYTE)
#define CAPTURE_NLUT 100
#define CAPTURE_LUT_RATIO 1.5 /* correction for the lut width */
#define CAPTURE_BMAX 20.0 /* the factor to multiply captures images with so that truncating doesn't render the images useless */
/* available options for the capture mode are: 
GCAPTURE_MODEL
GCAPTURE_OBJC
GCAPTURE_SKY
GCAPTURE_RES
PCAPTURE_MODEL
PCAPTURE_OBJC
PCAPTURE_SKY 
PCAPTURE_RES
PCAPTURE_PAR  
*/ 
/* precision used to calculate the brackets < a | W | b > 
   if set to float it creates 1%-5% errors in the results 
   users are advised against long double, the test run was never completed possibly due to compiler issues */
#define BRACKETFL double
#define DO_KAHAN_SUM 1 /* should be set to 1 to avoid numerical instability - this overwrites any optimization effort to use register variables */
#define KAHAN_VERBOSE 0 /* prints out the kahan steps in Kahan functions ending with Verbose, not effective if the function name does not end with Verbose */
#define USE_KAHAN_PACKAGE 1 /* should the Kahan package be used instead of inline Kahan-style addition */
#define USE_KAHAN_PACKAGE2 1 /* using extended Kahan package that covers dot products as well, mostly addresses thBracket functions */
#define USE_KAHAN_PACKAGE3 0 /* using extended Kahan package that covers dot products as well, mostly addresses Chi-squared calculation in thMle packages */
#define KAHANFL long double
#define OPTIMIZE_BRACKET 1
#define PROFILEFL FL32
#if 1
#define MLEFL long double
#define MLEDBLE long double
#define CHISQFL long double
#else 
#define MLEFL double /* previously set to FL32 - decided to make it equal to BRACKETFL to minimize rounding off errors during transformations */
#define MLEDBLE double
#define CHISQFL double
#endif

#define MLEFLNAN (sqrt((MLEFL) -1.0))
#define MLEFL_MIN 1.0E-37
#define MLEFL_MAX 1.0E+37
#define MLEDBLENAN (sqrt((MLEDBLE) -1.0))
#define CHISQFLNAN (sqrt((CHISQFL) -1.0))
#define CHISQFL_EPSILON ((CHISQFL) 1.0E-9)
#define CHISQ_VERY_LARGE_VALUE ((CHISQFL) 1.0E10)
#define CHISQ_IS_BAD ((CHISQFL) VALUE_IS_BAD)
#define MLE_CATCH_NAN 1 /* perform checks on whether the results are NAN */
#define MLE_DO_JTJ_APSECTOR 1 /* perform calculation of LM J matrix for amplitude and amplitude parameter sector - needed when calculating standard deviation of amplitude parameters */
#define MLE_LDATA_WEIGHT_DEEP_COPY 1 /* constructs a separate copy of weight inside LDATA structure */
#define MLE_SDSS_SN0 1 /* total signal to noise assumed for LRG cores during weight reduction */
#define STEPSHARPNESS 5.0 /* shaprness of the step function */
#define MLE_USE_STEP_FUNCTION  1 
#define MLE_APPLY_CONSTRAINTS 0 /* this enables the code to apply constraints on parameter shape variables  - also used by algorithm package - I suggest that you don't run demand MLE with this*/
#define MLE_CONDUCT_HIGHER_ORDER 0 /* this enables higher order fitting */
#define MLE_HIGHER_ORDER_ALPHA 0.75 /* default: 0.75 based on the journal reference - used for comparing second order correction dp2 to first order correction dp */
/* calibration */
#define PHPROPS_NEW_COPY_OBJC_IO 0
#define PHPROPS_NEW_COPY_CALIB_WOBJC_IO 1
#define PHPROPS_NEW_COPY_CALIB_PHOBJC_IO 1
#define PHPROPS_NEW_COPY_CALIB_FRAME 0
#define PHPROPS_NEW_COPY 0

#define EXPTIME 53.907456
/* the following is the accuracy level of profile calculation with quadrature
 * */

#define PROFILE_INIT_QUAD 16 /* changing this value to anything smaller affects the number of calls to QuadOnePix only minimally */

#define QUAD_TOLERANCE 1.00E-4 /* 1.27E-4 is a good break even threshold to study the behavior of the profile for gaussian profiles */
#define PROFILE_INIT_RADIUS 3
#define PROFILE_RATIO 3.0
#define PROFILE_TAIL 1.0
#define N_ROWC_NAMES 10
#define N_COLC_NAMES 10

/* the following are needed by thMask package and the modules that use them */
#define DEFAULT_FITREGBIT THGOODBIT /* these are  the pixels that are summed over by default */

/* the following is borrowed from thParTypes.h */

#ifndef THPARTYPES_H
#define MX_HDR_LINE_LEN 1000
#define MX_STRING_LEN 1000
#define MX_STRING_ARR 100 /* this is used in object classifier and shows the 
			     maximum of number of object types definable
			     and also the maximum number of conditions that 
			     can be set to define a type
			  */
#endif

#define PAR_NHDRCONFIG 144
#define PAR_NHDRECALIB 50

#define UNDO_NROW -1
#define UNDO_NCOL -1
#define UNDO_MROW -1
#define UNDO_MCOL -1
#define BAD_POS -1
#define SKY_N1 3  /* number of knots for B-spline along row */
#define SKY_N2 3  /* maximum degree of Tcheb poly is SKY_N2 - 1 */
#define SKYBSIZE 50
#define SKYCOEFFS_SOFT 0
#define SKY_FULL_REGION 0
#define DEFINE_FLAT_MSKY 1 /* debug mode only - defines Sky basis functions as flat (allowing for dark noise jump across amplifiers if the field has two amplifiers */
#define MAX_MSKY1 10 /* maximum number of basis functions in row direction for Sky */
#define MAX_MSKY2 10 /* maximum number of basis functions in col direction for Sky */

#define NXNODE_THBINREGION 256 /* size of sky and interporlation nodes in sky image supplied in frame- files - see THBINREGION */
#define NYNODE_THBINREGION 192
#define NX_THBINREGION 2048
#define NY_THBINREGION 1489
#define BINNODE_THBINREGION 8 /* bin size for sky in frame files */
#define BITSHIFT_THBINREGION 5 /* inspired by the interior of PHOTO */

#define RAWIMAGE_MAXROW 1360
#define PHOTO_OVERSCAN 128

/* the following controls the IO status of reconstructed sky when
   dealing with SKYMODEL and SKYMODELIO
   if set to the code is able to read and write REGIONs within
   SKYMODEL and SKYMODELIO
*/

#define SKYMODEL_IO_REGION 0

#define WMASK_NACTIVE 23 /* the maximum number of active masks that can be stored in LWMASK structure. active masks are particularly needed for Stochastic Gradient Descent */
/* how many pixels extra to the photo mask should be taken out of the 
   sky calculations */
#define ROOT_INDEX -1 /* reserved index for the main root mask and main root weight matrix */
#define THMASKADDPIX 0
#define THMASKCORRUPTCOL 0
#define THMASK_NULL 0 /* passes null for THMASK - for debug purposes only */ 
#define THMASK_BAD_PIXELS 1 /* the default value is 1  - it enables the code to mask bad pixels in the image during dot products - use 0 only in debug mode - this keyword is only invoked if THMASK_NULL is not set */
#define THMASK_BAD_INTERP 1 /* default =  1 */
#define THMASK_BAD_SATUR 1 /* default = 1 */
#define THMASK_BAD_SUBTRACTED 0 /* default = 0, masking these pixels create singular covariance matrices */
#define THMASK_BAD_GHOST 1 /* default = 1*/
#define THMASK_BAD_CR 1 /* default = 1 */
#define THMASK_BAD_BRIGHTOBJC 0 /* default = 0 */

#define MASK_UPDATE_AFTER_MAP_COMPILATION 1 /* update the ThMask after compilation of map to exclude high S/N regions of the FOV */

#define MASK_BAD_NONLINEAR_OBJC_MAG 15.0 /* brightness above which masking happens */
#define MASK_BAD_LINEAR_OBJC_MAG 17.0

#define MASK_BAD_FLAG 0x0001
#define MASK_BAD_AGGRESSIVE_FLAG 0x0002
#define MASK_BAD_MILD_FLAG 0x0004
#define MASK_BAD_UNKNOWN_FLAG 0x0008
#define MASK_BAD_ANY_FLAG (MASK_BAD_FLAG | MASK_BAD_AGGRESSIVE_FLAG | MASK_BAD_MILD_FLAG)

#define MASK_BAD_NONLINEAR_OBJC_FACTOR 0.3 /* 0.3 */
#define MASK_BAD_LINEAR_OBJC_FACTOR 2.0 /* 2.0 */
#define MASK_BAD_STAR_FACTOR 1.0 /* 1.0 */ /* base factor for star masking - for non-aggressive masks */
#define MASK_BAD_GALAXY_FACTOR 0.1 /* 0.1 */ /* base factor for galaxy masking - for non-aggressive masks */
#define MASK_BAD_STAR_AGGRESSIVE_FACTOR 1.0 /* 1.0 */ /* factor for aggressive star masking */
#define MASK_BAD_GALAXY_AGGRESSIVE_FACTOR 1.0 /* 1.0 */ /* factor for aggressive galaxy masking */
#define MASK_BAD_STAR_MILD_FACTOR 0.10 /* 1.0 */ /* factor for aggressive star masking */
#define MASK_BAD_GALAXY_MILD_FACTOR 0.05 /* 1.0 */ /* factor for aggressive galaxy masking */

#define MASK_BAD_STAR_RMIN 5.0 /* 5.0 */
#define MASK_BAD_GALAXY_RMIN 5.0 /* 5.0 */
#define MASK_BAD_STAR_AGGRESSIVE_RMIN 20.0 /* 20.0 */
#define MASK_BAD_GALAXY_AGGRESSIVE_RMIN 20.0 /* 20.0 */
#define MASK_BAD_STAR_MILD_RMIN 2.0 /* 3.0 */
#define MASK_BAD_GALAXY_MILD_RMIN 2.0 /* 3.0 */

#define MASK_BAD_STAR_SDSS_PREFERRED 0 /* use SDSS masks for stars if available */
#define MASK_BAD_GALAXY_SDSS_PREFERRED 0 /* use SDSS masks for galaxies if available */
#define MASK_BAD_AGGRESSIVE_DISTANCE 350 /* default = 500 - the distance in pixels from the target non-linear object where the masking is agressive and the amplitude is dropped from the fit - if set to 500 pixels, then 25% of the field per target is masked in non-agressive mode, if set to 150 pixels then 15% of the field per target is masked in non-agressive mode */
#define MASK_BAD_DISTANCE 40 /* the distance in pixels from the target non-linear object where the masking is mild and the amplitud is kept in the list */

#define MASK_BAD_STAR_CUTOFF_PSF 100 /* 100 */
#define MASK_BAD_STAR_MARGIN_MIN 10 /* 10 */
#define MASK_BAD_STAR_MARGIN_MAX 30 /* 30 */
#define MASK_BAD_STAR_MATGIN_DEFAULT 10 /* 10 */

#define MASK_BAD_STAR_AGGRESSIVE_CUTOFF_PSF 10 /* 10 */
#define MASK_BAD_STAR_AGGRESSIVE_MARGIN_MIN 10 /* 10 */
#define MASK_BAD_STAR_AGGRESSIVE_MARGIN_MAX 30 /* 30 */
#define MASK_BAD_STAR_AGGRESSIVE_MATGIN_DEFAULT 20 /* 20 */

#define MASK_BAD_STAR_MILD_CUTOFF_PSF 500 /* 100 */
#define MASK_BAD_STAR_MILD_MARGIN_MIN 5 /* 10 */
#define MASK_BAD_STAR_MILD_MARGIN_MAX 10 /* 30 */
#define MASK_BAD_STAR_MILD_MATGIN_DEFAULT 5 /* 10 */

#define MASK_BAD_STAR_SDSS_NPIX_RATIO 0.5 /* 0.5 */
#define MASK_BAD_GALAXY_SDSS_NPIX_RATIO 0.5 /* 0.5 */

#define MASK_BAD_STAR_AGGRESSIVE_SDSS_NPIX_RATIO 1.5 /* 1.5 */
#define MASK_BAD_GALAXY_AGGRESSIVE_SDSS_NPIX_RATIO 1.5 /* 1.5 */

#define MASK_BAD_STAR_MILD_SDSS_NPIX_RATIO 0.15 /* 0.5 */
#define MASK_BAD_GALAXY_MILD_SDSS_NPIX_RATIO 0.15 /* 0.5 */

#define MASK_BAD_GAIN_ESTIMATE 5.0

#define MASK_UPDATE_ANY (MASK_BAD_LINEAR_OBJC | MASK_BAD_NONLINEAR_OBJC | MASK_BAD_SKY_OBJC | MASK_GOOD_SKY_OBJC | MASK_BAD_WEIRD_OBJC)
#define DEFAULT_MASK_UPDATE_FLAG (MASK_BAD_LINEAR_OBJC | MASK_BAD_WEIRD_OBJC) /* MASK_NO_UPDATE */ /* MASK_BAD_LINEAR_OBJC */
/* 
   the degree of continiuity (k) for I- and M-spline
   used for estimation of light profile from annular
   integrals
*/
#define WING_SPLINE_ORDER 4
#define SPLINE_R_DITHER 0.01 /* dithering constant to get a radius other than an exact 0 */
#define SPLINE_UPPER_DITHER 1.0E-4 /* should not be less than 1.0E-3 - this is the upper limit ditter for spline to get rid of the discontinuity at the end of the spline range */
#define BSPLINE_ORDER 3

/* the following is used by the Mle , or L package */

#define MX_PAR_PER_MODEL 10
#define MX_PAR_PER_OBJECT 100 /* this is used in parameter cost related memory allocations */
#define NLIMPACKS 5 /* number of impacks in each  LWORK */
#define LPIMPACKINDEX 4 /* the location of the impack used by LPROFILE utility for image handling */
#define LWORK_N_WVEC 4 /* the size of array containing wVec's */
#define PSEUDO_INVERSE_FOR_SINGULAR 1 /* use SVD for singular matrices */
#define GENERAL_CONVERGENCE (CONVERGED | TOOSMALLSTEP | TOOSMALLDERIV | TOOMANYITER | ABSOLUTEF_CONVERGENCE | X_CONVERGENCE | RELATIVEF_CONVERGENCE | SINGULAR_CONVERGENCE | FALSE_CONVERGENCE | ERROR_CONVERGENCE | EDGE_CONVERGENCE)
/* -- object and model classification constant by SOHO -- */

#define WARN_CLASSIFY 0 /* should a warning be issued if classification cannot be completed ? */

#define BAD_MODEL_INDEX -1 /* a negative integer */
#define CDCANDIDATE_CLASSIFICATION 0 /* should CD candidates be excluded 
					from normal galaxy types */
#define THOBJCID    int /* pragma SCHEMA */
#define THOBJCTYPE  int     /* pragma SCHEMA */
#define THMODELTYPE int	 /* pragma SCHEMA */

#define UNKNOWN_OBJC 0
#define PHOTO_OBJC   1
/* #define SKY_OBJC     2 */
#define MX_THOBJCTYPE 100 /* this is the maximum number of object type definitions allowed */

#define THNCOLOR 3 /* g, r, i */
#define OBJC_CLASS_BAND R_BAND /* classificatio band is 'r' */

/* Lupton 2001 */
#define PHOTO_GALAXY_CLASS_LIMIT 0.145 /* the difference between PHOTO's psd and model mag that will cause the object to be classifies as galaxy-only */
#define PHOTO_DEV_EXP_FRACTION 1.0 /* $fraci = 1.0 from 'proc classify_objc' in ./etc/read_objc.tcl */
#define PHOTO_EXP_DEV_FRACTION 1.0 /* the same as above */

#define PHOTO_DEV_CLASS_LIMIT -1000.0 /* the difference in PHOTO magnitude that will cause the object to be classified as deV-only */
#define PHOTO_EXP_CLASS_LIMIT -1000.0 /* the difference in PHOTO magnitude that will cause the object to be classified as exp-only */
#define PHOTO_STAR_CLASS_LIMIT -1000.0 /* the difference between PHOTO's psf and model magnitude that will cause the object to be classified as star-only */ 

#define SEMI_ORTHO_PL 0 /* if set then use a power law which is semi orthogonal to the deV core in the form x / (1 + x ^ 2) ^ n - Jim suggested not to use it and intead use 1 / (1 + x ^ 2) ^ n used classically by the community */
#if SEMI_ORTHO_PL
#define DEFAULT_PL_INDEX 2.0 /* default power law index used to initiate fit - the overall profile behaves like x ^ -3 for large x if this index is set to 2.0 */
#else
#define DEFAULT_PL_INDEX 1.5 /* default power law index used to initiate fit - the overall profile behaves like x ^ -3 for large x if this index is set to 1.5 */
#endif

#define EXP1_DEV_RATIO 1.0
#define EXP1_EXP_RATIO 1.0
#define EXP2_DEV_RATIO 1.0
#define EXP2_EXP_RATIO 1.0
#define GAUSSIAN_DEV_RATIO 1.0
#define GAUSSIAN_EXP_RATIO 1.0
#define DEV1_DEV_RATIO 1.0
#define DEV1_EXP_RATIO 1.0
#define DEV2_DEV_RATIO 1.0
#define DEV2_EXP_RATIO 1.0
#define PL_DEV_RATIO 3.0
#define PL_EXP_RATIO 3.0
#define SERSIC_DEV_RATIO 1.0
#define SERSIC_EXP_RATIO 1.0
#define SERSIC1_DEV_RATIO 0.5
#define SERSIC1_EXP_RATIO 0.5
#define SERSIC2_DEV_RATIO 3.0
#define SERSIC2_EXP_RATIO 3.0
#define CORESERSIC_DEV_RATIO 1.0
#define CORESERSIC_EXP_RATIO 1.0

#define EXP2_EXP1_INIT_RATIO 4.0
#define DEV2_DEV1_INIT_RATIO 10.0
#define PL_DEV_INIT_RATIO 10.0
#define DEV1_INIT_RADIUS 500 /* if re_small is larger than this, then it is replaced by this amount. choose it to be a very big number to cancel its effect */
#define DEV2_INIT_RADIUS 0.002 /* if re_large is smaller than this then it is replaced by this amount. choose it to be a very small number to cancel its effect */
#define PL_INIT_RADIUS 0.002 /* if re_large is smaller than this then it is replaced by this amount. choose it to be a very small number to cancel its effect */
#define INIT_MODEL_WITH_DEV_FRACTION 0 /* should we use deV-frac when assigning counts - this flag should be turned off when assigning counts to DEV_GALAXY objects */
#define INIT_MLE_DEV1_WITH_EXACT_PHOTO 1 /* use the deV finding for deV1 initiation */
#define INIT_MLE_DEV2_RANDOM 0 /* used to initiate deV2 randomly only when deV1 is accurately set from PHOTO deV */

/* used by MGInitAllObjectTypes */
#define DEFAULT_CDCLASS DEVDEV_GCLASS
#define STAR_CLASSIFICATION (int) 0x0001
#define GALAXY_CLASSIFICATION (int) 0x0010
#define CD_CLASSIFICATION (int) 0x0100
#define ALL_CLASSIFICATION (STAR_CLASSIFICATION|GALAXY_CLASSIFICATION|CD_CLASSIFICATION)
#define DEFAULT_CLASSIFICATION ALL_CLASSIFICATION
#define ALLOW_FORCE_CD_CLASSIFICATION 1 /* it should be set to 1 only during debug mode */
/*  --- profile generation -- */
#define PI (double) 3.14159265 /* pi */
#define TWOPI (double) 6.28318530718 /* 2 pi */
#define THPI (FL32) 3.14159265 /* pi */
#define THTWOPI (FL32) 6.28318530718 /* 2 pi */
#define GAMMA3 (double) 2    /* Gamma(3) used in calculating mcount for exp models */
#define GAMMA8 (double) 5040 /* Gamma(8) used in calculating mcount for deV models */
#define GAMMA9 (double) 40320 /* Gamma (9) used in calculating mcount for deV models */
#define SQRT2 (double) 1.41421356237 /* 2 ^ 0.5 */
#define SQRT_HALF (double) 0.70710678118 /* 2 ^ -0.5 */
#define RADIAN_DEGREE_RATIO 0.0174532925 /* pi / 180 */
#define NU_DEV (double) 7.66925 /* mathematical constant in definition of deV-law should not be altered 
				DEFAULT: 7.66925 */
#define NU_EXP (double) 1.67834699 /* mathematical cosntant in def of exp-law.  
				   DEFAULT nu = 1.678346990 */

#define DEV_MCOUNT (double) (PI * GAMMA9 * pow(NU_DEV, -8.0) * exp(NU_DEV)) 
/* (0.010322783 * 2226.74349497) */ /* (22.6652331786) */
	/* when multiplied by re^2 and I_deV it returns then full count for a deV model */
#define EXP_MCOUNT (double) (PI * GAMMA3 * pow(NU_EXP, -2.0) * exp(NU_EXP))  /* when multiplied by re^2 and I_exp it returns then full count for a Exp model */
#define PL_HALFLIGHT_SCALE (double) 2.26444 /* half light scale when power law index p = 2 is 2.26444 */

#define ESTIMATE_SMALL_EXP 0 /* should large negative exponents be attempted or forced to zero - available for deV and exp profiles */ 
#define SMALL_EXPONENT -20.0 /* exponents below this value are pushed to zero */

#define APPLY_GAMMA_TOLERANCE 0 /* when calculating the mcount for power law function */
#define GAMMA_TOLERANCE 0.01 /* tolerance used when comparing various approximations to gamma function */
 
#define SET_SQUARE_REGION 0 /* if 1: a square region size of 2 X g X re
				if 0: a rectangular region to fit g-ellipse
				*/
#define MGFL FL64 /* precision of pixel calculation in thMGProfile.c */
#define MYNANN 17	/* number of annula in SOHO's profile generation */
#define MYNANN2 18	/* number of annula in SOHO's profile generation */
#define MYNANN3 26  /* number of annula in SOHO's profile generation */
#define MYNANN4 26  /* number of annula in SOHO's profile generation */
#define ANNULUS_PHOTO_VER 0 /* do not change - version number for annulus choice used by PHOTO */
#define ANNULUS_UPDATED_PSF_VER 1 /* do not change - version number for annulus used during PSF calculation */
#define ANNULUS_UPDATED_GALAXY_VER 2 /* do not change - version number for annulus used during Galaxy 1D profile calc */
#define ANNULUS_DEFAULT_VER ANNULUS_UPDATED_PSF_VER /* do not change */
#define SQUARE_AREA_DIV 50 /* number of division in each iteration for estimation of 
				the overlap area between a circle and a square */
#define SQUARE_AREA_ITER 1 /* number of iterations for finding the area of overlap
				between a square and a circle */
/* extends the region to this value times re when making the gaussian function */
#define GAUSSIAN_SIGMA_EXTENSION 5
#define DEV_SIGMA_EXTENSION 8	/* for PHOTO this value is 7 */ 
#define EXP_SIGMA_EXTENSION 5	/* for PHOTO this value is 4 */

/* the profile truncates this fraction of total light */
#define DEFAULT_LUMCUT 1.0E-4
#define DEFAULT_INNER_LUMCUT DEFAULT_LUMCUT
#define DEFAULT_OUTER_LUMCUT DEFAULT_LUMCUT

#define DEFAULT_GAUSSIAN_LUMCUT 1.0E-4 /* 1.0E-4 belongs to 3.0 x re cut in gaussian */
#define DEFAULT_GAUSSIAN_INNER_LUMCUT DEFAULT_GAUSSIAN_LUMCUT
#define DEFAULT_GAUSSIAN_OUTER_LUMCUT DEFAULT_GAUSSIAN_LUMCUT

#define DEFAULT_SERSIC_LUMCUT 1.0E-3 
#define DEFAULT_SERSIC_INNER_LUMCUT DEFAULT_SERSIC_LUMCUT
#define DEFAULT_SERSIC_OUTER_LUMCUT DEFAULT_SERSIC_LUMCUT

#define DEFAULT_CORESERSIC_LUMCUT 1.0E-3 
#define DEFAULT_CORESERSIC_INNER_LUMCUT DEFAULT_CORESERSIC_LUMCUT
#define DEFAULT_CORESERSIC_OUTER_LUMCUT DEFAULT_CORESERSIC_LUMCUT

#define DEFAULT_PL_LUMCUT 1.0E-4
#define DEFAULT_PL_INNER_LUMCUT DEFAULT_PL_LUMCUT
#define DEFAULT_PL_OUTER_LUMCUT DEFAULT_PL_LUMCUT

#define DEFAULT_DEV_LUMCUT 1.0E-4  /* PHOTO value is 0.07 resulting in an image half-size 7 x re; 
				for LUMCUT = 1.0E-4, half-size ~ 80 x re 
				for LUMCUT = 1.0E-3, half-size ~ 42.5 x re
				for LUMCUT = 1.0E-2, half-size ~ 19.6 x re */ 
#define DEFAULT_DEV_INNER_LUMCUT 1.0E-4
#define DEFAULT_DEV_OUTER_LUMCUT DEFAULT_DEV_LUMCUT /* don't change this */
#define DEFAULT_EXP_LUMCUT 1.0E-4  /* PHOTO value is ~ 0.017 resulting in an image half-size of 3 x re; 
				for LUMCUT = 1.0-4, half-size ~ 7 x re
				for LUMCIT = 1.0E-3, half-size ~ 5.5 x re
				for LUMCUT = 1.0E-2, half-size ~ 3.8 x re
				*/
#define DEFAULT_EXP_INNER_LUMCUT 1.0E-4 /* note that all lum cuts refer to the fraction of light falling outsode of a certain radius */
#define DEFAULT_EXP_OUTER_LUMCUT DEFAULT_EXP_LUMCUT /* don't change this */

#define DEFAULT_STAR_LUMCUT 1.0E-3
#define DEFAULT_STAR_INNER_LUMCUT DEFAULT_STAR_LUMCUT
#define DEFAULT_STAR_OUTER_LUMCUT DEFAULT_STAR_LUMCUT

/* extension from each side to allow for proper convolution with PSF */
#define DO_CRUDE_CALIB	    1
#define FLUX20_EXTNO	    7   /* the extension number in psField files for structures containing flux20 keyword */
#define CUTOFF_INNER_PSF_DN 0.1 /* the number of photons (DN) that PSF convolution should allow to fall beyond the modeled region */
#define CUTOFF_OUTER_PSF_DN 0.1 /* the number of photons (DN) that PSF convolution should allow to fall beyond the modeled region */ 
#define INNER_PSF_SIZE      901 /* this should be double the maximum wing radius */
#define INNER_PSF_SIZE_LOW  45  /* this should be double the minimum wing radius */
#define OUTER_PSF_SIZE	    31  /* PHOTO value is 31 */
#define BORDER_PSF_ONE      15  /* definition of border region for PSF matching  */
#define BORDER_PSF_TWO      100  /* definition of border region for PSF matching */
#define PSF_GLUE_METHOD_1   0    /* 0: glue and make sure the PSF integral in the KL square is the same as that of the WING
					(Lagrange multipliers, 3 variables)
				    1: first glue and then normalize the whole glued PSF
					(2 variables) 
				*/ 
#define PSF_REPLACE_PIX   1	/* should KL-PSF in low S/N regions be replaced with extended PSF */
#define PSF_REPLACE_RAD   10    /* the inner radius of the region for replacement of KL-PSF pixels with extended radial PSF */

#define PSF_NEGATIVE_COUNTS_ISSUE_ERROR 0 /* makes sure that the total counts in the PSF is positive before its construction.
					suggested value: 1 */
#define DEFAULT_GALAXY_PSF_CONVOLUTION CENTRAL_AND_WINGS_PSF /* RADIAL_PSF,  CENTRAL_PSF NO_PSF_CONVOLUTION,  CENTRAL_AND_WINGS_PSF */  
#define STAR_PSF_MARGIN     501
#define GAUSSIAN_PSF_MARGIN 51
#define DEV_PSF_MARGIN 	    31
#define EXP_PSF_MARGIN 	    31
#define SERSIC_PSF_MARGIN   31
#define cD_PSF_MARGIN	    301

#define PSF_CORRECTION_TOLERANCE 5.0E-4 /* tolerance of psf-correction due to new background */
#define PSF_RELATIVE_CORRECTION_TOLERANCE 1.0 /* tolerance of psf-correction due to new background estimation in relative terms to lost light and halo frac */
#define PSFNRAD 150   		/* PSF nrad for reporting in do-psf executable */
#define PSFOUTBOUNDARY         40  /* setting for PSF reporting - and possibly future PSF boundary definition */
#define DEFAULT_N_PSF_STAR 	2  /* how many random stars will be generated to make the PSF report */
#define DEFAULT_MAX_PSF_STAR    5  /* the maximum number of stars that can be used to make the PSF report */
#define DEFAULT_PSF_XCMIN	0
#define DEFAULT_PSF_XCMAX	DEFAULT_NROW
#define DEFAULT_PSF_YCMIN	0
#define DEFAULT_PSF_YCMAX 	DEFAULT_NCOL
#define N_PSF_REPORT_TYPE       3
#define PSF_REPORT_REGION       0    /* also refer to DUMP_PSF_REPORT_REGION */
#define PSF_REPORT_RADIAL_PROFILE 0  /* also refer to DUMP_PSF_REPORT_RADIAL_PROFILE */
#define MAX_PSF_WARNING_COUNT 2 /* number of maximum psf warning messages during a run */
/* the following ratio and difference is use to forego division into in/out regions and only use on */
#define NROW_OUT_IN_RATIO 1.2
#define NCOL_OUT_IN_RATIO 1.2
#define NROW_OUT_IN_MARGINE 10
#define NCOL_OUT_IN_MARGINE 10
#define PSF_BZERO 1000   /* this is the shift in the scaled PSF before outputting in report as an integer region */
#define PSF_BSCALE 1.0E6  /* this is the scale factor in PSF before converting it into integer to reduce loss; used by PSF_REPORT */
/* fftw is used for convolution */
#define FFTWFL double
#define NO_FFTW_DURING_TEST 0 /* this has to be 0 */

/* maximum and minimum radii for fit - used only when RSUPERLOG transformation is in place */
#define MAXRADIUS 4.0E2 /* suggested to be larger that _RE_MAX for all models */
#define MINRADIUS 9.0E-4 /* suggested to be smaller that _RE_MIN for all models */

/* maximum and minimum radii for fit - used only when RSUPERLOG transformation is in place and for CORESERSIC models only */
#define MAXRADIUSB 2.0E2 /* suggested to be larger that _RE_MAX for all models */
#define MINRADIUSB 9.0E-4 /* suggested to be smaller that _RE_MIN for all models */




/* default ellipticity when (ab) is bad */
#define DEFAULT_ELLIPTICITY 0.0
#define NARROWEST_ELLIPTICITY 0.9995 /* e = 0.980 .... a/b = 5
				      e = 0.995 .... a/b = 10
				      e = 0.998 .... a/b = 15 
				      e = 0.99874921777 ... a/b = 20 (SDSS default)
				      e = 0.999 .... a/b = 22
				      e = 0.9992 .... a/b = 25
				      e = 0.9995 .... a/b = 32
				      e = 0.9998 .... a/b = 50 */
#define MAXELL 0.9998 /* this value should be a bit bigger than
			 NARROWEST_ELLIPTICITY but still smaller than 1.0 
			======
			e = 0.9999 results in negative discrim for fake objc in float */
/* radii below these are considered too small in the initiation process 
so if too small they are converted to these particular radii *
*/
#define MAX_VW atanh(MAXELL)
#define MIN_VW -atanh(MAXELL)

#define EXP_RE_SMALL 2.0E-2 /* 5.0E-2, 1.0E-1 */
#define EXP_RE_LARGE 3.0E2 /* 5.0E1, 1.0E2 */ /* 1.0E5 */
#define DEV_RE_SMALL 2.0E-2 /* 5.0E-2, 1.0E-1 */
#define DEV_RE_LARGE 3.0E2 /* 5.0E11.0E2 */ /* 1.0E5 */
#define EXP1_RE_SMALL 2.0E-2 /* 2.0E-1, 1.0E-1 */
#define EXP1_RE_LARGE 3.0E2 /* 5.0E1, 1.0E2 */ /* 1.0E5 */
#define DEV1_RE_SMALL 2.0E-2 /* 5.0E-1, 1.0E-2 */
#define DEV1_RE_LARGE 3.0E2 /* 5.0E1, 1.0E2 */ /* 1.0E5 */
#define EXP2_RE_SMALL 2.0E-2 /* 5.0E-1, 4.0E-1 */
#define EXP2_RE_LARGE 3.0E2 /* 5.0E1, 1.0E2 */ /* 4.0E5 */
#define DEV2_RE_SMALL 2.0E-2 /* 5.0E-1, 4.0E-2 */
#define DEV2_RE_LARGE 3.0E2 /* 5.0E1, 1.0E2 */ /* 4.0E5 */
#define PL_RE_SMALL 4.0E-2
#define PL_RE_LARGE 1.0E2 /* 4.0E5 */
#define GAUSSIAN_RE_SMALL 1.0E-1
#define GAUSSIAN_RE_LARGE 1.0E2 /* 1.0E5 */

#define SERSIC_RE_SMALL 2.0E-2 /* 5.0E-2,  2.0 */   /* 1.0E-1 */
#define SERSIC_RE_LARGE 3.0E2 /* 5.0E1,  1.0E2 */ /* 1.0E5 */

/* Reasonable constraints on sersic index: investigation of random search findings for singe sersic profiles suggests that dominant majority of LRGs have sersic indices strictly less than 10, less than 3% of LRGs have a sersic index greater than 13. 2% can be explained by an extremely bright star (mag ~ 14) within 20 arcsec of the LRG, while the cause for anomalic 1% is not understood at the time of investigation (Feb 24, 2020). There is noLRG with a sersic index 10 < n < 13. This investigation is based on a sample of 102 LRGs from run = 2708 which is on stripe 82. The method of search is Random Search based on 10,000 realization for each field. Based on this investigation, we suggest that derivative based are constrained to n < 12 */
#define DEFAULT_SERSIC_INDEX_SMALL 0.1 /* 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.5, 0.10 */  /* 0.631 */ /* 0.10 */
#define DEFAULT_SERSIC_INDEX_LARGE 20.0 /* 30.0, 25.0, 20.0, 17.5, 15.0, 10.0, 8.0, 6.0, 17.0 */ /* 5.623 */ /* 17.0 */
#define DEFAULT_SERSIC1_INDEX_SMALL 0.1 /* 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.5, 0.10 */
#define DEFAULT_SERSIC1_INDEX_LARGE 20.0 /* 30.0, 25.0, 20.0, 17.5, 15.0, 10.0, 8.0, 6.0, 17.0 */
#define DEFAULT_SERSIC2_INDEX_SMALL 0.1 /* 0.1, 0.1, 0.1, 0.1, 0.1, 0.5, 0.10 */
#define DEFAULT_SERSIC2_INDEX_LARGE 20.0 /* 30.0, 25.0, 20.0, 17.5, 20.0, 10.0, 8.0, 6.0, 17.0 */

#define SERSIC_Z_TOLERANCE 1.0E-10
#define N_SERSIC 4.0 /* use this when not drawing a random sersic index for sersic model */
#define N_SERSIC1 3.0 /* use this when not drawing a random sersic index for sersic1 model */
#define N_SERSIC2 5.0 /* 7.0 */ /* use this when not drawing a random sersic index for sersic2 model */
#define DRAW_RANDOM_SERSIC_INDEX 1 /* if set to 1, draws random sersic index for sersic model in the following range */
#define DRAW_RANDOM_SERSIC1_INDEX 1 /* if set to 1, draws random sersic index for sersic1 model in the following range */
#define DRAW_RANDOM_SERSIC2_INDEX 1 /* if set to 1, draws random sersic index for sersic2 model in the following range */

#define RE_SERSIC_DRAW1 SERSIC_RE_SMALL
#define RE_SERSIC_DRAW2 SERSIC_RE_LARGE

#define N_SERSIC_DRAW1 1.0
#define N_SERSIC_DRAW2 5.0 /* 7.0 */

#define CORESERSIC_RB_SMALL 5.0E-2
#define CORESERSIC_RB_LARGE 5.0E1

#define CORESERSIC_RE_SMALL 1.0E-1
#define CORESERSIC_RE_LARGE 1.0E2 /* 1.0E5 */

#define DEFAULT_CORESERSIC_INDEX_SMALL 0.10
#define DEFAULT_CORESERSIC_INDEX_LARGE 17.0

#define CORESERSIC_DELTA_SMALL 0.5
#define CORESERSIC_DELTA_LARGE 50.0
#define CORESERSIC_GAMMA_SMALL 0.01
#define CORESERSIC_GAMMA_LARGE 1.90

#define NN_CORESERSIC 6.0 
#define D_CORESERSIC 3.0 /* use this when not drawing a random sersic index for sersic2 model */
#define G_CORESERSIC 0.10
#define DRAW_RANDOM_CORESERSIC_INDEX 1 /* if set to 1, draws random sersic index for coresersic model in the following range */
#define N_CORESERSIC_DRAW1 2.0
#define N_CORESERSIC_DRAW2 10.0
#define G_CORESERSIC_DRAW1 0.05
#define G_CORESERSIC_DRAW2 0.20
#define D_CORESERSIC_DRAW1 1.0
#define D_CORESERSIC_DRAW2 5.0
#define CORESERSIC_MAX_NDRAW 1000

#define CORESERSIC_NEWTON_INTEGRAL_VERYLARGE_XRATIO 50.0
#define CORESERSIC_NEWTON_INTEGRAL_VERYLARGE_NDIV 1000
#define CORESERSIC_NEWTON_INTEGRAL_LARGE_NDIV 1000
#define CORESERSIC_NEWTON_INTEGRAL_SMALL_NDIV 20
#define CORESERSIC_NEWTON_CONVERGENCE_IMAX 1000
#define CORESERSIC_NEWTON_CONVERGNENCE_EPSILONX 0.001
#define CORESERSIC_NEWTON_CONVERGENCE_EPSILONY 0.001

#define QUADFL double /* the precision used when applying quadrature in thMGProfile package */
#define CORESERSIC_APPROXIMATE_XRATIO ((QUADFL) 1.0E-10) /* use certain approximation when conducting Gaussian quadrature in the formula */

#define DEFAULT_ELL_DESC (SUPERCANONICAL_RSUPERLOG | INDIRECT_SERSIC_INDEX) /* CANONICAL */ /* SUPERCANONICAL_RSUPERLOG */ /* default elliptical description */

#define PROFILE_REG_AREA_FACTOR 2 /* if new reg can be this much smaller than the old reg, then reallocate reg */
#define XC_NULL -888.0
#define YC_NULL	-888.0
/* profile masks - naive masks */
#define N_ELEM_NAIVE_MASK 4

/* radial ratio constraint in package PCOST */

/* lower threshold - constraining how close core and halo radii can become */
#define PCOST_LOWER_THRESHOLD 0.1 /* value of radial raio where extra constraint should be imposed */
#define PCOST_LOWER_THRESHOLD2 0.01 /* value of radial raio where extra constraint should be imposed */
#define CHISQ_PCOST_LOWER_THESHOLD 3.0E6 /* close to chisq value of a fit */
#define PCOST_LOWER_INDEX 4.0 /* should be 3.0 or higher */

/* upper threshold - constraining how different core and radii can become */
#define PCOST_UPPER_THRESHOLD 10.0 /* value of the radial ratio where extra constraint should be imposed */
#define PCOST_UPPER_THRESHOLD2 20.0 /* the second upper threshold where cost introduced becomes comparable to chisq value of the fit */
#define CHISQ_PCOST_UPPER_THRESHOLD 3.0E6 /* close to chisq value of a fit */
#define PCOST_UPPER_INDEX 4.0 /* should be 3.0 or higher */

#define PCOST_RCORE_LOWER_THRESHOLD 0.1
#define PCOST_RCORE_LOWER_THRESHOLD2 0.05
#define CHISQ_PCOST_RCORE 3.0E6 
#define PCOST_RCORE_INDEX 4.0

/* ellipticity constraint */ 
#define PCOST_E -0.1 /* this is the lower edge of negative ellipticities allowed */
#define PCOST_AB_LOWER_THRESHOLD 20.0
#define PCOST_AB_LOWER_THRESHOLD2 40.0
#define CHISQ_PCOST_E 3.0E6
#define PCOST_E_INDEX 4.0

/* don't touch the following */
#define LNPCOST_LOWER_THRESHOLD log((float) PCOST_LOWER_THRESHOLD)
#define LNPCOST_LOWER_THRESHOLD2 log((float) PCOST_LOWER_THRESHOLD2)
#define LNPCOST_LOWER_THRESHOLD_MARGIN ((float) LNPCOST_LOWER_THRESHOLD - (float) LNPCOST_LOWER_THRESHOLD2)
#define LNPCOST_UPPER_THRESHOLD log((float) PCOST_UPPER_THRESHOLD)
#define LNPCOST_UPPER_THRESHOLD2 log((float) PCOST_UPPER_THRESHOLD2)
#define LNPCOST_UPPER_THRESHOLD_MARGIN ((float) LNPCOST_UPPER_THRESHOLD2 - (float) LNPCOST_UPPER_THRESHOLD)

#define LNPCOST_RCORE_LOWER_THRESHOLD log((float) PCOST_RCORE_LOWER_THRESHOLD)
#define LNPCOST_RCORE_LOWER_THRESHOLD2 log((float) PCOST_RCORE_LOWER_THRESHOLD2)
#define LNPCOST_RCORE_LOWER_THRESHOLD_MARGIN ((float) LNPCOST_RCORE_LOWER_THRESHOLD2 - (float) LNPCOST_RCORE_LOWER_THRESHOLD)

#define LNPCOST_AB_LOWER_THRESHOLD log((float) PCOST_AB_LOWER_THRESHOLD)
#define LNPCOST_AB_LOWER_THRESHOLD2 log((float) PCOST_AB_LOWER_THRESHOLD2)
#define LNPCOST_AB_MARGIN ((float) LNPCOST_AB_LOWER_THRESHOLD2 - (float) LNPCOST_AB_LOWER_THRESHOLD)

/* --- THREGION constants --- */

#define DEFAULT_NCOMPONENT -1 
#define WARN_HUGE_REGION 0 /* warn when a huge region or mask is encountered */
#define WARN_NULL_REG3 0 /* warn when REGION3 is defined to point to null row or col */

/* -- PSF convolution -- */
#define BAD_PSF_INDEX -1 /* negative integer */
#define RC_SCALE 1e-3 /* DO NOT CHANGE default PHOTO value is 1e-3 */

/* -- Proc and Frames -- */
#define FRAMEFILES_SIZE 78 /* size of framefiles structure, the workspace can contain this many strings of length MX_STRING_LEN */
#define MAX_FITID 2 /* the number of maximum MLE structures that is saved inside a frame */
#define ALLOW_MAX_NOBJC 0 /* should be 0 is dispatched binary */
#define DEFAULT_MAX_NOBJC 100000 /* is used only when the flag above is set */
#define ORDER_PHOBJC_LIST_AFTER_LOAD 1 /* order the list of phobjc after reading fpObj file * - should not matter in non-debug runs */
#define PHOTO_WEIGHT_DEFINITION 1 /* PHOTO defines statistical weight because its model is does not include dark noise, this term is added as a separate variance (read_noise) by PHOTO. Our software includes a model of the dark noise therefore only the image photon counts is used to calculate the poisson weight */
#define GAUSSIAN_NOISE 1 /* use Gaussian noise to generate simulations - particulalry useful to create a chi-squared value which is closer to 1.0 */
#define CONSTANT_GAUSSIAN_NOISE 0 /* only used during simulation to ensure valid Chi-squared distribution (otherwise gain variation for various amplifiers dominated the chi-squared dispersion */
#define DEFAULT_GAUSSIAN_NOISE_DN 15 /* the default amplitude of gaussian noise when CONSTANT_GAUSSIAN_NOISE is set */
/* -- memory management -- */

#define thFree shFree
#define thCalloc shCalloc
#define thMalloc shMalloc

#define KILOBYTE 1024
#define MEGABYTE 1048576
#define GIGABYTE 1073741824
#define TERABYTE 1099511627776

#define KEEP_FITMODEL_IN_MEMORY 0 /* if this flag is set to 1 then fit model(s) and its neighbors are kept in memory and never deleted once they are generated. the code is not tested fully under these conditions */
#define NON_EXISTENT_NODE -1 /* should be a negative integer */
#define DATA_NODE -2 /* should be a negative integer different from NON_EXISTENT_NODE */
#define INDEFINITE_TIMES -1 /* this is a value reserved for runtime flag in algorithm action */
#define MEMFL double	/* pragma SCHEMA */ 
/* setting this to longer data type that 'long int' will impede speed */
#define MEMDBLE double	/* pragma SCHEMA */
/* setting this data to a type shorter than 'long long int' 
	will create problems above 4GB 
*/

/* new region handling - allocation, prefabrication, fabrication */
#define WISE_REGION_ALLOCATION 1
#define PREFAB_ROWS 0

#define ALGORITHM_NOBJ_DEFAULT 3072 
/* ALGORITHM_NOBJC_DEFAULT is the default list size in compiling the memory algorithm. it is the maximum number of objects we guess. if the number of objects is larger than this, the list will be deleted and recreated. the number should be large enough to avoid multiple creation and deletion of the list resulting in memory fragmentation. the suitable guess is 2048 */
#define ALGORITHM_NSTEP_DEFAULT 1524288
#define ALGORITHM_FITMODEL_REGION_EXPANSION_FACTOR 200.0 /* used to reach out of the initial region estimation for non-linear fit objects when creating the algorithm to avoid re-calibration of the algorithm */
#define ALGORITHM_FITMODEL_REGION_SIZE 2048.0 /* the minimum size of region for models with non-linear fits during construction of algorithm */
#define INDEFINITE_MEMORY (MEMDBLE) -1.0 /* this value signals to the machine that it needs to use a classic method for memory management rather than design an algorithm */
#define MEMORY_DEFAULT (MEMDBLE) 2048 * MEGABYTE /* I used to use 1.3 GB but return a ~ 50% failure upon memory allocation by Dervish */
#define MEMORY_MIN     (MEMDBLE) 200 * MEGABYTE
#define MEMORY_MAX     (MEMDBLE) 4 * GIGABYTE
#define MEMORY_FUDGE_FACTOR (MEMDBLE) 2 /* you multiply this number with size of LWORK to get an estimate of all the memory needed by everything other than the algorithm */
#define FREE_TO_OS 1 /* should the degragmentation be done by freeing to OS or should it be done by Dervish's Garbage collector */
#define UPDATE_DEGREE_FROM_ADJ 1 /* calculate degree from updated adj each time */
/* -- fitting -- */
#if 1
	#if 1
		#if 1
		#define LMETHOD_DEFAULT HYBRIDLM /* EXTLM */
		#define LMETHODSUBTYPE_DEFAULT QUASINEWTON
		#else
		#define LMETHOD_DEFAULT EXTLM /* SGD GD HYBRIDLM, EXTLM */
		#define LMETHODSUBTYPE_DEFAULT LEVENBERGMARQUARDT /* STEEPESTDESCENT LEVENBERGMARQUARDT QUASINEWTON, LEVENBERGMARQUARDT */
		#endif
	#else
	#define LMETHOD_DEFAULT SGD /* SGD GD HYBRIDLM, EXTLM */
	#define LMETHODSUBTYPE_DEFAULT STEEPESTDESCENT /* STEEPESTDESCENT LEVENBERGMARQUARDT QUASINEWTON, LEVENBERGMARQUARDT */
	#endif
#else
	#if 1
		#if 1
		#define LMETHOD_DEFAULT RSV
		#define LMETHODSUBTYPE_DEFAULT RSVPURE
		#else
		#define LMETHOD_DEFAULT PGD
		#define LMETHODSUBTYPE_DEFAULT PGDPURE
		#endif
	#else
		#if 0
		#define LMETHOD_DEFAULT MGD
		#define LMETHODSUBTYPE_DEFAULT MGDPURE
		#else
		#define LMETHOD_DEFAULT NAG
		#define LMETHODSUBTYPE_DEFAULT NAGPURE
		#endif	
	#endif
#endif
#define PGD_SECOND_PHASE STEEPESTDESCENT  /* STEEPESTDESCENT LEVENBERGMARQUARDT */

/* levenberg marquardt fine tuning 
 * based on
 *
IMM DEPARTMENT OF MATHEMATICAL MODELLING	J. No. MARQ
8.4.1999 Technical University of Denmark	HBN/ms
DK-2800 Lyngby – Denmark
DAMPING PARAMETER IN MARQUARDT’S METHOD
Hans Bruun Nielsen
TECHNICAL REPORT IMM-REP-1999-05
*/

#define LM_LAMBDA_DEFAULT 0.2E-1  /* default is 0.2 */
#define LM_NU_DEFAULT 2.0
#define LM_NU_BOOST_FACTOR 2.0 /* default value is 2.0 */
#define LM_GAMMA_DEFAULT 3.0 /* used for non-constant tweaking of lambda - default = 3.0 */
#define LM_BETA_DEFAULT 2.0 /* used for non-constant tweaking of lambda - default = 2.0 */
#define LM_RHOBAR_DEFAULT 0.5 /* used for non-constant tweaking of lambda - default = 0.5 */
#define LM_Q_DEFAULT 3 /* used for non-constant tweaking of mabda - default = 3 - odd exponent */
#define LM_TAU_DEFAULT 1.0E-2
#define LM_E1_DEFAULT 1.0E-1 /*this is the value used to compare to derivative for convergence test:  default is 1.0E-6 */
#define LM_E2_DEFAULT 1.0E-3 /* this is the value used for h-convergnce test, default is 1.0E-7 */
#define LM_E1_PGD_SECOND_PHASE 1.0E-4 /* this is the correction to E1 and E2 values in the second phase of PGD */
#define LM_E2_PGD_SECOND_PHASE 1.0E-6 /* this is the correction to E1 and E2 values in the second phase of PGD */
#define LM_KMAX_DEFAULT 4990 /* suggested value = 200 for LM and = 3000 for PGD and NAG */
#define CONTROL_GD_STEPSIZE 1
#define CONTROL_PGD_STEPSIZE 0
#define CHECK_GD_STEPSIZE 1
#define ETA_FIXED_PGD 1 /* should the eta value be fixed for PGD */
#define ADADELTA 0 /* if set to 1, it conducts ADADELTA */
#define DEFAULT_LWMASK_NSAMPLE 32
#define DEFAULT_LWMASK_NMINI 1
#define START_WITH_AMPFIT 1 /* if set to 1 then start the nonlinear fit with an estimation of amplitudes */
#define AMPLITUDE_CV 0.0 /* used when randomizing the amplitude */
#define ETA_UPDATE_CLASSICAL 1 /* should the update of eta max be classical way */
#define LM_ETA_MAX_DEFAULT 1.0E-5 /* epsilon value for AdeDelta method in GD and SGD */
#define LM_ETA_MAX_PGD_SECOND_PHASE 2.0E-6 /* the starting eta_max in second phase of PGD */
#define LM_ETA_GD_MAX_LARGE 1.0E16
#define LM_ETA_GD_MAX_SMALL 1.0E-17
#define LM_MOMENTUM_DEFAULT 0.80
#define LM_LEARNING_RATE_DEFAULT 5.0E-3
#define LM_NAG_MIN_INIT_STEP 0.05
#define LM_NAG_RESTART_THRESH 50 /* if small deriveative is found in less than this many steps, then it is interpreted as a sign of edge effects and the minimizer is restarted */
#define LM_NAG_INIT_THRESH 30 /* 20, 10, the number of times the minimizer can be initialized due to small derivatives on the edges (edge effect) */
#define LM_NAG_INIT_THRESH_LOW 3 /* the number of times the minimizer can be initialized for any reason */
#define LM_NAG_XN_MIN_RANDOM (MLEFL) -1.0 /* the minimum xN value when drawing randomly within minimizer */
#define LM_NAG_XN_MAX_RANDOM (MLEFL) 1.0 /* the maximum xN value when drawing randomly within minimizer */
#define LM_PGD_XN_MIN_RANDOM (MLEFL) -1.0 /* the minimum xN value when drawing randomly within minimizer */
#define LM_PGD_XN_MAX_RANDOM (MLEFL) 1.0 /* the maximum xN value when drawing randomly within minimizer */
#define LM_RSV_XN_MIN_RANDOM (MLEFL) -4.0 /* the minimum xN value when drawing randomly within minimizer */
#define LM_RSV_XN_MAX_RANDOM (MLEFL) 2.5 /* the maximum xN value when drawing randomly within minimizer */
#define LM_NAG_EDGE_VAL_THRESH1 5.0 /* threshold value that defines the edge of the space */
#define LM_NAG_EDGE_VAL_THRESH2 -5.0 /* threshold value that defines the edge of the space */
#define LM_RHO_ADADELTA_DEFAULT 0.9 /* default rho value for AdaDelta method in GD and SGD */
#define LM_EPSILON_ADADELTA_DX_DEFAULT 1.0E-5 /* default epsilon value for AdaDelta in GD and SGD */
#define LM_EPSILON_ADADELTA_G_DEFAULT 1.0E-5 /* default epsilon value for AdaDelta in GD and SGD */
#define LM_MINIBATCH_CORRECTION_ADADELTA 1.0E-2 /* the correction factor used in calculating the AdaDelta stepsize */
#define LM_SMALL_STEP_DCHISQ_VALUE 0.005 /* chi-sq changes below this value count as _small step_ */
#define LM_SMALL_STEP_INCREMENT_NEGATIVE_DCHISQ 0.5 /* a small increase in chi-squared counts counts as a partial step */
#define LM_SMALL_STEP_KMAX /* the maximum number of small steps needed to raise convergence flag */
#define LM_DEFAULT_SECTOR APSECTOR /* suggested sector for conducting LM */
#define LM_CONDUCT_AMPFIT 0 /* this should definitely be set to 1 if the working sector is PSECTOR. For APSECTOR, it can be set to 0 to avoid degeneracy issues with ampliude component inversion */
#define LM_AMPFIT_CHANCE 0.05 /* the chance of conducting an amplitude fit when invoked */
#define LM_GAUSS_NEWTON_THRESHOLD 0.02 /* suggested value by Madsen 1988 where || F' ||  < 0.02 F results in a switch to Quasi-Newton formalism - default value is 0.02 according to Madsen 1988 */
#define QN_DELTA_DEFAULT 0.1 /* default delta_qn size when creating the LMETHOD structure */
#define QN_DELTA_MAX 1.0 /* the biggest delta_qn than can ever be assigned */
#define HYBRID_LM_ALLOWANCE 3 /* the number of times F' < 0.02 F (gauss-newton failure) is raised within Levenberg Marquardt
 before we switch to Quasi-Newton method -default is 3 according to Madsen 1988 */
#define HYBRID_QN_ALLOWANCE 10 /* the number of times F'(new) > F'(old) is raised (failure of Quasi-newton method) within Quasi Newton formalism before swicth is made back to Levenberg-Marquardt method. The default is 1 according to Madsen 1988 */
#define HYBRID_LM_SINGULARITY_ALLOWANCE 6 /* if a singular AP-matrix is reached how many step counts does this count toward the LM allowance */
#define QUASI_NEWTON 1
#define SYMMETRIC_RANK_ONE 1 /* When Quasi-Newton is set: if set to 1 use SR1 (symmetric rank one) update on Hessian matrix, if set to 0, it uses BGFS */
#define LM_COUNT_PGD_THRESH_DEFAULT 1 /* how often should random perturbation in PGD should happen */
#define LM_K_PGD_THRESH_DEFAULT 50 /* how often should random perturbation in PGD should happen */
#define LM_PERTURBATION_PGD_SIZE_DEFAULT ((MLEFL) 0.2) /* the size of the PGD random ball */
#define LM_COST_PGD_THRESH_DEFAULT ((MLEFL) 0.1) /* the necessary improvement of the PGD cost function after random shift to finalize the PGD step */
#define LM_MAG_FPRIME_PGD_THRESH ((MLEFL) 0.1)
/* perturbation codes for PGD  - do not alter */
#define PERTURB_DUE_TO_CONVERGE 0x0001
#define PERTURB_DUE_TO_LATE 0x0002
#define PERTURB_DUE_TO_EDGE 0x0008
#define NO_PERTURB 0x0000
#define PERTURB_GENERAL (PERTURB_DUE_TO_CONVERGE | PERTURB_DUE_TO_LATE | PERTURB_DUE_TO_EDGE)

#define NSTEP_MIN_ENOUGH 5 /* if the average number of steps is smaller than this, keep trying new attempts */
#define NSTEP_MIN_REQUIRED 3 /* you should take at least this many steps before your convergence is evaluated */
#define NSTEP_QN_MIN_REQUIRED 1 /* you need to at least take this many QN steps before QN convergence is tested */
/* Multi-init Levenberg Marquardt to address complex terraine */
#if 1

#if 0
#define DEFAULT_INIT_COUNT 5 /* default number of initiation attempts and fits */
#define MAX_INIT_COUNT 10 /* maximum number of iterations possible */
#define INITID_THRESH_STDEV_IQR 5 /* when should we start using IQR instead of STDEV to assess complexity of terrain */
#define EASY_TERRAIN_CHISQ_STDEV 0.5 /* what chi-square spread is labeled -easy- terrain */
#define HARD_TERRAIN_CHISQ_STDEV 100.0 /* what chi-squared spread is labeled -hard- terrain */
#define EASY_TERRAIN_CHISQ_IQR 0.5 /* what chi-squared spread is labeled -easy- terrain */
#define HARD_TERRAIN_CHISQ_IQR 100.0 /* what chi-squared spread is labeled -hard- terrain */

#define EASY_INIT_COUNT 2
#define MEDIUM_INIT_COUNT 5
#define HARD_INIT_COUNT 10

#define MAX_TRUE_INIT_COUNT 20

#else

	#if 0
	#define DEFAULT_INIT_COUNT 40 /* 10, default number of initiation attempts and fits */
	#define MAX_INIT_COUNT 40 /* 20,  maximum number of iterations possible */
	#define INITID_THRESH_STDEV_IQR 5 /* when should we start using IQR instead of STDEV to assess complexity of terrain */
	#define EASY_TERRAIN_CHISQ_STDEV 0.25 /* what chi-square spread is labeled -easy- terrain */
	#define HARD_TERRAIN_CHISQ_STDEV 50.0 /* what chi-squared spread is labeled -hard- terrain */
	#define EASY_TERRAIN_CHISQ_IQR 0.25 /* what chi-squared spread is labeled -easy- terrain */
	#define HARD_TERRAIN_CHISQ_IQR 50.0 /* what chi-squared spread is labeled -hard- terrain */

	#define EASY_INIT_COUNT 40 /* 4 */
	#define MEDIUM_INIT_COUNT 40 /* 10 */
	#define HARD_INIT_COUNT 40 /* 20 */

	#define MAX_TRUE_INIT_COUNT 80

	#else
	
	#define DEFAULT_INIT_COUNT 1 /* 10, default number of initiation attempts and fits */
	#define MAX_INIT_COUNT 1 /* 20,  maximum number of iterations possible */
	#define INITID_THRESH_STDEV_IQR 1 /* when should we start using IQR instead of STDEV to assess complexity of terrain */
	#define EASY_TERRAIN_CHISQ_STDEV 0.25 /* what chi-square spread is labeled -easy- terrain */
	#define HARD_TERRAIN_CHISQ_STDEV 50.0 /* what chi-squared spread is labeled -hard- terrain */
	#define EASY_TERRAIN_CHISQ_IQR 0.25 /* what chi-squared spread is labeled -easy- terrain */
	#define HARD_TERRAIN_CHISQ_IQR 50.0 /* what chi-squared spread is labeled -hard- terrain */

	#define EASY_INIT_COUNT 1 /* 4 */
	#define MEDIUM_INIT_COUNT 1 /* 10 */
	#define HARD_INIT_COUNT 1 /* 20 */

	#define MAX_TRUE_INIT_COUNT 10 /* 10 */

	#endif
#endif

#else

#define DEFAULT_INIT_COUNT 1 /* default number of initiation attempts and fits */
#define MAX_INIT_COUNT 1 /* maximum number of iterations possible */
#define INITID_THRESH_STDEV_IQR 5 /* when should we start using IQR instead of STDEV to assess complexity of terrain */
#define EASY_TERRAIN_CHISQ_STDEV 0.5 /* what chi-square spread is labeled -easy- terrain */
#define HARD_TERRAIN_CHISQ_STDEV 100.0 /* what chi-squared spread is labeled -hard- terrain */
#define EASY_TERRAIN_CHISQ_IQR 0.5 /* what chi-squared spread is labeled -easy- terrain */
#define HARD_TERRAIN_CHISQ_IQR 100.0 /* what chi-squared spread is labeled -hard- terrain */

#define EASY_INIT_COUNT 1
#define MEDIUM_INIT_COUNT 1
#define HARD_INIT_COUNT 1

#define MAX_TRUE_INIT_COUNT 20

#endif


#define PARAMETER_DRAW_GAUSSIAN 0 /* random draws of the init parameter are chosen from a gaussian distribution if this flag is 1 */
#define DEFAULT_PARAMETER_TWEAK_COEFF 4.0 /* the standard deviation used in tweaking object parameters in multi-init fits is obtained by multiplying this number with the standard error obtained by PHOTO fit - we have only tested 1.0 so far. There might be a way to obtimized this in order to obtain the best fit with the smallest number of initiation attempts */

/* these two are mathematical constants that should not be hampered with */
#define TAU_MIN 1.0E-8
#define TAU_MAX 1.0

/* recording of LM's LP's LRZ's */
#define DO_LFIT_RECORD 1
#define DEFAULT_LFITRECORD (LM_RECORD_ALL | LP_RECORD_ALL)  /* (LM_RECORD_ALL | LP_RECORD_LASTOFBEST) */ /* (LM_RECORD_ALL | LP_RECORD_ALL) */ /* LM_RECORD_FIRSTANDLAST */ /* (LM_RECORD_ALL | LP_RECORD_ALL) */ /* LM_RECORD_ALLGOOD */ /* (LP_RECORD_LASTOFBEST | LM_RECORD_ALL)*/ /* LP_RECORD_LASTOFBEST | LM_RECORD_ALLGOOD */ /* (LP_RECORD_FIRSTANDLAST | LM_RECORD_ALLGOOD) */ /* which LMs to record, suggested: RECORD_ALLGOOD */
#define LFIT_RECORD_ALL (LP_RECORD_ALL | LM_RECORD_ALL) /* record all LM's and LP's - particularly useful for simulations to generate profiles */
#define LM_FIRST_RECORD_K 1
#define LFIT_NSTEP_DEFAULT 10000 /* the maximum size of array rcording LM structure */
#define LFIT_NPROFILE_DEFAULT 10000 /* the maximum size of array that contains LPROFILE structures for objects in the field */
#define LFIT_NPROFILE_FIRSTANDLAST 100 /* the maximum size of array containing LPROFILE structures for objects in the field if only the first and the last steps are taken */
#define LFIT_NPROFILE_LASTOFBEST 10 /* this is the maximum number of LRGs we think might be in one field. The experiment shows the real maximum is 3 or 4 */

/* recording PROFILEs during fit */
#define LP_MAKE_1D_PROF 0 /* do we really make 1D profiles of objects? this makes the whole process very slow */
#define NCOMPONENT 2 /* maximum number of components the object for which 1D profile is created is assumed to have */
#define PROFILENRAD 1000 /* maximum radius in pixel where for which 1D profile is created */
#define PROFILEMRAD 2000 /* NCOMPONENT x PROFILENRAD */


/* Fitmask package */
#define NMASK_FITOBJC 35 /* maximum number of fit masks for which object specific SSE's should be caclulated */
#define DEFAULT_NFITMASK 25 /* default number of objmasks inside the fitmask */
#define DEFAULT_STAR_FITRADIUS 32.0 /* the default radius of a fitmask made for a star */
/* PHOTO CONSTANTS */
#define DEFAULT_RPETRO_MASK_COEFF 20.0 /* object specific chi-squared is calculated in a mask this many times the Petrosian radius */

#define PIXARCSEC 0.39585 /* width of a pixel in arcsec */
#define ARCSECPIX 2.5263  /* number of pixels in an arcsec */

#define UNKNOWN_CAMCOL -1
#define DEFAULT_CAMCOL UNKNOWN_CAMCOL

#define UNKNOWN_BAND -1
#define DEFAULT_BAND UNKNOWN_BAND
#define U_BAND 0
#define G_BAND 1
#define R_BAND 2
#define I_BAND 3
#define Z_BAND 4

#define UNKNOWN_BNDEX -1 /* used to map the contents of phobjc files */

#define U_ROW 3
#define G_ROW 4
#define R_ROW 1
#define I_ROW 2
#define Z_ROW 5
#define UNKNOWN_CAMROW -1
#define DEFAULT_CAMROW R_ROW

#define UNKNOWN_RUN -1
#define DEFAULT_RUN UNKNOWN_RUN

#define UNKNOWN_RERUN -1
#define DEFAULT_RERUN 137

#define UNKNOWN_FIELD -1
#define DEFAULT_FIELD UNKNOWN_FIELD

#define UNKNOWN_STRIPE -1
#define DEFAULT_STRIPE UNKNOWN_STRIPE

/* Calibration */
/*
	b  		m(f=0) 	m(10f0) 
u 	1.4 × 10-10	24.63	22.12
g 	0.9 × 10-10	25.11	22.60
r 	1.2 × 10-10	24.80	22.29
i 	1.8 × 10-10	24.36	21.85
z 	7.4 × 10-10	22.83	20.32

*/

#define DEFAULT_CALIBTYPE CRUDECALIB /* the default calibration method */
#define USE_CALIBPHOTOMGLOBAL 0 /* use calibPhotomGlibal-$camcol-$run.fits files generated by Blanton for calibration */
#if USE_CALIBPHOTOMGLOBAL /* nano-mag used instead of mag */

#define B_U 1.4E-4;
#define B_G 0.9E-4;
#define B_R 1.2E-4;
#define B_I 1.8E-4;
#define B_Z 7.4E-4;

#else 

#define B_U 1.4E-10;
#define B_G 0.9E-10;
#define B_R 1.2E-10;
#define B_I 1.8E-10;
#define B_Z 7.4E-10;

#endif

#define F0_U 1.0
#define F0_G 1.0
#define F0_R 1.0
#define F0_I 1.0
#define F0_Z 1.0

#define NANN2 15
#define NANN3 8

#define CALIB_PHOBJC_IO_USE_ALTERED_FITS_PACKAGE 1 /* default = 1; because sweep files are created by IDL Robert's FITS readers cannot read them due to the different expectation of the order of keywords in the header */
#define CALIB_PHOBJC_IO_CHAR_ARRAY 1 /* if set to 1 then uses static arrays for objcid etc in the relevant structure */
#define MATHFL double /* used for doing calculation inside of Calib package */
#define MATHDBLE double /* used for matrix inversion */ 
#define NBETA 20 /* used in GALACTIC_CALIB objects as regressions parameters */
#define GC_POLY_DEG 3 /* used for regression of galactic extinctions values */
#define DIRECT_MULTIPLY_INVERSE 0 /* while LR decomposition was used to calculate inverses, use direct multiplication to solve a linear system of equations after finding the inverse matrix - this is in hope of avoiding NAN's */
#define QGAMMA_INV_A_SMALL 0.4 /* the value of a below which we suspect the asymptotic expansion of inverse of q(a, x) fails */
#define QGAMMA_INV_Q_SMALL 0.1 /* the smallest value of q for which a non-asymptotic formula can be used */
#define QGAMMA_INV_X_DEFAULT 5.0 /* the value of x which will be returned when value of a < QGAMMA_INV_A_SMALL */

/* simulation and fit report */
#define MAX_SIM_NSTAR 10000 /* max number of stars in simulation */
#define MAX_SIM_NGALAXY 10000 /* max number of galaxies in simulation */
#define MAX_SIM_NLRG 10000 /* max number of galaxies in simulation */
#define DEFAULT_SIM_NSTAR 10000 /* default number of stars in simulation */
#define DEFAULT_SIM_NGALAXY 10000 /* default number of galaxies in simulation */
#define DEFAULT_SIM_STAR_THRESHOLD 22.2 /* default maximum magnitude for the simulated star */
#define DEFAULT_SIM_GALAXY_THRESHOLD 22.2 /* default maximum magnitude for the simulated galaxies */
#define DEFAULT_SIM_DEV_PURITY_THRESHOLD -0.99 /* default min frac_deV for the simulated galaxies */
#define DEFAULT_SIM_EXP_PURITY_THRESHOLD -0.99 /* default min frac_exp for the simulated galaxies, bigger than one means exclusion of all exp models  */
#define RESFL long double /* used in compiling the residual report */
#define DEFAULT_SIMFLAG NOSIMFORFINALFIT
#define DEFAULT_SIM_FPC_FLOAT 1

/* LRG package */
#define DEFAULT_C_LRG 0.7
#define LRG_BINNED ((int) (OBJECT1_BINNED1 | OBJECT1_BINNED2 | OBJECT1_BINNED4))
#define LRG_TOO_BRIGHT ((int) (OBJECT1_SATUR | OBJECT1_BRIGHT))

/* simulation constants */
#define CD_R_RATIO0 1.01 /* the extents of the radial ratio of deV1 and deV2 profiles of cD candidates for random secondary halos */
#define CD_R_RATIO1 1000.0

#define CD_B_RATIO0 0.99 /* the extents of brightness ratio of deV1 and deV2 profiles of cD candidates for random secondary halos */
#define CD_B_RATIO1 1.0E-6

#define CD_F_RATIO0 1.0E6 /* the extents of flux ratio of deV1 and deV2 profiles of cD candidates for random secondary halos */
#define CD_F_RATIO1 1.0E-6

#define CD_R_MAX 750.0 /* active flag - the maximum radius of cD halo in pixels created by simulations */
#define CD_M_MAX 25.0  /* inactive - the biggest (least luminous) magnitude of the cD-halo created by simulations */ 
#define CD_MU_MAX 500.0 /* inactive - the biggest (least luminous) surface magnitude of the cD halo within its half light radius used in simulations */

#define CD_RCORE_LOGNORMAL_TAIL 1 /* default = 1, this chooses between two possible treatmenst of large core radii in simulation */
#define CD_RCORE_RANDOMIZE_THRESH 74.0 /* the smallest core radius for which the radius is manipulated to avoid accumulation of radii in ~75 pixels dictated by PHOTO limits */
#define CD_RCORE_RANDOMIZE_NSIGMA 1.0 /* number of sigmas for deviation, used when CD_RCORE_LOGNORMAL_TAIL = 0 */
#define CD_RCORE_RANDOMIZE_LOGSIGMA 0.45 /* the standard deviation in log normal distribution for r_core for radii ~  CD_RCORE_RANDOMIZE_THRESH used when CD_RCORE_LOGNORMAL_TAIL = 1 */

#define CD_NTRY_MAX 1000000 /* the maximum number of rejections during random creation of halos before code fails */

/* parse package */
#define NO_DEMAND_CODE -1 /* the demand code when nothing is demanded and object list are read from fpObjc and fpBIN files */
#define DEMAND_PAR_ERR_ADJUSTMENT 10.0
typedef enum run_mode {
	MLE_MODE, MLE_MODE_PLUS, SIM_MODE, MLE_ON_DEMAND_MODE, N_RUN_MODE, UNKNOWN_RUN_MODE
	} RUN_MODE; 	/* pragma SCHEMA */
 
/* main-mle-lrg */
#define THRESHOLD_OBJCLIST_PRINT 20 /* if objclist is longer than this, do not print */
#endif
