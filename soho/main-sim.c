/* PHOTO libraries */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ftcl.h"
#include "dervish.h"
#include "strings.h"
#include "time.h"

#include "phUtils.h"

#include "phPhotoFitsIO.h"
#include "phSpanUtil.h"

#include "thMath.h" 
#include "thDebug.h" 
#include "thIo.h" 
#include "thAscii.h" 
#include "thProc.h" 
#include "thMask.h" 
#include "thMSky.h" 
#include "thObjcTypes.h"
#include "thMGalaxy.h"
#include "thAlgorithm.h"
#include "thMap.h"
#include "thMle.h"
#include "thResidual.h"
#include "thParse.h"
#include "thCalib.h"

typedef enum tweakflag {
	ABSOLUTE, FRACTIONAL, N_TWEAKFLAG, UNKNOWN_TWEAKFLAG
	} TWEAKFLAG;

#define SIM_SKY 1
#define ADD_POISSON_NOISE 1
#define DO_BKGRND 0
#define FIT_SHAPE  1
#define FIT_RE 1
#define FIT_E 0
#define FIT_PHI 0
#define FIT_CENTER 0 
#define LOCK_CENTER 1
#define FORCE_INIT_PAR 0
#define STAR_CLASSIFICATION 1 /* testing star classification */
#define GALAXY_CLASSIFICATION 1 /* testing galaxy classification */	
#define TABLEROW 5
#define N_RANDOM_FRAMES 2
#define N_OUTPUT_OBJC 5
#define FIXED_SKY 100.0
#define I_SKY 1.0
#define EllDesc SUPERCANONICAL
#define NSIMOBJC 3000
#define NFITOBJC 1
#define TWEAK 1
#define TWEAK_ALL 0
#define THIS_BAND R_BAND
/* image clustering and memory constants */
#define THMEMORY 1300
#define DO_GREEDY 1
#define DO_ALGORITHM 1
#define ALGORITHM_MANUAL 0

int verbose = 0;

static int band = 0;
static THPIX SKY_FRACTIONAL_TWEAK = 0.5;
#if FORCE_INIT_PAR
static THPIX I_TWEAK = 0.1; 
static THPIX CENTER_TWEAK = 1.0;
static THPIX SHAPE_TWEAK = 1.0;
static THPIX RE_TWEAK = 1.0;
static THPIX PHI_TWEAK = 1.0;
static THPIX E_TWEAK = 1.0;
#else 
#if TWEAK

static THPIX I_TWEAK = 1; 
#if FIT_CENTER
static THPIX CENTER_TWEAK = 1.0;
#endif
#if FIT_SHAPE
static THPIX SHAPE_TWEAK = 1.0;
#if FIT_PHI
static THPIX PHI_TWEAK = 1.0;
#endif
#if FIT_E
static THPIX E_TWEAK = 1.0;
#endif
#endif

#endif
#endif

static THPIX BETA_SHAPE = 0.3;
#if FORCE_INIT_PAR
static THPIX ELLIPTICITY = 0.7;
static THPIX PHI = 1.0;
static THPIX RE = 10.0;
static THPIX STRETCHINESS = 1.0;
static THPIX INTENSITY = 50;
#endif

static void usage(void);
void chain_model_output(char *job, CHAIN *objclist, THOBJCTYPE otype);
void model_output(char *job, THOBJC *objc);
int compare_re(const void *x1, const void *x2);
int compare_count(const void *x1, const void *x2);
void test_sorted_chain(CHAIN *chain, int (*compare)(const void *, const void *));
extern double ddot_(int *, double [], int *, double [], int *);
extern float sdot_(int *, float [], int *, float [], int *);

static RET_CODE tweak(void *p, THPIX e_tweak, char *pname, char **rnames, int rcount, CHAIN *chain, TWEAKFLAG tflag);
static RET_CODE add_poisson_noise_to_image(REGION *im, CHAIN *ampl);
static RET_CODE tweak_init_pars(LSTRUCT *l, CHAIN *objclist);

int
main(int ac, char *av[])
{

  char *name = "do-sim";
printf("%s: starting to run '%s' \n", name, name);

  /* Arguments */

SIM_PARSED_INPUT *input = thSimParsedInputNew();
simparser(ac, av, input);

printf("%s: *** input *** \n", name);
printf("%s: setupfile = %s\n", name, input->setupfile);
printf("%s: camcol    = %d\n", name, input->camcol);
printf("%s: band      = %d\n", name, input->band);
printf("%s: objcfile  = %s\n", name, input->objcfile);
printf("%s: framefile = %s\n", name, input->framefile);
printf("%s: smear     = %d\n", name, input->smearobjc);
printf("%s: skyobjc   = %p\n", name, input->sky);
printf("%s: objclist  = %p\n", name, input->objclist); 
printf("%s: *** end of input *** \n", name);


char *fname;
int noobjcfile = 0, readskyfile = 0, nrow, ncol, camcol, camrow, band, run, rerun, stripe, field;
unsigned int seed;
noobjcfile = (input->readobjcfile == 0);
readskyfile = input->readskyfile;
nrow = input->nrow;
ncol = input->ncol;
camcol = input->camcol;
band = input->band;
run = input->run;
rerun = input->rerun;
stripe = input->stripe;
field = input->field;
fname = input->framefile;
seed = input->seed;

RET_CODE status;
status = thCamrowGetFromBand(band, &camrow);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not retrieve camrow", name);
	return(-1);
}

CHAIN *myobjclist = input->objclist;
SKYPARS *mysky = input->sky;

printf("%s: initiating modules \n", name);

 printf("%s: initiating IO \n", name); 
 thInitIo();
 printf("%s: initiating Tank \n", name);
  thTankInit();
 printf("%s: initiating Ascii \n", name);
  thAsciiInit();  
 printf("%s: initiating SpanObjmask \n", name);
  initSpanObjmask();

FRAMEFILES *ff;
CHAIN *fchain;
char *strutype = "FRAMEFILES";	

if (input->ff != NULL) {
	fchain = shChainNew(strutype);
	shChainElementAddByPos(fchain, input->ff, strutype, TAIL, AFTER);
} else {
	printf("%s: reading framefile \n", name);
	  /* get the related schema */
  	ASCIISCHEMA *schema = NULL;
  	schema = thTankSchemaGetByType(strutype);
  	if (schema == NULL) {
    		printf("%s: No schema exists for structure %s", name, strutype);
  	}
  	/* get the structure constructor */
  	void *(*strunew)();
 	strunew = schema->constructor;
  	if (strunew == NULL) {
    		printf("%s: structure constructor for %s not available in tank", name, strutype);
  	}
  	/* construct a structure */
  	void *stru;
  	stru = (*strunew)();
  
	ff = (FRAMEFILES *) stru;

  	FILE *fil;
  	fil = fopen(fname, "r");
	if (fil == NULL) {
		thError("%s: ERROR - could not read framefile '%s'", name, fname);
		return(-1);
	}  
  	fchain = thAsciiChainRead(fil);
}

if (shChainSize(fchain) == 0) {
	thError("%s: no structures was properly read", name);
	}
  printf("%s: (%d) pieces of frame info read \n",
	 name, shChainSize(fchain));

  ff = (FRAMEFILES *) shChainElementGetByPos(fchain, 0);
  if (noobjcfile) strcpy(ff->phobjcfile, "");

  if (ff == NULL) {
    thError("%s: NULL returned from chain - check source code", name);
  } else {    
    /* fields of the ff structure refer to outbound memory locations */
    printf("phconfigfile = %s \n", ff->phconfigfile);
    printf("phecalibfile = %s \n", ff->phecalibfile);
    printf("phflatframefile = %s \n", ff->phflatframefile);
    printf("phmaskfile = %s \n", ff->phmaskfile);
    printf("phsmfile = %s \n", ff->phsmfile);
    printf("phpsfile = %s \n", ff->phpsfile);
    printf("phobjcfile = %s \n", ff->phobjcfile);
    printf("thsbfile = %s \n", ff->thsbfile);
    printf("thobfile = %s \n", ff->thobfile);
    printf("thmakselfile = %s \n", ff->thmaskselfile);
    printf("thobjcselfile = %s \n", ff->thobjcselfile);
    printf("thsmfile = %s \n", ff->thsmfile);
    printf("thpsfile = %s \n", ff->thpsfile);
    printf("thobjcfile = %s \n", ff->thobjcfile);
    printf("thflatframefile = %s \n", ff->thflatframefile);

  }

	char *simfile = ff->phflatframefile;
	char *simodelfile = ff->thflatframefile;

	printf("%s: initiating random numbers \n", name);
	RANDOM *rand;
	char *randstr;
	int nrandom;
	randstr = thCalloc(MX_STRING_LEN, sizeof(char));
	nrandom = (int) (nrow * ncol * (float) N_RANDOM_FRAMES);
	sprintf(randstr, "%d:2", nrandom);
	printf("%s: generating %d random numbers (random frames= %g) \n", name, nrandom, (float) N_RANDOM_FRAMES);
	rand = phRandomNew(randstr, 1);
	printf("%s: setting seed to %u \n", name, seed);
	unsigned int seed0 = phRandomSeedSet(rand, seed);
	printf("%s: old seed was %u \n", name, seed0);
	
  printf("%s: process and frame \n", name);
  PROCESS *proc;
  proc = thProcessNew();
  int i;
  int bndex[NCOLOR];
  for (i = 0; i < NCOLOR; i++) {
	bndex[i] = i;
	}
  FRAME *frame;
  REGION *im;
  
  frame = thFrameNew();
      if (frame->files != NULL) {
	thFramefilesDel(frame->files);
      }
      
      frame->files = ff; 
      frame->proc = proc;
      
      printf("%s: loading fpC file\n", name);
      thFrameLoadImage(frame);
      /* this part is only used during simulation */
      frame->data->image = shRegNew("simulated image", nrow, ncol, TYPE_THPIX);
      im = frame->data->image;

      /* note that because this is only a simulation no fpC file exists prior to this run */
	frame->id->camcol = camcol;
	frame->id->camrow = camrow;
	frame->id->nrow = nrow;
	frame->id->ncol = ncol;
	frame->id->rerun = rerun;
	frame->id->run = run;
	frame->id->field = field;
	frame->id->filter = band;

      printf("%s: loading amplifier information\n", name);
      thFrameLoadAmpl(frame); 

	/* no need to read the mask since we are gonna work with a fake sky only frame 
      printf("%s: loading PHOTO masks\n", name);
      thFrameLoadPhMask(frame);
	*/
	/* testing with PHOTO calibration data */
	printf("%s: loading PHCALIB \n", name);
	thFrameLoadCalib(frame);
	/* testing with SDSS objects */
      printf("%s: loading OBJC's \n", name);
      if (noobjcfile == 0) {
	printf("%s: reading fpObjc file \n", name);
	status = thFrameLoadPhObjc(frame);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not load fpObjc file", name);
		return(-1);
	}
      }
	if (myobjclist != NULL && shChainSize(myobjclist) != 0) {
      		printf("%s: adding objects to the list manually \n", name);
      		thFrameAddObjcChain(frame, myobjclist);
	}
	if (readskyfile == 1) {
		SKYPARS  *sky;
		if (input->sky == NULL) {
			input->sky = thSkyparsNew();
		}
		sky = input->sky;
		status = thFrameLoadPhSky(frame, sky);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not load PHOTO sky estimate", name);
			return(-1);
		}
	}
	/* testing PSF at this stage */ 

      printf("%s: loading PHOTO psf \n", name);
      status = thFrameLoadPhPsf(frame);
      if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not load PHOTO PSF", name);
		return(-1);
	}
      printf("%s: reshaping into SOHO psf \n", name);
      status = thFrameLoadThPsf(frame);
      if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not reshape PSF", name);
		return(-1);
	}
	/* should skip poisson weights since there is no fpC file loaded 
     printf("%s: loading weight matrix -- space saver \n", name); 
      thFrameLoadWeight(frame);
	*/

	/* mask is null at this point for this test */
      printf("%s: loading SOHO masks \n", name);
      thFrameLoadThMask(frame);

	/* PERFORMING FIT TO THE FAKE FRAME */
	/* initializing model and object bank */
	thModelInit();
	/* introducing sky to the object bank */
	printf("%s: initiating all models and object types  ... \n", name);
	printf("%s: initiating sky model and object type\n", name);
	thSkyObjcInit(nrow, ncol, frame->data->ampl, &g0sky);
	printf("%s: initiating other models \n", name);
	status = MGModelsInit();
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object models", name);
		return(-1);
	}
	int nm_max = 2;
	char *mname, **mnames = thCalloc(nm_max, sizeof(char *));
	for (i = 0; i < nm_max; i++) {
		mnames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
	} 
	mname = mnames[0];
	#if STAR_CLASSIFICATION
	strcpy(mnames[0], "star");
	printf("%s: initiating object type 'STAR' \n", name);
	status = MStarObjcInit(mnames[0]);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object type", name);
		return(-1);
	}
	#endif 

	#if CDCANDIDATE_CLASSIFICATION 
	strcpy(mnames[0], "deV");
	strcpy(mnames[1], "Exp2");
	printf("%s: initiating object type 'CDCANDIDATE' \n", name);
	printf("%s: adding two models '%s', '%s' \n", name, mnames[0], mnames[1]);
	status = McDCandidateObjcInit(mnames, 2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object type", name);
		return(-1);
	}
	#endif

	#if GALAXY_CLASSIFICATION

	strcpy(mnames[0], "deV");
	strcpy(mnames[1], "Exp");
	printf("%s: initiating object type 'GALAXY' \n", name);
	printf("%s: adding two models '%s', '%s' \n", name, mnames[0], mnames[1]);
	status = MGalaxyObjcInit(mnames, 2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object type", name);
		return(-1);
	}

	#endif

	int nsimobjc = 0;

	printf("%s: models and object types initiated \n", name);	
	/* define THOBJC with type SKY
	   construct a MAP
	*/




	THOBJCID id = 0;
	THOBJC *objc = NULL;
	CHAIN *objclist = NULL;
	objclist = shChainNew("THOBJC");

	OBJC_IO *phobjc;
	CHAIN *phobjcs;
	int iobjc, nobjc = 0;	

	phobjcs = frame->data->phobjc;
	status = thFrameGetPhObjcs(frame, &phobjcs, &bndex);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (phobjc chain) and (bndex array) from (frame)", name);
		return(-1);
	}
	if (phobjcs != NULL) nobjc = shChainSize(phobjcs);
	printf("%s: (%d) objects read from SDSS file \n", name, nobjc);


	MAPMACHINE *map;
#if 1 /* debug nov 5, 2012 */
	printf("%s: loading models into (mapmachine) \n", name);
	map = thMapmachineNew();
	/* inserting map into frame */
	frame->work->map = map;

	/* add all the models in the object to the map */
	char **rnames, **iornames;
	int nrname = 10;
	rnames = thCalloc(nrname, sizeof(char*));
	iornames = thCalloc(nrname, sizeof(char *));
	for (i = 0; i < nrname; i++) {
		rnames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
		iornames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
	}
	LSTRUCT *l;
	LWORK *lwork;

	REGION *simreg = NULL;

	int pos = 0;
	char *ioname;
	TYPE iotype;
	SKYPARS *ps;

	/* introducing sky to the simulated set of objects  */
	#define DO_SKY 1
	#if DO_SKY
	printf("%s: creating sky object \n", name);
	objc = thSkyObjcNew();
	if (objc == NULL) {
		thError("%s: ERROR - could no create object sample", name);
		return(-1);
	}
	printf("%s: loading sky object onto (mapmachine) \n", name);
	thMAddObjcAllModels(map, objc, band, SEPARATE);
	objc->thid = id++;
	shChainElementAddByPos(objclist, objc, "THOBJC", TAIL, AFTER);

	#if SIM_SKY

	printf("%s: uploading sky simulation parameters \n", name);
	status = thMCompile(map);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not compile the map", name);
		return(-1);
	}	
	/* constructing LSTRUCT */
	printf("%s: constructing likelihood data structure (lstruct) \n", name);
 	status = thFrameLoadLstruct(frame, NULL, DEFAULT_CALIBTYPE, DEFAULT_LFITRECORD);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not load (lstruct)", name);
		return(-1);
	}
	l = frame->work->lstruct;
	l->lmodel->lmachine = map;
	lwork = l->lwork;

	/* simulating the sky */
	if (mysky != NULL) {
		printf("%s: sky simulation \n", name); 
		objc = shChainElementGetByPos(objclist, pos);
		thObjcGetIopar(objc, (void **) &ps, &iotype);
		ioname = shNameGetFromType(iotype);	
		if (ps != NULL && !strcmp(ioname, "SKYPARS")) {	
			printf("%s: setting simulation values for sky in io-par \n", name);	
			thqqTrans(mysky, ioname, ps, ioname);
		} else {
			printf("%s: WARNING - null parameter in object (objc: %d) \n", name, objc->thid);
		}
	}

	thLDumpAmp(l, IOTOX);
	thLDumpPar(l, IOTOX);
	thLDumpPar(l, IOTOXN);

	thLDumpAmp(l, IOTOMODEL);
	thLDumpPar(l, IOTOMODEL);
	thLDumpPar(l, IOTOMODEL);

	#if DO_BKGRND

	printf("%s: simulating sky and saving as backgroundi \n", name);
	printf("%s: 1. memory estimates \n", name); 
	status = MakeImageAndMatrices(l, MEMORYESTIMATE);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get memory estimate for the model", name);
		return(-1);
	}
	printf("%s: 2. init images \n", name);
	status = MakeImageAndMatrices(l, INITIMAGE); /* MODELONLY */
   	if (status != SH_SUCCESS) {
		thError("%s: could not simulate the basis models for sky", name);
		return(-1);
	}
	status = thMakeM(l);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add up models to create sky", name);
		return(-1);
	}
	simreg = l->lwork->mN;
	bkgrnd = shRegNew("background image - sky", simreg->nrow, simreg->ncol, simreg->type);
	thRegPixCopy(simreg, bkgrnd);

	printf("%s: sky constructed and stored", name);
	#endif

	#endif

	#endif
#endif

	printf("%s: proceeding to the rest of the objects ... \n", name);
	phobjcs = frame->data->phobjc;
	nobjc = shChainSize(phobjcs);
	printf("%s: (%d) objects read from SDSS file \n", name, nobjc);

	/* introducing a galaxy */

	/* 
	printf("%s: simulating objects as '%s' \n", name, sim_name);
	*/
	printf("%s: sorting objects \n", name);
	shChainQsort(phobjcs, &compare_re);	
	test_sorted_chain(phobjcs, &compare_re);

	int ndeleted = 0;
	for (iobjc = 0; iobjc < nobjc; iobjc++) {

		/* initiating the object */
		phobjc = shChainElementGetByPos(phobjcs, iobjc);
		PHPROPS *phprop = phPropsNew();
		PHCALIB *calibData;
		calibData = frame->data->phcalib;
		status = thPhpropsPut(phprop, phobjc, bndex, NULL, NULL);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not put object information in 'PHPROP'", name);
			return(-1);
		} 
		objc = thObjcNewFromPhprop(phprop, band, CLASSIFY, NULL);
		if (objc == NULL) {
			thError("%s: ERROR - could not create object for object (%d) in the sdss list", name, iobjc);
			return(-1);
		}
		objc->thid = (THOBJCID) phobjc->id;
		THOBJCTYPE objctype;
		char *objcname;
		objctype = objc->thobjctype;
		status = thObjcNameGetFromType(objctype, &objcname);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get type name from object type (iobjc = %d)", name, iobjc);
			return(-1);
		}
		if (objctype != SKY_OBJC && objctype != PHOTO_OBJC) {
			/* one should create the ioprop of the object after it is properly classified */
 	
			status = thObjcInitMpars(objc);
			if (status != SH_SUCCESS) {
				char *tname = NULL;
				thObjcNameGetFromType(objc->thobjctype, &tname);
				thError("%s: ERROR - could not initiate model parameters for (objc: %d) of class '%s'", 
				name, objc->thid, tname);
				return(-1);
			}

			shChainElementAddByPos(objclist, objc, "THOBJC", TAIL, AFTER);		
			thMAddObjcAllModels(map, objc, band, SEPARATE);
			nsimobjc++;
			if (nsimobjc == NSIMOBJC) break;
		
		} else if (objctype == PHOTO_OBJC) {
			printf("%s: deleting unclassified object ... \n", name);
			ndeleted++; 
			thObjcDel(objc);
		}	


	}

	printf("%s: (%d) objects deleted due to lack of classification \n", name, ndeleted);

	printf("%s: compiling map ... \n", name);
	thMCompile(map);
	printf("%s: outputting (mapmachine) info \n", name); 
	/* 
	thMInfoPrint(map, EXTENSIVE);
	*/
	printf("%s: n(model) = %d, n(par) = %d, n(objc) = %d \n", name, map->namp, map->npar, map->nobjc);
	
	nobjc = shChainSize(objclist);
	printf("%s: total of (%d) objects added to (mapmachine) \n", name, nobjc);
	if (nobjc <= 1) {
		thError("%s: ERROR - no objects loaded from SDSS", name);
		return(-1);
	}
	int ii, j = 0;
	ii = j;
	strcpy(rnames[j++], "I");
	#if FIT_SHAPE 
	int ishape, jshape, nshape;
	ishape = j;
	if (EllDesc == ALGEBRAIC) {
		strcpy(rnames[j++], "a");
 		strcpy(rnames[j++], "b");
		strcpy(rnames[j++], "c");
	} else if (EllDesc == CANONICAL) {
		#if FIT_RE
		strcpy(rnames[j++], "re");
		#endif
		#if FIT_E
		strcpy(rnames[j++], "e");
		#endif
		#if FIT_PHI
		strcpy(rnames[j++], "phi");
		#endif
	} else if (EllDesc == SUPERCANONICAL || EllDesc == UBERCANONICAL) {
		#if FIT_RE
		strcpy(rnames[j++], "re"); 
		#endif
		#if FIT_E
		strcpy(rnames[j++], "E");
		#endif
		#if FIT_PHI
		strcpy(rnames[j++], "phi");
		#endif
	}	
	jshape = j;
	nshape = j;
	#endif
	#if FIT_CENTER
	int icenter, jcenter, ncenter;
	icenter = j;
	strcpy(rnames[j++], "xc");  
	strcpy(rnames[j++], "yc");
	jcenter = j;
	ncenter = (jcenter - icenter);
	#endif
	
	int k, kmax, kfit = 0;
	if (j > 1) { 
	printf("%s: adding variables to nonlinear fit (variables/model = %d)\n", 
		name, j - 1);
	kmax = MIN(NFITOBJC, shChainSize(objclist));
	printf("%s: number of objects for nonlinear fit = %d \n", name, kmax); 
	for (k = 0; k < kmax; k++) {

	THOBJCTYPE objctype;
	char *objcname;
	objc = shChainElementGetByPos(objclist, k);
	objctype = objc->thobjctype;
	thObjcNameGetFromType(objctype, &objcname);
	if (!strcmp(objcname, "DEVGALAXY") || 
		!strcmp(objcname, "EXPGALAXY") || 
		!strcmp(objcname, "GAUSSIAN") || 
		!strcmp(objcname, "GALAXY") || 
		!strcmp(objcname, "CDCANDIDATE")) {
	
		kfit++;
		if (!strcmp(objcname, "DEVGALAXY")) strcpy(mnames[0], "deV");
		if (!strcmp(objcname, "EXPGALAXY")) strcpy(mnames[0], "Exp");
		if (!strcmp(objcname, "GAUSSIAN")) strcpy(mnames[0], "gaussian");
		if (!strcmp(objcname, "GALAXY")) {
			strcpy(mnames[0], "deV");
			strcpy(mnames[1], "Exp");
		}
		if (!strcmp(objcname, "CDCANDIDATE")) {
			strcpy(mnames[0], "deV");
			strcpy(mnames[1], "Exp2");
		}	
	
		/* adding non-linear fit jobs */
		if (j > 1) {
		mname = mnames[0];	
		status = thMClaimObjcVar(map, objc, mname, rnames + 1, j - 1);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not add model '%s' in (objc: %d) to fit", name, mname, objc->thid);
			return(-1);	
		}
		mname = mnames[1];
		if (!strcmp(objcname, "GALAXY") || !strcmp(objcname, "CDCANDIDATE")) {
		status = thMClaimObjcVar(map, objc, mname, rnames + 1, j - 1);	
		if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add model '%s' in (objc: %d) to fit", name, mname, objc->thid);
		return(-1);	
		}
		}
		#if FIT_CENTER
		#if LOCK_CENTER
		if (!strcmp(objcname, "GALAXY") || !strcmp(objcname, "CDCANDIDATE")) {
		status = thMEquivObjcVars(map, objc, mnames[0], rnames + icenter, objc, mnames[1], rnames + icenter, ncenter);
		}	
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not claim centers as equiv", name);
			return(-1);
		}
		#endif
		#endif
		}
		printf("%s: (objc: %d) ", name, objc->thid);
		for (i = 0; i < j; i++) {
			printf(" '%s' ", rnames[i]);
		}
		printf("\n");
		}
	}
	} else {
		printf("%s: no nonlinear component to the fit", name);
	}
	/* compiling map */	
	printf("%s: compiling (mapmachine) \n", name);
	thMCompile(map);
 
	printf("%s: outputting (mapmachine) info \n", name); 
	/* 
	thMInfoPrint(map, EXTENSIVE);
	*/
	printf("%s: n(model) = %d, n(par) = %d, n(objc) = %d \n", name, map->namp, map->npar, map->nobjc);
	/* constructing LSTRUCT */
	printf("%s: constructing likelihood data structure (lstruct) \n", name);
 	status = thFrameLoadLstruct(frame, NULL, DEFAULT_CALIBTYPE, DEFAULT_LFITRECORD);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not load (lstruct)", name);
		return(-1);
	}
	status = thFrameGetLstruct(frame, &l);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (lstruct) out of (frame)", name);
		return(-1);
	}
	status = thLstructPutLmachine(l, map);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (lmachine) out of (lstruct)", name);
		return(-1);
	}
	status = thLstructGetLwork(l, &lwork);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (lwork) out of (lstruct)", name);
		return(-1);
	}
	
	/* simulating the galaxy */ 
	pos++;	
	#if FORCE_INIT_PAR	
	OBJCPARS *pg;
	const float inorm = 1.0/(float)((1U<<(8*sizeof(int)-1)) - 1);
	printf("%s: setting random simulation values for objects \n", name);
	for (pos = 0; pos < shChainSize(objclist); pos++) {

	THOBJCTYPE objctype;
	char *objcname;
	objc = shChainElementGetByPos(objclist, pos);
	objctype = objc->thobjctype;
	thObjcNameGetFromType(objctype, &objcname);
	if (!strcmp(objcname, "GALAXY") || !strcmp(objcname, "DEVGALAXY") || !strcmp(objcname, "EXPGALAXY") || !strcmp(objcname, "GAUSSIAN")) {
	thObjcGetIopar(objc, (void **) &pg, NULL);
	if (pg != NULL) {
	if (!strcmp(objcname, "GAUSSIAN")) {
		pg->re_gaussian = RE;
		pg->I_gaussian= INTENSITY;
		pg->xc_gaussian = nrow / 2.0 + nrow /4.0 * phRandom();
		pg->yc_gaussian = ncol / 2.0 + ncol / 4.0 * phRandom();
		pg->a_gaussian = 0.5 / pow(pg->re_gaussian, 2);
		pg->c_gaussian = 0.5 / pow(pg->re_gaussian, 2);
		pg->b_gaussian = sqrt(pg->a_gaussian * pg->c_gaussian) * BETA_SHAPE;
		pg->e_gaussian = ELLIPTICITY;
		pg->phi_gaussian = PHI;
		pg->E_gaussian = STRETCHINESS;
	} else if (!strcmp(objcname, "DEVGALAXY")){
		printf("%s: randomly defining DEVGALAXY\n", name);
		pg->re_deV = RE * fabs((FL32) phGaussdev());
		pg->I_deV = INTENSITY * fabs((FL32) phGaussdev());
		pg->xc_deV= nrow / 2.0 * (1.0 + inorm * (FL32) phRandom());
		pg->yc_deV = ncol / 2.0 * (1.0 + inorm * (FL32) phRandom());
		/* 
		pg->a_deV = 0.5 / pow(pg->re_deV, 2);
		pg->c_deV = 0.5 / pow(pg->re_deV, 2);
		pg->b_deV = sqrt(pg->a_deV * pg->c_deV) * BETA_SHAPE * phGaussdev();
		*/ 
		pg->e_deV = ELLIPTICITY * fabs((FL32) phGaussdev());;
		pg->phi_deV = PI / 2.0 * (FL32) phGaussdev();
		pg->E_deV = STRETCHINESS * fabs((FL32) phGaussdev());
	}  else if (!strcmp(objcname, "EXPGALAXY")){
		printf("%s: randomly defining EXPGALAXY \n", name);
		pg->re_Exp = RE * fabs((THPIX) phGaussdev());
		pg->I_Exp = INTENSITY * fabs((THPIX) phGaussdev());
		pg->xc_Exp= nrow / 2.0 * (1.0 + inorm * (FL32) phRandom());
		pg->yc_Exp = ncol / 2.0 * (1.0 + inorm * (FL32) phRandom());
		/* 
		pg->a_deV = 0.5 / pow(pg->re_deV, 2);
		pg->c_deV = 0.5 / pow(pg->re_deV, 2);
		pg->b_deV = sqrt(pg->a_deV * pg->c_deV) * BETA_SHAPE * phGaussdev();
		*/ 
		pg->e_Exp = ELLIPTICITY * (1.0 + inorm * (FL32) phRandom()) / 2.0;
		pg->phi_Exp = PI / 2.0 * (THPIX) phGaussdev();
		pg->E_Exp = STRETCHINESS * fabs((THPIX) phGaussdev());
	} else if (!strcmp(objcname, "GALAXY")) {
		printf("%s: randomly defining GALAXY\n", name);
		pg->re_deV = RE * fabs((FL32) phGaussdev());
		pg->I_deV = INTENSITY * fabs((FL32) phGaussdev());
		pg->xc_deV= nrow / 2.0 * (1.0 + inorm * (FL32) phRandom());
		pg->yc_deV = ncol / 2.0 * (1.0 + inorm * (FL32) phRandom());
		/* 
		pg->a_deV = 0.5 / pow(pg->re_deV, 2);
		pg->c_deV = 0.5 / pow(pg->re_deV, 2);
		pg->b_deV = sqrt(pg->a_deV * pg->c_deV) * BETA_SHAPE * phGaussdev();
		*/ 
		pg->e_deV = ELLIPTICITY * (1.0 + inorm * (FL32) phRandom()) / 2.0;
		pg->phi_deV = PI / 2.0 * (FL32) phGaussdev();
		pg->E_deV = STRETCHINESS * fabs((FL32) phGaussdev());
		
		pg->re_Exp = RE * fabs((THPIX) phGaussdev());
		pg->I_Exp = INTENSITY * fabs((THPIX) phGaussdev());
		pg->xc_Exp= pg->xc_deV;
		pg->yc_Exp = pg->yc_deV;
		/* 
		pg->a_deV = 0.5 / pow(pg->re_deV, 2);
		pg->c_deV = 0.5 / pow(pg->re_deV, 2);
		pg->b_deV = sqrt(pg->a_deV * pg->c_deV) * BETA_SHAPE * phGaussdev();
		*/ 
		pg->e_Exp = ELLIPTICITY * (1.0 + inorm * (FL32) phRandom()) / 2.0;
		pg->phi_Exp = PI / 2.0 * (THPIX) phGaussdev();
		pg->E_Exp = STRETCHINESS * fabs((THPIX) phGaussdev());

	}
	} else {
		printf("%s: WARNING - null parameter in object (objc: %d) \n", name, objc->thid);
	}

	}
	}
	/* dumping parameters into their respective location */ 
	printf("%s: assigning parameter and amplitude arrays in (mapmachine) from IO parameters \n", name);
	
	thLDumpAmp(l, IOTOMODEL);
	thLDumpPar(l, IOTOMODEL); 
	
	thLDumpAmp(l, IOTOX);
	thLDumpPar(l, IOTOX);
	thLDumpPar(l, IOTOXN);
	
	#else 
	
	printf("%s: assigining parameters and amplitude arrays in (mapmachine) from SDSS model parameters\n", 
	name);

	  
	thLDumpAmp(l, MODELTOIO); 
	thLDumpPar(l, MODELTOIO);
 
	thLDumpAmp(l, MODELTOX);
	thLDumpPar(l, MODELTOX);
	thLDumpPar(l, MODELTOXN);

	#endif

	/* showing the statistics */
	chain_model_output("sim parameters: ", objclist, UNKNOWN_OBJC);

	/* calculation needs proper initiation of quadrature methods and elliptical description */
	status = MGInitProfile(EllDesc);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate profile package", name);
		return(-1);
	}

	/* creating the simulated / fake image */
	printf("%s: simulating the frame: \n", name);
	printf("%s: * memory estimates \n", name);
	status = MakeImageAndMatrices(l, MEMORY_ESTIMATE);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not estimate the memory for the model elements", name);
		return(-1);
	}

	printf("%s: * algorithm design \n", name);
	ADJ_MATRIX *adj = thAdjMatrixNew();
	status = thCreateAdjacencyMatrix(adj, l);
 	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not create the adjacency matrix for the current likelihood structure", name);
		return(-1);
	}
	ALGORITHM *alg_cr = thAlgorithmNew();
	alg_cr->memory_total = (MEMDBLE) THMEMORY * (MEMDBLE) MEGABYTE;	
	status = design_algorithm(adj, alg_cr, CREATE_MODEL, INITRUN);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not design the algorithm", name);
		return(-1);
	} 
	output_algorithm(alg_cr, adj);

	printf("%s: * algorithm run \n", name);
	status = thAlgorithmRun(alg_cr, l);	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not run the algorithm", name);
		return(-1);
	}

	printf("%s: obtaining header from frame \n", name);
	status = thFrameUpdateHdr(frame);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not make proper (hdr) from (frame) information", name);
		return(-1);
	}
	HDR *hdr = NULL;
	status = thFrameGetHdr(frame, &hdr);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get proper (hdr) from (frame)", name);
		return(-1);
	}
	if (hdr == NULL) {
		thError("%s: WARNING - null (hdr) found in (frame)", name);
	}
	printf("%s: * copying simulated image onto data \n", name);	
	thRegPixCopy(l->lwork->mN, im);
	REGION *outreg = NULL;
	outreg = shRegNew("output region in U16", im->nrow, im->ncol, TYPE_U16);
	shHdrCopy(hdr, &outreg->hdr);

	#if ADD_POISSON_NOISE
	printf("%s: outputting continuous model onto file '%s' with proper header \n", name, simodelfile);
	#if FPCC_INT
	shAssert(shRegIntCopy(outreg, im) == SH_SUCCESS);
	shHdrCopy(hdr, &outreg->hdr); /* adding header so that MLE finds pertinent information in the header of fpC */
	shRegWriteAsFits(outreg, simodelfile, STANDARD, 2, DEF_NONE, NULL, 0);
	#else 
	shHdrCopy(hdr, &im->hdr); /* adding header so that MLE finds pertinent information in the header of fpC */
	shRegWriteAsFits(im, simodelfile, STANDARD, 2, DEF_NONE, NULL, 0);
	#endif
	printf("%s: simulating poisson noise \n", name);
	status = add_poisson_noise_to_image(im, frame->data->ampl);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add poisson noise to (image) in (lstruct)", name);
		return(-1);
	}
	#else
	printf("%s: poisson noise _not_ simulated \n", name);
	#endif
	printf("%s: outputting simulated model onto file '%s' with proper header \n", name, simfile);
	shAssert(shRegIntCopy(outreg, im) == SH_SUCCESS);
	shHdrCopy(hdr, &outreg->hdr); /* adding header so that MLE finds pertinent information in the header of fpC */
	shRegWriteAsFits(outreg, simfile, STANDARD, 2, DEF_NONE, NULL, 0);
	printf("%s: end of simulation \n", name);

return(1);

	/* re-creating the poisson weights */
	printf("%s: loading poisson weights \n", name);
      	thFrameLoadWeight(frame);
	printf("%s: loading SOHO masks \n", name);
      	thFrameLoadThMask(frame);
	
	printf("%s: number of objects = %d \n", name, shChainSize(objclist));
	#if TWEAK
	printf("%s: tweaking parameters to get initial guess ... \n", name);
	status = tweak_init_pars(l, objclist);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get the initial guess by tweaking", name);
		return(-1);
	}
	#endif
	/* outputting the tweaked parameters */
	chain_model_output("initial guess parameters (tweaked): ", objclist, UNKNOWN_OBJC);

#if DO_ALGORITHM
	clock_t clk1, clk2;
	time_t time1, time2, dif;
	printf("%s: * algorithm \n", name);

	#if ALGORITHM_MANUAL

	printf("%s: i. adjacency matrix \n", name);
	ADJ_MATRIX *adj = thAdjMatrixNew();
	status = thCreateAdjacencyMatrix(adj, l);
 	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not create the adjacency matrix for the current likelihood structure", name);
		return(-1);
	}

	printf("%s: ii. algorithm design (manual) \n", name);	
	printf("%s: ii.a. initial run \n", name);
	clk1 = clock();
	ALGORITHM *alg = thAlgorithmNew();
	alg->memory_total = (MEMDBLE) THMEMORY * (MEMDBLE) MEGABYTE;	
	status = design_algorithm(adj, alg, CREATE_MODEL | INNER_PRODUCTS, INITRUN);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not design the algorithm", name);
		return(-1);
	}
	clk2 = clock();
	/* 
	output_algorithm(alg, adj);
	*/
	printf("%s: design time: %g sec \n", name, ((float) (clk2 - clk1)) / (float) CLOCKS_PER_SEC);
	printf("%s: ii.b. middle run \n", name);
	clk1 = clock();
	ALGORITHM *alg2 = thAlgorithmNew();
	alg2->memory_total = (MEMDBLE) THMEMORY * (MEMDBLE) MEGABYTE;	
	
	status = design_algorithm(adj, alg2, INNER_PRODUCTS, MIDDLERUN);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not design the algorithm", name);
		return(-1);
	}
	clk2 = clock();
	printf("%s: design time: %g sec \n", name, ((float) (clk2 - clk1)) / (float) CLOCKS_PER_SEC);
	/* 	
	output_algorithm(alg2, adj);
	*/
	printf("%s: iii. running the algorithm (manual) \n", name);
	printf("%s: iii.a. initial run \n", name);
	time(&time1);
	status = thAlgorithmRun(alg, l);	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not run the algorithm", name);
		return(-1);
	}
	time(&time2);
	dif = difftime(time2, time1);	
	printf("%s: run time: %g sec \n", name, (float) dif);
	
	printf("%s: iii.b. middle run \n", name);
	time(&time1);
	status = thAlgorithmRun(alg2, l);	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not run the algorithm", name);
		return(-1);
	}
	time(&time2);
	dif = difftime(time2, time1);
	printf("%s: run time: %g sec \n", name, (float) dif);
	#else

	MEMDBLE memory = (MEMDBLE) THMEMORY * MEGABYTE;	
	printf("%s: i. l compilation (automatic) \n", name);	
	clk1 = clock();
	status = thLCompile(l, memory, SIMFINALFIT);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not compile (l)", name);
		return(-1);
	}
	clk2 = clock();
	printf("%s: compile time: %g sec \n", name, ((float) (clk2 - clk1)) / (float) CLOCKS_PER_SEC);

	#if LRUN_MANUAL	
	printf("%s: ii.a. l run (manual) \n", name);
	ALGORITHM *alg;
	printf("%s: ii.a.i. INITSTAGE \n", name);
	alg = l->lalg->algs[INITSTAGE];
	status = thAlgorithmRun(alg, l);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not run 'INITSTAGE' alg", name);
		return(-1);
	}
	int iter;
	for (iter = 0; iter < 5; iter++) {
	printf("%s: ii.a.ii.  MIDDLESTAGE (iteration = %d) \n", name, iter);
	alg = l->lalg->algs[MIDDLESTAGE];
	status = thAlgorithmRun(alg, l);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not run 'MIDDLESTAGE' alg", name);
		return(-1);
	}
	}
	printf("%s: ii.a.iii. ENDSTAGE \n", name);
	alg = l->lalg->algs[ENDSTAGE];
	status = thAlgorithmRun(alg, l);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not run 'ENDSTAGE' alg", name);
		return(-1);
	}
	#else
	printf("%s: ii.a. l run (manual) - skipping \n", name);
	printf("%s: ii.b. l run (automatic) \n", name);
	time(&time1);
	status = thLRun(l);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not run (l)", name);
		return(-1);
	}
	time(&time2);
	dif = difftime(time2, time1);	
	printf("%s: run time: %g sec \n", name, (float) dif);

	#endif
	#endif

printf("%s: successful end of simulation\n", name);
return(1);

#else /* classic run */

	printf("%s: * init images (not the model) \n", name);
	status = MakeImageAndMatrices(l, INITIMAGES);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not init images", name);
		return(-1);
	}
	printf("%s: * making the model \n", name);
	status = thMakeM(l);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not make model for (lstruct)", name);
		return(-1);
	}
#endif

	printf("%s: copying simulated image onto a new region \n", name);
	simreg = l->lwork->mN;
	thRegPixCopy(simreg, im);

	#if ADD_POISSON_NOISE
	/* adding poisson noise to the image */
	printf("%s: simulating poisson noise \n", name);
	status = add_poisson_noise_to_image(im, frame->data->ampl);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add poisson noise to (image) in (lstruct)", name);
		return(-1);
	}
	#endif
	/* dumping created model into an image file */
	printf("%s: outputting the simulated image onto '%s' \n", name, simfile);
	shRegWriteAsFits(im, simfile, STANDARD, 2, DEF_NONE, NULL, 0);

/* doing residual reports */

	REGION *diff;
	diff = shRegNew("residual image", simreg->nrow, simreg->ncol, simreg->type);
	thPutModelComponent(diff, simreg, 1.0); /* sim reg is the product of simulation */
	thAddModelComponent(diff, im, -1.0); /* im is the data  */	

	int rbin1 = 1, cbin1 = 1, rbin2 = 20, cbin2 = 20;
	printf("%s: iv. residual reports \n", name);
	printf("%s: iv.a. creating report for (rbin, cbin) = (%d, %d) \n", name, rbin1, cbin1);
	RESIDUAL_REPORT *res = NULL;
	status = thCreateResidualReport(diff, NULL, rbin1, cbin1, &res);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not create residual report", name);
		return(-1);
	}
	printf("%s: iv.a. compiling report \n", name);
	status = thCompileResidualReport(res);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not compile residual report", name);
		return(-1);
	}
	printf("%s: iv.b. creating chain of reports (rbin = %d, %d), (cbin = %d, %d) \n", name, rbin1, rbin2, cbin1, cbin2);
	CHAIN *reschain;
	reschain = shChainNew("RESIDUAL_REPORT");
	status = thCreateResidualReportChain(diff, NULL, rbin1, rbin2, cbin1, cbin2, reschain);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not create residual report chain", name);
		return(-1);
	}
	printf("%s: iv.b. compiling residual report chain \n", name);
	status = thCompileResidualReportChain(reschain);	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not compile residual report chain", name);
		return(-1);
	}

	status = output_residual_report_chain(reschain);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not output (residual report) chain", name);
		return(-1);
	}

printf("%s: return requested by user \n", name);
return(1);

	/* re-creating the poisson weights */
	printf("%s: loading poisson weights \n", name);
      	thFrameLoadWeight(frame);
	printf("%s: loading SOHO masks \n", name);
      	thFrameLoadThMask(frame);

	/* 
		outputting simulation paramters 
	*/
	/* 
		Tweaking Initial Parameters a bit 
	*/
	printf("%s: tweaking parameters to get initial guess ... \n", name);
	status = tweak_init_pars(l, objclist);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get the initial guess by tweaking", name);
		return(-1);
	}
	/* outputting the tweaked parameters */
	chain_model_output("initial guess parameters (tweaked): ", objclist, UNKNOWN_OBJC);

	/*
	   do LM-ext fit
	*/
	printf("%s: conducting the extended LM fit \n", name);
	status = thModelExtLMFitSimpleRun(l);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not conduct an extended LM fit", name);
		return(-1);
	}
	/* 
	   put fit parameter values and amplitudes in the THOBJC data structure 
	*/
	#if 1
	printf("%s: dumping fit amplitudes from X-array onto models \n", name);
	thLDumpAmp(l, XTOMODEL);
	printf("%s: dumping fit amplitudes and parameters in IO records \n", name);
	/* thLDumpAmp(l, MODELTOIO); */
	thLDumpPar(l, MODELTOIO);
	#endif

	/* no fit to be done by this program */
	
	printf("%s: end of the program \n", name);

  thErrShowStack();
  /* for unknown reason, this returns an error - distribution of the variates were checked and the results produced satisfy the 
	randomness condition.
  phRandomDel(rand);
	*/

  /* successfull run */
  return(0);
}


/*****************************************************************************/

static void
usage(void)
{
   char **line;

   static char *msg[] = {
      "Usage: read_atlas_image [options] output-file n_tr n_tc",
      "Your options are:",
      "       -?      This message",
      "       -h      This message",
      "       -i      Print an ID string and exit",
      "       -v      Turn up verbosity (repeat flag for more chatter)",
      NULL,
   };

   for(line = msg;*line != NULL;line++) {
      fprintf(stderr,"%s\n",*line);
   }
}	

void chain_model_output(char *job, CHAIN *objclist, THOBJCTYPE otype) {

THOBJC *objc;
int iobjc, nobjc;
nobjc = shChainSize(objclist);
for (iobjc = 0; iobjc < MIN(nobjc, N_OUTPUT_OBJC); iobjc++) {
	objc = shChainElementGetByPos(objclist, iobjc);
	model_output(job, objc);
}
printf("--- %s: end of output --- \n", job);
return;
}


void model_output(char *job, THOBJC *objc) {

CHAIN *thprops;
int nmodel, imodel;
static char *line = NULL;
if (line == NULL) line = thCalloc(MX_STRING_LEN, sizeof(char));

thprops = objc->thprop;
nmodel = shChainSize(thprops);
for (imodel = 0; imodel < nmodel; imodel++) {
	SCHEMA *s;
	SCHEMA_ELEM *se;
	int n, i;
	char *rname;
	THPIX *value;
	THPROP *prop;
	prop = shChainElementGetByPos(thprops, imodel);	 		
	s = shSchemaGetFromType(shTypeGetFromName(prop->pname));
	n = s->nelem;
	printf("--- %s parameters for (objc: %d) (model: '%s') --- \n", 
		job, objc->thid, prop->mname);
	for  (i = 0; i < n; i++) {
		se = s->elems + i;
		rname = se->name;
		value = shElemGet(prop->value, se, NULL);
		if (strlen(line) != 0) {
			sprintf(line, "%s, %15s = %.5e", line, rname, *value);
		} else {
			sprintf(line,"%15s = %.5e", rname, *value);
		}
		if (((i + 1)%TABLEROW == 0) || i == n - 1) {
			printf("%s\n", line);
			strcpy(line, "");
		}
	}
}
return;
}

RET_CODE tweak(void *p, THPIX e_tweak, char *pname, char **rnames, int rcount, CHAIN *chain,
		TWEAKFLAG tflag) {
char *name = "tweak";

/* this assumes that all records (schema_elems) are of type THPIX */

SCHEMA *s;
SCHEMA_ELEM *se;

if (p == NULL || pname == NULL || strlen(pname) == 0) {
	thError("%s: ERROR - void or unknown input parameter cannot be tweaked", name);
	return(SH_GENERIC_ERROR);
}
TYPE ptype;
ptype = shTypeGetFromName(pname);
s = shSchemaGetFromType(ptype);
if (s == NULL) {
	thError("%s: ERROR - unknown parameter type '%s'", name, pname);
	return(SH_GENERIC_ERROR);
}
if (rcount < 0) {
	thError("%s: ERROR - negative record count (%d)", name, rcount);
	return(SH_GENERIC_ERROR);
}
if (rcount > 0 && rnames == NULL) {
	thError("%s: ERROR - null record name, while record count is positive (%d)", name, rcount);
	return(SH_GENERIC_ERROR);
}


RET_CODE status;
int i;
void *x;
#define YTYPE FL32
YTYPE y;
char *ytype = "FL32";

if (rcount == 0) {
	int n; 
	int onlypos = 0;
	n = s->nelem;
	for (i = 0; i < n; i++) {
		se = s->elems + i;
		if (se == NULL) {
			thError("%s: WARNING - could not find %d-th record in '%s'", name, i, pname);
		} else {
			onlypos = !strcmp(se->name, "re");	
			x = shElemGet(p, se, NULL);
			status = thqqTrans(x, se->type, &y, ytype);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not read record '%s' of type '%s' in '%s' into a double variable", name, se->name, se->type, pname);
				return(status);
			}
			int poscheck = 1;
			while (poscheck) {
				if (tflag == FRACTIONAL) {
					y *= ((YTYPE) 1.00 + ((YTYPE) phGaussdev()) * (YTYPE) e_tweak);
				} else if (tflag == ABSOLUTE) {
					y += ((YTYPE) phGaussdev()) * (YTYPE) e_tweak;
				}
				poscheck = onlypos && (y > (YTYPE) 0);
			}
			status = thqqTrans(&y, ytype, x, se->type);
 			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not transform '%s' data types into '%s' for record '%s' of '%s'", name, ytype, se->type, se->name, pname);
				return(status);
			}
		}
	}
} else {	
	char *rname;
	int onlypos = 0;
	for (i = 0; i < rcount; i++) {
		rname = rnames[i];
		onlypos = (!strcmp(rname, "re"));
		if (rname != NULL && strlen(rname) != 0) {
			se = shSchemaElemGetFromType(ptype, rname);
			if (se == NULL) {
				thError("%s: WARNING - could not find record '%s' in '%s'", name, rname, pname);
			} else {
				x = shElemGet(p, se, NULL);
				status = thqqTrans(x, se->type, &y, ytype);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not read record '%s' of type '%s' in '%s' into a working variable of type '%s'", name, se->name, se->type, pname, ytype);
					return(status);
				}
				int poscheck = 1;
				while (poscheck) {
					if (tflag == FRACTIONAL) {
						y *= ((YTYPE) 1.00 + ((YTYPE) phGaussdev()) * (YTYPE) e_tweak);
					} else if (tflag == ABSOLUTE) {
						y += ((YTYPE) phGaussdev()) * (YTYPE) e_tweak;
					}
					poscheck = onlypos && (y > (YTYPE) 0);
				}
				status = thqqTrans(&y, ytype, x, se->type);
 				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not transform '%s' data type into '%s' for record '%s' of '%s'", name, ytype, se->type, se->name, pname);
					return(status);
				}

		}
		}
	}
}
#undef YTYPE 
return(SH_SUCCESS);
}

RET_CODE add_poisson_noise_to_image(REGION *im, CHAIN *ampl) {
char *name = "add_poisson_noise_to_image";

if (im == NULL) {
	thError("%s: ERROR - null input image", name);
	return(SH_GENERIC_ERROR);
}

if (ampl == NULL) {
	thError("%s: ERROR - null amplifier information", name);
	return(SH_GENERIC_ERROR);
}

int nrow, ncol, i, j, n0 = 0, n_ = 0, nplus = 0;
THPIX *row;
FL32 y;

nrow = im->nrow;
ncol = im->ncol;

int ia, na;
na = shChainSize(ampl);
for (ia = 0; ia < na; ia++) {

AMPL *a;
int row0, row1, col0, col1;
THPIX g;
FL32 mu;
a = shChainElementGetByPos(ampl, ia);
if (a == NULL) {
	thError("%s: WARNING - null (AMPL) structure discovered in (ampl) chain", name);
} else {
	row0 = a->row0;
	row1 = a->row1;
	col0 = a->col0;
	col1 = a->col1;
	g = a->gain;	
	printf("%s: amplifier (%d) - (gain = %g)\n", name, ia, g);
	for (i = MAX(row0, 0); i < MIN(nrow, row1); i++) {
		row = im->rows_thpix[i];
		for (j = MAX(0, col0); j < MIN(ncol, col1); j++) {
		y = (FL32) (row[j]);
		if (y > (FL32) 0.0) {
			mu = g * y;
			y = phPoissondev(mu) / g;
			/* 
			y += sqrt(y / g) * ((FL32) phGaussdev());
			*/
			row[j] = y;
			nplus++;
		} else if (y == (THPIX) 0.0) {
			n0++;
		} else {
			n_++;
		}
		}
	}
}
}

if (n0 > 0) printf("%s: (zero pixels: %d) \n", name, n0);
if (n_ > 0) printf("%s: (negative pixels: %d) \n", name, n_);
if (nplus > 0) printf("%s: (positive pixels: %d) \n", name, nplus);

return(SH_SUCCESS);
}

int compare_re(const void *x1, const void *x2) {
	OBJC_IO *y1, *y2;
	shAssert(x1 != NULL);
	shAssert(x2 != NULL);
	y1 = *(OBJC_IO **) x1;
	y2 = *(OBJC_IO **) x2;
	float z1, z2;
	z1 = (float) y1->r_deV[THIS_BAND];
	z2 = (float) y2->r_deV[THIS_BAND];
	int res;
	if (y1->objc_type == OBJ_GALAXY && 
		y2->objc_type == OBJ_GALAXY) {
		if (z1 > z2) {
			res = -1;
		} else if (z1 < z2) {
			res = 1;
		} else {
			res = 0;
		}
	} else if (y1->objc_type == OBJ_GALAXY && y2->objc_type != OBJ_GALAXY) {
		res = -1;
	} else if (y1->objc_type != OBJ_GALAXY && y2->objc_type == OBJ_GALAXY) {
		res = 1;
	} else {
		res = 0;
	}
	return(res);
}

int compare_count(const void *x1, const void *x2) {
	OBJC_IO *y1, *y2;
	shAssert(x1 != NULL);
	shAssert(x2 != NULL);
	y1 = *(OBJC_IO **) x1;
	y2 = *(OBJC_IO **) x2;
	int res;
	if (y1->objc_type == OBJ_GALAXY && y2->objc_type == OBJ_GALAXY) {
		float z1, z2;
		z1 = y1->counts_exp[THIS_BAND] + y1->counts_deV[THIS_BAND];	
		z2 = y2->counts_exp[THIS_BAND] + y2->counts_deV[THIS_BAND];
		if (z1 > z2) {
			res = -1;
		} else if (z1 < z2) {
			res = 1;
		} else {
			res = 0;
		}
	} else if (y1->objc_type == OBJ_GALAXY) {
		res = -1;
	} else if (y2->objc_type == OBJ_GALAXY) {
		res = 1;
	} else {
		res = 0;
	}
	return(res);
}

RET_CODE tweak_init_pars(LSTRUCT *l, CHAIN *objclist) {
char *name = "tweak_init_pars";

/* add all the models in the object to the map */
static char **rnames = NULL, **iornames = NULL, **mnames = NULL;
char *mname;
int nrname = 10;
int nm_max = 2;
int i;
if (rnames == NULL) {
	mnames = thCalloc(nm_max, sizeof(char *));
	rnames = thCalloc(nrname, sizeof(char*));
	iornames = thCalloc(nrname, sizeof(char *));
	for (i = 0; i < nrname; i++) {
		mnames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
		rnames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
		iornames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
	}
}

int pos = 0;
SKYPARS *ps;
THOBJC *objc = NULL;
TYPE ptype;
char *pname;
OBJCPARS *pg;

int ii, j = 0;
ii = j;
strcpy(rnames[j++], "I");
#if FIT_SHAPE 
int ishape, jshape, nshape;
ishape = j;
if (EllDesc == ALGEBRAIC) {
	strcpy(rnames[j++], "a");
 	strcpy(rnames[j++], "b");
	strcpy(rnames[j++], "c");
} else if (EllDesc == CANONICAL) {
	#if FIT_RE
	strcpy(rnames[j++], "re");
	#endif
	#if FIT_E
	strcpy(rnames[j++], "e");
	#endif
	#if FIT_PHI
	strcpy(rnames[j++], "phi");
	#endif
} else if (EllDesc == SUPERCANONICAL || EllDesc == UBERCANONICAL) {
	#if FIT_RE
	strcpy(rnames[j++], "re"); 
	#endif
	#if FIT_E
	strcpy(rnames[j++], "E");
	#endif
	#if FIT_PHI
	strcpy(rnames[j++], "phi");
	#endif
}	
jshape = j;
nshape = j;
#endif
#if FIT_CENTER
int icenter, jcenter, ncenter;
icenter = j;
strcpy(rnames[j++], "xc");  
strcpy(rnames[j++], "yc");
jcenter = j;
ncenter = (jcenter - icenter);
#endif

int ntweak = 0;
if (TWEAK_ALL) {
	ntweak = shChainSize(objclist);
} else {
	ntweak = MIN(1+ NFITOBJC, shChainSize(objclist));
}
RET_CODE status;
for (pos = 0; pos < ntweak; pos++) {

	THOBJCTYPE objctype;
	char *objcname;
	objc = shChainElementGetByPos(objclist, pos);
	objctype = objc->thobjctype;
	thObjcNameGetFromType(objctype, &objcname);
	if (objctype == SKY_OBJC) {

	/* sky */ 
	thObjcGetIopar(objc, (void **) &ps, &ptype);
	pname = shNameGetFromType(ptype);	
	if (ps != NULL) {
	status = tweak((void *) ps, SKY_FRACTIONAL_TWEAK, pname, NULL, 0, objclist, ABSOLUTE);
	if (status != SH_SUCCESS) {
		printf("%s: ERROR - could not tweak '%s' \n", name, pname);
		return(-1);
	}
	} else {
		printf("%s: WARNING - null parameter '%s' in object (objc: %d) - no tweaking \n", name, pname, objc->thid);
	}
	} else if (!strcmp(objcname, "DEVGALAXY") || !strcmp(objcname, "EXPGALAXY") || !strcmp(objcname, "GAUSSIAN") || (!strcmp(objcname, "GALAXY"))) {

	/* galaxy */
	OBJC_IO *ph;
	ph = ((PHPROPS *) objc->phprop)->phObjc;
	thObjcGetIopar(objc, (void **) &pg, &ptype);
	pname = shNameGetFromType(ptype);

	mname = mnames[0];
	if (!strcmp(objcname, "DEVGALAXY")) strcpy(mname, "deV");
	if (!strcmp(objcname, "EXPGALAXY")) strcpy(mname, "Exp");
	if (!strcmp(objcname, "GAUSSIAN")) strcpy(mname, "gaussian");
	if (!strcmp(objcname, "GALAXY"))  {
		strcpy(mnames[0], "deV");
		strcpy(mnames[1], "Exp");
	}
	if (!strcmp(objcname, "CDCANDIDATE")) {
		strcpy(mnames[0], "deV");
		strcpy(mnames[1], "Exp2");
	}

	if (pos < N_OUTPUT_OBJC) {
	printf("%s: tweaking (objc: %d): ", name, objc->thid);
	for (i = 0; i < j; i++) {
		sprintf(iornames[i], "%s_%s", rnames[i], mname);
		printf("'%s' ", iornames[i]);
	}
	printf("\n");
	}
	if (pg != NULL) {

	THPIX e_tweak;
	#if FORCE_INIT_PAR
	e_tweak = I_TWEAK;
	status = tweak((void *)pg, e_tweak, pname, iornames + ii, 
			1, objclist, FRACTIONAL);
	#else
	e_tweak = 0.0;
	if (!strcmp(mname, "deV")) {
		e_tweak = I_TWEAK * ph->counts_deVErr[band] / ((THPIX) DEV_MCOUNT * pow(DEV_RE_SMALL + ph->r_deV[band], 2));
		if (pos < N_OUTPUT_OBJC) printf("%s: i_tweak('%s') = %g; countErr = %g, rErr = %g \n", 
		name, mname, e_tweak, ph->counts_deVErr[band], ph->r_deV[band]);} 
	else if (!strcmp(mname, "Exp")) {
		e_tweak = I_TWEAK * ph->counts_expErr[band] / ((THPIX) EXP_MCOUNT * pow(EXP_RE_SMALL + ph->r_exp[band], 2));
		
		if (pos < N_OUTPUT_OBJC) printf("%s: i_tweak('%s') = %g; countErr = %g, rErr = %g \n", 
		name, mname, e_tweak, ph->counts_expErr[band], ph->r_exp[band]);
	}
	status = tweak((void *)pg, e_tweak, pname, iornames + ii, 
			1, objclist, ABSOLUTE);
	#endif
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not tweak '%s'", name, pname);
		return(status);
	} 
	#if FIT_SHAPE
	if (EllDesc == SUPERCANONICAL || EllDesc == UBERCANONICAL) {
		status = tweak((void *) pg, SHAPE_TWEAK, pname, iornames + ishape, 
		nshape, objclist, FRACTIONAL);
	} else {
		status = tweak((void *) pg, SHAPE_TWEAK, pname, iornames + ishape, 
		nshape, objclist, FRACTIONAL);
	}
	if (EllDesc == ALGEBRAIC) {
		pg->b_gaussian *= pow(1.0 - BETA_SHAPE, 2);
	}
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not tweak '%s'", name, pname);
		return(status);
	} 

	#endif
	#if FIT_CENTER
	for (i = 0; i < ncenter; i++) {
		e_tweak = 0.0;
		#if FORCE_INIT_PAR
		e_tweak = CENTER_TWEAK;
		#else 
		if (!strcmp(rnames[i + icenter], "xc")) {
			e_tweak = CENTER_TWEAK * ph->rowcErr[band];
		} else if (!strcmp(rnames[i + icenter], "yc")) {
			e_tweak = CENTER_TWEAK * ph->colcErr[band];
		}
		#endif
		status = tweak((void *) pg, e_tweak, pname, iornames + icenter + i, 
				1, objclist, ABSOLUTE);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not tweak '%s'", name, pname);
			return(status);
		}
	} 
	#endif
	
	if (!strcmp(objcname, "GALAXY")) {

	mname = mnames[1];
	if (pos < N_OUTPUT_OBJC) {
	printf("%s: tweaking (objc: %d): ", name, objc->thid);	
	for (i = 0; i < j; i++) {
		sprintf(iornames[i], "%s_%s", rnames[i], mname);
		printf("'%s' ", iornames[i]);
	}
	printf("\n");
	}

	e_tweak = 0.0;
	#if FORCE_INIT_PAR
	e_tweak = I_TWEAK;
	status = tweak((void *)pg, e_tweak, pname, iornames + ii, 
		1, objclist, FRACTIONAL);
	#else
	if (!strcmp(mname, "deV")) {
		e_tweak = I_TWEAK * ph->counts_deVErr[band] / ((THPIX) DEV_MCOUNT * pow(DEV_RE_SMALL + ph->r_deV[band], 2));	
		if (pos < N_OUTPUT_OBJC) printf("%s: i_tweak('%s') = %g; countErr = %g, rErr = %g \n", 
		name, mname, (float) e_tweak, (float) ph->counts_deVErr[band], (float) ph->r_deV[band]);
	} else if (!strcmp(mname, "Exp")) {
		e_tweak = I_TWEAK * ph->counts_expErr[band] / ((THPIX) EXP_MCOUNT * pow(EXP_RE_SMALL + ph->r_exp[band], 2));
		if (pos < N_OUTPUT_OBJC) printf("%s: i_tweak('%s') = %g; countErr = %g, rErr = %g \n", 
		name, mname, (float) e_tweak, (float) ph->counts_expErr[band], (float) ph->r_exp[band]);
	}
	status = tweak((void *)pg, e_tweak, pname, iornames + ii, 1, objclist, ABSOLUTE);
	#endif
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not tweak '%s'", name, pname);
		return(status);
	} 
	#if FIT_SHAPE
	if (EllDesc == SUPERCANONICAL || EllDesc == UBERCANONICAL) {
		status = tweak((void *) pg, SHAPE_TWEAK, pname, iornames + ishape, nshape, objclist, FRACTIONAL);
	} else {
		status = tweak((void *) pg, SHAPE_TWEAK, pname, iornames + ishape, nshape, objclist, FRACTIONAL);
	}
	if (EllDesc == ALGEBRAIC) {
		pg->b_gaussian *= pow(1.0 - BETA_SHAPE, 2);
	}
	#endif
	#if FIT_CENTER
	for (i = 0; i < ncenter; i++) {
		e_tweak = 0.0;
		#if FORCE_INIT_PAR
		e_tweak = CENTER_TWEAK;
		#else
		if (!strcmp(rnames[i + icenter], "xc")) {
			e_tweak = CENTER_TWEAK * ph->rowcErr[band];
		} else if (!strcmp(rnames[i + icenter], "yc")) {
			e_tweak = CENTER_TWEAK * ph->colcErr[band];
		}
		#endif
		status = tweak((void *) pg, e_tweak, pname, iornames + icenter + i, 
				1, objclist, ABSOLUTE);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not tweak '%s'", name, pname);
			return(status);
		}
	} 
	#endif

	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not tweak '%s'", name, pname);
		return(status);
	}

	}	
	} else {
		printf("%s: WARNING - null parameter '%s' in object (objc: %d) - no tweaking \n", name, pname, objc->thid);
	}

	}
	}
	/* dumping parameters into their respective location */ 
 
/* 	
	#if FORCE_INIT_PAR 
*/
	printf("%s: assigning parameter and amplitude arrays in (mapmachine) from tweaked IO parameters \n", name);
	thLDumpAmp(l, IOTOMODEL);
	thLDumpPar(l, IOTOMODEL);

	thLDumpAmp(l, IOTOX);
	thLDumpPar(l, IOTOX);
	thLDumpPar(l, IOTOXN);
/* 
	#else

	printf("%s: assigning SDSS parameters to (mapmachine) arrays", name);
	thLDumpAmp(l, MODELTOX);
	thLDumpPar(l, MODELTOX);

	#endif
*/


	printf("%s: assigning values to (a, p) from updated (aN, pN) \n", name);
	status = UpdateXFromXN(l, APSECTOR);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update parameters to the tweaked values", name);
		return(status);
	}

return(SH_SUCCESS);
}

void test_sorted_chain(CHAIN *chain, int (*compare)(const void *, const void *)) {
char *name = "test_sorted_chain";
if (chain == NULL || compare == NULL) {
	thError("%s: ERROR - null input", name);
	return;
}
int n = shChainSize(chain);
int i, error = 0;

printf("%s: listing chain elements (r_deV) (chain size = %d) \n", name, n);
for (i = 0; i < n; i++) {
	OBJC_IO *x1;
	x1 = shChainElementGetByPos(chain, i);
	if (x1->objc_type != OBJ_GALAXY) {
		printf(" %s, ", shEnumNameGetFromValue("OBJ_TYPE", x1->objc_type));	
	} else {
		printf(" %g,", (float) x1->r_deV[R_BAND]);
	}
}
printf("\n");

for (i = 0; i < n - 1; i++) {
	const void *x1, *x2;
	x1 = shChainElementGetByPos(chain, i);
	x2 = shChainElementGetByPos(chain, i+1);
	int res = compare(&x1, &x2);
	if (res == -1) {
		thError("%s: ERROR - chain descends at (i = %d)", name, i);
		error++;
	}
}
if (error) {
	thError("%s: ERROR - found at least (%d) misplaced elements in chain", name);
}
return;
}


