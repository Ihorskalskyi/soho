#include "thResidual.h"

RET_CODE thCompileResidualReport(RESIDUAL_REPORT *res) {
char *name = "thCompileResidualReport";
static char *binname = NULL;
if (res == NULL) {
	if (binname != NULL) thFree(binname);
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

if (binname == NULL) {
	binname = thCalloc(MX_STRING_LEN, sizeof(char));
}
strcpy(binname, "binned image - ");
OBJMASK *mask = res->mask;
OBJMASK *binmask = NULL;
int rbin = res->rbin;
int cbin = res->cbin;
AMPL *ampl = res->ampl;
REGION *diff = res->diff;
strcat(binname, diff->name);
REGION *bindiff = shRegNew(binname, 0, 0, TYPE_THPIX);
MCFLAG cflag = res->cflag;

if (cflag == COMPILED) {
	thError("%s: ERROR - cannot do multiple compilation of the same report", name);
	return(SH_GENERIC_ERROR);
}
if (cflag != ALTERED) {
	thError("%s: ERROR - unsupported compilation flag for report", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
int nrow, ncol;
nrow = diff->nrow;
ncol = diff->ncol;
int npix = nrow * ncol; /* this is to be used when mask is null */
THPIX npix_eff = npix;
if (mask != NULL) {
	status = mask_weight_info(res, &npix, &npix_eff);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract pixel number for mask", name);
		return(status);
	}
}
int binpix = npix;
THPIX binpix_eff = npix_eff;
status = bin_mask(mask, rbin, cbin, &binmask, &binpix, &binpix_eff);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not bin mask", name);
	return(status);
}
status = bin_reg(diff, rbin, cbin, bindiff);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not bin the difference region properly", name);
	return(status);
}
THPIX diffmean, diffstdev, diffvar;
status = analyze_reg(bindiff, binmask, &diffmean, &diffstdev, &diffvar);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not derive the statistical properties of the binned difference image", name);
	return(status);
}

res->npix = npix;
res->npix_eff = npix_eff;
res->binpix = binpix;
res->binpix_eff = binpix_eff;

res->mean = diffmean;
res->variance = diffvar;
res->stddev = diffstdev;

res->bindiff = bindiff;
res->binmask = binmask;

res->cflag = COMPILED;

return(SH_SUCCESS);
}

RET_CODE thCompileResidualReportChain(CHAIN *reschain) {
char *name = "thCompileResidualReportChain";
int nchain;
if (reschain == NULL || (nchain = shChainSize(reschain)) == 0) {
	thError("%s: WARNING - null or empty report chain", name);
	return(SH_SUCCESS);
}
RET_CODE status;
RESIDUAL_REPORT *res;
int ncompiled = 0, pos;
for (pos = 0; pos < nchain; pos++) {
	res = shChainElementGetByPos(reschain, pos);
	if (res->cflag != COMPILED) {
		status = thCompileResidualReport(res);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not compile residular report at (pos = %d) of chain", name, pos);
			return(status);
		}
	} else {
		ncompiled++;
	}
}
if (ncompiled == nchain) {
	thError("%s: WARNING - all (%d) elements of the chain were previously compiled", name, nchain);
	return(SH_SUCCESS);
} else if (ncompiled != 0) {
	thError("%s: WARNING - found (%d) previously compiled reports in the chain", name, ncompiled);
	return(SH_SUCCESS);
}

return(SH_SUCCESS);
}

RET_CODE bin_reg (REGION *inreg, int rbin, int cbin, REGION *outreg) {
char *name = "bin_reg";
if (inreg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (rbin <= 0 || cbin <= 0) {
	thError("%s: ERROR - unsupported binsize (rbin = %d, cbin = %d)", name, rbin, cbin);
	return(SH_GENERIC_ERROR);
}
if (outreg == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}

int row0, col0, nrow, ncol;
row0 = inreg->row0;
col0 = inreg->col0;
nrow = inreg->nrow;
ncol = inreg->ncol;

int outrow0, outcol0, outnrow, outncol;
outrow0 = row0 / rbin;
if (row0 % rbin != 0) outrow0++;
outcol0 = col0 / cbin;
if (col0 % cbin != 0) outcol0++;

int rowp0, colp0, drow0, dcol0, nrowp, ncolp;
rowp0 = outrow0 * rbin;
colp0 = outcol0 * cbin;

drow0 = rowp0 - row0;
dcol0 = colp0 - col0;

nrowp = nrow - drow0;
ncolp = ncol - dcol0;

outnrow = nrowp / rbin;
outncol = ncolp / cbin;

nrowp = outnrow * rbin;
ncolp = outncol * cbin;

if (outreg->nrow != outnrow || outreg->ncol != outncol) {
	p_shRegRowsFree(outreg);
	p_shRegVectorFree(outreg);
	if (p_shRegVectorGet(outreg, outnrow, TYPE_THPIX) == (int) NULL) {
		thError("%s: ERROR - could not allocate (row) addresses in (reg)", name);
		return(SH_GENERIC_ERROR);
	}	
	if (p_shRegRowsGet(outreg, outnrow, outncol, TYPE_THPIX) == (int) NULL) {
		thError("%s: ERROR - could not allocated (rows) in (reg)", name);
		return(SH_GENERIC_ERROR);
	}
}

/* making outreg zero */
shRegClear(outreg);

outreg->row0 = outrow0;
outreg->col0 = outcol0;

int i, j;
if (rbin == 1 && cbin == 1) {
	/* trivial binning */
	for (i = 0; i < nrow; i++) {
		THPIX *inrow_i = inreg->rows_thpix[i];
		THPIX *outrow_i = outreg->rows_thpix[i];
		memcpy(outrow_i, inrow_i, sizeof(THPIX) * ncol);
	}
} else {
	for (i = 0; i < nrowp; i++) {
		int inrow = drow0 + i;
		THPIX  *inrow_i = inreg->rows_thpix[inrow];
		int outrow = i / rbin;
		THPIX *outrow_i = outreg->rows_thpix[outrow];
		for (j = 0; j < ncolp; j++) {
			int incol = dcol0 + j;
			int outcol = j / cbin;
			outrow_i[outcol] += inrow_i[incol];
		}
	}
}

return(SH_SUCCESS);
}

RET_CODE mask_weight_info(RESIDUAL_REPORT *res, int *npix, THPIX *npix_eff) {
char *name = "mask_weight_info";
if (res == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (npix == NULL && npix_eff == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
OBJMASK *mask;
if ((mask = res->mask) == NULL) {
	thError("%s: WARNING - null mask - (npix) should be assigned from image", name);
	return(SH_SUCCESS);
}

if (npix != NULL) *npix = mask->npix;
if (npix_eff != NULL) *npix_eff = mask->npix;
return(SH_SUCCESS);
}


RET_CODE bin_mask(OBJMASK *inmask, int rbin, int cbin, OBJMASK **outmask, int *binpix, THPIX *binpix_eff) {
char *name = "bin_mask";
static REGION *wreg = NULL;
static MASK *wmask = NULL;
if (rbin == -1 && cbin == -1 && inmask == NULL && outmask == NULL && binpix == NULL && binpix_eff == NULL) {
	thError("%s: Warning - freeing working memory", name);
	shRegDel(wreg);
	shMaskDel(wmask);
	wreg = NULL;
	wmask = NULL;
	return(SH_SUCCESS);
}
if (outmask == NULL) {
	thError("%s: WARNING - null placeholdr for output", name);
	return(SH_SUCCESS);
}
if (rbin <= 0 || cbin <= 0) {
	thError("%s: ERROR - unsupported binning (rbin = %d, cbin = %d)", name, rbin, cbin);
	return(SH_GENERIC_ERROR);
}

if (inmask == NULL) {
	*outmask = NULL;
	if (binpix != NULL) {
		*binpix = *binpix / (rbin * cbin);
		*binpix_eff = *binpix;
	}
	return(SH_SUCCESS);
}

if (rbin == 1 && cbin == 1) {
	*outmask = phObjmaskCopy(inmask, (float) 0.0, (float) 0.0);	
	return(SH_SUCCESS);
}

if (wreg == NULL) {
	wreg = shRegNew("workspace: (bin_mask)", MAXNROW, MAXNCOL, TYPE_THPIX);
} 
if (wmask == NULL) {
	wmask = shMaskNew("workspace: (bin_mask)", MAXNROW, MAXNCOL);
}

RET_CODE status;
THPIX pixflag = 1.0;
status = convert_objmask_to_region(inmask, wreg, pixflag);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not convert (mask) into (region)", name);
	return(status);
}
REGION *wreg_binned = shRegNew("work space: binned at (bin_mask)", 0, 0, TYPE_THPIX);
status = bin_reg(wreg, rbin, cbin, wreg_binned);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not bin the (region) image of (mask)", name);
	return(status);
}
pixflag *= (THPIX) rbin * (THPIX) cbin;
status = convert_region_to_objmask(wreg_binned, outmask, pixflag);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not convert (region) image of binned (mask) onto (objmask)", name);
	return(status);
} 
if (binpix != NULL) *binpix = (*outmask)->npix;
if (binpix_eff != NULL) *binpix_eff = (*outmask)->npix;

shRegDel(wreg_binned);
return(SH_SUCCESS);
}

RET_CODE convert_objmask_to_region(OBJMASK *mask, REGION *reg, THPIX flag) {
char *name = "convert_objmask_to_region";
if (mask == NULL && reg == NULL) {
	thError("%s: WARNING - null input and output placeholder", name);
	return(SH_SUCCESS);
}
if (reg == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}	
if (flag == (THPIX) 0.0) {
	thError("%s: ERROR - unsupported flag (%g)", name, flag);
	return(SH_GENERIC_ERROR);
}

int i;
int nrow, ncol;
if (mask == NULL) {
	thError("%s: WARNING - null (mask) is a trivial case that shouldn't be handled by this function - setting all pixels to (flag = %g)", name, flag);
	nrow = reg->nrow;
	ncol = reg->ncol;
	for (i = 0; i < nrow; i++) {
		THPIX *row_i = reg->rows_thpix[i];
		memset(row_i, flag, ncol * sizeof(THPIX));
	}
	return(SH_SUCCESS);
}

shRegClear(reg);

reg->row0 = mask->row0;
reg->col0 = mask->col0;
int nspan = mask->nspan;
SPAN *s;
int ispan, badspan = 0, verybadspan = 0;
for (ispan = 0; ispan < nspan; ispan++) {
	int col1, col2, row, len;
	s = mask->s + ispan;
	row = s->y;
	col1 = s->x1;
	THPIX *row_i = reg->rows_thpix[row];
	if (col1 < 0) {
		badspan++;
		col1 = 0;
	}	
	col2 = s->x2;
	if (col2 >= ncol) {
		badspan++;
		col2 = ncol - 1;
	}
	if (col1 < ncol && col2 >= 0) {
		len = col2 - col1 + 1;
		memset(row_i + col1, flag, len * sizeof(THPIX));
	} else {
	 	verybadspan++;
	}
}

if (badspan != 0) {
	thError("%s: WARNING - (%d) out of (%d) spans did not fit in the working region", name, badspan, nspan);
}
if (verybadspan != 0) {
	thError("%s: WARNING - (%d) out of (%d) spans lay outside of the working region", name, verybadspan, nspan);
}

return(SH_SUCCESS);
}

RET_CODE convert_region_to_objmask(REGION *reg, OBJMASK **mask, THPIX flag) {
char *name = "convert_region_to_objmask";
if (reg == NULL) {
	thError("%s: WARNING - null input", name);
	if (mask != NULL) *mask = NULL;
	return(SH_SUCCESS);
}
if (mask == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (flag == (THPIX) 0.0) {
	thError("%s: ERROR - flag (%g) invoked", name, flag);
}
int nspan = 0;
int nrow = reg->nrow;
int ncol = reg->ncol;
int i, j, span_expired;
for (i = 0; i < nrow; i++) {
	span_expired = 1;
	THPIX *row_i = reg->rows_thpix[i];
	for (j = 0; j < ncol; j++) {
		if (row_i[j] == flag && span_expired) {
			nspan++;
			span_expired = 0;
		} else if (row_i[j] != flag) {
			span_expired = 1;
		}
	}
}
OBJMASK *wmask = phObjmaskNew(nspan);

int xmax, xmin, ymax, ymin;
int npix = 0;
int ispan = -1;
for (i = 0; i < nrow; i++) {
	int y, x1, x2;
	span_expired = 1;
	THPIX *row_i = reg->rows_thpix[i];
	for (j = 0; j < ncol; j++) {
		if (row_i[j] == flag && span_expired) {
			ispan++;
			y = i;
			x1 = j;
			if (ispan > 0) {
				xmin = MIN(xmin, x1);
				ymin = MIN(ymin, y);
				ymax = MIN(ymax, y);
			} else {
				xmin = x1;
				ymin = y;
				ymax = y;
			}
		} else if (row_i[j] != flag && !span_expired) {
			x2 = j - 1;
			span_expired = 1;
			wmask->s[ispan].y = y;
			wmask->s[ispan].x1 = x1;
			wmask->s[ispan].x2 = x2;
			npix += x2 - x1 + 1;
			xmax = MAX(xmax, x2);
		}
	}
}

if (ispan != nspan - 1) {
	thError("%s: ERROR - source code problem", name);
	return(SH_GENERIC_ERROR);
}

if (nspan == 0) {
	thError("%s: WARNING - empty mask", name);
}
wmask->nspan = nspan;
wmask->npix = npix;
wmask->cmin = xmin;
wmask->cmax = xmax;
wmask->rmin = ymin;
wmask->rmax = ymax;

*mask = wmask;
return(SH_SUCCESS);
}

RET_CODE analyze_reg(REGION *reg, OBJMASK *mask, THPIX *mean, THPIX *stddev, THPIX *variance) {
char *name = "analyze_reg";
if (reg == NULL) {
	thError("%s: ERROR - null input (reg)", name);
	return(SH_GENERIC_ERROR);
}
if (mean == NULL && stddev == NULL && variance == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
int nrow, ncol;
nrow = reg->nrow;
ncol = reg->ncol;

int npix;
long double total = 0.0, total2 = 0.0;
if (mask == NULL) {
	int i, j;
		for (i = 0; i < nrow; i++) {
		THPIX *row_i = reg->rows_thpix[i];
		for (j = 0; j < ncol; j++) {
			RESFL x = (RESFL) (row_i[j]);
			total += x;
			total2 += x * x;
		}
	}
	npix = nrow * ncol;
} else {
	int nspan = mask->nspan;
	SPAN *s;
	int mrrow0 = mask->row0 - reg->row0;
	int mrcol0 = mask->col0 - reg->col0;
	int i, j, col1, col2, ispan;
	for (ispan = 0; ispan < nspan; ispan++) {
		s = mask->s + ispan;
		i = s->y + mrrow0;
		if (i >= 0 && i < nrow) {
			THPIX *row_i = reg->rows_thpix[i];
			col1 = s->x1 + mrcol0;
			col2 = s->x2 + mrcol0;
			col1 = MAX(col1, 0);
			col2 = MIN(col2, ncol-1);
			for (j = col1; j <= col2; j++) {
				RESFL x = (RESFL) row_i[j];	
				total += x;
				total2 += x * x;
				npix++;
			}
		}
	}
}	

RESFL nfl = (RESFL) npix;
RESFL meanfl = total / nfl;
RESFL variancefl = (total2 - total * meanfl) / (nfl - (RESFL) 1.0);
RESFL stddevfl = sqrt(variancefl);

if (mean != NULL) *mean = (THPIX) meanfl;
if (variance != NULL) *variance = (THPIX) variancefl;
if (stddev != NULL) *stddev = (THPIX) stddevfl;

return(SH_SUCCESS);
}

RET_CODE output_residual_report(RESIDUAL_REPORT *res) {
char *name = "output_residual_report";
if (res == NULL) {
	printf("%s: null residual report \n", name);
	return(SH_SUCCESS);
}
int rbin, cbin, nbin;
rbin = res->rbin;
cbin = res->cbin;
nbin = rbin * cbin;
printf("%s: nbin (%4d, %4d x %4d) E[X] = %6g, DEV[X] = %6g, VAR[X] = %6g \n", name, nbin, rbin, cbin, res->mean, res->stddev, res->variance);
return(SH_SUCCESS);
}

RET_CODE output_residual_report_chain(CHAIN *reschain) {
char *name = "output_residual_report_chain";
if (reschain == NULL) {
	printf("%s: null (residual report) chain \n", name);
	return(SH_SUCCESS);
}
int i, n;
if ((n = shChainSize(reschain)) == 0) {
	printf("%s: empty (residual report) chain \n", name);
	return(SH_SUCCESS);
}

printf("%s: %4s  %3s  %3s  %10s  %10s  %10s \n", name, 
	"nbin", "rbin", "cbin", 
	"E[X]", "DEV[X]", "VAR[X]");
for (i = 0; i < n; i++) {
	RESIDUAL_REPORT *res = shChainElementGetByPos(reschain, i);
	int rbin, cbin, nbin;
	rbin = res->rbin;
	cbin = res->cbin;
	nbin = rbin * cbin;
	printf("%s: %4d %3d %3d %10.5e %10.5e %10.5e \n", name, 
		nbin, rbin, cbin, (float) res->mean, (float) res->stddev, (float) res->variance);
}

return(SH_SUCCESS);
}

	
