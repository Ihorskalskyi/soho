#include "thMathTypes.h"


VECT *thVectNew(int dim) {
  
  VECT *vec;

  vec = (VECT *) thCalloc(1, sizeof(VECT));
  vec->dim = dim;

  if (dim != 0) {
    vec->vec = (THPIX *) thCalloc(dim, sizeof(THPIX));
  } else {
    vec->vec = NULL;
  }

  vec->child = 0;
  return (vec);
}

RET_CODE thVectDel(VECT *vec) {
 
  if (vec == NULL) return(SH_SUCCESS);

  if (vec->vec != NULL && vec->child == 0) thFree(vec->vec);

  thFree(vec);

  return(SH_SUCCESS);

}


FUNC *thFuncNew() {
  
  FUNC *func;
  
  func = (FUNC *) thCalloc(1, sizeof(FUNC));

  func->name = thCalloc(MX_STRING_LEN, sizeof(char));

  func->row0 = 0;
  func->col0 = 0;

  /*
  func->indices = (int *) thCalloc(FUNC_N_INDICES, sizeof(int));
  func->pars = (THPIX *) thCalloc(FUNC_N_PARS, sizeof(THPIX));
  */

  func->indices = (int **) thCalloc(FUNC_N_INDICES, sizeof(int*));
  func->pars = (THPIX **) thCalloc(FUNC_N_PARS, sizeof(THPIX*));

   /* pointing indices in the right direction */
  int i = 0;

  if (i < FUNC_N_INDICES) {
    func->indices[i] = &(func->index0);
  }
  i++;
    if (i < FUNC_N_INDICES) {
    func->indices[i] = &(func->index1);
  }
  i++;
    if (i < FUNC_N_INDICES) {
    func->indices[i] = &(func->index2);
  }
  i++;
    if (i < FUNC_N_INDICES) {
    func->indices[i] = &(func->index3);
  }
  i++;
    if (i < FUNC_N_INDICES) {
    func->indices[i] = &(func->index4);
  }
  i++;
    if (i < FUNC_N_INDICES) {
    func->indices[i] = &(func->index5);
  }
  i++;
    if (i < FUNC_N_INDICES) {
    func->indices[i] = &(func->index6);
  }
  i++;
    if (i < FUNC_N_INDICES) {
    func->indices[i] = &(func->index7);
  }
  i++;
    if (i < FUNC_N_INDICES) {
    func->indices[i] = &(func->index8);
  }
  i++;
    if (i < FUNC_N_INDICES) {
    func->indices[i] = &(func->index9);
  }
  i++;
    if (i < FUNC_N_INDICES) {
    func->indices[i] = &(func->index10);
  }
  i++;
    
  /* pointing pars in the right direction */
  i = 0;
  if (i < FUNC_N_PARS) {
    func->pars[i] = &(func->par0);
  }
  i++;
    if (i < FUNC_N_PARS) {
    func->pars[i] = &(func->par1);
  }
  i++;
    if (i < FUNC_N_PARS) {
    func->pars[i] = &(func->par2);
  }
  i++;
    if (i < FUNC_N_PARS) {
    func->pars[i] = &(func->par3);
  }
  i++;
    if (i < FUNC_N_PARS) {
    func->pars[i] = &(func->par4);
  }
  i++;
    if (i < FUNC_N_PARS) {
    func->pars[i] = &(func->par5);
  }
  i++;
    if (i < FUNC_N_PARS) {
    func->pars[i] = &(func->par6);
  }
  i++;
    if (i < FUNC_N_PARS) {
    func->pars[i] = &(func->par7);
  }
  i++;
    if (i < FUNC_N_PARS) {
    func->pars[i] = &(func->par8);
  }
  i++;
    if (i < FUNC_N_PARS) {
    func->pars[i] = &(func->par9);
  }
  i++;
    if (i < FUNC_N_PARS) {
    func->pars[i] = &(func->par10);
  }
  i++;
    
  return(func);

}

RET_CODE thFuncDel(FUNC *func) {
  
  if (func == NULL) return (SH_SUCCESS);

  if (func->name != NULL) thFree(func->name);

  if (func->reg != NULL) shRegDel(func->reg);

  if (func->indices != NULL) thFree(func->indices);

  if (func->pars != NULL) thFree(func->pars);

  thFree(func);

  return(SH_SUCCESS);

}
  

RET_CODE thFuncPut(FUNC *func, 
		   int row0, int col0, 
		   int rowc, int colc,
		   int *indices, THPIX *pars,  
		   REGION *reg){


  if (func == NULL) {
    thError("%s: function provided is NULL -- no change was made");
    return(SH_GENERIC_ERROR);
  }

  func->row0 = row0;
  func->col0 = col0;
  func->rowc = rowc;
  func->colc = colc;

  int i;

  /* The following substitutions are done assuming that the 
     pointer arrays func->indices and func->pars are already 
     set to point to func->index[0..10] and func->par[0..10]
  */
  if (indices != NULL) {
    for (i = 0; i < FUNC_N_INDICES; i++) {
      *(func->indices[i]) = indices[i];
    }
  }
  
  if (pars != NULL) {
    for (i = 0; i < FUNC_N_PARS; i++) {
      *(func->pars[i]) = pars[i];
    }
  }
  
  if (reg != NULL) {
    func->reg = reg;
  }
  
  return(SH_SUCCESS);

}
  
