/* Standard C Libs */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dervish.h"
#include "strings.h"
/* SDSS construction */
#include "dervish.h"
#include "phObjc.h"
#include "phPhotoFitsIO.h"
/* SOHO Libs */
#include "sohoEnv.h"
#include "shDebug.h"

RET_CODE shCpFpObjc(char *infile, char *outfile);
OBJC *shReadFpObjc(char *file, RET_CODE *status);
RET_CODE shWriteFpObjc(char *file, OBJC *objc);


/**/

RET_CODE shCpFpObjc(char *infile, char *outfile)
{
  RET_CODE status;

  OBJC *ObjcList = NULL;
  
  ObjcList = shReadFpObjc(infile, &status);
  if (status != SH_SUCCESS) {
    phObjcDel(ObjcList, 1);
    return(status);
  }
  status = shWriteFpObjc(outfile, ObjcList);
  phObjcDel(ObjcList, 1);
  return(status);
}

OBJC *
shReadFpObjc(char *file, RET_CODE *status)
{
  OBJC *objc = NULL;
  PHFITSFILE *fd = NULL;
  int ncolor, j;
  
  int phStatus = 0;
  
  /*allocate file type */
  if ((fd = phFitsBinTblOpen(file,		/* file to open */
			     0,		/* open for read (flag == 0) */
			     NULL))		/* additional header info */
      == NULL) {
    *status = SH_GENERIC_ERROR;
    return(NULL);
  }
  if (fd == NULL){
    *status =  SH_GENERIC_ERROR;
    return(NULL);
  }
 /* reading ncolor */ 
  objc = phObjcNew(10);
  phFitsBinTblRowRead(fd, objc);
  shErrStackClear();
  phFitsBinTblRowUnread(fd);
  ncolor = objc->ncolor;
  phObjcDel(objc, 1);
  
 /* read the whole obj list */
  objc = phObjcNew(ncolor);
  for (j = 0; j < ncolor; j++){
    objc->color[j] = phObject1New();
    objc->color[j]->region = shRegNew("", 0, 0, TYPE_PIX);
  }
  phFitsBinTblRowRead(fd, objc);
  
  /* deallocate file type */
  phStatus = phFitsBinTblClose(fd);
  if (phStatus != 0) {
   *status =  SH_GENERIC_ERROR;
  } else {
    *status = SH_SUCCESS;
  }
  
  /* return the object list read */
  return(objc);
}

RET_CODE shWriteFpObjc(char *file, OBJC *objc)
{
  PHFITSFILE *fd = NULL;
  int phStatus = 0;

  /*allocate file type */
  if ((fd = phFitsBinTblOpen(file,		/* file to open */
			     1,		/* write (flag == 1; => creat)*/
			     NULL))	/* additional header info */
      == NULL) {
    return(SH_GENERIC_ERROR);
  }
  if (fd == NULL){
    return(SH_GENERIC_ERROR);
  }
  
  /* write to the file */

  phStatus = phFitsBinTblRowWrite(fd, objc);
  if (phStatus != 0) {
    phFitsBinTblClose(fd);
    return(SH_GENERIC_ERROR);
  }

  /* deallocate file type */
  phStatus = phFitsBinTblClose(fd);
  if (phStatus != 0) {
    return(SH_GENERIC_ERROR);
  }
 
  /* successful exit */
  return(SH_SUCCESS);
}
  
