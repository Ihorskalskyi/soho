#include "thPardiv.h"

int thGetIndexThpixPardivLinear(THPIX pval, THPIX *pdiv, int npdiv, RET_CODE *status) {

char *name = "thGetIndexThpixPardivLinear"; 

if (pdiv == NULL) {
	thError("%s: ERROR - null division characteristics", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(-1);
	}
int index;
index  = (int) ((npdiv * (pval - pdiv[0])) / pdiv[1]);

thInsertStatus(status, SH_SUCCESS);
return(index);

}

int thGetIndexIntPardivLinear(int pval, int *pdiv, int npdiv, RET_CODE *status) {
    
char *name = "thGetIndexIntPardivLinear";
   
if (pdiv == NULL) {
	thError("%s: ERROR - null division characteristics", name);
        thInsertStatus(status, SH_GENERIC_ERROR);
        return(-1);
        }
int index; 
index  = (int) ((npdiv * (pval - pdiv[0])) / pdiv[1]);

thInsertStatus(status, SH_SUCCESS);
return(index);

}


int thGetIndexThpixPardivLogarithmic(THPIX pval, THPIX *pdiv, int npdiv, RET_CODE *status) {

char *name = "thGetIndexThpixPardivLogarithmic";

if (pdiv == NULL) {
        thError("%s: ERROR - null division characteristics", name);
        thInsertStatus(status, SH_GENERIC_ERROR);
        return(-1);
        }
if (pdiv[1] <= 0 || pdiv[0] == 0.0 || (pval / pdiv[0]) <= 0) {
	thError("%s: ERROR - problem defining the log division", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(-1);
	}

int index;
index  = (int) ((npdiv * log(pval / pdiv[0])) / log(pdiv[1]));

thInsertStatus(status, SH_SUCCESS);
return(index);

}


int thGetIndexIntPardivLogarithmic(int pval, int *pdiv, int npdiv, RET_CODE *status) {

char *name = "thGetIndexThpixPardivLogarithmic";

if (pdiv == NULL) {
        thError("%s: ERROR - null division characteristics", name);
        thInsertStatus(status, SH_GENERIC_ERROR);
        return(-1);
        }

THPIX x;
x = ((THPIX) pval) / (THPIX) pdiv[0];
if (pdiv[1] <= 0 || pdiv[0] == 0.0 || x <= 0) {
        thError("%s: ERROR - problem defining the log division", name);
        thInsertStatus(status, SH_GENERIC_ERROR);
        return(-1);
        }

int index;
index  = (int) ((npdiv * log(x)) / log(pdiv[1]));

thInsertStatus(status, SH_SUCCESS);
return(index);
}

int thGetValIntLinear(int divindex, int *pdiv, int npdiv, RET_CODE *status) {

char *name = "thGetValIntLinear";

if (pdiv == NULL) {
	thError("%s: ERROR - null division", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return((int) sqrt(-1));
}

int pval;
pval = (divindex * pdiv[1]) / npdiv + pdiv[0];
thInsertStatus(status, SH_SUCCESS);
return(pval);
}

THPIX thGetValThpixLinear(int divindex,THPIX *pdiv, int npdiv, RET_CODE *status) {

char *name = "thGetValThpixLinear";

if (pdiv == NULL) {
        thError("%s: ERROR - null division", name);
        thInsertStatus(status, SH_GENERIC_ERROR);
        return(sqrt(-1));
}

THPIX pval;
pval = (divindex * pdiv[1]) / npdiv + pdiv[0];
thInsertStatus(status, SH_SUCCESS);             
return(pval);
}
 
int thGetValIntLogarithmic(int divindex, int *pdiv, int npdiv, RET_CODE *status) {

char *name = "thGetValIntLogarithmic";

if (pdiv == NULL) {
        thError("%s: ERROR - null division", name);
        thInsertStatus(status, SH_GENERIC_ERROR);
        return((int) sqrt(-1));
}

int pval;
pval = (int) (exp((divindex * log(pdiv[1])) / npdiv) * pdiv[0]);
thInsertStatus(status, SH_SUCCESS);
return(pval);
}

THPIX thGetValThpixLogarithmic(int divindex, THPIX *pdiv, int npdiv, RET_CODE *status) {

char *name = "thGetValIntLogarithmic";

if (pdiv == NULL) {
        thError("%s: ERROR - null division", name);
        thInsertStatus(status, SH_GENERIC_ERROR);
        return((int) sqrt(-1));
}

THPIX pval;
pval = (exp((divindex * log(pdiv[1])) / npdiv) * pdiv[0]);
thInsertStatus(status, SH_SUCCESS);
return(pval);
}

int thGetValIntPardiv(int divindex, int *pdiv, int npdiv, THPARDIV_TYPE divtype, RET_CODE *status) {
char *name = "thGetValIntPardiv";
int x;
RET_CODE status2;
switch (divtype) {
	case LINEAR_DIV:
	x = thGetValIntLinear(divindex, pdiv, npdiv, &status2);
	if (status2 != SH_SUCCESS) {
		thError("%s: could not find the value in division", name);
		}
	thInsertStatus(status, status2);
	return(x);
	break;
	case LOGARITHMIC_DIV:
	x = thGetValIntLogarithmic(divindex, pdiv, npdiv, &status2);
	if (status2 != SH_SUCCESS) {
		thError("%s: could not find the value in division", name);
		}
	thInsertStatus(status, status2);
	return(x);
	break;
	default:
	thError("%s: ERROR - unsupported division type", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return((int) sqrt(-1.0));
	break;
	}

/* you should never reach here */
thError("%s: ERROR - source code problem", name);
thInsertStatus(status, SH_GENERIC_ERROR);
return((int) sqrt(-1));
}

THPIX thGetValThpixPardiv(int divindex, THPIX *pdiv, int npdiv, THPARDIV_TYPE divtype, RET_CODE *status) {
char *name = "thGetValThpixPardiv";
THPIX x;
RET_CODE status2;
switch (divtype) {
	case LINEAR_DIV:
	x = thGetValThpixLinear(divindex, pdiv, npdiv, &status2);
	if (status2 != SH_SUCCESS) {
		thError("%s: could not find the value in division", name);
		}
	thInsertStatus(status, status2);
	return(x);
	break;
	case LOGARITHMIC_DIV:
	x = thGetValThpixLogarithmic(divindex, pdiv, npdiv, &status2);
	if (status2 != SH_SUCCESS) {
		thError("%s: could not find the value in division", name);
		}
	thInsertStatus(status, status2);
	return(x);
	break;
	default:
	thError("%s: ERROR - unsupported division type", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return((int) sqrt(-1.0));
	break;
	}

/* you should never reach here */
thError("%s: ERROR - source code problem", name);
thInsertStatus(status, SH_GENERIC_ERROR);
return((int) sqrt(-1));
}


