	/* sersic component */
	THPIX I_coresersic, J_coresersic;
	THPIX xc_coresersic, yc_coresersic;
	THPIX re_coresersic, e_coresersic, phi_coresersic;
	THPIX k_coresersic, n_coresersic, N_coresersic;
	THPIX a_coresersic, b_coresersic, c_coresersic;
	THPIX ue_coresersic, E_coresersic;
	THPIX v_coresersic, w_coresersic;
	THPIX V_coresersic, W_coresersic;
	THPIX mcount_coresersic;
	THPIX counts_coresersic;
	THPIX flux_coresersic, mag_coresersic;

	/* standard errors */
	THPIX IErr_coresersic, JErr_coresersic;
	THPIX xcErr_coresersic, ycErr_coresersic;
	THPIX reErr_coresersic, eErr_coresersic, phiErr_coresersic;
	THPIX kErr_coresersic, nErr_coresersic, NErr_coresersic;
	THPIX aErr_coresersic, bErr_coresersic, cErr_coresersic;
	THPIX ueErr_coresersic, EErr_coresersic;
	THPIX vErr_coresersic, wErr_coresersic;
	THPIX VErr_coresersic, WErr_coresersic;
	THPIX countsErr_coresersic;
	THPIX fluxErr_coresersic, magErr_coresersic;


