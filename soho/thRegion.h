#ifndef THREGION_H
#define THREGION_H

#include "thModelTypes.h"
#include "thRegionTypes.h"
#include "thAlgorithmTypes.h"
#include "thPsfTypes.h"
#include "thMleTypes.h"
#include "thPsf.h"

RET_CODE thRegion1ConvolveWithPsf(THREGION1 *threg, PSF_CONVOLUTION_TYPE force_psf_type);
RET_CODE thRegion2ConvolveWithPsf(THREGION2 *threg, PSF_CONVOLUTION_TYPE force_psf_type);
RET_CODE thRegion3ConvolveWithPsf(THREGION3 *threg, PSF_CONVOLUTION_TYPE force_psf_type);
RET_CODE thRegConvolveWithPsf(THREGION *threg);
RET_CODE thRegConvolveWithNullPsf(THREGION *threg);

THREGION *thRegNewFromModelElem(MODEL_ELEM *me, PSF_CONVOLUTION_INFO *psf_info);
THREGION *thRegNewFromModelName(char *mname, PSF_CONVOLUTION_INFO *psf_info);
RET_CODE thRegRenewFromModelElem(THREGION *threg, MODEL_ELEM *me, PSF_CONVOLUTION_INFO *psf_info);
RET_CODE thRegRenewFromModelName(THREGION *threg, char *mname, PSF_CONVOLUTION_INFO *psf_info);

RET_CODE thRegion1GetCorners(THREGION1 *threg, int *rows, int *cols);
RET_CODE thRegion2GetCorners(THREGION2 *threg, int *rows, int *cols);
RET_CODE thRegion3GetCorners(THREGION3 *threg, int *rows, int *cols);
RET_CODE thRegGetCorners(THREGION *threg, int *rows, int *cols);
RET_CODE thRegOverlapPix(THREGION *threg1, THREGION *threg2, FITSTAT fitstat1, FITSTAT fitstat2, MEMFL *pix);
RET_CODE thRegConvertToshReg(THREGION *threg, REGION *shreg);
RET_CODE thRegionConvertToshReg(THREGION *threg, REGION *reg);
RET_CODE thRegion1ConvertToshReg(THREGION1 *threg, REGION *reg);
RET_CODE thRegion2ConvertToshReg(THREGION2 *threg, REGION *reg);
RET_CODE thRegion3ConvertToshReg(THREGION3 *threg, REGION *reg);
RET_CODE shRegAddshRegCentered(REGION *reg1, THPIX amp, REGION *reg2);
RET_CODE shRegCountBadPixel(REGION *reg, int *nbadpixel);

#endif
