#ifndef THCTRANSFORM_H
#define THCTRANSFORM_H

#include "string.h"
#include "dervish.h"
#include "thConsts.h"
#include "thBasic.h"

#include "thCTransformTypes.h"

RET_CODE thCTransformInit(CDESCRIPTION desc, void *input);
RET_CODE thCTransformGetDesc(CDESCRIPTION *desc);

RET_CODE thCTransform(void *q, char *pname);
RET_CODE thCTransformFromPhoto(void *q, char *pname);
RET_CODE thCTransformFromSource(void *q, char *pname, CDESCRIPTION sDesc);

RET_CODE SetCTransformStaticPars(char *pname, THPIX value);
RET_CODE initCTransformPars(void *input);
#endif

