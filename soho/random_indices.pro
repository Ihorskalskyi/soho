FUNCTION random_indices, len, n_in, seed = seed, error = error
name = "random_indices"

if (len le 0) then begin
	print, name, ": ERROR - (length of array) should be positive"
	error = -1
	return, [-1]
endif
if (n_in le 0) then begin
	print, name, ": ERROR - number of indices desired should be positive"
	error = -1
	return, [-1]
endif
if (n_in gt len) then begin
	print, name, ": ERROR - number of indices desired should be less than or equal to the size of the array"
	error = -1
	return, [-1]
endif
if (n_in eq len) then begin
	inds = lindgen(len)
	error = 0
	return, inds
endif

swap = n_in gt len/2
IF swap THEN n = len-n_in ELSE n = n_in
inds = LonArr(n, /NOZERO)
M = n
     
WHILE n GT 0 DO BEGIN
	inds[M-n] = Long( RandomU(seed, n)*len )
        inds = inds[Sort(inds)]
        u = Uniq(inds)
        n = M-n_elements(u)
        inds[0] = inds[u]
ENDWHILE

IF swap THEN inds = Where(Histogram(inds,MIN=0,MAX=len-1) EQ 0)
RETURN, inds
end


