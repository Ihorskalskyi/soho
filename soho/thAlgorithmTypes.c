#include "thAlgorithmTypes.h"

NODEMEMORY *thNodememoryNew() {
NODEMEMORY *x = thCalloc(1, sizeof(NODEMEMORY));
return(x);
}

void thNodememoryDel(NODEMEMORY *x) {
if (x != NULL) thFree(x);
return;
}

ADJ_MATRIX *thAdjMatrixNew() {
	int nmax = ALGORITHM_NOBJ_DEFAULT;
	ADJ_MATRIX *adj = thCalloc(1, sizeof(ADJ_MATRIX));
	MEMFL **matrix = thCalloc(nmax, sizeof(MEMFL *));
	MEMFL **wmatrix = thCalloc(nmax, sizeof(MEMFL *));
	MEMFL *memspace = thCalloc(nmax, sizeof(MEMFL));
	MEMFL *degree = thCalloc(nmax, sizeof(MEMFL));
	MEMSTAT *memstat = thCalloc(nmax, sizeof(MEMSTAT));
	FITSTAT *fitstat = thCalloc(nmax, sizeof(FITSTAT));
	int *list = thCalloc(nmax, sizeof(int));
	NODEMEMORY *nmlist = thCalloc(nmax, sizeof(NODEMEMORY));
	int i;
	matrix[0] = thCalloc(nmax * nmax, sizeof(MEMFL));
	wmatrix[0] = thCalloc(nmax * nmax, sizeof(MEMFL));
	for (i = 0; i < nmax; i++) {	
		matrix[i] = (MEMFL *) (matrix[0]) + i*nmax;
		wmatrix[i] = (MEMFL *) (wmatrix[0]) + i*nmax;
	}
	adj->matrix = matrix;
	adj->wmatrix = wmatrix;
	adj->memspace = memspace;
	adj->memstat = memstat;
	adj->fitstat = fitstat;
	adj->list = list;
	adj->node_mem_list = nmlist;
	adj->degree = degree;
	adj->n = 0;
	adj->nmax = ALGORITHM_NOBJ_DEFAULT;
	return(adj);
}

void thAdjMatrixDel(ADJ_MATRIX *adj) {
	char *name = "thAdjMatrixDel";
	if (adj == NULL) return;
	if ((adj->matrix != NULL && adj->nmax <= 0) || 
		(adj->matrix == NULL && adj->nmax > 0)) {
		thError("%s: ERROR - improperly allocated (adj_matrix)", name);	
		return;
	}
	if ((adj->wmatrix != NULL && adj->nmax <= 0) || 
		(adj->wmatrix == NULL && adj->nmax > 0)) {
		thError("%s: ERROR - improperly allocated (adj_matrix)", name);	
		return;
	}
	if ((adj->list != NULL && adj->nmax <= 0) || 
		(adj->list == NULL && adj->nmax > 0)) {
		thError("%s: ERROR - improperly allocated (adj_matrix)", name);	
		return;
	}
	if ((adj->node_mem_list != NULL && adj->nmax <= 0) || 
		(adj->node_mem_list == NULL && adj->nmax > 0)) {
		thError("%s: ERROR - improperly allocated (adj_matrix)", name);	
		return;
	}
	if ((adj->degree != NULL && adj->nmax <= 0) || 
		(adj->degree == NULL && adj->nmax > 0)) {
		thError("%s: ERROR - improperly allocated (adj_matrix)", name);	
		return;
	}
	if ((adj->memspace != NULL && adj->nmax <= 0) || 
		(adj->memspace == NULL && adj->nmax > 0)) {
		thError("%s: ERROR - improperly allocated (adj_matrix)", name);	
		return;
	}
	if ((adj->memstat != NULL && adj->nmax <= 0) || 
		(adj->memstat == NULL && adj->nmax > 0)) {
		thError("%s: ERROR - improperly allocated (adj_matrix)", name);	
		return;
	}
	MEMFL **wmatrix = adj->wmatrix;
	MEMFL **matrix = adj->matrix;
	MEMFL *degree = adj->degree;
	MEMFL *memspace = adj->memspace;
	MEMSTAT *memstat = adj->memstat;
	int *list = adj->list;
	NODEMEMORY *nmlist = adj->node_mem_list;
	int nmax;
	nmax = adj->nmax;
	if (matrix != NULL) thFree(matrix[0]);
	if (wmatrix != NULL) thFree(wmatrix[0]);

	if (wmatrix != NULL) thFree(wmatrix);
	if (matrix != NULL) thFree(matrix);
	if (memspace != NULL) thFree(memspace);
	if (memstat != NULL) thFree(memstat);
	if (list != NULL) thFree(list);
	if (nmlist != NULL) thFree(nmlist);
	if (degree != NULL) thFree(degree);

	thFree(adj);
	return;
}

RET_CODE thAdjMatrixRenew(ADJ_MATRIX *adj, int n) {
char *name = "thAdjMatrixRenew";
if (adj == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (n < 0) {
	thError("%s: ERROR - improper matrix size (%d)", name, n);
	return(SH_GENERIC_ERROR);
}
int nmax = adj->nmax;
MEMFL **wmatrix = adj->wmatrix;
MEMFL **matrix = adj->matrix;
MEMFL *degree = adj->degree;
MEMFL *memspace = adj->memspace;
MEMSTAT *memstat = adj->memstat;
FITSTAT *fitstat = adj->fitstat;

int *list = adj->list;
NODEMEMORY *nmlist = adj->node_mem_list;
	
if ((nmax <= 0 && matrix != NULL) || (matrix == NULL && nmax > 0)) {
	thError("%s: ERROR - improperly allocated (adj_matrix) as input", name);
	return(SH_GENERIC_ERROR);
}	
if ((wmatrix != NULL && adj->nmax <= 0) || 
	(wmatrix == NULL && adj->nmax > 0)) {
	thError("%s: ERROR - improperly allocated (adj_matrix)", name);	
	return(SH_GENERIC_ERROR);
}
if ((list != NULL && adj->nmax <= 0) || 
	(list == NULL && adj->nmax > 0)) {
	thError("%s: ERROR - improperly allocated (adj_matrix)", name);	
	return(SH_GENERIC_ERROR);
}
if ((nmlist != NULL && adj->nmax <= 0) || 
	(nmlist == NULL && adj->nmax > 0)) {
	thError("%s: ERROR - improperly allocated (adj_matrix)", name);	
	return(SH_GENERIC_ERROR);
}
if ((degree != NULL && adj->nmax <= 0) || 
	(degree == NULL && adj->nmax > 0)) {
	thError("%s: ERROR - improperly allocated (adj_matrix)", name);	
	return(SH_GENERIC_ERROR);
}
if ((memspace != NULL && adj->nmax <= 0) || 
	(memspace == NULL && adj->nmax > 0)) {
	thError("%s: ERROR - improperly allocated (adj_matrix)", name);	
	return(SH_GENERIC_ERROR);
}
if ((memstat != NULL && adj->nmax <= 0) || 
	(memstat == NULL && adj->nmax > 0)) {
	thError("%s: ERROR - improperly allocated (adj_matrix)", name);	
	return(SH_GENERIC_ERROR);
}
if ((fitstat != NULL && adj->nmax <= 0) || 
	(fitstat == NULL && adj->nmax > 0)) {
	thError("%s: ERROR - improperly allocated (adj_matrix)", name);
	return(SH_GENERIC_ERROR);
}

if (nmax < n) {
	int i;
	thFree(matrix[0]);
	thFree(wmatrix[0]);

	if (wmatrix != NULL) thFree(wmatrix);
	if (matrix != NULL) thFree(matrix);
	if (memspace != NULL) thFree(memspace);
	if (memstat != NULL) thFree(memstat);
	if (fitstat != NULL) thFree(fitstat);
	if (list != NULL) thFree(list);
	if (nmlist != NULL) thFree(nmlist);
	if (degree != NULL) thFree(degree);

	matrix = thCalloc(n, sizeof(MEMFL *));
	wmatrix = thCalloc(n, sizeof(MEMFL *));
	memspace = thCalloc(n, sizeof(MEMFL));
	degree = thCalloc(n, sizeof(MEMFL));
	memstat = thCalloc(n, sizeof(MEMSTAT));
	fitstat = thCalloc(n, sizeof(FITSTAT));
	list = thCalloc(n, sizeof(int));
	nmlist = thCalloc(n, sizeof(NODEMEMORY));

	matrix[0] = thCalloc(n * n, sizeof(MEMFL));
	wmatrix[0] = thCalloc(n * n, sizeof(MEMFL));
	for (i = 0; i < n; i++) {	
		matrix[i] = (MEMFL *)matrix[0] + i*n;
		wmatrix[i] = (MEMFL *)wmatrix[0] + i*n;
	}

	adj->matrix = matrix;
	adj->wmatrix = wmatrix;
	adj->memspace = memspace;
	adj->memstat = memstat;
	adj->fitstat = fitstat;
	adj->list = list;
	adj->node_mem_list = nmlist;
	adj->degree = degree;
	adj->n = n;
	adj->nmax = n;
	return(SH_SUCCESS);
}

adj->n = n;

return(SH_SUCCESS);
}
	
ALGORITHM_STEP *thAlgorithmStepNew() {
ALGORITHM_STEP *x = thCalloc(1, sizeof(ALGORITHM_STEP));
x->node1 = NON_EXISTENT_NODE;
x->node2 = NON_EXISTENT_NODE;
x->action = UNKNOWN_ALGORITHM_ACTION;
x->runtime = INDEFINITE_TIMES;
return(x);
}

void thAlgorithmStepDel(ALGORITHM_STEP *x) {
if (x != NULL) thFree(x);
return;
}

ALGORITHM *thAlgorithmNew() {
ALGORITHM *x = thCalloc(1, sizeof(ALGORITHM));
int nmax = ALGORITHM_NSTEP_DEFAULT;
x->nmax = nmax;
x->steps = thCalloc(nmax, sizeof(ALGORITHM_STEP));
x->nsteps = 0;
return(x);
}

void thAlgorithmDel(ALGORITHM *x) {
if (x == NULL) return;
if (x->steps != NULL) thFree(x->steps);
thFree(x);
return;
}

RET_CODE thAlgorithmGetNsteps(ALGORITHM *alg, int *n) {
char *name = "thAlgorithmGetNsteps";
if (alg == NULL && n == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
}
if (alg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (n == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
int nmax = alg->nmax;
int nsteps = alg->nsteps;
if (nmax <= 0) {
	thError("%s: ERROR - improperly allocated (algorithm) - (nmax = %d)", name, nmax);
	return(SH_GENERIC_ERROR);
}
if (nsteps < 0 || nsteps > nmax) {
	thError("%s: ERROR - improperly allocated (algorithm) - (nsteps = %d) while (nmax = %d)", name, nsteps, nmax);
	return(SH_GENERIC_ERROR);
}
*n = nsteps;
return(SH_SUCCESS);
}


ALGORITHM_STEP *thAlgorithmStepGetByPos(ALGORITHM *alg, int pos, RET_CODE *status) {
char *name = "thAlgorithmStepGetByPos";
if (alg == NULL) {
	thError("%s: ERROR - null input (algorithm)", name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
}
int nsteps = alg->nsteps;
int nmax = alg->nmax;
if (nsteps < 0 || nsteps > nmax) {
	thError("%s: ERROR - improperly allocated (algorithm) (nsteps = %d) while (nmax = %d)", name, nsteps, nmax);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
}
if (pos < 0 || pos >= nsteps) {
	thError("%s: ERROR - (pos = %d) out of range for (nsteps = %d)", name, pos, nsteps);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
}

if (status != NULL) *status = SH_SUCCESS;
return(alg->steps + pos);
}

RET_CODE get_memory_total(ALGORITHM *algorithm, MEMDBLE *memory_total) {
char *name = "get_memory_total";
if (algorithm == NULL) {
	thError("%s: ERROR - null algorithm", name);
	return(SH_GENERIC_ERROR);
}
if (memory_total == NULL) {
	thError("%s: WARNING - null placeholder for output", name);
	return(SH_SUCCESS);
}

*memory_total = algorithm->memory_total;
return(SH_SUCCESS);
}

RET_CODE add_node_to_algorithm(ALGORITHM *algorithm, int node, int runtime) {
char *name = "add_node_to_algorithm";
if (algorithm == NULL) {
	thError("%s: ERROR - null algorithm", name);
	return(SH_GENERIC_ERROR);
}
if (algorithm->steps == NULL) {
	thError("%s: ERROR - improperly allocated (algorithm)", name);
	return(SH_GENERIC_ERROR);
}
if (algorithm->nsteps >= algorithm->nmax) {
	thError("%s: ERROR - reached the end of the allocated memory for (algorithm)", name);
	return(SH_GENERIC_ERROR);
}
if (runtime < 0 && runtime != INDEFINITE_TIMES) {
	thError("%s: ERROR - unsupported runtime (%d)", name, runtime);
	return(SH_GENERIC_ERROR);
} else if (runtime == 0) {
	thError("%s: WARNING - runtime (%d) makes null action", name, runtime);
}

ALGORITHM_STEP *step = algorithm->steps + algorithm->nsteps;
step->node1 = (ALGORITHM_NODE) node;
step->node2 = NON_EXISTENT_NODE;
step->action = NODE_LOAD;
step->runtime = runtime;
step->runtime_source = runtime;
algorithm->nsteps++;

return(SH_SUCCESS);
}

RET_CODE delete_node_from_algorithm(ALGORITHM *algorithm, int node, int runtime) {
char *name = "delete_node_from_algorithm";
if (algorithm == NULL) {
	thError("%s: ERROR - null algorithm", name);
	return(SH_GENERIC_ERROR);
}
if (algorithm->steps == NULL) {
	thError("%s: ERROR - improperly allocated (algorithm)", name);
	return(SH_GENERIC_ERROR);
}
if (algorithm->nsteps >= algorithm->nmax) {
	thError("%s: ERROR - reached the end of the allocated memory for (algorithm) (nsteps = %d, nmax = %d)", name, algorithm->nsteps, algorithm->nmax);
	return(SH_GENERIC_ERROR);
}
if (runtime < 0 && runtime != INDEFINITE_TIMES) {
	thError("%s: ERROR - unsupported runtime (%d)", name, runtime);
	return(SH_GENERIC_ERROR);
} else if (runtime == 0) {
	thError("%s: WARNING - runtime (%d) makes null action", name, runtime);
}

ALGORITHM_STEP *step = algorithm->steps + algorithm->nsteps;
step->node1 = (ALGORITHM_NODE) node;
step->node2 = NON_EXISTENT_NODE;
step->action = NODE_UNLOAD;
step->runtime = runtime;
step->runtime_source = runtime;
algorithm->nsteps++;

return(SH_SUCCESS);
}

RET_CODE add_edge_to_algorithm(ALGORITHM *algorithm, int node1, int node2) {
char *name = "add_edge_to_algorithm";
if (algorithm == NULL) {
	thError("%s: ERROR - null algorithm", name);
	return(SH_GENERIC_ERROR);
}
if (algorithm->steps == NULL) {
	thError("%s: ERROR - improperly allocated (algorithm)", name);
	return(SH_GENERIC_ERROR);
}
if (algorithm->nsteps >= algorithm->nmax) {
	thError("%s: ERROR - reached the end of the allocated memory for (algorithm)", name);
	return(SH_GENERIC_ERROR);
}
ALGORITHM_STEP *step = algorithm->steps + algorithm->nsteps;
step->node1 = (ALGORITHM_NODE) node1;
step->node2 = (ALGORITHM_NODE) node2;
step->action = EDGE_COMPUTE;
step->runtime = INDEFINITE_TIMES;
step->runtime_source = INDEFINITE_TIMES;
algorithm->nsteps++;

return(SH_SUCCESS);
}

RET_CODE add_pcost_to_algorithm(ALGORITHM *algorithm) {
char *name = "add_pcost_to_algorithm";
if (algorithm == NULL) {
	thError("%s: ERROR - null algorithm", name);
	return(SH_GENERIC_ERROR);
}
if (algorithm->steps == NULL) {
	thError("%s: ERROR - improperly allocated (algorithm)", name);
	return(SH_GENERIC_ERROR);
}
if (algorithm->nsteps >= algorithm->nmax) {
	thError("%s: ERROR - reached the end of the allocated memory for (algorithm)", name);
	return(SH_GENERIC_ERROR);
}
ALGORITHM_STEP *step = algorithm->steps + algorithm->nsteps;
step->node1 = NON_EXISTENT_NODE; 
step->node2 = NON_EXISTENT_NODE;
step->action = COST_COMPUTE;
step->runtime = INDEFINITE_TIMES;
step->runtime_source = INDEFINITE_TIMES;
algorithm->nsteps++;

return(SH_SUCCESS);
}

RET_CODE init_model_algorithm(ALGORITHM *algorithm) {
char *name = "init_model_algorithm"; 
if (algorithm == NULL) {
	thError("%s: ERROR - null algorithm", name);
	return(SH_GENERIC_ERROR);
}
if (algorithm->steps == NULL) {
	thError("%s: ERROR - improperly allocated (algorithm)", name);
	return(SH_GENERIC_ERROR);
}
if (algorithm->nsteps >= algorithm->nmax) {
	thError("%s: ERROR - reached the end of the allocated memory for (algorithm)", name);
	return(SH_GENERIC_ERROR);
}
ALGORITHM_STEP *step = algorithm->steps + algorithm->nsteps;
step->node1 = NON_EXISTENT_NODE;
step->node2 = NON_EXISTENT_NODE;
step->action = INIT_MODEL;
algorithm->nsteps++;

return(SH_SUCCESS);
}


RET_CODE add_node_to_model_algorithm(ALGORITHM *algorithm, int node) {
char *name = "add_node_to_model_algorithm";
if (algorithm == NULL) {
	thError("%s: ERROR - null algorithm", name);
	return(SH_GENERIC_ERROR);
}
if (algorithm->steps == NULL) {
	thError("%s: ERROR - improperly allocated (algorithm)", name);
	return(SH_GENERIC_ERROR);
}
if (algorithm->nsteps >= algorithm->nmax) {
	thError("%s: ERROR - reached the end of the allocated memory for (algorithm)", name);
	return(SH_GENERIC_ERROR);
}
ALGORITHM_STEP *step = algorithm->steps + algorithm->nsteps;
step->node1 = (ALGORITHM_NODE) node;
step->node2 = NON_EXISTENT_NODE;
step->action = NODE_ADDTOMODEL;
step->runtime = INDEFINITE_TIMES;
step->runtime_source = INDEFINITE_TIMES;
algorithm->nsteps++;

return(SH_SUCCESS);
}

RET_CODE thAlgorithmCopy(ALGORITHM *s, ALGORITHM *t) {
char *name = "thAlgorithmCopy";
if (s == NULL || t == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
} else if (s == t) {
	thError("%s: ERROR - cannot copy an (algorithm) ontp itself", name);
	return(SH_GENERIC_ERROR);
} 

t->memory_total = s->memory_total;
if (t->steps == NULL) {
	int nmax = ALGORITHM_NSTEP_DEFAULT;
	t->steps = thCalloc(nmax, sizeof(ALGORITHM_STEP));
	t->nmax = nmax;
}
if (t->nmax != 0) {
	thError("%s: WARNING - (steps) chain already set in (target algorithm)", name);
	thFree(t->steps);
	int nmax = ALGORITHM_NSTEP_DEFAULT;
	t->steps = thCalloc(nmax, sizeof(ALGORITHM_STEP));
	t->nmax = nmax;
}
int i, n;
n = s->nsteps;
ALGORITHM_STEP *tsteps = t->steps;
ALGORITHM_STEP *ssteps = s->steps;
for (i = 0; i < n; i++) {
	ALGORITHM_STEP *sstep = ssteps + i;
	ALGORITHM_STEP *tstep = tsteps + i;	
	RET_CODE status = thAlgorithmStepCopy(sstep, tstep);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not make copy of (step at i = %d)", name, i);
		return(status);
	}
}
t->nsteps = n;
t->parent = s;
return(SH_SUCCESS);
}

RET_CODE thAlgorithmStepCopy(ALGORITHM_STEP *s, ALGORITHM_STEP *t) {
char *name = "thAlgorithmStepCopy";
if (s == NULL || t == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
} else if (s == t) {
	thError("%s: ERROR - cannot copy an (algorithm_step) ontp itself", name);
	return(SH_GENERIC_ERROR);
}

memcpy(t, s, sizeof(ALGORITHM_STEP));
return(SH_SUCCESS);
}

RET_CODE thAlgorithmRefresh(ALGORITHM *s) {
char *name = "thAlgorithmRefresh";
if (s == NULL) {
	thError("%s: ERROR - null input placeholder", name);
	return(SH_GENERIC_ERROR);
} 

int i, n;
n = s->nsteps;
ALGORITHM_STEP *ssteps = s->steps;
for (i = 0; i < n; i++) {
	ALGORITHM_STEP *sstep = ssteps + i;
	RET_CODE status = thAlgorithmStepRefresh(sstep);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not refresh (step at i = %d)", name, i);
		return(status);
	}
}
return(SH_SUCCESS);
}

RET_CODE thAlgorithmStepRefresh(ALGORITHM_STEP *s) {
char *name = "thAlgorithmStepRefresh";
if (s == NULL) {
	thError("%s: WARNING - null input passed", name);
	return(SH_SUCCESS);
}
if (s->runtime_source < 0 && s->runtime_source != INDEFINITE_TIMES) {
	thError("%s: WARNING discovered (runtime_source = %d) in (step)", name, s->runtime_source);
}
s->runtime = s->runtime_source;
return(SH_SUCCESS);
}
