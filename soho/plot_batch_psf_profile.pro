pro plot_batch_psf_profile, run_list = run_list, camcol_list = camcol_list, boundary_list = boundary_list, nprof = nprof, arcsec = arcsec, $
	doprofile = doprofile, doimage = doimage, doreplace = doreplace, $
	pixelized = pixelized, azimuthal = azimuthal, sectional = sectional, $
	logscale = logscale, $
	debug = debug, dbinary = dbinary, verbose = verbose, logfile = logfile

DEFSYSV, '!PIXELSIZE', 0.396, 1 ;; pixel size in arcsec

if (n_elements(nprof) eq 0) then nprof = 5
;; dir = "./psf-glue-norm-1-correct-binary"
;; srcdir="/u/khosrow/thesis/opt/soho/single-galaxy/images/psf-analysis-norm-1-correct-binary"

;; dir = "./psf-glue-norm-0"
;; srcdir="/u/khosrow/thesis/opt/soho/single-galaxy/images/psf-analysis-norm-0"

if (n_elements(run_list) eq 0) then $
run_list=[4797, 2700, 2703, 2708, 2709, 2960, 3565, 3434, 3437, 2968, 4207, 1742, 4247, 4252, 4253, 4263, 4288, 1863, 1887, 2570, 2578, 2579, 109, 125, 211, 240, 241, 250, 251, 256, 259, 273, 287, 297, 307, 94, 5036, 5042, 5052, 2873, 21, 24, 1006, 1009, 1013, 1033, 1040, 1055, 1056, 1057, 4153, 4157, 4158, 4184, 4187, 4188, 4191, 4192, 2336, 2886, 2955, 3325, 3354, 3355, 3360, 3362, 3368, 2385, 2506, 2728, 4849, 4858, 4868, 4874, 4894, 4895, 4899, 4905, 4927, 4930, 4933, 4948, 2854, 2855, 2856, 2861, 2662, 3256, 3313, 3322, 4073, 4198, 4203, 3384, 3388, 3427, 3430, 4128, 4136, 4145, 2738, 2768, 2820, 1752, 1755, 1894, 2583, 2585, 2589, 2591, 2649, 2650, 2659, 2677, 3438, 3460, 3458, 3461, 3465, 7674, 7183]
;;nrun = n_elements(run_list)
;; run_list = run_list[1L:nrun-1L]
nrun = n_elements(run_list)

if (n_elements(camcol_list) eq 0) then $
camcol_list = [1, 2, 3, 4]

root="."
srcroot = "/u/khosrow/thesis/opt/soho/single-galaxy/images"

if (keyword_set(doreplace)) then begin
	dir_array = ["psf-glue-norm-2", "psf-glue-norm-3"]
	srcdir_array = ["psf-analysis-norm-2", "psf-analysis-norm-3"]
	suffix_array = ["-norm-2", "-norm-3"]
	title_array = ["(constrained, replaced)", "(unconstrained, replaced)"]
endif else begin
	dir_array = ["psf-glue-norm-0", "psf-glue-norm-1"]
	srcdir_array = ["psf-analysis-norm-0", "psf-analysis-norm-1"]
	suffix_array = ["-norm-0", "-norm-1"]
	title_array = ["(constrained)", "(unconstrained)"]
endelse 

if (keyword_set(dbinary)) then begin
	boundary_array = [0L, 5L, 10L, 15L, 19L] 
endif else begin
	if (n_elements(boundary_list) ne 0) then boundary_array = boundary_list else boundary_array = lindgen(20)
endelse 

inboundary_array = boundary_array
outboundary_array = boundary_array


ndir = n_elements(dir_array)
;; ndir = 1
nboundary = n_elements(boundary_array)

if (n_elements(logfile) eq 0) then logfile = "./plot-batch-psf-profile.log"
ulogfile = logfile

seed0 = total(10 ^ (lindgen(6) * 2) * bin_date(systime(0)))

for idir = 0, ndir-1L, 1 do begin
for iboundary = 0, nboundary - 1L, 1 do begin
for jboundary = 0, nboundary - 1L, 1 do begin

	dir=root+"/"+dir_array[idir]
	srcdir=srcroot+"/"+srcdir_array[idir]
	suffix = suffix_array[idir]
	inboundary = inboundary_array[iboundary]
	outboundary = outboundary_array[jboundary]
	inboundary_str = strtrim(string(inboundary, "(I)"), 2)
	outboundary_str = strtrim(string(outboundary, "(I)"), 2)
	boundary_str = inboundary_str+"x"+outboundary_str

	dobundle = 1
	doerror = 0
	dosinglepanel = 0

	xrange = [0, 104]
	if (keyword_set(arcsec)) then xrange = xrange * !PIXELSIZE
	seed = seed0
	yrange = [1.1E-6, 1.1E0]
	doxlog = 0
	doylog = 1

	if (outboundary gt inboundary + 1) then begin

	boundary = [inboundary, outboundary]
	inboundary_size_str = strtrim(string(!PIXELSIZE * inboundary, format = "(F10.1)"), 2)	
	outboundary_size_str = strtrim(string(!PIXELSIZE * outboundary, format = "(F10.1)"), 2)
	

	if (keyword_set(doprofile)) then begin

		lsize = 1L
		;; doing the profiles
		filename = "psf-analysis-profiles-b" + boundary_str + suffix + ".eps"
		if (keyword_set(arcsec)) then begin
			title = "PSF profile for r_{in} = "+inboundary_size_str+" arcsec, r_{out} =  "+outboundary_size_str+" arcsec "+title_array[idir]
		endif else begin
			title = "PSF profile for r_{in} = "+inboundary_str+" pixels, r_{out} = "+outboundary_str+" pixels "+title_array[idir]
		endelse
		epsfile = dir + "/" + filename
		table_draw_psf_profle, dir = srcdir, epsfile = epsfile, $
			camcol_list = camcol_list, run_list = run_list, $
			yrange = yrange, xrange = xrange, xlog = doxlog, ylog = doylog, $
			boundary = boundary, nprof = nprof, seed = seed, $
			mtitle = textoidl(title), lsize = lsize, $
			pixelized = keyword_set(pixelized), azimuthal = keyword_set(azimuthal), sectional = keyword_set(sectional), $
			debug = keyword_set(debug), verbose = keyword_set(verbose), $
			arcsec = keyword_set(arcsec), logfile = ulogfile

	endif

	if (keyword_set(doimage)) then begin

		;; doing the images	
		filename = "psf-analysis-images-b" + boundary_str + suffix + ".eps"
		if (keyword_set(arcsec)) then begin
			title = "PSF for r_{in} = "+inboundary_size_str+" arcsec, r_{out} =  "+outboundary_size_str+" arcsec "+title_array[idir]
		endif else begin
			title = "PSF for r_{in} = "+inboundary_str+" pixels, r_{out} = "+outboundary_str+" pixels "+title_array[idir]
		endelse
		epsfile = dir + "/" + filename
		image_xrange = [-max(xrange), max(xrange)]
		table_draw_psf_image, dir = srcdir, epsfile = epsfile, $
			camcol_list = camcol_list, run_list = run_list, $
			yrange = yrange, xrange = image_xrange, xlog = doxlog, ylog = doylog, $
			boundary = boundary, nprof = nprof, seed = seed, /doprofile, $
			mtitle = textoidl(title), /klprof, /gluedprof, /extprof, $
			pixelized = keyword_set(pixelized), azimuthal = keyword_set(azimuthal), sectional = keyword_set(sectional), $
			logscale = keyword_set(logscale), $
			debug = keyword_set(debug), verbose = keyword_set(verbose), $
			arcsec = keyword_set(arcsec), logfile = ulogfile

	endif

	endif ;; boundary if

endfor
endfor
endfor

return
end
