#include "thMSkyTypes.h"

SBINDEX *thSbindexNew() {
SBINDEX *sbindex;
sbindex = (SBINDEX *) thCalloc(1, sizeof(SBINDEX));
return(sbindex);
}

void thSbindexDel(SBINDEX *sbindex) {
if (sbindex == NULL) return;
thFree(sbindex);
}

SKYPARS *thSkyparsNew() {
SKYPARS *sp;
sp = (SKYPARS *) thCalloc(1, sizeof(SKYPARS));
return(sp);
}

void thSkyparsDel(SKYPARS *sp) {
if (sp == NULL) return;
thFree(sp);
return;
}

void thSkyparsReset(SKYPARS *sky) {
if (sky == NULL) return;
memset(sky, '\0', sizeof(SKYPARS));
return;
}


RET_CODE thDumpSkyparsWrite(FILE *fil, SKYPARS *sp) {
char *name = "thSkyparsWrite";
if (fil == NULL) {
	thError("%s: ERROR - null output file", name);
	return(SH_GENERIC_ERROR);
}
if (sp == NULL) {
	thError("%s: ERROR - null (skypars) to output", name);
	return(SH_GENERIC_ERROR);
}
#if 0
RET_CODE status;
status = thGeneralWrite(fil, sp, "SKYPARS");
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not write the (skypars) using general function", name);
	return(status);
}
#endif

char *fmt = "I_s00 = %7.2g, I_z00 = %7.2g, \n I_s01 = %7.2g, I_s02 = %7.2g, \n I_s11 = %7.2g, I_s12 = %7.2g, I_s10 = %7.2g \n I_s21 = %7.2g, I_s22 = %7.2g, I_s20 = %7.2g \n";
fprintf(fil, fmt, 
	sp->I_s00, sp->I_z00, 
	sp->I_s01, sp->I_s02, 
	sp->I_s11, sp->I_s12, sp->I_s10, 
	sp->I_s21, sp->I_s22, sp->I_s20);
return(SH_SUCCESS);
}

#if 0
RET_CODE thGeneralWrite(FILE *fil, void *x, char *xtype) {
char *name = "thGeneralWrite";
if (fil == NULL) {
	thError("%s: ERROR - null output file", name);
	return(SH_GENERIC_ERROR);
}
if (x == NULL) {
	thError("%s: ERROR - null variable to export", name);
	return(SH_GENERIC_ERROR);
}
if (xtype == NULL || strlen(xtype) == 0) {
	thError("%s: ERROR - null or empty variable name", name);
	return(SH_GENERIC_ERROR);
}
SCHEMA *s = shSchemaGet(xtype);
if (s == NULL) {
	thError("%s: ERROR - unsupported (type = '%s') - no schema found", name, xtype);
	return(SH_GENERIC_ERROR);
}
char fmt[MX_STRING_LEN], xstr[MAX_STRING_LEN];
memset(fmt, '\0', MX_STRING_LEN * sizeof(char));
memset(fmt, '\0', MX_STRING_LEN * sizeof(char));
int nelem = s->nelem;
if (nelem == 0) {
	thError("%s: WARNING - (type = '%s') doesn't seem to have any elements", name, xtype);
	return(SH_SUCCESS);
}
int i;
for (i = 0; i < nelem; i++) {
	float y, **yy = NULL;
	yy = (float *) shElemGet(x, se, NULL);
	y = *yy;
}
return(SH_GENERIC_ERROR);
}
#endif

SKYCOV *thSkycovNew() {
SKYCOV *x = thCalloc(1, sizeof(SKYCOV));
return(x);
}

void thSkycovDel(SKYCOV *x) {
if (x == NULL) return;
thFree(x);
return;
}


