#ifndef THRESIDUAL_H
#define THRESIDUAL_H

#include "thResidualTypes.h"

RET_CODE thCompileResidualReport(RESIDUAL_REPORT *res);
RET_CODE thCompileResidualReportChain(CHAIN *reschain);

RET_CODE mask_weight_info(RESIDUAL_REPORT *res, int *npix, THPIX *npix_eff);
RET_CODE bin_mask(OBJMASK *mask, int rbin, int cbin, OBJMASK **binned_mask, int *binpix, THPIX *binpix_eff);
RET_CODE bin_reg(REGION *reg, int rbin, int cbin, REGION *binned_reg);

RET_CODE convert_objmask_to_region(OBJMASK *mask, REGION *reg, THPIX flag);
RET_CODE convert_region_to_objmask(REGION *reg, OBJMASK **mask, THPIX flag);
RET_CODE analyze_reg(REGION *reg, OBJMASK *mask, THPIX *mean, THPIX *stddev, THPIX *variance);

RET_CODE output_residual_report(RESIDUAL_REPORT *res);
RET_CODE output_residual_report_chain(CHAIN *reschain);
#endif

