#ifndef THMGPROFILE_H
#define THMGPROFILE_H

#include "thProfile.h"
#include "thMGalaxyTypes.h"
#include "thRegionTypes.h"
#include "thCTransform.h"
	
RET_CODE MGInitProfile(CDESCRIPTION cdesc,void *input);

/* pixel functions that are used when damping the weight of the core - otherwise should have been static */
THPIX gaussian(PIXPOS *p, void *q);
THPIX deV(PIXPOS *p, void *q);
THPIX Exp(PIXPOS *p, void *q);
THPIX Pl(PIXPOS *p, void *q);
THPIX sersic(PIXPOS *p, void *q);
THPIX coresersic(PIXPOS *p, void *q);

THPIX gaussianStep(PIXPOS *p, void *q);
THPIX deVStep(PIXPOS *p, void *q);
THPIX ExpStep(PIXPOS *p, void *q);
THPIX PlStep(PIXPOS *p, void *q);
THPIX sersicStep(PIXPOS *p, void *q);

/* this equates the central pixel to 1 this function is never used */
RET_CODE starProf(void *q, THREGION *reg, THPIX *mcount, int index);
RET_CODE deltaProf(void *q, REGION *reg, THPIX *mcount);

RET_CODE gaussianProf(void *q, THREGION *reg, THPIX *mcount, int index);
RET_CODE DgaussianDaProf(void *q, THREGION *reg, int index);
RET_CODE DgaussianDbProf(void *q, THREGION *reg, int index);
RET_CODE DgaussianDcProf(void *q, THREGION *reg, int index);
RET_CODE DgaussianDxcProf(void *q, THREGION *reg, int index);
RET_CODE DgaussianDycProf(void *q, THREGION *reg, int index);
RET_CODE DgaussianDphiProf(void *q, THREGION *reg, int index);
RET_CODE DgaussianDeProf(void *q, THREGION *reg, int index);
RET_CODE DgaussianDreProf(void *q, THREGION *reg, int index);
RET_CODE DgaussianDueProf(void *q, THREGION *reg, int index);
RET_CODE DgaussianDEProf(void *q, THREGION *reg, int index);

RET_CODE nullProf(void *q, THREGION *reg, int index);

RET_CODE deVProf(void *q, THREGION *reg, THPIX *mcount, int index);
RET_CODE DdeVDaProf(void *q, THREGION *reg, int index);
RET_CODE DdeVDbProf(void *q, THREGION *reg, int index);
RET_CODE DdeVDcProf(void *q, THREGION *reg, int index);
RET_CODE DdeVDxcProf(void *q, THREGION *reg, int index);
RET_CODE DdeVDycProf(void *q, THREGION *reg, int index);
RET_CODE DdeVDphiProf(void *q, THREGION *reg, int index);
RET_CODE DdeVDeProf(void *q, THREGION *reg, int index);
RET_CODE DdeVDreProf(void *q, THREGION *reg, int index);
RET_CODE DdeVDueProf(void *q, THREGION *reg, int index);
RET_CODE DdeVDEProf(void *q, THREGION *reg, int index);
RET_CODE DdeVDvProf(void *q, THREGION *reg, int index);
RET_CODE DdeVDwProf(void *q, THREGION *reg, int index);
RET_CODE DdeVDVProf(void *q, THREGION *reg, int index);
RET_CODE DdeVDWProf(void *q, THREGION *reg, int index);

RET_CODE DDdeVDreDreProf(void *q, THREGION *reg, int index);
RET_CODE DDdeVDueDueProf(void *q, THREGION *reg, int index);

RET_CODE ExpProf(void *q, THREGION *reg, THPIX *mcount, int index);
RET_CODE DExpDaProf(void *q, THREGION *reg, int index);
RET_CODE DExpDbProf(void *q, THREGION *reg, int index);
RET_CODE DExpDcProf(void *q, THREGION *reg, int index);
RET_CODE DExpDxcProf(void *q, THREGION *reg, int index);
RET_CODE DExpDycProf(void *q, THREGION *reg, int index);
RET_CODE DExpDphiProf(void *q, THREGION *reg, int index);
RET_CODE DExpDeProf(void *q, THREGION *reg, int index);
RET_CODE DExpDreProf(void *q, THREGION *reg, int index);
RET_CODE DExpDueProf(void *q, THREGION *reg, int index);
RET_CODE DExpDEProf(void *q, THREGION *reg, int index);
RET_CODE DExpDvProf(void *q, THREGION *reg, int index);
RET_CODE DExpDwProf(void *q, THREGION *reg, int index);
RET_CODE DExpDVProf(void *q, THREGION *reg, int index);
RET_CODE DExpDWProf(void *q, THREGION *reg, int index);

RET_CODE PlProf(void *q, THREGION *reg, THPIX *mcount, int index);
RET_CODE DPlDaProf(void *q, THREGION *reg, int index);
RET_CODE DPlDbProf(void *q, THREGION *reg, int index);
RET_CODE DPlDcProf(void *q, THREGION *reg, int index);
RET_CODE DPlDxcProf(void *q, THREGION *reg, int index);
RET_CODE DPlDycProf(void *q, THREGION *reg, int index);
RET_CODE DPlDphiProf(void *q, THREGION *reg, int index);
RET_CODE DPlDeProf(void *q, THREGION *reg, int index);
RET_CODE DPlDreProf(void *q, THREGION *reg, int index);
RET_CODE DPlDueProf(void *q, THREGION *reg, int index);
RET_CODE DPlDEProf(void *q, THREGION *reg, int index);
RET_CODE DPlDvProf(void *q, THREGION *reg, int index);
RET_CODE DPlDwProf(void *q, THREGION *reg, int index);
RET_CODE DPlDVProf(void *q, THREGION *reg, int index);
RET_CODE DPlDWProf(void *q, THREGION *reg, int index);

RET_CODE DDPlDreDreProf(void *q, THREGION *reg, int index);
RET_CODE DDPlDueDueProf(void *q, THREGION *reg, int index);

RET_CODE sersicProf(void *q, THREGION *reg, THPIX *mcount, int index);
RET_CODE DsersicDaProf(void *q, THREGION *reg, int index);
RET_CODE DsersicDbProf(void *q, THREGION *reg, int index);
RET_CODE DsersicDcProf(void *q, THREGION *reg, int index);
RET_CODE DsersicDxcProf(void *q, THREGION *reg, int index);
RET_CODE DsersicDycProf(void *q, THREGION *reg, int index);
RET_CODE DsersicDphiProf(void *q, THREGION *reg, int index);
RET_CODE DsersicDeProf(void *q, THREGION *reg, int index);
RET_CODE DsersicDreProf(void *q, THREGION *reg, int index);
RET_CODE DsersicDueProf(void *q, THREGION *reg, int index);
RET_CODE DsersicDEProf(void *q, THREGION *reg, int index);
RET_CODE DsersicDvProf(void *q, THREGION *reg, int index);
RET_CODE DsersicDwProf(void *q, THREGION *reg, int index);
RET_CODE DsersicDVProf(void *q, THREGION *reg, int index);
RET_CODE DsersicDWProf(void *q, THREGION *reg, int index);
RET_CODE DsersicDnProf(void *q, THREGION *reg, int index);
RET_CODE DsersicDNProf(void *q, THREGION *reg, int index);
RET_CODE DDsersicDreDreProf(void *q, THREGION *reg, int index);
RET_CODE DDsersicDueDueProf(void *q, THREGION *reg, int index);

RET_CODE coresersicProf(void *q, THREGION *reg, THPIX *mcount, int index);
RET_CODE DcoresersicDxcProf(void *q, THREGION *reg, int index);
RET_CODE DcoresersicDycProf(void *q, THREGION *reg, int index);
RET_CODE DcoresersicDphiProf(void *q, THREGION *reg, int index);
RET_CODE DcoresersicDeProf(void *q, THREGION *reg, int index);
RET_CODE DcoresersicDreProf(void *q, THREGION *reg, int index);
RET_CODE DcoresersicDueProf(void *q, THREGION *reg, int index);
RET_CODE DcoresersicDEProf(void *q, THREGION *reg, int index);
RET_CODE DcoresersicDnProf(void *q, THREGION *reg, int index);
RET_CODE DcoresersicDNProf(void *q, THREGION *reg, int index);

RET_CODE DcoresersicDdeltaProf(void *q, THREGION *reg, int index);
RET_CODE DcoresersicDDProf(void *q, THREGION *reg, int index);
RET_CODE DcoresersicDgammaProf(void *q, THREGION *reg, int index);
RET_CODE DcoresersicDGProf(void *q, THREGION *reg, int index);
RET_CODE DcoresersicDrbProf(void *q, THREGION *reg, int index);
RET_CODE DcoresersicDubProf(void *q, THREGION *reg, int index);

RET_CODE coresersic_mcount_calculator(void *q, THPIX *mcount, int silent);
/* THREGION specifics */
RET_CODE StarSpecifyRegion(void *q, PSF_CONVOLUTION_INFO *psf_info, THREGION *threg, int index, int reprefab);
RET_CODE GalaxySpecifyRegion(void *q, PSF_CONVOLUTION_INFO *psf_info, THREGION *threg, int index, int reprefab,  
				void *specify_reg_pars);
RET_CODE GaussianGalaxySpecifyRegion(void *q, PSF_CONVOLUTION_INFO *psf_info, THREGION *threg, int index, int reprefab);

RET_CODE NullGalaxySpecifyRegion(void *q, PSF_CONVOLUTION_INFO *psf_info, THREGION *threg, int index, int reprefab);
RET_CODE deVGalaxySpecifyRegion(void *q, PSF_CONVOLUTION_INFO *psf_info, THREGION *threg, int index, int reprefab);
RET_CODE ExpGalaxySpecifyRegion(void *q, PSF_CONVOLUTION_INFO *psf_info, THREGION *threg, int index, int reprefab);
RET_CODE PlGalaxySpecifyRegion(void *q, PSF_CONVOLUTION_INFO *psf_info, THREGION *threg, int index, int reprefab);
RET_CODE sersicGalaxySpecifyRegion(void *q, PSF_CONVOLUTION_INFO *psf_info, THREGION *threg, int index, int reprefab);
RET_CODE coresersicGalaxySpecifyRegion(void *q, PSF_CONVOLUTION_INFO *psf_info, THREGION *threg, int index, int reprefab);

/* PSF stuff */
THPIX g_exp(void *q, THPIX cutoff);
THPIX g_deV(void *q, THPIX cutoff);
THPIX g_pl(void *q, THPIX cutoff);
THPIX g_sersic(void *q, THPIX cutoff);
THPIX g_coresersic(void *q, THPIX cutoff);

RET_CODE p_exp();
RET_CODE p_deV();
RET_CODE p_pl();
RET_CODE p_sersic();
RET_CODE p_coresersic();

RET_CODE derive_profile_region_chars(void *q, char *qname, void *g_ptr, 
			THPIX cutoff, 
			int *row0, int *col0, int *nrow, int *ncol);
RET_CODE finalize_working_region_chars(int row0_in, int col0_in, int nrow_in, int ncol_in, int margine_in,
				int row0_out, int col0_out, int nrow_out, int ncol_out, int margine_out, 
				int *row0_f, int *col0_f, int *nrow_f, int *ncol_f);

RET_CODE MGPsfInfoUpdate(void *q, char *qname, void *g_ptr, void *p_ptr, PSF_CONVOLUTION_INFO *psf_info);
RET_CODE MGPsfExp(void *q, PSF_CONVOLUTION_INFO *psf_info);
RET_CODE MGPsfDeV(void *q, PSF_CONVOLUTION_INFO *psf_info);
RET_CODE MGPsfPl(void *q, PSF_CONVOLUTION_INFO *psf_info);
RET_CODE MGPsfSersic(void *q, PSF_CONVOLUTION_INFO *psf_info);
RET_CODE MGPsfCoresersic(void *q, PSF_CONVOLUTION_INFO *psf_info);
RET_CODE finalize_inner_region_chars(int row0_in, int col0_in, int nrow_in, int ncol_in,
				int row_out, int col0_out, int nrow_out, int ncol_out,
				int *row0, int *col0, int *nrow, int *ncol);

#endif
