#!/bin/bash
#MY_COMMENT
name=MY_SHELL_FILE.sh
uzipfile=MY_LOCATION_FOR_STORED_ZIP_FILES/fpBIN-004797-u3-0120.fit.gz
gzipfile=MY_LOCATION_FOR_STORED_ZIP_FILES/fpBIN-004797-g3-0120.fit.gz
rzipfile=MY_LOCATION_FOR_STORED_ZIP_FILES/fpBIN-004797-r3-0120.fit.gz
izipfile=MY_LOCATION_FOR_STORED_ZIP_FILES/fpBIN-004797-i3-0120.fit.gz
zzipfile=MY_LOCATION_FOR_STORED_ZIP_FILES/fpBIN-004797-z3-0120.fit.gz

ufile=MY_LOCATION_FOR_UNZIPPED_FILES/fpBIN-004797-u3-0120.fit
gfile=MY_LOCATION_FOR_UNZIPPED_FILES/fpBIN-004797-g3-0120.fit
rfile=MY_LOCATION_FOR_UNZIPPED_FILES/fpBIN-004797-r3-0120.fit
ifile=MY_LOCATION_FOR_UNZIPPED_FILES/fpBIN-004797-i3-0120.fit
zfile=MY_LOCATION_FOR_UNZIPPED_FILES/fpBIN-004797-z3-0120.fit

declare -a zipfiles=($uzipfile $gzipfile $rzipfile $izipfile $zzipfile)
declare -a outfiles=($ufile $gfile $rfile $ifile $zfile)
declare -a inflated=(no no no no no)
declare -a existed=(no no no no no)


ufpCfile=MY_LOCATION_FOR_FPC_FILES/fpC-004797-u3-0120.fit
gfpCfile=MY_LOCATION_FOR_FPC_FILES/fpC-004797-g3-0120.fit
rfpCfile=MY_LOCATION_FOR_FPC_FILES/fpC-004797-r3-0120.fit
ifpCfile=MY_LOCATION_FOR_FPC_FILES/fpC-004797-i3-0120.fit
zfpCfile=MY_LOCATION_FOR_FPC_FILES/fpC-004797-z3-0120.fit

ufpCCfile=MY_LOCATION_FOR_FPCC_FILES/fpCC-004797-u3-0120.fit
gfpCCfile=MY_LOCATION_FOR_FPCC_FILES/fpCC-004797-g3-0120.fit
rfpCCfile=MY_LOCATION_FOR_FPCC_FILES/fpCC-004797-r3-0120.fit
ifpCCfile=MY_LOCATION_FOR_FPCC_FILES/fpCC-004797-i3-0120.fit
zfpCCfile=MY_LOCATION_FOR_FPCC_FILES/fpCC-004797-z3-0120.fit

declare -a fpCfiles=($ufpCfile $gfpCfile $rfpCfile $ifpCfile $zfpCfile)
declare -a fpCCfiles=($ufpCCfile $gfpCCfile $rfpCCfile $ifpCCfile $zfpCCfile)

nband=5

echo "$name: evaluating if files needs to be expanded"

for (( i=0; i<$nband; i++ ))
do

zipfile=${zipfiles[$i]}
destination=${outfiles[$i]}

if [ -e $destination ]; then 
existed[$i]=yes
rm -f $destination
fi
if [[ ! -e $destination  && -e $zipfile ]]; then
gunzip < $zipfile > $destination
inflated[$i]=yes
fi

done

echo "$name: inflated status: "
echo "$name: ${inflated[*]}"

echo "$name: executing the binary"
echo "$name: MY_FOLDER/MY_EXECUTABLE --MY_CONDOR_FLAG MY_CONDOR_FLAG_VALUE"

MY_FOLDER/MY_EXECUTABLE --MY_CONDOR_FLAG MY_CONDOR_FLAG_VALUE

echo "$name: deleting unzipped files"

for (( i=0; i<$nband; i++ ))
do

zipfile=${zipfiles[$i]}
destination=${outfiles[$i]}

if [[ ${existed[$i]} == no && ${inflated[$i]} == yes ]]; then
rm -f $destination
fi

done

echo "$name: zipping flat field files if requested"

for (( i=0; i<$nband; i++ ))
do

fpCfile=${fpCfiles[$i]}
fpCCfile=${fpCCfiles[$i]}

if [[ yes == yes && -e $fpCfile ]]; then
gzip $fpCfile
fi
if [[ yes == yes && -e $fpCCfile ]]; then
gzip $fpCCfile
fi

done


#end of condor shell script
