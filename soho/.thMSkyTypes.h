#ifndef THMSKYTYPES_H
#define THMSKYTYPES_H

#include "dervish.h"
#include "thSkyTypes.h"

typedef struct sbindex {
	FL32 I; 
	int irow, icol; /* for single amplifiers it starts from (0, 0) otherwise it include (-1, 0) as well */
	int mrow, mcol;
	CHAIN *ampl; /* amplifier information */
	} SBINDEX;

typedef struct skypars {
	FL32 I_s00, I_z00;
	FL32 I_s01, I_s02, I_s03, I_s04;
	FL32 I_s11, I_s12, I_s13, I_s14, I_s10;
	FL32 I_s21, I_s22, I_s23, I_s24, I_s20;
	FL32 I_s31, I_s32, I_s33, I_s34, I_s30;
	FL32 I_s41, I_s42, I_s43, I_s44, I_s40;
	FL32 I_s51, I_s52, I_s53, I_s54, I_s50;

	FL32 IErr_s00, IErr_z00;
	FL32 IErr_s01, IErr_s02, IErr_s03, IErr_s04;
	FL32 IErr_s11, IErr_s12, IErr_s13, IErr_s14, IErr_s10;
	FL32 IErr_s21, IErr_s22, IErr_s23, IErr_s24, IErr_s20;
	FL32 IErr_s31, IErr_s32, IErr_s33, IErr_s34, IErr_s30;
	FL32 IErr_s41, IErr_s42, IErr_s43, IErr_s44, IErr_s40;
	FL32 IErr_s51, IErr_s52, IErr_s53, IErr_s54, IErr_s50;

	FL32 chisq_model;
	int namp_model;
} SKYPARS; /* pragma SCHEMA */

typedef struct skycov {
	FL32 cov [50][50];
} SKYCOV;

SBINDEX *thSbindexNew();
void thSbindexDel(SBINDEX *sbindex);

SKYPARS  *thSkyparsNew();
void thSkyparsDel(SKYPARS *sp);
void thSkyparsReset(SKYPARS *sky);

RET_CODE thDumpSkyparsWrite(FILE *fil, SKYPARS *sp);

SKYCOV *thSkycovNew();
void thSkycovDel(SKYCOV *x);

#endif



