#ifndef THMODELTYPES_H
#define THMODELTYPES_H

#include <string.h>
#include "thConsts.h"
#include "thBasic.h"
#include "thObjcTypes.h"
#include "thPsfTypes.h"
#include "thRegionTypes.h"

typedef enum forceclass {
	CLASSIFY, FORCECLASSIFY, UNCLASSIFY, N_CLASSIFY, UNKNOWN_FLAG
	} FORCECLASS;

typedef enum construction_mode {
	UNKNOWN_CONSTRUCTION, WHOLEREGION, PIXELINTEGRAL, N_CONSTRUCTION_MODE
	} CONSTRUCTION_MODE;

typedef struct thfunc_elem {
	void *f;
	char *pname; /* name of the parameter w.r.t. which this function is calculated as a derivative */
	char *pname2; /* name of the second parameter */
} FUNC_ELEM;

typedef struct thmodel_elem {

	char *mname; /* name of the model */
	CHAIN *fs; /* constructor functions, f, Df */
	CHAIN *DDfs; /* constructor function for DDf */
	TYPE ptype; /* type of the input parameter */
	char *pname; /* type name of the input parameter */
	void *init; /* initiator of the parameter from PHOTO properties */
	void *transform; /* transformer of coordinate description to all other ones */
	/* the following was added to accomodate extended THREGION definition */
	void *reg_generator; /* only needed of cmode below is pixel_integration */
	void *psf_info_generator; /* a function that receives the parameter and returns psf_info */
	void *psf_mask_generator; /* a function that receives the parameter and index and returns psf mask */
	void *amplitude_specifier; /* if regtype is THREGION2_TYPE then this sets up the amplitudes */
	CONSTRUCTION_MODE cmode; /* construction mode - are the function f's above region-functions or pixel-ones */ 
	THREGION_TYPE regtype; /* the type of region that should be constructed */
	PSF_CONVOLUTION_TYPE convtype;  /* PSF convolution type */
	int ncomponent; /* number of components in the model */
	PSF_CONVOLUTION_INFO *psf_info; /* sample psf_info structure for this model */	
} MODEL_ELEM; /* pragma IGNORE */

typedef struct thobjc_elem {
	
	/* the following is needed only if the type is a derivative of another type - for non-derivatives the candidate type is PHPROP */
	char *candidatename;
	THOBJCTYPE candidatetype;
	
	char *classname; /* class name */	
	THOBJCTYPE classtype; /* hexadecimal object type */	
	/* classifier - 
	gets the candidate object properties (or PHPROPs) and returns 1 if the object belongs to this class 
	*/
	void * classifier;
	CHAIN *ms; /* component models - can be a referral to a PHOTO par */
	
	TYPE ptype;  /* the type of the paramter that is used to describe this object */
	char *pname; /* the type string for the parameter ths is used to describe this object */ 
	/* compactor - gets the candidate properties (or those of the submodels if the candidate is PHPROP and returns a structure of type ptype */
	void *compactor;

	CHAIN *pcs; /* parameter cost function and its derivative */
	
} OBJC_ELEM; /* pragma IGNORE */

typedef struct thpcost_elem {
	char *classname; /* object class name */
	THOBJCTYPE classtype; /* hexadecimal object type */
	char *rname1, *rname2; /* the derivative w.r.t. these paramter names are calculated */	
	void * pcost_function;
} PCOST_ELEM; /* pragma IGNORE */

#define thFuncElemNew thFunc_elemNew
#define thFuncElemDel thFunc_elemDel

#define thModelElemNew thModel_elemNew
#define thModelElemDel thModel_elemDel

#define thObjcElemNew thObjc_elemNew
#define thObjcElemDel thObjc_elemDel

#define thPCostElemNew thPcost_elemNew
#define thPCostElemDel thPcost_elemDel
#define thPCostElemNewFromName thPcost_elemNew

FUNC_ELEM *thFuncElemNew(void *f, char *pname, char *pname2);
void thFuncElemDel(FUNC_ELEM *felem);

MODEL_ELEM *thModelElemNew(char *mname, void *f0, char *pname, 
			CONSTRUCTION_MODE cmode, THREGION_TYPE regtype, 
			PSF_CONVOLUTION_TYPE convtype, int ncomponent, 
			void *init, 
			void *reg_generator, 
			void *psf_info_generator, 
			void *psf_mask_generator, 
			void *amplitude_specifier);
void thModelElemDel(MODEL_ELEM *melem); 

RET_CODE thModelElemGet(MODEL_ELEM *me, char **mname, TYPE *ptype, char **pname,  
		void **init, 
		void **reg_generator, 
		void **psf_info_generator, 
		void **psf_mask_generator, 
		void *amplitude_specifier, 
		CONSTRUCTION_MODE *cmode, THREGION_TYPE *regtype, 
		PSF_CONVOLUTION_TYPE *convtype, 
		int *ncomponent);

RET_CODE thModelElemGetFunc(MODEL_ELEM *melem, void **f0);
RET_CODE thModelElemAddDeriv2(MODEL_ELEM *melem, char *rname1, char *rname2, void *DDf);
RET_CODE thModelElemAddDeriv(MODEL_ELEM *melem, char *rname, void *Df); /* adds the derivate function to the list 
												of functions */
RET_CODE thModelElemGetDeriv2(MODEL_ELEM *melem, char *rname1, char *rname2, void **DDf);
RET_CODE thModelElemGetDeriv(MODEL_ELEM *melem, char *rname, void **Df);
RET_CODE thModelElemDelDeriv2(MODEL_ELEM *melem, char *rname1, char *rname2);
RET_CODE thModelElemDelDeriv(MODEL_ELEM *melem, char *rname); 

RET_CODE thModelElemAddToMBank(MODEL_ELEM *melem);
RET_CODE thModelElemDelFromBank(char *mname);
RET_CODE thModelElemGetByName(char *mname, MODEL_ELEM **melem);
RET_CODE thModelRemByName(char *mname, MODEL_ELEM **melem); /* deletes it completelt if the placeholder is null */

/* object elements */

OBJC_ELEM *thObjc_elemNew(char *candidatename, char *classname, void *classifier, char *pname, void *compactor);
void thObjc_elemDel(OBJC_ELEM *objcelem);

PCOST_ELEM *thPcost_elemNew(char *classname, char *rname1, char *rname2, void *pcost_function);
void thPcost_elemDel(PCOST_ELEM *pcostelem);

RET_CODE thObjcElemAddModel(OBJC_ELEM *objcelem, char *mname);
RET_CODE thObjcElemDelModel(OBJC_ELEM *objcelem, char *mname);

RET_CODE thObjcElemGetByName(char *objcname, OBJC_ELEM **objcelem);
RET_CODE thObjcElemGetByType(THOBJCTYPE objctype, OBJC_ELEM **objcelem); 
RET_CODE thObjcElemAddToObjcBank(OBJC_ELEM *objcelem);
RET_CODE thObjcElemDelFromBankByType(THOBJCTYPE classtype);
RET_CODE thObjcElemRemByName(char *mname, OBJC_ELEM **objcelem);

RET_CODE thObjcElemGetModelElem(OBJC_ELEM *oelem, char *mname, MODEL_ELEM **melem);
RET_CODE thObjcElemGetNmodel(OBJC_ELEM *objcelem, int *nmodel);

/* parameter cost handling for objects */
RET_CODE thObjcElemAddPCostElem(OBJC_ELEM *oelem, PCOST_ELEM *pcostelem);
RET_CODE thObjcElemGetPCostElem(OBJC_ELEM *oelem, char *rname1, char *rname2,  PCOST_ELEM **pcostelem);
RET_CODE thObjcElemDelPCostElem(OBJC_ELEM *oelem, char *rname1, char *rname2);

RET_CODE thObjcElemAddPCostFunction(OBJC_ELEM *oelem, char *rname1, char *rname2, void *pcost_function);
RET_CODE thObjcElemGetPCostFunction(OBJC_ELEM *oelem, char *rname1, char *rname2, void **pcost_function);

RET_CODE thPCostElemAddByClassName(char *classname, PCOST_ELEM *pcostelem);
RET_CODE thObjcClassAddPCostFunc(char *classname, char *rname1, char *rname2, void*pcost_function);
RET_CODE thPCostElemGetByClassName(char *classname, char *rname1, char *rname2, PCOST_ELEM **pcostelem);
RET_CODE thPCostFunctionGetByClassName(char *classname, char *rname1, char *rname2, void **pcost_function);
RET_CODE thObjcGetPCostElem(THOBJC *objc, char *rname1, char *rname2, PCOST_ELEM **pcostelem);
RET_CODE thObjcGetPCostFunction(THOBJC *objc, const char *rname1, const char *rname2, void **pcost_function); 

/* initiators and de-initiators of Model and Object Banks */
RET_CODE thModelInit();
RET_CODE thModelTerminate();

/* TYPE and NAME managekent for Model and Object Banks */

#if 0
RET_CODE thModelTypeGetFromName(char *mname, THMODELTYPE *mtype);
RET_CODE thModelNameGetFromType(THMODELTYPE mtype, char **mname);
RET_CODE thModelTypeGetNextAvailable(THMODELTYPE *type);
#endif

RET_CODE thObjcTypeGetFromName(char *oname, THOBJCTYPE *otype);
RET_CODE thObjcNameGetFromType(THOBJCTYPE otype, char **oname);
RET_CODE thObjcTypeGetNextAvailable(THOBJCTYPE *type);

/* image calculations */
THOBJC *thObjcNewFromPhprop(PHPROPS *p, int band, FORCECLASS flag, char *classname);
THOBJC *thObjcNewFromType(THOBJCTYPE type, PHPROPS *p, int band);
THOBJC *thObjcNewFromName(char *type, PHPROPS *p, int band);
RET_CODE thObjcRenewFromPhprop(THOBJC *objc);

RET_CODE thObjcClassify(THOBJC *objc); /* classify before fit */
RET_CODE thObjcForceClassifyByName(THOBJC *objc, char *classname);
RET_CODE thObjcForceClassifyByType(THOBJC *objc, THOBJCTYPE classtype);
RET_CODE thObjcReclassify(THOBJC *objc); /* classify after fit */

RET_CODE thObjcInitMpars(THOBJC *objc); /* takes a classified object and generates the parameter space for different models */
RET_CODE thObjcInitModel(THOBJC *objc, char *mname); /* initiate the parameters for the model */

RET_CODE thObjcDoModelf0(THOBJC *objc, char *mname, THREGION *reg, THPIX *mcount, int index);
RET_CODE thObjcDoModelDf(THOBJC *objc, char *mname, char *rname, THREGION *reg, int index);

RET_CODE thObjcPackPars(THOBJC *objc); /* take an object and packs the models pars into one single I/O partype */

/* verification */
RET_CODE thModelElemVerify(MODEL_ELEM *me);
RET_CODE thModelBankVerify(void);

RET_CODE thObjcGetNmodelByType(THOBJCTYPE objctype, int *nmodel);
RET_CODE thModelTestForRecordName(char *mname, char *rname, int *exists_in_model_par, int *exists_in_deriv_list);

#endif


