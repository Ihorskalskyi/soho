#ifndef THMASK_H
#define THMASK_H

#include "dervish.h"
#include "phSpanUtil.h"
#include "thConsts.h"
#include "thBasic.h"
#include "thMaskTypes.h"

RET_CODE thMaskUpdateFromSpanmask(MASK *mask, SPANMASK *sm, 
			    const int nbp);

MASK *thMaskGenFromSpanmask(SPANMASK *sm, 
			    const int nbp, 
			    RET_CODE *status);

RET_CODE thObjmaskChainAddToSpanmask(CHAIN *mchain, 
				 SPANMASK *sm, 
				 S_MASKTYPE which);

OBJMASK *thObjmaskChainMerge(CHAIN *mc, float *dx, float *dy);

RET_CODE thExpandPhSpanmask(SPANMASK *sm);

#endif
