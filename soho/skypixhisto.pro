fields =  lindgen(300)
runs =  [1033]

reruns =  ['150x5M50', '150x6M50',  $
           '150x5M100',  '150x6M100',  $
           '150x5', '150x6',  $
           '150x5N64',  '150x6N64', $
           '150x5R64',  '150x6R64']

reruns =  ['150x1', '150x2',  '150x3',  '150x4',  '150x5', $
           '150x6',  '150x7',  '150x8',  '150x9',  '150x10']



camcols = [2,  3,  4]
bands =  ['r', 'g', 'u', 'i', 'z']

directory =  '/u/khosrow/dss/data/'


print, "* reduced rerun = ",  reruns[r]

xr =  [0]
xc =  [0]
mr =  [0]
mc =  [0]

xr =  fetch_sky_fit(run = runs,  rerun =  reruns[r],  camcol =  camcols,  $
                    band =  bands,  field =  fields,  directory =  directory)


; filtering based on the availability of a given field
; camcol, band in (run, rerun) pair based on
; the value of run in the  SKYFIT structure

help,  xr

runlist = xr[*].run
index =  where(runlist ne -1)
runlist = [0]

xr =  xr[index]

help,  xr


npix = xr[*].npixel
print,  "*** good pixel statistics: "
print,  "median (NPIXEL) = ",  median(npix)
print,  "min    (NPIXEL) = ",  min(npix)
print,  "max    (NPIXEL) = ",  max(npix)
;; finding the most and the least crowded fields 
minnpix =  min(npix)
j = where(npix eq minnpix)
jj =  j[0]
maxnpix =  max(npix)
i =  where(npix eq maxnpix)
ii =  i[0]

;; plotting the histogram

badindex =  where(npix eq 0,  nbadindex)
print,  "* a total of ",  nbadindex,  " frames have 0 sky pixels"

index =  where(npix ne 0,  nfield)

if index[0] ne -1 then begin
    lognpix =  alog10(npix[index])
    help,  lognpix
    plot,  histogram(lognpix,  nbins = nfield / 10)
endif else begin
    print,  "all npixel values are 0.000"
endelse

print,  "the following file contains the minimum pixel count: "
print,  fetch_sky_filename(run =  xr[jj].run,  rerun =  xr[jj].rerun,  $
                           band =  xr[jj].band,  field =  xr[jj].field, $
                           camcol =  xr[jj].camcol,  $
                           directory = "/u/khosrow/dss/data/")

print,  "the following file contains the maxmimum pixel count: "
print,  fetch_sky_filename(run =  xr[ii].run,  rerun =  xr[ii].rerun,  $
                           band =  xr[ii].band,  field =  xr[ii].field, $
                           camcol =  xr[ii].camcol,  $
                           directory = "/u/khosrow/dss/data/")

END
