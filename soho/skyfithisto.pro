
fields =  lindgen(300)
runs =  [1033]
reruns =  ['150x1', '150x2',  '150x3',  '150x4',  '150x5',  '150x5', $
           '150x6',  '150x7',  '150x8',  '150x9',  '150x10']
camcols = [2, 3,  4]
bands =  ['u', 'g', 'r', 'i', 'z']

directory =  '/u/khosrow/dss/data/'


xr =  fetch_sky_goodness(run =  runs,  rerun =  reruns[r],  $
                         camcol =  camcols, field = fields, $
                         band =  bands,  directory =  directory)


xc =  fetch_sky_goodness(run =  runs,  rerun =  reruns[c],  $
                         camcol =  camcols, field = fields, $
                         band =  bands,  directory =  directory)

chisq =  compare_sky_fits(xr =  xr,  xc =  xc)

extract_sky_chisq,  chisq =  chisq

help,  chisq

x2 = chisq[*].x2
logx2 =  alog10(x2)

plot,  histogram(logx2,  nbins = 120),  /xstyle,  /ystyle

END
