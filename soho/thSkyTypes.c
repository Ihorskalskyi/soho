#include "thSkyTypes.h"
#include "thMath.h"

SKYCOEFFS *thSkycoeffsNew() {

  SKYCOEFFS *sc;
  sc = (SKYCOEFFS *)thMalloc(sizeof(SKYCOEFFS));

  return (sc);
}

SKYCOEFFS *thSkycoeffsMake(const int bsize){

  char *name = "thSkycoeffsMake";

  SKYCOEFFS *sc;

  if (bsize > SKYBSIZE) {
    thError("%s: too many sky (basis) dimensions (bsize = %d)", name, bsize);
    return(NULL);
  }
  
  sc = (SKYCOEFFS *) thMalloc(sizeof(SKYCOEFFS));
  sc->bsize = bsize;

#if SKYCOEFFS_SOFT 
  sc->c = (THPIX*) thCalloc(bsize, sizeof(THPIX));
  sc->cov = (THPIX**) thCalloc(bsize, sizeof(THPIX*));
  sc->invcov = (THPIX **) thCalloc(bsize, sizeof(THPIX*));

  int i;
  for (i = 0; i < bsize; i++) {
    sc->cov[i] = (THPIX *) thCalloc(bsize, sizeof(THPIX));
    sc->invcov[i] = (THPIX *) thCalloc(bsize, sizeof(THPIX));
  }

#else 

  /* initiating the skycoeffs */

  int i, j;

  for (i = 0; i < SKYBSIZE; i++) {
    sc->c[i] = (THPIX) 0.0;
    for (j = 0; j < SKYBSIZE; j++) {
      sc->cov[i][j] = (THPIX) 0.0;
      sc->invcov[i][j] = (THPIX) 0.0;
    }
  }
      
#endif

  return(sc);

}

RET_CODE thSkycoeffsDel(SKYCOEFFS *sc) {

  char *name = "thSkycoeffsDel";

  if (sc == NULL) return(SH_SUCCESS);

#if SKYCOEFFS_SOFT

  int i;

  if (sc->c != NULL) thFree(sc->c);

  if  (sc->cov != NULL) {
    for (i = 0; i < sc->bsize; i++) {
      thFree(sc->cov[i]);
    }
    
    thFree(sc->cov);
  }

  if (sc->invcov != NULL) {
    for (i = 0; i < sc->bsize; i++) {
      thFree(sc->invcov[i]);
    }
    thFree(sc->invcov);
  }

#endif

  thFree(sc);
  
  return (SH_SUCCESS);
}

RET_CODE thSkycoeffsCopy(SKYCOEFFS *sc, 
			 THPIX  *c, THPIX ** cov, THPIX ** invcov,
			 int bsize) {
  
  char *name = "thSkycoeffsCopy";
  int i;

#if SKYCOEFFS_SOFT
#else
  int j;
  THPIX *cptr;
#endif

  if (bsize <= 0) {
    thError("%s: bsize = 0 passed to function", name);
    return(SH_GENERIC_ERROR);
  }

  if (sc == NULL) {
    thError("%s: null (sc) passed", name);
    return(SH_GENERIC_ERROR);
  }
  
  if (bsize != sc->bsize) {
      thError("%s: Incompatible SKYCOEFF and supplied BSIZE", name);
      return(SH_GENERIC_ERROR);
  }


  if (c != NULL) {
#if SKYCOEFFS_SOFT
    if (sc->c == NULL) {
      sc->c = (THPIX *)thCalloc(bsize, sizeof (THPIX));
    }
#else

    for (i = bsize; i < SKYBSIZE; i++) {
      sc->c[i] = (THPIX) 0.0;
    }

#endif
    memcpy(sc->c, c, bsize * sizeof(THPIX));
  }
    

  if (cov != NULL) {
    
#if SKYCOEFFS_SOFT
    
    if (sc->cov == NULL) {
      sc->cov = (THPIX**) thCalloc(bsize, (sizeof(THPIX*)));
      for(i = 0; i < bsize; i++) {
	sc->cov[i] = (THPIX *) thCalloc(bsize, sizeof(THPIX));
      }
    }

#else

    for (i = bsize; i < SKYBSIZE; i++) {
      cptr = sc->cov[i];
      for (j = 0; j < SKYBSIZE; j++) {
        cptr[j] = (THPIX) 0.0;
      }
    }

    for (i = 0; i < SKYBSIZE; i++) {
      cptr = sc->cov[i];
      for (j = bsize; j < SKYBSIZE; j++) {
	cptr[j] = (THPIX) 0.0;
      }
    }

#endif

    for (i = 0; i < bsize; i++) {
      memcpy(sc->cov[i], cov[i], bsize * sizeof(THPIX));
    }
      
  }

  if (invcov != NULL) {
    
#if SKYCOEFFS_SOFT

    if (sc->invcov == NULL) {
      sc->invcov = (THPIX**) thCalloc(bsize, (sizeof(THPIX*)));
      for(i = 0; i < bsize; i++) {
	sc->invcov[i] = (THPIX *) thCalloc(bsize, sizeof(THPIX));
      }
    }

#else
    
    for (i = bsize; i < SKYBSIZE; i++) {
      cptr = sc->invcov[i];
      for (j = 0; j < SKYBSIZE; j++) {
	cptr[j] = (THPIX) 0.0;
      }
    }

    for (i = 0; i < SKYBSIZE; i++) {
      cptr = sc->cov[i];
      for (j = bsize; j < SKYBSIZE; j++) {
	cptr[j] = (THPIX) 0.0;
      }
    }
    
#endif

    for (i = 0; i < bsize; i++) {
      memcpy(sc->invcov[i], invcov[i], bsize * sizeof(THPIX));
    }
    
  }
   
  return(SH_SUCCESS);

}


RET_CODE thSkycoeffsPut(SKYCOEFFS *sc, 
			THPIX *c, THPIX **cov, THPIX **invcov,
			int *bsize) {

  char *name = "thSkycoeffsPut";

  if (sc == NULL) {
    thError("%s: SKYCOEFF structure NULL, no change made", name);
    return (SH_GENERIC_ERROR);
  }
 
  /* 
     does not check whether the size of the new arrays 
     and the old sizes are the same 
  */
  
  if (bsize != NULL) {
    sc->bsize = *bsize;
  }
  
  if (sc->bsize <= 0) {
    thError("%s: too small a (bsize = %d)", 
		   name, sc->bsize);
  }

#if SKYCOEFFS_SOFT
#else 
  int i;
#endif


  if (c != NULL) {
#if SKYCOEFFS_SOFT
    sc->c = c; 
#else
    memcpy(sc->c, c, sc->bsize * sizeof(THPIX));
#endif
  }

  if (cov != NULL) {
#if SKYCOEFFS_SOFT
    sc->cov = cov;
#else
    for (i = 0; i < sc->bsize; i++) {
      memcpy(sc->cov[i], cov[i], sc->bsize * sizeof(THPIX));
    }
#endif
  }

  if (invcov != NULL) {

#if SKYCOEFFS_SOFT
    sc->invcov = invcov;
#else
    for (i = 0; i < sc->bsize; i++) {
      memcpy(sc->invcov[i], invcov[i], sc->bsize * sizeof(THPIX));
    }
#endif
  }

  return(SH_SUCCESS);

}
    
SKYMODEL *thSkymodelNew(const int bsize, /* number of basis functions */ 
			const int nrow, const int ncol /* size of the image region */
			 ){

  char *name = "thSkymodelNew";

  if (bsize <= 0) {
    thError("%s: negative (bsize) value, use (thMalloc) instead", name);
    return(NULL);
  }

  SKYMODEL *sm;
  sm = thCalloc(1, sizeof(SKYMODEL));

  sm->sc = thSkycoeffsMake(bsize);
  if (nrow >= 0 && ncol >= 0) {
    sm->reg = shRegNew("skymodel", nrow, ncol, TYPE_THPIX);
  }

  return (sm);
  
}

RET_CODE thSkymodelDel(SKYMODEL *sm) {

  if (sm == NULL) return (SH_SUCCESS);
  
  if (sm->basis != NULL) thSkybasissetDel(sm->basis);

  if (sm->sc != NULL) thSkycoeffsDel(sm->sc);

  if (sm->reg != NULL) shRegDel(sm->reg);

  thFree(sm);

  return(SH_SUCCESS);

}


RET_CODE thSkymodelPut(SKYMODEL *model,
		       SKYBASISSET *basis,
		       SKYCOEFFS *sc,
		       REGION *reg) {


  int bsize, csize, brow, rrow, bcol, rcol;

  shAssert(model != NULL);

  bsize = -1;
  brow = -1;
  bcol = -1;
  if (basis != NULL) {
    bsize = basis->bsize;
    brow = basis->nrow;
    bcol = basis->ncol;
  } else if (model->basis != NULL) {
    bsize = model->basis->bsize;
    brow = model->basis->nrow;
    bcol = model->basis->ncol;
  }

  csize = -1;
  if (sc != NULL) {
    csize = sc->bsize;
  } else if (model->sc != NULL) {
    csize = model->sc->bsize;
  }

  rrow = -1;
  rcol = -1;
  if (reg != NULL) {
    rrow = reg->nrow;
    rcol = reg->ncol;
  } else if (model->reg != NULL) {
    rrow = model->reg->nrow;
    rcol = model->reg->ncol;
  }
  
  if (bsize != -1 && csize != -1) {
    shAssert(bsize == csize);
  }

  if (rrow != -1 && brow != -1) {
    shAssert(brow == rrow && bcol == rcol);
  }

  if (reg != NULL) {
    /* delete the REGION before re-directing it to the new region */
    shRegDel(model->reg);
    model->reg = reg;
  }

  if (basis != NULL) {
    /* delete basis set before redirecting it to the new location */
    thSkybasissetDel(model->basis);
    model->basis = basis;
  }

  if (sc != NULL) {
    /* delete coefficients before redirecting it to new location */
    thSkycoeffsDel(model->sc);
    model->sc = sc;
  }

  return(SH_SUCCESS);

}

 
RET_CODE thSkybasissetDel(SKYBASISSET *sbs)
{
  char *name = "thSkybasissetDel";
  
  if (sbs != NULL) {
    if (sbs->basis != NULL) {
      shChainDestroy(sbs->basis, &thSkybasisDel);
    }
  thFree(sbs);
  } /*else {
    thError("%s: NULL basis set passed, cannot free", name);
  }
    */

  return (SH_SUCCESS);

}

 SKYBASISSET *thSkybasissetNew() {

   char *name = "thSkybasissetNew";
   thError("%s: Warning - unable to set fields (nrow) and (ncol) of (skybasisset)", name);

   SKYBASISSET *sb = NULL;

   sb = thMalloc(sizeof(SKYBASISSET));
   sb->bsize = 0;
   sb->basis = shChainNew("SKYBASIS");

   

   return(sb);
 }

 SKYBASIS *thSkybasisNew() {
   
   char *name = "thSkybasisNew";
   
   SKYBASIS *b = NULL;

   b = thMalloc(sizeof(SKYBASIS));
   b->type = TYPE_UNKNOWN;
   b->val = NULL;

   return (b);

 }
   
RET_CODE thSkybasisDel(SKYBASIS *sb) {

  char *name = "thSkybasisDel";

  if (sb == NULL) return(SH_SUCCESS);

  switch (sb->type) {
  case TYPE_FUNC:
    thFuncDel((FUNC *) (sb->val));
    break;
  case TYPE_AMPL:
    thAmplDel((AMPL *) (sb->val));
    break;
  default:
    thError("%s: Unknown type, Nullifying the val without freeing it", name);
  }

  thFree(sb);
  return(SH_SUCCESS);

}
  
AMPL *thAmplNew() {

  AMPL *ampl;
  ampl = (AMPL *) thCalloc(1, sizeof(AMPL));

  return(ampl);
}

RET_CODE thAmplDel(AMPL *ampl) {

  thFree(ampl);

  return(SH_SUCCESS);

}


THBINREGION *thBinregionNew() {
THBINREGION *binregion = thCalloc(1, sizeof(THBINREGION));
return(binregion);
}

void thBinregionDel(THBINREGION *x) {
if (x == NULL) return;
thFree(x);
return;
}

