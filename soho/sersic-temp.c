THPIX sersic(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *)q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) exp(zz));
	}
	#else
	return((THPIX) (exp(-ksersic * (pow(z, exponentsersic) - (MGFL) 1.0))));
	#endif
}

THPIX sersicStep(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *)q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (exp(zz) * STEPFUNC(z)));
	}
	#else
	return((THPIX) (exp(-ksersic * (pow(z, exponentsersic) - (MGFL) 1.0)) * STEPFUNC(z)));
	#endif
}

THPIX DsersicDa(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);
	MGFL DzDa = (MGFL) -exponentsersic * ksersic * pow(dx, 2) * pow(z, exponentsersic - (MGFL) 1.0); 
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDa * exp(zz)));
	}
	#else
	return((THPIX) (DzDa * exp(-ksersic * (pow(z, exponentsersic) - (MGFL) 1.0))));
	#endif
}
THPIX DsersicDb(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	MGFL DzDb = - exponentsersic * ksersic * dx * dy * pow((double) z, exponentsersic - (MGFL) 1.0); 
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDb * exp(zz)));
	}
	#else
	return((THPIX) (DzDb * exp(-ksersic * (pow(z, exponentsersic) - (MGFL) 1.0))));
	#endif
}
THPIX DsersicDc(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	MGFL DzDc = - exponentsersic * ksersic * pow(dx, 2) * pow(z, exponentsersic - (MGFL) 1.0); 
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDc * exp(zz)));
	}
	#else
	return((THPIX) (DzDc * exp(-ksersic * (pow(z, exponentsersic) - (MGFL) 1.0))));
	#endif
}
THPIX DsersicDxc(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	MGFL DzDxc = exponentsersic * ksersic * (dx * qq->a + dy * qq->b) * pow(z, exponentsersic - (MGFL) 1.0); 
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDxc * exp(zz)));
	}
	#else
	return((THPIX) (DzDxc * exp(-ksersic * (pow(z, exponentsersic) - (MGFL) 1.0))));
	#endif
}
THPIX DsersicDyc(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	MGFL DzDyc = exponentsersic * ksersic * (dx * qq->b + dy * qq->c) * pow(z, exponentsersic - (MGFL) 1.0); /* 0.875 = 7 / 8 */ 
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDyc * exp(zz)));
	}
	#else
	return((THPIX) (DzDyc * exp(-ksersic * (pow(z, exponentsersic) - (MGFL) 1.0))));
	#endif
}
THPIX DsersicDre(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	MGFL DzDre = (exponentsersic * ksersic * pow(z, exponentsersic) / (MGFL) qq->re);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDre * exp(zz)));
	}
	#else
	return((THPIX) (DzDre * exp(-ksersic * (pow(z, exponentsersic) - (MGFL) 1.0))));
	#endif
}
THPIX DsersicDue(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
/* 
(MAX exp(u_eff) + MIN) /  (exp(u_eff) + 1) = r_eff
exp(u_eff) = (r_eff - MIN) / (MAX - r_eff)
du_eff / dr_eff = (e(u) + 2 + e(-u)) / (max - min))
*/
	MGFL DzDue = (THPIX) (exponentsersic * ksersic * pow(z, exponentsersic) / (MGFL) qq->re * (MGFL) qq->reUe);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDue * exp(zz)));
	}
	#else
	return((THPIX) (DzDue * exp(-ksersic * (pow(z, exponentsersic) - (MGFL) 1.0))));
	#endif
}

THPIX DsersicDe(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;	
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDe = -exponentsersic * ksersic * 
	(dx * (qq->ae * dx + 2.0 * qq->be * dy) + qq->ce * pow(dy, 2)) * pow(z, exponentsersic - (MGFL) 1.0);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDe * exp(zz)));
	}
	#else
	return((THPIX) ((DzDe) * exp(-ksersic * (pow(z, exponentsersic) - (MGFL) 1.0))));
	#endif
}
THPIX DsersicDphi(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;	
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDphi = -exponentsersic * ksersic * 
	(dx * (qq->aphi * dx + 2.0 * qq->bphi * dy) + qq->cphi * pow(dy, 2)) * pow(z, exponentsersic - (MGFL) 1.0);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDphi * exp(zz)));
	}
	#else
	return((THPIX) (DzDphi * exp(-ksersic * (pow(z, exponentsersic) - (MGFL) 1.0))));
	#endif
}
THPIX DsersicDE(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;	
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDE = -exponentsersic * ksersic * 
	(dx * (qq->aE * dx + 2.0 * qq->bE * dy) + qq->cE * pow(dy, 2)) * pow(z, exponentsersic - (MGFL) 1.0);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDE * exp(zz)));
	}
	#else
	return((THPIX) ((DzDE) * exp(-ksersic * (pow(z, exponentsersic) - (MGFL) 1.0))));
	#endif
}
THPIX DsersicDv(PIXPOS *p, void *q) {
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	SERSICPARS *qq = (SERSICPARS *) q;	
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDv = -exponentsersic * ksersic * 
	(dx * (qq->av * dx + 2.0 * qq->bv * dy) + qq->cv * pow(dy, 2)) * pow(z, exponentsersic - (MGFL) 1.0);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDv * exp(zz)));
	}
	#else
	return((THPIX) ((DzDv) * exp(-ksersic * (pow(z, exponentsersic) - (MGFL) 1.0))));
	#endif
}
THPIX DsersicDw(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDw = -exponentsersic * ksersic * 
	(dx * (qq->aw * dx + 2.0 * qq->bw * dy) + qq->cw * pow(dy, 2)) * pow(z, exponentsersic - (MGFL) 1.0);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDw * exp(zz)));
	}
	#else
	return((THPIX) ((DzDw) * exp(-ksersic * (pow(z, exponentsersic) - (MGFL) 1.0))));
	#endif
}

THPIX DsersicDV(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDV = -exponentsersic * ksersic * 
	(dx * (qq->aV * dx + 2.0 * qq->bV * dy) + qq->cV * pow(dy, 2)) * pow(z, exponentsersic - (MGFL) 1.0);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDV * exp(zz)));
	}
	#else
	return((THPIX) ((DzDV) * exp(-ksersic * (pow(z, exponentsersic) - (MGFL) 1.0))));
	#endif
}
THPIX DsersicDW(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;	
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDW = -exponentsersic * ksersic * 
	(dx * (qq->aW * dx + 2.0 * qq->bW * dy) + qq->cW * pow(dy, 2)) * pow(z, exponentsersic - (MGFL) 1.0);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDW * exp(zz)));
	}
	#else
	return((THPIX) ((DzDW) * exp(-ksersic * (pow(z, exponentsersic) - (MGFL) 1.0))));
	#endif
}

THPIX DsersicDn(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;	
	MGFL ksersic = (MGFL) qq->k;
	MGFL kn = (MGFL) qq->kn;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL DexponentsersicDn = -(MGFL) qq->inverse_2n / (MGFL) qq->n 
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL zz = (pow(z, exponentsersic) - (MGFL) 1.0); 
	MGFL ww = -ksersic * zz;
	MGFL DwwDn = -(kn + ksersic * ln(z) * DexponentsersicDn) * zz; 
	#if ESTIMATE_SMALL_EXP
	if (ww < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DwwDn * exp(ww)));
	}
	#else
	return((THPIX) ((DwwDn) * exp(ww)));
	#endif


THPIX DDsersicDreDre(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	MGFL DzzDre = (exponentsersic * ksersic * pow(z, exponentsersic) / (MGFL) qq->re);
	MGFL DDzzDreDre = (-(exponentsersic + (MGFL) 1.0) * DzzDre / (MGFL) qq->re);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) ((DDzzDreDre + DzzDre * DzzDre) * exp(zz)));
	}
	#else
	return((THPIX) ((DDzzDreDre + DzzDre * DzzDre) * exp(-ksersic * (pow(z, exponentsersic) - (MGFL) 1.0))));
	#endif
}

THPIX DDsersicDueDue(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzzDre = (exponentsersic * ksersic * pow(z, exponentsersic) / (MGFL) qq->re);	
	MGFL DDzzDreDre = (-(exponentsersic + (MGFL) 1.0) * DzzDre / (MGFL) qq->re);
	MGFL DzzDue = (DzzDre * (MGFL) qq->reUe);
	MGFL DDzzDueDue = DzzDre * (MGFL) qq->reUeUe + pow((MGFL) qq->reUe, 2.0) * DDzzDreDre;
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) ((DDzzDueDue + DzzDue * DzzDue) * exp(zz)));
	}
	#else
	return((THPIX) ((DDzzDueDue + DzzDue * DzzDue) * exp(-ksersic * (pow(z, exponentsersic) - (MGFL) 1.0))));
	#endif
}


