
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include "strings.h"
#include "dervish.h"

#include "dervish.h"
#include "phPhotoFitsIO.h"
#include "phSpanUtil.h"
#include "phUtils.h"
#include "phObjc.h"
#include "phDataIo.h"
#include "prvt/region_p.h"

#include "sohoEnv.h"
#include "thDebug.h"
#include "thFpcIo.h"

#define FpmFileType STANDARD              /* FITS file Type: STANDARD, NONSTANDARD, IMAGE */
#define FpmFileDef  DEF_DEFAULT /* = DEF_NUM_OF_ELEMS    /* = DEF_NONE: thCFitsIo.h */
#define NROW_FPM 1489
#define NCOLUMN_FPM 2048
#define REGION_FLAGS_FPM NO_FLAGS
#define MASKVAL_FPM 1

REGION *thObjmaskReadFpm(char *file,  
			 const int row0,  const int column0,  
			 const int row1,  const int column1,
			 const int *planes,
			 RET_CODE *thStatus);
RET_CODE thWriteFpm(char *file, 
		    MASK *mask);

RET_CODE thCpFpm(char *infile, char *outfile, /* input and output filenames */
		 const int row0, const int column0, 
		 const int row1, const int column1, /* corners of the image in INFILE */
		 const int *planes);

/*****************************************************************************/

REGION *thObjmaskReadFpm(char *file,  
			 const int row0,  const int column0,  
			 const int row1,  const int column1,
			 const int *planes,
			 RET_CODE *thStatus)
{

  char *name = "thObjmaskReadFpm";
  int  nrow,  ncolumn;
  //  MASK *sub = NULL, *fullmask = NULL, *mask = NULL;
  OBJMASK *Objmask; CHAIN *ObjmaskChain;
  int j, nspans;

  nrow = row1 - row0 + 1;
  ncolumn = column1 - column0 + 1;

  initSpanObjmask();
  ObjmaskChain = shChainNew ("OBJMASK");

  /* in order to read a binary structure with heaps components
     one needs to first define the schema_trans for that data type
     for data with non-heap components, photo/dervish successfully 
     constructs the schematrans from the header 
  */
  
  phSchemaLoadFromPhoto();
  
  SCHEMATRANS *om_trans;
  om_trans = shSchemaTransNew();
  shSchemaTransEntryAdd(om_trans, CONVERSION_BY_TYPE, "REFCNTR", "refcntr", 
			"", NULL, NULL, NULL, NULL, 0.0, -1);
  shSchemaTransEntryAdd(om_trans, CONVERSION_BY_TYPE, "NSPAN", "nspan", 
			"", NULL, NULL, NULL, NULL, 0.0, -1);
  shSchemaTransEntryAdd(om_trans, CONVERSION_BY_TYPE, "ROW0", "row0", 
			"", NULL, NULL, NULL, NULL, 0.0, -1);
  shSchemaTransEntryAdd(om_trans, CONVERSION_BY_TYPE, "COL0", "col0", 
			"", NULL, NULL, NULL, NULL, 0.0, -1);
  shSchemaTransEntryAdd(om_trans, CONVERSION_BY_TYPE, "RMIN", "rmin", 
			"", NULL, NULL, NULL, NULL, 0.0, -1);
  shSchemaTransEntryAdd(om_trans, CONVERSION_BY_TYPE, "RMAX", "rmax", 
			"", NULL, NULL, NULL, NULL, 0.0, -1);
  shSchemaTransEntryAdd(om_trans, CONVERSION_BY_TYPE, "CMIN", "cmin", 
			"", NULL, NULL, NULL, NULL, 0.0, -1);
  shSchemaTransEntryAdd(om_trans, CONVERSION_BY_TYPE, "CMAX", "cmax", 
			"", NULL, NULL, NULL, NULL, 0.0, -1);
  shSchemaTransEntryAdd(om_trans, CONVERSION_BY_TYPE, "NPIX", "npix", 
			"", NULL, NULL, NULL, NULL, 0.0, -1);
  shSchemaTransEntryAdd(om_trans, CONVERSION_BY_TYPE, "S", "s", 
			"heap", "SPAN", "-1", NULL, NULL, 0.0, -1);
  shSchemaTransEntryAdd(om_trans, CONVERSION_BY_TYPE, "L9_S", "l9_s", 
			"", NULL, NULL, NULL, NULL, 0.0, -1);
  phFitsBinDeclareSchemaTrans(om_trans, "OBJMASK");

  /* open the file as a PHFITSFILE */

  /*PHFITSFILE *
    phFitsBinTblOpen(char *file,		// file to open //
    int flag,		// open for read (flag == 0),
    write (flag == 1; => creat)
    append (flag == 2; => creat) //
    HDR *hdr)		// additional header info //
  */
  
  PHFITSFILE *fits;
  int phstatus = 0, quiet = 1;

  fits = phFitsBinTblOpen(file,	0, NULL);
  
  /* no need to read the extension = 0, since Binary handlers in PHOTO
     naturally place the pointer at the first binary table available 
  */

  int nfitsrow, imask, extno;
  for (imask = 0;  imask < NMASK_TYPES; imask++) {
    
    /* reading the extension header and evaluating the number of rows */
    nfitsrow = 0;
    phstatus = phFitsBinTblHdrRead(fits, "OBJMASK", NULL, &nfitsrow, quiet);

    /* if the nrow is successfully read */
    if (nfitsrow > 0 & phstatus >= 0 & planes[imask]) {

      /* checking the state of the binary structure to be read
	 used in phFitsBinTblRowRead */ 
      //FITSB_STATE *state = fits->machine[fits->start_ind]->states;
      
      /* 
	 Objmask = phObjmaskNew(
	 int nspans			// number of spans //
	 ); 
      */
      
      /* determine the real nspan */
      nspans = 10000;
      Objmask = phObjmaskNew(nspans);
      phstatus = phFitsBinTblRowRead(fits, Objmask);
      /* nspans  from Objmask */
      nspans = Objmask->nspan;
      
      phstatus = phFitsBinTblRowUnread(fits); 
      phObjmaskDel(Objmask);
      
      for (j = 0; j < fits->nrow; j++) {
	Objmask = phObjmaskNew(nspans); 
	/* read  Objmask */
	
	phstatus = phFitsBinTblRowRead(fits,Objmask);
	if ( phstatus < 0){
	  extno = imask + 1;
	  thError("%s: problem reading fits file %s [%d] at row %d", name, file, extno, j);
	  *thStatus = SH_GENERIC_ERROR;    
	  return(NULL);
	} else {
	  /* add it to chain */
	  shChainElementAddByPos(ObjmaskChain, Objmask, "OBJMASK", TAIL, AFTER);
	}
      }
    }
    /* closing the current extension and going to the next one */
    phstatus = phFitsBinTblEnd(fits);
  }
  
  /* closing the openned file */
  phstatus = phFitsBinTblClose(fits);
  if ( phstatus != 0) {
    thError("%s: problem closing the fits file %s", name, file);
    *thStatus = SH_GENERIC_ERROR; 
    return(NULL);
  }
  
  // Converting Objmask Chain into REGION
  
  /* void
     phRegionSetValFromObjmaskChain(REGION *reg, // the region to set //
     const CHAIN *chain, // the CHAIN of OBJMASKs specifying pixels //
     int val)	// to set to this value //
  */
  
  REGION *ObjmaskReg = NULL;
  /* debugging mode */
  int nchain = 0; 

  nchain = shChainSize(ObjmaskChain);
  if ( nchain > 0) {

    /* convert Objmask Chain into a region 
       maskval is for debugging purposes only 
    */
    ObjmaskReg = shRegNew(name, NROW_FPM, NCOLUMN_FPM, TYPE_U16);
    phRegionSetValFromObjmaskChain(ObjmaskReg, ObjmaskChain, MASKVAL_FPM);
    
    /* successful run */
    *thStatus = SH_SUCCESS;
  } else {
    /* unsuccessful run */
    thError("%s: could not read any object mask from the file %s", name, file);
    *thStatus = SH_GENERIC_ERROR;
  }

    return(ObjmaskReg);
}


RET_CODE thWriteFpm(char *file, 
		    MASK *mask)
{

  char *name = "thWriteFpc";
  RET_CODE thStatus;
  
  /* Write the MASK */

  /*
    RET_CODE thMaskWriteAsFits
    (
    MASK   *a_maskPtr,       // IN: Pointer to mask //
    char   *a_file,          // IN: Name of FITS file //
    DEFDIRENUM a_FITSdef,    // IN: Directory/file extension default type //
    FILE   *maskFilePtr,     // IN:  If != NULL then is a pipe to read from //
    int     a_writetape      // IN:  Flag to indicate that we must fork/exec an
    instance of "dd" to write a tape drive //
				      )
  */
  if (mask != NULL){
    thStatus = shMaskWriteAsFits (mask, file, FpmFileDef, NULL, 0);
  }
  return(thStatus);
}
  

RET_CODE thCpFpm(char *infile, char *outfile, /* input and output filenames */
		 const int row0, const int column0, 
		 const int row1, const int column1, /* corners of the image in INFILE */
		 const int *planes) 
{
  
  char *name = "thCpFpm";
  REGION *ObjmaskReg = NULL;
  RET_CODE thStatus;

  ObjmaskReg = thObjmaskReadFpm(infile, row0, column0, row1, column1, planes, &thStatus);
  if (thStatus != SH_SUCCESS) {
    thError("%s: unsucessful FPM read", name);
    shRegDel(ObjmaskReg);
    return(thStatus);
  } else {
    thWriteFpc(outfile, ObjmaskReg);
  }
    if (ObjmaskReg != NULL) {
    shRegDel(ObjmaskReg);
  }
  return(thStatus);
}
