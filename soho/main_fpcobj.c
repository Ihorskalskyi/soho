/*
 * Read fpC images (infile), and copy its content to Outfile.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dervish.h"
#include "phFits.h"
#include "phConsts.h"
#include "fitsio.h"
/* SOHO modules and libraries */
#include "sohoTypes.h"
#include "sohoEnv.h"
#include "strings.h"

/*
 * some symbols to prevent dervish.o from being loaded from libatlas.a
 * if we are also linking against real dervish
 */

int verbose = 0;

static void usage(void);
int *thFitsioError(int *status);

int
main(int ac, char *av[])
{
  /* from MAIN in READ_ATLAS */
  /* not used */
  int bkgd = SOFT_BIAS;		/* desired background level */
  int color = 0;			/* desired color */
  FITS *fits;				/* the table in question */
  REGION *reg;				/* region to write */
  int row, column;			/* desired row and column*/
  /* used */
  char *infile, *outfile;		/* input and output filenames */
  long row0, column0, row1, column1;	/* origin of image in region */

#define TfpC U16
  fitsfile *infptr, *outfptr, **inffptr, **outffptr;      /* CFITSIO fits file pointer*/
  int *status, *anynul, *nkeys, nexc;  /* CFITSIO io status*/
  char *filename, *err_text;           /* CFITSIO fits filename and error text*/
  int hdunum, *hdutype;                /* CFITSIO hdutype */
  const int nocomments = 1;            /* COMMENTS are read into header */
  char *card, **header, **exclist;     /* CFITSIO fits header string */
  long fpixel[2], lpixel[2];           /* corner pixels for the IMAGE portion taken */
  TfpC datatype;                       /* CFITSIO datatype for IMAGE*/
  TfpC *nulval;                        /* Blank Pixels Values - If set to 0 No Check Is Done */
  TfpC **fpciArr;                      /* Array containing the IMAGE read by CFITSIO; MAXIMUM size given*/
  
  
     
  while(ac > 1 && (av[1][0] == '-' || av[1][0] == '+')) {
    switch (av[1][1]) {
    case '?':
    case 'h':
      usage();
      exit(0);
      break;
    case 'v':
      verbose++;
      break;
    default:
      shError("Unknown option %s\n",av[1]);
      break;
    }
    ac--;
    av++;
  }
  if(ac <= 6) {
    shError("You must specify an input file, an output file, a row, and a column \n");
    exit(1);
  }
  infile = av[1]; outfile = av[2]; 
  *inffptr = infptr; *outffptr = outfptr;

  /*
   * dummy calls to pull .o files out of the real libdervish.a if we are
   * linking against it
   */
  (void)thTypeGetFromName("RHL");
  
  /*
   * INFILE - The FPC file to be read using CFITSIO
   */
  
  /*
   * open file
   */
  
  status = NULL;
  (int)fits_open_file(inffptr, infile, 0, status);
  if (thFitsioError(status) != NULL) {
    exit(1);
  }
 
  /*
   * read the header
   */
  
  hdutype = NULL;
  hdunum  = 0;
  (int)fits_movabs_hdu(infptr, hdunum, hdutype, status);
  if (thFitsioError(status) != NULL) {
    exit(1);
  }
 
  (exclist) = NULL;
  /* nocomments = 0; */
  (int)fits_hdr2str(infptr, nocomments, exclist, nexc,
		    header, nkeys, status);
  if (thFitsioError(status) != NULL) {
    exit(1);
  }
 
  /*
   * number of keywords in the header, and the first line of the header
   */
  
  fprintf(NULL, "Number of Keywords = %d\n", *nkeys);
  fprintf(NULL, "First line of Header: \n%a\n ", (*header)[0]); 
  
  /*
   * read binary table
   */
  
  hdutype = NULL;
  hdunum  = 1;
  (int)fits_movabs_hdu(infptr, hdunum, hdutype, status);
  if (thFitsioError(status) != NULL) {
    exit(1);
  }
  
  /*
   * datatype = TBYTE, TSBYTE, TSHORT, TUSHORT, TINT, TUINT, TLONG, TLONGLONG, TULONG, TFLOAT, TDOUBLE
   */
  
  (int)thReadBinTable(infptr, ObjArr, status); 

  if (thFitsioError(status) != NULL) {
    exit(1);
  } 
  
  /* OUTFILE - The Output file where the FPC file is Written */
  
  /*
   * open file (READWRITE)
   */
  
  /* Create and open a new empty output FITS file.*/
  
  status = NULL;
  (int)fits_create_file(outffptr, filename, status);
  if (thFitsioError(status) != NULL) {
    exit(1);
  }
 
  
  
  /* Write the Header */
  
  hdutype = NULL;
  hdunum  = 0;
  (int)fits_movabs_hdu(outfptr, hdunum, hdutype, status);
  if (thFitsioError(status) != NULL) {
    exit(1);
  }
 
  card = *header;
  while (card != NULL) {   
    (int)fits_write_record(outfptr, card, status);
    if (thFitsioError(status) != NULL) {
      exit(1);
    }
    card++;
  }
  
  /* Write the binary table */
  
  hdutype = NULL;
  hdunum  = 1;
  (int)fits_movabs_hdu(outfptr, hdunum, hdutype, status);
  if (thFitsioError(status) != NULL) {
    exit(1);
  }
 
  (int)thWriteBinTable(infptr, ObjArr, status); 
  if (thFitsioError(status) != NULL) {
    exit(1);
  }
  
  /*
   * close infile
   */
  (int) fits_close_file(infptr, status);
  if (thFitsioError(status) != NULL) {
    exit(1);
  }
 
  
  /*
   * close outfile
   */
  (int) fits_close_file(outfptr, status);
  if (thFitsioError(status) != NULL) {
    exit(1);
  }
  
 
  return(0);
}

/*****************************************************************************/

static void
usage(void)
{
   char **line;

   static char *msg[] = {
      "Usage: read_atlas_image [options] input-file output-file r0 c0 r1 c1",
      "Your options are:",
      "       -?      This message",
      "       -b #    Set background level to #",
      "       -c #    Use colour # (0..ncolor-1; default 0)",
      "       -h      This message",
      "       -i      Print an ID string and exit",
      "       -v      Turn up verbosity (repeat flag for more chatter)",
      NULL,
   };

   for(line = msg;*line != NULL;line++) {
      fprintf(stderr,"%s\n",*line);
   }
}



