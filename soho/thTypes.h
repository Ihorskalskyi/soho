#ifndef THTYPES_H
#define THTYPES_H

#define THPIX FL32 */
#define TYPE_THPIX TYPE_FL32  /* a type definition for later use */
#define rows_thpix rows_fl32 /* the rows in a region with type th_pix */

#define SUPERMASK_PIX U16
#define TYPE_SUPERMASK_PIX TYPE_U16
#define rows_sm rows_u16 /* dara rows in a region with pixel->type supermask_pix */

/* 

   You should use #define statements in order for make_io to be able to tell the difference
   make_io is then passed then files after THPIX is corrected by a perl script to its value 
   in terms of standard C types

   //typedef FL32 THPIX;

*/

#endif
