#include "thModelTypes.h"
#include "thObjcTypes.h"
#include "thMSkyTypes.h"
#include "thMGalaxyTypes.h"
#include "thDebug.h"
#include "thMSky.h"

static RET_CODE get_schema_name_type(char *sname, TYPE type, SCHEMA **s);

static THPIX *record_get_arr(void *q, TYPE t, char *r);
void record_put_arr(void *q, TYPE t, char *r, THPIX *y, int n);

THPIX *record_get_arr(void *q, TYPE t, char *r) {
	shAssert(q != NULL);
	shAssert(t != UNKNOWN_SCHEMA);
	shAssert(r != NULL);
	shAssert(strlen(r) != 0);

	THPIX *x;
	SCHEMA_ELEM *se;
	se = shSchemaElemGetFromType(t, r);
	shAssert(se != NULL);
	x = shElemGet(q, se, NULL);
return(x);
}

void record_put_arr(void *q, TYPE t, char *r, THPIX *y, int n) {
	shAssert(q != NULL);
	shAssert(t != UNKNOWN_SCHEMA);
	shAssert(r != NULL);
	shAssert(strlen(r) != 0);
	
	THPIX *x;
	SCHEMA_ELEM *se;
	se = shSchemaElemGetFromType(t, r);
	shAssert(se != NULL);
	x = shElemGet(q, se, NULL);
	memcpy(x, y, n * sizeof(THPIX));
return;
}

PHPROPS *thPhpropsNew() {
	PHPROPS *prop;
	prop = (PHPROPS *) thCalloc(1, sizeof(PHPROPS));
	#if PHPROPS_NEW_COPY_OBJC_IO
	prop->phObjc = thWObjcIoNew();
	#endif
	#if PHPROPS_NEW_COPY_CALIB_WOBJC_IO
	prop->cwphobjc = thCalibWObjcIoNew();
	#endif
	#if PHPROPS_NEW_COPY_CALIB_PHOBJC_IO
	prop->cphobjc = thCalibPhObjcIoNew();
	#endif
	#if PHPROPS_NEW_COPY_CALIB_FRAME
	prop->calibData = thCalibFrameNew();
	#endif
	int i; 
	for (i = 0; i < NCOLOR; i++) {
		prop->bndex[i] = UNKNOWN_BNDEX;
	}
	return(prop);
}

void thPhpropsDel(PHPROPS *prop) {
	if (prop == NULL) return;
	#if PHPROPS_NEW_COPY_OBJC_IO
	if (prop->phObjc != NULL) thWObjcIoDel(prop->phObjc);
	#endif 
	#if PHPROPS_NEW_COPY_CALIB_WOBJC_IO
	if (prop->cwphobjc != NULL) thCalibWObjcIoDel(prop->cwphobjc);
	#endif
	#if PHPROPS_NEW_COPY_CALIB_PHOBJC_IO
	if (prop->cphobjc != NULL) thCalibPhObjcIoDel(prop->cphobjc);
	#endif
	#if PHPROPS_NEW_COPY_CALIB_FRAME
	if (prop->calibData != NULL) thCalibFrameDel(prop->calibPhObjc);
	#endif
	#if PHPROPS_NEW_COPY_OBJMASK
	if (prop->objmask != NULL) phObjmaskDel(prop->objmask);
	#endif
	thFree(prop);
	return;
}

RET_CODE thPhpropsGet(PHPROPS *prop, WOBJC_IO **phObjc, int **bndex, CALIB_WOBJC_IO **cwphobjc, CALIB_PHOBJC_IO **cphobjc, PHCALIB **calibData) {
	char *name = "thPhPropsGet";
	if ((prop == NULL) && (phObjc != NULL || cwphobjc != NULL || cphobjc != NULL || calibData != NULL)) {
		if (phObjc != NULL) *phObjc = NULL;
		if (calibData != NULL) *calibData = NULL;
		if (cwphobjc != NULL) *cwphobjc = NULL;
		if (cphobjc != NULL) *cphobjc = NULL;
		thError("%s: ERROR - null input while expecting an output", name);
		return(SH_GENERIC_ERROR);
	}
	if (prop == NULL && phObjc == NULL && cwphobjc == NULL && cphobjc == NULL && calibData == NULL) {
		thError("%s: WARNING - null input and output", name);
		return(SH_SUCCESS);
	}
	if (phObjc != NULL) *phObjc = prop->phObjc;
	if (calibData != NULL) *calibData = prop->calibData;
	if (cwphobjc != NULL) *cwphobjc = prop->cwphobjc;
	if (cphobjc != NULL) *cphobjc = prop->cphobjc;
	if (bndex != NULL) *bndex = prop->bndex;
	return(SH_SUCCESS);
}

RET_CODE thPhpropsPut(PHPROPS *p, WOBJC_IO *phObjc, int *bndex, CALIB_WOBJC_IO *cwphobjc, CALIB_PHOBJC_IO *cphobjc,  PHCALIB *calibData, OBJMASK *mask) {
	char *name = "thPhpropsPut";

	#if PHPROPS_NEW_COPY_OBJC_IO	
	if (phObjc != NULL) {
		RET_CODE status;
		status = thWObjcIoCopy(p->phObjc, phObjc);
		if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy (phObjc) 'WOBJC_IO' onto 'PHPROPS'", name);
		return(status);
		}
	} 
	#else
	p->phObjc = phObjc;
	#endif
	#if PHPROPS_NEW_COPY_CALIB_WOBJC_IO	
	if (cwphobjc != NULL) {
		thCalibWObjcIoCopy(p->cwphobjc, cwphobjc);
	} 
	#else
	p->cwphobjc = cwphobjc;
	#endif
	#if PHPROPS_NEW_COPY_CALIB_PHOBJC_IO	
	if (cphobjc != NULL) {
		thCalibPhObjcIoCopy(p->cphobjc, cphobjc);
	} 
	#else
	p->cphobjc = cphobjc;
	#endif
	#if PHPROPS_NEW_COPY_CALIB_FRAME	
	if (calibData != NULL) {
		RET_CODE status;
		status = phCalibCopy(p->calibData, calibData);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not copy (phObjc) 'OBJC_IO' onto 'PHPROPS'", name);
			return(status);
		}
	} 
	#else
	p->calibData = calibData;
	#endif
	#if PHPROPS_NEW_COPY_OBJMASK
	if (mask != NULL) {
		if (p->objmask != NULL) phObjmaskDel(p->objmask);
		p->objmask = phObjmaskCopy(mask, 0, 0);
		if (mask == NULL) {
			thError("%s: ERROR - could not copy (mask)", name);
			return(SH_GENERIC_ERROR);
		}
	}
	#else
	p->objmask = mask;
	#endif


	if (bndex != NULL) {
		memcpy(p->bndex, bndex, NCOLOR * sizeof(int));
	}

	return(SH_SUCCESS);
}

RET_CODE thPhpropsPutFlagLRG(PHPROPS *phprop, int f) {
shAssert(phprop != NULL);
WOBJC_IO *phobjc = NULL;
CALIB_PHOBJC_IO *cphobjc = NULL;
CALIB_WOBJC_IO *cwphobjc = NULL;
RET_CODE status = thPhpropsGet(phprop,  &phobjc, NULL, &cwphobjc, &cphobjc, NULL);
if (status != SH_SUCCESS) return(status);
#if USE_FNAL_OBJC_IO
if (phobjc != NULL) phobjc->flagLRG = f;
#endif
if (cwphobjc != NULL) cwphobjc->flagLRG = f;
if (cphobjc != NULL) cphobjc->flagLRG = f;

return(SH_SUCCESS);
}


RET_CODE thWObjcIoCopy(WOBJC_IO *t, WOBJC_IO *s) {
	char *name = "thWObjcIoCopy";
	if (s == NULL && t != NULL) {
		thError("%s: ERROR - null source cannot be copies onto any targer", name);
		return(SH_GENERIC_ERROR);
	}
	if (s == NULL && t == NULL) {
		thError("%s: WARNING - null source and target", name);
		return(SH_SUCCESS);
	}
	memcpy(t, s, sizeof(WOBJC_IO));
	return(SH_SUCCESS);
}

THPROP *thPropNew(char *mname, char *pname, void *value) {
THPROP *prop;
prop = thCalloc(1, sizeof(THPROP));

prop->mname = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
prop->pname = (char *) thCalloc(MX_STRING_LEN, sizeof(char));

if (mname != NULL) strcpy(prop->mname, mname);
if (pname != NULL) strcpy(prop->pname, pname);
prop->value = value;
return(prop);
}

void thPropDel(THPROP *prop) {
if (prop == NULL) return;
if (prop->mname != NULL) thFree(prop->mname);
if (prop->pname != NULL) thFree(prop->pname);
thFree(prop);
return;
}

void thPropDestroy(THPROP *prop) {

if (prop == NULL) return;

if (prop->mname != NULL) thFree(prop->mname);
if (prop->pname != NULL) thFree(prop->pname);

if (prop->value != NULL && prop->pname != NULL && strlen(prop->pname) != 0) {
	TYPE t;
	t = shTypeGetFromName(prop->pname);
	SCHEMA *s;
	s = shSchemaGetFromType(t);
	s->destruct(prop->value);
}

return;
}

RET_CODE thPropPut(THPROP *prop, char *mname, char *pname, void *value) {
char *name = "thPropPut";
if (prop == NULL && mname == NULL && pname == NULL && value == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
}
if (prop == NULL) {
	thError("%s: ERROR - null place holder for (thprop) cannot hold any value", name);
	return(SH_GENERIC_ERROR);
}

if (mname != NULL) {
	if (prop->mname == NULL) prop->mname = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
	strcpy(prop->mname, mname);
	}
if (pname != NULL) {
	if (prop->pname == NULL) prop->pname = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
	strcpy(prop->pname, pname);
}
prop->value = value;
return(SH_SUCCESS);

}

RET_CODE thPropGet(THPROP *prop, char **mname, char **pname, 
	void **value) {
char *name = "thPropGetMnamePnameValue";
if (mname == NULL && pname == NULL && value == NULL) {
	thError("%s: WARNING - null placeholders, performing null", name);
	return(SH_SUCCESS);
}
if (prop == NULL) {
	thError("%s: ERROR - improperly allocated (thprop)");
	return(SH_GENERIC_ERROR);
}

if (mname != NULL) *mname = prop->mname;
if (pname != NULL) *pname = prop->pname;
if (value != NULL) *value = prop->value;

return(SH_SUCCESS);
}

RET_CODE thPropGetCountMag(THPROP *prop, THPIX *count, THPIX *mag) {
char *name = "thPropGetCountMag";
if (prop == NULL && count == NULL && mag == NULL) {
	thError("%s: WARNING - null intput and output", name);
	return(SH_SUCCESS);
}
if (prop == NULL) {
	thError("%s: ERROR - null input", name);
	if (count != NULL) *count = VALUE_IS_BAD;
	if (mag != NULL) *mag = VALUE_IS_BAD;
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
char *pname = NULL, *mname = NULL;
void *value = NULL;
status = thPropGet(prop, &mname, &pname, &value);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get contents of (thprop = %p)", name, (void *) prop);
	if (count != NULL) *count = VALUE_IS_BAD;
	if (mag != NULL) *mag = VALUE_IS_BAD;
	return(status);
}

TYPE ptype = shTypeGetFromName(pname);
SCHEMA *s = shSchemaGetFromType(ptype);
if (s == NULL) {
	thError("%s: ERROR - could not get schema for (pname = %s) found in (model = %s)", name, pname, mname);
	if (count != NULL) *count = VALUE_IS_BAD;
	if (mag != NULL) *mag = VALUE_IS_BAD;
	return(SH_GENERIC_ERROR);
}
int error = 0;
THPIX *ptr;
char *rname = NULL;
SCHEMA_ELEM *se = NULL;
if (count != NULL) {
	ptr = NULL;
	rname = "counts";
	se = shSchemaElemGetFromType(ptype, rname);
	if (se == NULL) {
		thError("%s: ERROR - could not find '%s' in type '%s'", name, rname, pname);
		error++;
	} else {
		ptr = shElemGet(value, se, NULL);
	}
	if (ptr == NULL) {
		thError("%s: ERROR - could not get the pointer to '%s' in '%s' for model '%s'", name, rname, pname, mname);
		*count = VALUE_IS_BAD;
		error++;
	} else {
		*count = *ptr;
	}
}
if (mag != NULL) {
	ptr = NULL;
	rname = "mag";
	se = shSchemaElemGetFromType(ptype, rname);
	if (se == NULL) {
		thError("%s: ERROR - could not find '%s' in type '%s'", name, rname, pname);
		error++;
	} else {
		ptr = shElemGet(value, se, NULL);
	}
	if (ptr == NULL) {
		thError("%s: ERROR - could not get the pointer to '%s' in '%s' for model '%s'", name, rname, pname, mname);
		*mag = VALUE_IS_BAD;
		error++;
	} else {
		*mag = *ptr;
	}
}

if (error > 0) return(SH_GENERIC_ERROR);
return(SH_SUCCESS);
}


RET_CODE thPropChainGetByMname(CHAIN *props, char *mname, char **pname, void **value) {
char *name = "thPropChainGetPnameValueByMname";

if (props == NULL || shChainSize(props) == 0) {
	if (mname == NULL || strlen(mname) == 0) {
		thError("%s: WARNING - empty input, generating empty output", name);
		if (pname != NULL) *pname = NULL;
		if (value != NULL) *value = NULL;
		return(SH_SUCCESS);
	}
	thError("%s: ERROR - improperly allocated (thprop) chain", name);
	if (pname != NULL) *pname = NULL;
	if (value != NULL) *value = NULL;
	return(SH_GENERIC_ERROR);
}

/* if mname is null or empty string then return the only prop listed */

if (mname == NULL || strlen(mname) == 0) {
	int n;
	if ((n = shChainSize(props)) != 1) {
		thError("%s: ERROR - empty model name provided, expected 1 model found %d models instead", name, n);
		if (pname != NULL) *pname = NULL;
		if (value != NULL) *value = NULL;
 		return(SH_GENERIC_ERROR);
	}
	THPROP *prop;
	prop = shChainElementGetByPos(props, 0);
	if (prop == NULL) {
		thError("%s: ERROR - null (thprop) found in (chain) while expecting non-null", name);
		if (pname != NULL) *pname = NULL;
		if (value != NULL) *value = NULL;
		return(SH_GENERIC_ERROR);
	}
	if (pname == NULL && value == NULL) {
		thError("%s: WARNING - null place holders, returning null", name);
		return(SH_SUCCESS);
	}
	if (pname != NULL) *pname = prop->pname;
	if (value != NULL) *value = prop->value;
	return(SH_SUCCESS);
}

/* now go one by one and find the proper mname */

THPROP *prop;
int i, found = 0;
for (i = 0; i < shChainSize(props); i++) {
	prop = shChainElementGetByPos(props, i);
	if (prop->mname != NULL && strlen(prop->mname) == strlen(mname) && !strcmp(prop->mname, mname)) {
		if (pname != NULL) *pname = prop->pname;
		if (value != NULL) *value = prop->value;
		found++;
		break;
	}
}
if (!found) {
	thError("%s: ERROR - could not find model (%s) in the property chain", name, mname);
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}
		
int thObjcHasModel(THOBJC *objc, char *mname) {
char *name = "thObjcHasModel";
if (objc == NULL || objc->thprop == NULL) {
	thError("%s: ERROR - improperly allocated (objc)", name);
	return(0);
}

int n;
n = shChainSize(objc->thprop);

if (mname == NULL || strlen(mname) == 0) {
	if (n == 1) return(1);
	return(0);
}

THPROP *prop;
int i, found = 0;
for (i = 0; i < n; i++) {
	prop = shChainElementGetByPos(objc->thprop, i);
	if (prop->mname != NULL && strlen(prop->mname) == strlen(mname) && !strcmp(prop->mname, mname)) {
		found++;
		break;
	}
}
return(found);
}

THOBJC *thThobjcNew() {

THOBJC *objc;
objc = thCalloc(1, sizeof(THOBJC));

objc->sdssname = thCalloc(MX_STRING_LEN, sizeof(char));
objc->thobjctype = UNKNOWN_OBJC;
objc->phprop = NULL;
objc->thprop = shChainNew("THPROP");

objc->ioname = thCalloc(MX_STRING_LEN, sizeof(char));
objc->iotype = UNKNOWN_SCHEMA;

objc->fitmask = thFitmaskNew();

return(objc);

}

RET_CODE thThobjcDel(THOBJC *objc) {
char *name = "thObjcDel";

if (objc == NULL) return(SH_SUCCESS);


/* NOTE: PHPROPS come from FRAMEDATA, and should not be removed from memory
 
if (objc->phprop != NULL && objc->phtype == UNKNOWN_SCHEMA) {
	if (objc->phname != NULL && strlen(objc->phname) != 0) {
		thError("%s: ERROR - unsupported photo object (%s) detected in (thobjc)", name, objc->phname);
	} else {
		thError("%s: ERROR - unknown photo object detected in (thobjc)", name);
	}
	return(SH_GENERIC_ERROR);
} else if (objc->phprop != NULL) {
	SCHEMA *s;
	s = shSchemaGetFromType(objc->phtype);
	s->destruct(objc->phprop);
	objc->phprop = NULL;
	}
*/
if (objc->sdssname != NULL) {
	thFree(objc->sdssname);
	objc->sdssname = NULL;
}

objc->phprop = NULL;
if (objc->phname != NULL) {
	thFree(objc->phname);
	objc->phname = NULL;
}

if (objc->thprop != NULL) {
	shChainDestroy(objc->thprop, &thPropDestroy);
	objc->thprop = NULL;
	}

if (objc->ioprop != NULL && objc->iotype == UNKNOWN_SCHEMA) {
	if (objc->ioname != NULL && strlen(objc->ioname) != 0) {
		thError("%s: ERROR - unsupported i/o object (%s) detected in (thobjc)", name, objc->ioname);
	} else {
		thError("%s: ERROR - unknown i/o object detected in (thobjc)", name);
	}
	return(SH_GENERIC_ERROR);
} else if (objc->ioprop != NULL) {
	SCHEMA *s;
	s = shSchemaGetFromType(objc->iotype);
	if (s == NULL) {
		thError("%s: ERROR - null (schema) for (iotype)", name);
		return(SH_GENERIC_ERROR);
	} else if (s->destruct == NULL) {
		thError("%s: ERROR - null (destructor) for (iotype)", name);
		return(SH_GENERIC_ERROR);
	}
	s->destruct((void *) objc->ioprop);
	objc->ioprop = NULL;
}
if (objc->ioname != NULL) {
	thFree(objc->ioname);
	objc->ioname = NULL;
}

if (objc->fitmask != NULL) {
	thFitmaskDel(objc->fitmask);
	objc->fitmask = NULL;
}

thFree(objc);
return(SH_SUCCESS);
}

RET_CODE thObjcGetIopar(THOBJC *objc, void **p, TYPE *t) {
char *name = "thObjcGetIopar";

if (objc == NULL && p == NULL) {
	thError("%s: WARNING - null input and place holder", name);
	return(SH_SUCCESS);
} 
if (objc == NULL) {
	thError("%s: ERROR - null input, cannot return any io parameter for the object", name);
	*p = NULL;
	return(SH_GENERIC_ERROR);
}
if (p == NULL && t == NULL) {
	thError("%s: ERROR - null place holder, cannot return any io parameter for the (objc: %d)", name, objc->thid);
	return(SH_GENERIC_ERROR);
}
if (p != NULL) *p = objc->ioprop;
if (t != NULL) {
	TYPE tx;
	tx = objc->iotype;
	if (tx != UNKNOWN_SCHEMA) {
		*t = tx;
	} else {
		char *ioname;
		ioname = objc->ioname;
		if (ioname == NULL || strlen(ioname) == 0) {
			thError("%s: WARNING - empty or null type indicator for io parameter for (objc: %d)", name, objc->thid);
		} else {
			tx = shTypeGetFromName(ioname);
			*t = tx;
			objc->iotype = tx;
			if (tx == UNKNOWN_SCHEMA) thError("%s: WARNING - io parameter (%s) for (objc: %d) doesn't have an uploaded schema", name, ioname, objc->thid);
			
		}
	}
}

return(SH_SUCCESS);
}		


RET_CODE thPhpropsGetNcolor(PHPROPS *p, int *ncolor) {
char *name = "thPhpropsGetNcolor";
if (p == NULL && ncolor == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
} else if (p == NULL) {
	thError("%s: ERROR - null input", name);
} else if (ncolor == NULL) {
	thError("%s: WARNING - null placeholder for (ncolor)", name);
	return(SH_SUCCESS);
}
*ncolor = NCOLOR;
return(SH_SUCCESS);
}

RET_CODE thPhpropsGetObjmask(PHPROPS *p, OBJMASK **mask) {
char *name = "thPhpropsGetObjmask";
if (mask == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (p == NULL) {
	thError("%s: ERROR - null input (phprops)", name);
	return(SH_GENERIC_ERROR);
}
*mask = p->objmask;
return(SH_SUCCESS);
}

RET_CODE thPhpropPutObjmask(PHPROPS *p, OBJMASK *mask) {
char *name = "thPhpropPutObjmask";
if (p == NULL) {
	thError("%s: ERROR - null (phprops)", name);
	return(SH_GENERIC_ERROR);
}
if (mask == NULL) {
	thError("%s: WARNING - null (objmask) to be inserted in (phprops)", name);
} else if (p->objmask != NULL) {
	thError("%s: WARNING - an (objmask) found in (phprops) - replacing it without deletion", name);
}
p->objmask = mask; 
return(SH_SUCCESS);
}

RET_CODE thPropCopy(THPROP *source, THPROP **target) {
char *name = "thPropCopy";

thError("%s: ERROR - function not supported", name);
return(SH_GENERIC_ERROR);
}

RET_CODE thPropCopyChain(CHAIN *schain, CHAIN **tchain) {
char *name = "thPropCopyChain";
if (tchain == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (schain == NULL) {
	thError("%s: ERROR - null input", name);
	*tchain = NULL;
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
CHAIN *chain = shChainNew("THPROP");
int i, n;
n = shChainSize(schain);
for (i = 0; i < n; i++) {	
	THPROP *sprop, *tprop = NULL;
	sprop = shChainElementGetByPos(schain, i);
	status = thPropCopy(sprop, &tprop);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy (thprop) at location (i = %d)", name, i);
		shChainDestroy(chain, &thPropDel);
		*tchain = NULL;
		return(status);
	} 
	shChainElementAddByPos(chain, tprop, "THPROP", TAIL, AFTER);
}

*tchain = chain;
return(SH_SUCCESS);
}

RET_CODE thPropAppendChain(CHAIN *schain, CHAIN *tchain) {
char *name = "thPropAppendChain";
if (tchain == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_SUCCESS);
}
if (schain == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
int i, n;
n = shChainSize(schain);
for (i = 0; i < n; i++) {
	THPROP *sprop, *tprop = NULL;
	sprop = shChainElementGetByPos(schain, i);
	status = thPropCopy(sprop, &tprop);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy (thprop) at location (i = %d)", name, i);
		return(status);
	} 
	shChainElementAddByPos(tchain, tprop, "THPROP", TAIL, AFTER);
}

return(SH_SUCCESS);
}

RET_CODE thObjcCopy(THOBJC *source, THOBJC **target, unsigned int cmode) {
char *name = "thObjcCopy";
if (target == NULL) {
	thError("%s: WARNING - null target placeholder", name);
	return(SH_SUCCESS);
}
if (source == NULL) {
	thError("%s: ERROR - null source", name);
	*target = NULL;
	return(SH_GENERIC_ERROR);
}
THOBJC *objc = thObjcNew();
/* copying the base - always copied */
objc->sdssid = source->sdssid;
objc->thid = source->thid;
objc->sdsstype = source->sdsstype;
strcpy(objc->sdssname, source->sdssname);
objc->thobjctype = source->thobjctype;

objc->band = source->band;

/* private space handling */
objc->copy = 1;
objc->source = source;
/* end of private space handling */

RET_CODE status;
size_t len = 0;
if (cmode & COPY_PHPROP) {
	objc->phtype = source->phtype;
	if (source->phname != NULL) {
		if (objc->phname == NULL) objc->phname = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
		strcpy(objc->phname, source->phname);
	} else if (objc->phname != NULL) {
		strcpy(objc->phname, "");
	}
	if (source->phprop == NULL) {
		thError("%s: WARNING - null (phprop) found in (objc: %x)", name, source->sdssid);
	} else {
		/* allocate new PHPROP and then copy the contents */
		SCHEMA *s = NULL;
		status = get_schema_name_type(source->phname, source->phtype, &s);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get the (schema) for (ioprop) in (objc: %x)", name, objc->sdssid);
			thObjcDel(objc);
			*target = NULL;
			return(status);
		} 
		if (s != NULL) {
			if (objc->phprop != NULL) {
				objc->phprop = s->construct();
				len = s->size;
				memcpy(objc->phprop, source->phprop, len);	
			} else {
				thError("%s: WARNING - null (phprop) found while (schema) is not null for (objc: %x)", name, objc->sdssid);
				objc->phprop = NULL;
			}
		} else {
			if (source->phprop != NULL) {
				thError("%s: ERROR - no schema for non-null (phprop) in (objc: %x)", name, objc->sdssid);
				thObjcDel(objc);
				*target = NULL;
				return(SH_GENERIC_ERROR);
			}
			objc->phprop = NULL;
		}	
	}
} else {
	objc->phtype = UNKNOWN_SCHEMA;
	if (objc->phname != NULL) strcpy(objc->phname, "");
	objc->phprop = NULL;
}

if (cmode & COPY_THPROP) {
	status = thPropAppendChain(source->thprop, objc->thprop);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy (thrprop) chain in (objc: %x)", name, source->sdssid);
		thObjcDel(objc);
		*target = NULL;
		return(SH_GENERIC_ERROR);
	}
} else if (objc->thprop != NULL) {
	shChainDel(objc->thprop);
	objc->thprop = NULL;
}

if (cmode & COPY_IOPROP) {
	objc->iotype = source->iotype;
	if (source->ioname != NULL) {
		if (objc->ioname == NULL) objc->ioname = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
		strcpy(objc->ioname, source->ioname);
	} else if (objc->ioname == NULL) {
		strcpy(objc->ioname, "");
	}
	if (source->ioprop == NULL) {
		thError("%s: WARNING - null (ioprop) found in (objc: %x)", name, source->sdssid);
	} else {	
	/* allocate new ioprop and copy the contents */
		SCHEMA *s = NULL;
		status = get_schema_name_type(source->ioname, source->iotype, &s);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get the (schema) for (ioprop) in (objc: %x)", name, objc->sdssid);
			thObjcDel(objc);
			*target = NULL;
			return(status);
		}
		if (s != NULL) {
			if (source->ioprop != NULL) {
				objc->ioprop = s->construct();
				len = s->size;
				memcpy(objc->ioprop, source->ioprop, len);	
			} else {
				thError("%s: WARNING - null (ioprop) found while (schema) is not null for (objc: %x)", name, objc->sdssid);
				objc->ioprop = NULL;
			}
		} else {
			if (source->ioprop != NULL) {
				thError("%s: ERROR -  no schema for non-null (ioprop) in (objc: %x)", name, objc->sdssid);
				thObjcDel(objc);
				*target = NULL;
				return(SH_GENERIC_ERROR);
			}
			objc->ioprop = NULL;
		}

	}
} else {
	objc->iotype = UNKNOWN_SCHEMA;
	if (objc->ioname != NULL) strcpy(objc->ioname, "");
	objc->ioprop = NULL;
}

*target = objc;
return(SH_SUCCESS);
}

RET_CODE thObjcCopyChain(CHAIN *sChain, CHAIN **tchain, unsigned int cmode) {
char *name = "thObjcCopyChain"; 
if (tchain == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (sChain == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;

CHAIN *chain = shChainNew("THOBJC");
int i, n;
n = shChainSize(sChain);
for (i = 0; i < n; i++) {
	THOBJC *sobjc, *tobjc = NULL;
	sobjc = shChainElementGetByPos(sChain, i);
	status = thObjcCopy(sobjc, &tobjc, cmode);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not make a copy of (thobjc) at location (i = %d)", name, i);
		shChainDestroy(chain, &thObjcDel);
		*tchain = NULL;
		return(status);
	}
	shChainElementAddByPos(chain, tobjc, "THOBJC", TAIL, AFTER);
}
*tchain = chain;
return(SH_SUCCESS);
}

RET_CODE thObjcAppendChain(CHAIN *sChain, CHAIN *tchain, unsigned int cmode) {
char *name = "thObjcAppendChain"; 
if (tchain == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (sChain == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
int i, n;
n = shChainSize(sChain);
for (i = 0; i < n; i++) {
	THOBJC *sobjc, *tobjc = NULL;
	sobjc = shChainElementGetByPos(sChain, i);
	status = thObjcCopy(sobjc, &tobjc, cmode);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not append (thobjc) at location (i = %d) of source chain", name, i);
		return(status);
	}
	shChainElementAddByPos(tchain, tobjc, "THOBJC", TAIL, AFTER);
}
return(SH_SUCCESS);
}


RET_CODE get_schema_name_type(char *sname, TYPE type, SCHEMA **s) {
char *name = "get_schema_name_type";
shAssert(s != NULL);
if (sname == NULL || strlen(sname) == 0) {
	if (type == UNKNOWN_SCHEMA) {
		*s = NULL;
		return(SH_SUCCESS);
	}
	*s = shSchemaGetFromType(type); 
	return(SH_SUCCESS);
} else if (type == UNKNOWN_SCHEMA) {
	*s = shSchemaGet(sname);
	return(SH_SUCCESS);
} else {
	TYPE type2 = shTypeGetFromName(sname);
	if (type2 != type) {
		thError("%s: ERROR - mismatching (type) and (name)", name);
		*s = NULL;
		return(SH_GENERIC_ERROR);
	}
	*s = shSchemaGetFromType(type);
	return(SH_SUCCESS);
}

thError("%s: ERROR - source code problem", name);
return(SH_GENERIC_ERROR);
}
RET_CODE thEmptyLineWrite(FILE *fil, char *c) {
char *name = "thEmptyLineWrite";
if (fil == NULL) {
	thError("%s: ERROR - cannot export to null file", name);
	return(SH_GENERIC_ERROR);
}

size_t clen = 0;
if (c != NULL) clen = strlen(c);

char line[clen * EMPTY_LINE_LENGTH + 2];
strcpy(line, "#");
if (clen != 0) {
	int i; char *p;
	for ( i=0, p = line + 1; i < EMPTY_LINE_LENGTH; ++i, p += clen ) {
		memcpy(p, c, clen);
	}
	*p = '\0';
}
fprintf(fil, "%s\n", line);
return(SH_SUCCESS);
}
 

RET_CODE thObjcWriteHdr(FILE *fil, THOBJC *objc) {
char *name = "thObjcWriteHdr";
if (fil == NULL) {
	thError("%s: ERROR - cannot export to null file", name);
	return(SH_GENERIC_ERROR);
}
if (objc == NULL) {
	thError("%s: ERROR - cannot export null (objc)", name);
	return(SH_GENERIC_ERROR);
}
fprintf(fil, "sdssid   = 0x%08lX, thid     = 0x%08X \n", (long unsigned int) objc->sdssid, (unsigned int) objc->thid);
fprintf(fil, "sdsstype = 0x%08X, sdssname  = '%20s', thobjctype = 0x%08X \n", (unsigned int) objc->sdsstype, objc->sdssname, (unsigned int) objc->thobjctype);
fprintf(fil, "band     = %2d   \n", objc->band);
fprintf(fil, "phname   = '%10s' \n", objc->phname);
fprintf(fil, "ioname   = '%10s', ioprop = %p \n", objc->ioname, objc->ioprop);
return(SH_SUCCESS);
}

RET_CODE thObjcWrite(FILE *fil, THOBJC *objc) {
char *name = "thObjcWrite";
if (fil == NULL) {
	thError("%s: ERROR - cannot export to null file", name);
	return(SH_GENERIC_ERROR);
}
if (objc == NULL) {
	thError("%s: ERROR - cannot export null (objc)", name);
	return(SH_GENERIC_ERROR);
}
#if DEEP_DEBUG_CAPTURE
printf("%s: export (objc = %p) \n", name, objc);
#endif

RET_CODE status;
status = thEmptyLineWrite(fil, "-");
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not output the empty line for separation", name);
	return(status);
}
status = thObjcWriteHdr(fil, objc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not write header for (objc)", name);
	return(status);
}

char *ioname = objc->ioname;
TYPE iotype = objc->iotype;
void *ioprop = objc->ioprop;
SCHEMA *s = NULL;
status = get_schema_name_type(ioname, iotype, &s);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get proper schema for io-structure in (objc)", name);
	return(status);
}
#if DEEP_DEBUG_CAPTURE
printf("%s: (ioname = '%s'), (ioprop = %p) \n", name, s->name, ioprop);
#endif
if (ioprop == NULL && s == NULL) {
	thError("%s: WARNING - null (ioprop), unsupported (iotype)", name);
} else if (s == NULL) {
	thError("%s: WARNING - null (schema)", name);
} else if (s->write == NULL) {
	thError("%s: WARNING - null (write) function for (ioname = '%s')", 
		name, s->name);	
} else {
	RET_CODE (*mywrite)(FILE *, char *, void *);
	mywrite = s->write;
	if (!strcmp(s->name, "SKYPARS") && mywrite == &shDumpGenericWrite) {
		status = thDumpSkyparsWrite(fil, ioprop);
	} else if (!strcmp(s->name, "OBJCPARS") && mywrite == &shDumpGenericWrite) {
		status = thDumpObjcparsWrite(fil, ioprop, objc->sdsstype);
	} else {
		#if DEEP_DEBUG_CAPTURE
		printf("%s: writing (iotype = '%s') in (%p) using function (%p), expecting (%p) \n", name, ioname, ioprop, mywrite, &shDumpGenericWrite);
	#	endif
		status = mywrite(fil, s->name, ioprop);
	}
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not write (ioprop) for (ioname = '%s')", 
			name, s->name);
		return(status);
	}
	}

return(SH_SUCCESS);
}

RET_CODE thObjcGetType(THOBJC *objc, THOBJCTYPE *type) {
shAssert(objc != NULL);
shAssert(type != NULL);
*type = objc->thobjctype;
return(SH_SUCCESS);
}

RET_CODE thObjcGetClassName(THOBJC *objc, char **classname) {
char *name = "thObjcGetClassName";
shAssert(objc != NULL);
shAssert(classname != NULL);
THOBJCTYPE type = objc->thobjctype;
RET_CODE status;
status = thObjcNameGetFromType(type, classname);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not find classname for objc type (=%d)", name, (int) type);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE thObjcGetClassType(THOBJC *objc, THOBJCTYPE *type) {
char *name = "thObjcGetClassType";
shAssert(objc != NULL);
shAssert(type != NULL);
*type = objc->thobjctype;

return(SH_SUCCESS);
}



RET_CODE thObjcGetNmodel(THOBJC *objc, int *nmodel) {
char *name = "thObjcGetNmodel";
if (objc == NULL && nmodel == NULL) {
	thError("%s: ERROR - null input and output placeholder", name);
	return(SH_SUCCESS);
}
if (objc == NULL) {
	thError("%s: ERROR - null input", name);
	*nmodel = -1;
	return(SH_GENERIC_ERROR);
}
if (nmodel == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}


int this_nmodel = -1;
if (objc->thprop == NULL) {
	thError("%s: ERROR - improperly allocated (objc = %p), null thprop chain found", name, (void *) objc);
	*nmodel = -1;
	return(SH_GENERIC_ERROR);
} 
this_nmodel = shChainSize(objc->thprop);
if (this_nmodel == 0) {
	thError("%s: WARNING - empty (thprop) chain in (objc = %p)", name, (void *) objc);
}
*nmodel = this_nmodel;
int that_nmodel = -1;
THOBJCTYPE objctype;
RET_CODE status;
status = thObjcGetClassType(objc, &objctype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (objctype) for (objc = %p)", name, (void *) objc);
	return(status);
}	
status = thObjcGetNmodelByType(objctype, &that_nmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (nmodel) appropriate for (objctype = %d)", name, (int) objctype);
	return(status);
}
if (this_nmodel != that_nmodel) {
	thError("%s: WARNING - (%d) models available in (objc = %d) while (objctype = %d) requires (%d) models", name, this_nmodel, (int) objctype, that_nmodel);
}

return(SH_SUCCESS);
}

RET_CODE thObjcGetModelPropByIndex(THOBJC *objc, int i, THPROP **prop) {
char *name = "thObjcGetModelPropByIndex";
shAssert(objc != NULL);
shAssert(prop != NULL);
CHAIN *pchain = objc->thprop;
if (pchain == NULL) {
	thError("%s: ERROR - improperly allocated (objc = %p)", name, (void *) objc);
	return(SH_GENERIC_ERROR);
}
THPROP *this_prop = NULL;
int n = shChainSize(pchain);
if (i >= 0 && i < n) {
	this_prop = shChainElementGetByPos(pchain, i);
} else {
	thError("%s: ERROR - model index (%d) out of acceptable range (nmodel = %d)", name, i, n);
	*prop = NULL;
	return(SH_GENERIC_ERROR);
}
*prop = this_prop;
return(SH_SUCCESS);
}

/* thObjcTypes might not be a good place for the following Draw functions, but since there is noy upper level module for thObjcTypes, I have placed them here */

RET_CODE thWObjcIoDrawRecord(WOBJC_IO *s, WOBJC_IO *t, char *rname, THPIX nsigma) {
char *name = "thWObjcIoDrawRecord";
if (s == NULL || t == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (rname == NULL || strlen(rname) == 0) {
	thError("%s: ERROR - empty or null record name submitted", name);
	return(SH_GENERIC_ERROR);
}
if (nsigma < (THPIX) 0.0) {
	thError("%s: ERROR - cannot accept negative width (nsigma = %g)", name, nsigma);
	return(SH_GENERIC_ERROR);
}
char errname[MX_STRING_LEN];
errname[0] = '\0';
strcpy(errname, rname);
strcat(errname, "Err");
/* check that both rname and errname exist in the schema for WOBJC_IO */
#if USE_FNAL_OBJC_IO
char *wobjc_name = "FNAL_OBJC_IO";
#else
char *wobjc_name = "OBJC_IO";
#endif

TYPE type = shTypeGetFromName(wobjc_name);
if (type == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - schema for 'WOBJC_IO' is not loaded", name);
	return(SH_GENERIC_ERROR);
}
SCHEMA_ELEM *se = shSchemaElemGetFromType(type, rname);
if (se == NULL) {
	thError("%s: ERROR - there is no record '%s' in WOBJC_IO", name, rname);
	return(SH_GENERIC_ERROR);
}
se = shSchemaElemGetFromType(type, errname);
if (se == NULL) {
	thError("%s: ERROR - there is no error record '%s' in WOBJC_IO", name, errname);
	return(SH_GENERIC_ERROR);
}
THPIX *x = record_get_arr(s, type, rname);
THPIX *err = record_get_arr(s, type, errname);

THPIX y[NCOLOR];
int i;
printf("%s: ", rname);
for (i = 0; i < NCOLOR; i++) {
	y[i] = x[i] + err[i] * nsigma * phGaussdev();
	if (y[i] * x[i] < 0.0) y[i] *= -1.0;
	printf("(%g, %g) ", x[i], y[i]);
}
printf("\n");
record_put_arr(t, type, rname, y, NCOLOR);
return(SH_SUCCESS);
}

RET_CODE thWObjcIoDraw(WOBJC_IO *s, WOBJC_IO *t, char **rnames, int nrecord, THPIX nsigma) {
char *name = "thWObjcIoDraw";
if (s == NULL || t == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (rnames == NULL || nrecord <= 0) {
	thError("%s: ERROR - no record list submitted", name);
	return(SH_GENERIC_ERROR);
}
if (nsigma < (THPIX) 0.0) {
	thError("%s: ERROR - cannot accept negative width (nsigma = %g)", name, nsigma);
	return(SH_GENERIC_ERROR);
}
int i;
RET_CODE status;
for (i = 0; i < nrecord; i++) {
	char *rname = rnames[i];
	status = thWObjcIoDrawRecord(s, t, rname, nsigma);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not draw for the record (i = %d, '%s')", name, i, rname);
		return(status);
	}
}
return(SH_SUCCESS);
}

RET_CODE output_phobjc_checklist(char *comment, WOBJC_IO *phobjc) {
shAssert(phobjc != NULL);

int band = 2; /* rband */

if (phobjc->objc_type != OBJ_GALAXY && phobjc->objc_type != OBJ_STAR) return(SH_GENERIC_ERROR);

int id = phobjc->id;
char *type;
type = shEnumNameGetFromValue("OBJ_TYPE", phobjc->objc_type);
float rowc = phobjc->objc_rowc;
float colc = phobjc->objc_colc;
float counts_dev = phobjc->counts_deV[band];
float counts_exp = phobjc->counts_exp[band];
float counts_star = phobjc->psfCounts[band];
float fracpsf = phobjc->fracPSF[band];
float counts_model1 = phobjc->counts_model[band];
float counts_model2 = counts_dev * fracpsf + counts_exp * (1.0 - fracpsf);
float error = (counts_model2 - counts_model1) / counts_model1;
float r_dev = phobjc->r_deV[band];
float r_exp = phobjc->r_exp[band];
float r_petro = phobjc->petroRad[band];
#if USE_FNAL_OBJC_IO
float counts_dev2 = phobjc->counts_deV2[band];
float counts_exp2 = phobjc->counts_exp2[band];
float r_dev2 = phobjc->r_deV2[band];
float r_exp2 = phobjc->r_exp2[band];
#endif

if (phobjc->objc_type == OBJ_GALAXY) {
	printf("%s: SDSS - objc (%d), type (%s), (rowc, colc) = (%g, %g), (counts_dev, counts_exp, fracpsf) = (%g, %g, %g), (counts_model1, counts_model2, error) = (%g, %g, %g) (r_dev, r_exp, r_petro) = (%g, %g, %g) \n", comment, id, type, rowc, colc, counts_dev, counts_exp, fracpsf, counts_model1, counts_model2, error, r_dev, r_exp, r_petro);
#if USE_FNAL_OBJC_IO
	printf("%s: SDSS - objc (%d), type (%s), (rowc, colc) = (%g, %g), second component: (counts_dev2, counts_exp2) = (%g, %g), (r_dev2, r_exp2) = (%g, %g) \n", comment, id, type, rowc, colc, counts_dev2, counts_exp2, r_dev2, r_exp2);
#endif
} else if (phobjc->objc_type == OBJ_STAR) {
	printf("%s: SDSS - objc (%d), type (%s), (rowc, colc) = (%g, %g), (counts_star) = (%g) \n", comment, id, type, rowc, colc, counts_star);
}

return(SH_SUCCESS);
}

RET_CODE output_objc_checklist(char *comment, THOBJC *objc) {
shAssert(objc != NULL);
PHPROPS *phprop = objc->phprop;
if (phprop == NULL) {
	printf("%s: null (phprop) found in (objc = %p) \n", comment, objc);
	return(SH_SUCCESS);
}
WOBJC_IO *phobjc = phprop->phObjc;

output_phobjc_checklist(comment, phobjc);
OBJ_TYPE phtype = objc->sdsstype;

OBJCPARS *ioprop = objc->ioprop;
if (ioprop != NULL) {
	if (phtype == OBJ_GALAXY) {
		float counts_dev_io = ioprop->counts_deV;
		float counts_exp_io = ioprop->counts_Exp;
		float counts_dev1_io = ioprop->counts_deV1;
		float counts_exp1_io = ioprop->counts_Exp1;
		float counts_dev2_io = ioprop->counts_deV2;
		float counts_exp2_io = ioprop->counts_Exp2;
		float counts_model_io = ioprop->counts_model;
		float mag_model_io = ioprop->mag_model;
		float r_dev_io = ioprop->re_deV;
		float r_exp_io = ioprop->re_Exp;
		float r_dev1_io = ioprop->re_deV1;
		float r_exp1_io = ioprop->re_Exp1;
		float r_dev2_io = ioprop->re_deV2;
		float r_exp2_io = ioprop->re_Exp2;
		
		printf("%s: SOHO - (counts_dev, counts_exp, counts_dev1, counts_exp1, counts_dev2, counts_exp2, counts_model, mag_model) = (%g, %g, %g, %g, %g, %g, %g, %g) \n", 
	comment, counts_dev_io, counts_exp_io, counts_dev1_io, counts_exp1_io, counts_dev2_io, counts_exp2_io, counts_model_io, mag_model_io);
		printf("%s: SOHO - (r_dev, r_exp, r_dev1, r_exp1, r_dev2, r_exp2) = (%g, %g, %g, %g, %g, %g) \n", comment, r_dev_io, r_exp_io, r_dev1_io, r_exp1_io, r_dev2_io, r_exp2_io);
	
	} else if (phtype == OBJ_STAR) {
		float counts_star_io = ioprop->counts_star;
		float mag_star_io = ioprop->mag_star;
		printf("%s: SOHO - (counts_star, mag_star) = (%g, %g) \n", comment, counts_star_io, mag_star_io);
	}
} else {
	printf("%s: SOHO: (ioprop) not set \n", comment);
}

return(SH_SUCCESS);
}

RET_CODE output_calib_phobjc_checklist(char *comment, CALIB_PHOBJC_IO *phobjc) {
shAssert(phobjc != NULL);

int band = 2; /* rband */

if (phobjc->objc_type != OBJ_GALAXY && phobjc->objc_type != OBJ_STAR) return(SH_GENERIC_ERROR);

int id = phobjc->id;
char *type;
type = shEnumNameGetFromValue("OBJ_TYPE", phobjc->objc_type);
float rowc = phobjc->objc_rowc;
float colc = phobjc->objc_colc;
float mag_dev = phobjc->deVMag[band];
float mag_exp = phobjc->expMag[band];
float mag_star = phobjc->psfMag[band];
float fracpsf = phobjc->fracdeV[band];
float mag_model = phobjc->modelMag[band];
float mag_cmodel = phobjc->cmodelMag[band];
float error = mag_model - mag_cmodel;

if (phobjc->objc_type == OBJ_GALAXY) {
	printf("%s: SDSS - objc (%d), type (%s), (rowc, colc) = (%g, %g), (mag_dev, mag_exp, fracpsf) = (%g, %g, %g), (mag_model, mag_cmodel, error) = (%g, %g, %g) \n", comment, id, type, rowc, colc, mag_dev, mag_exp, fracpsf, mag_model, mag_cmodel, error);
} else if (phobjc->objc_type == OBJ_STAR) {
	printf("%s: SDSS - objc (%d), type (%s), (rowc, colc) = (%g, %g), (mag_star) = (%g) \n", comment, id, type, rowc, colc, mag_star);
}

return(SH_SUCCESS);
}

void thPhpropChainPrintMatchStat(CHAIN *phprops) {
char *name = "thPhpropChainPrintMatchStat";
shAssert(phprops != NULL);
if (shChainSize(phprops) == 0) {
	thError("%s: WARNING - empty phprops chain passed", name);
	return;
}
int nobjc = shChainSize(phprops);
float *dm1, *dm2, *dm3, *drowc, *dcolc;
float sm1 = 0.0, sm2 = 0.0, sm3 = 0.0, srowc = 0.0, scolc = 0.0;
int i;
for (i = 0; i < nobjc; i++) {
	PHPROPS *phprop = shChainElementGetByPos(phprops, i);
	WOBJC_IO *phobjc = NULL;
	CALIB_WOBJC_IO *cwphobjc = NULL;
	CALIB_PHOBJC_IO *cphobjc = NULL;
	RET_CODE status = thPhpropsGet(phprop, &phobjc, NULL, &cwphobjc, &cphobjc, NULL);
	shAssert(status == SH_SUCCESS);
	float drowc_i = phobjc->objc_rowc - cphobjc->objc_rowc;
	float dcolc_i = phobjc->objc_colc - cphobjc->objc_colc;
	srowc += pow(drowc_i, 2.0);
	scolc += pow(dcolc_i, 2.0);
}
srowc /= (float) nobjc;
scolc /= (float) nobjc;
srowc = sqrt(srowc);
scolc = sqrt(scolc);
printf("%s: total of (%d) objects read from the list \n", name, nobjc);
printf("%s: SE(rowc) = %g pixels \n", name, (float) srowc);
printf("%s: SE(colc) = %g pixels \n", name, (float) scolc);

return;
}

RET_CODE thObjcGetPropByMname(THOBJC *objc, char *mname, THPROP **prop) {
char *name = "thObjcGetPropByMname";
if (objc == NULL && mname == NULL && prop == NULL) {
	thError("%s: WARNING - null input and output placeholder", name);
	return(SH_SUCCESS);
}
if (prop == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (mname == NULL || strlen(mname) == 0) {
	thError("%s: ERROR - null or empty (mname) passed", name);
	*prop = NULL;
	return(SH_GENERIC_ERROR);
}
if (objc == NULL) {
	thError("%s: ERROR - null (objc) passed", name);
	*prop = NULL;
	return(SH_GENERIC_ERROR);
}
CHAIN *props = objc->thprop;
int n = shChainSize(props);
int i, found = 0, err = 0;
RET_CODE status;
for (i = 0; i < n; i++) {
	THPROP *this_prop = shChainElementGetByPos(props, i);
	if (this_prop == NULL) {
		err++;
	} else {
		char *this_mname = NULL;
		status = thPropGet(this_prop, &this_mname, NULL, NULL);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (mname) from (thprop)", name);
			return(status);
		}
		if (this_mname == NULL || strlen(mname) == 0) {
			err++;
		} else {
			if (!strcmp(mname, this_mname)) {
				if (found == 0) {
					*prop = this_prop;
					found++;
				} else {
					found++;
				}
			}
		}
	}
}
if (err > 0) {
	thError("%s: ERROR - found (%d) error instances in (objc)", name, err);
}
if (found == 0) *prop = NULL;
if (found > 1) {
	thError("%s: WARNING - found (%d) instances of '%s' in (objc)", name, mname);
} 
return(SH_SUCCESS);
}

RET_CODE thObjcGetPhprops(THOBJC *objc, PHPROPS **phprops) {
char *name = "thObjcGetPhprops";
if (phprops == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (objc == NULL) {
	thError("%s: ERROR - null input (objc)", name);
	*phprops = NULL;
	return(SH_GENERIC_ERROR);
}
*phprops = objc->phprop;
#if DEBUG_OBJCTYPES
if (objc->phprop == NULL) {
	thError("%s: WARNING - null (phprops) found in (objc = %p)", name, (void *) objc);
}
#endif
return(SH_SUCCESS);
}

RET_CODE thObjcGetFitmask(THOBJC *objc, FITMASK **fitmask) {
char *name = "thObjcGetFitmask";
if (fitmask == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (objc == NULL) {
	thError("%s: ERROR - null input (thobjc)", name);
	return(SH_GENERIC_ERROR);
}
*fitmask = objc->fitmask;
return(SH_SUCCESS);
}

RET_CODE thObjcCompare(THOBJC *objc1, THOBJC *objc2, int *comparison) {
char *name = "thObjcCompare";
if (objc1 == NULL || objc2 == NULL || comparison == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (objc1 == objc2) {
	*comparison = 0;
	return(SH_SUCCESS);
}
RET_CODE status;
PHPROPS *phprops1 = NULL, *phprops2 = NULL;
status = thObjcGetPhprops(objc1, &phprops1);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (phprops) for (objc1)", name);
	return(status);
}
status = thObjcGetPhprops(objc2, &phprops2);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (phprops) for (objc2)", name);
	return(status);
}
if (phprops1 == phprops2 && phprops1 != NULL) {
	*comparison = 0;
	return(SH_SUCCESS);
} else if ((phprops1 == NULL || phprops2 == NULL) && phprops1 != phprops2) {
	*comparison = 1;
	return(SH_SUCCESS);
} else if (phprops1 == NULL && phprops2 == NULL) {
	thError("%s: WARNING - (phprops) found to be null in both objects", name);
	*comparison = 0;
	return(SH_SUCCESS);
}

*comparison = 1;
return(SH_SUCCESS);
}


RET_CODE thObjcGetPhCenter(THOBJC *objc, THPIX *xc, THPIX *yc) {
char *name = "thObjcGetPhCenter";
if (xc == NULL && yc == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (objc == NULL) {
	thError("%s: ERROR - null input (objc)", name);
	if (xc != NULL) *xc = THNAN;
	if (yc != NULL) *yc = THNAN;
	return(SH_GENERIC_ERROR);
}

THOBJCTYPE SKY_OBJC = UNKNOWN_OBJC;
MSkyGetType(&SKY_OBJC);

THOBJCTYPE type = objc->thobjctype;
if (type == SKY_OBJC) {
	thError("%s: WARNING - (sky) object passed - cannot locate the center", name);
	if (xc != NULL) *xc = THNAN;  
	if (yc != NULL) *yc = THNAN; 
	return(SH_SUCCESS);
}
if (type == UNKNOWN_OBJC) {
	thError("%s: WARNING - (unknown_objc) passed - cannot locate the center", name);
}

RET_CODE status;
PHPROPS *phprops = NULL;
status = thObjcGetPhprops(objc, &phprops);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (phprops) from (objc)", name);
	if (xc != NULL) *xc = THNAN; 
	if (yc != NULL) *yc = THNAN;
	return(SH_GENERIC_ERROR);
}
if (phprops == NULL) {
	thError("%s: ERROR - null (phprops) found in (objc)", name);
	if (xc != NULL) *xc = THNAN;
	if (yc != NULL) *yc = THNAN; 
	return(SH_GENERIC_ERROR); 
}
WOBJC_IO *wobjc = NULL;
status = thPhpropsGet(phprops, &wobjc, NULL, NULL, NULL, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (wobjc) from (phprops)", name);
	if (xc != NULL) *xc = THNAN;
	if (yc != NULL) *yc = THNAN; 
	return(status);
}
if (wobjc == NULL) {
	thError("%s: ERROR - null (wobjc_io) in (phprops)", name);
	if (xc != NULL) *xc = THNAN;  
	if (yc != NULL) *yc = THNAN;
	return(SH_GENERIC_ERROR);
}

if (xc != NULL) *xc = (THPIX) wobjc->objc_rowc;
if (yc != NULL) *yc = (THPIX) wobjc->objc_colc;
return(SH_SUCCESS);
}


