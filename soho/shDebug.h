#include "strings.h"
#include "sohoEnv.h"
#include "dervish.h"

RET_CODE shIoFpcDebug(char *name, 
		      HDR *header, REGION *reg);

RET_CODE shPrintHdr(HDR *header);

RET_CODE shPrintReg(REGION *reg);
