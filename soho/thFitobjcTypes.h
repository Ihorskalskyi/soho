#ifndef THFITOBJCTYPES_H
#define THFITOBJCTYPES_H

#include "dervish.h"

typedef enum fitobjc_type {
  PROCESS_TYPE,
  FRAME_TYPE,
  OBJCMODEL_TYPE,
  FUNCMODEL_TYPE,
  N_FITOBJC_TYPE,
  UNKNOWN_FITOBJC
} FITOBJC_TYPE;


typedef struct fitobjc {
  /* contents of the current FITOBJC */
  void *val;
  FITOBJC_TYPE type;
  /* upper level connections */
  void *upper;
  void *map;
} FITOBJC;

FITOBJC *thFitobjcNew();
RET_CODE thFitobjcDel(FITOBJC *objc);
RET_CODE thFitobjcPut(FITOBJC *objc, void *val, FITOBJC_TYPE type, 
		      void *upper, void *map);

#endif
