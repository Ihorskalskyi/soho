#!/bin/bash

source "./generate_temporary_file.bash"
source "./run_commands_in_file.bash"

name="photo-multi-objc"


ppsf=0.001
run=$DEFAULT_SDSS_RUN
rerun=$DEFAULT_SDSS_RERUN

logfile="./photo-batch-multi-objc-logfile.log"
band="{u g r i z}"

#declare -a camcol_a=(1 2 3 4 5 6)
#n1=6

declare -a camcol_a=(2 4 5 6)
n1=4


#declare -a dest_a=("sdss-g0-s0-lrg1"  "sdss-gA-sA-lrg1" "sdss-gA-s0-lrg1" "sdss-g0-s0-lrgA" "sdss-gA-s0-lrgA" "sdss-gA-sA-lrgA" "sdss-gA-s0-cd1" "sdss-gA-sA-cd1" "sdss-g0-s0-cd1") 
#declare -a model_a=("sdss-g0-s0-lrg1" "sdss-gA-sA-lrg1" "sdss-gA-s0-lrg1" "sdss-g0-s0-lrgA" "sdss-gA-s0-lrgA" "sdss-gA-sA-lrgA" "sdss-gA-s0-cd1" "sdss-gA-sA-cd1" "sdss-g0-s0-cd1")
#n2=6

declare -a dest_a=("sdss-gA-s0-cd1" "sdss-gA-sA-cd1")
declare -a model_a=("sdss-gA-s0-cd1" "sdss-gA-sA-cd1")
n2=2


#stripe 82
#4797
declare -a run_a=(4797 2700 2703 2708 2709 2960 3565 3434 3437 2968 4207 1742 4247 4252 4253 4263 4288 1863 1887 2570 2578 2579 109 125 211 240 241 250 251 256 259 273 287 297 307 94 5036 5042 5052 2873 21
24 1006 1009 1013 1033 1040 1055 1056 1057 4153 4157 4158 4184 4187 4188 4191 4192 2336 2886 2955 3325 3354 3355 3360 3362 3368 2385 2506 2728 4849 4858 4868 4874 4894 4895 4899 4905 4927 4930 4933 4948 2854 2855 2856
 2861 2662 3256 3313 3322 4073 4198 4203 3384 3388 3427 3430 4128 4136 4145 2738 2768 2820 1752 1755 1894 2583 2585 2589 2591 2649 2650 2659 2677 3438 3460 3458 3461 3465 7674 7183)
n4=120
#n4=1
n4=1

for (( i1=0; i1<$n1; i1++ ))
do

for (( i2=0; i2<$n2; i2++ ))
do

for (( i4=0; i4<$n4; i4++ ))
do

camcol=${camcol_a[$i1]}
model=${model_a[$i2]}
destination=${dest_a[$i2]}
executable=${exec_a[$i2]}

fpsf=${psf_a[$i3]}
psfdir=${psfdir_a[$i3]}
inner_lumcut=${inner_lumcut_a[$i3]}
outer_lumcut=${outer_lumcut_a[$i3]}

run=${run_a[$i4]}

echo "$name: band = $band, camcol = $camcol, destination: $destination"

nfield=2000	
rootdir="/u/khosrow/thesis/opt/soho/multi-object/images/sims"
workdir="$rootdir/$destination"
outdir="$workdir/$rerun/$run"

##
fpCDir="$outdir/$camcol/photo"
cmdDir="$outdir/$camcol/cmd"
field1=0

field2=$nfield
./photo-sim.sh --overwrite --setpsdir --camcol=$camcol --field1=$field1 --field2=$field2 --model=$model --logfile=$logfile --fpCDir=$fpCDir --cmdDir=$cmdDir --run=$run --band=$band

done
done
done
