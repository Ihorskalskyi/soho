#include "thMapTypes.h"
#include "thMSky.h"
#include "thDebug.h"

PARDEF *thPardefNew(char *id) {
PARDEF *pd;
pd = thCalloc(1, sizeof(PARDEF));
pd->id = thCalloc(MX_STRING_LEN, sizeof(char));
if (id != NULL) strcpy(pd->id, id);
pd->type = thCalloc(MX_STRING_LEN, sizeof(char));
pd->pname = thCalloc(MX_STRING_LEN, sizeof(char));
pd->pindex = -1;
pd->ptype = UNKNOWN_PARTYPE;
return(pd);
}

void thPardefDel(PARDEF *pd) {
char *name = "thPardefDel";
if (pd == NULL) return;
if (pd->id != NULL) thFree(pd->id);
if (pd->type != NULL) thFree(pd->type);
if (pd->pname != NULL) thFree(pd->pname);
if (pd->pval == NULL) {
	thFree(pd);
	return;
}
switch (pd->ptype) {
	case INTEGER_PARTYPE:
	thFree((int *) pd->pval);
	break;
	case PIX_PARTYPE:
	thFree((THPIX *) pd->pval);
	break;
	default:
	thError("%s: ERROR - unsupported type - can't free memory for value", name);
	/* can't handle the memory for value if don't know its type */
	}	
thFree(pd);
return;
}

PARLIST *thParlistNew(char *id) {
PARLIST *pl;
pl = thCalloc(1, sizeof(PARLIST));
pl->id = thCalloc(MX_STRING_LEN, sizeof(char));
if (id != NULL) strcpy(pl->id, id);
pl->pars = shChainNew("PARDEF");
return(pl);
}

void thParlistDel(PARLIST *pl) {
if (pl == NULL) return;
if (pl->id != NULL) thFree(pl->id);
if (pl->pars != NULL) shChainDestroy(pl->pars, &thPardefDel);
return;
}

void thPardefPut(PARDEF *pd, char *id, char *type, char *pname, int *pindex, THPAR_TYPE *ptype, void *pval) {
if (pd == NULL) return;

size_t l;
if (id != NULL) {
	l = strlen(id);
	if (pd->id == NULL) pd->id = thCalloc(MX_STRING_LEN, sizeof(char));
	memcpy(pd->id, id, l);
	pd->id[l] = '\0';	
}
if (type != NULL) {
	l = strlen(type);
	if (pd->type == NULL) pd->type = thCalloc(MX_STRING_LEN, sizeof(char));
	memcpy(pd->type, type, l);
        pd->type[l] = '\0';
}
if (pname != NULL) {
	l = strlen(pname);
	if (pd->pname == NULL) pd->pname = thCalloc(MX_STRING_LEN, sizeof(char));
        memcpy(pd->pname, pname, l);
        pd->pname[l] = '\0';
}
if (pindex != NULL) {
	pd->pindex = *pindex;
}
if (ptype != NULL) {
	switch (*ptype) {
	case INTEGER_PARTYPE:
	l = sizeof(int);
	break;
	case PIX_PARTYPE:
	l = sizeof(THPIX);
	break;
	default:
	l = 0;
	pd->ptype = *ptype;
	return;
	break;
	}
	if (pd->ptype == *ptype) {
		if (pval != NULL && pd->pval == NULL) pd->pval = thCalloc(1, l);
		if (pval != NULL) memcpy(pd->pval, pval, l);
	} else {
		pd->ptype = *ptype;
		if (pd->pval != NULL) thFree(pd->pval);
		pd->pval = thCalloc(1, l);
		if (pval != NULL) memcpy(pd->pval, pval, l);
	}		
}

return;
}

/* constructor(s) and deconstrcutor for mapmachine */

MAPMACHINE *thMapmachineNew() {
/*
char *name = "thMapmachineNew";
*/
MAPMACHINE *map;
map = thCalloc(1, sizeof(MAPMACHINE));

map->mapid = thCalloc(MX_STRING_LEN, sizeof(char));
map->mapsource = thCalloc(MX_STRING_LEN, sizeof(char));
map->maptarget = thCalloc(MX_STRING_LEN, sizeof(char));

map->alist = shChainNew("MREC");
map->plist = shChainNew("MREC");

map->band = UNKNOWN_BAND;
map->cflag = ALTERED;

return(map);
}

RET_CODE thMapmachineDel(MAPMACHINE *map) {
char *name = "thMapmachineDel";
if (map == NULL) return(SH_SUCCESS);

int i;
if (map->mapid != NULL) {
	thFree(map->mapid);
	map->mapid = NULL;
}
if (map->mapsource != NULL) {
	thFree(map->mapsource);
	map->mapsource = NULL;
}
if (map->maptarget != NULL) {
	thFree(map->maptarget);
	map->maptarget = NULL;
}

if (map->mnames != NULL) {
	thFree(map->mnames);
	map->mnames = NULL;
}

if (map->pnames != NULL) {
        thFree(map->pnames);
        map->pnames = NULL;
}

if (map->namp <= 0 && map->enames != NULL) {
	thError("%s: ERROR - improperly allocated (mapmachine), namp = 0, while (enames) is non-null", name);
	return(SH_GENERIC_ERROR);
} else if (map->enames != NULL) {
	for (i = 0; i < map->namp; i++) {
		if (map->enames[i] != NULL) {
			thFree(map->enames[i]);
		}
	}
	thFree(map->enames);
	map->enames = NULL;
}

if (map->ps != NULL) {
	thFree(map->ps);
	map->ps = NULL;
}

#if MAP_HAS_SCHEMA_INFO
if (map->rs != NULL && map->namp <= 0) {
	thError("%s: ERROR - improperly allocated (mapmachine), namp = 0, while (rs) is non-null", name);
	return(SH_GENERIC_ERROR);
} else if (map->rs != NULL) {
	for (i = 0; i < map->namp; i++) {
		if (map->rs[i] != NULL) thFree(map->rs[i]);
	}
	thFree(map->rs);
	map->rs = NULL;
}
if (map->etypes != NULL && map->namp <= 0) {
	thError("%s: ERROR - improperly allocated (mapmachine), namp = 0, while (enames) is non-null", name);
	return(SH_GENERIC_ERROR);
} else if (map->etypes != NULL) {
	for (i = 0; i < map->namp; i++) {
		if (map->etypes[i] != NULL) thFree(map->etypes[i]);
	}
	thFree(map->etypes);
	map->etypes = NULL;
}
if (map->rtypes != NULL && map->namp <= 0) {
	thError("%s: ERROR - improperly allocated (mapmachine), namp = 0, while (rtypes) is non-null", name);
	return(SH_GENERIC_ERROR);
} else if (map->rtypes != NULL) {
	for (i = 0; i < map->namp; i++) {
		if (map->rtypes[i] != NULL) thFree(map->rtypes[i]);
	}
	thFree(map->rtypes);
	map->rtypes = NULL;
}

#endif

if (map->alist != NULL) {
	shChainDestroy(map->alist, &thMrecDel);
	map->alist = NULL;
}
if (map->plist != NULL) {
	shChainDestroy(map->plist, &thMrecDel);
	map->plist = NULL;
}

if (map->nmpar != NULL) {
	thFree(map->nmpar);
	map->nmpar = NULL;
}

if (map->objcs != NULL) thFree(map->objcs);
if (map->uobjcs != NULL) thFree(map->uobjcs);
if (map->upobjcs != NULL) thFree(map->upobjcs);
if (map->uaobjcs != NULL) thFree(map->uaobjcs);

map->cflag = ALTERED;

thFree(map);
return(SH_SUCCESS);
}

RET_CODE thMapmachineCopy(MAPMACHINE *s, MAPMACHINE *t) {
char *name = "thMapmachineCopy";
if (s == NULL || t == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (s == t) {
	thError("%s: ERROR - cannot copt a (mapmachine) onto itself)", name);
	return(SH_GENERIC_ERROR);
}
if (s->mapid != NULL) {
	if (t->mapid == NULL) {
		t->mapid = thCalloc(MX_STRING_LEN, sizeof(char));
	}
	strcpy(t->mapid, s->mapid);
}
if (s->mapsource != NULL) {
	if (t->mapsource != NULL) {
		t->mapsource = thCalloc(MX_STRING_LEN, sizeof(char));
	}
	strcpy(t->mapsource, s->mapsource);
}
if (s->maptarget != NULL) {
	if (t->maptarget != NULL) {
		t->maptarget = thCalloc(MX_STRING_LEN, sizeof(char));
	}
	strcpy(t->maptarget, s->maptarget);
}
CHAIN *sas, *sps, *tas, *tps;
sas = s->alist;
sps = s->plist;
tas = t->alist;
tps = t->plist;
if (sas == NULL || sps == NULL) {
	thError("%s: ERROR - improperly allocated (source)", name);
	return(SH_GENERIC_ERROR);
}
if (tas == NULL || tps == NULL) {
	thError("%s: ERROR - improperly allocated (target)", name);
	return(SH_GENERIC_ERROR);
}	
if (shChainSize(tas) != 0 || shChainSize(tps) != 0) {
	thError("%s: ERROR - the (target) map is already used", name);
	return(SH_GENERIC_ERROR);
}
int i, na, np;
na = shChainSize(sas);
np = shChainSize(sps);
for (i = 0; i < na; i++) {
	MREC *srec = shChainElementGetByPos(sas, i);
	MREC *trec = thMrecDup(srec);
	if (trec == NULL) {
		thError("%s: ERROR - could not copy (mrec-alist) from (source) to (target) at location (i = %d)", name, i);
		return(SH_GENERIC_ERROR);
	}
	shChainElementAddByPos(tas, trec, "MREC", TAIL, AFTER);
}
for (i = 0; i < np; i++) {
	MREC *srec = shChainElementGetByPos(sps, i);
	MREC *trec = thMrecDup(srec);
	if (trec == NULL) {
		thError("%s: ERROR - could not copy (mrec-plist) from (source) to (target) at location (i = %d)", name, i);
		return(SH_GENERIC_ERROR);
	}
	shChainElementAddByPos(tps, trec, "MREC", TAIL, AFTER);
}

t->cflag = ALTERED;
if (s->cflag == COMPILED) {
	thError("%s: WARNING - (target) should be compiled before use", name);
}

t->parent = s;
return(SH_SUCCESS);
}


/* the following is developed based on the need addressed by the L-package (Mle) */

RET_CODE thMapmachineGetNpar(MAPMACHINE *map, int *npar) {
char *name = "thMapmachineGetNpar";
if (map == NULL) {
	thError("%s: ERROR - improperly allocated (mapmachine)", name);
	return(SH_GENERIC_ERROR);
	}

if (npar == NULL) {
	thError("%s: WARNING - null placeholder for (npar), cannot pass value", name);
	return(SH_SUCCESS);
	}

*npar = map->npar;
return(SH_SUCCESS);
}


RET_CODE thMapmachineGetNamp(MAPMACHINE *map, int *namp) {
char *name = "thMapmachineGetNpar";
if (map == NULL) {
	thError("%s: ERROR - improperly allocated (mapmachine)", name);
	return(SH_GENERIC_ERROR);
	}

if (namp == NULL) {
	thError("%s: WARNING - null placeholder for (namp), cannot pass value", name);
	return(SH_SUCCESS);
	}

*namp = map->namp;
return(SH_SUCCESS);
}

RET_CODE thMapmachinePutNmodel(MAPMACHINE *map, int nmodel) {
char *name = "thMapmachinePutNmodel";

if (map == NULL) {
	thError("%s: ERROR - improperly allocated (mapmachine)", name);
	return(SH_GENERIC_ERROR);
} 

map->namp = nmodel;
return(SH_SUCCESS);
}

RET_CODE thMapmachinePutNpar(MAPMACHINE *map, int npar) {
char *name = "thMapmachinePutNpar";
if (map == NULL) {
	thError("%s: ERROR - improperly allocated (mapmachine)", name);
	return(SH_GENERIC_ERROR);
}
map->npar = npar;
return(SH_SUCCESS);
}

RET_CODE thMapmachinePutNobjc(MAPMACHINE *map, int nobjc) {
char *name = "thMapmachinePutNobjc";
if (map == NULL) {
	thError("%s: ERROR - improperly allocated (mapmachine)", name);
	return(SH_GENERIC_ERROR);
}
map->nobjc = nobjc;
return(SH_SUCCESS);
}


RET_CODE thMapmachineGetModelPars(MAPMACHINE *map, int i, int *nmpar, int **ploc) {
/* this function returns the number of parameters for model i as well as the mapping between fit parameter list and the record list in th input parameter for the model */

char *name = "thMapmachineGetModelPars";
if (map == NULL || map->nmpar == NULL || map->ploc == NULL) {
	thError("%s: ERROR - improperly allocated (mapmachine)", name);
	return(SH_GENERIC_ERROR);
}

if (i < 0 || i >= map->namp) {
	thError("%s: ERROR - model number (%d) out of bound (0, %d)", name, i, map->namp - 1);
	return(SH_GENERIC_ERROR);
}

if (nmpar == NULL && ploc == NULL) {
	thError("%s: WARNING - null (ploc) and (nmpar) place holders, cannot pass info", name);
	return(SH_SUCCESS);
}
if (nmpar == NULL) {
	thError("%s: WARNING - returning (ploc) array without its size (nmpar) - can cause memory failure", name);
	}
if (nmpar != NULL) {
	*nmpar = map->nmpar[i];
	if (*nmpar < 0) {
		thError("%s: WARNING - negative number of parameters discovered for model %d (nmpar = %d)", name, i, *nmpar);
	}
}
if (ploc != NULL) *ploc = map->ploc[i];
return(SH_SUCCESS);
}

RET_CODE thMapmachineGetMname(MAPMACHINE *map, int i, char **mname) {
char *name = "thMapmachineGetMname";

if (map == NULL || map->mnames == NULL) {
	thError("%s: ERROR - improperly allocated (mapmachine)", name);
	return(SH_GENERIC_ERROR);
}

if (i < 0 || i >= map->namp) {
	thError("%s: ERROR - model number (%d) out of bound (0, %d)", name, i, map->namp - 1);
	return(SH_GENERIC_ERROR);
}

if (mname != NULL) {
	*mname = map->mnames[i];
} else {
	thError("%s: WARNING - null place holder for (mname), cannot pass value", name);
}

return(SH_SUCCESS);
}

RET_CODE thMapmachineGetPname(MAPMACHINE *map, int i, char **pname) {
char *name = "thMapmachineGetPname";
if (map == NULL || map->pnames == NULL) {
	thError("%s: ERROR - improperly allocated (mapmachine)", name);
	return(SH_GENERIC_ERROR);
}

if (i < 0 || i >= map->namp) {
	thError("%s: ERROR - model number (%d) out of bound (0, %d)", name, i, map->namp - 1);
	return(SH_GENERIC_ERROR);
}

if (pname != NULL) {
	*pname = map->pnames[i];
} else {
	thError("%s: WARNING = null placeholder for (pname), cannot pass value", name);
}

return(SH_SUCCESS);
}


RET_CODE thMapmachinePutPs(MAPMACHINE *map, void **ps) {
char *name = "thMapmachinePutPs";
if (map == NULL && ps != NULL) {
	thError("%s: ERROR - improperly allocated (mapmachine)", name);
	return(SH_GENERIC_ERROR);
}
if (map == NULL) {
	thError("%s: WARNING - fully null input - performing null", name);
	return(SH_SUCCESS);
}
map->ps = ps;
return(SH_SUCCESS);
}

RET_CODE thMapmachineGetPs(MAPMACHINE *map, void ***ps) {
char *name = "thMapmachineGetPs";
if (ps == NULL) {
	thError("%s: WARNING - null placeholder for (ps), returning null", name);
	return(SH_SUCCESS);
}
if (map == NULL) {
	thError("%s: ERROR - improperly allocated (mapmachine)", name);
	return(SH_GENERIC_ERROR);
}
*ps = map->ps;
return(SH_SUCCESS);
}

RET_CODE thMapmachineGetObjcs(MAPMACHINE *map, void ***objcs, int *nm) {
char *name = "thMapmachineGetObjcs";
if (objcs == NULL) {
	thError("%s: WARNING - null placeholder for (objcs)", name);
	if (nm != NULL) *nm = map->namp;
	return(SH_GENERIC_ERROR);
	}
/* when calling this function, the returned value is the list of objects. 
   are there one object per model that is stored or repetative objects
   are deleted from the list 
   Feb 7, 2012
*/
*objcs = map->objcs;
if (nm != NULL) *nm = map->namp;
return(SH_SUCCESS);
}

RET_CODE thMapmachineGetUObjcs(MAPMACHINE *map, void ***uobjcs, int *nobjc) {
char *name = "thMapmachineGetUObjcs";
if (uobjcs == NULL) {
	thError("%s: WARNING - null placeholder for (uobjcs)", name);
	if (nobjc != NULL) *nobjc = map->nobjc;
	return(SH_GENERIC_ERROR);
}
/* this function returns the unique list of objects, meaning that if the object has several models
included in the map machine it is returned only once in the array uobjcs */
*uobjcs = map->uobjcs;
if (nobjc != NULL) *nobjc = map->nobjc;
return(SH_SUCCESS);
}

RET_CODE thMapmachineGetUPObjcs(MAPMACHINE *map, void ***upobjcs, int *npobjc) {
char *name = "thMapmachineGetUPObjcs";
if (upobjcs == NULL) {
	thError("%s: WARNING - null placeholder for (upobjcs)", name);
	if (npobjc != NULL) *npobjc = map->npobjc;
	return(SH_GENERIC_ERROR);
}
/* this function returns the unique list of objects, meaning that if the object has several models
included in the map machine it is returned only once in the array uobjcs */
*upobjcs = map->upobjcs;
if (npobjc != NULL) *npobjc = map->npobjc;
return(SH_SUCCESS);
}

RET_CODE thMapmachineGetUAObjcs(MAPMACHINE *map, void ***uaobjcs, int *naobjc) {
char *name = "thMapmachineGetUAObjcs";
if (uaobjcs == NULL) {
	thError("%s: WARNING - null placeholder for (uaobjcs)", name);
	if (naobjc != NULL) *naobjc = map->naobjc;
	return(SH_GENERIC_ERROR);
}
/* this function returns the unique list of objects, meaning that if the object has several models
included in the map machine it is returned only once in the array uobjcs */
*uaobjcs = map->uaobjcs;
if (naobjc != NULL) *naobjc = map->naobjc;
return(SH_SUCCESS);
}



RET_CODE thMapmachinePutPt(MAPMACHINE *map, THPIX *pt) {
char *name = "thMapmachinePutPt";
if (map == NULL && pt != NULL) {
	thError("%s: ERROR - improperly allocated (mapmachine)", name);
	return(SH_GENERIC_ERROR);
	}
if (map == NULL) {
	thError("%s: WARNING - fully null input - performing null", name);
	return(SH_SUCCESS);
}
map->pt = pt;
return(SH_SUCCESS);
}

RET_CODE thMapmachineGetPt(MAPMACHINE *map, THPIX **pt) {
char *name = "thMapmachineGetPt";
if (pt == NULL) {
	thError("%s: WARNING - null placeholder for (pt), returning null", name);
	return(SH_SUCCESS);
}
if (map == NULL) {
	thError("%s: ERROR - improperly allocated (mapmachine)", name);
	return(SH_GENERIC_ERROR);
}
*pt = map->pt;
return(SH_SUCCESS);
}

RET_CODE thMapmachineGetCflag(MAPMACHINE *map, MCFLAG *cflag) {
char *name = "thMapmachineGetCflag";
if (cflag == NULL) {
	thError("%s: WARNING - null place holder for (cflag), returning null", name);
	return(SH_SUCCESS);
}
if (map == NULL) {
	thError("%s: ERROR - improperly allocated (mapmachine)", name);
	return(SH_GENERIC_ERROR);
}
*cflag = map->cflag;
return(SH_SUCCESS);
}
RET_CODE thMAccnmparGet(MAPMACHINE *map, int i, int j, int *a) {
#if 0
char *name = "thMAccnmparGet";
if (map == NULL) {
	thError("%s: ERROR - null map");
	return(SH_GENERIC_ERROR);
}
if (map->cflag != COMPILED) {
	thError("%s: ERROR - cannot process uncompiled map", name);
	return(SH_GENERIC_ERROR);
}
if (a == NULL) {
	thError("%s: WARNING - null place holder for combindex index", name);
	return(SH_SUCCESS);
}
int nc;
int nm, np;
nm = map->namp;
np = map->npar;
if ((nc = map->nacc) == 0) *a = BAD_INDEX;
int **accnmpar;
if ((accnmpar = map->accnmpar) == NULL) {
	thError("%s: ERROR - null (accnmpar) in (mapmachine)", name);
	return(SH_GENERIC_ERROR);
}
int b = BAD_INDEX;
if (i >= 0 && i < nm && j >= 0 && j <= np) b = accnmpar[i][j];
*a = b;
#endif
*a = map->accnmpar[i][j];
return(SH_SUCCESS);
}

RET_CODE thDDMAccnmparGet(MAPMACHINE *map, int i, int j, int k, int *a) {
shAssert(map != NULL);
shAssert(a != NULL);
shAssert(map->DDaccnmpar != NULL);

if (map->DDaccnmpar[i] != NULL) {
	*a = map->DDaccnmpar[i][j][k];
} else {
	*a = BAD_INDEX;
}

return(SH_SUCCESS);
}

RET_CODE thMapmachinePutCflag(MAPMACHINE *map, MCFLAG cflag) {
char *name = "thMapmachinePutCflag";
if (map == NULL) {
	thError("%s: ERROR - improperly allocated (mapmachine)", name);
	return(SH_GENERIC_ERROR);
}
map->cflag = cflag;
return(SH_SUCCESS);
}

MREC *thMrecNew(THOBJC *objc, char *mname, char *rname, int band) {
char *name = "thMrecNew";
MREC *mrec;

if (objc == NULL) {
	thError("%s: ERROR - improperly allocated (thobjc)", name);
	return(NULL);
}

/* if no model name is provided, then go check if there is only one model in the (thprop) */

if (mname == NULL || strlen(mname) == 0) {
	if (shChainSize(objc->thprop) != 1) {
	thError("%s: ERROR - no component name provided (mname), object has to have only 1 component (objc %d has %d instread)", 
		name, objc->thid, shChainSize(objc->thprop));
		return(NULL);
	} 
	char *pname;
	void *ps;
	thPropGet(shChainElementGetByPos(objc->thprop, 0), &mname, &pname, &ps);
	mrec = thCalloc(1, sizeof(MREC));
	mrec->mname = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
	strcpy(mrec->mname, mname);
	mrec->pname = pname;
	mrec->ps = ps;
	mrec->objc = objc;
	mrec->band = band;
	if (rname != NULL && strlen(rname) != 0) {
		mrec->rname = thCalloc(MX_STRING_LEN, sizeof(char));
	strcpy(mrec->rname, rname);
	}
	return(mrec);
}

RET_CODE status;
char *pname;
void *ps;
status = thPropChainGetByMname(objc->thprop, mname, &pname, &ps);
if (ps  == NULL || status != SH_SUCCESS) {
	thError("%s: ERROR - problem loading component (%s) from objc (%d)", name, mname, objc->thid);
	return(NULL);
}

mrec = thCalloc(1, sizeof(MREC));
mrec->objcid = objc->thid;
mrec->objctype = objc->thobjctype;
mrec->mname = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
strcpy(mrec->mname, mname);
mrec->pname = pname;
mrec->objc = objc;

if (rname == NULL || strlen(rname) == 0) {
	mrec->ps = ps;
	if ((mrec->ptype = shTypeGetFromName(pname)) == UNKNOWN_SCHEMA) thError("%s: WARNING - schema not defined for parameter of type (%s) - will have problem later on extracting records - (objc: %d, model: %s)", name, pname, mrec->objcid, mname);
} else {

	#if MAP_HAS_SCHEMA_INFO

	TYPE ptype;
	if ((ptype = shTypeGetFromName(pname)) == UNKNOWN_SCHEMA) {
		thError("%s: ERROR - schema not defined for patameter (%s), could not extract record (%s) - (objc: %d, model: %s)",
		name, pname, rname, mrec->objcid, mname);
	thFree(mrec->mname);
	thMrecDel(mrec);
	return(NULL);
	}

	SCHEMA_ELEM *se;
	se = shSchemaElemGetFromType(ptype, rname);
	if (se == NULL) {
		thError("%s: ERROR - '%s' doesn't have a record '%s'", 
		name, pname, rname);
		thFree(mrec->mname);
		thFree(mrec);
		return(NULL);
	}
	mrec->rname = se->name;
	mrec->ps = shElemGet(ps, se, NULL);
	mrec->ptype = shTypeGetFromName(se->type);
	mrec->etype = se->type;
	#else
	mrec->rname = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
	strcpy(mrec->rname, rname);
	mrec->ps = NULL;
	#endif

}	
	
return(mrec);
}

void thMrecDel(MREC *mrec) {
if (mrec == NULL) return;

if (mrec->mname != NULL) thFree(mrec->mname);
thFree(mrec);
return;
}

MREC *thMrecDup(MREC *s) {
char *name = "thMrecDup";
if (s == NULL) {
	thError("%s: WARNING - null (mrec) to duplicate", name);
	return(NULL);
}
	
MREC *t = thMrecNew(s->objc, s->mname, s->rname, s->band);
if (t == NULL) {
	thError("%s: ERROR - could not make a duplicate (mrec)", name);
	return(NULL);
}
return(t);
}



RET_CODE thMrecGetObjc(MREC *mrec, THOBJC **objc) {
char *name = "thMrecGetObjc";

if (objc == NULL) {
	thError("%s: WARNING - null place holders, returning null", name);
	return(SH_SUCCESS);
}

if (mrec == NULL) {
	thError("%s: ERROR - null (mrec)", name);
	return(SH_GENERIC_ERROR);
}

if (objc != NULL) *objc = mrec->objc;

return(SH_SUCCESS);
}

#if MAP_HAS_SCHEMA_INFO

RET_CODE thMrecGetPtypePs(MREC *mrec, TYPE *ptype, char **etype, void **ps) {
char *name = "thMrecGetPtypePs";

if (ps == NULL && ptype == NULL && etype == NULL) {
	thError("%s: WARNING - null place holders, returning null", name);
	return(SH_SUCCESS);
}

if (mrec == NULL) {
	thError("%s: ERROR - improperly allocated (mrec)", name);
	return(SH_GENERIC_ERROR);
}

if (ps != NULL) *ps = mrec->ps;
if (ptype != NULL) *ptype = mrec->ptype;
if  (etype != NULL) *etype = mrec->etype;

return(SH_SUCCESS);
}

#else

RET_CODE thMrecGetPtypePs(MREC *mrec, TYPE *ptype, void **ps) {
char *name = "thMrecGetPtypePs";

if (ps == NULL && ptype == NULL) {
	thError("%s: WARNING - null place holders, returning null", name);
	return(SH_SUCCESS);
}

if (mrec == NULL) {
	thError("%s: ERROR - improperly allocated (mrec)", name);
	return(SH_GENERIC_ERROR);
}

if (ps != NULL) *ps = mrec->ps;
if (ptype != NULL) *ptype = mrec->ptype;

return(SH_SUCCESS);
}

#endif

RET_CODE thMrecGetPname(MREC *mrec, char **pname) {
char *name = "thMrecGetPname";

if (pname == NULL) {
	thError("%s: WARNING - null place holder, returning null", name);
	return(SH_SUCCESS);
}

if (mrec == NULL) {
	thError("%s: ERROR - improperly allocated (mrec)", name);
	return(SH_GENERIC_ERROR);
}

*pname = mrec->pname;
return(SH_SUCCESS);
}


RET_CODE thMGetInModelMLocOfP(MAPMACHINE *map, int i, int j, int *k) {
char *name = "thMGetInModelMLocOfP";
if (map->cflag != COMPILED) {
	thError("%s: ERROR - can only operate on compiled (mapmachine)", name);
	return(SH_GENERIC_ERROR);
}

int nm, np;
nm = map->namp;
np = map->npar;
if (i <0 || i >= nm) {
	thError("%s: ERROR - only (%d) models are included in (mapmachine), asking for model (%d) falls outside this list", name, nm, i);
	return(SH_GENERIC_ERROR);
}
if (j < -1 || j >= np) {
	thError("%s: ERROR - only (%d) parameters are included in (mapmachine), asking for parameter (%d) falls outside this list", name, np, j);
	return(SH_GENERIC_ERROR);
}

if (k != NULL) {
	*k = map->accnmpar[i][j + 1];
}
return(SH_SUCCESS);
}

RET_CODE thMGetInModelMLocOfPP(MAPMACHINE *map, int imodel, int jpar1, int jpar2, int *k) {
char *name = "thMGetInModelMLocOfPP";
if (map->cflag != COMPILED) {
	thError("%s: ERROR - can only operate on compiled (mapmachine)", name);
	return(SH_GENERIC_ERROR);
}

int nm, np;
nm = map->namp;
np = map->npar;
if (imodel <0 || imodel >= nm) {
	thError("%s: ERROR - only (%d) models are included in (mapmachine), asking for model (%d) falls outside this list", name, nm, imodel);
	if (k != NULL) *k = BAD_INDEX;
	return(SH_GENERIC_ERROR);
}
if (jpar1 < 0 || jpar1 >= np || jpar2 < 0 || jpar2 >= np) {
	thError("%s: ERROR - only (%d) parameters are included in (mapmachine), asking for parameters (%d, %d) falls outside this list", name, np, jpar1, jpar2);
	if (k != NULL) *k = BAD_INDEX;
	return(SH_GENERIC_ERROR);
}

#if 0
RET_CODE status;
int kpar1 = BAD_INDEX, kpar2 = BAD_INDEX;
status = thMGetInModelMLocOfP(map, imodel, jpar1, &kpar1);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not ploc of (imodel = %d, jpar = %d)", name, imodel, jpar1);
	if (k != NULL) *k = BAD_INDEX;
	return(status);
}

status = thMGetInModelMLocOfP(map, imodel, jpar2, &kpar2);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not ploc of (imodel = %d, jpar = %d)", name, imodel, jpar2);
	if (k != NULL) *k = BAD_INDEX;
	return(status);
}

if (map->DDaccnmpar == NULL) {
	thError("%s: ERROR - null 'DDaccnmpar' matrix - wrongfully compiled map", name);
	if (k != NULL) *k = BAD_INDEX;
	return(SH_GENERIC_ERROR);
}

if (kpar1 == BAD_INDEX || kpar2 == BAD_INDEX) {
	if (k == NULL) *k = BAD_INDEX;
	return(SH_SUCCESS);
}
#else
int kpar1 = jpar1;
int kpar2 = jpar2;
#endif

if (kpar1 >= map->nmpar[imodel] || kpar2 >= map->nmpar[imodel]) {
	thError("%s: ERROR - found (kpar1 = %d, kpar2 = %d) while (nmpar[%d] = %d)", 
	name, kpar1, kpar2, imodel, map->nmpar[imodel]);
	return(SH_GENERIC_ERROR);
}

if (k != NULL) {
	if (map->DDaccnmpar[imodel] == NULL) {
		*k = BAD_INDEX;
	} else {
		*k = map->DDaccnmpar[imodel][kpar1][kpar2];
	}
}
return(SH_SUCCESS);
}


RET_CODE thMapGetObjcArr(MAPMACHINE *map, MOBJC_SECTOR sector, THOBJC ***objcs, int *nobjc) {
char *name = "thMapGetObjcArr";
if (objcs == NULL && nobjc == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (map == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (sector >= N_MOBJC_SECTOR) {
	thError("%s: ERROR - unsupported (mobjc_sector = %d)", name, sector);
	return(SH_GENERIC_ERROR);
}
if (objcs == NULL || nobjc == NULL) {
	thError("%s: WARNING - some necessary output placeholders are null", name);
}
#if DEBUG_MAPTYPES
char *sname = shEnumNameGetFromValue("MOBJC_SECTOR", sector);
printf("%s: sector = '%s', %d \n", name, sname, sector);
#endif
THOBJC **myobjcs = NULL;
int myn = 0;
switch (sector) {
	case ALL_MOBJCS:
		myobjcs = (THOBJC **) map->uobjcs;
		myn = map->nobjc;	
		break;
	case NONLINEAR_MOBJCS:
		myobjcs =(THOBJC **)  map->upobjcs;
		myn = map->npobjc;
		break;
	case LINEAR_MOBJCS:
		myobjcs = (THOBJC **) map->uaobjcs;
		myn = map->naobjc;
		break;
	case ALL_MSKY:
		{
		THOBJCTYPE SKY_OBJC = UNKNOWN_OBJC; 
		MSkyGetType(&SKY_OBJC);
		if (SKY_OBJC == UNKNOWN_OBJC) {
			thError("%s: ERROR - (SKY_OBJC) is unknown", name);		
			return(SH_GENERIC_ERROR);
		}
		#if DEBUG_MAPTYPES
		printf("%s: SKY_OBJC = %ld \n", name, (long int) SKY_OBJC);
		#endif
		int i, n;
		n = map->nobjc;
		if (n < 0) {
			thError("%s: ERROR - improperly allocated (map, nobjc = %d)", name, n);
			return(SH_GENERIC_ERROR);
		} else if (n == 0) {
			thError("%s: WARNING - no object was found in (map, nobjc = %d)", name, n);
		}
		THOBJC **allobjcs = (THOBJC **) map->uobjcs;
		#if DEBUG_MAPTYPES
		printf("%s: all-objcs-arr = %p, n = %d \n", name, allobjcs, n);
		#endif
		myobjcs = NULL;
		myn = 0;
		int nsky = 0;
		for (i = 0; i < n; i++) {
			THOBJC *objc = allobjcs[i];
			THOBJCTYPE type = UNKNOWN_OBJC;
			RET_CODE status = thObjcGetType(objc, &type);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get (objc) type at (i = %d) of (uobjc) list", name, i);
				if (objcs != NULL) *objcs = NULL;
				if (nobjc != NULL) *nobjc = -1;
				return(status);
			}
			if (type == SKY_OBJC) {
				#if DEBUG_MAPTYPES
				printf("%s: objc at (i = %d) is of type (sky) \n", name, i);
				#endif
				nsky++;	
				myobjcs = (THOBJC **) ((void **) allobjcs + i);
				myn = 1;
			}
		}
		if (nsky > 1) {
			thError("%s: WARNING - (nsky = %d) occurences of (sky) was found in object list - returning the last one", name, nsky);
		}	
		}
		break;
	default:	
		{
		char *sname = shEnumNameGetFromValue("MOBJC_SECTOR", sector);
		thError("%s: ERROR - does not support (sector = '%s')", name, sname);
		}
		if (objcs != NULL) *objcs = NULL;
		if (nobjc != NULL) *nobjc = -1;
		return(SH_GENERIC_ERROR);
		break;
	}

#if DEBUG_MAPTYPES
printf("%s: sector = '%s', objc-array = %p, nobjc = %d \n", name, sname, myobjcs, myn);
#endif
if (objcs != NULL) *objcs = myobjcs;
if (nobjc != NULL) *nobjc = myn;

return(SH_SUCCESS);
}

RET_CODE thMapGetLpars(MAPMACHINE *map, int *nm, int *np, int *npp, int *nc, int *nacc, int *nj) {
char *name = "thMapmachineGetLpars";
if (nm == NULL && np == NULL && nc == NULL && nacc == NULL && nj == NULL) {
	thError("%s: ERROR - null output placeholders", name);
	return(SH_SUCCESS);
}
if (map == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

if (map->cflag != COMPILED) {
	thError("%s: ERROR - (map) is not compiled, cannot obtain structural information", name);
	return(SH_GENERIC_ERROR);
}

if (nm != NULL) *nm = map->namp;
if (np != NULL) *np = map->npar;
if (npp != NULL) *npp = map->npp;
if (nc != NULL)  {
	int i, mc = 0;
	if (map->nmpar != NULL) {
		for (i = 0; i < map->namp; i++) {
			mc += map->nmpar[i];
		}
	}
	mc += map->namp;
	*nc = mc;
}
if (nacc != NULL) *nacc = map->nacc;
if (nj != NULL) *nj = map->npar;

return(SH_SUCCESS);	
}

RET_CODE thMapGetObjcRnamesPndex(MAPMACHINE *map, void *objc, char ***rnames, int *nrnames,int **pndex, int *npndex) {
char *name = "thMapGetObjcRnamesPndex";
shAssert(map != NULL);
shAssert(objc != NULL);
int npobjc = 0, i = 0, objc_index = -1;
npobjc = map->npobjc;
for (i = 0; i < npobjc; i++) {
	if (objc == (void *) map->upobjcs[i]) objc_index = i;
}
if (objc_index < 0) {
	thError("%s: ERROR - could not find (objc: %p) in the list available in map (npobjc = %d)", name, objc, npobjc);
	return(SH_GENERIC_ERROR);
}
if (rnames == NULL && nrnames == NULL && pndex == NULL && npndex == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}

if (rnames != NULL) *rnames = map->robjcnames[objc_index];
if (nrnames != NULL) *nrnames = map->nobjcpar[objc_index];
if (pndex != NULL) *pndex = map->pobjcloc[objc_index];
if (npndex != NULL) *npndex = map->nobjcpar[objc_index];

return(SH_SUCCESS);
}

RET_CODE thMapmachineModelIndexGetFromThprop(MAPMACHINE *map, THPROP *prop, int *imodel) {
char *name = "thMapmachineModelIndexFromThprop";
shAssert(map != NULL);
shAssert(prop != NULL);
shAssert(imodel != NULL);
RET_CODE status;
char *mname = NULL, *pname = NULL;
void *fparam = NULL;
status = thPropGet(prop, &mname, &pname, &fparam);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get model specifics from (thprop = %p)", name, (void *) prop);
	return(status);
}
int nmodel = map->namp;
int i;
char **mnames = map->mnames;
char **pnames = map->pnames;
void **fparams = map->ps;

int nfound = 0, found_index = BAD_INDEX;
for (i = 0; i < nmodel; i++) {
	char *mname_i = mnames[i];
	char *pname_i = pnames[i];
	void *fparam_i = fparams[i];
	if (fparam_i == fparam) {
		if (nfound == 0) found_index = i;
		nfound++;
		if (strcmp(mname_i, mname)) {
			thError("%s: WARNING - model name mistatch at (imodel = %d)", name, i); 
		}
		if (strcmp(pname_i, pname)) {
			thError("%s: WARNING - model parameter type mismatch (imodel = %d)", name, i);
		}
	}
}
if (nfound > 1) {
	thError("%s: WARNING - more than one match (%d) found to (prop = %p)", name, nfound, (void *) prop);
	}

*imodel = found_index;
return(SH_SUCCESS);
}

RET_CODE thMapmachineGetModelInfo(MAPMACHINE *map, int i, char **mname, void **p, void **objc) {
char *name = "thMapmachineGetModelInfo";
if (map == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (i < 0) {
	thError("%s: ERROR - unacceptable value for (imodel = %d)", name, i);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
int nmodel = -1;
status = thMapmachineGetNamp(map, &nmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (nmodel) from (map)", name);
	return(status);
}
if (i >= nmodel) {
	thError("%s: ERROR - uncceptable value for (imodel = %d) while (nmodel = %d)", name, i, nmodel);
	return(SH_GENERIC_ERROR);
}
if (mname == NULL && objc == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (mname != NULL) {
	if (map->mnames == NULL) {
		thError("%s: ERROR - null (mnames) array found in map", name);
		return(SH_GENERIC_ERROR);
	}
	*mname = map->mnames[i];
}
if (objc != NULL) {
	if (map->objcs == NULL) {
		thError("%s: ERROR - null (objcs) array found in map", name);
		return(SH_GENERIC_ERROR);
	}
	*objc = map->objcs[i];
}
if (p != NULL) {
	if (map->ps == NULL) {
		thError("%s: ERROR - null (ps) array in map", name);
		return(SH_GENERIC_ERROR);
	}
	*p = map->ps[i];
}
return(SH_SUCCESS);
}

MFIT_RNAME_LOC *thMfitRnameLocNew(){
MFIT_RNAME_LOC *x = thCalloc(1, sizeof(MFIT_RNAME_LOC));
return(x);
}

void thMfitRnameLocDel(MFIT_RNAME_LOC *x) {
if (x == NULL) return;
thFree(x);
return;
}


