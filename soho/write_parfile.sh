function write_parfile {
local name="write_parfile"

local Options=$@
local Optnum=$#

local ldatadir=no
local ldatadir2=no
local lfield=no
local lfield0=no
local lpsffield=no
local lrun=no
local lcamcol=no
local lband=no
local lmleparfile=no
local loutdir=no
local lobjcdir=no
local lcomment=no
local copypsf=no
local simulator=no
local sdsssimulator=no
local multibandmle=no
local linobjcdir=no
local linobjcdir2=no
local linskydir=no
local lsimdir=no
local lframesdir=no
local ltempdir=no

local datadir=empty
local field=empty
local field0=empty
local psffield=empty
local run=empty
local camcol=empty
local band=empty
local mleparfile=empty
local outdir=empty
local comment=empty
local inobjcdir=empty
local inobjcdir2=empty
local inskydir=empty
local simdir=empty
local framesdir=empty
local tempdir=empty

while getopts ':-:' OPTION ; do
  case "$OPTION" in
    -  ) [ $OPTIND -ge 1 ] && optind=$(expr $OPTIND - 1 ) || optind=$OPTIND
         eval OPTION="\$$optind"
         OPTARG=$(echo $OPTION | cut -d'=' -f2)
         OPTION=$(echo $OPTION | cut -d'=' -f1)
         case $OPTION in
             --datadir   ) ldatadir=yes; datadir="$OPTARG"     ;;
	     --datadir2  ) ldatadir2=yes; datadir2="$OPTARG"   ;;
	     --framesdir ) lframesdir=yes; framesdir="$OPTARG" ;;
	     --tempdir   ) ltempdir=yes; tempdir="$OPTARG" ;;
	     --field	 ) lfield=yes; field="$OPTARG"   ;;
	     --field0    ) lfield0=yes; field0="$OPTARG" ;;
	     --psffield  ) lpsffield=yes; psffield="$OPTARG"     ;;
	     --run       ) lrun=yes; run="$OPTARG"	 ;;
	     --camcol    ) lcamcol=yes; camcol="$OPTARG" ;;
	     --band      ) lband=yes; band="$OPTARG"	 ;;
	     --output    ) lmleparfile=yes; mleparfile="$OPTARG" 	;;
	     --outdir    ) loutdir=yes; outdir="$OPTARG"	;;
	     --objcdir   ) lobjcdir=yes; objcdir="$OPTARG"	;; 
	     --copypsf   ) copypsf=yes				;;
	     --comment   ) lcomment=yes; comment="$OPTARG"	;;
	     --simulator ) simulator=yes				;;
	     --sdsssimulator ) sdsssimulator=yes		;;
	     --multibandmle  ) multibandmle=yes			;;
	     --inobjcdir ) linobjcdir=yes; inobjcdir="$OPTARG"	;; #for sdss based simulation
	     --inobjcdir2 ) linobjcdir2=yes; inobjcdir2="$OPTARG" ;; #for sdss based simulation
	     --inskydir  ) linskydir=yes; inskydir="$OPTARG"	;; #for sdss based simulation
		--simdir    ) lsimdir=yes; simdir="$OPTARG"	;; #for outputinng simulation parameter
	     * )  echo "$name: invalide options (long)" ;;
         esac
       OPTIND=1
       shift
      ;;
    ? )  echo "$name: invalid options (short) "  ;;
  esac
done

#echo "$name: root = $root, field = $field, field0 = $field0, run = $run, camcol = $camcol, band = $band, output = $mleparfile"

if [ $ldatadir == no ] || [ $lfield == no ] || [ $lrun == no ] || [ $lcamcol == no ] || [ $lband == no ] || [ $lmleparfile == no ] || [ $lpsffield == no ]; then
	echo "$name: not enough arguments set"
	exit
fi
if [ $field == empty ] || [ $run == empty ] || [ $camcol == empty ] || [ $band == empty ] || [ $mleparfile == empty ] || [ $psffield == empty ]; then
	echo "$name: some arguments are invoked but not set"
	exit
fi
if [ $lfield0 == no ]; then
	lfield0=yes
	field0=$field
fi
if [ $lobjcdir == no ]; then
	objcdir="$outdir/objc"
fi
if [ $linobjcdir == no ]; then 
	inobjcdir="$datadir"
fi
if [ $linskydir == no ]; then
	inskydir="$datadir"
fi
if [ $lsimdir == no ]; then
	simdir="$outdir/$camcol/siminput"
fi
if [ $ldatadir2 == no ]; then 
	datadir2="$CPHOTO_OBJCS/$DEFAULT_SDSS_RERUN/$run/$camcol"
fi
if [ $lframesdir == no ]; then 
	framesdir="$CPHOTO_FRAMES/$DEFAULT_SDSS_RERUN/$run/$camcol"
fi
if [ $linobjcdir2 == no ]; then
	inobjcdir2="$datadir2"
fi

local fpCprefix="fpC"
local fpCCprefix="fpCC"
local frames_prefix="frame"
local psFieldprefix="psField"
local fpObjcprefix="fpObjc"
local photoObjcprefix="photoObj"
local gcprefix="gC"
local fpBINprefix="fpBIN"
local gpObjcprefix="gpObjc"
local lMprefix="lM"
local lPprefix="lP"
local lRprefix="lR"
local lZprefix="lZ"
local lWprefix="lW"
local psfRprefix="psfR"
local psfSprefix="psfS"


run=$(( 10#$run ))
psffield=$(( 10#$psffield ))
field=$(( 10#$field ))

runstr=`printf "%06d" $run`
psffieldstr=`printf "%04d" $psffield`
fieldstr=`printf "%04d" $field`

#the following lined was changed after FAHL's crash
#psfdir="/u/dss/data/$run/137/objcs/$camcol"
psfdir="$PHOTO_REDUX/$DEFAULT_SDSS_RERUN/$run/objcs/$camcol"
source_datadir="$PHOTO_REDUX/$DEFAULT_SDSS_RERUN/$run/objcs/$camcol"
source_datadir2="$CPHOTO_OBJCS/$DEFAULT_SDSS_RERUN/$run/$camcol"
source_framedir="$CPHOTO_FRAMES/$DEFAULT_SDSS_RERUN/$run/$camcol"
source_psField="$source_datadir/psField-$runstr-$camcol-$psffieldstr.fit"
target_psField="$datadir/psField-$runstr-$camcol-$fieldstr.fit"	

if [ $copypsf == yes ]; then

	if [[ $source_psField != $target_psField && ! -e $target_psField ]]; then 
		if [[  -e $source_psField ]]; then
			#echo "$name: target_psField = $target_psField"
			eval "cp $source_psField $target_psField"
			echo -n "c~"
		else 
			echo -n "x~"
		fi

	fi
	psFieldfile=$target_psField

else

	if [ ! -e $source_psField ]; then
		echo -n "x"
	fi
	psFieldfile=$source_psField

fi

if [[ $simulator == yes ]]; then 
	skyobjc_datadir="$source_datadir"
	skyobjc_datadir2="$datadir2"
else
	skyobjc_datadir="$datadir"
	skyobjc_datadir2="$datadir2"
fi

fpCfile="$datadir/$fpCprefix-$runstr-$band$camcol-$fieldstr.fit"
fpCCfile="$datadir/$fpCCprefix-$runstr-$band$camcol-$fieldstr.fit"
#psFieldfile=$target_psField #done above; differentiate between copy and non-copy
fpObjcfile="$skyobjc_datadir/$fpObjcprefix-$runstr-$camcol-$fieldstr.fit"
photoObjcfile="$inobjcdir2/$photoObjcprefix-$runstr-$camcol-$fieldstr.fits"
gcfile="$datadir/$gcprefix-$runstr-$camcol-$fieldstr.fit"
fpBINfile="$skyobjc_datadir/$fpBINprefix-$runstr-$band$camcol-$fieldstr.fit"
gpObjcfile="$objcdir/$gpObjcprefix-$runstr-$band$camcol-$fieldstr.fit"
lMfile="$objcdir/$lMprefix-$runstr-$band$camcol-$fieldstr.fit"
lPfile="$objcdir/$lPprefix-$runstr-$band$camcol-$fieldstr.fit"
lRfile="$objcdir/$lRprefix-$runstr-$band$camcol-$fieldstr.fit"
lZfile="$objcdir/$lZprefix-$runstr-$band$camcol-$fieldstr.fit"
lWfile="$objcdir/$lWprefix-$runstr-$band$camcol-$fieldstr.fit"
psfRfile="$objcdir/$psfRprefix-$runstr-$band$camcol-$fieldstr.fit"
psfSfile="$objcdir/$psfSprefix-$runstr-$band$camcol-$fieldstr.fit"
if [[ $simulator == yes ]]; then 
	lMfile="$simdir/$lMprefix-$runstr-$band$camcol-$fieldstr.fit"
	lPfile="$simdir/$lPprefix-$runstr-$band$camcol-$fieldstr.fit"
	lRfile="$simdir/$lRprefix-$runstr-$band$camcol-$fieldstr.fit"
	lZfile="$simdir/$lZprefix-$runstr-$band$camcol-$fieldstr.fit"
	lWfile="$simdir/$lWprefix-$runstr-$band$camcol-$fieldstr.fit"
fi


phcalibfile="/u/dss/redux/resolve/full_02apr06/calibs/default2/137/$run/nfcalib/calibPhotomGlobal-$runstr-$camcol.fits"

logsdir="$PHOTO_REDUX/$DEFAULT_SDSS_RERUN/$run/logs"
phconfigpattern="opConfig-*.par"
phecalibpattern="opECalib-*.par"

phconfigfile=$(find "$logsdir" -maxdepth 1 -type f -name "$phconfigpattern")
phecalibfile=$(find "$logsdir" -maxdepth 1 -type f -name "$phecalibpattern")

if [ $loutdir == no ]; then
	outdir=$datadir
fi

if [ $sdsssimulator == yes ]; then 
outphflatframefileformat="$datadir/$fpCprefix-$runstr-%c$camcol-$fieldstr.fit"
inphsmfileformat="$inskydir/$fpBINprefix-$runstr-%c$camcol-$fieldstr.fit"
outphsmfileformat="$simdir/$fpBINprefix-$runstr-%c$camcol-$fieldstr.fit"
inphobjcfile="$inobjcdir/$fpObjcprefix-$runstr-$camcol-$fieldstr.fit"
incalibobjcfile="$inobjcdir2/$photoObjcprefix-$runstr-$camcol-$fieldstr.fits"
outphobjcfile="$simdir/$fpObjcprefix-$runstr-$camcol-$fieldstr.fit"
inphobjcfile="$inobjcdir/$fpObjcprefix-$runstr-$camcol-$fieldstr.fit"
outthsmfileformat="$simdir/$gpBINprefix-$runstr-%c$camcol-$fieldstr.fit"
outthobjcfileformat="$simdir/$gpObjcprefix-$runstr-%c$camcol-$fieldstr.fit"
outthflatframefileformat="$datadir/$fpCCprefix-$runstr-%c$camcol-$fieldstr.fit"
outlMfileformat="$simdir/$lMprefix-$runstr-%c$camcol-$fieldstr.fit"
outlPfileformat="$simdir/$lPprefix-$runstr-%c$camcol-$fieldstr.fit"
outlRfileformat="$simdir/$lRprefix-$runstr-%c$camcol-$fieldstr.fit"
outlZfileformat="$simdir/$lZprefix-$runstr-%c$camcol-$fieldstr.fit"
outlWfileformat="$simdir/$lWprefix-$runstr-%c$camcol-$fieldstr.fit"
fi

if [ $multibandmle == yes ]; then
inphflatframefileformat="$datadir/$fpCprefix-$runstr-%c$camcol-$fieldstr.fit"
inthflatframefileformat="$datadir/$fpCCprefix-$runstr-%c$camcol-$fieldstr.fit"
inphsmfileformat="$skyobjc_datadir/$fpBINprefix-$runstr-%c$camcol-$fieldstr.fit"
outthobjcfileformat="$objcdir/$gpObjcprefix-$runstr-%c$camcol-$fieldstr.fit"
outpsfreportfileformat="$objcdir/$psfRprefix-$runstr-%c$camcol-$fieldstr.fit"
outpsfstatsfileformat="$objcdir/$psfSprefix-$runstr-%c$camcol-$fieldstr.fit"
outlMfileformat="$objcdir/$lMprefix-$runstr-%c$camcol-$fieldstr.fit"
outlPfileformat="$objcdir/$lPprefix-$runstr-%c$camcol-$fieldstr.fit"
outlRfileformat="$objcdir/$lRprefix-$runstr-%c$camcol-$fieldstr.fit"
outlZfileformat="$objcdir/$lZprefix-$runstr-%c$camcol-$fieldstr.fit"
outlWfileformat="$objcdir/$lWprefix-$runstr-%c$camcol-$fieldstr.fit"
fi

if [ $lcomment == yes ]; then
echo "# $comment" > $mleparfile
echo "structure-type     = FRAMEFILES" >> $mleparfile
else
echo "structure-type     = FRAMEFILES" > $mleparfile
fi
#echo "phconfigfile       = /u/dss/data/$run/137/logs/opConfig-50000.par" >> $mleparfile
#echo "phecalibfile       = /u/dss/data/$run/137/logs/opECalib-51081.par" >> $mleparfile
echo "phconfigfile       = $phconfigfile #address after FAHL's failure" >> $mleparfile
echo "phecalibfile       = $phecalibfile #address after FAHL's failure" >> $mleparfile
echo "phpsfile           = $psFieldfile # PHOTO PSF (KL) (I)" >> $mleparfile
echo "phobjcfile         = $fpObjcfile  # PHOTO object list and properties (I)" >> $mleparfile
echo "photoObjcfile      = $photoObjcfile # PHOTO object list and properties - caliberated (I)" >> $mleparfile
echo "phcalibfile        = #$phcalibfile #PHOTOOP calibration data for the $run, $camcol (I) " >> $mleparfile
echo "gcfile		 = $gcfile	#GALACTIC_CALIB regression file to be generated or to be used (I / O)" >> $mleparfile

if [ $sdsssimulator == no ]; then # the following files have band information in them should not be included in multi-band simulations

echo "phflatframefile    = $fpCfile     # PHOTO Flat-frame (I)" >> $mleparfile
echo "phsmfile		 = $fpBINfile   # PHOTO estimation of sky (I)" >> $mleparfile
echo "thflatframefile    = $fpCCfile    #continuous fpC image for simulation or fit (O)" >> $mleparfile
echo "thobjcfile         = $gpObjcfile  #object properties as found by mle (O)" >> $mleparfile
echo "thpsfreportfile    = $psfRfile    # location of PSF (report) file (O)" >> $mleparfile
echo "thpsfstatfile      = $psfSfile    # location of PSF (stat) file (O)" >> $mleparfile
echo "thlmfile		 = $lMfile      #location of LM dump (O)" >> $mleparfile
echo "thlpfile		 = $lPfile      #location of LP dump (O)" >> $mleparfile
echo "thlrfile		 = $lRfile      #location of LR dump (O)" >> $mleparfile
echo "thlzfile		 = $lZfile      #location of LZ dump (O)" >> $mleparfile
echo "thlwfile		 = $lWfile      #location of LW dump (O)" >> $mleparfile

fi

if [ $sdsssimulator == yes ]; then 

echo "outphflatframefileformat = $outphflatframefileformat #output fpC file format">> $mleparfile
echo "inphsmfileformat         = $inphsmfileformat #input fpBIN file format">> $mleparfile
echo "outphsmfileformat        = $outphsmfileformat  #output fpBIN file format">> $mleparfile
echo "inphobjcfile             = $inphobjcfile #input fpObjc file">> $mleparfile
echo "incalibobjcfile          = $incalibobjcfile #input photoObjc file" >> $mleparfile
echo "outphobjcfile            = $outphobjcfile #output fpObjc file" >> $mleparfile
echo "outthsmfileformat        = $outthsmfileformat #output gpBIN file format">> $mleparfile
echo "outthobjcfileformat      = $outthobjcfileformat #ouput gpObjc file format">> $mleparfile
echo "outthflatframefileformat = $outthflatframefileformat #output fpC file format">> $mleparfile
#echo "thlmfile		 = $lMfile      #location of LM dump (O)" >> $mleparfile
#echo "thlpfile		 = $lPfile      #location of LP dump (O)" >> $mleparfile
echo "outlMfileformat          = $outlMfileformat #output lM file format (chisq)" >> $mleparfile
echo "outlPfileformat          = $outlPfileformat #output lP file format (1D profs)" >> $mleparfile
echo "outlRfileformat          = $outlRfileformat #output lR file format (residual)" >> $mleparfile
echo "outlZfileformat          = $outlZfileformat #output lZ file format (z-scores of residual)" >> $mleparfile
echo "outlWfileformat          = $outlWfileformat #output lW file format (weight image)" >> $mleparfile


#echo "outphflatframefile =  $outphflatframefile #output fpC file format"
#echo "inphsmfile         =  $inphsmfile  #input fpBIN file"
#echo "outphsmfile        =  $outphsmfile #output fpBIN file"
#echo "outthsmfile        =  $outthsmfile #output gpBIN file"
#echo "outthobjcfile      =  $outthobjcfile #ouput gpObjc file"
#echo "outthflatframefile =  $outthflatframefile  #output fpC file"

fi

if [ $multibandmle == yes ]; then

echo "inphflatframefileformat = $inphflatframefileformat #input fpC file format" >> $mleparfile
echo "inthflatframefileformat = $inthflatframefileformat #input fpCC file format" >> $mleparfile
echo "inphsmfileformat        = $inphsmfileformat #input fpBIN file format" >> $mleparfile
echo "outthobjcfileformat     = $outthobjcfileformat #output gpObjc file format" >> $mleparfile
echo "outpsfreportfileformat  = $outpsfreportfileformat #output psf report file format" >> $mleparfile
echo "outlMfileformat         = $outlMfileformat #output lM file format (chisq tracking)" >> $mleparfile
echo "outlPfileformat         = $outlPfileformat #output lP file format (1D profiles for objects and their component models)" >> $mleparfile
echo "outlRfileformat         = $outlRfileformat #output lR file format (residual)" >> $mleparfile
echo "outlZfileformat         = $outlZfileformat #output lZ file format (z-score of the residual)" >> $mleparfile
echo "outlWfileformat         = $outlWfileformat #output lW file format (weight image)" >> $mleparfile
echo "outpsfstatsfileformat   = $outpsfstatsfileformat #output psf statistics file format" >> $mleparfile

fi

echo "outdir             = $outdir #output directory for all other products (O)" >> $mleparfile
}

