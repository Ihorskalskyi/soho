
/* SDSS construction */
#include "dervish.h"
#include "phObjc.h"
#include "phPhotoFitsIO.h"
/* SOHO Libs */
#include "sohoEnv.h"
#include "shDebug.h"

RET_CODE shCpFpObjc(char *infile, char *outfile);
OBJC *shReadFpObjc(char *file, RET_CODE *status);
RET_CODE shWriteFpObjc(char *file, OBJC *objc);

