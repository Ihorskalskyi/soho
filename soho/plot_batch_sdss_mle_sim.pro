pro plot_batch_sdss_mle_sim, debugio = debugio, comparison = comparison, $
	circular = circular, elliptical = elliptical, $
	deV = deV, exponential = exponential, star = star, $
	random = random, bias = bias, rnderror = rnderror, prob = prob, $
	camcollist = camcollist

name = "plot_batch_sdss_mle_sim"

if ((not keyword_set(circular)) and (not keyword_set(elliptical))) then begin
	circular = 1
	elliptical = 1
endif
if ((not keyword_set(star)) and (not keyword_set(deV)) and (not keyword_set(exponential))) then begin
	deV = 1
	exponential = 1
	star = 1
endif
if (keyword_set(random) + keyword_set(bias) + keyword_set(rnderror) + keyword_set(prob) ne 1) then begin
	print, name, ": ERROR - one and only one keyword among (random, bias, rnderror, prob) should be set"
	return
endif
if (keyword_set(random)) then prefix = "pick"
if (keyword_set(rnderror)) then prefix = "random-error"
if (keyword_set(bias)) then prefix = "bias"
if (keyword_set(prob)) then prefix = "prob"

run=LONG(GETENV('DEFAULT_SDSS_RUN'))
rerun=STRING(GETENV('DEFAULT_SDSS_RERUN'))
runlist = [run]

;;stripe 82;
;; 4797
runlist=[4797]; , 2700] ;; , 2703, 2708, 2709] ;; , 2960, 3565, 3434, 3437, 2968, 4207, 1742, 4247, 4252, 4253, 4263, 4288, 1863, 1887, 2570, 2578, 2579, 109, 125, 211, 240, 241, 250, 251, 256, 259, 273, 287, 297, 307, 94, 5036, 5042, 5052, 2873, $ ;; 21, 24, 
;; 1006, 1009, 1013, 1033, 1040, 1055, 1056, 1057, 4153, 4157, 4158, 4184, 4187, 4188, 4191, 4192, 2336, 2886, 2955, 3325, 3354, 3355, 3360, 3362, 3368, 2385, 2506, 2728, 4849, 4858, 4868, 4874, 4894, 4895, 4899, 4905, 4927, 4930, 4933, 4948, 2854, 2855, 2856, 2861, 2662, 3256, 3313, 3322, 4073, 4198, 4203, 3384, 3388, 3427, 3430, 4128, 4136, 4145, 2738, 2768, 2820, 1752, 1755, 1894, 2583, 2585, 2589, 2591, 2649, 2650, 2659, 2677, 3438, 3460, 3458, 3461, 3465, 7674, 7183]

;; n4=119




simroot="/u/khosrow/thesis/opt/soho/single-galaxy/images/sims"
mleroot="/u/khosrow/thesis/opt/soho/single-galaxy/images/mle"

n1 = 2L
n2 = 2L
n3 = 1L

source_model_array = [""]
if (keyword_set(deV)) then source_model_array = [source_model_array, ["deV"]]
if (keyword_set(exponential)) then source_model_array = [source_model_array, ["exp"]]
if (keyword_set(star)) then source_model_array = [source_model_array, ["star"]]
n1_temp = n_elements(source_model_array)
source_model_array = source_model_array[1L:n1_temp-1L]
n1 = n_elements(source_model_array)

target_model_array = ["deV", "exp", "deV-exp"]
;; MLE degrees of freedom:
;; single model only (6): re (1), center (2), phi (1), e(1), counts(1)
;; double model (11): locked centers
;; PHOTO degrees of freesom
;; double model (6): re(2), center (locked, 2), counts(2)
target_df_array = [5, 5, 9]
rerunstring_array = [""]  ;; , "high-precision/", "low-precision-random-init-2/"]
rerunprefix_array = [""]  ;;, "-high-precision", "-low-precision-random-init-2"]

unames = ['xc', 'yc', 'mag', 'counts', 're', 'e', 'phi', 'drc']
xcrange = [-2.0, 2.0]
ycrange = xcrange
magrange = [-1.0, 1.0]
countsrange  = [-1.0, 1.0]
rerange = [-3.0, 3.0]
erange = [-1.0, 1.0]
phirange = [-46.0, 46.0]
drcrange = [-1.0, 1.0]
uranges = [[xcrange], [ycrange], [magrange], [countsrange], [rerange], [erange], [phirange], [drcrange]]

doraw = 1
dohisto = 1
dosmooth = 0
doshisto = 0

dochisq = 1
doerror = 1
docomparerr = 1
docolor = 1
dounsigned = 0
dosigned = 1
dolog = 1
dographicallog = 1
dogood = 0
n_level = 40

doell = 1
domag = 1
doarcsec = 1

dorandom = keyword_set(random)
dobias = keyword_set(bias)
dornderror = keyword_set(rnderror)
doprob = keyword_set(prob)

dovs=1
dophoto=1
domle=1
doseparate = 1

seed = total(10 ^ (lindgen(6) * 2) * bin_date(systime(0)))

if (keyword_set(comparison)) then begin

;; this block generates the output similar to 2014 results for sake of comparison
correction_uranges = [[xcrange], [ycrange], [-6.0, 6.0], [-10.0, 10.0], [-10.0, 10.0], [erange], [phirange], [drcrange]]

directory = "./circular-y"
ncat = 1

rootsuffix="/circular"
simdir=simroot+rootsuffix
mledir=mleroot+rootsuffix

source_model = "exp"
target_model = "exp"
rerunprefix = ""


print, name, ": COMMENT - circular models: creating chisq and unsigned error plots"
chisqfilename = directory+"/corrected-chisq-circular"+rerunprefix+"-src-"+source_model+"-trg-"+target_model+".eps"
errorfilename = directory+"/corrected-error-circular"+rerunprefix+"-src-"+source_model+"-trg-"+target_model+".eps"

plot_sdss_mle_sim, /domag, /doarcsec, /doell, $
	target_model = target_model, source_model = source_model, $
	camcollist = camcollist, simprefix=simprefix, simroot=simdir, mleroot = mledir, $
	runlist = runlist, rerun = rerun, $
	n_level = n_level, $
	/catell, ncat = ncat, force_nstep_max = 200, $
	doerror = doerror, /dosigned, $
	errorfilename = errorfilename, unames = unames, uranges = correction_uranges, $
	seed = seed, nerrorbin = 40, $
	/doraw, /dohisto, $
	dolog = dolog, dographicallog = dographicallog, $
	docolor = docolor, debugio = keyword_set(debugio), $
	seed = seed, random = dorandom, bias = dobias, rnderror = dornderror, prob = doprob

source_model = "exp"
target_model = "deV-exp"
rerunprefix = ""


print, name, ": COMMENT - circular models: creating chisq and unsigned error plots"
chisqfilename = directory+"/corrected-chisq-circular"+rerunprefix+"-src-"+source_model+"-trg-"+target_model+".eps"
errorfilename = directory+"/corrected-error-circular"+rerunprefix+"-src-"+source_model+"-trg-"+target_model+".eps"

plot_sdss_mle_sim, /domag, /doarcsec, /doell, $
	target_model = target_model, source_model = source_model, $
	camcollist = camcollist, simprefix=simprefix, simroot=simdir, mleroot = mledir, $
	runlist = runlist, rerun = rerun, $
	n_level = n_level, $
	/catell, ncat = ncat, force_nstep_max = 200, $
	doerror = doerror, /dosigned, $
	errorfilename = errorfilename, unames = unames, uranges = correction_uranges, $
	seed = seed, nerrorbin = 40, $
	/doraw, /dohisto, $
	dolog = dolog, dographicallog = dographicallog, $
	docolor = docolor, debugio = keyword_set(debugio), $
	seed = seed, random = dorandom, bias = dobias, rnderror = dornderror, prob = doprob


return
endif

for i1 = 0L, (n1 - 1L), 1L do begin

source_model = source_model_array[i1]
if (strcmp(source_model, "deV") or strcmp(source_model, "exp")) then begin
	target_model_array = ["deV-exp", source_model]
	n2 = 2
endif
if (strcmp(source_model, "star")) then begin
	target_model_array = [source_model]
	n2 = 1
endif

for i2 = 0L, (n2 - 1L), 1L do begin
for i3 = 0L, (n3 - 1L), 1L do begin

print, name, ": i1, i2, i3 = ", i1, i2, i3

source_model = source_model_array[i1]
target_model = target_model_array[i2]
target_df = target_df_array[i2]
rerunstring = rerunstring_array[i3]
rerunprefix = rerunprefix_array[i3]


if (strcmp(source_model, "deV") or strcmp(source_model, "exp")) then begin

	if (keyword_set(circular)) then begin

	directory = "./circular-y"
	ncat = 1

	rootsuffix="/circular"
	simdir=simroot+rootsuffix
	mledir=mleroot+rootsuffix

	print, name, ": COMMENT - circular models: creating chisq and unsigned error plots"
	chisqfilename = directory+"/chisq-circular"+rerunprefix+"-src-"+source_model+"-trg-"+target_model+".eps"
	errorfilename = directory+"/"+prefix+"-circular"+rerunprefix+"-src-"+source_model+"-trg-"+target_model+".eps"
	vsfilename = directory+"/vs-circular"+rerunprefix+"-src-"+source_model+"-trg-"+target_model+".eps"

	dogood = 0
	plot_sdss_mle_sim, domag = domag, doarcsec = doarcsec, doell = doell, $
	target_model = target_model, source_model = source_model, $
	camcollist = camcollist, simprefix=simprefix, simroot=simdir, mleroot = mledir, $
	runlist = runlist, rerun = rerun, $
	dochisq = dochisq, chisqfilename = chisqfilename, n_level = n_level, $
	/catell, ncat = ncat, force_nstep_max = 200, $
	doerror = doerror, dosigned = dosigned, dounsigned = dounsigned, $
	errorfilename = errorfilename, unames = unames, uranges = uranges, $
	seed = seed, nerrorbin = 40, $
	doraw = doraw, dohisto = dohisto, dosmooth = dosmooth, doshisto = doshisto, $
	dolog = dolog, dographicallog = dographicallog, dogood = dogood, $
	docolor = docolor, docomparerr = docomparerr, debugio = keyword_set(debugio), $
	random = dorandom,  bias = dobias, rnderror = dornderror, prob = doprob, $
	dovs = dovs, dophoto = dophoto, domle = domle, doseparate = doseparate, epsfilename = vsfilename

	directory = "./circular-y"
	ncat = 1

	rootsuffix="/circular"
	simdir=simroot+rootsuffix
	mledir=mleroot+rootsuffix

	print, name, ": COMMENT - circular models: creating chisq and unsigned error plots"
	chisqfilename = directory+"/chisq-gb-circular"+rerunprefix+"-src-"+source_model+"-trg-"+target_model+".eps"
	errorfilename = directory+"/"+prefix+"-gb-circular"+rerunprefix+"-src-"+source_model+"-trg-"+target_model+".eps"
	vsfilename = directory+"/vs-gb-circular"+rerunprefix+"-src-"+source_model+"-trg-"+target_model+".eps"
	dogood = 0
	plot_sdss_mle_sim, domag = domag, doarcsec = doarcsec, doell = doell, $
	target_model = target_model, source_model = source_model, $
	camcollist = camcollist, simprefix=simprefix, simroot=simdir, mleroot = mledir, $
	runlist = runlist, rerun = rerun, $
	dochisq = dochisq, chisqfilename = chisqfilename, n_level = n_level, $
	/catell, ncat = ncat, force_nstep_max = 200, $
	doerror = doerror, dosigned = dosigned, dounsigned = dounsigned, $
	errorfilename = errorfilename, unames = unames, uranges = uranges, $
	seed = seed, nerrorbin = 40, $
	doraw = doraw, dohisto = dohisto, dosmooth = dosmooth, doshisto = doshisto, $
	dolog = dolog, dographicallog = dographicallog, dogood = dogood, $
	docolor = docolor, docomparerr = docomparerr, debugio = keyword_set(debugio), $
	random = dorandom,  bias = dobias, rnderror = dornderror, prob = doprob, $
	dovs = dovs, dophoto = dophoto, domle = domle, doseparate = doseparate, epsfilename = vsfilename
	
	endif

	if (keyword_set(elliptical)) then begin

	ncat = 5
	directory = "./elliptical-x"

	rootsuffix="/all"
	simdir=simroot+rootsuffix
	mledir=mleroot+rootsuffix

	print, name, ": COMMENT - non-circular models: creating chisq and unsigned error plots"
	chisqfilename = directory+"/chisq-elliptical"+rerunprefix+"-src-"+source_model+"-trg-"+target_model+".eps"
	errorfilename = directory+"/"+prefix+"-elliptical"+rerunprefix+"-src-"+source_model+"-trg-"+target_model+".eps"
	vsfilename = directory+"/vs-elliptical"+rerunprefix+"-scr-"+source_model+"-trg-"+target_model+".eps"

	dogood = 0
	plot_sdss_mle_sim, domag = domag, doarcsec = doarcsec, doell = doell, $
	target_model = target_model, source_model = source_model, $
	camcollist = camcollist, simprefix = simprefix, simroot = simdir, mleroot = mledir, $
	runlist = runlist, rerun = rerun, $
	dochisq = dochisq, chisqfilename = chisqfilename, n_level = n_level, $
	/catlum, ncat = ncat, force_nstep_max = 200, $
	doerror = doerror, dosigned = dosigned, dounsigned = dounsigned, $
	errorfilename = errorfilename, unames = unames, uranges = uranges, $
	seed = seed, nerrorbin = 40, $	
	doraw = doraw, dohisto = dohisto, dosmooth = dosmooth, doshisto = doshisto, $
	dolog = dolog, dographicallog = dographicallog, dogood = dogood, $
	docolor = docolor,  docomparerr = docomparerr, debugio = keyword_set(debugio), $
	random = dorandom,  bias = dobias, rnderror = dornderror, prob = doprob, $
	dovs = dovs, dophoto = dophoto, domle = domle, doseparate = doseparate, epsfilename = vsfilename

	endif

endif

if (strcmp(source_model, "star")) then begin

	directory = "./star-x"
	ncat = 1

	rootsuffix=""
	simdir=simroot+rootsuffix
	mledir=mleroot+rootsuffix

	print, name, ": COMMENT - star models: creating error plots"
	;;chisqfilename = directory+"/chisq-circular"+rerunprefix+"-src-"+source_model+"-trg-"+target_model+".eps"
	errorfilename = directory+"/"+prefix+rerunprefix+"-src-"+source_model+"-trg-star.eps"
	vsfilename = directory+"/vs"+rerunprefix+"-src-"+source_model+"-trg-star.eps"

	dogood = 0
	plot_sdss_mle_sim, domag = domag, doarcsec = doarcsec, doell = doell, $
	target_model = target_model, source_model = source_model, $
	camcollist = camcollist, simprefix=simprefix, simroot=simdir, mleroot = mledir, $
	runlist = runlist, rerun = rerun, $
	dochisq = dochisq, chisqfilename = chisqfilename, n_level = n_level, $
	/catell, ncat = ncat, force_nstep_max = 200, $
	doerror = doerror, dosigned = dosigned, dounsigned = dounsigned, $
	errorfilename = errorfilename, unames = unames, uranges = uranges, $
	seed = seed, nerrorbin = 40, $
	doraw = doraw, dohisto = dohisto, dosmooth = dosmooth, doshisto = doshisto, $
	dolog = dolog, dographicallog = dographicallog, dogood = dogood, $
	docolor = docolor, docomparerr = docomparerr, debugio = keyword_set(debugio), $
	random = dorandom,  bias = dobias, rnderror = dornderror, prob = doprob, $
	dovs = dovs, dophoto = dophoto, domle = domle, doseparate = doseparate, epsfilename = vsfilename


endif

endfor
endfor
endfor


end
