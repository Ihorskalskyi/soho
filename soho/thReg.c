#include "thReg.h"

THPIX thRegDotReg(REGION *reg1, REGION *reg2, 
		  REGION *wp, MASK *mask, /* wp and mask are optional
						will be 1 if NULL */
		  unsigned char maskbit ) 
{
  
  char *name = "thRegDotReg";

  int i, j, nrow, ncol;

  nrow = reg1->nrow;
  ncol = reg1->ncol;

  shAssert(reg2->nrow == nrow);
  shAssert(reg2->ncol == ncol);

  if (mask != NULL) {
    shAssert(mask->nrow == nrow);
    shAssert(mask->ncol == ncol);
  }

  if (wp != NULL) {
    shAssert(wp->nrow == nrow);
    shAssert(wp->ncol == ncol);
  }

  long double res = 0.0;
  long double rowres = 0.0;

  THPIX *rrows1, *rrows2, *wrows;
  unsigned char *mrows;

  if (wp == NULL && mask != NULL) {
    
    for (i = 0; i < nrow; i++) {
      rrows1 = reg1->rows_thpix[i];
      rrows2 = reg2->rows_thpix[i];
      mrows = mask->rows[i];
      rowres = (long double) 0.0;
      for (j = 0; j < ncol; j++) {
	if (mrows[j] & maskbit) {
	  rowres += (long double) (rrows1[j] * rrows2[j]);
	}
      }
      res += rowres;
    }
    
  } else if (wp != NULL && mask != NULL) {
    
    /* debug
    printf("%s: total number of sky pixels (%d) \n", 
	   name, thCountMaskPix(0, nrow-1, 0, ncol - 1, mask, maskbit));
    int pixcount = 0;
    */

    for (i = 0; i < nrow; i++) {
      rrows1 = reg1->rows_thpix[i];
      rrows2 = reg2->rows_thpix[i];
      mrows = mask->rows[i];
      wrows = wp->rows_thpix[i];
      rowres = (long double) 0.0;

      for (j = 0; j < ncol; j++) {
	
	if (mrows[j] & maskbit) {
	  /* debug
	  pixcount++;
	  */
	  rowres += (long double) (rrows1[j] * rrows2[j] * wrows[j]);
	}
      }
      res += rowres;
    } 
    
    /* debug
    printf("%s: second estim of sky pixels (%d) \n", 
	   name, pixcount);
    */

    /* 
       the following step-by-step multipliction 
       is 5 - 10 times slower than the direct 
       multiplication of the three matrices above

    REGION *treg;
    treg = shRegNew("temp", nrow, ncol, TYPE_THPIX);

    shRegPixCopy(reg1, treg);
    thRegCrossReg(treg, wp, treg);
    thRegCrossReg(treg, reg2, treg);
    res = thRegSum(treg, 0, nrow - 1, 0, ncol - 1,
		   NULL, mask, maskbit);

    shRegDel(treg);
    
    */
    
  } else if (wp != NULL && mask == NULL) {
   
    for (i = 0; i < nrow; i++) {
      rrows1 = reg1->rows_thpix[i];
      rrows2 = reg2->rows_thpix[i];
      wrows = wp->rows_thpix[i];
      rowres = (long double) 0.0;
      for (j = 0; j < ncol; j++) {
	
	rowres += (long double) (rrows1[j] * rrows2[j] * wrows[j]);
      }
      res += rowres;
    }

  } else if (wp == NULL && mask == NULL) {
	
    for (i = 0; i < nrow; i++) {
      rrows1 = reg1->rows_thpix[i];
      rrows2 = reg2->rows_thpix[i];
      rowres = (long double) 0.0;
      for (j = 0; j < ncol; j++) {
	
	rowres += (long double) (rrows1[j] * rrows2[j]); 
      }
      res += rowres;
    }

  }
      
  return(((THPIX)res));
      
}


RET_CODE thRegAddScalar(THPIX c, REGION *reg, REGION *res,
			int row0, int row1, int col0, int col1)
{

  char *name = "thRegAddScalar";

  if (reg == NULL) {
    thError("%s: NULL pointer passed as region - no change made to result", name);
    return(SH_GENERIC_ERROR);
  }

  int nrow, ncol;

  nrow = reg->nrow;
  ncol = reg->ncol;

  if (res == NULL) {
    res = shRegNew(reg->name, nrow, ncol, TYPE_THPIX);
  } else if (nrow != res->nrow || ncol != res->ncol) {
    thError("%s: mismatching input and output region size", name);
    return(SH_GENERIC_ERROR);
  }

  int i, j;

  row0 = MAX(row0, 0);
  row1 = MIN(row1, nrow - 1);
  
  col0 = MAX(col0, 0);
  col1 = MIN(col1, ncol - 1);

  THPIX *resrows, *rrows;

  for (i = 0; i< row0; i++) {
    resrows = res->rows_thpix[i];
    rrows = reg->rows_thpix[i];
    for (j = 0; j < ncol; j++) {
      resrows[j] = rrows[j];
    }
  }

  for (i = row1 + 1; i < nrow; i++) {
    resrows = res->rows_thpix[i];
    rrows = reg->rows_thpix[i];
    for (j = 0; j < ncol; j++) {
      resrows[j] = rrows[j];
    }
  }

  for (i = row0; i <= row1; i++) {
    resrows = res->rows_thpix[i];
    rrows = reg->rows_thpix[i];
    for (j = 0; j < col0; j++) {
      resrows[j] = rrows[j];
    }
  }
  
  for (i = row0; i <= row1; i++) {
    resrows = res->rows_thpix[i];
    rrows = reg->rows_thpix[i];
    for (j = col1 + 1; j < ncol; j++) {
      resrows[j] = rrows[j];
    }
  }
  
  for (i = row0; i <= row1; i++) {
    resrows = res->rows_thpix[i];
    rrows = reg->rows_thpix[i];
    for (j = col0; j <= col1; j++) {
      resrows[j] = rrows[j] + c;
    }
  }

  return(SH_SUCCESS);
  
}
  
RET_CODE thRegCrossScalar(THPIX c, REGION *reg, REGION *res)
{

  char *name = "thRegCrossScalar";

  if (reg == NULL) {
    thError("%s: NULL pointer passed as region - no change made to result", name);
    return(SH_GENERIC_ERROR);
  }

  int i, j, nrow, ncol;

  nrow = reg->nrow;
  ncol = reg->ncol;

  if (res == NULL) {
    thError("%s: the output region should be allocated prior to calling this function", name);
    return(SH_GENERIC_ERROR);
  } else if (res->nrow == 0 && res->ncol == 0) {
    res->rows_thpix = (THPIX **) thMalloc(nrow * sizeof(THPIX *));
    for (i = 0; i < nrow; i++) {
      res->rows_thpix[i] = (THPIX *) thMalloc(ncol * sizeof(THPIX));
    }
  } else if (nrow != res->nrow || ncol != res->ncol) {
    thError("%s: mismatching input and output region size -- (nrow = %d, %d), (ncol = %d, %d)", 
		   name, nrow, res->nrow, ncol, res->ncol);
    return(SH_GENERIC_ERROR);
  }

  THPIX *resrows, *rrows;

  for (i = 0; i<nrow; i++) {
    resrows = res->rows_thpix[i];
    rrows = reg->rows_thpix[i];
    for (j = 0; j < ncol; j++) {
      resrows[j] = (rrows[j] * c);
    }
  }

  return(SH_SUCCESS);
  
}

RET_CODE thRegAddReg(REGION *reg1, REGION *reg2, /* input regions */
		     REGION *reg12 /* output region */
		     ) {

    char *name = "thRegAddReg";

  if (reg1 == NULL || reg2 == NULL) {
    thError("%s: One or both of the regions passed to the function is NULL", 
		   name);
    return(SH_GENERIC_ERROR);
  }

  int nrow, ncol, i, j;

  nrow = reg1->nrow;
  ncol = reg1->ncol;

 if( reg2->nrow != nrow || reg2->ncol != ncol){
    thError("%s: size mismatch between input (reg1) and (reg2) -- (nrow = %d, %d), (ncol = %d, %d)", name, nrow, reg2->nrow, ncol, reg2->ncol);
    return(SH_GENERIC_ERROR);
 }


 if (reg12 == NULL) {
   thError("%s: the result region should be allocated prior to calling this function", name);
   return(SH_GENERIC_ERROR);
 } else if (reg12->nrow == 0 && reg12->ncol == 0) {
   reg12->rows_thpix = (THPIX **) thMalloc(nrow * sizeof(THPIX*));
   for (i = 0; i < nrow; i++) {
     reg12->rows_thpix[i] = (THPIX *) thMalloc(ncol * sizeof(THPIX));
   }
 } else if (reg12->nrow != nrow || reg12->ncol != ncol) {
   thError("%s: size mismatch between input and output regions", name);
   return(SH_GENERIC_ERROR);
 }
 
 THPIX *rrows12, *rrows1, *rrows2;

 for (i = 0; i < nrow; i++) {
    rrows12 = reg12->rows_thpix[i];
    rrows1 =  reg1->rows_thpix[i];
    rrows2 = reg2->rows_thpix[i];
    for (j = 0; j < ncol; j++){
      rrows12[j] = rrows2[j] + rrows1[j];
    }
  }

  return(SH_SUCCESS);

}


RET_CODE thRegCrossReg(REGION *reg1, REGION *reg2, /* input regions */
		       REGION *reg12 /*output region */
			     ) {

  char *name = "thRegionCrossRegion";

  if (reg1 == NULL || reg2 == NULL) {
    thError("%s: One or both of the regions passed to the function is NULL", name);
    return(SH_GENERIC_ERROR);
  }

  int nrow, ncol, i, j;

  nrow = reg1->nrow;
  ncol = reg1->ncol;

  if( reg2->nrow != nrow || reg2->ncol != ncol){
    thError("%s: size mismatch between input regions", name);
    return(SH_GENERIC_ERROR);
  }

  if (reg12 == NULL) {
    reg12 = shRegNew(name, nrow, ncol, TYPE_THPIX);
  }

  if (reg12->nrow != nrow || reg12->ncol != ncol) {
    thError("%s: size mismatch between input and output regions", name);
    return(SH_GENERIC_ERROR);
  }

  THPIX *rrows12, *rrows1, *rrows2;

  for (i = 0; i < nrow; i++) {
    rrows12 = reg12->rows_thpix[i];
    rrows1 =  reg1->rows_thpix[i];
    rrows2 = reg2->rows_thpix[i];
    for (j = 0; j < ncol; j++){
      rrows12[j] = rrows1[j] * rrows2[j];
    }
  }

  return(SH_SUCCESS);

}


 THPIX thRegSum(REGION *reg, 
		int row0, int row1, int col0, int col1,
		REGION *wp, MASK *mask, unsigned char maskbit) {

   char *name = "thRegSum";
   
   long double res = (long double) 0.0;
   long double rowres = (long double) 0.0;

   if (reg == NULL) {
     thError("%s: Empty REGION passed to function", name);
     return(res);
   }

   int nrow, ncol;
   int i, j;

   nrow = reg->nrow;
   ncol = reg->ncol;

   row0 = MAX(row0, 0);
   row1 = MIN(row1, nrow - 1);

   col0 = MAX(col0, 0);
   col1 = MIN(col1, ncol - 1);
   
   if (col0 > col1 || row0 > row1) {
     return(res);
   }


   THPIX *rrows, *wrows;
   unsigned char *mrows;

   if (wp == NULL) {

   if (mask != NULL) {
     
     /* making sure that mask and reg are compatible */
     shAssert(mask->nrow == nrow && mask->ncol == ncol);

     for (i = row0; i <= row1; i++) {
       rrows = reg->rows_thpix[i];
       mrows = mask->rows[i];
       rowres = (long double) 0.0;
       for (j = col0; j <= col1; j++) {
	 if (mrows[j] & maskbit) {
	   rowres += (long double) (rrows[j]);
	     }
       }
       res += rowres;
     }

   } else {

     for (i = row0; i <= row1; i++) {
       rrows = reg->rows_thpix[i];
       rowres = (long double) 0.0;
       for (j = col0; j <= col1; j++) {
	 rowres += (long double) (rrows[j]);
       }
       res += rowres;
     }
   }
   
   } else {
     
     /* making sure that wp and reg are compatible */
     shAssert(wp->nrow == nrow && wp -> ncol == ncol);

     if (mask != NULL) {
      
       /* making sure that mask and reg are compatible */
       shAssert(mask->nrow == nrow && mask->ncol == ncol);

       for (i = row0; i <= row1; i++) {
	 rrows = reg->rows_thpix[i];
	 wrows = wp->rows_thpix[i];
	 mrows = mask->rows[i];
	 rowres = (long double) 0.0;
	 for (j = col0; j <= col1; j++) {
	   if (mrows[j] & maskbit) {
	     rowres += (long double) (rrows[j] * wrows[j]);
	       }
	 }
	 res += rowres;
       }
       
     } else {

       for (i = row0; i <= row1; i++) {
	 rrows = reg->rows_thpix[i];
	 wrows = wp->rows_thpix[i];
	 rowres = (long double) 0.0;
	 for (j = col0; j <= col1; j++) {
	   rowres += (long double) (rrows[j] * wrows[j]);
	     }
	 res += rowres;
       }
     }
     
   }
   return(((THPIX) res));
   
 }
 

 int thCountMaskPix(int row0, int row1, 
		    int col0, int col1, 
		    MASK *mask, unsigned char maskbit) {

   int i, j, count = 0;
   
   row0 = MAX(row0, 0);
   col0 = MAX(col0, 0);
   
   if (mask != NULL) {
     col1 = MIN(col1, mask->ncol - 1);
     row1 = MIN(row1, mask->nrow - 1);
   }
   
   if (row0 > row1 || col0 > col1) {
     return(count);
   }
   
   if (mask != NULL) {

     for (i = row0; i <= row1; i++) {
       for (j = col0; j <= col1; j++) {
	 count += (mask->rows[i][j] == maskbit);
       }
     }
     
   } else {

     count = (col1 - col0 + 1) * (row1 - row0 + 1);
     
   }
   return(count);
 }
       
RET_CODE thRegMakeZero(REGION *reg) {
  
  char *name = "thRegMakeZero";
  
  if (reg == NULL) {
    thError("%s: Warning - null region passed", name);
    return(SH_SUCCESS);
  }

  int nrow, ncol;
  nrow = reg->nrow;
  ncol = reg->ncol;

  if (nrow == 0 || ncol == 0) {
    thError("%s: Warning - unallocated rows for the region (nrow = %d, ncol = %d", name, nrow, ncol);
    return(SH_SUCCESS);
  }

  int i, j;
  if (reg->type == TYPE_THPIX) {
    THPIX *rrows_thpix; 
    for (i = 0; i < nrow; i++) {
      rrows_thpix = reg->rows_thpix[i];
      for (j = 0; j < ncol; j++) {
	rrows_thpix[j] = (THPIX) 0.0;
      }
    }
    return(SH_SUCCESS);
  }
  
  if (reg->type == TYPE_FL32) {
    FL32 *rrows_fl32;
    for (i = 0; i < nrow; i++) {
      rrows_fl32 = reg->rows_fl32[i];
      for (j = 0; j < ncol; j++) {
	rrows_fl32[j] = (FL32) 0.0;
      }
    }
    return(SH_SUCCESS);
  }
  
  if (reg->type == TYPE_U32) {
    U32 *rrows_u32;
    for (i = 0; i < nrow; i++) {
      rrows_u32 = reg->rows_u32[i];
      for (j = 0; j < ncol; j++) {
	rrows_u32[j] = (U32) 0;
      }
    }
    return(SH_SUCCESS);
  }

  if (reg->type == TYPE_S32) {
    S32 *rrows_s32;
    for (i = 0; i < nrow; i++) {
      rrows_s32 = reg->rows_s32[i];
      for (j = 0; j < ncol; j++) {
	rrows_s32[j] = (S32) 0;
      }
    }
    return(SH_SUCCESS);
  }

  if (reg->type == TYPE_U8) {
     U8 *rrows_u8;
     for (i = 0; i < nrow; i++) {
       rrows_u8 = reg->rows_u8[i];
       for (j = 0; j < ncol; j++) {
	 rrows_u8[j] = (U8) 0;
       }
     }
     return(SH_SUCCESS);
  }
  
  if (reg->type == TYPE_U16) {
    U16 *rrows_u16;
    for (i = 0; i < nrow; i++) {
      rrows_u16 = reg->rows_u16[i];
      for (j = 0; j < ncol; j++) {
	rrows_u16[j] = (U16) 0;
      }
    }
    return(SH_SUCCESS);
  }
  
  if (reg->type == TYPE_S8) {
    S8 *rrows_s8;
    for (i = 0; i < nrow; i++) {
      rrows_s8 = reg->rows_s8[i];
      for (j = 0; j < ncol; j++) {
	rrows_s8[j] = (S8) 0;
      }
    }
    return(SH_SUCCESS);
  }
  
  if (reg->type == TYPE_S16) {
    S16 *rrows_s16;
    for (i = 0; i < nrow; i++) {
      rrows_s16 = reg->rows_s16[i];
      for (j = 0; j < ncol; j++) {
	rrows_s16[j] = (S16) 0;
      }
    }
    return(SH_SUCCESS);
  }
  
  thError("%s: ERROR - region type not supported", name);
  return(SH_GENERIC_ERROR);
  
}

RET_CODE thRegInplaceAddRegCrossScalar (REGION *res, REGION *reg, THPIX x) {

  char *name = "thRegInplaceAddRegCrossScalar";
  
  if (res == NULL && reg == NULL) {
    thError("%s: Warning - both regions are null", name);
    return(SH_SUCCESS);
  }

  int nrow, ncol;

  if (res == NULL && reg != NULL) {
    nrow = reg->nrow;
    ncol = reg->ncol;
    if (nrow == 0 || ncol == 0) {
      thError("%s: Warning - null and unallocated regions", name);
      return(SH_SUCCESS);
    } else {
      thError("%s: ERROR - cannot add null to a sizable region", name);
      return(SH_GENERIC_ERROR);
    }
  }

  if (res != NULL && reg == NULL) {
    nrow = res->nrow;
    ncol = res->ncol;
    if (nrow == 0 || ncol == 0) {
      thError("%s: Warning - unallocated and null regions", name);
      return(SH_SUCCESS);
    } else {
      thError("%s: ERROR - cannot add a sizable region to a null region", name);
      return(SH_GENERIC_ERROR);
    }
  }

  nrow = res->nrow;
  ncol = res->ncol;
  
  if (nrow != reg->nrow || ncol != reg->ncol) {
    thError("%s: ERROR - mismatching row numbers (%d, %d) vs. (%d, %d)", 
		   name, nrow, ncol, reg->nrow, reg->ncol);
    return(SH_GENERIC_ERROR);
  }

  if (reg->type != TYPE_THPIX || res->type != TYPE_THPIX) {
    thError("%s: ERROR - unsupported type of region(s)", name);
    return(SH_GENERIC_ERROR);
  }

  int i, j;
  THPIX *resrows_thpix;
  THPIX *regrows_thpix;
  for (i = 0; i < nrow; i++) {
    resrows_thpix = res->rows_thpix[i];
    regrows_thpix = reg->rows_thpix[i];
    for (j = 0; j < ncol; j++) {
      resrows_thpix[j] += x * regrows_thpix[j];
    }
  }

  return(SH_SUCCESS);
}
	
RET_CODE thRegConvertPixType (REGION *reg,REGION  *res, 
			     FL64 bzero, FL64 bscale) {

  char *name = "thRegConvertPixType";

  /* 
     this function assumes that RES is a region of size compatible with
     REG
  */

  if (reg == NULL) {
    thError("%s: null passed as source region", name);
    return(SH_GENERIC_ERROR);
  }

  int nrow, ncol;
  nrow = reg->nrow;
  ncol = reg->ncol;
  
  /*
  FL64 *resrows_fl64, *rrows_fl64; 
  */
  FL32 *resrows_fl32, *rrows_fl32;
  S32 *resrows_s32, *rrows_s32;
  U32 *resrows_u32, *rrows_u32;
  S16 *resrows_s16, *rrows_s16; 
  U16 *resrows_u16, *rrows_u16;
  S8 *resrows_s8, *rrows_s8; 
  U8 *resrows_u8, *rrows_u8; 


  if( (nrow == 0) || (ncol == 0)) {
    return(SH_SUCCESS);
  }

  if (res == NULL) {
    thError("%s: null passed as (res) region", name);
    return(SH_GENERIC_ERROR);
  }

  int i, j;
  FL64 bzerofl64, bscalefl64;
  bzerofl64 = bzero;
  bscalefl64 = bscale;
  switch (res->type){

    /* if the result type is FL64 */
    /*
  case TYPE_FL64:

    switch (reg->type){
    case TYPE_FL64:
      for (i = 0; i < nrow; i++) {
	resrows_fl64 = res->rows_fl64[i];
	rrows_fl64 = reg->rows_fl64[i];
	for (j = 0; j < ncol; j++) {
	  resrows_fl64[j] = bzerofl64 + bscalefl64 * (rrows_fl64[j]);
	}
      }
      break;
    case TYPE_FL32:
      for (i = 0; i < nrow; i++) {
	resrows_fl64 = res->rows_fl64[i];
	rrows_fl32 = reg->rows_fl32[i];
	for (j = 0; j < ncol; j++) {
	  resrows_fl64[j] = (FL64) 
	    (bzerofl64 + bscalefl64 * (rrows_fl32[j])); 
	  }
      }
      break;
    case TYPE_S32:
      for (i = 0; i < nrow; i++) {
	resrows_fl64 = res->rows_fl64[i];
	rrows_s32 = reg->rows_s32[i];
	for (j = 0; j < ncol; j++) {
	  resrows_fl64[j] = (FL64) 
	    (bzerofl64 + bscalefl64 * (rrows_s32[j])); 
	    }
      }
      break;
    case TYPE_U32:
      for (i = 0; i < nrow; i++) {
	resrows_fl64 = res->rows_fl64[i];
	rrows_u32 = reg->rows_u32[i];
	for (j = 0; j < ncol; j++) {
	  resrows_fl64[j] = (FL64) 
	    (bzerofl64 + bscalefl64 * (rrows_u32[j])); 
	}
      }
      break;
    case TYPE_S16:
      for (i = 0; i < nrow; i++) {
	resrows_fl64 = res->rows_fl64[i];
	rrows_s16 = reg->rows_s16[i];
	for (j = 0; j < ncol; j++) {
	  resrows_fl64[j] = (FL64) 
	    (bzerofl64 + bscalefl64 * (rrows_s16[j])); 
	}
      }
      break;
    case TYPE_S8:
      for (i = 0; i < nrow; i++) {
	resrows_fl64 = res->rows_fl64[i];
	rrows_s8 = reg->rows_s8[i];
	for (j = 0; j < ncol; j++) {
	  resrows_fl64[j] = (FL64) 
	    (bzerofl64 + bscalefl64 * (rrows_s8[j])); 
	}
      }
      break;
    case TYPE_U8:
      for (i = 0; i < nrow; i++) {
	resrows_fl64 = res->rows_fl64[i];
	rrows_u8 = reg->rows_u8[i];
	for (j = 0; j < ncol; j++) {
	  resrows_fl64[j] = (FL64) 
	    (bzerofl64 + bscalefl64 * (rrows_u8[j])); 
	}
      }
      break;
    default:
      thError("%s: unsupported source region type", name);
      return(SH_GENERIC_ERROR);
      break;
    }
    break;
    */
    /* if result region type is fl32 */
  case TYPE_FL32:
    
    switch (reg->type){
      /*
    case TYPE_FL64:
      for (i = 0; i < nrow; i++) {
	resrows_fl32 = res->rows_fl32[i];
	rrows_fl64 = reg->rows_fl64[i];
	for (j = 0; j < ncol; j++) {
	  resrows_fl32[j] = (FL32) 
	    (bzerofl64 + bscalefl64 * (rrows_fl64[j])); 
	  }
      }
      break;
      */
    case TYPE_FL32:
      for (i = 0; i < nrow; i++) {
	resrows_fl32 = res->rows_fl32[i];
	rrows_fl32 = reg->rows_fl32[i];
	for (j = 0; j < ncol; j++) {
	  resrows_fl32[j] = (FL32) 
	    (bzerofl64 + bscalefl64 * (rrows_fl32[j])); 
	}
      }
      break;
    case TYPE_S32:
      for (i = 0; i < nrow; i++) {
	resrows_fl32 = res->rows_fl32[i];
	rrows_s32 = reg->rows_s32[i];
	for (j = 0; j < ncol; j++) {
	  resrows_fl32[j] = (FL32) 
	    (bzerofl64 + bscalefl64 * (rrows_s32[j])); 
	    }
      }
      break;
    case TYPE_U32:
      for (i = 0; i < nrow; i++) {
	resrows_fl32 = res->rows_fl32[i];
	rrows_u32 = reg->rows_u32[i];
	for (j = 0; j < ncol; j++) {
	  resrows_fl32[j] = (FL32) 
	    (bzerofl64 + bscalefl64 * (rrows_u32[j]));
	}
      }
      break;
    case TYPE_S16:
      for (i = 0; i < nrow; i++) {
	resrows_fl32 = res->rows_fl32[i];
	rrows_s16 = reg->rows_s16[i];
	for (j = 0; j < ncol; j++) {
	  resrows_fl32[j] = (FL32) 
	    (bzerofl64 + bscalefl64 * (rrows_s16[j])); 
	}
      }
      break;
    case TYPE_U16:
      for (i = 0; i < nrow; i++) {
	resrows_fl32 = res->rows_fl32[i];
	rrows_u16 = reg->rows_u16[i];
	for (j = 0; j < ncol; j++) {
	  resrows_fl32[j] = (FL32) 
	    (bzerofl64 + bscalefl64 * (rrows_u16[j])); 
	}
      }
      break;
    case TYPE_S8:
      for (i = 0; i < nrow; i++) {
	resrows_fl32 = res->rows_fl32[i];
	rrows_s8 = reg->rows_s8[i];
	for (j = 0; j < ncol; j++) {
	  resrows_fl32[j] = (FL32) 
	    (bzerofl64 + bscalefl64 * (rrows_s8[j])); 
	}
      }
      break;
    case TYPE_U8:
      for (i = 0; i < nrow; i++) {
	resrows_fl32 = res->rows_fl32[i];
	rrows_u8 = reg->rows_u8[i];
	for (j = 0; j < ncol; j++) {
	  resrows_fl32[j] = (FL32) 
	    (bzerofl64 + bscalefl64 * (rrows_u8[j])); 
	}
      }
      break;
    default:
      thError("%s: unsupported source region type", name);
      return(SH_GENERIC_ERROR);
      break;
    }
    break;
    /* if the target type is S32 */

  case TYPE_S32:

     switch (reg->type){
       /*
     case TYPE_FL64:
       for (i = 0; i < nrow; i++) {
	 resrows_s32 = res->rows_s32[i];
	 rrows_fl64 = reg->rows_fl64[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_s32[j] = (S32) 
	     (bzerofl64 + bscalefl64 * (rrows_fl64[j])); 
	 }
       }
       break;
       */
     case TYPE_FL32:
       for (i = 0; i < nrow; i++) {
	 resrows_s32 = res->rows_s32[i];
	 rrows_fl32 = reg->rows_fl32[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_s32[j] = (S32) 
	     (bzerofl64 + bscalefl64 * (rrows_fl32[j])); 
	 }
       }
       break;
     case TYPE_S32:
       for (i = 0; i < nrow; i++) {
	 resrows_s32 = res->rows_s32[i];
	 rrows_s32 = reg->rows_s32[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_s32[j] = (S32) 
	     (bzerofl64 + bscalefl64 * (rrows_s32[j])); 
	 }
       }
       break;
     case TYPE_U32:
       for (i = 0; i < nrow; i++) {
	 resrows_s32 = res->rows_s32[i];
	 rrows_u32 = reg->rows_u32[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_s32[j] = (S32) 
	     (bzerofl64 + bscalefl64 * (rrows_u32[j])); 
	 }
       }
       break;
     case TYPE_S16:
       for (i = 0; i < nrow; i++) {
	 resrows_s32 = res->rows_s32[i];
	 rrows_s16 = reg->rows_s16[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_s32[j] = (S32) 
	     (bzerofl64 + bscalefl64 * (rrows_s16[j])); 
	 }
       }
       break;
     case TYPE_U16:
       for (i = 0; i < nrow; i++) {
	 resrows_s32 = res->rows_s32[i];
	 rrows_u16 = reg->rows_u16[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_s32[j] = (S32) 
	     (bzerofl64 + bscalefl64 * (rrows_u16[j])); 
	 }
       }
       break;
     case TYPE_S8:
       for (i = 0; i < nrow; i++) {
	 resrows_s32 = res->rows_s32[i];
	 rrows_s8 = reg->rows_s8[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_s32[j] = (S32) 
	     (bzerofl64 + bscalefl64 * (rrows_s8[j])); 
	 }
       }
       break;
     case TYPE_U8:
       for (i = 0; i < nrow; i++) {
	 resrows_s32 = res->rows_s32[i];
	 rrows_u8 = reg->rows_u8[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_s32[j] = (S32) 
	     (bzerofl64 + bscalefl64 * (rrows_u8[j])); 
	 }
      }
       break;
     default:
       thError("%s: unsupported source region type", name);
       return(SH_GENERIC_ERROR);
       break;
     }
     break;
     /* if the target type is U32 */
     
  case TYPE_U32:

    switch (reg->type){
      /*
    case TYPE_FL64:
      for (i = 0; i < nrow; i++) {
	resrows_u32 = res->rows_u32[i];
	rrows_fl64 = reg->rows_fl64[i];
	for (j = 0; j < ncol; j++) {
	  resrows_u32[j] = (U32) 
	    (bzerofl64 + bscalefl64 * (rrows_fl64[j])); 
	}
      }
      break;
      */
    case TYPE_FL32:
      for (i = 0; i < nrow; i++) {
	resrows_u32 = res->rows_u32[i];
	rrows_fl32 = reg->rows_fl32[i];
	for (j = 0; j < ncol; j++) {
	  resrows_u32[j] = (U32) 
	    (bzerofl64 + bscalefl64 * (rrows_fl32[j])); 
	}
      }
      break;
    case TYPE_S32:
      for (i = 0; i < nrow; i++) {
	resrows_u32 = res->rows_u32[i];
	rrows_s32 = reg->rows_s32[i];
	for (j = 0; j < ncol; j++) {
	  resrows_u32[j] = (U32) 
	    (bzerofl64 + bscalefl64 * (rrows_s32[j])); 
	}
      }
      break;
    case TYPE_U32:
      for (i = 0; i < nrow; i++) {
	resrows_u32 = res->rows_u32[i];
	rrows_u32 = reg->rows_u32[i];
	for (j = 0; j < ncol; j++) {
	  resrows_u32[j] = (U32) 
	    (bzerofl64 + bscalefl64 * (rrows_u32[j])); 
	}
      }
      break;
    case TYPE_S16:
      for (i = 0; i < nrow; i++) {
	resrows_u32 = res->rows_u32[i];
	rrows_s16 = reg->rows_s16[i];
	for (j = 0; j < ncol; j++) {
	  resrows_u32[j] = (U32) 
	    (bzerofl64 + bscalefl64 * (rrows_s16[j])); 
	}
      }
      break;
    case TYPE_U16:
      for (i = 0; i < nrow; i++) {
	resrows_u32 = res->rows_u32[i];
	rrows_u16 = reg->rows_u16[i];
	for (j = 0; j < ncol; j++) {
	  resrows_u32[j] = (U32) 
	    (bzerofl64 + bscalefl64 * (rrows_u16[j])); 
	}
      }
      break;
    case TYPE_S8:
      for (i = 0; i < nrow; i++) {
	resrows_u32 = res->rows_u32[i];
	rrows_s8 = reg->rows_s8[i];
	for (j = 0; j < ncol; j++) {
	  resrows_u32[j] = (U32) 
	    (bzerofl64 + bscalefl64 * (rrows_s8[j])); 
	}
      }
      break;
    case TYPE_U8:
      for (i = 0; i < nrow; i++) {
	resrows_u32 = res->rows_u32[i];
	rrows_u8 = reg->rows_u8[i];
	for (j = 0; j < ncol; j++) {
	  resrows_u32[j] = (U32) 
	    (bzerofl64 + bscalefl64 * (rrows_u8[j])); 
	}
      }
      break;
    default:
      thError("%s: unsupported source region type", name);
      return(SH_GENERIC_ERROR);
      break;
    }
    break;
    /* if the target type is S16 */
    
  case TYPE_S16:

     switch (reg->type){
       /*
    case TYPE_FL64:
      for (i = 0; i < nrow; i++) {
	resrows_s16 = res->rows_s16[i];
	rrows_fl64 = reg->rows_fl64[i];
	for (j = 0; j < ncol; j++) {
	  resrows_s16[j] = (S16) 
	    (bzerofl64 + bscalefl64 * (rrows_fl64[j])); 
	}
      }
      break;
       */
     case TYPE_FL32:
       for (i = 0; i < nrow; i++) {
	 resrows_s16 = res->rows_s16[i];
	 rrows_fl32 = reg->rows_fl32[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_s16[j] = (S16) 
	     (bzerofl64 + bscalefl64 * (rrows_fl32[j])); 
	 }
       }
       break;
     case TYPE_S32:
       for (i = 0; i < nrow; i++) {
	resrows_s16 = res->rows_s16[i];
	rrows_s32 = reg->rows_s32[i];
	for (j = 0; j < ncol; j++) {
	  resrows_s16[j] = (S16) 
	    (bzerofl64 + bscalefl64 * (rrows_s32[j])); 
	}
       }
       break;
     case TYPE_U32:
       for (i = 0; i < nrow; i++) {
	 resrows_s16 = res->rows_s16[i];
	 rrows_u32 = reg->rows_u32[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_s16[j] = (S16) 
	     (bzerofl64 + bscalefl64 * (rrows_u32[j])); 
	 }
       }
       break;
     case TYPE_S16:
       for (i = 0; i < nrow; i++) {
	 resrows_s16 = res->rows_s16[i];
	 rrows_s16 = reg->rows_s16[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_s16[j] = (S16) 
	     (bzerofl64 + bscalefl64 * (rrows_s16[j])); 
	 }
       }
       break;
     case TYPE_U16:
       for (i = 0; i < nrow; i++) {
	 resrows_s16 = res->rows_s16[i];
	 rrows_u16 = reg->rows_u16[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_s16[j] = (S16) 
	     (bzerofl64 + bscalefl64 * (rrows_u16[j])); 
	 }
       }
       break;
     case TYPE_S8:
       for (i = 0; i < nrow; i++) {
	 resrows_s16 = res->rows_s16[i];
	 rrows_s8 = reg->rows_s8[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_s16[j] = (S16) 
	     (bzerofl64 + bscalefl64 * (rrows_s8[j])); 
	 }
       }
       break;
     case TYPE_U8:
       for (i = 0; i < nrow; i++) {
	 resrows_s16 = res->rows_s16[i];
	 rrows_u8 = reg->rows_u8[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_s16[j] = (S16) 
	     (bzerofl64 + bscalefl64 * (rrows_u8[j])); 
	 }
       }
       break;
     default:
       thError("%s: unsupported source region type", name);
      return(SH_GENERIC_ERROR);
      break;
     }
     break;
     /* if the target type is U16 */
     
  case TYPE_U16:
    
    switch (reg->type){
      /*
    case TYPE_FL64:
      for (i = 0; i < nrow; i++) {
	resrows_u16 = res->rows_u16[i];
	rrows_fl64 = reg->rows_fl64[i];
	for (j = 0; j < ncol; j++) {
	  resrows_u16[j] = (U16) (bzerofl64 + bscalefl64 * (rrows_fl64[j])); 
	}
	}
      break;
      */
    case TYPE_FL32:
      for (i = 0; i < nrow; i++) {
	resrows_u16 = res->rows_u16[i];
	rrows_fl32 = reg->rows_fl32[i];
	for (j = 0; j < ncol; j++) {
	  resrows_u16[j] = (U16) 
	    (bzerofl64 + bscalefl64 * (rrows_fl32[j])); 
	  }
      }
      break;
    case TYPE_S32:
      for (i = 0; i < nrow; i++) {
	resrows_u16 = res->rows_u16[i];
	rrows_s32 = reg->rows_s32[i];
	for (j = 0; j < ncol; j++) {
	  resrows_u16[j] = (U16) 
	    (bzerofl64 + bscalefl64 * (rrows_s32[j])); 
	    }
      }
      break;
    case TYPE_U32:
      for (i = 0; i < nrow; i++) {
	resrows_u16 = res->rows_u16[i];
	rrows_u32 = reg->rows_u32[i];
	for (j = 0; j < ncol; j++) {
	  resrows_u16[j] = (U16) 
	    (bzerofl64 + bscalefl64 * (rrows_u32[j])); 
	    }
      }
      break;
    case TYPE_S16:
      for (i = 0; i < nrow; i++) {
	resrows_u16 = res->rows_u16[i];
	rrows_s16 = reg->rows_s16[i];
	for (j = 0; j < ncol; j++) {
	  resrows_u16[j] = (U16) 
	    (bzerofl64 + bscalefl64 * (rrows_s16[j])); 
	}
      }
      break;
    case TYPE_U16:
      for (i = 0; i < nrow; i++) {
	resrows_u16 = res->rows_u16[i];
	rrows_u16 = reg->rows_u16[i];
	for (j = 0; j < ncol; j++) {
	  resrows_u16[j] = (U16) 
	    (bzerofl64 + bscalefl64 * (rrows_u16[j])); 
	}
      }
      break;
    case TYPE_S8:
      for (i = 0; i < nrow; i++) {
	resrows_u16 = res->rows_u16[i];
	rrows_s8 = reg->rows_s8[i];
	for (j = 0; j < ncol; j++) {
	  resrows_u16[j] = (U16) 
	    (bzerofl64 + bscalefl64 * (rrows_s8[j])); 
	}
      }
      break;
    case TYPE_U8:
      for (i = 0; i < nrow; i++) {
	resrows_u16 = res->rows_u16[i];
	rrows_u8 = reg->rows_u8[i];
	for (j = 0; j < ncol; j++) {
	  resrows_u16[j] = (U16) 
	    (bzerofl64 + bscalefl64 * (rrows_u8[j])); 
	}
      }
      break;
    default:
      thError("%s: unsupported source region type", name);
      return(SH_GENERIC_ERROR);
      break;
     }
    break;
     /* if the target type is S8 */

  case TYPE_S8:

     switch (reg->type){
       /*
    case TYPE_FL64:
      for (i = 0; i < nrow; i++) {
	resrows_s8 = res->rows_s8[i];
	rrows_fl64 = reg->rows_fl64[i];
	for (j = 0; j < ncol; j++) {
	  resrows_s8[j] = (S8) 
	    (bzerofl64 + bscalefl64 * (rrows_fl64[j])); 
	    }
      }
      break;
       */
     case TYPE_FL32:
       for (i = 0; i < nrow; i++) {
	 resrows_s8 = res->rows_s8[i];
	 rrows_fl32 = reg->rows_fl32[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_s8[j] = (S8) 
	     (bzerofl64 + bscalefl64 * (rrows_fl32[j])); 
	     }
      }
       break;
     case TYPE_S32:
       for (i = 0; i < nrow; i++) {
	resrows_s8 = res->rows_s8[i];
	rrows_s32 = reg->rows_s32[i];
	for (j = 0; j < ncol; j++) {
	  resrows_s8[j] = (S8) 
	    (bzerofl64 + bscalefl64 * (rrows_s32[j])); 
	    }
       }
       break;
     case TYPE_U32:
       for (i = 0; i < nrow; i++) {
	 resrows_s8 = res->rows_s8[i];
	 rrows_u32 = reg->rows_u32[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_s8[j] = (S8) 
	     (bzerofl64 + bscalefl64 * (rrows_u32[j])); 
	     }
       }
       break;
     case TYPE_S16:
       for (i = 0; i < nrow; i++) {
	 resrows_s8 = res->rows_s8[i];
	 rrows_s16 = reg->rows_s16[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_s8[j] = (S8) 
	     (bzerofl64 + bscalefl64 * (rrows_s16[j]));
	     }
       }
       break;
     case TYPE_U16:
       for (i = 0; i < nrow; i++) {
	 resrows_s8 = res->rows_s8[i];
	 rrows_u16 = reg->rows_u16[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_s8[j] = (S8) 
	     (bzerofl64 + bscalefl64 * (rrows_u16[j]));
	     }
       }
       break;
     case TYPE_S8:
       for (i = 0; i < nrow; i++) {
	resrows_s8 = res->rows_s8[i];
	rrows_s8 = reg->rows_s8[i];
	for (j = 0; j < ncol; j++) {
	  resrows_s8[j] = (S8) 
	    (bzerofl64 + bscalefl64 * (rrows_s8[j])); 
	}
       }
       break;
     case TYPE_U8:
       for (i = 0; i < nrow; i++) {
	 resrows_s8 = res->rows_s8[i];
	 rrows_u8 = reg->rows_u8[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_s8[j] = (S8) 
	     (bzerofl64 + bscalefl64 * (rrows_u8[j])); 
	 }
       }
       break;
     default:
       thError("%s: unsupported source region type", name);
       return(SH_GENERIC_ERROR);
       break;
     }
     break;
     /* if the target type is U8 */

  case TYPE_U8:

     switch (reg->type){
       /*
     case TYPE_FL64:
       for (i = 0; i < nrow; i++) {
	 resrows_u8 = res->rows_u8[i];
	 rrows_fl64 = reg->rows_fl64[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_u8[j] = (U8) 
	     (bzerofl64 + bscalefl64 * (rrows_fl64[j])); 
	 }
       }
       break;
       */
     case TYPE_FL32:
       for (i = 0; i < nrow; i++) {
	 resrows_u8 = res->rows_u8[i];
	 rrows_fl32 = reg->rows_fl32[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_u8[j] = (U8) 
	     (bzerofl64 + bscalefl64 * (rrows_fl32[j]));
	 }
       }
       break;
     case TYPE_S32:
       for (i = 0; i < nrow; i++) {
	 resrows_u8 = res->rows_u8[i];
	 rrows_s32 = reg->rows_s32[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_u8[j] = (U8) 
	     (bzerofl64 + bscalefl64 * (rrows_s32[j])); 
	 }
       }
       break;
     case TYPE_U32:
       for (i = 0; i < nrow; i++) {
	 resrows_u8 = res->rows_u8[i];
	 rrows_u32 = reg->rows_u32[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_u8[j] = (U8) 
	     (bzerofl64 + bscalefl64 * (rrows_u32[j])); 
	 }
       }
       break;
     case TYPE_S16:
       for (i = 0; i < nrow; i++) {
	 resrows_u8 = res->rows_u8[i];
	 rrows_s16 = reg->rows_s16[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_u8[j] = (U8) 
	     (bzerofl64 + bscalefl64 * (rrows_s16[j])); 
	 }
       }
       break;
     case TYPE_U16:
       for (i = 0; i < nrow; i++) {
	 resrows_u8 = res->rows_u8[i];
	 rrows_u16 = reg->rows_u16[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_u8[j] = (U8) 
	     (bzerofl64 + bscalefl64 * (rrows_u16[j])); 
	 }
       }
       break;
     case TYPE_S8:
       for (i = 0; i < nrow; i++) {
	 resrows_u8 = res->rows_u8[i];
	 rrows_s8 = reg->rows_s8[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_u8[j] = (U8) 
	     (bzerofl64 + bscalefl64 * (rrows_s8[j])); 
	 }
       }
       break;
     case TYPE_U8:
       for (i = 0; i < nrow; i++) {
	 resrows_u8 = res->rows_u8[i];
	 rrows_u8 = reg->rows_u8[i];
	 for (j = 0; j < ncol; j++) {
	   resrows_u8[j] = (U8) 
	     (bzerofl64 + bscalefl64 * (rrows_u8[j])); 
	     }
       }
       break;
     default:
       thError("%s: unsupported source region type", name);
       return(SH_GENERIC_ERROR);
       break;
     }
     break;
  default:
    thError("%s: unsupported traget region type", name);
    return(SH_GENERIC_ERROR);
  }
  
  return(SH_SUCCESS);

}

RET_CODE thCompressRegion(REGION *reg, PIXDATATYPE restype,
			  FL64 *bscale, FL64 *bzero) {

  /* this function finds the values of bscale and bzero such that
     a region (reg) with type region->type can be safely (with no overflow)
     represented in restype pixel type */

  char *name = "thCompressRegion";

  if (reg == NULL) {
    thError("%s: source region passed as null", name);
    return(SH_GENERIC_ERROR);
  }

  if (restype == reg->type) {
    *bscale = (THPIX) 1.0;
    *bzero = (THPIX) 0.0;
    return(SH_SUCCESS);
  }
      
  
  /*
  FL64 rmin_fl64, rmax_fl64;
  */
  FL32 rmin_fl32, rmax_fl32, *rrows_fl32;
  S32  rmin_s32, rmax_s32, *rrows_s32;
  U32  rmin_u32, rmax_u32, *rrows_u32;
  S16  rmin_s16, rmax_s16, *rrows_s16;
  U16  rmin_u16, rmax_u16, *rrows_u16;
  S8   rmin_s8, rmax_s8, *rrows_s8;
  U8   rmin_u8, rmax_u8, *rrows_u8;

  FL64 max_fl64, min_fl64, maxType, minType;
  int i, j, nrow, ncol;

  nrow = reg->nrow;
  ncol = reg->ncol;

  switch (reg->type){

    /*
  case TYPE_FL64:
    for (i = 0; i < nrow; i++) {
      rrows_fl64 = reg->rows_fl64[i];
      rmin_fl64 = rmax_fl64 = *rrows_fl64;
      for (j = 0; j < ncol; j++) {
	rmin_fl64 = MIN(rmin_fl64, rrows_fl64[j]); 
	rmax_fl64 = MAX(rmax_fl64, rrows_fl64[j]); 
      }
    }
    max_fl64 = (FL64) rmax_fl64;
    min_fl64 = (FL64) rmin_fl64;
    maxType = FL64_MAX;
    minType = FL64_MIN;
    break;
    */
  case TYPE_FL32:
    rmin_fl32 = rmax_fl32 = **(reg->rows_fl32);
    for (i = 0; i < nrow; i++) {
      rrows_fl32 = reg->rows_fl32[i];
      for (j = 0; j < ncol; j++) {
	rmin_fl32 = MIN(rmin_fl32, rrows_fl32[j]);
	rmax_fl32 = MAX(rmax_fl32, rrows_fl32[j]); 
      }
    }
    max_fl64 = (FL64) rmax_fl32;
    min_fl64 = (FL64) rmin_fl32;
    maxType = (FL64) FL32_MAX;
    minType = (FL64) FL32_MIN;
    break;
  case TYPE_S32:
    rmin_s32 = rmax_s32 = **(reg->rows_s32);
    for (i = 0; i < nrow; i++) {
      rrows_s32 = reg->rows_s32[i];
      for (j = 0; j < ncol; j++) {
	rmin_s32 = MIN(rmin_s32, rrows_s32[j]);
	rmax_s32 = MAX(rmax_s32, rrows_s32[j]); 
      }
    }
    max_fl64 = (FL64) rmax_s32;
    min_fl64 = (FL64) rmin_s32;
    maxType = (FL64) S32_MAX;
    minType = (FL64) S32_MIN;
    break;
  case TYPE_U32:
    rmin_u32 = rmax_u32 = **(reg->rows_u32);
    for (i = 0; i < nrow; i++) {
      rrows_u32 = reg->rows_u32[i];
      for (j = 0; j < ncol; j++) {
	rmin_u32 = MIN(rmin_u32, rrows_u32[j]);
	rmax_u32 = MAX(rmax_u32, rrows_u32[j]); 
      }
    }
    max_fl64 = (FL64) rmax_u32;
    min_fl64 = (FL64) rmin_u32;
    maxType = (FL64) U32_MAX;
    minType = (FL64) U32_MIN;
    break;
  case TYPE_S16:
    rmin_s16 = rmax_s16 = **(reg->rows_s16);
    for (i = 0; i < nrow; i++) {
      rrows_s16 = reg->rows_s16[i];
      for (j = 0; j < ncol; j++) {
	rmin_s16 = MIN(rmin_s16, rrows_s16[j]);
	rmax_s16 = MAX(rmax_s16, rrows_s16[j]); 
      }
    }
    max_fl64 = (FL64) rmax_s16;
    min_fl64 = (FL64) rmin_s16;
    maxType = S16_MAX;
    minType = S16_MIN;
    break;
  case TYPE_U16:
    rmin_u16 = rmax_u16 = **(reg->rows_u16);
    for (i = 0; i < nrow; i++) {
      rrows_u16 = reg->rows_u16[i];
      for (j = 0; j < ncol; j++) {
	rmin_u16 = MIN(rmin_u16, rrows_u16[j]);
	rmax_u16 = MAX(rmax_u16, rrows_u16[j]); 
      }
    }
    max_fl64 = (FL64) rmax_u16;
    min_fl64 = (FL64) rmin_u16;
    maxType = U16_MAX;
    minType = U16_MIN;
    break;
  case TYPE_S8:
    rmin_s8 = rmax_s8 = **(reg->rows_s8);
    for (i = 0; i < nrow; i++) {
      rrows_s8 = reg->rows_s8[i];
      for (j = 0; j < ncol; j++) {
	rmin_s8 = MIN(rmin_s8, rrows_s8[j]);
	rmax_s8 = MAX(rmax_s8, rrows_s8[j]); 
      }
    }
    max_fl64 = (FL64) rmax_s8;
    min_fl64 = (FL64) rmin_s8;
    maxType = S8_MAX;
    minType = S8_MIN;
    break;
  case TYPE_U8:
    rmin_u8 = rmax_u8 = **(reg->rows_u8);
    for (i = 0; i < nrow; i++) {
      rrows_u8 = reg->rows_u8[i];
      for (j = 0; j < ncol; j++) {
	rmin_u8 = MIN(rmin_u8, rrows_u8[j]);
	rmax_u8 = MAX(rmax_u8, rrows_u8[j]); 
      }
    }
    max_fl64 = (FL64) rmax_u8;
    min_fl64 = (FL64) rmin_u8;
    maxType = U8_MAX;
    minType = U8_MIN;
    break;
  default:
    thError("%s: unsupported source region type", name);
    return(SH_GENERIC_ERROR);
    break;
  }
  
  switch (restype){
    /*
  case TYPE_FL64:
    maxType = FL64_MAX;
    minType = FL64_MIN;
    break;
    */
  case TYPE_FL32:
    maxType = (FL64) FL32_MAX;
    minType = (FL64) FL32_MIN;
    break;
  case TYPE_S32:
    maxType = (FL64) S32_MAX;
    minType = (FL64) S32_MIN;
    break;
  case TYPE_U32:
    maxType = (FL64) U32_MAX;
    minType = (FL64) U32_MIN;
    break;
  case TYPE_S16:
    maxType = (FL64) S16_MAX;
    minType = (FL64) S16_MIN;
    break;
  case TYPE_U16:
    maxType = (FL64) U16_MAX;
    minType = (FL64) U16_MIN;
    break;
  case TYPE_S8:
    maxType = (FL64) S8_MAX;
    minType = (FL64) S8_MIN;
    break;
  case TYPE_U8:
    maxType = (FL64) U8_MAX;
    minType = (FL64) U8_MIN;
    break;
  default:
    thError("%s: unsupported source region type", name);
    return(SH_GENERIC_ERROR);
    break;
  }


  /* the following values produce the mapping
     min --> MIN
     max --> MAX
  */

  FL64 bzero_fl64, bscale_fl64;

  bzero_fl64 = (minType * (max_fl64 - min_fl64) 
	   - min_fl64 * (maxType - minType)) / 
    (max_fl64 - min_fl64);

  bscale_fl64 = (maxType - minType) / (max_fl64 - min_fl64);

  
  *bzero = (FL64) bzero_fl64;
  *bscale = (FL64) bscale_fl64;
    
  return(SH_SUCCESS);
}

RET_CODE thRegMinusReg(REGION *mmd, REGION *m, REGION *d) {
char *name = "thRegMinusReg";
if (mmd == NULL || m == NULL || d == NULL) {
	thError("%s: ERROR - improperly allocated (region)", name);
	return(SH_GENERIC_ERROR);
}
int nrow, ncol;
nrow = mmd->nrow;
ncol = mmd->ncol;

if (nrow != m->nrow || ncol != m->ncol) {
	thError("%s: ERROR - mismatching region (nrow, ncol): A-B (%d, %d), A (%d, %d)", name, nrow, ncol, m->nrow, m->ncol);
	return(SH_GENERIC_ERROR);
	}

if (nrow != d->nrow || ncol != d->ncol) {
	thError("%s: ERROR - mismatching region (nrow, ncol): A-B (%d, %d), B (%d, %d)", name, nrow, ncol, d->nrow, d->ncol);
	return(SH_GENERIC_ERROR);
	}

THPIX *mrows, *drows, *mmdrows;
int i, j;
for (i = 0; i < nrow; i++) {
	mrows = m->rows_thpix[i];
	drows = d->rows_thpix[i];
	mmdrows = mmd->rows_thpix[i];
	for (j = 0; j < ncol; j++) {
		mmdrows[j] = mrows[j] - drows[j];
	}
	}

return(SH_SUCCESS);
}

RET_CODE thRegPixCopy(REGION *regin, REGION *regout) {
char *name = "thRegPixCopy";

REGION *a, *b;
a = regin; 
b = regout;
if (a == NULL && b == NULL) {
	thError("%s: WARNING - null input and null output", name);
	return(SH_SUCCESS);
}

if (a == NULL || b == NULL) {
	thError("%s: ERROR - null input or null output", name);
	return(SH_GENERIC_ERROR);
}

if (a->type != TYPE_THPIX || b->type != TYPE_THPIX) {
	thError("%s: ERROR - only (TYPE_THPIX) pixel type is supported", name);
	return(SH_GENERIC_ERROR);
}       

int anrow, ancol, bnrow, bncol;
anrow = a->nrow;
ancol = a->ncol;
bnrow = b->nrow;
bncol = b->ncol;

int arow0, acol0, brow0, bcol0, abrow0, abcol0;
arow0 = a->row0;
acol0 = a->col0;
brow0 = b->row0;
bcol0 = b->col0;
abrow0 = arow0 - brow0;
abcol0 = acol0 - bcol0;

int i1, i2, j1, j2;
i1 = MAX(0, - abrow0);
i2 = MIN(anrow, bnrow - abrow0);
j1 = MAX(0, - abcol0);
j2 = MIN(ancol, bncol - abcol0);

if (i2 >= i1 && j2 >= j1) {
THPIX *arow, *brow;
int i;
size_t l;
l = (j2 - j1 + 1) * sizeof(THPIX);
for (i = i1; i < i2; i++) {
	arow = a->rows_thpix[i] + j1;
	brow = b->rows_thpix[i + abrow0] + j1 + abcol0;	
	memcpy(brow, arow, l);
}
}

return(SH_SUCCESS);
}

RET_CODE shRegReshape(REGION *reg, int nrow, int ncol) {
char *name = "shRegReshape";
if (reg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (nrow < 0 || ncol < 0) {
	thError("%s: ERROR - (nrow, ncol) = (%d, %d) not supported", name, nrow, ncol);
	return(SH_GENERIC_ERROR);
}
if (nrow == 0 || ncol == 0) {
	nrow = 0;
	ncol = 0;
}
if (reg->nrow == nrow && reg->ncol == ncol) {
	#if VERBOSE_REGION
	thError("%s: WARNING - (reg = %dx%d) doesn't need reshaping", name, nrow, ncol);
	#endif
	return(SH_SUCCESS);
}

TYPE t = reg->type;
p_shRegRowsFree(reg);
p_shRegVectorFree(reg);

if (nrow == 0 || ncol == 0) {
	reg->nrow = 0;
	reg->ncol = 0;
	return(SH_SUCCESS);
}

if (p_shRegVectorGet(reg, nrow, t) == (int) NULL) {
	thError("%s: ERROR - could not allocate (row) addresses in (reg)", name);
	return(SH_GENERIC_ERROR);
}	
if (p_shRegRowsGet(reg, nrow, ncol, t) == (int) NULL) {
	thError("%s: ERROR - could not allocate (rows) in (reg)", name);
	return(SH_GENERIC_ERROR);
}

reg->nrow = nrow;
reg->ncol = ncol;
return(SH_SUCCESS);
}

