/*
 * Read fpC images (infile), and copy its content to Outfile.
 */
/* PHOTO libraries */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dervish.h"
#include "strings.h"

#include "phPhotoFitsIO.h"
#include "phSpanUtil.h"
#include "phUtils.h"
#include "phObjc.h"
#include "phDataIo.h"
#include "prvt/region_p.h"


#include "sohoEnv.h"
#include "thDebug.h"
#include "thObjmaskIo.h"

/*
 * some symbols to prevent dervish.o from being loaded from libatlas.a
 * if we are also linking against real dervish
 */

int verbose = 0;

static void usage(void);




int
main(int ac, char *av[])
{
  /* Arguments */
  char *infile, *outfile;		/* input and output filenames */
  int row0, column0, row1, column1;	/* cornders of the image in region */

  int p, *planes, *allplanes;

  allplanes = thMalloc(NMASK_TYPES * sizeof(int));
  planes = thMalloc(NMASK_TYPES * sizeof(int));

  for (p = 0; p < NMASK_TYPES; p++) {
    planes[p] = 0;
    allplanes[p] = 1;
  }


  while(ac > 1 && (av[1][0] == '-' || av[1][0] == '+')) {
    switch (av[1][1]) {
    case '?':
    case 'h':
      usage();
      exit(0);
      break;
    case 'v':
      verbose++;
      break;
    case 'a':
      thFree(planes);
      planes = allplanes;
      break;
    case '0':
      planes[0] = 1;
      break;
    case '1':
      planes[1] = 1;
      break;
    case '2':
      planes[2] = 1;
      break;
    case '3':
      planes[3] = 1;
      break;
    case '4':
      planes[4] = 1;
      break;
    case '5':
      planes[5] = 1;
      break;
    case '6':
      planes[6] = 1;
      break;
    case '7':
      planes[7] = 1;
      break;
    case '8':
      planes[8] = 1;
      break;
    case '9':
      planes[9] = 1;
      break;
    default:
      shError("Unknown option %s\n",av[1]);
      break;
    }
    ac--;
    av++;
  }
  if(ac <= 6) {
    shError("You must specify an input file, an output file, a row, and a column \n");
    exit(1);
  }
  infile = av[1]; outfile = av[2]; 
  row0 = atoi(av[3]); column0 = atoi(av[4]); 
  row1 = atoi(av[5]); column1 = atoi(av[6]);
  
  
  RET_CODE thstatus;
  thstatus = thCpFpm(infile, outfile, row0, column0, row1, column1, planes);
 
  /* thow the error messages stores in the stack during the run */
  thErrShowStack();
  
  if (thstatus != SH_SUCCESS) {
    exit(1);
  }
  
  /* successfull run */
  return(0);
}

/*****************************************************************************/

static void
usage(void)
{
   char **line;

   static char *msg[] = {
      "Usage: read_atlas_image [options] input-file output-file r0 c0 r1 c1",
      "Your options are:",
      "       -?      This message",
      "       -b #    Set background level to #",
      "       -c #    Use colour # (0..ncolor-1; default 0)",
      "       -h      This message",
      "       -i      Print an ID string and exit",
      "       -v      Turn up verbosity (repeat flag for more chatter)",
      "       -a      Image all mask planes (each described below)",
      "       -num    Image mask plane num (0..9)",
      "       0:      INTERPOLATED ",
      "       1:      SATURATED ",
      "       2:      NOTCHECKED", 
      "       3:      OBJECT",
      "       4:      BRIGHTOBJECT",
      "       5:      BINOBJECT",
      "       6:      CATOBJECT",
      "       7:      SUBTRACTED",
      "       8:      GHOST",
      "       9:      CR",
      NULL
   };

   for(line = msg;*line != NULL;line++) {
      fprintf(stderr,"%s\n",*line);
   }
}

