function write_condorshell {
local name="write_condorshell"

local Options=$@
local Optnum=$#

local ldatadir=no
local ldatadir2=no
local lfield=no
local lpsffield=no
local lrun=no
local lcamcol=no
local lmleparfile=no
local loutdir=no
local lobjcdir=no
local lcomment=no
local copypsf=no
local simulator=no
local linobjcdir=no
local lincalibobjcdir=no
local linskydir=no
local loutskydir=no
local loutheasacdir=no
local lexecutable=no
local lphotofile=no
local largument=no
local lfpCDir=no
local lfpCCDir=no
local zipfpC=no
local zipfpCC=no

local datadir=empty
local field=empty
local psffield=empty
local run=empty
local camcol=empty
local mleparfile=empty
local outdir=empty
local comment=empty
local inobjcdir=empty
local inskydir=empty
local outskydir=empty
local heasacdir=empty
local executable=empty
local photofile=empty
local argument=empty
local fpCDir=empty
local fpCCDir=empty

while getopts ':-:' OPTION ; do
  case "$OPTION" in
    -  ) [ $OPTIND -ge 1 ] && optind=$(expr $OPTIND - 1 ) || optind=$OPTIND
         eval OPTION="\$$optind"
         OPTARG=$(echo $OPTION | cut -d'=' -f2)
         OPTION=$(echo $OPTION | cut -d'=' -f1)
         case $OPTION in
             --datadir   ) ldatadir=yes; datadir="$OPTARG"     ;;
	     --datadir2  ) ldatadir2=yes; datadir2="$OPTARG"	;;
	     --field	 ) lfield=yes; field="$OPTARG"   ;;
	     --field0    ) lfield0=yes; field0="$OPTARG" ;;
	     --psffield  ) lpsffield=yes; psffield="$OPTARG"     ;;
	     --run       ) lrun=yes; run="$OPTARG"	 ;;
	     --camcol    ) lcamcol=yes; camcol="$OPTARG" ;;
	     --output    ) lmleparfile=yes; mleparfile="$OPTARG" 	;;
	     --outdir    ) loutdir=yes; outdir="$OPTARG"	;;
	     --objcdir   ) lobjcdir=yes; objcdir="$OPTARG"	;; 
	     --copypsf   ) copypsf=yes				;;
	     --comment   ) lcomment=yes; comment="$OPTARG"	;;
	     --simulator ) simulator=yes				;;
	     --sdsssimulator ) sdsssimulator=yes		;;
	     --inobjcdir ) linobjcdir=yes; inobjcdir="$OPTARG"	;; #for sdss based simulation
	     --incalibobjcdir ) lincalibobjcdir=yes; incalibobjcdir="$OPTARG" ;; # for sdss based simulation
	     --inskydir  ) linskydir=yes; inskydir="$OPTARG"	;; #for sdss based simulation
		--outskydir    ) loutskydir=yes; outskydir="$OPTARG"	;; #for outputinng simulation parameter
		--outheasacdir ) loutheasacdir=yes; outheasacdir="$OPTARG"	;; #for outputting photoObj files after changing the header to BITPIX = 8
		--executable ) lexecutable=yes; executable="$OPTARG"	;;
		--photofile  ) lphotofile=yes; photofile="$OPTARG"	;;
		--argument   ) largument=yes; argument="$OPTARG"	;;
		--shellfile  ) lshellfile=yes; shellfile="$OPTARG"	;;
		--zipfpCC    ) zipfpCC=yes	;;
		--zipfpC     ) zipfpC=yes	;;	
		--fpCCDir    ) lfpCCDir=yes; fpCCDir="$OPTARG"	;;
		--fpCDir     ) lfpCDir=yes; fpCDir="$OPTARG"	;;
	     * )  echo "$name: invalide options (long)" ;;
         esac
       OPTIND=1
       shift
      ;;
    ? )  echo "$name: invalid options (short) "  ;;
  esac
done

#echo "$name: root = $root, field = $field, field0 = $field0, run = $run, camcol = $camcol, band = $band, output = $mleparfile"

if [ $lfield == no ] || [ $lrun == no ] || [ $lcamcol == no ] || [ $lexecutable == no ] || [ $largument == no ]; then
	echo "$name: not enough arguments set"
	exit
fi
if [ $field == empty ] || [ $run == empty ] || [ $camcol == empty ]; then
	echo "$name: some arguments are invoked but not set"
	exit
fi
if [ $lobjcdir == no ]; then
	objcdir="$outdir/objc"
fi
if [ $linobjcdir == no ]; then 
	inobjcdir="$datadir"
fi
if [ $linskydir == no ]; then
	inskydir="$datadir"
fi
if [ $loutskydir == no ]; then
	outskydir="$PHOTO_TEMP"
fi
if [ $lfpCDir == no ]; then
	fpCdir="$outdir/photo"	
fi
if [ $lfpCCDir == no ]; then
	fpCCDir="$outdir/photo"
fi
if [ $lincalibobjcdir == no ]; then
	incalibobjcdir="$CPHOTO_OBJCS/$DEFAULT_SDSS_RERUN/$run/$camcol"
fi
if [ $ldatadir2 == no ]; then
	datadir2=$incalibobjcdir
fi
if [ $loutheasacdir == no ]; then
	outheasacdir="$outskydir"
fi
	
local fpCprefix="fpC"
local fpCCprefix="fpCC"
local psFieldprefix="psField"
local fpObjcprefix="fpObjc"
local fpBINprefix="fpBIN"
local gpObjcprefix="gpObjc"
local lMprefix="lM"
local psfRprefix="psfR"
local psfSprefix="psfS"
local photoObjcprefix="photoObj"

runstr=$(( 10#$run ))
field=$(( 10#$field ))

runstr=`printf "%06d" $run`
fieldstr=`printf "%04d" $field`

if [[ $simulator == yes ]]; then 
	skyobjc_datadir="$source_datadir"
	skyobjc_datadir2="$source_datadir2"
else
	skyobjc_datadir="$datadir"
	skyobjc_datadir2="$datadir2"
fi


local band="u"
local fpBIN_ufile="fpBIN-$runstr-$band$camcol-$fieldstr.fit"
local fpC_ufile="fpC-$runstr-$band$camcol-$fieldstr.fit"
local fpCC_ufile="fpCC-$runstr-$band$camcol-$fieldstr.fit"
band="g"
local fpBIN_gfile="fpBIN-$runstr-$band$camcol-$fieldstr.fit"
local fpC_gfile="fpC-$runstr-$band$camcol-$fieldstr.fit"
local fpCC_gfile="fpCC-$runstr-$band$camcol-$fieldstr.fit"
band="r"
local fpBIN_rfile="fpBIN-$runstr-$band$camcol-$fieldstr.fit"
local fpC_rfile="fpC-$runstr-$band$camcol-$fieldstr.fit"
local fpCC_rfile="fpCC-$runstr-$band$camcol-$fieldstr.fit"
band="i"
local fpBIN_ifile="fpBIN-$runstr-$band$camcol-$fieldstr.fit"
local fpC_ifile="fpC-$runstr-$band$camcol-$fieldstr.fit"
local fpCC_ifile="fpCC-$runstr-$band$camcol-$fieldstr.fit"
band="z"
local fpBIN_zfile="fpBIN-$runstr-$band$camcol-$fieldstr.fit"
local fpC_zfile="fpC-$runstr-$band$camcol-$fieldstr.fit"
local fpCC_zfile="fpCC-$runstr-$band$camcol-$fieldstr.fit"

local photoObjc_file="photoObj-$runstr-$camcol-$fieldstr.fits"
local heasac_modification_file="$outheasacdir/heasac-mod.txt"

if [ ! -e $heasac_modification_file ]; then

local heasac_modification_string=empty
read -r -d '' heasac_modification_string <<-END_PROC_HEASAC
BITPIX=8 /
END_PROC_HEASAC
echo -e "${heasac_modification_string}" >$heasac_modification_file

fi

modify=/u/khosrow/thesis/opt/soho/modify.py

local condor_shell_contents=empty
read -r -d '' condor_shell_contents <<-END_PROC_SHELL

#!/bin/bash
#$comment
name=$shellfile
uzipfile=$inskydir/$fpBIN_ufile.gz
gzipfile=$inskydir/$fpBIN_gfile.gz
rzipfile=$inskydir/$fpBIN_rfile.gz
izipfile=$inskydir/$fpBIN_ifile.gz
zzipfile=$inskydir/$fpBIN_zfile.gz

ufile=$outskydir/$fpBIN_ufile
gfile=$outskydir/$fpBIN_gfile
rfile=$outskydir/$fpBIN_rfile
ifile=$outskydir/$fpBIN_ifile
zfile=$outskydir/$fpBIN_zfile

declare -a zipfiles=(\$uzipfile \$gzipfile \$rzipfile \$izipfile \$zzipfile)
declare -a outfiles=(\$ufile \$gfile \$rfile \$ifile \$zfile)
declare -a inflated=(no no no no no)
declare -a existed=(no no no no no)
declare -a deleted_unzipped=(no no no no no)
declare -a zipped_fpC=(no no no no no)
declare -a zipped_fpCC=(no no no no no)

in_photoobjcfile=$datadir2/$photoObjc_file
in_incalibobjcfile=$incalibobjcdir/$photoObjc_file
out_photoobjcfile=$outheasacdir/$photoObjc_file
out_incalibobjcfile=$outheasacdir/$photoObjc_file

declare -a inheasac_files=(\$in_photoobjcfile \$in_incalibobjcfile)
declare -a outheasac_files=(\$out_photoobjcfile \$out_incalibobjcfile)
declare -a existed_heasac=(no no)
declare -a deleted_heasac=(no no)

nheasac=2

ufpCfile=$fpCDir/$fpC_ufile
gfpCfile=$fpCDir/$fpC_gfile
rfpCfile=$fpCDir/$fpC_rfile
ifpCfile=$fpCDir/$fpC_ifile
zfpCfile=$fpCDir/$fpC_zfile

ufpCCfile=$fpCCDir/$fpCC_ufile
gfpCCfile=$fpCCDir/$fpCC_gfile
rfpCCfile=$fpCCDir/$fpCC_rfile
ifpCCfile=$fpCCDir/$fpCC_ifile
zfpCCfile=$fpCCDir/$fpCC_zfile

declare -a fpCfiles=(\$ufpCfile \$gfpCfile \$rfpCfile \$ifpCfile \$zfpCfile)
declare -a fpCCfiles=(\$ufpCCfile \$gfpCCfile \$rfpCCfile \$ifpCCfile \$zfpCCfile)

nband=5

echo "\$name: evaluating if files needs to be expanded"

for (( i=0; i<\$nband; i++ ))
do

	zipfile=\${zipfiles[\$i]}
	destination=\${outfiles[\$i]}

	if [ -e \$destination ]; then 
		existed[\$i]=yes
		rm -f \$destination
	fi
	if [[ ! -e \$destination  && -e \$zipfile ]]; then
		gunzip < \$zipfile > \$destination
		inflated[\$i]=yes
	fi

done

echo "\$name: inflated status: "
echo "\$name: \${inflated[*]}"


echo "\$name: evaluating if fits file headers need to be modified"


for (( i=0; i<\$nheasac; i++ ))
do
	source=\${inheasac_files[\$i]}
	destination=\${outheasac_files[\$i]}

	if [ -e \$destination ]; then 
		existed_heasac[\$i]=yes
		rm -f \$destination
	fi

done

for (( i=0; i<\$nheasac; i++ ))
do

	source=\${inheasac_files[\$i]}
	destination=\${outheasac_files[\$i]}

	if [[ -e \$source && ! -e \$destination ]]; then
		$modify \$source \$destination
		#cp \$source  \$destination
		#fmodhead \$destination\[0\] $heasac_modification_file
		#fthedit \$destination\[1\] @/u/khosrow/thesis/opt/soho/heasac-mod-1.txt 
		processed_heasac[\$i]=yes
	fi

done


echo "\$name: processed status: "
echo "\$name: \${processed_heasac[*]}"


echo "\$name: executing the binary"
echo "\$name: $executable $argument"

$executable $argument

echo "\$name: deleting heasarc processed files"

for (( i=0; i<\$nheasac; i++ ))
do
	destination=\${outheasac_files[\$i]}
	if [ -e \$destination ]; then 
		deleted_heasac[\$i]=yes
		rm -f \$destination
	fi

done


echo "\$name: deletion status: "
echo "\$name: \${deleted_heasac[*]}"

echo "\$name: deleting unzipped files"

for (( i=0; i<\$nband; i++ ))
do

	zipfile=\${zipfiles[\$i]}
	destination=\${outfiles[\$i]}

	if [[ \${existed[\$i]} == no && \${inflated[\$i]} == yes ]]; then
		deleted_unzipped[\$i]=yes	
		rm -f \$destination
	fi

done

echo "\$name: deletion status: "
echo "\$name: \${deleted_unzipped[*]}"


echo "\$name: zipping flat field files if requested"

for (( i=0; i<\$nband; i++ ))
do

	fpCfile=\${fpCfiles[\$i]}
	fpCCfile=\${fpCCfiles[\$i]}
	
	if [[ $zipfpC == yes && -e \$fpCfile ]]; then
		zipped_fpC[\$i]=yes	
		gzip -f \$fpCfile
	fi
	if [[ $zipfpCC == yes && -e \$fpCCfile ]]; then
		zipped_fpCC[\$i]=yes
		gzip -f \$fpCCfile
	fi

done

echo "\$name: zipping status (fpC): "
echo "\$name: \${zipped_fpC[*]}"

echo "\$name: zipping status (fpCC): "
echo "\$name: \${zipped_fpCC[*]}"

eval "photo -file $photofile"

#end of condor shell script

END_PROC_SHELL

echo -e "${condor_shell_contents}" >$shellfile
}





