function fetch_sky_goodness, run =  run,  rerun =  rerun,  $
                             camcol =  camcol, field = field, $
                             band =  band,  directory =  directory

if ((n_elements(run) eq 0) || (n_elements(rerun) eq 0) || $
    (n_elements(camcol) eq 0) || (n_elements(field) eq 0)  || $
    (n_elements(band) eq 0) || (n_elements(directory) eq 0)) then begin
    print, "* error: too few arguments supplied"
    return,  -1

endif else begin

if (n_elements(run) gt 1) then begin
    for i = 0,  n_elements(run) - 1,  1 do begin
        thisgoodness =  fetch_sky_goodness(run =  run[i],  rerun =  rerun,  $
                                           camcol =  camcol, field = field, $
                                           band =  band,  directory =  directory)
        if (n_elements(goodness) eq 0) then begin
            goodness =  [thisgoodness]
        endif else begin
            goodness =  [goodness,  thisgoodness]
        endelse
    endfor
    return,  goodness

endif else begin


if (n_elements(rerun) gt 1) then begin
    for i = 0,  n_elements(rerun) - 1,  1 do begin
        thisgoodness =  fetch_sky_goodness(run =  run,  rerun =  rerun[i],  $
                                           camcol =  camcol, field = field, $
                                           band =  band,  directory =  directory)
        if (n_elements(goodness) eq 0) then begin
            goodness =  [thisgoodness]
        endif else begin
            goodness =  [goodness,  thisgoodness]
        endelse
    endfor
    return,  goodness
endif else begin

if (n_elements(camcol) gt 1) then begin
    for i = 0,  n_elements(camcol) - 1,  1 do begin
        thisgoodness =  fetch_sky_goodness(run =  run,  rerun =  rerun,  $
                                           camcol =  camcol[i], field = field, $
                                           band =  band,  directory =  directory)
        if (n_elements(goodness) eq 0) then begin
            goodness =  [thisgoodness]
        endif else begin
            goodness =  [goodness,  thisgoodness]
        endelse
    endfor
    return,  goodness
endif else begin

if (n_elements(field) gt 1) then begin
    for i = 0,  n_elements(field) - 1,  1 do begin
        thisgoodness =  fetch_sky_goodness(run =  run,  rerun =  rerun,  $
                                           camcol =  camcol, field = field[i], $
                                           band =  band,  directory =  directory)
        if (n_elements(goodness) eq 0) then begin
            goodness =  [thisgoodness]
        endif else begin
            goodness =  [goodness,  thisgoodness]
        endelse
    endfor
    return,  goodness
endif else begin

if (n_elements(band) gt 1) then begin
    for i = 0,  n_elements(band) - 1,  1 do begin
        thisgoodness =  fetch_sky_goodness(run =  run,  rerun =  rerun,  $
                                           camcol =  camcol, field = field, $
                                           band =  band[i],  directory =  directory)
        if (n_elements(goodness) eq 0) then begin
            goodness =  [thisgoodness]
        endif else begin
            goodness =  [goodness,  thisgoodness]
        endelse
    endfor
    return,  goodness
endif 

endelse
endelse
endelse
endelse
endelse

runstr =  STRING(run,  format =  '(I06)')
fieldstr =  STRING(field,  format =  '(I04)')


filename =  directory + '/' + strtrim(run,  2) + '/' $
  + rerun + '/objcs/' + strtrim(camcol,  2) + '/' + $
  '/gpSKY-' + $
  runstr + '-' + band + strtrim(camcol,  2) + '-' + $
  fieldstr + '.fit'

x = {run: -1L,  rerun: "",  camcol: -1L,  field: -1L,  band: "unknown",  $
    loglikelihood: 0.0D0, npixel: -1L, bsize: 0L}

filesearch =  filename + '*'


filepath = file_search(filesearch,  count =  count)

if (count eq 0) then begin
    goodness =  x
    return,  goodness
endif

filename =  filepath[0]

sm = mrdfits(filename,  1,  hdr,  row =  0,  /silent)

tagnames =  tag_names(sm)

logl1 =  where(tagnames eq "LOGL")
logl2 =  where(tagnames eq "LOGLIKELIHOOD")

if (logl1[0] eq -1 and logl2[0] eq -1) then begin
    print,  "* file = ",  filename
    print,  "* the imported structure does not contain goodness measures "
    goodness =  x
    return,  goodness
end

bsize1 =  where(tagnames eq "BSIZE")
if (bsize1[0] eq -1) then begin
    print,  "* file = ",  filename
    print,  "* the imported structure does not contain number of basis functions"
    goodness =  x
    return,  goodness
endif

npixel1 =  where(tagnames eq "NPIXEL")
if (npixel1[0] eq -1) then begin
    ;print,  "* file = ",  filename
    ;print,  "* the imported structure does not contain number of pixels"
    ;goodness =  x
    ;return,  goodness
endif else begin
    x.npixel =  sm.npixel
endelse

x.run = run
x.rerun =  rerun
x.camcol =  camcol
x.field =  field
x.band =  band
if (logl1[0] eq -1) then begin 
    x.loglikelihood =  sm.loglikelihood
endif else begin
    x.loglikelihood =  sm.logl
endelse
x.bsize =  sm.bsize

goodness =  x

return,  goodness

END

pro extract_fit_goodness,  goodness =  goodness

run =  goodness[*].run
index =  where(run ne -1L)
goodness =  goodness[index]

return
END



function compare_sky_fits,  xr =  xr,  xc =  xc
 
if ((n_elements(xr) eq 0) || (n_elements(xc) eq 0)) then begin
    print,  "* both XC and XR should be passed"
    return,  -1
endif

if (n_elements(xr) ne n_elements(xc)) then begin
    print,  "* XC and XR should have the same size"
    return,  -1
endif

if (n_elements(xr) gt 1) then begin
    for i =  0,  n_elements(xr) - 1,  1 do begin
        thisxr =  xr[i]
        thisxc =  xc[i]
        thischisq =  compare_sky_fits(xr =  thisxr,  xc =  thisxc)
        
        if (n_elements(chisq) eq 0) then begin
            chisq =  [thischisq]
        endif else begin
            chisq =  [chisq,  thischisq]
        endelse

    endfor

    return,  chisq
endif

chisq =  {nu: -1L,  X2: 0.0D0}

if ((xr.run eq -1) || (xc.run eq -1)) then begin
    return,  chisq
endif

if ((xr.npixel ne -1L) and (xc.npixel ne -1L)) then begin
 
    if (xr.npixel ne xc.npixel) then begin
        print,  "* mismatching number of data points between xc, xr: ",  $
                xc.npixel,  xr.npixel
        return, chisq
    endif
    npixel =  xr.npixel

endif else begin
    if (xr.npixel ne -1L) then begin
        npixel =  xr.npixel
    endif else begin
        if (xc.npixel ne -1L) then begin
            npixel = xc.npixel
        endif else begin
            return,  chisq
        endelse
    endelse
endelse

chisq.nu =  xc.bsize - xr.bsize
chisq.X2 =  (xr.loglikelihood - xc.loglikelihood) / (xc.loglikelihood / (npixel - xc.bsize - 1))

return,  chisq

END

pro extract_sky_chisq,  chisq =  chisq

nu =  chisq[*].nu

index =  where(nu ne -1)

if (index[0] eq -1) then begin
    print,  "* no correct value of chisq was found"
    return
endif

help,  index
chisq =  chisq[index]

return
END

function fetch_sky_fit,  run = run,  rerun =  rerun,  camcol =  camcol,  $
                         band =  band,  field =  field,  directory =  directory,  $
                         coeffs = coeffs,  invcov =  invcov,  cov =  cov

if ((n_elements(run) eq 0) || (n_elements(rerun) eq 0) || $
    (n_elements(camcol) eq 0) || (n_elements(field) eq 0) || $
    (n_elements(band) eq 0)) then begin
    print,  "* not enough information passed to fetch sky fit"
    return,  -1
endif

if (n_elements(run) ne 1) then begin
    for i =  0, n_elements(run) - 1,  1 do begin
        thisfit =  fetch_sky_fit(run = run[i],  rerun =  rerun,  $
                                 camcol =  camcol,  band =  band,  $
                                 field =  field,  directory =  directory,  $
                                 coeffs = keyword_set(coeffs),  $
                                 invcov =  keyword_set(invcov),  $
                                 cov =  keyword_set(cov))
        if (n_elements(fit) eq 0) then begin
            fit =  [thisfit] 
        endif else begin
            fit =  [fit,  thisfit]
        endelse
    endfor
    return,  fit
endif else begin
    if (n_elements(rerun) ne 1) then begin
        for i =  0, n_elements(rerun) - 1,  1 do begin
            thisfit =  fetch_sky_fit(run = run,  rerun =  rerun[i],  $
                                     camcol =  camcol,  band =  band,  $
                                     field =  field,  directory =  directory,  $
                                     coeffs = keyword_set(coeffs),  $
                                     invcov =  keyword_set(invcov),  $
                                     cov =  keyword_set(cov))
            if (n_elements(fit) eq 0) then begin
                fit =  [thisfit] 
            endif else begin
                fit =  [fit,  thisfit]
            endelse
        endfor
        return,  fit
    endif else begin
        if (n_elements(camcol) ne 1) then begin
            for i =  0, n_elements(camcol) - 1,  1 do begin
                thisfit =  fetch_sky_fit(run = run,  rerun =  rerun,  $
                                         camcol =  camcol[i],  band =  band,  $
                                         field =  field,  directory =  directory,  $
                                         coeffs = keyword_set(coeffs),  $
                                         invcov =  keyword_set(invcov),  $
                                         cov =  keyword_set(cov))
                if (n_elements(fit) eq 0) then begin
                    fit =  [thisfit] 
                endif else begin
                    fit =  [fit,  thisfit]
                endelse
            endfor
            return,  fit
        endif else begin
            if (n_elements(band) ne 1) then begin
                for i =  0, n_elements(band) - 1,  1 do begin
                    thisfit =  fetch_sky_fit(run = run,  rerun =  rerun,  $
                                             camcol =  camcol,  band =  band[i],  $
                                             field =  field,  directory =  directory,  $
                                             coeffs = keyword_set(coeffs),  $
                                             invcov =  keyword_set(invcov),  $
                                             cov =  keyword_set(cov))
                    if (n_elements(fit) eq 0) then begin
                        fit =  [thisfit] 
                    endif else begin
                        fit =  [fit,  thisfit]
                    endelse
                endfor
                return,  fit
            endif else begin
                if (n_elements(field) ne 1) then begin
                    for i =  0, n_elements(field) - 1,  1 do begin
                        thisfit =  fetch_sky_fit(run = run,  rerun =  rerun,  $
                                                 camcol =  camcol,  band =  band,  $
                                                 field =  field[i],  directory =  directory,  $
                                                 coeffs = keyword_set(coeffs),  $
                                                 invcov =  keyword_set(invcov),  $
                                                 cov =  keyword_set(cov))
                        if (n_elements(fit) eq 0) then begin
                            fit =  [thisfit] 
                        endif else begin
                            fit =  [fit,  thisfit]
                        endelse
                    endfor
                    return,  fit
                endif
            endelse
        endelse
    endelse
endelse

filename =  fetch_sky_filename(run =  run,  rerun =  rerun,  $
                               camcol =  camcol, band =  band, $
                               field =  field,  directory = directory)

fit =  fetch_sky_fit_structure(coeffs = keyword_set(coeffs),  $
                             cov = keyword_set(cov),  $
                             invcov = keyword_set(invcov))

if (filename eq "") then begin
    return,  fit
endif

x = mrdfits(filename,  1,  /silent)

fit.run = run
fit.rerun = rerun
fit.field = field
fit.camcol = camcol
fit.band = band

if (keyword_set(coeffs) and  tag_test(x, "C") and tag_test(fit,  "C")) then begin
    fit.c =  ptr_new(x.c)
endif

if (keyword_set(cov) and tag_test(x,  "COV") and tag_test(fit,  "COV")) then begin
    fit.cov =  ptr_new(x.cov)
endif

if (keyword_set(invcov) and tag_test(x,  "INVCOV") and tag_test(fit,  "INVCOV")) then begin
    fit.invcov =  ptr_new(x.invcov)
endif

if (tag_test(x,  "BSIZE") and tag_test(fit,  "BSIZE")) then begin
    fit.bsize =  x.bsize
    if (tag_test(fit,  "NAMPL")) then begin
        bsize =  fit.bsize
        n1 =  NINT(sqrt(bsize - 1))
        n2 =  NINT(sqrt(bsize))
        if (n1 ne n2) then begin
            fit.nampl =  1
        endif else begin
            fit.nampl =  2
        endelse
    endif
endif

if (tag_test(x,  "NPIXEL") and tag_test(fit,  "NPIXEL")) then begin
    fit.npixel =  x.npixel
endif

if (tag_test(x,  "LOGLIKELIHOOD") and tag_test(fit,  "LOGLIKELIHOOD")) then begin
    fit.loglikelihood =  x.loglikelihood
endif

if (tag_test(x,  "LOGLIKELIHOOD") and tag_test(fit,  "LOGL")) then begin
    fit.logl =  x.loglikelihood
endif

if (tag_test(x,  "LOGL") and tag_test(fit,  "LOGLIKELIHOOD")) then begin
    fit.loglikelihood =  x.logl
endif

if (tag_test(x,  "LOGL") and tag_test(fit,  "LOGL")) then begin
    fit.logl =  x.logl
endif

;help,  fit,  /stru

return,  fit

END

function get_sky_matrices,  fit =  fit,  $
                            ir =  ir,  ic = ic,  $
                            ampl1 =  ampl1,  ampl2 =  ampl2

if (n_elements(fit) eq 0) then begin
    print,  "* too few arguments, fit structures cannot be empty"
    return,  -1
endif

if (n_elements(ir) ne n_elements(ic)) then begin
    print,  "* mistmatching number of row and column indices"
    return,  -1
endif

if (n_elements(fit) gt 1) then begin
    for i = 0,  n_elements(fit) - 1,  1 do begin
        thisskym = get_sky_matrices(fit =  fit[i],  $
                                    ir =  ir,  ic = ic,  $
                                    ampl1 =  keyword_set(ampl1),  ampl2 =  keyword_set(ampl2))
        if (n_elements(skym) eq 0) then begin
            skym =  [thisskym] 
        endif else begin
            skym =  [skym,  thisskym]
        endelse
    endfor
    return,  skym
endif

nr =  NINT(sqrt(fit.bsize))
count =  0

if (n_elements(ir) ne 0) then begin
    condition = ((ir[*] ne 0) + (ic[*] ne 0)) * $
                    (ir[*] lt nr) * $
                    (ic[*] lt nr) * $
                    (ir[*] ge 0)  * $
                    (ic[*] ge 0)

    index =  where(condition,  count)
endif

if ((count eq 0) and (not keyword_set(ampl1)) and (not keyword_set(ampl2))) then begin
    print,  "* too few arguments, at list amplifier delta functions should be read"
    return,  -1
endif

if (keyword_set(ampl1)) then wi = [0]
if (keyword_set(ampl2) and (fit.nampl gt 1)) then begin
    if n_elements(wi) eq 0 then begin
        wi =  [1]
    endif else begin
        wi = [wi, 1]
    endelse
endif

nampl =  fit.nampl

;print,  "* nample = ",  nampl

for i = 0, count - 1, 1 do begin
    wi1 = nampl - 1L + nr * ic[index[i]] + ir[index[i]]
    if n_elements(wi) eq 0 then begin
        wi =  [wi1]
    endif else begin
        wi = [wi, wi1]
    endelse
endfor

wc =  n_elements(wi)

c = replicate(0.0D0,  wc)
cov =  replicate(0.0D0,  wc, wc)
invcov =  replicate(0.0D0,  wc,  wc)

for i =  0,  wc - 1,  1 do begin
    c[i] = (*(fit.c))[wi[i]]
    for j =  0,  wc -1,  1 do begin
        cov[i,  j] =  (*(fit.cov))[wi[i],  wi[j]]
        invcov[i,  j] =  (*(fit.invcov))[wi[i],  wi[j]]
    endfor
endfor


skyms =  {run: fit.run,  rerun: fit.rerun, $
          camcol: fit.camcol,  field: fit.field, $
          band: fit.band, $
          c: c,  cov: cov,  invcov: invcov}


return,  skyms
END

function compare_skyfit_chisq,  fit1,  fit2

if (n_elements(fit1) ne n_elements(fit2)) then begin
    print,  "* mismatching size of fit1 and fit2 data structures"
    return,  -1
endif

if (n_elements(fit1) eq 0) then begin
    print,  "* too few arguments passed"
    return,  -1
endif

if (n_elements(fit1) gt 1) then begin
    for i =  0,  n_elements(fit1) - 1,  1 do begin
        thischisq = compare_skyfit_chisq(fit1[i],  fit2[i])
        if n_elements(chisq) eq 0 then begin
            chisq = [thischisq]
        endif else begin
            chisq =  [chisq,  thischisq]
        endelse
    endfor
    return,  chisq
endif

nw =  n_elements(fit1.c)

cov12 =  invert((fit1.invcov + fit2.invcov))

invcov = fit1.invcov ## cov12 ## fit2.invcov

c12 = reform(fit1.c - fit2.c,  1,  nw)
c12t = reform(c12, nw, 1)

chisq = c12t ## invcov ## c12

return,  chisq
END

function fetch_sky_filename,  run =  run,  rerun =  rerun,  $
                              camcol =  camcol, band =  band, $
                              field =  field,  directory = directory


runstr =  STRING(run,  format =  '(I06)')
fieldstr =  STRING(field,  format =  '(I04)')


filename =  directory + '/' + strtrim(run,  2) + '/' $
  + rerun + '/objcs/' + strtrim(camcol,  2) + '/' + $
  '/gpSKY-' + $
  runstr + '-' + band + strtrim(camcol,  2) + '-' + $
  fieldstr + '.fit'

filesearch =  filename + '*'

filepath = file_search(filesearch,  count =  count)

if (count eq 0) then begin
    filename =  ""
    return,  filename
endif

filename =  filepath[0]
return,  filename

END

function fetch_sky_fit_structure,  coeffs = coeffs,  $
                                   cov = cov,  $
                                   invcov = invcov

c =  ptr_new()
cov =  ptr_new()
invcov = ptr_new()

struct =  {SKYFIT,  run: -1L,  rerun: "",  $
           camcol: -1L,  field: -1L,  band: "unknown",  $
           bsize: -1L,  LOGL: 0.0D0, npixel: -1L, nampl: -1L,  $
           c: c,  cov: cov,  invcov: invcov}

return,  struct
END

function tag_test,  structure,  tag

names = tag_names(structure)

index =  where(names eq tag,  count)

return,  count
END

pro free_skyfit,  fit = fit

if n_elements(fit) eq 0 then begin
    return
endif

if n_elements(fit) gt 1 then begin
    for i = 0,  n_elements(fit) - 1,  1 do begin
        free_skyfit, fit =  fit[i]
    endfor
    return
endif

if tag_test(fit,  "C") then begin
    ptr_free,  fit.c
    fit.c =  ptr_new()
endif

if  tag_test(fit,  "COV") then begin
    ptr_free,  fit.cov
    fit.cov =  ptr_new()
endif

if tag_test(fit,  "INVCOV") then begin
    ptr_free,  fit.invcov
    fit.invcov =  ptr_new()
endif

return
END
