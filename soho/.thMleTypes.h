#ifndef THMLETYPES_H
#define THMLETYPES_H

#include "thMathTypes.h"
#include "thPsfTypes.h"
#include "thMaskTypes.h"
#include "thMapTypes.h"
#include "thModelTypes.h"
#include "thReg.h"
#include "thQq.h"
#include "thRegionTypes.h"
#include "thRegion.h"
#include "thAlgorithmTypes.h"

#define LMACHINE MAPMACHINE
#define thLmachineNew thMapmachineNew
#define thLmachineDel thMapmachineDel
#define thLmachineCopy thMapmachineCopy
#define thLmachineGetNPar	 thMapmachineGetNpar
#define thLmachineGetNModel 	 thMapmachineGetNamp
#define thLmachineGetModelPars	 thMapmachineGetModelPars
#define thLmachineGetModelName 	 thMapmachineGetMname
#define thLmachineGetParname 	 thMapmachineGetPname
#define thLmachineCompile thMCompile

#define LFBANK void
#define FITID int

#define LWMASK WMASK
#define thLwmaskNew thWmaskNew
#define thLwmaskDel thWmaskDel
#define thLwmaskCopy thWmaskCopy

#define LCOPY_UNKNOWN (0)
#define LCOPY_SOURCE (1 << 0)
#define LCOPY_FIT    (1 << 1)
#define LCOPY_METHOD (1 << 2)
#define LCOPY_ALGORITHM (1 << 3)
#define LCOPY_TEST (1 << 4)
#define LCOPY_PRIV (1 << 5)
#define LCOPY_ALL (LCOPY_SOURCE | LCOPY_FIT | LCOPY_METHOD | LCOPY_ALGORITHM | LCOPY_TEST | LCOPY_PRIV)

typedef enum lmethodtype {
  LM = 0, 
  EXTLM,
  HYBRIDLM,
  GD, SGD, PGD, MGD, NAG, RSV,  
  NEWTON,
  SALM, /* SA global, LM local */
  SA,
  SANE,
  N_LMETHODTYPE,
  UNKNOWN_LMETHODTYPE
} LMETHODTYPE; /* pragma SCHEMA */

typedef enum lmethodsubtype {
  LEVENBERGMARQUARDT = 0, 
  QUASINEWTON,
  STEEPESTDESCENT,
  PGDPURE, MGDPURE, NAGPURE, RSVPURE,  
  UNKNOWN_LMETHODSUBTYPE,
  N_LMETHODSUBTYPE,
} LMETHODSUBTYPE; /* pragma SCHEMA */


typedef enum limagemode {
	POISSON_IMAGE, CONTINUOUS_IMAGE, UNKNOWN_MLEIMAGE
} LIMAGEMODE; /* this is meant for test runs only */

typedef enum linitmode {
	SDSS_INIT, SDSSpERROR_INIT, FORCED_INIT, N_LINITMODE, UNKNOWN_LINITMODE
} LINITMODE; /* this is meant for test runs only */

typedef enum lmcrit {
	LM_COST_DECREASED_ACC = 0,
	LM_DERIV_IMPROVED_ACC = 1 << 1, 
	LM_DERIV_INCREASED_REJ = 1 << 2, 	
	LM_COST_INCREASED_REJ = 1 << 3,
	LM_CONVERGED_ACC = 1 << 4,
	UNKNOWN_LMCRIT = 1 << 5,
	N_LMCRIT = 1 << 6
} LMCRIT; /* pragma SCHEMA */

typedef enum convergence {
	CONTINUE = 0,
	CONVERGED = 1<<1,
	TOOSMALLSTEP = 1<<2,
	TOOSMALLDERIV = 1<<3, 
	TOOMANYITER = 1<<4,
	ABSOLUTEF_CONVERGENCE = 1<<5, /* Dorris Convergence Tests */ 
	X_CONVERGENCE = 1<<6, 
	RELATIVEF_CONVERGENCE = 1<<7, 
	SINGULAR_CONVERGENCE = 1<<8, 
	FALSE_CONVERGENCE = 1<<9,
	UNABLETOASSESSSTEP = 1 << 10,
	ERROR_CONVERGENCE = 1<<11, /* the MLE algorithm finished because an error occured */ 
	EDGE_CONVERGENCE = 1 << 12, /* edge of the parameter space was reached too many times */
	UNKNOWN_CONVERGENCE = 1<<13, 	
	N_CONVERGENCE = 1<<14, /* number of available flags */
} CONVERGENCE;

typedef enum lm_step_status {
	LM_STEP_REJECTED = 0,
	LM_STEP_ACCEPTED,
	LM_STEP_SIMULATED, /* this flag is to be used only during simulations */
	UNKNOWN_LM_STEP_STATUS,
	N_LM_STEP_STATUS
} LM_STEP_STATUS; /* pragma SCHEMA */

typedef enum lsector {
	ASECTOR,
	APSECTOR,
	PSECTOR,
	UNKNOWN_SECTOR,
	N_SECTOR
	} LSECTOR;

typedef enum xupdate {
	OUTDATED,
	UPDATED,
	UNKNOWN_XUPDATE,
	N_XUPDATE
} XUPDATE;

typedef enum runflag {
	MEMORY_ESTIMATE,
	INITIMAGES,
	INITIMAGES_AND_DM, 
	IMAGES,
	IMAGES_AND_DM,
	DM,
	UNKNOWN_RUNFLAG,
	N_RUNFLAG
} RUNFLAG; /* pragma SCHEMA */

typedef enum assignflag {
	/* more specific flags specifying the source and the target */
	MODELTOIO,
	MODELTOX,
	MODELTOXN,
	IOTOMODEL,
	IOTOX,
	IOTOXN,
	XTOMODEL,
	XTOIO,
	XNTOMODEL,
	XNTOIO,
	UNKNOWN_ASSIGN,
	N_ASSIGNFLAG
} ASSIGNFLAG; /* pragma SCHEMA */

typedef enum lstage {
	INITSTAGE, MIDDLESTAGE, ENDSTAGE, SIMSTAGE,
	N_LSTAGE, UNKNOWN_STAGE
} LSTAGE; /* pragma SCHEMA */

typedef enum runtype {
	SIMPLERUN, ALGRUN, UNKNOWN_RUNTYPE, N_RUNTYPE
} RUNTYPE; /* pragma SCHEMA */

typedef enum simflag {
	NOSIMFORFINALFIT, SIMFINALFIT, N_SIMFLAG, UNKNOWN_SIMFLAG
} SIMFLAG; /* pragma SCHEMA */

typedef enum lfitrecordtype {
	NULL_LFITRECORD = 0, /* record neither LM nor LP */
	LM_RECORD_FIRSTANDLAST = 1<<0,
	LM_RECORD_ALLGOOD = 1<<1, 
	LM_RECORD_ALL = 1<<2,
	LP_RECORD_FIRSTANDLAST = 1<<3,
	LP_RECORD_ALLGOOD = 1<<4, 
	LP_RECORD_ALL = 1<<5,
	LP_RECORD_LASTOFBEST = 1<<6,
	LP_RECORD_NOW = 1<<7
} LFITRECORDTYPE;


#define LP_RECORD_ANY (LP_RECORD_FIRSTANDLAST | LP_RECORD_ALLGOOD | LP_RECORD_ALL) 
#define LM_RECORD_ANY (LM_RECORD_FIRSTANDLAST | LM_RECORD_ALLGOOD | LM_RECORD_ALL) 

typedef struct lmethod {
  
  LMETHODTYPE type;
  CONVERGENCE cflag;

  /* parameters for Levenberg-Marquardt */
  FL64 lambda;
  FL64 nu, gamma, beta, rhobar;
  FL64 tau;
  int q;
  /* convergence for LM */
  FL64 e1, e2;
  int k, kmax;
  LM_STEP_STATUS status; /* accepted or rejected */

/* records to decide sub-types */
   LMETHODSUBTYPE wtype;
  int step_count, step_mode; /* the number of steps where a particular submethod failed - useful in hybrid methods to decide if we should switch to Quasi-Newtonian approach */
  
  /*records for quasi-newtonian */
  FL64 delta_qn;

  /* record for gradient descent */
  FL64 eta_max;
  FL64 rho_adadelta, epsilon_adadelta_dx, epsilon_adadelta_g;

  /* other methods */
  FL64 T;      /* used by SA and SANE */
  FL64 sigmaL; /* used by SANE */

  /* chisq values */
  FL64 cost, chisq, pcost; /* this is particulalry needed for tracking the chisq values */ 
  
  /* small step count to detect convergence that is too slow */
  FL64 small_step_count;

  /* PGD */
  int count_PGD_thresh, count_PGD_noise;
  int k_PGD_thresh, k_PGD_noise, perturbation_PGD_condition;
  FL64 mag_Fprime_PGD_thresh;
  FL64 perturbation_PGD_size;
  FL64 cost_PGD_noise, chisq_PGD_noise, cost_PGD_thresh;
	
  /* MGD */
  FL64 momentum;
  FL64 lr_gd;
  int k_fprime_incidence;

} LMETHOD; /* pragma SCHEMA */ 

typedef struct lmodvar {

  MLEFL **covap;
  MLEFL **invcovap;

  MLEFL **cov;
  MLEFL **invcov;

  int namp, npar, nap;
  
  FL32 L0;

} LMODVAR; /* pragma IGNORE */


typedef struct lmodel {
  
  /* list of amplitudes and non-linear parameters */

  MLEFL *a, *p, *mcount;
  int namp, npar, nap;
 
  /* 
     map machine maps 
     parameter list onto object properties 
  */
  LMACHINE *lmachine;
  /* variance */
  LMODVAR *modvar;

  /* the following show the bit used to designate the fit region */
  #if (THMASKisMASK | THMASKisSUPERMASK)
  MASKBIT fitregbit; 
  #endif

  LINITMODE initmode;

} LMODEL; /* pragma IGNORE */
  

typedef struct ldata {
  
  /* the main image */
  REGION *image;
  REGION *weight;
  /* the background model and its associated error */
  LMODEL *background;
  /* the set of pixels to be used */
  THMASK *mask;
  /* psf and its error */
  PSFMODEL *psf;
  /* psf error is not handled in the current version 
  PSFERROR *psferror;
  */
  LIMAGEMODE imagemode;

  /* parent and child */
  void *parent;
} LDATA; /* pragma IGNORE */


typedef struct lvalue {

  /* value for the likelihood function */
  FL32 L;
  /* derivative at that location w.r.t model paramaters */
  FL32 *dL;
  /* the r-side of the MLE equation */
  FL32 **ddL;
  /* the l-side of the MLE equation */
  FL32 *dL0;
  
  int npar;

} LVALUE; /* pragma IGNORE */

typedef struct limpack {
	THREGION **mreg;
	MLEFL *mcount;
	THREGION ***DDmreg;
	int nmpar, nmpar_max;
	/* initial guess images and mcounts, reduced image - added aug 2017 */
	THREGION *mreg0, *mreg_reduced;
	MLEFL mcount0;
	/* end of initial guess images and mcounts, reduced image - added aug 2017 */
} LIMPACK; /* pragma IGNORE */

typedef struct lwork {

	/* current par */
	REGION *m;
	REGION *dmm;
	/* 
	old -- oct 16, 2012  -- components and their derivatives 
	THREGION **mreg; 
	*/
	/* psf related */
	PSF_CONVOLUTION_INFO **mpsf; /* convolution info for models */
	WPSF *wpsf;
		
	/* goodness of fit */	
	CHISQFL chisq, cost;
	CHISQFL *dpcost, **ddpcost, pcost; /* parameter cost function */
	/* step to a new par */
	MLEFL *da;
	MLEFL *dp;
	/* second order correction to a new par */
	MLEFL *db;
	MLEFL *dq;
	/* temporary da and dp */
	MLEFL *daT;
	MLEFL *dpT;
	/* old par - added  Nov 2017 */
	MLEFL *a, *p, *mcount;

	/* new par */
	MLEFL *aN;
	MLEFL *pN;
	MLEFL *mcountN;

	/* initial guess pars - added on aug 2017 */
	MLEFL *a0;
	MLEFL *p0;
	MLEFL *mcount0;
	/* end of initial guess pars - added on aug 2017 */

	/* temporary work space  - added on april 2018 */
	MLEFL *aT;
	MLEFL *pT;
	MLEFL *mcountT;
	MLEFL **covapT, **covaT, **covpT;
	/* end of temporary work space */

	/* previous step values. Used when updating the value of x from xN */
	MLEFL *aold;
	MLEFL *pold;
	MLEFL *mcountold;

	/* flags to show update status */
	XUPDATE aupdate, pupdate;

	/* Images for new par */
	REGION *mN;
	REGION *dmmN;
	THREGION **mregN; /* components and their derivatives */
 	THREGION **DDmregN; /* second derivative of model components */
	
	CHISQFL chisqN, costN;

	CHISQFL *dpcostN, **ddpcostN, pcostN; /* parameter cost function */

	/* images for initial guess, chisq value for initial guess, cost for initial guess  - added on aug 2017 */
	REGION *m0, *d_reduced; /* initial guess for the while model (addition of object models and the reduced data | data_reduced > = | data - m0 > */ 
	THREGION **mreg0, **mreg_reduced;  /* | reduced image >  = | model image - initial guess > */ 
	CHISQFL chisq0, cost0, pcost0;
	/* end of images for initial guess, chisq value for initial guess, cost for initial guess  - added on aug 2017 */

	/* the j-matrices are only computed for values of parameters that are accepted. 
	there is no need to separate new and old j-components */
	MLEFL **jtj;
	MLEFL **wjtj;
	MLEFL *jtdmm; /* Jt | W | d - m > */		
	MLEFL *jtddm;  /* Jt DD m / Dp Dp  */
	MLEFL **jtjpdiag; /* Jt J + lambda diag(Jt J) */
	/* this has a different data type making it compatible withinvestion function */
	MLEFL *Djtj;   /* Diagonal part of Jt J */

	MLEFL **jtjap; /* the -ap versions are matrices of the same definition also including amplitudes */
	MLEFL **wjtjap; /* the -ap versions are matrices of the same definition also including amplitudes */
	MLEFL *jtdmmap;     /* Jt | W | d - m > for a-p sector */	
	MLEFL *jtddmap;  /* Jt DD m / Dp Dp  */
	MLEFL **jtjpdiagap; /* Jt J + lambda diag (Jt J) for a-p sector */
	MLEFL *Djtjap; /* Diagonal part of Jt J for a-p sector */

	/* the following helps with calculation of Quasi-Newtonian methods */
	MLEFL *jtdmm_prev; /* the previous steps value is stored here = F'(x_k+1) */
	MLEFL *jtdmmap_prev; /* the previous steps value is stored here = F'(x_k)*/
	MLEFL **Bqn, **Bqnap; /* Quasi-Newtonian estimate of inverse of hessian matrix = B_k+1*/
	MLEFL **Bqn_prev, **Bqnap_prev; /* = B_k */
	MLEFL **Hqn, **Hqnap;
	MLEFL **Hqn_prev, **Hqnap_prev;
	MLEFL **BBqn, **BBqnap; /* Quasi-Newtonian estimate of inverse of hessian matrix = B_k+1*/
	MLEFL **BBqn_prev, **BBqnap_prev; /* = B_k */
	MLEFL **HHqn, **HHqnap;
	MLEFL **HHqn_prev, **HHqnap_prev;
	

	/* the following is for AdaDelta */
	MLEFL *rms_g, *rms_dx;
	MLEFL *rms_ginv;
	MLEFL *E_gg, *E_dxdx;
	MLEFL *rms_g_prev, *rms_dx_prev;
	MLEFL *rms_ginv_prev;
	MLEFL *E_gg_prev, *E_dxdx_prev;
	MLEFL *rms_gap, *rms_dxap;
	MLEFL *rms_ginvap;
	MLEFL *E_ggap, *E_dxdxap;
	MLEFL *rms_gap_prev, *rms_dxap_prev;
	MLEFL *rms_ginvap_prev;
	MLEFL *E_ggap_prev, *E_dxdxap_prev;
	MLEFL rho_adadelta, epsilon_adadelta_dx, epsilon_adadelta_g;

	/* when working on P-sector only */
	MLEFL **DaDp, **DaDpT, **wN, **wwN, **wA, *wC;
	/* 
	MLEFL *dx, *dxap; --  dleta(x_k) */

	/* brackets - they don't need to be saved for comparison, they are only computed on the run and used at instance */
	MLEFL *mid;
	MLEFL *midmm;
	MLEFL **mimj;
	MLEFL dd;
	#if OLD_MAP
	MLEFL ***mimjk;
	MLEFL ****mijmkl; 
	MLEFL **mijdmm;
	MLEFL **mijd; 
	#else
	MLEFL **MM; /* this matrix is to replace mijmkl, mijmk in the old formalism */
	MLEFL **MddM; /* this matrix contains multiplication of Dm with DDm used in higher order fitting */
	MLEFL *MDmm; /* this matrix is to replace mijdmm and midmm in the old formalism */
	MLEFL *MD; /* this matrix is to replace mijd and mid in the old formalism */
	#endif


	/* temporary spaces [nap][nap] to do calculation in 
	e.g. inversion of matrix in qk */
	MLEFL *wqny, *wqnz;
	MLEFL **wap, **wwap;
	MLEFL **wp, **wwp;
	MLEFL *wx, *wwx;
	char **wrnames;
	int *wpndexmap;
	REGION *wreg, *wreg2;
	/* work space for reduced model images - added on aug 2017 */
	REGION *wreg_reduced1, *wreg_reduced2; 
	/* end of work space for reduced model images - added on aug 2017 */
	PSFWING *wpsfwing;
	/* working MATRIX and VECTOR structures used by PHOTO functions */
	MAT *wMatp, *wMata, *wMatap;
	MAT *wwMatp, *wwMata, *wwMatap;
	VEC **wVecp, **wVeca, **wVecap;
	/* dimensions */
	int nm, np, nap, nacc, nc, nj; 
	/* nc is the total number of models + their available derivatives */
	int npp, nDDacc; /* the total number of second derivatives available */
	
	LIMPACK **impacks;
	
	MEMDBLE memory;

} LWORK; /* pragma IGNORE */

typedef struct lalg {
	RUNTYPE runtype;
	ALGORITHM **algs;
} LALG; /* pragma IGNORE */

typedef struct ltest {
	REGION *model, *data, *diff;
	/* private stuff */
	void *parent;
} LTEST; /* pragma IGNORE */

typedef struct lcalib {
	CALIBTYPE type;
	CRUDE_CALIB *cc;
} LCALIB; /* pragma IGNORE */


typedef struct lprofile {

	/* the following from SDSS Object structyre */

	char objid[19];
   	char parentid[19];
  	char fieldid[19];  
   	unsigned char skyversion;
   	unsigned char mode;
  	unsigned char clean;
   	short int run;
   	char rerun[3];
   	unsigned char camcol;
   	short int field;

	int id;				/* id number for this objc */
   	int parent;			        /* id of parent for deblends */
   	int nchild;				/* number of children */
   	OBJ_TYPE objc_type;			/* overall classification */
   	float objc_prob_psf;			/* Bayesian probability of being PSF */
   	int catID;			        /* catalog id number */
   	float objc_rowc;	/* row position of centre */
	float objc_colc;	/* column position of center */

	int flagLRG, flagLRG2, flagLRG3;

	/* the following is from SOHO object entry */
	/* star component */
	FL32 I_star, J_star;
	FL32 xc_star, yc_star;
 	FL32 mcount_star;
	FL32 counts_star;
	FL32 flux_star, mag_star;

	/* standard errors */
	FL32 IErr_star, JErr_star;
	FL32 xcErr_star, ycErr_star;
	FL32 countsErr_star;
	FL32 fluxErr_star;
	FL32 magErr_star;

	/* accumulated galaxy models */
	FL32 counts_model;
	FL32 xc_model, yc_model;	
	FL32 flux_model, mag_model;

	/*standard errors */
	FL32 countsErr_model;
	FL32 xcErr_model, ycErr_model;	
	FL32 fluxErr_model, magErr_model;

	/* deV component */
	FL32 I_deV, J_deV;
	FL32 xc_deV, yc_deV;
	FL32 re_deV, e_deV, phi_deV;
	FL32 a_deV, b_deV, c_deV;
	FL32 ue_deV, E_deV;
	FL32 v_deV, w_deV;
	FL32 V_deV, W_deV;
	FL32 mcount_deV;
	FL32 counts_deV;
	FL32 flux_deV, mag_deV;

	/* standard errors */
	FL32 IErr_deV, JErr_deV;
	FL32 xcErr_deV, ycErr_deV;
	FL32 reErr_deV, eErr_deV, phiErr_deV;
	FL32 aErr_deV, bErr_deV, cErr_deV;
	FL32 ueErr_deV, EErr_deV;
	FL32 vErr_deV, wErr_deV;
	FL32 VErr_deV, WErr_deV;
	FL32 countsErr_deV;
	FL32 fluxErr_deV, magErr_deV;

	/* first (inner) deV component */
	FL32 I_deV1, J_deV1;
	FL32 xc_deV1, yc_deV1;
	FL32 re_deV1, e_deV1, phi_deV1;
	FL32 a_deV1, b_deV1, c_deV1;
	FL32 ue_deV1, E_deV1;
	FL32 v_deV1, w_deV1;
	FL32 V_deV1, W_deV1;
	FL32 mcount_deV1;
	FL32 counts_deV1;
	FL32 flux_deV1, mag_deV1;
	
	/* standard errors */
	FL32 IErr_deV1, JErr_deV1;
	FL32 xcErr_deV1, ycErr_deV1;
	FL32 reErr_deV1, eErr_deV1, phiErr_deV1;
	FL32 aErr_deV1, bErr_deV1, cErr_deV1;
	FL32 ueErr_deV1, EErr_deV1;
	FL32 vErr_deV1, wErr_deV1;
	FL32 VErr_deV1, WErr_deV1;
	FL32 countsErr_deV1;
	FL32 fluxErr_deV1, magErr_deV1;

	/* second (outer) deV component */
	FL32 I_deV2, J_deV2;
	FL32 xc_deV2, yc_deV2;
	FL32 re_deV2, e_deV2, phi_deV2;
	FL32 a_deV2, b_deV2, c_deV2;
	FL32 ue_deV2, E_deV2;
	FL32 v_deV2, w_deV2;
	FL32 V_deV2, W_deV2;
	FL32 mcount_deV2;
	FL32 counts_deV2;
	FL32 flux_deV2, mag_deV2;
	
	/* standard errors */
	FL32 IErr_deV2, JErr_deV2;
	FL32 xcErr_deV2, ycErr_deV2;
	FL32 reErr_deV2, eErr_deV2, phiErr_deV2;
	FL32 aErr_deV2, bErr_deV2, cErr_deV2;
	FL32 ueErr_deV2, EErr_deV2;
	FL32 vErr_deV2, wErr_deV2;
	FL32 VErr_deV2, WErr_deV2;
	FL32 countsErr_deV2;
	FL32 fluxErr_deV2, magErr_deV2;

	/* exponential component */
	FL32 I_Exp, J_Exp;
	FL32 xc_Exp, yc_Exp;
	FL32 re_Exp, e_Exp, phi_Exp;
	FL32 a_Exp, b_Exp, c_Exp;
	FL32 ue_Exp, E_Exp;
	FL32 v_Exp, w_Exp;
	FL32 V_Exp, W_Exp;
	FL32 mcount_Exp;
	FL32 counts_Exp;
	FL32 flux_Exp, mag_Exp;

	/* standard errors */
	FL32 IErr_Exp, JErr_Exp;
	FL32 xcErr_Exp, ycErr_Exp;
	FL32 reErr_Exp, eErr_Exp, phiErr_Exp;
	FL32 aErr_Exp, bErr_Exp, cErr_Exp;
	FL32 ueErr_Exp, EErr_Exp;
	FL32 vErr_Exp, wErr_Exp;
	FL32 VErr_Exp, WErr_Exp;
	FL32 countsErr_Exp;	
	FL32 fluxErr_Exp, magErr_Exp;

	/* first (inner) exponential component */

	FL32 I_Exp1, J_Exp1;
	FL32 xc_Exp1, yc_Exp1;
	FL32 re_Exp1, e_Exp1, phi_Exp1;
	FL32 a_Exp1, b_Exp1, c_Exp1;
	FL32 ue_Exp1, E_Exp1;
	FL32 v_Exp1, w_Exp1;
	FL32 V_Exp1, W_Exp1;
	FL32 mcount_Exp1;
	FL32 counts_Exp1;
	FL32 flux_Exp1, mag_Exp1;

	/* standard errors */
	FL32 IErr_Exp1, JErr_Exp1;
	FL32 xcErr_Exp1, ycErr_Exp1;
	FL32 reErr_Exp1, eErr_Exp1, phiErr_Exp1;
	FL32 aErr_Exp1, bErr_Exp1, cErr_Exp1;
	FL32 ueErr_Exp1, EErr_Exp1;
	FL32 vErr_Exp1, wErr_Exp1;
	FL32 VErr_Exp1, WErr_Exp1;
	FL32 countsErr_Exp1;
	FL32 fluxErr_Exp1, magErr_Exp1;

	/* second (outer) exponential component */

	FL32 I_Exp2, J_Exp2;
	FL32 xc_Exp2, yc_Exp2;
	FL32 re_Exp2, e_Exp2, phi_Exp2;
	FL32 a_Exp2, b_Exp2, c_Exp2;
	FL32 ue_Exp2, E_Exp2;
	FL32 v_Exp2, w_Exp2;
	FL32 V_Exp2, W_Exp2;
	FL32 mcount_Exp2;
	FL32 counts_Exp2;
	FL32 flux_Exp2, mag_Exp2;

	/* standard errors */
	FL32 IErr_Exp2, JErr_Exp2;
	FL32 xcErr_Exp2, ycErr_Exp2;
	FL32 reErr_Exp2, eErr_Exp2, phiErr_Exp2;
	FL32 aErr_Exp2, bErr_Exp2, cErr_Exp2;
	FL32 ueErr_Exp2, EErr_Exp2;
	FL32 vErr_Exp2, wErr_Exp2;
	FL32 VErr_Exp2, WErr_Exp2;
	FL32 countsErr_Exp2;
	FL32 fluxErr_Exp2, magErr_Exp2;

	/* powerlaw component */
/* 
	FL32 I_Pl, J_Pl;
	FL32 xc_Pl, yc_Pl;
	FL32 re_Pl, e_Pl, phi_Pl, n_Pl;
	FL32 a_Pl, b_Pl, c_Pl;
	FL32 ue_Pl, E_Pl;
	FL32 v_Pl, w_Pl;
	FL32 V_Pl, W_Pl;
	FL32 mcount_Pl;
	FL32 counts_Pl;
	FL32 flux_Pl, mag_Pl;
*/
	/* standard errors */
/* 
	FL32 IErr_Pl, JErr_Pl;
	FL32 xcErr_Pl, ycErr_Pl;
	FL32 reErr_Pl, eErr_Pl, phiErr_Pl, nErr_Pl;
	FL32 aErr_Pl, bErr_Pl, cErr_Pl;
	FL32 ueErr_Pl, EErr_Pl;
	FL32 vErr_Pl, wErr_Pl;
	FL32 VErr_Pl, WErr_Pl;
	FL32 countsErr_Pl;
	FL32 fluxErr_Pl, magErr_Pl;
*/

	/* sersic component */
	FL32 I_sersic, J_sersic;
	FL32 xc_sersic, yc_sersic;
	FL32 re_sersic, e_sersic, phi_sersic, n_sersic;
	FL32 a_sersic, b_sersic, c_sersic;
	FL32 ue_sersic, E_sersic;
	FL32 v_sersic, w_sersic;
	FL32 V_sersic, W_sersic;
	FL32 mcount_sersic;
	FL32 counts_sersic;
	FL32 flux_sersic, mag_sersic;

	/* standard errors */
	FL32 IErr_sersic, JErr_sersic;
	FL32 xcErr_sersic, ycErr_sersic;
	FL32 reErr_sersic, eErr_sersic, phiErr_sersic, nErr_sersic;
	FL32 aErr_sersic, bErr_sersic, cErr_sersic;
	FL32 ueErr_sersic, EErr_sersic;
	FL32 vErr_sersic, wErr_sersic;
	FL32 VErr_sersic, WErr_sersic;
	FL32 countsErr_sersic;
	FL32 fluxErr_sersic, magErr_sersic;

	/* first (inner) sersic component */
	FL32 I_sersic1, J_sersic1;
	FL32 xc_sersic1, yc_sersic1;
	FL32 re_sersic1, e_sersic1, phi_sersic1;
	FL32 k_sersic1, n_sersic1, N_sersic1;
	FL32 a_sersic1, b_sersic1, c_sersic1;
	FL32 ue_sersic1, E_sersic1;
	FL32 v_sersic1, w_sersic1;
	FL32 V_sersic1, W_sersic1;
	FL32 mcount_sersic1;
	FL32 counts_sersic1;
	FL32 flux_sersic1, mag_sersic1;

	/* standard errors */
	FL32 IErr_sersic1, JErr_sersic1;
	FL32 xcErr_sersic1, ycErr_sersic1;
	FL32 reErr_sersic1, eErr_sersic1, phiErr_sersic1;
	FL32 kErr_sersic1, nErr_sersic1, NErr_sersic1;
	FL32 aErr_sersic1, bErr_sersic1, cErr_sersic1;
	FL32 ueErr_sersic1, EErr_sersic1;
	FL32 vErr_sersic1, wErr_sersic1;
	FL32 VErr_sersic1, WErr_sersic1;
	FL32 countsErr_sersic1;
	FL32 fluxErr_sersic1, magErr_sersic1;

	
	/* second (outer) sersic component */
	FL32 I_sersic2, J_sersic2;
	FL32 xc_sersic2, yc_sersic2;
	FL32 re_sersic2, e_sersic2, phi_sersic2;
	FL32 k_sersic2, n_sersic2, N_sersic2;
	FL32 a_sersic2, b_sersic2, c_sersic2;
	FL32 ue_sersic2, E_sersic2;
	FL32 v_sersic2, w_sersic2;
	FL32 V_sersic2, W_sersic2;
	FL32 mcount_sersic2;
	FL32 counts_sersic2;
	FL32 flux_sersic2, mag_sersic2;

	/* standard errors */
	FL32 IErr_sersic2, JErr_sersic2;
	FL32 xcErr_sersic2, ycErr_sersic2;
	FL32 reErr_sersic2, eErr_sersic2, phiErr_sersic2;
	FL32 kErr_sersic2, nErr_sersic2, NErr_sersic2;
	FL32 aErr_sersic2, bErr_sersic2, cErr_sersic2;
	FL32 ueErr_sersic2, EErr_sersic2;
	FL32 vErr_sersic2, wErr_sersic2;
	FL32 VErr_sersic2, WErr_sersic2;
	FL32 countsErr_sersic2;
	FL32 fluxErr_sersic2, magErr_sersic2;

	/* coresersic component */
 
	FL32 I_coresersic, J_coresersic;
	FL32 xc_coresersic, yc_coresersic;
	FL32 re_coresersic, rb_coresersic;
	FL32 e_coresersic, phi_coresersic;
	FL32 k_coresersic, n_coresersic, N_coresersic;
	FL32 gamma_coresersic, delta_coresersic;
	FL32 G_coresersic, D_coresersic;
	FL32 a_coresersic, b_coresersic, c_coresersic;
	FL32 ue_coresersic, ub_coresersic, E_coresersic;
	FL32 v_coresersic, w_coresersic;
	FL32 V_coresersic, W_coresersic;
	FL32 mcount_coresersic;
	FL32 counts_coresersic;
	FL32 flux_coresersic, mag_coresersic;

	/* standard errors */
	FL32 IErr_coresersic, JErr_coresersic;
	FL32 xcErr_coresersic, ycErr_coresersic;
	FL32 reErr_coresersic, rbErr_corsersic;
	FL32 eErr_coresersic, phiErr_coresersic;
	FL32 kErr_coresersic, nErr_coresersic, NErr_coresersic;
	FL32 gammaErr_coresersic, deltaErr_coresersic;
	FL32 GErr_coresersic, DErr_coresersic; 
	FL32 aErr_coresersic, bErr_coresersic, cErr_coresersic;
	FL32 ueErr_coresersic, ubErr_coresersic, EErr_coresersic;
	FL32 vErr_coresersic, wErr_coresersic;
	FL32 VErr_coresersic, WErr_coresersic;
	FL32 countsErr_coresersic;
	FL32 fluxErr_coresersic, magErr_coresersic;
	

	
	/* second (outer) sersic component */
	/*
	FL32 I_sersic2, J_sersic2;
	FL32 xc_sersic2, yc_sersic2;
	FL32 re_sersic2, e_sersic2, phi_sersic2, n_sersic2;
	FL32 mcount_sersic2;
	*/
	/* power law component */
	/*
	HPIX I_pl, J_pl;
	FL32 xc_pl, yc_pl;
	FL32 n_pl;
	FL32 mcount_pl;
	*/
	/* second (outer) power-law component */
	/* 
	FL32 I_pl2, J_pl2;
	FL32 xc_pl2, yc_pl2;
	FL32 n_pl2;
	FL32 mcount_pl2;

	*/
	/* gaussian */
/* 
	FL32 I_gaussian, J_gaussian;
	FL32 a_gaussian, b_gaussian, c_gaussian;
	FL32 xc_gaussian, yc_gaussian;
	FL32 re_gaussian, e_gaussian, phi_gaussian;
	FL32 E_gaussian; 
	FL32 v_gaussian, w_gaussian;
	FL32 V_gaussian, W_gaussian;
	FL32 mcount_gaussian;
	FL32 counts_gaussian;
	FL32 flux_gaussian, mag_gaussian;
*/
	/* standard errors */	
/* 
	FL32 IErr_gaussian, JErr_gaussian;
	FL32 aErr_gaussian, bErr_gaussian, cErr_gaussian;
	FL32 xcErr_gaussian, ycErr_gaussian;
	FL32 reErr_gaussian, eErr_gaussian, phiErr_gaussian;
	FL32 EErr_gaussian; 
	FL32 vErr_gaussian, wErr_gaussian;
	FL32 VErr_gaussian, WErr_gaussian;
	FL32 countsErr_gaussian;
	FL32 fluxErr_gaussian, magErr_gaussian;
*/
	/* information for all moderls */
	/* accurate and crude calibration info
	FL32 kk, aa;
	FL32 flux20; 
	*/
	int band;

	/* now 1D profile information */

	int nmodel;
	char *modelnames[NCOMPONENT];
	FL32 modelmags[NCOMPONENT];
	FL32 modelcounts[NCOMPONENT];
	FL32 modelprofiles[PROFILEMRAD]; /* 1D radial profile for each model */
	FL32 modelCprofiles[PROFILEMRAD]; /* cumulative 1D radial profile for each model */

	char *objcname; /* name of the object type used during the fit */
	FL32 mag, counts; /* mix model mag and counts */
	FL32 profile[PROFILENRAD];  /* mix model 1D radial profile */
	FL32 Cprofile[PROFILENRAD]; /* mix model cumulative 1D profile */

	FL32 rad[PROFILENRAD]; /* radius for which the profile is calculates */

	/* other information about this fit */
	FL32 sky; /* sky at the center of the object as found by SOHO */
	int step; /* fit step this object was calculated at */
	FL32 chisq; /* value of chi-squared for this step */


	/* copy made from LMETHOD */
	LMETHODTYPE lm_type;
  	CONVERGENCE lm_cflag;

  	int lm_k, lm_kmax;
  	int lm_status; /* accepted or rejected */

  	/* chisq values */
  	FL64 lm_cost, lm_chisq, lm_pcost; /* this is particulalry needed for tracking the chisq values */ 

} LPROFILE; /* pragma SCHEMA */

typedef struct lfit {
	HDR *hdr;
	LMETHOD *lms;
	int nlm, lm_nmax;
	LPROFILE *lps;
	int nlp, lp_nmax;
	LMETHOD *lm_init, *lm_fin;
	LFITRECORDTYPE rtype;
	char *lm_filename;
	char *lp_filename;
} LFIT; /* pragma IGNORE */



typedef struct lstruct {
	LDATA *ldata;
	LWMASK *lwmask;	
	LMODEL *lmodel;
	LWORK  *lwork;
	LMODVAR *lcov;
	LMETHOD *lmethod;
	/* the following is under construction */
	LFBANK *lfbank; /* bank of model and derivative functions for construction of images */

	LALG *lalg; /* algorithm for different stages of fit and modeling */
	LTEST *ltest; /* data structures needed to test the residuals of a simulated fit */
	LCALIB *lcalib;
	LFIT *lfit; /* chain for tracking fit parameters */
/* private stuff, used by capture package */
	void *lpriv;
	char *lprivtype;

  /* source frame - used when unpredictaed changed to the software - also by (capture) package */
	void *source;
	void *parent;
	int cmode;
} LSTRUCT; /* pragma IGNORE */

typedef struct lfpack {
	char *fname;
	void **mf;
	int nmpar;
} LFPACK; /* pragma IGNORE */

/* constructors and destructors */

LDATA *thLdataNew();
LMODEL *thLmodelNew();
LWORK *thLworkNew();
LMODVAR *thLmodvarNew();
LMETHOD *thLmethodNew();
LSTRUCT *thLstructNew();

RET_CODE thLdataDel(LDATA *ldata);
RET_CODE thLmodelDel(LMODEL *lmodel);
RET_CODE thLworkDel(LWORK *lwork);
RET_CODE thLmodvarDel(LMODVAR *lmodvar);
RET_CODE thLmethodDel(LMETHOD *lmethod);
RET_CODE thLstructDel(LSTRUCT *lstruct);

LIMPACK *thLimpackNew();
void thLimpackDel(LIMPACK *impack);
RET_CODE thLimpackGetMemory(LIMPACK *impack, MEMFL *memory);

/* allocators and deallocators */

RET_CODE thLmodelAlloc(LMODEL *lmodel, int namp, int npar);
RET_CODE thLmodelDealloc(LMODEL *lmodel);
RET_CODE thLworkAlloc(LWORK *lwork, LMODEL *model, LMACHINE *map, int nm, int np, int npp, int nc, int nacc, int nj, int nrow, int ncol);
RET_CODE thLworkDealloc(LWORK *lwork);
RET_CODE thLmodvarAlloc(LMODVAR *lmodvar, int namp, int npar);
RET_CODE thLmodvarDealloc(LMODVAR *lmodvar);

RET_CODE thLdataClean(LDATA *ldata);
RET_CODE thLmethodDefault(LMETHOD *lmethod);
RET_CODE thLstructDealloc(LSTRUCT *lstruct);


RET_CODE thLdataPut(LDATA *ldata, REGION *image, REGION *weight, LMODEL *background, THMASK *mask, PSFMODEL *psf);
RET_CODE thLdataGet(LDATA *ldata, REGION **image, REGION **weight, LMODEL **background, THMASK **mask, PSFMODEL **psf);
RET_CODE thLwmaskPut(LWMASK *lwmask, LDATA *ldata, LMODEL *lmodel);
RET_CODE thLwmaskCountPix(LWMASK *lwmask, int *npix);

RET_CODE thLworkGetMddM(LWORK *lwork, MLEFL ***mDDm, int *nDm, int *nDDm);
RET_CODE thLworkGetMM(LWORK *lwork, MLEFL ***mm, int *nmm);
RET_CODE thLworkGetMDmm(LWORK *lwork, MLEFL **mdmm);

/* basic function */

RET_CODE IncrementX(LSTRUCT *lstruct, LSECTOR sector);
RET_CODE IncrementA(LSTRUCT *lstruct);
RET_CODE IncrementP(LSTRUCT *lstruct);
RET_CODE UpdateLMDamping(LSTRUCT *lstruct, MLEFL lambda, MLEFL nu);
RET_CODE UpdateXFromXN(LSTRUCT *lstruct, LSECTOR sector);
RET_CODE ExtractLMConvergence(LSTRUCT *lstruct, int *k, MLEFL *e1, MLEFL *e2, int *kmax);
RET_CODE ExtractLMStepNo(LSTRUCT *lstruct, int *k, int *kmax);
RET_CODE UpdateLMStepNo(LSTRUCT *lstruct, int k);
RET_CODE UpdateLMConvergence(LSTRUCT *lstruct, CONVERGENCE flag);
RET_CODE UpdateLMSmallStepCount(LSTRUCT *lstruct, CHISQFL Dchisq);
RET_CODE ExtractLMCurrentP(LSTRUCT *lstruct, MLEFL **p);
RET_CODE ExtractLMDamping(LSTRUCT *lstruct, 
			MLEFL *lambda, MLEFL *beta, 
			MLEFL *gamma, MLEFL *rhobar, 
			MLEFL *nu, int *q, int *step_count, int *step_mode);

/* basic functions - under construction */

RET_CODE thLstructGetNX(LSTRUCT *lstruct, LSECTOR sector, int *nx);
RET_CODE thLstructGetNModel(LSTRUCT *lstruct, int *nmodel);
RET_CODE thLstructGetNPar(LSTRUCT *lstruct, int *npar);
RET_CODE thLstructGetLmachine(LSTRUCT *lstruct, LMACHINE **lmachine);
RET_CODE thLstructGetPN(LSTRUCT *lstruct, MLEFL **pN);
RET_CODE thLstructGetLwmask (LSTRUCT *lstruct, LWMASK **lwmask);
RET_CODE thLstructGetChisq(LSTRUCT *lstruct, CHISQFL *chisq, CHISQFL *cost);
RET_CODE thLstructGetChisqN(LSTRUCT *lstruct, CHISQFL *chisqN, CHISQFL *costN);
RET_CODE thLstructGetAmp(LSTRUCT *lstruct, MLEFL **amp);
RET_CODE thLstructGetAmpN(LSTRUCT *lstruct, MLEFL **amp);
RET_CODE thLstructAssignDa(LSTRUCT *lstruct);

RET_CODE UpdateChisq(LSTRUCT *lstruct);
RET_CODE CopyChisqNChisq(LSTRUCT *lstruct);

RET_CODE thLstructGetFpack(LSTRUCT *lstruct, int i, LFPACK **fpack);
RET_CODE thConstructImagePack(LSTRUCT *lstruct, int i, LIMPACK *impack);
RET_CODE thGetFParam(LMACHINE *lmachine, int i, void **fparam);
RET_CODE thDelFparam(LMACHINE *lmachine, int i, void *fparam);

RET_CODE thLMijMklPut(LSTRUCT *lstruct, int i, int j, int k, int l, MLEFL x);
RET_CODE thLMijMklmPut(LSTRUCT *lstruct, int i, int j, int k, int l, int m, MLEFL x);
RET_CODE thLMijMkPut(LSTRUCT *lstruct, int i, int j, int k, MLEFL x);
RET_CODE thLMijDmmPut(LSTRUCT *lstruct, int i, int j, MLEFL x);
RET_CODE thLMijDPut(LSTRUCT *lstruct, int i, int j, MLEFL x);
RET_CODE thLDDPut(LSTRUCT *lstruct, MLEFL x);

RET_CODE thLMijMklmGet(LSTRUCT *lstruct, int i, int j, int k, int l, int m, MLEFL *x);
RET_CODE thLMijMklGet(LSTRUCT *lstruct, int i, int j, int k, int l, MLEFL *x);
RET_CODE thLMijMkGet(LSTRUCT *lstruct, int i, int j, int k, MLEFL *x);
RET_CODE thLMijDmmGet(LSTRUCT *lstruct, int i, int j, MLEFL *x);
RET_CODE thLMijDGet(LSTRUCT *lstruct, int i, int j, MLEFL *x);
RET_CODE thLDDGet(LSTRUCT *lstruct, MLEFL *x);

RET_CODE thLExtractMiMj(LSTRUCT *lstruct, MLEFL ***mimj);
RET_CODE thExtractBrackets(LSTRUCT *lstruct, 
	MLEFL **mid, MLEFL **midmm, 
	MLEFL ***mimj, MLEFL ***mijdmm, 
	MLEFL ****mimjk, MLEFL *****mijmkl);
RET_CODE thExtractMi(LSTRUCT *lstruct, int i, THREGION **Mi);
RET_CODE thExtractMij(LSTRUCT *lstruct, int i, int j, THREGION **Mij);
RET_CODE thExtractMijk(LSTRUCT *lstruct, int i, int j, int k, THREGION **Mijk);
RET_CODE thExtractD(LSTRUCT *lstruct, REGION **D);
RET_CODE thExtractM(LSTRUCT *lstruct, REGION **M); 
RET_CODE thExtractDmm(LSTRUCT *lstruct, REGION **dmm);
RET_CODE thExtractJtj(LSTRUCT *lstruct, LSECTOR sector, MLEFL ***Jtj);
RET_CODE thLExtractJtDDm(LSTRUCT *lstruct, LSECTOR sector, MLEFL **jtddm);
RET_CODE thExtractJtdmm(LSTRUCT *lstruct, LSECTOR sector, MLEFL **Jtdmm);

MLEFL thBracket(REGION *a, LWMASK *lwmask, REGION *b, RET_CODE *status);
MLEFL thBracketOne(REGION *a, LWMASK *lwmask, RET_CODE *status);
MLEFL thOneBracketOne(LWMASK *lwmask, RET_CODE *status);
MLEFL thReg3BracketReg(THREGION3 *a, LWMASK *lwmask, REGION *b, RET_CODE *status);
MLEFL thReg3BracketReg3(THREGION3 *a, LWMASK *lwmask, THREGION3 *b, RET_CODE *status);
MLEFL thRegHalfBracket(REGION *reg1, LWMASK *lwmask, THREGION *threg2, RET_CODE *status);
MLEFL thRegBracket(THREGION *a, LWMASK *lwmask, THREGION *b, RET_CODE *status);

RET_CODE thPutModelComponent(REGION *m, REGION *mi, FL32 amp);
RET_CODE thAddModelComponent(REGION *m, REGION *mi, FL32 amp);
RET_CODE thRegPutModelComponent(REGION *m, THREGION *mi, FL32 amp);
RET_CODE thRegAddModelComponent(REGION *m, THREGION *mi, FL32 amp);


/* io-translation - under construction: the one issue to be decided is to whether use name of the models in translation of argument names or not */


RET_CODE thLDumpCov(LSTRUCT *lstruct);
RET_CODE thLDumpX(LSTRUCT *lstruct, ASSIGNFLAG aflag, LSECTOR sector);
RET_CODE thLDumpAmp(LSTRUCT *lstruct, ASSIGNFLAG aflag);
RET_CODE thLDumpPar(LSTRUCT *lstruct, ASSIGNFLAG aflag);
RET_CODE thObjcTransIo(THOBJC *objc, ASSIGNFLAG aflag);
RET_CODE thLXDumpIo(LSTRUCT *lstruct, ASSIGNFLAG aflag);
RET_CODE thLXDumpModel(LSTRUCT *lstruct, ASSIGNFLAG aflag);

RET_CODE ExtractDX2(LSTRUCT *lstruct, LSECTOR sector, MLEFL **dx, int *n);
RET_CODE ExtractXxNdX(LSTRUCT *lstruct, LSECTOR sector, MLEFL **xold, MLEFL **xT, MLEFL **x, MLEFL **xN, MLEFL **dx, int *n);
RET_CODE ExtractMcount(LSTRUCT *lstruct, LSECTOR sector, MLEFL ***mcount, MLEFL ***mcountN);
RET_CODE thExtractNx(LSTRUCT *lstruct, LSECTOR sector, int *nx);

RET_CODE thLstructSetXupdate(LSTRUCT *lstruct, XUPDATE update, LSECTOR sector);
RET_CODE thLstructGetXupdate(LSTRUCT *lstruct, XUPDATE *update, LSECTOR sector);

/* Algorithm handling */
RET_CODE thFreeImpackMemory(LIMPACK *impack);

/* testing the fit */
LTEST *thLtestNew();
void thLtestDel(LTEST *ltest);

/* inclusion of the algorithm */
LALG *thLalgNew();
void thLalgDel(LALG *lalg);

/* New put and get commands for Lstruct */
RET_CODE thLstructPutLmachine(LSTRUCT *lstruct, LMACHINE *lmachine);
RET_CODE thLstructGetLwork(LSTRUCT *lstruct, LWORK **lwork);
RET_CODE thLstructGetImpacks(LSTRUCT *lstruct, LIMPACK ***impacks);
RET_CODE thLstructGetAlgByStage(LSTRUCT *lstruct, LSTAGE stage, ALGORITHM **alg);
RET_CODE thLstructGetConvergence(LSTRUCT *l, int *n, CONVERGENCE *c, LM_STEP_STATUS *step_status);

RET_CODE thLstructGetInitmode(LSTRUCT *l, LINITMODE *imode);
RET_CODE thLstructPutInitmode(LSTRUCT *l, LINITMODE imode);
RET_CODE thLstructGetImagemode(LSTRUCT *l, LIMAGEMODE *imode);
RET_CODE thLstructPutImagemode(LSTRUCT *l, LIMAGEMODE imode);

RET_CODE thLstructGetLdata(LSTRUCT *l, LDATA **ldata);
RET_CODE thLstructGetLmodel(LSTRUCT *l, LMODEL **lmodel);

RET_CODE thLstructCopyLastparchain(LSTRUCT *l, CHAIN **pChain, unsigned int cmode);

RET_CODE thLstructGetPrivate(LSTRUCT *l, void **priv, char **privtype);
RET_CODE thLstructPutPrivate(LSTRUCT *l, void *priv, char *privtype);
RET_CODE thLstructDelPrivate(LSTRUCT *l);

RET_CODE thLstructGetSource(LSTRUCT *l, void **source);
RET_CODE thLstructPutSource(LSTRUCT *l, void *source);
RET_CODE thLstructPutLwmask(LSTRUCT *l, LWMASK *lwmask);

RET_CODE thLstructGetMN(LSTRUCT *l, REGION **mN);
RET_CODE thLmodelGetLmachine(LMODEL *lmodel, LMACHINE **lmachine);
RET_CODE thLdataGetLpars(LDATA *ldata, int *nrow, int *ncol);

RET_CODE thLstructCopy(LSTRUCT *lsource, LSTRUCT *ltarget, int cmode);

RET_CODE thLdataCopy(LDATA *s, LDATA *t);
RET_CODE thLworkCopy(LWORK *s, LWORK *t);
RET_CODE thLprofileCopy(LPROFILE *s, LPROFILE *t, int full);
RET_CODE thLmethodCopy(LMETHOD *s, LMETHOD *t, int full);
RET_CODE thLtestCopy(LTEST *s, LTEST *t);
RET_CODE thLalgCopy(LALG *s, LALG *t);
RET_CODE thLmodelCopy(LMODEL *s, LMODEL *t);
RET_CODE thLmodvarCopy(LMODVAR *s, LMODVAR *t);

RET_CODE thLworkPut(LWORK *lwork, PSFMODEL *psf_model);
RET_CODE thLmethodPutCflag(LMETHOD *lm, CONVERGENCE cflag);

RET_CODE thObjcDumpCounts(THOBJC *objc);
RET_CODE thLDumpCountsIo(LSTRUCT *lstruct);

RET_CODE thLdataGetPsfModel(LDATA *ld, PSFMODEL **psf);
RET_CODE thLGetCrudeCalib(LSTRUCT *l, CRUDE_CALIB **cc);

RET_CODE thLstructGetLCalib(LSTRUCT *l, LCALIB **lc);
RET_CODE thLCalibGetType(LCALIB *lc, CALIBTYPE *type);
RET_CODE thLCalibPutType(LCALIB *lc, CALIBTYPE type);
RET_CODE thLCalibGetCrudeCalib(LCALIB *lc, CRUDE_CALIB **cc);
RET_CODE thLCalibPutCrudeCalib(LCALIB *lc, CRUDE_CALIB *cc);

LCALIB *thLCalibNew();
void thLCalibDel(LCALIB *lc);

LPROFILE *thLprofileNew();
void thLprofileDel(LPROFILE *x);

LFIT *thLfitNew();
void thLfitDel(LFIT *lf);
RET_CODE thLfitAlloc(LFIT *lf, LFITRECORDTYPE rtype);
RET_CODE thLfitDealloc(LFIT *lf);
RET_CODE thLfitRefresh(LFIT *lf);
RET_CODE thLfitAddLmethod(LFIT *lf, LMETHOD *lm, XUPDATE update);

RET_CODE thLstructGetLfit(LSTRUCT *l, LFIT **lf);
RET_CODE thLstructGetLmethod(LSTRUCT *l,LMETHOD **lm);
RET_CODE thLfitGetInitFiniLM(LFIT *lf, LMETHOD **init, LMETHOD **fini);
RET_CODE thLmethodPutChisq(LMETHOD *lm, CHISQFL chisq, CHISQFL cost);
RET_CODE thLmethodGetChisq(LMETHOD *lm, CHISQFL *chisq, CHISQFL *cost);
RET_CODE thLmethodPutStatus(LMETHOD *lm, LM_STEP_STATUS step_status, int step_count, int step_mode);
RET_CODE thLmethodPutStepCount(LMETHOD *lm, int step_count, int step_mode);
RET_CODE thLmethodGetType(LMETHOD *lm, LMETHODTYPE *type, LMETHODSUBTYPE *wtype);
RET_CODE thLmethodPutType(LMETHOD *lm, LMETHODTYPE type, LMETHODSUBTYPE wtype);
RET_CODE thLmethodGetStepCount(LMETHOD *lm, int *step_count, int *step_mode);
RET_CODE thLmethodGetDeltaQn(LMETHOD *lm, MLEFL *delta_qn);
RET_CODE thLmethodPutDeltaQn(LMETHOD *lm, MLEFL delta_qn);

RET_CODE thLFitGetChisqMin(LFIT *lf, CHISQFL *chisq_min, CHISQFL *cost_min, int *record_count);
RET_CODE thLfitGetRtype(LFIT *lf, LFITRECORDTYPE *rtype);
RET_CODE thLfitPutRtype(LFIT *lf, LFITRECORDTYPE rtype);
RET_CODE thLfitGetNLprofile(LFIT *lf, int *nlp, int *nlpmax);
RET_CODE thLfitPutNLprofile(LFIT *lf, int nlp);
RET_CODE thLfitGetLprofileByIndex(LFIT *lf, int index, LPROFILE **lprofile);

RET_CODE thLCGet(LSTRUCT *lstruct, CHISQFL *pcost);
RET_CODE thLPiCGet(LSTRUCT *lstruct, int i, CHISQFL *dpcost);
RET_CODE thLPiPjCGet(LSTRUCT *lstruct, int i, int j, CHISQFL *ddpcost);

RET_CODE thLGetObjcRnamesPndex(LSTRUCT *lstruct, void *objc, char ***rnames, int *nrnames,int **pndex, int *npndex);
RET_CODE thLstructModelIndexGetByThprop(LSTRUCT *l, THPROP *prop, int *imodel);

RET_CODE thLworkGetWpsfwing(LWORK *lwork, PSFWING **wpsfwing);
RET_CODE thLworkGetWpsf(LWORK *lwork, WPSF **wpsf);
RET_CODE thTransModelPars(THOBJC *objc, char*pioname, void *pio, ASSIGNFLAG aflag);
RET_CODE thLprofileAddLmethodInfo(LPROFILE *lprofile, LMETHOD *lmethod);

RET_CODE thLworkCopyJBqnPrev(LWORK *lwork);
RET_CODE thLworkGetQN(LWORK *lwork, 
	MLEFL ***Bk, MLEFL ***Bkplus1, MLEFL ***BBk, MLEFL ***BBkplus1,
	MLEFL **dfk, MLEFL **dfkplus1, 
	MLEFL ***Hk, MLEFL ***Hkplus1, MLEFL ***HHk, MLEFL ***HHkplus1,
	MLEFL **dxk, int *n, LSECTOR sector);
RET_CODE thLworkGetQNWorkSpace(LWORK *lwork, MLEFL **wqny, MLEFL **wqnz, int *n, LSECTOR sector);

RET_CODE thLworkGetX(LWORK *lwork, MLEFL **x, int *n, LSECTOR sector);
RET_CODE thLworkGetXN(LWORK *lwork, MLEFL **x, int *n, LSECTOR sector);
RET_CODE thLworkGetWMatVec(LWORK *lwork, LSECTOR sector, MAT **wMat, MAT **wwMat, VEC ***wVec);

RET_CODE UpdateQnPrevFromQn(LSTRUCT *lstruct, LSECTOR sector);

RET_CODE thLworkGetMemSize(LWORK *lw, MEMDBLE *memory);
RET_CODE thLimpackGetMemSize(LIMPACK *impack, MEMDBLE *memory);
RET_CODE shRegGetMemSize(REGION *reg, MEMDBLE *memory);
RET_CODE thWpsfGetMemSize(WPSF *wpsf, MEMDBLE *memory);
RET_CODE thPsfModelGetMemSize(PSFMODEL *psf_model, MEMDBLE *memory);
RET_CODE thPsfRegGetMemSize(PSF_REG *psfreg, MEMDBLE *memory);
RET_CODE thPsfWingGetMemSize(PSFWING *psfwing, MEMDBLE *memory);
RET_CODE thFftwKernelGetMemsize(FFTW_KERNEL *fftw_kernel, MEMDBLE *memory);

RET_CODE thLworkCopyJAdaDeltaPrev(LWORK *lwork, LMETHOD *lm);
RET_CODE thLworkGetEGgAdaDelta(LWORK *lwork, LSECTOR sector, MLEFL **E_gg, MLEFL **E_gg_prev, int *nx);
RET_CODE thLworkGetRmsGAdaDelta(LWORK *lwork, LSECTOR sector, MLEFL **rms_g, MLEFL **rms_g_prev, int *nx);
RET_CODE thLworkGetRmsDxAdaDelta(LWORK *lwork, LSECTOR sector, MLEFL **rms_dx);
RET_CODE thLworkPutRmsDxAdaDelta(LWORK *lwork, LSECTOR sector, MLEFL *rms_dx);

RET_CODE thLworkGetRhoAdaDelta(LWORK *lwork, MLEFL *rho_adadelta, MLEFL *epsilon_adadelta_dx, MLEFL *epsilon_adadelta_g);
RET_CODE thLworkGetJtdmm(LWORK *lwork, LSECTOR sector, MLEFL **jtdmm, int *nx);
RET_CODE thLworkGetJtj(LWORK *lwork, LSECTOR sector, MLEFL ***jtj, int *nx);
RET_CODE thLworkGetWJtj(LWORK *lwork, LSECTOR sector, MLEFL ***wjtj, int *nx);

RET_CODE thLworkGetRSV(LWORK *lwork, MLEFL **dfk, MLEFL **dfkplus1, MLEFL **dxk, int *n, LSECTOR sector);
RET_CODE thLworkGetPGD(LWORK *lwork, MLEFL **dfk, MLEFL **dfkplus1, MLEFL **dxk, int *n, LSECTOR sector);
RET_CODE thLworkGetMGD(LWORK *lwork, MLEFL **dfk, MLEFL **dfkplus1, MLEFL **dxk, int *n, LSECTOR sector);
RET_CODE thLworkGetGD(LWORK *lwork, MLEFL **dfk, MLEFL **dfkplus1, MLEFL **rms_g, MLEFL **rms_ginv, MLEFL **rms_dx, MLEFL **dx, int *n, LSECTOR sector);
RET_CODE thLmethodGetEta(LMETHOD *lmethod, MLEFL *eta);
RET_CODE thLmethodPutEta(LMETHOD *lmethod, MLEFL eta);

RET_CODE thLmethodGetRhoAdaDelta(LMETHOD *lm, MLEFL *rho_adadelta, MLEFL *epsilon_adadelta_dx, MLEFL *epsilon_adadelta_g);

RET_CODE thLworkGetEDxdx(LWORK *lwork, LSECTOR sector, MLEFL **E_dxdx, MLEFL **E_dxdx_prev, int *nx);
RET_CODE thLworkPutEDxdx(LWORK *lwork, LSECTOR sector, MLEFL *E_dxdx);

RET_CODE UpdateEAdaDelta(LWORK *lwork, LMETHOD *lm);
RET_CODE thLworkGetDx(LWORK *lwork, LSECTOR sector, MLEFL **dx, int *nx);

RET_CODE thLworkGetDaDp(LWORK *lwork, MLEFL ***DaDp, MLEFL ***DaDpT, MLEFL ***wN, MLEFL ***wA, MLEFL ***wwN, MLEFL **wC, MAT **wQ1, MAT **wQ2, VEC **wX, int *nmodel, int *nparam);

RET_CODE thLmethodGetMGD(LMETHOD *lmethod, MLEFL *momentum, MLEFL *lr_gd, LMETHODSUBTYPE *wmethod_type);
RET_CODE thLmethodGetPGD(LMETHOD *lmethod, 
	int *count_PGD_noise, int *count_PGD_thresh, 
	int *k_PGD_noise, int *k_PGD_thresh, 
	int *perturbation_PGD_condition, MLEFL *perturbation_PGD_size, 
	MLEFL *mag_Fprime_PGD_thresh, MLEFL *eta_max, 
	MLEFL *cost_PGD_noise, MLEFL *chisq_PGD_noise, MLEFL *cost_PGD_thresh, 
	LMETHODSUBTYPE *wmethod_type);

RET_CODE thLmethodPGDPut(LMETHOD *lmethod, int k_PGD_noise, int perturbation_PGD_condition, MLEFL eta_gd_max, MLEFL cost_PGD_noise, MLEFL chisq_PGD_noise, LMETHODSUBTYPE wmethod_type);
RET_CODE thLmethodRSVPut(LMETHOD *lmethod, LMETHODSUBTYPE wmethod_type);

RET_CODE thLmethodGetFprimeIncidence(LMETHOD *lmethod, int *k_fprime_incidence);
RET_CODE thLmethodPutFprimeIncidence(LMETHOD *lmethod, int k_fprime_incidence);

#endif

