#include "strings.h"
#include "dervish.h"
#include "sohoEnv.h"
#include "shDebug.h"


MASK *shMaskReadFpm(char *file,  
		     const int row0,  const int column0,  
		     const int row1,  const int column1, 
		     RET_CODE *shStatus);

RET_CODE shWriteFpm(char *file, 
		    MASK *mask);

RET_CODE shCpFpm(char *infile, char *outfile, /* input and output filenames */
		 const int row0, const int column0, 
		 const int row1, const int column1 /* corners of the image in INFILE */);
