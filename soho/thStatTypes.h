#ifndef THSTATTYPES_H
#define THSTATTYPES_H

#include "thConsts.h"
#include "thBasic.h"
#include "thMathTypes.h"

#include "fftw.h"

#define thBasicStatNew thBasic_statNew
#define thBasicStatDel thBasic_statDel

typedef struct basic_stat {
	int n;
	float mean, s;
	float median, iqr, q1, q3;
	float min, max, range;
} BASIC_STAT; /* pragma IGNORE */

typedef struct basicstat {
	int n;
	THPIX mean, s;
	THPIX median, iqr, q1, q3;
	THPIX min, max, range;
} BASICSTAT; /* pragma IGNORE */

BASIC_STAT *thBasic_statNew();
void thBasic_statDel(BASIC_STAT *stat);
void thBasicStatInit(BASIC_STAT *x);

BASICSTAT *thBasicstatNew();
void thBasicstatDel(BASICSTAT *stat);

#endif

