#ifndef THPCOST_H
#define THPCOST_H

#include "thPCostTypes.h"
#include "thObjcTypes.h"
#include "thMGalaxyTypes.h"

void thPCostInit();
void thPCostFini();

RET_CODE check_object_validity(THOBJC *objc, int *valid);

RET_CODE cD_pcost(THOBJC *objc, CHISQFL *pcost);
RET_CODE cD_DpcostDhalo(THOBJC *objc, CHISQFL *pcost);
RET_CODE cD_DpcostDcore(THOBJC *objc, CHISQFL *pcost); 
RET_CODE cD_DDpcostDhaloDhalo(THOBJC *objc, CHISQFL *pcost);
RET_CODE cD_DDpcostDhaloDcore(THOBJC *objc, CHISQFL *pcost);
RET_CODE cD_DDpcostDcoreDcore(THOBJC *objc, CHISQFL *pcost);

RET_CODE cD_DpcostDuhalo(THOBJC *objc, CHISQFL *pcost);
RET_CODE cD_DpcostDucore(THOBJC *objc, CHISQFL *pcost); 
RET_CODE cD_DDpcostDuhaloDuhalo(THOBJC *objc, CHISQFL *pcost);
RET_CODE cD_DDpcostDuhaloDucore(THOBJC *objc, CHISQFL *pcost);
RET_CODE cD_DDpcostDucoreDucore(THOBJC *objc, CHISQFL *pcost);

RET_CODE cD_DpcostDehalo(THOBJC *objc, CHISQFL *pcost);
RET_CODE cD_DpcostDecore(THOBJC *objc, CHISQFL *pcost);
RET_CODE cD_DDpcostDecoreDecore(THOBJC *objc, CHISQFL *pcost);
RET_CODE cD_DDpcostDehaloDehalo(THOBJC *objc, CHISQFL *pcost);
RET_CODE cD_DDpcostDehaloDecore(THOBJC *objc, CHISQFL *pcost);

RET_CODE cD_DpcostDEhalo(THOBJC *objc, CHISQFL *pcost);
RET_CODE cD_DpcostDEcore(THOBJC *objc, CHISQFL *pcost);
RET_CODE cD_DDpcostDEcoreDEcore(THOBJC *objc, CHISQFL *pcost);
RET_CODE cD_DDpcostDEhaloDEhalo(THOBJC *objc, CHISQFL *pcost);
RET_CODE cD_DDpcostDEhaloDEcore(THOBJC *objc, CHISQFL *pcost);

RET_CODE cDeV_pcost_generic(THOBJC *objc, PCOST_GENERIC *pcost_all);
RET_CODE cDeV_pcost(THOBJC *objc, CHISQFL *pcost);
RET_CODE cDeV_DpcostDcore(THOBJC *objc, CHISQFL *pcost);
RET_CODE cDeV_DDpcostDcoreDcore(THOBJC *objc, CHISQFL *pcost);
RET_CODE cDeV_DpcostDucore(THOBJC *objc, CHISQFL *pcost);
RET_CODE cDeV_DDpcostDucoreDucore(THOBJC *objc, CHISQFL *pcost);
RET_CODE cDeV_DpcostDecore(THOBJC *objc, CHISQFL *pcost);
RET_CODE cDeV_DDpcostDecoreDecore(THOBJC *objc, CHISQFL *pcost);
RET_CODE cDeV_DDpcostDEcoreDEcore(THOBJC *objc, CHISQFL *pcost);
RET_CODE cDeV_DpcostDEcore(THOBJC *objc, CHISQFL *pcost);

#endif
