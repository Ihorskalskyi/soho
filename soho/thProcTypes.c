#include "thProcTypes.h"

static int is_wobjc_match(WOBJC_IO *wobjc1, WOBJC_IO *wobjc2);

FRAMEDATA *thFramedataNew() {

  char *name = "thFramedataNew";

  FRAMEDATA *data;
  data = (FRAMEDATA *) thCalloc(1, sizeof(FRAMEDATA));

 return(data);

}

void thFramedataDel(FRAMEDATA *fd) {

  /* deleting should have an option to state how deep one should go
     the shallow option keeps the basis models for sky and objects and
     deleets the chains around them
  */

  char *name = "thFramedataDel";

  thError("%s: temporary deconstructor - should not be included in the dispatched binary", name);

 return;

}

FRAMEFILES *thFramefilesNew() {
  
  FRAMEFILES *ff;
  ff = (FRAMEFILES *) thCalloc(1, sizeof(FRAMEFILES));
  ff->workspace = (char *) thCalloc(FRAMEFILES_SIZE * MX_STRING_LEN, sizeof(char));
  char *workspace = ff->workspace;
  int i = 0;
  ff->phconfigfile = workspace + i * MX_STRING_LEN; i++;
  ff->phecalibfile =workspace + i * MX_STRING_LEN; i++;
  ff->phflatframefile =  workspace + i * MX_STRING_LEN; i++;
  ff->phmaskfile = workspace + i * MX_STRING_LEN; i++;
  ff->phatlasfile = workspace + i * MX_STRING_LEN; i++;
  ff->phsmfile = workspace + i * MX_STRING_LEN; i++;
  ff->phpsfile = workspace + i * MX_STRING_LEN; i++;
  ff->phobjcfile = workspace + i * MX_STRING_LEN; i++;
  ff->photoObjcfile = workspace + i * MX_STRING_LEN; i++;
  ff->phcalibfile = workspace + i * MX_STRING_LEN; i++;
  ff->gcfile = workspace + i * MX_STRING_LEN; i++;
  ff->thsbfile = workspace + i * MX_STRING_LEN; i++;
  ff->thobfile = workspace + i * MX_STRING_LEN; i++;
  ff->thmaskselfile = workspace + i * MX_STRING_LEN; i++;
  ff->thobjcselfile = workspace + i * MX_STRING_LEN; i++;
  ff->thsmfile = workspace + i * MX_STRING_LEN; i++;
  ff->thpsfile = workspace + i * MX_STRING_LEN; i++;
  ff->thobjcfile = workspace + i * MX_STRING_LEN; i++;
  ff->thflatframefile = workspace + i * MX_STRING_LEN; i++;
  ff->thlmfile = workspace + i * MX_STRING_LEN; i++;
  ff->thlpfile = workspace + i * MX_STRING_LEN; i++;
  ff->thlrfile = workspace + i * MX_STRING_LEN; i++;
  ff->thlzfile = workspace + i * MX_STRING_LEN; i++;
  ff->thlwfile = workspace + i * MX_STRING_LEN; i++;
  ff->thpsfreportfile = workspace + i * MX_STRING_LEN; i++;
  ff->thpsfstatfile = workspace + i * MX_STRING_LEN; i++;

/* the following was added to allow multi-band multi-object simulations - Jan 26, 2016 */
ff->outphflatframefileformat = workspace + i * MX_STRING_LEN; i++;
ff->inphmaskfileformat = workspace + i * MX_STRING_LEN; i++;
ff->inphsmfileformat = workspace + i * MX_STRING_LEN; i++;
ff->outphsmfileformat= workspace + i * MX_STRING_LEN; i++;
ff->inphobjcfile =  workspace + i * MX_STRING_LEN; i++;
ff->incalibobjcfile =  workspace + i * MX_STRING_LEN; i++;
ff->outphobjcfile =  workspace + i * MX_STRING_LEN; i++;
ff->outthsmfileformat =  workspace + i * MX_STRING_LEN; i++;
ff->outthobjcfileformat = workspace + i * MX_STRING_LEN; i++;
ff->outthflatframefileformat = workspace + i * MX_STRING_LEN; i++;

ff->outphflatframefile= workspace + i * MX_STRING_LEN; i++;
ff->inphsmfile= workspace + i * MX_STRING_LEN; i++;
ff->outphsmfile= workspace + i * MX_STRING_LEN; i++;
ff->outthsmfile= workspace + i * MX_STRING_LEN; i++;
ff->outthobjcfile= workspace + i * MX_STRING_LEN; i++;
ff->outthflatframefile= workspace + i * MX_STRING_LEN; i++;


ff->inphflatframefileformat = workspace + i * MX_STRING_LEN; i++;
ff->inthflatframefileformat = workspace + i * MX_STRING_LEN; i++;
ff->inphflatframefile= workspace + i * MX_STRING_LEN; i++;
ff->inthflatframefile= workspace + i * MX_STRING_LEN; i++;

ff->outlMfileformat = workspace + i * MX_STRING_LEN; i++;
ff->outlPfileformat = workspace + i * MX_STRING_LEN; i++;
ff->outlRfileformat = workspace + i * MX_STRING_LEN; i++;
ff->outlZfileformat = workspace + i * MX_STRING_LEN; i++;
ff->outlWfileformat = workspace + i * MX_STRING_LEN; i++;
ff->outpsfreportfileformat = workspace + i * MX_STRING_LEN; i++;
ff->outpsfstatsfileformat = workspace + i * MX_STRING_LEN; i++;

ff->outdir = workspace + i * MX_STRING_LEN; i++;
#if SET_DEFAULT_OUTDIR
strcpy(ff->outdir, ".");
#endif
/* making sure there is no memory error */
shAssert(i < FRAMEFILES_SIZE);

  return(ff);
  
}

void thFramefilesDel(FRAMEFILES *ff) {
  
  if (ff == NULL) return; 
  thFree(ff->workspace); 
  thFree(ff);

  return;

}

RET_CODE thFramefilesGetOutdir(FRAMEFILES *ff, char **outdir) {
char *name = "thFramefileGetOutdir";
if (outdir == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (ff == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
*outdir = ff->outdir;
if (*outdir == NULL) {
	thError("%s: WARNING - improperly allocated (framefiles) - will face a problem soon", name);
}
return(SH_SUCCESS);
}


FRAMEID *thFrameidNew() {
  
  char *name = "thFrameidNew";

  FRAMEID *id;
  id = (FRAMEID *) thCalloc(1, sizeof(FRAMEID));
  id->nrow = UNKNOWN_NROW;
  id->ncol = UNKNOWN_NCOL;
  
  return(id);
}

void thFrameidDel(FRAMEID *id) {

  char *name = "thFrameidDel";
  
  if (id == NULL) return;

  thFree(id);
  
  return;
}

PHSTAT *phStatNew() {
	PHSTAT *stat;
	stat = thCalloc(1, sizeof(PHSTAT));
	stat->sky = thBasicStatNew();
	return(stat);
}

void phStatDel(PHSTAT *stat) {
	if (stat == NULL) return;
	thBasicStatDel(stat->sky);
	return;
}

void phStatInit(PHSTAT *stat) {
	if (stat == NULL) return;
	thBasicStatInit(stat->sky);
	return;
}

FRAMESTAT *thFramestatNew() {
	FRAMESTAT *stat = thCalloc(1, sizeof(FRAMESTAT));
  	int i;
	stat->phstat = thCalloc(OBJ_NTYPE, sizeof(PHSTAT *));
	stat->phcalibstat = thCalloc(OBJ_NTYPE, sizeof(PHSTAT *));
	for (i = 0; i < OBJ_NTYPE; i++) {
		stat->phstat[i] = phStatNew();
		stat->phcalibstat[i] = phStatNew();
	}
	stat->accphstat = phStatNew();
	return(stat);
}

void thFramestatDel(FRAMESTAT *stat) {
	if (stat == NULL) return;
	phStatDel(stat->accphstat);
	int i;
	if (stat->phstat != NULL) {
		for (i = 0; i < OBJ_NTYPE; i++) {
			phStatDel(stat->phstat[i]);
		}
		thFree(stat->phstat);
	}
	if (stat->phcalibstat != NULL) {
		for (i = 0; i < OBJ_NTYPE; i++) {
			phStatDel(stat->phcalibstat[i]);
		}
		thFree(stat->phcalibstat);
	}
	return;
}	

FRAMEWORK *thFrameworkNew() {
 
   FRAMEWORK *fw;
   fw = thCalloc(1, sizeof(FRAMEWORK));
   return(fw);
}

void thFrameworkDel(FRAMEWORK *fw) {

if (fw == NULL) return;
int i;
for (i = 0; i < MAX_FITID; i++) {
	if (fw->lstruct[i] != NULL) thLstructDel(fw->lstruct[i]);
	if (fw->map[i] != NULL) thLmachineDel(fw->map[i]);
}

thFree(fw);
return;
}


FRAME *thFrameNew() {

  char *name = "thFrameNew";

  FRAME *f;
  f = (FRAME *) thMalloc(sizeof(FRAME));

  f->id = (FRAMEID *) thFrameidNew();
  f->files = (FRAMEFILES *) thFramefilesNew();
  f->data  = (FRAMEDATA *) thFramedataNew();
  f->sky   = NULL;
  f->psf   = NULL; /* the psf-module is written as to create this each time 
			(PSFMODEL *) thPsfmodelNew();
			*/
  f->objcs =  shChainNew("OBJCMODEL");
  f->work = thFrameworkNew();
  f->io = NULL;
  f->io_chain = shChainNew("FRAMEIO");
  f->stat = thFrameStatNew();
 
  f->proc = NULL;

  thError("%s: WARNING - did not construct field (sky)", name);

  return(f);
  
}

void thFrameDel(FRAME *f) {
  
  if (f == NULL) return;
  
  thFrameidDel(f->id);
  thFramefilesDel(f->files);

  if (f->data != NULL) {

    if (f->proc == NULL) {
      thFramedataDel(f->data);
    } else {
      CHAIN *chain[2];
      SKYBASISSET *sbs;

      sbs = f->data->sbs;
      
      chain[0] = NULL;
      if (sbs != NULL) {
	chain[0] = sbs->basis;
      }
      f->data->sbs = NULL;
      chain[1] = f->data->objcbs;
      f->data->objcbs = NULL;
      
      /* removing all the structures specific to a frame */

      /* AMPLs are specific to frames */

      if (chain[0] != NULL) {

	int i, pos = 0;
	SKYBASIS *sb;

	for (i = 0; i < shChainSize(chain[0]); i++) {
	  sb = (SKYBASIS *) shChainElementGetByPos(chain[0], pos);
	  if (sb->type == TYPE_AMPL) {
	    sb = (SKYBASIS *) shChainElementRemByPos(chain[0], pos);
	    thSkybasisDel(sb);
	  } else {
	    sb->val = NULL;
	    pos++;
	  }
	  
	}
      			       
      }
      
      /* 
	 other structures (FUNCs) should not be removed because they are 
	 in a proc tank; the CHAINs themselves are constructed within
	 the frame and so they should be deleted.
      */
      
      thSkybasissetDel(sbs);
      if (chain[1] != NULL) {
	shChainDel(chain[1]);
      }
      /* now delete the framedata */
      thFramedataDel(f->data);
    }
  }

  
  if (f->sky != NULL) {
    f->sky->basis = NULL; /* already deleted */
    thSkymodelDel(f->sky);
  }

#if 0 /* searching for a memory bug - jan 23, 2018 */
  if (f->psf != NULL) thPsfmodelDel(f->psf);
#endif
  /* there might be a need to a deeper removal of objects */
  if (f->objcs != NULL) {
    /*thObjcmodelDel(f->objc); */
    shChainDel(f->objcs);
  }
  /* freeing the work space */
  if (f->work != NULL) thFrameworkDel(f->work);
  if (f->io_chain != NULL) {
	shChainDestroy(f->io_chain,  &thFrameioDel);
	f->io_chain = NULL;
	f->io = NULL;
  } else {
	thFrameioDel(f->io);
	f->io = NULL;
	}
  if (f->stat != NULL) thFrameStatDel(f->stat);
 
 thFree(f);

  return;

}

PROCFILES *thProcfilesNew () {

  PROCFILES *pf;
  
  pf = (PROCFILES *) thCalloc(1, sizeof(PROCFILES));

  pf->thframefile  = (char *)thCalloc(MX_STRING_LEN, sizeof(char));
  pf->thsbfile  = (char *)thCalloc(MX_STRING_LEN, sizeof(char));
  pf->thobfile  = (char *)thCalloc(MX_STRING_LEN, sizeof(char));
  pf->thmaskselfile  = (char *)thCalloc(MX_STRING_LEN, sizeof(char));
  pf->thobjcselfile  = (char *)thCalloc(MX_STRING_LEN, sizeof(char));
  pf->thsmfile  = (char *)thCalloc(MX_STRING_LEN, sizeof(char));
  pf->thobjcfile  = (char *)thCalloc(MX_STRING_LEN, sizeof(char));
  pf->thdeffiles = (char **) thCalloc(MX_STRING_ARR, sizeof(char *));
  int i;
  for (i = 0; i < MX_STRING_ARR; i++) {
    pf->thdeffiles[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }
  return(pf);

}


void thProcfilesDel(PROCFILES *pf) {

  if (pf == NULL) return;

  if (pf->thframefile  != NULL) thFree(pf->thframefile  );
  if (pf->thsbfile  != NULL) thFree(pf->thsbfile  );
  if (pf->thobfile  != NULL) thFree(pf->thobfile  );
  if (pf->thmaskselfile  != NULL) thFree(pf->thmaskselfile  );
  if (pf->thobjcselfile  != NULL) thFree(pf->thobjcselfile  );
  if (pf->thsmfile  != NULL) thFree(pf->thsmfile  );
  if (pf->thobjcfile  != NULL) thFree(pf->thobjcfile  );
  if (pf->thdeffiles != NULL) {
    int i;
    for (i = 0; i < MX_STRING_ARR; i++) {
      if (pf->thdeffiles[i] != NULL) {
	thFree(pf->thdeffiles[i]);
      }
    }
    thFree(pf->thdeffiles);
  }
  thFree(pf);
  
  return;
}

PROCESS *thProcessNew() {

  PROCESS * proc;
  proc = (PROCESS *) thCalloc(1, sizeof(PROCESS));

  proc->thprocessfile = (char *) thCalloc(MX_STRING_LEN, sizeof(char));

  return(proc);

}

void thProcessDel(PROCESS *proc) {
  
  if (proc == NULL) return;

  if (proc->thprocessfile != NULL) thFree(proc->thprocessfile);

  if (proc->files != NULL) thProcfilesDel(proc->files);

  int i;
  if (proc->frames != NULL) {
    
    for (i = 0; i < shChainSize(proc->frames); i++) {
      thFrameDel((FRAME *) shChainElementGetByPos(proc->frames, i));
    }
    shChainDel(proc->frames);
  }

  if (proc->ecalibtank != NULL) {
    shChainDestroy(proc->ecalibtank, &thFree);
  }

  if (proc->ccdconfigtank != NULL) {
    shChainDestroy(proc->ccdconfigtank, &thFree);
  }

  /* this might cause error since the same functions might be destroyed while destroying 
     object and sky basis set 
  */

  if (proc->sbftank != NULL) {
    shChainDestroy(proc->sbftank, &thFuncDel);
  }

  if (proc->obftank != NULL) {
    shChainDestroy(proc->obftank, &thFuncDel);
  }

  thFree(proc);

  return;

}


PROCTANK *thProctankNew(char *filename) {
  PROCTANK *pt;

  pt = (PROCTANK *) thMalloc(sizeof(PROCTANK));

  pt->chain = NULL;

  pt->filename = (char *) thMalloc(MX_STRING_LEN * sizeof(char));
  
  if (filename == NULL) {
    pt->filename[0] = '\0';
    return(pt);
  }

  memcpy(pt->filename, filename, strlen(filename) + 1);

  return(pt);

}

void thProctankDel(PROCTANK *pt) {
  
  if (pt->filename != NULL) thFree(pt->filename);
  
  if (pt->chain != NULL) shChainDel(pt->chain);

  thFree(pt);

}

int thProcGetNFrame(PROCESS *proc, RET_CODE *status) {
  
  if (proc == NULL) {
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(-1);
  }

  if (proc->frames == NULL) {
    if (status != NULL) *status = SH_SUCCESS;
    return(0);
  }
  
  if (status != NULL) *status = SH_SUCCESS;
  return(shChainSize(proc->frames));

}

int thFrameGetNObjcmodel (FRAME *frame, RET_CODE *status) {
  
  if (frame == NULL) {
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(-1);
  }
  
  if (frame->objcs == NULL) {
    if (status != NULL) *status = SH_SUCCESS;
    return(0);
  }
  
  if (status != NULL) *status = SH_SUCCESS;
  return(shChainSize(frame->objcs));

}

RET_CODE  thFrameGetLpars(FRAME *f, int *nm, int *np, int *npp, int *nc, int *nacc, int *nj, int *nrow, int *ncol) {
char *name = "thFrameGetLpars";
if (f == NULL) {
	thError("%s: ERROR - null (frame) cannot be accessed for structural parameters", name);
	return(SH_GENERIC_ERROR);
}

int mynrow = UNKNOWN_NROW, myncol = UNKNOWN_NCOL;
if (nrow != NULL || ncol != NULL) {
	if (f->data != NULL) {
		REGION *reg = NULL;	
		MASK *mask = NULL;

		if (f->data->image != NULL) {
			reg = f->data->image; 
		} else if (f->data->weight != NULL) {
			reg = f->data->weight;
			thError("%s: WARNING - extracting (nrow, ncol) from (weight) in (framedata) while (image) is null", name);
		} else if (f->data->thmask != NULL) {
			mask = f->data->thmask;
			thError("%s: WARNING - extracting (nrow, ncol) from (thmask) in (framedata) while (image) is null", name);
		} else if (f->data->phmask != NULL) {
			mask = f->data->phmask;
			thError("%s: WARNING - extracting (nrow, ncol) from (phmask) in (framedata) while (image) is null", name);
		} else if (f->id != NULL) {
			mynrow = f->id->nrow;
			myncol = f->id->ncol;
		}

		if (reg != NULL) {
			if (nrow != NULL) *nrow = reg->nrow;
			if (ncol != NULL) *ncol = reg->ncol;
		} else if (mask != NULL) {
			if (nrow != NULL) *nrow = mask->nrow;
			if (ncol != NULL) *ncol = mask->ncol;
		} else if (mynrow != UNKNOWN_NROW && myncol != UNKNOWN_NCOL) {
			if (nrow != NULL) *nrow = mynrow;
			if (ncol != NULL) *ncol = myncol;
		} else {
			thError("%s: ERROR - not enough information available in (framedata) to extract (nrow, ncol)", name);
			return(SH_GENERIC_ERROR);
		}
	} else {
		thError("%s: ERROR - not enough information available in (frame) to extract (nrow, ncol)", name);
		return(SH_GENERIC_ERROR);
	}
}	

RET_CODE status;
if (nm != NULL || np != NULL || npp != NULL || nc != NULL || nj != NULL || nacc != NULL) {
	LMACHINE *map;
	FRAMEWORK *work = NULL;
	status = thFrameGetFramework(f, &work);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (framework) from (frame)", name);
		return(status);
	}
	if (work == NULL) {
		thError("%s: ERROR - null (framework) found in (frame), cannot obtain structural information", name);	
		return(SH_GENERIC_ERROR);
	}
	status = thFrameworkGetMap(work, &map);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get map from  (famework)", name);
		return(status);
	} 
	if (map == NULL) {
		thError("%s: ERROR - null (lmachine) found in (framework), cannot obtain structural information", name);
		return(SH_GENERIC_ERROR);
	}
	if (map->cflag != COMPILED) {
		thError("%s: ERROR - (lmachine) is not compiled, cannot obtain structural information", name);
		return(SH_GENERIC_ERROR);
	}
	if (nm != NULL) *nm = map->namp;
	if (np != NULL) *np = map->npar;
	if (npp != NULL) *npp = map->npp;
	if (nc != NULL)  {
		int i, mc = 0;
		if (map->nmpar != NULL) {
			for (i = 0; i < map->namp; i++) {
				mc += map->nmpar[i];
			}
		}
		mc += map->namp;
		*nc = mc;
	}
	if (nacc != NULL) *nacc = map->nacc;
	if (nj != NULL) *nj = map->npar;

}
if (nrow == NULL && ncol == NULL && nm == NULL && np == NULL && npp == NULL && nc == NULL && nj == NULL) {
	thError("%s: WARNING - null placeholders, cannot return structural information", name);
}
return(SH_SUCCESS);
	
}

RET_CODE thFrameGetLdataPars(FRAME *f, REGION **image, REGION **weight, THMASK **mask, PSFMODEL **psf) {
char *name = "thFrameGetLdataPars";
if (f == NULL) {
	thError("%s: ERROR - cannot derive image information from null (frame)", name);
	return(SH_GENERIC_ERROR);
}

if (image != NULL) {
	if (f->data == NULL) {
		thError("%s: ERROR - (data) is null in (frame) -- no (image) can be extracted", name);
		return(SH_GENERIC_ERROR);
	}
	*image = f->data->image;
	if (*image == NULL) thError("%s: WARNING - null (image) in (framedata) - expect problems later on", name);
}

if (weight != NULL) {
	if (f->data == NULL) {
		thError("%s: ERROR - (data) is null in (frame) -- no (image) can be extracted", name);
		return(SH_GENERIC_ERROR);
	}
	*weight = f->data->weight;
	if (*weight == NULL) thError("%s: WARNING - null (weight) in (framedata) - expect problems later on", name);
}

if (psf != NULL) {
	*psf = f->psf;
	if (*psf == NULL) thError("%s: WARNING - null (psf) in (frame) - good for sky and test runs only", name);
	}

if (mask != NULL) {
	*mask = f->data->thmask;
}

return(SH_SUCCESS);
}

RET_CODE thFrameGetLstruct(FRAME *f, LSTRUCT **l) {
char *name = "thFrameGetLstruct";
if (l == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (f == NULL || f->work == NULL) {
	thError("%s: ERROR - null or improperly allocted input", name);
	*l = NULL;
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = thFrameworkGetLstruct(f->work, l);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lstruct) from (framework)", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE thFrameGetStamp(FRAME *f, char *stamp) {
char *name = "thFrameGetStamp";

return(SH_GENERIC_ERROR);
}

RET_CODE thFrameGetFrameio(FRAME *f, FRAMEIO **fio) {
char *name = "thFrameGetFrameio";
if (fio == NULL) {
	thError("%s: WARNING - null placeholder", name);
	return(SH_SUCCESS);
}
if (f == NULL) {
	thError("%s: ERROR - null input", name);
	*fio = NULL;
	return(SH_GENERIC_ERROR);
}
*fio = f->io;
if (*fio == NULL) {
	thError("%s: WARNING - null (frameio) - improperly allocated (frame)", name);
}
return(SH_SUCCESS);
}

RET_CODE thFrameGetFrameioChain(FRAME *f, CHAIN **io_chain) {
char *name = "thFrameGetFrameioChain";
if (io_chain == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (f == NULL) {
	thError("%s: ERROR - null input", name);
	*io_chain = NULL;
	return(SH_GENERIC_ERROR);
}
*io_chain = f->io_chain;
if (f->io_chain == NULL) {
	thError("%s: WARNING - null (io_chain) - improperly allocated (frame)", name);
}
return(SH_SUCCESS);
}


RET_CODE thFrameGetFramefiles(FRAME *f, FRAMEFILES **ff) {
char *name = "thFrameGetFramefiles";
if (ff == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (f == NULL) {
	thError("%s: ERROR - null input", name);
	*ff = NULL;
	return(SH_GENERIC_ERROR);
}
*ff = f->files;
if (*ff == NULL) {
	thError("%s: WARNING - null (framefiles) - improperly allocated (frame)", name);
}
return(SH_SUCCESS);
}

RET_CODE thFrameGetFrameid(FRAME *f, FRAMEID **id) {
char *name = "thFrameGetFrameid";
if (id == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (f == NULL) {
	thError("%s: ERROR - null input", name);
	*id = NULL;
	return(SH_GENERIC_ERROR);
}
*id = f->id;
if (*id == NULL) {
	thError("%s: WARNING - null (frameid) - improperly allocated (frame)", name);
}
return(SH_SUCCESS);
}


RET_CODE thFrameGetFramedata(FRAME *f, FRAMEDATA **data) {
char *name = "thFrameGetFramedata";
if (data == NULL) {
	thError("%s: WARNING = null output placeholder", name);
	return(SH_SUCCESS);
}
if (f == NULL) {
	thError("%s: ERROR - null input", name);
	*data = NULL;
	return(SH_GENERIC_ERROR);
}
*data = f->data;
if (*data == NULL) {
	thError("%s: WARNING - null (framedata) - improperly allocated (frame)", name);
}
return(SH_SUCCESS);
}


RET_CODE thFrameGetFramework(FRAME *f, FRAMEWORK **work) {
char *name = "thFrameGetFramework";
if (work == NULL) {
	thError("%s: WARNING = null output placeholder", name);
	return(SH_SUCCESS);
}
if (f == NULL) {
	thError("%s: ERROR - null input", name);
	*work = NULL;
	return(SH_GENERIC_ERROR);
}
*work = f->work;
if (*work == NULL) {
	thError("%s: WARNING - null (framework) - improperly allocated (frame)", name);
}
return(SH_SUCCESS);
}


RET_CODE thFrameioGetLogDump(FRAMEIO *fio, LOGDUMP **log) {
char *name = "thFrameioGetLogDump";
if (log == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (fio == NULL) {
	thError("%s: ERROR - null input", name);
	*log = NULL;
	return(SH_GENERIC_ERROR);
}
*log = fio->log;
if (*log == NULL) {
	thError("%s: WARNING - null (logdump) - improperly allocated (frameio)", name);
}
return(SH_SUCCESS);
}

RET_CODE thFrameioGetSkyDump(FRAMEIO *fio, SKYDUMP **sky) {
char *name = "thFrameioGetSkyDump";
if (sky == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (fio == NULL) {
	thError("%s: ERROR - null input", name);
	*sky = NULL;
	return(SH_GENERIC_ERROR);
}
*sky = fio->sky;
if (*sky == NULL) {
	thError("%s: WARNING - null (skydump) - improperly allocated (frameio)", name);
}
return(SH_SUCCESS);
}


RET_CODE thFrameioGetObjcDump(FRAMEIO *fio, OBJCDUMP **objc) {
char *name = "thFrameioGetObjcDump";
if (objc == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (fio == NULL) {
	thError("%s: ERROR - null input", name);
	*objc = NULL;
	return(SH_GENERIC_ERROR);
}
*objc = fio->objcs;
if (*objc == NULL) {
	thError("%s: WARNING - null (objcdump) - improperly allocated (frameio)", name);
}
return(SH_SUCCESS);
}

RET_CODE thFrameioGetLMDump(FRAMEIO *fio, LMDUMP **lm) {
char *name = "thFrameioGetLMDump";
if (lm == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (fio == NULL) {
	thError("%s: ERROR - null input", name);
	*lm = NULL;
	return(SH_GENERIC_ERROR);
}
*lm = fio->lms;
if (*lm == NULL) {
	thError("%s: WARNING - null (lmdump) - improperly allocated (frameio)", name);
}
return(SH_SUCCESS);
}

RET_CODE thFrameioGetLPDump(FRAMEIO *fio, LPDUMP **lp) {
char *name = "thFrameioGetLPDump";
if (lp == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (fio == NULL) {
	thError("%s: ERROR - null input", name);
	*lp = NULL;
	return(SH_GENERIC_ERROR);
}
*lp = fio->lps;
if (*lp == NULL) {
	thError("%s: WARNING - null (lpdump) - improperly allocated (frameio)", name);
}
return(SH_SUCCESS);
}

RET_CODE thFrameioGetLRZDump(FRAMEIO *fio, LRZDUMP **lrz) {
char *name = "thFrameioGetLRZDump";
if (lrz == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (fio == NULL) {
	thError("%s: ERROR - null input", name);
	*lrz = NULL;
	return(SH_GENERIC_ERROR);
}
*lrz = fio->lrz;
if (*lrz == NULL) {
	thError("%s: WARNING - null (lrzdump) - improperly allocated (frameio)", name);
}
return(SH_SUCCESS);
}


RET_CODE thFrameAddObjcChain(FRAME *f, CHAIN *objcs) {
char *name = "thFrameAddObjcChain";
if (f == NULL) {
	thError("%s: ERROR - null frame", name);
	return(SH_GENERIC_ERROR);
}
if (objcs == NULL || shChainSize(objcs) == 0) {
	thError("%s: WARNING - null or empty objc chain", name);
	return(SH_SUCCESS);
}
FRAMEDATA *fd = f->data;
if (fd == NULL) {
	thError("%s: ERROR - null (framedata) - improperly allocated (frame)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status = thFramedataAddObjcChain(fd, objcs);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not copy objects onto (framedata)", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE thFramedataAddObjcChain(FRAMEDATA *fd, CHAIN *objcs) {
char *name = "thFramedataAddObjcChain";
if (fd == NULL) {
	thError("%s: ERROR - null frame data", name);
	return(SH_GENERIC_ERROR);
}
if (objcs == NULL || shChainSize(objcs) == 0) {
	thError("%s: WARNING - null or empty objc chain", name);
	return(SH_SUCCESS);
}

CHAIN *phobjcs = fd->phobjc;
CHAIN *phobjcs_source = fd->phobjc_source;
#if USE_FNAL_OBJC_IO
char *wobjc_name = "FNAL_OBJC_IO";
#else
char *wobjc_name = "OBJC_IO";
#endif

if (phobjcs == NULL) {
	fd->phobjc = shChainNew(wobjc_name);
	phobjcs = fd->phobjc;
}
if (phobjcs_source == NULL) {
	fd->phobjc_source = shChainNew(wobjc_name);
	phobjcs_source = fd->phobjc_source;
}
int n = shChainSize(objcs);
WOBJC_IO *x; int i;
for (i = 0; i < n; i++) {
	x = shChainElementGetByPos(objcs, i);
	shChainElementAddByPos(phobjcs_source, x, wobjc_name, TAIL, AFTER);
	WOBJC_IO *y = thWObjcIoNew(x->ncolor);
	memcpy(y, x, sizeof(WOBJC_IO));
	shChainElementAddByPos(phobjcs, y, wobjc_name, TAIL, AFTER);
}

return(SH_SUCCESS);
}


RET_CODE thFramefilesGetHdr(FRAMEFILES *ff, HDR **hdr) {	
char *name = "thFramefilesGetHdr";
if (hdr == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (ff == NULL) {
	thError("ERROR - null input (framefiles)", name);
	*hdr = NULL;
	return(SH_GENERIC_ERROR);
}
*hdr = ff->hdr;
return(SH_SUCCESS);
}

RET_CODE thFramefilesPutHdr(FRAMEFILES *ff, HDR *hdr) {
char *name = "thFramefilesPutHdr";
if (ff == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (hdr == NULL && ff->hdr != NULL) {
	thError("%s: WARNING - setting (framefiles) header to null while it was non-null - could cause inaccessible blocks in memory", name);
} else if (hdr == ff->hdr && hdr != NULL) {
	thError("%s: WARNING - putting the same (hdr) back into (framefiles)?!", name);
} else if (hdr == NULL && ff->hdr == NULL) {
	thError("%s: WARNING - asked to put null (hdr) in (framefiles) while it is already null?!", name);
}
ff->hdr = hdr;
return(SH_SUCCESS);
}

RET_CODE thFramefilesGetHdr2(FRAMEFILES *ff, HDR **hdr) {	
char *name = "thFramefilesGetHdr2";
if (hdr == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (ff == NULL) {
	thError("ERROR - null input (framefiles)", name);
	*hdr = NULL;
	return(SH_GENERIC_ERROR);
}
*hdr = ff->hdr2;
return(SH_SUCCESS);
}

RET_CODE thFramefilesGetHdr3(FRAMEFILES *ff, HDR **hdr) {	
char *name = "thFramefilesGetHdr3";
if (hdr == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (ff == NULL) {
	thError("ERROR - null input (framefiles)", name);
	*hdr = NULL;
	return(SH_GENERIC_ERROR);
}
*hdr = ff->hdr3;
return(SH_SUCCESS);
}


RET_CODE thFrameGetHdr(FRAME *f, HDR **hdr) {
char *name = "thFrameGetHdr";
RET_CODE status;
FRAMEFILES *ff = NULL;
status = thFrameGetFramefiles(f, &ff);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (framefiles) from (frame)", name);
	return(status);
}
status = thFramefilesGetHdr(ff, hdr);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (hdr) from (framefiles)", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE thFrameGetHdr2(FRAME *f, HDR **hdr) {
char *name = "thFrameGetHdr2";
RET_CODE status;
FRAMEFILES *ff = NULL;
status = thFrameGetFramefiles(f, &ff);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (framefiles) from (frame)", name);
	return(status);
}
status = thFramefilesGetHdr2(ff, hdr);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (hdr) from (framefiles)", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE thFrameGetHdr3(FRAME *f, HDR **hdr) {
char *name = "thFrameGetHdr";
RET_CODE status;
FRAMEFILES *ff = NULL;
status = thFrameGetFramefiles(f, &ff);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (framefiles) from (frame)", name);
	return(status);
}
status = thFramefilesGetHdr3(ff, hdr);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (hdr) from (framefiles)", name);
	return(status);
}
return(SH_SUCCESS);
}



RET_CODE thFrameidGetBand(FRAMEID *id, int *band) {
char *name = "thFrameidGetBand";
if (band == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (id == NULL) {
	thError("%s: ERROR - null input", name);
	*band = UNKNOWN_BAND;
	return(SH_GENERIC_ERROR);
}
*band = id->filter;
return(SH_SUCCESS);
}

RET_CODE thFrameGetBand(FRAME *f, int *band) {
char *name = "thFrameGetBand";
if (band == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (f == NULL) {
	thError("%s: ERROR - null input", name);
	*band = UNKNOWN_BAND;
	return(SH_GENERIC_ERROR);
}
FRAMEID *id = f->id;
if (id == NULL) {
	thError("%s: ERROR - null (id) - improperly allocated (frame)", name);
	*band = UNKNOWN_BAND;
	return(SH_GENERIC_ERROR);
}
*band = id->filter;
return(SH_SUCCESS);
}

RET_CODE thFrameGetPhObjcs(FRAME *f, CHAIN **phobjcs, int **bndex) {
char *name = "thFrameGetPhObjcs";
if (phobjcs == NULL && bndex == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (f == NULL) {
	thError("%s: ERROR - null input", name);
	if (phobjcs != NULL) *phobjcs = NULL;
	if (bndex != NULL) *bndex = NULL;
	return(SH_GENERIC_ERROR);
}
FRAMEDATA *fd = f->data;
if (fd == NULL) {
	thError("%s: ERROR - improperly allocated (frame) - null (framedata)", name);
	if (phobjcs != NULL) *phobjcs = NULL;
	if (bndex != NULL) *bndex = NULL;
	return(SH_GENERIC_ERROR);
}

if (phobjcs != NULL) *phobjcs = fd->phobjc;
if (bndex != NULL) *bndex = fd->bndex;
return(SH_SUCCESS);
}

RET_CODE thFramePutHdr2(FRAME *f, HDR *hdr) {
char *name = "thFramePutHdr2";
if (f == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
FRAMEFILES *files = f->files;
if (files == NULL) {
	thError("%s: ERROR - null (files) - improperly allocated (frame)", name);
	return(SH_GENERIC_ERROR);
}

if (files->hdr2 == hdr) return(SH_SUCCESS);
if (files->hdr2 != NULL) shHdrDel(files->hdr2);
files->hdr2 = hdr;
return(SH_SUCCESS);
}

RET_CODE thFramePutHdr3(FRAME *f, HDR *hdr) {
char *name = "thFramePutHdr3";
if (f == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
FRAMEFILES *files = f->files;
if (files == NULL) {
	thError("%s: ERROR - null (files) - improperly allocated (frame)", name);
	return(SH_GENERIC_ERROR);
}

if (files->hdr3 == hdr) return(SH_SUCCESS);
if (files->hdr3 != NULL) shHdrDel(files->hdr3);
files->hdr3 = hdr;
return(SH_SUCCESS);
}


RET_CODE thFrameGetCrudeCalib(FRAME *f, CRUDE_CALIB **cc) {
char *name = "thFrameGetCrudeCalib";
if (f == NULL) {
	thError("%s: ERROR - null frame", name);
	return(SH_GENERIC_ERROR);
}
if (cc == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
RET_CODE status;
FRAMEDATA *fdata = NULL;
status = thFrameGetFramedata(f, &fdata);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (framedata) from (frame)", name);
	return(status);
}
status = thFramedataGetCrudeCalib(fdata, cc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (cc) from (fdata)", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE thFramedataGetCrudeCalib(FRAMEDATA *fd, CRUDE_CALIB **cc) {
char *name = "thFramedataGetCrudeCalib";
if (fd == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (cc == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
RET_CODE status;
PHPSFMODEL *psf = NULL;
status = thFramedataGetPhPsfModel(fd, &psf);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (phpsfmodel)", name);
	return(status);
}
status = phPsfModelGetCrudeCalib(psf, cc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (cc) from (psf)", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE thFramedataGetPhPsfModel(FRAMEDATA *fd, PHPSFMODEL **psf) {
char *name = "thFramedataGetPhPsfModel";
if (fd == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (psf == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_SUCCESS);
}
*psf = fd->phpsf;
return(SH_SUCCESS);
}

RET_CODE thFramedataGetThSpanmask(FRAMEDATA *fd, SPANMASK **thsm) {
char *name = "thFramedataGetThSpanmask";
if (thsm == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (fd == NULL) {
	thError("%s: ERROR - null input placeholder", name);
	return(SH_GENERIC_ERROR);
}
*thsm = fd->thsm;
return(SH_SUCCESS);
}


RET_CODE thFrameGetInitFiniLM(FRAME *f, LMETHOD **init, LMETHOD **fini) {
char *name = "thFrameGetInitFiniLM";
if (init == NULL && fini == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (f == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
LFIT *lf = NULL;
RET_CODE status;
LSTRUCT *l = NULL;
status = thFrameGetLstruct(f, &l);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lstruct) from (frame)", name);
	return(status);
}
status = thLstructGetLfit(l, &lf);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lfit) from (lstruct)", name);
	return(status);
}
status = thLfitGetInitFiniLM(lf, init, fini);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (init, fini-LM) from (lfit)", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE thFramedataCopyObjcs(FRAMEDATA *t, FRAMEDATA *s, int deep) {
char *name = "thFramedataCopy";
if (s == NULL && t == NULL) {
	thError("%s: WARNING - null source and target", name);
	return(SH_SUCCESS);
}
if (s == NULL || t == NULL) {
	thError("%s: ERROR - null source or target", name);
	return(SH_GENERIC_ERROR);
}

if (t->copy == 0) {
	if (t->phobjc != NULL) shChainDestroy(t->phobjc, &thWObjcIoDel);
	if (t->cwphobjc != NULL) shChainDestroy(t->cwphobjc, &thCalibWObjcIoDel);
	if (t->cphobjc != NULL) shChainDestroy(t->cphobjc, &thCalibPhObjcIoDel);
	if (t->phprop != NULL) shChainDestroy(t->phprop, &phPropsDel);
}

t->phobjc = s->phobjc;
t->cwphobjc = s->cwphobjc;
t->cphobjc = s->cphobjc;
t->phprop = s->phprop;

t->copy = 1;
return(SH_SUCCESS);
}


RET_CODE thFrameCopyDataObjcs(FRAME *t, FRAME *s, int deep) {
char *name = "thFrameCopyData";
if (s == NULL && t == NULL) {
	thError("%s: WARNING - null source and target", name);
	return(SH_SUCCESS);
}
if (s == NULL || t == NULL) {
	thError("%s: ERROR - null source or target", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status = thFramedataCopyObjcs(t->data, s->data, deep);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not copy framedata from source to target", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE thFrameworkGetMap(FRAMEWORK *work, LMACHINE **map) {
char *name = "thFrameworkGetMap";
if (work == NULL && map == NULL) {
	thError("%s: ERROR - null input and output placeholder", name);
	return(SH_GENERIC_ERROR);
}
int fitid = work->fitid;
if (fitid < 0 || fitid >= MAX_FITID) {
	thError("%s: ERROR - unsupported (fitid = %d)", name, fitid);
	return(SH_GENERIC_ERROR);
}
if (work == NULL) {
	thError("%s: ERROR - null input placeholder", name);
	*map = NULL;
	return(SH_GENERIC_ERROR);
} else if (map == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
*map = work->map[fitid];
return(SH_SUCCESS);
}

RET_CODE thFrameworkGetLstruct(FRAMEWORK *work, LSTRUCT **lstruct) {
char *name = "thFrameworkGetLstruct";
if (work == NULL && lstruct == NULL) {
	thError("%s: ERROR - null input and output placeholder", name);
	return(SH_GENERIC_ERROR);
}
int fitid = work->fitid;
if (fitid < 0 || fitid >= MAX_FITID) {
	thError("%s: ERROR - unsupported (fitid = %d)", name, fitid);
	return(SH_GENERIC_ERROR);
}
if (work == NULL) {
	thError("%s: ERROR - null input placeholder", name);
	*lstruct = NULL;
	return(SH_GENERIC_ERROR);
} else if (lstruct == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
*lstruct = work->lstruct[fitid];
return(SH_SUCCESS);
}


RET_CODE thFrameworkPutMap(FRAMEWORK *work, LMACHINE *map) {
char *name = "thFrameworkPutMap";
if (work == NULL && map == NULL) {
	thError("%s: ERROR - null input placeholders", name);
	return(SH_GENERIC_ERROR);
}
int fitid = work->fitid;
if (fitid < 0 || fitid >= MAX_FITID) {
	thError("%s: ERROR - unsupported (fitid = %d)", name, fitid);
	return(SH_GENERIC_ERROR);
}
if (work == NULL) {
	thError("%s: ERROR - null (work) placeholder", name);
	return(SH_GENERIC_ERROR);
} else if (map == NULL) {
	thError("%s: WARNING - null (map) placeholder", name);	
}
work->map[fitid] = map;
return(SH_SUCCESS);
}

RET_CODE thFrameworkPutLstruct(FRAMEWORK *work, LSTRUCT *lstruct) {
char *name = "thFrameworkPutLstruct";
if (work == NULL && lstruct == NULL) {
	thError("%s: ERROR - null input placeholders", name);
	return(SH_GENERIC_ERROR);
}
int fitid = work->fitid;
if (fitid < 0 || fitid >= MAX_FITID) {
	thError("%s: ERROR - unsupported (fitid = %d)", name, fitid);
	return(SH_GENERIC_ERROR);
}
if (work == NULL) {
	thError("%s: ERROR - null (work) placeholder", name);
	return(SH_GENERIC_ERROR);
} else if (lstruct == NULL) {
	thError("%s: WARNING - null (map) placeholder", name);	
}
work->lstruct[fitid] = lstruct;
return(SH_SUCCESS);
}

RET_CODE thFrameworkSetFitid(FRAMEWORK *work, int fitid) {
char *name = "thFrameSetFitid";
if (work == NULL) {
	thError("%s: ERROR - null (framework)", name);
	return(SH_GENERIC_ERROR);
}
if (fitid < 0 || fitid >= MAX_FITID) {
	thError("%s: ERROR - unsupported (fitid = %d)", name, fitid);
	return(SH_GENERIC_ERROR);
}
work->fitid = fitid;
return(SH_SUCCESS);
}

RET_CODE thFrameworkGetFitid(FRAMEWORK *work, int *fitid) {
char *name = "thFrameworkGetFitid";
if (work == NULL && fitid == NULL) {
	thError("%s: WARNING - null input and output placeholder", name);
	return(SH_SUCCESS);
}
if (fitid == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (work == NULL) {
	thError("%s: ERROR - null (framework)", name);
	return(SH_GENERIC_ERROR);
}
*fitid = work->fitid;
if (work->fitid < 0 || work->fitid >= MAX_FITID) {
	thError("%s: WARNING - unacceptable value discovered (fitid = %d)", name, work->fitid);
}
return(SH_SUCCESS);
}

RET_CODE thFrameSetFitid(FRAME *f, int fitid) {
char *name = "thFrameSetFitid";
if (f == NULL) {
	thError("%s: ERROR - null (frame)", name);
	return(SH_GENERIC_ERROR);
}
FRAMEWORK *work = NULL;
RET_CODE status;
status = thFrameGetFramework(f, &work);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (framework) from (frame)", name);
	return(status);
}
status = thFrameworkSetFitid(work, fitid);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not set (fitid = %d) in (framework)", name, fitid);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE thFrameGetFitid(FRAME *f, int *fitid) {
char *name = "thFrameGetFitid";
if (f == NULL) {
	thError("%s: ERROR - null (frame)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
FRAMEWORK *work = NULL;
status = thFrameGetFramework(f, &work);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (work) from (frame)", name);
	return(status);
}
status = thFrameworkGetFitid(work, fitid);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (fitid) from (framework)", name);
	return(status);
}
return(SH_SUCCESS);
}


RET_CODE thFrameDataPutGaussianNoiseDN(FRAMEDATA *fd, THPIX gaussian_noise_dn) {
char *name = "thFrameDataPutGaussianNoiseDN";
if (fd == NULL) {
	thError("%s: ERROR - null (framedata)", name);
	return(SH_GENERIC_ERROR);
}
fd->gaussian_noise_dn = gaussian_noise_dn;
return(SH_SUCCESS);
}

RET_CODE thFrameDataGetGaussianNoiseDN(FRAMEDATA *fd, THPIX *gaussian_noise_dn) {
char *name = "thFrameDataGetGaussianNoiseDN";
if (gaussian_noise_dn == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (fd == NULL) {
	thError("%s: ERROR - null input (framedata)", name);
	return(SH_GENERIC_ERROR);
}
*gaussian_noise_dn = fd->gaussian_noise_dn;
return(SH_SUCCESS);
}

RET_CODE thFramePutGaussianNoiseDN(FRAME *f, THPIX gaussian_noise_dn) {
char *name = " thFramePutGaussianNoiseDN";
shAssert(f != NULL);
RET_CODE status;
status = thFrameDataPutGaussianNoiseDN(f->data, gaussian_noise_dn);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not put (gaussian_noise_dn) in (framedata)", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE thFrameGetGaussianNoiseDN(FRAME *f, THPIX *gaussian_noise_dn) {
char *name = "thFrameGetGaussianNoiseDN";
shAssert(f != NULL);
RET_CODE status;
status = thFrameDataGetGaussianNoiseDN(f->data, gaussian_noise_dn);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (gaussian_noise_dn) from (framedata)", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE thWObjcIOCopyChain(CHAIN *schain, CHAIN *tchain) {
char *name = "thWObjcIOCopyChain";
if (schain == NULL || shChainSize(schain) == 0) {
	thError("%s: WARNING - null or empty source chain - nothing to copy", name);
	return(SH_SUCCESS);
}
if (tchain == NULL) {
	thError("%s: ERROR - null target chain", name);
	return(SH_GENERIC_ERROR);
}
if (shChainSize(tchain) != 0) {
	thError("%s: WARNING - target chain not empty - appending", name);
}
int i, null_count = 0, n = shChainSize(schain);

#if USE_FNAL_OBJC_IO
char *wobjc_io_name = "FNAL_OBJC_IO";
#else
char *wobjc_io_name = "OBJC_IO";
#endif

RET_CODE status;
for (i = 0; i < n; i++) {
	WOBJC_IO *x = shChainElementGetByPos(schain, i);
	if (x != NULL) {
		WOBJC_IO *y = thWObjcIoNew(x->ncolor);
		status = thWObjcIoCopy(y, x);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not copy (wobjc_io) at source[%d]", name, i);
			return(status);
		}
		shChainElementAddByPos(tchain, y, wobjc_io_name, TAIL, AFTER);
	} else {
		null_count++;
	}
}
if (null_count > 0) {
	thError("%s: WARNING - (%d) instances of null found in source chain were not copied to target", name, null_count);
}

return(SH_SUCCESS);
}

RET_CODE thFrameReplacePhObjc(FRAME *f, CHAIN *phobjc) {
char *name = "thFrameReplacePhObjc";
if (f == NULL) {
	thError("%s: ERROR - null frame", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status = thFramedataReplacePhObjc(f->data, phobjc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not replace (phobjc) list in (framedata)", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE thFramedataReplacePhObjc(FRAMEDATA *fd, CHAIN *phobjc) {
char *name = "thFramedataReplacePhObjc";
if (fd == NULL) {
	thError("%s: ERROR - null (framedata)", name);
	return(SH_GENERIC_ERROR);
}
thError("%s: WARNING - use this function in debug or demand mode only - reserved for expert user", name);
if (fd->phobjc_source != NULL && shChainSize(fd->phobjc_source) > 0) shChainDestroy(fd->phobjc_source, &thWObjcIoDel);
if (fd->phobjc != NULL && shChainSize(fd->phobjc) > 0) shChainDestroy(fd->phobjc, &thWObjcIoDel);

RET_CODE status = thFramedataAddObjcChain(fd, phobjc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add (phobjc) list to (framedata)", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE thFramedataRecopyObjcList(FRAMEDATA *fd) {
char *name = "thFramedataRecopyObjcList";
if (fd == NULL) {
	thError("%s: ERROR - null (framedata)", name);
	return(SH_GENERIC_ERROR);
}

CHAIN *pobjcs = NULL, *pobjcs_source = NULL;
pobjcs = fd->phobjc;
pobjcs_source = fd->phobjc_source;

if (pobjcs == NULL || pobjcs_source == NULL) {
	thError("%s: ERROR - null (pobjc) or (pbjcs_source) in (framedata)", name);
	return(SH_GENERIC_ERROR);
}
int n = shChainSize(pobjcs);
int n_source = shChainSize(pobjcs_source);

if (n != n_source) {
	thError("%s: WARNING - (n = %d) while (n_source = %d)", name, n, n_source);
}

if (n == 0 && n_source == 0) {
	thError("%s: WARNING - empty (pobjc, n = %d) list and source list (n_source = %d)", name, n, n_source);
	return(SH_SUCCESS);
}
if (n == 0) {
	thError("%s: WARNING - empty (pobjc, n = %d) list while source list (n_source = %d)", name, n, n_source);
	return(SH_SUCCESS);
}
int i;
RET_CODE status;
for (i = 0; i < n; i++) {
	WOBJC_IO *wobjc = shChainElementGetByPos(pobjcs, i);
	if (wobjc != NULL) {
		int nmatch = 0, imatch = -1;
		WOBJC_IO *wobjc_source = NULL;
		status = thWObjcIoFindMatchInChain(pobjcs_source, wobjc, &wobjc_source, &imatch, &nmatch);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not find a match to (wobjc, i = %d) in source chain", name, i);
			return(status);
		}
		if (wobjc_source == NULL) {
			thError("%s: ERROR - null source (wobjc) returned while looking for a match (i = %d)", name, i);
			return(SH_GENERIC_ERROR);
		}
		if (nmatch > 1) {
			thError("%s: WARNING - more than one match found for (wobjc, i = %d) in source chain", name, i);
		} else if (nmatch <= 0) {
			thError("%s: ERROR - (nmatch = %d) found for (wobjc, i = %d)", name, nmatch, i);
			return(SH_GENERIC_ERROR);
		}
		memcpy(wobjc, wobjc_source, sizeof(WOBJC_IO));
	} else {
		thError("%s: WARNING - null (phobjc) found at (i = %d)", name, i);
	}
}

return(SH_SUCCESS);
}

RET_CODE thWObjcIoFindMatchInChain(CHAIN *source_chain, WOBJC_IO *wobjc, WOBJC_IO **wobjc_match, int *imatch, int *nmatch) {
char *name = "thWObjcIoFindMatchInChain";
if (source_chain == NULL) {
	thError("%s: ERROR - null (source_chain) passed", name);
	if (wobjc_match != NULL) *wobjc_match = NULL;
	if (imatch != NULL) *imatch = -1;
	if (nmatch != NULL) *nmatch = 0;
	return(SH_GENERIC_ERROR);
}
if (wobjc == NULL) {
	thError("%s: ERROR - null (wobjc_io) passed", name);
	if (wobjc_match != NULL) *wobjc_match = NULL;
	if (imatch != NULL) *imatch = -1;
	if (nmatch != NULL) *nmatch = 0;
	return(SH_GENERIC_ERROR);
}
if (wobjc_match == NULL && imatch == NULL && nmatch == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
int n = shChainSize(source_chain);
if (n == 0) {
	thError("%s: ERROR - empty (source_chain) passed", name);
	if (wobjc_match != NULL) *wobjc_match = NULL;
	if (imatch != NULL) *imatch = -1;
	if (nmatch != NULL) *nmatch = 0;
	return(SH_GENERIC_ERROR);
}
int i = -1, match_count = 0;
for (i = 0; i < n; i++) {
	WOBJC_IO *wobjc_source = shChainElementGetByPos(source_chain, i);
	if (is_wobjc_match(wobjc, wobjc_source)) {
		if (match_count == 0) {
			if (wobjc_match != NULL) *wobjc_match = wobjc_source;
			if (imatch != NULL) *imatch = i;
		}
		match_count++;
	}
}
if (match_count == 0) {
	if (wobjc_match != NULL) *wobjc_match = NULL;
	if (imatch != NULL) *imatch = -1;
	if (nmatch != NULL) *nmatch = 0;
} else {
	if (nmatch != NULL) *nmatch = match_count;
}

return(SH_SUCCESS);
}

int is_wobjc_match(WOBJC_IO *wobjc1, WOBJC_IO *wobjc2) {
shAssert(wobjc1 != NULL);
shAssert(wobjc2 != NULL);

int is_match = ((wobjc1->id == wobjc2->id) && 
	(wobjc1->parent == wobjc2->parent) && 	
	(wobjc1->ncolor == wobjc2->ncolor) && 
	(wobjc1->nchild == wobjc2->nchild) && 
	(wobjc1->objc_type == wobjc2->objc_type) && 
	(wobjc1->objc_prob_psf == wobjc2->objc_prob_psf) && 
	(wobjc1->catID == wobjc2->catID) && 
	(wobjc1->objc_flags == wobjc2->objc_flags) && 
	(wobjc1->objc_flags2 == wobjc2->objc_flags2));

return(is_match);
}

RET_CODE thFramePutFrameio(FRAME *f, FRAMEIO *fio) {
char *name = "thFramePutFrameio";
if (f == NULL) {
	thError("%s: ERROR - null frame", name);
	return(SH_GENERIC_ERROR);
}
if (fio == NULL) {
	thError("%s: ERROR - null (frameio)", name);
	return(SH_GENERIC_ERROR);
}
f->io = fio;
CHAIN *io_chain = f->io_chain;
if (io_chain == NULL) {
	f->io_chain  = shChainNew("FRAMEIO");
	io_chain = f->io_chain;
}
int n = shChainSize(io_chain);
int i, found = 0;
for (i = 0; i < n; i++) {
	FRAMEIO *io_instance = shChainElementGetByPos(io_chain, i);
	if (io_instance == fio) found++;
}
if (found > 0) {
	thError("%s: WARNING - (frameio) already exists (count = %d) in (io_chain), only replacing the active (frameio)", name, found);
} else {
	shChainElementAddByPos(io_chain, fio, "FRAMEIO", TAIL, AFTER);
}
return(SH_SUCCESS);
}

RET_CODE thFrameFreeFrameio(FRAME *f) {
char *name = "thFrameFreeFrameio";
if (f == NULL) {
	thError("%s: ERROR - null frame", name);
	return(SH_GENERIC_ERROR);
}
f->io = NULL;
return(SH_SUCCESS);
}


