#include "thStat.h"
#include "phMathUtils.h"

RET_CODE thBasicStatFromArray(float *data, float *dataerror, BASIC_STAT *stat, int n) {
char *name = "thBasicStatFromArray";
shAssert(data != NULL);
shAssert(stat != NULL);
shAssert(n >= 0);

if (n == 0) {
	thBasicStatInit(stat);
	stat->n = 0;
	return(SH_SUCCESS);
}

int i;
THPIX max, min;
max = min = data[0];
for (i = 1; i < n; i++) {
	max = MAX(max, data[i]);
	min = MIN(min, data[i]);
}
stat->min = min;
stat->max = max;

if (max == min) {
	stat->median = max;
	stat->iqr = 0.0;
	stat->q1 = max;
	stat->q3 = max;
	stat->mean = max;
	stat->s = 0.0;
	return(SH_SUCCESS);
}

phFloatArrayMean(data, n, &(stat->mean), &(stat->s));
THPIX cv = thabs(stat->s / stat->mean);
if (cv < 1.0E-5) {
	stat->q1 = stat->mean;
	stat->q3 = stat->mean;
	stat->median = stat->mean;
	stat->iqr = 0.0E0;
	return(SH_SUCCESS);
}

phFloatArrayStats(data, n, 0, &(stat->median), &(stat->iqr), &(stat->q1), &(stat->q3));

return(SH_SUCCESS);

}


