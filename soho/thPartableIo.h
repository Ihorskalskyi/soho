#ifndef THPARTABLEIO_H
#define THPARTABLEIO_H

#include "string.h"
#include "stdio.h"
#include "dervish.h"
#include "thFuncmodelIoTypes.h"

typedef enum dbfilestatus {
	DB_HOF, /* Header of File */
	DB_HODB, /* Header of DB */ 
	DB_HOS, /* Header of Structure */
	DB_MOS, /* Middle of Structure */
	DB_EOS, /* End of Structure */
	DB_EODB, /* End of Database */
	DB_EOF, /* End of File */
	DB_UNKNOWN_STATUS
	} DBFILESTATUS;

typedef struct dbfile {
	char *filename;
	FILE *f;
	char *mode;
	DBFILESTATUS status;
	} DBFILE;

DBFILE *thDbfileNew();
void thDbfileDel(DBFILE *f);

DBFILE *OpenDBFile(char *filename, char *mode, RET_CODE *status);
void CloseDBFile(DBFILE *f, RET_CODE *status);
/* creates a DBFILE for reading 
closes the DBFIle
*/
void SeekInitDB(DBFILE *f, char *filename, RET_CODE *status);
/* goes to a separator character, here '{', and reads the file name after it and passes the line 
will return, EOF or HOS when returning
*/
void SeekHdrDB(DBFILE *f, char *funcname, char *type, RET_CODE *status);
/* finds the upcoming occurance of funcname and table type name in the form ':funcname:typename'
returns EOF, MOS, EOS, EODB when returns 
*/
void SeekLineDB(DBFILE *f, char *line, RET_CODE *status);
/* puts you at the beginning of the next line and tells you what to expect
returns HOS, MOS, HODB, EOS, EODB, EOF
*/
PARELEM *ReadParelemDB(char *line, char *type, RET_CODE *status);
/* reads the next line as a PARELEM */
void SeekEndDB(DBFILE *f, RET_CODE *status);
/* Passes the END OF DB character '}' */

PARTABLE *ReadPartableDB(DBFILE *f, RET_CODE *status);
CHAIN *ReadPartableChainDB(DBFILE *f, char *filename, RET_CODE *status);


void SeekCharDB(DBFILE *f, char c, RET_CODE *status);


#endif
