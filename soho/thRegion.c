#include "thRegion.h"

static const SCHEMA *schema_psf_conv_type = NULL;
static const SCHEMA *schema_thregion_type = NULL;
static const SCHEMA *schema_fabrication_flag = NULL;

static void init_static_vars (void);
static int compare_int(const void *x1, const void *x2);
static RET_CODE intersect_rectangles(int *rows1, int *cols1, 
				int *rows2, int *cols2, 
				int *rows, int *cols, int *overlap);

static RET_CODE expand_rectangle_corners(int *rows, int *cols, THPIX factor, THPIX size);

void init_static_vars (void) {
	TYPE t;
	t = shTypeGetFromName("PSF_CONVOLUTION_TYPE");
	shAssert(t != UNKNOWN_SCHEMA);
	schema_psf_conv_type = shSchemaGetFromType(t);
	t = shTypeGetFromName("THREGION_TYPE");
	shAssert(t != UNKNOWN_SCHEMA);
	schema_thregion_type = shSchemaGetFromType(t);
	t = shTypeGetFromName("FABRICATION_FLAG");
	shAssert(t != UNKNOWN_SCHEMA);
	schema_fabrication_flag = shSchemaGetFromType(t);
	return;
}

RET_CODE thRegion1ConvolveWithPsf(THREGION1 *threg, PSF_CONVOLUTION_TYPE force_psf_type) {
char *name = "thRegion1ConvolveWithPsf";
if (threg == NULL) {
	thError("%s: WARNING - null input", name);
	return(SH_SUCCESS);
}
RET_CODE status;
PSF_CONVOLUTION_INFO *psf_info;
status = thRegion1GetPsfConvInfo(threg, &psf_info);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not retrieve (psf_info) from (thregion1)", name);
	return(status);
}

PSF_CONVOLUTION_TYPE psf_type;
psf_type = force_psf_type;
if (force_psf_type == UNKNOWN_PSF_CONVOLUTION) {
	status = thPsfConvInfoGetPsfConvType(psf_info, &psf_type);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not retrieve (psf_convolution_type) from (psf_info)", name);
		return(status);
	}
}
int n;
status = thPsfConvInfoGetNComponent(psf_info, &n);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not retrieve the number of PSF components from (psf_info)", name);
	return(status);
}
if (n == DEFAULT_NCOMPONENT) {
	psf_info->ncomponent = 1;
} else if (n < 0) {
	thError("%s: ERROR - discovered negative value for (ncomponent = %d) in (psf_info)", name, n);
	return(SH_GENERIC_ERROR);
}

if (psf_type == NO_PSF_CONVOLUTION) {
	threg->fab = PSFCONVOLVED;
	return(SH_SUCCESS);
} else {
	REGION *reg;
	reg = threg->reg; /* the reg is set such that PSF convolve doesn't need to reallocate the region if used as an output */
	#if DEBUG_PSF_CONVOLVE 
	char *debug_file_before_convolution = "./debug-file-tesp-sersic-before-convolution.fits";
	if (!strcmp("sersic", reg->name)) { 
		shRegWriteAsFits(reg, debug_file_before_convolution, STANDARD, 2, DEF_NONE, NULL, 0);
	}
	#endif
	int nbadpixel = 0;
	status = shRegCountBadPixel(reg, &nbadpixel);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not count bad pixels in dervish (reg)", name);
		return(status);
	}
	if (nbadpixel != 0) {
		thError("%s: ERROR - found (%d) bad pixels in the main image before convolution", name, nbadpixel);
		return(SH_GENERIC_ERROR);
	}
	status = shRegConvolve(reg, reg, psf_info);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not convolve dervish region with (psf_info)", name);
		return(status);
	}
	#if DEBUG_PSF_CONVOLVE 
	char *debug_file_after_convolution = "./debug-file-tesp-sersic-after-convolution.fits";
	if (!strcmp("sersic", reg->name)) { 
		shRegWriteAsFits(reg, debug_file_after_convolution, STANDARD, 2, DEF_NONE, NULL, 0);
	}
	#endif

	nbadpixel = 0;
	status = shRegCountBadPixel(reg, &nbadpixel);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not count bad pixels in dervish (reg)", name);
		return(status);
	}
	if (nbadpixel != 0) {
		thError("%s: ERROR - found (%d) bad pixels in the main image after convolution", name, nbadpixel);
		return(SH_GENERIC_ERROR);
	}

	threg->fab = PSFCONVOLVED;
	return(SH_SUCCESS);
}
	
if (schema_psf_conv_type == NULL) init_static_vars();
thError("%s: ERROR - (psf_conv_type) '%s' not supported", 
	name, (schema_psf_conv_type->elems[psf_type]).name);
return(SH_GENERIC_ERROR);

}

RET_CODE thRegion2ConvolveWithPsf(THREGION2 *threg, PSF_CONVOLUTION_TYPE force_psf_type) {
char *name = "thRegion2ConvolveWithPsf";
if (threg == NULL) {
	thError("%s: WARNING - null input", name);
	return(SH_SUCCESS);
}
RET_CODE status;
PSF_CONVOLUTION_INFO *psf_info;
status = thRegion2GetPsfConvInfo(threg, &psf_info);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not retrieve (psf_info) from (thregion1)", name);
	return(status);
}
PSF_CONVOLUTION_TYPE psf_type;
psf_type = force_psf_type;
if (force_psf_type == UNKNOWN_PSF_CONVOLUTION) {
	status = thPsfConvInfoGetPsfConvType(psf_info, &psf_type);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not retrieve (psf_convolution_type) from (psf_info)", name);
		return(status);
	}
}
int n;
status = thPsfConvInfoGetNComponent(psf_info, &n);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not retrieve the number of PSF components from (psf_info)", name);
	return(status);
}

if (psf_type == NO_PSF_CONVOLUTION) {
	int i;
	for (i = 0; i < n; i++) {
		threg->fab_i[i] = PSFCONVOLVED;
	}
	threg->fab = PSFCONVOLVED;
	return(SH_SUCCESS);
}		

if (schema_psf_conv_type == NULL) init_static_vars();
thError("%s: ERROR - (psf_conv_type) '%s' not supported", 
	name, (schema_psf_conv_type->elems[psf_type]).name);
return(SH_GENERIC_ERROR);

}

RET_CODE thRegion3ConvolveWithPsf(THREGION3 *threg, PSF_CONVOLUTION_TYPE force_psf_type) {
char *name = "thRegion3ConvolveWithPsf";
if (threg == NULL) {
	thError("%s: WARNING - null input", name);
	return(SH_SUCCESS);
}
RET_CODE status;
PSF_CONVOLUTION_INFO *psf_info;
status = thRegion3GetPsfConvInfo(threg, &psf_info);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not retrieve (psf_info) from (thregion3)", name);
	return(status);
}
PSF_CONVOLUTION_TYPE psf_type;
psf_type = force_psf_type;
if (force_psf_type == UNKNOWN_PSF_CONVOLUTION) {
	status = thPsfConvInfoGetPsfConvType(psf_info, &psf_type);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not retrieve (psf_convolution_type) from (psf_info)", name);
		return(status);
	}
}
int n;
status = thPsfConvInfoGetNComponent(psf_info, &n);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not retrieve the number of PSF components from (psf_info)", name);
	return(status);
}
if (psf_type == NO_PSF_CONVOLUTION) {
	threg->fab = PSFCONVOLVED;
	return(SH_SUCCESS);
}

if (schema_psf_conv_type == NULL) init_static_vars();
thError("%s: ERROR - (psf_conv_type) '%s' not supported", 
	name, (schema_psf_conv_type->elems[psf_type]).name);
return(SH_GENERIC_ERROR);
}

RET_CODE thRegConvolveWithNullPsf(THREGION *threg) {
char *name = "thRegConvolveWithNullPsf";
if (threg == NULL) {
	thError("%s: WARNING - null input", name);
	return(SH_SUCCESS);
}

RET_CODE status;
THREGION_TYPE regtype = threg->type;
if (regtype == THREGION1_TYPE) {
	THREGION1 *threg1;
	threg1 = threg->threg;
	status = thRegion1ConvolveWithPsf(threg1, NO_PSF_CONVOLUTION);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not convolve (thregion1) with PSF", name);
		return(status);
	}
	return(SH_SUCCESS);
} else if (regtype == THREGION2_TYPE) {
	THREGION2 *threg2;
	threg2 = threg->threg;
	status = thRegion2ConvolveWithPsf(threg2, NO_PSF_CONVOLUTION);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not convolve (thregion2) with PSF", name);
		return(status);
	}
	return(SH_SUCCESS);
} else if (regtype == THREGION3_TYPE) {
	THREGION3 *threg3 = threg->threg;
	status = thRegion3ConvolveWithPsf(threg3, NO_PSF_CONVOLUTION);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not convolved (thregion3) with PSF", name);
		return(status);
	}
	return(SH_SUCCESS);
} else {
	if (schema_thregion_type == NULL) init_static_vars ();
	thError("%s: ERROR - (thregion_type) '%s' not supported", 
		name, (schema_thregion_type->elems[regtype]).name);
	return(SH_GENERIC_ERROR);
}

thError("%s: ERROR - source code problem - debug suggested", name);
return(SH_GENERIC_ERROR);
}



RET_CODE thRegConvolveWithPsf(THREGION *threg) {
char *name = "thRegConvolveWithPsf";
if (threg == NULL) {
	thError("%s: WARNING - null input", name);
	return(SH_SUCCESS);
}

RET_CODE status;
THREGION_TYPE regtype = threg->type;
if (regtype == THREGION1_TYPE) {
	THREGION1 *threg1;
	threg1 = threg->threg;
	status = thRegion1ConvolveWithPsf(threg1, UNKNOWN_PSF_CONVOLUTION);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not convolve (thregion1) with PSF", name);
		return(status);
	}
	return(SH_SUCCESS);
} else if (regtype == THREGION2_TYPE) {
	THREGION2 *threg2;
	threg2 = threg->threg;
	status = thRegion2ConvolveWithPsf(threg2, UNKNOWN_PSF_CONVOLUTION);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not convolve (thregion2) with PSF", name);
		return(status);
	}
	return(SH_SUCCESS);
} else if (regtype == THREGION3_TYPE) {
	THREGION3 *threg3 = threg->threg;
	status = thRegion3ConvolveWithPsf(threg3, UNKNOWN_PSF_CONVOLUTION);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not convolved (thregion3) with PSF", name);
		return(status);
	}
	return(SH_SUCCESS);
} else {
	if (schema_thregion_type == NULL) init_static_vars ();
	thError("%s: ERROR - (thregion_type) '%s' not supported", 
		name, (schema_thregion_type->elems[regtype]).name);
	return(SH_GENERIC_ERROR);
}

thError("%s: ERROR - source code problem - debug suggested", name);
return(SH_GENERIC_ERROR);
}

THREGION *thRegNewFromModelElem(MODEL_ELEM *me, PSF_CONVOLUTION_INFO *psf_info) {
char *name = "thRegNewFromModelElem";
if (me == NULL) {
	thError("%s: ERROR - null input", name);
	return(NULL);
}
char *mname;
THREGION_TYPE regtype;
PSF_CONVOLUTION_TYPE conv_type;
int nc;
mname = me->mname;
nc = me->ncomponent;
regtype = me->regtype;
conv_type = me->convtype;

THREGION *threg;
threg = thRegNew(mname, TYPE_THPIX, regtype);
if (threg == NULL) {
	if (schema_thregion_type == NULL) init_static_vars ();
	thError("%s: ERROR - could not produce a basic (thregion) of type '%s'", 
		name, (schema_thregion_type->elems[regtype]).name);
	return(NULL);
}

if (psf_info == NULL) {
	if (thRegGetPsfConvInfo(threg, &psf_info) != SH_SUCCESS) {
		thError("%s: ERROR - could not get (psf_info) from (thregion)", name);	
		thRegDel(threg);
		return(NULL);
	}
} else {
	if (thRegPutPsfConvInfo(threg, psf_info) != SH_SUCCESS) {
		thError("%s: ERROR - could not update (psf_info) in (thregion)", name);
		thRegDel(threg);
		return(NULL);
	}
}

if (psf_info == NULL) {
	thError("%s: ERROR - (psf_info) is null - expecting non-null", name);
	thRegDel(threg);
	return(NULL);
}
if (thPsfConvInfoUpdate(psf_info, conv_type, nc) != SH_SUCCESS) {
	thError("%s: ERROR - could not get (psf_info) as desired", name);
	thRegDel(threg);
	return(NULL);
}

return(threg); 
}

THREGION *thRegNewFromModelName(char *mname, PSF_CONVOLUTION_INFO *psf_info) {
char *name = "thRegNewFromModelName";
if (mname == NULL || strlen(mname) == 0) {
	thError("%s: ERROR - null or empty model name", name);	
	return(NULL);
}

RET_CODE status;
MODEL_ELEM *me;
status = thModelElemGetByName(mname, &me);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (model_elem) for (mname) '%s'", name, mname);
	return(NULL);
}
return(thRegNewFromModelElem(me, psf_info));
}

RET_CODE thRegRenewFromModelElem(THREGION *threg, MODEL_ELEM *me, 
					PSF_CONVOLUTION_INFO *psf_info) {
char *name = "thRegReNewFromModelElem";
if (threg == NULL && me == NULL) {
	thError("%s: WARNING - null input and output placeholder", name);	
	return(SH_SUCCESS);
}
if (threg == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (me == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
THREGION_TYPE regtype;
regtype = threg->type;
if (threg->type != regtype) {
	status = thRegRenew(threg, "", TYPE_THPIX, regtype);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not renew (thregion) to cast it in type '%s'", 
			name, (schema_thregion_type->elems[regtype]).name);
		return(status);
	}
}
if (psf_info == NULL) {
	status = thRegGetPsfConvInfo(threg, &psf_info);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (psf_info) from (thregion)", name);	
		return(status);
	}
} else {
	status = thRegPutPsfConvInfo(threg, psf_info);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put the new (psf_info) in (thregion)", name);
		return(status);
	}
}

PSF_CONVOLUTION_TYPE old_convtype, convtype;
status = thPsfConvInfoGetPsfConvType(psf_info, &old_convtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (conv_type) from (psf_info)", name);
	return(status);
}
convtype = me->convtype;

int nc, old_nc;
status = thPsfConvInfoGetNComponent(psf_info, &old_nc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (ncomponent) from (psf_info)", name);
	return(status);
}
nc = me->ncomponent;

if (nc != old_nc || convtype != old_convtype) {
	if (schema_psf_conv_type == NULL) init_static_vars ();
	status = thPsfConvInfoUpdate(psf_info, convtype, nc);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update (psf_info) to (conv_type) '%s' and (ncomponent) (%d)", 
			name, (schema_psf_conv_type->elems[convtype]).name, nc);
		return(status);
	}
}

return(SH_SUCCESS);
}

RET_CODE thRegRenewFromModelName(THREGION *threg, char *mname, PSF_CONVOLUTION_INFO *psf_info) {
char *name = "thRegRenewFromModelName";

if (threg == NULL && mname == NULL) {
	thError("%s: WARNING - null input and output placeholder", name);	
	return(SH_SUCCESS);
}
if (threg == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (mname == NULL || strlen(mname) == 0) {
	thError("%s: ERROR - null or empty model name", name);
	return(SH_GENERIC_ERROR);
}


RET_CODE status;
MODEL_ELEM *me;
status = thModelElemGetByName(mname, &me);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (model_elem) for (mname) '%s'", name, mname);
	return(status);
}
status = thRegRenewFromModelElem(threg, me, psf_info);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not renew (threg) from (model_elem) for model '%s'", name, mname);
	return(status);
}
return(SH_SUCCESS);

}

RET_CODE thRegion1GetCorners(THREGION1 *threg, int *rows, int *cols) {
char *name = "thRegion1GetCorners";
if (threg == NULL && rows == NULL && cols == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
}
if (threg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (rows == NULL && cols == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
FABRICATION_FLAG fab;
fab = threg->fab;
if (fab != PREFAB && fab != FAB && fab != PSFCONVOLVED) {
	if (schema_fabrication_flag == NULL) init_static_vars();
	thError("%s: ERROR - unsupported (fabrication_flag) '%s'", name, (schema_fabrication_flag->elems[fab]).name);
	return(SH_GENERIC_ERROR);
}

if (rows != NULL) {
	rows[0] = threg->row0;
	rows[1] = threg->row0 + threg->nrow - 1;
}
if (cols != NULL) {
	cols[0] = threg->col0;
	cols[1] = threg->col0 + threg->ncol - 1;
}
return(SH_SUCCESS);
}	

RET_CODE thRegion2GetCorners(THREGION2 *threg, int *rows, int *cols) {
char *name = "thRegion2GetCorners";
if (threg == NULL && rows == NULL && cols == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
}
if (threg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (rows == NULL && cols == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
FABRICATION_FLAG fab;
fab = threg->fab;
if (fab != PREFAB && fab != FAB && fab != PSFCONVOLVED) {
	if (schema_fabrication_flag == NULL) init_static_vars();
	thError("%s: ERROR - unsupported (fabrication_flag) '%s'", name, (schema_fabrication_flag->elems[fab]).name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
int i, n;
status = thReg2GetNComponent(threg, &n);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the number of components for (thregion2)", name);
	return(status);
}

if (n < 1) {
	thError("%s: ERROR - unsupported number of components (%d)", name, n);
	return(SH_GENERIC_ERROR);
}

if (rows != NULL) {
	rows[0] = threg->row0[0];
	rows[1] = threg->row0[0] + threg->nrow[0] - 1;
	for (i = 1; i < n; i++) {
		rows[0] = MIN(rows[0], threg->row0[i]);
		rows[1] = MAX(rows[1], threg->row0[i] + threg->nrow[i] - 1);
	}
}
if (cols != NULL) {
	cols[0] = threg->col0[0];
	cols[1] = threg->col0[0] + threg->ncol[0] - 1;
	for (i = 1; i < n; i++) {
		cols[0] = MIN(cols[0], threg->col0[i]);
		cols[1] = MAX(cols[1], threg->col0[i] + threg->ncol[i] - 1);
	}
}

return(SH_SUCCESS);
}	

RET_CODE thRegion3GetCorners(THREGION3 *threg, int *rows, int *cols) {
char *name = "thRegion3GetCorners";
if (threg == NULL && rows == NULL && cols == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
}
if (threg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (rows == NULL && cols == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
FABRICATION_FLAG fab;
fab = threg->fab;
if (fab != PREFAB && fab != FAB && fab != PSFCONVOLVED) {
	if (schema_fabrication_flag == NULL) init_static_vars();
	thError("%s: ERROR - unsupported (fabrication_flag) '%s'", name, (schema_fabrication_flag->elems[fab]).name);
	return(SH_GENERIC_ERROR);
}

if (rows != NULL) {
	rows[0] = threg->row0;
	rows[1] = threg->row0 + threg->nrow - 1;
}
if (cols != NULL) {
	cols[0] = threg->col0;
	cols[1] = threg->col0 + threg->ncol - 1;
}
return(SH_SUCCESS);
}	

RET_CODE thRegGetCorners(THREGION *threg, int *rows, int *cols) {
char *name = "thRegGetCorners";
if (threg == NULL && rows == NULL && cols == NULL) {
	thError("%s: WARNING - null input and output", name);	
	return(SH_SUCCESS);
}
if (threg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (rows == NULL && cols == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
RET_CODE status;
THREGION_TYPE regtype = threg->type;
if (regtype == THREGION1_TYPE) {
	status = thRegion1GetCorners(threg->threg, rows, cols);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get corners for (thregion1)", name);
		return(status);
	}
} else if (regtype == THREGION2_TYPE) {
	status = thRegion2GetCorners(threg->threg, rows, cols);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get corners for (thregion2)", name);
		return(status);
	}
} else if (regtype == THREGION3_TYPE) {
	status = thRegion3GetCorners(threg->threg, rows, cols);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get corners for (thregion3)", name);
		return(status);
	}
} else {
	if (schema_thregion_type == NULL) init_static_vars ();
	thError("%s: ERROR - unsupported (thregion_type) '%s'", 
		name, (schema_thregion_type->elems[regtype]).name);
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS); 	
}

int compare_int(const void *x1, const void *x2) {
	/* ascending order if used with qsort */
	int i1, i2;
	i1 = * (int *) x1;
	i2 = * (int *) x2;
	return(i1 - i2);
}


RET_CODE intersect_rectangles(int *rows1, int *cols1, int *rows2, int *cols2, int *rows, int *cols, int *overlap) {
char *name = "intersect_rectangles";
if (rows == NULL || cols == NULL || rows1 == NULL || cols1 == NULL || rows2 == NULL || cols2 == NULL || overlap == NULL) {
	thError("%s: ERROR - null input or output found", name);
	return(SH_GENERIC_ERROR);
}

qsort(rows1, 2, sizeof(int), &compare_int);
qsort(cols1, 2, sizeof(int), &compare_int);
qsort(rows2, 2, sizeof(int), &compare_int);
qsort(cols2, 2, sizeof(int), &compare_int);

int rl1, rl2, cl1, cl2, dr, dc;
rl1 = rows1[1] - rows1[0] + 1;
cl1 = cols1[1] - cols1[0] + 1;
rl2 = rows2[1] - rows2[0] + 1;
cl2 = cols2[1] - cols2[0] + 1;

static int rlist[4], clist[4];

rlist[0] = rows1[0];
rlist[1] = rows1[1];
rlist[2] = rows2[0];
rlist[3] = rows2[1];

clist[0] = cols1[0];
clist[1] = cols1[1];
clist[2] = cols2[0];
clist[3] = cols2[1];

qsort(rlist, 4, sizeof(int), &compare_int);
qsort(clist, 4, sizeof(int), &compare_int);

rows[0] = rlist[1];
rows[1] = rlist[2];

cols[0] = clist[1];
cols[1] = clist[2];

dr = rlist[3] - rlist[0] + 1;
dc = clist[3] - clist[0] + 1;

if ((dr < rl1 + rl2) && (dc < cl1 + cl2)) {
	*overlap = 1;
} else {
	*overlap = 0;
}

return(SH_SUCCESS);

}

RET_CODE thRegOverlapPix(THREGION *threg1, THREGION *threg2, FITSTAT fitstat1, FITSTAT fitstat2, MEMFL *pix) {
char *name = "thRegOverlapPix";
if (threg1 == NULL && threg2 == NULL && pix == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
}

if (threg1 == NULL) {
	thError("%s: ERROR - null input (threg1)", name);
	return(SH_GENERIC_ERROR);
}
if (threg2 == NULL) {
	thError("%s: ERROR - null input (threg2)", name);
	return(SH_GENERIC_ERROR);
}
if (pix == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}

RET_CODE status;
int rows1[2], cols1[2], rows2[2], cols2[2], rows[2], cols[2];
status = thRegGetCorners(threg1, rows1, cols1);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the corner coordinates for (threg1)", name);
	return(status);
}
if (fitstat1 == FITMODEL) {
	status = expand_rectangle_corners(rows1, cols1, (THPIX) ALGORITHM_FITMODEL_REGION_EXPANSION_FACTOR, (THPIX) ALGORITHM_FITMODEL_REGION_SIZE);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not expand the corners of reg1 (found to be FITMODEL)", name);
		return(status);
	}
}
status = thRegGetCorners(threg2, rows2, cols2);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the corner coordinates for (threg2)", name);
	return(status);
}
if (fitstat2 == FITMODEL) {
	status = expand_rectangle_corners(rows2, cols2, (THPIX) ALGORITHM_FITMODEL_REGION_EXPANSION_FACTOR, (THPIX) ALGORITHM_FITMODEL_REGION_SIZE);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not expand the corners of reg1 (found to be FITMODEL)", name);
		return(status);
	}
}
int overlap;
status = intersect_rectangles(rows1, cols1, rows2, cols2, rows, cols, &overlap);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the overlap of two rectangular regions", name);
	return(status);
}
MEMFL pixx;
if (overlap == 0) {
	pixx = 0;
} else {
	pixx = (MEMFL) (fabs(rows[1] - rows[0] + 1.0) * fabs(cols[1] - cols[0] + 1.0));
}

*pix = pixx;
if (pixx < (MEMFL) 0) {
	thError("%s: ERROR - found negative pixel count for overlap between two regions", name);
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}


RET_CODE thRegConvertToshReg(THREGION *threg, REGION *shreg) {
char *name = "thRegConvertToshReg";

THREGION_TYPE type = UNKNOWN_THREGION_TYPE;
void *reg = NULL;
RET_CODE status;
status = thRegGetTypeAndReg(threg, &type, &reg);
if (type == THREGION1_TYPE) {
	status = thRegion1ConvertToshReg(reg, shreg);
} else if (type == THREGION2_TYPE) {
	status = thRegion2ConvertToshReg(reg, shreg);
} else if (type == THREGION3_TYPE) {
	status = thRegion3ConvertToshReg(reg, shreg);
} else {
	thError("%s: ERROR - unsupport threg (type = '%s')", name, shEnumNameGetFromValue("THREGION_TYPE", type));
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}


RET_CODE thRegionConvertToshReg(THREGION *threg, REGION *reg) {
char *name = "thRegConvertToshReg";
if (threg == NULL && reg == NULL) {
	thError("%s: WARNING - null input and output placeholder", name);
	return(SH_SUCCESS);
}
if (threg == NULL) {
	thError("%s: WARNING - null input, setting dervish reg to (0)", name);
	shRegClear(reg);
	return(SH_SUCCESS);
}
if (reg == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
int nc = -1;
status = thRegGetNComponent(threg, &nc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (ncomponent) for (threg = %p)", name, (void *) threg);
	return(status);
}
if (nc < 0) {
	thError("%s: ERROR - received (ncomponent = %d) for (threg = %p)", name, (void *) threg);
	return(SH_GENERIC_ERROR);
}
if (nc == 0) {
	thError("%s: WARNING - (ncomponent = %d) found for (threg = %p)", name, (void *) threg);
}

shRegClear(reg);

FABRICATION_FLAG fab;
status = thRegGetFabFlag(threg, &fab);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (fab) for (threg = %p)", name, (void *) threg);
	return(status);
}
/* 
printf("%s: fab flag (= '%s') found in (threg = %p) \n", name, shEnumNameGetFromValue("FABRICATION_FLAG", fab), (void *) threg);
*/

int i;
for (i = 0; i < nc; i++) {
	PSF_CONVOLUTION_INFO *psf_info = NULL;
	REGION *reg1 = NULL;
	NAIVE_MASK *mask = NULL;
	THPIX amp = 0.0;
	status = thRegGet(threg, &fab, &psf_info, &reg1, NULL, NULL, &mask, &amp, i);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not seek content for component (%d) in threg", name, i);
		return(status);
	} 
	if (reg1 == NULL) {
		thError("%s: WARNING - null contents found in (threg = %p)[%d]", name, (void *) threg, i);
	} else if (reg1->nrow == 0 || reg1->ncol == 0) {
		thError("%s: WARNING - empty reg (name = '%s', nr = %d, nc = %d  found in (threg = %p)[%d]", name, reg1->name, reg1->nrow, reg1->ncol,  (void *) threg, i);
	}
	if (amp == (THPIX) 0.0) {
		thError("%s: WARNING - (amp = %g) found for reg (name = '%s', nr = %d, nc = %d) in (threg = %p)[%d]", name, (float) amp, reg1->name, reg1->nrow, reg1->ncol, (void *) threg, i);
	}
	status = shRegAddshRegCentered(reg1, amp, reg);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy source (reg) into target (reg)", name);
		return(status);
	}
}

return(SH_SUCCESS);
}


RET_CODE thRegion1ConvertToshReg(THREGION1 *threg, REGION *reg) {
char *name = "thRegion1ConvertToshReg";
if (threg == NULL && reg == NULL) {
	thError("%s: WARNING - null input and output placeholder", name);
	return(SH_SUCCESS);
}
if (threg == NULL) {
	thError("%s: WARNING - null input, setting dervish reg to (0)", name);
	shRegClear(reg);
	return(SH_SUCCESS);
}
if (reg == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}

REGION *reg1 = threg->reg;
REGION *reg2 = reg;

RET_CODE status;
if (reg1 == NULL) {
	thError("%s: WARNING - null contents found in (thregion1)", name);
} else if (reg1->nrow == 0 || reg1->ncol == 0) {
	thError("%s: WARNING - empty region (name = '%s', nr = %d, nc = %d) found in (thregion1)", name, 
	reg1->name, reg1->nrow, reg1->ncol);
}
shRegClear(reg2);
status = shRegAddshRegCentered(reg1, (THPIX) 1.0, reg2);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not copy source (reg) into target (reg)", name);
	return(status);
}

return(SH_SUCCESS);
}


RET_CODE thRegion2ConvertToshReg(THREGION2 *threg, REGION *reg) {
char *name = "thRegion2ConvertToshReg";
if (threg == NULL && reg == NULL) {
	thError("%s: WARNING - null input and output placeholder", name);
	return(SH_SUCCESS);
}
if (threg == NULL) {
	thError("%s: WARNING - null input, setting dervish reg to (0)", name);
	shRegClear(reg);
	return(SH_SUCCESS);
}
if (reg == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
} 

REGION *reg2 = reg;
shRegClear(reg2);
int i, nc = -1;
RET_CODE status;
status = thReg2GetNComponent(threg, &nc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (ncomponent) for (thregion2)", name);
	return(status);
}
if (nc < 0) {
	thError("%s: ERROR - unsupoprted (ncomponent = %d) in (thregion2)", name, nc);
	return(SH_GENERIC_ERROR);
}
if (nc == 0) {
	thError("%s: WARNING - (thregion2) contains no components (nc = %d)", name, nc);
	return(SH_SUCCESS);
}

for (i = 0; i < nc; i++) {
	REGION *reg1 = NULL;
	THPIX amp = 0.0;
	FABRICATION_FLAG fab = UNKNOWN_FABRICATION_FLAG;
	status = thRegion2Get(threg, &fab, NULL, &reg1, NULL, &amp, i);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not fetch components of (threg2, i = %d)", name, i);
		return(status);
	}
	if (amp != (THPIX) 0.0) {
		if (reg1 == NULL) {
			thError("%s: WARNING - null content found in (thregion2)", name);
		} else if (reg1->nrow == 0 || reg1->ncol == 0) {
			thError("%s: WARNING - empty region '%s'[%d] (nr = %d, nc = %d) found in contents of (thregion2, fab = '%s')", name, 
			reg1->name,  i, reg1->nrow, reg1->ncol, shEnumNameGetFromValue("FABRICATION_FLAG", fab));
		}
		status = shRegAddshRegCentered(reg1, amp, reg2);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not copy source (reg[%d]) into target (reg)", name, i);
			return(status);
		}
	}
}

return(SH_SUCCESS);
}

RET_CODE thRegion3ConvertToshReg(THREGION3 *threg, REGION *reg) {
char *name = "thRegion3ConvertToshReg";
thError("%s: ERROR - function not currently supported", name);
return(SH_GENERIC_ERROR);
}


RET_CODE shRegAddshRegCentered(REGION *reg1, THPIX amp, REGION *reg2) {
char *name = "shRegAddshRegCentered";

if (reg1 == NULL && reg2 == NULL) {
	thError("%s: ERROR - null input and output placeholder", name);
	return(SH_SUCCESS);
}
if (reg1 == NULL) {
	thError("%s: WARNING - null input - not changing the output", name);
	return(SH_SUCCESS);
}
if (reg2 == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}

int nr1, nc1, nr2, nc2;
nr1 = reg1->nrow;
nc1 = reg1->ncol;
nr2 = reg2->nrow;
nc2 = reg2->ncol;
TYPE t1, t2;
t1 = reg1->type;
t2 = reg2->type;
char *name1 = reg1->name;
char *name2 = reg2->name;
if (t1 != t2) {
	thError("%s: ERROR - mismatching types between source and target (reg)", name);
	return(SH_GENERIC_ERROR);
}
if (nr1 == 0 || nc1 == 0) {
	thError("%s: WARNING - empty source region (name = '%s', nr = %d, nc = %d)", name, name1, nr1, nc1);
	return(SH_SUCCESS);
}
if (nr2 == 0 || nc2 == 0) {
	thError("%s: WARNING - empty target region (name = '%s', nr = %d, nc = %d)", name, nr2, nc2, name2);
	return(SH_SUCCESS);
}
if (nr1 % 2 != nr2 % 2) {
	thError("%s: WARNING - addition works better if (name1 = '%s', nr1 = %d) and (name2 = '%s', nr2 = %d) have the same parity", name, name1, nr1, name2, nr2);
}
if (nc1 % 2 != nc2 % 2) {
	thError("%s: WARNING - addition works better if (reg1->nc = %d) and (reg2->nc = %d) have the same parity", name, nc1, nc2);
	}
int nr, nc;
nr = MIN(nr1, nr2);
nc = MIN(nc1, nc2);
int row01, row02, col01, col02;
row01 = (nr1 - nr)/2;
row02 = (nr2 - nr)/2;
col01 = (nc1 - nc)/2;
col02 = (nc2 - nc)/2;
REGION *sreg1 = shSubRegNew("subregion for source", reg1, nr, nc, row01, col01, NO_FLAGS);
REGION *sreg2 = shSubRegNew("subregion for target", reg2, nr, nc, row02, col02, NO_FLAGS);
if (sreg1 == NULL) {
	thError("%s: ERROR - could not make subregion for source", name);
	return(SH_GENERIC_ERROR);
}
if (sreg2 == NULL) {
	thError("%s: ERROR - could not make subregion for target", name);
	return(SH_GENERIC_ERROR);
}
int i, j;
if (t1 == TYPE_THPIX && t2 == TYPE_THPIX) {
	for (i = 0; i < nr; i++) {
		THPIX *srow_i = sreg1->rows_thpix[i];
		THPIX *trow_i = sreg2->rows_thpix[i];
		for (j = 0; j < nc; j++) {
			trow_i[j] += srow_i[j] * amp;
		}
	}
} else {
	thError("%s: ERROR - pixel type not supported (source = '%s', target = '%s')", name, 
		shEnumNameGetFromValue("PIXDATATYPE", t1), 
		shEnumNameGetFromValue("PIXDATATYPE", t2));
	return(SH_GENERIC_ERROR);
}

shRegDel(sreg1);
shRegDel(sreg2);
return(SH_SUCCESS);
}

RET_CODE shRegCountBadPixel(REGION *reg, int *nbadpixel) {
char *name = "shRegCountBadPixel";
if (reg == NULL || nbadpixel == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
int nbad = 0;
if (reg->type != TYPE_THPIX) {
	thError("%s: ERROR - region pixel type not supported", name);
	*nbadpixel = -1;
	return(SH_GENERIC_ERROR);
}
int nrow = reg->nrow;
int ncol = reg->ncol;
int i, j;
for (i = 0; i < nrow; i++) {
	THPIX *rows_i = reg->rows_thpix[i];
	for (j = 0; j < ncol; j++) nbad += (isnan(rows_i[j]) || isinf(rows_i[j]));
}
*nbadpixel = nbad;
return(SH_SUCCESS);
}


RET_CODE expand_rectangle_corners(int *rows, int *cols, THPIX factor, THPIX size) {
char *name = "expand_rectangle_corners";
if (rows == NULL || cols == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (factor < 1.0) {
	thError("%s: ERROR - found (factor = %g) while looking for an expansion", name, (float) factor);
	return(SH_GENERIC_ERROR);
}
if (size <= (THPIX) 0.0) {
	thError("%s: WARNING - ineffective minimum (size = %g) passed", name, (float) size);
}
THPIX rowc = ((THPIX) rows[0] + (THPIX) rows[1]) / 2.0;
THPIX colc = ((THPIX) cols[0] + (THPIX) cols[1]) / 2.0;
THPIX dr = (THPIX) rows[1] - (THPIX) rows[0];
THPIX dc = (THPIX) cols[1] - (THPIX) cols[0];
dr *= factor;
dc *= factor;
if (dr < size) dr = size;
if (dc < size) dc = size;

THPIX row_up = rowc + dr;
THPIX row_low = rowc - dr;
THPIX col_up = colc + dc;
THPIX col_low = colc - dc;

rows[0] = (int) (row_low - 0.5);
rows[1] = (int) (row_up + 0.5);
cols[0] = (int) (col_low - 0.5);
cols[1] = (int) (col_up + 0.5);

return(SH_SUCCESS);
}
