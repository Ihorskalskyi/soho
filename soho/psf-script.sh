#!/bin/bash

source "./write_condorjob.sh"
source "./write_parfile.sh"
source "./create_field_array.sh"
source "./find_npsf.sh"

name="psf-script"
DATE=$(date +"%m-%d-%Y")
TIME=$(date +"%T") 


debug=no
verbose=no
docondor=no
separatecamcol=no

lroot=no
lband=no
lcamcol=no
lrun=no
lrerun=no
lmodel=no
lfstep=no
lfield0=no
lfield1=no
lcondordir=no
lparfiledir=no
ldatadir=no
loutdir=no
lexecutable=no
lEXTRA_FLAGS=no
lexcept=no
lcommandfile=no
lnpsfstar=no
lppsf=no

droot="/u/khosrow/thesis/opt/soho"
ddatadir="$root/single-galaxy/images/psf-analysis"
dband="r"
dcamcol="4"
drun=$DEFAULT_SDSS_RUN
drerun=$DEFAULT_SDSS_RERUN
dfield0=101
dfield1=490
dexecutable="do-psf"
dexcept=0
dnpsfstar=5
dppsf=0.5

fstep=1
while getopts ':-:' OPTION ; do
  case "$OPTION" in
    -  ) [ $OPTIND -ge 1 ] && optind=$(expr $OPTIND - 1 ) || optind=$OPTIND
         eval OPTION="\$$optind"
         OPTARG=$(echo $OPTION | cut -d'=' -f2)
         OPTION=$(echo $OPTION | cut -d'=' -f1)
         case $OPTION in
	     --debug     ) debug=yes			 ;;
	     --docondor  ) docondor=yes			 ;;
     	     --verbose   ) verbose=yes			 ;;
	     --root      ) lroot=yes; root="$OPTARG"     ;;
	     --field0	 ) lfield0=yes; field0="$OPTARG"   ;;
	     --field1    ) lfield1=yes; field1="$OPTARG" ;;
     	     --run       ) lrun=yes; run="$OPTARG"	 ;;
	     --rerun     ) lrerun=yes; rerun="$OPTARG"    ;; 
	     --camcol    ) lcamcol=yes; camcol="$OPTARG" ;;
	     --band      ) lband=yes; band="$OPTARG"	 ;;
             --condordir ) lcondordir=yes; condordir="$OPTARG" ;;     
	     --parfiledir) lparfiledir=yes; parfiledir="$OPTARG"		;;
	     --outdir	 ) loutdir=yes; outdir="$OPTARG" ;;
	     --datadir   ) ldatadir=yes; datadir="$OPTARG"     ;;
	     --executable	) lexecutable=yes; executable="$OPTARG"		;;
	     --except           ) lexcept=yes; except="$OPTARG"			;;
	     --npsfstar		) lnpsfstar=yes; npsfstar="$OPTARG"		;;
	     --ppsf		) lppsf=yes; ppsf="$OPTARG"			;;
	     --commandfile	) lcommandfile=yes; commandfile="$OPTARG"	;;
	     --EXTRA_FLAGS	) lEXTRA_FLAGS=yes; EXTRA_FLAGS="$OPTARG"	;;
	* )  echo "$name: invalide options (long)" ;;
         esac
       OPTIND=1
       shift
      ;;
    ? )  echo "$name: invalid options (short) "  ;;
  esac
done

if [ $lroot == no ]; then
	root=$droot
	echo "$name: argument (root)   set to '$root'"
fi
if [ $lrun == no ]; then
	run=$drun
	echo "$name: argument (run)    set to '$run'"
fi
if [ $lrerun == no ]; then
	rerun=$drerun;
	echo "$name: argument (rerun)  set to '$rerun'"
fi
if [ $lmodel == no ]; then
	model=$dmodel;
	echo "$name: argument (model) set to '$model'"
fi
if [ $lcamcol == no ]; then
	camcol=$dcamcol
	echo "$name: argument (camcol) set to '$camcol'"
fi
if [ $lband == no ]; then
	band=$dband
	echo "$name: argument (band)   set to '$band'"
fi
if [ $ldatadir == no ]; then
	datadir=$ddatadir
	echo "$name: argument (datadir) set to '$datadir'"
fi
if [ $loutdir == no ]; then
	outdir=$datadir;
	echo "$name: argument (outdir) set to '$outdir'"
fi
if [ $lcondordir == no ]; then
	condordir="$outdir/condor"
	echo "$name: argument (condordir) set to '$condordir'"
fi
if [ $lparfiledir == no ]; then
	parfiledir="$outdir/parfiles"
	echo "$name: argument (parfiledir) set to '$parfiledir'"
fi
if [ $lexecutable == no ]; then
	executable="$root/$dexecutable"
	echo "$name: argument (executable) set to '$executable'"
fi
if [ $lexcept == no ]; then
	except=$dexcept
	echo "$name: argument (except) set to '$except'"
fi
if [ $lnpsfstar == no ]; then
	npsfstar=$dnpsfstar
	echo "$name: argument (npsfstar) set to '$npsfstar'"
fi
if [ $lppsf == no ]; then
	ppsf=$dppsf
	echo "$name: argument (ppsf) set to '$ppsf'"
fi
if [ $lcommandfile == yes ]; then
	if [[ -e $commandfile ]]; then
		echo "$name: WARNING - export file for commands '$commandfile' already exists. appending"
	fi
fi	
if [ $lEXTRA_FLAGS == no ]; then
	EXTRA_FLAGS=""
fi

echo "$name: debug    = $debug"
echo "$name: verbose  = $verbose"
echo "$name: EXTRA_FLAGS = '$EXTRA_FLAGS'"

if [ ! -d $condordir ]; then
	echo "$name: making (condordir) '$condordir'"
	mkdir -p $condordir
fi
clogdir="$condordir/log"
cerrordir="$condordir/error"
coutputdir="$condordir/output"
csubmitdir="$condordir/submit"
objcdir="$outdir/objc"

if [ ! -d $clogdir ]; then
	echo "$name: making (clogdir) '$clogdir'"
	mkdir -p $clogdir
fi
if [ ! -d $cerrordir ]; then
	echo "$name: making (cerrordir) '$cerrordir'"
	mkdir -p $cerrordir
fi
if [ ! -d $coutputdir ]; then
	echo "$name: making (coutputdir) '$coutputdir'"
	mkdir -p $coutputdir
fi
if [ ! -d $csubmitdir ]; then
	echo "$name: making (csubmitdir) '$csubmitdir'"
	mkdir -p $csubmitdir
fi
if [ ! -d $parfiledir ]; then
	echo "$name: making (parfiledir) '$parfiledir'"
	mkdir -p $parfiledir
fi
if [ ! -d $datadir ]; then
	echo "$name: making (datadir) '$datadir'"
	mkdir -p $datadir
fi
if [ ! -d $outdir ]; then
	echo "$name: making (outdir) '$outdir'"
	mkdir -p $outdir
fi
if [ ! -d $objcdir ]; then
	echo "$name: making (objcdir) '$objcdir'"
	mkdir -p $objcdir
fi
srcdir_redux="$PHOTO_REDUX/$DEFAULT_SDSS_RERUN/$run/objcs/$camcol"
srcdir_data="$PHOTO_DATA/$DEFAULT_SDSS_RERUN/$run/objcs/$camcol"
srcdir_calib="$PHOTO_CALIB/$DEFAULT_SDSS_RERUN/$run/objcs/$camcol"
if [ ! -d $srcdir_redux ]; then 
	echo "$name: SDSS source directory '$srcdir_redux' does not exist"
	exit
fi


tempfile="./temp-psf-condor-submit.output"
fpCprefix="fpC"
fpCCprefix="fpCC"
psfRprefix="psfR"
psfSprefix="psfS"

ifield=$(( 10#$field0 ))
run=$(( 10#$run ))
runstr=`printf "%06d" $run`

# find the first field and the number of fields with psField files available
echo "$name: investigating available fields for run = $run, band = $band"
field0=$(find_f0 --run=$run --camcol=$camcol --band=$band)
if [ $field0 -eq -1 ]; then
	echo "$name: the fold does not contain any psField files"
	exit
fi
echo "$name: investigating number of fields for run = $run, band = $band"
npsf=$(find_npsf --run=$run --camcol=$camcol --field0=$field0 --band=$band)
echo "$name: field0 = $field0, npsf = $npsf"

let "field1 = $field0 + $npsf - 1"
#creating an array of available fields
field_array_string=$(create_field_array --datadir="$datadir" --field0="$field0" --run=$run --camcol="$camcol" --field1=$field1 --band=$band --fstep=$fstep)
field_array=(${field_array_string//,/ })
nfield_array=${#field_array[@]}
if [[ $debug == yes ]]; then  
	echo "$name: fields in string = $field_array_string"
	echo "$name: fields in array  = ${field_array[@]}"
fi
njob=0
nbadjob=0
#while [[ $dataexists == yes ]]
fremaining=`echo "$nfield_array*$ppsf" | bc -l`
nremaining=${fremaining%.*}
let nremaining++
echo "$name: n-remaining = $nremaining"
while [[ $nremaining -gt 0 ]]
do
	#selecting a random field from the array of fields
	nfield_array=${#field_array[@]} 
	if [[ $nfield_array -gt 1 ]]; then 
		let nfield_array++
		irand=$(python -S -c "import random; print random.randrange(1,$nfield_array)")
		let nfield_array--
		let nremaining--
	else 
		irand=$nfield_array
		let nremaining--
	fi
	let irand--
	#echo "$name: irand = $irand"
	ifield=${field_array[$irand]} #taking a random element from the field array
	#echo "$name: ifield = $ifield"
	fieldstr=`printf "%04d" $ifield`
	parfilename="ff-psf-$runstr-$band$camcol-$fieldstr.par"
	srcparfile="$datadir/$parfilename"
	targetparfile="$parfiledir/$parfilename"
	
	if [[ $debug == yes ]]; then 	
		#echo "$name: remaining field array = ${field_array[@]}"
		echo "$name: irand = $irand, nfield_array = $nfield_array, ifield = $ifield, fieldstr = $fieldstr"
	fi	
	#condor job
	#making new par file based on the old one and altering the outdir
	comment="'DATE: $DATE, TIME: $TIME, EXTRA_FLAGS: $EXTRA_FLAGS'"
	write_parfile --datadir="$datadir" --field=$ifield --psffield=$ifield --run=$run --camcol=$camcol --band=$band --outdir="$outdir" --comment="$comment" --output="$targetparfile"	
	echo -n "~"
	psfRfile="$objcdir/$psfRprefix-$runstr-$band$camcol-$fieldstr.fit"
	psfSfile="$objcdir/$psfSprefix-$runstr-$band$camcol-$fieldstr.fit"


	condorprefix="condor-psf-$runstr-$fieldstr-$band$camcol"
	condor_submitfile="$csubmitdir/$condorprefix.submit"
	condor_errorfile="$cerrordir/$condorprefix.error"
	condor_logfile="$clogdir/$condorprefix.log"
	condor_outputfile="$coutputdir/$condorprefix.out"
	condor_argument="--framefile $targetparfile --psfreportfile $psfRfile --psfstatfile $psfSfile --run $runstr --field $fieldstr --band $band --camcol $camcol --npsfstar $npsfstar $EXTRA_FLAGS"
	condor_comment="date: $DATE, time: $TIME"
	#writing condor job
	write_condorjob --submitfile=$condor_submitfile --errorfile=$condor_errorfile --logfile=$condor_logfile --outputfile=$condor_outputfile --executable="$executable" --arguments="$condor_argument" --comment="$condor_comment"
	if [[ $debug == yes ]]; then 
		echo "$name: $condor_submitfile"
	else 
		if [[ $verbose == yes ]]; then 
			echo "$name: psfR = $psfRfile, psfS = $psfSfile"
		fi
		if [ $debug == yes ]; then
			echo -n 'e'
		else 

			if [ $docondor == yes ]; then
 
				if [ $lcommandfile == yes ]; then
					echo "condor_submit $condor_submitfile >> $tempfile" >> $commandfile
				else
					eval "condor_submit $condor_submitfile >> $tempfile"
				fi

			else

				echo "$name: $executable $condor_argument >> $tempfile"
				eval "$executable $condor_argument >> $tempfile"

			fi

			echo -n "."
		fi
	fi

	let njob++
	# now deleting the selected field from the list, and updating the array size, it is important to delete null array elements after deleting an element.
	unset field_array[$irand]
	field_array=(${field_array[@]})
	nfield_array=${#field_array[@]}
	#if the next field is out of the range then stop processing it#
	# let "ifield+=$fstep"
	#if [[ $ifield -gt $field1 ]]; then 
	#	dataexists=no
	#fi
done

echo ""
echo "$name: $njob jobs submitted"
if [[ $nbadjob -gt 0 ]]; then
	echo "$name: $nbadjob cases of non-existent fpC / fpCC files found - jobs not submitted"
fi

