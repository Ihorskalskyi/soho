#include "thDebug.h"
#include "thMath.h"
#include "math.h"
#include "stdlib.h"
#include "phRandom.h"

typedef enum {
  ftype, dtype, ldtype, unknowntype
} THPIX_REAL_TYPE;

static THPIX_REAL_TYPE thpixtype;

static MATHDBLE epsilon1 (MATHDBLE x);
static MATHDBLE epsilon2(MATHDBLE x);
static MATHDBLE epsilon3(MATHDBLE x);
static MATHDBLE epsilon4(MATHDBLE x);
static MATHDBLE epsilon(MATHDBLE eta, MATHDBLE a);
static MATHDBLE eta_zero(MATHDBLE q, MATHDBLE a);

RET_CODE thMathInit() {
  int msize, dsize, fsize, ldsize;
  msize = sizeof(THPIX);
  fsize = sizeof(float);
  dsize = sizeof(double);
  ldsize = sizeof(long double);
  
  thpixtype = unknowntype;

  if (msize == fsize) {
    thpixtype = ftype;
  }

  if (msize == dsize) {
    thpixtype = dtype;
  }
  if (msize == ldsize) {
    thpixtype = ldtype;
  }

  if (thpixtype == unknowntype) {
    return(SH_GENERIC_ERROR);
  }

  return(SH_SUCCESS);
}

RET_CODE LMatrixInvert(MLEFL **arr, MLEFL *b, MLEFL *c, int n, int *singular) { 
  char *name = "LMatrixInvert";

  if (singular == NULL) {
	thError("%s: ERROR - null output placeholder for singularity indicator", name); 
	return(SH_GENERIC_ERROR);
	}
  MATHDBLE *db;
  MATHDBLE **darr;
  int converted = 0;
	
  MATHDBLE *darri;
  MLEFL *arri;

  int i, j;
  if (sizeof(MLEFL) != sizeof(MATHDBLE)) {
	converted = 1;
  	darr = (MATHDBLE **) thCalloc(n, sizeof(MATHDBLE *));
  	darr[0] = (MATHDBLE *) thCalloc(n * n, sizeof(MATHDBLE));
  	db = (MATHDBLE *) thCalloc(n, sizeof (MATHDBLE));
  	
	for (i = 0; i <n; i++) {
		db[i] = (MATHDBLE) b[i];
		darr[i] = darr[0] + n * i;
		darri = darr[i];
		arri = arr[i];
		for (j = 0; j < n; j++) {
			darri[j] = (MATHDBLE) arri[j];
      		}
   	}
  } else {
	converted = 0;
	darr = arr;
	db = c;
	for (i = 0; i < n; i++) c[i] = b[i];
  }

  if (thDMatrixInvert(darr, db, n) < 0) {

	*singular = 1;
	#if DEBUG_MATH_SINGULAR
	thError("%s: WARNING - singular matrix[%d x %d]", name, n, n);
	#endif
	fflush(stderr);
	fflush(stdout);
	} else {
 
	#if DIRECT_MULTIPLY_INVERSE
	/* direct multiplication */
	   for (i = 0; i < n; i++) {
		db[i] = (MATHDBLE) b[i];
		}
	   for (i = 0; i < n; i++) {
		arri = arr[i];
		darri = darr[i];
		MATHDBLE tmp = 0;
		for (j = 0; j < n; j++) {
			tmp += darri[j] * db[j];
			if (converted) arri[j] = (MLEFL) (darri[j]);
		}
		c[i] = (MLEFL) tmp;
		}

	#else
		if (converted) {
		for (i = 0; i < n; i++) {
     		 	c[i] = (MLEFL) (db[i]);
   			arri = arr[i];
     			 darri = darr[i];
      			for (j = 0; j < n; j++) {
				arri[j] = (MLEFL) (darri[j]);
      			}
    		}
		}
	#endif
	}
    
  if (converted) {
	    thFree(darr[0]);
	    thFree(darr);
	    thFree(db);
	}

  return(SH_SUCCESS);
}
 

RET_CODE thMatrixInvert(THPIX **arr, THPIX *b, int n) {
  
  char *name = "thMatrixInvert";

  MATHDBLE *db;
  MATHDBLE **darr;

  darr = (MATHDBLE **) thCalloc(n, sizeof(MATHDBLE *));
  darr[0] = (MATHDBLE *) thCalloc(n * n, sizeof(MATHDBLE));
  db = (MATHDBLE *) thCalloc(n, sizeof (MATHDBLE));

  MATHDBLE *darri;
  THPIX *arri;

  int i, j;
  for (i = 0; i <n; i++) {
	db[i] = (MATHDBLE) b[i];
	darr[i] = darr[0] + n * i;
	darri = darr[i];
	arri = arr[i];
	for (j = 0; j < n; j++) {
		darri[j] = (MATHDBLE) arri[j];
      	}
	}
	
  if (thDMatrixInvert(darr, db, n) < 0) {
	thError("%s: ERROR - singular matrix[%d x %d]", name, n, n);
	fflush(stderr);
	fflush(stdout);
	return(SH_GENERIC_ERROR);
	}
 
#if DIRECT_MULTIPLY_INVERSE
/* direct multiplication */
   for (i = 0; i < n; i++) {
	db[i] = (MATHDBLE) b[i];
	}
   for (i = 0; i < n; i++) {
	arri = arr[i];
	darri = darr[i];
	MATHDBLE tmp = 0;
	for (j = 0; j < n; j++) {
		tmp += darri[j] * db[j];
		arri[j] = (THPIX) (darri[j]);
	}
	b[i] = (THPIX) tmp;
	}

#else
	for (i = 0; i < n; i++) {
     	 	b[i] = (THPIX) (db[i]);
   		arri = arr[i];
     		 darri = darr[i];
      		for (j = 0; j < n; j++) {
			arri[j] = (THPIX) (darri[j]);
      		}
    	}

#endif

    thFree(darr[0]);
    thFree(darr);
    thFree(db);


  return(SH_SUCCESS);
}
    
  
/*
The following subroutines calculate T_n,m(x, y) = T_n(x) T_m(y)     
*/

void assign_coord(VECT  *vec, THPIX x0, THPIX x1){

  int i, n;
  THPIX alpha;

  n = vec->dim;
  alpha = (x1 - x0) / ((THPIX) (2 * n));
  
  for (i = 0; i < n; i++) {
    vec->vec[i] = ((THPIX) (2 * i + 1)) * alpha + x0;
  }
}

void assign_knots(VECT  *vec, THPIX x0, THPIX x1){

  int i, n;
  THPIX alpha;

  n = vec->dim;
  alpha = (x1 - x0) / ((THPIX) (n - 1));
  
  for (i = 0; i < n; i++) {
    vec->vec[i] = ((THPIX) i) * alpha + x0;
  }
}

VECT *do_tcheb_vec(VECT *vec, int n) {

  int i, dim;
  THPIX nu;
  VECT *y;

  nu = (THPIX) n;
  dim = vec->dim;

  y = thVectNew(dim);

  for (i = 0; i < dim; i++) {
    y->vec[i] = cos(nu * acos(vec->vec[i]));
  }
  y->r = i; /* should this not read v->r = nu (or n) */
  return(y);

}

REGION *gen_reg_vec_vec(VECT *row, VECT *col){

  int i, j, nrow, ncol;

  THPIX row_val, *reg_val;

  nrow = row->dim;
  ncol = col->dim;

  REGION *reg;

  reg = shRegNew("", nrow, ncol, TYPE_THPIX);

  for (i = 0; i < nrow; i++) {
    reg_val = (reg->rows_thpix)[i];
    row_val = (row->vec)[i];
    for (j = 0; j < ncol; j++) {
	  reg_val[j] = (THPIX) (row_val * (col->vec)[j]);
    }
  }
  
  return(reg);

}

CHAIN *gen_tcheb_chain_vec(VECT *vec, int n) {

  int i;

  VECT *tcheb_vec; 
  CHAIN *tcheb_chain;


  tcheb_chain = shChainNew("VECT");
  for(i = 0; i < n; i++) {
    tcheb_vec = do_tcheb_vec(vec, i);
    tcheb_vec->r = i;
    shChainElementAddByPos(tcheb_chain, tcheb_vec, "VECT", TAIL, AFTER);
  }

  return(tcheb_chain);

}

CHAIN *gen_bspline_chain_vec(VECT *rcoords, VECT *t) {
  
  int i, Nt, Ni;
  Ni = rcoords->dim;
  Nt = t->dim;

  VECT *bspline_vec; 
  CHAIN *bspline_chain;
  THPIX **bspline;
  
  /*
  bspline_chain = shChainNew("VECT");
  for(i = 0; i < Nt; i++) {
    
    bspline_vec = thVectNew(Ni);
    gen_Bspline_basis(rcoords->vec, bspline_vec->vec, t->vec, i, Nt, Ni); 

    shChainElementAddByPos(bspline_chain, bspline_vec, "VECT", TAIL, AFTER);
  }
  
  return(bspline_chain);
  */
  int k = BSPLINE_ORDER;

  bspline_chain = shChainNew("VECT");
  int n_basis = -1;
  bspline = get_Bspline_basis (rcoords->vec, t->vec, rcoords->dim, t->dim, k, &n_basis);
  shAssert(n_basis > 0);
  for(i = 0; i < n_basis; i++) {
    
    bspline_vec = (VECT *) thCalloc(1, sizeof(VECT));
    bspline_vec->dim = Ni;
    bspline_vec->r = i;
    bspline_vec->vec = bspline[i];
    bspline_vec->child = 1;
    shChainElementAddByPos(bspline_chain, bspline_vec, "VECT", TAIL, AFTER);
  }
  
  return(bspline_chain);

}


CHAIN *gen_tcheb_chain_func(int nrow, int ncol,  
			    /* size of the region */
			    int n_tr, int n_tc,  
			    /* number of tchebychev polynomials 
			       in each direction
			    */
			    THPIX r0, THPIX r1,     
			    /* beginning and end coordinate in 
			       direction of rows
			       suggested values: 0.0, 1.0
			    */
			    THPIX c0, THPIX c1      
			    /* beginning and end coordinate in 
			       direction of columns
			       suggested values: 0.0, 1.0
			    */
			    ) {

  VECT *rcoords, *ccoords;

  rcoords = thVectNew(nrow);
  ccoords = thVectNew(ncol);

  assign_coord(rcoords, r0, r1);
  assign_coord(ccoords, c0, c1);

  CHAIN *tcheb_rchain, *tcheb_cchain;
  tcheb_rchain = gen_tcheb_chain_vec(rcoords, n_tr);
  tcheb_cchain = gen_tcheb_chain_vec(ccoords, n_tc);

  CHAIN *func_chain;
  func_chain = shChainNew("FUNC");


  int *indices;
  indices = (int *) thCalloc(FUNC_N_INDICES, sizeof(int));

  THPIX *pars;
  pars = (THPIX *) thCalloc(FUNC_N_PARS, sizeof(THPIX));

  int i, j; 
  VECT *rvec, *cvec;
  REGION *reg;
  FUNC *func;


  for (i = 0; i < n_tr; i++) {
    for (j = 0; j < n_tc; j++) {

      rvec = (VECT *) shChainElementGetByPos(tcheb_rchain, i);
      cvec = (VECT *) shChainElementGetByPos(tcheb_cchain, j);

      reg = gen_reg_vec_vec(rvec, cvec);
      
      func = thFuncNew();
      
      sprintf(func->name, "TCHEBYCHEV (i_row = %d, i_col = %d)", i, j);
      
      indices[0] = i;
      indices[1] = j;

      pars[0] = r0;
      pars[1] = r1;
      pars[2] = c0;
      pars[3] = c1;

      thFuncPut(func, 0, 0, 0, 0, indices, pars, reg);

      shChainElementAddByPos(func_chain, func, "FUNC", TAIL, AFTER);

    }
  }

  thFree(indices);
  return(func_chain);

}
   

CHAIN *gen_Bspline_vec(int n, int m, THPIX x0, THPIX x1) {
  VECT *coords;

  coords = thVectNew(n);
  assign_coord(coords, x0, x1);

  /* get the knots */
  VECT *t;
  t = thVectNew(m);
  assign_knots(t, x0, x1);

  /* now create the function chain */
  CHAIN *z_chain;
  z_chain = gen_bspline_chain_vec(coords, t);

  return(z_chain);
}

CHAIN *gen_tcheb_vec(int n, int m, THPIX x0, THPIX x1) {

  VECT *coords;
  coords = thVectNew(n);
  assign_coord(coords, x0, x1);

  /* now create the function chain */
  CHAIN *z_chain;
  z_chain = gen_tcheb_chain_vec(coords, m);
  
  return(z_chain);
}

CHAIN *gen_Bspline_tcheb_chain_func(int nrow, int ncol,  
				    /* size of the region */
				    int n_tr, int n_tc,  
				    /* number of tchebychev polynomials 
				       in each direction
				    */
				    THPIX r0, THPIX r1,     
				    /* beginning and end coordinate in 
				       direction of rows
				       suggested values: 0.0, 1.0
				    */
				    THPIX c0, THPIX c1      
				    /* beginning and end coordinate in 
				       direction of columns
				       suggested values: 0.0, 1.0
				    */
				    ) {

  VECT *rcoords, *ccoords;

  rcoords = thVectNew(nrow);
  ccoords = thVectNew(ncol);

  assign_coord(rcoords, r0, r1);
  assign_coord(ccoords, c0, c1);

  /* get the knots */
  VECT *t;
  t = thVectNew(n_tr);
  assign_knots(t, r0, r1);

  /* now create the function chain */
  CHAIN *rchain, *cchain;
  rchain = gen_bspline_chain_vec(rcoords, t);
  cchain = gen_tcheb_chain_vec(ccoords, n_tc);

  CHAIN *func_chain;
  func_chain = shChainNew("FUNC");


  int *indices;
  indices = (int *) thCalloc(FUNC_N_INDICES, sizeof(int));

  THPIX *pars;
  pars = (THPIX *) thCalloc(FUNC_N_PARS, sizeof(THPIX));

  int i, j, imax, jmax; 
  VECT *rvec, *cvec;
  REGION *reg;
  FUNC *func;

  /* multiply to get the 2d basis functions */
  
  imax = shChainSize(rchain);
  jmax = shChainSize(cchain);

  for (i = 0; i < imax; i++) {
    for (j = 0; j < jmax; j++) {

      rvec = (VECT *) shChainElementGetByPos(rchain, i);
      cvec = (VECT *) shChainElementGetByPos(cchain, j);

      reg = gen_reg_vec_vec(rvec, cvec);
      
      func = thFuncNew();
      
      sprintf(func->name, "BSPLINE x TCHEBYCHEV (irow= %d, icol= %d), (imax= %d, jmax= %d)", i, j, imax, jmax);
      
      indices[0] = i;
      indices[1] = j;

      pars[0] = r0;
      pars[1] = r1;
      pars[2] = c0;
      pars[3] = c1;

      thFuncPut(func, 0, 0, 0, 0, indices, pars, reg);

      shChainElementAddByPos(func_chain, func, "FUNC", TAIL, AFTER);

    }
  }

  thFree(indices);
  return(func_chain);

}

int thGetVectPosByIndex(CHAIN *vchain, int r) {
char *name = "thGetVectPosByIndex";
if (vchain == NULL) {
	thError("%s: ERROR - null (vchain) passed", name);
	return(BAD_POS);
}
int n = shChainSize(vchain);
if (n == 0) {
	thError("%s: ERROR - empty (vchain) passed", name);
	return(BAD_POS);
}

VECT *v;
int i, pos = BAD_POS, match = 0;
for (i = 0; i < n; i++) {
	v = shChainElementGetByPos(vchain, i);
	if (v != NULL) {
		if (v->r == r) {
			if (!match) pos = i;	
			match++;
		}
	} 
}
if (match > 1) thError("%s: WARNING - (%d) vectors in the chain matched (r = %d)", 
		name, match, r);
if (match == 0) thError("%s: WARNING - no vectors in the chain matched (r = %d)", name);
return(pos);
}

int thGetFuncPosByPar(CHAIN *fchain, int *indices, THPIX *pars) {

  char *name = "thGetPosByPar";

  if (fchain == NULL || shChainSize(fchain) == 0) return(-1);
 
  int nchain;
  nchain = shChainSize(fchain);

  if (pars == NULL && indices == NULL) return(-1);

  int i, match, ipos, npos = 0, pos = -1;

  FUNC *func;

  #if DEEP_DEBUG_MATH
  printf("%s: searching for (", name);
  for (i = 0; i < FUNC_N_INDICES; i++) {
	printf("%d ", indices[i]);
  }
  printf(") \n%s: ", name);
  #endif

  for (ipos = 0; ipos < nchain; ipos++) {
    
    func = (FUNC *) shChainElementGetByPos(fchain, ipos);
    
    match = 1;
    if (indices != NULL) {
	#if DEEP_DEBUG_MATH
	printf("(");
	#endif
     	for (i = 0; i < FUNC_N_INDICES; i++) {
		#if DEEP_DEBUG_MATH
		printf("%d ", func->indices[i][0]);
		#endif
		match = match && (indices[i] == func->indices[i][0]);
	}
	#if DEEP_DEBUG_MATH
	printf(")");
	if (match) printf("**");
	#endif
      }
 
    if (pars != NULL) {
       for (i = 0; i < FUNC_N_PARS; i++) {
		match = match && (pars[i] == func->pars[i][0]);
       }
    }

    	if (match) {
		pos = ipos;
		npos++;
	}

  }

#if DEEP_DEBUG_MATH
printf("\n");
#endif
  if (npos > 1) {
	thError("%s: ERROR - more than one matching function (%d) was found in the list", name, npos);
	pos = -1;
	}
  return(pos);

}


/* 
   the following section helps calculate the coefficients to b-spline
   with nonuniformaly spaced nodes

*/


void gen_Bspline_basis(THPIX *x, THPIX *b, 
		       THPIX *t, int j, 
		       int Nx, int N) {
  
  /* t are assumed to be ordered, 
     x is not assumed to be ordered
     node -1, -2, and N+1 are not in the node array
  */

  int i, k;
  THPIX *delta;
  delta = (THPIX *) thCalloc(N + 1, sizeof(THPIX));
  for (i = 0; i < N; i++) {
    delta[i] = t[i+1] - t[i];
  }
  delta[N] = delta[N-1];

  for (i = 0; i < Nx; i++) {
    for (k = -2; k < 2; k++) {
      if (calc_spline_Dj(x[i], t, delta, j + k, N)) {
	gen_spline_Bjk(x + i, b+i, t, delta, j, k, N, 1);
	break;
      }
      if (k == 2) {
	b[i] = (THPIX) 0.0;
      }
    }
  }
    
  thFree(delta);
  return;
}



void gen_spline_Bjk(THPIX *x, THPIX *b, THPIX *t, THPIX *delta, 
		    int j, int k, int N, int Nx) {
  
  char *name = "gen_spline_Bj";

  int i;

  if (k < -2 || k > 1) {
    for (i = 0; i < Nx; i++) {
      b[i] = (THPIX) 0.0;
    }
    return;
  }
  
  THPIX deltaJm2, deltaJm1, deltaJ, deltaJp1, deltaJp2;
  THPIX xJm2, xJm1, xJ, xJp1, xJp2;

  if (j - 2 >= 0 && j - 2 <= N) {
    deltaJm2 = delta[j - 2];
    xJm2 = t[j - 2];
  } else if (j - 2 < 0) {
    deltaJm2 = delta[0];
    xJm2 = t[0] + (j - 2) * delta[0];
  } else if (j - 2 > N) {
    deltaJm2 = delta[N];
    xJm2 = t[N] + (j - 2 - N) * delta[N];
  } else {
    thError("%s: improper value for j (%d) , delta(j-2) cannot be defined", name, j);
    return;
  }

  if (j - 1 >= 0 && j - 1 <= N) {
    deltaJm1 = delta[j - 1];
    xJm1 = t[j - 1];
  } else if (j - 1 < 0) {
    deltaJm1 = delta[0];
    xJm1 = t[0] + (j - 1) * delta[0];
  } else if (j - 1 > N) {
    deltaJm1 = delta[N];
    xJm1 = t[N] + (j - 1 - N) * delta[N];
  } else {
    thError("%s: improper value for (j = %d, N = %d), delta(j-1) cannot be defined", name, j, N);
    return;
  }

  if (j >= 0 && j <= N) {
    deltaJ = delta[j];
    xJ = t[j];
  } else if (j < 0) {
    deltaJ = delta[0];
    xJ = t[0] + j * delta[0];
  } else if (j > N) {
    deltaJ = delta[N];
    xJ = t[j] + (j - N) * delta[N];
  } else {
    thError("%s: improper value for (j = %d, N = %d), delta(j) cannot be defined", name, j, N);
    return;
  }

   if (j + 1 >= 0 && j + 1 <= N) {
    deltaJp1 = delta[j + 1];
    xJp1 = t[j + 1];
  } else if (j + 1 < 0) {
    deltaJp1 = delta[0];
    xJp1 = t[0] + (j + 1) * delta[0];
  } else if (j + 1 > N) {
    deltaJp1 = delta[N];
    xJp1 = t[N] + (j + 1 - N) * delta[N];
  } else {
    thError("%s: improper value for (j = %d, N = %d), delta(j+1) cannot be defined", name, j, N);
    return;
  }

   if (j + 2 >= 0 && j + 2 <= N) {
    deltaJp2 = delta[j + 2];
    xJp2 = t[j + 2];
  } else if (j + 2 < 0) {
    deltaJp2 = delta[0];
    xJp2 = t[0] + (j + 2) * delta[0];
  } else if (j + 2 > N) {
    deltaJp2 = delta[N];
    xJp2 = t[N] + (j + 2 - N) * delta[N];
  } else {
    thError("%s: improper value for (j = %d, N = %d), delta(j+2) cannot be defined", name, j, N);
    return;
  }
   
   THPIX y1, y2, y3, z1, z2, z3, w1, w2;
  
   z1 = deltaJm2 + deltaJm1;
   z2 = deltaJm1 + deltaJ;
   z3 = deltaJ + deltaJp1;
   
   w1 = deltaJm2 + deltaJm1 + deltaJ;
   w2 = deltaJm1 + deltaJ + deltaJp1;
   
   THPIX wz1, wz2, wz3; 

   if (k == -2) {
     
     wz1 = deltaJm2 * z1 * w1;
     
     for (i = 0; i < Nx; i++) {
       y1 = pow((x[i] - xJm2), 3);
       b[i] = y1 / wz1;
     }
     return;
   }

   if (k == -1) {
    
     wz1 = deltaJm1 * z1 * w1;
     wz2 = deltaJm1 * z2 * w2;
     wz3 = deltaJm1 * z2 * w1;

     for (i = 0; i < Nx; i++) {
       y1 = pow((x[i] - xJm2), 2) * (xJ - x[i]);    
       y2 = pow((x[i] - xJm1), 2) * (xJp2 - x[i]);
       y3 = (x[i] - xJm2) * (x[i] - xJm1) * (xJp1 - x[i]);
       b[i] = y1 / wz1 + y2 / wz2 + y3 / wz3;
     }
     return;
   }
   
   if (k == 0) {

      wz1 = deltaJ * z3 * w2;
      wz2 = deltaJ * z2 * w1;
      wz3 = deltaJ * z2 * w2;
      
      for (i = 0; i < Nx; i++) {
	y1 = pow((xJp2 - x[i]), 2) * (x[i] - xJ);
	y2 = pow((xJp1 - x[i]), 2) * (x[i] - xJm2);
	y3 = (x[i] - xJm1) * (xJp1 - x[i]) * (xJp2 - x[i]);
	b[i] = y1 / wz1 + y2 / wz2 + y3 / wz3;
      }
     return;
   }
   
   if (k == 1) {
	/* 
     y1 = pow((xJp2 - x[i]), 3);
	*/
      for (i = 0; i < Nx; i++) {
        y1 = pow((xJp2 - x[i]), 3);
	wz1 = deltaJp1 * z3 * w2;
	b[i] = y1 / wz1;
      }
      return;
   }
   
   for (i = 0; i < Nx; i++) {
     b[i] = (THPIX) 0.0;
   }

   return;
   
}


/* the following does the same calculation but in place */

void gen_Bspline_basis_inplace(THPIX *x, int Nx, 
		       THPIX *t, int j, int N) {
  
  /* t are assumed to be ordered, 
     x is not assumed to be ordered
     node -1, -2, and N+1 are not in the node array
  */

  int i, k;
  THPIX *delta;
  delta = (THPIX *) thCalloc(N + 1, sizeof(THPIX));
  for (i = 0; i < N; i++) {
    delta[i] = t[i+1] - t[i];
  }
  delta[N] = delta[N-1];

  for (i = 0; i < Nx; i++) {
    for (k = -2; k < 2; k++) {
      if (calc_spline_Dj(x[i], t, delta, j + k, N)) {
	gen_spline_Bjk_inplace(x + i, t, delta, j, k, N, 1);
	break;
      }
      if (k == 2) {
	x[i] = (THPIX) 0.0;
      }
    }
  }
    
  thFree(delta);
  return;
}

int calc_spline_Dj(THPIX x, THPIX *t, THPIX *delta, 
		   int j, int N) {
  
  if (j > N+1 || j < -1) {
    return(0);
  }

  if (j == N) {
    if (x >= t[N] && x < t[N] + delta[N]) return(1);
    return(0);
  }

  if (j == -1) {
    if (x >= t[0] - delta[0] && x < t[0]) return(1);
    return(0);
  }

  if (j == -2) {
    if (x >= t[0] - 2.0 * delta[0] && x < t[0] - delta[0]) return(1);
    return(0);
  }

  if (x < t[j + 1] && x >= t[j]) return(1);
  return(0);
}


void gen_spline_Bjk_inplace(THPIX *x, THPIX *t, THPIX *delta, 
		    int j, int k, int N, int Nx) {
  
  char *name = "gen_spline_Bj";

  if (k < -2 || k > 1) {
    *x = (THPIX) 0.0;
  }
  
  THPIX deltaJm2, deltaJm1, deltaJ, deltaJp1, deltaJp2;
  THPIX xJm2, xJm1, xJ, xJp1, xJp2;

  if (j - 2 >= 0 && j - 2 <= N) {
    deltaJm2 = delta[j - 2];
    xJm2 = t[j - 2];
  } else if (j - 2 < 0) {
    deltaJm2 = delta[0];
    xJm2 = t[0] + (j - 2) * delta[0];
  } else if (j - 2 >  N) {
    deltaJm2 = delta[N];
    xJm2 = t[N] + (j - 2 - N) * delta[N];
  } else {
    thError("%s: improper value for j (%d) , delta(j-2) cannot be defined", name, j);
    return;
  }

  if (j - 1 >= 0 && j - 1 <= N) {
    deltaJm1 = delta[j - 1];
    xJm1 = t[j - 1];
  } else if (j - 1 < 0) {
    deltaJm1 = delta[0];
    xJm1 = t[0] + (j - 1) * delta[0];
  } else if (j - 1 > N) {
    deltaJm1 = delta[N];
    xJm1 = t[N] + (j - 1 - N) * delta[N];
  } else {
    thError("%s: improper value for (j = %d, N = %d), delta(j-1) cannot be defined", name, j, N);
    return;
  }

  if (j >= 0 && j <= N) {
    deltaJ = delta[j];
    xJ = t[j];
  } else if (j < 0) {
    deltaJ = delta[0];
    xJ = t[0] + j * delta[0];
  } else if (j > N) {
    deltaJ = delta[N];
    xJ = t[j] + (j - N) * delta[N];
  } else {
    thError("%s: improper value for (j = %d, N = %d), delta(j) cannot be defined", name, j, N);
    return;
  }

   if (j + 1 >= 0 && j + 1 <= N) {
    deltaJp1 = delta[j + 1];
    xJp1 = t[j + 1];
  } else if (j + 1 < 0) {
    deltaJp1 = delta[0];
    xJp1 = t[0] + (j + 1) * delta[0];
  } else if (j + 1 > N) {
    deltaJp1 = delta[N];
    xJp1 = t[N] + (j + 1 - N) * delta[N];
  } else {
    thError("%s: improper value for (j = %d, N = %d), delta(j+1) cannot be defined", name, j, N);
    return;
  }

   if (j + 2 >= 0 && j + 2 <= N) {
    deltaJp2 = delta[j + 2];
    xJp2 = t[j + 2];
  } else if (j + 1 < 0) {
    deltaJp2 = delta[0];
    xJp2 = t[0] + (j + 2) * delta[0];
  } else if (j + 2 > N) {
    deltaJp2 = delta[N];
    xJp2 = t[N] + (j + 2 - N) * delta[N];
  } else {
    thError("%s: improper value for (j = %d, N = %d), delta(j+2) cannot be defined", name, j, N);
    return;
  }
   
   int i;
   THPIX y1, y2, y3, z1, z2, z3, w1, w2;
  
   z1 = deltaJm2 + deltaJm1;
   z2 = deltaJm1 + deltaJ;
   z3 = deltaJ + deltaJp1;
   
   w1 = deltaJm2 + deltaJm1 + deltaJ;
   w2 = deltaJm1 + deltaJ + deltaJp1;
   
   THPIX wz1, wz2, wz3; 

   if (k == -2) {
     
     wz1 = deltaJm2 * z1 * w1;
     
     for (i = 0; i < Nx; i++) {
       y1 = pow((x[i] - xJm2), 3);
       x[i] = y1 / wz1;
     }
     return;
   }

   if (k == -1) {
    
     wz1 = deltaJm1 * z1 * w1;
     wz2 = deltaJm1 * z2 * w2;
     wz3 = deltaJm1 * z2 * w1;

     for (i = 0; i < Nx; i++) {
       y1 = pow((x[i] - xJm2), 2) * (xJ - x[i]);    
       y2 = pow((x[i] - xJm1), 2) * (xJp2 - x[i]);
       y3 = (x[i] - xJm2) * (x[i] - xJm1) * (xJp1 - x[i]);
       x[i] = y1 / wz1 + y2 / wz2 + y3 / wz3;
     }
     return;
   }
   
   if (k == 0) {

      wz1 = deltaJ * z3 * w2;
      wz2 = deltaJ * z2 * w1;
      wz3 = deltaJ * z2 * w2;
      
      for (i = 0; i < Nx; i++) {
	y1 = pow((xJp2 - x[i]), 2) * (x[i] - xJ);
	y2 = pow((xJp1 - x[i]), 2) * (x[i] - xJm2);
	y3 = (x[i] - xJm1) * (xJp1 - x[i]) * (xJp2 - x[i]);
	x[i] = y1 / wz1 + y2 / wz2 + y3 / wz3;
      }
     return;
   }
   
   if (k == 1) {
     /* 
	y1 = pow((xJp2 - x[i]), 3);
	*/ 
     for (i = 0; i < Nx; i++) {
	y1 = pow((xJp2 - x[i]), 3);
	wz1 = deltaJp1 * z3 * w2;
	x[i] = y1 / wz1;
      }
      return;
   }
   
   for (i = 0; i < Nx; i++) {
     x[i] = (THPIX) 0.0;
   }

   return;
   
}

/* the following calculates the basis functions for 
   M-spline and I-spline
*/


/* 
   in the following the nodes are assumed to be provided in such a way as to 
   satisfy the relationships
   t0 = t1 = ... = t(k-1) < tk < ... < t(n-1) = tn = ... = t(n + k - 1)
*/
void gen_Mspline_Mik (THPIX *x, THPIX **Mik, THPIX *t, 
		      int Nx, int Ni, int k) {
  
  char *name = "get_Mspline_Mik";
  
  if (k < 1) {
    thError("%s: ERROR - unsupported value of k (%d) in M-spline", name, k);
    return;
  }

  int i, j;
  THPIX *MikI, coeff_delta, y;

  if (k == 1) {
    for (i = 0; i < Ni; i++) {

      MikI = Mik[i];
      if (t[i + 1] != t[i]) {
	coeff_delta = ((THPIX) 1.0) / (t[i + 1] - t[i]);
	for (j = 0; j < Nx; j++) {
	  y = x[j];
	  if (y >= t[i] && y < t[i + 1]) {
	    MikI[j] = coeff_delta;
	  }
	}
      } else {
	for (j = 0; j < Nx; j++) {
	  MikI[j] = (THPIX) 0.0;
	}
      }
    }
    return;
  }

  THPIX **Mikm1;
  Mikm1 = (THPIX **) thCalloc(Ni + 1, sizeof(THPIX *));
  Mikm1[0] = thCalloc(Nx * (Ni + 1), sizeof(THPIX));
  for (i = 1; i < Ni + 1; i++) {
      Mikm1[i] = Mikm1[i-1] + Nx; 
  }

  /* 
  gen_Mspline_Mik(x, Mikm1 + 1, t+1, Nx, Ni - 1, k-1);
  */
  gen_Mspline_Mik(x, Mikm1, t, Nx, Ni, k-1);
#if DEBUG_MATH_2
if(Nx <= 20) {
	printf("%s: t[t.index] = (dt = %d) \n", name, Ni + k);
	for (i = 0; i < Ni + k; i++) {
	printf("[%2d]: %6.1g, ", i, t[i]);
	}
	printf("\n");
	printf("%s: M[degree.of.freedom][t.index] = (df = %d, dt = %d, k = %d) \n", name, Ni, Nx, k-1);
	for (i = 0; i < Ni; i++) {
		THPIX *MikI = Mikm1[i];
		for (j = 0; j < Nx; j++) {
			printf("[%2d][%2d]= %6.1g,", i, j, (float) MikI[j]);
		}
		printf("\n");
	}
}
#endif

  THPIX k_coeff;
  k_coeff = ((THPIX) k) / ((THPIX) (k-1));

  THPIX *Mikm1I, *Mikm1Ip1,  kt_coeff;

  for (i = 0; i < Ni; i++) { /* note that Ni is degree of freedom and is different from the size of t-array, t-array is k-units bigger than degree of freedom at the original call of this function */

    MikI = Mik[i];
    Mikm1I = Mikm1[i];
    Mikm1Ip1 = Mikm1[i + 1];
    THPIX tIpK = t[i+k], tI = t[i];

/*  before I discovered the problem with I-M-spline - upon test this doesn't change the result and is not source of the problem */
#if 0
   for (j = 0; j < Nx; j++) {
      y = x[j];
      if (y >= t[i] && y < t[i + 1]) {
	MikI[j] = (y - t[i]) * Mikm1I[j] * kt_coeff;
      } else if (y >= t[i + 1] && y < t[i + k - 1]) {
	MikI[j] = ((y - t[i]) * Mikm1I[j] + 
		   (t[i + k] - y) * Mikm1Ip1[j]) * kt_coeff;
      } else if (y >= t[i + k - 1] && y < t[i + k]) {
	MikI[j] = (t[i + k] - y) * Mikm1Ip1[j] * kt_coeff;
      } else {
	MikI[j] = (THPIX) 0.0;
      }
    }

#else

    if (i + 1 < Ni) {

	    if (tIpK > tI) {
	       kt_coeff = k_coeff  / (tIpK - tI);
		for (j = 0; j < Nx; j++) {
			y = x[j];
       	 		MikI[j] = ((y - tI) * Mikm1I[j] + (tIpK-y) * Mikm1Ip1[j]) * kt_coeff;
    			}
    	    } else {
	    	for (j = 0; j < Nx; j++) MikI[j] = (THPIX) 0.0;
    	    }
    } else {
	if (tIpK > tI) {
	       kt_coeff = k_coeff  / (tIpK - tI);
		for (j = 0; j < Nx; j++) {
			y = x[j];
       	 		MikI[j] = (y - tI) * Mikm1I[j] * kt_coeff;
    			}
    	    } else {
	    	for (j = 0; j < Nx; j++) MikI[j] = (THPIX) 0.0;
    	    }
   }

#endif

  }	

  thFree(Mikm1[0]);
  thFree(Mikm1);

}

THPIX *get_Mspline_knots(THPIX *x, int Nx, int k, int *dknot) {
  char *name = "get_Mspline_knots";

  if (Nx <2) {
	thError("%s: ERROR - received (Nx = %d) while only (Nx >= 2) is accepted", name, Nx);
	return(NULL);
  }
  /* the size and range of indices should be checked for this function */
  THPIX *t;
  t = (THPIX *) thCalloc((Nx + 2 * k - 2), sizeof(THPIX));

  int i;

  for (i = 0; i < k; i++) {
    t[i] = x[0];
  }
  for (i = k; i < Nx + k - 2; i++) {
    t[i] = x[i - k + 1];
  }
  THPIX U = x[Nx - 1] + SPLINE_UPPER_DITHER;
  for (i = Nx + k - 2; i < Nx + 2 * k - 2; i++) {
    t[i] = U;
  }

  if (dknot != NULL) *dknot = Nx + 2 * k - 2;
  return(t);
}

THPIX **get_Ispline_basis (THPIX *x, 
			   THPIX *in_t, 
			   int Nx, int N_int, int k, int *df) {
 
  char *name = "get_Ispline_basis"; 
 /* it is not clear how nodes should change between 
     M(x | k + 1, t) and I(x | k + 1, t) 
     recursive formula:
        t_{j} <= x < t_{j+1}
       	Ii(x|k, t) is zero if i > j, and equals one if j − k + 1 > i
        otherwise 
	I_i(x | k, t) = SUM(m = i ... j, (t_{m+k+1} - t{m}) M_{m} (x|k+1, t) / (k+1))
  */

  int i, j, m, n;

  THPIX *t;

/* Ni is the degrees of freedom for Iik for the pair (N_int, k). Degrees of freedom for M(k+1, t) is Ni + 1. Note that unlike the stanadard treatment N_int in this context also includes the two ends of the x-range */
  int Ni = N_int + k - 1; /* this used to read N_int + k - 2 now it read N_int + k - 1, the new basis is the constant function */
  if (df != NULL) *df = Ni;

  int dfM = 0;
  THPIX **Mikp1 = get_Mspline_basis(x, in_t, Nx, N_int, k+1, &dfM);
  if (Mikp1 == NULL || dfM <= 0) {
	thError("%s: ERROR - could not calculate M-spline basis calculation for (k = %d)", name, k+1);
	return(NULL);
  }
#if DEBUG_MATH_2
	printf("%s: x values [size = %d] \n", name, Nx);
	for (i = 0; i < Nx; i++) {
		printf("[%2d]= %10.5g, ", i, x[i]);
	}
	printf("\n");
	if (Nx <= 20) {
		printf("%s: M[degree.of.freedom][t.index] = (df = %d, dt = %d, k = %d) \n", name, Ni, Nx, k + 1);
	        for (i = 0; i < Ni; i++) {
			THPIX *Mikp1I = Mikp1[i];
			for (j = 0; j < Nx; j++) {
				printf("[%2d][%2d]= %6.1g, ", i, j, (float) Mikp1I[j]);
			}
			printf("\n");
		}
	}
#endif

int dknot = 0;
  t = get_Mspline_knots(in_t, N_int, k + 1, &dknot); /* changed k to k + 1 for debugging purposes - when changed it results in I values <= 1.0 otherwise I values are sometimes greater than 1 */
  if (t == NULL) {
	thError("%s: ERROR - could not generate knot-array for (N_int = %d, k = %d)", name, N_int, k);
	return(NULL);
   }
 
  /* find the span (t(j), t(j+1)) where each x lies */
  int *t_index, tindex;
  t_index = (int *) thCalloc(Nx, sizeof(int));
  for (j = 0; j < Nx; j++) {
    
    t_index[j] = -1;
    for (n = 0; n < dknot; n++) { /* the range should read n = k - 1; n < N_int + k - 2 = df */
      if (x[j] >= t[n] && x[j] < t[n + 1]) {
	t_index[j] = n;
	break;
      }
    }
  }
 
  THPIX **Iik;
  Iik = (THPIX **) thCalloc(Ni, sizeof(THPIX *)); /* size of should be df */
  for (i = 0; i < Ni; i++) {
    Iik[i] = (THPIX *) thCalloc(Nx, sizeof(THPIX));
  }

  THPIX *IikI, *Mmkp1M;

  for (i = 0; i < Ni; i++) {
    IikI = Iik[i];
    for (j = 0; j < Nx; j++) {
      tindex = t_index[j];
#if 0
      if (tindex != -1 && tindex - k + 1 > i) { /* note that if we condition this on tindex != -1, we will get a zero for the x = last t-value. on the other hand the problem wit not making this condition available is to give a 0 when x is not within the range - I decided to take out this condition and assume that the user is able to support the values of x within the desired range otherwise the spline will simply not work */
#else
	if (tindex - k + 1 > i) {
#endif
	IikI[j] = (THPIX) 1.0;
      }
    } 
  }

  THPIX m_coeff;

#if 1
  for (i = 0; i < Ni; i++) {
    IikI = Iik[i];
    for (m = i; m < Ni; m++) { /* should read (m = i; m < df - 1; m++) */
      Mmkp1M = Mikp1[m];
      m_coeff = (t[m + k + 1] - t[m]) / (k + (THPIX) 1.0); /* this used to generate and invalid read error */
      for (j = 0; j < Nx; j++) {
	tindex = t_index[j];
	if (tindex >= i && m <= tindex && tindex - k + 1 <= i) {
	  IikI[j] += m_coeff * Mmkp1M[j];
	}
      }
    }
  }
#else
/* this part was written to make sure where the source of the memory read error was */ 
for (i = 0; i < Ni; i++) {
	IikI = Iik[i];
	for (j = 0; j < Nx; j++) {
		tindex = t_index[j];
		for (m = i; m <= tindex; m++) {
			Mmkp1M = Mikp1[m];
			m_coeff = (t[m + k + 1] - t[m]) / (k + (THPIX) 1.0); 
    			IikI[j] += m_coeff * Mmkp1M[j];
		}
	}
} 
#endif

#if DEBUG_MATH_2
if (Nx <= 20) {
	printf("%s: x-values (Nx = %d) \n", name, Nx);
	for (i = 0; i < Nx; i++) {
		printf("[%2d]= %11.5g, ",i, (float) x[i]);
	}
	printf("\n");
	printf("%s: I[degree.of.freedom][t.index] = (df = %d, dt = %d, k = %d) \n", name, Ni, Nx, k);
	for (i = 0; i < Ni; i++) {
		IikI = Iik[i];
		for (j = 0; j < Nx; j++) {
			printf("[%2d][%2d]: %6.2g,", i, j, (float) IikI[j]);
		}
		printf("\n");
	}
}
#endif

  for (i = 0; i < dfM; i++) {
    thFree(Mikp1[i]);
  } 
  thFree(Mikp1);
  thFree(t_index);
  thFree(t);

  return(Iik);
}


THPIX **get_Mspline_basis (THPIX *x, 
			   THPIX *in_t, 
			   int Nx, int N_int, int k, int *dfM) {

  THPIX *t;
  t = get_Mspline_knots(in_t, N_int, k, NULL);

  int i, Ni;
  Ni = N_int + k - 2;
  if (dfM != NULL) *dfM = Ni;

  THPIX **Mik;
  Mik = (THPIX **) thCalloc(Ni, sizeof(THPIX*));
  for (i = 0; i < Ni; i++) {
    Mik[i] = (THPIX *) thCalloc(Nx, sizeof(THPIX));
  }

  gen_Mspline_Mik (x, Mik, t, Nx, Ni, k);
  thFree(t);
  
  return(Mik);
}
  
THPIX **get_Bspline_basis (THPIX *x, 
			   THPIX *in_t, 
			   int Nx, int N_int, int k, int *n_basis) {
  
  THPIX *t;
  t = get_Bspline_knots(in_t, N_int, k);

  int i, Ni;
  Ni = N_int + k - 2;

  THPIX **Bik;
  Bik = (THPIX **) thCalloc(Ni, sizeof(THPIX*));
  Bik[0] = (THPIX *) thCalloc(Ni * Nx, sizeof(THPIX));
  for (i = 0; i < Ni; i++) {
    Bik[i] = Bik[0] + i * Nx;
  }

#if 0
/* void gen_Bspline_basis(THPIX *x, THPIX *b, 
		       THPIX *t, int j, 
		       int Nx, int N) {

*/
 
  for (i = 0; i < Ni; i++) {
	THPIX *b = Bik[i];
  	gen_Bspline_basis(x, b, t, i, Nx, N_int) {
	}
#else
/* discovered the following error Oct 2017 */
  gen_Mspline_Mik (x, Bik, t,  
		   Nx, Ni, k);

#endif
  shAssert(n_basis != NULL);
  *n_basis = Ni; 
  
  thFree(t);
  
  return(Bik);
}
  
THPIX *get_Bspline_knots(THPIX *x, int Nx, int k) {
  
  THPIX *t;
  t = (THPIX *) thCalloc(Nx + 2 * k - 3, sizeof(THPIX));

  int i;

  for (i = 0; i < k; i++) {
    t[i] = x[0] + (k - i - 1) * (x[0] - x[1]);
  }
  for (i = k; i < Nx + k - 1; i++) {
    t[i] = x[i - k + 1];
  }
  for (i = Nx + k - 1; i < Nx + 2 * k - 2; i++) {
    t[i] = x[Nx - 1] + (i - Nx - k + 2) * (x[Nx - 1] - x[Nx - 2]);
  }

  return(t);
}

double thGamma(double z) {

if (z < 0.5) {
	double return_value = (double) PI / (sin((double) PI * z) * thGamma((double) (1.0 - z)));
	return(return_value);
	}

static int init = 0;
static double *p = NULL;
const int g = 7;
if (!init) {
	p = thCalloc(g + 2, sizeof(double));
	p[0] = 0.99999999999980993;
	p[1] = 676.5203681218851;
	p[2] = -1259.1392167224028;
     	p[3] = 771.32342877765313;
	p[4] = -176.61502916214059;
	p[5] =  12.507343278686905;
	p[6] = -0.13857109526572012;
	p[7] =  9.9843695780195716e-6;
	p[8] =  1.5056327351493116e-7;
	init = 1;	
}

z -= (double) 1.0;
int i;
double x;
x = p[0];
for (i = 1; i < g + 2; i++) {
	x += p[i] / (z + (double) i);
}
double t;
t = z + (double) g + (double) 0.5;

double ln_gamma_value = (z + (double) 0.5) * log(t) - t;
double fudge_factor = x * sqrt((double) 2.0 * (double) PI);
double gamma_value = exp(ln_gamma_value) * fudge_factor;

return(gamma_value);
}

double thLnGamma(double z) {

if (z < 0.5) {
	double return_value = log((double) PI / (sin((double) PI * z)) +  thLnGamma((double) (1.0 - z)));
	return(return_value);
	}

static int init = 0;
static double *p = NULL;
const int g = 7;
if (!init) {
	p = thCalloc(g + 2, sizeof(double));
	p[0] = 0.99999999999980993;
	p[1] = 676.5203681218851;
	p[2] = -1259.1392167224028;
     	p[3] = 771.32342877765313;
	p[4] = -176.61502916214059;
	p[5] =  12.507343278686905;
	p[6] = -0.13857109526572012;
	p[7] =  9.9843695780195716e-6;
	p[8] =  1.5056327351493116e-7;
	init = 1;	
}

z -= (double) 1.0;
int i;
double x;
x = p[0];
for (i = 1; i < g + 2; i++) {
	x += p[i] / (z + (double) i);
}
double t;
t = z + (double) g + (double) 0.5;

double ln_gamma_value = (z + (double) 0.5) * log(t) - t + log(x * sqrt((double) 2.0 * (double) PI));
return(ln_gamma_value);
}


THPIX circle_square_overlap(THPIX xc, THPIX yc, THPIX r, THPIX xs, THPIX ys, THPIX a, int n) {
char *name = "circle_square_overlap";
/* this recursive function caclulate the approximate area of overlap between a square and a circle */
shAssert(r >= (THPIX) 0.0);
shAssert(n >= 0);

if (r == (THPIX) 0.0) return((THPIX) 0.0);

if ((thabs(xs - xc) + thabs(ys - yc) <= r) && (thabs(xs - xc) + thabs(ys - yc + a) <= r) && (thabs(xs - xc + a) + thabs(ys - yc) <= r) && (thabs(xs - xc + a) + thabs(ys - yc + a) <= r)) {
	return(a * a);
} else if ((thabs(xs - xc) + thabs(ys - yc) >= r * SQRT2) && (thabs(xs - xc) + thabs(ys - yc + a) >= r * SQRT2) && (thabs(xs - xc + a) + thabs(ys - yc) >= r * SQRT2) && (thabs(xs - xc + a) + thabs(ys - yc + a) >= r * SQRT2) && thabs(a) < r) 	{
	return((THPIX) 0.0);
}
	
if (n == 0) {
	shAssert(thabs(a) < r * SQRT2);
	THPIX xcc = xs + a / (THPIX) 2.0;
	THPIX ycc = ys + a / (THPIX) 2.0;
	if (pow(xcc - xc, (THPIX) 2.0) + pow(ycc - yc, (THPIX) 2.0) - pow(r, (THPIX) 2.0) >= (THPIX) 0.0) {
		return((THPIX) 0.0 );
	} else {
		return(a * a);
	}
} else {
	int i, j;
	THPIX area = 0.0;
	THPIX adiv = a / (THPIX) SQUARE_AREA_DIV;

	for (i = 0; i < SQUARE_AREA_DIV; i++) {
		for (j = 0; j < SQUARE_AREA_DIV; j++) {
			area += circle_square_overlap(xc, yc, r, xs + adiv * (THPIX) i, ys + adiv * (THPIX) j, adiv, n - 1);
		}
	}
	return(area);
}

thError("%s: ERROR - source code error", name);
shAssert(1 == 0);
return(THNAN);
}

THPIX ring_square_overlap(THPIX xc, THPIX yc, THPIX r1, THPIX r2, THPIX xs, THPIX ys, THPIX a, int n) {

THPIX rmin, rmax;
if (r1 > r2) {
	rmin = r2;
	rmax = r1;
} else if (r1 < r2) {
	rmin = r1;
	rmax = r2;
} else {
	return((THPIX) 0.0);
}

return(circle_square_overlap(xc, yc, rmax, xs, ys, a, n) - circle_square_overlap(xc, yc, rmin, xs, ys, a, n));
}

/*****************************************************************************/
/*
 * <AUTO EXTRACT>
 *
 * Invert an n*n double matrix arr in place using Gaussian Elimination
 * with pivoting; return -1 for singular matrices
 *
 * Also solve the equation arr*b = x, replacing b by x (if b is non-NULL)
 *
 * The matrix arr is destroyed during the inversion
 */
int
thDMatrixInvert(MATHDBLE **arr,		/* matrix to invert */
		MATHDBLE *b,		/* Also solve arr*x = b; may be NULL */
		int n)			/* dimension of iarr and arr */
{
   MATHDBLE abs_arr_jk;			/* |arr[j][k]| */
   int row, col;			/* pivotal row, column */
   int i, j, k;
   int *icol, *irow;			/* which {column,row}s we pivoted on */
   int *pivoted;			/* have we pivoted on this column? */
   MATHDBLE pivot;				/* value of pivot */
   MATHDBLE ipivot;			/* 1/pivot */
   MATHDBLE tmp;

   icol = alloca(3*n*sizeof(int));
   irow = icol + n;
   pivoted = irow + n;

   for(i = 0;i < n;i++) {
      pivoted[i] = 0;
   }

   for(i = 0;i < n;i++) {
/*
 * look for a pivot
 */
      row = col = -1;
      pivot = 0;
      for(j = 0;j < n;j++) {
	 if(pivoted[j] != 1) {
	    for(k = 0;k < n;k++) {
	       if(!pivoted[k]) {	/* we haven't used this column */
		  abs_arr_jk = (arr[j][k] > 0) ? arr[j][k] : -arr[j][k];
		  if(abs_arr_jk >= pivot) {
		     pivot = abs_arr_jk;
		     row = j; col = k;
		  }
	       } else if(pivoted[k] > 1) { /* we've used this column already */
		  return(-1);		/* so it's singular */
	       }
	    }
	 }
      }
      shAssert(row >= 0);
/*
 * got the pivot; now move it to the diagonal, i.e. move the pivotal column
 * to the row'th column
 *
 * We have to apply the same permutation to the solution vector, b
 */
      pivoted[col]++;			/* we'll pivot on column col */
      icol[i] = col; irow[i] = row;
      if((pivot = arr[row][col]) == 0.0) {
	 return(-1);			/* singular */
      }

      if(row != col) {
	 for(j = 0;j < n;j++) {
	    tmp = arr[row][j];
	    arr[row][j] = arr[col][j];
	    arr[col][j] = tmp;
	 }
	 if(b != NULL) {
	    tmp = b[row];
	    b[row] = b[col];
	    b[col] = tmp;
	 }
      }
/*
 * divide pivotal row by pivot
 */
      ipivot = 1/pivot;
      arr[col][col] = 1.0;
      for(j = 0;j < n;j++) {
	 arr[col][j] *= ipivot;
      }
      if(b != NULL) {
	 b[col] *= ipivot;
      }
/*
 * and reduce other rows
 */
      for(j = 0;j < n;j++) {
	 if(j == col) {			/* not the pivotal one */
	    continue;
	 }

	 tmp = arr[j][col];
	 arr[j][col] = 0.0;
	 for(k = 0;k < n;k++) {
	    arr[j][k] -= tmp*arr[col][k];
	 }
	 if(b != NULL) {
	    b[j] -= tmp*b[col];
	 }
      }
   }
/*
 * OK, we're done except for undoing a permutation of the columns
 */
   for(i = n - 1;i >= 0;i--) {
      if(icol[i] != irow[i]) {
	 for(j = 0;j < n;j++) {
	    tmp = arr[j][icol[i]];
	    arr[j][icol[i]] = arr[j][irow[i]];
	    arr[j][irow[i]] = tmp;
	 }
      }
   }

   return(0);
}

float  parameter_epsilon_random_draw(void) {
char *name = "parameter_epsilon_random_draw";
if (!phRandomIsInitialised()) {
	thError("%s: ERROR - random package is not initialized", name);
}
float epsilon = VALUE_IS_BAD;
#if PARAMETER_DRAW_GAUSSIAN
epsilon = (float) phGaussdev();
#else
epsilon = ((float) phRandomUniformdev() - (float) 0.5) * sqrt((float) 12.0) ;
#endif
return(epsilon);
}

/* based on
 * source:
 *
 * MATHEMATICS OF COMPUTATION
 * VOLUME 58, NUMBER 198
 * APRIL 1992, PAGES 755-764
 *
 * ASYMPTOTIC INVERSION OF INCOMPLETE GAMMA FUNCTIONS
 * N. M. TEMME.
 *
 */

MATHDBLE epsilon1 (MATHDBLE x) {
int n = 17;
MATHDBLE a[17] = {
-(MATHDBLE)1.0/(MATHDBLE)3.0, 
+(MATHDBLE)1.0 /(MATHDBLE) 36.0, 
-(MATHDBLE)1.0 /(MATHDBLE) 1620.0, 
-(MATHDBLE)7.0 /(MATHDBLE) 6480.0, 
+(MATHDBLE)5.0 /(MATHDBLE) 18144.0, 
-(MATHDBLE)11.0 /(MATHDBLE) 382725.0, 
-(MATHDBLE)101.0 /(MATHDBLE) 16329600.0, 
+(MATHDBLE)37.0 /(MATHDBLE) 9797760.0, 
-(MATHDBLE) 454973.0 /(MATHDBLE) 498845952000.0, 
+(MATHDBLE)1231.0 /(MATHDBLE) 15913705500.0, 
+(MATHDBLE)2745493.0 /(MATHDBLE) 84737299046400.0,
-(MATHDBLE) 2152217.0 /(MATHDBLE) 127673385840000.0, 
+(MATHDBLE)119937661.0 /(MATHDBLE) 30505427656704000.0, 
-(MATHDBLE)449.0 /(MATHDBLE) 1595917323000.0, 
-(MATHDBLE) 756882301459.0 /(MATHDBLE) 4455179048226816000000.0, 
+(MATHDBLE)12699400547.0 /(MATHDBLE) 153146779782796800000.0, 
-(MATHDBLE)3224618478943.0 /(MATHDBLE) 170264214140233973760000.00};

MATHDBLE epsilon = (MATHDBLE) 0.0;
int i;
for (i = n - 1; i >= 0; i--) {
	MATHDBLE coeff = a[i];
	epsilon = epsilon * x +(MATHDBLE) coeff;
}
return(epsilon);
}

MATHDBLE epsilon2(MATHDBLE x) {
int n = 11;
MATHDBLE a[11] = {
-(MATHDBLE)7.0 / (MATHDBLE) 405.0,
-(MATHDBLE)7.0 / (MATHDBLE) 2592.0,
+(MATHDBLE)533.0 / (MATHDBLE) 204120.0,
-(MATHDBLE)1579.0 / (MATHDBLE) 2099520.0,
+(MATHDBLE)109.0 / (MATHDBLE) 1749600.0,
+(MATHDBLE)10217.0 / (MATHDBLE) 251942400.0,
-(MATHDBLE)9281803.0 / (MATHDBLE) 436490208000.0,
+(MATHDBLE)919081.0 / (MATHDBLE) 185177664000.0,
-(MATHDBLE)100824673.0 / (MATHDBLE) 571976768563200.0, 
-(MATHDBLE)311266223.0 / (MATHDBLE) 899963447040000.0,
+(MATHDBLE)52310527831.0 / (MATHDBLE) 343186061137920000.0
};

MATHDBLE epsilon = (MATHDBLE) 0.0;
int i;
for (i = n - 1; i >= 0; i--) {
	MATHDBLE coeff = a[i];
	epsilon = epsilon * x +(MATHDBLE) coeff;
}
return(epsilon);
}

MATHDBLE epsilon3(MATHDBLE x) {
int n = 9;
MATHDBLE a[9] = {
+(MATHDBLE)449.0/ (MATHDBLE) 102060.0,
-(MATHDBLE)63149.0/ (MATHDBLE) 20995200.0,
+(MATHDBLE)29233.0 / (MATHDBLE) 36741600.0,
+(MATHDBLE)346793.0 / (MATHDBLE) 5290790400.0,
-(MATHDBLE)18442139.0 / (MATHDBLE) 130947062400.0,
+(MATHDBLE)14408797.0 / (MATHDBLE) 246903552000.0,
-(MATHDBLE)1359578327.0 / (MATHDBLE) 129994720128000.0,
-(MATHDBLE)69980826653.0 / (MATHDBLE) 39598391669760000.0,
+(MATHDBLE)987512909021.0 / (MATHDBLE) 514779091706880000.0
};

MATHDBLE epsilon = (MATHDBLE) 0.0;
int i;
for (i = n - 1; i >= 0; i--) {
	MATHDBLE coeff = a[i];
	epsilon = epsilon * x +(MATHDBLE) coeff;
}
return(epsilon);
}

MATHDBLE epsilon4(MATHDBLE x) {
int n = 7;
MATHDBLE a[7] = {
+(MATHDBLE)319.0 / (MATHDBLE) 183708.0,
-(MATHDBLE)269383.0 / (MATHDBLE) 4232632320.0,
-(MATHDBLE)449882243.0 / (MATHDBLE) 982102968000.0,
+(MATHDBLE)1981235233.0 / (MATHDBLE) 6666395904000.0, 
-(MATHDBLE)16968489929.0 / (MATHDBLE) 194992080192000.0,
-(MATHDBLE)16004851139.0 / (MATHDBLE) 26398927779840000.0, 
+(MATHDBLE)636178018081.0 /  (MATHDBLE) 48260539847520000.0
};

MATHDBLE epsilon = (MATHDBLE) 0.0;
int i;
for (i = n - 1; i >= 0; i--) {
	MATHDBLE coeff = a[i];
	epsilon = epsilon * x +(MATHDBLE) coeff;
}
return(epsilon);
}

MATHDBLE epsilon(MATHDBLE eta, MATHDBLE a) {
MATHDBLE e1, e2, e3, e4;
e1 = epsilon1(eta);
e2 = epsilon2(eta);
e3 = epsilon3(eta);
e4 = epsilon4(eta);
MATHDBLE epsilon_eta_a = (e1 + (e2 + (e3 + e4 / a) / a) / a) / a;
return(epsilon_eta_a);
}

MATHDBLE qgamma_inv_asym(MATHDBLE q, MATHDBLE a) {
char *name = "qgamma_inv_asym";

#if 0
thError("%s: WARNING - this function is running in debug mode", name);
return((MATHDBLE) (20.0));
#else
if (isnan(q) || isinf(q)) {
	thError("%s: ERROR - NAN detected at q-value passed", name);
	shAssert(!(isnan(q) || isinf(q)));
}
if (isnan(a) || isnan(a)) {
	thError("%s: ERROR - NAN detected at a-value passed", name);
	shAssert(!(isnan(a) || isinf(a)));
}
if (q < (MATHDBLE) 0.0 || q >= (MATHDBLE) 1.0) {
	thError("%s: ERROR - (q = %G) too large or too small", name, (double) q);
	shAssert(q >= (MATHDBLE) 0.0 && q < (MATHDBLE) 1.0);
}
if (a <= (MATHDBLE) 0.0) {
	thError("%s: ERROR - unsupported (a = %G < 0.0)", name, (double) a);
	shAssert(a > (MATHDBLE) 0.0);
}
MATHDBLE eta0 = eta_zero(q, a);
if (isnan(eta0) || isinf(eta0)) {
	thError("%s: ERROR - NAN value detected for (eta0)", name);
	shAssert(!(isnan(eta0) || isinf(eta0)));
	}
MATHDBLE epsilon_eta0_a = epsilon(eta0, a);
if (isnan(epsilon_eta0_a) || isinf(epsilon_eta0_a)) {
	thError("%s: ERROR - NAN value detected for (epislon)", name);
	shAssert(!(isnan(epsilon_eta0_a) || isinf(epsilon_eta0_a)));
}
MATHDBLE eta = eta0 + epsilon_eta0_a;
int n = 6;
MATHDBLE b[6] = {
(MATHDBLE) 1.0, 
(MATHDBLE) 1.0, 
(MATHDBLE) 1.0 / (MATHDBLE) 3.0, 
(MATHDBLE) 1.0 / (MATHDBLE) 36.0, 
(MATHDBLE) -1.0 / (MATHDBLE) 270.0, 
(MATHDBLE) 1.0 / (MATHDBLE) 4320.0};

MATHDBLE lambda = (MATHDBLE) 0.0;
#if 0 
for some not well understood reason this returns (NAN) in certain calls while q and a are the same - could be a memory bug 
lambda = b[0] + (b[1] + (b[2] + (b[3] + (b[4] + b[5] * eta) * eta) * eta) * eta) * eta;
#endif
int i;
for (i = n - 1; i >= 0; i--) {
	MATHDBLE coeff = b[i];
	lambda = lambda * eta + coeff;
}
#if DEBUG_GAMMA
printf("%s: (q, a) = (%G, %G), (eta, eta0, epsilon) = (%G, %G, %G), lambda = %G \n", 
	name, (double) q, (double) a, (double) eta, (double) eta0, (double) epsilon_eta0_a, (double) lambda);
#endif
if (isnan(lambda) || isinf(lambda)) {
	thError("%s: ERROR - NAN value detected for (lambda)", name);
	shAssert(!(isnan(lambda) || isinf(lambda)));
}
if ((eta > 0.0 && lambda < 1.0) || (eta < 0.0 && lambda > 1.0)) {
	thError("%s: WARNING - sign(eta) =!= sign(lambda - 1), (q, a) = (%G, %G), (eta0, eta, lambda) = (%G, %G, %G)", 
	name, (double) q, (double) a, (double) eta0, (double) eta, (double) lambda);
	}
MATHDBLE x = 0.0;
if (lambda < (MATHDBLE) 0.0) {
	if (a < (MATHDBLE) QGAMMA_INV_A_SMALL && q < (MATHDBLE) QGAMMA_INV_Q_SMALL) {
		#if 0
		x = (MATHDBLE) QGAMMA_INV_X_DEFAULT;		
		thError("%s: WARNING - divergent asymptotic series for (q, a; x) = (%G, %G, %G)", name, (double) q, (double) a, (double) x);
		#else
		MATHDBLE x0 = ((MATHDBLE) -0.874) * log(q) + (MATHDBLE) 0.5837;
		MATHDBLE alpha = ((MATHDBLE) 0.8956) * pow(q, (MATHDBLE) 0.1531);	
		x = x0 * pow(a, alpha);
		thError("%s: WARNING - divergent asymptotic series for (q, a; x) = (%G, %G; %G)", name, (double) q, (double) a, (double) x);
		#endif
	} else {
		thError("%s: ERROR - parameters out of range of asymptotic expansion - (q, a) = (%G, %G), (eta0, eta, lambda) = (%G, %G, %G)",
		name, (double) q, (double) a, (double) eta0, (double) eta, (double) lambda); 
		shAssert(lambda >= (MATHDBLE) 0.0);
	}
} else {
	x = lambda * a;
}
return(x);
#endif
}

MATHDBLE eta_zero(MATHDBLE q, MATHDBLE a) {
MATHDBLE q_times_two = (MATHDBLE) 2.0 * q;
/* upon test it became clear that ertfc_inv_asym is only reliable when x < 0.1 */
MATHDBLE x = erfc_inv(q_times_two);
MATHDBLE eta0 = x * pow((MATHDBLE) 2.0 / a, (MATHDBLE) 0.5);
return(eta0);
}

MATHDBLE erf_inv_asym(MATHDBLE x) {
MATHDBLE y, z;
y = (MATHDBLE) 1.0 - x;
z = erfc_inv_asym(y);
return(z);
}

MATHDBLE erfc_inv_asym(MATHDBLE x) {
/* source:
 * DIGITAL LIBRARY OF MATHEMATICAL FUNCTIONS
 * NATIONAL INSTITUTE OF STANDARDS
 * 7.17 (iii)
 * https://dlmf.nist.gov/7.17
 */

char *name = "erfc_inv_asym";
if (fabs(x) > 0.1) {
	thError("%s: WARNING - asymptotic expansion not very accurate for (x = %g > 0.1)", name, (float) x);
}
if (x <= 0.0) {
	thError("%s: ERROR - negative values or zero (x = %g) not supported", name, (float) x);
	shAssert(x > 0.0);
}

MATHDBLE u, v, a2, a3, a4, y;
u = (MATHDBLE) -2.0 / log((MATHDBLE) PI * x * x * log((MATHDBLE) 1.0 / x)); 
v = log(log((MATHDBLE) 1.0 / x)) - (MATHDBLE) 2.0 + log((MATHDBLE) PI);
a2 = (MATHDBLE) 1.0 / (MATHDBLE) 8.0 * v;
a3 = - (MATHDBLE) 1.0 / (MATHDBLE) 32.0 * (v * v + (MATHDBLE) 6.0 * v - (MATHDBLE) 6.0);
a4 = (MATHDBLE) 1.0 / (MATHDBLE) 384.0 * ((MATHDBLE) 4.0 * pow(v, 3.0) + (MATHDBLE) 27.0 * v * v + (MATHDBLE) 108.0 * v - 300.0);
y = pow(u, (MATHDBLE) -0.5) + (a2 + (a3 + a4 * u) * u) * pow(u, (MATHDBLE) 1.5);
return(y);
}

MATHDBLE erf_inv(MATHDBLE x) {
/* source:
 * A HANDY APPROXIMATION FOR THE ERROR FUNCTION AND ITS INVERSE
 * SERGEI WINITZKI
 * FEB 6, 2008
 *
 * Main Reference:
 * S. Winitzki, Uniform approximations for transcendental functions, in Proc. ICCSA- 2003, LNCS 2667/2003, p. 962.
 */
char *name = "erf_inv";
if (fabs(x) >= (MATHDBLE) 1.0) {
	thError("%s: ERROR - unsupported value for (x = %g) expected in range [-1, 1]", name, (float) x);
	shAssert(fabs(x) < 1.0);
}
MATHDBLE a, b, c, y;
a = (MATHDBLE) 8.0 * ((MATHDBLE) PI - (MATHDBLE) 3.0) / ((MATHDBLE) 3.0 * (MATHDBLE) PI * ((MATHDBLE) 4.0 - (MATHDBLE) PI));
b = (MATHDBLE) 0.5 * log((MATHDBLE) 1.0 - x * x);
c = (MATHDBLE) 2.0 / ((MATHDBLE) PI * a);
y = pow((- b - c + pow((pow(c + b, 2.0) - (MATHDBLE) 2.0 * b / a), 0.5)), 0.5);
if (x < (MATHDBLE) 0.0) y = -y;
return(y);
}

MATHDBLE erfc_inv(MATHDBLE x) {
MATHDBLE y = (MATHDBLE) 1.0 - x;
MATHDBLE z = erf_inv(y);
return(z);
}

RET_CODE test_gamma_package() {
char *name = "test_gamma_package";
char *file = "./test-gamma-package.txt";
MATHDBLE *x, *y1, *y2, **y3, *a, x_min, x_max, a_max, a_min, delta, delta_a;
int i, j, n = 10000, m = 40;
x_min = (MATHDBLE) 1.0 / (MATHDBLE) n;
x_max = (MATHDBLE) 1.0 - x_min;
a_max = 40.0;
a_min = 1.0;
delta = (x_max - x_min) / ((MATHDBLE) n - (MATHDBLE) 1.0);
delta_a = (a_max - a_min) / ((MATHDBLE) m - 1.0);
x = shCalloc(n, sizeof(MATHDBLE));
y1 = shCalloc(n, sizeof(MATHDBLE));
y2 = shCalloc(n, sizeof(MATHDBLE));
y3 = shCalloc(m, sizeof(MATHDBLE *));
y3[0] = shCalloc(n * m, sizeof(MATHDBLE));

for (i = 0; i < m; i++) y3[i] = y3[0] + i * n;
a = shCalloc(m, sizeof(MATHDBLE));

for (i = 0; i < n; i++) x[i] = x_min + (MATHDBLE) i * delta;
for (i = 0; i < m; i++) a[i] = a_min + (MATHDBLE) i * delta_a;

for (i = 0; i < n; i++) {
	y1[i] = erfc_inv_asym((MATHDBLE) x[i]);
	y2[i] = erf_inv((MATHDBLE) x[i]);
}
for (i = 0; i < m; i++) {
	MATHDBLE a_item = a[i];
	MATHDBLE *y3_i = y3[i]; 
	for (j = 0; j < n; j++) y3_i[j] = qgamma_inv_asym(x[j], a_item);
}

printf("%s: outputting test results to '%s' \n", name, file);
FILE *fptr = fopen(file, "w");
fprintf(fptr, "x, erfc_inv_asym(x), erf_inv(x) \n");
for (i = 0; i < n; i++) {
	fprintf(fptr, "%G, %G, %G \n", (double) x[i], (double) y1[i], (double) y2[i]);
}
fprintf(fptr, "\n ---------------------- \n \n \n");
fprintf(fptr, "a, q, qgamma_inv(q;a) \n");
for (i = 0; i < m; i++) {
	MATHDBLE a_item = a[i];
	MATHDBLE *y3_i = y3[i]; 
	for (j = 0; j < n; j++) fprintf(fptr, "%G, %G, %G \n", (double) a_item, (double) x[j], (double) y3_i[j]);
}
fprintf(fptr, "\n ---------------------- \n \n \n");
fclose(fptr);

shFree(x); 
shFree(a);
shFree(y1);
shFree(y2);
shFree(y3[0]);
shFree(y3);
return(SH_SUCCESS);
}

RET_CODE LMatrixSolveSVD(MLEFL **A, MLEFL *b, MLEFL *x, int nx, MAT *wA, MAT *wQ, VEC **wVecs) {
char *name = "LMatrixSolveSVD";
if (A == NULL || b == NULL || x == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (nx <= 0) {
	thError("%s: ERROR - unacceptable (nx = %d)", name, nx);
	return(SH_GENERIC_ERROR);
}
if (wVecs == NULL) {	
	thError("%s: ERROR - null array of wVecs(VEC)", name);
	return(SH_GENERIC_ERROR);
}
VEC *wb = wVecs[0];
VEC *wx = wVecs[1];
VEC *wlambda = wVecs[2];
VEC *wwVec = wVecs[3];
if (wA == NULL || wQ == NULL || wb == NULL || wx == NULL) {
	thError("%s: ERROR - null workspace", name);
	return(SH_GENERIC_ERROR);
}
if (nx != wA->m || nx != wQ->m || 
	nx != wb->dim || nx != wx->dim || nx != wwVec->dim || nx != wlambda->dim) {
	thError("%s: ERROR - the workspace dimension does not agree with (nx = %d)", name, nx);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
status = LCopyOntoMat(A, wA);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not convert C-array (A) onto MAT", name);
	return(status);
}
status = LCopyOntoVec(b, wb);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not convert C-array (b) onto VEC", name);
	return(status);
}

int i;
status = thMatSetIdentity(wQ);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not set matrix (Q = I)", name);
	return(status);
}
wlambda = phEigen(wA, wQ, wlambda);
for(i = 0; i < nx; i++) {
	if(fabs(wlambda->ve[i]) < 1e-6) wlambda->ve[i] = 0.0;
}

status = thEigenBackSub(wQ, wlambda, wb, wx, wwVec);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not psudo-inverse using (Q, lambda) pair", name);
	return(status);
}

status = LCopyFromVec(wx, x);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not convert (wx) onto C-array for (x)", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE LMatrixInvertSVD(MLEFL **A, MAT *wA, MAT *wQ, VEC *wlambda, int nx) {
char *name = "LMatrixSolveSVD";
if (A == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (nx <= 0) {
	thError("%s: ERROR - unacceptable (nx = %d)", name, nx);
	return(SH_GENERIC_ERROR);
}
if (wQ == NULL || wA == NULL || wlambda == NULL) {
	thError("%s: ERROR - null workspace", name);
	return(SH_GENERIC_ERROR);
}
if (nx != wA->m || nx != wQ->m || nx != wlambda->dim) {
	thError("%s: ERROR - the workspace dimension does not agree with (nx = %d)", name, nx);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
status = LCopyOntoMat(A, wA);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not convert C-array (A) onto MAT", name);
	return(status);
}

int i;
status = thMatSetIdentity(wQ);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not set matrix (Q = I)", name);
	return(status);
}
wlambda = phEigen(wA, wQ, wlambda);
for(i = 0; i < nx; i++) {
	if(fabs(wlambda->ve[i]) < 1e-6) wlambda->ve[i] = 0.0;
}

status = thEigenBack(wQ, wlambda, wA);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not psudo-inverse using (Q, lambda) pair", name);
	return(status);
}

return(SH_SUCCESS);
}



RET_CODE thEigenBackSub(const MAT *Q, const VEC *lambda, const VEC *b, VEC *x, VEC *w)
{
   int i, j;
   int m;                               /* == U->m */
   Real **Qme, *Qme_i;                  /* == unalias Q->me, Q->me[i] */
   Real *bve;                           /* == unalias b->ve*/
   Real val;

   shAssert(Q != NULL && Q->me != NULL && Q->m == Q->n);
   shAssert(lambda != NULL && lambda->ve != NULL && lambda->dim == Q->m);
   shAssert(b != NULL && b->ve != NULL && b->dim == Q->m);
   shAssert(w != NULL && w->ve != NULL && w->dim == Q->m);
   shAssert(x != NULL && x->ve != NULL && x->dim == Q->m);
 
   m = Q->m;
   Qme = Q->me;

   for(i = 0;i < m;i++) {
      if(lambda->ve[i] == 0.0) {
         w->ve[i] = 0;
      } else {
         val = 0;
         bve = b->ve;
         for(j = 0;j < m;j++) {
            val += Qme[j][i]*bve[j];
         }
         w->ve[i] = val/lambda->ve[i];
      }
   }

   for(i = 0;i < m;i++) {
      val = 0;
      Qme_i = Qme[i];
      for(j = 0;j < m;j++) {
         val += Qme_i[j]*w->ve[j];
      }
      x->ve[i] = val;
   }

return(SH_SUCCESS);
}

RET_CODE thEigenBack(const MAT *Q, const VEC *wlambda, const MAT *A) {
   int i, j, k;
   int m;                               /* == U->m */
   Real **Qme, **Ame;                  /* == unalias Q->me, Q->me[i] */

   shAssert(Q != NULL && Q->me != NULL && Q->m == Q->n);
   shAssert(wlambda != NULL && wlambda->ve != NULL && wlambda->dim == Q->m);
   shAssert(A != NULL && A->me != NULL && A->m == A->n);
   shAssert(A->m == Q->m);
 
   m = Q->m;
   Qme = Q->me;

   Ame = A->me;

   for(i = 0; i < m; i++) {
	for(j = 0; j < m; j++) {
		MLEFL val = 0;
		for(k = 0; k < m; k++) {
			MLEFL lambda = (MLEFL) wlambda->ve[k];
			if (fabs(lambda) > 1.0E-6) val += ((MLEFL) Qme[i][k]) * ((MLEFL) 1.0 / ((MLEFL) wlambda->ve[k])) * ((MLEFL) Qme[j][k]);
		}
	  	Ame[i][j] = (Real) val;
	}
   }
 
return(SH_SUCCESS);
}



RET_CODE LCopyMatOnto(MLEFL **A, MAT *wA) {
char *name = "LCopyMatOnto";
if (A == NULL || wA == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
int m = wA->m;
int n = wA->n;
if (m <= 0 || n <= 0) {
	thError("%s: ERROR - unacceptable MAT size (m = %d,n = %d)", name, m, n);
	return(SH_GENERIC_ERROR);
}

int i, j;
MLEFL *A_i;
Real **wA_c, *wA_i;
wA_c = wA->me;
for (i = 0; i < m; i++) {
	wA_i = wA_c[i];
	A_i = A[i];
	if (A_i == NULL) {
		thError("%s: ERROR - C-array A not allocated properly", name);
		return(SH_GENERIC_ERROR);
	}
	for (j = 0; j < n; j++) {
		A_i[j] = (MLEFL) wA_i[j];
	}
}

return(SH_SUCCESS);
}




RET_CODE LCopyOntoMat(MLEFL **A, MAT *wA) {
char *name = "LCopyOntoMat";
if (A == NULL || wA == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
int m = wA->m;
int n = wA->n;
if (m <= 0 || n <= 0) {
	thError("%s: ERROR - unacceptable MAT size (m = %d,n = %d)", name, m, n);
	return(SH_GENERIC_ERROR);
}

int i, j;
MLEFL *A_i;
Real **wA_c, *wA_i;
wA_c = wA->me;
for (i = 0; i < m; i++) {
	wA_i = wA_c[i];
	A_i = A[i];
	if (A_i == NULL) {
		thError("%s: ERROR - C-array A not allocated properly", name);
		return(SH_GENERIC_ERROR);
	}
	for (j = 0; j < n; j++) {
		wA_i[j] = (Real) A_i[j];
	}
}

return(SH_SUCCESS);
}

RET_CODE LCopyOntoVec(MLEFL *x, VEC *wx) {
char *name = "LCopyOntoVec";
if (x == NULL || wx == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
int m = wx->dim;
if (m <= 0) {
	thError("%s: ERROR - unsupported (dimension = %d)", name, m);
	return(SH_GENERIC_ERROR);
}
int i;
for (i = 0; i < m; i++) {
	wx->ve[i] = (Real) (x[i]);
}

return(SH_SUCCESS);
}

RET_CODE LCopyFromVec(VEC *wx, MLEFL *x) {
char *name = "LCopyFromVec";
if (x == NULL || wx == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
int m = wx->dim;
if (m <= 0) {
	thError("%s: ERROR - unsupported (dimension = %d)", name, m);
	return(SH_GENERIC_ERROR);
}
int i;
for (i = 0; i < m; i++) {
	x[i] = (MLEFL) (wx->ve[i]);
}

return(SH_SUCCESS);
}

RET_CODE thMatSetIdentity(MAT *A) {
char *name = "thMatSetIdentity";
if (A == NULL) {
	thError("%s: ERROR - null matrix", name);
	return(SH_GENERIC_ERROR);
}
int m = A->m;
int n = A->n;
int p = MIN(m, n);
int i;
phMatClear(A);
for (i = 0; i < p; i++) {
	A->me[i][i] = (Real) 1.0;
}
return(SH_SUCCESS);
}

RET_CODE LMatrixAbsoluteValueSVD(MLEFL **A, MLEFL **AA, int nx, MAT *wA, MAT *wQ, VEC **wVecs) {
char *name = "LMatrixAbsoluteValueSVD";
if (A == NULL || AA == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (nx <= 0) {
	thError("%s: ERROR - unacceptable (nx = %d)", name, nx);
	return(SH_GENERIC_ERROR);
}
if (wVecs == NULL) {	
	thError("%s: ERROR - null array of wVecs(VEC)", name);
	return(SH_GENERIC_ERROR);
}
VEC *wb = wVecs[0];
VEC *wx = wVecs[1];
VEC *wlambda = wVecs[2];
VEC *wwVec = wVecs[3];
if (wA == NULL || wQ == NULL || wb == NULL || wx == NULL) {
	thError("%s: ERROR - null workspace", name);
	return(SH_GENERIC_ERROR);
}
if (nx != wA->m || nx != wQ->m || 
	nx != wb->dim || nx != wx->dim || nx != wwVec->dim || nx != wlambda->dim) {
	thError("%s: ERROR - the workspace dimension does not agree with (nx = %d)", name, nx);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
status = LCopyOntoMat(A, wA);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not convert C-array (A) onto MAT", name);
	return(status);
}

int i, j, k;
status = thMatSetIdentity(wQ);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not set matrix (Q = I)", name);
	return(status);
}
int count_negative = 0;
wlambda = phEigen(wA, wQ, wlambda);
for(i = 0; i < nx; i++) {
	if (wlambda->ve[i] < 0.0) count_negative++;
	wlambda->ve[i] = fabs(wlambda->ve[i]);
}

if (count_negative == 0) {
	printf("%s: no negative eigenvalues found (count_negative = %d of %d) \n", name, count_negative, nx);
	for (i = 0; i < nx; i++) memcpy(AA[i], A[i], nx * sizeof(MLEFL));
} else if (count_negative > 0) {
	printf("%s: some negative eigenvalues found (count_negative = %d of %d) \n", name, count_negative, nx);
	for(i = 0; i < nx; i++) {
		for(j = 0; j < nx; j++) {
			MLEFL val = 0;
			for(k = 0; k < nx; k++) {
       	     			val += ((MLEFL) wQ->me[i][k]) * ((MLEFL) wlambda->ve[k]) * ((MLEFL) wQ->me[j][k]);
       	  		}
       		  AA[i][j] = val;
      		}
	}
} else {
	thError("%s: ERROR - count of negative eigenvalues (count_negative = %d) not acceptable", name, count_negative);
	return(count_negative);
}


return(SH_SUCCESS);
}



