#ifndef THBUNDLE_H
#define THBUNDLE_H

#include "igraph.h"

typedef int OBJC_MEMORY_STATUS;
#define LINEAR (0)
#define HUB (1 << 0)
#define FIT (1 << 1)
#define FIT_NEIGHBOR (1 << 2)

#if 0
typedef enum objc_memory_status {
	HUB, /* these are objects that have a lot of neighbors, once created they are kept in the memory until their adjacencies are totally exhausted */
	FIT, /* these are objects that are fit for their non-linear model parameters. These objects, once created are never destroyed */
	FIT_NEIGHBOR, /* these are objects neighboring FIT objects. These objects too, once created are never destroyed */
	LINEAR, /* these are object which are only fit for their amplitude and are neither hubs nor FIT_NEIGHBORs. They are the most prone to deletion and recreation */
	N_STATUS,
	UNKNOWN_OBJC_MEMORY_STATUS 
} OBJC_MEMORY_STATUS;
#endif
	
typedef struct memory_usage {
	/* the total amount that can be used */
	long int total; /* the total amount of memory that can be used by the system */

	long int total_hub; /* the total amount of memory used for hubs */
	long int total_fit; /* the total amount of memory used for fit and their neighbors */
	long int total_linear; /* the total amount of memory used for the rest of objects */

	/* the current amount under use */
	long int current;
	
	long int current_hub;
	long int current_fit;
	long int current_linear;

	/* single cluster limits */
	long int single_cluster;

} MEMORY_USAGE;
	

typedef struct thbundle {

	
	int best_bundle;
	int old_best_bundle;

} THBUNDLE;









#endif


