#ifndef THOBJCTYPES_H
#define THOBJCTYPES_H

#include "string.h"
#include "phObjc.h"
#include "thConsts.h"
#include "thFnalTypes.h"
#include "thBasic.h"
#include "thMathTypes.h"
#include "thCalibTypes.h"
#include "thFitmaskTypes.h"

#if 0 /* the new PHPROPS should be a composite structure that also has calibration data */
#define PHPROPS OBJC_IO
#endif
#define THPROPS FUNCPARS

#define phPropsNew thPhpropsNew
#define phPropsDel thPhpropsDel
#define phPropsGet thPhpropsGet
#define phPropsPut thPhpropsPut
#define phPropsDraw thPhpropsDraw

#define thObjcNew thThobjcNew
#define thObjcDel thThobjcDel


typedef struct phprops {
	
	WOBJC_IO *phObjc; /* imported from phObjcFile */
	CALIB_WOBJC_IO *cwphobjc; /* CalibObjc enerated inside the code */
	CALIB_PHOBJC_IO *cphobjc; /* CalibObjc imported */
	PHCALIB *calibData; /* imported from CalibPhotoGlobal */
	OBJMASK *objmask; /* object mask derived from Atlas images produced by PHOTO */
	int bndex[NCOLOR]; /* this is the position of each filter in phobjc */

} PHPROPS;

typedef struct singleobjc {
  /* object types; should it be an array of types or simply one type 
   currently accepts an array of types */
  char **phType;
  char **thType;
  /* properties - the type indicators are defined above, they are renamed for
     simple follow up elsewhere 
  */
  PHPROPS *phProps;
 
  /*  
  CHAIN *thprops;
  */
  /* masks -- there should be pointers to the same masks from within
   thProps and phProps */
  OBJMASK *phMask;
  OBJMASK *thMask;
  /* 
     reconstructed model for the object -- there should be a pointer to the
     same reconstructed model from within thProps
  */
  REGION *thModel;

} SINGLEOBJC;

typedef struct thprop {

	char *mname;
	char *pname;
	void *value;

} THPROP;


typedef struct thObjc {

	/* identification */
	long int sdssid; /* id of this object as listed by photo catalog */
	THOBJCID thid; /* id of this object as listed by soho - the same as sdssid if it is not a newly discovered object */


	/* typology */
	OBJ_TYPE sdsstype;
	char *sdssname;
	THOBJCTYPE thobjctype; /* typology of the object according to soho rules */

	/* photometry */
	void *phprop; /* sdss properties of the object */
	char *phname;
	TYPE phtype;

	CHAIN *thprop; /* soho properties of the each component of this model */
	

	/* I-O */
	void *ioprop; /*  properties of this object caste for io */
	char *ioname; /* the name of the ioprop type */
        TYPE iotype; /* type of the ioprop */

	/* residual analysis for this object */	
	FITMASK *fitmask;

	int band;
	int bndex; /* index of this particular band in photo objc_io */

	/* private - checks if this object has been copied */
	int copy; 	
	void *source;	
} THOBJC;

#define COPY_NONE   ((unsigned int) 0)
#define COPY_PHPROP ((unsigned int) 1 << 1)
#define COPY_THPROP ((unsigned int) 1 << 2)
#define COPY_IOPROP ((unsigned int) 1 << 3)

/* handlers */
THPROP *thPropNew(char *mname, char *pname, void *value);
void thPropDel(THPROP *prop);
void thPropDestroy(THPROP *prop);

PHPROPS *thPhpropsNew();
void thPhpropsDel(PHPROPS *phprops);

RET_CODE thPhpropsGet(PHPROPS *prop, WOBJC_IO **phObjc, int **bndex, CALIB_WOBJC_IO **cwPhObjc, CALIB_PHOBJC_IO **cPhObjc, PHCALIB **calibData);
RET_CODE thPhpropsGetNcolor(PHPROPS *p, int *ncolor);
RET_CODE thPhpropsPut(PHPROPS *p, WOBJC_IO *phObjc, int *bndex, CALIB_WOBJC_IO *cwPhObjc, CALIB_PHOBJC_IO *cPhObjc,  PHCALIB *calibData, OBJMASK *mask);
RET_CODE thPhpropsPutFlagLRG(PHPROPS *phprop, int f);

RET_CODE thPhpropsGetObjmask(PHPROPS *p, OBJMASK **mask);
RET_CODE thPhpropPutObjmask(PHPROPS *p, OBJMASK *mask);

THOBJC *thThobjcNew();
RET_CODE thThobjcDel(THOBJC *objc);

RET_CODE thPropPut(THPROP *prop, char *mname, char *pname, void *value);
RET_CODE thPropGet(THPROP *prop, char **mname, char **pname, void **value);
RET_CODE thPropGetCountMag(THPROP *prop, THPIX *count, THPIX *mag);

RET_CODE thPropChainGetByMname(CHAIN *props, char *mname, char **pname, void **value);
int thObjcHasModel(THOBJC *objc, char *mname);

/* get and put */
RET_CODE thObjcGetIopar(THOBJC *objc, void **p, TYPE *t);

/* copy functions, used in capture package */
RET_CODE thPropCopy(THPROP *source, THPROP **target);
RET_CODE thPropCopyChain(CHAIN *schain, CHAIN **tchain);
RET_CODE thObjcCopy(THOBJC *source, THOBJC **target, unsigned int cmode);
RET_CODE thObjcCopyChain(CHAIN *sChain, CHAIN **tchain, unsigned int cmode);

RET_CODE thEmptyLineWrite(FILE *fil, char *c);
RET_CODE thObjcWriteHdr(FILE *fil, THOBJC *objc);
RET_CODE thObjcWrite(FILE *fil, THOBJC *objc);

RET_CODE thObjcGetType(THOBJC *objc, THOBJCTYPE *type);
RET_CODE thObjcGetClassName(THOBJC *objc, char **classname);
RET_CODE thObjcGetClassType(THOBJC *objc, THOBJCTYPE *type);
RET_CODE thObjcGetNmodel(THOBJC *objc, int *nmodel);
RET_CODE thObjcGetNmodelByType(THOBJCTYPE objctype, int *nmodel);
RET_CODE thObjcGetModelPropByIndex(THOBJC *objc, int i, THPROP **prop);
/* drawing random parameter in objc_io */
RET_CODE thWObjcIoDrawRecord(WOBJC_IO*s, WOBJC_IO *t, char *rname, THPIX nsigma);
RET_CODE thWObjcIoDraw(WOBJC_IO *s, WOBJC_IO *t, char **rnames, int nrecord, THPIX nsigma);

RET_CODE output_phobjc_checklist(char *comment, WOBJC_IO *phobjc);
RET_CODE output_objc_checklist(char *comment, THOBJC *objc);
RET_CODE output_calib_phobjc_checklist(char *comment, CALIB_PHOBJC_IO *phobjc);

void thPhpropChainPrintMatchStat(CHAIN *phprops);

RET_CODE thObjcGetPropByMname(THOBJC *objc, char *mname, THPROP **prop);
RET_CODE thObjcGetPhprops(THOBJC *objc, PHPROPS **phprops);
RET_CODE thObjcGetFitmask(THOBJC *objc, FITMASK **fitmask);

RET_CODE thObjcCompare(THOBJC *objc1, THOBJC *objc2, int *comparison);
RET_CODE thObjcGetPhCenter(THOBJC *objc, THPIX *xc, THPIX *yc);

#endif
