#include "thAsciischema.h"
#include "thProcTypes.h"
#include "thDefTypes.h"

CHAIN *thschematank; /* this is the schema tank which 
			stores all the ASCIISCHEMA's dispatched 
		     */

ASCIISCHEMAELEM *thAsciischemaelemNew() {

  ASCIISCHEMAELEM *elem;
  elem = (ASCIISCHEMAELEM *) thCalloc(1, sizeof(ASCIISCHEMAELEM));
 
  elem->name = thCalloc(MX_STRING_LEN, sizeof(char));
  elem->offset = -1;
  elem->type = THASCIIUNKNOWN;
  /* default is a scalar field */
  elem->heaptype = THASCIINOHEAP;
  elem->comments = thCalloc(MX_STRING_LEN, sizeof(char));

  return(elem);

}

RET_CODE thAsciischemaelemDel(ASCIISCHEMAELEM *elem) {

  if (elem == NULL) return(SH_SUCCESS);

  if (elem->name != NULL) thFree(elem->name);
  if (elem->comments != NULL) thFree(elem->comments);

  thFree(elem);

  return(SH_SUCCESS);
}

RET_CODE thAsciischemaelemPut(ASCIISCHEMAELEM *elem, 
			      char *name, int *offset, THASCIITYPE *type, 
			      THASCIIHEAPTYPE *heaptype, char *comments){

  if (elem == NULL) return(SH_GENERIC_ERROR);

  int len;

  if (name != NULL) {
    len = strlen(name);
    memcpy(elem->name, name, (len * sizeof(char)));
  }

  if (offset != NULL) {
    elem->offset = *offset;
  }

  if (type != NULL) {
    elem->type = *type;
  }

  if (heaptype != NULL) {
    elem->heaptype = *heaptype;
  }

  if (comments != NULL) {
    len = strlen(comments);
    memcpy(elem->comments, comments, (len * sizeof(char)));
  }
  return(SH_SUCCESS);

}

int thAsciischemaelemAdd(ASCIISCHEMA *asciischema, ASCIISCHEMAELEM *elem) {

  char *name = "thAsciischemaelemAddToSchema";

  /* 
     output description:

     -1: problematic wrong - no element was added to the schema 
     0 : successful run, element was added and should not be freed 
         when returning from this function
     +1: successful run, element existed, its contents were changed
         element should be freed after returning from this function
  */

  if (asciischema == NULL) return(-1);
  if (elem == NULL) return(0);

  if (elem->name == NULL || strlen(elem->name) == 0) return(-1);

  int i;
  i = thAsciischemaelemGetPosByName(asciischema, elem->name);
 
  ASCIISCHEMAELEM *el;

  if (i != -1) {
    /* caution: if this happens you can free *elem after calling this function */
    el = (ASCIISCHEMAELEM *) shChainElementGetByPos(asciischema->elchain, i);
    thAsciischemaelemCopy(el, elem); /* destination, source */
    thError("%s: Warning: Update existing elem information", name);
    return(1);
  } else {
    shChainElementAddByPos(asciischema->elchain, 
			   elem, "ASCIISCHEMAELEM", TAIL, AFTER);
    return(0);
  }

}

RET_CODE thAsciischemaelemCopy(ASCIISCHEMAELEM *dst, ASCIISCHEMAELEM* src) {

  char *name = "thAsciischemaelemCopy";
  
  if (src == NULL) {
    thError("%s: NULL destination pointer", name);
    return(SH_GENERIC_ERROR);
  }
  
  if (dst == NULL) {
    thError("%s: NULL destination pointer", name);
    return(SH_GENERIC_ERROR);
  }

  memcpy(dst->name, src->name, (strlen(src->name) * sizeof(char)));
  dst->offset = src->offset;
  dst->type = src->type;
  dst->heaptype = src->heaptype;
  memcpy(dst->comments, src->comments, (strlen(src->comments) * sizeof(char)));

  return(SH_SUCCESS);
  
}

int thAsciischemaelemGetPosByName(ASCIISCHEMA *asciischema, char *elname) {

  char *name = "thAsciischemaelemFindByName";
  
  if (asciischema == NULL) {
    thError("%s: NULL schema", name);
    return(-1);
  }

  if (asciischema->elchain == NULL) {
    thError("%s: schema has not valuable contents", name);
    return(-1);
  }

  if (elname == NULL || strlen(elname) == 0) {
    thError("%s: no proper element name provided", name);
    return(-1);
  } 

  int i, nelem, iel = -1;
  nelem = shChainSize(asciischema->elchain);
  
  ASCIISCHEMAELEM *el;
  for (i = 0; i < nelem; i++) {
    el = (ASCIISCHEMAELEM *) shChainElementGetByPos(asciischema->elchain, i);
    if (thAsciischemaelemMatchByName(el, elname) == 0) {
      iel = i;
    }
  }

  return(iel);

}
  
ASCIISCHEMAELEM *thAsciischemaelemGetByName(ASCIISCHEMA *asciischema, char *elname) {
  
  char *name = "thAsciischemaelemGetByName";

  int iel;
  iel = thAsciischemaelemGetPosByName(asciischema, elname);

  if (iel < 0) {
    thError("%s: ERROR - no element (%s) exists in schema", name, elname);
    return(NULL);
  }

  ASCIISCHEMAELEM *el;
  el = (ASCIISCHEMAELEM *) shChainElementGetByPos(asciischema->elchain, iel);
  return(el);

}


int thAsciischemaelemMatchByName(ASCIISCHEMAELEM *elem, char *elname) {

  char *name = "thAsciischemaelemMatchByName";
  
  if (elem == NULL) {
    thError("%s: NULL input element", name);
    return(-1);
  }

  if ((elem->name == NULL) || (strlen(elem->name) == 0)) {
    thError("%s: incomplete element passed", name);
    return(-1);
  }

  if (elname == NULL || strlen(elname) == 0) {
    thError("%s: NULL name string", name);
    return(-1);
  }

  if (strlen(elname) != strlen(elem->name)) return(-1);

  return(memcmp(elname, elem->name, strlen(elname)));

}

ASCIISCHEMA *thAsciischemaNew(char *type) {
  
  char *name = "thAsciischemaNew";

  if (type == NULL || strlen(type) == 0) {
    thError("%s: No proper type name was passed", name);
    return(NULL);
  }

  ASCIISCHEMA *asciischema;

  asciischema = (ASCIISCHEMA *) thCalloc(1, sizeof(ASCIISCHEMA));
  asciischema->type = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  memcpy(asciischema->type, type, strlen(type));
  asciischema->elchain = (CHAIN *) shChainNew("ASCIISCHEMAELEM");

  return(asciischema);

}

RET_CODE thAsciischemaDel(ASCIISCHEMA *asciischema) {
  
  char *name = "thAsciischemaDel";


  if (asciischema->elchain != NULL) {

    int i, nelem;
    nelem = shChainSize(asciischema->elchain);
    
    ASCIISCHEMAELEM *elem;
    for (i = 0; i < nelem; i++) {
      elem = (ASCIISCHEMAELEM *) shChainElementGetByPos(asciischema->elchain, i);
      thAsciischemaelemDel(elem);
    }
   
    shChainDel(asciischema->elchain);
   
  }

  if (asciischema->type != NULL) {
    thFree(asciischema->type);
  }

  thFree(asciischema);

  return(SH_SUCCESS);

}


RET_CODE thAsciischemaelemListAdd(ASCIISCHEMA *asciischema, 
				  char **elname, int *offset, 
				  THASCIITYPE *type, THASCIIHEAPTYPE *heaptype,
				  char **comments,
				  int nelem) {
  
  /* note: this function does not check if the element names are proper */

  char *name = "thAsciischemaelemListAdd";

  ASCIISCHEMAELEM *el;

  RET_CODE status;
  int i, pout;
  for (i = 0; i < nelem; ++i) {
    el = thAsciischemaelemNew();
    thAsciischemaelemPut(el, elname[i], offset + i, type + i, heaptype + i, comments[i]);
    if ((pout = thAsciischemaelemAdd(asciischema, el)) == 1) thFree(el);
    if (pout == -1) {
      thError("%s: problematic element, position %d of the list", 
		     name, i);
      status = SH_GENERIC_ERROR;
    }
  }

  return(status);

}

RET_CODE thTankInit() {

  char *name = "thTankInit";

  if (thschematank == NULL) {
    thschematank = shChainNew("ASCIISCHEMA");
  } else {
    thError("%s: Warning, tank already initialized", name);
    
  }

  /* now importing schema one by one */

  ASCIISCHEMA *asciischema;
  asciischema = set_schema_framefiles();
  thTankSchemaAdd(asciischema);

  asciischema = set_schema_procfiles();
  thTankSchemaAdd(asciischema);

  /* successful operations */
   
  return(SH_SUCCESS);
}


RET_CODE thTankFini() {
  char *name = "thTankFini";

  if (thschematank ==  NULL) return(SH_SUCCESS);
  
  int i, nschema;
  nschema = shChainSize(thschematank);

  ASCIISCHEMA *asciischema;
  for (i = 0; i < nschema; i++) {
    asciischema = thTankSchemaGetByPos(i);
    thAsciischemaDel(asciischema);
  }

  shChainDel(thschematank);

  return(SH_SUCCESS);

}


int thTankSchemaAdd(ASCIISCHEMA *asciischema) {

  char *name = "thTankSchemaAdd";

  if (asciischema == NULL) {
    thError("%s: NULL schema passed", name);
    return(-1);
  }

  if ((asciischema->type == NULL) || (strlen(asciischema->type) == 0)) {
    thError("%s: problematic schema passed", name);
    return(-1);
  }

  int i;
  i = thTankSchemaGetPosByType(asciischema->type);

  ASCIISCHEMA *oldschema;
  if (i == -1) {
    /* the schema was not found, adding the new schema to the chain */
    shChainElementAddByPos(thschematank, asciischema, 
			   "ASCIISCHEMA", TAIL, AFTER);
    return(0);
  } else {
    oldschema = (ASCIISCHEMA *) shChainElementGetByPos(thschematank, i);
    thAsciischemaDel(oldschema);
    shChainElementChangeByPos(thschematank, i, asciischema);
    thError("%s: old schema was replaced by new schema", name);
    return(1);
  }

}

ASCIISCHEMA *thTankSchemaGetByPos(int pos) {

  char *name = "thTankSchemaGetByPos";

  if (thschematank == NULL) {
    thError("%s: no tank available", name);
    return(NULL);
  }

  ASCIISCHEMA *thschema;
  thschema = (ASCIISCHEMA *) shChainElementGetByPos(thschematank, pos);

  return(thschema);

}
  
ASCIISCHEMA *thTankSchemaGetByType(char *type) {
  
  char *name = "thTankSchemaGetByType";

  int ischema = -1;
  ischema = thTankSchemaGetPosByType(type);


  if (ischema != -1) {
    ASCIISCHEMA *asciischema;
    asciischema = thTankSchemaGetByPos(ischema);
    return(asciischema);
  } else {
    return(NULL);
  }
    
}

int thTankSchemaGetPosByType(char *type) {
  
  char *name = "thTankSchemaGetPosByType";

  if (thschematank == NULL) {
    thError("%s: no tank available", name);
    return(-1);
  }
  
  int nschema;
  nschema = shChainSize(thschematank);

  if (nschema == 0) {
    thError("%s: tank is empty", name);
    return(-1);
  }

  int i, ischema = -1;
  ASCIISCHEMA *asciischema;
  for (i = 0; i < nschema; i++) {
    asciischema = thTankSchemaGetByPos(i);
    if (strmatch(asciischema->type, type)) {
      ischema = i;
      break;
    }
  }

  return(ischema);
    
}

ASCIISCHEMA *set_schema_framefiles() {

  char *name = "set_schema_framefiles";

  /* 
     This should be easily doable with SCHEMA's and SCHEMATRANS's
     but many of the handlers are of static type in Dervish and PHOTO
     Don't wanna change it for now 
  */

  int i, nelem = 54;

  char **namelist;

  namelist = (char **) thCalloc(nelem, sizeof(char *));
  namelist[0] = (char *) thCalloc(MX_STRING_LEN * nelem, sizeof(char));
  for (i = 1; i < nelem; i++) {
    namelist[i] = namelist[0] + i * MX_STRING_LEN;
  }

  i = 0;
  namelist[i] =  "phconfigfile";i++;
  namelist[i] =  "phecalibfile";i++;
  namelist[i] =  "phflatframefile";i++;
  namelist[i] =  "phmaskfile";i++;
  namelist[i] =  "phatlasfile"; i++;
  namelist[i] =  "phsmfile";i++;
  namelist[i] =  "phpsfile";i++;
  namelist[i] =  "phobjcfile";i++;
  namelist[i] =  "photoObjcfile"; i++;
  namelist[i] =  "phcalibfile";i++; 
  namelist[i] =  "gcfile"; i++;
  namelist[i] =  "thsbfile";i++;
  namelist[i] =  "thobfile";i++;
  namelist[i] =  "thmaskselfile";i++;
  namelist[i] =  "thobjcselfile";i++;
  namelist[i] =  "thsmfile";i++;
  namelist[i] =  "thpsfile";i++;
  namelist[i] =  "thobjcfile";i++;
  namelist[i] =  "thflatframefile"; i++;
  namelist[i] =  "thlmfile"; i++;
  namelist[i] =  "thlpfile"; i++;
  namelist[i] =  "thlrfile"; i++;
  namelist[i] =  "thlzfile"; i++;
  namelist[i] =  "thlwfile"; i++;
  namelist[i] =  "thpsfreportfile"; i++;
  namelist[i] =  "thpsfstatfile"; i++;
  namelist[i] =  "outdir"; i++; 
/* added in 2016 for multi-object simulations */
namelist[i] =  "outphflatframefileformat";  i++; 
namelist[i] =  "inphmaskfileformat";  i++; 
namelist[i] =  "inphsmfileformat";  i++; 
namelist[i] =  "outphsmfileformat";   i++; 
namelist[i] =  "inphobjcfile";  i++; 
namelist[i] =  "incalibobjcfile"; i++;
namelist[i] =  "outphobjcfile";  i++; 
namelist[i] =  "outthsmfileformat";  i++; 
namelist[i] =  "outthobjcfileformat";  i++; 
namelist[i] =  "outthflatframefileformat";  i++; 

namelist[i] =  "outphflatframefile";  i++; 
namelist[i] =  "inphsmfile";  i++; 
namelist[i] =  "outphsmfile";   i++; 
namelist[i] =  "outthsmfile";  i++; 
namelist[i] =  "outthobjcfile";  i++; 
namelist[i] =  "outthflatframefile";  i++; 


namelist[i] =  "inphflatframefileformat";  i++; 
namelist[i] =  "inthflatframefileformat";  i++; 
namelist[i] =  "inphflatframefile";  i++; 
namelist[i] =  "inthflatframefile";  i++; 
namelist[i] =  "outlMfileformat";  i++;
namelist[i] =  "outlPfileformat";  i++; 
namelist[i] =  "outlRfileformat";  i++;
namelist[i] =  "outlZfileformat";  i++; 
namelist[i] =  "outlWfileformat";  i++; 
namelist[i] =  "outpsfreportfileformat";  i++; 
namelist[i] =  "outpsfstatsfileformat";  i++;

  if (i != nelem) {
    thError("%s: could not appropriate the Ascii schema, expected (%d) names, received (%d) names", name, nelem, i);
    return (NULL);
  }

  int *offsetlist;
  offsetlist = (int *) thCalloc(nelem, sizeof(int));
  
  i = 0;
  offsetlist[i] = offsetof(FRAMEFILES, phconfigfile); i++;
  offsetlist[i] = offsetof(FRAMEFILES, phecalibfile); i++;
  offsetlist[i] = offsetof(FRAMEFILES, phflatframefile); i++;
  offsetlist[i] = offsetof(FRAMEFILES, phmaskfile); i++;
  offsetlist[i] = offsetof(FRAMEFILES, phatlasfile); i++;
  offsetlist[i] = offsetof(FRAMEFILES, phsmfile); i++;
  offsetlist[i] = offsetof(FRAMEFILES, phpsfile); i++;
  offsetlist[i] = offsetof(FRAMEFILES, phobjcfile); i++;
  offsetlist[i] = offsetof(FRAMEFILES, photoObjcfile); i++;
  offsetlist[i] = offsetof(FRAMEFILES, phcalibfile); i++;
  offsetlist[i] = offsetof(FRAMEFILES, gcfile); i++;
  offsetlist[i] = offsetof(FRAMEFILES, thsbfile); i++;
  offsetlist[i] = offsetof(FRAMEFILES, thobfile); i++;
  offsetlist[i] = offsetof(FRAMEFILES, thmaskselfile); i++;
  offsetlist[i] = offsetof(FRAMEFILES, thobjcselfile); i++;
  offsetlist[i] = offsetof(FRAMEFILES, thsmfile); i++;
  offsetlist[i] = offsetof(FRAMEFILES, thpsfile); i++;
  offsetlist[i] = offsetof(FRAMEFILES, thobjcfile); i++;
  offsetlist[i] = offsetof(FRAMEFILES, thflatframefile); i++;
  offsetlist[i] = offsetof(FRAMEFILES, thlmfile); i++;
  offsetlist[i] = offsetof(FRAMEFILES, thlpfile); i++;
  offsetlist[i] = offsetof(FRAMEFILES, thlrfile); i++;
  offsetlist[i] = offsetof(FRAMEFILES, thlzfile); i++;
  offsetlist[i] = offsetof(FRAMEFILES, thlwfile); i++;
  offsetlist[i] = offsetof(FRAMEFILES, thpsfreportfile); i++;
  offsetlist[i] = offsetof(FRAMEFILES, thpsfstatfile); i++;
  offsetlist[i] = offsetof(FRAMEFILES, outdir); i++;
/* added in 2016 for multiobject simulations */
offsetlist[i] = offsetof(FRAMEFILES, outphflatframefileformat);  i++;
offsetlist[i] = offsetof(FRAMEFILES, inphmaskfileformat);  i++;
offsetlist[i] = offsetof(FRAMEFILES, inphsmfileformat);  i++;
offsetlist[i] = offsetof(FRAMEFILES, outphsmfileformat);  i++;
offsetlist[i] = offsetof(FRAMEFILES, inphobjcfile);  i++;
offsetlist[i] = offsetof(FRAMEFILES, incalibobjcfile);  i++;
offsetlist[i] = offsetof(FRAMEFILES, outphobjcfile);  i++;
offsetlist[i] = offsetof(FRAMEFILES, outthsmfileformat);  i++;
offsetlist[i] = offsetof(FRAMEFILES, outthobjcfileformat);  i++;
offsetlist[i] = offsetof(FRAMEFILES, outthflatframefileformat);  i++;
offsetlist[i] = offsetof(FRAMEFILES, outphflatframefile);  i++;
offsetlist[i] = offsetof(FRAMEFILES, inphsmfile);  i++;
offsetlist[i] = offsetof(FRAMEFILES, outphsmfile);  i++;
offsetlist[i] = offsetof(FRAMEFILES, outthsmfile);  i++;
offsetlist[i] = offsetof(FRAMEFILES, outthobjcfile);  i++;
offsetlist[i] = offsetof(FRAMEFILES, outthflatframefile);  i++;


offsetlist[i] = offsetof(FRAMEFILES, inphflatframefileformat);  i++; 
offsetlist[i] = offsetof(FRAMEFILES, inthflatframefileformat);  i++; 
offsetlist[i] = offsetof(FRAMEFILES, inphflatframefile);  i++; 
offsetlist[i] = offsetof(FRAMEFILES, inthflatframefile);  i++; 
offsetlist[i] = offsetof(FRAMEFILES, outlMfileformat);  i++; 
offsetlist[i] = offsetof(FRAMEFILES, outlPfileformat);  i++; 
offsetlist[i] = offsetof(FRAMEFILES, outlRfileformat);  i++; 
offsetlist[i] = offsetof(FRAMEFILES, outlZfileformat);  i++; 
offsetlist[i] = offsetof(FRAMEFILES, outlWfileformat);  i++; 
offsetlist[i] = offsetof(FRAMEFILES, outpsfreportfileformat);  i++; 
offsetlist[i] = offsetof(FRAMEFILES, outpsfstatsfileformat);  i++;




  if (i != nelem) {
    thError("%s: could not appropriate the Ascii schema, expected (%d) offsets, received (%d) offsets", name, nelem, i);
    return (NULL);
  }
    
  THASCIITYPE *typelist;
  typelist = (THASCIITYPE *) thCalloc(nelem, sizeof(THASCIITYPE));
  for (i = 0; i < nelem; i++) {
    typelist[i] = THASCIIS;
  }

  /* comment fields */
  char **comments;

  comments = (char **) thCalloc(nelem, sizeof(char *));
  comments[0] = thCalloc(nelem * MX_STRING_LEN, sizeof(char));
  for (i = 1; i < nelem; i++) {
    comments[i] = comments[0] + i * MX_STRING_LEN;
  }

  i = 0;
  comments[i] = "address of the opConfig parfile"; i++;
  comments[i] = "address of the opECalib parfile"; i++;
  
  comments[i] = "address of fpC file generated by PHOTO"; i++;
  comments[i] = "address of fpM file generated by PHOTO"; i++;
  comments[i] = "address of fpAtlas file generated by PHOTO"; i++;
  comments[i] = "address of fpBIN file generated by PHOTO"; i++;
  comments[i] = "address of psField file generated bt PHOTO"; i++;
  comments[i] = "address of fpObjc file generated by PHOTO"; i++;
  comments[i] = "address of photoObj file generated by PHOTO"; i++;
  comments[i] = "address of calibPhotomGlobal-$RUN-$CAMCOL.fits files generated by PHOTOOP since DR3"; i++;
  comments[i] = "address of the GALACTIC_CALIB file (in / out)"; i++;
  /* the following two files should be imported once for all */
  comments[i] = "address of the sky basis functions to be read - imported once for all"; i++;
  comments[i] = "address of the object basis models to be read - imported once for all"; i++;
    
  
  /* selection and creation rules */
  comments[i] = "address of file containing the rule to make each mask"; i++;
  comments[i] = "address of the object selection rules for fitting"; i++;
  
  /* output and input */
  comments[i] = "address of the sky model file for SOHO"; i++;
  comments[i] = "address of the PSF model file for SOHO"; i++;
  comments[i] = "address of object model parameter file for SOHO"; i++;
  comments[i] = "address of model file generated by SOHO"; i++;
  comments[i] = "address of the file containing LM parameters"; i++;
  comments[i] = "address of the file containing LProfiles for objects and their component models"; i++;
  comments[i] = "address of the file containing residual = fit model - data"; i++;
  comments[i] = "address of the file containing z-score for residuals"; i++;
  comments[i] = "address of the file containing weight image"; i++;
  comments[i] = "address of the file containing the extensive psf reports done for random positions in the field"; i++;
   comments[i] = "address of the file containing the statistical analysis of the psf reports listed in psfreportfile"; i++;
  /* directory for outputting anything else */

  /* products outdir */
  comments[i] = "location of products when not specified in framesfile list"; i++;
/* added in 2016 for multiobject simulations */
comments[i] = "output fpC file format - band to be inserted"; i++;
comments[i] = "input fpM file format - band to be inserted"; i++;
comments[i] = "input fpBIN file format - band to be inserted"; i++;
comments[i] = " output fpBIN file format - band to be inserted"; i++;
comments[i] = "input fpObjc file"; i++;
comments[i] = "input photoObj file"; i++;
comments[i] = "output fpObjc file" ; i++;
comments[i] = " output gpBIN file format - band to be inserted"; i++;
comments[i] = "output gpObjc file format - band to be inserted"; i++;
comments[i] = "output fpC file format - band to be inserted"; i++;

comments[i] = "output fpC file format - after band was inserted"; i++;
comments[i] = "input fpBIN file format - after band was inserted"; i++;
comments[i] = "output fpBIN file format - after band was inserted"; i++;
comments[i] = "output gpBIN file format - after band was inserted"; i++;
comments[i] = "ouput gpObjc file format - after band was inserted"; i++;
comments[i] = "output fpC file format - after band was inserted"; i++;

comments[i] = "input fpC file format - band to be inserted"; i++; 
comments[i] = "input fpC file format - band to be inserted"; i++;
comments[i] = "input fpC file format - after band was inserted"; i++;
comments[i] = "input fpC file format - after band was inserted"; i++;

comments[i] = "output LM file format to trace chisq during MLE"; i++;
comments[i] = "output LProfile file format to generate 1D profile for objects and their model components"; i++;
comments[i] = "output LR file format to output residual = fit model - data"; i++;
comments[i] = "output LZ file format to output z-score of the residuals"; i++;
comments[i] = "output LW file format to output weight image"; i++;
comments[i] = "output psf report file format"; i++;
comments[i] = "output psf statistics file format"; i++;




  if (i != nelem) {
    thError("%s: could not appropriate the Ascii schema, expected (%d) comments, received (%d) comments", name, nelem, i);
    return (NULL);
  }

  /* heap type indicators */
  THASCIIHEAPTYPE *heaptypelist;
  heaptypelist = (THASCIIHEAPTYPE *) thCalloc(nelem, sizeof(THASCIIHEAPTYPE));
  for (i = 0; i < nelem; i++) {
    heaptypelist[i] = THASCIINOHEAP;
  }

  ASCIISCHEMA *asciischema;
  asciischema = thAsciischemaNew("FRAMEFILES");
  thAsciischemaelemListAdd(asciischema, 
			   namelist, offsetlist, typelist, heaptypelist, comments, nelem);

  /* now add the constructor */
  
  asciischema->constructor = &thFramefilesNew;

  return(asciischema);
  
}


ASCIISCHEMA *set_schema_procfiles() {

  char *name = "set_schema_framefiles";

  /* 
     This should be easily doable with SCHEMA's and SCHEMATRANS's
     but many of the handlers are of static type in Dervish and PHOTO
     Don't wanna change it for now 
  */

  int i, nelem = 8;

  char **namelist;

  namelist = (char **) thCalloc(nelem, sizeof(char *));
  for (i = 0; i < nelem; i++) {
    namelist[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }

  i = 0;
  namelist[i] =  "thframefile"; i++;
  namelist[i] =  "thsbfile"; i++;
  namelist[i] =  "thobfile"; i++;
  namelist[i] =  "thmaskselfile"; i++;
  namelist[i] =  "thobjcselfile"; i++;
  namelist[i] =  "thsmfile"; i++;
  namelist[i] =  "thobjcfile"; i++;
 int n0;
  n0 = i;
  namelist[i] =  "thdeffiles"; i++;

  
  if (i != nelem) {
    thError("%s: could not appropriate the Ascii schema", name);
    return (NULL);
  }

  int *offsetlist;
  offsetlist = (int *) thCalloc(nelem, sizeof(int));
  
  i = 0;
  offsetlist[i] = offsetof(PROCFILES, thframefile); i++;
  offsetlist[i] = offsetof(PROCFILES, thsbfile); i++;
  offsetlist[i] = offsetof(PROCFILES, thobfile); i++;
  offsetlist[i] = offsetof(PROCFILES, thmaskselfile); i++;
  offsetlist[i] = offsetof(PROCFILES, thobjcselfile); i++;
  offsetlist[i] = offsetof(PROCFILES, thsmfile); i++;
  offsetlist[i] = offsetof(PROCFILES, thobjcfile); i++;
  offsetlist[i] = offsetof(PROCFILES, thdeffiles); i++;
  if (i != nelem) {
    thError("%s: could not appropriate the Ascii schema", name);
    return (NULL);
  }
    
  THASCIITYPE *typelist;
  typelist = (THASCIITYPE *) thCalloc(nelem, sizeof(THASCIITYPE));
  for (i = 0; i < nelem; i++) {
    typelist[i] = THASCIIS;
  }

  /* comment fields */
  char **comments;

  comments = (char **) thCalloc(nelem, sizeof(char *));
  for (i = 0; i < nelem; i++) {
    comments[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }

  i = 0;
  comments[i] = "the address in which all framefiles are stored"; i++;
  
  /* the following two files should be imported once for all */
  comments[i] = "address of the sky basis functions to be read - imported once for all"; i++;
  comments[i] = "address of the object basis models to be read - imported once for all"; i++;
    
  
  /* selection and creation rules */
  comments[i] = "address of file containing the rule to make each mask"; i++;
  comments[i] = "address of the object selection rules for fitting"; i++;
  
  /* output and input */
  comments[i] = "address of the sky model file for SOHO"; i++;
  comments[i] = "address of object model parameter file for SOHO"; i++;
  comments[i] = "list of definition files for mask and objects"; i++;

    /* heap type indicators */
  THASCIIHEAPTYPE *heaptypelist;
  heaptypelist = (THASCIIHEAPTYPE *) thCalloc(nelem, sizeof(THASCIIHEAPTYPE));
  for (i = 0; i < n0; i++) {
    heaptypelist[i] = THASCIINOHEAP;
  }
  heaptypelist[n0] = THASCIIHEAP;

  ASCIISCHEMA *asciischema;
  asciischema = thAsciischemaNew("PROCFILES");
  thAsciischemaelemListAdd(asciischema, 
			   namelist, offsetlist, 
			   typelist, heaptypelist, 
			   comments, nelem);

  /* now add the constructor */
  
  asciischema->constructor = &thProcfilesNew;

  return(asciischema);
  
}

ASCIISCHEMA *set_schema_sentence() {

  char *name = "set_schema_sentence";

  /* 
     This should be easily doable with SCHEMA's and SCHEMATRANS's
     but many of the handlers are of static type in Dervish and PHOTO
     Don't wanna change it for now 
  */

  int i, nelem = 110;

  char **namelist;
  namelist = (char **) thCalloc(nelem, sizeof(char *));
  for (i = 0; i < nelem; i++) {
    namelist[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }

  /* name definition */
  i = 0;
  namelist[i] = "thId"; i++;
  namelist[i] = "comment"; i++;
  /* allowed operators are GT, GE, LT, LE, EQ */
  namelist[i] = "operator"; i++;
  /* tolerance is used only when operator is set to EQ */
  namelist[i] = "tolerance"; i++;
  /* the central value of test */
  namelist[i] = "value"; i++;


  /* properties: */
  /* what follows are the innars of OBJC_IO structure  */

  namelist[i] = "id"; i++;				/* id number for this objc */
  namelist[i] = "parent"; i++;			        /* id of parent for deblends */
  namelist[i] = "ncolor"; i++;			/* number of colours */
  namelist[i] = "nchild"; i++;				/* number of children */
  namelist[i] = "objc_type"; i++;			/* overall classification */
  namelist[i] = "objc_prob_psf"; i++;			/* Bayesian probability of being PSF */
  namelist[i] = "catID"; i++;			        /* catalog id number */
  namelist[i] = "objc_flags"; i++;			/* flags from OBJC */
  namelist[i] = "objc_flags2"; i++;			/* flags2 from OBJC */
  namelist[i] = "objc_rowc"; i++;
  namelist[i] = "objc_rowcErr"; i++;	/* row position and error of centre */
  namelist[i] = "objc_colc"; i++;
  namelist[i] = "objc_colcErr"; i++;	/* column position and error */
  namelist[i] = "rowv"; i++;
  namelist[i] = "rowvErr"; i++;			/* row velocity, in pixels/frame (NS)*/
  namelist[i] = "colv"; i++;
  namelist[i] = "colvErr"; i++;			/* col velocity, in pixels/frame (NS)*/
  
  /*
   * Unpacked OBJECT1s
   */

  int n0, n1;
  n0 = i; /* the lower limit for heap type field index */
  namelist[i] = "rowc"; i++;
  namelist[i] = "rowcErr"; i++;
  namelist[i] = "colc"; i++;
  namelist[i] = "colcErr"; i++;
  namelist[i] = "sky"; i++;
  namelist[i] = "skyErr"; i++;
/*
 * PSF and aperture fits/magnitudes
 */
  namelist[i] = "psfCounts"; i++;
  namelist[i] = "psfCountsErr"; i++;
  namelist[i] = "fiberCounts"; i++;
  namelist[i] = "fiberCountsErr"; i++;
  namelist[i] = "petroCounts,"; i++;
  namelist[i] = "petroCountsErr"; i++;
  namelist[i] = "petroRad"; i++;
  namelist[i] = "petroRadErr"; i++;
  namelist[i] = "petroR50"; i++;
  namelist[i] = "petroR50Err"; i++;
  namelist[i] = "petroR90"; i++;
  namelist[i] = "petroR90Err"; i++;
/*
 * Shape of object
 */
  namelist[i] = "Q"; i++;
  namelist[i] = "QErr"; i++;
  namelist[i] = "U"; i++;
  namelist[i] = "UErr"; i++;

  namelist[i] = "M_e1"; i++;
  namelist[i] = "M_e2"; i++;
  namelist[i] = "M_e1e1Err"; i++;
  namelist[i] = "M_e1e2Err"; i++;
  namelist[i] = "M_e2e2Err"; i++;
  namelist[i] = "M_rr_cc"; i++;
  namelist[i] = "M_rr_ccErr"; i++;
  namelist[i] = "M_cr4"; i++;
  namelist[i] = "M_e1_psf"; i++;
  namelist[i] = "M_e2_psf"; i++;
  namelist[i] = "M_rr_cc_psf"; i++;
  namelist[i] = "M_cr4_psf"; i++;
/*
 * Properties of a certain isophote.
 */
  namelist[i] = "iso_rowc"; i++;
  namelist[i] = "iso_rowcErr"; i++;
  namelist[i] = "iso_rowcGrad"; i++;
  namelist[i] = "iso_colc"; i++;
  namelist[i] = "iso_colcErr"; i++;
  namelist[i] = "iso_colcGrad"; i++;
  namelist[i] = "iso_a"; i++;
  namelist[i] = "iso_aErr"; i++;
  namelist[i] = "iso_aGrad"; i++;
  namelist[i] = "iso_b"; i++;
  namelist[i] = "iso_bErr"; i++;
  namelist[i] = "iso_bGrad"; i++;
  namelist[i] = "iso_phi"; i++;
  namelist[i] = "iso_phiErr"; i++;
  namelist[i] = "iso_phiGrad"; i++;

/*
 * Model parameters for deV and exponential models
 */
  namelist[i] = "r_deV"; i++;
  namelist[i] = "r_deVErr"; i++;
  namelist[i] = "ab_deV"; i++;
  namelist[i] = "ab_deVErr"; i++;
  namelist[i] = "phi_deV"; i++;
  namelist[i] = "phi_deVErr"; i++;
  namelist[i] = "counts_deV"; i++;
  namelist[i] = "counts_deVErr"; i++;
  namelist[i] = "r_exp"; i++;
  namelist[i] = "r_expErr"; i++;
  namelist[i] = "ab_exp"; i++;
  namelist[i] = "ab_expErr"; i++;
  namelist[i] = "phi_exp"; i++;
  namelist[i] = "phi_expErr"; i++;
  namelist[i] = "counts_exp"; i++;
  namelist[i] = "counts_expErr"; i++;

  namelist[i] = "counts_model"; i++;
  namelist[i] = "counts_modelErr"; i++;
/*
 * Measures of image structure
 */
  namelist[i] = "texture"; i++;
/*
 * Classification information
 */
  namelist[i] = "star_L"; i++;
  namelist[i] = "star_lnL"; i++;
  namelist[i] = "exp_L"; i++;
  namelist[i] = "exp_lnL"; i++;
  namelist[i] = "deV_L"; i++;
  namelist[i] = "deV_lnL"; i++;
  namelist[i] = "fracPSF"; i++;

  namelist[i] = "flags"; i++;
  namelist[i] = "flags2"; i++;
  
  namelist[i] = "type"; i++;
  namelist[i] = "prob_psf"; i++;
  n1 = i; /* the upper limit for the heap field index */

  /* type definition for schema  */
  THASCIITYPE *typelist;
  typelist = (THASCIITYPE *) thCalloc(nelem, sizeof(THASCIITYPE));

  i = 0;
  typelist[i] = THASCIIS; i++; /**thId; */
  typelist[i] = THASCIIS; i++; /*  *comment; */
  typelist[i] = THASCIIS; i++; /* *operator; */
  typelist[i] = THASCIIF; i++; /* tolerance; */
  typelist[i] = THASCIIF; i++; /* value; */


  /* properties: */
  /* what follows are the innars of OBJC_IO structure  */

  typelist[i] = THASCIII; i++; /* id;			 id number for this objc */
  typelist[i] = THASCIII; i++; /* parent			        id of parent for deblends */
  typelist[i] = THASCIII; i++; /* ncolor;			 number of colours */
  typelist[i] = THASCIII; i++; /* nchild;				 number of children */
  typelist[i] = THASCIII; i++; /* objc_type;		        overall classification */
  typelist[i] = THASCIIF; i++; /* objc_prob_psf;			Bayesian probability of being PSF */
  typelist[i] = THASCIII; i++; /* catID;			        catalog id number */
  typelist[i] = THASCIII; i++; /* objc_flags;			flags from OBJC */
  typelist[i] = THASCIII; i++; /* objc_flags2;			flags2 from OBJC */
  typelist[i] = THASCIIF; i++; /* objc_rowc, objc_rowcErr;	row position and error of centre */
  typelist[i] = THASCIIF; i++; /* objc_colc, objc_colcErr;	column position and error */
  typelist[i] = THASCIIF; i++; /* rowv, rowvErr;			row velocity, in pixels/frame (NS)*/
  typelist[i] = THASCIIF; i++; /* colv, colvErr;			col velocity, in pixels/frame (NS)*/

  /*
 * Unpacked OBJECT1s
 */
  typelist[i] = THASCIIF; i++; /* rowc; */
  typelist[i] = THASCIIF; i++; /* rowcErr; */
  typelist[i] = THASCIIF; i++; /* colc; */
  typelist[i] = THASCIIF; i++; /* colcErr; */
  typelist[i] = THASCIIF; i++; /* sky; */
  typelist[i] = THASCIIF; i++; /* skyErr; */
  /*
   * PSF and aperture fits/magnitudes
   */
  typelist[i] = THASCIIF; i++; /* psfCounts; */
  typelist[i] = THASCIIF; i++; /* psfCountsErr; */
  typelist[i] = THASCIIF; i++; /* fiberCounts; */
  typelist[i] = THASCIIF; i++; /* fiberCountsErr; */
  typelist[i] = THASCIIF; i++; /* petroCounts; */
  typelist[i] = THASCIIF; i++; /* petroCountsErr; */
  typelist[i] = THASCIIF; i++; /* petroRad; */
  typelist[i] = THASCIIF; i++; /* petroRadErr; */
  typelist[i] = THASCIIF; i++; /* petroR50; */
  typelist[i] = THASCIIF; i++; /* petroR50Err; */
  typelist[i] = THASCIIF; i++; /* petroR90; */
  typelist[i] = THASCIIF; i++; /* petroR90Err; */
  /*
   * Shape of object
   */
  typelist[i] = THASCIIF; i++; /* Q; */
  typelist[i] = THASCIIF; i++; /* QErr; */
  typelist[i] = THASCIIF; i++; /* U; */
  typelist[i] = THASCIIF; i++; /* UErr; */
  
  typelist[i] = THASCIIF; i++; /* M_e1; */
  typelist[i] = THASCIIF; i++; /* M_e2; */
  typelist[i] = THASCIIF; i++; /* M_e1e1Err; */
  typelist[i] = THASCIIF; i++; /* M_e1e2Err; */
  typelist[i] = THASCIIF; i++; /* M_e2e2Err; */
  typelist[i] = THASCIIF; i++; /* M_rr_cc; */
  typelist[i] = THASCIIF; i++; /* M_rr_ccErr; */
  typelist[i] = THASCIIF; i++; /* M_cr4; */
  typelist[i] = THASCIIF; i++; /* M_e1_psf; */
  typelist[i] = THASCIIF; i++; /* M_e2_psf; */
  typelist[i] = THASCIIF; i++; /* M_rr_cc_psf; */
  typelist[i] = THASCIIF; i++; /* M_cr4_psf; */
  /*
   * Properties of a certain isophote.
 */
  typelist[i] = THASCIIF; i++; /* iso_rowc; */
  typelist[i] = THASCIIF; i++; /* iso_rowcErr; */
  typelist[i] = THASCIIF; i++; /* iso_rowcGrad; */
  typelist[i] = THASCIIF; i++; /* iso_colc; */
  typelist[i] = THASCIIF; i++; /* iso_colcErr; */
  typelist[i] = THASCIIF; i++; /* iso_colcGrad; */
  typelist[i] = THASCIIF; i++; /* iso_a; */
  typelist[i] = THASCIIF; i++; /* iso_aErr; */
  typelist[i] = THASCIIF; i++; /* iso_aGrad; */
  typelist[i] = THASCIIF; i++; /* iso_b; */
  typelist[i] = THASCIIF; i++; /* iso_bErr; */
  typelist[i] = THASCIIF; i++; /* iso_bGrad; */
  typelist[i] = THASCIIF; i++; /* iso_phi; */
  typelist[i] = THASCIIF; i++; /* iso_phiErr; */
  typelist[i] = THASCIIF; i++; /* iso_phiGrad; */
  /*
   * Model parameters for deV and exponential models
   */
  typelist[i] = THASCIIF; i++; /* r_deV; */
  typelist[i] = THASCIIF; i++; /* r_deVErr; */
  typelist[i] = THASCIIF; i++; /* ab_deV; */
  typelist[i] = THASCIIF; i++; /* ab_deVErr; */
  typelist[i] = THASCIIF; i++; /* phi_deV; */
  typelist[i] = THASCIIF; i++; /* phi_deVErr; */
  typelist[i] = THASCIIF; i++; /* counts_deV; */
  typelist[i] = THASCIIF; i++; /* counts_deVErr; */
  typelist[i] = THASCIIF; i++; /* r_exp; */
  typelist[i] = THASCIIF; i++; /* r_expErr; */
  typelist[i] = THASCIIF; i++; /* ab_exp; */
  typelist[i] = THASCIIF; i++; /* ab_expErr; */
  typelist[i] = THASCIIF; i++; /* phi_exp; */
  typelist[i] = THASCIIF; i++; /* phi_expErr; */
  typelist[i] = THASCIIF; i++; /* counts_exp; */
  typelist[i] = THASCIIF; i++; /* counts_expErr; */
  
  typelist[i] = THASCIIF; i++; /* counts_model; */
  typelist[i] = THASCIIF; i++; /* counts_modelErr; */
  /*
 * Measures of image structure
 */
  typelist[i] = THASCIIF; i++; /* texture; */
  /*
   * Classification information
   */
  typelist[i] = THASCIIF; i++; /* star_L; */
  typelist[i] = THASCIIF; i++; /* tar_lnL; */
  typelist[i] = THASCIIF; i++; /* exp_L; */
  typelist[i] = THASCIIF; i++; /* exp_lnL; */
  typelist[i] = THASCIIF; i++; /* deV_L; */
  typelist[i] = THASCIIF; i++; /* deV_lnL; */
  typelist[i] = THASCIIF; i++; /* fracPSF; */
  
  typelist[i] = THASCIII; i++; /* flags; */
  typelist[i] = THASCIII; i++; /* flags2; */
  
  typelist[i] = THASCIII; i++; /* type; */
  typelist[i] = THASCIIF; i++; /* prob_psf; */

  /* offset definitions */
  int *offsetlist;
  offsetlist = (int *) thCalloc(nelem, sizeof(int));

  i = 0;
  offsetlist[i] = offsetof(SENTENCE, thId); i++;
  offsetlist[i] = offsetof(SENTENCE, comment); i++;
  offsetlist[i] = offsetof(SENTENCE, operator); i++;
  offsetlist[i] = offsetof(SENTENCE, tolerance); i++;
  offsetlist[i] = offsetof(SENTENCE, value); i++;


  /* properties: */
  /* what follows are the innars of OBJC_IO structure  */

  offsetlist[i] = offsetof(SENTENCE,  id); i++;				/* id number for this objc */
  offsetlist[i] = offsetof(SENTENCE,  parent); i++;			        /* id of parent for deblends */
  offsetlist[i] = offsetof(SENTENCE,  ncolor); i++;			/* number of colours */
  offsetlist[i] = offsetof(SENTENCE,  nchild); i++;				/* number of children */
  offsetlist[i] = offsetof(SENTENCE,  objc_type); i++;			/* overall classification */
  offsetlist[i] = offsetof(SENTENCE,  objc_prob_psf); i++;			/* Bayesian probability of being PSF */
  offsetlist[i] = offsetof(SENTENCE,  catID); i++;			        /* catalog id number */
  offsetlist[i] = offsetof(SENTENCE,  objc_flags); i++;			/* flags from OBJC */
  offsetlist[i] = offsetof(SENTENCE,  objc_flags2); i++;			/* flags2 from OBJC */
  offsetlist[i] = offsetof(SENTENCE,  objc_rowc); i++;
  offsetlist[i] = offsetof(SENTENCE,  objc_rowcErr); i++;	/* row position and error of centre */
  offsetlist[i] = offsetof(SENTENCE,  objc_colc); i++;
  offsetlist[i] = offsetof(SENTENCE,  objc_colcErr); i++;	/* column position and error */
  offsetlist[i] = offsetof(SENTENCE,  rowv); i++;
  offsetlist[i] = offsetof(SENTENCE,  rowvErr); i++;			/* row velocity, in pixels/frame (NS)*/
  offsetlist[i] = offsetof(SENTENCE,  colv); i++;
  offsetlist[i] = offsetof(SENTENCE,  colvErr); i++;			/* col velocity, in pixels/frame (NS)*/
   
/*
 * Unpacked OBJECT1s
 */
  offsetlist[i] = offsetof(SENTENCE,  rowc); i++;
  offsetlist[i] = offsetof(SENTENCE,  rowcErr); i++;
  offsetlist[i] = offsetof(SENTENCE,  colc); i++;
  offsetlist[i] = offsetof(SENTENCE,  colcErr); i++;
  offsetlist[i] = offsetof(SENTENCE,  sky); i++;
  offsetlist[i] = offsetof(SENTENCE,  skyErr); i++;
  /*
 * PSF and aperture fits/magnitudes
 */
  offsetlist[i] = offsetof(SENTENCE,  psfCounts); i++;
  offsetlist[i] = offsetof(SENTENCE,  psfCountsErr); i++;
  offsetlist[i] = offsetof(SENTENCE,  fiberCounts); i++;
  offsetlist[i] = offsetof(SENTENCE,  fiberCountsErr); i++;
  offsetlist[i] = offsetof(SENTENCE,  petroCounts); i++;
  offsetlist[i] = offsetof(SENTENCE,  petroCountsErr); i++;
  offsetlist[i] = offsetof(SENTENCE,  petroRad); i++;
  offsetlist[i] = offsetof(SENTENCE,  petroRadErr); i++;
  offsetlist[i] = offsetof(SENTENCE,  petroR50); i++;
  offsetlist[i] = offsetof(SENTENCE,  petroR50Err); i++;
  offsetlist[i] = offsetof(SENTENCE,  petroR90); i++;
  offsetlist[i] = offsetof(SENTENCE,  petroR90Err); i++;
  /*
   * Shape of object
   */
  offsetlist[i] = offsetof(SENTENCE,  Q); i++;
  offsetlist[i] = offsetof(SENTENCE,  QErr); i++;
  offsetlist[i] = offsetof(SENTENCE,  U); i++;
  offsetlist[i] = offsetof(SENTENCE,  UErr); i++;
  
  offsetlist[i] = offsetof(SENTENCE,  M_e1); i++;
  offsetlist[i] = offsetof(SENTENCE,  M_e2); i++;
  offsetlist[i] = offsetof(SENTENCE,  M_e1e1Err); i++;
  offsetlist[i] = offsetof(SENTENCE,  M_e1e2Err); i++;
  offsetlist[i] = offsetof(SENTENCE,  M_e2e2Err); i++;
  offsetlist[i] = offsetof(SENTENCE,  M_rr_cc); i++;
  offsetlist[i] = offsetof(SENTENCE,  M_rr_ccErr); i++;
  offsetlist[i] = offsetof(SENTENCE,  M_cr4); i++;
  offsetlist[i] = offsetof(SENTENCE,  M_e1_psf); i++;
  offsetlist[i] = offsetof(SENTENCE,  M_e2_psf); i++;
  offsetlist[i] = offsetof(SENTENCE,  M_rr_cc_psf); i++;
  offsetlist[i] = offsetof(SENTENCE,  M_cr4_psf); i++;
  /*
   * Properties of a certain isophote.
   */
  offsetlist[i] = offsetof(SENTENCE,  iso_rowc); i++;
  offsetlist[i] = offsetof(SENTENCE,  iso_rowcErr); i++;
  offsetlist[i] = offsetof(SENTENCE,  iso_rowcGrad); i++;
  offsetlist[i] = offsetof(SENTENCE,  iso_colc); i++;
  offsetlist[i] = offsetof(SENTENCE,  iso_colcErr); i++;
  offsetlist[i] = offsetof(SENTENCE,  iso_colcGrad); i++;
  offsetlist[i] = offsetof(SENTENCE,  iso_a); i++;
  offsetlist[i] = offsetof(SENTENCE,  iso_aErr); i++;
  offsetlist[i] = offsetof(SENTENCE,  iso_aGrad); i++;
  offsetlist[i] = offsetof(SENTENCE,  iso_b); i++;
  offsetlist[i] = offsetof(SENTENCE,  iso_bErr); i++;
  offsetlist[i] = offsetof(SENTENCE,  iso_bGrad); i++;
  offsetlist[i] = offsetof(SENTENCE,  iso_phi); i++;
  offsetlist[i] = offsetof(SENTENCE,  iso_phiErr); i++;
  offsetlist[i] = offsetof(SENTENCE,  iso_phiGrad); i++;
  /*
   * Model parameters for deV and exponential models
   */
  offsetlist[i] = offsetof(SENTENCE,  r_deV); i++;
  offsetlist[i] = offsetof(SENTENCE,  r_deVErr); i++;
  offsetlist[i] = offsetof(SENTENCE,  ab_deV); i++;
  offsetlist[i] = offsetof(SENTENCE,  ab_deVErr); i++;
  offsetlist[i] = offsetof(SENTENCE,  phi_deV); i++;
  offsetlist[i] = offsetof(SENTENCE,  phi_deVErr); i++;
  offsetlist[i] = offsetof(SENTENCE,  counts_deV); i++;
  offsetlist[i] = offsetof(SENTENCE,  counts_deVErr); i++;
  offsetlist[i] = offsetof(SENTENCE,  r_exp); i++;
  offsetlist[i] = offsetof(SENTENCE,  r_expErr); i++;
  offsetlist[i] = offsetof(SENTENCE,  ab_exp); i++;
  offsetlist[i] = offsetof(SENTENCE,  ab_expErr); i++;
  offsetlist[i] = offsetof(SENTENCE,  phi_exp); i++;
  offsetlist[i] = offsetof(SENTENCE,  phi_expErr); i++;
  offsetlist[i] = offsetof(SENTENCE,  counts_exp); i++;
  offsetlist[i] = offsetof(SENTENCE,  counts_expErr); i++;

  offsetlist[i] = offsetof(SENTENCE,  counts_model); i++;
  offsetlist[i] = offsetof(SENTENCE,  counts_modelErr); i++;
  /*
   * Measures of image structure
   */
  offsetlist[i] = offsetof(SENTENCE,  texture); i++;
  /*
   * Classification information
   */
  offsetlist[i] = offsetof(SENTENCE,  star_L); i++;
  offsetlist[i] = offsetof(SENTENCE,  star_lnL); i++;
  offsetlist[i] = offsetof(SENTENCE,  exp_L); i++;
  offsetlist[i] = offsetof(SENTENCE,  exp_lnL); i++;
  offsetlist[i] = offsetof(SENTENCE,  deV_L); i++;
  offsetlist[i] = offsetof(SENTENCE,  deV_lnL); i++;
  offsetlist[i] = offsetof(SENTENCE,  fracPSF); i++;
  
  offsetlist[i] = offsetof(SENTENCE,  flags); i++;
  offsetlist[i] = offsetof(SENTENCE,  flags2); i++;
  
  offsetlist[i] = offsetof(SENTENCE,  type); i++;
  offsetlist[i] = offsetof(SENTENCE,  prob_psf); i++;
  
  /* no comments for now */
  /* comment fields */
  char **comments;
  
  comments = (char **) thCalloc(nelem, sizeof(char *));
  for (i = 0; i < nelem; i++) {
    comments[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }

  /* heap type indicators */
  THASCIIHEAPTYPE *heaptypelist;
  heaptypelist = (THASCIIHEAPTYPE *) thCalloc(nelem, sizeof(THASCIIHEAPTYPE));
  for (i = 0; i < n0; i++) {
    heaptypelist[i] = THASCIINOHEAP;
  }
  for (i = n0; i < n1; i++) {
    heaptypelist[i] = THASCIIHEAP;
  }
  
  ASCIISCHEMA *asciischema;
  asciischema = thAsciischemaNew("SENTENCE");
  thAsciischemaelemListAdd(asciischema, 
			   namelist, offsetlist, 
			   typelist, heaptypelist, 
			   comments, nelem);

  /* now add the constructor */
  asciischema->constructor = &thSentenceNew;

  return(asciischema);
  
}

ASCIISCHEMA *set_schema_objcdef() {

  char *name = "set_schema_objcdef";

  int i, nelem = 5;

  char **namelist;
  namelist = (char **) thCalloc(nelem, sizeof(char *));
  for (i = 0; i < nelem; i++) {
    namelist[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }

  i = 0;
  namelist[i] = "name"; i++;
  namelist[i] = "parent"; i++;
  namelist[i] = "phId"; i++;
  namelist[i] = "comment"; i++;
  namelist[i] = "conditions"; i++;



  /* offset definitions */
  int *offsetlist;
  offsetlist = (int *) thCalloc(nelem, sizeof(int));
  
  i = 0;
  offsetlist[i] = offsetof(OBJCDEF, name); i++;
  offsetlist[i] = offsetof(OBJCDEF, parent); i++;
  offsetlist[i] = offsetof(OBJCDEF, phId); i++;
  offsetlist[i] = offsetof(OBJCDEF, comment); i++;
  offsetlist[i] = offsetof(OBJCDEF, conditions); i++;


  /* type definition for schema  */
  THASCIITYPE *typelist;
  typelist = (THASCIITYPE *) thCalloc(nelem, sizeof(THASCIITYPE));

  i = 0;
  typelist[i] = THASCIIS; i++;
  typelist[i] = THASCIIS; i++;
  typelist[i] = THASCIII; i++;
  typelist[i] = THASCIIS; i++;
  typelist[i] = THASCIIS; i++;

  /* no comments for now */
  /* comment fields */
  char **comments;
  
  comments = (char **) thCalloc(nelem, sizeof(char *));
  for (i = 0; i < nelem; i++) {
    comments[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }
  
  THASCIIHEAPTYPE *heaptypelist;
  heaptypelist = (THASCIIHEAPTYPE *) thCalloc(nelem, sizeof(THASCIIHEAPTYPE));
  for (i = 0; i < nelem; i++) {
    heaptypelist[i] = THASCIIHEAP;
  }
  heaptypelist[0] = THASCIINOHEAP; /* name */
  heaptypelist[3] = THASCIINOHEAP; /* comments */

  ASCIISCHEMA *asciischema;
  asciischema = thAsciischemaNew("OBJCDEF");
  thAsciischemaelemListAdd(asciischema, 
			   namelist, offsetlist, 
			   typelist, heaptypelist, 
			   comments, nelem);
  
  /* now add the constructor */
  
  asciischema->constructor = &thObjcdefNew;

  return(asciischema);
  
}

ASCIISCHEMA *set_schema_maskdef() {

  char *name = "set_schema_objcdef";

  int i, nelem = 5;

  char **namelist;
  namelist = (char **) thCalloc(nelem, sizeof(char *));
  for (i = 0; i < nelem; i++) {
    namelist[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }

  i = 0;
  namelist[i] = "objctype"; i++; 
  namelist[i] = "phMaskbit"; i++; 
  namelist[i] = "phFlag"; i++;  
  namelist[i] = "unit"; i++; 
  namelist[i] = "arithmetic"; i++; 

  /* type definition for schema  */
  THASCIITYPE *typelist;
  typelist = (THASCIITYPE *) thCalloc(nelem, sizeof(THASCIITYPE));
  
  i = 0;
  typelist[i] = THASCIIS; i++; 
  typelist[i] = THASCIII; i++;
  typelist[i] = THASCIIS; i++;
  typelist[i] = THASCIIS; i++; 
  typelist[i] = THASCIIS; i++;
  
  /* offset definitions */
  int *offsetlist;
  offsetlist = (int *) thCalloc(nelem, sizeof(int));

  i = 0;
  offsetlist[i] = offsetof(MASKDEF, objctype); i++; 
  offsetlist[i] = offsetof(MASKDEF, phMaskbit); i++; 
  offsetlist[i] = offsetof(MASKDEF, phFlag); i++; 
  offsetlist[i] = offsetof(MASKDEF, unit); i++; 
  offsetlist[i] = offsetof(MASKDEF, arithmetic); i++; 

  /* no comments for now */
  /* comment fields */
  char **comments;
  comments = (char **) thCalloc(nelem, sizeof(char *));
  for (i = 0; i < nelem; i++) {
    comments[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }
  

  THASCIIHEAPTYPE *heaptypelist;
  heaptypelist = (THASCIIHEAPTYPE *) thCalloc(nelem, sizeof(THASCIIHEAPTYPE));
  heaptypelist[0] = THASCIINOHEAP;
  for (i = 1; i < nelem; i++) {
    heaptypelist[i] = THASCIINOHEAP;
  }

  ASCIISCHEMA *asciischema;
  asciischema = thAsciischemaNew("MASKDEF");
  thAsciischemaelemListAdd(asciischema, 
			   namelist, offsetlist, 
			   typelist, heaptypelist,
			   comments, nelem);
  
  /* now add the constructor */
  
  asciischema->constructor = &thMaskdefNew;
  
  return(asciischema);
  
}
