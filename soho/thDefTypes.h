#ifndef THDEFTYPES_H
#define THDEFTYPES_H

#include "string.h"

#include "dervish.h"                                         
#include "phSpanUtil.h"        
#include "phObjc.h"
                                  
#include "thConsts.h"   
#include "thObjcTypes.h"                                                    

typedef enum thOperator {
  GTOPERATOR, GEOPERATOR, 
  LTOPERATOR, LEOPERATOR, 
  EQOPERATOR, NEQOPERATOR, 
  UNKNOWNOPERATOR
} THOPERATOR;

typedef struct sentence {
  char *thId;
  char *comment;
  /* allowed operators are GT, GE, LT, LE, EQ */
  char *operator;
  /* tolerance is used only when operator is set to EQ */
  THPIX tolerance;
  /* the central value of test */
  THPIX value;


  /* properties: */
  /* what follows are the innars of OBJC_IO structure  */

  int id;				/* id number for this objc */
  int parent;			        /* id of parent for deblends */
  const int ncolor;			/* number of colours */
  int nchild;				/* number of children */
  OBJ_TYPE objc_type;			/* overall classification */
  float objc_prob_psf;			/* Bayesian probability of being PSF */
  int catID;			        /* catalog id number */
  int objc_flags;			/* flags from OBJC */
  int objc_flags2;			/* flags2 from OBJC */
  float objc_rowc, objc_rowcErr;	/* row position and error of centre */
  float objc_colc, objc_colcErr;	/* column position and error */
  float rowv, rowvErr;			/* row velocity, in pixels/frame (NS)*/
  float colv, colvErr;			/* col velocity, in pixels/frame (NS)*/
  
  ATLAS_IMAGE *aimage;			/* the atlas image */
  struct test_info *test;		/* information for testers */
  /*
   * Unpacked OBJECT1s
   */
  float rowc[NCOLOR], rowcErr[NCOLOR];
  float colc[NCOLOR], colcErr[NCOLOR];
  float sky[NCOLOR], skyErr[NCOLOR];
  /*
   * PSF and aperture fits/magnitudes
   */
  float psfCounts[NCOLOR], psfCountsErr[NCOLOR];
  float fiberCounts[NCOLOR], fiberCountsErr[NCOLOR];
  float petroCounts[NCOLOR], petroCountsErr[NCOLOR];
  float petroRad[NCOLOR], petroRadErr[NCOLOR];
  float petroR50[NCOLOR], petroR50Err[NCOLOR];
  float petroR90[NCOLOR], petroR90Err[NCOLOR];
  /*
   * Shape of object
   */
  float Q[NCOLOR], QErr[NCOLOR], U[NCOLOR], UErr[NCOLOR];
  
  float M_e1[NCOLOR], M_e2[NCOLOR];
  float M_e1e1Err[NCOLOR], M_e1e2Err[NCOLOR], M_e2e2Err[NCOLOR];
  float M_rr_cc[NCOLOR], M_rr_ccErr[NCOLOR];
  float M_cr4[NCOLOR];
  float M_e1_psf[NCOLOR], M_e2_psf[NCOLOR];
  float M_rr_cc_psf[NCOLOR];
  float M_cr4_psf[NCOLOR];
  /*
   * Properties of a certain isophote.
   */
  float iso_rowc[NCOLOR], iso_rowcErr[NCOLOR], iso_rowcGrad[NCOLOR];
  float iso_colc[NCOLOR], iso_colcErr[NCOLOR], iso_colcGrad[NCOLOR];
  float iso_a[NCOLOR], iso_aErr[NCOLOR], iso_aGrad[NCOLOR];
  float iso_b[NCOLOR], iso_bErr[NCOLOR], iso_bGrad[NCOLOR];
  float iso_phi[NCOLOR], iso_phiErr[NCOLOR], iso_phiGrad[NCOLOR];
  /*
   * Model parameters for deV and exponential models
   */
  float r_deV[NCOLOR], r_deVErr[NCOLOR];
  float ab_deV[NCOLOR], ab_deVErr[NCOLOR];
  float phi_deV[NCOLOR], phi_deVErr[NCOLOR];
  float counts_deV[NCOLOR], counts_deVErr[NCOLOR];
  float r_exp[NCOLOR], r_expErr[NCOLOR];
  float ab_exp[NCOLOR], ab_expErr[NCOLOR];
  float phi_exp[NCOLOR], phi_expErr[NCOLOR];
  float counts_exp[NCOLOR], counts_expErr[NCOLOR];
  
  float counts_model[NCOLOR], counts_modelErr[NCOLOR];
  /*
   * Measures of image structure
   */
  float texture[NCOLOR];
  /*
   * Classification information
   */
  float star_L[NCOLOR], star_lnL[NCOLOR];
  float exp_L[NCOLOR], exp_lnL[NCOLOR];
  float deV_L[NCOLOR], deV_lnL[NCOLOR];
  float fracPSF[NCOLOR];
  
  int flags[NCOLOR];
  int flags2[NCOLOR];
  
  OBJ_TYPE type[NCOLOR];
  float prob_psf[NCOLOR];
  /*
   * Profile and extent of object
   */
  int nprof[NCOLOR];
  /* 
     float profMean[NCOLOR][NANN];
     float profErr[NCOLOR][NANN];
  */  

} SENTENCE;

typedef struct  ObjcDef {
   /* this is the type name for this definition */
  char *name; 
   /* this is the array of possible parents, can be a PHOTO object type */
  char **parent; 
  /* the list of PHOTO id for the object - makes the definitions frame specific */
  int phId;
  /* the comment by the operator when defining this object type */
  char *comment;
  /* the logical sentences used to define this type */
  char **conditions;

  SENTENCE **sentences;

} OBJCDEF;

typedef struct MaskDef {
  /* the list of object types to which this masking option applies */
  char **objctype;
  /* the following is useful for pixel which are not flagged as part of an object
     but otherwise flagged as bad, for example cosmic rays -- this option enables us
     to expand and define such regions.
     default value should be a field with all bits set. 
  */
  int phMaskbit;
  /* the following option can be as follows:
     0: DEFAULT -- keep the sdssmask and generate a mask of this size in addition to it
     1: while keeping the sdssmask, add some pixels to the SDSS mask, the units can be arcsrc or pixel
     2: replace the SDSS mask with a mask of this size
  */
  char *phFlag;
  /* possible values for units:
     0: in PIXELS
     1: in ARCSEC
     2: in ARCMIN
     3: in DEG
  */
  char *unit;
  /* the arithematic senetnce to calculate the number invoked above */
  char *arithmetic;

  /* link to the above sentence */
  SENTENCE *sen;
} MASKDEF;


OBJCDEF *thObjcdefNew();
void thObjcdefDel(OBJCDEF *def);

MASKDEF *thMaskdefNew ();
void thMaskdefDel (MASKDEF* def);

SENTENCE *thSentenceNew ();
void thSentenceDel(SENTENCE *sen);


#endif
