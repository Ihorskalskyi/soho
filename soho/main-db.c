/*
 * Read fpC images (infile), and copy its content to Outfile.
 */
/* PHOTO libraries */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ftcl.h"
#include "dervish.h"
#include "strings.h"
#include <stdarg.h>

#include "phUtils.h"

#include "phPhotoFitsIO.h"

#include "thProc.h"
#include "thMath.h"
#include "thDebug.h"
#include "thIo.h"
#include "thPartableIo.h"
#include "thFuncmodelIo.h"
#include "thFuncLoad.h"
#include "thQq.h"
#include "thFMDBGen.h"
#include "thTestFunc.h"

int verbose = 0;

static int nrow = 1489;
static int ncol = 2048; 

static void usage(void);


int
main(int ac, char *av[])
{

  char *name = "gen-db";
  /* Arguments */
  char *fname;		/* input and output filenames */

  while(ac > 1 && (av[1][0] == '-' || av[1][0] == '+')) {
    switch (av[1][1]) {
    case '?':
    case 'h':
      usage();
      exit(0);
      break;
    case 'v':
      verbose++;
      break;
    default:
      shError("Unknown option %s\n",av[1]);
      break;
    }
    ac--;
    av++;
  }
  if(ac <= 0) {
    shError("You must specify the database generation filename \n");
    exit(1);
  }
  fname = av[1]; 
 
  if (ac > 1) {
    	nrow = atoi(av[2]);
	if (ac > 2) {	
    		ncol = atoi(av[3]);
	}
  }

  thInitIo();
  thFuncLoadInit();
  thFuncLoadAddEntry("sinxcosy", &sinxcosy);
  thFuncLoadAddEntry("sinxsiny", &sinxsiny);
  thFuncLoadAddEntry("deV", &deV);
  deVprofile(NULL, NULL);
  SersicProfile(NULL, NULL);
  BsplineXTcheb(NULL, NULL);  
  
  InitQuad();

  thQqInit(&thParlistNewFromPartable, &thParlistDel, 
	   &thParlistGetNPardef, &thParlistGetPardefByPos,  
	   &thPardefGetName, &thParlistGetPardefByName, 
	   &thPardefCopy, &thValuePutInPardef, &thPardefValuePutInChar);


  FMDBGenInit(&thCountFuncInFmfits, &thFmfitsWriteFuncmodel, 
	&thFmfitsAddNewPartable, &TH_SUCCESS,  /* header uploader and sealer */
	&thGetParlistFromPartable, &thGetRowMaxFromPartable);

  FUNCMODEL *wf;
  wf = thFuncmodelNew();
  wf->nrow = nrow; 
  wf->ncol = ncol;
  
  DBFILE *file;
  CHAIN *pt_chain = NULL;
  char *ofile;
  ofile = thCalloc(SIZE, sizeof(char));
	
  file = OpenDBFile(fname, "r", NULL);
  
  SCHEMA *s;
  void *wq;
  int i, n = 0, l = 1;
  FMFITSFILE *fmfits;
  PARTABLE *pt;
  RET_CODE status, (*f)(void *, void *);

  while (file->status != DB_EOF) {

  pt_chain = ReadPartableChainDB(file, ofile, &status);
  if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not properly read the %dth DB table", name, l);
  } else {

  n = 0;  
  if (pt_chain != NULL) n = shChainSize(pt_chain);
	
  fmfits = thFmfitsOpen(ofile, 1, NULL, NULL);
  for (i = 0; i < n; i++) {
	pt = shChainElementGetByPos(pt_chain, i);
	s = shSchemaGetFromType(shTypeGetFromName(pt->tablename));
        wq = s->construct();
	f = thFuncPtrGetFromName(pt->funcname);
	thDBGen(fmfits, f, wq, wf, pt->funcname, pt, pt->tablename);
	s->destruct(wq);
	}
  thFmfitsClose(fmfits);
  thFmfitsfileDel(fmfits);
  if (pt_chain != NULL) shChainDestroy(pt_chain, &thPartableDestroy);
  /* now showing the error stack to ensure that everything went fine */
  
  }
  l++;
  }
  thErrShowStack();

  /* successfull run */
  return(0);
}

/*****************************************************************************/

static void
usage(void)
{
   char **line;

   static char *msg[] = {
      "Usage: gen-db [options] db-file nrow ncol",
      "Your options are:",
      "       -?      This message",
      "       -h      This message",
      "       -i      Print an ID string and exit",
      "       -v      Turn up verbosity (repeat flag for more chatter)",
      NULL,
   };

   for(line = msg;*line != NULL;line++) {
      fprintf(stderr,"%s\n",*line);
   }
}

