#ifndef THFUNCMODELIO_H
#define THFUNCMODELIO_H

#include <string.h>
#include "phPhotoFitsIO.h" 

#include "thReg.h"
#include "thFuncmodelIoTypes.h"

/* 
1. gives the row number in the FITS hdr which is used to tabulate the pixel values of the function model
	for a specific parlist
2.  given the row number in the fits file header, returns the value of all of the parameters used to describe
	the related model 
3. given the row number, returns the value of a specific parameter */

int thGetIndexFromPardef(PARELEM *pe, PARDEF *pd, RET_CODE *status);
int thGetFitsRowFromParlist (PARTABLE *partable, PARLIST *parlist, RET_CODE *status);
RET_CODE thGetParlistFromPartable (PARTABLE *partable, int row, PARLIST *parlist);
RET_CODE thGetPardefFromPartable (PARTABLE *partable, int row, PARDEF *pardef);

/* reading and writing FMHDU chain */

RET_CODE thFmhduReadFits(PHFITSFILE *fits, CHAIN *fmhdu);
RET_CODE thFmhduWriteFits(PHFITSFILE *fits, CHAIN *fmhdu);

/* reading and writing the partable */
RET_CODE thPartableReadFits(PHFITSFILE *fits, PARTABLE *pt);
RET_CODE thPartableWriteFits(PHFITSFILE *fits, PARTABLE *pt);

/* the following are the readers of function models. one can manually read the models, the extra job these handlers do is to displace the image by row0, col0, and fit it back to CCD characetristics. */

RET_CODE thImageReadFitsFromRow(PHFITSFILE *fits, REGION *reg, int fitsrow, int *wfitsrow);
/* the following only handles the first component */
RET_CODE thImageReadFitsFromParlist(PHFITSFILE *fits, REGION *reg, PARTABLE *partable, PARLIST *parlist, int *wfitsrow);
/* FMHDU has the information on the function components (used to differentiate between direction space representation, RE of FFT, and IM of FFT of function */
RET_CODE thFuncmodelReadFits(PHFITSFILE *fits, PARTABLE *partable, FUNCMODEL *funcmodel, FMHDU *fmhdu, int *wfitsrow);

/* the following are the writers of the function models */

RET_CODE thImageWriteFitsInRow(PHFITSFILE *fits, REGION *reg, int *wfitsrow);
RET_CODE thFuncmodelWriteFits(PHFITSFILE *fits, FUNCMODEL *funcmodel, int *wfitsrow);

/*
RET_CODE thImageWriteFitsInParlist(PHFITSFILE *fits, PARTABLE *partable, PARLIST *parlist, REGION *reg);
RET_CODE thFuncmodelWriteFits(PHFITSFILE *fits, PARTABLE *partable, FUNCMODEL *funcmodel, FMHDU *fmhdu, int *wfitsrow);
*/
/* working with extended FITS file structure 
   FMFITSFILE 
*/

FMFITSFILE *thFmfitsOpen(char *filename, int mode, HDR *hdr, RET_CODE *status);

REGION *thFmfitsReadImageFromRow(FMFITSFILE *fmfits, int fitsrow, RET_CODE *status);
RET_CODE thFmfitsReadFuncmodel(FMFITSFILE *fmfits, FUNCMODEL *fm);
/* these should not be supported 
FUNCMODEL *thFmfitsReadFuncmodelFromRow(FMFITSFILE *fmfits, int fitsrow, RET_CODE *status);
REGION *thFmfitsReadImage(FMFITSFILE *fmfits, char *fname, PARLIST *pl, RET_CODE *status);
*/

RET_CODE thFmfitsWriteImageToRow(FMFITSFILE *fmfits, REGION *reg);
RET_CODE thFmfitsWriteFuncmodel(FMFITSFILE *fmfits, FUNCMODEL *fm);

/* The following are not supported - only used for APPEND mode 
ET_CODE thFmfitsWriteFuncmodelToRow(FMFITSFILE *fmfits, int fitsrow, FUNCMODEL *fm);
RET_CODE thFmfitsWriteImage(FMFITSFILE *fmfits, char *fname, PARLIST *pl, REGION *reg);
*/

RET_CODE thFmfitsAddPartable(FMFITSFILE *fmfits, PARTABLE *pt);
RET_CODE thFmfitsClose(FMFITSFILE *fmfits);

/* 
   seeing if a given set of parameters is accessible within 
   a specific FMFITSFILE
 
   PARSTATUS is a structure which contains the information on the 
   accessibility as well the closest neighbors to _real_ parameters
*/

PARSTATUS *thFmfitsGetStatus(FMFITSFILE *fmfits, char *fname, PARLIST *pl, RET_CODE *status);

int *thOrderFmhdu(CHAIN *fmhdu_chain, RET_CODE *status);

RET_CODE thChainReorder(CHAIN *c, int *order);

int thLocateFuncInFmhdu(CHAIN *fmhdus, char *fname, RET_CODE *status);
int thCountFuncInFmfits(FMFITSFILE *fmfits, char *fname);

RET_CODE thFmfitsAddNewPartable(FMFITSFILE *fmfits, PARTABLE *pt, FUNCMODEL *wf);
RET_CODE TH_SUCCESS (void *a, void *b, int c);

int thGetRowMaxFromPartable(PARTABLE *pt);
PARLIST *thParlistNewFromPartable(PARTABLE *pt);
int thParlistGetNPardef(PARLIST *pl);
PARDEF *thParlistGetPardefByPos(PARLIST *pl, int pos);
char *thPardefGetName(PARDEF *pd);
PARDEF *thParlistGetPardefByName(PARLIST *pl, char *pname);
void thPardefCopy(PARDEF *pd1, PARDEF *pd2);
void thValuePutInPardef(void *q, char *type, PARDEF *pd);
void thPardefValuePutInChar(PARDEF *pd, char *cval);
FMHDU *thFmhduNewFromPartable(PARTABLE *pt, CHAIN *fmhdus, int fmhdu_loc, int image_loc, RET_CODE *status);	

/* utils for handling extended region type */
RET_CODE thRegTransExtreg(REGION *reg, EXTREGION  *extreg);

#endif
