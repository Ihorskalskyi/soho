

	for (( imodel1=0; imodel1 <$nmodel; imodel1++ ))
	do
	modelname1=${model_a[$imodel1]}

	for (( imodel2=0; imodel2 <$nmodel; imodel2++ ))
	do
	modelname2=${model_a[$imodel2]}

	ntotal=$ntotal0
	nall=$nall0
	ifield=$ifield0
	for (( isky=0; isky<$nsky; isky++ ))
	do
	sky1=${sky_a[$isky]}
	sky2=${sky_a[$isky]}
	modelskystring="-sky '-s00 $sky1 -z00 $sky2'"

	for (( icounts1=0; icounts1<$ncounts1; icounts1++ ))
	do
	counts1=${counts1_a[$icounts1]}

	for (( ire1=0; ire1<$nre; ire1++ ))
	do
	re1=${re_a[$ire1]}

	for (( ie1=0; ie1<$ne; ie1++ ))
	do
	e1=${e_a[$ie1]}

	for (( iphi1=0; iphi1<$nphi; iphi1++ ))
	do
	phi1=${phi_a[$iphi1]}
	modelstring1="-$modelname1 '-counts $counts1 -xc $xc -yc $yc -re $re1 -e $e1 -phi $phi1'"

	for (( icounts21=0; icounts21<$ncounts21_ratio; icounts21++ ))
#	for (( icounts2=0; icounts2<$ncounts2; icounts2++ ))
	do
	counts21=${counts21_ratio_a[$icounts21]}
	counts2=$(perl -e "printf "%g", $counts1*$counts21")
#	counts2=${counts1_a[$icounts2]}

	#counts2=`echo "$counts1*$counts21" | bc -l`
	#echo "counts1= $counts1, counts21 = $counts21, counts2 = $counts2"

	for (( ire2=0; ire2<$nre; ire2++ ))
	do
	re2=${re_a[$ire2]}

	for (( ie2=0; ie2<$ne; ie2++ ))
	do
	e2=${e_a[$ie2]}

	for (( iphi2=0; iphi2<$nphi; iphi2++ ))
	do
	phi2=${phi_a[$iphi2]}
	modelstring2="-$modelname2 '-counts $counts2 -xc $xc -yc $yc -re $re2 -e $e2 -phi $phi2'"
	lumcondition=$(echo "$counts2 > $COUNTS2_CUT" | bc) 
	if [ $lumcondition -ne 0 ]; then
	randport=$(python -S -c "import random; print random.randrange(1,$ntotal)")
	else
	randport=$(perl -e "printf "%d", $nall+1")
	fi
	#let ntotal--
	ntotal=$(perl -e "printf "%d", $ntotal-$RANDOM_SCALE")
	if [ $randport -le $nall ]; then

		#designing the file names #
		field=`printf "%04d" $ifield`
		dir="$root/$modelname1-$modelname2/$f1-$f2"
		framefile="$dir/ff-$runstr-$field-$band$camcol.par"
		condor_outputfile="$logdir/single-galaxy.sim.$f1-$f2-$runstr-$field-$band$camcol.log"
		condor_submitfile="$condordir/condor-$runstr-$field-$band$camcol.submit"
		condor_errorfile="$condordir/condor-$runstr-$field-$band$camcol.error"
		condor_logfile="$condordir/condor-$runstr-$field-$band$camcol.log"
		condor_argument="\"-band $band -camcol $camcol -framefile $framefile -noobjcfile  $modelstring1 $modelstring2 $modelskystring\""	
		condor_comment="date: $DATE, time: $TIME"
	
		write_parfile --root=$dir --band=$band --camcol=$camcol --run=$run --field=$field --field0=$field0 --output=$framefile

		write_condorjob --restrict --submitfile=$condor_submitfile --errorfile=$condor_errorfile --logfile=$condor_logfile --outputfile=$condor_outputfile --executable="$executable" --arguments="$condor_argument" --comment="$condor_comment"	

		let ifield++
		#let nall--
		nall=$(perl -e "printf "%d", $nall-$RANDOM_SCALE")
		let nproc++
		if [ $verbose == yes ]; then

			echo "band=$band camcol=$camcol run=$run field=$field psf=$f1"
			echo "model1: $modelstring1"
			echo "model2: $modelstring2"
			echo "sky:    $modelskystring"
			echo "condor: $condor_submitfile"

		else 

			echo -n "."

		fi

		echo "$run, $camcol, $field, $sky1, $sky2, $modelname1, $counts1, $re1, $e1, $phi1, $modelname2, $counts2, $re2, $e2, $phi2, $dir" >> $LOGFILE

		if [ $debug == yes ]; then 
			echo "condor: $condor_submitfile"
		else
			let njob++
			eval "condor_submit $condor_submitfile >> $tempfile"
		fi

	fi

	done
	done
	done
	done
	done

	done
	done 
	done
	done
	done
	done
	
