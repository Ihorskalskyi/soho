/*
 * Read fpC images (infile), and copy its content to Outfile.
 */
/* PHOTO libraries */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dervish.h"
#include "phFits.h"
#include "phConsts.h"
/* SOHO modules and libraries */
#include "fitsio.h"
#include "sohoTypes.h"
#include "sohoEnv.h"
/* general C libraries added for SOHO */
#include "strings.h"

#define TfpC U16
/*
 * some symbols to prevent dervish.o from being loaded from libatlas.a
 * if we are also linking against real dervish
 */

int verbose = 0;

static void usage(void);
int *thFitsioError(int *status);
int *read_fpc(fitsfile *infptr, 
	      const long row0,  const long column0,  
	      const long row1,  const long column1, 
	      char **header, TfpC *fpciArr);
int *write_fpc(fitsfile *outfptr, 
	       char **header, TfpC *fpciArr);
int *cp_fpc(char *infile, char *outfile, /* input and output filenames */
	    const long row0, const long column0, 
	    const long row1, const long column1 /* origin of image in INFILE region */);

int
main(int ac, char *av[])
{
  /* from MAIN in READ_ATLAS */
  /* not used */
  int bkgd = SOFT_BIAS;		        /* desired background level */
  int color = 0;			/* desired color */
  FITS *fits;				/* the table in question */
  REGION *reg;				/* region to write */
  int row, column;			/* desired row and column*/


  /* Arguments */
  char *infile, *outfile;		/* input and output filenames */
  long row0, column0, row1, column1;	/* origin of image in region */
   
  while(ac > 1 && (av[1][0] == '-' || av[1][0] == '+')) {
    switch (av[1][1]) {
    case '?':
    case 'h':
      usage();
      exit(0);
      break;
    case 'v':
      verbose++;
      break;
    default:
      shError("Unknown option %s\n",av[1]);
      break;
    }
    ac--;
    av++;
  }
  if(ac <= 6) {
    shError("You must specify an input file, an output file, a row, and a column \n");
    exit(1);
  }
  infile = av[1]; outfile = av[2]; 
  row0 = atoi(av[3]); column0 = atoi(av[4]); 
  row1 = atoi(av[5]); column1 = atoi(av[6]);

  /*
   * dummy calls to pull .o files out of the real libdervish.a if we are
   * linking against it
   */
  (void)thTypeGetFromName("RHL");
  
  if (cp_fpc(infile, outfile, row0, column0, row1, column1) != NULL) {
    exit(1);
  }
  
  /* successfull run */
  return(0);
}

/*****************************************************************************/

static void
usage(void)
{
   char **line;

   static char *msg[] = {
      "Usage: read_atlas_image [options] input-file output-file r0 c0 r1 c1",
      "Your options are:",
      "       -?      This message",
      "       -b #    Set background level to #",
      "       -c #    Use colour # (0..ncolor-1; default 0)",
      "       -h      This message",
      "       -i      Print an ID string and exit",
      "       -v      Turn up verbosity (repeat flag for more chatter)",
      NULL,
   };

   for(line = msg;*line != NULL;line++) {
      fprintf(stderr,"%s\n",*line);
   }
}

/*****************************************************************************/

int *read_fpc(fitsfile *infptr,  
	      const long row0,  const long column0,  
	      const long row1,  const long column1, 
	      char **header, TfpC *fpciArr)
{
  int *status, *anynul, *nkeys, nexc;  /* CFITSIO io status*/
  int hdunum, *hdutype;                /* CFITSIO hdutype */
  const int nocomments = 1;            /* COMMENTS are read into header */
  char *card, **exclist;               /* CFITSIO fits header string */
  long fpixel[2], lpixel[2];           /* corner pixels for the IMAGE portion taken */
  TfpC datatype;                       /* CFITSIO datatype for IMAGE*/
  TfpC *nulval;                        /* Blank Pixels Values - If set to 0 No Check Is Done */

  /*
   * read the header
   */
  
  hdutype = NULL;
  hdunum  = 0;
  (int)fits_movabs_hdu(infptr, hdunum, hdutype, status);
  if (thFitsioError(status) != NULL) {
    exit(1);
  }
 
  (exclist) = NULL;
  /* nocomments = 0; */
  (int)fits_hdr2str(infptr, nocomments, exclist, nexc,
		    header, nkeys, status);
  if (thFitsioError(status) != NULL) {
    exit(1);
  }
 
  /*
   * number of keywords in the header, and the first line of the header
   */
  
  fprintf(NULL, "Number of Keywords = %d\n", *nkeys);
  fprintf(NULL, "First line of Header: \n%a\n ", (*header)[0]); 
  
  /*
   * read fpc image
   */
  
  hdutype = NULL;
  hdunum  = 1;
  (int)fits_movabs_hdu(infptr, hdunum, hdutype, status);
  if (thFitsioError(status) != NULL) {
    exit(1);
  }
  
  /*
   * datatype = TBYTE, TSBYTE, TSHORT, TUSHORT, TINT, TUINT, TLONG, TLONGLONG, TULONG, TFLOAT, TDOUBLE
   */
  
  /*
   * corner pixels
   */
  fpixel[0] = itol(row0); fpixel[1] = itol(column0);
  lpixel[0] = itol(row1); lpixel[1] = itol(column1); 
  nulval = NULL; /* no checks done on image to find bad pixels */
  (int)fits_read_subset(infptr, datatype, fpixel, lpixel, NULL,
			nulval, fpciArr, anynul, status);
  if (thFitsioError(status) != NULL) {
    exit(1);
  } 
  /* successful run */
  return(NULL);

}



int *write_fpc(fitsfile *outfptr, 
	       char **header, TfpC *fpciArr)
{
  int *status, *anynul, *nkeys, nexc;  /* CFITSIO io status*/
  int hdunum, *hdutype;                /* CFITSIO hdutype */
  const int nocomments = 1;            /* COMMENTS are read into header */
  char *card, **exclist;     /* CFITSIO fits header string */
  TfpC datatype;                       /* CFITSIO datatype for IMAGE*/
  TfpC *nulval;                        /* Blank Pixels Values - If set to 0 No Check Is Done */
  
  /* Write the Header */
  
  hdutype = NULL;
  hdunum  = 0;
  (int)fits_movabs_hdu(outfptr, hdunum, hdutype, status);
  if (thFitsioError(status) != NULL) {
    exit(1);
  }
 
  card = *header;
  while (card != NULL) {   
    (int)fits_write_record(outfptr, card, status);
    if (thFitsioError(status) != NULL) {
      exit(1);
    }
    card++;
  }
  
  /* Write the Image */
  
  hdutype = NULL;
  hdunum  = 1;
  (int)fits_movabs_hdu(outfptr, hdunum, hdutype, status);
  if (thFitsioError(status) != NULL) {
    exit(1);
  }
 (int)fits_write_subset(outfptr, datatype, NULL, NULL,
			fpciArr, status);
 if (thFitsioError(status) != NULL) {
   exit(1);
 }
 
  /* successfull run */
  return(NULL);
}
  
int *cp_fpc(char *infile, char *outfile, /* input and output filenames */
	    const long row0, const long column0, const long row1, const long column1 /* origin of image in INFILE region */)
{
  /* Variables */
  fitsfile *infptr, *outfptr, **inffptr, **outffptr;      /* CFITSIO fits file pointer*/
  int *status;                                            /* CFITSIO io status*/
  char **header;                                          /*CFITSIO header */
  TfpC *fpciArr;                                          /* Array containing the IMAGE read by CFITSIO; MAXIMUM size given*/

  /*
   * open INFILE, OUTFILE
   */
  
  status = NULL;
  (int)fits_open_file(inffptr, infile, 0, status);
  if (thFitsioError(status) != NULL) {
    exit(1);
  }  

  status = NULL;
  (int)fits_create_file(outffptr, outfile, status);
  if (thFitsioError(status) != NULL) {
    exit(1);
  }
 
  infptr = *inffptr; outfptr = *outffptr;
  /* read INFILE and write onto OUTFILE  */
  if (read_fpc(infptr, row0, column0, row1, column1, header, fpciArr) != NULL) {
    exit(1);
  }
  if (write_fpc(outfptr, header, fpciArr) != NULL) {
    exit(1);
  }
  
  /*
   * close infile, outfile
   */
  status = NULL;
  (int) fits_close_file(infptr, status);
  if (thFitsioError(status) != NULL) {
    exit(1);
  }
 
  status = NULL;
  (int) fits_close_file(outfptr, status);
  if (thFitsioError(status) != NULL) {
    exit(1);
  }
  
 
  return(NULL);
}
