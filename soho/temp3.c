else if (demand == 81 || demand == 91 || demand == 86 || demand == 96) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {2.5E5, 2.5E5, 2.5E5, 2.5E5, 2.5E5}; /* 10th percentile */
	float r_deV[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; /* a little bit less than the third quartile */
	float r_deVErr[NCOLOR] = {0.7, 0.7, 0.7, 0.7, 0.7}; /* a little bit less than the third quartile */
	float counts_exp[NCOLOR] = {2.5E5, 2.5E5, 2.5E5, 2.5E5, 2.5E5}; /* 10th percentile */
	float r_exp[NCOLOR] = {6.0, 6.0, 6.0, 6.0, 6.0}; /* a little bit less than the third quartile */	
	float r_expErr[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; /* a little bit less than the third quartile */	
	/* 
	float n_exp[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; 
	float n_expErr[NCOLOR] = {0.2, 0.2, 0.2, 0.2, 0.2};
 	*/

	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0;
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;

		if ((demand % 10) >  5) {
			#if USE_FNAL_OBJC_IO
			objc->counts_exp[i] = (float) counts_exp[i];
			objc->r_exp[i] = (float) r_exp[i];	
			objc->r_expErr[i] = (float) r_expErr[i];	
			objc->ab_exp[i] = (float) 1.0;
		
			objc->counts_pl[i] = (float) 0.0;
			objc->r_pl[i] = (float) VALUE_IS_BAD;
			objc->ab_pl[i] = (float) 1.0;
			objc->n_pl[i] = (float) VALUE_IS_BAD;
			objc->n_plErr[i] = (float) VALUE_IS_BAD;


			objc->counts_exp2[i] = (float) 0.0;
			objc->r_exp2[i] = (float) VALUE_IS_BAD;
			objc->ab_exp2[i] = (float) 1.0;
			objc->n_exp2[i] = (float) VALUE_IS_BAD;
			objc->n_exp2Err[i] = (float) VALUE_IS_BAD;

			#else
			objc->counts_exp[i] = (float) counts_exp[i];
			objc->r_exp[i] = (float) r_exp[i];
			objc->r_expErr[i] = (float) r_expErr[i];
			objc->ab_exp[i] = (float) 1.0;
			#endif
		} else {
			#if USE_FNAL_OBJC_IO
			objc->counts_exp[i] = (float) 0.0;
			objc->r_exp[i] = (float) VALUE_IS_BAD;	
			objc->r_expErr[i] = (float) 0.0;	
			objc->ab_exp[i] = (float) 1.0;
			objc->counts_pl[i] = (float) 0.0;
			objc->r_pl[i] = (float) VALUE_IS_BAD;
			objc->ab_pl[i] = (float) 1.0;
			objc->n_pl[i] = (float) VALUE_IS_BAD;
			objc->n_plErr[i] = (float) VALUE_IS_BAD;
			objc->counts_exp2[i] = (float) 0.0;
			objc->r_exp2[i] = (float) VALUE_IS_BAD;
			objc->ab_exp2[i] = (float) 1.0;
			objc->n_exp2[i] = (float) VALUE_IS_BAD;
			objc->n_exp2Err[i] = (float) VALUE_IS_BAD;
			#else
			objc->counts_pl[i] = (float) 0.0;
			objc->r_pl[i] = (float) VALUE_IS_BAD;
			objc->r_plErr[i] = (float) 0.0;
			objc->ab_pl[i] = (float) 1.0;
			#endif
		}

		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	if (demand % 10 > 5) {
		cdclass = CORESERSIC_GCLASS;
	} else {
		cdclass = DEV_GCLASS;
	}
	if (((demand / 10)%10)%2 == 1) galaxy_psf_convolution = NO_PSF_CONVOLUTION;

} else if (demand == 82 || demand == 92 || demand == 87 || demand == 97) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {2.5E5, 2.5E5, 2.5E5, 2.5E5, 2.5E5}; /* 10th percentile */
	float r_deV[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; /* a little bit less than the third quartile */
	float r_deVErr[NCOLOR] = {0.7, 0.7, 0.7, 0.7, 0.7}; /* a little bit less than the third quartile */
	float counts_exp[NCOLOR] = {4.5E5, 4.5E5, 4.5E5, 4.5E5, 4.5E5}; /* 10th percentile */
	float r_exp[NCOLOR] = {8.0, 8.0, 8.0, 8.0, 8.0}; /* a little bit less than the third quartile */	
	float r_expErr[NCOLOR] = {2.7, 2.7, 2.7, 2.7, 2.7}; /* a little bit less than the third quartile */	
/* 
	float n_exp[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; 
	float n_expErr[NCOLOR] = {0.2, 0.2, 0.2, 0.2, 0.2};
 */
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;

		if ((demand % 10) > 5) {
			#if USE_FNAL_OBJC_IO
			objc->counts_exp[i] = (float) counts_exp[i];
			objc->r_exp[i] = (float) r_exp[i] / 1.0;	
			objc->r_expErr[i] = (float) r_expErr[i] / 1.0;	
			objc->ab_exp[i] = (float) 1.0;
			
			objc->counts_exp2[i] = (float) 0.0;
			objc->r_exp2[i] = (float) VALUE_IS_BAD;
			objc->ab_exp2[i] = (float) 1.0;
			objc->n_exp2[i] = (float) VALUE_IS_BAD
			objc->n_exp2Err[i] = (float) VALUE_IS_BAD
		
			objc->counts_pl[i] = (float) 0.0;
			objc->r_pl[i] = (float) VALUE_IS_BAD;
			objc->ab_pl[i] = (float) 1.0;
			objc->n_pl[i] = (float) VALUE_IS_BAD;
			objc->n_plErr[i] = (float) VALUE_IS_BAD;	


			#else
			objc->counts_exp[i] = (float) counts_exp[i];
			objc->r_exp[i] = (float) r_exp[i] / 1.0;
			objc->r_expErr[i] = (float) r_expErr[i] / 1.0;
			objc->ab_exp[i] = (float) 1.0;
			#endif
		} else {
			#if !USE_FNAL_OBJC_IO
			objc->counts_exp[i] = (float) 0.0;
			objc->r_exp[i] = (float) VALUE_IS_BAD;	
			objc->r_expErr[i] = (float) 0.0;	
			objc->ab_exp[i] = (float) 1.0;
			objc->n_exp[i] = n_exp[i];
			objc->n_expErr[i] = n_expErr[i];	
			objc->counts_exp2[i] = (float) 0.0;
			objc->r_exp2[i] = (float) VALUE_IS_BAD;
			objc->ab_exp2[i] = (float) 1.0;
			#else
			objc->counts_pl[i] = (float) 0.0;
			objc->r_pl[i] = (float) VALUE_IS_BAD;
			objc->r_plErr[i] = (float) 0.0;
			objc->ab_pl[i] = (float) 1.0;
			#endif
		}

		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	if (demand % 10 > 5) {
		cdclass = CORESERSIC_GCLASS;
	} else {
		cdclass = DEV_GCLASS;
	}
	if (((demand / 10)%10)%2 == 1) galaxy_psf_convolution = NO_PSF_CONVOLUTION;

} else if (demand == 83 || demand == 93 || demand == 88 || demand == 98) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {8.5E4, 8.5E4, 8.5E4, 8.5E4, 8.5E4}; /* 10th percentile */
	float r_deV[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; /* a little bit less than the third quartile */
	float r_deVErr[NCOLOR] = {0.7, 0.7, 0.7, 0.7, 0.7}; /* a little bit less than the third quartile */
	float counts_exp[NCOLOR] = {8.5E4, 8.5E4, 8.5E4, 8.5E4, 8.5E4}; /* 10th percentile */
	float r_exp[NCOLOR] = {6.0, 6.0, 6.0, 6.0, 6.0}; /* a little bit less than the third quartile */	
	float r_expErr[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; /* a little bit less than the third quartile */	
/* 
	float n_exp[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; 
	float n_expErr[NCOLOR] = {0.2, 0.2, 0.2, 0.2, 0.2};
 */
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;

		if ((demand % 10) >  5) {
			#if !USE_FNAL_OBJC_IO
			objc->counts_exp[i] = (float) counts_exp[i];
			objc->r_exp[i] = (float) r_exp[i] / 1.0;	
			objc->r_expErr[i] = (float) r_expErr[i] / 1.0;	
			objc->ab_exp[i] = (float) 1.0;
			objc->n_exp[i] = n_exp[i];
			objc->n_expErr[i] = n_expErr[i];		
			objc->counts_exp2[i] = (float) 0.0;
			objc->r_exp2[i] = (float) VALUE_IS_BAD;
				objc->ab_exp2[i] = (float) 1.0;
			#else
			objc->counts_pl[i] = (float) counts_exp[i];
			objc->r_pl[i] = (float) r_exp[i] / 1.0;
			objc->r_plErr[i] = (float) r_expErr[i] / 1.0;
			objc->ab_pl[i] = (float) 1.0;
			#endif
		} else {
			#if !USE_FNAL_OBJC_IO
			objc->counts_exp[i] = (float) 0.0;
			objc->r_exp[i] = (float) VALUE_IS_BAD;	
			objc->r_expErr[i] = (float) 0.0;	
			objc->ab_exp[i] = (float) 1.0;
			objc->n_exp[i] = n_exp[i];
			objc->n_expErr[i] = n_expErr[i];	
			objc->counts_exp2[i] = (float) 0.0;
			objc->r_exp2[i] = (float) VALUE_IS_BAD;
			objc->ab_exp2[i] = (float) 1.0;
			#else
			objc->counts_pl[i] = (float) 0.0;
			objc->r_pl[i] = (float) VALUE_IS_BAD;
			objc->r_plErr[i] = (float) 0.0;
			objc->ab_pl[i] = (float) 1.0;
			#endif
		}

		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	if (demand % 10 > 5) {
		cdclass = CORESERSIC_GCLASS;
	} else {
		cdclass = DEV_GCLASS;
	}
	if (((demand / 10)%10)%2 == 1) galaxy_psf_convolution = NO_PSF_CONVOLUTION;

} else if (demand == 84 || demand == 94 || demand == 89 || demand == 99) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {8.5E4, 8.5E4, 8.5E4, 8.5E4, 8.5E4}; /* 10th percentile */
	float r_deV[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; /* a little bit less than the third quartile */
	float r_deVErr[NCOLOR] = {0.7, 0.7, 0.7, 0.7, 0.7}; /* a little bit less than the third quartile */
	float counts_exp[NCOLOR] = {1.5E5, 1.5E5, 1.5E5, 1.5E5, 1.5E5}; /* 10th percentile */
	float r_exp[NCOLOR] = {8.0, 8.0, 8.0, 8.0, 8.0}; /* a little bit less than the third quartile */	
	float r_expErr[NCOLOR] = {2.7, 2.7, 2.7, 2.7, 2.7}; /* a little bit less than the third quartile */	
/* 
	float n_exp[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; 
	float n_expErr[NCOLOR] = {0.2, 0.2, 0.2, 0.2, 0.2};
*/
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;

		if ((demand % 10) > 5) {
			#if USE_FNAL_OBJC_IO
			objc->counts_exp[i] = (float) counts_exp[i];
			objc->r_exp[i] = (float) r_exp[i] / 1.0;	
			objc->r_expErr[i] = (float) r_expErr[i] / 1.0;	
			objc->ab_exp[i] = (float) 1.0;

			objc->counts_exp2[i] = (float) 0.0;
			objc->r_exp2[i] = (float) VALUE_IS_BAD;
			objc->ab_exp2[i] = (float) 1.0;
	
			objc->n_pl[i] = (float) VALUE_IS_BAD;
			objc->n_plErr[i] = (float) VALUE_IS_BAD;
			objc->counts_pl[i] = (float) 0.0;
			objc->r_pl[i] = (float) VALUE_IS_BAD;
			objc->ab_pl[i] = (float) 1.0;

			#else
			objc->counts_exp[i] = (float) counts_exp[i];
			objc->r_exp[i] = (float) r_exp[i] / 1.0;
			objc->r_expErr[i] = (float) r_expErr[i] / 1.0;
			objc->ab_exp[i] = (float) 1.0;
			#endif
		} else {
			#if USE_FNAL_OBJC_IO
			objc->counts_exp[i] = (float) 0.0;
			objc->r_exp[i] = (float) VALUE_IS_BAD;	
			objc->r_expErr[i] = (float) 0.0;	
			objc->ab_exp[i] = (float) 1.0;
			objc->counts_exp2[i] = (float) 0.0;
			objc->r_exp2[i] = (float) VALUE_IS_BAD;
			objc->ab_exp2[i] = (float) 1.0;
			objc->n_pl[i] = (float) VALUE_IS_BAD;
			objc->n_plErr[i] = (float) VALUE_IS_BAD;
			objc->counts_pl[i] = (float) 0.0;
			objc->r_pl[i] = (float) VALUE_IS_BAD;
			objc->ab_pl[i] = (float) 1.0;
			#else
			objc->counts_pl[i] = (float) 0.0;
			objc->r_pl[i] = (float) VALUE_IS_BAD;
			objc->r_plErr[i] = (float) 0.0;
			objc->ab_pl[i] = (float) 1.0;
			#endif
		}

		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	if (demand % 10 > 5) {
		cdclass = CORESERSIC_GCLASS;
	} else {
		cdclass = DEV_GCLASS;
	}
	if (((demand / 10)%10)%2 == 1) galaxy_psf_convolution = NO_PSF_CONVOLUTION;

} 
