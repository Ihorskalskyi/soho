#include "thResidualTypes.h"

RESIDUAL_REPORT *thResidualReportNew() {
RESIDUAL_REPORT *x;
x = thCalloc(1, sizeof(RESIDUAL_REPORT));
x->cflag = ALTERED;
return(x);
}

void thResidualReportDel(RESIDUAL_REPORT *x) {
if (x == NULL) return;
if (x->binmask != NULL) phObjmaskDel(x->binmask);
if (x->bindiff != NULL) shRegDel(x->bindiff);
thFree(x);
return;
}

RET_CODE thResidualReportPutMask(RESIDUAL_REPORT *res, OBJMASK *mask) {
char *name = "thResidualReportPutMask";
if (res == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (mask == NULL) {
	thError("%s: WARNING - replacing existing mask with a null mask", name);
}
if (res->mask != mask) {
	res->cflag = ALTERED;
	res->mask = mask;
} else {
	thError("%s: WARNING - putting the old mask in place", name);
}

return(SH_SUCCESS);
}


RET_CODE thCreateResidualReport(REGION *diff, OBJMASK *mask, int rbin, int cbin, RESIDUAL_REPORT **res) {
char *name = "thCreateResidualReport";
/* 
if (lstruct == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
*/
if (rbin <= 0 || cbin <= 0) {
	thError("%s: ERROR - unsupported binsize (rbin = %d, cbin = %d)", name, rbin, cbin);
	return(SH_GENERIC_ERROR);
}
if (res == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
RESIDUAL_REPORT *ress = thResidualReportNew();
ress->ampl = NULL; /* lstruct->ampl; */
ress->diff = diff;
ress->mask = mask;
ress->rbin = rbin;
ress->cbin = cbin;
ress->lstruct = NULL;
ress->fitid = (FITID) 0;
*res = ress;

return(SH_SUCCESS);
}

RET_CODE thCreateResidualReportChain(REGION *diff, OBJMASK *mask, int rbin1, int rbin2, int cbin1, int cbin2, CHAIN *reschain) {
char *name = "thCreateResidualReportChain";
/* 
if (lstruct == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
*/
if (reschain == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (rbin1 <= 0 || rbin2 <= 0) {
	thError("%s: ERROR - unsupported (rbin) pair (%d, %d)", name, rbin1, rbin2);
	return(SH_GENERIC_ERROR);
}
if (cbin1 <= 0 || cbin2 <= 0) {
	thError("%s: ERROR - unsupported (cbin) pair (%d, %d)", name, cbin1, cbin2);
	return(SH_GENERIC_ERROR);
}
int nchain = 0;
if ((nchain = shChainSize(reschain)) != 0) {
	thError("%s: WARNING - output chain has (%d) members, adding new reports to them", name, nchain);
}
RET_CODE status;
RESIDUAL_REPORT *res;

int rbin_i = MIN(rbin1, rbin2);
int rbin_f = MAX(rbin1, rbin2);
int cbin_i = MIN(cbin1, cbin2);
int cbin_f = MAX(cbin1, cbin2);

int rbin, cbin;
for (rbin = rbin_i; rbin <= rbin_f; rbin++) {
	for (cbin = cbin_i; cbin <= cbin_f; cbin++) {
		status = thCreateResidualReport(diff, mask, rbin, cbin, &res);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not make residular report for (rbin = %d, cbin = %d)", name, rbin, cbin);
			return(status);
		}
		shChainElementAddByPos(reschain, res, "RESIDUAL_REPORT", TAIL, AFTER);
	}
}

return(SH_SUCCESS);
}


