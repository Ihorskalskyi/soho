#include "thProfileTypes.h"

PIXPOS *thPixposNew() {
PIXPOS *pixpos;
pixpos = (PIXPOS *) thCalloc(1, sizeof(PIXPOS));
return(pixpos);
}
void thPixposDel(PIXPOS *p) {
if (p == NULL) return;
thFree(p);
return;
}
QUADRATURE *thQuadratureNew() {
QUADRATURE *q;
q = thCalloc(1, sizeof(QUADRATURE));
return(q);
}

void thQuadratureDel(QUADRATURE *q) {
if (q == NULL) return;
thFree(q);
return;
}

void thPixposSet(PIXPOS *pixpos, THPIX row, THPIX col) {
if (pixpos == NULL) return;
pixpos->row = row;
pixpos->col = col;
return;
}
void thPixposGet(PIXPOS *pixpos, THPIX *row, THPIX *col) {
if (pixpos == NULL) return;
if (row != NULL) *row = pixpos->row;
if (col != NULL) *col = pixpos->col;
}



