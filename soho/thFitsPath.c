#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dervish.h"
#include "phFits.h"
#include "phConsts.h"
#include "fitsio.h"
/* SOHO modules and libraries */
#include "sohoTypes.h"
#include "sohoEnv.h"
#include "sohoString.h"

/* 
   FORMATS for conversion of 
   RERUN, CAMCOL, RUNNUM, FIELD 
   to string 
*/
#define CAMCOLFMT %i1.1
#define RUNNUMFMT %i6.6
#define RERUNFMT %i5.5
#define FIELDFMT %i4.4

/* INTERFACEs */
void thFitsPath(const ftype *ftype, 
		const int *runnum, *camcol, *rerun, 
		char *datadir);
void thFitsName(const FType *FType1, 
		const int *runnum, *camcol, *field, *rerun, *filter, 
		const boolean nopath, 
		int *exten, *indx, char *filename);

/* The following functions are PRIVATE */

void thAstransFitsName(const char *filename, 
		  const int *field, *camcol, const char *filtername,
		  int *exten, *indx,);
void thfCpCalibFitsName(const char *filename, const int *field,
			int *exten, *indx,);
void thPsFieldFitsName(const char *filename, 
		       const int *field, *camcol, const char *filtername,
		       int *exten, *indx);
/* End of INTERFACES */

void thFitsPath(const ftype *ftype, 
		const int *runnum, *camcol, *rerun, 
		char *datadir);
{
  char *camcolstr, *runnumstr, *rerunstr;
  char *PhReduxDir, *PhRedixCalibImageDir, *PhRedixCalibObjDir, *PhSweepDir, 
    *PhCalibDir, *PhDataDir, *PhResolveDir, *PhSkyDir;
  /*if (n_params() LT 2) then $
    message, 'Syntax - datadir = sdss_path( ftype, runnum, [ camcol, rerun= ] )'
  */

  sprintf(camcolstr, 'CAMCOLFMT', camcol);
  sprintf(runnumstr, 'RUNNUMFMT', runnum);
  sprintf(rerunstr,  'RERUNFMT', rerun)

  PhReduxDir = '$PHOTO_REDUX/' + RerunStr + '/' + RunnumStr + '';
  PhReduxCalibImageDir = '$PHOTO_REDUX/calibimage/' + RerunStr + '/' + RunnumStr + '/' + CamColStr;
  PhRedixCalibObjDir   = '$PHOTO_REDUX/calibobj/' + RerunStr + '/' + RunnumStr + '/' + CamcolStr;
  PhSweepDir = '$PHOTO_SWEEP/' + RerunStr + '';
  PhCalibDir = '$PHOTO_CALIB/' + RerunStr + '/' + RunnumStr + '/nfcalib';
  PhDataDir  = '$PHOTO_DATA/' + RunnumStr + '/';
  PhResolveDir='$PHOTO_RESOLVE/' + RerunStr + '/' + RunnumStr + ''; 
  PhSkyDir   = '$PHOTO_SKY/' + RerunStr + '/' + RunnumStr + '';

   switch(ftype) 
     {
     case apObj: 
       datadir = PhReduxDir + '/objcs/' + CamcolStr; break;
     case asTrans: 
       datadir = PhReduxDir + '/astrom'; break;
     case asTranscol: 
       datadir = PhReduxDir + '/astrom/' + CamcolStr; break;
     case calibImage: 
       datadir = PhReduxCalibImageDir; break;
     case calibMatch: 
       datadir = PhReduxDir + '/nfcalib'; break;
     case calibObj: 
       datadir = PhRedixCalibObjDir; break;
     case calibObj_gal: 
       datadir = PhSweepDir; break;
     case calibObj_galmoments: 
       datadir = PhSweepDir;break;
     case calibObj_star: 
       datadir = PhSweepDir; break;
     case calibPhotom: 
       datadir = PhReduxDir + '/nfcalib'; break;
     case calibPhotomGlobal: 
       datadir = PhCalibDir; break;
     case exPhotom: 
       datadir = PhReduxDir + '/photo'; break;
     case fakeIdR: 
       datadir =  PhDataDir + '/fake_fields/' + CamcolStr; break;
     case fcPCalib: 
       datadir = PhReduxDir + '/nfcalib'; break;
     case fpAtlas, fpC, fpBin, fpFieldStat, fpM, fpObj: 
       datadir = PhReduxDir + '/objcs/' + CamcolStr; break;
     case hoggAstrom: 
       datadir = PhReduxDir + '/astrom'; break;
     case hoggAstrom_log: 
       datadir = PhReduxDir + '/logs'; break;
     case hoggBB: 
       datadir = PhReduxDir + '/calib'; break;
     case hoggBias_log: 
       datadir = PhReduxDir + '/logs'; break;
     case hoggFF: 
       datadir = PhReduxDir + '/calib'; break;
     case hoggFlat_log: 
       datadir = PhReduxDir + '/logs'; break;
     case hoggObj: 
       datadir = PhReduxDir + '/objcs/' + CamcolStr; break;
     case hoggObj_log:
       datadir = PhReduxDir + '/logs'; break;
     case idB: 
       datadir = PhReduxDir + '/photo/calib'; break;
     case idFF:
       datadir = PhReduxDir + '/objcs/' + CamcolStr; break;
     case idFrameLog:
       datadir = PhReduxDir + '/logs'; break;
     case idR, idRR:
       datadir = PhDataDir  + '/fields/' + CamcolStr; break;
     case idReport:
       datadir = PhReduxDir + '/logs'; break;
     case koAstrom: 
       datadir = PhReduxDir + '/astrom'; break;
     case koCat: 
       datadir = PhReduxDir + '/ssc'; break;
     case koTycho2: 
       datadir = PhReduxDir + '/astrom'; break;
     case pcalibMatchObj, pcalibTrimIndx, pcalibTrimObj: 
       datadir = PhResolveDir + '/nfcalib'; break;
     case psBB, psFF, psField: 
       datadir = PhReduxDir + '/objcs/' + CamcolStr; break;
     case psCT: 
       datadir = PhReduxDir + '/photo'; break;
     case psFang: 
       datadir = PhReduxDir + '/psFangs/' + CamcolStr; break;
     case psKO: 
       datadir = PhReduxDir + '/PS/' + CamcolStr; break;
     case reLocalRun, reGlobalRun: /* ; (deprecated) */
       datadir = PhReduxDir + '/resolve'; break; 
     case reObjGlobal: 
       datadir = PhResolveDir + '/resolve/' + CamcolStr; break;
     case reObjRun: 
       datadir = PhReduxDir + '/resolve/' + CamcolStr; break;
     case reObjTmp: 
       datadir =  PhResolveDir + '/resolve/' + CamcolStr; break;
     case resolve_log: 
       datadir = PhReduxDir + '/resolve'; break;
     case scFang: 
       datadir = PhReduxDir + '/fangs/' + CamcolStr; break;
     case skyfield: 
       datadir = PhSkyDir + '/sky/' + CamcolStr; break;
     case skyframes, skymask, skymodel, skyymodel, skyvec, skyweights: 
       datadir = PhSkyDir + '/sky'; break;
     case tsObj, psObj, tsField: 
       datadir = PhReduxDir + '/calibChunks/' + CamcolStr; 
       break;
     case wiField, wiRun, wiTrimRun, wiRunQA, wiScanline: 
       datadir = PhReduxDir + '/window'; break;
     default: 
       (void) shError('FType not supported by shFitsPath');
       return(NULL);
       break;
     }

   return(NULL);
}

(void) thFitsName(const FType *FType1, 
		  const int *runnum, *camcol, *field, *rerun, *filter, 
		  const boolean nopath, 
		  int *exten, *indx, char *filename);
{
  FType *fType;
  char *fname

  switch(fType1)
    {
    case reObj: 
#if PHOTO_RESOLVE
      ftype = reObjGlobal; 
#else 
      ftype = reObjRun;
#endif
      break;
    default: 
      ftype = ftype1;
      break;
    }
    
  (void)thGetFilterName(filter, filtername);
  (void)thGetFitsTypeString(ftype, fileprefix);
  
  switch(ftype)
    {
    case asTrans, asTranscol:
      format =  '%a_RUNNUMFMT.fit';
      sprintf(fname, format, fileprefix, runnum);
      break;
    case apObj:
      format = '%a_RUNNUMFMT_%a_CAMCOLFMT_FIELDFMT.fit';
      sprint(fname, format, fileprefix, runnum, filtername, camcol, field);
      break;
    case calibImage:
      format = '%a_RUNNUMFMT_%a_CAMCOLFMT_FIELDFMT.fits';
      sprintf(fname, format, fileprefix, runnum, filtername, camcol, field);
      exten = 1;
      break;
    case calibMatch, calibPhotom, calibPhotomGlobal:
      format = '%a_i6.6_i1.1.fits';
      sprintf(fname, format, fileprefix, runnum, camcol);
      exten = 1;
      break;
    case calibObj:
      format = '%a_RUNNUMFMT_CAMCOLFMT_FIELDFMT.fits';
      sprintf(fname, format, fileprefix, runnum, camcol, field);
      exten = 1;
      break;
    case calibObj_gal: 
      format = '%a_i6.6_i1.1-gal.fits';
      sprintf(fname, format, fileprefix, runnum, camcol);
      exten = 1;
      break;
    case calibObj_galmoments:
      format = '%a_i6.6_i1.1-galmoments.fits';
      sprintf(fname, format, fileprefix, runnum, camcol);
      exten = 1;
      break;
    case calibObj_star:
      format = '%a_i6.6_i1.1-star.fits';
      sprintf(fname, format, fileprefix, runnum, camcol);
      exten = 1;
      break;
    case exPhotom:
      /*
	fname = ftype $
	;       + '-' + string(format='(i6.6)', runnum) $ 
	+ '-' + string(format='(i1.1)', camcol) $
	+ '.fit';
      */
      
      format = '%a_i1.1.fit';
      sprintf(fname, format, fileprefix, camcol);
      exten = 1;
     break;
    case fcPCalib:
      format = '%a_i6.6_i1.1.fits';
      sprintf(fname, format, fileprefix, runnum, camcol);
      break;
    case fpAtlas, fpFieldStat, fpObjc: 
      format = '%a_RUNNUMFMT_CAMCOLFMT_FIELDFMT.fit';
      sprintf(fname, format, fileprefix, runnum, camcol, field);
      break;
    case fpBIN, fpC, fpM: 
      format = '%a_RUNNUMFMT_%a_CAMCOLFMT_FIELDFMT.fit';
      sprintf(fname, format, fileprefix, runnum, filtername, camcol, field);
      break;
    case hoggBB:
      format = '%a_RUNNUMFMT_%a_CAMCOLFMT.fits';
      sprintf(fname, format, fileprefix, runnum, filtername, camcol);
      exten = 0;
     break;
    case hoggAstrom: 
      format = '%a_RUNNUMFMT_%a_CAMCOLFMT.fits';
      (void)thGetFitsTypeString(ftype, fileprefix);
      sprintf(fname, format, fileprefix, runnum, filtername, camcol);
      *exten = 1;
      break;
    case hoggAstrom_log, hoggBias_log, hoggFlat_log, hoggObj_log:
      format = '%a_RUNNUMFMT_CAMCOLFMT.log';
      sprintf(fname, format, fileprefix, runnum, camcol);
      break;
    case hoggFF: 
      format = '%a_RUNNUMFMT_%a_CAMCOLFMT.fits';
      sprintf(fname, format, fileprefix, runnum, filtername, camcol);
      (void) thFilePath(fname, datadir, fullname);
      *exten = 0;
      break;
    case hoggObj:
      format = '%a_RUNNUMFMT_CAMCOLFMT_FIELDFMT.fits';
      sprintf(fname, format, fileprefix, runnum, camcol, field);
      exten = 1;
      break;
    case idB, idFF:
      format = '%a_RUNNUMFMT_%a_CAMCOLFMT.fit';
      sprintf(fname, format, fileprefix, runnum, filtername, camcol);
      break;
    case idR, idRR, fakeIdR:
      ftype_use= ftype;
      if (ftype == fakeIdR) {
	ftype=idR;
      }
      format = '%a_RUNNUMFMT_%a_CAMCOLFMT_FIELDFMT.fit';
      sprintf(fname, format, fileprefix, runnum, filtername, camcol, field);
      break;
    case koAstrom, koCat, koTycho2:
      format = '%a_RUNNUMFMT.fit';
      sprintf(fname, format, fileprefix, runnum);
      break;
    case pcalibMatchObj, pcalibTrimIndx, pcalibTrimObj:
      format = '%a_RUNNUMFMT_CAMCOLFMT.fits';
      sprintf(fname, format, fileprefix, runnum, camcol);
      exten = 1;
      break;
    case psBB:
      format = '%a_RUNNUMFMT_CAMCOLFMT_FIELDFMT.fit';
      sprintf(fname, format, fileprefix, runnum, camcol, field);
      break;
    case psCT:
      format = '%a_RUNNUMFMT_CAMCOLFMT.fit';
      sprintf(fname, format, fileprefix, runnum, camcol);
      break;
    case psFang:
      format = '%a_RUNNUMFMT_CAMCOLFMT_FIELDFMT.fit';
      sprintf(fname, format, fileprefix, runnum, camcol, field);
      break;
    case psFF:
      format = '%a_RUNNUMFMT_%a_CAMCOLFMT.fit';
      sprintf(fname, format, fileprefix, runnum, filtername, camcol);
      break;
    case psField:
      format = '%a_RUNNUMFMT_CAMCOLFMT_FIELDFMT.fit';
      sprintf(fname, format, fileprefix, runnum, camcol, field);
      break;
    case psKO:
      format = '%a_RUNNUMFMT_CAMCOLFMT_FIELDFMT.fit';
      sprintf(fname, format, fileprefix, runnum, camcol, field);
      exten = 1;
      break;
      /*
	The following CASE was matched via STRMATCH, thould be rewritten upon necessity
	Example of the IDL line:
	endif else if (strmatch(ftype,'reLocalRun')) then begin
      */
    case reLocalRun, reGlobalRun, reObjGlobal, reObjRun, reObjTmp:
      format = '%a_RUNNUMFMT_CAMCOLFMT_FIELDFMT.fits';
      sprintf(fname, format, fileprefix, runnum, camcol, field);
      exten = 1;
      break;
      /*
	The following CASE was matched via STRMATCH, thould be rewritten upon necessity
	Example of the IDL line:
	endif else if (strmatch(ftype,'resolve.log')) then begin
      */
    case resolve_log:
      format = '%a_RUNNUMFMT_CAMCOLFMT.log'; 
      sprintf(fname, format, fileprefix, runnum, camcol);
      exten = 1;
    case scFang:
      format = '%a_RUNNUMFMT_CAMCOLFMT_FIELDFMT.fit';
      sprintf(fname, format, fileprefix, runnum, camcol, field);
      break;
    case skyfield:
      format = '%a_RUNNUMFMT_CAMCOLFMT_FIELDFMT_%a.fits';
      sprintf(fname, format, fileprefix, runnum, camcol, field, filtername);
      break;
    case skyframes, skymask:
      format = '%a_RUNNUMFMT_CAMCOLFMT_%a.fits';
      sprintf(fname, format, fileprefix, runnum, camcol, filtername);
      break;
    case skymodel, skyvec, skyweights, skyymodel:
      format = '%a_RUNNUMFMT_%a.fits';
      sprintf(fname, format, fileprefix, runnum, filtername);
      break;
    case tsField, tsObj, psObj : 
      format = '%a_RUNNUMFMT_CAMCOLFMT_%a_%4.4.fit';
      sprintf(fname, format, fileprefix, runnum, camcol, rerunstring, field);
      exten = 1;
      break;
    case tsFieldInfo:
      format = '%a_RUNNUMFMT_CAMCOLFMT_FIELDFMT.fits';
      sprintf(fname, format, fileprefix, runnum, camcol, field);
      exten = 1;
      break;
    case wiRunQA:
      format = '%a_RUNNUMFMT.ps';
      sprintf(fname, format, fileprefix, runnum);
      exten = 1;
      break;
    case wiRun, wiScanline:
      format = '%a_RUNNUMFMT_CAMCOLFMT.fits';
      sprintf(fname, format, fileprefix, runnum, camcol);
      exten = 1;
      break;
    default: 
      shError('FType Not Supported');
      return(NULL);
      break;
    }
  
  if (nopath) {
    *fullname = *fname;
  }else{
    
    (void)thFilePath(fname, datadir, fullname);
    
    switch(ftype)
      {
      case Astrans, Astranscol:
	(void) thAstransFitsName(fname, field, camcol, filtername, exten, indx);
	break;
      case fcPCalib, hoggBB, hoggAstrom, hoggFF:
	(void) thfCpCalibFitsName(fname, field, exten, indx);
	break;
      case psField:
	(void) thPsFieldFitsName(fname, field, camcol, filtername, exten, indx);
	break;
      }
  }
  
  return(NULL);
}

/* The following functions thould be strictly PRIVATE */

void thAstransFitsName(const char *filename, 
		       const int *field, *camcol, const char *filtername,
		       int *exten, *indx,);
{
  fitsfile **fptr;
  char *filename;
  int *status, status2

  char *keynames[4];
  int *values[4];

  int *field1, *camcol1, *filtername1;
  int ifilter, jfilter, icamcol, jcamcol;
  
  (int) fits_open_file(fptr, filename, 0, status);

  /* exists if the file is not successfully openned */
  if (status != NULL){
    (void*) thCfitsioError(status);
    exten = NULL;
    indx = NULL;
    /* Do we need to close the fits file opened? 
       Most probably yes, because if the file exists but its name is provided
       along with a non-existing extension number in [], the file is openned but on its ext = 0, so it still needs to be 
       closed for strategic purposes
    */
    (int) fits_close_file(*fptr, status2);
    (void*) thCfitsioError(status2);
    return(status);
  }

  *keynames = {'FIELD0', 'NFIELD0', 'CAMCOLS', 'FILTERS'};
  *values   = {NULL, NULL, NULL, NULL};
  (int) fits_read_key(*fptr, datatype, keynames, values, comment, status);
  
  if (status == NULL) {
    
    field1 = field;
    while (field1 != null) {
      filtername1 = filtername;
      while (filtername != null) {
	camcol1 = camcol;
	while (camcol != null) {
       
	  filters = strtok(values[3], " ");
	  nfilter = strlen(filters);
	  (void) thStrWhere(filters, filter1, jfilter)
	  
	 
	  camcols = strtok(values[2]);
	  (void) thStrWhere(camcols, camcol1, jcamcol)

	  if ((jfilter >= 0) && (jcamcol >= 0) && (nfilter > 0)){
	    *exten = jfilter + jcamcol * nfilter + 1;
	  } else {
	    exten = NULL;
	  }
	  
	  *indx = *field - field0;
	  /*ibad = where(field < field0 || field >= field0+nfields, nbad)
	    if (nbad GT 0) then indx[ibad] = -1L */
	  if ((*field1 < field0) || (field1 >= (field0 + nfield))) {
	    *indx = -1;
	  }
	  index+;
	  camcol1+;}
	filtername1+;}
      field1+;}} else {
	(void*) thCfitsioError(status);
      }

  (int) fits_close_file(*fptr, status2);
  (void*) thCfitsioError(status2);
  return(status);
}


void thfCpCalibFitsName(const char *filename, const int *field,
		       int *exten, *indx,);
{
  fitsfile **fptr;
  char *filename;
  int *status, status2

  char *keynames[2];
  int *values[2];

  int *field1, *camcol1, *filtername1;
  int ifilter, jfilter, icamcol, jcamcol;
  
  (int) fits_open_file(fptr, filename, 0, status);

  /* exists if the file is not successfully openned */
  if (status != NULL){
    (void*) thCfitsioError(status);
    exten = NULL;
    indx = NULL;
    /* Do we need to close the fits file opened? 
       Most probably yes, because if the file exists but its name is provided
       along with a non-existing extension number in [], the file is openned but on its ext = 0, so it still needs to be 
       closed for strategic purposes
    */
    (int) fits_close_file(*fptr, status2);
    (void*) thCfitsioError(status2);
    return(status);
  }

  *keynames = {'FIELD0', 'NFIELD0'};
  *values   = {NULL, NULL};
  (int) fits_read_key(*fptr, datatype, keynames, values, comment, status);
  
  if (status == NULL) {
    
    field1 = field;
    while (field1 != null) {
      if (*field1 < field0+nfields){
	*exten = 1;
	*indx = *field1 - field0;
      }else{
	shError('No fcPCalib file with data for field ' + field1);
	exten = NULL;
	indx = NULL;
      }
      indx+;
      field1+;}} else {
	(void*) thCfitsioError(status);
      }
  
  (int) fits_close_file(*fptr, status2);
  (void*) thCfitsioError(status2);
  return(status);
}

void thPsFieldFitsName(const char *filename, 
		       const int *field, *camcol, const char *filtername,
		       int *exten, *indx);
{
  fitsfile **fptr;
  char *filename;
  int *status, status2

  char *keynames[4];
  int *values[4];

  int *field1, *camcol1, *filtername1;
  int *jfilter
  
  (int) fits_open_file(fptr, filename, 0, status);

  /* exists if the file is not successfully openned */
  if (status != NULL){
    (void*) thCfitsioError(status);
    exten = NULL;
    indx = NULL;
    /* Do we need to close the fits file opened? 
       Most probably yes, because if the file exists but its name is provided
       along with a non-existing extension number in [], the file is openned but on its ext = 0, so it still needs to be 
       closed for strategic purposes
    */
    (int) fits_close_file(*fptr, status2);
    (void*) thCfitsioError(status2);
    return(status);
  }

  *keynames = {'FIELD0', 'NFIELD0', 'CAMCOLS', 'FILTERS'};
  *values   = {NULL, NULL, NULL, NULL};
  (int) fits_read_key(*fptr, datatype, keynames, values, comment, status);
  
  if (status == NULL) {
    filters = strtok(values[3], " ");
    nfilter = strlen(filters);
    (void) thStrWhere(filters, filtername, jfilter);
    while (jfilter != NULL){
      *exten = *jfilter+1;
      if (*field < (field0 + nfield)) {indx = jfilter;}
      jfilter+; indx+; exten+;
    }
  }else {
    (void*) thCfitsioError(status);
    }

(int) fits_close_file(*fptr, status2);
(void*) thCfitsioError(status2);
return(status);
}
