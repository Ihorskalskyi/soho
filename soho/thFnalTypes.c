#include "thFnalTypes.h"

static int static_ncolor = NCOLOR;
/*****************************************************************************/
/*
 * Create or destroy an OBJC structure modified for IO. 
 */

FNAL_OBJC_IO *thFnalObjcIoNew(int ncolor){
   FNAL_OBJC_IO *new;
   
   shAssert(ncolor <= NCOLOR);
   new = thMalloc(sizeof(FNAL_OBJC_IO));
   *(int *)&new->ncolor = ncolor;
   new->aimage = phAtlasImageNew(ncolor);
   new->test = phTestInfoNew(ncolor);

   return(new);
}


/*
 * Destroy an OBJC_IO. If deep is true, all structures allocated within the
 * FNAL_OBJC_IO will be freed; if it's false they will not
 */
void
thFnalObjcIoDel(FNAL_OBJC_IO *fnal_objc_io, int deep)
{
   if(fnal_objc_io == NULL) return;

   if(deep) {
      phAtlasImageDel(fnal_objc_io->aimage,deep);
   }
   phTestInfoDel(fnal_objc_io->test,deep);

   thFree(fnal_objc_io);
}

RET_CODE thFnalObjcIoCopyToObjcIo(OBJC_IO *new, FNAL_OBJC_IO *obj1) {

   shAssert(new != NULL);
   shAssert(obj1 != NULL);

   new->id = obj1->id;
   new->parent = obj1->parent;
   *(int *)&new->ncolor = obj1->ncolor;
   new->nchild = obj1->nchild;
   new->objc_type = obj1->objc_type;
   new->objc_prob_psf = obj1->objc_prob_psf;
   new->catID = obj1->catID;
   new->objc_flags = obj1->objc_flags;
   new->objc_flags2 = obj1->objc_flags2;
   new->objc_rowc = obj1->objc_rowc;
   new->objc_rowcErr = obj1->objc_rowcErr;
   new->objc_colc = obj1->objc_colc;
   new->objc_colcErr = obj1->objc_colcErr;
   new->rowv = obj1->rowv;
   new->rowvErr = obj1->rowvErr;
   new->colv = obj1->colv;
   new->colvErr = obj1->colvErr;
   #if 0
   new->aimage = obj1->aimage;
   if(objc->test == NULL) {
      new->test = phTestInfoNew(objc->ncolor);
   } else {
      new->test = objc->test;
      p_shMemRefCntrIncr(objc->test);
   }
   phTestInfoSetFromObjc(objc, new->test);
   #endif
	int c;
   for(c = 0;c < obj1->ncolor;c++) {
      
      new->rowc[c] = obj1->rowc[c]; 
      new->rowcErr[c] = obj1->rowcErr[c] ;
      new->colc[c] = obj1->colc[c] ;
      new->colcErr[c] = obj1->colcErr[c] ;
      new->sky[c] = obj1->sky[c] ;
      new->skyErr[c] = obj1->skyErr[c] ;
      new->psfCounts[c] = obj1->psfCounts[c] ;
      new->psfCountsErr[c] = obj1->psfCountsErr[c] ;
      new->fiberCounts[c] = obj1->fiberCounts[c] ;
      new->fiberCountsErr[c] = obj1->fiberCountsErr[c] ;
      new->petroCounts[c] = obj1->petroCounts[c] ;
      new->petroCountsErr[c] = obj1->petroCountsErr[c] ;
      new->petroRad[c] = obj1->petroRad[c] ;
      new->petroRadErr[c] = obj1->petroRadErr[c] ;
      new->petroR50[c] = obj1->petroR50[c] ;
      new->petroR50Err[c] = obj1->petroR50Err[c] ;
      new->petroR90[c] = obj1->petroR90[c] ;
      new->petroR90Err[c] = obj1->petroR90Err[c] ;
      new->Q[c] = obj1->Q[c] ;
      new->U[c] = obj1->U[c] ;
      new->QErr[c] = obj1->QErr[c] ;
      new->UErr[c] = obj1->UErr[c] ;
      new->M_e1[c] = obj1->M_e1[c] ;
      new->M_e2[c] = obj1->M_e2[c] ;
      new->M_e1e1Err[c] = obj1->M_e1e1Err[c] ;
      new->M_e1e2Err[c] = obj1->M_e1e2Err[c] ;
      new->M_e2e2Err[c] = obj1->M_e2e2Err[c] ;
      new->M_rr_cc[c] = obj1->M_rr_cc[c] ;
      new->M_rr_ccErr[c] = obj1->M_rr_ccErr[c] ;
      new->M_cr4[c] = obj1->M_cr4[c] ;
      new->M_e1_psf[c] = obj1->M_e1_psf[c] ;
      new->M_e2_psf[c] = obj1->M_e2_psf[c] ;
      new->M_rr_cc_psf[c] = obj1->M_rr_cc_psf[c] ;
      new->M_cr4_psf[c] = obj1->M_cr4_psf[c] ;
      new->nprof[c] = obj1->nprof[c] ;
      int i_ann;
	for (i_ann = 0; i_ann < NANN; i_ann++) {
	new->profMean[c][i_ann] = obj1->profMean[c][0];
	new->profErr[c][i_ann] = obj1->profErr[c][0];
	}
      new->iso_rowc[c] = obj1->iso_rowc[c] ;
      new->iso_rowcErr[c] = obj1->iso_rowcErr[c] ;
      new->iso_rowcGrad[c] = obj1->iso_rowcGrad[c] ;
      new->iso_colc[c] = obj1->iso_colc[c] ;
      new->iso_colcErr[c] = obj1->iso_colcErr[c] ;
      new->iso_colcGrad[c] = obj1->iso_colcGrad[c] ;
      new->iso_a[c] = obj1->iso_a[c] ;
      new->iso_aErr[c] = obj1->iso_aErr[c] ;
      new->iso_aGrad[c] = obj1->iso_aGrad[c] ;
      new->iso_b[c] = obj1->iso_b[c] ;
      new->iso_bErr[c] = obj1->iso_bErr[c] ;
      new->iso_bGrad[c] = obj1->iso_bGrad[c] ;
      new->iso_phi[c] = obj1->iso_phi[c] ;
      new->iso_phiErr[c] = obj1->iso_phiErr[c] ;
      new->iso_phiGrad[c] = obj1->iso_phiGrad[c] ;
      new->r_deV[c] = obj1->r_deV[c] ;
      new->r_deVErr[c] = obj1->r_deVErr[c] ;
      new->ab_deV[c] = obj1->ab_deV[c] ;
      new->ab_deVErr[c] = obj1->ab_deVErr[c] ;
      new->phi_deV[c] = obj1->phi_deV[c] ;
      new->phi_deVErr[c] = obj1->phi_deVErr[c] ;
      new->counts_deV[c] = obj1->counts_deV[c] ;
      new->counts_deVErr[c] = obj1->counts_deVErr[c] ;
      new->r_exp[c] = obj1->r_exp[c] ;
      new->r_expErr[c] = obj1->r_expErr[c] ;
      new->ab_exp[c] = obj1->ab_exp[c] ;
      new->ab_expErr[c] = obj1->ab_expErr[c] ;
      new->phi_exp[c] = obj1->phi_exp[c] ;      
      new->phi_expErr[c] = obj1->phi_expErr[c] ;      
      new->counts_exp[c] = obj1->counts_exp[c] ;      
      new->counts_expErr[c] = obj1->counts_expErr[c] ;      
      new->counts_model[c] = obj1->counts_model[c] ;      
      new->counts_modelErr[c] = obj1->counts_modelErr[c] ;      
      new->star_L[c] = obj1->star_L[c];  new->star_lnL[c] = obj1->star_lnL[c] ;
      new->exp_L[c] = obj1->exp_L[c] ; new->exp_lnL[c] = obj1->exp_lnL[c] ;
      new->deV_L[c] = obj1->deV_L[c] ; new->deV_lnL[c] = obj1->deV_lnL[c] ;
      new->fracPSF[c] = obj1->fracPSF[c] ;
      new->texture[c] = obj1->texture[c] ;
      new->flags[c] = obj1->flags[c] ;
      new->flags2[c] = obj1->flags2[c] ;
      new->type[c] = obj1->type[c] ;
      new->prob_psf[c] = obj1->prob_psf[c] ;
   }

return(SH_SUCCESS);

}

RET_CODE thObjcIoCopyToFnalObjcIo(FNAL_OBJC_IO *new, OBJC_IO *obj1) {

   shAssert(new != NULL);
   shAssert(obj1 != NULL);

   new->id = obj1->id;
   new->parent = obj1->parent;
   *(int *)&new->ncolor = obj1->ncolor;
   new->nchild = obj1->nchild;
   new->objc_type = obj1->objc_type;
   new->objc_prob_psf = obj1->objc_prob_psf;
   new->catID = obj1->catID;
   new->objc_flags = obj1->objc_flags;
   new->objc_flags2 = obj1->objc_flags2;
   new->objc_rowc = obj1->objc_rowc;
   new->objc_rowcErr = obj1->objc_rowcErr;
   new->objc_colc = obj1->objc_colc;
   new->objc_colcErr = obj1->objc_colcErr;
   new->rowv = obj1->rowv;
   new->rowvErr = obj1->rowvErr;
   new->colv = obj1->colv;
   new->colvErr = obj1->colvErr;
   #if 0
   new->aimage = obj1->aimage;
   if(objc->test == NULL) {
      new->test = phTestInfoNew(objc->ncolor);
   } else {
      new->test = objc->test;
      p_shMemRefCntrIncr(objc->test);
   }
   phTestInfoSetFromObjc(objc, new->test);
   #endif
	int c;
   for(c = 0;c < obj1->ncolor;c++) {
      
      new->rowc[c] = obj1->rowc[c]; 
      new->rowcErr[c] = obj1->rowcErr[c] ;
      new->colc[c] = obj1->colc[c] ;
      new->colcErr[c] = obj1->colcErr[c] ;
      new->sky[c] = obj1->sky[c] ;
      new->skyErr[c] = obj1->skyErr[c] ;
      new->psfCounts[c] = obj1->psfCounts[c] ;
      new->psfCountsErr[c] = obj1->psfCountsErr[c] ;
      new->fiberCounts[c] = obj1->fiberCounts[c] ;
      new->fiberCountsErr[c] = obj1->fiberCountsErr[c] ;      
/* these two records are specific to FNAL copy */
      new->fiber2Counts[c] = VALUE_IS_BAD; 
      new->fiber2CountsErr[c] = VALUE_IS_BAD;
      new->petroCounts[c] = obj1->petroCounts[c] ;
      new->petroCountsErr[c] = obj1->petroCountsErr[c] ;
      new->petroRad[c] = obj1->petroRad[c] ;
      new->petroRadErr[c] = obj1->petroRadErr[c] ;
      new->petroR50[c] = obj1->petroR50[c] ;
      new->petroR50Err[c] = obj1->petroR50Err[c] ;
      new->petroR90[c] = obj1->petroR90[c] ;
      new->petroR90Err[c] = obj1->petroR90Err[c] ;
      new->Q[c] = obj1->Q[c] ;
      new->U[c] = obj1->U[c] ;
      new->QErr[c] = obj1->QErr[c] ;
      new->UErr[c] = obj1->UErr[c] ;
      new->M_e1[c] = obj1->M_e1[c] ;
      new->M_e2[c] = obj1->M_e2[c] ;
      new->M_e1e1Err[c] = obj1->M_e1e1Err[c] ;
      new->M_e1e2Err[c] = obj1->M_e1e2Err[c] ;
      new->M_e2e2Err[c] = obj1->M_e2e2Err[c] ;
      new->M_rr_cc[c] = obj1->M_rr_cc[c] ;
      new->M_rr_ccErr[c] = obj1->M_rr_ccErr[c] ;
      new->M_cr4[c] = obj1->M_cr4[c] ;
      new->M_e1_psf[c] = obj1->M_e1_psf[c] ;
      new->M_e2_psf[c] = obj1->M_e2_psf[c] ;
      new->M_rr_cc_psf[c] = obj1->M_rr_cc_psf[c] ;
      new->M_cr4_psf[c] = obj1->M_cr4_psf[c] ;
      new->nprof[c] = obj1->nprof[c] ;
      memcpy(new->profMean[c],obj1->profMean,NANN*sizeof(obj1->profMean[c]));
      memcpy(new->profErr[c],obj1->profErr,NANN*sizeof(obj1->profErr[c]));
      new->iso_rowc[c] = obj1->iso_rowc[c] ;
      new->iso_rowcErr[c] = obj1->iso_rowcErr[c] ;
      new->iso_rowcGrad[c] = obj1->iso_rowcGrad[c] ;
      new->iso_colc[c] = obj1->iso_colc[c] ;
      new->iso_colcErr[c] = obj1->iso_colcErr[c] ;
      new->iso_colcGrad[c] = obj1->iso_colcGrad[c] ;
      new->iso_a[c] = obj1->iso_a[c] ;
      new->iso_aErr[c] = obj1->iso_aErr[c] ;
      new->iso_aGrad[c] = obj1->iso_aGrad[c] ;
      new->iso_b[c] = obj1->iso_b[c] ;
      new->iso_bErr[c] = obj1->iso_bErr[c] ;
      new->iso_bGrad[c] = obj1->iso_bGrad[c] ;
      new->iso_phi[c] = obj1->iso_phi[c] ;
      new->iso_phiErr[c] = obj1->iso_phiErr[c] ;
      new->iso_phiGrad[c] = obj1->iso_phiGrad[c] ;
      new->r_deV[c] = obj1->r_deV[c] ;
      new->r_deVErr[c] = obj1->r_deVErr[c] ;
      new->ab_deV[c] = obj1->ab_deV[c] ;
      new->ab_deVErr[c] = obj1->ab_deVErr[c] ;
      new->phi_deV[c] = obj1->phi_deV[c] ;
      new->phi_deVErr[c] = obj1->phi_deVErr[c] ;
      new->counts_deV[c] = obj1->counts_deV[c] ;
      new->counts_deVErr[c] = obj1->counts_deVErr[c] ;
      new->r_exp[c] = obj1->r_exp[c] ;
      new->r_expErr[c] = obj1->r_expErr[c] ;
      new->ab_exp[c] = obj1->ab_exp[c] ;
      new->ab_expErr[c] = obj1->ab_expErr[c] ;
      new->phi_exp[c] = obj1->phi_exp[c] ;      
      new->phi_expErr[c] = obj1->phi_expErr[c] ;      
      new->counts_exp[c] = obj1->counts_exp[c] ;      
      new->counts_expErr[c] = obj1->counts_expErr[c] ;      
      new->counts_model[c] = obj1->counts_model[c] ;      
      new->counts_modelErr[c] = obj1->counts_modelErr[c] ;      
      new->star_L[c] = obj1->star_L[c];  new->star_lnL[c] = obj1->star_lnL[c] ;
      new->exp_L[c] = obj1->exp_L[c] ; new->exp_lnL[c] = obj1->exp_lnL[c] ;
      new->deV_L[c] = obj1->deV_L[c] ; new->deV_lnL[c] = obj1->deV_lnL[c] ;
      new->fracPSF[c] = obj1->fracPSF[c] ;
      new->texture[c] = obj1->texture[c] ;
      new->flags[c] = obj1->flags[c] ;
      new->flags2[c] = obj1->flags2[c] ;
      new->type[c] = obj1->type[c] ;
      new->prob_psf[c] = obj1->prob_psf[c] ;
   }

return(SH_SUCCESS);

}
