#include "thModelTypes.h"
#include "thDebug.h"
#include "thCalib.h"
#include "thMath.h"

static CHAIN *MBank = NULL, *OBank = NULL;
static SCHEMA *photo_schema_obj_type = NULL;

static RET_CODE DummyGenericPixelIntegrator(void *p, void *f, THREGION *reg, THPIX *mcount);

RET_CODE DummyGenericPixelIntegrator(void* p, void *f, THREGION *reg, THPIX *mcount) {
char *name = "DummyGenericPixelIntegrator";
thError("%s: ERROR - dummy function - should never reach here, source code problem", name);
return(SH_GENERIC_ERROR);
}
	

static void init_static_vars (void); 

void init_static_vars (void)  {
	TYPE t;
	if (photo_schema_obj_type == NULL) {
		t = shTypeGetFromName("OBJ_TYPE");
		shAssert(t != UNKNOWN_SCHEMA);
		photo_schema_obj_type = shSchemaGetFromType(t);
	}
	return;
}

/* FUNC_ELEM handlers */

FUNC_ELEM *thFuncElemNew(void *f, char *pname, char *pname2) {
char *name = "thFuncElemNew";
if (f == NULL) {
	thError("%s: ERROR - null function pointer", name);
	return(NULL);
}
FUNC_ELEM *felem;
felem = (FUNC_ELEM *) thCalloc(1, sizeof(FUNC_ELEM));
felem->f = f;
felem->pname = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
felem->pname2 = (char *) thCalloc(MX_STRING_LEN, sizeof(char)); 
if (pname != NULL) strcpy(felem->pname, pname);
if (pname2 != NULL) strcpy(felem->pname2, pname2);

return(felem);
}

void thFuncElemDel(FUNC_ELEM *felem) {
if (felem == NULL) return;
thFree(felem->pname);
thFree(felem->pname2);
thFree(felem);
return;
}

/* MODEL_ELEM handlers */

MODEL_ELEM *thModelElemNew(char *mname, void *f0, char *pname, 
			CONSTRUCTION_MODE cmode, THREGION_TYPE regtype, 
			PSF_CONVOLUTION_TYPE convtype, int ncomponent, 
			void *init, 
			void *reg_generator, 
			void *psf_info_generator, 
			void *psf_mask_generator, 
			void *amplitude_specifier) {
char *name = "thModelElemNew";
if (mname == NULL || strlen(mname) == 0) {
	thError("%s: ERROR - cannot make a (model_elem) with no name", name);
	return(NULL);
}
if (f0 == NULL) {
	thError("%s: ERROR - need a main image constructor to generate model '%s'",
	name, mname);
	return(NULL);
}
if (init == NULL) {
	thError("%s: ERROR - need a parameter initiator to generate model '%s'", name, mname);
	return(NULL);
}
TYPE ptype;
if (pname != NULL && strlen(pname) != 0 && (ptype = shTypeGetFromName(pname)) == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - unknown schema for parameter type (%s)", name, pname);
	return(NULL);
}

if (cmode != WHOLEREGION && cmode != PIXELINTEGRAL) {
	thError("%s: ERROR - unsupported mode of construction for model '%s'", name, mname);
	return(NULL);
}

if (regtype != THREGION1_TYPE && regtype != THREGION2_TYPE && 
	regtype != THREGION3_TYPE) {
	thError("%s: ERROR - unsupported type of extended region type for model '%s'", name, mname);
	return(NULL);
}

/* 
if (cmode == PIXELINTEGRAL && reg_generator == NULL) {
	thError("%s: ERROR - the region generator is null while construction mode is set to 'PIXELINTEGRAL' for model '%s'",
		name, mname);
	return(NULL);
}

if (cmode == WHOLEREGION && reg_generator != NULL) {
	thError("%s: ERROR - expecting a null region generator for model '%s' with construction mode 'WHOLEREGION'", 
		name, mname);
	return(NULL);
}
*/

if (regtype == THREGION1_TYPE && convtype == NO_PSF_CONVOLUTION && ncomponent != 1 && ncomponent != DEFAULT_NCOMPONENT) {
	thError("%s: ERROR - for (regtype) 'THREGION1' and (psf_conv) 'NO_PSF_CONVOLUTION' number of components should be set to (1) or to (DEFAULT_NCOMPONENT), found (%d) instead", name, ncomponent);
	return(NULL);
}

/* the following naturally checks for compatibility of convtype and nc*/
PSF_CONVOLUTION_INFO *psf_info;
if ((psf_info = thPsfConvInfoNew(convtype, ncomponent)) == NULL) {
	thError("%s: ERROR - could not make sample (psf_info)", name);
	return(NULL);
}


/* model elements */

MODEL_ELEM *melem;
melem = thCalloc(1, sizeof(MODEL_ELEM));

melem->fs = shChainNew("FUNC_ELEM");
melem->DDfs = shChainNew("FUNC_ELEM");
FUNC_ELEM *f;

f = thFuncElemNew(f0, "", "");
shChainElementAddByPos(melem->fs, f, "FUNC_ELEM", TAIL, AFTER);

melem->mname = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
melem->pname = (char *) thCalloc(MX_STRING_LEN, sizeof(char));

strcpy(melem->mname, mname);
if (pname != NULL) strcpy(melem->pname, pname);
melem->ptype = ptype;
melem->init = init;

/* the following was added to adopt the extended region definition */
melem->reg_generator = reg_generator; /* this will require index number to generate the region */
melem->psf_info_generator = psf_info_generator;
melem->psf_mask_generator = psf_mask_generator;
melem->amplitude_specifier = amplitude_specifier;

melem->cmode = cmode;
melem->regtype = regtype;
melem->convtype = convtype;
melem->ncomponent = ncomponent;
melem->psf_info = psf_info;

return(melem);
}

void thModelElemDel(MODEL_ELEM *me) {
if (me == NULL) return;
if (me->mname != NULL) thFree(me->mname);
if (me->pname != NULL) thFree(me->pname);
if (me->fs != NULL) shChainDestroy(me->fs, thFuncElemDel);
if (me->DDfs != NULL) shChainDestroy(me->DDfs, thFuncElemDel);
thFree(me);
return;
}


RET_CODE thModelElemGet(MODEL_ELEM *me, char **mname, TYPE *ptype, char **pname,  
		void **init, 
		void **reg_generator, 
		void **psf_info_generator, 
		void **psf_mask_generator, 
		void *amplitude_specifier, 
		CONSTRUCTION_MODE *cmode, THREGION_TYPE *regtype,
		PSF_CONVOLUTION_TYPE *convtype, 
		int *ncomponent) {
char *name = "thModelElemGet";
if (me == NULL) { 
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (mname != NULL) *mname = me->mname;
if (ptype != NULL) *ptype = me->ptype;
if (pname != NULL) *pname = me->pname;
if (init != NULL) *init = me->init;
if (reg_generator != NULL) *reg_generator = me->reg_generator;
if (psf_info_generator != NULL) *psf_info_generator = me->psf_info_generator;
if (psf_mask_generator != NULL) *psf_mask_generator = me->psf_mask_generator;
if (amplitude_specifier != NULL) *(void **) amplitude_specifier = me->amplitude_specifier;
if (cmode != NULL) *cmode = me->cmode;
if (regtype != NULL) *regtype = me->regtype;
if (convtype != NULL) *convtype = me->convtype;
if (ncomponent != NULL) *ncomponent = me->ncomponent;

return(SH_SUCCESS);
}
RET_CODE thModelElemGetFunc(MODEL_ELEM *melem, void **f0) {
char *name = "thModelElemGetFunc";

if (melem == NULL || melem->fs == NULL || shChainSize(melem->fs) == 0) {
	thError("%s: ERROR - improperly allocated (model_elem)", name);
	if (f0 != NULL) f0 = NULL;
	return(SH_GENERIC_ERROR);
}

if (f0 == NULL) {
	thWarn("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}

int i, n;
n = shChainSize(melem->fs);
FUNC_ELEM *fe;

for (i = 0; i <  n; i++) {
	fe = shChainElementGetByPos(melem->fs, i);
	if (fe->pname == NULL || strlen(fe->pname) == 0) {
		*f0 = fe->f;
		return(SH_SUCCESS);
	}
}

thError("%s: ERROR - could not locate a main image function in model (%s)", name, melem->mname);
return(SH_GENERIC_ERROR);
}

RET_CODE thModelElemAddDeriv2(MODEL_ELEM *melem, char *rname1, char *rname2, void *DDf) {
char *name = "thModelElemAddDeriv2";
if (melem == NULL) {
	thError("%s: ERROR - improperly allocated (model_elem)", name);
	return(SH_GENERIC_ERROR);
}
if (rname1 == NULL || strlen(rname1) == 0) {
	thError("%s: ERROR - null or empty (rname1)", name);
	return(SH_GENERIC_ERROR);
}
if (rname2 == NULL || strlen(rname2) == 0) {
	thError("%s: ERROR - null or empty (rname2)", name);
	return(SH_GENERIC_ERROR);
}

CHAIN *fs;
if (melem->DDfs == NULL) melem->DDfs = shChainNew("FUNC_ELEM");
fs = melem->DDfs;
int i, n;
n = shChainSize(fs);

int ncomponent;
ncomponent = melem->ncomponent;

FUNC_ELEM *fe;
for (i = 0; i < n; i++) {
	fe = shChainElementGetByPos(fs, i);
	if ((!strcmp(rname1, fe->pname) && !strcmp(rname2, fe->pname2)) || 
	(!strcmp(rname2, fe->pname) && !strcmp(rname1, fe->pname2))) {
		thError("%s: ERROR - trying to add the derivative with respect to (%s, %s) while a function already exists in model (%s)", name, rname1, rname2, melem->mname);
		return(SH_GENERIC_ERROR);
	}
}

if (melem->ptype == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - structure of the input model parameter is uknown for model (%s)", name, melem->mname);
	return(SH_GENERIC_ERROR);
}

SCHEMA_ELEM *se1;
se1 = shSchemaElemGetFromType(melem->ptype, rname1);
if (se1 == NULL) {
	thError("%s: ERROR - record (%s) does not exist in parameter (%s) of model (%s)", name, rname1, melem->pname, melem->mname);
	return(SH_GENERIC_ERROR);
}
SCHEMA_ELEM *se2;
se2 = shSchemaElemGetFromType(melem->ptype, rname2);
if (se2 == NULL) {
	thError("%s: ERROR - record (%s) does not exist in parameter (%s) of model (%s)", name, rname2, melem->pname, melem->mname);
	return(SH_GENERIC_ERROR);
}

fe = thFuncElemNew(DDf, rname1, rname2);
shChainElementAddByPos(fs, fe, "FUNC_ELEM", TAIL, AFTER);
return(SH_SUCCESS);
}




RET_CODE thModelElemAddDeriv(MODEL_ELEM *melem, char *rname, void *Df) {
char *name = "thModelElemAddDeriv";
if (melem == NULL) {
	thError("%s: ERROR - improperly allocated (model_elem)", name);
	return(SH_GENERIC_ERROR);
}

CHAIN *fs;
if (melem->fs == NULL) melem->fs = shChainNew("FUNC_ELEM");
fs = melem->fs;
int i, n;
n = shChainSize(fs);

int ncomponent;
ncomponent = melem->ncomponent;

FUNC_ELEM *fe;
for (i = 0; i < n; i++) {
	fe = shChainElementGetByPos(fs, i);
	if ((rname == NULL || strlen(rname) == 0) && (fe->pname == NULL || strlen(fe->pname) == 0)) {
		thError("%s: ERROR - trying to add the main image function, while one already exists in model (%s)", name, melem->mname);
		return(SH_GENERIC_ERROR);
  		}
	if (rname != NULL && fe->pname != NULL && !strcmp(rname, fe->pname)) {
		thError("%s: ERROR - trying to add the derivative with respect to (%s) while a function already exists in model (%s)", name, rname, melem->mname);
		return(SH_GENERIC_ERROR);
	}
}

if (melem->ptype == UNKNOWN_SCHEMA && rname != NULL && strlen(rname) != 0) {
	thError("%s: ERROR - structure of the input model parameter is uknown for model (%s)", name, melem->mname);
	return(SH_GENERIC_ERROR);
}
if (rname == NULL || strlen(rname) == 0) {
	fe = thFuncElemNew(Df, "", "");
	shChainElementAddByPos(fs, fe, "FUNC_ELEM", HEAD, BEFORE);
	return(SH_SUCCESS);
}

SCHEMA_ELEM *se;
se = shSchemaElemGetFromType(melem->ptype, rname);
if (se == NULL) {
	thError("%s: ERROR - record (%s) does not exist in parameter (%s) of model (%s)", name, rname, melem->pname, melem->mname);
	return(SH_GENERIC_ERROR);
}

fe = thFuncElemNew(Df, rname, "");
shChainElementAddByPos(fs, fe, "FUNC_ELEM", TAIL, AFTER);
return(SH_SUCCESS);
}

RET_CODE thModelElemGetDeriv2(MODEL_ELEM *melem, char *rname1, char *rname2, void **DDf) {
char *name = "thModelElemGetDeriv2";
if (melem == NULL || melem->DDfs == NULL) {
	thError("%s: ERROR - improperly allocated (model_elem)", name);
	if (DDf != NULL) *DDf = NULL;
	return(SH_GENERIC_ERROR);
}

if (rname1 == NULL || strlen(rname1) == 0) {
	thError("%s: ERROR - null or empty record name (rname1)", name);
	return(SH_GENERIC_ERROR);
} else if (rname2 == NULL || strlen(rname2) == 0) {
	thError("%s: ERROR - null or empty record name (rname2)", name);
	return(SH_GENERIC_ERROR);
}

CHAIN *fs;
fs = melem->DDfs;
int i, n;
n = shChainSize(fs);
FUNC_ELEM *fe;
for (i = 0; i < n; i++) {
	fe = shChainElementGetByPos(fs, i);
	if (
	fe->pname != NULL && 
	((!strcmp(fe->pname, rname1) && !strcmp(fe->pname2, rname2)) || (!strcmp(fe->pname, rname2) && !strcmp(fe->pname2, rname1)))
	) {
		if (DDf == NULL) {
			thWarn("%s: WARNING - null place holder, performing null", name);
		} else {
			*DDf = fe->f;
		}
		return(SH_SUCCESS);
	}
}

/* the function was not found */
if (DDf != NULL) *DDf = NULL;
#if DEBUG_MODELTYPES 
thError("%s: WARNING - could not locate DDf wrt. (%s, %s) in model (%s)", name, rname1, rname2, melem->mname);
#endif
return(SH_SUCCESS);
}




RET_CODE thModelElemGetDeriv(MODEL_ELEM *melem, char *rname, void **Df) {
char *name = "thModelElemGetDeriv";
if (melem == NULL || melem->fs == NULL) {
	thError("%s: ERROR - improperly allocated (model_elem)", name);
	if (Df != NULL) *Df = NULL;
	return(SH_GENERIC_ERROR);
}

CHAIN *fs;
fs = melem->fs;
int i, n;
n = shChainSize(fs);
FUNC_ELEM *fe;
for (i = 0; i < n; i++) {
	fe = shChainElementGetByPos(fs, i);
	if (rname == NULL && (fe->pname == NULL || strlen(fe->pname) == 0)) {
		if (Df == NULL) {
			thWarn("%s: WARNING - null place holder, performing null", name);
			return(SH_SUCCESS);
		} else {
			*Df = fe->f;
			return(SH_SUCCESS);
		} 
	}
	if (rname != NULL && fe->pname != NULL && !strcmp(fe->pname, rname)) {
		if (Df == NULL) {
			thWarn("%s: WARNING - null place holder, performing null", name);
		} else {
			*Df = fe->f;
		}
		return(SH_SUCCESS);
	}
}

/* the function was not found */
if (Df != NULL) *Df = NULL;
if (rname != NULL && strlen(rname) != 0) {
	thError("%s: WARNING - could not locate the derivative function w.r.t. (%s) in model (%s)", name, rname, melem->mname);
	return(SH_SUCCESS);
} else {
	thError("%s: WARNING - could not locate the main image function in model (%s)", name, melem->mname);
	return(SH_SUCCESS);
}
return(SH_GENERIC_ERROR);
}

RET_CODE thModelElemDelDeriv2(MODEL_ELEM *melem, char *rname1, char *rname2) {
char *name = "thModelElemDelDeriv2";
if (melem == NULL || melem->fs == NULL) {
	thError("%s: ERROR - improperly allocated (model_elem)", name);
	return(SH_GENERIC_ERROR);
}
if (rname1 == NULL || strlen(rname1) == 0) {
	thError("%s: ERROR - null or empty (rname1)", name);
	return(SH_GENERIC_ERROR);
} else if (rname2 == NULL || strlen(rname2) == 0) {
	thError("%s: ERROR - null or empty (rname2)", name);
	return(SH_GENERIC_ERROR);
}
int i, n;
n = shChainSize(melem->DDfs);
FUNC_ELEM *fe = NULL;
for (i = 0; i < n; i++) {
	fe = shChainElementGetByPos(melem->DDfs, i);
	if ((!strcmp(fe->pname, rname1) && !strcmp(fe->pname2, rname2)) || 
	(!strcmp(fe->pname, rname2) && !strcmp(fe->pname2, rname1))) {
		shChainElementRemByPos(melem->DDfs, i);
		thFuncElemDel(fe);
		return(SH_SUCCESS);
	}
}

thWarn("%s: WARNING - model element (%s) does not include derivative w.r.t. (%s, %s)", name, melem->mname, rname1, rname2);

return(SH_SUCCESS);
}



RET_CODE thModelElemDelDeriv(MODEL_ELEM *melem, char *rname) {
char *name = "thModelElemDelDeriv";
if (melem == NULL || melem->fs == NULL) {
	thError("%s: ERROR - improperly allocated (model_elem)", name);
	return(SH_GENERIC_ERROR);
}
if (rname == NULL) rname = "";
int i, n;
n = shChainSize(melem->fs);
FUNC_ELEM *fe = NULL;
for (i = 0; i < n; i++) {
	fe = shChainElementGetByPos(melem->fs, i);
	if (!strcmp(fe->pname, rname)) {
		shChainElementRemByPos(melem->fs, i);
		thFuncElemDel(fe);
		return(SH_SUCCESS);
	}
}
if (strlen(rname) != 0) {
	thWarn("%s: WARNING - model element (%s) does not include derivative w.r.t. (%s)", name, melem->mname, rname);
} else {
	thWarn("%s: WARNING - model element (%s) does not include the main image function", name, melem->mname);
}
return(SH_SUCCESS);
}

RET_CODE thModelElemAddToMBank(MODEL_ELEM *melem) {
char *name = "thModelElemAddToMBank";
if (melem == NULL || melem->mname == NULL || strlen(melem->mname) == 0) {
	thError("%s: ERROR - improperly allocated (model_elem)", name);
	return(SH_GENERIC_ERROR);
}

if (MBank == NULL) {
	thError("%s: ERROR - initiate Model Bank before adding (model_elem)s", name);
	return(SH_GENERIC_ERROR);
}

#if 0
int i, n;
n = shChainSize(MBank);
MODEL_ELEM *me;

int removed = 0;
for (i = 0; i < n; i++) {
	me = shChainElementGetByPos(MBank, i);
	if (me != NULL && !strcmp(me->mname, melem->mname)) {
		thError("%s: WARNING - a model of name (%s) already listed in Model Bank - deleting old model", name, me->mname);
		shChainElementRemByPos(MBank, i);
		thModelElemDel(me);
		removed++;
	}
}
if (removed != 0) {
	thError("%s: WARNING - a total of (%d) models removed from the model bank", name, removed);
}
#else

RET_CODE status;
status = thModelElemDelFromBank(melem->mname);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not delete model '%s' from model bank", name, melem->mname);
	return(status);
}

#endif
shChainElementAddByPos(MBank, melem, "MODEL_ELEM", TAIL, AFTER);
return(SH_SUCCESS);
}

RET_CODE thModelElemDelFromBank(char *mname) {
char *name = "thModelElemDelFromBank";
if (mname == NULL || strlen(mname) == 0) {
	thError("%s: ERROR - null model name", name);
	return(SH_GENERIC_ERROR);
}

if (MBank == NULL) {
	thError("%s: ERROR - initiate Model Bank before deleting (model_elem)s", name);
	return(SH_GENERIC_ERROR);
}

int i, n, posfound = -1;
n = shChainSize(MBank);
MODEL_ELEM *me;
int pos = 0;
for (i = 0; i < n; i++) {
	me = shChainElementGetByPos(MBank, pos);
	if (!strcmp(me->mname, mname)) {
		shChainElementRemByPos(MBank, pos);
		thModelElemDel(me);
		posfound = pos;	
	} else {
		pos++;
	}
}

#if DEBUG_MODELTYPES
if (posfound == -1) {
	thError("%s: WARNING - no model of name '%s' found in bank", name, mname);
}
#endif

return(SH_SUCCESS);
}


RET_CODE thModelElemGetByName(char *mname, MODEL_ELEM **melem) {
char *name = "thModelElemGetByName";
if (mname == NULL || strlen(mname) == 0) {
	thError("%s: ERROR - empty model name cannot be searched for in Model Bank", name);
	if (melem != NULL) *melem = NULL;
	return(SH_GENERIC_ERROR);	
}

if (MBank == NULL) {
	thError("%s: ERROR - initiate Model Bank before searching for any models", name);
	if (melem != NULL) *melem = NULL;
	return(SH_GENERIC_ERROR);
}

int i, n;
n = shChainSize(MBank);
MODEL_ELEM *me;
for (i = 0; i < n; i++) {
	me = shChainElementGetByPos(MBank, i);
	if (me == NULL) {
		thError("%s: WARNING - null (model_element) found in the bank at (i = %d out of n = %d)", name, i, n);
	} else if (me->mname == NULL) {
		thError("%s: WARNING - (model_element) with (mname = NULL) found in the bank at (i = %d out of n = %d)", name, i, n);
	} else {
		if (!strcmp(me->mname, mname)) {
			if (melem != NULL) *melem = me;
			return(SH_SUCCESS);
		}
	}
}
if (melem != NULL) *melem = NULL;
return(SH_GENERIC_ERROR);
}

RET_CODE thModelRemByName(char *mname, MODEL_ELEM **melem) {
char *name = "thModelRemByName";
if (mname == NULL || strlen(mname) == 0) {
	thError("%s: ERROR - empty model name cannot be searched for in Model Bank", name);
	if (melem != NULL) *melem = NULL;
	return(SH_GENERIC_ERROR);
}

if (MBank == NULL) {
	thError("%s: ERROR - initiate Model Bank before removing any models", name);
	if (melem != NULL) *melem = NULL;
	return(SH_GENERIC_ERROR);
}

int i, n;
n = shChainSize(MBank);
MODEL_ELEM *me;
for (i = 0; i < n; i++) {
	me = shChainElementGetByPos(MBank, i);
	if (!strcmp(me->mname, mname)) {
		shChainElementRemByPos(MBank, i);
		if (melem == NULL) {
			thModelElemDel(me);
		} else {
			*melem = me;
		}
		return(SH_SUCCESS);
	}
}
thError("%s: ERROR - could not locate model (%s) in Model Bank", name, mname);
return(SH_GENERIC_ERROR);
}

/* Object Element Handlers */
OBJC_ELEM *thObjcElemNew(char *candidatename, char *classname, void *classifier, char *pname, void *compactor) {
char *name = "thObjcElemNew";
if (classname == NULL || strlen(classname) == 0 || 
	(classifier == NULL && 
	strcmp("UNKNOWN_OBJC", classname) && 
	strcmp("PHOTO_OBJC", classname) && 
	strcmp("SKY", classname))){
	thError("%s: ERROR - classname and classifier needs to be provided", name);
	return(NULL);
}

TYPE ptype;
if (pname != NULL && strlen(pname) != 0 && (ptype = shTypeGetFromName(pname)) == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - object description parameter (%s) not supported (unknown_schema)", name, pname);
	return(NULL);
}

THOBJCTYPE candidatetype;
if (candidatename != NULL && strlen(candidatename) != 0 && 
	(thObjcTypeGetFromName(candidatename, &candidatetype) != SH_SUCCESS || candidatetype == UNKNOWN_OBJC)) {
	thError("%s: ERROR - candidate type (%s) is unknown", name, candidatename);
	return(NULL);
}

THOBJCTYPE classtype;
RET_CODE status;
status = thObjcTypeGetNextAvailable(&classtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not find an available type indicator for objc class (%s)", name, classname);
	return(NULL);
}
if (pname == NULL) pname = "";
if (candidatename == NULL) candidatename = "";

OBJC_ELEM *oelem;
oelem = (OBJC_ELEM *) thCalloc(1, sizeof(OBJC_ELEM));

oelem->candidatename = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
oelem->classname = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
oelem->pname = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
oelem->ms = shChainNew("MODEL_ELEM");

strcpy(oelem->candidatename, candidatename);
strcpy(oelem->classname, classname);
strcpy(oelem->pname, pname);

if (strlen(pname) != 0) {
	oelem->ptype = ptype;
	} else {
	oelem->ptype = UNKNOWN_SCHEMA;
	}
if (strlen(candidatename) != 0) {
	oelem->candidatetype = candidatetype;
} else {
	oelem->candidatetype = UNKNOWN_OBJC;
	}
oelem->classtype = classtype;
oelem->classifier = classifier;
oelem->ptype = ptype;
oelem->compactor = compactor;

return(oelem);
}

void thObjcElemDel(OBJC_ELEM *objcelem) {
if (objcelem == NULL) return;
if (objcelem->candidatename != NULL) thFree(objcelem->candidatename);
if (objcelem->classname != NULL) thFree(objcelem->classname);
if (objcelem->ms != NULL) shChainDel(objcelem->ms);
if (objcelem->pname != NULL) thFree(objcelem->pname);

thFree(objcelem);
return;
}

PCOST_ELEM *thPCostElemNew(char *classname, char *rname1, char *rname2, void *pcost_function) {
char *name = "thPCostElemNewFromName";
if (classname == NULL || strlen(classname) == 0) {
	thError("%s: ERROR - object (classname) should be provided", name);
	return(NULL);
}
THOBJCTYPE classtype;
RET_CODE status;
status = thObjcTypeGetFromName(classname, &classtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get classtype for classname = '%s'", name, classname);
	return(NULL);
}

PCOST_ELEM *pcostelem = thCalloc(1, sizeof(PCOST_ELEM));

pcostelem->classname = thCalloc(MX_STRING_LEN, sizeof(char));
strcpy(pcostelem->classname, classname);
pcostelem->classtype = classtype;

pcostelem->rname1 = thCalloc(MX_STRING_LEN, sizeof(char));
pcostelem->rname2 = thCalloc(MX_STRING_LEN, sizeof(char));
char *rrname1 = NULL, *rrname2 = NULL;
if ((rname1 == NULL || strlen(rname1) == 0) && (rname2 == NULL || strlen(rname2) == 0)) {
	rrname1 = "";
	rrname2 = "";
} else if (rname1 == NULL || strlen(rname1) == 0) {
	rrname1 = rname2;
	rrname2 = "";
} else if (rname2 == NULL || strlen(rname2) == 0) {
	rrname1 = rname1;
	rrname2 = "";
} else {
	rrname1 = rname1;
	rrname2 = rname2;
}
strcpy(pcostelem->rname1, rrname1);
strcpy(pcostelem->rname2, rrname2);  
pcostelem->pcost_function = pcost_function;

return(pcostelem);
}

PCOST_ELEM *thPCostElemNewFromType(THOBJCTYPE classtype, char *rname1, char *rname2, void *pcost_function) {
char *name = "thPCostElemNewFromType";
char *classname;
RET_CODE status;
status = thObjcNameGetFromType(classtype, &classname);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get classname for classtype = '%x'", name, classtype);
	return(NULL);
}

PCOST_ELEM *pcostelem = thCalloc(1, sizeof(PCOST_ELEM));

pcostelem->classname = thCalloc(MX_STRING_LEN, sizeof(char));
strcpy(pcostelem->classname, classname);
pcostelem->classtype = classtype;

pcostelem->rname1 = thCalloc(MX_STRING_LEN, sizeof(char));
pcostelem->rname2 = thCalloc(MX_STRING_LEN, sizeof(char));
char *rrname1 = NULL, *rrname2 = NULL;
if (rname1 == NULL || strlen(rname1) == 0) {
	rrname1 = rname2;
	rrname2 = rname1;
} else {
	rrname1 = rname1;
	rrname2 = rname2;
}
if (rname1 != NULL) strcpy(pcostelem->rname1, rrname1);
if (rname2 != NULL) strcpy(pcostelem->rname2, rrname2);  
pcostelem->pcost_function = pcost_function;

return(pcostelem);
}



void thPCostElemDel(PCOST_ELEM *pcostelem) {
if (pcostelem == NULL) return;
if (pcostelem->classname != NULL) thFree(pcostelem->classname);
if (pcostelem->rname1 != NULL) thFree(pcostelem->rname1);
if (pcostelem->rname2 != NULL) thFree(pcostelem->rname2);
thFree(pcostelem);
return;
}

RET_CODE thObjcElemAddModel(OBJC_ELEM *objcelem, char *mname) {
char *name = "thObjcElemAddModel";

if (objcelem == NULL && mname == NULL) {
	thWarn("%s: WARNING - null input, performing null", name);
	return(SH_SUCCESS);
}
if (mname == NULL || strlen(mname) == 0) {
	thError("%s: ERROR - no model name provided", name);
	return(SH_GENERIC_ERROR);
}
if (objcelem == NULL) {
	thError("%s: ERROR - null (objc_elem)", name);
	return(SH_GENERIC_ERROR);
}

MODEL_ELEM *me;
if (objcelem->ms == NULL) objcelem->ms = shChainNew("MODEL_ELEM");
int i, n;
n = shChainSize(objcelem->ms);
for (i = 0; i < n; i++) {
	me = shChainElementGetByPos(objcelem->ms, i);
	if (!strcmp(me->mname, mname)) {
		thWarn("%s: WARNING - model (%s) already declared as a component of the object (%s)", name, mname, objcelem->classname);
		return(SH_SUCCESS);
	}
}

RET_CODE status;
if (objcelem->candidatetype != UNKNOWN_OBJC && objcelem->candidatetype != PHOTO_OBJC) {
	OBJC_ELEM *oe;
	status = thObjcElemGetByName(objcelem->candidatename, &oe);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not locate the parent object type (%s) in the Object Bank", name, objcelem->candidatename);
		return(SH_GENERIC_ERROR);
	}
	if (oe->ms == NULL) {
		thError("%s: ERROR - parent object definition (%s) does not include any component models", name, oe->classname);
		return(SH_GENERIC_ERROR);
	}
	n = shChainSize(oe->ms);
	for (i = 0; i < n; i++) {
		me = shChainElementGetByPos(oe->ms, i);
		if (!strcmp(me->mname, mname)) {
			shChainElementAddByPos(objcelem->ms, me, "MODEL_ELEM", TAIL, AFTER);
			return(SH_SUCCESS);
		}
	}
	thError("%s: ERROR - model (%s) is not a component of the parent object type (%s)", name, mname, oe->classname);
	return(SH_GENERIC_ERROR);
}

status = thModelElemGetByName(mname, &me);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not locate model (%s) in Model Bank", name, mname);
	return(status);
}

shChainElementAddByPos(objcelem->ms, me, "MODEL_ELEM", TAIL, AFTER);
return(SH_SUCCESS);
}

RET_CODE thObjcElemGetByName(char *objcname, OBJC_ELEM **objcelem) {
char *name = "thObjcElemGetByName";
if (objcelem == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (objcname == NULL && objcelem != NULL) {
	thError("%s: ERROR - cannot search for null in the object bank", name);
	*objcelem = NULL;
	return(SH_GENERIC_ERROR);
	}
if (OBank == NULL) {
	thError("%s: ERROR - you need to initiate Object Bank before searching within it", name);
	return(SH_GENERIC_ERROR);
}

OBJC_ELEM *oe;
int i, n;
n = shChainSize(OBank);
for (i = 0; i < n; i++) {
	oe = shChainElementGetByPos(OBank, i);
	if (oe != NULL && !strcmp(oe->classname, objcname)) {
		*objcelem = oe;
		return(SH_SUCCESS);
	}
}

thError("%s: ERROR - could not locate object (%s) in the Object Bank", name, objcname);
if (objcelem != NULL) *objcelem = NULL;
return(SH_GENERIC_ERROR);

}

RET_CODE thObjcElemGetByType(THOBJCTYPE objctype, OBJC_ELEM **objcelem) {
char *name = "thObjcElemGetByType";
if (objctype == UNKNOWN_OBJC) {
	thError("%s: ERROR - (there is no (objc_elem) for (unknown_objc)", name);
	return(SH_GENERIC_ERROR);
}

if (OBank == NULL) {
	thError("%s: ERROR - initiate Object Bank before referring to any types", name);
	return(SH_GENERIC_ERROR);
}

OBJC_ELEM *oe;
int i, n;
n = shChainSize(OBank);

for (i = 0; i < n; i++) {
	oe = shChainElementGetByPos(OBank, i);
	if (oe->classtype == objctype) {
		if (objcelem != NULL) {
			*objcelem = oe;
		} else {
			thWarn("%s: WARNING - null place holder for (objc_elem)", name);
		}
		return(SH_SUCCESS);
	}
}

if (objcelem != NULL) *objcelem = NULL;
return(SH_GENERIC_ERROR);
}


RET_CODE thObjcElemAddToObjcBank(OBJC_ELEM *objcelem) {
char *name = "thObjcElemAddToObjcBank";
if (objcelem == NULL) {
	thWarn("%s: WARNING - null input, performing null", name);
	return(SH_SUCCESS);
}

if (OBank == NULL) {
	thError("%s: ERROR - initiate Object Bank before adding any objects to it", name);
	return(SH_GENERIC_ERROR);
	}

OBJC_ELEM *oe;
int removed = 0, i, n;
n = shChainSize(OBank);
for (i = 0; i < n; i++) {
	oe = shChainElementGetByPos(OBank, i);
	if (oe != NULL && !(strcmp(oe->classname, objcelem->classname))) {
		thError("%s: WARNING - an object of the same name (%s) found in the Object Bank - removing", name, oe->classname);
		removed++;
		shChainElementRemByPos(OBank, i);
		thObjcElemDel(oe);
	} else if (oe != NULL && oe->classtype == objcelem->classtype) {
		thError("%s: ERROR - an object of the same class type (%s) as the given class (%s) found in the Object Bank", name,
		oe->classname, objcelem->classname);
		return(SH_GENERIC_ERROR);
	}
}

shChainElementAddByPos(OBank, objcelem, "OBJC_ELEM", TAIL, AFTER);
return(SH_SUCCESS);
}

RET_CODE thObjcElemDelFromBankByType(THOBJCTYPE classtype) {
char *name = "thObjcElemDelFromBankByType";
if (OBank == NULL) {
	thError("%s: ERROR - initiate Object Bank before adding any objects to it", name);
	return(SH_GENERIC_ERROR);
	}

OBJC_ELEM *oe;
int i, n, posfound = -1;
n = shChainSize(OBank);
for (i = 0; i < n; i++) {
	oe = shChainElementGetByPos(OBank, i);
	if (oe->classtype == classtype) {
		shChainElementRemByPos(OBank, i);
		thObjcElemDel(oe);
		posfound = i;
	}
}
if (posfound == -1) {
	thError("%s: WARNING - no object element of class type (%d) found in bank", name, (int) classtype);
	
}
return(SH_SUCCESS);
}

RET_CODE thObjcElemRemByName(char *oname, OBJC_ELEM **objcelem) {
char *name = "thObjcElemRemByName";

if (oname == NULL && objcelem == NULL) {
	thError("%s: null input, performing null", name);
	return(SH_SUCCESS);
}

if (oname == NULL || strlen(oname) == 0) {
	thError("%s: ERROR - null or empty class name for object", name);
	return(SH_GENERIC_ERROR);
}

if  (OBank == NULL) {
	thError("%s: Object Bank should be initiated before removing any object elements", name);
	return(SH_GENERIC_ERROR);
}

int i, n;
n = shChainSize(OBank);
OBJC_ELEM *oe;
for (i = 0; i < n; i++) {
	oe = shChainElementGetByPos(OBank, i);
	if (!strcmp(oe->classname, oname)) {
		shChainElementRemByPos(OBank, i);
		if (objcelem != NULL) {
			*objcelem = oe;
		} else {
			thObjcElemDel(oe);
		}
		return(SH_SUCCESS);
	}
}

thError("%s: ERROR - could not locate object type (%s) in the Object Bank", name, oname);
if (objcelem != NULL) *objcelem = NULL;
return(SH_GENERIC_ERROR);
}

RET_CODE thObjcElemGetModelElem(OBJC_ELEM *oelem, char *mname, MODEL_ELEM **melem) {
char *name = "thObjcElemGetModelElem";
if (oelem == NULL && mname == NULL && melem == NULL) {
	thWarn("%s: WARNING - null input, performing null", name);
	return(SH_SUCCESS);
}
if (oelem == NULL && mname != NULL) {
	thError("%s: ERROR - cannot search for model (%s) in a null (objc_elem)", name, mname);
	if (melem != NULL) *melem = NULL;
	return(SH_GENERIC_ERROR);
} 
if (oelem != NULL && mname == NULL) {
	thError("%s: ERROR - no model name provided to search within the objc_elem (%s)", name, oelem->classname);
	if (melem != NULL) *melem = NULL;
	return(SH_GENERIC_ERROR);
}
if (oelem == NULL && mname == NULL && melem != NULL) {
	thError("%s: ERROR - cannot search for a no-name model in a null (objc_elem)", name);
	*melem = NULL;
	return(SH_GENERIC_ERROR);
}

int i, n;
if (oelem->ms == NULL) {
	thError("%s: ERROR - no model available in definition of objc_elem (%s)", name, oelem->classname);
	return(SH_GENERIC_ERROR);
}
CHAIN *ms;
ms = oelem->ms;
n = shChainSize(ms);

MODEL_ELEM *me;
for (i = 0; i < n; i++) {
	me = shChainElementGetByPos(ms, i);
	if (!strcmp(me->mname, mname)) {
		if (melem != NULL) {
			*melem = me;
		} else {
			thError("%s: ERROR - null place holder for (model_elem)", name);
		}
		return(SH_SUCCESS);
	}
}
if (melem != NULL) *melem = NULL;
return(SH_GENERIC_ERROR);
}

RET_CODE thObjcElemGetNmodel(OBJC_ELEM *objcelem, int *nmodel) {
char *name = "thObjcElemGetNmodel";
shAssert(objcelem != NULL);
shAssert(nmodel != NULL);
CHAIN *ms = NULL;
ms = objcelem->ms;
if (ms == NULL) {
	thError("%s: ERROR - improperly allocated (objcelem) - empty model chain", name);
	return(SH_GENERIC_ERROR);
}
*nmodel = shChainSize(ms);
return(SH_SUCCESS);
}


/* parameter cost handling for objects */
RET_CODE thObjcElemAddPCostElem(OBJC_ELEM *oelem, PCOST_ELEM *pcostelem) {
char *name = "thObjcElemAddPCostElem";
if (oelem == NULL && pcostelem == NULL) {
	thError("%s: WARNING - null placeholders", name);
	return(SH_SUCCESS);
} else if (pcostelem == NULL) {
	thError("%s: WARNING - null (pcostelem)", name);
	return(SH_SUCCESS);
} else if (oelem == NULL) {
	thError("%s: ERROR - null (object_elem)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
char *rname1 = pcostelem->rname1;
char *rname2 = pcostelem->rname2;
PCOST_ELEM *pcostelem2 = NULL;
status = thObjcElemGetPCostElem(oelem, rname1, rname2,  &pcostelem2);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not seek for overlapping cost functions in object elemnt", name);
	return(SH_GENERIC_ERROR);
}
if (pcostelem2 != NULL) {
	thError("%s: ERROR - a similar (pcost_element) found in (object_element)", name);
	return(SH_GENERIC_ERROR);
}
if (oelem->pcs == NULL) oelem->pcs = shChainNew("PCOST_ELEM");
CHAIN *pcs = oelem->pcs;
shChainElementAddByPos(pcs, pcostelem, "PCOST_ELEM", TAIL, AFTER);
return(SH_SUCCESS);
}

RET_CODE thObjcElemGetPCostElem(OBJC_ELEM *oelem, char *rname1, char *rname2,  PCOST_ELEM **pcostelem) {
char *name = "thObjcElemGetPCostElem";
if (oelem == NULL) {
	thError("%s: ERROR - (object_element) not passed", name);
	return(SH_GENERIC_ERROR);
}
if (pcostelem == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
CHAIN *pcs = oelem->pcs;
if (pcs == NULL || shChainSize(pcs) == 0) {
	*pcostelem = NULL;
	return(SH_SUCCESS);
}
char *rrname1 = rname1;
char *rrname2 = rname2;
if (rrname1 == NULL) rrname1 = "";
if (rrname2 == NULL) rrname2 = "";
int found = 0, i, n;
n = shChainSize(pcs);
for (i = 0 ; i < n; i++) {
	PCOST_ELEM *pcostelem2 = shChainElementGetByPos(pcs, i);
	if (pcostelem2 != NULL) {
		char *sname1 = pcostelem2->rname1;
		char *sname2 = pcostelem2->rname2;
		if (sname1 == NULL) sname1 = "";
		if (sname2 == NULL) sname2 = "";
		if ((!strcmp(rrname1, sname1) && !strcmp(rrname2, sname2)) || 
			(!strcmp(rrname1, sname2) && !strcmp(rrname2, sname1))) {
			*pcostelem = pcostelem2;
			found++;
		}
	}
}
if (found == 0) {
	*pcostelem = NULL;
} else if (found > 1) {
	thError("%s: WARNING - more than one (%d)  match found, the last match returned", name, found);
}
return(SH_SUCCESS);
}

RET_CODE thObjcElemDelPCostElem(OBJC_ELEM *oelem, char *rname1, char *rname2) {
char *name = "thObjcElemDelPCostElem";
if (oelem == NULL) {
	thError("%s: ERROR - (object_element) not passed", name);
	return(SH_GENERIC_ERROR);
}
CHAIN *pcs = oelem->pcs;
if (pcs == NULL || shChainSize(pcs) == 0) {
	thError("%s: WARNING - no (pcost_elem) found in (object_elem)", name);
	return(SH_SUCCESS);
}
char *rrname1 = rname1;
char *rrname2 = rname2;
if (rrname1 == NULL) rrname1 = "";
if (rrname2 == NULL) rrname2 = "";
int found = 0, i, j = 0, n;
n = shChainSize(pcs);
for (i = 0 ; i < n; i++) {
	PCOST_ELEM *pcostelem2 = shChainElementGetByPos(pcs, j);
	if (pcostelem2 != NULL) {
		char *sname1 = pcostelem2->rname1;
		char *sname2 = pcostelem2->rname2;
		if (sname1 == NULL) sname1 = "";
		if (sname2 == NULL) sname2 = "";
		if ((!strcmp(rrname1, sname1) && !strcmp(rrname2, sname2)) || 
			(!strcmp(rrname1, sname2) && !strcmp(rrname2, sname1))) {
			shChainElementRemByPos(pcs, j);
			thPCostElemDel(pcostelem2);
			found++;
		} else {
			j++;
		} 
	} else {
		j++;
	}
}
if (found == 0) {
	thError("%s: WARNING - no match was found", name);
} else if (found > 1) {
	thError("%s: WARNING - more than one (%d) match found, all matches deleted", name, found);
}
return(SH_SUCCESS);
}

RET_CODE thObjcElemAddPCostFunction(OBJC_ELEM *oelem, char *rname1, char *rname2, void *pcost_function) {
char *name = "thObjcElemAddPCostFunction";
if (oelem == NULL) {
	thError("%s: ERROR - null (object_elem)", name);
	return(SH_GENERIC_ERROR);
}
char *classname = oelem->classname;
PCOST_ELEM *pcostelem = thPCostElemNew(classname, rname1, rname2, pcost_function);
RET_CODE status;
status = thObjcElemAddPCostElem(oelem, pcostelem);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add (pcost_elem) to (object_elem)", name);
	thPCostElemDel(pcostelem);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE thObjcElemGetPCostFunction(OBJC_ELEM *oelem, char *rname1, char *rname2, void **pcost_function) {
char *name = "thObjcElemGetPCostFunction";
if (oelem == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (pcost_function == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}

RET_CODE status;
PCOST_ELEM *pcostelem = NULL;
status = thObjcElemGetPCostElem(oelem, rname1, rname2,  &pcostelem); 
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not obtain (pcost_elem) from (objc_elem)", name);
	return(status);
}
if (pcostelem == NULL) {
	*pcost_function = NULL;
} else {
	*pcost_function = pcostelem->pcost_function;
}
return(SH_SUCCESS);
}

RET_CODE thPCostElemAddByClassName(char *classname, PCOST_ELEM *pcostelem) {
char *name = "thPCostElemAddByClassName";
OBJC_ELEM *oelem = NULL;
RET_CODE status;
status = thObjcElemGetByName(classname, &oelem);  
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (objc_elem) for (classname = '%s')", name, classname);
	return(status);
}
status = thObjcElemAddPCostElem(oelem, pcostelem);  
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add (pcost_elem) to (objc_elem)", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE thObjcClassAddPCostFunc(char *classname, char *rname1, char *rname2, void *pcost_function) {
char *name = "thObjcClassAddPCostFunc";
OBJC_ELEM *oelem = NULL;
RET_CODE status;
status = thObjcElemGetByName(classname, &oelem);  
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (objc_elem) for (classname = '%s')", name, classname);
	return(status);
}
status = thObjcElemAddPCostFunction(oelem, rname1, rname2, pcost_function);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add (pcost_function) to (objc_elem) for (classname = '%s')", name, classname);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE thPCostElemGetByClassName(char *classname, char *rname1, char *rname2, PCOST_ELEM **pcostelem) {
char *name = "thPCostElemGetByClassName";
OBJC_ELEM *oelem = NULL;
RET_CODE status;
status = thObjcElemGetByName(classname, &oelem);  
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (objc_elem) for (classname = '%s')", name, classname);
	return(status);
}
status = thObjcElemGetPCostElem(oelem, rname1, rname2,  pcostelem); 
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (pcost_elem) from (object_elem) for (classname = '%s')", name, classname);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE thPCostFunctionGetByClassName(char *classname, char *rname1, char *rname2, void **pcost_function) {
char *name = "thPCostFunctionGetByClassName";
OBJC_ELEM *oelem = NULL;
RET_CODE status;
status = thObjcElemGetByName(classname, &oelem);  
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (objc_elem) for (classname = '%s')", name, classname);
	return(status);
}
status = thObjcElemGetPCostFunction(oelem, rname1, rname2, pcost_function);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (pcost_function) from (objc_elem) for (classname = '%s')", name, classname);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE thObjcGetPCostElem(THOBJC *objc, char *rname1, char *rname2, PCOST_ELEM **pcostelem) {
char *name = "thObjcGetPCostElem";
char *classname = NULL;
RET_CODE status;
status = thObjcGetClassName(objc, &classname);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get classtype for objc", name);
	return(status);
}
status = thPCostElemGetByClassName(classname, rname1, rname2, pcostelem);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get  (pcost_elem) for (classname = '%s')", name, classname);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE thObjcGetPCostFunction(THOBJC *objc, const char *rname1, const char *rname2, void **pcost_function) {
char *name = "thObjcGetPCostFunction";
if (objc == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (pcost_function == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
char *classname = NULL;
RET_CODE status;
status = thObjcGetClassName(objc, &classname);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get classtype for objc", name);
	return(status);
}
status = thPCostFunctionGetByClassName(classname, rname1, rname2, pcost_function); 
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not  get (pcost_function) for (classname = '%s')", name, classname);
	return(status);
}
return(SH_SUCCESS);
}

/* initiators and de-initiators of Model and Object Banks */
RET_CODE thModelInit() {
char *name = "thModelInit";

static int init = 0;
if (OBank != NULL || MBank != NULL) {
	thError("%s: ERROR - model and object banks already initiated, need to call terimnator before re-initiating", name);
	return(SH_GENERIC_ERROR);
}

if (init) thWarn("%s: WARNING - initiating model and object banks for th %d-th time during a single run", name, init + 1);

MBank = shChainNew("MODEL_ELEM");
OBank = shChainNew("OBJC_ELEM");

OBJC_ELEM *oe;

oe = thObjcElemNew(NULL, "UNKNOWN_OBJC", NULL, NULL, NULL);
oe->classtype = UNKNOWN_OBJC;
thObjcElemAddToObjcBank(oe);

#if USE_FNAL_OBJC_IO
oe = thObjcElemNew(NULL, "PHOTO_OBJC", NULL, "FNAL_OBJC_IO", NULL);
#else
oe = thObjcElemNew(NULL, "PHOTO_OBJC", NULL, "OBJC_IO", NULL);
#endif
oe->classtype = PHOTO_OBJC;
thObjcElemAddToObjcBank(oe);

init++;
return(SH_SUCCESS);
}

RET_CODE thModelTerminate() {
char *name = "thModelTerminate";
if (OBank == NULL && MBank == NULL) {
	thError("%s: ERROR - object and model banks never initiated, could not terminate", name);
	return(SH_GENERIC_ERROR);
}

if (MBank != NULL) shChainDestroy(MBank, &thModelElemDel);
if (OBank != NULL) shChainDestroy(OBank, &thObjcElemDel);
MBank = NULL;
OBank = NULL;

return(SH_SUCCESS);
}





/* TYPE and NAME managekent for Model and Object Banks */

#if 0
RET_CODE thModelTypeGetFromName(char *mname, THMODELTYPE *mtype);
RET_CODE thModelNameGetFromType(THMODELTYPE mtype, char **mname);
RET_CODE thModelTypeGetNextAvailable(THMODELTYPE *type);
#endif

RET_CODE thObjcTypeGetFromName(char *oname, THOBJCTYPE *otype) {
char *name = "thObjcTypeGetFromName";
if (oname == NULL) {
	thError("%s: ERROR - null input", oname);
	if (otype != NULL) *otype = UNKNOWN_OBJC;
	return(SH_GENERIC_ERROR);
}
if (OBank == NULL) {
	thError("%s: ERROR - initiate the Object Bank before referring to any types", name);
	if (otype != NULL) *otype = UNKNOWN_OBJC;
	return(SH_GENERIC_ERROR);
}

int i, n;
n = shChainSize(OBank);
OBJC_ELEM *oe;
for (i = 0; i < n; i++) {
	oe = shChainElementGetByPos(OBank, i);
	if (!strcmp(oe->classname, oname)) {
		if (otype != NULL) {
			*otype = oe->classtype;
		} else {
			thWarn("%s: WARNING - null place holder for object type", name);
		}
		return(SH_SUCCESS);
	}
 }
if (otype != NULL) *otype = UNKNOWN_OBJC;
return(SH_GENERIC_ERROR);
}

RET_CODE thObjcNameGetFromType(THOBJCTYPE otype, char **oname) {
char *name = "thObjcNameGetFromType";
if (otype == UNKNOWN_OBJC) {
	thError("%s: ERROR - (uknown_objc) does not have a type name associated with it", name);
	if (oname != NULL) *oname = NULL;
	return(SH_GENERIC_ERROR);
}

if (OBank == NULL) {
	thError("%s: ERROR - initiate Object Bank before referring to any types", name);
	return(SH_GENERIC_ERROR);
}

int i, n;
n = shChainSize(OBank);
OBJC_ELEM *oe;
for (i = 0; i < n; i++) {
	oe = shChainElementGetByPos(OBank, i);
	if (oe->classtype == otype) {
		if (oname != NULL) {
			*oname = oe->classname;
		} else {
			thWarn("%s: WARNING - null place holder for object type", name);
		}
		return(SH_SUCCESS);
	}
}
if (oname != NULL) *oname = NULL;
return(SH_GENERIC_ERROR);
}

RET_CODE thObjcTypeGetNextAvailable(THOBJCTYPE *type) {
char *name = "thObjcTypeGetNextAvailable";
if (type == NULL) {
	thWarn("%s: WARNING - null place holder", name);
	return(SH_SUCCESS);
}
THOBJCTYPE t;
if (OBank == NULL) {
	thError("%s: ERROR - initiate Object Bank before referring to any types", name);
	return(SH_GENERIC_ERROR);
	}

int i, n;
n = shChainSize(OBank);

OBJC_ELEM *oe;
t = UNKNOWN_OBJC;
for (i = 0; i < n; i++) {
	oe = shChainElementGetByPos(OBank, i);
	t = MAX(t, oe->classtype);
}
if (t >= MX_THOBJCTYPE) {
	thError("%s: ERROR - no available type", name);
	if (type != NULL) *type = UNKNOWN_OBJC;
	return(SH_GENERIC_ERROR);
} else {
	t++;
	if (type != NULL) *type = t;
	return(SH_SUCCESS);
}

}

/* image calculations */
RET_CODE thObjcClassify(THOBJC *objc) {
 /* classify before fit */
char *name = "thObjcClassify";
if (objc == NULL) {
	thError("%s: WARNING - null input, peforming null", name);
	return(SH_SUCCESS);
}

if (OBank == NULL) {
	thError("%s: ERROR - need to initiate the Object Bank before classifying any object", name);
	return(SH_GENERIC_ERROR);
}
int objcid;
objcid = (int) objc->thid;

int sdssid;
sdssid = (int) objc->sdssid;
char *sdssname;
OBJ_TYPE sdsstype;
sdssname = objc->sdssname;
sdsstype = objc->sdsstype;

PHPROPS *phprop;
phprop = objc->phprop;

/* go over all object types, deduce their classifiers and then see if they past the test */

int i, n;
n = shChainSize(OBank);
OBJC_ELEM *oe;

int found = 0;
int (*classifier)(PHPROPS *);

char *classname = NULL;
for (i = 0; i < n; i++) {
	oe = shChainElementGetByPos(OBank, i);
	if (oe != NULL) {
		if ((oe->classifier != NULL) && ((oe->candidatetype == UNKNOWN_OBJC || oe->candidatetype == PHOTO_OBJC))) {
			classifier = oe->classifier;
			if (classifier == NULL) {
				thError("%s: ERROR - null classifier found for object (%d) in the bank", name, i);
				return(SH_GENERIC_ERROR);
			}
			if (classifier(phprop)) {	
				if (found == 0) {
					objc->thobjctype = oe->classtype;
					classname = oe->classname;
				}
				found++;
			}
		}
	}
}

if (!found) {
	#if WARN_CLASSIFY 
	thError("%s: WARNING - object's prop didn't match any class (objc: %d, sdssid: %d, sdsstype: '%s', %d)",
	name, objcid, sdssid, sdssname, sdsstype);
	#endif
	return(SH_SUCCESS);
} else if (found > 1) {
	thError("%s: WARNING - object matched (%d > 1) types (objcid: %d, sdssid: %d, classname: %s)", 
		name, found, objcid, sdssid, classname);
}

return(SH_SUCCESS);
}


RET_CODE thObjcForceClassifyByName(THOBJC *objc, char *classname) {
 /* classify before fit */
char *name = "thObjcForceClassifyByName";
if (objc == NULL) {
	thError("%s: WARNING - null input, peforming null", name);
	return(SH_SUCCESS);
}
if (classname == NULL || strlen(classname) == 0) {
	thError("%s: ERROR - null or empty classname", name);
	return(SH_GENERIC_ERROR);
}

if (OBank == NULL) {
	thError("%s: ERROR - need to initiate the Object Bank before classifying any object", name);
	return(SH_GENERIC_ERROR);
}
int objcid;
objcid = (int) objc->thid;

int sdssid;
sdssid = (int) objc->sdssid;
char *sdssname;
OBJ_TYPE sdsstype;
sdssname = objc->sdssname;
sdsstype = objc->sdsstype;

PHPROPS *phprop;
phprop = objc->phprop;

/* go over all object types, deduce their classifiers and then see if they past the test */

int i, n;
n = shChainSize(OBank);
OBJC_ELEM *oe;

int found = 0;
for (i = 0; i < n; i++) {
	oe = shChainElementGetByPos(OBank, i);
	if (oe != NULL) {
		if (!strcmp(oe->classname, classname)) {
			found++;
			objc->thobjctype = oe->classtype;
		}
	}
}

if (!found) {
	#if WARN_CLASSIFY 
	thError("%s: WARNING - object class name could not be found in the bank (objc: %d, sdssid: %d, sdsstype: '%s', %d, classname: '%s')",
	name, objcid, sdssid, sdssname, sdsstype, classname);
	#endif
	return(SH_SUCCESS);
} else if (found > 1) {
	thError("%s: WARNING - object matched (%d > 1) types (objcid: %d, sdssid: %d, classname: %s)", 
		name, found, objcid, sdssid, classname);
}

return(SH_SUCCESS);
}

RET_CODE thObjcForceClassifyByType(THOBJC *objc, THOBJCTYPE classtype) {
 /* classify before fit */
char *name = "thObjcForceClassifyByType";
if (objc == NULL) {
	thError("%s: WARNING - null input, peforming null", name);
	return(SH_SUCCESS);
}

if (OBank == NULL) {
	thError("%s: ERROR - need to initiate the Object Bank before classifying any object", name);
	return(SH_GENERIC_ERROR);
}
int objcid;
objcid = (int) objc->thid;

int sdssid;
sdssid = (int) objc->sdssid;
char *sdssname;
OBJ_TYPE sdsstype;
sdssname = objc->sdssname;
sdsstype = objc->sdsstype;

PHPROPS *phprop;
phprop = objc->phprop;

/* go over all object types, deduce their classifiers and then see if they past the test */

int i, n;
n = shChainSize(OBank);
OBJC_ELEM *oe;

int found = 0;
char *classname = NULL;
for (i = 0; i < n; i++) {
	oe = shChainElementGetByPos(OBank, i);
	if (oe != NULL) {
		if (oe->classtype == classtype) {
			found++;
			objc->thobjctype = oe->classtype;
			classname = oe->classname;
		}
	}
}

if (!found) {
	#if WARN_CLASSIFY 
	thError("%s: WARNING - object class name could not be found in the bank (objc: %d, sdssid: %d, sdsstype: '%s', %d, classtype: '%d')",
	name, objcid, sdssid, sdssname, sdsstype, (int) classtype);
	#endif
	return(SH_SUCCESS);
} else if (found > 1) {
	thError("%s: WARNING - object matched (%d > 1) types (objcid: %d, sdssid: %d, classtype: %d)", 
		name, found, objcid, sdssid, (int) classtype);
}

return(SH_SUCCESS);
}



RET_CODE thObjcReclassify(THOBJC *objc) {
char *name = "thObjcReclassify";
if (objc == NULL) {
	thWarn("%s: WARNING - null input, peforming null", name);
	return(SH_SUCCESS);
}


if (OBank == NULL) {
	thError("%s: ERROR - need to initiate the Object Bank before classifying any object", name);
	return(SH_GENERIC_ERROR);
}

THOBJCID objcid;
objcid = objc->thid;

long int sdssid;
sdssid = objc->sdssid;

CHAIN *thprop;
thprop = objc->thprop;

THOBJCTYPE objctype;
objctype = objc->thobjctype;

if  (objctype == PHOTO_OBJC || objctype == UNKNOWN_OBJC) {
	thError("%s: ERROR - object needs to be analyzed before re-classification (objc: 0x%d, sdssid: 0x%d)", name, objcid, sdssid);
	return(SH_GENERIC_ERROR);
}

/* go over all object types, deduce their classifiers and then see if they past the test */

int i, n;
n = shChainSize(OBank);
OBJC_ELEM *oe;

int found = 0;
int (*classifier)(CHAIN *);

for (i = 0; i < n; i++) {
	oe = shChainElementGetByPos(OBank, i);
	if (oe->candidatetype == objc->thobjctype) {
		classifier = oe->classifier;
		if (classifier(thprop)) {
			found++;
			objc->thobjctype = oe->classtype;
		}
	}
}
		
if (found == 0) {
	thError("%s: ERROR - object's PHOTO properties did not match any secondary object type (objc: 0x%d, sdssid: 0x%d)", name, objcid, sdssid);
	return(SH_GENERIC_ERROR);
} else if (found > 1) {
	thError("%s: ERROR - object's PHOTO propties matched multiple (%d) secondary object types (objc: 0x%d, sdssid: 0x%d)", name, found, objcid, sdssid);
	return(SH_GENERIC_ERROR);
	}

return(SH_SUCCESS);
}


RET_CODE thObjcInitMpars(THOBJC *objc) { 
/* takes a classified object and generates the parameter space for different models */ 
char *name = "thObjcInitMpars";
if (objc == NULL) {
	thWarn("%s: WARNING - null input, peforming null", name);
	return(SH_SUCCESS);
}

RET_CODE status;
OBJC_ELEM *oe;
status = thObjcElemGetByType(objc->thobjctype, &oe);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not find the object definition for type in (objc: %s)", name, objc->thid);
	return(status);
}

#if 0
/* if this section is included, it initiates all models for the parent object type */
if  (oe->candidatype != UNKNOWN_OBJC && oe->candidatetype != PHOTO_OBJC) {
	OBJC_ELEM *ce;
	status = thObjcElemGetByType(oe->candidatetype,  &ce);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not find  the parent type for type (%s) in (objc: %d)", name, oe->classname, objc->thid);
		return(status);
	}
	thError("%s: WARNING - invoking the model initiators in parent type (%s) for (objc: %d)", name, ce->classname, objc->thid);
	oe = ce;
}
#endif

/* go check thpars */
int band;
band = objc->band;

CHAIN *thprops;
if (objc->thprop == NULL) objc->thprop = shChainNew("THPROP");
thprops = objc->thprop;

PHPROPS *phprop;
phprop = objc->phprop;

int i, n;
n = shChainSize(thprops);
THPROP *prop;

int j, m;
m = shChainSize(oe->ms);

MODEL_ELEM *me;
for (i = 0; i < m; i++) {
	int found = 0;
	me = shChainElementGetByPos(oe->ms, i);
	for (j = 0; j < n; j++) {
		prop = shChainElementGetByPos(thprops, j);
		found = !strcmp(me->mname, prop->mname);
		if (found) break;
	}
	if (!found) {
		prop = thPropNew(me->mname, me->pname, NULL);
		shChainElementAddByPos(thprops, prop, "THPROP", TAIL, AFTER);
		} else if (strcmp(prop->pname, me->pname)) {
			thError("%s: ERROR - model (%s) accepts a parameter of type (%s), found one of type (%s) in (objc: %d)",
				name, me->mname, me->pname, prop->pname, objc->thid);
			return(SH_GENERIC_ERROR);
		}
	if (prop->value == NULL && me->ptype != UNKNOWN_SCHEMA) {
		SCHEMA *s;
		s = shSchemaGetFromType(me->ptype);
		prop->value = s->construct();
		}
	RET_CODE (*init)(PHPROPS *, void *, int);
	init = me->init;
	if (prop->value != NULL && init != NULL && me->ptype != UNKNOWN_SCHEMA) {
		status = init(phprop, prop->value, band);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not initiate parameter for model (%s) in (objc: %d)", 
					name, me->mname, objc->thid);
			return(status);
		}
		RET_CODE (*transform)(void *, char *);
		transform = me->transform;
		if (transform != NULL) {
			status = transform(prop->value, me->pname);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not transform model parameter '%s' to valid coordinate description", name, me->pname);
			}
		}
	}
}

return(SH_SUCCESS);
}

RET_CODE thObjcInitModel(THOBJC *objc, char *mname){ /* initiate the parameters for the model */
char *name = "thObjcInitModel";
if (objc == NULL && mname == NULL) {
	thWarn("%s: WARNING - null input, peforming null", name);
	return(SH_SUCCESS);
}

RET_CODE status;
OBJC_ELEM *oe;
status = thObjcElemGetByType(objc->thobjctype, &oe);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not find the object definition for type in (objc: %s)", name, objc->thid);
	return(status);
}

#if 0
/* if this section is inlcuded it initiates all models for the parent type */ 
if  (oe->candidatype != UNKNOWN_OBJC && oe->candidatetype != PHOTO_OBJC) {
	OBJC_ELEM *ce;
	status = thObjcElemGetByType(oe->candidatetype,  &ce);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not find  the parent type for type (%s) in (objc: %d)", name, oe->classname, objc->thid);
		return(status);
	}
	thError("%s: WARNING - invoking the model initiators in parent type (%s) for (objc: %d)", name, ce->classname, objc->thid);
	oe = ce;
}
#endif

/* go check thpars */

int band;
band = objc->band;
CHAIN *thprops;
if (objc->thprop == NULL) objc->thprop = shChainNew("THPROP");
thprops = objc->thprop;

PHPROPS *phprop;
phprop = objc->phprop;

int i, n;
n = shChainSize(thprops);

THPROP *prop;
MODEL_ELEM *me;
status = thObjcElemGetModelElem(oe, mname, &me);

int found = 0;
for (i = 0; i < n; i++) {
	prop = shChainElementGetByPos(thprops, i);
	found = !strcmp(me->mname, prop->mname);
	if (found) break;
}

if (!found) {
	prop = thPropNew(me->mname, me->pname, NULL);
	shChainElementAddByPos(thprops, prop, "THPROP", TAIL, AFTER);
} else {
	if (strcmp(prop->pname, me->pname)) {
		thError("%s: ERROR - model (%s) accepts a parameter of type (%s), found one of type (%s) in (objc: %d)",
			name, mname, me->pname, prop->pname, objc->thid);
		return(SH_GENERIC_ERROR);
	}
	if (prop->value == NULL && me->ptype != UNKNOWN_SCHEMA) {
		SCHEMA *s;
		s = shSchemaGetFromType(me->ptype);
		prop->value = s->construct();
	}
	RET_CODE (*init)(PHPROPS *, void *, int);
	init = me->init;
	if (prop->value != NULL && init != NULL && me->ptype != UNKNOWN_SCHEMA) {
		status = init(phprop, prop->value, band);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not initiate parameter for model (%s) in (objc: %d)", 
					name, me->mname, objc->thid);
			return(status);
		}
	}
}

return(SH_SUCCESS);
}

RET_CODE thObjcDoModelf0(THOBJC *objc, char *mname, THREGION *reg, THPIX *mcount, int index) {
char *name = "thObjcDoModelf0";
if (objc == NULL && mname == NULL && reg == NULL) {
	thWarn("%s: WARNING - null input, peforming null", name);
	return(SH_SUCCESS);
}
if (objc == NULL && mname != NULL) {
	thError("%s: ERROR - no object provided to calculate model for", name);
	return(SH_GENERIC_ERROR);
}

if (objc != NULL && mname == NULL) {
	thError("%s: ERROR - no model name provided to search in the object (objc: %d)", name, objc->thid);
	return(SH_GENERIC_ERROR);
}

if (objc != NULL && mname != NULL && reg == NULL) {
	thError("%s: ERROR - no output region provided to calculate model (%s) in object (objc: %d)", name, mname, objc->thid);
	return(SH_GENERIC_ERROR);
}

if (objc == NULL && mname == NULL && reg != NULL) {
	thError("%s: ERROR - no object or model provided, cannot fill the output region", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
OBJC_ELEM *oe;
status = thObjcElemGetByType(objc->thobjctype, &oe);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not find the type specified for object (objc: %d)", name, objc->thid);
	return(status);
}
MODEL_ELEM *me;
status = thObjcElemGetModelElem(oe, mname, &me);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - model (%s) is not a subcomponent of object type (%s) in (objc: %d)", 
		name, mname, oe->classname, objc->thid);
	return(status);
}
CONSTRUCTION_MODE cmode = me->cmode;

void *vf0;
status = thModelElemGetFunc(me, &vf0);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get main image function for model (%s) (objc: %d)", name, mname, objc->thid);
	return(status);
}

void *val;
CHAIN *thprops;
thprops = objc->thprop;
status = thPropChainGetByMname(thprops, mname, NULL, &val);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the parameter value for model (%s) in object (objc: %d)", name, mname, objc->thid);
	return(status);
}

if (cmode == WHOLEREGION) {
	
	RET_CODE (*f0)(void *, THREGION *, THPIX *, int);
	f0 = vf0;
	status = f0(val, reg, mcount, index);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not properly calculate the main image for model (%s) (objc: %d)", name, mname, objc->thid);
		return(status);
	}

} else if (cmode == PIXELINTEGRAL) {

	RET_CODE (*specify_region) (void *, THREGION *, int);
	specify_region = me->reg_generator;
	if (specify_region == NULL) {
		thError("%s: ERROR - null region generator in creation of component (%s) of model '%s' via pixel integration",
			name, index, mname);
		return(SH_GENERIC_ERROR);
	}
	status = specify_region(val, reg, index);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not specify a region for pixel integration of component (%d) of model '%s'", 
			name, index, mname);
		return(status);
	}
	status = DummyGenericPixelIntegrator(val, vf0, reg, mcount);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not integrate via generic pixel integrator, component (%d) of model '%s'", 
			name, index, mname); 
		return(status);
	}

} else {
	thError("%s: ERROR - unsupported construction mode for model '%s'", name, mname);
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

THOBJC *thObjcNewFromPhprop(PHPROPS *p, int band, FORCECLASS flag, char *classname){
/* this function classifies the object if possible */
char *name = "thObjcNewFromPhprop";

if (flag != CLASSIFY && flag != UNCLASSIFY && flag != FORCECLASSIFY) {
	thError("%s: ERROR - unknown classification flag", name);
	return(NULL);
}

RET_CODE status;
#if USE_CALIBPHOTOMGLOBAL
status = thPhpropsCalibrate(p, LUPTITUDE);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calibrate photometric properties", name);
	return(NULL);
}
#endif

WOBJC_IO *pobjc = NULL;
int *bndex = NULL;
status = thPhpropsGet(p, &pobjc, &bndex, NULL, NULL, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not obtain (pobjc) 'WOBJC_IO' from (p) 'PHPROPS'", 
		name);
	return(NULL);
}
THOBJCTYPE type;
THOBJC *objc = NULL;
objc = thObjcNew();

objc->band = band;
objc->bndex = bndex[band];
objc->phprop = p;
objc->thobjctype = PHOTO_OBJC;
objc->sdssid = pobjc->id;
objc->sdsstype = pobjc->objc_type;
strcpy(objc->sdssname, shEnumNameGetFromValue("OBJ_TYPE", pobjc->objc_type));
if (flag == CLASSIFY || flag == FORCECLASSIFY) {
	if (flag == CLASSIFY) {
		status = thObjcClassify(objc);
	} else if (flag == FORCECLASSIFY) {
		status = thObjcForceClassifyByName(objc, classname);
	}
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not classify object (sdssid: 0x%d)", name, objc->sdssid);
		thObjcDel(objc);
		return(NULL);
	} else if ((type = objc->thobjctype) == UNKNOWN_OBJC) {
		#if WARN_CLASSIFY
		thError("%s: WARNING - could not classify object (sdssid: %d)", name, objc->sdssid);
		#endif
		return(objc);
	} else if (type == PHOTO_OBJC) {
		#if WARN_CLASSIFY
		thError("%s: WARNING - object remains classified as PHOTO_OBJC (sdssid: %d)", name, objc->sdssid);
		#endif
		return(objc);
	} 

OBJC_ELEM *oe;
MODEL_ELEM *me;
THPROP *prop;
RET_CODE status;
status = thObjcElemGetByType(type, &oe);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - type not available in the object bank", name);
	thObjcDel(objc);
	return(NULL);
}
CHAIN *ms;
int i, nm;
ms = oe->ms;
nm = shChainSize(ms);
RET_CODE (*init)(PHPROPS *, void *, int);
char *mname;
SCHEMA *s;
void *v;
if (objc->thprop == NULL) objc->thprop = shChainNew("THPROP");
for (i = 0; i < nm; i++) {
	me = shChainElementGetByPos(ms, i);
	if (me != NULL) {
	mname = me->mname;
	if (me->pname == NULL || strlen(me->pname) == 0) {
		thError("%s: ERROR - null (pname) for model-elem (%d)", name, i);
		thObjcDel(objc);
		return(NULL);
	}
	s = shSchemaGetFromType(shTypeGetFromName(me->pname));
	if (s == NULL) {
		thError("%s: ERROR - unrecognized data type for model '%s', pname = '%s'", 
		name, mname, me->pname);
		thObjcDel(objc);
		return(NULL);
		}
	v = s->construct();
	if (p != NULL) {
		init = me->init;
		if (init == NULL) {
			thError("%s: ERROR - null initiator for model-elem (%d)", name, i);
			thObjcDel(objc);
			return(NULL);
		}
		status = init(p, v, band);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not initiate the model parameters ('%s') from SDSS values", name, mname);
			thObjcDel(objc);
			return(NULL);
		}
	}
	prop = thPropNew(mname, me->pname, v);
	shChainElementAddByPos(objc->thprop, prop, "THPROP", TAIL, AFTER);
	}
}

char *ioname;
ioname = oe->pname;
objc->phtype = UNKNOWN_SCHEMA;
strcpy(objc->ioname, ioname);
if (ioname == NULL || strlen(ioname) == 0) {
	thError("%s: ERROR - null or empty (ioname) for objc", name);
	thObjcDel(objc);
	return(NULL);
}
objc->iotype = shTypeGetFromName(ioname);
s = shSchemaGetFromType(objc->iotype);
if (s == NULL) {
	thError("%s: ERROR - cannot construct io-parameter for objc", name);
	thObjcDel(objc);
	return(NULL);
}

objc->ioprop = s->construct();
return(objc);

} else if (flag == UNCLASSIFY) {
	return(objc);
}

thError("%s: ERROR - source code error", name);
return(NULL);

}

RET_CODE thObjcRenewFromPhprop(THOBJC *objc){

/* this function classifies the object if possible */
char *name = "thObjcRenewFromPhProp";

if (objc == NULL) {
	thError("%s: ERROR - null (objc) passed - cannot renew", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
THOBJCTYPE type;

PHPROPS *p = objc->phprop;
int band = objc->band;
type = objc->thobjctype;

WOBJC_IO *pobjc = NULL;
int *bndex = NULL;
status = thPhpropsGet(p, &pobjc, &bndex, NULL, NULL, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not obtain (pobjc) 'WOBJC_IO' from (p) 'PHPROPS'", name);
	return(SH_GENERIC_ERROR);
}

if (objc->bndex != bndex[band]) {
	thError("%s: ERROR - (objc->bndex = %d) does not match the expected value (%d) while (band = %d)", name, objc->bndex, bndex[band], band);
	return(SH_GENERIC_ERROR);
}
if (type == PHOTO_OBJC) {
	thError("%s: ERROR - (objc) should be classified before - found 'PHOTO_OBJC' as type", name);
	return(SH_GENERIC_ERROR);
} else if (type == UNKNOWN_OBJC) {
	thError("%s: ERROR - (objc) should be classified before - found 'UNKNOWN_OBJC' as type", name);
	return(SH_GENERIC_ERROR);
}

OBJC_ELEM *oe;
MODEL_ELEM *me;
status = thObjcElemGetByType(type, &oe);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - type (%d) not available in the object bank", name, (int) type);
	return(SH_GENERIC_ERROR);
}

CHAIN *ms;
int i, nm;
ms = oe->ms;
nm = shChainSize(ms);

for (i = 0; i < nm; i++) {
	me = shChainElementGetByPos(ms, i);
	if (me != NULL) {
	char *mname = me->mname;

	THPROP *prop = NULL;
	status = thObjcGetPropByMname(objc, mname, &prop);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not find (thprop) for (mname = '%s') in (objc = %p)", name, mname, (void *) objc);
		return(status);
	} 
	void *v = prop->value;
	char *pname = prop->pname;
	if (pname == NULL || strlen(pname) == 0) {
		thError("%s: ERROR - null (pname) for (mname = '%s'", name, mname);
		return(SH_GENERIC_ERROR);
	}
	if (v == NULL) {
		thError("%s: ERROR - null variable found in (thprop) for (mname = '%s')", name, mname);
		return(SH_GENERIC_ERROR);
	}
	if (p != NULL) {
		RET_CODE (*init)(PHPROPS *, void *, int);
		init = me->init;
		if (init == NULL) {
			thError("%s: ERROR - null initiator for (mname = '%s')", name, mname);
			return(SH_GENERIC_ERROR);
		}
		status = init(p, v, band);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not initiation parameter for model '%s' from SDSS values", name, mname);
			return(status);
		}
	}
	}
}

return(SH_SUCCESS);
}




THOBJC *thObjcNewFromType(THOBJCTYPE type, PHPROPS *p, int band) {
char *name = "thObjcNewFromTypes";

THOBJC *objc;
objc = thObjcNew();

objc->band = band;
objc->phprop = p;
objc->thobjctype = type;

OBJC_ELEM *oe;
MODEL_ELEM *me;
THPROP *prop;
RET_CODE status;
status = thObjcElemGetByType(type, &oe);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - type not available in the object bank", name);
	thObjcDel(objc);
	return(NULL);
}
CHAIN *ms;
int i, nm;
ms = oe->ms;
nm = shChainSize(ms);
RET_CODE (*init)(PHPROPS *, void *, int);
char *mname;
SCHEMA *s;
void *v;
if (objc->thprop == NULL) objc->thprop = shChainNew("THPROP");
for (i = 0; i < nm; i++) {
	me = shChainElementGetByPos(ms, i);
	mname = me->mname;
	s = shSchemaGetFromType(shTypeGetFromName(me->pname));
	if (s == NULL) {
		thError("%s: ERROR - unrecognized data type for model '%s'", 
		name, mname);
		thObjcDel(objc);
		return(NULL);
		}
	v = s->construct();
	if (p != NULL) {
		init = me->init;
		status = init(p, v, band);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not initiate parameter for model '%s' from SDSS values", name, mname);
			thObjcDel(objc);
			return(NULL);
		}
	}
	prop = thPropNew(mname, me->pname, v);
	shChainElementAddByPos(objc->thprop, prop, "THPROP", TAIL, AFTER);
}

char *ioname;
ioname = oe->pname;
objc->phtype = UNKNOWN_SCHEMA;
strcpy(objc->ioname, ioname);
s = shSchemaGetFromType(objc->iotype = shTypeGetFromName(ioname));
if (s == NULL) {
	thError("%s: ERROR - cannot construct io-parameter for objc", name);
	thObjcDel(objc);
	return(NULL);
}
objc->ioprop = s->construct();

return(objc);
}


THOBJC *thObjcNewFromName(char *type, PHPROPS *p, int band) {
char *name = "thObjcNewFromName";
if (type == NULL || strlen(type) == 0) {
	thError("%s: ERROR - need to provide a proper object type", name);
	return(NULL);
}
THOBJCTYPE t;
RET_CODE status;
status = thObjcTypeGetFromName(type, &t);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get type for '%s'", name, type);
	return(NULL);
}
THOBJC *objc;
objc = thObjcNewFromType(t, p, band);
return(objc);
}



RET_CODE thObjcDoModelDf(THOBJC *objc, char *mname, char *rname, THREGION *reg, int index) {
char *name = "thObjcDoModelDf";
if (objc == NULL && mname == NULL && rname == NULL && reg == NULL) {
	thWarn("%s: WARNING - null input, peforming null", name);
	return(SH_SUCCESS);
}

if (rname == NULL) rname = "";

if (objc == NULL && mname != NULL) {
	thError("%s: ERROR - no object provided to calculate model for", name);
	return(SH_GENERIC_ERROR);
}
if (objc != NULL && mname == NULL) {
	thError("%s: ERROR - no model name provided to search in the object (objc: %d)", name, objc->thid);
	return(SH_GENERIC_ERROR);
}

if (objc != NULL && mname != NULL && reg == NULL) {
	thError("%s: ERROR - no output region provided to calculate derivate of model (%s) w.r.t. (%s) in object (objc: %d)", 
	name, mname, rname, objc->thid);
	return(SH_GENERIC_ERROR);
}

if (objc == NULL && mname == NULL && reg != NULL) {
	thError("%s: ERROR - no object or model provided, cannot fill the output region", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
OBJC_ELEM *oe;
status = thObjcElemGetByType(objc->thobjctype, &oe);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not find the type specified for object (objc: %d)", name, objc->thid);
	return(status);
}
MODEL_ELEM *me;
status = thObjcElemGetModelElem(oe, mname, &me);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - model (%s) is not a subcomponent of object type (%s) in (objc: %d)", 
		name, mname, oe->classname, objc->thid);
	return(status);
}
CONSTRUCTION_MODE cmode = me->cmode;

void *vDf;
status = thModelElemGetDeriv(me, rname, &vDf);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get derivate image function for model (%s) w.r.t. (%s) (objc: %d)", 
	name, mname, rname, objc->thid);
	return(status);
}

CHAIN *thprops;
thprops = objc->thprop;
void *val;
status = thPropChainGetByMname(thprops, mname, NULL, &val);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the parameter value for model (%s) in object (objc: %d)", name, mname, objc->thid);
	return(status);
}

if (cmode == WHOLEREGION) {
	
	RET_CODE (*Df)(void *, THREGION *, int);
	Df = vDf;
	status = Df(val, reg, index);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not properly calculate the derivative image for model (%s) w.r.t. (%s) (objc: %d)", 
		name, mname, rname, objc->thid);
		return(status);
	}

} else if (cmode == PIXELINTEGRAL) {

	RET_CODE (*specify_region) (void *, THREGION *, int);
	specify_region = me->reg_generator;
	if (specify_region == NULL) {
		thError("%s: ERROR - null region generator in creation of component (%s) of model '%s' via pixel integration",
			name, index, mname);
		return(SH_GENERIC_ERROR);
	}
	status = specify_region(val, reg, index);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not specify a region for pixel integration of component (%d) of model '%s'", 
			name, index, mname);
		return(status);
	}
	status = DummyGenericPixelIntegrator(val, vDf, reg, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not integrate via generic pixel integrator, component (%d) of model '%s'", 
			name, index, mname); 
		return(status);
	}

} else {
	thError("%s: ERROR - unsupported construction mode for model '%s'", name, mname);
	return(SH_GENERIC_ERROR);
}


return(SH_SUCCESS);
}

RET_CODE thObjcPackPars(THOBJC *objc) { /* take an object and packs the models pars into one single I/O partype */
char *name = "thObjcPackPars";
if (objc == NULL) {
	thWarn("%s: WARNING - null input, peforming null", name);
	return(SH_SUCCESS);
}

thError("%s: ERROR - under construction", name);
return(SH_GENERIC_ERROR);
}

/* VERIFICATION */

RET_CODE thModelElemVerify(MODEL_ELEM *me) {
char *name = "thModelElemVerify";
if (me == NULL) {
	thError("%s: ERROR - null (model elem)", name);	
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
char *mname = NULL;
TYPE ptype = UNKNOWN_SCHEMA;
char *pname = NULL;
void *init = NULL, *reg_generator = NULL, *psf_info_generator = NULL, 
	*psf_mask_generator = NULL, *amplitude_specifier = NULL;
CONSTRUCTION_MODE cmode = UNKNOWN_CONSTRUCTION;
THREGION_TYPE regtype = UNKNOWN_THREGION_TYPE;
int ncomponent = 0;
PSF_CONVOLUTION_TYPE psftype = UNKNOWN_PSF_CONVOLUTION;

status = thModelElemGet(me, &mname, &ptype, &pname,  
		&init, 
		&reg_generator, 
		&psf_info_generator, 
		&psf_mask_generator, 
		&amplitude_specifier, 
		&cmode, &regtype,&psftype, 
		&ncomponent);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get records from (model elem", name);
	return(status);
}
int badness = 0;
if (mname == NULL) {
	thError("%s: ERROR - null (mname)", name);
	badness++;
} else if (strlen(mname) == 0) {
	thError("%s: ERROR - empty string for (mname)", name);
	badness++;
}
if (ptype == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - undefined schema for parameter (ptype)", name);
	badness++;
}
if (pname == NULL) {
	thError("%s: ERROR - null parameter name (pname)", name);
	badness++;
} else if (strlen(pname) == 0) {
	thError("%s: ERROR - empty string for parameter name (pname)", name);
	badness++;
}
if (cmode == UNKNOWN_CONSTRUCTION) {
	thError("%s: ERROR - unknown construction mode (cmode)", name);
	badness++;
}
if (regtype == UNKNOWN_THREGION_TYPE) {
	thError("%s: ERROR - unknown (thregion) type", name);
	badness++;
}
if (ncomponent <= 0) {
	thError("%s: ERROR - unacceptable (ncomponent) (%d)", name, ncomponent);
	badness++;
}
if (psftype == UNKNOWN_PSF_CONVOLUTION) {
	thError("%s: ERROR - unknown psf convolution type", name);
	badness++;
}
if (init == NULL) {
	thError("%s: ERROR - null (init) function", name);
	badness++;
}
if (reg_generator == NULL) {
	thError("%s: ERROR - null (reg_generator) function", name);
	badness++;
}

if (badness != 0) {
	thError("%s: ERROR - found (%d) problematic records in (model elem)", 
		name, badness);
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE thModelBankVerify(void) {
char *name = "thModelBankCheckMem";
if (MBank == NULL) {
	thError("%s: WARNING - null model bank", name);
	return(SH_SUCCESS);
}
int n;
n = shChainSize(MBank);
if (n == 0) {
	thError("%s: WARNING - empty model bank", name);
	return(SH_SUCCESS);
}
RET_CODE status, fstatus = SH_SUCCESS;
int i, badelems = 0;
MODEL_ELEM *me;
for (i = 0; i < n; i++) {
	me = shChainElementGetByPos(MBank, i);
	status = thModelElemVerify(me);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - problematic (model element) at location (%d)", 
			name, i);
		fstatus = status;
		badelems++;
	}
}
if (badelems != 0) {
	thError("%s: ERROR - discovered (%d) problematic (model elemnts) out of (%d)",
		name, badelems, n);
	return(fstatus);
}

return(SH_SUCCESS);
}

RET_CODE thObjcGetNmodelByType(THOBJCTYPE objctype, int *nmodel) {
char *name = "thObjcGetNmodelByType";
if (nmodel == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
OBJC_ELEM *objcelem = NULL;
status = thObjcElemGetByType(objctype, &objcelem);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not find (objcelem) for (objctype = %d)", name, (int) objctype);
	return(status);
} else if (objcelem == NULL) {
	thError("%s: ERROR - received null (objcelem) while searching for (objctype = %d)", name, (int) objctype);
	return(SH_GENERIC_ERROR);
}
int this_nmodel = -1;
status = thObjcElemGetNmodel(objcelem, &this_nmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (nmodel) for (objcelem) for (objctype = %d)", name, (int) objctype);
	*nmodel = -1;
	return(status);
}
*nmodel = this_nmodel;

return(SH_SUCCESS);
}

RET_CODE thModelTestForRecordName(char *mname, char *rname, int *exists_in_model_par, int *exists_in_deriv_list) {
char *name = "thModelTestForRecordName";
if (mname == NULL || rname == NULL || exists_in_model_par == NULL || exists_in_deriv_list == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
MODEL_ELEM *me = NULL;
status = thModelElemGetByName(mname, &me);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (model_elem) for model '%s'", name, mname);
	return(status);
}
char *pname = NULL;
TYPE ptype = UNKNOWN_SCHEMA;
status = thModelElemGet(me, NULL, &ptype, &pname,  NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get necessary information from (model_elem) for model '%s'", name, mname);
	return(status);
}
if (ptype == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - parameter type for model '%s' is not declared", name, mname);
	return(SH_GENERIC_ERROR);
}
SCHEMA_ELEM *se = shSchemaElemGetFromType(ptype, rname);
*exists_in_model_par = (se != NULL);

void *Df = NULL;
status = thModelElemGetDeriv(me, rname, &Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not seek (Df) for record '%s' in model '%s'", name, rname, mname);
	return(status);
}
*exists_in_deriv_list = (Df != NULL);
return(SH_SUCCESS);
}


