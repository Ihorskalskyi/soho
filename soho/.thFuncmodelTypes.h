#ifndef FUNCMODELTYPES_H
#define FUNCMODELTYPES_H

#include "math.h"
#include "dervish.h"
#include "thConsts.h"
#include "thMapTypes.h"

/* N_FUNCMODEL_COMP is the total number of componets for a simple function,
this includes (real component in the direct space, real component in the inverse space, and imaginary component
in the inverse space)
*/

#define N_FUNCMODEL_COMP_INDEX 3
typedef enum funcmodel_comp_index {
	DIRECT_FUNCMODELINDEX, RE_INV_FUNCMODELINDEX, IM_INV_FUNCMODELINDEX
} FUNCMODEL_COMP_INDEX;


/*
notice we have change this to long which is 4 byte integer
#define FUNCMODEL_STAT_TYPE U16
*/

typedef enum th_funcmodel_stat_planes {
  TH_COMPLEX_FUNCMODEL_BITNO,
  TH_HAS_FFTW_FUNCMODEL_BITNO,
  TH_HAS_DIR_FUNCMODEL_BITNO,
  TH_CALCULATE_FUNCMODEL_BITNO,
  TH_SAVED_FUNCMODEL_BITNO,
  TH_UNKNOWN_FUNCMODEL_BITNO,
  N_TH_FUNCMODEL_STAT_PLANES
} TH_FUNCMODEL_STAT_PLANES;

#define COMPLEX_FUNCMODEL   (1 << TH_COMPLEX_FUNCMODEL_BITNO)
#define HAS_FFTW_FUNCMODEL  (1 << TH_HAS_FFTW_FUNCMODEL_BITNO)
#define HAS_DIR_FUNCMODEL   (1 << TH_HAS_DIR_FUNCMODEL_BITNO)
#define CALCULATE_FUNCMODEL (1 << TH_CALCULATE_FUNCMODEL_BITNO)
#define SAVED_FUNCMODEL     (1 << TH_SAVED_FUNCMODEL_BITNO)
#define UNKNOWN_FUNCMODEL   (1 << TH_UNKNOWN_FUNCMODEL_BITNO)


/* OBJCMODEL_STAT_TYPE
#define long U16
*/

typedef enum th_objcmodel_stat_planes {
  TH_FIT_OBJCMODEL_BITNO,  
  TH_PSFSTAR_OBJCMODEL_BITNO,
  TH_SKY_OBJCMODEL_BITNO,
  TH_UNKNOWN_OBJCMODEL_BITNO,
  N_TH_OBJCMODEL_STAT_PLANES 
} TH_OBJCMODEL_STAT_PLANES;

#define FIT_OBJCMODEL (1 << TH_FIT_OBJCMODEL_BITNO);
#define PSFSTAR_OBJCMODEL (1 << TH_PSFSTAR_OBJCMODEL_BITNO);
#define SKY_OBJCMODEL (1 << TH_SKY_OBJCMODEL_BITNO);
#define UNKNOWN_OBJCMODEL (1 << TH_UNKNOWN_OBJCMODEL_BITNO);

typedef struct funcmodel {

  /* name and id of the FUNCMODEL */
  char *funcid, *funcname;
  /* the FUNMODEL status as define above */
  U32 stat;
  /* parameter listing */
  PARLIST *parlist;
  /* the following is non-null if func is complex */
  CHAIN *components;
  /* the representation of a simple funcmodel has three components
	1. direct representation f(x1, x2)
        2. real part of its fftw RE f~(k1, k2)
   	3. imaginary part of its fftw IM f~(k1, k2)
  */
  REGION **regs; /* these are subregions of the working space */
  int ncomp;
  int *compindex;
  /* for SIMPLE FUNCMODELs the representation is given below */
  REGION *dirreg, *fftwreg;
  /* 
     there should be a decision criteria whether the value for 
     COMPLEX_FUNCMODELs are also saved in the regions declared 
     above
  */

  /* the rest is for housekeeping */
  /* expansion coefficient used for approximation purposes */
  FL32 coeff;
  int nrow, ncol;
 
  REGION **wregs;

} FUNCMODEL;


typedef struct objcmodel {

  /* type and id of the object model */
  char *objcid, *objctype;
  /* OBJCMODEL stat */
  U32 stat;
  /* parameter listing */
  PARLIST *parlist;
  /* components (funcmodels) */
  CHAIN *funcs;
  /* representation */
  REGION **regs;
  int ncomp;
  /* I doubt if the representation fields would ever be needed */

} OBJCMODEL;

FUNCMODEL *thFuncmodelNew ();
void thFuncmodelDel(FUNCMODEL *func);
int thFuncmodelGetNComponent (FUNCMODEL *func, RET_CODE *status);

OBJCMODEL *thObjcmodelNew ();
void thObjcmodelDel (OBJCMODEL *objc);
int thObjcmodelGetNFuncmodel (OBJCMODEL *objc, RET_CODE *status);

#endif
