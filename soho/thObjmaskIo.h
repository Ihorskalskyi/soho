
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include "strings.h"
#include "dervish.h"

#include "dervish.h"
#include "phPhotoFitsIO.h"
#include "phSpanUtil.h"
#include "phUtils.h"
#include "phObjc.h"
#include "phDataIo.h"
#include "prvt/region_p.h"

#include "sohoEnv.h"
#include "thDebug.h"
#include "thFpcIo.h"


#define FpmFileType STANDARD              /* FITS file Type: STANDARD, NONSTANDARD, IMAGE */
#define FpmFileDef  DEF_DEFAULT /* = DEF_NUM_OF_ELEMS    /* = DEF_NONE: thCFitsIo.h */
#define NROW_FPM 1489
#define NCOLUMN_FPM 2048
#define REGION_FLAGS_FPM NO_FLAGS
#define MASKVAL_FPM 1


REGION *thObjmaskReadFpm(char *file,  
			 const int row0,  const int column0,  
			 const int row1,  const int column1,
			 const int *planes,
			 RET_CODE *thStatus);
RET_CODE thWriteFpm(char *file, 
		    MASK *mask);

RET_CODE thCpFpm(char *infile, char *outfile, /* input and output filenames */
		 const int row0, const int column0, 
		 const int row1, const int column1, /* corners of the image in INFILE */
		 const int *planes);

