#include "strings.h"
#include "dervish.h"
#include "sohoEnv.h"
#include "thDebug.h"


MASK *thMaskReadFpm(char *file,  
		     const int row0,  const int column0,  
		     const int row1,  const int column1, 
		     RET_CODE *thStatus);

RET_CODE thWriteFpm(char *file, 
		    MASK *mask);

RET_CODE thCpFpm(char *infile, char *outfile, /* input and output filenames */
		 const int row0, const int column0, 
		 const int row1, const int column1 /* corners of the image in INFILE */);
