any_with_ext () ( 

local mydir="$1"
local mypattern="$2"
local any=false
if test -n "$(find $mydir -maxdepth 1 -name $mypattern -print -quit)"
then
any=true
fi
echo $any
)

function count_files_in_dir {
local name="count_files_in_dir"

local Options=$@
local Optnum=$#

local lpath=no
local path=empty

while getopts ':-:' OPTION ; do
  case "$OPTION" in
    -  ) [ $OPTIND -ge 1 ] && optind=$(expr $OPTIND - 1 ) || optind=$OPTIND
         eval OPTION="\$$optind"
         OPTARG=$(echo $OPTION | cut -d'=' -f2)
         OPTION=$(echo $OPTION | cut -d'=' -f1)
         case $OPTION in
	     --path       ) lpath=yes; path="$OPTARG"	 ;;
	     * )  echo "$name: invalide options (long)" ;;
         esac
       OPTIND=1
       shift
      ;;
    ? )  echo "$name: invalid options (short) "  ;;
  esac
done

if [ $lpath == no ]; then
	echo "$name: not enough arguments set"
	echo -1
	exit
fi
if [ "$path" == empty ]; then
	echo "$name: path not set"
	echo -1
	exit
fi
file=0
if [ -e $path ]; then
	for fd in `ls -A $path` ; do
		if [ -e "$fd" ]; then
			((file++))
		fi
	done
fi
echo $file
}


