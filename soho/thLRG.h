#ifndef THLRG_H
#define THLRG_H

#include "thMGalaxy.h"
#include "thCalib.h"

#define LRG_NOT   0x00
#define LRG_CUT_1 0x01
#define LRG_CUT_2 0x02
#define LRG_STATUS_UNKNOWN 0x10

int is_LRG(PHPROPS *objc);
RET_CODE thLRGSetPhprops(PHPROPS *objc);
RET_CODE thWObjcIoMakeRandomDeV2(WOBJC_IO *objc);
RET_CODE thWObjcIoMakeRandomPl(WOBJC_IO *objc);
#endif

