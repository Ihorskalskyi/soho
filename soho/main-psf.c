/* PHOTO libraries */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ftcl.h"
#include "dervish.h"
#include "strings.h"
#include "time.h"

#include "phUtils.h"

#include "phPhotoFitsIO.h"
#include "phSpanUtil.h"

#include "thMath.h" 
#include "thDebug.h" 
#include "thIo.h" 
#include "thAscii.h" 
#include "thProc.h" 
#include "thMask.h" 
#include "thMSky.h" 
#include "thObjcTypes.h"
#include "thMGalaxy.h"
#include "thAlgorithm.h"
#include "thMap.h"
#include "thMle.h"
#include "thResidual.h"
#include "thParse.h"

typedef enum tweakflag {
	ABSOLUTE, FRACTIONAL, N_TWEAKFLAG, UNKNOWN_TWEAKFLAG
	} TWEAKFLAG;

#define SIM_SKY 1
#define ADD_POISSON_NOISE 0
#define DO_BKGRND 0
#define FIT_SHAPE  0
#define FIT_RE 1
#define FIT_E 0
#define FIT_PHI 0
#define FIT_CENTER 0 
#define LOCK_CENTER 1
#define FORCE_INIT_PAR 0
#define STAR_CLASSIFICATION 1 /* testing star classification */
#define GALAXY_CLASSIFICATION 1 /* testing galaxy classification */	
#define TABLEROW 5
#define N_RANDOM_FRAMES 2
#define N_OUTPUT_OBJC 5
#define FIXED_SKY 0.0
#define I_SKY 0.0
#define EllDesc SUPERCANONICAL
#define NSIMOBJC 1
#define NFITOBJC 0
#define TWEAK 1
#define TWEAK_ALL 0
#define THIS_BAND R_BAND
/* image clustering and memory constants */
#define THMEMORY 1300
#define DO_GREEDY 1
#define DO_ALGORITHM 1
#define ALGORITHM_MANUAL 0

int verbose = 0;

static int nrow = 1489;
static int ncol = 2048; 
static int band = 0;
static THPIX SKY_FRACTIONAL_TWEAK = 0.5;
#if FORCE_INIT_PAR
static THPIX I_TWEAK = 0.1; 
static THPIX CENTER_TWEAK = 1.0;
static THPIX SHAPE_TWEAK = 1.0;
static THPIX RE_TWEAK = 1.0;
static THPIX PHI_TWEAK = 1.0;
static THPIX E_TWEAK = 1.0;
#else 
#if TWEAK

static THPIX I_TWEAK = 1; 
#if FIT_CENTER
static THPIX CENTER_TWEAK = 1.0;
#endif
#if FIT_SHAPE
static THPIX SHAPE_TWEAK = 1.0;
#if FIT_PHI
static THPIX PHI_TWEAK = 1.0;
#endif
#if FIT_E
static THPIX E_TWEAK = 1.0;
#endif
#endif

#endif
#endif

static THPIX BETA_SHAPE = 0.3;
#if FORCE_INIT_PAR
static THPIX ELLIPTICITY = 0.7;
static THPIX PHI = 1.0;
static THPIX RE = 10.0;
static THPIX STRETCHINESS = 1.0;
static THPIX INTENSITY = 50;
#endif

static void usage(void);
void chain_model_output(char *job, CHAIN *objclist, THOBJCTYPE otype);
void model_output(char *job, THOBJC *objc);
int compare_re(const void *x1, const void *x2);
int compare_count(const void *x1, const void *x2);
void test_sorted_chain(CHAIN *chain, int (*compare)(const void *, const void *));
/* void thChainQsort(CHAIN *chain,
             int (*compar)(const void *, const void *));
*/
extern double ddot_(int *, double [], int *, double [], int *);
extern float sdot_(int *, float [], int *, float [], int *);

static RET_CODE tweak(void *p, THPIX e_tweak, char *pname, char **rnames, int rcount, CHAIN *chain, TWEAKFLAG tflag);
static RET_CODE add_poisson_noise_to_image(REGION *im, CHAIN *ampl);
static RET_CODE tweak_init_pars(LSTRUCT *l, CHAIN *objclist);
static RET_CODE generate_random_star_pos(int xcmin, int xcmax, int ycmin, int ycmax, THPIX *xclist, THPIX *yclist, int nstar);

main(int ac, char *av[])
{

  char *name = "do-psf";
  /* Arguments */

/* 
  char *wingfile = "./fit/wing-psf-sdss.fits";
  char *correctedfile = "./fit/glued-psf-sdss.fits";
  char *klfile = "./fit/kl-psf-sdss.fits";

  char *avwingfile = "./fit/av-wing-psf-sdss.fits";
  char *avcorrectedfile = "./fit/av-glued-psf-sdss.fits";
  char *avklfile = "./fit/av-kl-psf-sdss.fits";
 

  char *radialfile = "./fit/all-psf-radial-sdss.txt";
  char *tablefile = "./fit/spline-table.txt";
*/

RET_CODE status;
PSF_PARSED_INPUT *input = thSimParsedInputNew();
status = psfparser(ac, av, input);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not parse input", name);
	return(-1);
}
printf("%s: *** input *** \n", name);
printf("%s: setupfile = %s\n", name, input->setupfile);
printf("%s: camcol    = %d\n", name, input->camcol);
printf("%s: band      = %d\n", name, input->band);
printf("%s: objcfile  = %s\n", name, input->objcfile);
printf("%s: framefile = %s\n", name, input->framefile);
printf("%s: *** output *** \n", name);
printf("%s: PSF report file (KL, wing, glued) : %s", name, input->psfreportfile);
printf("%s: *** end of input *** \n", name);

int noobjcfile, npsfstar, camcol, band, seed, field, run;
char *fname = NULL, *psfreportfile = NULL, *psfstatfile = NULL, *rerun = NULL, cband;
THPIX xcmin, xcmax, ycmin, ycmax, xc, yc;
CHAIN *myobjclist = NULL;
SKYPARS *mysky = NULL;

noobjcfile = (input->readobjcfile == 0);
fname = input->framefile;
camcol = input->camcol;
band = input->band;
cband = input->bandname;
field = input->field;
run = input->run;
rerun = input->rerun;
seed = input->seed;
xcmin = input->xcmin;
xcmax = input->xcmax;
ycmin = input->ycmin;
ycmax = input->ycmax;
npsfstar = input->npsfstar;
psfreportfile = input->psfreportfile;
psfstatfile = input->psfstatfile;
printf("%s: initiating modules \n", name);

 printf("%s: initiating IO \n", name); 
 thInitIo();
 printf("%s: initiating Tank \n", name);
  thTankInit();
 printf("%s: initiating Ascii \n", name);
  thAsciiInit();  
 printf("%s: initiating SpanObjmask \n", name);
  initSpanObjmask();

printf("%s: reading framefile \n", name);

  /* get the related schema */
  ASCIISCHEMA *schema = NULL;
  char *strutype = "FRAMEFILES";	
  schema = thTankSchemaGetByType(strutype);

  if (schema == NULL) {
    printf("%s: No schema exists for structure %s", 
		   name, strutype);
  }

  /* get the structure constructor */
  void *(*strunew)();
  strunew = schema->constructor;
  if (strunew == NULL) {
    printf("%s: structure constructor for %s not available in tank", 
		   name, strutype);
  }

  /* reading the framefile */
  fname = input->framefile;
  void *stru;
  stru = (*strunew)();

  FRAMEFILES *ff;
  ff = (FRAMEFILES *) stru;

  FILE *fil;
  fil = fopen(fname, "r");
  if (fil == NULL) {
	thError("%s: ERROR - could not open framefile '%s'", name, fname);
	return(-1);
	} 
  CHAIN *fchain;
  fchain = thAsciiChainRead(fil);

  if (shChainSize(fchain) == 0) {
	thError("%s: no structures was properly read from the par file", name);
	}
  printf("%s: (%d) pieces of frame info read \n", name, shChainSize(fchain));

  int nframe,  iframe = 0;
  nframe = shChainSize(fchain);

	printf("%s: total of (%d) frames read \n", name, nframe);

  for (iframe = 0; iframe < nframe; iframe++) {
	
	printf("%s: --------------------------- \n", name);
	printf("%s: working on frame (%d) \n", name, iframe);
	ff = (FRAMEFILES *) shChainElementGetByPos(fchain, iframe); 
 
	 if (ff == NULL) {
	    thError("%s: NULL returned from chain - check source code", name);
	  } else {  
	    SCHEMA *s = shSchemaGet("FRAMEFILES");
	    if (s == NULL) {
		thError("%s: WARNING - structure 'FRAMEFILES' not surpported by schema", name);
	    } else {
	    int i, nse = s->nelem;
	    
		for (i = 0; i < nse; i++) {
		SCHEMA_ELEM *se = s->elems + i;
		if (!strcmp(se->type, "char")) {
			char **value = shElemGet(ff, se, NULL);
			printf("%s: %s = '%s' \n", name, se->name, *value);
		}
	    }
	    }
	}
   }

	int nrandom = npsfstar * 1000;
	printf("%s: initiating random numbers (%d) \n", name, nrandom);	
	char *randstr;
	randstr = thCalloc(MX_STRING_LEN, sizeof(char));
	sprintf(randstr, "%d:2", nrandom);
	RANDOM *rand;
	rand = phRandomNew(randstr, 1);
	printf("%s: setting seed to %u \n", name, seed);
	unsigned int seed0 = phRandomSeedSet(rand, seed);
	printf("%s: old seed was %u \n", name, seed0);
	
  printf("%s: process and frame \n", name);
  PROCESS *proc;
  proc = thProcessNew();
  int i;
  FRAME *frame;
  REGION *im;
  
  frame = thFrameNew();
      if (frame->files != NULL) {
	thFramefilesDel(frame->files);
      }
      
      frame->files = ff; 
      frame->proc = proc;

      /* testing PSF at this stage */ 

      printf("%s: loading fake fpC file\n", name);
      thFrameFakeImage(frame, run, field, cband, camcol, rerun);
      im = frame->data->image;

       printf("%s: loading amplifier information\n", name);
      thFrameLoadAmpl(frame); 
	printf("%s: loading PHCALIB \n", name);
	thFrameLoadCalib(frame);
	printf("%s: loading PHOTO psf \n", name);
      thFrameLoadPhPsf(frame);
      printf("%s: reshaping into SOHO psf \n", name);
      thFrameLoadThPsf(frame);
      printf("%s: loading fake OBJC's \n", name);
      /* 
	thFrameFakePhObjc(frame);
	if (myobjclist != NULL && shChainSize(myobjclist) != 0) {
      		printf("%s: adding objects to the list manually \n", name);
      		thFrameAddObjcChain(frame, myobjclist);
	}
      */
     printf("%s: loading weight matrix -- space saver \n", name); 
      thFrameLoadWeight(frame);

	/* PERFORMING FIT TO THE FAKE FRAME */
	/* initializing model and object bank */
	thModelInit();
	/* introducing sky to the object bank */
	printf("%s: initiating all models and object types  ... \n", name);
	printf("%s: initiating sky model and object type\n", name);
	thSkyObjcInit(nrow, ncol, frame->data->ampl, &g0sky);
	printf("%s: initiating other models \n", name);
	status = MGModelsInit();
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object models", name);
		return(-1);
	}
	int nm_max = 2;
	char *mname, **mnames = thCalloc(nm_max, sizeof(char *));
	for (i = 0; i < nm_max; i++) {
		mnames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
	} 
	mname = mnames[0];
	#if STAR_CLASSIFICATION
	strcpy(mnames[0], "star");
	printf("%s: initiating object type 'STAR' \n", name);
	status = MStarObjcInit(mnames[0]);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object type", name);
		return(-1);
	}
	#endif 

	#if CDCANDIDATE_CLASSIFICATION 
	strcpy(mnames[0], "deV");
	strcpy(mnames[1], "Exp2");
	printf("%s: initiating object type 'CDCANDIDATE' \n", name);
	printf("%s: adding two models '%s', '%s' \n", name, mnames[0], mnames[1]);
	status = McDCandidateObjcInit(mnames, 2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object type", name);
		return(-1);
	}
	#endif

	#if GALAXY_CLASSIFICATION

	strcpy(mnames[0], "deV");
	strcpy(mnames[1], "Exp");
	printf("%s: initiating object type 'GALAXY' \n", name);
	printf("%s: adding two models '%s', '%s' \n", name, mnames[0], mnames[1]);
	status = MGalaxyObjcInit(mnames, 2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object type", name);
		return(-1);
	}

	#endif

	printf("%s: models and object types initiated \n", name);	
	/* define THOBJC with type SKY
	   construct a MAP
	*/

	printf("%s: creating map to access lstruct \n", name);

	MAPMACHINE *map;
	map = thMapmachineNew();
	/* inserting map into frame */
	frame->work->map = map;

	/* add all the models in the object to the map */
	/* introducing sky to the simulated set of objects  */
	printf("%s: creating sky object \n", name);
	THOBJC *objc = thSkyObjcNew();
	if (objc == NULL) {
		thError("%s: ERROR - could no create object sample", name);
		return(-1);
	}
	printf("%s: loading sky object onto (mapmachine) \n", name);
	thMAddObjcAllModels(map, objc, band, SEPARATE);
	printf("%s: compiling map \n", name);
	status = thMCompile(map);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not compile the map", name);
		return(-1);
	}	

	LSTRUCT *l;

/* constructing LSTRUCT */
	printf("%s: constructing likelihood data structure (lstruct) \n", name);
 	status = thFrameLoadLstruct(frame, NULL, DEFAULT_CALIBTYPE, DEFAULT_LFITRECORD);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not load (lstruct)", name);
		return(-1);
	}
	status = thFrameGetLstruct(frame, &l);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (lstruct) out of (frame)", name);
		return(-1);
	}
	PSFWING *psfwing = thPsfwingNew();
	REGION *psfreg = shRegNew("psf-reg", 0, 0, TYPE_THPIX);
	psfwing->reg = psfreg;

	PSF_CONVOLUTION_TYPE conv_types[N_PSF_REPORT_TYPE] = {CENTRAL_AND_WINGS_PSF, CENTRAL_PSF, RADIAL_PSF};

	/* genrate star positions */

	/* loop over inner boundary size */

	 	/* loop over star positions

			/* loop over PSF components
			   1. generate certain component of PSF 
			   2. generate annular sum
			   3. add data to PSF report (component: xc, yc, sigma, FWHM, sigma1, sigma2, sum)
			*/
	
			/* for a particular boundary add background, amplitude correction to PSF report */

			/* end loop over PSF components

		/* end loop over star positions */

		/* write report to a new HDU of the psf report file

		/* add brief version of particular boundary report to a chain of brief reports */
	
	/* end loop over inner boundary size */

	THPIX *xclist, *yclist;
	xclist = thCalloc(npsfstar, sizeof(THPIX));
	yclist = thCalloc(npsfstar, sizeof(THPIX));

	status = generate_random_star_pos(xcmin, xcmax, ycmin, ycmax, xclist, yclist, npsfstar);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not generate random star positions", name);
		return(-1);
	}

	printf("%s: PSF will be measured in the following random locations (%d): \n", name, npsfstar);
	int istar;
	for (istar = 0; istar < npsfstar; istar++) {
		printf("%d: (%g, %g) \n", istar, xclist[istar], yclist[istar]);
	}

	PSFDUMP *psf_dump;
	psf_dump = thPsfDumpNew();
	status = thPsfDumpPutFilename(psf_dump, psfreportfile, psfstatfile);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put 'filename' in (psf dump)", name);
		return(-1);
	}
	CHAIN *psfreport_chain = NULL, *psfstat_chain = NULL; 
	psfstat_chain = shChainNew("BRIEFPSFREPORT");

	int ib, jb, ipsf;
	/* assigning the inboundary */
	int *inboundary, *outboundary, nb;
	#if DEBUG_MAIN_PSF
	nb = 4;
	inboundary = thCalloc(nb, sizeof(int));
	inboundary[0] = 0;
	inboundary[1] = 10;
 
	inboundary[2] = 15;
	inboundary[3] = 19;

	outboundary = thCalloc(nb, sizeof(int));
	outboundary[0] = 0;
	outboundary[1] = 10;
 
	outboundary[2] = 15;
	outboundary[3] = 19;


	/* 
	boundary[4] = 20;
	*/
	#else
	nb = 20;
	inboundary = thCalloc(nb, sizeof(int));
	outboundary = thCalloc(nb, sizeof(int));
	for (ib = 0; ib < nb; ib++) {
		inboundary[ib] = ib;
		outboundary[ib] = ib;
	}
	#endif

	PSF_CONVOLUTION_REPORT *psf_conv_report = thPsfConvReportNew();
	for (ib = 0; ib < nb; ib++) {
		for (jb = 0; jb < nb; jb++) {

		int inbound, outbound;
		inbound = inboundary[ib];
		outbound = outboundary[jb];
		
		if (outbound > inbound + 1) {
	
			psfreport_chain = shChainNew("PSFREPORT");
			status = thPsfInit(inbound, outbound);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not re-init PSF module", name);
				return(-1);
			}
			printf("%s: inboundary = %d, outboundary = %d ", name, inbound, outbound); fflush(stdout);
			for (istar = 0; istar < npsfstar; istar++) {
				printf("~"); fflush(stdout);
				THPIX background, amplitude;
				amplitude = background = VALUE_IS_BAD;
				xc = xclist[istar];
				yc = yclist[istar];
				/* simulate the PSF in this location */
				PSFREPORT *psfreport;
				psfreport = thPsfReportNew();
				status = thPsfReportInit(psfreport, xc, yc, inbound, outbound);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not initiate (psfreport) for location (%g, %g), boundary (%d, %d)", name, (float) xc, (float) yc, inbound, outbound);
					return(-1);
				}
				for (ipsf = 0; ipsf < N_PSF_REPORT_TYPE; ipsf++) {
					printf("."); fflush(stdout);
					/* generate this particular component in this particular location */
					PSF_CONVOLUTION_TYPE conv_type = conv_types[ipsf];
					PSF_CONVOLUTION_REPORT *this_psf_conv_report;
					this_psf_conv_report = NULL;
					if (conv_type == CENTRAL_AND_WINGS_PSF) this_psf_conv_report = psf_conv_report;
					status = thLstructGenPsf(l, psfreg, this_psf_conv_report, xc, yc, conv_type);
					if (status != SH_SUCCESS) {
						thError("%s: ERROR - could not generate (psf) image for type (%s)", name, shEnumNameGetFromValue("PSF_CONVOLUTION_TYPE", conv_type));
						return(-1);
					}
					/* generate annular sum and stats */
					status = thPsfReportCompileFromPsfWing(psfwing, psfreport, ipsf);
					if (status != SH_SUCCESS) {
						thError("%s: ERROR - could not compile (psfreport) from (psfwing)", name);
						return(-1);
					}
					/* decide if you can trust background and amplitude */
				}
				status = thPsfConvReportGet(psf_conv_report, NULL, &amplitude, &background);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not get (amplitude) and (background) from (psf_conv_report)", name);
					return(status);
				}
				status = thPsfReportFinalize(psfreport, psf_conv_report);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not finalize (psfreport)", name);
					return(-1);
				}
				shChainElementAddByPos(psfreport_chain, psfreport, "PSFREPORT", TAIL, AFTER);
			}
	
			/* now generate the brief statistical report */
			BRIEFPSFREPORT *psfstat = NULL;
			psfstat = thBriefPsfReportNew();		
			status = thPsfStatFromPsfReport(psfreport_chain, psfstat);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not compile (psf stat) from (psf report)", name);
				return(-1);
			}
			shChainElementAddByPos(psfstat_chain, psfstat, "BRIEFPSFREPORT", TAIL, AFTER);	
			int deep = 0;
			status = thPsfDumpPutData(psf_dump, psfreport_chain, NULL, deep);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR = could not put (psfreport chain) into (psf dump) for (ib = %d)", name, ib);
				return(-1);
			}
			psfreport_chain = NULL;
			int ret, free_to_os = 1;
			ret = shMemDefragment(free_to_os);
			if (ret != 0) {
				thError("%s: ERROR - problem defragmenting memory", name);
				return(-1);
			}
			printf("\n"); fflush(stdout);
		}
	}
	}	

	int deep = 0;
	status = thPsfDumpPutData(psf_dump, NULL, psfstat_chain, deep);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put (data) in (psf dump)", name);
		thErrShowStack();
		return(-1);
	}
	psfstat_chain = NULL;
	status = thDumpPsf(psf_dump);	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump psf reports and stats", name);
		thErrShowStack();
		return(-1);
	}	
	printf("%s: end of psf simulation ... \n", name);
	thErrShowStack();
	return(0);
}


/*****************************************************************************/

static void usage(void)
{
   char **line;

   static char *msg[] = {
      "Usage: read_atlas_image [options] output-file n_tr n_tc",
      "Your options are:",
      "       -?      This message",
      "       -h      This message",
      "       -i      Print an ID string and exit",
      "       -v      Turn up verbosity (repeat flag for more chatter)",
      NULL,
   };

   for(line = msg;*line != NULL;line++) {
      fprintf(stderr,"%s\n",*line);
   }
}	

void chain_model_output(char *job, CHAIN *objclist, THOBJCTYPE otype) {

THOBJC *objc;
int iobjc, nobjc;
nobjc = shChainSize(objclist);
for (iobjc = 0; iobjc < MIN(nobjc, N_OUTPUT_OBJC); iobjc++) {
	objc = shChainElementGetByPos(objclist, iobjc);
	model_output(job, objc);
}
printf("--- %s: end of output --- \n", job);
return;
}


void model_output(char *job, THOBJC *objc) {

CHAIN *thprops;
int nmodel, imodel;
static char *line = NULL;
if (line == NULL) line = thCalloc(MX_STRING_LEN, sizeof(char));

thprops = objc->thprop;
nmodel = shChainSize(thprops);
for (imodel = 0; imodel < nmodel; imodel++) {
	SCHEMA *s;
	SCHEMA_ELEM *se;
	int n, i;
	char *rname;
	THPIX *value;
	THPROP *prop;
	prop = shChainElementGetByPos(thprops, imodel);	 		
	s = shSchemaGetFromType(shTypeGetFromName(prop->pname));
	n = s->nelem;
	printf("--- %s parameters for (objc: %d) (model: '%s') --- \n", 
		job, objc->thid, prop->mname);
	for  (i = 0; i < n; i++) {
		se = s->elems + i;
		rname = se->name;
		value = shElemGet(prop->value, se, NULL);
		if (strlen(line) != 0) {
			sprintf(line, "%s, %15s = %.5e", line, rname, *value);
		} else {
			sprintf(line,"%15s = %.5e", rname, *value);
		}
		if (((i + 1)%TABLEROW == 0) || i == n - 1) {
			printf("%s\n", line);
			strcpy(line, "");
		}
	}
}
return;
}

RET_CODE tweak(void *p, THPIX e_tweak, char *pname, char **rnames, int rcount, CHAIN *chain,
		TWEAKFLAG tflag) {
char *name = "tweak";

/* this assumes that all records (schema_elems) are of type THPIX */

SCHEMA *s;
SCHEMA_ELEM *se;

if (p == NULL || pname == NULL || strlen(pname) == 0) {
	thError("%s: ERROR - void or unknown input parameter cannot be tweaked", name);
	return(SH_GENERIC_ERROR);
}
TYPE ptype;
ptype = shTypeGetFromName(pname);
s = shSchemaGetFromType(ptype);
if (s == NULL) {
	thError("%s: ERROR - unknown parameter type '%s'", name, pname);
	return(SH_GENERIC_ERROR);
}
if (rcount < 0) {
	thError("%s: ERROR - negative record count (%d)", name, rcount);
	return(SH_GENERIC_ERROR);
}
if (rcount > 0 && rnames == NULL) {
	thError("%s: ERROR - null record name, while record count is positive (%d)", name, rcount);
	return(SH_GENERIC_ERROR);
}


RET_CODE status;
int i;
void *x;
#define YTYPE FL32
YTYPE y;
char *ytype = "FL32";

if (rcount == 0) {
	int n; 
	int onlypos = 0;
	n = s->nelem;
	for (i = 0; i < n; i++) {
		se = s->elems + i;
		if (se == NULL) {
			thError("%s: WARNING - could not find %d-th record in '%s'", name, i, pname);
		} else {
			onlypos = !strcmp(se->name, "re");	
			x = shElemGet(p, se, NULL);
			status = thqqTrans(x, se->type, &y, ytype);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not read record '%s' of type '%s' in '%s' into a double variable", name, se->name, se->type, pname);
				return(status);
			}
			int poscheck = 1;
			while (poscheck) {
				if (tflag == FRACTIONAL) {
					y *= ((YTYPE) 1.00 + ((YTYPE) phGaussdev()) * (YTYPE) e_tweak);
				} else if (tflag == ABSOLUTE) {
					y += ((YTYPE) phGaussdev()) * (YTYPE) e_tweak;
				}
				poscheck = onlypos && (y > (YTYPE) 0);
			}
			status = thqqTrans(&y, ytype, x, se->type);
 			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not transform '%s' data types into '%s' for record '%s' of '%s'", name, ytype, se->type, se->name, pname);
				return(status);
			}
		}
	}
} else {	
	char *rname;
	int onlypos = 0;
	for (i = 0; i < rcount; i++) {
		rname = rnames[i];
		onlypos = (!strcmp(rname, "re"));
		if (rname != NULL && strlen(rname) != 0) {
			se = shSchemaElemGetFromType(ptype, rname);
			if (se == NULL) {
				thError("%s: WARNING - could not find record '%s' in '%s'", name, rname, pname);
			} else {
				x = shElemGet(p, se, NULL);
				status = thqqTrans(x, se->type, &y, ytype);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not read record '%s' of type '%s' in '%s' into a working variable of type '%s'", name, se->name, se->type, pname, ytype);
					return(status);
				}
				int poscheck = 1;
				while (poscheck) {
					if (tflag == FRACTIONAL) {
						y *= ((YTYPE) 1.00 + ((YTYPE) phGaussdev()) * (YTYPE) e_tweak);
					} else if (tflag == ABSOLUTE) {
						y += ((YTYPE) phGaussdev()) * (YTYPE) e_tweak;
					}
					poscheck = onlypos && (y > (YTYPE) 0);
				}
				status = thqqTrans(&y, ytype, x, se->type);
 				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not transform '%s' data type into '%s' for record '%s' of '%s'", name, ytype, se->type, se->name, pname);
					return(status);
				}

		}
		}
	}
}
#undef YTYPE 
return(SH_SUCCESS);
}

RET_CODE add_poisson_noise_to_image(REGION *im, CHAIN *ampl) {
char *name = "add_poisson_noise_to_image";

if (im == NULL) {
	thError("%s: ERROR - null input image", name);
	return(SH_GENERIC_ERROR);
}

if (ampl == NULL) {
	thError("%s: ERROR - null amplifier information", name);
	return(SH_GENERIC_ERROR);
}

int nrow, ncol, i, j, n0 = 0, n_ = 0, nplus = 0;
THPIX *row;
FL32 y;

nrow = im->nrow;
ncol = im->ncol;

int ia, na;
na = shChainSize(ampl);
for (ia = 0; ia < na; ia++) {

AMPL *a;
int row0, row1, col0, col1;
THPIX g;
FL32 mu;
a = shChainElementGetByPos(ampl, ia);
if (a == NULL) {
	thError("%s: WARNING - null (AMPL) structure discovered in (ampl) chain", name);
} else {
	row0 = a->row0;
	row1 = a->row1;
	col0 = a->col0;
	col1 = a->col1;
	g = a->gain;	
	printf("%s: amplifier (%d) - (gain = %g)\n", name, ia, g);
	for (i = MAX(row0, 0); i < MIN(nrow, row1); i++) {
		row = im->rows_thpix[i];
		for (j = MAX(0, col0); j < MIN(ncol, col1); j++) {
		y = (FL32) (row[j]);
		if (y > (FL32) 0.0) {
			mu = g * y;
			y = phPoissondev(mu) / g;
			/* 
			y += sqrt(y / g) * ((FL32) phGaussdev());
			*/
			row[j] = y;
			nplus++;
		} else if (y == (THPIX) 0.0) {
			n0++;
		} else {
			n_++;
		}
		}
	}
}
}

if (n0 > 0) printf("%s: (zero pixels: %d) \n", name, n0);
if (n_ > 0) printf("%s: (negative pixels: %d) \n", name, n_);
if (nplus > 0) printf("%s: (positive pixels: %d) \n", name, nplus);

return(SH_SUCCESS);
}

int compare_re(const void *x1, const void *x2) {
	OBJC_IO *y1, *y2;
	shAssert(x1 != NULL);
	shAssert(x2 != NULL);
	y1 = *(OBJC_IO **) x1;
	y2 = *(OBJC_IO **) x2;
	float z1, z2;
	z1 = (float) y1->r_deV[THIS_BAND];
	z2 = (float) y2->r_deV[THIS_BAND];
	int res;
	if (y1->objc_type == OBJ_GALAXY && 
		y2->objc_type == OBJ_GALAXY) {
		if (z1 > z2) {
			res = -1;
		} else if (z1 < z2) {
			res = 1;
		} else {
			res = 0;
		}
	} else if (y1->objc_type == OBJ_GALAXY && y2->objc_type != OBJ_GALAXY) {
		res = -1;
	} else if (y1->objc_type != OBJ_GALAXY && y2->objc_type == OBJ_GALAXY) {
		res = 1;
	} else {
		res = 0;
	}
	return(res);
}

int compare_count(const void *x1, const void *x2) {
	OBJC_IO *y1, *y2;
	shAssert(x1 != NULL);
	shAssert(x2 != NULL);
	y1 = *(OBJC_IO **) x1;
	y2 = *(OBJC_IO **) x2;
	int res;
	if (y1->objc_type == OBJ_GALAXY && y2->objc_type == OBJ_GALAXY) {
		float z1, z2;
		z1 = y1->counts_exp[THIS_BAND] + y1->counts_deV[THIS_BAND];	
		z2 = y2->counts_exp[THIS_BAND] + y2->counts_deV[THIS_BAND];
		if (z1 > z2) {
			res = -1;
		} else if (z1 < z2) {
			res = 1;
		} else {
			res = 0;
		}
	} else if (y1->objc_type == OBJ_GALAXY) {
		res = -1;
	} else if (y2->objc_type == OBJ_GALAXY) {
		res = 1;
	} else {
		res = 0;
	}
	return(res);
}

RET_CODE tweak_init_pars(LSTRUCT *l, CHAIN *objclist) {
char *name = "tweak_init_pars";

/* add all the models in the object to the map */
static char **rnames = NULL, **iornames = NULL, **mnames = NULL;
char *mname;
int nrname = 10;
int nm_max = 2;
int i;
if (rnames == NULL) {
	mnames = thCalloc(nm_max, sizeof(char *));
	rnames = thCalloc(nrname, sizeof(char*));
	iornames = thCalloc(nrname, sizeof(char *));
	for (i = 0; i < nrname; i++) {
		mnames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
		rnames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
		iornames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
	}
}

int pos = 0;
SKYPARS *ps;
THOBJC *objc = NULL;
TYPE ptype;
char *pname;
OBJCPARS *pg;

int ii, j = 0;
ii = j;
strcpy(rnames[j++], "I");
#if FIT_SHAPE 
int ishape, jshape, nshape;
ishape = j;
if (EllDesc == ALGEBRAIC) {
	strcpy(rnames[j++], "a");
 	strcpy(rnames[j++], "b");
	strcpy(rnames[j++], "c");
} else if (EllDesc == CANONICAL) {
	#if FIT_RE
	strcpy(rnames[j++], "re");
	#endif
	#if FIT_E
	strcpy(rnames[j++], "e");
	#endif
	#if FIT_PHI
	strcpy(rnames[j++], "phi");
	#endif
} else if (EllDesc == SUPERCANONICAL || EllDesc == UBERCANONICAL) {
	#if FIT_RE
	strcpy(rnames[j++], "re"); 
	#endif
	#if FIT_E
	strcpy(rnames[j++], "E");
	#endif
	#if FIT_PHI
	strcpy(rnames[j++], "phi");
	#endif
}	
jshape = j;
nshape = j;
#endif
#if FIT_CENTER
int icenter, jcenter, ncenter;
icenter = j;
strcpy(rnames[j++], "xc");  
strcpy(rnames[j++], "yc");
jcenter = j;
ncenter = (jcenter - icenter);
#endif

int ntweak = 0;
if (TWEAK_ALL) {
	ntweak = shChainSize(objclist);
} else {
	ntweak = MIN(1+ NFITOBJC, shChainSize(objclist));
}
RET_CODE status;
for (pos = 0; pos < ntweak; pos++) {

	THOBJCTYPE objctype;
	char *objcname;
	objc = shChainElementGetByPos(objclist, pos);
	objctype = objc->thobjctype;
	thObjcNameGetFromType(objctype, &objcname);
	if (objctype == SKY_OBJC) {

	/* sky */ 
	thObjcGetIopar(objc, (void **) &ps, &ptype);
	pname = shNameGetFromType(ptype);	
	if (ps != NULL) {
	status = tweak((void *) ps, SKY_FRACTIONAL_TWEAK, pname, NULL, 0, objclist, ABSOLUTE);
	if (status != SH_SUCCESS) {
		printf("%s: ERROR - could not tweak '%s' \n", name, pname);
		return(-1);
	}
	} else {
		printf("%s: WARNING - null parameter '%s' in object (objc: %d) - no tweaking \n", name, pname, objc->thid);
	}
	} else if (!strcmp(objcname, "DEVGALAXY") || !strcmp(objcname, "EXPGALAXY") || !strcmp(objcname, "GAUSSIAN") || (!strcmp(objcname, "GALAXY"))) {

	/* galaxy */
	OBJC_IO *ph;
	ph = ((PHPROPS *) objc->phprop)->phObjc;
	thObjcGetIopar(objc, (void **) &pg, &ptype);
	pname = shNameGetFromType(ptype);

	mname = mnames[0];
	if (!strcmp(objcname, "DEVGALAXY")) strcpy(mname, "deV");
	if (!strcmp(objcname, "EXPGALAXY")) strcpy(mname, "Exp");
	if (!strcmp(objcname, "GAUSSIAN")) strcpy(mname, "gaussian");
	if (!strcmp(objcname, "GALAXY"))  {
		strcpy(mnames[0], "deV");
		strcpy(mnames[1], "Exp");
	}
	if (!strcmp(objcname, "CDCANDIDATE")) {
		strcpy(mnames[0], "deV");
		strcpy(mnames[1], "Exp2");
	}

	if (pos < N_OUTPUT_OBJC) {
	printf("%s: tweaking (objc: %d): ", name, objc->thid);
	for (i = 0; i < j; i++) {
		sprintf(iornames[i], "%s_%s", rnames[i], mname);
		printf("'%s' ", iornames[i]);
	}
	printf("\n");
	}
	if (pg != NULL) {

	THPIX e_tweak;
	#if FORCE_INIT_PAR
	e_tweak = I_TWEAK;
	status = tweak((void *)pg, e_tweak, pname, iornames + ii, 
			1, objclist, FRACTIONAL);
	#else
	e_tweak = 0.0;
	if (!strcmp(mname, "deV")) {
		e_tweak = I_TWEAK * ph->counts_deVErr[band] / PI / pow(DEV_RE_SMALL + ph->r_deV[band], 2);
		if (pos < N_OUTPUT_OBJC) printf("%s: i_tweak('%s') = %g; countErr = %g, rErr = %g \n", 
		name, mname, e_tweak, ph->counts_deVErr[band], ph->r_deV[band]);} 
	else if (!strcmp(mname, "Exp")) {
		e_tweak = I_TWEAK * ph->counts_expErr[band] / PI / pow(EXP_RE_SMALL + ph->r_exp[band], 2);
		
		if (pos < N_OUTPUT_OBJC) printf("%s: i_tweak('%s') = %g; countErr = %g, rErr = %g \n", 
		name, mname, e_tweak, ph->counts_expErr[band], ph->r_exp[band]);
	}
	status = tweak((void *)pg, e_tweak, pname, iornames + ii, 
			1, objclist, ABSOLUTE);
	#endif
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not tweak '%s'", name, pname);
		return(status);
	} 
	#if FIT_SHAPE
	if (EllDesc == SUPERCANONICAL || EllDesc == UBERCANONICAL) {
		status = tweak((void *) pg, SHAPE_TWEAK, pname, iornames + ishape, 
		nshape, objclist, FRACTIONAL);
	} else {
		status = tweak((void *) pg, SHAPE_TWEAK, pname, iornames + ishape, 
		nshape, objclist, FRACTIONAL);
	}
	if (EllDesc == ALGEBRAIC) {
		pg->b_gaussian *= pow(1.0 - BETA_SHAPE, 2);
	}
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not tweak '%s'", name, pname);
		return(status);
	} 

	#endif
	#if FIT_CENTER
	for (i = 0; i < ncenter; i++) {
		e_tweak = 0.0;
		#if FORCE_INIT_PAR
		e_tweak = CENTER_TWEAK;
		#else 
		if (!strcmp(rnames[i + icenter], "xc")) {
			e_tweak = CENTER_TWEAK * ph->rowcErr[band];
		} else if (!strcmp(rnames[i + icenter], "yc")) {
			e_tweak = CENTER_TWEAK * ph->colcErr[band];
		}
		#endif
		status = tweak((void *) pg, e_tweak, pname, iornames + icenter + i, 
				1, objclist, ABSOLUTE);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not tweak '%s'", name, pname);
			return(status);
		}
	} 
	#endif
	
	if (!strcmp(objcname, "GALAXY")) {

	mname = mnames[1];
	if (pos < N_OUTPUT_OBJC) {
	printf("%s: tweaking (objc: %d): ", name, objc->thid);	
	for (i = 0; i < j; i++) {
		sprintf(iornames[i], "%s_%s", rnames[i], mname);
		printf("'%s' ", iornames[i]);
	}
	printf("\n");
	}

	e_tweak = 0.0;
	#if FORCE_INIT_PAR
	e_tweak = I_TWEAK;
	status = tweak((void *)pg, e_tweak, pname, iornames + ii, 
		1, objclist, FRACTIONAL);
	#else
	if (!strcmp(mname, "deV")) {
		e_tweak = I_TWEAK * ph->counts_deVErr[band] / PI / pow(DEV_RE_SMALL + ph->r_deV[band], 2);	
		if (pos < N_OUTPUT_OBJC) printf("%s: i_tweak('%s') = %g; countErr = %g, rErr = %g \n", 
		name, mname, (float) e_tweak, (float) ph->counts_deVErr[band], (float) ph->r_deV[band]);
	} else if (!strcmp(mname, "Exp")) {
		e_tweak = I_TWEAK * ph->counts_expErr[band] / PI / pow(EXP_RE_SMALL + ph->r_exp[band], 2);
		if (pos < N_OUTPUT_OBJC) printf("%s: i_tweak('%s') = %g; countErr = %g, rErr = %g \n", 
		name, mname, (float) e_tweak, (float) ph->counts_expErr[band], (float) ph->r_exp[band]);
	}
	status = tweak((void *)pg, e_tweak, pname, iornames + ii, 1, objclist, ABSOLUTE);
	#endif
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not tweak '%s'", name, pname);
		return(status);
	} 
	#if FIT_SHAPE
	if (EllDesc == SUPERCANONICAL || EllDesc == UBERCANONICAL) {
		status = tweak((void *) pg, SHAPE_TWEAK, pname, iornames + ishape, nshape, objclist, FRACTIONAL);
	} else {
		status = tweak((void *) pg, SHAPE_TWEAK, pname, iornames + ishape, nshape, objclist, FRACTIONAL);
	}
	if (EllDesc == ALGEBRAIC) {
		pg->b_gaussian *= pow(1.0 - BETA_SHAPE, 2);
	}
	#endif
	#if FIT_CENTER
	for (i = 0; i < ncenter; i++) {
		e_tweak = 0.0;
		#if FORCE_INIT_PAR
		e_tweak = CENTER_TWEAK;
		#else
		if (!strcmp(rnames[i + icenter], "xc")) {
			e_tweak = CENTER_TWEAK * ph->rowcErr[band];
		} else if (!strcmp(rnames[i + icenter], "yc")) {
			e_tweak = CENTER_TWEAK * ph->colcErr[band];
		}
		#endif
		status = tweak((void *) pg, e_tweak, pname, iornames + icenter + i, 
				1, objclist, ABSOLUTE);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not tweak '%s'", name, pname);
			return(status);
		}
	} 
	#endif

	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not tweak '%s'", name, pname);
		return(status);
	}

	}	
	} else {
		printf("%s: WARNING - null parameter '%s' in object (objc: %d) - no tweaking \n", name, pname, objc->thid);
	}

	}
	}
	/* dumping parameters into their respective location */ 
 
/* 	
	#if FORCE_INIT_PAR 
*/
	printf("%s: assigning parameter and amplitude arrays in (mapmachine) from tweaked IO parameters \n", name);
	thLDumpAmp(l, IOTOMODEL);
	thLDumpPar(l, IOTOMODEL);

	thLDumpAmp(l, IOTOX);
	thLDumpPar(l, IOTOX);
	thLDumpPar(l, IOTOXN);
/* 
	#else

	printf("%s: assigning SDSS parameters to (mapmachine) arrays", name);
	thLDumpAmp(l, MODELTOX);
	thLDumpPar(l, MODELTOX);

	#endif
*/


	printf("%s: assigning values to (a, p) from updated (aN, pN) \n", name);
	status = UpdateXFromXN(l, APSECTOR);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update parameters to the tweaked values", name);
		return(status);
	}

return(SH_SUCCESS);
}

void test_sorted_chain(CHAIN *chain, int (*compare)(const void *, const void *)) {
char *name = "test_sorted_chain";
if (chain == NULL || compare == NULL) {
	thError("%s: ERROR - null input", name);
	return;
}
int n = shChainSize(chain);
int i, error = 0;

printf("%s: listing chain elements (r_deV) (chain size = %d) \n", name, n);
for (i = 0; i < n; i++) {
	OBJC_IO *x1;
	x1 = shChainElementGetByPos(chain, i);
	if (x1->objc_type != OBJ_GALAXY) {
		printf(" %s, ", shEnumNameGetFromValue("OBJ_TYPE", x1->objc_type));	
	} else {
		printf(" %g,", (float) x1->r_deV[R_BAND]);
	}
}
printf("\n");

for (i = 0; i < n - 1; i++) {
	const void *x1, *x2;
	x1 = shChainElementGetByPos(chain, i);
	x2 = shChainElementGetByPos(chain, i+1);
	int res = compare(&x1, &x2);
	if (res == -1) {
		thError("%s: ERROR - chain descends at (i = %d)", name, i);
		error++;
	}
}
if (error) {
	thError("%s: ERROR - found at least (%d) misplaced elements in chain", name);
}
return;
}

RET_CODE generate_random_star_pos(int xcmin, int xcmax, int ycmin, int ycmax, THPIX *xclist, THPIX *yclist, int nstar) {
char *name = "generate_random_star_pos";
shAssert(nstar > 0);
shAssert(xclist != NULL);
shAssert(yclist != NULL);

int i;
for (i = 0; i < nstar; i++) {
	THPIX r1, r2;
	r1 = (THPIX) phRandomUniformdev();
	r2 = (THPIX) phRandomUniformdev();
	xclist[i] = (THPIX) xcmin + ((THPIX) (xcmax - xcmin)) * r1;
	yclist[i] = (THPIX) ycmin + ((THPIX) (ycmax - ycmin)) * r2;
}
return(SH_SUCCESS);
}



