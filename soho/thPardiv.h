#ifndef THPARDIV_H
#define THPARDIV_H

#include "math.h"
#include "thBasic.h"
#include "thPardivTypes.h"

int thGetIndexThpixPardivLinear(THPIX pval, THPIX *pdiv, int npdiv, RET_CODE *status);
int thGetIndexIntPardivLinear(int pval, int *pdiv, int npdiv, RET_CODE *status);
int thGetIndexThpixPardivLogarithmic(THPIX pval, THPIX *pdiv, int npdiv, RET_CODE *status);
int thGetIndexIntPardivLogarithmic(int pval, int *pdiv, int npdiv, RET_CODE *status);

int thGetValIntPardiv(int divindex, int *pdiv, int npdiv, THPARDIV_TYPE divtype, RET_CODE *status);
THPIX thGetValThpixPardiv(int divindex, THPIX *pdiv, int npdiv, THPARDIV_TYPE divtype, RET_CODE *status);
int thGetValIntLinear(int divindex, int *pdiv, int npdiv, RET_CODE *status);
THPIX thGetValThpixLinear(int divindex, THPIX *pdiv, int npdiv, RET_CODE *status);
int thGetValIntLogarithmic(int divindex, int *pdiv, int npdiv, RET_CODE *status);
THPIX thGetValThpixLogarithmic(int divindex, THPIX *pdiv, int npdiv, RET_CODE *status);

#endif
