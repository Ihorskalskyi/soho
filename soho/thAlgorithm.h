#include "thAlgorithmTypes.h"

void free_algorithm_work_space();

RET_CODE design_algorithm(ADJ_MATRIX *adj_matrix, void *algorithm, ALGORITHM_DESIGN design, RUNSTAGE runstage);
RET_CODE init_wadj(ADJ_MATRIX *adj_matrix, ALGORITHM_DESIGN design, RUNSTAGE runstage);
RET_CODE try_naive_run_algorithm(ADJ_MATRIX *adj_matrix, ALGORITHM *algorithm, NAIVERUN *naiverun);
RET_CODE try_naive_del_algorithm(ADJ_MATRIX *adj_matrix, ALGORITHM *algorithm, NAIVERUN *naiverun);

RET_CODE get_degree_from_adj(MEMFL **adj, MEMFL *degree, int n, ALGORITHM_DESIGN design);
RET_CODE unload_null_nodes(MEMSTAT *memstat, MEMFL *degree, FITSTAT *fitstat, int n, void *alg);
RET_CODE get_memspace_from_adj(MEMFL **adj, MEMFL *memspace, int n);
RET_CODE find_the_next_node(MEMSTAT *memstat, MEMFL *degree, int *list, int n, int *node);
RET_CODE load_node(MEMFL *degree, MEMSTAT *memstat, MEMFL *memspace, int *list, int n, int node, int work_node, void *algorithm);
RET_CODE do_edges_to_node(MEMSTAT *memstat, MEMFL **adj, MEMFL *degree, int n, int node, void *algorithm, ALGORITHM_DESIGN design);
RET_CODE find_best_neighbor(MEMFL **adj, MEMFL *degree, int n, int node, int *neighbor);
RET_CODE add_node_to_list(int neighbor, int *list, int i_neighbor, int n);
RET_CODE order_loaded_nodes_by_degree(MEMSTAT *memstat, MEMFL *degree, int *list, int n);
RET_CODE output_algorithm(ALGORITHM *alg, ADJ_MATRIX *adj_matrix);
RET_CODE output_adj_matrix(ADJ_MATRIX *adj);
