#include <time.h>
#include <stdio.h>
#include "thProc.h"
#include "thCalib.h"
#include "thMap.h"
#include "thDump.h"
#include "phRandom.h"

#if 1
#define NSIGMA_CLIP 2.32634             /* how many sigma to clip at */
#define FIX_IQR 1.02376                 /* correction to sigma due to trimming*/
#define MEDIAN_CLIP_BIAS 0.060          /* For a Poisson process, median =
                                           clipped_median + MEDIAN_CLIP_BIAS */
#else
#define NSIGMA_CLIP 1.28155             /* how many sigma to clip at */
#define FIX_IQR 1.28621                 /* correction to sigma due to trimming*/
#define MEDIAN_CLIP_BIAS 0.120          /* For a Poisson process, median =
                                           clipped_median + MEDIAN_CLIP_BIAS */
#endif


static int filenamematch(char *f1, char *f2);
static int filenamematchin(char *trgf, char *srcf);
static int find_filter_no_in_list(FILTER filter, char *filterlist, int *nfilter);
static RET_CODE create_bndex_from_hdrline(char *hdrline, int *bndex);

RET_CODE create_quick_stat_for_objc_io_list_type(CHAIN *chain, PHSTAT *phstat, OBJ_TYPE type, int band);
RET_CODE create_quick_stat_for_calib_phobjc_io_list_type(CHAIN *chain, PHSTAT *phstat, OBJ_TYPE type, int band);
RET_CODE create_quick_stat_for_objc_io_list(CHAIN *chain, FRAMESTAT *fstat, int band);
RET_CODE create_quick_stat_for_calib_phobjc_io_list(CHAIN *chain, FRAMESTAT *fstat, int band);
static RET_CODE choose_good_objects(CHAIN *objcs);
static RET_CODE choose_good_calib_objects(CHAIN *objcs);

int find_id_in_phobjcs(int id, CHAIN *phobjcs);
int find_id_in_cphobjcs(int id, CHAIN *cphobjcs);
int find_id_in_cwphobjcs(int id, CHAIN *cwphobjcs);
int find_id_in_phatlas(int id, CHAIN *phatlas);

static int output_atlas_info(CHAIN *atlas);
static int output_phobjc_chain_info(CHAIN *objc_chain);

static int compare_wobjc_io_count_galaxy(const void *x1, const void *x2);

static RET_CODE find_mask_centeroid(OBJMASK *mask, THPIX *rowc, THPIX *colc);
static OBJMASK *find_closest_bright_mask(FRAME *f, WOBJC_IO *objc);
typedef struct mask_id_objc_id_pair {
	int imask, iobjc;
	float distance;
} MASK_ID_OBJC_ID_PAIR;

int compare_mask_id_objc_id_pair(void  *pair1, void *pair2);
float measure_distance_between_mask_and_objc(OBJMASK *mask, WOBJC_IO *objc);
RET_CODE match_bright_masks_to_objclist(CHAIN *bright_masks, CHAIN *objclist);

RET_CODE thFrameLoadAmpl(FRAME *f) {

  char *name = "thProcLoadAmpl";

  if (f == NULL) {
    thError("%s: null frame passed", name);
    return(SH_GENERIC_ERROR);
  }

  if (f->files == NULL) {
    thError("%s: null frame files", name);
    return(SH_GENERIC_ERROR);
  }

  if (f->id == NULL) {
    thError("%s: null basic frame info", name);
    return(SH_GENERIC_ERROR);
  }

  RET_CODE status;
  
  PAR_DATA *pardata;
  pardata = (PAR_DATA *) thCalloc(1, sizeof(PAR_DATA));

  if (f->proc != NULL) {

    /* load from tank */
    status = thProcLoadEcalib((PROCESS *) (f->proc), f->files->phecalibfile);

    if (status != SH_SUCCESS) {
      thError("%s: problem loading (ecalib) onto proc tank", name);
      return(status);
    }

PROCESS *proc = f->proc;
    status = thProcLoadCcdconfig(proc, f->files->phconfigfile);
    if (status != SH_SUCCESS) {
      thError("%s: problem loading (ccdconfig) onto proc tank", name);
      return(status);
    }


    pardata->ecalib = thProcTankGetChainByFilename (proc->ecalibtank, f->files->phecalibfile);
    pardata->ccdconfig = thProcTankGetChainByFilename (proc->ccdconfigtank, f->files->phconfigfile);

  } else {

    pardata->ecalib = thParReadEcalib(f->files->phecalibfile, &status);
    if (status !=  SH_SUCCESS || pardata->ecalib == NULL) {
      thError("%s: could not read ecalib information -- cannot generate (ampl)",
		     name);
      return(SH_GENERIC_ERROR);
    }

    pardata->ccdconfig = thParReadCcdconfig(f->files->phconfigfile, &status);
    if (status !=  SH_SUCCESS || pardata->ecalib == NULL) {
      thError("%s: could not read ccdconfig information -- cannot generate (ampl)",
		     name);
      return(SH_GENERIC_ERROR);
    } 
  }
  
if (f->data == NULL) f->data = thFramedataNew();
int camrow, camcol;
camrow = f->id->camrow;
camcol = f->id->camcol;
CHAIN *ampl = thAmplChainGenFromPardata(pardata, camrow, camcol, &status);
if (ampl == NULL) {
	thError("%s: ERROR - null (ampl) obtained from pardata", name);
	return(SH_GENERIC_ERROR);
}
f->data->ampl = ampl;

if (f->proc == NULL) {    
    shChainDestroy(pardata->ecalib, &thFree);
    shChainDestroy(pardata->ccdconfig, &thFree);
  }

  thFree(pardata);
  return (SH_SUCCESS);

} 

RET_CODE thFrameLoadImage(FRAME *f) {

  char *name = "thFrameLoadImage";

  if (f == NULL) {
    thError("%s: ERROR - null frame passed", name);
    return(SH_GENERIC_ERROR);
  }
  
  if (f->files == NULL) {
    thError("%s: ERROR - null frame files", name);
    return(SH_GENERIC_ERROR);
  }

  RET_CODE status;

  HDR *hdr;
  hdr = shHdrNew();
  status = shHdrReadAsFits(hdr, f->files->phflatframefile, DEF_NONE, NULL, 0, 0);
  if (status != SH_SUCCESS) {
    thError("%s: ERROR - could not read the header for file (%s)", 
		   name, f->files->phflatframefile);
    return(status);
  }

  if (f->id == NULL) {
    f->id = thFrameidNew();
  }
  status = thUpdateFrameidFromHdr(f->id, hdr);
  if (status != SH_SUCCESS) {
    thError("%s: ERROR - could not update (frameid)", name);
    return(status);
  }

  char * flatfile = f->files->phflatframefile;
  int extno = 0;
  REGION *image;
  image = shRegNew("", 0, 0, TYPE_THPIX);
  status = shRegReadAsFits(image, flatfile, 
			   extno, 1, DEF_NONE, NULL, 0, 0, 0);

  if (status != SH_SUCCESS) {
    thError("%s: ERROR - problem reading the fpc file", name);
    return(status);
  }

  /* the header is later used when dumping the generated information */
  /*
    shHdrDel(hdr);
  */

  int softbias = 0;

  /* 
     according to RHL some negative DN values are expected in 
     fpC files. this could be due to electronic noise in CCDs
     Jim says this is mainly read noise. 
     so I corrected statictical weight to read : 

     w = g / (s > 0 ? (s + g (readnoise) ^ 2) : g (readnoise ^ 2))
  
  */
  if (shHdrGetInt(hdr, "SOFTBIAS", &softbias) == SH_SUCCESS) {
	/* note that because the region is already in float format 
 * - writing (int) (pixel + 0.499) - SOFT_BIAS
	in the form pixel + (0.499 - SOFT_BIAS)  will end up adding 0.5 to each pixel
	This is the way PHOTO package's utils.c module handles the conversion from U16 to FL32 but we should not use it here.*/	
    shRegAddWithDbl((double) (-softbias), image, image);
    shHdrDelByKeyword(hdr, "SOFTBIAS");
  }

double gaussian_noise_dn = (double) DEFAULT_GAUSSIAN_NOISE_DN;
if (shHdrGetDbl(hdr, "GAUSSDN", &gaussian_noise_dn) == SH_SUCCESS) {
	status = thFramePutGaussianNoiseDN(f, (THPIX) gaussian_noise_dn);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not place (gaussian_noise_dn = %g) in (frame)", name, (float) gaussian_noise_dn);
		return(status);
	}
} else {
	#if (GAUSSIAN_NOISE && CONSTANT_GAUSSIAN_NOISE)
	gaussian_noise_dn = (double) DEFAULT_GAUSSIAN_NOISE_DN;
	thError("%s: WARNING - no line found in header for (gaussian_nois_dn, default  = %g)", name, (float) gaussian_noise_dn);
	status = thFramePutGaussianNoiseDN(f, (THPIX) gaussian_noise_dn);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not place (gaussian_noise_dn = %g) in (frame)", name, (float) gaussian_noise_dn);
		return(status);
	}
	#endif
}

	
/* the following should be saved so that when outputting the simulation soft bias et al is sent out correctly on the file 
  shHdrDelByKeyword(hdr, "BUNIT");
  shHdrDelByKeyword(hdr, "BUNITS");
*/

/* the following keywords are not allowed to appear more than once in a header
delete here and it will be automatically generated when writing */

  shHdrDelByKeyword(hdr, "SIMPLE");
  shHdrDelByKeyword(hdr, "BITPIX");
  shHdrDelByKeyword(hdr, "NAXIS");
  shHdrDelByKeyword(hdr, "NAXIS1");
  shHdrDelByKeyword(hdr, "NAXIS2");
  shHdrDelByKeyword(hdr, "BZERO");
  shHdrDelByKeyword(hdr, "BSCALE");
  shHdrDelByKeyword(hdr, "EXTEND");

  f->files->hdr = hdr;
  
  if (f->data == NULL) f->data = thFramedataNew();

  if (f->data->image != NULL) shRegDel(f->data->image);
  
  f->data->image = image;

  #if READ_CALIBRATED_FRAME
   extno++;
   REGION *calibration = shRegNew("calibration conversion coefficient", 0, 0, TYPE_THPIX);
   status = shRegReadAsFits(calibration, flatfile, 
			   extno, 1, DEF_NONE, NULL, 0, 0, 0);
   if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not read (extno = %d) of flat file for calibration conversion units", name, extno); 
	return(status);
	}
  if (f->data->calibration_image != NULL) shRegDel(f->data->calibration_image);
  f->data->calibration_image = calibration;

  PHFITSFILE *fits = NULL;
  fits = phFitsBinTblOpen(flatfile, 0, NULL);
  if (fits == NULL) {
	thError("%s: ERROR - could not open frame model as a fits file for binary input", name);
	return(SH_GENERIC_ERROR);
	} 

   /* skip the first two binary extensions */
   int quiet = 0;
   if (phFitsBinTblHdrRead(fits, NULL, NULL, NULL, quiet) != 0) {
	thError("%s: ERROR - could not load the binary header of ext[1] of frame model", name);
	return(SH_GENERIC_ERROR);
  	}
   if (phFitsBinTblEnd(fits) != 0) {
	thError("%s: ERROR - could not skip ext[1] of the frame model", name);
	return(SH_GENERIC_ERROR);
   }
   if (phFitsBinTblHdrRead(fits, NULL, NULL, NULL, quiet) != 0) {
	thError("%s: ERROR - could not load the binary header of ext[2] of frame model", name);
	return(SH_GENERIC_ERROR);
   }
   if (phFitsBinTblEnd(fits) != 0) {
	thError("%s: ERROR - could not skip ext[2] of the frame model", name);
	return(SH_GENERIC_ERROR);
	}

  THBINREGION *simage = thFitsBinregionRead(fits, &status);
  if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not read sky image from frame model", name);
	return(status);
	} 
  if (f->data->sky_binned_image != NULL) thBinregionDel(f->data->sky_binned_image);
  f->data->sky_binned_image = simage;

  BINREGION *sphotoimage = phBinregionFromThBinregion(simage); 
  if (sphotoimage == NULL) {
	thError("%s: ERROR - could not convert frame model binned sky to PHOTO binned sky", name);
	return(SH_GENERIC_ERROR);	
  }

  RANDOM *rand = NULL;
  REGION *sky_image = phBinregionUnBin(sphotoimage, rand, 0, 0);
  if (sky_image == NULL) {
	thError("%s: ERROR - could not UnBin binned sky image", name);
	return(SH_GENERIC_ERROR);
  }
  if (f->data->sky_image != NULL) shRegDel(f->data->sky_image);
  f->data->sky_image = sky_image;

  status = uncalibrate_frame_to_fpC_image(image, calibration, sky_image);
  if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not convert frame model to fpC equivalent", name);
	return(status);
	}

  #endif

  return(SH_SUCCESS);

}

 RET_CODE thFrameFakeImage(FRAME *f, int run, int field, char band, int camcol, char *rerun) {

  char *name = "thFrameFakeImage";

  if (f == NULL) {
    thError("%s: ERROR - null frame passed", name);
    return(SH_GENERIC_ERROR);
  }
  
  if (f->files == NULL) {
    thError("%s: ERROR - null frame files", name);
    return(SH_GENERIC_ERROR);
  }

  RET_CODE status;

 thError("%s: WARNING - faking fpC image", name);
  char *fakefile = "<fake fpC file>";
  strcpy(f->files->phflatframefile, fakefile);

  HDR *hdr;
  hdr = shHdrNew();
  
  status = thImageFakeHdr(hdr, run, field, band, camcol, NULL);
  if (status != SH_SUCCESS) {
    thError("%s: ERROR - could create a fake fpC header", name);
    return(status);
  }

  if (f->id == NULL) {
    f->id = thFrameidNew();
  }
  status = thUpdateFrameidFromHdr(f->id, hdr);
  if (status != SH_SUCCESS) {
    thError("%s: ERROR - could not update (frameid)", name);
    return(status);
  }

  REGION *image;
  image = shRegNew("", 0, 0, TYPE_THPIX);
  status = thCreateFakeImage(image);
  if (status != SH_SUCCESS) {
    thError("%s: ERROR - could not create fake fpC image", name);
    return(status);
  }

  int softbias = 0;

  /* 
     according to RHL some negative DN values are expected in 
     fpC files. this could be due to electronic noise in CCDs
     Jim says this is mainly read noise. 
     so I corrected statictical weight to read : 

     w = g / (s > 0 ? (s + g (readnoise) ^ 2) : g (readnoise ^ 2))
  
  */
  if (shHdrGetInt(hdr, "SOFTBIAS", &softbias) == SH_SUCCESS) {
 	/* note that because the region is already in float format 
 * - writing (int) (pixel + 0.499) - SOFT_BIAS
	in the form pixel + (0.499 - SOFT_BIAS)  will end up adding 0.5 to each pixel
	This is the way PHOTO package's utils.c module handles the conversion from U16 to FL32 but we should not use it here.*/	
   
   shRegAddWithDbl((double) (-softbias), image, image);
    shHdrDelByKeyword(hdr, "SOFTBIAS");
  }

/* the following should be saved so that when outputting the simulation soft bias et al is sent out correctly on the file 
  shHdrDelByKeyword(hdr, "BUNIT");
  shHdrDelByKeyword(hdr, "BUNITS");
*/

/* the following keywords are not allowed to appear more than once in a header
delete here and it will be automatically generated when writing */

  shHdrDelByKeyword(hdr, "SIMPLE");
  shHdrDelByKeyword(hdr, "BITPIX");
  shHdrDelByKeyword(hdr, "NAXIS");
  shHdrDelByKeyword(hdr, "NAXIS1");
  shHdrDelByKeyword(hdr, "NAXIS2");
  shHdrDelByKeyword(hdr, "BZERO");
  shHdrDelByKeyword(hdr, "BSCALE");
  shHdrDelByKeyword(hdr, "EXTEND");

  f->files->hdr = hdr;
  
  if (f->data == NULL) f->data = thFramedataNew();

  if (f->data->image != NULL) shRegDel(f->data->image);
  
  f->data->image = image;

  return(SH_SUCCESS);

}
  
 
RET_CODE thFrameLoadPhMask(FRAME *f) {
  
  char *name = "thFrameLoadPhMask";

  if (f == NULL) {
    thError("%s: ERROR - null frame passed", name);
    return(SH_GENERIC_ERROR);
  }
  
  if (f->files == NULL) {
    thError("%s: ERROR - null frame files", name);
    return(SH_GENERIC_ERROR);
  }

  if (f->data->image == NULL || 
      f->data->image->nrow <= 0 || f->data->image->ncol <= 0) {
    thError("%s: ERROR - nrow, ncol data were not avaialble from the frame. needs to load the flatframe properly", name);
    return(SH_GENERIC_ERROR);
  }

  int nrow, ncol;
  nrow = f->data->image->nrow;
  ncol = f->data->image->ncol;

  RET_CODE status;

  SPANMASK *sm;
  PHFITSFILE *mfile;

  mfile = phFitsBinTblOpen(f->files->phmaskfile, 0, NULL);

  if (mfile == NULL) {
    thError("%s: ERROR - file registery couldn't be allocated for input file %s", 
		   name, f->files->phmaskfile);
    return(SH_GENERIC_ERROR);
  }

  sm = thFitsSpanmaskRead(mfile, nrow, ncol, &status);

  if (status != SH_SUCCESS) {
    thError("%s: ERROR - problem reading the spanmask generated by PHOTO", 
		   name);
    return(status);
  }

  /* 
     the following enlarges the spanmask 
  */

  /*
  S_MASK_INTERP = 0,           pixel's value has been interpolated 
  S_MASK_SATUR,                pixel is/was saturated  
  S_MASK_NOTCHECKED,           pixel was NOT examined for an object 
  S_MASK_OBJECT,               pixel is part of some object 
  S_MASK_BRIGHTOBJECT,         pixel is part of bright object 
  S_MASK_BINOBJECT,             pixel is part of binned object 
  S_MASK_CATOBJECT,            pixel is part of a catalogued object 
  S_MASK_SUBTRACTED,           model has been subtracted from pixel 
  S_MASK_GHOST,                pixel is part of a ghost 
  S_MASK_CR,                   pixel is part of a cosmic ray 
  S_NMASK_TYPES
  */

#if 0
  status = thExpandPhSpanmask(sm);
  if (status != SH_SUCCESS) {
    thError("%s: problem expanding PHOTO spanmask", name);
    return(SH_GENERIC_ERROR);
  }
#endif

  MASK *mask = NULL;
  #if 0
  mask = thMaskGenFromSpanmask(sm, NMASK_TYPES, &status);
  #else
  thError("%s: WARNING - (PHOTO) span masks are not converted to (dervish) style masks", name);
  #endif

  if (f->data == NULL) f->data = thFramedataNew();

  if (f->data->phsm != NULL) {
    thError("%s: WARNING - phsm object already loaded - unloading", name);
    phSpanmaskDel(f->data->phsm);
  }
  if (f->data->phmask != NULL) {
    thError("%s: WARNING - phmask object already loaded - unloading", name);
    shMaskDel(f->data->phmask);
  }

  f->data->phsm = sm;
  f->data->phmask = mask;

  if (status != SH_SUCCESS) {
    thError("%s: problem loading dervish style mask; spanmask successfully loaded", name);
    return(status);
  }

  return(SH_SUCCESS);

}

RET_CODE thFrameLoadPhAtlas(FRAME *f) {
  
  char *name = "thFrameLoadPhAtlas";

  if (f == NULL) {
    thError("%s: ERROR - null frame passed", name);
    return(SH_GENERIC_ERROR);
  }
  
  if (f->files == NULL) {
    thError("%s: ERROR - null frame files", name);
    return(SH_GENERIC_ERROR);
  }

  RET_CODE status;

  CHAIN *atlas;
  PHFITSFILE *Afile;

  Afile = phFitsBinTblOpen(f->files->phatlasfile, 0, NULL);

  if (Afile == NULL) {
    thError("%s: ERROR - file registery couldn't be allocated for input file %s", 
		   name, f->files->phatlasfile);
    return(SH_GENERIC_ERROR);
  }

  atlas = thFitsAtlasRead(Afile, &status);
  if (status != SH_SUCCESS) {
    thError("%s: ERROR - problem reading the spanmask generated by PHOTO", 
		   name);
    return(status);
  }
  if (f->data == NULL) f->data = thFramedataNew();

  if (f->data->phatlas != NULL) {
    thError("%s: WARNING - (phatlas) object already loaded - unloading", name);
    shChainDestroy(f->data->phatlas, &phAtlasImageDel);
    f->data->phatlas = NULL;
  }

  f->data->phatlas = atlas;

  #if DEBUG_PROC_ATLAS_LOAD
  output_atlas_info(atlas);
  #endif
  return(SH_SUCCESS);

}




RET_CODE thFrameLoadSkybasisset(FRAME *f) {

  char *name = "thFramedataLoadSkybasis";

  if (f == NULL) {
    thError("%s: ERROR - null frame passed", name);
    return(SH_GENERIC_ERROR);
  }
  
  if (f->files == NULL) {
    thError("%s: ERROR - null frame files", name);
    return(SH_GENERIC_ERROR);
  }
  
  if ((f->data) == NULL) {
    thError("%s: ERROR - no data loaded onto frame, amplifier data is needed",
		   name);
    return(SH_GENERIC_ERROR);
  }

  if ((f->data->ampl == NULL) || (shChainSize(f->data->ampl) == 0)) {
    thError("%s: ERROR - no amplifier info loaded", name);
    return(SH_GENERIC_ERROR);
  }

  if (f->data->image == NULL) {
    thError("%s: ERROR - image should be loaded to determine the image size", 
		   name);
    return(SH_GENERIC_ERROR);
  }

  PHFITSFILE *bfile;
  CHAIN *bchain;
  RET_CODE status;

  if ( f->proc != NULL) {

    status = thProcLoadSbf(((PROCESS *) (f->proc)), f->files->thsbfile);
    if (status != SH_SUCCESS) {
      thError("%s: WARNING - could not load (sbf) onto proc from (%s)", 
		     name,  f->files->thsbfile);
    }

    bchain = thProcTankGetChainByFilename(((PROCESS *) (f->proc))->sbftank, f->files->thsbfile);

  } else {
  
    bfile = phFitsBinTblOpen(f->files->thsbfile, 0, NULL);

    bchain = thFitsFuncRead(bfile, &status);
  
    phFitsBinTblClose(bfile);

    if (status != SH_SUCCESS) {
      thError("%s: ERROR - problem reading the basis function files", name);
      return(status);
    }
  }
    
  /* 
     now delete the constant function from the list of basis functions 
     because it is degenrate with amplifier delta functions
  */

  int nconst = 0, pos, *indices;
  FUNC *bfunc;

  /* creating the indicies for constant functions */
  indices = thCalloc(FUNC_N_INDICES, sizeof(int));
  while ((pos = thGetFuncPosByPar(bchain, indices, NULL)) >= 0) {
    bfunc = (FUNC *)shChainElementRemByPos(bchain, pos);
    thFuncDel(bfunc);
    /*
    thChainElementDelByPos(bchain, pos);
    shChainElementChangeByPos(bchain, pos, NULL);
    */
    nconst++;
  }
  
  if (nconst > 1) {
    thError("%s: ERROR - too many (%d) constant basis functions were detected in the basis set", name, nconst);
    return(SH_GENERIC_ERROR);
  }
  
  
  /* now combin AMPL and FUNC chains to get the BASIS chain you want */

  SKYBASISSET *sbs = NULL;
  sbs = thSkybasissetNew();

  status = thSkybasissetAddAmplChain(sbs, f->data->ampl);
  if (status != SH_SUCCESS) {
    thError("%s: ERROR - problem adding function chain to the sky basis set", 
		   name);
    return(status);
  }
  
  sbs->nrow = f->data->image->nrow;
  sbs->ncol = f->data->image->ncol;

  status = thSkybasissetAddFuncChain(sbs, bchain);

  if (status != SH_SUCCESS) {
    thError("%s: ERROR - problem adding function chain to the sky basis set", 
		   name);
    return(status);
  }

  f->data->sbs = sbs;

  return(SH_SUCCESS);

}
  
RET_CODE thFrameLoadObjcbasisset(FRAME *f) {

  char *name = "thFrameLoadObjcbasisset";
  
  thError("%s: this function is not finalized", name);
  
  return(SH_GENERIC_ERROR);

}

RET_CODE create_quick_stat_for_objc_io_list_type(CHAIN *chain, PHSTAT *phstat, OBJ_TYPE type, int band) {
char *name = "create_quick_stat_for_objc_io_list_type";
shAssert(chain != NULL);
shAssert(phstat != NULL);
RET_CODE status;
phStatInit(phstat);
int i, n;
n = shChainSize(chain);
float *data = thCalloc(n, sizeof(float));
float *dataerror = thCalloc(n, sizeof(float));
int nobjc = 0;
WOBJC_IO *objc = NULL;
for (i = 0; i < n; i++) {
	objc = shChainElementGetByPos(chain, i);
	if (objc->objc_type == type) {
		data[nobjc] = objc->sky[band];
		dataerror[nobjc] = objc->skyErr[band];
		nobjc++;
	}
}

printf("%s: creating sky statistics \n", name);
fflush(stdout);
fflush(stderr);

BASIC_STAT *stat = phstat->sky;
status = thBasicStatFromArray(data, dataerror, stat, nobjc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract statistics from array", name);
	return(status);
	}
thFree(data);
thFree(dataerror);
return(SH_SUCCESS);

}

RET_CODE create_quick_stat_for_calib_phobjc_io_list_type(CHAIN *chain, PHSTAT *phstat, OBJ_TYPE type, int band) {
char *name = "create_quick_stat_for_calib_phobjc_io_list_type";
shAssert(chain != NULL);
shAssert(phstat != NULL);
RET_CODE status;
phStatInit(phstat);
int i, n;
n = shChainSize(chain);
float *data = thCalloc(n, sizeof(float));
float *dataerror = thCalloc(n, sizeof(float));
int nobjc = 0;
CALIB_PHOBJC_IO *objc = NULL;
for (i = 0; i < n; i++) {
	objc = shChainElementGetByPos(chain, i);
	if (objc->objc_type == type) {
		data[nobjc] = objc->skyflux[band];
		dataerror[nobjc] = sqrt(1.0 / objc->skyflux_ivar[band]);
		if (!isnan(data[nobjc]) && !isinf(data[nobjc]) && isnan(dataerror[nobjc]) && isinf(dataerror[nobjc])) nobjc++;
	}
}

if (nobjc < 3) {
	thError("%s: WARNING - too few (%d) of (type = '%s')", name, nobjc, shEnumNameGetFromValue("OBJ_TYPE", type));
	return(SH_SUCCESS);
}
printf("%s: creating sky statistics \n", name);
fflush(stdout);
fflush(stderr);

BASIC_STAT *stat = phstat->sky;
status = thBasicStatFromArray(data, dataerror, stat, nobjc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract statistics from array", name);
	return(status);
	}
thFree(data);
thFree(dataerror);
return(SH_SUCCESS);

}



RET_CODE create_quick_stat_for_objc_io_list(CHAIN *chain, FRAMESTAT *fstat, int band) {
char *name = "create_quick_stat_for_objc_io_list";	
shAssert(chain != NULL);
shAssert(fstat != NULL);
shAssert(fstat->phstat != NULL);
OBJ_TYPE type;
RET_CODE status;
for (type = 0; type < OBJ_NTYPE; type++) {
	PHSTAT *phstat = fstat->phstat[type];
	status = create_quick_stat_for_objc_io_list_type(chain, phstat, type, band);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not create quick stat for type '%s'", name, shEnumNameGetFromValue("OBJC_TYPE", type));
		return(status);
	}
	}
return(SH_SUCCESS);
}

RET_CODE create_quick_stat_for_calib_phobjc_io_list(CHAIN *chain, FRAMESTAT *fstat, int band) {
char *name = "create_quick_stat_for_calib_objc_io_list";	
shAssert(chain != NULL);
shAssert(fstat != NULL);
if (shChainSize(chain) == 0) {
	thError("%s: WARNING - empty input chain for objects passed", name);
	return(SH_SUCCESS);
}
shAssert(fstat->phcalibstat != NULL);
OBJ_TYPE type;
RET_CODE status;
for (type = 0; type < OBJ_NTYPE; type++) {
	PHSTAT *phstat = fstat->phcalibstat[type];
	status = create_quick_stat_for_calib_phobjc_io_list_type(chain, phstat, type, band);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not create quick stat for type '%s'", name, shEnumNameGetFromValue("OBJC_TYPE", type));
		return(status);
	}
	}
return(SH_SUCCESS);
}


int compare_wobjc_io_count_galaxy(const void *x1, const void *x2) {
	WOBJC_IO *y1, *y2;
	shAssert(x1 != NULL);
	shAssert(x2 != NULL);
	y1 = *(WOBJC_IO **) x1;
	y2 = *(WOBJC_IO **) x2;
	int res;
	if (y1->objc_type == OBJ_GALAXY && y2->objc_type == OBJ_GALAXY) {
		float z1, z2;
		z1 = y1->counts_deV[R_BAND] * y1->fracPSF[R_BAND] + y1->counts_exp[R_BAND] * (1.0 - y1->fracPSF[R_BAND]);
		z2 = y2->counts_deV[R_BAND] * y2->fracPSF[R_BAND] + y2->counts_exp[R_BAND] * (1.0 - y2->fracPSF[R_BAND]);
		if (z1 > z2) {
			res = -1;
		} else if (z1 < z2) {
			res = 1;
		} else {
			res = 0;
		}
	} else if (y1->objc_type == OBJ_GALAXY) {
		res = -1;
	} else if (y2->objc_type == OBJ_GALAXY) {
		res = 1;
	} else {
		res = 0;
	}
	return(res);
}

RET_CODE thFrameLoadPhObjc(FRAME *f) {

  char *name = "thFrameLoadPhObjc";
  
  if (f == NULL) {
    thError("%s: ERROR - null frame passed", name);
    return(SH_GENERIC_ERROR);
  }
  
  if (f->files == NULL) {
    thError("%s: ERROR - null (files) in (frame) ", name);
    return(SH_GENERIC_ERROR);
  }

  FRAMEDATA *data;
  if ((data = f->data) == NULL) {
	thError("%s: ERROR - null (data) in (frame)", name);
	return(SH_GENERIC_ERROR);
	}

  char *fname = f->files->phobjcfile; 

   /* reading the main header */
   RET_CODE status;
   HDR *hdr = shHdrNew();
   status = shHdrReadAsFits(hdr, fname, DEF_NONE, NULL, 0, 0);
   if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the main header from file '%s'", name, fname);
	return(status);
   }
   char hdrline[MX_STRING_LEN];
   memset(hdrline, '\0', MX_STRING_LEN *sizeof(char));
   char *keyword = "FILTERS";
   status = shHdrGetAscii(hdr, keyword, hdrline);
   if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get header line for (keyword = '%s') in file '%s'", name, keyword, fname);
	return(status);
   }
   status = create_bndex_from_hdrline(hdrline, data->bndex);
   if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not create (bndex array) from header line in file '%s'", name, fname);
	return(status);
	}  
   status = thFramePutHdr2(f, hdr);
   if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not put in position header for (file = '%s')", name, fname);
	return(status);
	}


/* now opening the second hdu and reading the contents */
  PHFITSFILE *ofile;
  ofile = phFitsBinTblOpen(fname, 0, NULL);
  
  if (ofile == NULL) {
    thError("%s: ERROR - cannot open the phobjc file '%s'", name, fname);
    return(SH_GENERIC_ERROR);
  }

  /* 
     this function has the ability to append the new object lists 
     to the old one, but we return an error if an object list is already
     loaded
  */
  if (data->phobjc != NULL && shChainSize(data->phobjc) != 0) {
    thError("%s: ERROR - some objects seem to already have been loaded, unload before loading new objects", name);
    return(SH_GENERIC_ERROR);
  }
  if (data->phobjc_source != NULL && shChainSize(data->phobjc_source) != 0) {
    thError("%s: ERROR - some objects previously loaded onto the source chain, unload before loading new objects", name);
    return(SH_GENERIC_ERROR);
  }

  CHAIN *ochain;
  ochain = thFitsWObjcIoRead(ofile, &status);
  if (status != SH_SUCCESS) {
    thError("%s: could not read the object chain from the fits file", name);
    return(status);
  }

  status = choose_good_objects(ochain);
  if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not delete unnecessary objects", name);
	return(status);
	}

  #if DEBUG_PROC_PHOBJC_INFO
  output_phobjc_chain_info(ochain);
  #endif	



  int band = -1;	
  status = thFrameGetBand(f, &band);
  if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (band) from (frame)", name);
	return(status);
	}
  status = create_quick_stat_for_objc_io_list(ochain, f->stat, band);
  if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not create quick stat for (objc_io) list", name);
	return(status);
	}

  if (data->phobjc_source == NULL) {
    data->phobjc_source = ochain;
    
   #if ORDER_PHOBJC_LIST_AFTER_LOAD
    void *sort_function = NULL;
    sort_function = &compare_wobjc_io_count_galaxy;
    status = thChainQsort(data->phobjc_source, sort_function);
    if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not sort the (objc) list", name);
	return(status);
	}
    #endif	
  } else {
    shChainJoin(data->phobjc_source, ochain);
  }
  
  #if USE_FNAL_OBJC_IO
  char *wobjc_io_name = "FNAL_OBJC_IO";
  #else
  char *wobjc_io_name = "OBJC_IO";
  #endif

  CHAIN *ochain_copy = shChainNew(wobjc_io_name);
  status = thWObjcIOCopyChain(ochain, ochain_copy);
  if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy source chain to secondary chain", name);
		return(status);
	}
   if (data->phobjc == NULL) {
   	data->phobjc = ochain_copy;
   } else {
	 thError("%s: WARNING - joining the already existing (phobjc, size = %d) chain with the currently reach (objc) chain", name, shChainSize(data->phobjc));
   	 shChainJoin(data->phobjc, ochain_copy);
   }

	 
  if (phFitsBinTblEnd(ofile) != 0) {
	thError("%s: ERROR - could not end binary fits file", name);
	return(SH_GENERIC_ERROR);
	} 
   if (phFitsBinTblClose(ofile) != 0) {
	thError("%s: ERROR - could not close binary fits file", name);
	return(SH_GENERIC_ERROR);
	} 
  return(SH_SUCCESS); 
}

RET_CODE thFrameLoadPhCalibObjc(FRAME *f, GC_SOURCE gcsource) {
char *name = "thFrameLoadPhCalibObjc";
RET_CODE status;
if (gcsource == SWEEP_FILE || gcsource == SWEEP_FILE_AND_WRITE) {
	status = thFrameLoadPhCalibObjcFromSweepFile(f);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not load calib_phobjc_io structure from sweep file", name);
		return(status);
	}
} else if (gcsource == GC_FILE) {
	status = thFrameLoadPhCalibObjcFromGC(f);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not load calib_phobjc_io structure from galactic calib data", name);
		return(status);
	}
} else {
	thError("%s: ERROR - (gcsource = '%s') not supported", name, shEnumNameGetFromValue("GCSOURCE", gcsource));
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}


RET_CODE thFrameLoadPhCalibObjcFromGC(FRAME *f) {
char *name = "thFrameLoadPhCalibObjcFromGC";
shAssert(f != NULL);
FRAMEDATA *data = f->data;
shAssert(data != NULL);
CHAIN *phobjcs = data->phobjc;
CHAIN *cwphobjcs = data->cwphobjc;
CHAIN *cphobjcs = data->cphobjc;
CHAIN *gcs = data->gc;

if (phobjcs == NULL) {
	thError("%s: ERROR - null (phobjcs) chain found in (data)", name);
	return(SH_GENERIC_ERROR);
}
if (cwphobjcs == NULL) {
	thError("%s: ERROR - null (cwphobjcs) chain found in (data)", name);
	return(SH_GENERIC_ERROR);
}
shAssert(phobjcs != NULL);
shAssert(cwphobjcs != NULL);
shAssert(shChainSize(phobjcs) == shChainSize(cwphobjcs));
shAssert(cphobjcs == NULL || shChainSize(cphobjcs) == 0);
shAssert(gcs != NULL);
if (cphobjcs == NULL) {
	cphobjcs = shChainNew("CALIB_PHOBJC_IO");
	data->cphobjc = cphobjcs;
}
int i, n = shChainSize(cwphobjcs);
for (i = 0; i < n; i++) {
	CALIB_PHOBJC_IO *cphobjc = thCalibPhObjcIoNew();
	WOBJC_IO *phobjc = shChainElementGetByPos(phobjcs, i);
	CALIB_WOBJC_IO *cwphobjc = shChainElementGetByPos(cwphobjcs, i);
	RET_CODE status = thCalibPhObjcIoFromGC(cphobjc, cwphobjc, phobjc, gcs);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not generate 'CALIB_PHOBJC_IO' for (iobjc = %d)", name, i);
		return(status);
	}
	shChainElementAddByPos(cphobjcs, cphobjc, "CALIB_PHOBJC_IO", TAIL, AFTER);
}

return(SH_SUCCESS);
}

RET_CODE thFrameLoadPhCalibObjcFromSweepFile(FRAME *f) {

  char *name = "thFrameLoadPhCalibObjcFromSweepFile";
  
  if (f == NULL) {
    thError("%s: ERROR - null frame passed", name);
    return(SH_GENERIC_ERROR);
  }
  
  if (f->files == NULL) {
    thError("%s: ERROR - null (files) in (frame) ", name);
    return(SH_GENERIC_ERROR);
  }

  FRAMEDATA *data;
  if ((data = f->data) == NULL) {
	thError("%s: ERROR - null (data) in (frame)", name);
	return(SH_GENERIC_ERROR);
	}

  char *fname = f->files->photoObjcfile; 

   /* reading the main header */
   RET_CODE status;
   HDR *hdr = shHdrNew();
   status = shHdrReadAsFits(hdr, fname, DEF_NONE, NULL, 0, 0);
   if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the main header from file '%s'", name, fname);
	return(status);
   }

char *keyword;
#if 0
int lineno;
char line[1000];
shHdrGetLineno(hdr, "BITPIX", &lineno);
shHdrMakeLineWithInt("BITPIX", 8, NULL, line);
shHdrReplaceLine(hdr, lineno, line);
#else
int bitpix;
keyword = "BITPIX";
status = shHdrGetInt(hdr, keyword, &bitpix);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the value for keyword '%s' in the header", name, keyword);
	thError("%s: ERROR - could not interpret header from file '%s'", name, fname);
	return(status);
}
if (bitpix != 8) {
	thError("%s: ERROR - PHOTO's readers expect BITPIX=8 in header[0], received (%d)", name, bitpix);
	thError("%s: ERROR - could not interpret header from file '%s'", name, fname);
 	return(SH_GENERIC_ERROR);
}
#endif

  shHdrDelByKeyword(hdr, "SIMPLE");
  shHdrDelByKeyword(hdr, "BITPIX");
  shHdrDelByKeyword(hdr, "NAXIS");
  shHdrDelByKeyword(hdr, "NAXIS1");
  shHdrDelByKeyword(hdr, "NAXIS2");
  shHdrDelByKeyword(hdr, "BZERO");
  shHdrDelByKeyword(hdr, "BSCALE");
  shHdrDelByKeyword(hdr, "EXTEND");

   char hdrline[MX_STRING_LEN];
   memset(hdrline, '\0', MX_STRING_LEN *sizeof(char));
   keyword = "FILTERS";
   status = shHdrGetAscii(hdr, keyword, hdrline);
   if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get header line for (keyword = '%s') in file '%s'", name, keyword, fname);
	return(status);
   }
   status = create_bndex_from_hdrline(hdrline, data->bndex_calib);
   if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not create (bndex array) from header line in file '%s'", name, fname);
	return(status);
	}  
   status = thFramePutHdr3(f, hdr);
   if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not put in position header for (file = '%s')", name, fname);
	return(status);
	}
/* now opening the second hdu and reading the contents */

#if CALIB_PHOBJC_IO_USE_ALTERED_FITS_PACKAGE
  THFITSFILE *ofile;
  ofile = thFitsBinTblOpen(fname, 0, hdr);
#else 
  PHFITSFILE *ofile;
  ofile = phFitsBinTblOpen(fname, 0, hdr);
#endif
  if (ofile == NULL) {
    thError("%s: ERROR - cannot open the photoObj file '%s'", name, fname);
    return(SH_GENERIC_ERROR);
  }


	#if DEBUG_PROC_IO
	char *debugfile = "/u/khosrow/thesis/opt/debug-Calib-PhObjc-IO.fits";
	status = thDebugWriteCalibPhObjcIo(debugfile, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not write the debug fits file '%s'", 
			name, debugfile);
		return(status);
	}
	#endif

  /* 
     this function has the ability to append the new object lists 
     to the old one, but we return an error if an object list is already
     loaded
  */
  if (data->cphobjc != NULL && shChainSize(data->cphobjc) != 0) {
    thError("%s: ERROR - some objects seem to already have been loaded, unload before loading new objects", name);
    return(SH_GENERIC_ERROR);
  }
  
  CHAIN *ochain;
  ochain = thFitsCalibPhObjcIoRead(ofile, &status);
  if (status != SH_SUCCESS) {
    thError("%s: could not read the object chain from the fits file '%s'", name, fname);
    return(status);
  }

  status = choose_good_calib_objects(ochain);
  if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not delete unnecessary objects", name);
	return(status);
	}

  int band = -1;	
  status = thFrameGetBand(f, &band);
  if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (band) from (frame)", name);
	return(status);
	}
  status = create_quick_stat_for_calib_phobjc_io_list(ochain, f->stat, band);
  if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not create quick stat for (objc_io) list", name);
	return(status);
	}

  if (data->cphobjc == NULL) {
    data->cphobjc = ochain;
  } else {
    shChainJoin(data->cphobjc, ochain);
  }
 
#if CALIB_PHOBJC_IO_USE_ALTERED_FITS_PACKAGE
   if (thFitsBinTblEnd(ofile) != 0) {
	thError("%s: ERROR - could not end binary fits file '%s'", name, fname);
	return(SH_GENERIC_ERROR);
	} 
   if (thFitsBinTblClose(ofile) != 0) {
	thError("%s: ERROR - could not close binary fits file '%s'", name, fname);
	return(SH_GENERIC_ERROR);
	}
#else
 if (phFitsBinTblEnd(ofile) != 0) {
	thError("%s: ERROR - could not end binary fits file '%s'", name, fname);
	return(SH_GENERIC_ERROR);
	} 
   if (phFitsBinTblClose(ofile) != 0) {
	thError("%s: ERROR - could not close binary fits file '%s'", name, fname);
	return(SH_GENERIC_ERROR);
	}
#endif

 
  return(SH_SUCCESS); 
}






RET_CODE thFrameLoadCalib(FRAME *f) {

  char *name = "thFrameLoadCalib";
  
  if (f == NULL) {
    thError("%s: ERROR - null frame passed", name);
    return(SH_GENERIC_ERROR);
  }
  
  if (f->files == NULL) {
    thError("%s: ERROR - null (files) in (frame) ", name);
    return(SH_GENERIC_ERROR);
  }

  FRAMEDATA *data;
  if ((data = f->data) == NULL) {
	thError("%s: ERROR - null (data) in (frame)", name);
	return(SH_GENERIC_ERROR);
	}

  FRAMEID *id;
  if ((id = f->id) == NULL) {
	thError("%s: ERROR - null (id) in (frame)", name);
	return(SH_GENERIC_ERROR);
	}
 
  PHFITSFILE *ofile;
  ofile = phFitsBinTblOpen(f->files->phcalibfile, 0, NULL);
  
  if (ofile == NULL) {
    thError("%s: ERROR - could not open the phcalib file '%s'", 
		name, f->files->phcalibfile);
    return(SH_GENERIC_ERROR);
  }

  /* 
     this function has the ability to append the new object lists 
     to the old one, but we return an error if an object list is already
     loaded
  */
  if (data->phcalib != NULL) {
    thError("%s: some objects seem to already have been loaded, unload before loading new objects", name);
    return(SH_GENERIC_ERROR);
  }
  
  RET_CODE status;
  PHCALIB *fcalib;
  fcalib = thFitsFrameCalibRead(ofile, id->field, &status);

  if (status != SH_SUCCESS) {
    thError("%s: could not read the object chain from the fits file", name);
    return(status);
  }

  if (data->phcalib == NULL) {
    data->phcalib = fcalib;
  } else {
    status = thPhcalibCopy(data->phcalib, fcalib);
    if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy (fcalib) onto (phcalib) in (fdata)", name);
		return(status);
	}
	thPhcalibDel(fcalib);
  }
  
  return(SH_SUCCESS);  
}

RET_CODE thFrameLoadPhPsf(FRAME *f) {

  /* 
     the function currently loads psf for only one frame
     should we generate a psf bank inside each process ? 

  */

  char *name = "thFrameLoadPhPsf"; 
  if (f == NULL) {
    thError("%s: ERROR - null (frame)", name);
    return(SH_GENERIC_ERROR);
  }
  
  if (f->files == NULL) {
    thError("%s: ERROR - null (files) in (frame)", name);
    return(SH_GENERIC_ERROR);
  }

  if (f->files->phpsfile == NULL || f->files->phpsfile[0] == '\0') {
    thError("%s: ERROR - null (phpsf filename)", name);
    return(SH_GENERIC_ERROR);
  }

  if (f->data->phpsf != NULL) {
    if (f->data->phpsf->basis != NULL || 
	f->data->phpsf->wing != NULL) {
      thError("%s: ERROR - phPsf data seems already loaded - should unload before calling", 
		     name);
      return(SH_GENERIC_ERROR);
    }
  } else {
    f->data->phpsf = thPhpsfmodelNew();
  }
  PHPSFMODEL *phpsf = f->data->phpsf;

  char *psfilename;
  psfilename = f->files->phpsfile;

  RET_CODE status;

  /* open the psField file and get the filter number from the header */

  /* thw following header reading command changes the value of f->id !!! */

  HDR *hdr;
  hdr = shHdrNew();
  
  status = shHdrReadAsFits(hdr, psfilename, DEF_NONE, NULL, 0, 0);

  if (status != SH_SUCCESS) {
    shError("%s: ERROR - could not read the header for file (%s)", 
	    name, psfilename);
    return(status);
  }
  

  char *filterlist;
  int extno, nfilter;
  filterlist = (char *) thCalloc(MX_STRING_LEN, sizeof(char));

  status = shHdrGetAscii(hdr, "FILTERS", filterlist);
  if (status != SH_SUCCESS) {
    shError("%s: ERROR - could not find keyword FILTER in the header", 
	    name);
    return(status);
  }
  extno = find_filter_no_in_list(f->id->filter, filterlist, &nfilter);
  if (extno < 0) {
    shError("%s: ERROR - could not find filter (%d) in filter list (%s)",
	    name, f->id->filter, filterlist);
    return(SH_GENERIC_ERROR);
  }
  
  shHdrDel(hdr);

  /* now put the filter index in the phpsf */
  phpsf->filter = extno;

  /* open the binary extensions in the psf-fitsfile */
  
  PHFITSFILE *psfits;
  psfits = phFitsBinTblOpen(psfilename, 0, NULL);
  if (psfits == NULL) {
    thError("%s: ERROR - could not open the binary table in file (%s)", 
		   name, psfilename);
    return(SH_GENERIC_ERROR);
  }
  
  /* go to the specific extension and read the basis */

  int i, nfitsrow, phstatus, quiet = 0;
  for (i = 0; i < extno; i++) {
    phstatus = phFitsBinTblHdrRead(psfits, NULL, NULL, 
				   &nfitsrow, quiet);
    if (phstatus < 0) {
      thError("%s: ERROR - problem reading past ext (%d) in file (%s)", 
		     name, i, psfilename);
      phFitsBinTblClose(psfits);
      return(SH_GENERIC_ERROR);
    }
    phstatus = phFitsBinTblEnd(psfits);
    if (phstatus < 0) {
      thError("%s: ERROR - problem reading past ext (%d) in file (%s)", 
		     name, i, psfilename);
      phFitsBinTblClose(psfits);
      return(SH_GENERIC_ERROR);
    }
  }

  /* now read the klc chain */
  phpsf->basis = thFitsPhPsfBasisRead(psfits, &status);
  if (status != SH_SUCCESS) {
    thError("%s: ERROR - problem reading the central pars of the psf from file (%s)", name, psfilename);
    phFitsBinTblClose(psfits);
    return(SH_GENERIC_ERROR);
  }
  phstatus = phFitsBinTblEnd(psfits);
  if (phstatus < 0) {
      thError("%s: ERROR - problem reading past ext (%d) in file (%s)", 
		     name, i, psfilename);
      phFitsBinTblClose(psfits);
      return(SH_GENERIC_ERROR);
  }
  
  /* 
     now pass the remaining extensions until the head 
     of the extension where wing data is stored 
  */

  for (i = extno + 1; i < nfilter; i++) {
    phstatus = phFitsBinTblHdrRead(psfits, NULL, NULL, 
				   &nfitsrow, quiet);
    if (phstatus < 0) {
      thError("%s: ERROR - problem reading past ext (%d) in file (%s)", name, i, psfilename);
      phFitsBinTblClose(psfits);
      return(SH_GENERIC_ERROR);
    }
    phstatus = phFitsBinTblEnd(psfits);
    if (phstatus < 0) {
      thError("%s: ERROR - problem reading past ext (%d) in file (%s)", name, i, psfilename);
      phFitsBinTblClose(psfits);
      return(SH_GENERIC_ERROR);
    }
  }

  /* import the wings */
  
  phpsf->wing = thFitsPhPsfWingRead(psfits, &status);

  phstatus = phFitsBinTblEnd(psfits);
  if (phstatus < 0) {
      thError("%s: ERROR - problem reading past ext (%d) in file (%s)", 
		     name, nfilter, psfilename);
      phFitsBinTblClose(psfits);
      return(SH_GENERIC_ERROR);
  }

#if DO_CRUDE_CALIB
  /* now pass until flux20 extension is reached */
  int cc_extno = FLUX20_EXTNO; /* based on the investigation from the currently available psField files */
  for (i = nfilter+2; i < cc_extno; i++) {
	phstatus = phFitsBinTblHdrRead(psfits, NULL, NULL, &nfitsrow, quiet);
    if (phstatus < 0) {
      thError("%s: ERROR - problem reading past ext (%d) in file (%s)", name, i, psfilename);
      phFitsBinTblClose(psfits);
      return(SH_GENERIC_ERROR);
    }
    phstatus = phFitsBinTblEnd(psfits);
    if (phstatus < 0) {
      thError("%s: ERROR - problem reading past ext (%d) in file (%s)", name, i, psfilename);
      phFitsBinTblClose(psfits);
      return(SH_GENERIC_ERROR);
    }
  }
 /* now read flux20 informarion */
  phstatus = phFitsBinTblHdrRead(psfits, "CRUDE_CALIB", NULL, &nfitsrow, quiet);
    if (phstatus < 0) {
      thError("%s: ERROR - problem reading past ext (%d) in file (%s)", name, cc_extno, psfilename);
      phFitsBinTblClose(psfits);
      return(SH_GENERIC_ERROR);
    }
#if DEBUG_PROC
	printf("%s: DEBUG - nfilter = %d, nfitsrow = %d \n", name, nfilter, nfitsrow);
#endif
  if (nfilter > nfitsrow) {
	thError("%s: ERROR - number of filters (%d) is greater than number of rows (%d) available in extention (%d) of file '%s'", name, nfilter, nfitsrow, cc_extno, psfilename);
	phFitsBinTblClose(psfits);
	return(SH_GENERIC_ERROR);
  }

  CHAIN *ccs = NULL;
  if ((ccs = phpsf->ccs) == NULL) {
	phpsf->ccs = shChainNew("CRUDE_CALIB");
	ccs = phpsf->ccs;
	}
  shAssert(strcmp(shNameGetFromType(shChainTypeGet(ccs)), "CRUDE_CALIB") == 0);
  shAssert(shChainSize(ccs) == 0);
int row;
  CRUDE_CALIB *cc = NULL;
  for (row = 0; row < nfitsrow; row++) {

  cc = thCrudeCalibNew();
  phstatus = phFitsBinTblRowRead(psfits, cc);
  if (phstatus != 0) {
	thError("%s: ERROR - could not read row (%d) in extension (%d) of file '%s'", name, row, cc_extno, psfilename);
	phFitsBinTblClose(psfits);
	return(SH_GENERIC_ERROR);
  }

  status = thCrudeCalibInsertBValue(cc);
  shAssert(status == SH_SUCCESS); 
  shChainElementAddByPos(ccs, cc, "CRUDE_CALIB", TAIL, AFTER);
  if (row == extno) phpsf->cc = cc;

#if DEBUG_PROC 
printf("%s: DEBUG - flux20 = %g, flux20Err = %g, filter = %c, b = %g \n", name, cc->flux20, cc->flux20Err, cc->filter, cc->b);
#endif

}
	cc = phpsf->cc;
	/* adding crude calibration keywords to hdr */
	HDR *hdr2 = NULL;
	status = thFrameGetHdr(f, &hdr2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get main (hdr) from (frame)", name);
		return(status);
	}
	if (hdr2 == NULL) {
		thError("%s: WARNING - null main (hdr) found in (frame) - cannot add (crude_calib) info to header", name);
	} else {
		char *keyword, *comment;
		keyword = "FLUX20";
		comment = "for crude calib - psField";
		shHdrDelByKeyword(hdr2, keyword);
		status = shHdrInsertDbl(hdr2, keyword, (double) cc->flux20, comment);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not add keyword '%s' to (hdr)", name, keyword);
			return(status);
		}
		keyword = "BFILTER";
		comment = "for crude calib - psField";
		shHdrDelByKeyword(hdr2, keyword);
		status = shHdrInsertDbl(hdr2, keyword, (double) cc->b, comment);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not add keyword '%s' to (hdr)", name, keyword);
			return(status);
		}
	}	
#endif
  /* close the file */
  phstatus = phFitsBinTblClose(psfits);
  if (phstatus < 0) {
    thError("%s: ERROR - problem closing fits binary file (%s)", 
		   name, psfilename);
    return(SH_GENERIC_ERROR);
  }
    
  return(SH_SUCCESS);
  
}

int find_filter_no_in_list(FILTER filter, char *filterlist, int *nfilter) {
  
  char *name = "find_filter_no_in_list";

  if (filterlist == NULL || filterlist[0] == '\0') {
    thError("%s: null input", name);
    return(-1);
  }

  int i;
  int pos[TH_N_FILTER];

  for  (i = 0; i < TH_N_FILTER; i++) {
    pos[i] = -1;
  }

  int loc = 0, filternum = 0; 
  for (i = 0; i < strlen(filterlist); i++) {
    switch (filterlist[i]) {
    case 'u':
    case 'U': 
      pos[THFILTER_U] = loc;
      loc++;
      filternum++;
      break;
    case 'g':
    case 'G':
      pos[THFILTER_G] = loc;
      loc++;
      filternum++;
      break;
    case 'r':
    case 'R':
      pos[THFILTER_R] = loc;
      loc++;
      filternum++;
      break;
    case 'i':
    case 'I':
      pos[THFILTER_I] = loc;
      loc++;
      filternum++;
      break;
    case 'z':
    case 'Z':
      pos[THFILTER_Z] = loc;
      loc++;
      filternum++;
      break;
    case ' ':
      break;
    default:
      loc++;
      filternum++;
    }

  }
  
  if (nfilter != NULL) *nfilter = filternum;
  return(pos[filter]);  
  }
   
RET_CODE thFrameLoadThPsf(FRAME *f) {

  char *name = "thFrameLoadThPsf";
  
  if (f == NULL) {
    thError("%s: ERROR - null frame passed", name);
    return(SH_GENERIC_ERROR);
  }
  
  if (f->data->phpsf == NULL) {
    thError("%s: ERROR - PHOTO PSF not loaded", name);
    return(SH_GENERIC_ERROR);
  }
  if (f->data->phpsf->basis == NULL) {
    thError("%s: ERROR - inner PHOTO PSF not loaded.", name);
    return(SH_GENERIC_ERROR);
  }
  if (f->data->phpsf->wing == NULL) {
    thError("%s: ERROR - PHOTO wings not loaded, should be loaded before calling this function");
    return(SH_GENERIC_ERROR);
  }
  if (f->psf != NULL) {
    thError("%s: ERROR - PSF already loaded", name);
    return(SH_GENERIC_ERROR);
  }
  RET_CODE status;
  f->psf = thConvertPhPsfToThPsf (f->data->phpsf, &status);
  if (status != SH_SUCCESS) {
    	thError("%s: ERROR - problem casting PSF data into structure", name);
	return(status);
  }
  
  return(SH_SUCCESS);
}


RET_CODE thFrameLoadThPsfFromFile(FRAME *f) {

  char *name = "thFrameLoadThPsfFromFile";

  if (f == NULL) {
    thError("%s: null input", name);
    return(SH_GENERIC_ERROR);
  }

  if (f->files == NULL) {
    thError("%s: null (files) in frame", name);
    return(SH_GENERIC_ERROR);
  }

  if (f->files->thpsfile == NULL || f->files->thpsfile[0] == '\0') {
    thError("%s: empty (thps) file name", name);
    return(SH_GENERIC_ERROR);
  }

  if (f->data == NULL) {
    f->data = (FRAMEDATA *) thFramedataNew();
  }

  if (f->psf != NULL) {
    thError("%s: (thpsf) already loaded", name);
    return(SH_GENERIC_ERROR);
  }

  f->psf = (PSFMODEL *) thPsfmodelNew();

  /* now opening the file */

  int phstatus;
  char *psfilename;
  psfilename = f->files->thpsfile;

  PHFITSFILE *psfits;
  psfits = phFitsBinTblOpen(psfilename, 0, NULL);
  if (psfits == NULL) {
    thError("%s: could not open the binary table in file (%s)", 
		   name, psfilename);
    return(SH_GENERIC_ERROR);
  }
  
  RET_CODE status;
  /* now read the basis */
  /* note that the central part of PSF is saved in the same fashion as PHOTO
     the same I/O tool would work fine
  */
  f->psf->basis = thFitsPhPsfBasisRead(psfits, &status);
  if (status != SH_SUCCESS) {
    thError("%s: problem reading the central pars of the psf from file (%s)", name, psfilename);
    phFitsBinTblClose(psfits);
    return(SH_GENERIC_ERROR);
  }
  phstatus = phFitsBinTblEnd(psfits);
  if (phstatus < 0) {
    thError("%s: problem closing the fist binary extension in file (%s)", 
		   name, psfilename);
    phFitsBinTblClose(psfits);
    return(SH_GENERIC_ERROR);
  }

  /* now read the wings */
  
  f->psf->wing = thFitsPsfWingRead(psfits, &status);
  if (status != SH_SUCCESS) {
    thError("%s: problem reading the wings from file (%s[2])",
		   name, psfilename);
    return(SH_GENERIC_ERROR);
  }

  /* close the file */
  phstatus = phFitsBinTblClose(psfits);
  if (phstatus < 0) {
    thError("%s: problem closing fits binary file (%s)", 
		   name, psfilename);
    return(SH_GENERIC_ERROR);
  }
  
  return(SH_SUCCESS);
  
}


RET_CODE thFrameLoadWeight(FRAME *f) {

  
  char *name = "thFrameLoadWeight";

  if (f == NULL) {
    thError("%s: ERROR - null frame passed", name);
    return(SH_GENERIC_ERROR);
  }
  
  if ((f->data) == NULL) {
    thError("%s: ERROR - no data loaded onto frame, amplifier data is needed",
		   name);
    return(SH_GENERIC_ERROR);
  }

  if ((f->data->ampl == NULL) || (shChainSize(f->data->ampl) == 0)) {
    thError("%s: ERROR - no amplifier info loaded", name);
    return(SH_GENERIC_ERROR);
  }

  REGION *image;
  image = f->data->image;
  if ((image == NULL) || 
      image->nrow == 0 || image->ncol == 0) {
    thError("%s: ERROR - no proper image loaded", name);
    return(SH_GENERIC_ERROR);
  }

  REGION *weight;
  weight = f->data->weight;
  if (weight == NULL || 
      weight->nrow != image->nrow || 
      weight->ncol != image->ncol) {
    shRegDel(weight);
    weight = shRegNew("poisson-weight", image->nrow, image->ncol, TYPE_THPIX);
    f->data->weight = weight;
  }

  #if (GAUSSIAN_NOISE && CONSTANT_GAUSSIAN_NOISE)
  THPIX gaussian_noise_dn = DEFAULT_GAUSSIAN_NOISE_DN;
  RET_CODE status;
  status = thFrameGetGaussianNoiseDN(f, &gaussian_noise_dn);
  if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (gaussian_noise_dn) from (frame)", name);
	return(status);
	}
  printf("%s: using constant gaussian noise weights (gaussian_noise_dn = %g) \n", name, (float) gaussian_noise_dn);	
  #endif

  AMPL *ampl;
  int ia;

  THPIX gain, var, readnoise, *wr, *ir;
  int row, row0, row1, col, col0, col1;

  for (ia = 0; ia < shChainSize(f->data->ampl); ia++) {
    ampl = (AMPL *) shChainElementGetByPos(f->data->ampl, ia);
    gain = ampl->gain;
    row0 = ampl->row0;
    row1 = ampl->row1;
    col0 = ampl->col0;
    col1 = ampl->col1;
    readnoise = ampl->readnoise;

    var = gain * pow(readnoise, 2);

    printf("%s: i_amplifier = %d, row0 = %d, row1 = %d, col0 = %d, col1 = %d \n", name, ia, row0, row1, col0, col1);
    #if (GAUSSIAN_NOISE && CONSTANT_GAUSSIAN_NOISE)
    printf("%s: calculating weight with constant gaussian noise = %g \n", name, (float) gaussian_noise_dn);
    #else
    	#if PHOTO_WEIGHT_DEFINITION
    	printf("%s: calculating weight based on PHOTO definition (amplifier = %d, gain = %g, readnoise = %g) \n", name, ia, (float) gain, (float) readnoise);
    	#else	 
   	 printf("%s: calculating weight without PHOTO definition (amplifier = %d, gain = %g) \n", name, ia, (float) gain);
    	#endif
    #endif

    for (row = row0; row <= row1; row++) {
      wr = weight->rows_thpix[row];
      ir = image->rows_thpix[row];
      for (col = col0; col <= col1; col++) {
	#if (GAUSSIAN_NOISE && CONSTANT_GAUSSIAN_NOISE)
		wr[col] = (THPIX) 1.0 / pow((THPIX) gaussian_noise_dn, 2.0); 
	#else
		#if PHOTO_WEIGHT_DEFINITION
		wr[col] = gain / (ir[col] > 0 ? 
			  (0.5 + sqrt(pow(ir[col] + var, 2) + 0.25)) : var) ;
		/* the following is a less accurate but faster estimate for the weight
		wr[col] = gain / (ir[col] > 0 ? ir[col] + var: var) ;
		*/
		#else
		wr[col] = (ir[col] > 0 ? gain / ir[col]: 0.0);
		#endif
	#endif
	
      }
    }

  }

#if DEBUG_PROC_PRINT_WEIGHT_STATS
  BASIC_STAT *stat = thBasicStatNew();
  int nweight = weight->nrow * weight->ncol;
  int nimage = image->nrow * image->ncol;
  float werrors[nweight];
  BASIC_STAT *wstat = stat;
  for (row = 0; row < nweight; row++) werrors[row] = 1.0E-10;

  printf("%s: creating image matrix statistics \n", name);
  fflush(stderr);
  fflush(stdout);
  status = thBasicStatFromArray(image->rows_thpix[0], werrors, stat, nimage);
  printf("%s: image statistics: mean = %g, median = %g, min = %g, max = %g \n", name, wstat->mean, wstat->median, wstat->min, wstat->max);
  printf("%s: image statistics: iqr = %g, std-dev = %g, q1 = %g, q3 = %g \n", name, wstat->iqr, wstat->s, wstat->q1, wstat->q3);
 
  printf("%s: creating weight matrix statistics \n", name);
  fflush(stderr);
  fflush(stdout);
  status = thBasicStatFromArray(weight->rows_thpix[0], werrors, wstat, nweight);  
  printf("%s: weight matrix statistics: mean = %g, median = %g, min = %g, max = %g \n", name, wstat->mean, wstat->median, wstat->min, wstat->max);
  printf("%s: weight matrix statistics: iqr = %g, std-dev = %g, q1 = %g, q3 = %g \n", name, wstat->iqr, wstat->s, wstat->q1, wstat->q3);
  thBasicStatDel(wstat);
#endif

  return(SH_SUCCESS);

}

RET_CODE thFrameLoadThMask(FRAME *f) {

#if THCLOCK
  clock_t clk1, clk2;
#endif

  /* timing results:
     
  0.85 CPUs
  subtracting bad mask from sky

  0.19 CPUs
  gerneating dervish mask from span mask
  
  0.06 CPUs
  subtracting allobjects from the rectangular mask
  
  0.05 CPUs
  subtracting badmask from mask allobjects mask

  0.01 CPUs
  canonizing badmask
  canonizing skymask

  */
  char *name = "thFrameLoadThMask";

  if (f == NULL) {
    thError("%s: ERROR - null frame passed", name);
    return(SH_GENERIC_ERROR);
  }
  
  if ((f->data) == NULL) {
    thError("%s: ERROR - no data loaded onto frame, amplifier data is needed",
		   name);
    return(SH_GENERIC_ERROR);
  }

  if (f->data->phsm == NULL) {
    thError("%s: ERROR - PHOTO spanmask should be loaded before loading thMasks", name);
    return(SH_GENERIC_ERROR);
  }

#if DEBUG_MEMORY
printf("%s: memory statistics before loading (thmask)", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#endif
    /* 
     we should be able to load mask selection rules, for now we will
     simply map the given PHOTO spanmask identically
  */

  thError("%s: WARNING - caution when working with mask selection files", name);

  SPANMASK *thsm = NULL;
  SPANMASK *phsm = NULL;
  THMASK *thmask = NULL;

  FRAMEDATA *data = f->data;
  phsm = data->phsm;
  int nrow, ncol;
  nrow = phsm->nrow;
  ncol = phsm->ncol;

CHAIN *phobjcs = data->phobjc;
CHAIN *phatlas = data->phatlas;

  RET_CODE status;
  /*
  typedef enum {
  S_MASK_INTERP = 0,           pixel's value has been interpolated 
  S_MASK_SATUR,                pixel is/was saturated  
  S_MASK_NOTCHECKED,           pixel was NOT examined for an object 
  S_MASK_OBJECT,               pixel is part of some object 
  S_MASK_BRIGHTOBJECT,         pixel is part of bright object 
  S_MASK_BINOBJECT,             pixel is part of binned object 
  S_MASK_CATOBJECT,            pixel is part of a catalogued object 
  S_MASK_SUBTRACTED,           model has been subtracted from pixel 
  S_MASK_GHOST,                pixel is part of a ghost 
  S_MASK_CR,                   pixel is part of a cosmic ray 
  S_NMASK_TYPES                number of types; MUST BE LAST 
  } S_MASKTYPE;
  */

  /*
    typedef enum {
            TH_MASK_BAD = 0,     pixel's value should not be used in
                                 fitting to any object or for sky 
            TH_MASK_SKY,                   pixel part of the sky 
            TH_MASK_FITOBJC,                to be used for current fits 
            TH_NMASK_TYPES               number of types, MUST BE LAST 
    } TH_MASKTYPE;
  */
  #if THMASK_NULL
 	thError("%s: WARNING - ineffective masking, passing null", name);
	/* 
	thmask = thMaskNew(name, nrow, ncol);
	MASKBIT maskbit = 1; 
	maskbit <<= ((int) TH_MASK_GOOD);
	status = thMaskSetPix(thmask, maskbit);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not set mask pixels to (%x)", name, (int) maskbit); 
		return(status);
	}	
	*/
  #else
  thsm = phSpanmaskNew(nrow, ncol);
  
  /* extremely bad pixel: the ones which should not be used
     for any sort of fit - object or sky 
  */


  int **iphm, ithm;
  int nmask_types = ((int) NMASK_TYPES) + 1;
  iphm = (int **) thCalloc(((int) TH_NMASK_TYPES), sizeof(int*));
  iphm[0] = thCalloc(((int) TH_NMASK_TYPES) * nmask_types, sizeof(int));
  for (ithm = 0; ithm < TH_NMASK_TYPES; ithm++) {
    iphm[ithm] = iphm[0] + ithm * nmask_types;
    iphm[ithm][0] = -1;
  }

  /*
  S_MASK_INTERP = 0,           pixels value has been interpolated 
  S_MASK_SATUR,                pixel is/was saturated  
    S_MASK_NOTCHECKED,           pixel was NOT examined for an object 
    S_MASK_OBJECT,               pixel is part of some object 
    S_MASK_BRIGHTOBJECT,         pixel is part of bright object 
   S_MASK_BINOBJECT,             pixel is part of binned object 
    S_MASK_CATOBJECT,            pixel is part of a catalogued object 
    S_MASK_SUBTRACTED,           model has been subtracted from pixel 
    S_MASK_GHOST,                pixel is part of a ghost 
    S_MASK_CR,                   pixel is part of a cosmic ray 
  */
  
  int i, *j;
  /* 
     bad pixels - never to be used in any fits 
     might need to include all extremely bright objects later
   */

  #if THMASK_BAD_PIXELS
  i = 0;
  int do_bad_mask_interp = THMASK_BAD_INTERP, do_bad_mask_satur = THMASK_BAD_SATUR, do_bad_mask_subtracted = THMASK_BAD_SUBTRACTED, do_bad_mask_ghost = THMASK_BAD_GHOST, do_bad_mask_cr = THMASK_BAD_CR;
  int do_bad_mask_brightobjc = THMASK_BAD_BRIGHTOBJC; 
  if (do_bad_mask_interp) {
	iphm[TH_MASK_BAD][i] = S_MASK_INTERP; i++;
	}
  if (do_bad_mask_satur) {
	iphm[TH_MASK_BAD][i] = S_MASK_SATUR; i++;
	}
  if (do_bad_mask_subtracted) {
	iphm[TH_MASK_BAD][i] = S_MASK_SUBTRACTED; i++;
	}
  if (do_bad_mask_ghost) {
	  iphm[TH_MASK_BAD][i] = S_MASK_GHOST; i++;
	}
  if (do_bad_mask_cr) {
	iphm[TH_MASK_BAD][i] = S_MASK_CR; i++;
	}
  if (do_bad_mask_brightobjc) {
	iphm[TH_MASK_BAD][i] = S_MASK_BRIGHTOBJECT; i++;
	}

  iphm[TH_MASK_BAD][i] = -1;
  #else
  thError("%s: WARNING - creating an empty bad pixel filter - can only be used during debug mode", name);
  i = 0;
  iphm[TH_MASK_BAD][i] = -1; i++;
  #endif

  i = 0;
  printf("%s: contents of TH_BAD_MASK: ", name);
  int print_next = (iphm[TH_MASK_BAD][i] != -1);
  while (print_next) {
	printf("'%s', ", shEnumNameGetFromValue("S_MASKTYPE", iphm[TH_MASK_BAD][i]));
	i++;
	print_next = (iphm[TH_MASK_BAD][i] != -1);
   }
   printf("\n");

  /* 
     all non-sky but good pixels 
     for now, all objects of any sort
   */
  i = 0;
  iphm[TH_MASK_ALLOBJC][i] = S_MASK_OBJECT; i++;
  /* 
  iphm[TH_MASK_ALLOBJC][i] = S_MASK_BRIGHTOBJECT; i++;
  */
  iphm[TH_MASK_ALLOBJC][i] = S_MASK_BINOBJECT; i++;
  iphm[TH_MASK_ALLOBJC][i] = S_MASK_CATOBJECT; i++;
  iphm[TH_MASK_ALLOBJC][i] = -1;
  
  /* all objects used for current fit */

  i = 0;
  iphm[TH_MASK_FITOBJC][i] = -1;

  /* all sky pixels, all pixels that are not in the non-sky
     but are not in BAD either
  */

  i = 0;
  iphm[TH_MASK_SKY][i] = -1;

  i = 0;
  iphm[TH_MASK_GOOD][i] = -1;

  /* will make this out of a dervish mask after generating the others */

      
  /* now add all the chains as prescribed by the above 
     mask mappings.

     Each bit plane is to be represented by one MASK and not by a chain of masks
     due to possiblity of multiple presence of a pixel in several object masks
     inside the same bit plane (overlapping objetcs or a CR trace being saturared too

  */
  
  /* 
     because bad pixel masks are used in defining other mask types
     we generate it before the other masks in a separate loop

     Jim Gunn says that we should also avoid the outer regions
     of PSF in very bright stars. I plan to input brightness limits
     for stars in selection files.

  */

  /* 
     take note of the canonization flags, if new pixels are added by hand
     further canonization of each objmask would be needed
  */

  OBJMASK *objmask = NULL;

  CHAIN *badmask = NULL, *mchain = NULL;
  CHAIN *skymask = NULL;
  badmask = shChainNew("OBJMASK");
  skymask = shChainNew("OBJMASK");

  ithm = TH_MASK_BAD;
  j = iphm[ithm];
  while ((*j) >= 0) {
    mchain = phsm->masks[*j];
    printf("%s: size of chain for '%s' = %d \n", name, shEnumNameGetFromValue("S_MASKTYPE", *j), shChainSize(mchain)); 
    if (mchain != NULL && shChainSize(mchain) != 0) {
      for (i = 0; i < shChainSize(mchain); i++) {
	objmask = shChainElementGetByPos(mchain, i);
	shChainElementAddByPos(badmask, objmask, "OBJMASK", TAIL, AFTER);
      }
    }
    j++;
  }


int npix_sky = 0;
if (phobjcs != NULL && phatlas != NULL) {
	int i_objc, n_objc, n_unsupported_objc_type = 0;
  	n_objc = shChainSize(phobjcs);
  	for (i_objc = 0; i_objc < n_objc; i_objc++) {
		WOBJC_IO *objc_i = shChainElementGetByPos(phobjcs, i_objc); 
		int nchild = objc_i->nchild;
		int id = objc_i->id;
		OBJ_TYPE type_i = objc_i->objc_type;
		if (nchild == 0 && type_i == OBJ_UNK) {
			int aii = find_id_in_phatlas(id, phatlas); 
			ATLAS_IMAGE *ai = shChainElementGetByPos(phatlas, aii);
			OBJMASK *objmask_i = ai->mask[R_BAND];
			if (objmask_i != NULL) {
				shChainElementAddByPos(badmask, objmask_i, "OBJMASK", TAIL, AFTER);
			}
		} else if (type_i != OBJ_SKY) {
			int aii = find_id_in_phatlas(id, phatlas); 
			ATLAS_IMAGE *ai = shChainElementGetByPos(phatlas, aii);
			OBJMASK *objmask_i = ai->mask[R_BAND];
			if (objmask_i != NULL) {
				shChainElementAddByPos(skymask, objmask_i, "OBJMASK", TAIL, AFTER);
				npix_sky += objmask_i->npix;
				#if MASK_OBJ_SKY_AS_BAD /* if this is set, OBJ_SKY pixels are not used for chi-squared fitting */	
				shChainElementAddByPos(badmask, objmask_i, "OBJMASK", TAIL, AFTER);
				#endif
			}
		} else if (type_i != OBJ_UNK && type_i != OBJ_STAR && 
			type_i != OBJ_GALAXY && type_i != OBJ_SKY) {
			n_unsupported_objc_type++;
		}
	}
	if (n_unsupported_objc_type != 0) {
		thError("%s: WARNING - found (%d) unsupported object types in object list", name, n_unsupported_objc_type);
	}
} else if (phobjcs == NULL) {
	thError("%s: WARNING - (photo) objects are not loaded, cannot mask (OBJ_UNK) pixels", name);
} else if (phatlas == NULL) {
	thError("%s: WARNING - (atlas images) are not loaded, cannot mask (OBJ_UNK) pixels", name);
}

if (npix_sky != 0) {
	#if MASK_OBJ_SKY_AS_BAD
	thError("%s: WARNING - (%d) pixels found in OBJ_SKY masked as bad pixels", name, npix_sky);
	#else
	thError("%s: WARNING - (%d) pixels found in OBJ_SKY not added to bad pixels", name, npix_sky);
	#endif
} else {
	thError("%s: WARNING - no pixels found in OBJ_SKY", name);
}

#if THMASKCORRUPTCOL > 0
  objmask = phObjmaskFromRect(0, 0, ncol - 1, THMASKCORRUPTCOL-1);
  shChainElementAddByPos(badmask, objmask, "OBJMASK", TAIL, AFTER);
  objmask = phObjmaskFromRect(0, nrow - THMASKCORRUPTCOL, 
			      ncol - 1, nrow-1);
  shChainElementAddByPos(badmask, objmask, "OBJMASK", TAIL, AFTER);
#endif

  if (badmask != NULL && shChainSize(badmask) != 0) {

    status = thCanonizeObjmaskChain(badmask, 0, 0); /* might need to change flags */
    if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not canonize (badmask)", name);
	return(status);
	}
    objmask = thObjmaskChainMerge(badmask, NULL, NULL); /* the result is canonized */
    if (objmask != NULL) {
	status = thCanonizeObjmask(objmask, 0);  
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not canonize (objmask)", name);
		return(status);
	}
    	printf("%s: mask type = '%s', npix = %d \n", name, shEnumNameGetFromValue("TH_MASKTYPE", ithm), objmask->npix);
    	if (thsm != NULL && ithm < (int) S_NMASK_TYPES) {
		phObjmaskAddToSpanmask(objmask, thsm, ithm);
	} else if (ithm >= (int) S_NMASK_TYPES) {
		thError("%s: ERROR - expected (ithm = %d) < (S_NMASK_TYPES = %d)", name, ithm, (int) S_NMASK_TYPES);
		return(SH_GENERIC_ERROR);
	} else {
		thError("%s: ERROR - found (thsm = %p)", name, thsm);
		return(SH_GENERIC_ERROR);
	}
	} else {
	thError("%s: WARNING - null result found from merging badmask chain", name);
	}
	
  }
  if (badmask != NULL) {
    shChainDel(badmask);
  }

  /* now set the other masks */
  CHAIN *mchaingood = NULL;

  for (ithm = 0; ithm < TH_NMASK_TYPES; ithm++) {

    if (ithm != TH_MASK_BAD) {
      
      j = iphm[ithm];
      mchain = shChainNew("OBJMASK");
      while ((*j) >= 0) {
	for (i = 0; i < shChainSize(phsm->masks[*j]); i++) {
	  objmask = (OBJMASK *) shChainElementGetByPos(phsm->masks[*j], i); 
	  shChainElementAddByPos(mchain, objmask, "OBJMASK", TAIL, AFTER);
	}
	/*
	phObjmaskChainMerge(mchain, phsm->masks[*j]);
	*/
	j++;
      }
      /* 
	 make sure that the bad pixels are not in this mask 
         note that the Intersection operation makes a copy of
	 the object masks too
      */
      mchaingood = phMaskNotIntersection(mchain, 
					 thsm->masks[TH_MASK_BAD]);
      shChainDel(mchain);

      if (mchaingood != NULL && shChainSize(mchaingood) != 0) {
	status = thCanonizeObjmaskChain(mchaingood, 0, 0); /* might need to change flags */
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not canonize (mchaingood)", name);
		return(status);
	}
	/* now merge all the object masks to form one big mask
	   this will eliminate the multiple occurence of one pixel in 
	   several obj masks on the same bit plane 
	*/ 
	objmask = thObjmaskChainMerge(mchaingood, NULL, NULL); /* the result is canonized */
	shChainDel(mchaingood);
	if (objmask != NULL && thsm != NULL && ithm < (int) S_NMASK_TYPES) {
		phObjmaskAddToSpanmask(objmask, thsm, ithm);
		printf("%s: mask type = '%s', npix = %d \n", name, shEnumNameGetFromValue("TH_MASKTYPE", ithm), objmask->npix);
	} else if (objmask == NULL) {
		thError("%s: WARNING - found (objmask = %p)", name, objmask);
	} else if (thsm == NULL) {
		thError("%s: ERROR - null (thsm) spanmask", name);
		return(SH_GENERIC_ERROR);
	} else {
		thError("%s: ERROR - expected (ithm = %d) < (S_NMASK_TYPES = %d)", name, ithm, (int) S_NMASK_TYPES);
		return(SH_GENERIC_ERROR);
	}
      }
      /* here the mask type "ithm" is generated */
    }
    
  }
  /* now constructing the sky mask */
  OBJMASK *flatmask, *goodmask, *skyobjcmask, *notobjcmask;
  flatmask = phObjmaskFromRect(0, 0, ncol-1, nrow-1);
  notobjcmask = phObjmaskNotIntersectionObjmaskChain(flatmask, thsm->masks[TH_MASK_ALLOBJC]);
  if (notobjcmask != NULL) {
	status = thCanonizeObjmask(notobjcmask, 0);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - coult not canonize (notobjcmask)", name);
		return(status);
	}
	} else {
	thError("%s: WARNING - found (notobjcmask = NULL)", name);	
	}
  goodmask = phObjmaskNotIntersectionObjmaskChain(flatmask, thsm->masks[TH_MASK_BAD]); 
  
  ithm = TH_MASK_SKY;
  if (skymask != NULL) {
	status = thCanonizeObjmaskChain(skymask, 0, 0); /* might need to change flags */
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not canonize (skymask)", name);
		return(status);
	}
  } else {
	thError("%s: WARNING - (skymask == NULL) found", name);
  }
  skyobjcmask = thObjmaskChainMerge(skymask, NULL, NULL); /* the result is canonized */
  if (skyobjcmask != NULL) {
	status = thCanonizeObjmask(skyobjcmask, 0);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not canonize (skyobjcmask)", name);
		return(status);
	}
	} else {
	thError("%s: WARNING - (skyobjcmask = NULL) was found", name);
	}
  if (skyobjcmask != NULL && thsm != NULL && (int) TH_MASK_SKY < (int) S_NMASK_TYPES) {
	phObjmaskAddToSpanmask(skyobjcmask, thsm, TH_MASK_SKY);
	printf("%s: mask type = '%s', npix = %d \n", name, shEnumNameGetFromValue("TH_MASKTYPE", ithm), skyobjcmask->npix);
  } else if (skyobjcmask == NULL) {
	thError("%s: WARNING - found (skyobjcmask = %p)", name, skyobjcmask);
  } else if ((int) TH_MASK_SKY >= (int) S_NMASK_TYPES) {
	thError("%s: ERROR - expected (TH_MASK_SKY = %d) < (S_NMASK_TYPES = %d)", name, (int) TH_MASK_SKY, (int) S_NMASK_TYPES);
	return(SH_GENERIC_ERROR);
  } else {
	thError("%s: ERROR - found null (thsm = %p)", name, thsm);
	return(SH_GENERIC_ERROR);
  }
  shChainDel(skymask);

  ithm = TH_MASK_GOOD;
  if (goodmask != NULL) {
	status = thCanonizeObjmask(goodmask, 0);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not canonize (goodmask)", name);
		return(status);
	}
	} else {
	thError("%s: WARNING - (goodmask = NULL) found", name);
	}
  if (goodmask != NULL && thsm != NULL && (int) TH_MASK_GOOD < (int) S_NMASK_TYPES) {
	phObjmaskAddToSpanmask(goodmask, thsm, TH_MASK_GOOD);
  	printf("%s: mask type = '%s', npix = %d \n", name, shEnumNameGetFromValue("TH_MASKTYPE", ithm), goodmask->npix);
  } else if (goodmask == NULL) {
	thError("%s: WARNING - found (goodmask = NULL)", name);
  } else if ((int) TH_MASK_GOOD >= (int) S_NMASK_TYPES) {
	thError("%s: ERROR - expected (TH_MASK_GOOD = %d) > (S_NMASK_TYPES = %d)", name, (int) TH_MASK_GOOD, (int) S_NMASK_TYPES);
	return(SH_GENERIC_ERROR);
  } else {
	thError("%s: ERROR - found null (thsm = %p) spanmask", name, thsm);
	return(SH_GENERIC_ERROR); 
  }
  phObjmaskDel(notobjcmask);
  phObjmaskDel(flatmask);
  /* now constructing the dervish type mask */

  thmask = thMaskGenFromSpanmask(thsm, TH_NMASK_TYPES, &status);
  if (f->data->thsm != NULL) {
    thError("%s: WARNING - thsm object already loaded - unloading", name);
    phSpanmaskDel(f->data->thsm);
  }
  if (f->data->thmask != NULL) {
    thError("%s: WARNING - thmask object already loaded - unloading", name);
    thMaskDel(f->data->thmask);
    f->data->thmask = NULL;
  }

  if (status != SH_SUCCESS) {
    thError("%s: unable to load dervish style mask; spanmask successfully loaded", name);
    return(status);
  }

thFree(iphm[0]);
thFree(iphm);

#endif

  f->data->thsm = thsm;
  f->data->thmask = thmask;

#if DEBUG_MEMORY
printf("%s: memory statistics after loading (thmask)", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#endif


  return(SH_SUCCESS);

}

RET_CODE thFrameUpdateThMask(FRAME *f, MAPMACHINE *map, MASK_UPDATE mupdate) {
char *name = "thFrameUpdateThMask";
if (f == NULL) {
	thError("%s: ERROR - null input (frame)", name);
	return(SH_GENERIC_ERROR);
} else if (f->data == NULL) {
	thError("%s: ERROR - null (framedata)", name);
	return(SH_GENERIC_ERROR);
}
if (map == NULL) {
	thError("%s: ERROR - null input (map)", name);
	return(SH_GENERIC_ERROR);
}
if (!(mupdate & MASK_UPDATE_ANY)) {
	thError("%s: WARNING - ineffective (mask_update) flag - mask will not be updated", name);
	return(SH_SUCCESS);
}

RET_CODE status;

LSTRUCT *lstruct = NULL;
status = thFrameGetLstruct(f, &lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lstruct) from (frame)", name);
	return(status);
}
LWORK *lwork = NULL;
status = thLstructGetLwork(lstruct, &lwork);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwork) from (lstruct)", name);
	return(status);
}
WPSF *wpsf = NULL;
status = thLworkGetWpsf(lwork, &wpsf);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (wpsf) from (lwork)", name);
	return(status);
}

SPANMASK *phsm = f->data->phsm;
SPANMASK *thsm = f->data->thsm;
THMASK *thmask = f->data->thmask;
if (thsm == NULL) {
	thError("%s: ERROR - null (thsm) found in (framedata)", name);
	return(SH_GENERIC_ERROR);
}
if (thmask == NULL) {
	thError("%s: ERROR - null (thmask) found in (framedata)", name);
	return(SH_GENERIC_ERROR);
}
if (phsm == NULL) {
	thError("%s: ERROR - null (phsm) found in (framedata)", name);
	return(SH_GENERIC_ERROR);
}

if (mupdate & (MASK_BAD_LINEAR_OBJC | MASK_BAD_NONLINEAR_OBJC)) {
	void **objcs = NULL;
	int nobjc = 0;	
	CHAIN *badmask_chain = thsm->masks[TH_MASK_BAD];
	CHAIN *goodmask_chain = thsm->masks[TH_MASK_GOOD];
	CHAIN *allobjcmask_chain = thsm->masks[TH_MASK_ALLOBJC];
	OBJMASK *badmask = NULL, *goodmask = NULL, *allobjcmask = NULL;
	if (badmask_chain != NULL && shChainSize(badmask_chain) == 1) {
		badmask = shChainElementGetByPos(badmask_chain, 0);
	} else {
		thError("%s: ERROR - null (badmask) found in (thsm)", name);
		return(SH_GENERIC_ERROR);
	}
	if (goodmask_chain != NULL && shChainSize(goodmask_chain) == 1) {
		goodmask = shChainElementGetByPos(goodmask_chain, 0);
	} else {
		thError("%s: ERROR - null (goodmask) found in (thsm)", name);
		return(SH_GENERIC_ERROR);
	}
	if (allobjcmask_chain != NULL && shChainSize(allobjcmask_chain) == 1) {
		allobjcmask = shChainElementGetByPos(allobjcmask_chain, 0);
	} else if (allobjcmask_chain == NULL) {
		thError("%s: ERROR - null (allobjcmask) found in (thsm)", name);
		return(SH_GENERIC_ERROR);
	} else {
		thError("%s: ERROR - (allobjcmask_chain) size in (thsm) expected to be 1 found (%d)", name, shChainSize(allobjcmask_chain));
		return(SH_GENERIC_ERROR);
	}
	if (badmask == NULL || goodmask == NULL || allobjcmask == NULL) {
		thError("%s: ERROR - null (badmask, goodmask, allobjcmask) found in (thsm)", name);
		return(SH_GENERIC_ERROR);
	}
	int type_count = 0;
	THPIX factor = 0.0;

	THOBJC **pobjcs = NULL;
	int npobjc = 0;
	status = thMapmachineGetUPObjcs(map, (void ***) &pobjcs, &npobjc);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get list of (UPObjcs)", name);
		return(status);
	}

	for (type_count = 0; type_count < 2; type_count++) {	

		int do_it = 0;
		if (type_count == 0 && (mupdate & MASK_BAD_LINEAR_OBJC)) {
			status = thMapmachineGetUAObjcs(map, (void ***) &objcs, &nobjc);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could no get list of (UAObjcs)", name);
				return(status);
			}
			factor = (THPIX) MASK_BAD_LINEAR_OBJC_FACTOR;
			do_it = 1;
		} else if (type_count == 1 && (mupdate & MASK_BAD_NONLINEAR_OBJC)) {
			status = thMapmachineGetUPObjcs(map, (void ***) &objcs, &nobjc);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get list of (UPObjcs)", name);
				return(status);
			}
			factor = (THPIX) MASK_BAD_NONLINEAR_OBJC_FACTOR; 
			do_it = 1;
		}
	if (do_it) {
	
		int i;
		for (i = 0; i < nobjc; i++) {
			THOBJC *objc = objcs[i];
			int aggressive = MASK_BAD_UNKNOWN_FLAG;
			status = thMDecideDeclaimObjc(objc, pobjcs, npobjc, &aggressive);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not decide if aggressive masking is needed", name);
				return(status);
			}
			OBJMASK *objmask = thObjcMakeObjmaskHighSN(objc, wpsf, factor, aggressive, &status);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get (objmask) for high (S/N) regions of (objc)", name);
				return(status);
			}
			if (objmask != NULL) {
				phObjmaskOrObjmask(badmask, objmask);
				phObjmaskAndNotObjmask(goodmask, objmask);
				phObjmaskAndNotObjmask(allobjcmask, objmask);
				phObjmaskDel(objmask);
			}
		
		}
		}
	}
	if (badmask != NULL) {
		status = thCanonizeObjmask(badmask, 0);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not canonize (badmask)", name);
			return(status);
		}
	}
	if (goodmask != NULL) {
		status = thCanonizeObjmask(goodmask, 0);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not canonize (goodmask)", name);
			return(status);
		}
	}
	if (allobjcmask != NULL) {
		status = thCanonizeObjmask(allobjcmask, 0);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not canonize (allobjcmask)", name);
			return(status);
		}
	}
	status = thMaskUpdateFromSpanmask(thmask, thsm, TH_NMASK_TYPES);		
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update (thmask) from (thsm)", name);
		return(status);
	}
} 

if (mupdate & MASK_BAD_WEIRD_OBJC) {
	printf("%s: flag (mask_update = 'MASK_BAD_WEIRD_OBJC') found \n", name);
	CHAIN *objcs = NULL;
	CHAIN *badmask_chain = thsm->masks[TH_MASK_BAD];
	CHAIN *goodmask_chain = thsm->masks[TH_MASK_GOOD];
	CHAIN *allobjcmask_chain = thsm->masks[TH_MASK_ALLOBJC];
	OBJMASK *badmask = NULL, *goodmask = NULL, *allobjcmask = NULL;
	if (badmask_chain != NULL && shChainSize(badmask_chain) == 1) {
		badmask = shChainElementGetByPos(badmask_chain, 0);
	} else {
		thError("%s: ERROR - null (badmask) found in (thsm)", name);
		return(SH_GENERIC_ERROR);
	}
	if (goodmask_chain != NULL && shChainSize(goodmask_chain) == 1) {
		goodmask = shChainElementGetByPos(goodmask_chain, 0);
	} else {
		thError("%s: ERROR - null (goodmask) found in (thsm)", name);
		return(SH_GENERIC_ERROR);
	}
	if (allobjcmask_chain != NULL && shChainSize(allobjcmask_chain) == 1) {
		allobjcmask = shChainElementGetByPos(allobjcmask_chain, 0);
	} else if (allobjcmask_chain == NULL) {
		thError("%s: ERROR - null (allobjcmask) found in (thsm)", name);
		return(SH_GENERIC_ERROR);
	} else {
		thError("%s: ERROR - (allobjcmask_chain) size in (thsm) expected to be 1 found (%d)", name, shChainSize(allobjcmask_chain));
		return(SH_GENERIC_ERROR);
	}
	status = thFrameGetPhObjcs(f, &objcs, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (phobjc) list from (frame)", name);
		return(SH_GENERIC_ERROR);
	}
	if (badmask == NULL || goodmask == NULL || allobjcmask == NULL) {
		thError("%s: ERROR - null (badmask, goodmask, allobjcmask) found in (thsm)", name);
		return(SH_GENERIC_ERROR);
	}
	CHAIN *brightmask_chain = phsm->masks[S_MASK_BRIGHTOBJECT];
	CHAIN *unknownmask_chain = shChainNew("OBJMASK");
	int imask, nmask = 0;
	if (brightmask_chain != NULL) nmask = shChainSize(brightmask_chain);
	for (imask = 0; imask < nmask; imask++) {
		OBJMASK *mask = shChainElementGetByPos(brightmask_chain, imask);
		int id = (int) (mask->sum);
		int iobjc = find_id_in_phobjcs(id, objcs);
		WOBJC_IO *objc = shChainElementGetByPos(objcs, iobjc); 
		if (objc == NULL) {
			thError("%s: ERROR - the bright (mask) does not match any object", name); 
			return(SH_GENERIC_ERROR);
		} else if (objc->objc_type != OBJ_STAR && objc->objc_type != OBJ_GALAXY) {
			printf("%s: object (id = %d, type = '%s', rowc = %g, colc = %g) matches mask (id = %d, npix = %d, nspan = %d) \n", name, objc->id, shEnumNameGetFromValue("OBJ_TYPE", objc->objc_type), (float) objc->objc_rowc, (float) objc->objc_colc, mask->id, mask->npix, mask->size);
			shChainElementAddByPos(unknownmask_chain, mask, "OBJMASK", TAIL, AFTER);
		}
	}
	OBJMASK *unknown_mask = thObjmaskChainMerge(unknownmask_chain, NULL, NULL);
	if (unknown_mask != NULL && unknown_mask->npix > 0) {
		phObjmaskOrObjmask(badmask, unknown_mask);
		phObjmaskAndNotObjmask(goodmask, unknown_mask);
		phObjmaskAndNotObjmask(allobjcmask, unknown_mask);
	}
	if (unknown_mask != NULL) phObjmaskDel(unknown_mask);
	if (badmask != NULL) {
		status = thCanonizeObjmask(badmask, 0);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not canonize (badmask)", name);
			return(status);
		}
	}
	if (goodmask != NULL) {
		status = thCanonizeObjmask(goodmask, 0);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not canonize (goodmask)", name);
			return(status);
		}
	}
	if (allobjcmask != NULL) {
		status = thCanonizeObjmask(allobjcmask, 0);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not canonize (allobjcmask)", name);
			return(status);
		}
	}
	status = thMaskUpdateFromSpanmask(thmask, thsm, TH_NMASK_TYPES);		
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update (thmask) from (thsm)", name);
		return(status);
	}


}


#if DEBUG_MEMORY
printf("%s: memory statistics before memory defragmentation", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#endif

int ret, free_to_os = FREE_TO_OS;
ret = shMemDefragment(free_to_os);
if (ret != 0) {
	thError("%s: ERROR - problem defragmenting memory", name);
	return(SH_GENERIC_ERROR);
}

#if DEBUG_MEMORY
printf("%s: memory statistics after memory defragmentation", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#endif


return(SH_SUCCESS);
}

RET_CODE thFrameLoadLstruct(FRAME *f, LMODEL *background, CALIBTYPE calibtype, LFITRECORDTYPE rtype) {
char *name = "thFrameLoadLstruct";
if (f == NULL) {
	thError("%s: ERROR - null input, cannot load (lstruct)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;

#if DEBUG_MEMORY
printf("%s: memory statistics before loading (lstruct)", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#endif

FRAMEWORK *work = NULL;
status = thFrameGetFramework(f, &work);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (framework) from (frame)", name);
	return(status);
}

if (work == NULL) {
	thError("%s: ERROR - null (framework), cannot load (lmachine)", name);
	return(SH_GENERIC_ERROR);
}
LMACHINE *lmachine = NULL;
status = thFrameworkGetMap(work, &lmachine);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmachine) from (lwork)", name);
	return(status);
}
if (lmachine == NULL || lmachine->cflag != COMPILED) {
	thError("%s: ERROR - (frame) doesn't have a compiled (map)", name);
	return(SH_GENERIC_ERROR);
}
int **invploc;
invploc = lmachine->invploc;

int nm = -1, np = -1, npp = -1, nc = -1, nacc = -1, nj = -1, nrow = -1, ncol = -1; /* structural parameters */

status = thFrameGetLpars(f, &nm, &np, &npp, &nc, &nacc, &nj, &nrow, &ncol);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get structural parameters for (lstruct) from (frame)", name);
	return(status);
}

LSTRUCT *lstruct = NULL;
status = thFrameworkGetLstruct(work, &lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lstruct) from (lwork)", name);
	return(status);
}
if (lstruct != NULL) {
	thError("%s: WARNING - a (lstruct) already loaded on (frame), deallocating", name);
	status = thLstructDealloc(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not deallocate (lstruct)", name);
		return(status);
	}
} else {
	lstruct = thLstructNew();
	status = thFrameworkPutLstruct(work, lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put (lstruct) in (lwork)", name);
		return(status);
	}
}

status = thLstructPutSource(lstruct, (void *) f);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not assign (frame) and (source) for (lstruct)", name);
	return(status);
}

LWORK *lwork = NULL;
LDATA *ldata = NULL;
LMODEL *lmodel = NULL;
LMODVAR *lcov = NULL;
LMETHOD *lmethod = NULL;
LWMASK *lwmask = NULL;
LTEST *ltest = NULL;
LCALIB *lcalib = NULL;

lwork = lstruct->lwork;
ldata = lstruct->ldata;
lmodel =lstruct->lmodel;
lcov =  lstruct->lcov;
lmethod = lstruct->lmethod;
status = thLstructGetLwmask(lstruct, &lwmask);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwmask) from (lstruct)", name);
	return(status);
}
ltest = lstruct->ltest;
lcalib = lstruct->lcalib;

status = thLmodelAlloc(lmodel, nm, np);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not allocate memory for (lmodel) in (lstruct)", name);
	return(status);
}
lmodel->modvar = lcov;
lmodel->lmachine = lmachine;

status = thLmodvarAlloc(lcov, nm, np);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not allocate memory for (lcov) in (lstruct)", name);
	return(status);
}

status =  thLworkAlloc(lwork, lmodel, lmachine, nm, np, npp, nc, nacc, nj, nrow, ncol);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not allocate memory for (lwork) in (lstruct)", name);
	return(status);
}

REGION *image = NULL;
REGION *weight = NULL;
THMASK *mask = NULL;
PSFMODEL *psf = NULL;
status = thFrameGetLdataPars(f, &image, &weight, &mask, &psf);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get image properties from (frame)", name);
	return(status);
}

status = thLdataPut(ldata, image, weight, background, mask, psf);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not assign value to (ldata) in (lstruct)", name);
	return(status);
}
status = thLwmaskPut(lwmask, ldata, lmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not assign value to (lwmask) in (lstruct)", name);
	return(status);
}
status = thLwmaskPrintInfo(lwmask, name);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not print (lwmask) info", name);
	return(status);
} 

/* 
status = thLwmaskSample(lwmask, lwmask_nactive);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not sample (lwmask)", name);
	return(status);
}
*/

status = thLworkPut(lwork, psf);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not assign value to psf-related variables in (lwork)", name);
	return(status);
}

status = thLmethodDefault(lmethod);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not assign default values to (lmethod) in (lstruct)", name);
	return(status);
}

status = thLCalibPutType(lcalib, calibtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not put (calib_type) in (lcalib)", name);
	return(status);
}
CRUDE_CALIB *cc = NULL;
status = thPsfModelGetCrudeCalib(psf, &cc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (crude_calib) from (phpsf)", name);
	return(status);
}
status = thLCalibPutCrudeCalib(lcalib, cc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not put (crude_calib) in (lcalib)", name);
	return(status);
}
/* doing the (lfit) */
LFIT *lfit = NULL;
status = thLstructGetLfit(lstruct, &lfit);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lfit) from (lstruct)", name);
	return(status);
}
status = thLfitAlloc(lfit, rtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not allocated (lfit) from (lstruct) while (rtype = %d)", 
	name, (int) rtype);
	return(status);
}

MEMDBLE memory = 0;
status = thLworkGetMemSize(lwork, &memory);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get memory size for (lwork)", name);
	return(status);
}
lwork->memory = memory;

#if DEBUG_MEMORY
printf("%s: memory statistics after loading (lstruct)", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#endif

return(SH_SUCCESS);
}

RET_CODE thFrameFitSky(FRAME *f) {

  char *name = "thFrameFitSky";

  if (f == NULL) {
    thError("%s: null frame passed", name);
    return(SH_GENERIC_ERROR);
  }
  
  if ((f->data) == NULL) {
    thError("%s: no data loaded onto frame, image and skybasis info needed",
		   name);
    return(SH_GENERIC_ERROR);
  }

  if ((f->data->image) == NULL) {
    thError("%s: no fpC data loaded onto frame",
		   name);
    return(SH_GENERIC_ERROR);
  }

  if ((f->data->weight) == NULL) {
    thError("%s: Warning - no poisson weight loaded onto frame, proceeding with wp == 1",
		   name);
  }

  if ((f->data->thmask) == NULL) {
    thError("%s: Warning - no mask loaded onto frame, using all pixels for fit",
		   name);
  }

  if (f->sky != NULL) {
    thError("%s: skymodel seems to be already loaded onto the frame - unload before calling this function", 
		   name);
  }
    
  if (f->data->sbs == NULL) {
    thError("%s: sky basis set (sbs) is null", name);
    return(SH_GENERIC_ERROR);
  }

  SKYMODEL *sm;
  REGION *wp, *image;
  MASK *mask;
  SKYBASISSET *sbs;
  
  image = f->data->image;
  wp = f->data->weight;
  mask = f->data->thmask;
 
  sbs = f->data->sbs;

  int nrow, ncol, bsize;
  nrow = image->nrow;
  ncol = image->ncol;
  bsize = sbs->bsize;

  sm = thSkymodelNew(bsize, nrow, ncol);
  sm->basis = sbs;

  RET_CODE status;
  status = thSkycoeffsFitSkybasisset(sm->sc, sm->basis, wp, image, mask);
  if (status != SH_SUCCESS) {
    thError("%s: problem fitting sky to the data", name);
    return(status);
  }
  
  status = thSkymodelReconstruct(sm,  sm->basis, sm->sc);
  if (status != SH_SUCCESS) {
    thError("%s: problem reconstructing (sky) based on (skymodel)",
		   name);
    return(SH_GENERIC_ERROR);
  }
  
  THPIX sse;
  status = thSse(sm->reg,
		 image, wp, mask,
		 THSKYBIT,
		 &sse);

  sm->loglikelihood = - (sse) / ((THPIX) 2.0);

  if (status != SH_SUCCESS) {
    thError("%s: problem calculating SSE", 
		   name);
    return(SH_GENERIC_ERROR);
  }
  
  /* counting the number of pixels in the sky mask */
  sm->npixel = thCountMaskPix(0, image->nrow, 
			      0, image->ncol, 
			      mask, THSKYBIT);
  
  f->sky = sm;
  return(SH_SUCCESS);

}

/* 
   the following are process level loading function 
*/

RET_CODE thProcLoadEcalib(PROCESS *proc, char *file) {
  
  char *name = "thProcLoadAmpl";
  /* this function loads the amplifier structure 
     into the proc tank
  */

  int pos;
  pos = thProcTankGetPosByFilename(proc->ecalibtank, file);

  RET_CODE status = SH_SUCCESS;

  if (pos < 0) {

    CHAIN *ecalibtank;
    if (proc->ecalibtank == NULL) {
      ecalibtank = shChainNew("PROCTANK");
    } else {
      ecalibtank = proc->ecalibtank;
    }

    CHAIN *ecalibchain;
    PROCTANK *tankelem;
    ecalibchain = thParReadEcalib(file, &status);
    
    if (status == SH_SUCCESS) {

      tankelem = thProctankNew(file);
      tankelem->chain = ecalibchain;
      
      shChainElementAddByPos(ecalibtank, tankelem, "PROCTANK", TAIL, AFTER);
      
      if (proc->ecalibtank == NULL) {
	proc->ecalibtank = ecalibtank;
      }

    } else {
      thError("%s: could not read the par file (%s) properly", name, file);
    }

  }
  
  return(status);

}

RET_CODE thProcLoadCcdconfig(PROCESS *proc, char *file) {
  
  char *name = "thProcLoadAmpl";
  /* this function loads the amplifier structure 
     into the proc tank
  */

  int pos;
  pos = thProcTankGetPosByFilename(proc->ccdconfigtank, file);

  RET_CODE status = SH_SUCCESS;

  if (pos < 0) {

    CHAIN *ccdconfigtank;
    if (proc->ccdconfigtank == NULL) {
      ccdconfigtank = shChainNew("PROCTANK");
    } else {
      ccdconfigtank = proc->ccdconfigtank;
    }

    CHAIN *ccdconfigchain;
    PROCTANK *tankelem;
    ccdconfigchain = thParReadCcdconfig(file, &status);
    
    if (status == SH_SUCCESS) {

      tankelem = thProctankNew(file);
      tankelem->chain = ccdconfigchain;
      
      shChainElementAddByPos(ccdconfigtank, tankelem, "PROCTANK", TAIL, AFTER);
      
      if (proc->ccdconfigtank == NULL) {
	proc->ccdconfigtank = ccdconfigtank;
      }

    } else {
      thError("%s: could not read the par file (%s) properly", name, file);
    }

  }
  
  return(status);

}


RET_CODE thProcLoadSbf(PROCESS *proc, char *file) {
  
  char *name = "thProcLoadSbf";
  /* this function loads Sky Basis Functions (not a SKYBASISSET)
     into the proc tank
  */
  
   int pos;
  pos = thProcTankGetPosByFilename(proc->sbftank, file);

  RET_CODE status = SH_SUCCESS;

  if (pos < 0) {

    CHAIN *sbftank;
    if (proc->sbftank == NULL) {
      sbftank = shChainNew("PROCTANK");
    } else {
      sbftank = proc->sbftank;
    }

    CHAIN *sbfchain;
    PROCTANK *tankelem;
    PHFITSFILE *bfile;

    bfile = phFitsBinTblOpen(file, 0, NULL);

    sbfchain = thFitsFuncRead(bfile, &status);
    
    phFitsBinTblClose(bfile);

    if (status == SH_SUCCESS) {

      tankelem = thProctankNew(file);
      tankelem->chain = sbfchain;
      
      shChainElementAddByPos(sbftank, tankelem, "PROCTANK", TAIL, AFTER);
      
      if (proc->sbftank == NULL) {
	proc->sbftank = sbftank;
      }

    } else {
      thError("%s: could not read the fits file (%s) properly", name, file);
    }

  }
  
  return(status);

}

RET_CODE thProcLoadObf(PROCESS *proc, char *file){

  char *name = "thProcLoadObf";
  /* this function loads Object Basis Functions
     into the proc tank
  */

  thError("%s: unsupported function", name);
  return(SH_GENERIC_ERROR);

}

int thProcTankGetPosByFilename(CHAIN *tchain, char *filename) {

/* 
  char *name = "thProcTankGetPosByFilename";
*/
  /* this function gives the position of the tank chain given a filename
     otherwise returns -1 
  */
  
  if (tchain == NULL) {
     /* this should not be an error message
	 thError("%s: ERROR - null chain passed", name);
	*/
    return(-1);
  }

  int i, ntank, pos = -1;
  ntank = shChainSize(tchain);

  for (i = 0; i < ntank; i++) {
    if (filenamematch(filename, 
		      ((PROCTANK *)shChainElementGetByPos(tchain, i))->filename)) {
      pos = i;
      break;
    }
  }
 
  return(pos);
  
}

CHAIN *thProcTankGetChainByFilename (CHAIN *tchain, char *filename) {
  
  char *name = "thProcTankGetChainByFilename";
  /* this function gives the chain of the tank chain given a filename
     otherwise returns NULL 
  */

  if (tchain == NULL) {
    thError("%s: null chain passed", name);
    return(NULL);
  }
  
  int pos;
  pos = thProcTankGetPosByFilename(tchain, filename);
  
  if (pos < 0) return (NULL);

  CHAIN *echain;
  echain = ((PROCTANK *) shChainElementGetByPos(tchain, pos))->chain;
  
  return(echain);
  
}

static int filenamematch(char *f1, char *f2) {

  /* 
     the following matches the name of the files
     contained in f1 and f2.
     it doesn't match the rest of the address. 
  */

  char *fi1, *fi2, *fil1, *fil2;
  fil1 = fi1 = f1;
  fil2 = fi2 = f2;

  while (fi1 != NULL) {
    
    fil1 =fi1;
    fi1 = strchr(fi1, '/');
    if (fi1 != NULL) fi1++;
  }

  while (fi2 != NULL) {
    fil2 = fi2;
    
    fi2 = (strchr(fi2, '/'));
    if (fi2 != NULL) fi2++;

  }

  int l1;
  l1 = strlen(fil1);

  if (l1 != strlen(fil2)) return(0);

  if (memcmp(fil1, fil2, l1) != 0) {
    return(0);
  } else {
    return(1);
  }

}


FRAME *thProcGetFrameByPos(PROCESS *proc, int pos) {

  char *name = "thProcGetFrameByPos";

  if (proc == NULL) {
    thError("%s: can't locate any frames in a null (proc)",
		   name);
    return(NULL);
  }

  if (proc->frames == NULL) {
    thError("%s: no (frames) allocated for (proc)",
		   name);
    return(NULL);
  }

  FRAME *f;
  f = shChainElementGetByPos(proc->frames, pos);

  return(f);

}

int thProcGetPosByFramefiles (PROCESS *proc, FRAMEFILES *ff) {

  char *name = "thProcGetFrameByFramefiles";

  if (proc == NULL) {
    thError("%s: can't locate any frames in a null (proc)",
		   name);
    return(-1);
  }
  
  if (proc->frames == NULL) {
    thError("%s: no (frames) allocated for (proc)",
		   name);
    return(-1);
  }
  
  int pos = -1, i, nf;
  FRAME *xf;

  nf = shChainSize(proc->frames);

  for (i = 0; i < nf; i++) {
    xf = (FRAME *) shChainElementGetByPos(proc->frames, i);
    if (thFramefilesIn(xf->files, ff)) {
      pos = i;
      break;
    }
  }
   
  return(pos);

}

FRAME *thProcGetFrameByFramefiles (PROCESS *proc, FRAMEFILES *ff) {

  int pos;
  pos = thProcGetPosByFramefiles(proc, ff);

  if (pos < 0) {
    return(NULL);
  }

  FRAME *f;
  f = shChainElementGetByPos(proc->frames, pos);
  
  return(f);

}

int thFramefilesIn(FRAMEFILES *trgff, FRAMEFILES *srcff) {

  int res = 1;

  if (res) {
    res = filenamematchin(trgff->phconfigfile, srcff->phconfigfile);
  }
   if (res) {
    res = filenamematchin(trgff->phecalibfile, srcff->phecalibfile);
  }
    if (res) {
    res = filenamematchin(trgff->phflatframefile, srcff->phflatframefile);
  }
 if (res) {
    res = filenamematchin(trgff->phmaskfile, srcff->phmaskfile);
  }
 if (res) {
    res = filenamematchin(trgff->phsmfile, srcff->phsmfile);
  }
 if (res) {
    res = filenamematchin(trgff->phpsfile, srcff->phpsfile);
  }
 if (res) {
    res = filenamematchin(trgff->phobjcfile, srcff->phobjcfile);
  }
 if (res) {
   res = filenamematchin(trgff->phcalibfile, srcff->phcalibfile);
 }
 if (res) {
    res = filenamematchin(trgff->thsbfile, srcff->thsbfile);
  }
 if (res) {
    res = filenamematchin(trgff->thobfile, srcff->thobfile);
  }
 if (res) {
    res = filenamematchin(trgff->thmaskselfile, srcff->thmaskselfile);
  }
 if (res) {
    res = filenamematchin(trgff->thobjcselfile, srcff->thobjcselfile);
  }
 if (res) {
    res = filenamematchin(trgff->thsmfile, srcff->thsmfile);
  }
 if (res) {
    res = filenamematchin(trgff->thpsfile, srcff->thpsfile);
  }
 if (res) {
    res = filenamematchin(trgff->thobjcfile, srcff-> thobjcfile);
  }
  return(res);

}

static int filenamematchin(char *trgf, char *srcf) {

  if (srcf == NULL || srcf[0] == '\0') {
    return(1);
  }

  return(filenamematch(trgf, srcf));
	
}

RET_CODE thProcLoadFramefiles(PROCESS *proc) {

  char  *name = "thProcLoadFramefiles";
  
  if (proc == NULL) {
    thError("%s: null (proc) passed - cannot load (proc)", 
		   name);
    return(SH_GENERIC_ERROR);
  }

  if (proc->thprocessfile == NULL || proc->thprocessfile[0] == '\0') {
    thError("%s: (thprocessfile) not set -- cannot load (proc)",
		   name);
    return(SH_GENERIC_ERROR);
  }

  if ((proc->frames != NULL) && (shChainSize(proc->frames) > 0)) {
    thError("%s: no (frames) should be loaded onto (proc) before calling", name);
    return(SH_GENERIC_ERROR);
  }

  char *fname;
  fname = proc->thprocessfile;

  FILE *fil;
  fil = fopen(fname, "r");
  if  (fil == NULL) {
    thError("%s: could not open file (%s)", 
		   name, fname);
    return(SH_GENERIC_ERROR);
  }
  
  CHAIN *fchain;
  fchain = thAsciiChainRead(fil);
  
  if (fchain == NULL) {
    thError("%s: could not read the ascii file (%s)", 
		   name, fname);
    return(SH_GENERIC_ERROR);
  }

  fclose(fil);

  int i, nff = 0, nchain;
  nchain = shChainSize(fchain);
  TYPE type, fftype;
  fftype = shTypeGetFromName("FRAME");
  FRAME *f;
  
  for (i = 0; i < nchain; i++) {
    type = shChainElementTypeGetByPos(fchain, i);
    if (type == fftype) {
      nff++;
      f = thFrameNew();
      thFramefilesCopy(f->files, 
		       (FRAMEFILES *) shChainElementGetByPos(fchain, i));
      if (proc->frames == NULL) {
	proc->frames = shChainNew("FRAME");
      }
      shChainElementAddByPos(proc->frames, f, "FRAME", TAIL, AFTER);
    }
  }
    
  if (nff == nchain) {
    shChainDestroy(fchain, &thFramefilesDel);
  } else {
    thError("%s: Warning - don't know how to destroy the loaded chain",
		   name);
  }

  if (nff == 0){
    thError("%s: no (framefiles) existed in the ascii file (%s)", 
		   name, fname);
    return(SH_GENERIC_ERROR);
  }

  return(SH_SUCCESS);

}

CHAIN *thDeffilesRead(char **files) {
  
  char *name = "thDeffileRead";

  if (files == NULL) {
    thError("%s: null input", name);
  }
  
  int i;
  for (i = 0; i < MX_STRING_ARR; i++) {
    if (files[i] == NULL || files[i][0] == '\0') {
      break;
    }
  }
  int ndef;
  if (i < MX_STRING_ARR) {
    ndef = i+1;
  } else {
    ndef = 0;
  }

  CHAIN *tempchain, *defchain;
  char *fname;
  FILE *fil;

  defchain = shChainNew("GENERIC");
  for (i = 0; i < ndef; i++) {
    fname = files[i];
    fil = fopen(fname, "r");
    if (fil == NULL) {
      thError("%s: could not open file (%s)",
		     name, fname);
    } else {

    tempchain = thAsciiChainRead(fil);
    fclose(fil);
    if (shChainSize(tempchain) == 0) {
      thError("%s: Warning - no definition related structures read from file (%s)", 
		     name, fname);
    }
    shChainJoin(defchain, tempchain);
    }
  }

  shChainDel(tempchain);
  return(defchain);
}

RET_CODE thProcAddObjcdef(PROCESS *proc, OBJCDEF *elem) {
  
  char *name = "thProcAddObjcdef";

  if (proc == NULL || elem == NULL) {
    thError("%s: null input", name);
    return(SH_GENERIC_ERROR);
  }
  
  if (proc->objcdefchain == NULL) {
    proc->objcdefchain = shChainNew("OBJCDEF");
  }

  if (shChainTypeGet(proc->objcdefchain) != shTypeGetFromName("OBJCDEF")) {
    thError("%s: improperly typed (objcdefchain)", name);
    return(SH_GENERIC_ERROR);
  }
  
  shChainElementAddByPos(proc->objcdefchain, elem, "OBJCDEF", TAIL, AFTER);
  
  return(SH_SUCCESS);
}
  
RET_CODE thProcAddMaskdef(PROCESS *proc, MASKDEF *elem) {
  
  char *name = "thProcAddMaskdef";

  if (proc == NULL || elem == NULL) {
    thError("%s: null input", name);
    return(SH_GENERIC_ERROR);
  }
  
  if (proc->maskdefchain == NULL) {
    proc->maskdefchain = shChainNew("MASKDEF");
  }

  if (shChainTypeGet(proc->maskdefchain) != shTypeGetFromName("MASKDEF")) {
    thError("%s: improperly typed (maskdefchain)", name);
    return(SH_GENERIC_ERROR);
  }


  shChainElementAddByPos(proc->maskdefchain, elem, "MASKDEF", TAIL, AFTER);
  
  return(SH_SUCCESS);
}

RET_CODE thProcAddSentence(PROCESS *proc, SENTENCE *elem) {
  
  char *name = "thProcAddSentence";

  if (proc == NULL || elem == NULL) {
    thError("%s: null input", name);
    return(SH_GENERIC_ERROR);
  }
  
  if (proc->sentencechain == NULL) {
    proc->sentencechain = shChainNew("SENTENCE");
  }

  if (shChainTypeGet(proc->sentencechain) != shTypeGetFromName("SENTENCE")) {
    thError("%s: improperly typed (sentencechain)", name);
    return(SH_GENERIC_ERROR);
  }

  shChainElementAddByPos(proc->sentencechain, elem, "SENTENCE", TAIL, AFTER);
  
  return(SH_SUCCESS);
}

RET_CODE thProcSeparateDef(PROCESS *proc) {

  char *name = "thProcSeparateDef";
  
  if (proc == NULL) {
    thError("%s: null input", name);
    return(SH_GENERIC_ERROR);
  }

  if (proc->deftank == NULL) {
    thError("%s: null (deftank)", name);
    return(SH_GENERIC_ERROR);
  }

  int nchain;
  nchain = shChainSize(proc->deftank);

  TYPE elemtype, objcdeftype, maskdeftype, sentencetype;
  objcdeftype = shTypeGetFromName("OBJCDEF");
  maskdeftype = shTypeGetFromName("MASKDEF");
  sentencetype = shTypeGetFromName("SENTENCE");

  void *elem;
  int error = 0;
  
  RET_CODE status;
  int i;
  for (i = 0; i < nchain; i++) {

    elemtype = shChainElementTypeGetByPos(proc->deftank, i);
    elem = shChainElementGetByPos(proc->deftank, i);
    if (elemtype == objcdeftype) {
      status = thProcAddObjcdef(proc, (OBJCDEF *) elem);
      if (status != SH_SUCCESS) {
	thError("%s: problem adding element (%d) to object definition chain", 
		       name, i);
	error++;
      }
    } else if (elemtype == maskdeftype) {
      status = thProcAddMaskdef(proc, (MASKDEF *) elem);
      if (status != SH_SUCCESS) {
	thError("%s: problem adding element (%d) to mask definition chain", 
		       name, i);
	error++;
      }
    } else if (elemtype == sentencetype) {
      status = thProcAddSentence(proc, (SENTENCE *) elem);
      if (status != SH_SUCCESS) {
	thError("%s: problem adding element (%d) to sentence definition chain", 
		       name, i);
	error++;
      }
    } else {
      thError("%s: unknown chain element (%d) type", name, i);
    }
    
  }
  
  if (error == 0) {
    return(SH_SUCCESS);
  } else {
    return(SH_GENERIC_ERROR);
  }

}

RET_CODE thProcCreateDefTree(PROCESS *proc) {

  char *name = "thProcCreateDefTree";
  RET_CODE status;

  if (proc == NULL) {
    thError("%s: null input", name);
    return(SH_GENERIC_ERROR);
  }

  if (proc->objcdefchain == NULL || shChainSize(proc->objcdefchain) == 0) {
    thError("%s: null or empty (objdefchain)", name);
    return(SH_GENERIC_ERROR);
  }

  if (proc->sentencechain == NULL) {
    proc->sentencechain = shChainNew("GENERIC");
  }

  if (proc->objcdeftree == NULL) {
    proc->objcdeftree = shChainNew("GENERIC");
  }

  if (shChainSize(proc->objcdeftree) != 0) {
    thError("%s: (objcdeftree) already generated, empty before calling the function", 
		   name);
    return(SH_GENERIC_ERROR);
  }
  
  status = thAddObjcDefTree(proc->objcdefchain, proc->sentencechain, proc->objcdeftree);
  if (status != SH_SUCCESS) {
    thError("%s: could not appropriate generate object definition tree", name);
    return(SH_GENERIC_ERROR);
  }

  status = thAddMaskDefTree(proc->maskdefchain, proc->sentencechain, proc->objcdeftree);
  if (status != SH_SUCCESS) {
    thError("%s: could not appropriate generate mask definition tree", name);
    return(SH_GENERIC_ERROR);
  }

  return(SH_SUCCESS);
}

RET_CODE thProcLoadDef(PROCESS *proc) {

  char *name = "thProcLoadDef";
  RET_CODE status;

  if (proc == NULL) {
    thError("%s: null input", name);
    return(SH_GENERIC_ERROR);
  }

  if (proc->files == NULL) {
    thError("%s: null (files) in (proc)", name);
    return(SH_GENERIC_ERROR);
  }

  if (proc->files->thdeffiles == NULL || proc->files->thdeffiles[0][0] == '\0') {
    thError("%s: empty definition file list", name);
    return(SH_GENERIC_ERROR);
  }

  proc->deftank = thDeffilesRead(proc->files->thdeffiles);

  status = thProcSeparateDef(proc);
  if (status != SH_SUCCESS) {
    thError("%s: could not separate contents of the definition files", 
		   name);
    return(status);
  }
  status = thProcCreateDefTree(proc);
  if (status != SH_SUCCESS) {
    thError("%s: could not generate the definition tree",
		   name);
    return(status);
  }

  return(SH_SUCCESS);

}

  /* FRAME innards handling */
RET_CODE thFramefilesCopy(FRAMEFILES *trg, FRAMEFILES *src) {

  char *name = "thFramefilesCopy";

  if (src == NULL) {
    thError("%s: source (framefiles) passed as null", 
		   name);
    return(SH_GENERIC_ERROR);
  }
 
  if (trg == NULL) {
    thError("%s: target (framefiles) passed as null", 
		   name);
    return(SH_GENERIC_ERROR);
  }

  strcpy(trg->phconfigfile, src->phconfigfile);
  strcpy(trg->phecalibfile, src->phecalibfile);
  strcpy(trg->phflatframefile, src->phflatframefile);
  strcpy(trg->phmaskfile, src->phmaskfile);
  strcpy(trg->phsmfile, src->phsmfile);
  strcpy(trg->phpsfile, src->phpsfile);
  strcpy(trg->phobjcfile, src->phobjcfile);
  strcpy(trg->phcalibfile, src->phcalibfile);
  strcpy(trg->thsbfile, src->thsbfile);
  strcpy(trg->thobfile, src->thobfile);
  strcpy(trg->thmaskselfile, src->thmaskselfile);
  strcpy(trg->thobjcselfile, src->thobjcselfile);
  strcpy(trg->thsmfile, src->thsmfile);
  strcpy(trg->thpsfile, src->thpsfile);
  strcpy(trg->thobjcfile, src->thobjcfile);

  return(SH_SUCCESS);

}

FRAMEID *thFrameidGenFromHdr(HDR *hdr) {

  if (hdr == NULL) return(NULL);

  FRAMEID *id;
  id = thFrameidNew();

  char *filter;
  filter = thCalloc(MX_STRING_LEN, sizeof(char));
  int i;
  RET_CODE status;
  
  status = shHdrGetInt(hdr, "RUN", &i);
  if (status == SH_SUCCESS) id->run = i;
  
  status = shHdrGetInt(hdr, "RERUN", &i);
  if (status == SH_SUCCESS) id->rerun = i;
  
  status = shHdrGetInt(hdr, "CAMROW", &i);
  if (status == SH_SUCCESS) id->camrow = i;
  
  status = shHdrGetInt(hdr, "CAMCOL", &i);
  if (status == SH_SUCCESS) id->camcol = i;
  
  status = shHdrGetInt(hdr, "FRAME", &i);
  if (status == SH_SUCCESS) id->field = i;
  
  status = shHdrGetInt(hdr, "STRIPE", &i);
  if (status == SH_SUCCESS) id->stripe = i;
  
  status = shHdrGetAscii(hdr, "FILTER", filter);
  if (status == SH_SUCCESS) id->filter = thFilterGetFromName(filter);
  
  thFree(filter);

  return(id);

}

RET_CODE thUpdateFrameidFromHdr(FRAMEID *id, HDR *hdr) {

  char *name = "thUpdateFrameidFromHdr";

  if (hdr == NULL) {
    thError("%s: null hdr passed", name);
    return(SH_GENERIC_ERROR);
  }

  if (id == NULL) {
    thError("%s: null id passed", name);
    return(SH_GENERIC_ERROR);
  }
  
  char *filter;
  filter = thCalloc(10, sizeof(char));

  int i;
  RET_CODE status;
  
  status = shHdrGetInt(hdr, "RUN", &i);
  if (status == SH_SUCCESS) id->run = i;
  
  status = shHdrGetInt(hdr, "RERUN", &i);
  if (status == SH_SUCCESS) id->rerun = i;
  
  status = shHdrGetInt(hdr, "CAMROW", &i);
  if (status == SH_SUCCESS) id->camrow = i;
  
  status = shHdrGetInt(hdr, "CAMCOL", &i);
  if (status == SH_SUCCESS) id->camcol = i;
  
  status = shHdrGetInt(hdr, "FRAME", &i);
  if (status == SH_SUCCESS) id->field = i;
  
  status = shHdrGetInt(hdr, "STRIPE", &i);
  if (status == SH_SUCCESS) id->stripe = i;
  
  status = shHdrGetAscii(hdr, "FILTER", filter);
  if (status == SH_SUCCESS) id->filter = thFilterGetFromName(filter);
  
  thFree(filter);

  return(SH_SUCCESS);

}

RET_CODE thUpdateHdrFromFrameid(HDR *hdr, FRAMEID *id) {
char *name = "thUpdateHdrFromFrameid";
if (hdr == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (id == NULL) {
	thError("%s: ERROR - null input (frameid)", name);
	return(SH_GENERIC_ERROR);
}
char *comment = "generated from FRAMEID by thUpdateHdrFromFrameid";
RET_CODE status;
char *keyword;

char mytime[MX_STRING_LEN]; mytime[0] = '\0';
time_t curtime;
struct tm *loctime;
/* Get the current time. */
curtime = time (NULL);
/* Convert it to local time representation. */
loctime = localtime (&curtime);
keyword = "DATE";
char *fmt = "%x";
strftime(mytime, MX_STRING_LEN, fmt, loctime);
shHdrDelByKeyword(hdr, keyword);
status = shHdrInsertAscii(hdr, keyword, mytime, comment);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not insert value for keyword '%s' into header", name);
	return(status);
}
keyword = "TIME";
fmt = "%X";
strftime(mytime, MX_STRING_LEN, fmt, loctime);
shHdrDelByKeyword(hdr, keyword);
status = shHdrInsertAscii(hdr, keyword, mytime, comment);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not insert value for keyword '%s' into header", name);
	return(status);
}

keyword = "ORIGIN";
char *origin = "unknown";
shHdrDelByKeyword(hdr, keyword);
status = shHdrInsertAscii(hdr, keyword, origin, comment);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not insert value for keyword '%s' into header", name);
	return(status);
}
keyword = "VERS";
char *version = "unknown";
shHdrDelByKeyword(hdr, keyword);
status = shHdrInsertAscii(hdr, keyword, version, comment);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not insert value for keyword '%s' into header", name);
	return(status);
}
keyword = "RUN";
comment = "Run number";
if (id->run == 0) {
	thError("%s: WARNING - (frameid) doesn't seem to include proper value of '%s'", name, keyword);
}
shHdrDelByKeyword(hdr, keyword);
status = shHdrInsertInt(hdr, keyword, id->run, comment);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not insert value for keyword '%s' from (frameid)", name, keyword);
	return(status);
}
keyword = "RERUN";
comment = "Rerun if needed";
if (id->rerun == 0) {
	thError("%s: WARNING - (frameid) doesn't seem to include proper value of '%s'", name, keyword);
}
shHdrDelByKeyword(hdr, keyword);
status = shHdrInsertInt(hdr, keyword, id->rerun, comment);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not insert value for keyword '%s' from (frameid)", name, keyword);
	return(status);
}
keyword = "FRAME";
comment = "Frame sequence number within the run";
if (id->field <= 0) {
	thError("%s: WARNING - (frameid) doesn't seem to include proper value of '%s'", name, keyword);
}
shHdrDelByKeyword(hdr, keyword);
status = shHdrInsertInt(hdr, keyword, id->field, comment);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not insert value for keyword '%s' from (frameid)", name, keyword);
	return(status);
}
keyword = "STRIPE";
comment="Stripe index number (23 <--> eta=0)";
if (id->stripe == 0) {
	thError("%s: WARNING - (frameid) doesn't seem to include proper value of '%s'", name, keyword);
}
shHdrDelByKeyword(hdr, keyword);
status = shHdrInsertInt(hdr, keyword, id->stripe, comment);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not insert value for keyword '%s' from (frameid)", name, keyword);
	return(status);
}
keyword = "CAMROW";
comment = "Row in the imaging camera";
if (id->camrow == 0) {
	thError("%s: WARNING - (frameid) doesn't seem to include proper value of '%s'", name, keyword);
}
shHdrDelByKeyword(hdr, keyword);
status = shHdrInsertInt(hdr, keyword, id->camrow, comment);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not insert value for keyword '%s' from (frameid)", name, keyword);
	return(status);
}
if (THIOPIXTYPE == TYPE_U16) {
	keyword = "SOFTBIAS";
	comment = "software ""bias"" added to all DN";
	shHdrDelByKeyword(hdr, keyword);
	status = shHdrInsertInt(hdr, keyword, SOFT_BIAS, comment);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not insert the value for keyword '%s' from (frameid)", name, keyword);
		return(status);
	}
}
keyword = "FILTER";
comment = "filter used";
if (id->filter == 0) {
	thError("%s: WARNING - (frameid) doesn't seem to include proper value of '%s'", name, keyword);
}
shHdrDelByKeyword(hdr, keyword);
status = shHdrInsertAscii(hdr, keyword, thNameGetFromFilter(id->filter), comment);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not insert value for keyword '%s' from (frameid)", name, keyword);
	return(status);
}
keyword = "CAMCOL";
comment = "column in the imaging camera";
if (id->camcol == 0) {
	thError("%s: WARNING - (frameid) doesn't seem to include proper value of '%s'", name, keyword);
}
shHdrDelByKeyword(hdr, keyword);
status = shHdrInsertInt(hdr, keyword, id->camcol, comment);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not insert value for keyword '%s' from (frameid)", name, keyword);
	return(status);
}

return(SH_SUCCESS);
}




int thFrameidMatchHdr(FRAMEID *id, HDR *hdr) {

  char *name = "thFrameidMatchHdr";

  if (hdr == NULL || id == NULL) {
    thError("%s: null inputs, cannot match", name);
    return(-1);
  }

  char *filter;
  filter = thCalloc(10, sizeof(char));

  int i, match = 1;
  RET_CODE status;
  
  status = shHdrGetInt(hdr, "RUN", &i);
  if (status == SH_SUCCESS && match) match = (i == id->run);
  
  status = shHdrGetInt(hdr, "RERUN", &i);
  if (status == SH_SUCCESS && match) match = (i == id->rerun);
  
  status = shHdrGetInt(hdr, "CAMROW", &i);
  if (status == SH_SUCCESS && match) match = (i == id->camrow);

  status = shHdrGetInt(hdr, "CAMCOL", &i);
  if (status == SH_SUCCESS && match) match = (i == id->camcol);
  
  status = shHdrGetInt(hdr, "FRAME", &i);
  if (status == SH_SUCCESS && match) match = (i == id->field);
  
  status = shHdrGetInt(hdr, "STRIPE", &i);
  if (status == SH_SUCCESS && match) match = (i == id->stripe);
  
  status = shHdrGetAscii(hdr, "FILTER", filter);
  if (status == SH_SUCCESS && match) match = (id->filter == thFilterGetFromName(filter));
  
  return(match);

}
  
int thFilterGetFromName(char *fname) {

  char fchar;
  sscanf(fname,"%c", &fchar);

  int fno;

  switch (fchar) {
  case 'u':
    fno = THFILTER_U;
    break;
  case 'g':
    fno = THFILTER_G;
    break;
  case 'r':
    fno = THFILTER_R;
    break;
  case 'i':
    fno = THFILTER_I;
    break;
  case 'z':
    fno = THFILTER_Z;
    break;
  default:
    fno = THFILTER_UNKNOWN;
    break;
  }

  return(fno);
}

char *thNameGetFromFilter(int filter) {
char *fname = NULL;
switch (filter) {
case THFILTER_U:
	fname = "u";
	break;
case THFILTER_G:
	fname = "g";
	break;
case THFILTER_R:
	fname = "r";
	break;
case THFILTER_I:
	fname = "i";
	break;
case THFILTER_Z:
	fname = "z";
	break;
default:
	break;
}
return(fname);
}

RET_CODE thFrameDumpThPsf (FRAME *f) {
  
  char *name = "thFrameDumpThPsf";
  
  if (f == NULL) {
    thError("%s: null input", name);
    return(SH_GENERIC_ERROR);
  }
  if (f->files == NULL) {
    thError("%s: null frame files", name);
    return(SH_GENERIC_ERROR);
  }
  if (f->files->thpsfile == NULL || f->files->thpsfile[0] == '\0') {
    thError("%s: empty thpsf filename", name);
    return(SH_GENERIC_ERROR);
  }

  PHFITSFILE *fd;
  fd = phFitsBinTblOpen(f->files->thpsfile, 1, f->files->hdr);
  if  (fd == NULL) {
    thError("%s: could not open file (%s) to write psf", 
		   name, f->files->thsmfile);
    return(SH_GENERIC_ERROR);
  }
  
  /* writing the inner portions of the PSF */
  if (phFitsBinTblHdrWrite(fd, "PSF_BASIS", NULL) < 0) {
    thError("%s: could not write the structure header to fits file", 
		   name);
    return(SH_GENERIC_ERROR);
  }
  
  if (phFitsBinTblRowWrite(fd, f->psf->basis) < 0) {
    thError("%s: could not write (psf) to fits file row",
		   name);
    return(SH_GENERIC_ERROR);
  }
  
  if (phFitsBinTblEnd(fd) < 0) {
    thError("%s: could not close the fits file", name);
    return(SH_GENERIC_ERROR);
  }

   /* writing the outer portions of the PSF */
  if (phFitsBinTblHdrWrite(fd, "PSFWING", NULL) < 0) {
    thError("%s: could not write the structure header to fits file", 
		   name);
    return(SH_GENERIC_ERROR);
  }
  
  if (phFitsBinTblRowWrite(fd, f->psf->wing) < 0) {
    thError("%s: could not write (psf) to fits file row",
		   name);
    return(SH_GENERIC_ERROR);
  }
  
  if (phFitsBinTblClose(fd) < 0) {
    thError("%s: could not close the fits file", name);
    return(SH_GENERIC_ERROR);
  }

  

  return(SH_SUCCESS);  
}

RET_CODE thFrameDumpSkymodel (FRAME *f) {

  char *name = "thFrameDumpSkymodel";

  PHFITSFILE *fd;

  fd = phFitsBinTblOpen(f->files->thsmfile, 1, f->files->hdr);

  if  (fd == NULL) {
    thError("%s: could not open file (%s) to write skymodel", 
		   name, f->files->thsmfile);
    return(SH_GENERIC_ERROR);
  }

  if (phFitsBinTblHdrWrite(fd, "SKYMODEL", NULL) < 0) {
    thError("%s: could not write the structure header to fits file", 
		   name);
    return(SH_GENERIC_ERROR);
  }

  if (phFitsBinTblRowWrite(fd, f->sky) < 0) {
    thError("%s: could not write (skymodel) to fits file row",
		   name);
    return(SH_GENERIC_ERROR);
  }
  
  if (phFitsBinTblClose(fd) < 0) {
    thError("%s: could not close the fits file", name);
    return(SH_GENERIC_ERROR);
  }

  return(SH_SUCCESS);

}

RET_CODE thProcDumpSkymodel (PROCESS *proc) {

  char *name = "thProcDumpSkymodel";

  if (proc == NULL) {
    thError("%s: null proc passed", name);
    return(SH_GENERIC_ERROR);
  }
  
  if ((proc->frames == NULL) || (shChainSize(proc->frames) == 0)) {
    thError("%s: no (frames) available in (proc)", name);
    return(SH_GENERIC_ERROR);
  }

  int i, nf;
  nf = shChainSize(proc->frames);
  
  FRAME *f;
  RET_CODE status;

  for (i = 0; i < nf; i++) {
    f = shChainElementGetByPos(proc->frames, i);
    if (thFrameDumpSkymodel (f) != SH_SUCCESS) {
      thError("%s: could not dump skymodel for frame #(%d) -- proceeding to the next frame", name, i);
      status = SH_GENERIC_ERROR;
    }
  }

  return(status);
}

  
RET_CODE thProcDumpSkymodelChain (PROCESS *proc) {

  /* this function dumps the chain of all sky models in a proc in 
     the file given
  */
  
  char *name = "thProcDumpSkymodelChain";

  PHFITSFILE *fd;

 
  if ((proc->frames == NULL) || (shChainSize(proc->frames) == 0)) {
    thError("%s: no (frames) available in (proc)", name);
    return(SH_GENERIC_ERROR);
  }

  CHAIN *smiochain;
  smiochain = thSkymodelIoPackProc(proc);

  if (smiochain == NULL) {
    thError("%s: could not generate a chain of (skymodelio)",
		   name);
    return(SH_GENERIC_ERROR);
  }
 
   fd = phFitsBinTblOpen(proc->files->thsmfile, 1, NULL);

  if  (fd == NULL) {
    thError("%s: could not open file (%s) to write skymodel", 
		   name, proc->files->thsmfile);
    return(SH_GENERIC_ERROR);
  }

  if (phFitsBinTblHdrWrite(fd, "SKYMODELIO", NULL) < 0) {
    thError("%s: could not write the structure header to fits file", 
		   name);
    return(SH_GENERIC_ERROR);
  }

  if (phFitsBinTblChainWrite(fd, smiochain) < 0) {
    thError("%s: could not properly dump chain of (skymodelio) in file",
		   name);
    shChainDestroy(smiochain, &thSkymodelioDel);
    phFitsBinTblClose(fd);
    return(SH_GENERIC_ERROR);
  }

  shChainDestroy(smiochain, &thSkymodelioDel);

  if (phFitsBinTblClose(fd) < 0) {
    thError("%s: could not close fits file",
		   name);
    return(SH_GENERIC_ERROR);
  }

  return(SH_SUCCESS);

}
  
RET_CODE thFrameUpdateHdr(FRAME *f) {
char *name = "thFrameUpdateHdr";
if (f == NULL) {
	thError("%s: ERROR - null input (frame)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
FRAMEID *id = NULL;
status = thFrameGetFrameid(f, &id);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (frameid) from (frame)", name);
	return(status);
}
FRAMEFILES *ff = NULL;
status = thFrameGetFramefiles(f, &ff);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (framefiles) from (frame)", name);
	return(status);
}
HDR *hdr = NULL;
status = thFramefilesGetHdr(ff, &hdr);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (hdr) from (framefiles)", name);
	return(status);
}
if (hdr == NULL) {
	thError("%s: WARNING - null (hdr) found in (framefiles) - creating and placing a new (hdr)", name);
	hdr = shHdrNew();
	status = thFramefilesPutHdr(ff, hdr);
  	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put newly generated (hdr) into (framefiles)", name);
		return(status);
	}
}
status = thUpdateHdrFromFrameid(hdr, id);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update (hdr) from (frameid)", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE create_bndex_from_hdrline(char *hdrline, int *bndex) {
char *name = "create_bndex_from_hdrline";
if (hdrline == NULL || bndex == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
int i;
for (i = 0; i < NCOLOR; i++) {
	bndex[i] = UNKNOWN_BNDEX;
}
int len = strlen(hdrline);
int index = 0;
for (i = 0; i < len; i++) {
	char c = hdrline[i];
	if (c != ' ') {
		int band = UNKNOWN_BAND;
		RET_CODE status = thBandGetFromName(c, &band);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get band number from name for (band = '%c'", name, c);
			return(SH_GENERIC_ERROR);
		}
    		bndex[band] = index;
		index++;	
	}
}
return(SH_SUCCESS);
}

RET_CODE choose_good_objects(CHAIN *objcs) {
/* written on july 20, 2014 */
char *name = "choose_good_objects";
if (objcs == NULL || shChainSize(objcs) == 0) {
	thError("%s: WARNING - null or empty object chain passed", name);
	return(SH_SUCCESS);
}
int i, n;
n = shChainSize(objcs);

char *order_field = "nchild";
RET_CODE status;
status = shChainSort(objcs, order_field, 1); 
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not sort object chain by record = %s", name, order_field);
	return(status);
}


i = 0;
WOBJC_IO *objc = shChainElementGetByPos(objcs, i);
while (i < n && objc->nchild == 0) {
	i++;
	objc = shChainElementGetByPos(objcs, i);
}
if (i == 0) {
	thError("%s: WARNING - all objects seems to be either BLENDED or BRIGHT", name);
}
int deep = 1;
if (i < n) {
	int j;
	for (j = i; j < n; j++) {
		objc = shChainElementRemByPos(objcs, TAIL);
		thWObjcIoDel(objc, deep);
	}
}

if (n <= shChainSize(objcs)) {
	thError("%s: WARNING - initial objc chain size (n = %d) while reduced objc chain size (= %d)", name, n, shChainSize(objcs));
}
order_field = "id";
status = shChainSort(objcs, order_field, 1); 
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not sort object chain by record = %s", name, order_field);
	return(status);
}
if (n <= shChainSize(objcs)) {
	thError("%s: WARNING - initial objc chain size (n = %d) while sorted and reduced chain size (= %d)", name, n, shChainSize(objcs));
}

return(SH_SUCCESS);
}

RET_CODE choose_good_calib_objects(CHAIN *objcs) {
/* written on july 25, 2016 */
char *name = "choose_good_calib_objects";
if (objcs == NULL || shChainSize(objcs) == 0) {
	thError("%s: WARNING - null or empty object chain passed", name);
	return(SH_SUCCESS);
}
int i, n;
n = shChainSize(objcs);

char *order_field = "nchild";
RET_CODE status;
status = shChainSort(objcs, order_field, 1); 
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not sort object chain by record = %s", name, order_field);
	return(status);
}
i = 0;
CALIB_PHOBJC_IO *objc = shChainElementGetByPos(objcs, i);
while (i < n && objc->nchild == 0) {
	i++;
	objc = shChainElementGetByPos(objcs, i);
}
if (i == 0) {
	thError("%s: WARNING - all objects seems to be either BLENDED or BRIGHT", name);
}
if (i < n) {
	int j;
	for (j = i; j < n; j++) {
		objc = shChainElementRemByPos(objcs, i);
		thCalibPhObjcIoDel(objc);
	}
}

order_field = "id";
status = shChainSort(objcs, order_field, 1); 
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not sort object chain by record = %s", name, order_field);
	return(status);
}

return(SH_SUCCESS);
}


		
RET_CODE thFrameDoCrudeCalib(FRAME *f) {
char *name = "thFrameDoCrudeCalib";
if (f == NULL) {
	thError("%s: ERROR - null (frame)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
LSTRUCT *l = NULL;
status = thFrameGetLstruct(f, &l);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lstruct) from (frame)", name);
	return(status);
}
CRUDE_CALIB *cc = NULL;
status = thFrameGetCrudeCalib(f, &cc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (crude_calib) from (frame)", name);
	return(status);
}
status = thLCrudeCalib(l, cc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calibrate (l)", name);
	return(status);
}
return(SH_SUCCESS);
}


RET_CODE thImageFakeHdr(HDR *hdr, int run, int field, char cband, int camcol, char *rerun) {
char *name = "thImageFakeHdr";
if (hdr == NULL) {
	thError("%s: ERROR - null input");
	return(SH_GENERIC_ERROR);
}
int camrow = -1, band = -1;
RET_CODE status;
status = thBandGetFromName(cband, &band);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get band number for (band = '%c'", name, cband);
	return(status);
}
status = thCamrowGetFromBand(band, &camrow);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (camrow) for (band = %d)", name, cband);
	return(status);
}

char filter[2]; filter[0] = cband;
int stripe = -1;
int softbias = SOFT_BIAS;
if (rerun == NULL) rerun = "FAKE";

shHdrDelByKeyword(hdr, "RUN");
shHdrDelByKeyword(hdr, "RERUN");
shHdrDelByKeyword(hdr, "FRAME");
shHdrDelByKeyword(hdr, "STRIPE");
shHdrDelByKeyword(hdr, "CAMROW");
shHdrDelByKeyword(hdr, "FILTER");
shHdrDelByKeyword(hdr, "CAMCOL");
shHdrDelByKeyword(hdr, "SOFTBIAS");

status = shHdrInsertInt(hdr, "RUN", run, "Run number");
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not insert keyword 'RUN' in the header", name);
	return(status);
}
status = shHdrInsertAscii(hdr, "RERUN", rerun, "Rerun if needed");
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not insert keyword 'RERUN' in the header", name);
	return(status);
}
status = shHdrInsertInt(hdr, "FRAME", field, "Frame sequence number within the run");
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not insert keyword 'FRAME' in the header", name);
	return(status);
}
status = shHdrInsertInt(hdr, "STRIPE", stripe, "Fake Stripe index number (23 <--> eta=0)");
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not insert keyword 'STRIPE' in the header", name);
	return(status);
}
status = shHdrInsertInt(hdr, "CAMROW", camrow, "Row in the imaging camera");
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not insert keyword 'CAMROW' in the header", name);
	return(status);
}
status = shHdrInsertAscii(hdr, "FILTER", filter, "filter used");
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not insert keyword 'FILTER' in the header", name);
	return(status);
}
status = shHdrInsertInt(hdr, "CAMCOL", camcol, "column in the imaging camera");
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not insert keyword 'CAMCOL' in the header", name);
	return(status);
}
status = shHdrInsertInt(hdr, "SOFTBIAS", softbias, "software bias added to all DN");
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not insert keyword 'SOFTBIAs' in the header", name);
	return(status);
}
return(SH_SUCCESS);
}
                                               
RET_CODE thCreateFakeImage(REGION *reg) {
char *name = "thCreateFrakeImage";
if (reg == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (reg->type != TYPE_THPIX) {
	thError("%s: ERROR - (reg) does not have acceptable type", name);
	return(SH_GENERIC_ERROR);
}
shRegReshape(reg, DEFAULT_NROW, DEFAULT_NCOL);
strcpy(reg->name, name);
shRegClear(reg);
shRegAddWithDbl((double) (2 * SOFT_BIAS), reg, reg);
return(SH_SUCCESS);
}



RET_CODE thFrameLoadPhSky(FRAME *f) {
  char *name = "thFrameLoadPhSky";
  /* this function can only be used during simulations */
  if (f == NULL) {
    thError("%s: ERROR - null frame passed", name);
    return(SH_GENERIC_ERROR);
  }
  
  if (f->files == NULL) {
    thError("%s: ERROR - null (files) in (frame) ", name);
    return(SH_GENERIC_ERROR);
  }

  FRAMEDATA *data;
  if ((data = f->data) == NULL) {
	thError("%s: ERROR - null (data) in (frame)", name);
	return(SH_GENERIC_ERROR);
	}

	char *fname = f->files->phsmfile; 
	   /* reading the main header */
	   RET_CODE status;
	  HDR *hdr;
		hdr = shHdrNew();
	    status = shHdrReadAsFits(hdr, fname, DEF_NONE, NULL, 0, 1);
	 	if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not read header from fpBIN file '%s'", name, fname);
			return(status);
		}
	int shift = 0, nrow = 0, ncol = 0, bin_row = 0, bin_col = 0;
	status = shHdrGetInt(hdr, "SHIFT", &shift);
	status = shHdrGetInt(hdr, "NROW", &nrow);
	status = shHdrGetInt(hdr, "NCOL", &ncol);
	status = shHdrGetInt(hdr, "BIN_ROW", &bin_row);
	status = shHdrGetInt(hdr, "BIN_COL", &bin_col);
	REGION *skyreg = shRegNew("skyreg", 0, 0, TYPE_FL32);
	status = shRegReadAsFits(skyreg, fname, 0, 1, DEF_NONE, NULL, 0, 0, 1);
	if (status != SH_SUCCESS) {
    		thError("%s: ERROR - problem reading the fpBIN file", name);
    		return(status);
  	}

	float median = (float) 0.0; 
	int n;
	n = skyreg->nrow * skyreg->ncol;
	 phFloatArrayStats(skyreg->rows_fl32[0], n, 0,	&median, NULL, NULL, NULL);
	shRegDel(skyreg); 
	median = median / (float) (1 << shift);

	if (data->sky == NULL) {
		data->sky = thSkyparsNew();
	} else {
		thSkyparsReset(data->sky);
	}
	SKYPARS *sky = data->sky;
	sky->I_s00 = (THPIX) median;
	sky->I_z00 = sky->I_s00;
  return(SH_SUCCESS); 
}

RET_CODE thFrameLoadCObjc(FRAME *f) {
char *name = "thFrameLoadCobjc";

shAssert(f != NULL);
FRAMEDATA *data = f->data;
shAssert(data != NULL);
PHPSFMODEL *psf = data->phpsf;
shAssert(psf != NULL);
CHAIN *ccs = psf->ccs;
CHAIN *phobjc = data->phobjc;
if (phobjc == NULL) {
	thError("%s: ERROR - photo object list not properly loaded onto the frame", name);
	return(SH_GENERIC_ERROR);
}
shAssert(data->cwphobjc == NULL);
CHAIN *cwobjc = shChainNew("CALIB_WOBJC_IO");
data->cwphobjc = cwobjc;
RET_CODE status;
status = thCrudeCalibWObjcIoChain(ccs, phobjc, cwobjc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not crude_calib chain of objects", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE thFrameLoadPhprops(FRAME *f) {
char *name = "thFrameLoadPhprops";
shAssert(f != NULL);
FRAMEDATA *data = f->data;
shAssert(data != NULL);
CHAIN *phobjcs = data->phobjc;
CHAIN *cwphobjcs = data->cwphobjc;
CHAIN *cphobjcs = data->cphobjc;
CHAIN *phatlas = data->phatlas;
SPANMASK *phsm = data->phsm;
int good_phobjc = 0, good_cwphobjc = 0, good_cphobjc = 0, good_ai = 0;
if (phobjcs == NULL || shChainSize(phobjcs) == 0) {
	thError("%s: WARNING - empty uncaliberated object chain (phobjc) found in (data)", name);
	} else {
	good_phobjc = 1;
	}
if (cwphobjcs == NULL || shChainSize(cwphobjcs) == 0) {
	thError("%s: WARNING - empty calibrated working object chain (cwphobjc) found in data", name);
	} else {
	good_cwphobjc = 1;
	}
if (cphobjcs == NULL || shChainSize(cphobjcs) == 0) {
	thError("%s: WARNING - empty calibrated sweep object chain (cphobjc) found in data", name);
	} else {
	good_cphobjc = 1;
	}
if (phatlas == NULL || shChainSize(phatlas) == 0) {
	thError("%s: WARNING - empty (atlas image) chain found in data", name);
	} else {
	good_ai = 1;
	}
CHAIN *phprops = data->phprop;
if (phprops == NULL) {
	phprops = shChainNew("PHPROP");
	data->phprop = phprops;
}
int good_any = good_phobjc + good_cwphobjc + good_cphobjc;
if (good_any == 0) {
	thError("%s: WARNING - empty chain on all three types of object information", name);
	return(SH_SUCCESS);
} else if (good_any == 1) {
	thError("%s: WARNING - only one type of object information available in data", name);
	}

RET_CODE status;
if (phsm != NULL) {
	status =  match_bright_masks_to_objclist(phsm->masks[S_MASK_BRIGHTOBJECT], phobjcs);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not match bright object (masks) to (phobjc) list", name);
		return(status);
	}
} else {
	thError("%s: WARNING - (phsm) found to be null in (framedata) - unable to match (bright object masks)", name);
}

PHCALIB *calibData = data->phcalib;
int *bndex = data->bndex; 
shAssert(shChainSize(phprops) == 0);
if (good_phobjc == 1) {
	int nobjc = shChainSize(phobjcs);
	WOBJC_IO *objc = NULL;
	int i, id;
	int ti = 0, tcwi = 0, tci = 0, taii = 0; 
	for (i = 0; i < nobjc; i++) {
		objc = shChainElementGetByPos(phobjcs, i);
		id = objc->id;
		int cwi = find_id_in_cwphobjcs(id, cwphobjcs);
		int ci = find_id_in_cphobjcs(id, cphobjcs);
		int aii = find_id_in_phatlas(id, phatlas);
		CALIB_WOBJC_IO *cwphobjc = NULL;
		CALIB_PHOBJC_IO *cphobjc = NULL;
		ATLAS_IMAGE *ai = NULL;
		if (cwi < 0) {
			cwphobjc = NULL;
			tcwi++;
		} else {
			cwphobjc = shChainElementGetByPos(cwphobjcs, cwi);
		}
		if (ci < 0) {
			cphobjc = NULL;
			tci++;
		} else {
			cphobjc = shChainElementGetByPos(cphobjcs, ci);
		}
		OBJMASK *objmask = NULL;
		if (aii < 0) {
			thError("%s: WARNING - could not find an (atlas image) for objc (id = %d)", name, id);
			ai = NULL;
			objmask = NULL;
			taii++;
		} else {
			ai = shChainElementGetByPos(phatlas, aii);
			objmask = ai->mask[R_BAND];
			if (objmask == NULL || objmask->npix <= 0) {
				objmask = ai->master_mask;
			}
		}
		OBJMASK *brightmask = find_closest_bright_mask(f, objc);
		if (objmask == NULL) {
			objmask = brightmask;
		} else if (brightmask != NULL) {
			phObjmaskOrObjmask(objmask, brightmask);
			if (objmask != NULL) {
				status = thCanonizeObjmask(objmask, 0);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not canonize (objmask) at (i = %d, nobjc = %d)", name, i, nobjc);
					return(status);
				}
			}
		}
		if (ci < 0 || cwi < 0) ti++;
		
		PHPROPS *phprop = thPhpropsNew();
		RET_CODE status = thPhpropsPut(phprop, objc, bndex, cwphobjc, cphobjc, calibData, objmask);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not successfully put all object data inside (phprops) for (iobjc = %d)", name, i);
			return(status);
		}
		status = thLRGSetPhprops(phprop);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not set 'flagLRG' for (phprops) in (iobjc = %d)", name, i);
			return(status);
		}
		shChainElementAddByPos(phprops, phprop, "PHPROPS", TAIL, AFTER);
	}
	if (ti > 0) {
		thError("%s: WARNING - total of (ti = %d) objects could not be asked but were added", name, ti);
	}
	if (taii > 0) {
		thError("%s: WARNING - total of (taii = %d) objects did not have a matching atlas image", name, taii);
	}
	return(SH_SUCCESS);	
	} else if (good_cwphobjc == 1) {
	int nobjc = shChainSize(cwphobjcs);
	CALIB_WOBJC_IO *cwphobjc = NULL;
	int cwi, id;
	int ti = 0, tcwi = 0, tci = 0, taii = 0;
	for (cwi = 0; cwi < nobjc; cwi++) {
		cwphobjc = shChainElementGetByPos(cwphobjcs, cwi);
		id = cwphobjc->id;
		int i = find_id_in_phobjcs(id, phobjcs);
		int ci = find_id_in_cphobjcs(id, cphobjcs);
		int aii = find_id_in_phatlas(id, phatlas);
		WOBJC_IO *phobjc = NULL;
		CALIB_PHOBJC_IO *cphobjc = NULL;
		ATLAS_IMAGE *ai = NULL;
		if (i < 0) {
			phobjc = NULL;
			ti++;
		} else {
			phobjc = shChainElementGetByPos(phobjcs, i);
		}
		if (ci < 0) {
			cphobjc = NULL;
			tci++;
		} else {
			cphobjc = shChainElementGetByPos(cphobjcs, ci);
		}
 		OBJMASK *objmask = NULL;
		if (aii < 0) {
			thError("%s: WARNING - could not find an (atlas image) for objc (id = %d)", name, id);
			ai = NULL;
			objmask = NULL;
			taii++;
		} else {
			ai = shChainElementGetByPos(phatlas, aii);
			objmask = ai->mask[R_BAND];
			if (objmask == NULL || objmask->npix <= 0) {
				objmask = ai->master_mask;
			}
		}
		OBJMASK *brightmask = find_closest_bright_mask(f, phobjc);
		if (objmask == NULL) {
			objmask = brightmask;
		} else if (brightmask != NULL) {
			phObjmaskOrObjmask(objmask, brightmask);
			if (objmask != NULL) {
				status = thCanonizeObjmask(objmask, 0);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not canonize (objmask) at  (cwi = %d, nobjc = %d)", name, cwi, nobjc);
					return(status);
				}
			}
		} 

		if (i < 0 || ci < 0) tcwi++;
		PHPROPS *phprop = thPhpropsNew();
		RET_CODE status = thPhpropsPut(phprop, phobjc, bndex, cwphobjc, cphobjc, calibData, objmask);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not successfully put all object data inside (phprops) for (cwiobjc = %d)", name, i);
			return(status);
		}
		shChainElementAddByPos(phprops, phprop, "PHPROPS", TAIL, AFTER);
	}
	if (tcwi > 0) {
		thError("%s: WARNING - total of (tcwi = %d) objects could not be asked but were added", name, tcwi);
	}
	if (taii > 0) {
		thError("%s: WARNING - total of (taii = %d) objects did not have a matching atlas image", name, taii);
	}
	return(SH_SUCCESS);	
	} else if (good_cphobjc == 1) {
	int nobjc = shChainSize(cphobjcs);
	CALIB_PHOBJC_IO *cphobjc = NULL;
	int ci, id;
	int ti = 0, tcwi = 0, tci = 0, taii = 0;
	for (ci = 0; ci < nobjc; ci++) {
		cphobjc = shChainElementGetByPos(cphobjcs, ci);
		id = cphobjc->id;
		int i = find_id_in_phobjcs(id, phobjcs);
		int cwi = find_id_in_cwphobjcs(id, cwphobjcs);
		int aii = find_id_in_phatlas(id, phatlas);
		WOBJC_IO *phobjc = NULL;
		CALIB_WOBJC_IO *cwphobjc = NULL;
		ATLAS_IMAGE *ai = NULL;
		if (i < 0) {
			phobjc = NULL;
			ti++;
		} else {
			phobjc = shChainElementGetByPos(phobjcs, i);
		}
		if (cwi < 0) {
			cwphobjc = NULL;
			tcwi++;
		} else {
			cwphobjc = shChainElementGetByPos(cwphobjcs, cwi);
		}
 		OBJMASK *objmask = NULL;
		if (aii < 0) {
			thError("%s: WARNING - could not find an (atlas image) for objc (id = %d)", name, id);
			ai = NULL;
			objmask = NULL;
			taii++;
		} else {
			ai = shChainElementGetByPos(phatlas, aii);
			objmask = ai->mask[R_BAND];
			if (objmask == NULL || objmask->npix <= 0) {
				objmask = ai->master_mask;
			}
		}
		OBJMASK *brightmask = find_closest_bright_mask(f, phobjc);
		if (objmask == NULL) {
			objmask = brightmask;
		} else if (brightmask != NULL) {
			phObjmaskOrObjmask(objmask, brightmask);
			if (objmask != NULL) {
				status = thCanonizeObjmask(objmask, 0);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not canonize (objmask) at (ci = %d, nobjc = %d)", name, ci, nobjc);
					return(status);
				}
			}
		}
		 
		if (i < 0 || cwi < 0) tci++;
		PHPROPS *phprop = thPhpropsNew();
		RET_CODE status = thPhpropsPut(phprop, phobjc, bndex, cwphobjc, cphobjc, calibData, objmask);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not successfully put all object data inside (phprops) for (cwiobjc = %d)", name, i);
			return(status);
		}
		shChainElementAddByPos(phprops, phprop, "PHPROPS", TAIL, AFTER);
	}
	if (tci > 0) {
		thError("%s: WARNING - total of (tci = %d) objects could not be asked but were added", name, tci);
	}
	if (taii > 0) {
		thError("%s: WARNING - total of (taii = %d) objects did not have a matching atlas image", name, taii);
	}

	return(SH_SUCCESS);	
} 
	
thError("%s: ERROR - binary problem", name);
return(SH_GENERIC_ERROR);
}

int find_id_in_phobjcs(int id, CHAIN *phobjcs) {
if (phobjcs == NULL || shChainSize(phobjcs) == 0) return(-1);
int i, n = shChainSize(phobjcs);
int this_id, res = -1;
for (i = 0; i < n; i++) {
	WOBJC_IO *objc = shChainElementGetByPos(phobjcs, i);
	this_id = (int) objc->id;
	if (id == this_id) {
		res = i;
		break;
	}
}
return(res);
}

int find_id_in_cphobjcs(int id, CHAIN *cphobjcs) {
if (cphobjcs == NULL || shChainSize(cphobjcs) == 0) return(-1);
int i, n = shChainSize(cphobjcs);
int this_id, res = -1;
for (i = 0; i < n; i++) {
	CALIB_PHOBJC_IO *cphobjc = shChainElementGetByPos(cphobjcs, i);
	this_id = (int) cphobjc->id;
	if (id == this_id) {
		res = i;
		break;
	}
}
return(res);
}

int find_id_in_cwphobjcs(int id, CHAIN *cwphobjcs) {
if (cwphobjcs == NULL || shChainSize(cwphobjcs) == 0) return(-1);
int i, n = shChainSize(cwphobjcs);
int this_id, res = -1;
for (i = 0; i < n; i++) {
	CALIB_WOBJC_IO *cwphobjc = shChainElementGetByPos(cwphobjcs, i);
	this_id = (int) cwphobjc->id;
	if (id == this_id) {
		res = i;
		break;
	}
}
return(res);
}

int find_id_in_phatlas(int id, CHAIN *phatlas) {
if (phatlas == NULL || shChainSize(phatlas) == 0) return(-1);
int i, n = shChainSize(phatlas);
int res = -1;
for (i = 0; i < n; i++) {
	int this_id = -1;
	ATLAS_IMAGE *ai = shChainElementGetByPos(phatlas, i);
	if (ai != NULL) this_id = (int) ai->id;
	if (id == this_id) {
		res = i;
		break;
	}
}
return(res);
}	


RET_CODE thFrameLoadGalacticCalib(FRAME *f, GC_SOURCE source) {
char *name = "thFrameLoadGalacticCalib";
shAssert(f != NULL);
shAssert(f->data != NULL);
FRAMEDATA *data = f->data;
if (data->gc != NULL && shChainSize(data->gc) != 0) {
	thError("%s: ERROR - galactic_calib chain in frmae already contains information", name);
	return(SH_GENERIC_ERROR);
}
if (data->gc != NULL) shChainDel(data->gc);
data->gc = NULL; 
	
if (source == GC_FILE) {
	FRAMEFILES *ff = f->files;
	if (ff == NULL) {
		thError("%s: ERROR - (framefiles) not loaded in frame", name);
		return(SH_GENERIC_ERROR);
	}
	char *gcfile = ff->gcfile;
	if (gcfile == NULL || strlen(gcfile) == 0) {
		thError("%s: ERROR - no galactic calib filename loaded", name);
		return(SH_GENERIC_ERROR);
	}
  	PHFITSFILE *fits = phFitsBinTblOpen(gcfile, 0, NULL);
	if (fits == NULL) {
		thError("%s: ERROR - could not load file '%s'", name, gcfile);
		return(SH_GENERIC_ERROR);
	}
	int nfitsrow = 0, quiet = 0;
	int phstatus = phFitsBinTblHdrRead(fits, "GALACTIC_CALIB", NULL, &nfitsrow, quiet);
  	if (nfitsrow > 0 && phstatus >= 0) {
	CHAIN *gc_chain = shChainNew("GALACTIC_CALIB");
	int i;
    	for (i = 0; i < nfitsrow; i++){
     	 	GALACTIC_CALIB *gc = thGalacticCalibNew();
      		phstatus = phFitsBinTblRowRead(fits, gc);
   		if (phstatus < 0) {
			thError("%s: ERROR - problem reading fits file at row %d", name, i);
		shChainDestroy(gc_chain, &thGalacticCalibDel);
		return(SH_GENERIC_ERROR);
      		} else {
			shChainElementAddByPos(gc_chain, gc, "GALACTIC_CALIB", TAIL, AFTER);
 		}
	}
	data->gc = gc_chain;	
	return(SH_SUCCESS);
	} else {
		thError("%s: ERROR - empty extension or problem reading the header in file '%s'", name, gcfile);
		return(SH_GENERIC_ERROR);
	}

} else if (source == SWEEP_FILE || source == SWEEP_FILE_AND_WRITE) {

	CHAIN *cphobjcs = data->cphobjc;
	if (cphobjcs == NULL || shChainSize(cphobjcs) == 0) {
		thError("%s: ERROR - calibrated objects not loaded from sweep files", name);
		return(SH_GENERIC_ERROR);
	}
	data->gc = thGalacticCalibMakeFromObjcs(cphobjcs, GC_POLY_DEG);
	if (source == SWEEP_FILE_AND_WRITE) {
		RET_CODE status = thFrameWriteGalacticCalib(f);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not write the derived 'galactic calib' structure", name);
			return(status);
		}
	}
	return(SH_SUCCESS);

} 

thError("%s: ERROR - unsupported source '%s'", name, shEnumNameGetFromValue("GC_SOURCE", source));
return(SH_GENERIC_ERROR);
}


RET_CODE thFrameWriteGalacticCalib(FRAME *f) {
char *name = "thFrameWriteGalacticCalib";
shAssert(f != NULL);
FRAMEDATA *data = f->data;
FRAMEFILES *ff = f->files;
shAssert(data != NULL);
shAssert(ff != NULL);
char *gcfile = ff->gcfile;
CHAIN *gc = data->gc;
if (gcfile == NULL || strlen(gcfile) == 0) {
	thError("%s: ERROR - empty or null file name for 'gcfile'", name);
	return(SH_GENERIC_ERROR);
}
if (gc == NULL || shChainSize(gc) == 0) {
	thError("%s: ERROR - empty or null 'galactic calib' found in frame", name); 
	return(SH_GENERIC_ERROR);
}
HDR *hdr = NULL;
RET_CODE status = thFrameGetHdr3(f, &hdr);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get 'hdr3' from 'frame'", name);
	return(status);
	}

PHFITSFILE *fd;
fd = phFitsBinTblOpen(gcfile, 1, hdr);
if  (fd == NULL) {
    thError("%s: could not open file (%s) to write 'galactic calib'", name, gcfile);
    return(SH_GENERIC_ERROR);
  }

char *type = "GALACTIC_CALIB";
if (phFitsBinTblHdrWrite(fd, type, NULL) < 0) {
    thError("%s: could not write '%s' header to fits file '%s'", name, type, gcfile);
    return(SH_GENERIC_ERROR);
}

if (phFitsBinTblChainWrite(fd, gc) < 0) {
    thError("%s: could not write 'galactic calib' chain to fits file", name);
    return(SH_GENERIC_ERROR);
  }
  
if (phFitsBinTblClose(fd) < 0) {
    thError("%s: could not close the fits file", name);
    return(SH_GENERIC_ERROR);
  }

  return(SH_SUCCESS);

}


RET_CODE thFrameLoadData(FRAME *frame, GC_SOURCE gcsource) {
char *name = "thFrameLoadData";
if (frame == NULL) {
	thError("%s: ERROR - null frame", name);
	return(SH_GENERIC_ERROR);
}

#if DEBUG_MEMORY
printf("%s: memory statistics before loading (data) for (frame)", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#endif


int err_count = 0;
RET_CODE status;
#if DEBUG_PROC_LOAD_DATA
printf("%s: loading amplifier information\n", name);
#endif
status = thFrameLoadAmpl(frame); 
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not read amplifier information", name);
	err_count++;
}


/* no need to read the mask since we are gonna work with a fake sky only frame 
*/

#if DEBUG_PROC_LOAD_DATA
printf("%s: loading PHOTO masks\n", name);
#endif
status = thFrameLoadPhMask(frame);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not load (photo masks)", name);
	err_count++;
}


#if DEBUG_PROC_LOAD_DATA
printf("%s: loading PHOTO atlas images\n", name);
#endif
status = thFrameLoadPhAtlas(frame);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not load (photo atlas images)", name);
	err_count++;
}

#if DEBUG_PROC_LOAD_DATA
printf("%s: loading PHOTO sky\n", name);
#endif

status = thFrameLoadPhSky(frame);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not load PHOTO sky estimate", name);
	err_count++;
}
#if DEBUG_PROC_LOAD_DATA
printf("%s: loading PHOTO psf \n", name);
#endif
status = thFrameLoadPhPsf(frame);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not load PHOTO PSF", name);
	err_count++;
}
#if DEBUG_PROC_LOAD_DATA
printf("%s: reshaping into SOHO psf \n", name);
#endif
status = thFrameLoadThPsf(frame);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not reshape PSF", name);
	err_count++;
}
#if IO_LOAD_PHCALIB
	#if DEBUG_PROC_LOAD_DATA
	printf("%s: loading PHCALIB \n", name);
	#endif
	status = thFrameLoadCalib(frame); 
	if (status != SH_SUCCESS) {
		thError("%s: WARNING - could not load the calib file", name);
		return(status); 
		}
#endif
/* testing with SDSS objects */
#if DEBUG_PROC_LOAD_DATA
printf("%s: loading OBJC's \n", name);
printf("%s: reading fpObjc file \n", name);
#endif
status = thFrameLoadPhObjc(frame);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not load fpObjc file", name);
	err_count++;
	
}
#if DEBUG_PROC_LOAD_DATA
printf("%s: caliberating objects for atmospheric extinction \n", name);
#endif
status = thFrameLoadCObjc(frame);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not load (caliberated objects)", name);
	err_count++;
}
#if DEBUG_PROC_LOAD_DATA
printf("%s: reading thmask file \n", name);
#endif
status = thFrameLoadThMask(frame);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get thmasks for objects", name);
	}

if (gcsource == SWEEP_FILE || gcsource == SWEEP_FILE_AND_WRITE) {

	#if DEBUG_PROC_LOAD_DATA
	printf("%s: reading photoObj file \n", name);
	#endif
	status = thFrameLoadPhCalibObjc(frame, gcsource);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not load photoObj file", name);
		err_count++;
	}
	#if DEBUG_PROC_LOAD_DATA
	printf("%s: loading galactic extinction data from photoObj data \n", name);
	#endif
	status = thFrameLoadGalacticCalib(frame, gcsource);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not load 'galactic calib' for frame", name);
		err_count++;
	}

} else if (gcsource == GC_FILE) {

	#if DEBUG_PROC_LOAD_DATA
	printf("%s: loading galactic extinction data from galactic calib file \n", name);
	#endif
	status = thFrameLoadGalacticCalib(frame, gcsource);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not load 'galactic calib' for frame", name);
		err_count++;
	}
	#if DEBUG_PROC_LOAD_DATA
	printf("%s: creating photoObj equivalent data \n", name);
	#endif
	status = thFrameLoadPhCalibObjc(frame, gcsource);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not load photoObj data", name);
		err_count++;
	}

} else {
	thError("%s: ERROR - unsupported (gcsource = '%s')", name, shEnumNameGetFromValue("GCSOURCE", gcsource));
	return(SH_GENERIC_ERROR);
}

#if DEBUG_PROC_LOAD_DATA
printf("%s: loading (phprops) \n", name);
#endif
status = thFrameLoadPhprops(frame);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - couldn ot load (phprops) for frame", name);
	err_count++;
}

#if DEBUG_MEMORY
printf("%s: memory statistics after loading (data) for (frame)", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#endif

#if DEBUG_MEMORY
printf("%s: memory statistics before memory defragmentation", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#endif

int ret, free_to_os = FREE_TO_OS;
ret = shMemDefragment(free_to_os);
if (ret != 0) {
	thError("%s: ERROR - problem defragmenting memory", name);
	return(SH_GENERIC_ERROR);
}

#if DEBUG_MEMORY
printf("%s: memory statistics after memory defragmentation", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#endif

if (err_count > 0) {
	thError("%s: confronted (%d) I/O errors while attempting to load data", name, err_count);
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE thFramefilesPutBand(FRAMEFILES *ff, char band, RUN_MODE mode) {
char *name = "thFramefilesPutBand";
shAssert(ff != NULL);

RET_CODE status;
char bname;
status = thNameGetFromBand(band, &bname);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could find the band name for band = %d", name, band);
	return(status);
}

/* the following records were added to allow multi-object multi-band simulations - Jan 26, 2016*/
sprintf(ff->outphflatframefile, ff->outphflatframefileformat, bname); 
sprintf(ff->inphsmfile, ff->inphsmfileformat, bname);
sprintf(ff->outphsmfile, ff->outphsmfileformat, bname); 
sprintf(ff->outthsmfile, ff->outthsmfileformat, bname);
sprintf(ff->outthobjcfile, ff->outthobjcfileformat, bname);
sprintf(ff->outthflatframefile, ff->outthflatframefileformat, bname);

sprintf(ff->inphflatframefile, ff->inphflatframefileformat, bname); 
sprintf(ff->inthflatframefile, ff->inthflatframefileformat, bname);

sprintf(ff->phmaskfile, ff->inphmaskfileformat, bname);

if (mode == SIM_MODE) {
	strcpy(ff->phflatframefile, ff->outphflatframefile);
	strcpy(ff->thflatframefile, ff->outthflatframefile);
	strcpy(ff->phsmfile, ff->inphsmfile);
	strcpy(ff->thsmfile, ff->outthsmfile);
	/* the following are added so that simulation chisq and light profile are output if desired */
	sprintf(ff->thlmfile, ff->outlMfileformat, bname);
	sprintf(ff->thlpfile, ff->outlPfileformat, bname);
	sprintf(ff->thlrfile, ff->outlRfileformat, bname);
	sprintf(ff->thlzfile, ff->outlZfileformat, bname);
	sprintf(ff->thlwfile, ff->outlWfileformat, bname);


} else if (mode == MLE_MODE || mode == MLE_MODE_PLUS || mode == MLE_ON_DEMAND_MODE) {
	strcpy(ff->phflatframefile, ff->inphflatframefile);
	strcpy(ff->thflatframefile, ff->inthflatframefile);
	strcpy(ff->phsmfile, ff->inphsmfile);
	strcpy(ff->thsmfile, ff->outthsmfile);
	/* now inserting band into some more formats */
	sprintf(ff->thlmfile, ff->outlMfileformat, bname);
	sprintf(ff->thlpfile, ff->outlPfileformat, bname);
	sprintf(ff->thlrfile, ff->outlRfileformat, bname);
	sprintf(ff->thlzfile, ff->outlZfileformat, bname);
	sprintf(ff->thlwfile, ff->outlWfileformat, bname);
	sprintf(ff->thpsfreportfile, ff->outpsfreportfileformat, bname);
	sprintf(ff->thpsfstatfile, ff->outpsfstatsfileformat, bname);
} else {
	thError("%s: WARNING - some filenames were not transferred (run-mode = '%s')", name, shEnumNameGetFromValue("RUN_MODE", mode));
} 

sprintf(ff->thobjcfile, ff->outthobjcfileformat, bname);

return(SH_SUCCESS);
}

RET_CODE uncalibrate_frame_to_fpC_image(REGION *image, REGION *calibration, REGION *sky_image) {
char *name = "uncalibrate_frame_to_fpC_image";
if (image == NULL || calibration == NULL || sky_image == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (image->type != TYPE_THPIX || calibration->type != TYPE_THPIX || sky_image->type != TYPE_THPIX) {
	thError("%s: ERROR - unsupported pixel type in input", name);
	return(SH_GENERIC_ERROR);
}

if (image->nrow == 0 || calibration->nrow == 0 || sky_image->nrow == 0 || image->ncol == 0 || calibration->ncol == 0 || sky_image->ncol == 0) {
	thError("%s: ERROR - (nrow = 0) or (ncol = 0) detected in inputs", name);
	return(SH_GENERIC_ERROR);
}

if (image->nrow < calibration->nrow || image->nrow < sky_image->nrow) {
	thError("%s: ERROR - (calibration) and (sky) should both be larger than (image)", name);
	return(SH_GENERIC_ERROR);
}
if (image->ncol < sky_image->ncol) {
	thError("%s: ERROR - (sky) should be larger than (image)", name);
	return(SH_GENERIC_ERROR);
}
if (calibration->ncol != 1) {
	thError("%s: ERROR - expected (ncol = 1) in calibration, found (%d)", name, calibration->ncol);
	return(SH_GENERIC_ERROR);
}
int i, j;
int nrow, ncol;
nrow = image->nrow;
ncol = image->ncol;

for (i  = 0; i < nrow; i++) {
	THPIX *image_rows_i = image->rows_thpix[i];
	THPIX calib = calibration->rows_thpix[i][0];
	THPIX *sky_rows_i = sky_image->rows_thpix[i];
	for (j = 0; j < ncol; j++) {
		image_rows_i[j] = image_rows_i[j] / calib + sky_rows_i[j];
	}
}

return(SH_SUCCESS);
}

RET_CODE thFrameTweakFitWobjcPar(FRAME *f, int fitflag) {
char *name = "thFrameTweakFitWobjcPar";
if (f == NULL) {
	thError("%s: ERROR - null frame", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
FRAMEDATA *fd = NULL;
printf("%s: getting frame data \n", name);
fflush(stdout);
fflush(stderr);
status = thFrameGetFramedata(f, &fd);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (framedata) from (frame)", name);
	return(status);
}
printf("%s: recopying objc list from frame data \n", name);
fflush(stdout);
fflush(stderr);
status = thFramedataRecopyObjcList(fd);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not copy (phobjc) list from source in (framedata)", name);
	return(status);
}

LSTRUCT *l = NULL;
printf("%s: getting lstruct \n", name);
fflush(stdout);
fflush(stderr);
status = thFrameGetLstruct(f, &l);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lstruct) from (frame)", name);
	return(status);
}

MAPMACHINE *map = NULL;
printf("%s: getting map \n", name);
fflush(stderr);
fflush(stdout);
status = thLstructGetLmachine(l, &map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (map) from (lstruct)", name);
	return(status);
}
THOBJC **upobjcs;
int npobjcs = -1;
printf("%s: getting UPobjc list \n", name);
fflush(stderr);
fflush(stdout);
status = thMapmachineGetUPObjcs(map, (void ***) &upobjcs, &npobjcs);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (UPobjc) list from (map)", name);
	return(status);
}
if ((upobjcs == NULL && npobjcs != 0) || (npobjcs < 0)) {
	thError("%s: ERROR - found (npobjcs = %d) while (UPobjc = %p) in (map)", name, npobjcs, (void **) upobjcs);
	return(SH_GENERIC_ERROR);
}
if (npobjcs == 0) {
	if (upobjcs != NULL && upobjcs[0] != NULL) {
		thError("%s: ERROR - found (npobjcs = %d) while (UPobjc[0] = %p)", name, npobjcs, (void *) upobjcs[0]);
		return(SH_GENERIC_ERROR);
	}
	thError("%s: WARNING - found no (UPobjcs) in (map)", name);
	return(SH_SUCCESS);
}

printf("%s: tweaking objects [iobjc = ]: ", name);
fflush(stderr);
fflush(stdout);
int i;
for (i = 0; i < npobjcs; i++) {
	printf("%d, ", i);
	fflush(stderr);
	fflush(stdout);
	THOBJC *objc = upobjcs[i];
	status = thObjcTweakWobjc(objc, fitflag);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not tweak (objc = %p, iobjc = %d)", name, (void *) objc, i);
		return(status);
	}
}
printf("\n");
fflush(stdout);
fflush(stderr);
return(SH_SUCCESS);
}

RET_CODE thFrameSubtractSky(FRAME *f) {
char *name = "thFrameSubtractSky";
if (f == NULL) {
	thError("%s: ERROR - null (frame)", name);
	return(SH_GENERIC_ERROR);
} else if (f->data == NULL) {
	thError("%s: ERROR - null (framedata) found in (frame)", name);
	return(SH_GENERIC_ERROR);
}
REGION *image = f->data->image;
if (image == NULL) {	
	thError("%s: ERROR - found null (image) in (framedata)", name);
	return(SH_GENERIC_ERROR);
}
HDR *hdr = f->files->hdr;
if (hdr == NULL) {
	thError("%s: ERROR - null header found in (framefiles)", name);
	return(SH_GENERIC_ERROR);
}

double sky_level = 0.0;
if (shHdrGetDbl(hdr, "SKYFPCC", &sky_level) == SH_SUCCESS) {
	printf("%s: fpCC sky level found (sky_level = %g) \n", name, (float) sky_level);
	shRegAddWithDbl ((double) -sky_level, image, image);
} else {
	thError("%s: ERROR - could not find (fpCC) sky level from (hdr)", name);
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

static int output_atlas_info(CHAIN *atlas) {
char *name = "output_atlas_info";
if (atlas == NULL) {
	thError("%s: WARNING - null (atlas chain)", name);
	return(0);
}
if (shChainSize(atlas) == 0) {
	thError("%s: WARNING - empty (atlas chain)", name);
	return(0);
}
int n, i;
n = shChainSize(atlas);
int *npix, *id;
npix = shCalloc(n, sizeof(int));
id = shCalloc(n, sizeof(int));
int n_good_ai = 0, n_null_objmask = 0, n_bad_objmask = 0, n_null_ai = 0;
int npix_min = 0, npix_max = 0;

for (i = 0; i < n; i++) {
	ATLAS_IMAGE *ai = shChainElementGetByPos(atlas, i);
	int npix_i = 0, nspan_i = 0;
	if (ai == NULL) {
		#if DEEP_DEBUG_PROC_ATLAS_LOAD
		thError("%s: WARNING - null (atlas image) at location [%d] of (atlas chain)", name, i);
		#endif
		npix_i = -2;
		nspan_i = -2;
		n_null_ai++;
		id[i] = -3;
	} else {
		id[i] = ai->id;
		OBJMASK *objmask = ai->mask[R_BAND];
		if (objmask == NULL) {
			#if DEEP_DEBUG_PROC_ATLAS_LOAD
			OBJMASK *master_mask = ai->master_mask;
			if (master_mask != NULL) {
				thError("%s: WARNING - null (objmask), mastermask [npix = %d, nspan = %d, parent = %d] found in (atlast image)[%d]", 
				name, master_mask->npix, master_mask->nspan, ai->parent, id[i]);
				
			} else {
				thError("%s: WARNING - null (objmask), null (mastermask) found in (atlas image) [%d]", name, id[i]);
			}
			#endif
			n_null_objmask++;
			npix_i = -1;
			nspan_i = -1;
		} else {			
			npix_i = objmask->npix;
			nspan_i = objmask->nspan;
			if (npix_i <= 0) {
				#if DEEP_DEBUG_PROC_ATLAS_LOAD
				thError("%s: WARNING - unusual objmask size [npix = %d, nspan = %d] master_mask size [npix = %d, nspan = %d, parent = %d] found at atlas image [%d]", 
				name, npix_i, nspan_i, master_mask->npix, ai->parent, master_mask->nspan, id[i]);
				#endif
				n_bad_objmask++;
			} else {
				#if DEEP_DEBUG_PROC_ATLAS_LOAD
				printf("%s: REPORT - good objmask size [npix = %d, nspan = %d] master_mask size [npix = %d, nspan = %d, parent = %d] found at atlas image [%d] \n", 
				name, npix_i, nspan_i, master_mask->npix, ai->parent, master_mask->nspan, id[i]);
				#endif
				if (n_good_ai > 0) {
					npix_min = MIN(npix_min, npix_i);
					npix_max = MAX(npix_max, npix_i);
				} else {
					npix_min = npix_i;
					npix_max = npix_i;
				}
				n_good_ai++;
			}
		}
	}
	npix[i] = npix_i;
}
printf("%s: size of atlas chain = %d \n", name, n);
printf("%s: size of good atlas images in chain = %d \n", name, n_good_ai);
printf("%s: size of null entries in atlas chain = %d \n", name, n_null_ai);
printf("%s: size of null objmask in atlas images = %d \n", name, n_null_objmask);
printf("%s: size of bad non-null objmask in atlas images = %d \n", name, n_bad_objmask);
printf("%s: maximum pixel size of objmask = %d \n", name, npix_max);
printf("%s: minimum pixel size of objmask = %d \n", name, npix_min);
#if DEEP_DEBUG_PROC_ATLAS_LOAD
printf("%s: ids from atlas images = ", name);
for (i = 0; i < n; i++) printf("%d, ", id[i]);
printf("\n");	
#endif
thFree(npix);
thFree(id);
return(0);
}

int output_phobjc_chain_info(CHAIN *objc_chain) {
char *name = "output_phobjc_chain_info";
if (objc_chain == NULL) {
	thError("%s: WARNING - null (objc chain)", name);	
	return(-1);
}
if (shChainSize(objc_chain) == 0) {
	thError("%s: WARNING - empty (objc chain)", name);
	return(-1);
}
int i, n;
int n_parent = 0, n_null_objc = 0, n_nonnull_objc = 0, n_star = 0, n_galaxy = 0, n_other = 0;
n = shChainSize(objc_chain);
int *id = shCalloc(n, sizeof(int));

for (i = 0; i < n; i++) {
	WOBJC_IO *objc = shChainElementGetByPos(objc_chain, i);
	if (objc == NULL) {
		thError("%s: WARNING - null object found in objc chain [%d]", name, i);
		n_null_objc++;
		id[i] = -1;
	} else {
		id[i] = objc->id;
		if (objc->nchild != 0) n_parent++;
		n_nonnull_objc++;
		OBJ_TYPE type = objc->objc_type;
		if (type == OBJ_STAR) {
			n_star++;
		} else if (type == OBJ_GALAXY) {
			n_galaxy++;
		} else {
			n_other++;
		}
	}
} 
printf("%s: size of objc chain = %d \n", name, n);
printf("%s: size of null objc in chain = %d \n", name, n_null_objc);
printf("%s: size of non-null objc in chain = %d \n", name, n_nonnull_objc);
printf("%s: size of star objects = %d \n", name, n_star);
printf("%s: size of galaxy objects = %d \n", name, n_galaxy);
printf("%s: size of other objects = %d \n", name, n_other);
printf("%s: size of parent objects = %d \n", name, n_parent);
#if DEEP_DEBUG_PROC_PHOBJC_INFO
printf("%s: ids from objc chain = ", name);
for (i = 0; i < n; i++) printf("%d, ", id[i]);
printf("\n");	
#endif
thFree(id);
return(0);
}
 
RET_CODE find_mask_centeroid(OBJMASK *mask, THPIX *rowc, THPIX *colc) {
char *name = "find_mask_centeroid";

shAssert(mask != NULL);
shAssert(rowc != NULL);
shAssert(colc != NULL);

if (mask->npix <= 0) {
	thError("%s: ERROR - (npix = %d) in mask", name, mask->npix);
	*rowc = THNAN;
	*colc = THNAN;
	return(SH_GENERIC_ERROR);
}
int nspan = mask->size;
if (nspan <= 0) {
	thError("%s: ERROR - (nspan = %d) in mask", name, nspan);
	*rowc = THNAN;
        *colc = THNAN;
        return(SH_GENERIC_ERROR);
}
int i, j, npix = 0;
THPIX row, col, row_sum = 0.0, col_sum = 0.0;
for (i = 0; i < nspan; i++) {
	SPAN *span = mask->s + i;
	row = (THPIX) span->y;
	for (j = span->x1; j <= span->x2; j++) {
		col = (THPIX) j;
		row_sum += row;
		col_sum += col;
		npix++;
	} 
}
if (npix != mask->npix) {
	thError("%s: ERROR - counted (npix = %d) while expected (npix = %d)", name, npix, mask->npix);
	*rowc = THNAN;
        *colc = THNAN;
        return(SH_GENERIC_ERROR);
}

THPIX rowc_measured = (THPIX) mask->row0 + (THPIX) row_sum / (THPIX) npix;
THPIX colc_measured = (THPIX) mask->col0 + (THPIX) col_sum / (THPIX) npix;
*rowc = rowc_measured;
*colc = colc_measured;

return(SH_SUCCESS);
}

int compare_mask_id_objc_id_pair(void *pair1, void *pair2) {
	shAssert(pair1 != NULL);
	shAssert(pair2 != NULL);
	float difference = ((MASK_ID_OBJC_ID_PAIR *) pair1)->distance - ((MASK_ID_OBJC_ID_PAIR *) pair2)->distance;
	return((int) (1000 * difference));
}

float measure_distance_between_mask_and_objc(OBJMASK *mask, WOBJC_IO *objc) {
shAssert(mask != NULL);
shAssert(objc != NULL);
float rowc = -1.0, colc = -1.0;
find_mask_centeroid(mask, &rowc, &colc);
float row = objc->objc_rowc, col = objc->objc_colc;
float distance = pow((row - rowc) * (row - rowc) + (col - colc) * (col - colc), 0.5);
return(distance);
}

RET_CODE match_bright_masks_to_objclist(CHAIN *bright_masks, CHAIN *objclist) {
char *name = "match_bright_masks_to_objclist";
if (bright_masks == NULL) {
	thError("%s: ERROR - null input mask chain for bright objects", name);
	return(SH_GENERIC_ERROR);
}
if (objclist == NULL) {
	thError("%s: ERROR - null input chain for objects", name);
	return(SH_GENERIC_ERROR);
}

int nmask = shChainSize(bright_masks);
int nobjc = shChainSize(objclist);

if (nmask == 0 || nobjc == 0) {
	thError("%s: WARNING - found (nmask = %d, nobjc = %d)", name, nmask, nobjc);
	return(SH_SUCCESS);
}
MASK_ID_OBJC_ID_PAIR **d_matrix = shCalloc(nmask, sizeof(MASK_ID_OBJC_ID_PAIR *));
d_matrix[0] = shCalloc(nmask * nobjc, sizeof(MASK_ID_OBJC_ID_PAIR));

int i, j;
for (i = 0; i < nmask; i++) {
	d_matrix[i] = d_matrix[0] + i * nobjc;
}
for (i = 0; i < nmask; i++) {
	OBJMASK *mask = shChainElementGetByPos(bright_masks, i);
	for (j = 0; j < nobjc; j++) {
		WOBJC_IO *objc = shChainElementGetByPos(objclist, j);
		MASK_ID_OBJC_ID_PAIR *d_matrix_ij = d_matrix[i] + j;
		d_matrix_ij->imask = i;
		d_matrix_ij->iobjc = j;
		d_matrix_ij->distance = (float) measure_distance_between_mask_and_objc(mask, objc);
	}
}
	
int *match_index = shCalloc(nmask, sizeof(int));
for (i = 0; i < nmask; i++) match_index[i] = -1;

int n_d_matrix = nmask * nobjc;
MASK_ID_OBJC_ID_PAIR *dd_matrix = d_matrix[0];
qsort(dd_matrix, n_d_matrix, sizeof(MASK_ID_OBJC_ID_PAIR), &compare_mask_id_objc_id_pair);
for (i = 0; i < n_d_matrix; i++) {
	MASK_ID_OBJC_ID_PAIR *mask_objc_pair = dd_matrix + i;
	int iobjc = mask_objc_pair->iobjc;
	int imask = mask_objc_pair->imask;
	/* 
	THPIX distance = mask_objc_pair->distance;
	printf("(%d, %d, %g), ", imask, iobjc, (float) distance);
	*/
	OBJMASK *mask = shChainElementGetByPos(bright_masks, imask);
	WOBJC_IO *objc = shChainElementGetByPos(objclist, iobjc);
	if (match_index[imask] == -1) {
		match_index[imask] = iobjc;
				mask->sum = (float) objc->id;
		printf("%s matched (mask, id = %d) with (objc, id = %d, type = '%s') at a (distance = %g) \n", name, mask->id, objc->id, shEnumNameGetFromValue("OBJ_TYPE", objc->objc_type),  (float) mask_objc_pair->distance);
	}
}

shFree(d_matrix[0]);
shFree(d_matrix);
shFree(match_index);

return(SH_SUCCESS);
}

void print_interesting_mask_info(OBJMASK *mask) {
char *name = "print_interesting_mask_info";
shAssert(mask != NULL);
int id = mask->id;
if (id == 756 || id == 767 || id == 775 || id == 787) {
	THPIX rowc = -1.0, colc = -1.0;
	find_mask_centeroid(mask, &rowc, &colc);
	printf("%s: interesting mask found (id = %d, npix = %d, rowc = %g, colc = %g, nspan = %d) \n", name, id, mask->npix, (float) rowc, (float) colc, mask->size);
}
return;
}


OBJMASK *find_closest_bright_mask(FRAME *f, WOBJC_IO *objc) {
char *name = "find_closest_bright_mask";

shAssert(f != NULL);
shAssert(objc != NULL);
FRAMEDATA *data = f->data;
shAssert(data != NULL);
SPANMASK *phsm = data->phsm;
shAssert(phsm != NULL);

/* 
if (!(objc->objc_flags & OBJECT1_BRIGHT)) {
	printf("%s: objc (id = %d) is  not bright \n", name, objc->id);
	return(NULL);
}
*/

OBJMASK *objmask = NULL;
CHAIN *bright_masks = phsm->masks[S_MASK_BRIGHTOBJECT];

RET_CODE status;

int n = shChainSize(bright_masks);
int i;

THPIX rowc = (THPIX) (objc->objc_rowc);
THPIX colc = (THPIX) (objc->objc_colc);

THPIX d_final = 1.0E20;
int match_count = 0;
static int total_match_count = 0;
for (i = 0; i < n; i++) {
	OBJMASK *mask = shChainElementGetByPos(bright_masks, i);
	/* 
	print_interesting_mask_info(mask);
	*/
	THPIX mrowc = ((THPIX) mask->rmin + (THPIX) mask->rmax) / 2.0;
	THPIX mcolc = ((THPIX) mask->cmin + (THPIX) mask->cmax) / 2.0;

	status = find_mask_centeroid(mask, &mrowc, &mcolc);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get centeroid for (mask, id = %d)", name, mask->id);
		return(NULL);
	}

	THPIX d = pow((rowc - mrowc) * (rowc - mrowc) + (colc - mcolc) * (colc - mcolc), 0.5);
	#if 0
	THPIX drow = (THPIX) mask->rmax - (THPIX) mask->rmin;
	THPIX dcol = (THPIX) mask->cmax - (THPIX) mask->cmin;
	THPIX mask_size = pow(drow * drow + dcol * dcol, 0.5); 
	if (d < mask_size && d < d_final) {
		d_final = d;
		objmask = mask;
	}
	#else
	if ((int) mask->sum == (int) objc->id) {
		d_final = d;
		objmask = mask;
		match_count++;
	}
	#endif

}


if (match_count > 1) {
	thError("%s: WARNING - (match_count = %d) found among bright masks", name, match_count);
}
if (objmask != NULL) {
	total_match_count++; 
	printf("%s: found bright mask (id = %d, npix = %d, n_mask = %d, match_tot = %d) at (%g) pixels away from object (id = %d) \n", name, objmask->id, objmask->npix, n, total_match_count, (float) d_final, objc->id);
} else {
	/* 
	printf("%s: unable to find bright mask for object \n", name);
	*/
}

return(objmask);
}


RET_CODE thFrameAssessTerrainComplexity(FRAME *f, int initid, int initn, int *initk) {
char *name = "thFrameAssessTrainComplexity";
shAssert(initid >= 0);
shAssert(initk != NULL);
shAssert(initn >= 0);

THPIX iqr = THNAN, stdev = THNAN; 
THPIX nstep_mean = -1.0, nstep_mean_all = -1.0;
RET_CODE status;

status = thFrameCalcChisqDispersion(f, &iqr, &stdev, &nstep_mean, &nstep_mean_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (iqr) and (stdev) of chi-squared value for frame", name);
	return(status);
}

int init_count = initn;

if (nstep_mean < (THPIX) NSTEP_MIN_ENOUGH) {
	init_count = MAX((initid + 2), (initn + 1));
	init_count = MIN(init_count, HARD_INIT_COUNT);
	*initk = init_count;
	return(SH_SUCCESS);
}

if (((iqr < (THPIX) -1.0E-5) || (stdev < (THPIX) -1.0E-5)) && iqr != CHISQ_IS_BAD && stdev != CHISQ_IS_BAD) {
	thError("%s: ERROR - round off problem (iqr = %G, stdev = %G) while expected (> 0)", name, (double) iqr, (double) stdev);
	return(SH_GENERIC_ERROR);
}

if (initid <= INITID_THRESH_STDEV_IQR) {
	if (stdev != (THPIX) CHISQ_IS_BAD) {
		if (stdev <= (THPIX) EASY_TERRAIN_CHISQ_STDEV) {
			init_count = EASY_INIT_COUNT;
		} else if (stdev <= (THPIX) HARD_TERRAIN_CHISQ_STDEV) {
			init_count = MEDIUM_INIT_COUNT;
		} else {
			init_count = HARD_INIT_COUNT;
		}
	} else { 
		init_count = MAX((initid + 2), (initn + 1));
		init_count = MIN(init_count, HARD_INIT_COUNT);
	}
} else {
	if (iqr != (THPIX) CHISQ_IS_BAD) {
		if (iqr <= (THPIX) EASY_TERRAIN_CHISQ_IQR) {
			init_count = EASY_INIT_COUNT;
		} else if (iqr <= (THPIX) HARD_TERRAIN_CHISQ_IQR) {
			init_count = MEDIUM_INIT_COUNT;
		} else {
			init_count = HARD_INIT_COUNT;
		}
	} else {
		init_count = MAX((initid + 2), (initn + 1));
		init_count = MIN(init_count, HARD_INIT_COUNT);
	}

}

*initk = init_count;
return(SH_SUCCESS);
}

RET_CODE thFrameCalcChisqDispersion(FRAME *f, THPIX *iqr, THPIX *stdev, THPIX *nstep_mean, THPIX *nstep_mean_all) {
char *name = "thFrameCalcChisqDispersion";
if (iqr == NULL && stdev == NULL) {
	thError("%s: WARNING - null input and output placeholder", name);
	return(SH_SUCCESS);
}
if (f == NULL) {
	thError("%s: ERROR - null input (frame)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;

CHAIN *io_chain = NULL;
status = thFrameGetFrameioChain(f, &io_chain);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (io_chain) from (frame)", name);
	return(status);
}
if (io_chain == NULL || shChainSize(io_chain) == 0) {
		thError("%s: ERROR - null or empty input (io_chain)", name);
		return(SH_GENERIC_ERROR);
}

int count = 0, i, n = shChainSize(io_chain);
CHISQFL chisq_array[n], chisq_min;
float da[n];
THPIX nstep_total = 0.0, nstep_total_all = 0.0;
for (i = 0; i < n; i++) {
	FRAMEIO *io = shChainElementGetByPos(io_chain, i);
	if (io != NULL) {	
		LMDUMP *lmdump = NULL;
		status = thFrameioGetLMDump(io, &lmdump);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (lmdump, i = %d) from (frameio, n = %d)", name, i, n);
			return(status);
		}
		if (lmdump != NULL) {
			CHISQFL chisq = 0.0;
			int nstep = -1, nstep_all = -1;
			CHISQFL pcost = -1.0;
			status = thLMDumpGetMinChisq(lmdump, &chisq, &nstep, &nstep_all, &pcost);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get min (chisq) from (lmdump)", name);
				return(status);
			}
			if (chisq != CHISQ_IS_BAD) {
				chisq_array[count] = chisq;
				nstep_total += (THPIX) nstep;
				nstep_total_all += (THPIX) nstep_all;
				if (count == 0) {
					chisq_min = chisq;
				} else if (chisq < chisq_min) {
					chisq_min = chisq;
				}
				da[count] = 1.0E-3;
				count++;
			}
		}
	}
}

if (count == 0) {
	thError("%s: WARNING - no (chisq) info found in (io_chain)", name);
	if (iqr != NULL) *iqr = THNAN;
	if (stdev != NULL) *stdev = THNAN;
	if (nstep_mean != NULL) *nstep_mean = THNAN;
	if (nstep_mean_all != NULL) *nstep_mean_all = THNAN;
} else if (count == 1) {
	if (iqr != NULL) *iqr = THNAN;
	if (stdev != NULL) *stdev = THNAN;
	if (nstep_mean != NULL) *nstep_mean = nstep_total;
	if (nstep_mean_all != NULL) *nstep_mean = nstep_total_all;
} else {

	float chisq_array_float[count];
	for (i = 0; i < count; i++) {
		chisq_array_float[i] = (float) (chisq_array[i] - chisq_min);
	}
	THPIX nstep_avg = nstep_total / (THPIX) count;
	THPIX nstep_avg_all = nstep_total_all / (THPIX) count;
	printf("%s: creating chisq array statistics \n", name);
	fflush(stderr);
	fflush(stdout);

	BASIC_STAT *stat = thBasicStatNew();
	status = thBasicStatFromArray(chisq_array_float, da, stat, count);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not produce statistics from (reduced chisq array)", name);
		return(status);
	}
	if (iqr != NULL) *iqr = (THPIX) stat->iqr;
	if (stdev != NULL) *stdev = (THPIX) stat->s;
	if (nstep_mean != NULL) *nstep_mean = nstep_avg;
	if (nstep_mean_all != NULL) *nstep_mean_all = nstep_avg_all;
	thBasicStatDel(stat);
}

return(SH_SUCCESS);
}


