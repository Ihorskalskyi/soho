/* PHOTO libraries */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ftcl.h"
#include "dervish.h"
#include "strings.h"
#include "time.h"

#include "phUtils.h"

#include "phPhotoFitsIO.h"
#include "phSpanUtil.h"

#include "thMath.h" 
#include "thDebug.h" 
#include "thIo.h" 
#include "thAscii.h" 
#include "thProc.h" 
#include "thMask.h" 
#include "thMSky.h" 
#include "thObjcTypes.h"
#include "thMGalaxy.h"
#include "thAlgorithm.h"
#include "thMap.h"
#include "thMle.h"
#include "thResidual.h"
#include "thParse.h"
#include "thCalib.h"
#include "thLRG.h"

typedef enum tweakflag {
	ABSOLUTE, FRACTIONAL, N_TWEAKFLAG, UNKNOWN_TWEAKFLAG
	} TWEAKFLAG;

#define SIM_SKY 1
#define ADD_POISSON_NOISE 1
#define DO_BKGRND 0
#define FIT_SHAPE  1
#define FIT_RE 1
#define FIT_E 0
#define FIT_PHI 0
#define FIT_CENTER 0 
#define LOCK_CENTER 1
#define FORCE_INIT_PAR 0
#define TABLEROW 5
#define N_RANDOM_FRAMES 10
#define N_OUTPUT_OBJC 5
#define EllDesc SUPERCANONICAL
#define NSIMOBJC 5000 /* 3000 */
#define NFITOBJC 1
#define TWEAK 1
#define TWEAK_ALL 0
#define THIS_BAND R_BAND
/* image clustering and memory constants */
#define THMEMORY 1300
#define DO_GREEDY 1
#define DO_ALGORITHM 1
#define ALGORITHM_MANUAL 0

int verbose = 0;

static THPIX SKY_FRACTIONAL_TWEAK = 0.5;
#if FORCE_INIT_PAR
static THPIX I_TWEAK = 0.1; 
static THPIX CENTER_TWEAK = 1.0;
static THPIX SHAPE_TWEAK = 1.0;
static THPIX RE_TWEAK = 1.0;
static THPIX PHI_TWEAK = 1.0;
static THPIX E_TWEAK = 1.0;
#else 
#if TWEAK

static THPIX I_TWEAK = 1; 
#if FIT_CENTER
static THPIX CENTER_TWEAK = 1.0;
#endif
#if FIT_SHAPE
static THPIX SHAPE_TWEAK = 1.0;
#if FIT_PHI
static THPIX PHI_TWEAK = 1.0;
#endif
#if FIT_E
static THPIX E_TWEAK = 1.0;
#endif
#endif

#endif
#endif

#if FORCE_INIT_PAR
static THPIX ELLIPTICITY = 0.7;
static THPIX PHI = 1.0;
static THPIX RE = 10.0;
static THPIX STRETCHINESS = 1.0;
static THPIX INTENSITY = 50;
#endif

static void usage(void);
void chain_model_output(char *job, CHAIN *objclist, THOBJCTYPE otype);
void model_output(char *job, THOBJC *objc);
int compare_re(const void *x1, const void *x2);
int compare_count(const void *x1, const void *x2);
void test_sorted_chain(CHAIN *chain, int (*compare)(const void *, const void *));
extern double ddot_(int *, double [], int *, double [], int *);
extern float sdot_(int *, float [], int *, float [], int *);

static RET_CODE tweak(void *p, THPIX e_tweak, char *pname, char **rnames, int rcount, CHAIN *chain, TWEAKFLAG tflag);
static RET_CODE add_poisson_noise_to_image(REGION *im, CHAIN *ampl, THPIX gaussian_noise_dn, CHISQFL *chisq_float, CHISQFL *chisq_int);
static RET_CODE tweak_init_pars(LSTRUCT *l, CHAIN *objclist, int band);

/* added on January 16, 2016 */
static RET_CODE thwart_single_objc(WOBJC_IO *objc, int bndex, float tweakf, char **thwart_record_list, int nthwart, unsigned int seed);
static RET_CODE thwart_phobjcs(CHAIN *phobjcs, int bndex, float tweakf, char **thwart_record_list, int nthwart, unsigned int seed);
static RET_CODE process_frame(FRAME *frame, FRAME *frame_init, SIM_PARSED_INPUT *input, int init);
static RET_CODE add_frame_stat_to_hdr(FRAMESTAT *stat, SIM_PARSED_INPUT *input, CHISQFL chisq_int, CHISQFL chisq_float,THPIX gaussian_noise_dn, HDR *hdr);

/* added on June 11, 2016 */
static CHAIN *select_random_stars_galaxies_from_list(CHAIN *objclist, SIM_PARSED_INPUT *input, int *chosen_list, int take_from_list);
static int objc_pass_threshold(THOBJC *objc, SIM_PARSED_INPUT *input);

int
main(int ac, char *av[])
{

  char *name = "do-thwart";
printf("%s: starting to run '%s' \n", name, name);

phSchemaLoadFromPhoto();

thSchemaLoadFromSoho();


  /* Arguments */

SIM_PARSED_INPUT *input = thSimParsedInputNew();
RET_CODE status = simparser(ac, av, input);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not parse input", name);
	return(-1);
}

printf("%s: *** input *** \n", name);
printf("%s: setupfile = %s\n", name, input->setupfile);
printf("%s: camcol    = %d\n", name, input->camcol);
printf("%s: framefile = %s\n", name, input->framefile); /* this should include input and output objc file and sky file */
printf("%s: tweak f   = %g\n", name, input->smearf);
printf("%s: objclist  = %p\n", name, input->objclist); 
printf("%s: *** end of input *** \n", name);


char *fname;
int noobjcfile = 0, readskyfile = 0, nrow, ncol, camcol, camrow, band, run, rerun, stripe, field;
unsigned int seed;
int sim_on_demand = (input->demand != NO_DEMAND_CODE);
noobjcfile = (input->readobjcfile == 0);
readskyfile = input->readskyfile;
nrow = input->nrow;
ncol = input->ncol;
camcol = input->camcol;
run = input->run;
rerun = input->rerun;
stripe = input->stripe;
field = input->field;
fname = input->framefile;
seed = input->seed;

CHAIN *myobjclist = input->objclist;
SKYPARS *mysky = input->sky;
SKYPARS **myskylist = input->skylist;

printf("%s: initiating modules \n", name);

 printf("%s: initiating IO \n", name); 
 thInitIo();
 printf("%s: initiating Tank \n", name);
  thTankInit();
 printf("%s: initiating Ascii \n", name);
  thAsciiInit();  
 printf("%s: initiating SpanObjmask \n", name);
  initSpanObjmask();

FRAMEFILES *ff;
CHAIN *fchain;
char *strutype = "FRAMEFILES";	

if (input->ff != NULL) {
	fchain = shChainNew(strutype);
	shChainElementAddByPos(fchain, input->ff, strutype, TAIL, AFTER);
} else {
	printf("%s: reading framefile \n", name);
	  /* get the related schema */
  	ASCIISCHEMA *schema = NULL;
  	schema = thTankSchemaGetByType(strutype);
  	if (schema == NULL) {
    		printf("%s: No schema exists for structure %s", name, strutype);
  	}
  	/* get the structure constructor */
  	void *(*strunew)();
 	strunew = schema->constructor;
  	if (strunew == NULL) {
    		printf("%s: structure constructor for %s not available in tank", name, strutype);
  	}
  	/* construct a structure */
  	void *stru;
  	stru = (*strunew)();
  
	ff = (FRAMEFILES *) stru;

  	FILE *fil;
  	fil = fopen(fname, "r");
	if (fil == NULL) {
		thError("%s: ERROR - could not read framefile '%s'", name, fname);
		return(-1);
	}  
  	fchain = thAsciiChainRead(fil);
}

if (shChainSize(fchain) == 0) {
	thError("%s: no structures was properly read", name);
	}
  printf("%s: (%d) pieces of frame info read \n",
	 name, shChainSize(fchain));

  ff = (FRAMEFILES *) shChainElementGetByPos(fchain, 0);
  if (noobjcfile) strcpy(ff->inphobjcfile, "");

  if (ff == NULL) {
    thError("%s: NULL returned from chain - check source code", name);
  } else {  
	#if DEBUG_MAIN_THWART 
    /* fields of the ff structure refer to outbound memory locations */
    printf(">>> phconfigfile = %s \n", ff->phconfigfile);
    printf(">>> phecalibfile = %s \n", ff->phecalibfile);
    printf(">>> out-phflatframefile = %s \n", ff->outphflatframefileformat);
    printf("phmaskfile = %s \n", ff->phmaskfile);
    printf(">>> in-phsmfile = %s \n", ff->inphsmfileformat); /* input fpBIN file format - band to be inserted */
    printf(">>> out-phsmfile = %s \n", ff->outphsmfileformat);  /* output fpBIN file format - band to be inserted */
    printf(">>> phpsfile = %s \n", ff->phpsfile);
    printf(">>> in- phobjcfile  = %s \n", ff->inphobjcfile); /* input fpObjc file */
    printf(">>> out-phobjcfile = %s \n", ff->outphobjcfile); /* output fpObjc file */ 
    printf("thsbfile = %s \n", ff->thsbfile);
    printf("thobfile = %s \n", ff->thobfile);
    printf("thmakselfile = %s \n", ff->thmaskselfile);
    printf("thobjcselfile = %s \n", ff->thobjcselfile);
    printf(">>> out-thsmfile = %s \n", ff->outthsmfileformat); /* output gpBIN file format - band to be inserted */
    printf("thpsfile = %s \n", ff->thpsfile);
    printf(">>> out-thobjcfile = %s \n", ff->outthobjcfileformat); /* ouput gpObjc file format - band to be inserted */
    printf(">>> out-thflatframefile  = %s \n", ff->outthflatframefileformat); /* output fpC file format - band to be inserted */
	#endif

  }

	printf("%s: initiating random numbers \n", name);
	RANDOM *rand;
	char *randstr;
	int nrandom;
	randstr = thCalloc(MX_STRING_LEN, sizeof(char));
	nrandom = (int) (nrow * ncol * (float) N_RANDOM_FRAMES);
	sprintf(randstr, "%d:2", nrandom);
	printf("%s: generating %d random numbers (random frames= %g) \n", name, nrandom, (float) N_RANDOM_FRAMES);
	rand = phRandomNew(randstr, 1);
	printf("%s: setting seed to %u \n", name, seed);
	unsigned int seed0 = phRandomSeedSet(rand, seed);
	printf("%s: old seed was %u \n", name, seed0);

 printf("%s: process and frame \n", name);
 PROCESS *proc;
 proc = thProcessNew();
 int i;
 int bndex[NCOLOR];
 for (i = 0; i < NCOLOR; i++) {
	bndex[i] = i;
}

int iband = 0;
int band_list[NCOLOR] = {2, 0, 1, 3, 4};
int success_list[NCOLOR] = {0, 0, 0, 0, 0};
int perform_list[NCOLOR] = {1, !sim_on_demand, !sim_on_demand, !sim_on_demand, !sim_on_demand};
FRAME *frames[NCOLOR];

int n_perform = 0, perform_count = 0;
for (iband = 0; iband < NCOLOR; iband++) n_perform += (perform_list[iband] != 0);

for (iband = 0; iband < NCOLOR; iband++) { 

	int perform = perform_list[iband];

	if (perform) {

	band = band_list[iband];
	status = thCamrowGetFromBand(band, &camrow);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not retrieve camrow", name);
		return(-1);
	}
	printf("%s: frame for band %d \n", name, band);
	status = thFramefilesPutBand(ff, band, SIM_MODE);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not insert band in framefile", name);
		return(-1);
	}
   	
	FRAME *frame = thFrameNew();
	frames[band] = frame;
  	REGION *im = NULL;
      	if (frame->files != NULL) {
		thFramefilesDel(frame->files);
      	}
      
      	frame->files = ff; 
      	frame->proc = proc;
      
      	printf("%s: loading fpC file\n", name);
      	thFrameLoadImage(frame);
      	/* this part is only used during simulation */
      	frame->data->image = shRegNew("simulated image", nrow, ncol, TYPE_THPIX);
      	im = frame->data->image;

      /* note that because this is only a simulation no fpC file exists prior to this run */
	frame->id->camcol = camcol;
	frame->id->camrow = camrow;
	frame->id->nrow = nrow;
	frame->id->ncol = ncol;
	frame->id->rerun = rerun;
	frame->id->run = run;
	frame->id->field = field;
	frame->id->filter = band;

	input->band = band;
	int init;
	init = 0;
	if (perform_count == 0) init = 1;
	if ((perform_count == n_perform - 1) && (n_perform > 1)) init = -1;
	FRAME *frame_init;
	if (init == 1) frame_init = NULL;
	status = process_frame(frame, frame_init, input, init);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not process frame for band = %d", name, band);
		success_list[iband] = 0;
	} else {
		success_list[iband] = 1;
	}
	if (init == 1) frame_init = frame;

	perform_count++;
	}
}

/* for unknown reason, this returns an error - distribution of the variates were checked and the results produced satisfy the 
randomness condition.
phRandomDel(rand);
*/

printf("%s: reporting on success for each band \n", name);
for (iband = 0; iband < NCOLOR; iband++) {
	band = band_list[iband];
	char cband = '\0';
	thNameGetFromBand(band, &cband);
	char *successreport; 
	if (success_list[iband] == 1) {
		successreport = "successful";
	} else if (success_list[iband] == 0) {
		successreport = "failed";
	} else {
		successreport = "unknown";
	}
	printf("%s: report - band = '%c': %s \n", name, cband, successreport);
}	

 /* no fit to be done by this program */
printf("%s: end of the program \n", name);

thErrShowStack();
  /* successfull run */
  return(0);
}


/*****************************************************************************/

static void
usage(void)
{
   char **line;

   static char *msg[] = {
      "Usage: read_atlas_image [options] output-file n_tr n_tc",
      "Your options are:",
      "       -?      This message",
      "       -h      This message",
      "       -i      Print an ID string and exit",
      "       -v      Turn up verbosity (repeat flag for more chatter)",
      NULL,
   };

   for(line = msg;*line != NULL;line++) {
      fprintf(stderr,"%s\n",*line);
   }
}	

void chain_model_output(char *job, CHAIN *objclist, THOBJCTYPE otype) {

THOBJC *objc;
int iobjc, nobjc, noutput;
nobjc = shChainSize(objclist);
printf("%s: number of objects = %d \n", job, nobjc);
noutput = MIN(nobjc, N_OUTPUT_OBJC);
printf("%s: number of output objects = %d \n", job, noutput);
for (iobjc = 0; iobjc < noutput; iobjc++) {
	objc = shChainElementGetByPos(objclist, iobjc);
	model_output(job, objc);
}
printf("--- %s: end of output --- \n", job);
return;
}


void model_output(char *job, THOBJC *objc) {

CHAIN *thprops;
int nmodel, imodel;
static char *line = NULL;
if (line == NULL) line = thCalloc(MX_STRING_LEN, sizeof(char));

thprops = objc->thprop;
nmodel = shChainSize(thprops);
for (imodel = 0; imodel < nmodel; imodel++) {
	SCHEMA *s;
	SCHEMA_ELEM *se;
	int n, i;
	char *rname;
	THPIX *value;
	THPROP *prop;
	prop = shChainElementGetByPos(thprops, imodel);	 		
	s = shSchemaGetFromType(shTypeGetFromName(prop->pname));
	n = s->nelem;
	printf("--- %s parameters for (objc: %d) (model: '%s') --- \n", 
		job, objc->thid, prop->mname);
	for  (i = 0; i < n; i++) {
		se = s->elems + i;
		rname = se->name;
		value = shElemGet(prop->value, se, NULL);
		if (strlen(line) != 0) {
			sprintf(line, "%s, %15s = %.5e", line, rname, *value);
		} else {
			sprintf(line,"%15s = %.5e", rname, *value);
		}
		if (((i + 1)%TABLEROW == 0) || i == n - 1) {
			printf("%s\n", line);
			strcpy(line, "");
		}
	}
}
return;
}

RET_CODE tweak(void *p, THPIX e_tweak, char *pname, char **rnames, int rcount, CHAIN *chain,
		TWEAKFLAG tflag) {
char *name = "tweak";

/* this assumes that all records (schema_elems) are of type THPIX */

SCHEMA *s;
SCHEMA_ELEM *se;

if (p == NULL || pname == NULL || strlen(pname) == 0) {
	thError("%s: ERROR - void or unknown input parameter cannot be tweaked", name);
	return(SH_GENERIC_ERROR);
}
TYPE ptype;
ptype = shTypeGetFromName(pname);
s = shSchemaGetFromType(ptype);
if (s == NULL) {
	thError("%s: ERROR - unknown parameter type '%s'", name, pname);
	return(SH_GENERIC_ERROR);
}
if (rcount < 0) {
	thError("%s: ERROR - negative record count (%d)", name, rcount);
	return(SH_GENERIC_ERROR);
}
if (rcount > 0 && rnames == NULL) {
	thError("%s: ERROR - null record name, while record count is positive (%d)", name, rcount);
	return(SH_GENERIC_ERROR);
}


RET_CODE status;
int i;
void *x;
#define YTYPE FL32
YTYPE y;
char *ytype = "FL32";

if (rcount == 0) {
	int n; 
	int onlypos = 0;
	n = s->nelem;
	for (i = 0; i < n; i++) {
		se = s->elems + i;
		if (se == NULL) {
			thError("%s: WARNING - could not find %d-th record in '%s'", name, i, pname);
		} else {
			onlypos = !strcmp(se->name, "re");	
			x = shElemGet(p, se, NULL);
			status = thqqTrans(x, se->type, &y, ytype);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not read record '%s' of type '%s' in '%s' into a double variable", name, se->name, se->type, pname);
				return(status);
			}
			int poscheck = 1;
			while (poscheck) {
				if (tflag == FRACTIONAL) {
					y *= ((YTYPE) 1.00 + ((YTYPE) phGaussdev()) * (YTYPE) e_tweak);
				} else if (tflag == ABSOLUTE) {
					y += ((YTYPE) phGaussdev()) * (YTYPE) e_tweak;
				}
				poscheck = onlypos && (y > (YTYPE) 0);
			}
			status = thqqTrans(&y, ytype, x, se->type);
 			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not transform '%s' data types into '%s' for record '%s' of '%s'", name, ytype, se->type, se->name, pname);
				return(status);
			}
		}
	}
} else {	
	char *rname;
	int onlypos = 0;
	for (i = 0; i < rcount; i++) {
		rname = rnames[i];
		onlypos = (!strcmp(rname, "re"));
		if (rname != NULL && strlen(rname) != 0) {
			se = shSchemaElemGetFromType(ptype, rname);
			if (se == NULL) {
				thError("%s: WARNING - could not find record '%s' in '%s'", name, rname, pname);
			} else {
				x = shElemGet(p, se, NULL);
				status = thqqTrans(x, se->type, &y, ytype);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not read record '%s' of type '%s' in '%s' into a working variable of type '%s'", name, se->name, se->type, pname, ytype);
					return(status);
				}
				int poscheck = 1;
				while (poscheck) {
					if (tflag == FRACTIONAL) {
						y *= ((YTYPE) 1.00 + ((YTYPE) phGaussdev()) * (YTYPE) e_tweak);
					} else if (tflag == ABSOLUTE) {
						y += ((YTYPE) phGaussdev()) * (YTYPE) e_tweak;
					}
					poscheck = onlypos && (y > (YTYPE) 0);
				}
				status = thqqTrans(&y, ytype, x, se->type);
 				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not transform '%s' data type into '%s' for record '%s' of '%s'", name, ytype, se->type, se->name, pname);
					return(status);
				}

		}
		}
	}
}
#undef YTYPE 
return(SH_SUCCESS);
}

RET_CODE add_poisson_noise_to_image(REGION *im, CHAIN *ampl,  THPIX gaussian_noise_dn, CHISQFL *chisq_float, CHISQFL *chisq_int) {
char *name = "add_poisson_noise_to_image";

if (im == NULL) {
	thError("%s: ERROR - null input image", name);
	return(SH_GENERIC_ERROR);
}

if (ampl == NULL) {
	thError("%s: ERROR - null amplifier information", name);
	return(SH_GENERIC_ERROR);
}
shAssert(chisq_float != NULL);
shAssert(chisq_int != NULL);

int nrow, ncol, i, j, n0 = 0, n_ = 0, nplus = 0;
THPIX *row;
FL32 y, z;

nrow = im->nrow;
ncol = im->ncol;

int ia, na;
na = shChainSize(ampl);
printf("%s: number of amplifiers found for this field: (na = %d) \n", name, na);

MLEDBLE chisq_value = (MLEDBLE) 0.0, residual = 0.0, chisq_value_temp = 0.0, chisq_item;
MLEDBLE chisq_value2 = (MLEDBLE) 0.0, residual2 = 0.0, chisq_value2_temp = 0.0, chisq_item2;
chisq_value = residual = chisq_value_temp = chisq_item = (MLEDBLE) 0.0;
chisq_value2 = residual2 = chisq_value2_temp = chisq_item2 = (MLEDBLE) 0.0;
for (ia = 0; ia < na; ia++) {

AMPL *a;
int row0, row1, col0, col1;
THPIX g, readnoise, var;
FL32 mu;

a = shChainElementGetByPos(ampl, ia);
if (a == NULL) {
	thError("%s: WARNING - null (AMPL) structure discovered in (ampl) chain", name);
} else {
	row0 = a->row0;
	row1 = a->row1;
	col0 = a->col0;
	col1 = a->col1;
	g = a->gain;
	readnoise = a->readnoise;
    	var = g * pow(readnoise, 2);
	printf("%s: amplifier [%d] borders: (row0 = %d, col0 = %d), (row1 = %d, col1 = %d) \n", name, ia, row0, col0, row1, col1);
	#if GAUSSIAN_NOISE && CONSTANT_GAUSSIAN_NOISE
		printf("%s: using constant gaussian noise instead of gain and readnoise: GAUSSIAN_NOISE_DN = %g \n", name, (float) gaussian_noise_dn);
	#else
		printf("%s: amplifier (%d) - (gain = %g)\n", name, ia, g);
		#if PHOTO_WEIGHT_DEFINITION
    		printf("%s: calculating chisq based on PHOTO definition (amplifier = %d, gain = %g, readnoise = %g) \n", name, ia, (float) g, (float) readnoise);
    		#else	 
    		printf("%s: calculating chisq without PHOTO definition (amplifier = %d, gain = %g, readnoise = %g) \n", name, ia, (float) g, (float) readnoise);
    		#endif
	#endif

	int row_start = row0, col_start = col0;
	int row_end = row1, col_end = col1;
	if (row_start < 0) row_start = 0;
	if (col_start < 0) col_start = 0;
	if (row_end >= nrow) row_end = nrow - 1;
	if (col_end >= ncol) col_end = ncol - 1;

	for (i = row_start; i <= row_end; i++) {
		row = im->rows_thpix[i];
		for (j = col_start; j <= col_end; j++) {
		y = (FL32) (row[j]);
		if (y > (FL32) 0.0) {
			#if GAUSSIAN_NOISE
				#if CONSTANT_GAUSSIAN_NOISE
				z = y + ((FL32) gaussian_noise_dn) * phGaussdev();
				#else
				z = y + (sqrt(y / g + var)) * phGaussdev();	
				#endif
			#else
			mu = g * y;
			z = phPoissondev(mu) / g + sqrt(var) * phGaussdev();
			#endif
			
			row[j] = z;
			int z2 = (int) (z + 0.5);
			FL32 weight = 0.0, weight2 = 0.0;
			#if GAUSSIAN_NOISE && CONSTANT_GAUSSIAN_NOISE
			weight = weight2 = (FL32) 1.0 / pow((FL32) gaussian_noise_dn, 2.0);
			#else
				#if PHOTO_WEIGHT_DEFINITION
				/* if z was integer do the following */
				weight2 = g / (z2 > 0 ? 
				(0.5 + sqrt(pow(z2 + var, 2) + 0.25)) : var) ;
				/* the following is a less accurate but faster estimate for the weight
				wr[col] = gain / (ir[col] > 0 ? ir[col] + var: var) ;
				*/
				/* now that z is not ineteger do the following: */
				weight = g / (z > 0 ? z + var: var);
				#else
				weight = (z > 0 ? g / z: 0.0);
				weight2 = weight;
				#endif
			#endif

			#if DO_KAHAN_SUM
			chisq_item = (MLEDBLE) ((y - z) * (y - z) * weight) - residual;
			chisq_item2 = (MLEDBLE) ((y - (FL32) z2) * (y - (FL32) z2) * weight2) - residual2;	
			chisq_value_temp = chisq_value + chisq_item;
			chisq_value2_temp = chisq_value2 + chisq_item2;
			residual = (MLEDBLE) (chisq_value_temp - chisq_value) - (MLEDBLE) chisq_item;
			residual2 = (MLEDBLE) (chisq_value2_temp - chisq_value2) - (MLEDBLE) chisq_item2;
			chisq_value = chisq_value_temp;
			chisq_value2 = chisq_value2_temp;
			

			#else
			chisq_value += (y - z) * (y - z) * weight;
			chisq_value2 += (y - (FL32) z2) * (y - (FL32) z2) * weight2;	
			#endif
			nplus++;
		} else if (y == (THPIX) 0.0) {
			n0++;
		} else {
			n_++;
		}
		}
	}
}
}

*chisq_float = (CHISQFL) chisq_value;
*chisq_int = (CHISQFL) chisq_value2;
if (n0 > 0) printf("%s: (zero pixels: %d) \n", name, n0);
if (n_ > 0) printf("%s: (negative pixels: %d) \n", name, n_);
if (nplus > 0) printf("%s: (positive pixels: %d) \n", name, nplus);
printf("%s: (chisq for continuous model: %g) \n", name, (float) chisq_value);

return(SH_SUCCESS);
}

int compare_re(const void *x1, const void *x2) {
	WOBJC_IO *y1, *y2;
	shAssert(x1 != NULL);
	shAssert(x2 != NULL);
	y1 = *(WOBJC_IO **) x1;
	y2 = *(WOBJC_IO **) x2;
	float z1, z2;
	z1 = (float) y1->r_deV[THIS_BAND];
	z2 = (float) y2->r_deV[THIS_BAND];
	int res;
	if (y1->objc_type == OBJ_GALAXY && 
		y2->objc_type == OBJ_GALAXY) {
		if (z1 > z2) {
			res = -1;
		} else if (z1 < z2) {
			res = 1;
		} else {
			res = 0;
		}
	} else if (y1->objc_type == OBJ_GALAXY && y2->objc_type != OBJ_GALAXY) {
		res = -1;
	} else if (y1->objc_type != OBJ_GALAXY && y2->objc_type == OBJ_GALAXY) {
		res = 1;
	} else {
		res = 0;
	}
	return(res);
}

int compare_count(const void *x1, const void *x2) {
	WOBJC_IO *y1, *y2;
	shAssert(x1 != NULL);
	shAssert(x2 != NULL);
	y1 = *(WOBJC_IO **) x1;
	y2 = *(WOBJC_IO **) x2;
	int res;
	if (y1->objc_type == OBJ_GALAXY && y2->objc_type == OBJ_GALAXY) {
		float z1, z2;
		z1 = (y1->counts_exp[R_BAND]) * (1.0 - y1->fracPSF[R_BAND]) + y1->counts_deV[R_BAND] * y1->fracPSF[R_BAND];	
		z2 = (y2->counts_exp[R_BAND]) * (1.0 - y2->fracPSF[R_BAND]) + y2->counts_deV[R_BAND] * y2->fracPSF[R_BAND];
		if (z1 > z2) {
			res = -1;
		} else if (z1 < z2) {
			res = 1;
		} else {
			res = 0;
		}
	} else if (y1->objc_type == OBJ_GALAXY) {
		res = -1;
	} else if (y2->objc_type == OBJ_GALAXY) {
		res = 1;
	} else {
		res = 0;
	}
	return(res);
}

RET_CODE tweak_init_pars(LSTRUCT *l, CHAIN *objclist, int band) {
char *name = "tweak_init_pars";

/* add all the models in the object to the map */
static char **rnames = NULL, **iornames = NULL, **mnames = NULL;
char *mname;
int nrname = 10;
int nm_max = 2;
int i;
if (rnames == NULL) {
	mnames = thCalloc(nm_max, sizeof(char *));
	rnames = thCalloc(nrname, sizeof(char*));
	iornames = thCalloc(nrname, sizeof(char *));
	for (i = 0; i < nrname; i++) {
		mnames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
		rnames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
		iornames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
	}
}

int pos = 0;
SKYPARS *ps;
THOBJC *objc = NULL;
TYPE ptype;
char *pname;

int ii, j = 0;
ii = j;
strcpy(rnames[j++], "I");
#if FIT_SHAPE 
int ishape, jshape, nshape;
ishape = j;
if (EllDesc == ALGEBRAIC) {
	strcpy(rnames[j++], "a");
 	strcpy(rnames[j++], "b");
	strcpy(rnames[j++], "c");
} else if (EllDesc == CANONICAL) {
	#if FIT_RE
	strcpy(rnames[j++], "re");
	#endif
	#if FIT_E
	strcpy(rnames[j++], "e");
	#endif
	#if FIT_PHI
	strcpy(rnames[j++], "phi");
	#endif
} else if (EllDesc == SUPERCANONICAL || EllDesc == UBERCANONICAL) {
	#if FIT_RE
	strcpy(rnames[j++], "re"); 
	#endif
	#if FIT_E
	strcpy(rnames[j++], "E");
	#endif
	#if FIT_PHI
	strcpy(rnames[j++], "phi");
	#endif
}	
jshape = j;
nshape = j;
#endif
#if FIT_CENTER
int icenter, jcenter, ncenter;
icenter = j;
strcpy(rnames[j++], "xc");  
strcpy(rnames[j++], "yc");
jcenter = j;
ncenter = (jcenter - icenter);
#endif

int ntweak = 0;
if (TWEAK_ALL) {
	ntweak = shChainSize(objclist);
} else {
	ntweak = MIN(1+ NFITOBJC, shChainSize(objclist));
}
RET_CODE status;
for (pos = 0; pos < ntweak; pos++) {

	THOBJCTYPE objctype;
	char *objcname;
	objc = shChainElementGetByPos(objclist, pos);
	objctype = objc->thobjctype;
	thObjcNameGetFromType(objctype, &objcname);
	if (objctype == SKY_OBJC) {

	/* sky */ 
	thObjcGetIopar(objc, (void **) &ps, &ptype);
	pname = shNameGetFromType(ptype);	
	if (ps != NULL) {
	status = tweak((void *) ps, SKY_FRACTIONAL_TWEAK, pname, NULL, 0, objclist, ABSOLUTE);
	if (status != SH_SUCCESS) {
		printf("%s: ERROR - could not tweak '%s' \n", name, pname);
		return(-1);
	}
	} else {
		printf("%s: WARNING - null parameter '%s' in object (objc: %d) - no tweaking \n", name, pname, objc->thid);
	}
	} else if (!strcmp(objcname, "DEVGALAXY") || !strcmp(objcname, "EXPGALAXY") || !strcmp(objcname, "GAUSSIAN") || (!strcmp(objcname, "GALAXY"))) {

	/* galaxy */
	WOBJC_IO *pg;
	pg = ((PHPROPS *) objc->phprop)->phObjc;
	thObjcGetIopar(objc, (void **) &pg, &ptype);
	pname = shNameGetFromType(ptype);

	mname = mnames[0];
	if (!strcmp(objcname, "DEVGALAXY")) strcpy(mname, "deV");
	if (!strcmp(objcname, "EXPGALAXY")) strcpy(mname, "Exp");
	if (!strcmp(objcname, "GAUSSIAN")) strcpy(mname, "gaussian");
	if (!strcmp(objcname, "GALAXY"))  {
		strcpy(mnames[0], "deV");
		strcpy(mnames[1], "Exp");
	}
	if (!strcmp(objcname, "CDCANDIDATE")) {
		strcpy(mnames[0], "deV");
		strcpy(mnames[1], "deV2");
	}


	/* tweak all objects */
	printf("%s: tweaking (objc: %d): ", name, objc->thid);
	for (i = 0; i < j; i++) {
		sprintf(iornames[i], "%s_%s", rnames[i], mname);
		printf("'%s' ", iornames[i]);
	}
	printf("\n");
	
	if (pg != NULL) {

	THPIX e_tweak;
	#if FORCE_INIT_PAR
	e_tweak = I_TWEAK;
	status = tweak((void *)pg, e_tweak, pname, iornames + ii, 
			1, objclist, FRACTIONAL);
	#else
	e_tweak = 0.0;
	if (!strcmp(mname, "deV")) {
		e_tweak = I_TWEAK * pg->counts_deVErr[band] / ((THPIX) DEV_MCOUNT * pow(DEV_RE_SMALL + pg->r_deV[band], 2));
		if (pos < N_OUTPUT_OBJC) printf("%s: i_tweak('%s') = %g; countErr = %g, rErr = %g \n", 
		name, mname, e_tweak, pg->counts_deVErr[band], pg->r_deV[band]);} 
	else if (!strcmp(mname, "Exp")) {
		e_tweak = I_TWEAK * pg->counts_expErr[band] / ((THPIX) EXP_MCOUNT * pow(EXP_RE_SMALL + pg->r_exp[band], 2));
		
		if (pos < N_OUTPUT_OBJC) printf("%s: i_tweak('%s') = %g; countErr = %g, rErr = %g \n", 
		name, mname, e_tweak, pg->counts_expErr[band], pg->r_exp[band]);
	}
	status = tweak((void *)pg, e_tweak, pname, iornames + ii, 
			1, objclist, ABSOLUTE);
	#endif
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not tweak '%s'", name, pname);
		return(status);
	} 
	#if FIT_SHAPE
	status = tweak((void *) pg, SHAPE_TWEAK, pname, iornames + ishape, nshape, objclist, FRACTIONAL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not tweak '%s'", name, pname);
		return(status);
	} 

	#endif
	#if FIT_CENTER
	for (i = 0; i < ncenter; i++) {
		e_tweak = 0.0;
		#if FORCE_INIT_PAR
		e_tweak = CENTER_TWEAK;
		#else 
		if (!strcmp(rnames[i + icenter], "xc")) {
			e_tweak = CENTER_TWEAK * ph->rowcErr[band];
		} else if (!strcmp(rnames[i + icenter], "yc")) {
			e_tweak = CENTER_TWEAK * ph->colcErr[band];
		}
		#endif
		status = tweak((void *) pg, e_tweak, pname, iornames + icenter + i, 
				1, objclist, ABSOLUTE);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not tweak '%s'", name, pname);
			return(status);
		}
	} 
	#endif
	
	if (!strcmp(objcname, "GALAXY")) {

	mname = mnames[1];
	printf("%s: tweaking (objc: %d): ", name, objc->thid);	
	for (i = 0; i < j; i++) {
		sprintf(iornames[i], "%s_%s", rnames[i], mname);
		printf("'%s' ", iornames[i]);
	}
	printf("\n");

	e_tweak = 0.0;
	#if FORCE_INIT_PAR
	e_tweak = I_TWEAK;
	status = tweak((void *)pg, e_tweak, pname, iornames + ii, 
		1, objclist, FRACTIONAL);
	#else
	if (!strcmp(mname, "deV")) {
		e_tweak = I_TWEAK * pg->counts_deVErr[band] / ((THPIX) DEV_MCOUNT * pow(DEV_RE_SMALL + pg->r_deV[band], 2));	
		if (pos < N_OUTPUT_OBJC) printf("%s: i_tweak('%s') = %g; countErr = %g, rErr = %g \n", 
		name, mname, (float) e_tweak, (float) pg->counts_deVErr[band], (float) pg->r_deV[band]);
	} else if (!strcmp(mname, "Exp")) {
		e_tweak = I_TWEAK * pg->counts_expErr[band] / ((THPIX) EXP_MCOUNT * pow(EXP_RE_SMALL + pg->r_exp[band], 2));
		if (pos < N_OUTPUT_OBJC) printf("%s: i_tweak('%s') = %g; countErr = %g, rErr = %g \n", 
		name, mname, (float) e_tweak, (float) pg->counts_expErr[band], (float) pg->r_exp[band]);
	}
	status = tweak((void *)pg, e_tweak, pname, iornames + ii, 1, objclist, ABSOLUTE);
	#endif
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not tweak '%s'", name, pname);
		return(status);
	} 
	#if FIT_SHAPE
	status = tweak((void *) pg, SHAPE_TWEAK, pname, iornames + ishape, nshape, objclist, FRACTIONAL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not tweak the shape", name);
		return(status);
	}
	#endif
	#if FIT_CENTER
	for (i = 0; i < ncenter; i++) {
		e_tweak = 0.0;
		#if FORCE_INIT_PAR
		e_tweak = CENTER_TWEAK;
		#else
		if (!strcmp(rnames[i + icenter], "xc")) {
			e_tweak = CENTER_TWEAK * ph->rowcErr[band];
		} else if (!strcmp(rnames[i + icenter], "yc")) {
			e_tweak = CENTER_TWEAK * ph->colcErr[band];
		}
		#endif
		status = tweak((void *) pg, e_tweak, pname, iornames + icenter + i, 
				1, objclist, ABSOLUTE);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not tweak '%s'", name, pname);
			return(status);
		}
	} 
	#endif

	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not tweak '%s'", name, pname);
		return(status);
	}

	}	
	} else {
		printf("%s: WARNING - null parameter '%s' in object (objc: %d) - no tweaking \n", name, pname, objc->thid);
	}

	}
	}
	/* dumping parameters into their respective location */ 
 
/* 	
	#if FORCE_INIT_PAR 
*/
	printf("%s: assigning parameter and amplitude arrays in (mapmachine) from tweaked IO parameters \n", name);
	thLDumpAmp(l, IOTOMODEL);
	thLDumpPar(l, IOTOMODEL);

	thLDumpAmp(l, IOTOX);
	thLDumpPar(l, IOTOX);
	thLDumpPar(l, IOTOXN);
/* 
	#else

	printf("%s: assigning SDSS parameters to (mapmachine) arrays", name);
	thLDumpAmp(l, MODELTOX);
	thLDumpPar(l, MODELTOX);

	#endif
*/


	printf("%s: assigning values to (a, p) from updated (aN, pN) \n", name);
	status = UpdateXFromXN(l, APSECTOR);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update parameters to the tweaked values", name);
		return(status);
	}

return(SH_SUCCESS);
}

void test_sorted_chain(CHAIN *chain, int (*compare)(const void *, const void *)) {
char *name = "test_sorted_chain";
if (chain == NULL || compare == NULL) {
	thError("%s: ERROR - null input", name);
	return;
}
int n = shChainSize(chain);
int i, error = 0;

#if 0
printf("%s: listing chain elements (r_deV) (chain size = %d) \n", name, n);
for (i = 0; i < n; i++) {
	WOBJC_IO *x1;
	x1 = shChainElementGetByPos(chain, i);
	if (x1->objc_type != OBJ_GALAXY) {
		printf(" %s, ", shEnumNameGetFromValue("OBJ_TYPE", x1->objc_type));	
	} else {
		printf(" %g,", (float) x1->r_deV[R_BAND]);
	}
}
printf("\n");
#endif

for (i = 0; i < n - 1; i++) {
	const void *x1, *x2;
	x1 = shChainElementGetByPos(chain, i);
	x2 = shChainElementGetByPos(chain, i+1);
	int res = compare(&x1, &x2);
	if (res == -1) {
		/* thError("%s: ERROR - chain descends at (i = %d)", name, i);
		*/
		error++;
	}
}
if (error) {
	thError("%s: ERROR - found at least (%d) misplaced elements in chain", name, error);
}
return;
}

RET_CODE thwart_single_objc(WOBJC_IO *objc, int bndex, float tweakf, char **thwart_record_list, int nthwart, unsigned int seed) {
char *name = "thwart_single_objc";
if (objc == NULL) {
	thError("%s: ERROR - null object cannot we tweaked", name);
	return(SH_GENERIC_ERROR);
}
shAssert(objc != NULL);
objc->r_deV[bndex] += objc->r_deVErr[bndex] * (float) phGaussdev() * tweakf;
objc->r_exp[bndex] += objc->r_expErr[bndex] * (float) phGaussdev() * tweakf;
return(SH_SUCCESS);
}


RET_CODE thwart_phobjcs(CHAIN *phobjcs, int bndex, float tweakf, char **thwart_record_list, int nthwart, unsigned int seed) {
char *name = "thwart_phobjcs";

shAssert(phobjcs != NULL);
WOBJC_IO *objc;
RET_CODE status;
int i, nobjc;
nobjc = shChainSize(phobjcs);
for (i = 0; i < nobjc; i++) {
	objc = shChainElementGetByPos(phobjcs, i);
	status = thwart_single_objc(objc, bndex, tweakf, thwart_record_list, nthwart, seed);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not thwart the phobjc at i = ", name, i);
		return(status);
	}
}
return(SH_SUCCESS);
}


RET_CODE process_frame(FRAME *frame, FRAME *frame_init, SIM_PARSED_INPUT *input, int init) {
char *name = "process_frame";
shAssert(frame != NULL);
shAssert(input != NULL);
shAssert(init == 0 || init == 1 || init == -1);

RET_CODE status; 
char *fname;
int noobjcfile = 0, readskyfile = 0, nrow, ncol, camcol, camrow, band, run, rerun, stripe, field, do_cD_classify = 0, fpC_do_float = 0;
unsigned int seed;
GCLASS cdclass = UNKNOWN_GCLASS;
noobjcfile = (input->readobjcfile == 0);
readskyfile = input->readskyfile;
nrow = input->nrow;
ncol = input->ncol;
camcol = input->camcol;
band = input->band;
run = input->run;
rerun = input->rerun;
stripe = input->stripe;
field = input->field;
fname = input->framefile;
seed = input->seed;
do_cD_classify = input->do_cD_classify;
cdclass = input->cdclass;
fpC_do_float = input->fpC_do_float;
THPIX gaussian_noise_dn = (THPIX) input->gaussian_noise_dn;

status = thCamrowGetFromBand(band, &camrow);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not retrieve camrow", name);
	return(status);
}
CHAIN *myobjclist = input->objclist;
SKYPARS *mysky = input->sky;
SKYPARS **myskylist = input->skylist;
int sim_on_demand = (input->demand != NO_DEMAND_CODE);
int demand_code = input->demand;

if (sim_on_demand) {
	PSF_CONVOLUTION_TYPE galaxypsf = input->galaxypsf;
	status = thMGalaxyPsfSet(galaxypsf);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not set Galaxy Psf Convolution method to '%s'", name, shEnumNameGetFromValue("PSF_CONVOLUTION_TYPE", galaxypsf));
		return(status);
	}
}
GC_SOURCE gcsource = input->gcsource;
if (init != 1 && gcsource == SWEEP_FILE_AND_WRITE) {
	gcsource = SWEEP_FILE;
	printf("%s: changing gcsource to 'SWEEP_FILE' \n", name);
}

printf("%s: loading data for frame \n", name);
status = thFrameLoadData(frame, gcsource);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not load data for frame", name);
	return(status);
}
 

if (init != 1 && frame_init != NULL) {
	printf("%s: copying object data from initial frame \n", name);
	status = thFrameCopyDataObjcs(frame, frame_init, 0);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy data from frame", name);
		return(status);
	}
}

thPhpropChainPrintMatchStat(frame->data->phprop);
input->sky = frame->data->sky;

/* PERFORMING FIT TO THE FAKE FRAME */
/* initializing model and object bank */
if (init != 1) {
	status = thModelTerminate();
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not terminate previously initiated model banks", name);
		return(status);
	}
}
status = thModelInit();
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not initate model banks", name);
	return(status);
}
/* introducing sky to the object bank */
printf("%s: initiating all models and object types  ... \n", name);
printf("%s: initiating sky model and object type\n", name);
thSkyObjcInit(nrow, ncol, frame->data->ampl, &g0sky);
printf("%s: initiating other models \n", name);
MGModelsInit();

int classflag = (STAR_CLASSIFICATION | GALAXY_CLASSIFICATION);
if (do_cD_classify) classflag |= CD_CLASSIFICATION;
status = thMGInitAllObjectTypes(classflag, cdclass, DEVEXP_GCLASS, SIM_MODE);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not initialize all the necessary object types after initializing models", name);
	return(status);
}

int i, nm_max = 2;
char *mname, **mnames = thCalloc(nm_max, sizeof(char *));
for (i = 0; i < nm_max; i++) {
	mnames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
} 
mname = mnames[0];

printf("%s: models and object types initiated \n", name);	
/* define THOBJC with type SKY
construct a MAP
*/

int nsimobjc = 0;

THOBJCID id = 0;
THOBJC *objc = NULL;
CHAIN *objclist = NULL;
objclist = shChainNew("THOBJC");

WOBJC_IO *phobjc;
CALIB_WOBJC_IO *cphobjc;
CHAIN *phobjcs, *cphobjcs, *cwphobjcs;
int iobjc, nobjc = 0;	

phobjcs = frame->data->phobjc;
int *bndex = NULL;
status = thFrameGetPhObjcs(frame, &phobjcs, &bndex);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (phobjc chain) and (bndex array) from (frame)", name);
	return(status);
}
/* thwart the phObjc information for this particular band in the records given */
int bmdex = 0;
float tweakf = input->smearf;
char **thwart_record_list = NULL;
int nthwart = 1;
status = thwart_phobjcs(phobjcs, bmdex, tweakf, thwart_record_list, nthwart, seed);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not thwart phobjc records ", name);
	return(status);
}
if (phobjcs != NULL) nobjc = shChainSize(phobjcs);
printf("%s: (%d) objects read from SDSS file \n", name, nobjc);


MAPMACHINE *map;
printf("%s: loading models into (mapmachine) \n", name);
map = thMapmachineNew();
/* inserting map into frame */
status = thFrameworkPutMap(frame->work, map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not put (map) in (framework)", name);
	return(status);
}

/* add all the models in the object to the map */
char **rnames, **iornames;
int nrname = 10;
rnames = thCalloc(nrname, sizeof(char*));
iornames = thCalloc(nrname, sizeof(char *));
for (i = 0; i < nrname; i++) {
	rnames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
	iornames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
}
LSTRUCT *l;
LWORK *lwork;
REGION *simreg = NULL;

int pos = 0;
char *ioname;
TYPE iotype;
SKYPARS *ps;

/* introducing sky to the simulated set of objects  */
#define DO_SKY 1
#if DO_SKY
printf("%s: creating sky object \n", name);
objc = thSkyObjcNew();
if (objc == NULL) {
	thError("%s: ERROR - could no create object sample", name);
	return(SH_GENERIC_ERROR);
}
printf("%s: loading sky object onto (mapmachine) \n", name);
thMAddObjcAllModels(map, objc, band, SEPARATE);
objc->thid = id++;
shChainElementAddByPos(objclist, objc, "THOBJC", TAIL, AFTER);

#if SIM_SKY
printf("%s: uploading sky simulation parameters \n", name);
status = thMCompile(map);
if (status != SH_SUCCESS) {
thError("%s: ERROR - could not compile the map", name);
	return(status);
}	
/* constructing LSTRUCT */
printf("%s: constructing likelihood data structure (lstruct) \n", name);
status = thFrameLoadLstruct(frame, NULL, DEFAULT_CALIBTYPE, LFIT_RECORD_ALL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not load (lstruct)", name);
	return(status);
}
status = thFrameworkGetLstruct(frame->work, &l);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lstruct) from (framework)", name);
	return(status);
}
l->lmodel->lmachine = map;
lwork = l->lwork;

/* simulating the sky */
mysky = input->sky;
if (sim_on_demand) {
	if (input->sky != NULL) {
		thError("%s: WARNING - two conflicting definitions of sky parased as input", name);
		printf("%s: previously I_s00 = %g, I_z00 = %g \n", name, (float) mysky->I_s00, (float) mysky->I_z00);
	}
	if (myskylist == NULL) {
		thError("%s: WARNING - empty (skylist) parsed as input while demand (code = %d)", name, demand_code);
		mysky = NULL;
	} else {
		memcpy(mysky, myskylist[band], sizeof(SKYPARS));
		printf("%s: sky set to I_s00 = %g, I_z00 = %g \n", name, (float) mysky->I_s00, (float) mysky->I_z00);
	}
}
if (mysky != NULL) {
	printf("%s: sky simulation \n", name); 
	objc = shChainElementGetByPos(objclist, pos);
	thObjcGetIopar(objc, (void **) &ps, &iotype);
	ioname = shNameGetFromType(iotype);	
	if (ps != NULL && !strcmp(ioname, "SKYPARS")) {	
		printf("%s: setting simulation values for sky in io-par \n", name);	
		thqqTrans(mysky, ioname, ps, ioname);
	} else {
		printf("%s: WARNING - null parameter in object (objc: %d) \n", name, objc->thid);
	}
}

thLDumpAmp(l, IOTOX);
thLDumpPar(l, IOTOX);
thLDumpPar(l, IOTOXN);

thLDumpAmp(l, IOTOMODEL);
thLDumpPar(l, IOTOMODEL);
thLDumpPar(l, IOTOMODEL);

#endif
#endif

printf("%s: proceeding to the rest of the objects (non-sky)... \n", name);
phobjcs = frame->data->phobjc;
cwphobjcs = frame->data->cwphobjc;
cphobjcs = frame->data->cphobjc;
CHAIN *phprops = frame->data->phprop;

shAssert(shChainSize(phobjcs) == shChainSize(cphobjcs));
shAssert(shChainSize(phobjcs) == shChainSize(cwphobjcs));
shAssert(shChainSize(phprops) == shChainSize(phobjcs));
nobjc = shChainSize(phobjcs);
printf("%s: (%d) objects read from SDSS file \n", name, nobjc);

/* introducing a galaxy */

/* 
printf("%s: simulating objects as '%s' \n", name, sim_name);
*/
/* sorting should not be done because it messes with the order of calib vs. non-calib photo objects */
/* 
printf("%s: sorting objects \n", name);
shChainQsort(phobjcs, &compare_re);	
test_sorted_chain(phobjcs, &compare_re);
*/
int cd_force_classify = 0;
if (sim_on_demand) {
	printf("%s: over-riding SDSS list on demand (code = %d) \n", name, demand_code);
	printf("%s: sorting the list made on demand to late force cD classification \n", name);
	shChainQsort(myobjclist, &compare_count);
	printf("%s: preparing object list for simulation on demand \n", name);
	int demand_nobjc = shChainSize(myobjclist);
	PHCALIB *calibData = frame->data->phcalib;

	CHAIN *demand_phprop_list = shChainNew("PHPROPS");
	
	for (iobjc = 0; iobjc < demand_nobjc; iobjc++) {
		WOBJC_IO *demand_objc = shChainElementGetByPos(myobjclist, iobjc);
		status = output_phobjc_checklist(name, demand_objc);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not output (PHOTO) properties of object (%d)",
			name, iobjc);
		}
		PHPROPS *demand_phprop = thPhpropsNew();
		CALIB_WOBJC_IO *cwphobjc = NULL;
		CALIB_PHOBJC_IO *cphobjc = NULL;
		status = thPhpropsPut(demand_phprop, demand_objc, bndex, cwphobjc, cphobjc, calibData);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not make (phprop) from object data on demand inside for (iobjc = %d)", name, iobjc);
			return(status);
		}
		shChainElementAddByPos(demand_phprop_list, demand_phprop, "PHPROPS", TAIL, AFTER);
	}
	nobjc = shChainSize(demand_phprop_list);
	phprops = demand_phprop_list;
	printf("%s: made a list of (%d) objects based on demand (code = %d) - replaced with the original list \n", name, nobjc, demand_code);
	cd_force_classify = 1;
}


printf("%s: getting crude-calib from frame \n", name);
CRUDE_CALIB *cc = NULL;
status = thFrameGetCrudeCalib(frame, &cc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get 'crude_calib' from (frame)", name);
	return(status);
	}

printf("%s: looping over phprops chain \n", name);
int ndeleted = 0;
for (iobjc = 0; iobjc < nobjc; iobjc++) {

	/* initiating the object */
	#if 0
	WOBJC_IO *phobjc = shChainElementGetByPos(phobjcs, iobjc);
	CALIB_WOBJC_IO *cwphobjc = shChainElementGetByPos(cwphobjcs, iobjc);
	CALIB_PHOBJC_IO *cphobjc = shChainElementGetByPos(cphobjcs, iobjc);
	shAssert((int) cphobjc->id == (int) phobjc->id);
	shAssert((int) cwphobjc->id == (int) phobjc->id);

	PHPROPS *phprop = phPropsNew();
	PHCALIB *calibData;
	calibData = frame->data->phcalib;
	status = thPhpropsPut(phprop, phobjc, bndex, cwphobjc, cphobjc, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put object information in 'PHPROP'", name);
		return(status);
	}
	#else
	PHPROPS *phprop = shChainElementGetByPos(phprops, iobjc);
	status = thPhpropsGet(phprop, &phobjc, NULL, NULL, NULL, NULL);
	shAssert(status == SH_SUCCESS);
	#endif
	if (cd_force_classify && sim_on_demand && iobjc == 0) {	
		objc = thObjcNewFromPhprop(phprop, band, FORCECLASSIFY, "CDCANDIDATE");
	} else {
		objc = thObjcNewFromPhprop(phprop, band, CLASSIFY, NULL);
	}
	/* 
	status = output_objc_checklist(name, objc);
	*/
	if (objc == NULL) {
		thError("%s: ERROR - could not create object for object (%d) in the sdss list", name, iobjc);
		return(status);
	}
	objc->thid = (THOBJCID) phobjc->id;
	THOBJCTYPE objctype;
	char *objcname;
	objctype = objc->thobjctype;
	status = thObjcNameGetFromType(objctype, &objcname);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get type name from object type (iobjc = %d)", name, iobjc);
		return(status);
	}
	if (objctype != SKY_OBJC && objctype != PHOTO_OBJC) {
		/* before creating the ioprop make sure to make cD halo */
		if (!sim_on_demand && do_cD_classify && (init == 1 || frame_init == NULL) && !strcmp(objcname, "CDCANDIDATE")) {
			if (cdclass == DEVDEV_GCLASS) {
				status = thWObjcIoMakeRandomDeV2(phobjc);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not make a random cD halo around the canadidate (id: %d)", 
					name, phobjc->id);
					return(status);
				}
			} else if (cdclass == DEVPL_GCLASS) {
				status = thWObjcIoMakeRandomPl(phobjc);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not make a random cD halo around the canadidate (id: %d)", 
					name, phobjc->id);
					return(status);
				}
			} else {
				thError("%s: WARNING - this cD class ('%s') does not support a random halo", 
				name, shEnumNameGetFromValue("GCLASS", cdclass));
			}
		} 		
		/* one should create the ioprop of the object after it is properly classified */
		char *tname = NULL;
		thObjcNameGetFromType(objc->thobjctype, &tname);
 		status = thObjcInitMpars(objc);
		/* 
		status = output_objc_checklist(name, objc);
		*/
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not initiate model parameters for (objc: %d) of class '%s'", name, objc->thid, tname);
			return(status);
		}
		status = thObjcTransIo(objc, MODELTOIO); 
		/* 
		status = output_objc_checklist(name, objc);
		*/
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not translate model parameter to io parameter in (objc: %d) of class '%s'", name, objc->thid, tname);
		}
 		status = thObjcDumpCounts(objc);
		/* 
		status = output_objc_checklist(name, objc);
		*/
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not dump counts for (objc: %d) fo class '%s'", name, objc->thid, tname);
			return(status);
		}
		status = thObjcCrudeCalib(cc, objc);
		/* 
		status = output_objc_checklist(name, objc);
		*/
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not calibrate (objc: %d) of class '%s'", name, objc->thid, tname);
			return(status);
		}
		/* 
		status = output_objc_checklist(name, objc);
		*/
		shChainElementAddByPos(objclist, objc, "THOBJC", TAIL, AFTER);		
		/* thMAddObjcAllModels(map, objc, band, SEPARATE); */
		nsimobjc++;
		if (nsimobjc == NSIMOBJC) break;
		
	} else if (objctype == PHOTO_OBJC) {
		#if 0
		printf("%s: deleting unclassified object ... \n", name);
		#endif
		ndeleted++; 
		thObjcDel(objc);
	}	
}

printf("%s: (%d) objects were deleted due to lack of classification \n", name, ndeleted);

/* 
nobjc = shChainSize(objclist);
for (iobjc = 0; iobjc < nobjc; iobjc++) {
	objc = shChainElementGetByPos(objclist, iobjc);
	status = output_objc_checklist(name, objc);
}
*/
CHAIN *robjclist = NULL;
int nstar = input->nstar;
int ngalaxy = input->ngalaxy;
int nlrg = input->nlrg;
if (sim_on_demand) {
	printf("%s: simulation on demand (code = %d), adjust number of objects to simulated all objects \n", name, demand_code);
	nstar = MAX_SIM_NSTAR; 
	ngalaxy = MAX_SIM_NGALAXY;
	nlrg = -1;
	input->nstar = nstar;
	input->nlrg = nlrg;
	input->ngalaxy = ngalaxy;
}	

/* 
nobjc = shChainSize(objclist);
for (iobjc = 0; iobjc < nobjc; iobjc++) {
	objc = shChainElementGetByPos(objclist, iobjc);
	status = output_objc_checklist(name, objc);
}
*/

printf("%s: creating random list of (%d) stars, (%d) galaxies and (%d) LRGs \n", name, nstar, ngalaxy, nlrg);
static int *chosen_list = NULL;
int take_from_list = 0;
if (init == 1) {
	take_from_list = 0;
	chosen_list = thCalloc(nobjc, sizeof(int));
	} else {
	take_from_list = 1;
}

THPIX star_threshold, galaxy_threshold;
star_threshold = (THPIX) (input->star_threshold);
galaxy_threshold = (THPIX) (input->galaxy_threshold);

/* 
nobjc = shChainSize(objclist);
for (iobjc = 0; iobjc < nobjc; iobjc++) {
	objc = shChainElementGetByPos(objclist, iobjc);
	status = output_objc_checklist(name, objc);
}
*/

printf("%s: selecting random starts and galaxies \n", name);
robjclist = select_random_stars_galaxies_from_list(objclist, input, chosen_list, take_from_list);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not choose a random sample of (%d) stars, (%d) galaxies and (%d) LRGs from the list", name, nstar, ngalaxy, nlrg);
	return(status);
}	
if (init == -1) {
	thFree(chosen_list);
	chosen_list = NULL;
}

nsimobjc=0;
ndeleted=0;
nobjc = shChainSize(robjclist);
for (iobjc = 0; iobjc < nobjc; iobjc++) {
	objc = shChainElementGetByPos(robjclist, iobjc);
	THOBJCTYPE objctype;
	char *objcname;
	objctype = objc->thobjctype;
	status = thObjcNameGetFromType(objctype, &objcname);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get type name from object type (iobjc = %d)", name, iobjc);
		return(status);
	}
	if (objctype != SKY_OBJC && objctype != PHOTO_OBJC) {
		thMAddObjcAllModels(map, objc, band, SEPARATE);
		nsimobjc++;
		if (nsimobjc == NSIMOBJC) break;
		
	} else if (objctype == PHOTO_OBJC) {
		#if 0
		printf("%s: deleting unclassified object ... \n", name);
		#endif
		ndeleted++; 
	}	
}

printf("%s: (%d) objects added to the simulation from the random list \n", name, nsimobjc);
printf("%s: (%d) objects deleted from the random list due to lack of clear classification \n", name, ndeleted);


printf("%s: compiling map for simulation... \n", name);
thMCompile(map);
printf("%s: outputting (mapmachine) info \n", name); 
/* 
thMInfoPrint(map, EXTENSIVE);
*/
printf("%s: n(model) = %d, n(par) = %d, n(objc) = %d \n", name, map->namp, map->npar, map->nobjc);
nobjc = shChainSize(robjclist);
printf("%s: total of (%d) objects added to (mapmachine) \n", name, nobjc);
if (nobjc == 0) {
	thError("%s: WARNING - no objects loaded from SDSS", name);
	/* 
	return(SH_GENERIC_ERROR);
	*/
}

printf("%s: initiating model galaxy profiles \n", name);
status = MGInitProfile(DEFAULT_ELL_DESC);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not initiate profile package", name);
	return(status);
}

int fitflag = MFIT_RE | MFIT_E | MFIT_PHI | MFIT_CENTER | MFIT_LOCK_CENTER;
char **target_classes = thCalloc(1, sizeof(char *));
target_classes[0] = "CDCANDIDATE"; 
int n_target_classes = 1;
status = thMAddObjcChainToParFit(map, robjclist, fitflag, target_classes, n_target_classes, HALO_AND_CORE, NULL);  
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add objects to the (mapmachine)", name);
	return(status);
}
/* compiling map */	
printf("%s: compiling (mapmachine) \n", name);
thMCompile(map);
 
printf("%s: outputting (mapmachine) info \n", name); 
/* 
thMInfoPrint(map, EXTENSIVE);
*/
printf("%s: n(model) = %d, n(par) = %d, n(objc) = %d \n", name, map->namp, map->npar, map->nobjc);
/* constructing LSTRUCT */
char cband = '\0';
status = thNameGetFromBand(band, &cband);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get band name for (band = %d)", name, band);
	return(status);
}
LFITRECORDTYPE rtype = LM_RECORD_ALL;
if (cband == 'r') rtype = LFIT_RECORD_ALL;

printf("%s: constructing likelihood data structure (lstruct) \n", name);
status = thFrameLoadLstruct(frame, NULL, DEFAULT_CALIBTYPE, rtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not load (lstruct)", name);
	return(status);
}
status = thFrameGetLstruct(frame, &l);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lstruct) out of (frame)", name);
	return(status);
}
status = thLstructPutLmachine(l, map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmachine) out of (lstruct)", name);
	return(status);
}
status = thLstructGetLwork(l, &lwork);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwork) out of (lstruct)", name);
	return(status);
}
	
/* simulating the galaxy */ 
pos++;	
printf("%s: assigining parameters and amplitude arrays in (mapmachine) from SDSS model parameters\n", 
name);
	  
thLDumpAmp(l, MODELTOIO); 
thLDumpPar(l, MODELTOIO);
 
thLDumpAmp(l, MODELTOX);
thLDumpPar(l, MODELTOX);
thLDumpPar(l, MODELTOXN);

/* showing the statistics */
/* 
chain_model_output("sim parameters: ", robjclist, UNKNOWN_OBJC);
*/

/* calculation needs proper initiation of quadrature methods and elliptical description */
status = MGInitProfile(EllDesc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not initiate profile package", name);
	return(-1);
}

/* creating the simulated / fake image */
printf("%s: simulating the frame: \n", name);
printf("%s: * memory estimates \n", name);
status = MakeImageAndMatrices(l, MEMORY_ESTIMATE);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not estimate the memory for the model elements", name);
	return(status);
}

printf("%s: * algorithm design \n", name);
ADJ_MATRIX *adj = thAdjMatrixNew();
status = thCreateAdjacencyMatrix(adj, l);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not create the adjacency matrix for the current likelihood structure", name);
	return(status);
}
ALGORITHM *alg_cr = thAlgorithmNew();
alg_cr->memory_total = (MEMDBLE) THMEMORY * (MEMDBLE) MEGABYTE;	
status = design_algorithm(adj, alg_cr, CREATE_MODEL, INITRUN);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not design the algorithm", name);
	return(status);
} 
output_algorithm(alg_cr, adj);

printf("%s: * algorithm run \n", name);
status = thAlgorithmRun(alg_cr, l);	
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run the algorithm", name);
	return(status);
}

printf("%s: obtaining header from frame \n", name);
status = thFrameUpdateHdr(frame);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make proper (hdr) from (frame) information", name);
	return(status);
}
HDR *hdr = NULL;
status = thFrameGetHdr(frame, &hdr);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get proper (hdr) from (frame)", name);
	return(status);
}
if (hdr == NULL) {
	thError("%s: WARNING - null (hdr) found in (frame)", name);
}
printf("%s: * copying simulated image onto data \n", name);	
REGION *im = frame->data->image;
REGION *im_continuous = shRegNew("continuous simulated image", im->nrow, im->ncol, TYPE_FL32);  
thRegPixCopy(l->lwork->mN, im);
thRegPixCopy(l->lwork->mN, im_continuous);
REGION *outreg = NULL;

if (fpC_do_float) {
	outreg = shRegNew("output region in FL32", im->nrow, im->ncol, TYPE_FL32);
	shHdrCopy(hdr, &outreg->hdr);
} else {
	outreg = shRegNew("output region in U16", im->nrow, im->ncol, TYPE_U16);
	shHdrCopy(hdr, &outreg->hdr);
}

CHISQFL chisq_int = (CHISQFL) 0.0, chisq_float = (CHISQFL) 0.0;
printf("%s: simulating poisson noise \n", name);
status = add_poisson_noise_to_image(im, frame->data->ampl, gaussian_noise_dn, &chisq_float, &chisq_int);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add poisson noise to (image) in (lstruct)", name);
	return(status);
}

MLEDBLE degree_freedom = (MLEDBLE) (im->nrow * im->ncol); 
MLEDBLE reduced_chisq_int = (MLEDBLE) chisq_int / degree_freedom;
MLEDBLE reduced_chisq_float = (MLEDBLE) chisq_float / degree_freedom;
MLEDBLE chisq_std_dev = sqrt(2.0 / degree_freedom);
MLEDBLE z_int = (reduced_chisq_int - (MLEDBLE) 1.0) / chisq_std_dev;
MLEDBLE z_float = (reduced_chisq_int - (MLEDBLE) 1.0) / chisq_std_dev;

printf("%s: integer simulation chisq calculated as (chisq = %20.10G, reduced_chisq = %20.10G) \n", name, (double) chisq_int, (double) reduced_chisq_int);
printf("%s: float   simulation chisq calculated as (chisq = %20.10G, reduced_chisq = %20.10G) \n", name, (double) chisq_float, (double) reduced_chisq_float);
printf("%s: degree of freedom (= %d), reduced chisq std dev = (%20.10G) \n", name, (int) degree_freedom, (double) chisq_std_dev);
printf("%s: z - value for reduced chisq: integer (z = %20.10G), float (z = %20.10G) \n", name, (double) z_int, (double) z_float);

FRAMEFILES *ff = frame->files;
char *simfile = ff->phflatframefile;
char *simodelfile = ff->thflatframefile;
printf("%s: outputting continuous model onto file '%s' with proper header \n", name, simodelfile);


status = add_frame_stat_to_hdr(frame->stat, input, chisq_int, chisq_float, gaussian_noise_dn, hdr);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add (frame stat) and (chisq) to (hdr)", name);
	return(status);
}

#if FPCC_INT
printf("%s: saving continous model in ineteger \n", name);
shAssert(shRegIntCopy(outreg, im_continuous) == SH_SUCCESS);
shHdrCopy(hdr, &outreg->hdr); /* adding header so that MLE finds pertinent information in the header of fpCC */
shRegWriteAsFits(outreg, simodelfile, STANDARD, 2, DEF_NONE, NULL, 0);
#else 
printf("%s: saving continous model in float \n", name);
shHdrCopy(hdr, &im_continuous->hdr); /* adding header so that MLE finds pertinent information in the header of fpCC */
shRegWriteAsFits(im_continuous, simodelfile, STANDARD, 2, DEF_NONE, NULL, 0);
#endif


printf("%s: outputting poisson grained model onto file '%s' with proper header \n", name, simfile);
HDR *hdr2;
if (fpC_do_float) {
	printf("%s: saving poisson grained model in float \n", name);
	shRegPixCopy(im, outreg);
	shHdrCopy(hdr, &(outreg->hdr)); /* adding header so that MLE finds pertinent information in the header of fpC */
	hdr2 = &(outreg->hdr);
	shHdrDelByKeyword(hdr2, "SOFTBIAS");
	shHdrDelByKeyword(hdr2, "SOFTBIAS");
} else {
	printf("%s: saving poisson grained model in integer \n", name);
	shAssert(shRegIntCopy(outreg, im) == SH_SUCCESS);
	shHdrCopy(hdr, &(outreg->hdr)); /* adding header so that MLE finds pertinent information in the header of fpC */
	HDR *hdr2 = &(outreg->hdr);
	int softbias = 0;
	if (shHdrGetInt(hdr2, "SOFTBIAS", &softbias) == SH_GENERIC_ERROR) {
		printf("%s: adding keyword 'SOFTBIAS' (= %d) to header \n", name, (int) SOFT_BIAS);
		status = shHdrInsertInt(hdr2, "SOFTBIAS", (int) SOFT_BIAS, "software bias added to all DN");
	} else if (softbias != (int) SOFT_BIAS) {
		printf("%s: keyword 'SOFTBIAS' found in header with the wrong value (= %d) expected %d \n", name, softbias, (int) SOFT_BIAS);
		printf("%s: replacing keyword 'SOFTBIAS' (= %d) in header \n", name, (int) SOFT_BIAS);
		shHdrDelByKeyword(hdr2, "SOFTBIAS");	
		shHdrInsertInt(hdr2, "SOFTBIAS", (int) SOFT_BIAS, "software bias added to all DN");
	}
}
shRegWriteAsFits(outreg, simfile, STANDARD, 2, DEF_NONE, NULL, 0);

LMETHOD *lmethod = NULL;
status = thLstructGetLmethod(l, &lmethod);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmethod) from (lstruct)", name);
	return(status);
}
printf("%s: adding (chisq_int = %20.10G, chisq_float = %20.10G) to (lmethod) \n", name, (double) chisq_int, (double) chisq_float);

status = thLmethodPutChisq(lmethod, (CHISQFL) chisq_int, (CHISQFL) chisq_float);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not put (chisq) value in (lmethod)", name);
	return(status);
}
printf("%s: updating step status in (lmethod) to indicate simulation \n", name);

status = thLmethodPutStatus(lmethod, LM_STEP_SIMULATED);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not put (status) to indicate (simulation) in (lmethod)", name);
	return(status);
}

if (cband == 'r') {
	printf("%s: updating (lmethod) and (lprofile) for (band = %d, cband = '%c') with the 1D light profile of the cD object(s) \n", name, band, cband);
} else {
	printf("%s: updating (lmethod) but skipping (lprofile) for (band = %d, cband = '%c') to speed up the run \n", name, band, cband);
}	
status = thLRecordLfit(l);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add (record) to (lfit)", name);
	return(status);
}

printf("%s: outputting simulation parameters onto file '%s' \n", name, ff->thobjcfile);

printf("%s: initilizing dump \n", name);
	status = thFrameInitIo(frame);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not init dumpers for this frame", name);
		return(status);
	}
printf("%s: finalizing frame dumpers \n", name);
status = thFrameFiniIo(frame);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not finalize dumpers for frame", name);
	return(status);
}
printf("%s: dumping log, sdss pars, init pars and fit findings\n", name);
status = thFrameDumpIo(frame);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not dump frame information", name);
	return(status);
}

printf("%s: end of simulation \n", name);

return(SH_SUCCESS);


/* re-creating the poisson weights */
printf("%s: loading poisson weights \n", name);
thFrameLoadWeight(frame);
printf("%s: loading SOHO masks \n", name);
thFrameLoadThMask(frame);
	
printf("%s: number of objects = %d \n", name, shChainSize(robjclist));
#if TWEAK
printf("%s: tweaking parameters to get initial guess ... \n", name);
status = tweak_init_pars(l, robjclist, band);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the initial guess by tweaking", name);
	return(status);
}
#endif
/* outputting the tweaked parameters */
chain_model_output("initial guess parameters (tweaked): ", robjclist, UNKNOWN_OBJC);

clock_t clk1, clk2;
time_t time1, time2, dif;
printf("%s: * algorithm \n", name);
MEMDBLE memory = (MEMDBLE) THMEMORY * MEGABYTE;	
printf("%s: i. l compilation (automatic) \n", name);	
clk1 = clock();
status = thLCompile(l, memory, SIMFINALFIT);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not compile (l)", name);
	return(status);
}
clk2 = clock();
printf("%s: compile time: %g sec \n", name, ((float) (clk2 - clk1)) / (float) CLOCKS_PER_SEC);
printf("%s: ii.a. l run (manual) - skipping \n", name);
printf("%s: ii.b. l run (automatic) \n", name);
time(&time1);
status = thLRun(l);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run (l)", name);
	return(status);
}
time(&time2);
dif = difftime(time2, time1);	
printf("%s: run time: %g sec \n", name, (float) dif);

printf("%s: successful end of simulation\n", name);
return(status);


printf("%s: copying simulated image onto a new region \n", name);
simreg = l->lwork->mN;
thRegPixCopy(simreg, im);

/* adding poisson noise to the image */
printf("%s: simulating poisson noise \n", name);
status = add_poisson_noise_to_image(im, frame->data->ampl, gaussian_noise_dn, &chisq_float, &chisq_int);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add poisson noise to (image) in (lstruct)", name);
	return(status);
}
/* dumping created model into an image file */
printf("%s: outputting the simulated image onto '%s' \n", name, simfile);
shRegWriteAsFits(im, simfile, STANDARD, 2, DEF_NONE, NULL, 0);

/* doing residual reports */
REGION *diff;
diff = shRegNew("residual image", simreg->nrow, simreg->ncol, simreg->type);
thPutModelComponent(diff, simreg, 1.0); /* sim reg is the product of simulation */
thAddModelComponent(diff, im, -1.0); /* im is the data  */	

int rbin1 = 1, cbin1 = 1, rbin2 = 20, cbin2 = 20;
printf("%s: iv. residual reports \n", name);
printf("%s: iv.a. creating report for (rbin, cbin) = (%d, %d) \n", name, rbin1, cbin1);
RESIDUAL_REPORT *res = NULL;
status = thCreateResidualReport(diff, NULL, rbin1, cbin1, &res);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not create residual report", name);
	return(status);
}
printf("%s: iv.a. compiling report \n", name);
status = thCompileResidualReport(res);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not compile residual report", name);
	return(status);
}
printf("%s: iv.b. creating chain of reports (rbin = %d, %d), (cbin = %d, %d) \n", name, rbin1, rbin2, cbin1, cbin2);
CHAIN *reschain;
reschain = shChainNew("RESIDUAL_REPORT");
status = thCreateResidualReportChain(diff, NULL, rbin1, rbin2, cbin1, cbin2, reschain);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not create residual report chain", name);
	return(status);
}
printf("%s: iv.b. compiling residual report chain \n", name);
status = thCompileResidualReportChain(reschain);	
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not compile residual report chain", name);
	return(status);
}

status = output_residual_report_chain(reschain);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not output (residual report) chain", name);
	return(-1);
}

return(SH_SUCCESS);
}


RET_CODE add_frame_stat_to_hdr(FRAMESTAT *stat, SIM_PARSED_INPUT *input, CHISQFL chisq_int, CHISQFL chisq_float, THPIX gaussian_noise_dn, HDR *hdr) {
char *name = "add_frame_stat_to_hdr";
shAssert(stat != NULL);
shAssert(hdr != NULL);
shAssert(input != NULL);

double median, mean, iqr, s, s00;
median = (double) stat->phstat[OBJ_STAR]->sky->median;
mean = (double) stat->phstat[OBJ_STAR]->sky->mean;
iqr = (double) stat->phstat[OBJ_STAR]->sky->iqr;
s = (double) stat->phstat[OBJ_STAR]->sky->s;
s00 = (double) input->sky->I_s00;

double chisq_save, chisq_save2;
chisq_save = (double) chisq_int;
chisq_save2 = (double) chisq_float;

printf("%s: Sky from stars (median = %g, iqr = %g, mean = %g, s = %g) \n", name, (float) median, (float) iqr, (float) mean, (float) s);
printf("%s: Sky used in simulation (s00 = %g) \n", name, (float) s00);
printf("%s: Chisq obtained for simulation (chisq  = %20.10G), (chisq_float = %20.10G)  \n", name, (double) chisq_save, (double) chisq_save2);
#if GAUSSIAN_NOISE && CONSTANT_GAUSSIAN_NOISE 
printf("%s: gaussian noise amplitude = %20.10G \n", name, (double) gaussian_noise_dn);
#endif

shHdrDelByKeyword(hdr, "SKYMED");
shHdrDelByKeyword(hdr, "SKYMEAN");
shHdrDelByKeyword(hdr, "SKYIQR");
shHdrDelByKeyword(hdr, "SKYSTDEV");
shHdrDelByKeyword(hdr, "SKYFPCC");

shHdrInsertDbl(hdr, "SKYMED", median, "MEDIAN of sky at location of stars");
shHdrInsertDbl(hdr, "SKYMEAN", mean, "MEAN of sky at location of stars");
shHdrInsertDbl(hdr, "SKYIQR", iqr, "IQR of sky at location of stars");
shHdrInsertDbl(hdr, "SKYSTDEV", s, "STD DEV of sky at location of stars");
shHdrInsertDbl(hdr, "SKYFPCC", s00, "Sky value used in simulation");

shHdrDelByKeyword(hdr, "CHISQ");
shHdrInsertDbl(hdr, "CHISQ", (double) chisq_save, "CHISQ for simulation (int)");
shHdrDelByKeyword(hdr, "CHISQFL");
shHdrInsertDbl(hdr, "CHISQFL", (double) chisq_save2, "CHISQ for simulation (float)");
#if GAUSSIAN_NOISE && CONSTANT_GAUSSIAN_NOISE
shHdrDelByKeyword(hdr, "GAUSSDN");
shHdrInsertDbl(hdr, "GAUSSDN", (double) gaussian_noise_dn, "Guassian noise for simulation");
#endif

return(SH_SUCCESS);
}

CHAIN *select_random_stars_galaxies_from_list(CHAIN *objclist, SIM_PARSED_INPUT *input, 
int *chosen_list, int take_from_list) { 

char *name = "select_random_stars_galaxies_from_list";
shAssert(input != NULL);
int mstar = input->nstar;
int mgalaxy = input->ngalaxy;
int mlrg = input->nlrg;

shAssert(objclist != NULL);
shAssert(mstar >= 0);
shAssert(mgalaxy >= 0);
shAssert(chosen_list != NULL);
shAssert(take_from_list == 0 || take_from_list == 1);

float star_threshold = input->star_threshold;
float galaxy_threshold = input->galaxy_threshold;
int dolrg = 0;
if (mlrg < 0) {
	mlrg = 0;
	dolrg = 0;
	printf("%s: not testing for LRG \n", name);
} else {
	printf("%s: will test for LRG \n", name);
	dolrg = 1;
	}

CHAIN *rlist = shChainNew("THOBJC");
if (shChainSize(objclist) == 0) {
	thError("%s: WARNING - empty object list passed", name);
	return(rlist);
}

int nstar = 0;
int ngalaxy = 0;
int nlrg = 0;
int nobjc = shChainSize(objclist);
int iobjc;

printf("%s: number of objects passed for random selection (nobjc = %d) \n", name, nobjc);

if (take_from_list == 0) {

#define UNKNOWN_CODE -1
#define STAR_CODE 0
#define GALAXY_CODE 1
#define LRG_CODE 2

	int *object_codes = NULL;
	if (nobjc > 0) object_codes = thCalloc(nobjc, sizeof(int));
	for (iobjc = 0; iobjc < nobjc; iobjc++) {
	
		THOBJC *objc = shChainElementGetByPos(objclist, iobjc);
		/* 
		output_objc_checklist(name, objc);
		*/
		switch (objc->sdsstype) {
		case OBJ_STAR: 
			nstar++; 
			object_codes[iobjc] = STAR_CODE;
			break;
		case OBJ_GALAXY:
			if (dolrg == 1) {
				PHPROPS *phprops = objc->phprop;
				int is_LRG_flag = is_LRG(phprops);
				if (is_LRG_flag & LRG_STATUS_UNKNOWN) {
					thError("%s: ERROR - 'LRG_STATUS_UNKNOWN' reached for (objc = %p)", name, (void *) objc);
					return(NULL);
				} else if (is_LRG(phprops) > 0) {
					object_codes[iobjc] = LRG_CODE;
					nlrg++;
				} else {
					object_codes[iobjc] = GALAXY_CODE;
					ngalaxy++;
				}
			} else { 
				object_codes[iobjc] = GALAXY_CODE;
				ngalaxy++; 
			}
			break;
		default:
			object_codes[iobjc] = UNKNOWN_CODE; 
			break;
		} 
	}

	printf("%s: total of (%d) stars found in the object sample \n", name, nstar);
	printf("%s: total of (%d) galaxies found in the object sample \n", name, ngalaxy);
	printf("%s: total of (%d) LRGs found in the object sample \n", name, nlrg);

	int *sndex = NULL, *gndex = NULL, *lrgndex = NULL;
	int *sdeleted = NULL, *gdeleted = NULL, *lrgdeleted = NULL;

	if (nstar > 0) {
		sndex = thCalloc(nstar, sizeof(int));
		sdeleted = thCalloc(nstar, sizeof(int));
	}
	if (ngalaxy > 0) {
		 gndex = thCalloc(ngalaxy, sizeof(int));
		 gdeleted = thCalloc(ngalaxy, sizeof(int));
	}	
	if (nlrg > 0) {
		lrgndex = thCalloc(nlrg, sizeof(int));
		lrgdeleted = thCalloc(nlrg, sizeof(int));
	}
	int istar = 0, igal = 0, ilrg = 0;
	for (iobjc = 0; iobjc < nobjc; iobjc++) {
		int code = object_codes[iobjc];
		if (code == STAR_CODE) {
			sndex[istar] = iobjc;
			istar++;
		} else if (code == GALAXY_CODE) {
			gndex[igal] = iobjc;
			igal++;
		} else if (code == LRG_CODE) {
			lrgndex[ilrg] = iobjc;
			ilrg++;
		} else {
			shAssert(code == UNKNOWN_CODE);
		}
	}

	shAssert(istar == nstar);
	shAssert(igal == ngalaxy);
	shAssert(ilrg == nlrg);

#undef UNKNOWN_CODE
#undef STAR_CODE
#undef GALAXY_CODE
#undef LRG_CODE 

	int npdeleted = 0;
	int ndeleted=0;
	printf("%s: star deletion list to be generated to match magnitude thresholds \n", name);
	for (istar = 0; istar < nstar; istar++) sdeleted[istar] = -1;	
	for (istar = 0; istar < nstar; istar++) {	
		iobjc = sndex[istar];	
		THOBJC *objc = shChainElementGetByPos(objclist, iobjc);
	/* 
		output_objc_checklist(name, objc);
	*/
		if (objc_pass_threshold(objc, input) == 0) {
			sndex[istar] = -1;
			sdeleted[ndeleted] = istar;
			ndeleted++;
		}
	}
	printf("%s: (%d) stars added to the deleted list to match magnitude threshold (%g) \n", 
		name, ndeleted, star_threshold);

	printf("%s: star deletion list to be appended to match requested sample size \n", name);
	/* number of passed stars */
	int npstar = nstar - ndeleted;
	int dpstar = npstar - mstar;
	npdeleted = 0;
	while (npdeleted < dpstar) {
		int ideleted;
		ideleted = ((int) (phRandomUniformdev() * (nstar * 2 + 3) * (nstar * 2 + 3)))%nstar;
		if (sndex[ideleted] != -1) {
			sndex[ideleted] = -1;
			sdeleted[ndeleted] = ideleted;
			ndeleted++;
			npdeleted++;
		}
	}

	printf("%s: (%d) stars randomly added to the deleted list \n", name, npdeleted);
	printf("%s: total of (%d) out of (%d) stars added to the deleted list \n", name, ndeleted, nstar);

	ndeleted = 0;
	printf("%s: galaxy deletion list to be generated to match magnitude thresholds \n", name);
	for (igal = 0; igal < ngalaxy; igal++) gdeleted[igal] = -1;		
	for (igal = 0; igal < ngalaxy; igal++) {
		iobjc = gndex[igal];
		THOBJC *objc = shChainElementGetByPos(objclist, iobjc);
	/*	output_objc_checklist(name, objc);
	*/
		if (objc_pass_threshold(objc, input) == 0) {
			gndex[igal] = -1;
			gdeleted[ndeleted] = igal;
			ndeleted++;
		}
	}
	printf("%s: (%d) galaxies added to the deleted list to match magnitude threshold (%g) \n", 
		name, ndeleted, galaxy_threshold);
	printf("%s: galaxy deletion list to be appended to match requested sample size \n", name);
	/* number of passed stars */
	int npgalaxy = ngalaxy - ndeleted;
	int dpgalaxy = npgalaxy - mgalaxy;
	npdeleted = 0;

	printf("%s: remaining galaxies (%d) while looking for (%d) galaxies \n", name, npgalaxy, mgalaxy);
	printf("%s: preceeding to delete (%d) galaxies randomly \n", name, dpgalaxy);

	while (npdeleted < dpgalaxy) {
		int ideleted;
		ideleted = ((int) (phRandomUniformdev() * (ngalaxy * 2 + 3) * (ngalaxy * 2 + 3)))%ngalaxy;
		if (gndex[ideleted] != -1) {
			gndex[ideleted] = -1;
			gdeleted[ndeleted] = ideleted;
			ndeleted++;
			npdeleted++;
		}
	}
	
	printf("%s: (%d) galaxies randomly added to the deleted list \n", name, npdeleted);
	printf("%s: total of (%d) out of (%d) galaxies added to the deleted list \n", name, ndeleted, ngalaxy);



	ndeleted = 0;
	printf("%s: LRG deletion list to be generated to match magnitude thresholds \n", name);
	for (ilrg = 0; ilrg < nlrg; ilrg++) lrgdeleted[ilrg] = -1;		
	for (ilrg = 0; ilrg < nlrg; ilrg++) {
		iobjc = lrgndex[ilrg];
		THOBJC *objc = shChainElementGetByPos(objclist, iobjc);
	/* 
		output_objc_checklist(name, objc);
	*/
		if (objc_pass_threshold(objc, input) == 0) {
			lrgndex[ilrg] = -1;
			lrgdeleted[ndeleted] = ilrg;
			ndeleted++;
		}
	}
	printf("%s: (%d) LRGs added to the deleted list to match magnitude threshold (%g) \n", 
		name, ndeleted, galaxy_threshold);
	printf("%s: LRG deletion list to be appended to match requested sample size \n", name);
	/* number of passed stars */
	int nplrg = nlrg - ndeleted;
	int dplrg = nplrg - mlrg;
	npdeleted = 0;

	while (npdeleted < dplrg) {
		int ideleted;
		ideleted = ((int) (phRandomUniformdev() * (nlrg * 2 + 3) * (nlrg * 2 + 3)))%nlrg;
		if (lrgndex[ideleted] != -1) {
			lrgndex[ideleted] = -1;
			lrgdeleted[ndeleted] = ideleted;
			ndeleted++;
			npdeleted++;
		}
	}

	printf("%s: (%d) LRGs randomly added to the deleted list \n", name, npdeleted);
	printf("%s: total of (%d) out of (%d) LRGs added to the deleted list \n", name, ndeleted, nlrg);


	int nastar = 0, nagalaxy = 0, nalrg = 0;
	int jobjc;
	for (iobjc = 0; iobjc < nobjc; iobjc++) chosen_list[iobjc] = -1;
	jobjc = 0;
	for (istar = 0; istar < nstar; istar++) {
		iobjc = sndex[istar];
		if (iobjc != -1) {
			chosen_list[jobjc] = iobjc;
			jobjc++;
			nastar++;
		}
	}
	printf("%s: a total of (%d) stars out of (%d) added to 'chosen_list' \n", name, nastar, nstar);
	for (igal = 0; igal < ngalaxy; igal++) {
		iobjc = gndex[igal];
		if (iobjc != -1) {
			chosen_list[jobjc] = iobjc;
			jobjc++;
			nagalaxy++;
		}
	}
	printf("%s: a total of (%d) out of (%d) galaxies added to 'chosen_list' \n", name, nagalaxy, ngalaxy);
	for (ilrg = 0; ilrg < nlrg; ilrg++) {
		iobjc = lrgndex[ilrg];
		if (iobjc != -1) {
			chosen_list[jobjc] = iobjc;
			jobjc++;
			nalrg++;
		}
	}	
	printf("%s: a total of (%d) out of (%d) LRGs added to 'chosen_list' \n", name, nalrg, nlrg);
	printf("%s: a total of (%d) objects added to 'chosen_list' \n", name, jobjc);

	thFree(sndex);
	thFree(gndex);
	thFree(lrgndex);
	thFree(sdeleted);
	thFree(gdeleted);
	thFree(lrgdeleted);
	thFree(object_codes);
}

int jobjc, nchosen = 0;
for (jobjc = 0; jobjc < nobjc; jobjc++) {
	iobjc = chosen_list[jobjc];
	if (iobjc != -1) {
		THOBJC *objc = shChainElementGetByPos(objclist, iobjc);
		shChainElementAddByPos(rlist, objc, "THOBJC", TAIL, AFTER);
		nchosen++;
		output_objc_checklist(name, objc);
	}
}
printf("%s: total of (%d) objects from 'chosen_list' added to random list \n", name, nchosen);


return(rlist);
}


int objc_pass_threshold(THOBJC *objc, SIM_PARSED_INPUT *input) {

int pass = 0;
shAssert(input != NULL);
shAssert(objc != NULL);
shAssert(objc->ioprop != NULL);
shAssert(objc->iotype != UNKNOWN_SCHEMA);
shAssert(objc->sdsstype == OBJ_STAR || objc->sdsstype == OBJ_GALAXY);
TYPE type = shTypeGetFromName("OBJCPARS");
shAssert(objc->iotype == type);

PHPROPS *phprop = objc->phprop;
shAssert(phprop != NULL);
WOBJC_IO *phobjc = phprop->phObjc;
shAssert(phobjc != NULL);

int band = input->band;
float star_threshold = input->star_threshold;
float galaxy_threshold = input->galaxy_threshold;
float deV_purity_threshold = input->deV_purity_threshold;
float exp_purity_threshold = input->exp_purity_threshold;

shAssert(band >= 0 && band < NCOLOR);

if (objc->sdsstype == OBJ_STAR) {
	pass = (((OBJCPARS *)(objc->ioprop))->mag_star <= star_threshold);
} else if (objc->sdsstype == OBJ_GALAXY) {
	float frac_deV = phobjc->fracPSF[band];
	float frac_exp = 1.0 - frac_deV;
	pass = ((((OBJCPARS *)(objc->ioprop))->mag_model <= galaxy_threshold) && ((frac_deV >= deV_purity_threshold) || (frac_exp >= exp_purity_threshold)));
}
return(pass);
}


