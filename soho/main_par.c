/*
 * Read fpC images (infile), and copy its content to Outfile.
 */
/* PHOTO libraries */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ftcl.h"
#include "dervish.h"
#include "strings.h"

#include "phUtils.h"

#include "thPar.h"
#include "thDebug.h"

int verbose = 0;

static void usage(void);

static void print_ccdpars(CCDPARS *ccd);

static void print_ccdconfig(CCDCONFIG *ccd);

int
main(int ac, char *av[])
{

  char *name = "read_par";
  /* Arguments */
  char *directory, *fname;		/* input and output filenames */
  int camrow, camcol; 

  while(ac > 1 && (av[1][0] == '-' || av[1][0] == '+')) {
    switch (av[1][1]) {
    case '?':
    case 'h':
      usage();
      exit(0);
      break;
    case 'v':
      verbose++;
      break;
    default:
      shError("Unknown option %s\n",av[1]);
      break;
    }
    ac--;
    av++;
  }
  if(ac <= 4) {
    shError("You must specify a directory, a file name, a camrow and a camcol \n");
    exit(1);
  }
  directory = av[1]; fname = av[2]; 
  camrow = atoi(av[3]); camcol = atoi(av[4]);

  /* reading the par file */

  //ftclIniPar(directory, fname);

  CCDPARS *ccdpars;

  /* examining if it reads functions in ccdpars.o */
  
  CCDDEFECT *ccddefect; 

  ccddefect = phCcddefectNew();
  phCcddefectDel(ccddefect);

  ccdpars = phCcdparsNew();
  phCcdparsDel(ccdpars);

  /* read the relavant ccd parameters for camrow and camcol */
  //ccdpars = phCcdparsFetchById(camrow, camcol);

  /* printing the contents of ccdpars */

  char *fadd;
  fadd = (char *) thMalloc((2 + strlen(directory) + strlen(fname)) * sizeof(char));
  sprintf(fadd, "%s/%s", directory, fname);
  printf("%s: reading paramter file: %s \n", name, fadd);
  
  
  int nhdr = 144;
  
  PAR_FILE *pfile;
  pfile = ini_par_file(fadd, nhdr);

  CCDCONFIG *p;
  CHAIN *pchain;
  pchain = shChainNew("CCDCONFIG");
  
  
  read_stru_chain(pfile, 
		  read_CCDCONFIG, /* stru reader */ 
		  "CCDCONFIG", sizeof(CCDCONFIG),  /* stru characteristics */
		  100, pchain); 


  
  thErrShowStack();

  printf("%s: read CCDCONFIG structure \n", name);

  p =  shChainElementGetByPos(pchain, 0);

  print_ccdconfig(p);

  /* successfull run */
  return(0);
}

/*****************************************************************************/

static void
usage(void)
{
   char **line;

   static char *msg[] = {
      "Usage: read_atlas_image [options] input-file output-file r0 c0 r1 c1",
      "Your options are:",
      "       -?      This message",
      "       -b #    Set background level to #",
      "       -c #    Use colour # (0..ncolor-1; default 0)",
      "       -h      This message",
      "       -i      Print an ID string and exit",
      "       -v      Turn up verbosity (repeat flag for more chatter)",
      NULL,
   };

   for(line = msg;*line != NULL;line++) {
      fprintf(stderr,"%s\n",*line);
   }
}


static void 
print_ccdpars(CCDPARS *ccd) {

 
  printf("*** CCD Parameters: \n");

  printf("%d %s \n",  ccd->iRow,              "/*  camRow */");
  printf("%d %s \n",  ccd->iCol,              "/*  camCol */");
  printf("%d %s \n",  ccd->filter,            "/*  filter id (stoughton specific) */");
  printf("%d %s \n",  ccd->namps,             "/*  number of amps being used (1 or 2) */");
  printf("%d %s \n",  ccd->rowRef,            "/*  Middle Pixel Of Data Rows In CCD */");
  printf("%d %s \n",  ccd->colRef,            "/*  Middle Pixel Of Data Cols In CCD */");
  printf("%f %s \n",  ccd->xPos,              "/*  mm offset from center of camera */");
  printf("%f %s \n",  ccd->yPos,              "/*  mm offset from center of camera */");
  printf("%f %s \n",  ccd->rotation,          "/*  rotation of ccd in camera (degrees) */");
  printf("%d %s \n",  ccd->nrows,             "/*  number of rows in image on disk (inc bias etc)*/");
  printf("%d %s \n",  ccd->ncols,             "/*  number of cols in image on disk */");
  printf("%d %s \n",  ccd->nDataCol,          "/*  Data Columns For This CCD */");
  printf("%d %s \n",  ccd->nDataRow,          "/*  Data Rows For This CCD */");
  printf("%d %s \n",  ccd->amp0,              "/*  which amp used for left side (low) pixels */");
  printf("%d %s \n",  ccd->amp1,              "/*  which amp used for right side (high) pixels */");
  printf("%d %s \n",  ccd->sPrescan0,         "/*  starting col for left Prescan */");
  printf("%d %s \n",  ccd->nPrescan0,         "/*  number of Prescan rows for left amp */");
  printf("%d %s \n",  ccd->sPrescan1,         "/*  starting col for right Prescan */");
  printf("%d %s \n",  ccd->nPrescan1,         "/*  number for right amp */");
  printf("%d %s \n",  ccd->sPostscan0,        "/*  starting col for left Postscan */");
  printf("%d %s \n",  ccd->nPostscan0,        "/*  number of Postscan rows for left amp */");
  printf("%d %s \n",  ccd->sPostscan1,        "/*  starting col for right Postscan */");
  printf("%d %s \n",  ccd->nPostscan1,        "/*  number for right amp */");
  printf("%d %s \n",  ccd->sPreBias0,         "/*  starting col for left PreBias */");
  printf("%d %s \n",  ccd->nPreBias0,         "/*  number of PreBias rows for left amp */");
  printf("%d %s \n",  ccd->sPreBias1,         "/*  starting col for right PreBias */");
  printf("%d %s \n",  ccd->nPreBias1,         "/*  number for right amp */");
  printf("%d %s \n",  ccd->sPostBias0,        "/*  starting col for left PostBias */");
  printf("%d %s \n",  ccd->nPostBias0,        "/*  number of PostBias rows for left amp */");
  printf("%d %s \n",  ccd->sPostBias1,        "/*  starting col for right PostBias */");
  printf("%d %s \n",  ccd->nPostBias1,        "/*  number for right amp */");
  printf("%d %s \n",  ccd->sData0,            "/*  starting col for left data sec */");
  printf("%d %s \n",  ccd->nData0,            "/*  number of data rows for left data sec */");
  printf("%d %s \n",  ccd->sData1,            "/*  starting col for right data sec */");
  printf("%d %s \n",  ccd->nData1,            "/*  number for right data sec */");
  printf("%f %s \n",  ccd->biasLevel0,        "/*  Bias Level (DN) For Left Amp */");
  printf("%f %s \n",  ccd->biasLevel1,        "/*  Bias Level (DN) For Right Amp */");
  printf("%f %s \n",  ccd->readNoise0,        "/*  Read noise (sigma, DN) for left amp */");
  printf("%f %s \n",  ccd->readNoise1,        "/*  Read noise (sigma, DN) for right amp */");
  printf("%f %s \n",  ccd->gain0,             "/*  Gain (e/DN) for left amp */");
  printf("%f %s \n",  ccd->gain1,             "/*  Gain for right amp */");
  printf("%f %s \n",  ccd->fullWell0,         "/*  full well level for left side (DN) */");
  printf("%f %s \n",  ccd->fullWell1,         "/*  full well level for right side (DN) */");
  printf("%f %s \n",  ccd->xOff,              "/*  offset in pixels from nominal xPos */");
  printf("%f %s \n",  ccd->yOff,              "/*  offset in pixels from nominal yPos */");
  printf("%f %s \n",  ccd->rowOffset,         "/*  rows offset for astrom chips to align frames */");
  printf("%d %s \n",  ccd->ccdIdent,          "/*  ccd serial number */"); 


  printf("*** End of CCD pars \n");

  return;

}

static void print_ccdconfig(CCDCONFIG *ccd) {

  printf("*** ccdconfig: \n");


  printf("%s %s \n",  ccd->program, " /* program");
  printf("%d %s \n",  ccd->camRow, "/* camRow");
  printf("%d %s \n",  ccd->camCol, "/* camCol");
  printf("%d %s \n",  ccd->rowBinning, "/* rowBinning");
  printf("%d %s \n",  ccd->colBinning, "/* colBinning");
  printf("%d %s \n",  ccd->amp0, "/* amp0");
  printf("%d %s \n",  ccd->amp1, "/* amp1");
  printf("%d %s \n",  ccd->amp2, "/* amp2");
  printf("%d %s \n",  ccd->amp3, "/* amp3");
  printf("%d %s \n",  ccd->nrows, "/* nrows");
  printf("%d %s \n",  ccd->ncols, "/* ncols");

  printf("*** end of ccdconfig \n");

  return;
}
