#ifndef ASCIISCHEMA_H
#define ASCIISCHEMA_H

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "dervish.h"
#include "thConsts.h"

#ifndef strmatch
#define strmatch(a, b) ((strlen(a) == strlen(b)) && (memcmp(a, b, strlen(a))==0))
#endif


typedef enum thasciitype {
  THASCIIUNKNOWN, 
  THASCIIS, /* string */ 
  THASCIII, THASCIIL, THASCIILL, /* integers */ 
  THASCIIF, THASCIID, THASCIILD  /* floats */
} THASCIITYPE;

typedef enum thasciiheaptype {
  THASCIINOHEAP, THASCIIHEAP
} THASCIIHEAPTYPE;

typedef struct asciischemaelem {
  char *name;
  int offset;
  THASCIITYPE type;

  /* if this is an array: */
  THASCIIHEAPTYPE heaptype;
  int size; 
  
  char *comments; /* these are the standard comments to be 
		     written along the variable in the Ascii 
		     file */
} ASCIISCHEMAELEM; 

typedef struct asciischema {
  char *type;
  CHAIN *elchain;
  /* the following function pointer 
     generates error in diskio */
  void* (*constructor)(void);

} ASCIISCHEMA;


ASCIISCHEMAELEM *thAsciischemaelemNew();
RET_CODE thAsciischemaelemDel(ASCIISCHEMAELEM *elem);
RET_CODE thAsciischemaelemPut(ASCIISCHEMAELEM *elem, 
			      char *name, int *offset, THASCIITYPE *type, 
			      THASCIIHEAPTYPE *heaptype, char *comments);
int thAsciischemaelemAdd(ASCIISCHEMA *asciischema, ASCIISCHEMAELEM *elem);
RET_CODE thAsciischemaelemCopy(ASCIISCHEMAELEM *dst, ASCIISCHEMAELEM* src);
int thAsciischemaelemGetPosByName(ASCIISCHEMA *asciischema, char *elname);

ASCIISCHEMAELEM *thAsciischemaelemGetByName(ASCIISCHEMA *asciischema, 
					    char *elname);
int thAsciischemaelemMatchByName(ASCIISCHEMAELEM *asciielem, 
				 char *elname);

ASCIISCHEMA *thAsciischemaNew(char *type);
RET_CODE thAsciischemaDel(ASCIISCHEMA *asciischema);
RET_CODE thAsciischemaelemListAdd(ASCIISCHEMA *asciischema, 
				  char **elname, int *offset, 
				  THASCIITYPE *type, THASCIIHEAPTYPE *heaptype,
				  char **comments,
				  int nelem);
RET_CODE thTankInit();
RET_CODE thTankFini();

int thTankSchemaAdd(ASCIISCHEMA *asciischema);
ASCIISCHEMA *thTankSchemaGetByPos(int pos);
ASCIISCHEMA *thTankSchemaGetByType(char *type);
int thTankSchemaGetPosByType(char *type);

ASCIISCHEMA *set_schema_framefiles();
ASCIISCHEMA *set_schema_procfiles();

ASCIISCHEMA *set_schema_sentence();
ASCIISCHEMA *set_schema_objcdef();
ASCIISCHEMA *set_schema_maskdef();

#endif
