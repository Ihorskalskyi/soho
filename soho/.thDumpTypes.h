#ifndef THDUMPTYPES_H
#define THDUMPTYPES_H

#include "dervish.h"
#include "thConsts.h"
#include "thMSkyTypes.h"

#define thPsfDumpNew thPsfdumpNew
#define thPsdDumpDel thPsfdumpDel

typedef enum dump_mode {
	DUMP_DEFAULT, DUMP_OVERWRITE, DUMP_APPEND, UNKNOWN_DUMP_MODE, N_DUMP_MODE
} DUMP_WRITE_MODE;  

typedef struct ccdinfo {
	int run; 
	char *rerun;
	int field;
	int camcol;
	char band;
	int nampl;
	float g0;
	float g1;
} CCDINFO;

typedef struct runinfo {
	char *stamp;
	long int start_time, end_time, run_length;
	char *machine;
	int processor;
} RUNINFO;

typedef struct alginfo {
	float memory;
	int load1, unload1, add1, cost1, design_time1;
	int load2, unload2, add2, cost2, design_time2;
	int load3, unload3, add3, cost3, design_time3;
} ALGINFO; 

typedef struct fieldinfo {
	int n_objc;
	int n_sdss_objc;
	int n_sky_model;
	int n_sdss_galaxy;
	int n_sdss_star;
} FIELDINFO;

typedef struct fitinfo {
	int n_nonlinear_objc;
	int n_nonlinear_model;
	char *model1; int n_linear1, n_nonlinear1;
	char *model2; int n_linear2, n_nonlinear2;
	char *model3; int n_linear3, n_nonlinear3;
	char *model4; int n_linear4, n_nonlinear4;
} FITINFO;

typedef struct ioinfo {
	char *fpCfile;
	char *fpObjcfile;
	char *psFieldfile;
	char *gpBINfile; /* output */
	char *gpObjc; /* output */
} IOINFO;

typedef struct psfdump {
	HDR *hdr;
	char *statfile, *reportfile;
	CHAIN *statchain, *reportchain;
} PSFDUMP; /* pragma IGNORE */

typedef struct logdump {
	char *filename;
	char *stamp;
	CCDINFO *ccdinfo;
	RUNINFO *runinfo;
	ALGINFO *alginfo;
	FIELDINFO *fieldinfo;
	FITINFO *fitinfo;
	IOINFO *ioinfo;
} LOGDUMP; /* pragma IGNORE */

typedef struct skydump {
	char *filename;
	HDR *hdr;
	SKYPARS *skypars;
	SKYCOV *skycov;
	REGION *reg;
} SKYDUMP; /* pragma IGNORE */

typedef struct objcdump {
	char *filename;
	HDR *hdr;
	CHAIN *sdssobjc;
	CHAIN *sdssobjc2;
	CHAIN *initobjc;
	CHAIN *fitobjc;
	CHAIN *lm; /* adds the possibility of dumping LMETHOD to gpObjc files */
	CHAIN *lp_init; /* adds the possibility of dumping LPROFILE to gpObjc files */
	CHAIN *lp_fit; /* adds the possibility of dumping LPROFILE to gpObjc files */
} OBJCDUMP; /* pragma IGNORE */

typedef struct lmdump {
	char *filename;
	HDR *hdr;
	CHAIN *lm;
	int doflag;
} LMDUMP; /* pragma IGNORE */

typedef struct lpdump {
	char *filename;
	HDR *hdr;
	CHAIN *lp;
	int doflag;
} LPDUMP; /* pragma IGNORE */

typedef struct lrzdump {
	char *rfilename;
	char *zfilename;
	char *wfilename;
	HDR *hdr;
	REGION *r_image, *z_image, *w_image;
	int doflag;
} LRZDUMP;

typedef struct thframeio {
	LOGDUMP *log;
	SKYDUMP *sky;
	OBJCDUMP *objcs;
	LMDUMP *lms;
	LPDUMP *lps;
	LRZDUMP *lrz;
} FRAMEIO; /* pragma IGNORE */

/* constructors and deconstructors */

CCDINFO *thCcdinfoNew();
void thCcdinfoDel(CCDINFO *x);

RUNINFO *thRuninfoNew();
void thRuninfoDel(RUNINFO *x);

ALGINFO *thAlginfoNew();
void thAlginfoDel(ALGINFO *x);

FIELDINFO *thFieldinfoNew();
void thFieldinfoDel(FIELDINFO *x);

FITINFO *thFitinfoNew();
void thFitinfoDel(FITINFO *fitinfo);

IOINFO *thIoinfoNew();
void thIoinfoDel(IOINFO *x);

PSFDUMP *thPsfdumpNew ();
void thPsfdumpDel(PSFDUMP *x);

LOGDUMP *thLogdumpNew();
void thLogdumpDel(LOGDUMP *x);

SKYDUMP *thSkydumpNew();
void thSkydumpDel(SKYDUMP *x);

OBJCDUMP *thObjcdumpNew(char *pname);
void thObjcdumpDel(OBJCDUMP *x);

LMDUMP *thLmdumpNew();
void thLmdumpDel(LMDUMP *x);

LPDUMP *thLpdumpNew();
void thLpdumpDel(LPDUMP *x);

LRZDUMP *thLrzdumpNew();
void thLrzdumpDel(LRZDUMP *x);

FRAMEIO *thFrameioNew(char *pname);
void thFrameioDel(FRAMEIO *x);

RET_CODE thObjcDumpGetLMChain(OBJCDUMP *objc, CHAIN **lm);

RET_CODE thPsfDumpPutFilename(PSFDUMP *psfdump, char *reportfile, char *statfile);
RET_CODE thPsfDumpPutData(PSFDUMP *psfdump, CHAIN *reportchain, CHAIN *statchain, int deep);
RET_CODE thPsfDumpGetFilename(PSFDUMP *psfdump, char **reportfile, char **statfile);
RET_CODE thPsfDumpGetData(PSFDUMP *psfdump, CHAIN **reportchain, CHAIN **statchain);
RET_CODE thPsfDumpGetHdr(PSFDUMP *dump, HDR **hdr);

RET_CODE thLRZDumpGetRZWImage(LRZDUMP *lrzdump, REGION **r_image, REGION **z_image, REGION **w_image);
#endif


