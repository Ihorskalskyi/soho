#include "thQq.h"
#include "thConsts.h"
	
static int (*thQGetN)(void *);
static void * (*thQEGetByPos)(void *, int );
static char * (*thEGetName)(void *);
static void * (*thQEGetByName)(void *, char *);
static void * (*thECopy)(void *, void *);
static void (*theETrans)(void *, char *, void *);
static void (*thECharTrans)(void *, char *);
 
RET_CODE thQqInit(void *ptr_thQNew, void *ptr_thQDel, 
	void *ptr_thQGetN, void *ptr_thQEGetByPos, 
	void *ptr_thEGetName, void *ptr_thQEGetByName, 
	void *ptr_thECopy, void *ptr_theETrans, void *ptr_thECharTrans) {

char *name = "thQqInit";

static int init = 0;

if (init) {

	if (ptr_thQNew != thQNew || ptr_thQDel != thQDel || 
		ptr_thQGetN != thQGetN || ptr_thQEGetByPos != thQEGetByPos || 
		ptr_thEGetName != thEGetName ||
		ptr_thQEGetByName != thQEGetByName || ptr_thECopy != thECopy || 
		ptr_theETrans != theETrans || ptr_thECharTrans != thECharTrans) {
		thError("%s: ERROR - module already initialized, cannot change the linkings", name);
		return(SH_GENERIC_ERROR);
	}
	thError("%s: WARNING - module already initialized with the same linkings", name);
	return(SH_SUCCESS);
}

if (ptr_thQNew == NULL || ptr_thQDel == NULL || 
	ptr_thQGetN == NULL || ptr_thQEGetByPos == NULL || 
	ptr_thEGetName == NULL || 
	ptr_thQEGetByName == NULL || ptr_thECopy == NULL || 
	ptr_theETrans == NULL || ptr_thECharTrans == NULL) {
	thError("%s: ERROR - some or all of the functions are null, cannot initiate the module", name);
	return(SH_GENERIC_ERROR);
	} 

if (!init) {
	thQNew = ptr_thQNew;
	thQDel = ptr_thQDel;
	thQGetN = ptr_thQGetN;
	thQEGetByPos = ptr_thQEGetByPos;
	thEGetName = ptr_thEGetName;
	thQEGetByName = ptr_thQEGetByName;
	thECopy = ptr_thECopy;
	theETrans = ptr_theETrans;
	thECharTrans = ptr_thECharTrans;
	init = 1;
}
return(SH_SUCCESS);
}


RET_CODE thQQTrans(void *Qin, void *Qout) {
char *name = "thQQTrans";
if (Qin == NULL || Qout == NULL) {
	thError("%s: ERROR - null input or output", name);
	return(SH_GENERIC_ERROR);
}

int i, n;
n = thQGetN(Qout);

void *Eout, *Ein;
char *Ename;
for (i = 0; i < n; i++) {
	Eout = thQEGetByPos(Qout, i);
	Ename = thEGetName(Eout);
	Ein = thQEGetByName(Qin, Ename);
	if (Ein != NULL) thECopy(Ein, Eout);
	}
return(SH_SUCCESS);
}

RET_CODE thQqTrans(void *Q, void *q,  char *type) {

char *name = "thQqTrans";
TYPE qtype;
if (Q == NULL || q == NULL || type == NULL || (qtype = shTypeGetFromName(type)) == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - null input or output, or unknown type", name);
	return(SH_GENERIC_ERROR);
}

int i, n;
SCHEMA *s;
s = shSchemaGetFromType(qtype);
n = s->nelem;

SCHEMA_ELEM *se;
void *e;
char *ename;
char *etype;
void *E;
for (i = 0; i < n; i++) {
	se = s->elems + i;
	ename = se->name;
	etype = se->type;
	e = shElemGet(q, se, NULL);
	E = thQEGetByName(Q, ename);
	if (E != NULL) theETrans(e, etype, E);
}

return(SH_SUCCESS);
}

RET_CODE thqQTrans(void *q,  char *type, void *Q) {

char *name = "thqQTrans";
TYPE qtype;
if (Q == NULL || q == NULL || 
	type == NULL || (qtype = shTypeGetFromName(type)) == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - null input or output, or unknown type", name);
	return(SH_GENERIC_ERROR);
}

int i, n;
n = thQGetN(Q);

void *E;
char *Ename;
char *Evalue;
Evalue = (char *) thCalloc(SIZE, sizeof(char));

SCHEMA_ELEM *se;
for (i = 0; i < n; i++) {
	E = thQEGetByPos(Q, i);
	thECharTrans(E, Evalue);
	Ename = thEGetName(E);
	se = shSchemaElemGetFromType(qtype, Ename);
 	if (se != NULL) shElemSet(q, se, Evalue);
	}
thFree(Evalue);
return(SH_SUCCESS);
}

RET_CODE thqqTrans(void *qin,  char *typein, void *qout,  char *typeout) {

char *name = "thqqTrans";
TYPE qtypein, qtypeout;
if (typein != NULL && !strcmp(typein, "FL96")) typein = "LDOUBLE";
if (typeout != NULL && !strcmp(typeout, "FL96")) typeout = "LDOUBLE";

if (qin == NULL || qout == NULL || 
	typein == NULL || typeout == NULL || 
	(qtypein = shTypeGetFromName(typein)) == UNKNOWN_SCHEMA || 
	(qtypeout = shTypeGetFromName(typeout)) == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - null input or output, or unknown type", name);
	return(SH_GENERIC_ERROR);
}
#if DEBUG_QQ_TRANS
printf("%s: type-in = '%s', type-out = '%s' \n", name, typein, typeout);
#endif
   /* Since the TYPE and type_name are Now strictly equivalent (08/96)
      we can cache these types because we know that they are necessary 
      defined (use of make_io) */

   static TYPE LOGICAL_ID, CHAR, UCHAR, SCHAR;
   static TYPE SHORT, USHORT, INT, UINT;
   static TYPE LONG, ULONG, FLOAT, DOUBLE, LONGDOUBLE;
   static TYPE STR, PTR;
   static TYPE TYPE_ID;
   static int init = 0;
   
   if (!init) {
     LOGICAL_ID = shTypeGetFromName("LOGICAL");
     CHAR = shTypeGetFromName("CHAR");
     UCHAR = shTypeGetFromName("UCHAR");
     SCHAR = shTypeGetFromName("SCHAR");
     SHORT = shTypeGetFromName("SHORT");
     USHORT = shTypeGetFromName("USHORT");
     INT = shTypeGetFromName("INT");
     UINT = shTypeGetFromName("UINT");
     LONG = shTypeGetFromName("LONG");
     ULONG = shTypeGetFromName("ULONG");
     FLOAT = shTypeGetFromName("FLOAT");
     DOUBLE = shTypeGetFromName("DOUBLE");
     LONGDOUBLE = shTypeGetFromName("LDOUBLE"); /* long double */
     STR = shTypeGetFromName("STR");
     PTR = shTypeGetFromName("PTR");
     TYPE_ID = shTypeGetFromName("TYPE");
     init = 1;
   }
   	if (qtypein == STR || qtypein == PTR || qtypein == TYPE_ID) {
		thError("%s: ERROR - unsupported basic types (%s, %s)", name, typein, typeout);
		return(SH_GENERIC_ERROR);
		}
	if (qtypein == LOGICAL_ID || qtypein == CHAR || qtypein == UCHAR || qtypein == SCHAR ||
	    qtypein == SHORT || qtypein == USHORT || qtypein == INT || qtypein == UINT || qtypein == LONG || 
		qtypein == ULONG || qtypein == FLOAT || qtypein == DOUBLE || qtypein == LONGDOUBLE) {
		if (qtypein != qtypeout) {
			if (qtypeout == DOUBLE) {
				if (qtypein == FLOAT) {
					*(double *)qout = (double) (*((float *)qin));
					#if DEBUG_QQ_TRANS
					printf("%s: q-in = %g, q-out = %G \n", name, (*((float *)qin)), *(double *)qout);
					#endif
				}
				if (qtypein == LONGDOUBLE) *(double *)qout = (double) (* ((FL96 *) qin));
				if (qtypein == CHAR)  *(double *)qout = (double) (* ((char *)qin));
				if (qtypein == UCHAR) *(double *)qout = (double) (* ((unsigned char *)qin));
				if (qtypein == SCHAR) *(double *)qout = (double) (* ((signed char *)qin));
				if (qtypein == SHORT) *(double *)qout = (double) (* ((short *)qin));
				if (qtypein == USHORT) *(double *)qout = (double) (* ((unsigned short *)qin));
				if (qtypein == INT)    *(double *)qout = (double) (* ((int *)qin));
				if (qtypein == UINT)   *(double *)qout = (double) (* ((unsigned int *)qin));
				if (qtypein == LONG)   *(double *)qout = (double) (* ((long *)qin));
				if (qtypein == ULONG)  *(double *)qout = (double) (* ((unsigned long *)qin));
				return(SH_SUCCESS);
			}
			if (qtypeout == FLOAT) {
				if (qtypein == DOUBLE) {
					*(float *)qout = (float) (* ((double *)qin));
					#if DEBUG_QQ_TRANS
					printf("%s: q-in = %G, q-out = %g \n", name, (* ((double *)qin)), *(float *)qout);
					#endif
				}	
				if (qtypein == LONGDOUBLE) *(float *)qout = (float) (*((FL96 *)qin));
				if (qtypein == CHAR)  *(float *)qout = (float) (* ((char *)qin));
				if (qtypein == UCHAR) *(float *)qout = (float) (* ((unsigned char *)qin));
				if (qtypein == SCHAR) *(float *)qout = (float) (* ((signed char *)qin));
				if (qtypein == SHORT) *(float *)qout = (float) (* ((short *)qin));
				if (qtypein == USHORT) *(float *)qout = (float) (* ((unsigned short *)qin));
				if (qtypein == INT) *(float *)qout = (float) (* ((int *)qin));
				if (qtypein == UINT) *(float *)qout = (float) (* ((unsigned int *)qin));
				if (qtypein == LONG) *(float *)qout = (float) (* ((long *)qin));
				if (qtypein == ULONG) *(float *)qout = (float) (* ((unsigned long *)qin));
				return(SH_SUCCESS);
			}
			if (qtypeout == LONGDOUBLE) {
				if (qtypein == DOUBLE) *(FL96 *)qout = (FL96) (* ((double *)qin));
				if (qtypein == FLOAT) *(FL96 *)qout = (FL96) (*((float *)qin));
				if (qtypein == CHAR)  *(FL96 *)qout = (FL96) (* ((char *)qin));
				if (qtypein == UCHAR) *(FL96 *)qout = (FL96) (* ((unsigned char *)qin));
				if (qtypein == SCHAR) *(FL96 *)qout = (FL96) (* ((signed char *)qin));
				if (qtypein == SHORT) *(FL96 *)qout = (FL96) (* ((short *)qin));
				if (qtypein == USHORT) *(FL96 *)qout = (FL96) (* ((unsigned short *)qin));
				if (qtypein == INT) *(FL96 *)qout = (FL96) (* ((int *)qin));
				if (qtypein == UINT) *(FL96 *)qout = (FL96) (* ((unsigned int *)qin));
				if (qtypein == LONG) *(FL96 *)qout = (FL96) (* ((long *)qin));
				if (qtypein == ULONG) *(FL96 *)qout = (FL96) (* ((unsigned long *)qin));
				return(SH_SUCCESS);
			}

			thError("%s: ERROR - mismatching basic types (%s, %s)", name, typein, typeout);	
			return(SH_GENERIC_ERROR);
		}
		size_t l;
			if (qtypeout == LOGICAL_ID) {l = sizeof(LOGICAL);}
			else if (qtypeout == CHAR) {l = sizeof(char);}
			else if (qtypeout == UCHAR) {l = sizeof(unsigned char);}
			else if (qtypeout == SCHAR) {l = sizeof (signed char);}
			else if (qtypeout == SHORT) {l = sizeof(short int);}
			else if (qtypeout == USHORT) {l = sizeof(unsigned short int);}
			else if (qtypeout == INT) {l = sizeof(int);}
			else if (qtypeout == UINT) {l = sizeof(unsigned int);}
			else if (qtypeout == LONG) {l = sizeof(long);}
			else if (qtypeout == ULONG) {l = sizeof(unsigned long);}
			else if (qtypeout == FLOAT) {l = sizeof(float);}
			else if (qtypeout == DOUBLE) {l = sizeof(double);}
			else if (qtypeout == LONGDOUBLE) {l = sizeof(FL96);}
			else { 
			thError("%s: ERROR - source code problem", name);
			return(SH_GENERIC_ERROR);
			}
		memcpy(qout, qin, l);
		return(SH_SUCCESS);
	}    
RET_CODE status;
int i, n;
 SCHEMA *sout;
sout = shSchemaGetFromType(qtypeout);
n = sout->nelem;

void *ein, *eout;
 SCHEMA_ELEM *sein, *seout;
 char *ename, *etypeout, *etypein;
for (i = 0; i < n; i++) {
	seout = sout->elems + i;
	ename = seout->name;
	eout = shElemGet(qout, seout, NULL);
	etypeout = seout->type;
	sein = shSchemaElemGetFromType(qtypein, ename);
	if (sein != NULL) {
		etypein = sein->type;
		ein = shElemGet(qin, sein, NULL);
		status = thqqTrans(ein, etypein, eout, etypeout);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not convert inner record (%s)", name, ename);
			return(SH_GENERIC_ERROR);
			}
	}
}
return(SH_SUCCESS);
}

RET_CODE thQJoin(void **Qin, int n, void *Qout) {
char *name = "thQJoin";
if (Qin == NULL || n <= 0 || Qout == NULL) {
	thError("%s: null input or output", name);
	return(SH_GENERIC_ERROR);
	}
RET_CODE status = SH_GENERIC_ERROR;
int i;
void *Q;
for (i = 0; i < n; i++) {
	Q = *(Qin + i);
	if (Q != NULL) status = thQQTrans(Q, Qout);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not join in the %d-th Q", name, i);
		return(status);
	}
	}
return(status);
}	
	
RET_CODE thqJoin(void **qin,  char **typein, int n, void *qout,  char *typeout) {
char *name = "thqJoin";
if (qin == NULL || typein == NULL || n <= 0 || qout == NULL || typeout == NULL) {
	thError("%s: ERROR - null input or output");
	return(SH_GENERIC_ERROR);
	}
RET_CODE status = SH_GENERIC_ERROR;
int i;
void *q;
 char *type;
for (i = 0; i < n; i++) {
	q = *(qin + i);
	type = *(typein + i);
	if (q != NULL) status = thqqTrans(q, type, qout, typeout);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - unable to join the %d-th element", name, i);
		return(status);
	}
}
return(status);
}

RET_CODE thQDisjoin(void *Qin, void **Qout, int n) {
char *name = "thQDisjoin";
if (Qin == NULL || Qout == NULL || n <= 0) {
	thError("%s: ERROR - null input or output", name);
	return(SH_GENERIC_ERROR);
	}
int i;
RET_CODE status = SH_GENERIC_ERROR;
void *Q;
for (i = 0; i < n; i++) {
	Q = *(Qout + i);
	if (Q != NULL) status = thQQTrans(Qin, Q);
	if (status != SH_SUCCESS) {
		thError("%s: could not disjoin onto the %d-th variable", name, i);
		return(status);
	}
}
return(status);
}

RET_CODE thqDisjoin(void *qin,  char *typein, void **qout,  char **typeout, int n) {

char *name = "thqDisjoin";

if (qin == NULL || qout == NULL || typeout == NULL || n <= 0) {
	thError("%s: ERROR - null input or output", name);
	return(SH_GENERIC_ERROR);
	}

RET_CODE status;
int i;
void *q;
char *type;
for (i = 0; i < n; i++) {
	q = *(qout + i);
	type = *(typeout + i);
	if (q != NULL) status = thqqTrans(qin, typein, q, type);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not disjoin onto the %d-th variable", name, i);
		return(status);
	}
}
return(status);
}

void *thqNew(char *type) {
char *name = "thqNew";
TYPE qtype;
if (type == NULL || (qtype = shTypeGetFromName(type)) == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - unknown data type", name);
	return(NULL);
}
void *q;
SCHEMA *s;
s = shSchemaGetFromType(qtype);
q = s->construct();
return(q);
}

void thqDel(void *q, char *type) {
char *name = "thqDel";
if (q == NULL) return;
TYPE qtype;
if (type == NULL || (qtype = shTypeGetFromName(type)) == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - uknown data type", name);
	return;
}
SCHEMA *s;
s = shSchemaGetFromType(qtype);
s->destruct(q);
return;
}
