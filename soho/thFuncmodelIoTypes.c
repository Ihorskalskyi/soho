#include "thFuncmodelIoTypes.h"

PARELEM *thParelemNew() {
	
PARELEM *pe;
pe = thCalloc(1, sizeof(PARELEM));

pe->index = -1;
pe->pname = thCalloc(MX_STRING_LEN, sizeof(char));
pe->divtype = UNKNOWN_PARDIV_TYPE;
pe->n = -1;
pe->npdiv = -1;

return(pe);
}

void thParelemDel(PARELEM *pe) {

if (pe == NULL) return;
if (pe->pname != NULL) thFree(pe->pname);
if (pe->pdiv != NULL) thFree(pe->pdiv);
thFree(pe);

return;
}


PARTABLE *thPartableNew() {

PARTABLE *pt;

pt = thCalloc(1, sizeof(PARTABLE));
pt->tablename = thCalloc(MX_STRING_LEN, sizeof(char));
pt->funcname = thCalloc(MX_STRING_LEN, sizeof(char));
/* the following is designated as GENERIC since PARTABLE
   can store PARELEMIO and PARELEM
   If you designate the chain to be of type PARELEM only
   you will have problem with I/O
*/
pt->table = shChainNew("GENERIC");
pt->nrow = -1;
pt->ncol = -1;

return(pt);

}

void thPartableDel(PARTABLE *pt) {
	
if (pt == NULL) return;
if (pt->tablename != NULL) thFree(pt->tablename);
if (pt->funcname != NULL) thFree(pt->funcname);
if (pt->table != NULL) shChainDel(pt->table);
thFree(pt);
return;
}


void thPartableDestroy(PARTABLE *pt) {

if (pt == NULL) return;
if (pt->tablename != NULL) thFree(pt->tablename);
if (pt->funcname != NULL) thFree(pt->funcname);
if (pt->table != NULL) shChainDestroy(pt->table, &thParelemDel);
thFree(pt);
return;
}

void thPartableioDestroy(PARTABLE *pt) {

if (pt == NULL) return;
if (pt->tablename != NULL) thFree(pt->tablename);
if (pt->funcname != NULL) thFree(pt->funcname);
if (pt->table != NULL) shChainDestroy(pt->table, &thParelemioDel);
thFree(pt);
return;
}



PARSTATUS *thParstatusNew() {

PARSTATUS *ps;
ps = thCalloc(1, sizeof(PARSTATUS));

ps->neighbors = shChainNew("PARLIST");
return(ps);
}

void thParstatusDel(PARSTATUS *ps) {

if (ps == NULL) return;
if (ps->neighbors != NULL) shChainDel(ps->neighbors);
thFree(ps);
return;
}

void thParstatusDestroy(PARSTATUS *ps) {

if (ps == NULL) return;
if (ps->neighbors != NULL) shChainDestroy(ps->neighbors,&thParlistDel);
thFree(ps);
return;
}

FMHDU *thFmhduNew() {
FMHDU *fmhdu;
fmhdu = thCalloc(1, sizeof(FMHDU));

fmhdu->funcname = thCalloc(MX_STRING_LEN, sizeof(char));
fmhdu->funcid = thCalloc(MX_STRING_LEN, sizeof(char));

fmhdu->stat = UNKNOWN_FUNCMODEL;

/*
fmhdu->hduPtable = -1;
*/

fmhdu->ncomp = -1;
int i;
for (i = 0; i < N_FUNCMODEL_COMP_INDEX; i++) {
	fmhdu->compindex[i] = -1;
}
fmhdu->firstrow = -1;

return(fmhdu);
}

void thFmhduDel(FMHDU *fmhdu) {
if (fmhdu == NULL) return;
if (fmhdu->funcid != NULL) thFree(fmhdu->funcid);
if (fmhdu->funcname != NULL) thFree(fmhdu->funcname);
thFree(fmhdu);
return;
}

FMFITSFILE *thFmfitsfileNew() {

FMFITSFILE *f;
f = thCalloc(1, sizeof(FMFITSFILE));

f->fname = thCalloc(MX_STRING_LEN, sizeof(char));
f->mode = -1;
f->fmhdus = shChainNew("FMHDU");
f->pts = shChainNew("PARTABLE");
f->row = -1;

return(f);
}

void thFmfitsfileDel(FMFITSFILE *f) {

if (f == NULL) return;

if (f->fname != NULL) thFree(f->fname);
if (f->fmhdus != NULL) shChainDel(f->fmhdus);
if (f->pts != NULL) shChainDel(f->pts);

return;
}

void thFmfitsfileDestroy(FMFITSFILE *f) {

if (f == NULL) return;
if (f->fname != NULL) thFree(f->fname);
if (f->fmhdus != NULL) shChainDestroy(f->fmhdus, &thFmhduDel);
if (f->pts != NULL) shChainDestroy(f->pts, &thPartableDestroy);

return;
}

RET_CODE thFmfitsfilePut(FMFITSFILE *f, char *filename, PHFITSFILE *fits, int *mode, HDR *hdr, CHAIN *fmhdus, CHAIN *pts, int *current_row, int *image_loc, int *fmhdu_loc) {

char *name = "thFmfitsfilePut";
if (f == NULL) {
	thError("%s: ERROR - null FMFITSFILE pointer", name);
	return(SH_GENERIC_ERROR);
	}

if (filename != NULL) {
	if (f->fname == NULL) f->fname = thCalloc(MX_STRING_LEN, sizeof(char));
	memcpy(f->fname, filename, strlen(filename) + 1);
	}

if (fits != NULL) {
	f->fits = fits;
}
if (mode != NULL) {
	f->mode = *mode;
}
if (hdr != NULL) {
	f->hdr = hdr;
}
if (fmhdus != NULL) {
	f->fmhdus = fmhdus;
}
if (pts != NULL) {
	f->pts = pts;
}
if (current_row != NULL) {
	f->row = *current_row;
}
if (image_loc != NULL) {
	f->image_loc = *image_loc;
}
if (fmhdu_loc != NULL) {
	f->fmhdu_loc = *fmhdu_loc;
}
return(SH_SUCCESS);
}

  
PARELEMIO *thParelemioNew() {

PARELEMIO *pe = thCalloc(1, sizeof(PARELEMIO));
pe->pname = thCalloc(MX_STRING_LEN, sizeof(char));
pe->ptype = UNKNOWN_PARTYPE;

return(pe);
}

void thParelemioDel(PARELEMIO *pe) {
if (pe == NULL) return;

thFree(pe);
return;
}

EXTREGION *thExtregionNew(char *rname, int nrow, int ncol, PIXDATATYPE type) {
char *name = "thExtregNew";
if (nrow < 0 || ncol < 0) {
	thError("%s: ERROR - negative dimensions for region", name);
	return(NULL);
}

EXTREGION *extreg;
extreg = thCalloc(1, sizeof(EXTREGION));

extreg->reg = shRegNew(rname, nrow, ncol, type);
/* notice that nrowp and ncolp are set to 0 */
return(extreg);
}

void thExtregionDel(EXTREGION *extreg) {
if (extreg == NULL) return;
if (extreg->reg != NULL) shRegDel(extreg->reg);
thFree(extreg);
return;
}

PARELEMIO *thParelemioConvertParelem (PARELEM *pe) {

char *name = "thParelemioConvertParelem";

if (pe == NULL) return(NULL);

PARELEMIO *peio;
peio = thParelemioNew();

peio->index = pe->index;
memcpy(peio->pname, pe->pname, strlen(pe->pname));
peio->divtype = pe->divtype;
peio->ptype = pe->ptype;
peio->n = pe->n;
peio->npdiv = pe->npdiv;

int i;
switch (pe->ptype) {
case INTEGER_PARTYPE:
	for (i = 0; i < peio->n; i++) {
		peio->pdiv2[i] = *((int *) pe->pdiv + i);
	}
	break;
case PIX_PARTYPE:
	for (i = 0; i < peio->n; i++) {
		peio->pdiv1[i] = *((int *) pe->pdiv + i);
	}
	break;
default:
	thError("%s: WARNING - unknown type for parelem, cannot handle value conversion", name);
	break;
}
 
return(peio);
} 

PARELEM *thParelemConvertParelemio(PARELEMIO *peio) {

char *name = "thParelemConvertParelemio";

if (peio == NULL) {return(NULL);}

PARELEM *pe;
pe = thParelemNew();

pe->index = peio->index;
memcpy(pe->pname, peio->pname, strlen(peio->pname));
pe->divtype = peio->divtype;
pe->ptype = peio->ptype;
pe->n = peio->n;
pe->npdiv = peio->npdiv;

if (peio->n <= 0) return(pe);

int i;
switch (peio->ptype) {
case (INTEGER_PARTYPE):
	pe->pdiv = (int *) thCalloc(peio->n, sizeof(int));
	for (i = 0; i < peio->n; i++) {
		*((int*) pe->pdiv + i) = peio->pdiv2[i];
	}
	break;
case (PIX_PARTYPE):
	pe->pdiv = (THPIX *) thCalloc(peio->n, sizeof(THPIX));
	for (i = 0; i < peio->n; i++) {
                *((THPIX*) pe->pdiv + i) = peio->pdiv1[i];
        }
        break;
default: 
	thError("%s: WARNING - unsupported partype for conversion", name);
	break;
}

return(pe);
}

PARTABLE *thPartableioConvertPartable(PARTABLE *pt) {
if (pt == NULL) return(NULL);

PARTABLE *ptio;
ptio = thPartableNew();
memcpy(ptio->tablename, pt->tablename, strlen(pt->tablename) + 1);
memcpy(ptio->funcname, pt->funcname, strlen(pt->funcname) + 1);
ptio->nrow = pt->nrow;
ptio->ncol = pt->ncol;

if (pt->table == NULL) return(ptio);

PARELEM *pe;
PARELEMIO *peio;
int i;
for (i = 0; i < shChainSize(pt->table); i++) {
	pe = shChainElementGetByPos(pt->table, i);
	peio = thParelemioConvertParelem(pe);
	shChainElementAddByPos(ptio->table, peio, "PARELEMIO", TAIL, AFTER);
	}
return(ptio);
}

PARTABLE *thPartableConvertPartableio (PARTABLE *ptio) {

if (ptio == NULL) return(NULL);

PARTABLE *pt;
pt = thPartableNew();
memcpy(pt->tablename, ptio->tablename, strlen(pt->tablename) + 1);
memcpy(pt->funcname, ptio->funcname, strlen(ptio->funcname) + 1);
pt->nrow = ptio->nrow;
pt->ncol = ptio->ncol;

if (ptio->table == NULL) return(pt);

PARELEM *pe;
PARELEMIO *peio;
int i;

for (i = 0; i < shChainSize(ptio->table); i++) {
        peio = shChainElementGetByPos(ptio->table, i);
        pe = thParelemConvertParelemio(peio);
        shChainElementAddByPos(pt->table, pe, "PARELEM", TAIL, AFTER);
        }
return(pt);
}
/* pardef and parlist relations */

int thMatchPardefParelem (PARDEF *pd, PARELEM *pe) {
if (pd == NULL && pe == NULL) return(1);
if (pd == NULL || pe == NULL) return(0);

if ((pd->pname == NULL && pe->pname != NULL) || 
    (pd->pname != NULL && pe->pname == NULL)) return(1);

if (pd->pname != NULL) { 
        int l1, l2; 
        l1 = strlen(pd->pname); 
        l2 = strlen(pe->pname); 
        if (l1 != l2) return(0); 
        if (memcmp(pd->pname, pe->pname, l1)) return(0);
}
if (pd->ptype != UNKNOWN_PARTYPE && pe->ptype != UNKNOWN_PARTYPE) return(pd->ptype == pe->ptype);

return(1);
}

/* basic handling of par tables and parelements and relating them to pardef and parlist */

int thGetIndexThpixPardiv (THPIX pval, THPIX *pdiv, int npdiv, THPARDIV_TYPE divtype, RET_CODE *status) {

char *name = "thGetIndexThpixPardiv";

if (pdiv == NULL) {
	thError("%s: ERROR - null input", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	}

RET_CODE status2;
int index;
switch (divtype) {
	case LINEAR_DIV: 
	index = thGetIndexThpixPardivLinear(pval, pdiv, npdiv, &status2);
	break;
	case LOGARITHMIC_DIV:
	index = thGetIndexThpixPardivLogarithmic(pval, pdiv, npdiv, &status2);
	break;
	default:
	thError("%s: ERROR - division type not supported", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(-1);
	}
if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not calculate the index", name);
	thInsertStatus(status, status2);
	return(-1);
}

thInsertStatus(status, SH_SUCCESS);
return(index);
}

int thGetIndexIntPardiv (int pval, int *pdiv, int npdiv, THPARDIV_TYPE divtype, RET_CODE *status) {

char *name = "thGetIndexIntPardiv";

if (pdiv == NULL) {
	thError("%s: ERROR - null input or output", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	}

RET_CODE status2;
int index;
switch (divtype) {
	case LINEAR_DIV: 
	index = thGetIndexIntPardivLinear(pval, pdiv, npdiv, &status2);
	break;
	case LOGARITHMIC_DIV:
	index = thGetIndexIntPardivLogarithmic(pval, pdiv, npdiv, &status2);
	break;
	default:
	thError("%s: ERROR - division type not supported", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(-1);
	}
if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not calculate the index", name);
	thInsertStatus(status, status2);
	return(-1);
}

thInsertStatus(status, SH_SUCCESS);
return(index);
}

/* returns a correspondce table whose value is the index in the parlist for a parelem in the partable */
int thCorrPartableParlist(PARTABLE *pt, PARLIST *pl, int *ctable, RET_CODE *status) {

	char *name = "thCorrPartableParlist";
	
	if (pl == NULL || pt == NULL) {
		thError("%s: ERROR - null input or output", name);
		thInsertStatus(status, SH_GENERIC_ERROR);
		return(1);
	}

	int i, j, npl, npt;
	npl = shChainSize(pl->pars);
	npt = shChainSize(pt->table);
	if (npt != npl) {
		thError("%s: WARNING - pt and pl not of the same size", name);
	}
	
	PARELEM *pe; 
	PARDEF *pd;
	int corrj, corr = 1;
	for (i = 0; i < npt; i++) {
		pe = shChainElementGetByPos(pt->table, i);
		corrj = 0; j = 0;
		while (j < npl && corrj == 0) {
			pd = shChainElementGetByPos(pl->pars, j);
			if (thMatchPardefParelem(pd, pe)) {
				if (ctable != NULL) ctable[i] = j;
				corrj = 1;
			}
			j++;
		}
		if (corrj == 0 && ctable != NULL) ctable[i] = -1;
		corr = corr * corrj;
	}
	corr = 1- corr;
	if (corr == 1) {
		thError("%s: ERROR - some parelements had no corresponding pardef", name);
		thInsertStatus(status, SH_GENERIC_ERROR);
	}

return(corr);
}

/* returns a correpondence table whose value is the index in the partable for a pardef in the parlist */
int thCorrParlistPartable(PARLIST *pl, PARTABLE *pt, int *ctable, RET_CODE *status) {

	char *name = "thCorrParlistPartable";
	
	if (pl == NULL || pt == NULL) {
		thError("%s: ERROR - null input or output", name);
		thInsertStatus(status, SH_GENERIC_ERROR);
		return(1);
	}

	int i, j, npl, npt;
	npl = shChainSize(pl->pars);
	npt = shChainSize(pt->table);
	if (npt != npl) {
		thError("%s: WARNING - pt and pl not of the same size", name);
	}
	
	PARELEM *pe;
	PARDEF *pd;
	int corrj, corr = 1;
	for (i = 0; i < npl; i++) {
		pd = shChainElementGetByPos(pl->pars, i);
		corrj = 0; j = 0;
		while (j < npt && corrj == 0) {
			pe = shChainElementGetByPos(pt->table, j);
			if (thMatchPardefParelem(pd, pe)) {
				if (ctable != NULL) ctable[i] = j;
				corrj = 1;
			}
			j++;
		}
		if (corrj == 0 && ctable != NULL) ctable[i] = -1;
		corr = corr * corrj;
	}
	corr = 1- corr;
	if (corr == 1) {
		thError("%s: ERROR - some pardefs had no corresponding parelement", name);
		thInsertStatus(status, SH_GENERIC_ERROR);
	}

return(corr);
}

/* given a pardef and a parelement, the function calculates the division-index of
   the pardef-value in parelement */
int thGetDivindexFromPardef (PARELEM *pe, PARDEF *pd, RET_CODE *status) {
	
char *name = "thGetDivindexFromPardef";
if (pe == NULL || pd == NULL) {
	thError("%s: ERROR - null input and output", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(-1);
	}
if (pe->ptype != pd->ptype) {
	thError("%s: ERROR - mismatching types between input and output");
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(-1);
	}
int index;
switch (pe->ptype) {
	case INTEGER_PARTYPE:
	index = thGetIndexIntPardiv (*(int *) pd->pval, pe->pdiv, pe->npdiv, pe->divtype, status);

	break;
	case PIX_PARTYPE:
	index = thGetIndexThpixPardiv(*(THPIX *) pd->pval, pe->pdiv, pe->npdiv, pe->divtype, status);
	break;
	default:
	thError("%s: ERROR - unsupported par type", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(-1);
	}
thInsertStatus(status, SH_SUCCESS);
return(index);
}


int thGetRowFromDivindex(int *divindex, int *npdiv, int nt, RET_CODE *status) {

char *name = "thGetRowFromDivindex";

if (divindex == NULL || npdiv == NULL) {
	thError("%s: ERROR - null input", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(-1);
	}

if (nt <= 0) {
	thError("%s: ERROR - number of parameters should be positive", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(-1);
	}

int i, row;
row = divindex[0];
for (i = 1; i < nt; i++) {
	row = divindex[i] + row * npdiv[i];
}

thInsertStatus(status, SH_SUCCESS);
return(row);
}

int *thGetNpardivFromPartable (PARTABLE *pt, int *nt, RET_CODE *status) {

char *name = "thGetNpardivFromPartable";

if (pt == NULL) {
	thError("%s: ERROR - null input", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	thInsertInt(nt, 0);
	return(NULL);
}

int i, ntab;
ntab = shChainSize(pt->table);

int *npdiv;
npdiv = thCalloc(ntab, sizeof(int));

PARELEM *pe;
for (i = 0; i < ntab; i++) {
	pe = shChainElementGetByPos(pt->table, i);
	if (pe == NULL) {
		thError("%s: ERROR - empty element detected in par table", name);
		thInsertStatus(status, SH_GENERIC_ERROR);
		thFree(npdiv);
		return(NULL);
	}
	npdiv[i] = pe->npdiv;
}

thInsertInt(nt, ntab);
thInsertStatus(status, SH_SUCCESS);
return(npdiv);
}

int *thGetDivindexFromRow(int row, int *npdiv, int nt, RET_CODE *status) {

char *name = "thGetDivindexFromRow";

if (row < 0) {
	thError("%s: ERROR - row should be positive or zero", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(NULL);
	}

if (npdiv == NULL) {
	thError("%s: ERROR - null input", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(NULL);
	}

if (nt <= 0) {
	thError("%s: ERROR - table size should be positive", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(NULL);
	}
int i, *divindex;
divindex = thMalloc(nt * sizeof(int));

for (i = nt - 1; i > 0; i--) {
	if (npdiv[i - 1] <= 0) {
		thError("%s: ERROR - non-positive division for i = %d", name, i);
		thInsertStatus(status, SH_GENERIC_ERROR);
		thFree(divindex);
		return(NULL);
	}
	divindex[i] = row % npdiv[i];
	row = row / npdiv[i];
	}
divindex[0] = row; 

if (row >= npdiv[0]) {
	thError("%s: ERROR - row exceeded allowed range within a box", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	thFree(divindex);
	return(NULL);
}

thInsertStatus(status, SH_SUCCESS);
return(divindex);
}

int thGetMaxrowFromNpardiv(int *npdiv, int nt) {
	
char *name = "thGetMaxrowFromNpardiv";

if (npdiv == NULL) {
	thError("%s: ERROR - null input", name);
	return(-1);
	}
if (nt <= 0) {
	thError("%s: ERROR - inadmissable table size", name);
	return(-1);
	}

int i, row = 1;
for (i = 0; i < nt; i++) {
	if (npdiv[i] <= 0) {
		thError("%s: ERROR - negative division for i = %d", name, i);
		return(-1);
	}
	row = row * npdiv[i];
}

return(row);
}

int *thGetPartableIndexFromParlist (PARTABLE *pt, PARLIST *pl, int *np, RET_CODE *status) {
	
char *name = "thGetPartableIndexFromParlist";

if (pt == NULL || pl == NULL) {
	thError("%s: ERROR - null input", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	thInsertInt(np, 0);
	return(NULL);
	}

int nl;
nl = shChainSize(pl->pars);

if (nl == 0) {
	thError("%s: WARNING - empty par list", name);
	thInsertStatus(status, SH_SUCCESS);
	thInsertInt(np, 0);
	return(NULL);
	}

int nt;
nt = shChainSize(pt->table);
if (nt == 0) {
	thError("%s: WARNING - empty par table", name);
	}

int *index;
index = thCalloc(nl, sizeof(int));

PARELEM *pe;
PARDEF *pd;

int i, j, match;
for (i = 0; i < nl; i++) {
	pd = shChainElementGetByPos(pl->pars, i);
	j = 0;
	match = 0;
	while(j < nt && match == 0) {
		pe = shChainElementGetByPos(pt->table, j);
		match = thMatchPardefParelem(pd, pe);
		j++;
	}
	if (match) {
		index[i] = j - 1;
	} else {
		index[i] = -1;
	}
}

thInsertInt(np, nl);
thInsertStatus(status, SH_SUCCESS);
return(index);
}


int thGetPartableIndexFromPardef (PARTABLE *pt, PARDEF *pd, RET_CODE *status) {

char *name = "thGetPartableIndexFromPardef";

if (pt == NULL || pd == NULL) {
	thError("%s: ERROR - null input", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(-1);
	}

int j, match, nt;
j = 0;
match = 0;
nt = shChainSize(pt->table);
if (nt == 0) {
	thError("%s: WARNING - empty par table", name);
	}

PARELEM *pe;
while(j < nt && match == 0) {
	pe = shChainElementGetByPos(pt->table, j);
	match = thMatchPardefParelem(pd, pe);
	j++;
}

thInsertStatus(status, SH_SUCCESS);
if (match) return(j);
return(-1);

}


RET_CODE thGetPardefFromParelem (PARELEM *pe, int divindex, PARDEF *pd) {

char *name = "thGetPardefFromParelem";

if (pe == NULL || pd == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
	}

if (divindex < 0 || divindex >= pe->npdiv) {
	thError("%s: unacceptable index value %d ~ (0, %d)", name, divindex, pe->npdiv);
	return(SH_GENERIC_ERROR);
	}

RET_CODE status;
void *val;
switch (pe->ptype) {
	case INTEGER_PARTYPE:
	val = thMalloc(sizeof(int));
	*((int *) val) = thGetValIntPardiv(divindex, pe->pdiv, pe->npdiv, pe->divtype, &status);
	break;
	case PIX_PARTYPE:
	val = thMalloc(sizeof(THPIX));
	*((THPIX *) val) = thGetValThpixPardiv(divindex, pe->pdiv, pe->npdiv, pe->divtype, &status);
	break;
	default:
 	thError("%s: ERROR - unknown division type", name);
	return(SH_GENERIC_ERROR);
	break;
	}

if (status != SH_SUCCESS) {
	thError("%s: ERROR - couldn't retain value from division info", name);
	return(status);
}

/* this function should be written */
thPardefPut(pd, pe->pname, NULL, NULL, NULL, &(pe->ptype), val); 
return(status);
}
