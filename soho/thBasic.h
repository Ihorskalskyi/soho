#ifndef THBASIC_H
#define THBASIC_H


#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "shCAssert.h"
#include "shCErrStack.h"
#include "shCEnvscan.h"
#include "shCUtils.h"

#include "dervish.h"
#include "phSpanUtil.h"

#define RUNTIME_ERROR 1
#define RUNTIME_WARNING 0
#define STACK_ERROR 1
#define STACK_WARNING 1

#define thError shError
#define thWarn shError

void thInsertStatus(RET_CODE *status, RET_CODE status2); 
void thInsertInt(int *x, int y);
int thErrorStatus(RET_CODE *status);
OBJMASK *thObjmaskFromCircleInRect(int rowc, int colc, int r, 
					int row0, int col0, 
					int row1, int col1);


void my_thError(char *fmt, ...);
void my_thWarn(char *fmt, ...);

RET_CODE centralize_region(REGION *reg, int *row0, int *col0);
RET_CODE decentralize_region(REGION *reg, int row0, int col0);

RET_CODE thChainQsort(CHAIN *chain, int (*compar)(const void *, const void *));
RET_CODE thCanonizeObjmask(OBJMASK *sv, int nearly_sorted);

RET_CODE thCanonizeObjmaskChain(CHAIN *chain, int canonize_objmasks, int nearly_sorted);

#endif
