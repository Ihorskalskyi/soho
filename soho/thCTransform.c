#include "thCTransform.h"
#include "thDebug.h"
#include "thMath.h"
#include "thMGalaxyTypes.h"

static CDESCRIPTION tdesc = UNKNOWN_CDESCRIPTION;

static THPIX SERSIC_INDEX_SMALL = (THPIX) DEFAULT_SERSIC_INDEX_SMALL;
static THPIX CORESERSIC_INDEX_SMALL = (THPIX) DEFAULT_CORESERSIC_INDEX_SMALL;
static THPIX SERSIC1_INDEX_SMALL = (THPIX) DEFAULT_SERSIC1_INDEX_SMALL;
static THPIX SERSIC2_INDEX_SMALL = (THPIX) DEFAULT_SERSIC2_INDEX_SMALL;

static THPIX SERSIC_INDEX_LARGE = (THPIX) DEFAULT_SERSIC_INDEX_LARGE;
static THPIX CORESERSIC_INDEX_LARGE = (THPIX) DEFAULT_CORESERSIC_INDEX_LARGE;
static THPIX SERSIC1_INDEX_LARGE = (THPIX) DEFAULT_SERSIC1_INDEX_LARGE;
static THPIX SERSIC2_INDEX_LARGE = (THPIX) DEFAULT_SERSIC2_INDEX_LARGE;


/* basic put and get only suitable for this package */
static THPIX record_get(void *q, TYPE t, char *r);
static void record_put(void *q, TYPE t, char *r, THPIX y);	
/* mathematical transformation to help restrain the value of the parameter between a min and a max value */
static THPIX klog(THPIX x, THPIX min, THPIX max);
static THPIX kexp(THPIX x, THPIX min, THPIX max);
static THPIX Dklog(THPIX x, THPIX min, THPIX max);
static THPIX Dkexp(THPIX x, THPIX min, THPIX max);
static THPIX DDkexp(THPIX x, THPIX min, THPIX max);

static THPIX ulog(THPIX x, THPIX min);
static THPIX uexp(THPIX x, THPIX min);
static THPIX Dulog(THPIX x, THPIX min);
static THPIX Duexp(THPIX x, THPIX min);
static THPIX DDuexp(THPIX x, THPIX min);

static RET_CODE find_phi_from_abc (THPIX a, THPIX b, THPIX c, THPIX *phi);
static RET_CODE calculate_abc_abcphi_abce(THPIX alpha, THPIX beta, THPIX phi, THPIX e, 
	THPIX *a, THPIX *b, THPIX *c, 
	THPIX *aphi, THPIX *bphi, THPIX *cphi, 
	THPIX *ae, THPIX *be, THPIX *ce);

static RET_CODE find_sersic_k_kn_gamma_2n(THPIX n, THPIX *k, THPIX *kn, THPIX *gamma_2n, THPIX *ln_gamma_2n);

static RET_CODE find_coresersic_r50_k50(void *q, THPIX *r50, THPIX *k50);
static THPIX midpoint_integral(void *func_ptr, THPIX a, THPIX b, void *q, int ndiv);
static THPIX coresersic_radial_integrand(THPIX r, void *q);

static int has_sersic_index(char *pname);
static RET_CODE get_index_bounds(char *pname, void *pvalue, THPIX *index_small, THPIX *index_large);
THPIX record_get(void *q, TYPE t, char *r) {
	shAssert(q != NULL);
	shAssert(t != UNKNOWN_SCHEMA);
	shAssert(r != NULL);
	shAssert(strlen(r) != 0);

	THPIX *x;
	SCHEMA_ELEM *se;
	se = shSchemaElemGetFromType(t, r);
	shAssert(se != NULL);
	x = shElemGet(q, se, NULL);
return(*x);
}

void record_put(void *q, TYPE t, char *r, THPIX y) {
	shAssert(q != NULL);
	shAssert(t != UNKNOWN_SCHEMA);
	shAssert(r != NULL);
	shAssert(strlen(r) != 0);
	
	THPIX *x;
	SCHEMA_ELEM *se;
	se = shSchemaElemGetFromType(t, r);
	shAssert(se != NULL);
	x = shElemGet(q, se, NULL);
	*x = y;
return;
}

/* mathematical transformation to help restrain the value of a parameter between a min and a max value */
THPIX klog(THPIX x, THPIX min, THPIX max) {
shAssert(max > min);

if (x <= min) {
	x = min + (max - min) / 1.0E5* phRandomUniformdev();
} else if (x >= max) {
	x = max - (max - min) / 1.0E5 * phRandomUniformdev();
}
/* 
shAssert(x > min && x < max);
if (x <= min || x >= max) {
	return(THNAN);
}
*/
THPIX y = atanh(((THPIX) 2.0 * x - min - max) / (max - min));
return(y);
}  

THPIX kexp(THPIX x, THPIX min, THPIX max) {
shAssert(max > min);
THPIX y = (THPIX) 0.5 * (min * ((THPIX) 1.0 - tanh(x)) + max * ((THPIX) 1.0 + tanh(x)));
return(y);
}

THPIX Dklog(THPIX x, THPIX min, THPIX max) {
shAssert(max > min);
shAssert(x > min && x < max);
THPIX y = 2.0 / ((max - min) * ((THPIX) 1.0 - pow(((THPIX) 2.0 * x - min - max) / (max - min), 2)));
return(y);
}

THPIX Dkexp(THPIX x, THPIX min, THPIX max) {
shAssert(max > min);
THPIX y = (THPIX) 0.5 * (max - min) * ((THPIX) 1.0 - pow(tanh(x), 2));
return(y);
}

THPIX DDkexp(THPIX x, THPIX min, THPIX max) {
shAssert(max > min);
THPIX y = (THPIX) 0.5 * (max - min) * ((THPIX) 1.0 - pow(tanh(x), 2)) * (THPIX) -2.0 * tanh(x);
return(y);
}


THPIX ulog(THPIX x, THPIX min) {
shAssert(x > min);
THPIX y = (THPIX) log((double) x - (double) min);
return(y);
}

THPIX uexp(THPIX x, THPIX min) {
shAssert(x == x);
THPIX y = (THPIX) ((double) min + exp((double) x));
return(y);
}

THPIX Dulog(THPIX x, THPIX min) {
shAssert(x > min);
THPIX y = (THPIX) ((double) 1.0 / ((double) x - (double) min));
return(y);
}
THPIX Duexp(THPIX x, THPIX min) {
shAssert(x == x);
THPIX y = (THPIX) exp((double) x);
return(y);
}
THPIX DDuexp(THPIX x, THPIX min) {
shAssert(x == x);
THPIX y = (THPIX) exp((double) x);
return(y);
}


RET_CODE thCTransformInit(CDESCRIPTION init_tdesc, void *input) {
char *name = "thCTransformInit";
RET_CODE status;

if ((init_tdesc == UNKNOWN_CDESCRIPTION) || (init_tdesc == N_CDESCRIPTION)) {
	thError("%s: ERROR - unsupported 'CDESCRIPTION' flag", name);
	return(SH_GENERIC_ERROR);
}
if (tdesc != UNKNOWN_CDESCRIPTION && tdesc != init_tdesc) {
	thError("%s: WARNING - package already initialized, changing the default target description", name);
} else if (tdesc == init_tdesc) {
	thError("%s: WARNING - package already initialized for the same coordinate system - you need not re-initialize it", name);
}

tdesc = init_tdesc;

status = initCTransformPars(input);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not initialize static pars", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE thCTransformGetDesc(CDESCRIPTION *init_tdesc) {
char *name = "thCTransformGetDesc";
shAssert(init_tdesc != NULL);
*init_tdesc = tdesc;
return(SH_SUCCESS);
}


RET_CODE thCTransform(void *q, char *pname) {
char *name = "thCTransform";
RET_CODE status;
status = thCTransformFromSource (q, pname, tdesc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not transform variable of type '%s' from the working source description", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE thCTransformFromPhoto(void *q, char *pname) {
char *name = "generic_photo_transform";
RET_CODE status;
status = thCTransformFromSource(q, pname, (CANONICAL | DIRECT_SERSIC_INDEX));
if (status != SH_SUCCESS) {
	thError("%s: ERROR - 'PHOTO' products are generically assumed to be of 'CANONICAL' type - failed to transform", name);
	return(status);
}
return(SH_SUCCESS);
}


RET_CODE find_phi_from_abc (THPIX a, THPIX b, THPIX c, THPIX *phi) { 
char *name = "find_phi_from_abc";
if (phi == NULL) {
	thError("%s: ERROR - null placeholder for phi", name);
	return(SH_GENERIC_ERROR);
}
THPIX amb, apb; /* alpha - beta, alpha + beta */
amb = -pow(pow(a-c, 2) + pow(2.0 * b, 2), 0.5);
apb = a + c;

if (amb != (THPIX) 0.0) { /* otherwise fully circular not change the angle at all */
	THPIX s2phi, c2phi, tphi, old_phi;
	c2phi = (2.0 * a - apb) / (amb);
	s2phi = (2.0 * b) / (amb);
	if (c2phi != (THPIX) -1.0) {
		tphi = s2phi / ((THPIX) 1.0 + c2phi);
		old_phi = atan(tphi);
	} else {
		old_phi = 2.0 * atan(1.0);
	}
	*phi =  2.0 * atan(1.0) - old_phi; /* adapting SDSS / PHOTO definitio of phi */ 
} 

/* 
THPIX theta = 0.0;
if (b == (THPIX) 0.0) {
	if (a <= c) {
		theta = 0.0;
	} else {
		theta = 2.0 * atan(1.0);
	}
} else if (a != c) {
	theta = 0.5 * atan(2.0 * b / (a - c));	
}
*/
return(SH_SUCCESS);
}

RET_CODE calculate_abc_abcphi_abce(THPIX alpha, THPIX beta, THPIX phi, THPIX e, 
	THPIX *a, THPIX *b, THPIX *c, 
	THPIX *aphi, THPIX *bphi, THPIX *cphi, 
	THPIX *ae, THPIX *be, THPIX *ce) {

char *name = "calculate_abc_abcphi_abce";
if (a == NULL || b == NULL || c == NULL || 
	aphi == NULL || bphi == NULL || cphi == NULL || 
	ae == NULL || be == NULL || ce == NULL) {
	thError("%s: ERROR - null placeholder", name);
	return(SH_GENERIC_ERROR);
}
THPIX c2phi, s2phi, epsilon;
c2phi = cos(((THPIX) 2.0) * phi);
s2phi = sin(((THPIX) 2.0) * phi);
epsilon = sqrt(1.0 - pow(e, (THPIX) 2.0));
	
*a = 0.5 * (-c2phi * (alpha - beta) + (alpha + beta)); /* adapting SDSS / PHOTO definition of phi */
*b = 0.5 * (alpha - beta) * s2phi; /* adapting SDSS / PHOTO definition of phi */
*c = 0.5 * (+c2phi * (alpha - beta) + (alpha + beta)); /* adapting SDSS / PHOTO definition of phi */
	
/* derivative of (a, b, c) w.r.t. phi */
*aphi = +(alpha - beta) * s2phi; /* adapting SDSS / PHOTO definition of phi */
*bphi = +(alpha - beta) * c2phi; /* adapting SDSS / PHOTO definition of phi */
*cphi = -(alpha - beta) * s2phi; /* adapting SDSS / PHOTO definition of phi */
/* derivative of (a, b, c) w.r.t. e */
*ae = 0.5 * (-e / epsilon) * ((alpha - beta) - (alpha + beta) * c2phi); /* adapting SDSS / PHOTO definition of phi */
*be = 0.5 * (-e / epsilon) * (alpha + beta) * s2phi; /* adapting SDSS / PHOTO definition of phi */
*ce = 0.5 * (-e / epsilon) * ((alpha - beta) + (alpha + beta) * c2phi); /* adapting SDSS / PHOTO definition of phi */

return(SH_SUCCESS);
}

RET_CODE thCTransformFromSource(void *q, char *pname, CDESCRIPTION sDesc) {
char *name = "thCTransformFromSource";

TYPE t;
if (q == NULL) {
	thError("%s: WARNING - null variable", name);
	return(SH_SUCCESS);
} else if (pname == NULL || strlen(pname) == 0) {
	thError("%s: ERROR - empty parameter type string (pname)", name);
	return(SH_GENERIC_ERROR);
} else if ((t = shTypeGetFromName(pname)) == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - parameter of type '%s' is not declared properly", name, pname);
	return(SH_GENERIC_ERROR);
}

const SCHEMA *s;
s = shSchemaGetFromType(t);

if (s == NULL) {
	thError("%s: ERROR - could not obtain schema information for '%s'", name, pname);
	return(SH_GENERIC_ERROR);
}
/* 
static THPIX halfpi = 2.0 * atan(1.0);
*/

RET_CODE status;
if (has_sersic_index(pname)) {
	THPIX index_large = SERSIC_INDEX_LARGE;
	THPIX index_small = SERSIC_INDEX_SMALL;
	status = get_index_bounds(pname, q, &index_small, &index_large);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get index bounds for pname = '%s'", name, pname);
		return(status);
	}
	THPIX n, N, nN;
	if (sDesc & DIRECT_SERSIC_INDEX) {

		n = record_get(q, t, "n");
		N = klog(n, index_small, index_large);
		nN = Dkexp(N, index_small, index_large);
	
		record_put(q, t, "N", N);
	
	} else if (sDesc & INDIRECT_SERSIC_INDEX) {

		N = record_get(q, t, "N");	
		n = kexp(N, index_small, index_large);
		nN = Dkexp(N, index_small, index_large);
	
		record_put(q, t, "n", n);

	} else {
		thError("%s: ERROR - unknown sersic index treatment mode while (ptype = '%s')", name, pname);
		return(SH_GENERIC_ERROR);
	}

	THPIX k, kn, gamma_2n, ln_gamma_2n;
	status = find_sersic_k_kn_gamma_2n(n, &k, &kn, &gamma_2n, &ln_gamma_2n);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get necessary sersic parameters for (n = %g)", name, (float) n);
		return(status);
	}
	THPIX inverse_2n = (THPIX) 1.0 / ((THPIX) 2.0 * n);
	int is_nan_gamma = (isnan(gamma_2n) || isinf(gamma_2n)) && (isnan(ln_gamma_2n) || isinf(ln_gamma_2n));
	if (isnan(n) || isinf(n) || isnan(k) || isinf(k) || isnan(kn) || isinf(kn) || 
		is_nan_gamma || isnan(inverse_2n) || isinf(inverse_2n)) {
		thError("%s: ERROR - unacceptable sersic parameter found among (n = %g, k = %g, kn = %g, gamma_2n = %g, ln_gamma_2n = %g, inverse_2n = %g)", 
		name, (float) n, (float) k, (float) kn, (float) gamma_2n, (float) ln_gamma_2n, (float) inverse_2n);
		return(SH_GENERIC_ERROR);
	}

	record_put(q, t, "nN", nN);
	record_put(q, t, "k", k);
	record_put(q, t, "kn", kn);	
	record_put(q, t, "gamma_2n", gamma_2n);
	record_put(q, t, "ln_gamma_2n", ln_gamma_2n);
	record_put(q, t, "inverse_2n", inverse_2n);


}

if (!strcmp(pname, "CORESERSICPARS")) {
	THPIX delta, D, dD;
	THPIX gamma, G, gG;

	if (sDesc & DIRECT_SERSIC_INDEX) {

		delta = record_get(q, t, "delta");
		D = klog(delta, (THPIX) CORESERSIC_DELTA_SMALL, (THPIX) CORESERSIC_DELTA_LARGE);
		dD = Dkexp(D, (THPIX) CORESERSIC_DELTA_SMALL, (THPIX) CORESERSIC_DELTA_LARGE);
	
		record_put(q, t, "D", D);
	
		gamma = record_get(q, t, "gamma");
		G = klog(gamma, (THPIX) CORESERSIC_GAMMA_SMALL, (THPIX) CORESERSIC_GAMMA_LARGE);
		gG = Dkexp(G, (THPIX) CORESERSIC_GAMMA_SMALL, (THPIX) CORESERSIC_GAMMA_LARGE);
	
		record_put(q, t, "G", G);

	} else if (sDesc & INDIRECT_SERSIC_INDEX) {

		D = record_get(q, t, "D");	
		delta = kexp(D, (THPIX) CORESERSIC_DELTA_SMALL, (THPIX) CORESERSIC_DELTA_LARGE);
		dD = Dkexp(D, (THPIX) CORESERSIC_DELTA_SMALL, (THPIX) CORESERSIC_DELTA_LARGE);
	
		record_put(q, t, "delta", delta);

		G = record_get(q, t, "G");	
		gamma = kexp(G, (THPIX) CORESERSIC_GAMMA_SMALL, (THPIX) CORESERSIC_GAMMA_LARGE);
		gG = Dkexp(G, (THPIX) CORESERSIC_GAMMA_SMALL, (THPIX) CORESERSIC_GAMMA_LARGE);
	
		record_put(q, t, "gamma", gamma);

	} else {
		thError("%s: ERROR - unknown index treatment mode while (ptype = '%s')", name, pname);
		return(SH_GENERIC_ERROR);
	}

	record_put(q, t, "dD", dD);
	record_put(q, t, "gG", gG);

	
}


if (sDesc & LOGHYPERBOLIC_LOGR) {
	
	THPIX V, W;
	THPIX v, w, ue;
	THPIX coshv, sinhv, expw, expnw, re2, ren2;
	THPIX re, reUe, reUeUe, e, E, phi, alpha, beta, s2phi, c2phi;
	THPIX aV, aW, bV, bW, cV, cW;
	THPIX av, bv, cv, aw, bw, cw, a, b, c;

	ue = record_get(q, t, "ue");
	V = record_get(q, t, "V");
	W = record_get(q, t, "W");

	#if RLOG_U
	re = uexp(ue, MINRADIUS); /* , MAXRADIUS); */
	reUe = Duexp(ue, MINRADIUS); /* , MAXRADIUS); */
	reUeUe = DDuexp(ue, MINRADIUS); /* ; , MAXRADIUS); */
	#else
	re = kexp(ue, MINRADIUS, MAXRADIUS);
	reUe = Dkexp(ue, MINRADIUS, MAXRADIUS);
	reUeUe = DDkexp(ue, MINRADIUS, MAXRADIUS);
	#endif

	record_put(q, t, "re", re);
	record_put(q, t, "reUe", reUe);
	record_put(q, t, "reUeUe", reUeUe);

	THPIX v1, w1;	
	THPIX vv1, vw1, wv1, ww1; /* derivatives */
	THPIX v1V, w1W; /* derivatives */
	THPIX vV, vW, wV, wW; /* derivatives */

	v1 = kexp(V, MIN_VW, MAX_VW);
	w1 = kexp(W, MIN_VW, MAX_VW);
	v1V = Dkexp(V, MIN_VW, MAX_VW);
	w1W = Dkexp(W, MIN_VW, MAX_VW);

	v = (v1 + w1) * SQRT_HALF;
	w = (v1 - w1) * SQRT_HALF;

	vv1 = SQRT_HALF;
	vw1 = SQRT_HALF;
	wv1 = SQRT_HALF;
	ww1 = -SQRT_HALF;

	vV = vv1 * v1V; /* chain rule */
	vW = vw1 * w1W;
	wV = wv1 * v1V;
	wW = ww1 * w1W;

	/* inverse is the following
	   v1 = (v + w) * SQRT_HALF;
	   w1 = (v - w) * SQRT_HALF;
	*/

	coshv = cosh(v);
	sinhv = sinh(v);
	expw = exp(w);
	expnw = exp(-w);
	re2 = pow(re, 2.0);
	ren2 = pow(re, -2.0);

	a = ren2 * coshv * expw;
	c = ren2 * coshv * expnw;
	b = ren2 * sinhv;

	if (a != a || b != b || c != c) {
		thError("%s: ERROR - undefined values for (a, b, c, = %g, %g, %g) given (re, v, w  = %g, %g, %g)", name, a, b, c, re, v, w);
		return(SH_GENERIC_ERROR);
	}

	av = b * expw;
	cv = b * expnw;
	bv = ren2 * coshv;

	aw = a;
	cw = -c;
	bw = (THPIX) 0.0;

	record_put(q, t, "a", a);
	record_put(q, t, "b", b);
	record_put(q, t, "c", c);	
	record_put(q, t, "av", av);
	record_put(q, t, "bv", bv);
	record_put(q, t, "cv", cv);
	record_put(q, t, "aw", aw);
	record_put(q, t, "bw", bw);
	record_put(q, t, "cw", cw);
		
	aV = av * vV + aw * wV; /* chain rule */
	aW = av * vW + aw * wW;
	bV = bv * vV + bw * wV;
	bW = bv * vW + bw * wW;
	cV = cv * vV + cw * wV;
	cW = cv * vW + cw * wW;

	record_put(q, t, "aV", aV);
	record_put(q, t, "aW", aW);
	record_put(q, t, "bV", bV);
	record_put(q, t, "bW", bW);
	record_put(q, t, "cV", cV);
	record_put(q, t, "cW", cW);

	THPIX D = pow(a - c, 2) + 4.0 * pow(b, 2);
	if (D < 0) {
		thError("%s: ERROR - negative discriminant (D) for (re, v, w = %g, %g, %g)", name, re, v, w);
		return(SH_GENERIC_ERROR);
	}
	alpha = (a + c) - sqrt(D);
	beta = (a + c) + sqrt(D);
	
	e = 1.0 - alpha / beta;
	if (e < 0.0 || beta == 0.0) {
		thError("%s: ERROR - could not calculate (e) for (re, v, w = %g, %g, %g)", name, re, v, w);
		return(SH_GENERIC_ERROR);
	}
	e = sqrt(e);
	if (e != e) {
		thError("%s: ERROR - undefined (e) for (re, v, w = %g, %g, %g)", name, re, v, w);
		return(SH_GENERIC_ERROR);
	}
	RET_CODE status;
	status = find_phi_from_abc(a, b, c, &phi);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not find 'phi' from (a, b, c)", name);
		return(status);
	}

	s2phi = sin(2.0 * phi);
	c2phi = cos(2.0 * phi);

	E = atanh(e / MAXELL);

	record_put(q, t, "phi", phi);
	record_put(q, t, "s2phi", s2phi);
	record_put(q, t, "c2phi", c2phi);
	record_put(q, t, "e", e);
	record_put(q, t, "E", E);
	record_put(q, t, "alpha", alpha);
	record_put(q, t, "beta", beta);

	#if DEEP_VERBOSE_CTRANSFORM
	printf("%s: (re, e, e-1, phi, E = %g, %g, %g, %g, %g), (ue, V, W = %g, %g, %g) \n", name, re, e, e-1.0, phi, E, ue, V, W);
	#endif
} else if (sDesc & LOGHYPERBOLIC) {
	THPIX V, W;
	THPIX v, w, re, rb;
	THPIX coshv, sinhv, expw, expnw, re2, ren2;
	THPIX ue, ub, e, E, phi, alpha, beta, s2phi, c2phi;
	THPIX aV, aW, bV, bW, cV, cW;
	THPIX av, bv, cv, aw, bw, cw, a, b, c;

	re = record_get(q, t, "re");
	if (!strcmp(pname, "CORESERSICPARS")) rb = record_get(q, t, "rb");
	V = record_get(q, t, "V");
	W = record_get(q, t, "W");

	THPIX v1, w1;	
	THPIX vv1, vw1, wv1, ww1; /* derivatives */
	THPIX v1V, w1W; /* derivatives */
	THPIX vV, vW, wV, wW; /* derivatives */

	v1 = kexp(V, MIN_VW, MAX_VW);
	w1 = kexp(W, MIN_VW, MAX_VW);
	v1V = Dkexp(V, MIN_VW, MAX_VW);
	w1W = Dkexp(W, MIN_VW, MAX_VW);

	v = (v1 + w1) * SQRT_HALF;
	w = (v1 - w1) * SQRT_HALF;

	vv1 = SQRT_HALF;
	vw1 = SQRT_HALF;
	wv1 = SQRT_HALF;
	ww1 = -SQRT_HALF;

	vV = vv1 * v1V; /* chain rule */
	vW = vw1 * w1W;
	wV = wv1 * v1V;
	wW = ww1 * w1W;

	/* inverse is the following
	   v1 = (v + w) * SQRT_HALF;
	   w1 = (v - w) * SQRT_HALF;
	*/

	coshv = cosh(v);
	sinhv = sinh(v);
	expw = exp(w);
	expnw = exp(-w);
	re2 = pow(re, 2.0);
	ren2 = pow(re, -2.0);

	a = ren2 * coshv * expw;
	c = ren2 * coshv * expnw;
	b = ren2 * sinhv;

	if (a != a || b != b || c != c) {
		thError("%s: ERROR - undefined values for (a, b, c, = %g, %g, %g) given (re, v, w  = %g, %g, %g)", name, a, b, c, re, v, w);
		return(SH_GENERIC_ERROR);
	}

	av = b * expw;
	cv = b * expnw;
	bv = ren2 * coshv;

	aw = a;
	cw = -c;
	bw = (THPIX) 0.0;

	record_put(q, t, "a", a);
	record_put(q, t, "b", b);
	record_put(q, t, "c", c);	
	record_put(q, t, "av", av);
	record_put(q, t, "bv", bv);
	record_put(q, t, "cv", cv);
	record_put(q, t, "aw", aw);
	record_put(q, t, "bw", bw);
	record_put(q, t, "cw", cw);
		
	aV = av * vV + aw * wV; /* chain rule */
	aW = av * vW + aw * wW;
	bV = bv * vV + bw * wV;
	bW = bv * vW + bw * wW;
	cV = cv * vV + cw * wV;
	cW = cv * vW + cw * wW;

	record_put(q, t, "aV", aV);
	record_put(q, t, "aW", aW);
	record_put(q, t, "bV", bV);
	record_put(q, t, "bW", bW);
	record_put(q, t, "cV", cV);
	record_put(q, t, "cW", cW);

	THPIX D = pow(a - c, 2) + 4.0 * pow(b, 2);
	if (D < 0) {
		thError("%s: ERROR - negative discriminant (D) for (re, v, w = %g, %g, %g)", name, re, v, w);
		return(SH_GENERIC_ERROR);
	}
	alpha = (a + c) - sqrt(D);
	beta = (a + c) + sqrt(D);
	
	e = 1.0 - alpha / beta;
	if (e < 0.0 || beta == 0.0) {
		thError("%s: ERROR - could not calculate (e) for (re, v, w = %g, %g, %g)", name, re, v, w);
		return(SH_GENERIC_ERROR);
	}
	e = sqrt(e);
	if (e != e) {
		thError("%s: ERROR - undefined (e) for (re, v, w = %g, %g, %g)", name, re, v, w);
		return(SH_GENERIC_ERROR);
	}
	RET_CODE status;
	status = find_phi_from_abc(a, b, c, &phi);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not find 'phi' from (a, b, c)", name);
		return(status);
	}


	s2phi = sin(2.0 * phi);
	c2phi = cos(2.0 * phi);

	E = atanh(e / MAXELL);
	#if RLOG_U
	ue = ulog(re, MINRADIUS); /* , MAXRADIUS); */
	if (!strcmp(pname, "CORESERSICPARS")) ub = ulog(rb, MINRADIUSB); 
	#else
	ue = klog(re, MINRADIUS, MAXRADIUS);
	if (!strcmp(pname, "CORESERSICPARS")) ub = klog(rb, MINRADIUSB, MAXRADIUSB);
	#endif 

	record_put(q, t, "ue", ue);
	record_put(q, t, "phi", phi);
	record_put(q, t, "s2phi", s2phi);
	record_put(q, t, "c2phi", c2phi);
	record_put(q, t, "e", e);
	record_put(q, t, "E", E);
	record_put(q, t, "alpha", alpha);
	record_put(q, t, "beta", beta);

	if (!strcmp(pname, "CORESERSICPARS")) record_put(q, t, "ub", ub);
	#if DEEP_VERBOSE_CTRANSFORM
	printf("%s: (re, e, e-1, phi, E = %g, %g, %g, %g, %g), (V, W = %g, %g) \n", name, re, e, e-1.0, phi, E, V, W);
	#endif

} else if (sDesc & HYPERBOLIC) {
	THPIX v, w, re, rb;
	THPIX coshv, sinhv, expw, expnw, re2, ren2;
	THPIX ue, ub, e, E, phi, alpha, beta, s2phi, c2phi;
	THPIX av, bv, cv, aw, bw, cw, a, b, c;

	re = record_get(q, t, "re");
	if (!strcmp(pname, "CORESERSICPARS")) rb = record_get(q, t, "rb");
	v = record_get(q, t, "v");
	w = record_get(q, t, "w");
	
	coshv = cosh(v);
	sinhv = sinh(v);
	expw = exp(w);
	expnw = exp(-w);
	re2 = pow(re, 2.0);
	ren2 = pow(re, -2.0);

	a = ren2 * coshv * expw;
	c = ren2 * coshv * expnw;
	b = ren2 * sinhv;

	if (a != a || b != b || c != c) {
		thError("%s: ERROR - undefined values for (a, b, c, = %g, %g, %g) given (re, v, w  = %g, %g, %g)", name, a, b, c, re, v, w);
		return(SH_GENERIC_ERROR);
	}

	av = b * expw;
	cv = b * expnw;
	bv = ren2 * coshv;

	aw = a;
	cw = -c;
	bw = (THPIX) 0.0;

	record_put(q, t, "a", a);
	record_put(q, t, "b", b);
	record_put(q, t, "c", c);	
	record_put(q, t, "av", av);
	record_put(q, t, "bv", bv);
	record_put(q, t, "cv", cv);
	record_put(q, t, "aw", aw);
	record_put(q, t, "bw", bw);
	record_put(q, t, "cw", cw);
			
	THPIX D = pow(a - c, 2) + 4.0 * pow(b, 2);
	if (D < 0) {
		thError("%s: ERROR - negative discriminant (D) for (re, v, w = %g, %g, %g)", name, re, v, w);
		return(SH_GENERIC_ERROR);
	}
	alpha = (a + c) - sqrt(D);
	beta = (a + c) + sqrt(D);
	
	e = 1.0 - alpha / beta;
	if (e < 0.0 || beta == 0.0) {
		thError("%s: ERROR - could not calculate (e) for (re, v, w = %g, %g, %g)", name, re, v, w);
		return(SH_GENERIC_ERROR);
	}
	e = sqrt(e);
	if (e != e) {
		thError("%s: ERROR - undefined (e) for (re, v, w = %g, %g, %g)", name, re, v, w);
		return(SH_GENERIC_ERROR);
	}
	RET_CODE status;
	status = find_phi_from_abc(a, b, c, &phi);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not calculate 'phi' from (a, b, c)", name);
		return(status);
	}
	s2phi = sin(2.0 * phi);
	c2phi = cos(2.0 * phi);

	E = atanh(e / MAXELL);
	/*
	ue = (THPIX) log(((double) re-(double) MINRADIUS) / ((double) MAXRADIUS - (double) re));
	*/
	#if RLOG_U
	ue = ulog(re, MINRADIUS); /* , MAXRADIUS); */
	if (!strcmp(pname, "CORESERSICPARS")) ub = ulog(rb, MINRADIUSB);
	#else
	ue = klog(re, MINRADIUS, MAXRADIUS);
	if (!strcmp(pname, "CORESERSICPARS")) ub = klog(rb, MINRADIUSB, MAXRADIUSB);
	#endif 


	record_put(q, t, "ue", ue);
	record_put(q, t, "phi", phi);
	record_put(q, t, "s2phi", s2phi);
	record_put(q, t, "c2phi", c2phi);
	record_put(q, t, "e", e);
	record_put(q, t, "E", E);
	record_put(q, t, "alpha", alpha);
	record_put(q, t, "beta", beta);

	if (!strcmp(pname, "CORESERSICPARS")) record_put(q, t, "ub", ub);
	#if DEEP_VERBOSE_CTRANSFORM
	printf("%s: (re, e, e-1, phi, E = %g, %g, %g, %g, %g), (v, w = %g, %g) \n", name, re, e, e-1.0, phi, E, v, w);
	#endif


} else if (sDesc & CANONICAL) { /* meaning that (re, e, phi) should be given instead of (a, b, c) */

	THPIX re, rb, e, phi, ue, ub, E, v, w, alpha, beta, s2phi, c2phi;
	THPIX aphi, bphi, cphi, ae, be, ce, a, b, c;

	re = record_get(q, t, "re");
	if (!strcmp(pname, "CORESERSICPARS")) rb = record_get(q, t, "rb");
	e = record_get(q, t, "e");
	phi = record_get(q, t, "phi");
	/* 
	re = qq->re;
	e = qq->e;
	phi = qq->phi;
	*/

	#if RLOG_U
	ue = ulog(re, MINRADIUS); /* , MAXRADIUS); */
	if (!strcmp(pname, "CORESERSICPARS")) ub = ulog(rb, MINRADIUSB); /* , MAXRADIUS); */
	#else
	ue = klog(re, MINRADIUS, MAXRADIUS);
	if (!strcmp(pname, "CORESERSICPARS")) ub = klog(rb, MINRADIUSB, MAXRADIUSB);
	#endif 


	THPIX epsilon = sqrt(1.0 - pow(e, (THPIX) 2.0));
	E = atanh(e / MAXELL);
	
	alpha = epsilon / pow(re, 2);   /* 1 / a^2 */
	beta = 1.0 / pow(re, 2) / epsilon; /* 1 / b^2 */
	if (alpha != alpha || beta != beta) {
		thError("%s: ERROR - undefine value for intermediary (alpha) and (beta) for (e = %g), (re = %g)", name, e, re);
		return(SH_GENERIC_ERROR);
	}

	c2phi = cos(2.0 * phi);
	s2phi = sin(2.0 * phi);

	RET_CODE status;
	status = calculate_abc_abcphi_abce(alpha, beta, phi, e, &a, &b, &c, &aphi, &bphi, &cphi, &ae, &be, &ce);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not calculate (a, b, c) and it derivatives w.r.t. 'e' and 'phi'", name);
		return(status);
	}
	if (a != a || b != b || c != c) {
		thError("%s: ERROR - undefined values for (a, b, c = %g, %g, %g) given (re, e, e-1, phi = %g, %g, %g, %g)", name, a, b, c, re, e, e-1.0, phi);
		return(SH_GENERIC_ERROR);
	} 
	
	v = (THPIX) atanh((double) b / pow((double) a * (double) c, 0.5));
	w = (THPIX) (0.5 * log((double) a / (double) c));
	if (v != v || w != w) {
		thError("%s: ERROR - undefined values for (v, w = %g, %g) given (re, e, e-1, phi = %g, %g, %g, %g)", name, v, w, re, e, e-1.0, phi);
		return(SH_GENERIC_ERROR);
	}	

	THPIX v1, w1, V, W;	
	v1 = (v + w) * SQRT_HALF;
	w1 = (v - w) * SQRT_HALF;
	V = klog(v1, MIN_VW, MAX_VW);
	W = klog(w1, MIN_VW, MAX_VW);
	if (V != V || W != W) {
		thError("%s: ERROR - undefined values for (V, W = %g, %g) given (re, e, e-1, phi = %g, %g, %g, %g)", name, V, W, re, e, e-1.0, phi);
		return(SH_GENERIC_ERROR);
	}

	record_put(q, t, "alpha", alpha);
	record_put(q, t, "beta", beta);
	record_put(q, t, "c2phi", c2phi);
	record_put(q, t, "s2phi", s2phi);
	record_put(q, t, "a", a);
	record_put(q, t, "b", b);
	record_put(q, t, "c", c);
	record_put(q, t, "ae", ae);
	record_put(q, t, "be", be);
	record_put(q, t, "ce", ce);
	record_put(q, t, "aphi", aphi);
	record_put(q, t, "bphi", bphi);
	record_put(q, t, "cphi", cphi);
	record_put(q, t, "E", E);
	record_put(q, t, "ue", ue);	
	record_put(q, t, "v", v);
	record_put(q, t, "w", w);
	record_put(q, t, "V", V);
	record_put(q, t, "W", W);
	
	if (!strcmp(pname, "CORESERSICPARS")) record_put(q, t, "ub", ub);	
	#if DEEP_VERBOSE_CTRANSFORM
	printf("%s: (re, e, e-1, phi, E = %g, %g, %g, %g, %g), (ue, v, w, V, W = %g, %g, %g, %g, %g) \n", name, re, e, e-1.0, phi, E, ue, v, w, V, W);
	#endif
} else if (sDesc & ALGEBRAIC) {
	THPIX alpha, beta, e, phi, re, E, a, b, c;
	/* 
	a = qq->a;
	b = qq->b;
	c = qq->c;
	*/

	a = record_get(q, t, "a");
	b = record_get(q, t, "b");
	c = record_get(q, t, "c");

	THPIX D = pow(a - c, 2) + 4.0 * pow(b, 2);
	if (D < 0) {
		thError("%s: ERROR - negative discriminant (D) for (a, b, c = %g, %g, %g)", name, a, b, c);
		return(SH_GENERIC_ERROR);
	}
	alpha = (a + c) - sqrt(D);
	beta = (a + c) + sqrt(D);
	if (alpha != alpha || beta != beta) {
		thError("%s: ERROR - undefined intermediary variables (alpha, beta) for (a, b, c = %g, %g, %g)", name, a, b, c);
		return(SH_GENERIC_ERROR);
	}
	if (alpha * beta <= 0.0) {
		thError("%s: ERROR - cannot calculate (re) for (a, b, c, = %g, %g, %g)", name, a, b, c);
		return(SH_GENERIC_ERROR);
	}
	re = pow(alpha * beta, -0.25);
	if (re != re) {
		thError("%s: ERROR - undefined (re) for (a, b, c = %g, %g, %g)", name, a, b, c);
		return(SH_GENERIC_ERROR);
	}
	e = 1.0 - alpha / beta;
	if (e < 0.0 || beta == 0.0) {
		thError("%s: ERROR - could not calculate (e) for (a, b, c = %g, %g, %g)", name, a, b, c);
		return(SH_GENERIC_ERROR);
	}
	e = sqrt(e);
	if (e != e) {
		thError("%s: ERROR - undefined (e) for (a, b, c = %g, %g, %g)", name, a, b, c);
		return(SH_GENERIC_ERROR);
	}
	RET_CODE status;
	status = find_phi_from_abc(a, b, c, &phi);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not calculate 'phi' from (a, b, c)", name);
		return(status);
	}
	E = atanh(e / MAXELL);
	
	record_put(q, t, "re", re);
	record_put(q, t, "phi", phi);
	record_put(q, t, "e", e);
	record_put(q, t, "E", E);
	record_put(q, t, "alpha", alpha);
	record_put(q, t, "beta", beta);

	#if DEEP_VERBOSE_CTRANSFORM
	printf("%s: (re, e, phi, E = %g, %g, %g, %g) \n", name, re, e, phi, E);
	#endif

} else if (sDesc & SUPERCANONICAL) { 
	THPIX re, e, E, phi, alpha, beta, s2phi, c2phi;
	THPIX aphi, bphi, cphi, ae, be, ce, a, b, c;
	THPIX aE, bE, cE, eE, eEE;
 
	re = record_get(q, t, "re");
	E = record_get(q, t, "E");
	phi = record_get(q, t, "phi");

	e = MAXELL * tanh(E);
	eE = MAXELL * (1.0 - pow(e / MAXELL, 2));
	eEE = -2.0 * e / MAXELL * eE;

	alpha = (sqrt(1.0 - pow(e, 2.0))) / pow(re, 2.0);   /* 1 / a^2 */
	beta = 1.0 / pow(re, 2) / sqrt(1.0 - pow(e, 2.0)); /* 1 / b^2 */

	if (alpha != alpha || beta != beta) {
		thError("%s: ERROR - (alpha = %g, beta = %g) do not pass (NAN) test, (re, e, e-1, phi = %g, %g, %g, %g)", 
				name, alpha, beta, re, e, e-1.0, phi);
		return(SH_GENERIC_ERROR);
	}
	
	c2phi = cos(2.0 * phi);
	s2phi = sin(2.0 * phi);
	
	RET_CODE status;
	status = calculate_abc_abcphi_abce(alpha, beta, phi, e, &a, &b, &c, &aphi, &bphi, &cphi, &ae, &be, &ce);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not calculate (a, b, c) and it derivatives w.r.t. 'e' and 'phi'", name);
		return(status);
	}
	if (a != a || b != b || c != c) {
		thError("%s: ERROR - undefined values for (a, b, c = %g, %g, %g) given (re, e, e-1, phi = %g, %g, %g, %g)", name, a, b, c, re, e, e-1.0, phi);
		return(SH_GENERIC_ERROR);
	} 
	
	/* derivatives of (a, b, c) w.r.t. E */
	aE = ae * eE;
	bE = be * eE;
	cE = ce * eE;
 
	record_put(q, t, "alpha", alpha);
	record_put(q, t, "beta", beta);
	record_put(q, t, "c2phi", c2phi);
	record_put(q, t, "s2phi", s2phi);
	record_put(q, t, "a", a);
	record_put(q, t, "b", b);
	record_put(q, t, "c", c);
	record_put(q, t, "ae", ae);
	record_put(q, t, "be", be);
	record_put(q, t, "ce", ce);
	record_put(q, t, "aphi", aphi);
	record_put(q, t, "bphi", bphi);
	record_put(q, t, "cphi", cphi);
	record_put(q, t, "aE", aE);
	record_put(q, t, "bE", bE);
	record_put(q, t, "cE", cE);
	record_put(q, t, "e", e);
	record_put(q, t, "eE", eE);
	record_put(q, t, "eEE", eEE);

	#if DEBUG_CTRANSFORM
	printf("%s: DEBUG - re = %8.4g, phi = %8.4g, e = %8.4g, E = %8.4g \n", name, re, phi, e, E);
	printf("%s: DEBUG - a  = %8.4g, b   = %8.4g, c = %8.4g \n", name, a, b, c);
	double D = pow((double) a - (double) c, 2) + 4.0 * pow(b, 2);
	printf("%s: DEBUG - D  = %8.4lg \n", name, D);
	#endif

	#if DEEP_VERBOSE_CTRANSFORM
	printf("%s: (re, e, phi, E = %g, %g, %g, %g) \n", name, re, e, phi, E);
	#endif	
	} else if (sDesc & UBERCANONICAL) { 
	THPIX re, e, E, phi, alpha, beta, s2phi, c2phi;
	THPIX aphi, bphi, cphi, ae, be, ce, a, b, c;
	THPIX aE, bE, cE, eE, eEE;

	/* 
	re = qq->re;
	E = qq->E;
	phi = qq->phi;
	*/

	re = record_get(q, t, "re");
	E = record_get(q, t, "E");
	phi = record_get(q, t, "phi");

	/* e-E relation is the only difference between UBERCANONICAL and SUPERCANONICAL */
	e = 0.5 * (1.0 + tanh(E)) * MAXELL; 
	eE = 0.5 * MAXELL * (1.0 - pow(2.0 * e / MAXELL - 1.0, 2));
	eEE = -2.0 * eE * (2.0 * e / MAXELL - 1.0);

	alpha = (sqrt(1.0 - pow(e, 2))) / pow(re, 2);   /* 1 / a^2 */
	beta = 1.0 / pow(re, 2) / sqrt(1.0 - pow(e, 2)); /* 1 / b^2 */

	if (alpha != alpha || beta != beta) {
		thError("%s: ERROR - (alpha, beta = %g, %g) do not pass (NAN) test - (re, e, e-1, phi = %g, %g, %g, %g)", 
			name, alpha, beta, re, e, e-1.0, phi);
		return(SH_GENERIC_ERROR);
	}
	c2phi = cos(2.0 * phi);
	s2phi = sin(2.0 * phi);
	
	RET_CODE status;
	status = calculate_abc_abcphi_abce(alpha, beta, phi, e, &a, &b, &c, &aphi, &bphi, &cphi, &ae, &be, &ce);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not calculate (a, b, c) and it derivatives w.r.t. 'e' and 'phi'", name);
		return(status);
	}
	if (a != a || b != b || c != c) {
		thError("%s: ERROR - undefined values for (a, b, c = %g, %g, %g) given (re, e, e-1, phi = %g, %g, %g, %g)", name, a, b, c, re, e, e-1.0, phi);
		return(SH_GENERIC_ERROR);
	} 
	/* derivatives of (a, b, c) w.r.t. E */
	aE = ae * eE;
	bE = be * eE;
	cE = ce * eE;

	record_put(q, t, "alpha", alpha);
	record_put(q, t, "beta", beta);
	record_put(q, t, "c2phi", c2phi);
	record_put(q, t, "s2phi", s2phi);
	record_put(q, t, "a", a);
	record_put(q, t, "b", b);
	record_put(q, t, "c", c);
	record_put(q, t, "ae", ae);	
	record_put(q, t, "be", be);
	record_put(q, t, "ce", ce);
	record_put(q, t, "aphi", aphi);
	record_put(q, t, "bphi", bphi);
	record_put(q, t, "cphi", cphi);
	record_put(q, t, "aE", aE);
	record_put(q, t, "bE", bE);
	record_put(q, t, "cE", cE);
	record_put(q, t, "e", e);
	record_put(q, t, "eE", eE);
	record_put(q, t, "eEE", eEE);
	
	#if DEEP_VERBOSE_CTRANSFORM
	printf("%s: (re, e, phi, E = %g, %g, %g, %g) \n", name, re, e, phi, E);
	#endif
} else if (sDesc & SUPERCANONICAL_RSUPERLOG) { 
	THPIX ue, ub, re, rb, e, E, phi, alpha, beta, s2phi, c2phi;
	THPIX aphi, bphi, cphi, ae, be, ce, a, b, c;
	THPIX aE, bE, cE, eE, eEE, reUe, reUeUe, rbUb, rbUbUb;

	ue = record_get(q, t, "ue");
	if (!strcmp(pname, "CORESERSICPARS")) ub = record_get(q, t, "ub");
	E = record_get(q, t, "E");
	phi = record_get(q, t, "phi");

	#if RLOG_U
	re = uexp(ue, MINRADIUS); /* , MAXRADIUS); */
	reUe = Duexp(ue, MINRADIUS); /* , MAXRADIUS); */
	reUeUe = DDuexp(ue, MINRADIUS); /* ; , MAXRADIUS); */
	rb = uexp(ub, MINRADIUSB); /* , MAXRADIUS); */
	rbUb = Duexp(ub, MINRADIUSB); /* , MAXRADIUS); */
	rbUbUb = DDuexp(ub, MINRADIUSB); /* ; , MAXRADIUS); */
	#else
	re = kexp(ue, MINRADIUS, MAXRADIUS);
	reUe = Dkexp(ue, MINRADIUS, MAXRADIUS);
	reUeUe = DDkexp(ue, MINRADIUS, MAXRADIUS);
	rb = kexp(ub, MINRADIUSB, MAXRADIUSB);
	rbUb = Dkexp(ub, MINRADIUSB, MAXRADIUSB);
	rbUbUb = DDkexp(ub, MINRADIUSB, MAXRADIUSB);
	#endif


	e = MAXELL * tanh(E);
	eE = MAXELL * (1.0 - pow(e / MAXELL, 2));
	eEE = -2.0 * e / MAXELL * eE;

	alpha = (sqrt(1.0 - pow(e, 2))) / pow(re, 2);   /* 1 / a^2 */
	beta = 1.0 / pow(re, 2) / sqrt(1.0 - pow(e, 2)); /* 1 / b^2 */

	if (alpha != alpha || beta != beta) {
		thError("%s: ERROR - (alpha, beta = %g, %g) do not pass (NAN) test - (re, e, e-1, phi = %g, %g, %g, %g)", 
			name, alpha, beta, re, e, e-1.0, phi);
		return(SH_GENERIC_ERROR);
	}

	c2phi = cos(2.0 * phi);
	s2phi = sin(2.0 * phi);
	
	RET_CODE status;
	status = calculate_abc_abcphi_abce(alpha, beta, phi, e, &a, &b, &c, &aphi, &bphi, &cphi, &ae, &be, &ce);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not calculate (a, b, c) and it derivatives w.r.t. 'e' and 'phi'", name);
		return(status);
	}
	if (a != a || b != b || c != c) {
		thError("%s: ERROR - undefined values for (a, b, c = %g, %g, %g) given (re, e, e-1, phi = %g, %g, %g, %g)", name, a, b, c, re, e, e-1.0, phi);
		return(SH_GENERIC_ERROR);
	} 
	/* derivatives of (a, b, c) w.r.t. E */
	aE = ae * eE;
	bE = be * eE;
	cE = ce * eE;

	record_put(q, t, "alpha", alpha);
	record_put(q, t, "beta", beta);
	record_put(q, t, "c2phi", c2phi);
	record_put(q, t, "s2phi", s2phi);
	record_put(q, t, "a", a);
	record_put(q, t, "b", b);
	record_put(q, t, "c", c);
	record_put(q, t, "ae", ae);
	record_put(q, t, "be", be);
	record_put(q, t, "ce", ce);
	record_put(q, t, "aphi", aphi);
	record_put(q, t, "bphi", bphi);
	record_put(q, t, "cphi", cphi);
	record_put(q, t, "aE", aE);
	record_put(q, t, "bE", bE);
	record_put(q, t, "cE", cE);
	record_put(q, t, "e", e);
	record_put(q, t, "eE", eE);
	record_put(q, t, "eEE", eEE);
	record_put(q, t, "re", re);
	record_put(q, t, "reUe", reUe);
	record_put(q, t, "reUeUe", reUeUe);

	if (!strcmp(pname, "CORESERSICPARS")) record_put(q, t, "rb", rb);
	if (!strcmp(pname, "CORESERSICPARS")) record_put(q, t, "rbUb", rbUb);
	if (!strcmp(pname, "CORESERSICPARS")) record_put(q, t, "rbUbUb", rbUbUb);


	#if DEBUG_CTRANSFORM
	printf("%s: DEBUG - re = %8.4g, phi = %8.4g, e = %8.4g, E = %8.4g \n", name, re, phi, e, E);
	printf("%s: DEBUG - a  = %8.4g, b   = %8.4g, c = %8.4g \n", name, a, b, c);
	double D = pow((double) a - (double) c, 2) + 4.0 * pow(b, 2);
	printf("%s: DEBUG - D  = %8.4lg \n", name, D);
	#endif
	
	#if DEEP_VERBOSE_CTRANSFORM
	printf("%s: (re, e, phi, E = %g, %g, %g, %g) \n", name, re, e, phi, E);
	#endif
} else {
	thError("%s: ERROR - (sDesc) not supported", name);
	return(SH_GENERIC_ERROR);
}


if (!strcmp(pname, "CORESERSICPARS")) {
	THPIX r50 = VALUE_IS_BAD;
	THPIX k50 = VALUE_IS_BAD;
	status = find_coresersic_r50_k50(q, &r50, &k50);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not find (r50, k50) for 'CORESERSIC' variable", name);
		return(status);
	}
	record_put(q, t, "r50", r50);	
	record_put(q, t, "k50", k50);	
}

return(SH_SUCCESS);
}

RET_CODE find_sersic_k_kn_gamma_2n(THPIX n, THPIX *k, THPIX *kn, THPIX *gamma_2n, THPIX *ln_gamma_2n) {
char *name = "find_sersic_k_kn_gamma_2n";

if (k == NULL || kn == NULL || gamma_2n == NULL || ln_gamma_2n == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}

MATHDBLE db = 0.0, b = 0.0;
shAssert(n > 0.0);

#if 1
if (n >= (THPIX) 1.0) {
	int m = 5;
	MATHDBLE a[5] = {
	(MATHDBLE) -1.0 / (MATHDBLE) 3.0, 
	(MATHDBLE) 4.0 / (MATHDBLE) 405.0, 
	(MATHDBLE) 46.0 / (MATHDBLE) 255115.0, 
	(MATHDBLE) 131.0 / (MATHDBLE) 1148175.0, 
	(MATHDBLE) -2194697.0 / (MATHDBLE) 30690717750.0};
	int i;
	for (i = m-1; i >= 0; i--) {
		MATHDBLE coeff = a[i];
		b += b / (MATHDBLE) n + coeff;
		db = db / (MATHDBLE) n + coeff * (MATHDBLE) (-i);
	}
	db = (MATHDBLE) 2.0 + db / (MATHDBLE) n;
	b = b + (MATHDBLE) 2.0 * (MATHDBLE) n;
} else if (n >= (THPIX) 0.175) {
	b = (MATHDBLE) 2.0 * (MATHDBLE) n - (MATHDBLE) 1.0 / (MATHDBLE) 3.0 + (MATHDBLE) 0.00119 * pow((MATHDBLE) n, (MATHDBLE) -1.149);
	db = (MATHDBLE) 2.0 + (MATHDBLE) 0.00119 * (MATHDBLE) (-1.149) * pow((MATHDBLE) n, (MATHDBLE) -2.149);
} else if (n >= (THPIX) 0.035) {
	b = (MATHDBLE) 2.0 * (MATHDBLE) n - (MATHDBLE) 1.0 / (MATHDBLE) 3.0 - (MATHDBLE) 1.794 * (MATHDBLE) n + (MATHDBLE) 0.327; 
	db = (MATHDBLE) 2.0 - (MATHDBLE) 1.794;
} else {
	thError("%s: WARNING - (n = %g) is too small for this function", name, (float) n);
	b = (MATHDBLE) 0.0;
	db = (MATHDBLE) 0.0;
}
#else
b = 2.0 * n - 1.0 / 3.0;
db = 2.0;
#endif
double two_times_n = (double) 2.0 * (double) n;
double gamma_2n_const = thGamma(two_times_n);
double ln_gamma_2n_const = thLnGamma(two_times_n);
int is_nan_gamma = (isnan(gamma_2n_const) || isinf(gamma_2n_const)) && (isnan(ln_gamma_2n_const) || isinf(ln_gamma_2n_const)); 
if (is_nan_gamma) {
	thError("%s: ERROR - (NAN) value detectectd in gamma(%g) = %G, ln_gamma(%g) = %G", name, (float) two_times_n, (double) gamma_2n_const, (float) two_times_n, (double) ln_gamma_2n_const);
	return(SH_GENERIC_ERROR);
}

#if 0 
if (gamma_2n_const < two_times_n - 1.0) {
	thError("%s: ERROR - unacceptable value of gamma(%g) = %g", name, (float) two_times_n, (float) gamma_2n_const);
  	return(SH_GENERIC_ERROR);
}
#endif
if (b <= (MATHDBLE) 0.0 || isinf(b) || isnan(b)) {
	thError("%s: ERROR - (NAN / zero) value detected in k_n for (n = %g, k_n = %g, dk/dn = %g) \n", name, (float) n, (float) b, (float) db);
	return(SH_GENERIC_ERROR);
}
if (k != NULL) *k = (THPIX) b;
if (kn != NULL) *kn = (THPIX) db;
if (gamma_2n != NULL) *gamma_2n = (THPIX) gamma_2n_const;
if (ln_gamma_2n != NULL) *ln_gamma_2n = (THPIX) ln_gamma_2n_const;
return(SH_SUCCESS);
}


RET_CODE find_coresersic_r50_k50(void *q, THPIX *r50, THPIX *k50) {
char *name = "find_coresersic_r50_k50";
if (q == NULL || r50 == NULL || k50 == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}

*r50 = VALUE_IS_BAD;
*k50 = VALUE_IS_BAD;

return(SH_SUCCESS);
THPIX re = record_get(q, shTypeGetFromName("CORESERSICPARS"), "re");
THPIX k_value = record_get(q, shTypeGetFromName("CORESERSICPARS"), "k");
THPIX n = record_get(q, shTypeGetFromName("CORESERSICPARS"), "n");

THPIX x50_i = re;
THPIX x_inf = re * CORESERSIC_NEWTON_INTEGRAL_VERYLARGE_XRATIO;
THPIX f50_i = f(x50_i, q);
THPIX f_total = midpoint_integral(&coresersic_radial_integrand, 
	0.0, x_inf, q, CORESERSIC_NEWTON_INTEGRAL_VERYLARGE_NDIV);
THPIX g50_i = midpoint_integral(&coresersic_radial_integrand, 
	0.0, x50_i, q, CORESERSIC_NEWTON_INTEGRAL_LARGE_NDIV) - 0.5 * f_total;
THPIX x50_ip1, g50_ip1; 
int i = 0, converged = 0;

THPIX delta_x, delta_g;
while (!converged && i < CORESERSIC_NEWTON_CONVERGENCE_IMAX) {
	x50_ip1 = x50_i - (g50_i) / f50_i;
	g50_ip1 = g50_i + midpoint_integral(&coresersic_radial_integrand, 
	x50_i, x50_ip1, q, CORESERSIC_NEWTON_INTEGRAL_SMALL_NDIV);

	delta_x = fabs(x50_ip1 - x50_i) / re;
	delta_g = fabs(g50_ip1) / f_total;
	if (delta_x < CORESERSIC_NEWTON_CONVERGNENCE_EPSILONX || delta_g < CORESERSIC_NEWTON_CONVERGENCE_EPSILONY) converged = 1;
	if (converged) {
		g50_ip1 = midpoint_integral(&coresersic_radial_integrand, 
		0.0, x50_ip1, q, CORESERSIC_NEWTON_INTEGRAL_LARGE_NDIV);
		delta_g = fabs(g50_ip1) / f_total;
		converged = 0;
		if (delta_x < CORESERSIC_NEWTON_CONVERGNENCE_EPSILONX || delta_g < CORESERSIC_NEWTON_CONVERGENCE_EPSILONY) converged = 1;
	}
	x50_i = x50_ip1;
	g50_i = g50_ip1;
	f50_i = f(x50_i, q);	
	i++;
}
if (!converged) {
	g50_ip1 = midpoint_integral(&coresersic_radial_integrand, 
	0.0, x50_ip1, q, CORESERSIC_NEWTON_INTEGRAL_VERYLARGE_NDIV);
	delta_g = fabs(g50_ip1) / f_total;
	if (delta_x < CORESERSIC_NEWTON_CONVERGNENCE_EPSILONX || delta_g < CORESERSIC_NEWTON_CONVERGENCE_EPSILONY) converged = 1;
	if (!converged) thError("%s: WARNING - reached (i = %d) with no convergence, |dx| = %g, |g(x)| = %g", name, i, delta_x, delta_g);
}

THPIX r50_value = x50_i;
THPIX k50_value = k_value * pow((r50_value / re), 1.0 / n);

*r50 = r50_value;
*k50 = k50_value;
return(SH_SUCCESS);
}

THPIX midpoint_integral(void *func_ptr, THPIX a, THPIX b, void *q, int ndiv) {
int i;
shAssert(ndiv >= 1);
if (b == a) return((THPIX) 0.0);

THPIX (*func) (THPIX, void *) = func_ptr;
THPIX delta = (b - a) / ((THPIX) ndiv);
THPIX x = a + delta / 2.0;
THPIX y_total, y_error = 0.0;
for (i = 0; i < ndiv; i++) {
	THPIX y_item = func(x, q) * delta - y_error;
	THPIX y_total2 = y_total + y_item;
	y_error = (y_total2 - y_total) - y_item;
	y_total = y_total2;
	x += delta;
}

return(y_total);
}


THPIX coresersic_radial_integrand(THPIX r, void *q) {
shAssert(r > 0.0);
CORESERSICPARS *qq = (CORESERSICPARS *) q;

THPIX qq_delta = qq->delta;
THPIX value = pow((1.0 + pow((qq->rb / r), qq_delta)), qq->gamma / qq_delta) * exp(-qq->k * pow(((pow(qq->rb, qq_delta) + pow(r, qq_delta)) / pow(qq->re, qq_delta)), 1.0 / (qq->n * qq_delta))) * r;
return(value);
}

int has_sersic_index(char *pname) {
char *name = "has_sersic_index";
if (pname == NULL || strlen(pname) == 0) {
	thError("%s: WARNING - null or empty (pname)", name);
	return(0);
}
if (!strcmp(pname, "SERSICPARS") || !strcmp(pname, "CORESERSICPARS") || !strcmp(pname, "SERSIC1PARS") || !strcmp(pname, "SERSIC2PARS")) return(1);

return(0);
}

RET_CODE get_index_bounds(char *pname, void *pvalue, THPIX *index_small, THPIX *index_large) {
char *name = "get_index_bounds";
if (pname == NULL || strlen(pname) == 0) {
	thError("%s: ERROR - null or empty input", name);
	return(SH_GENERIC_ERROR);
}
if (index_small == NULL || index_large == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (!strcmp(pname, "SERSICPARS")) {
	SERSICPARS *psersic = pvalue;
	if (psersic == NULL) {
		thError("%s: WARNING - null variable passed", name);
		*index_small = SERSIC_INDEX_SMALL;
		*index_large = SERSIC_INDEX_LARGE;
	} else if (psersic->prv_cid == 0) {
		#if DEBUG_CTRANSFORM
		printf("%s: SERSICPARS \n", name);
		#endif
		*index_small = SERSIC_INDEX_SMALL;
		*index_large = SERSIC_INDEX_LARGE;
	} else if (psersic->prv_cid == 1) {
		#if DEBUG_CTRANSFORM
		printf("%s: SERSIC1PARS \n", name);
		#endif
		*index_small = SERSIC1_INDEX_SMALL;
		*index_large = SERSIC1_INDEX_LARGE;
	} else if (psersic->prv_cid == 2) {
		#if DEBUG_CTRANSFORM
		printf("%s: SERSIC2PARS \n", name);
		#endif
		*index_small = SERSIC2_INDEX_SMALL;
		*index_large = SERSIC2_INDEX_LARGE;
	} else {
		thError("%s: WARNING - unsupported (prv_cid = %d) in '%s'", psersic->prv_cid, pname);
		*index_small = SERSIC_INDEX_SMALL;
		*index_large = SERSIC_INDEX_LARGE;
	}
} else if (!strcmp(pname, "CORESERSICPARS")) {
	*index_small = CORESERSIC_INDEX_SMALL;
	*index_large = CORESERSIC_INDEX_LARGE;
} else {
	thError("%s: ERROR - pname = '%s' is not a supported", name, pname);
	*index_small = THNAN;
	*index_large = THNAN;
	return(SH_GENERIC_ERROR);
}
return(SH_SUCCESS);
}


RET_CODE SetCTransformStaticPars(char *pname, THPIX value) {
char *name = "SetCTransformStaticPars";
if (pname == NULL || strlen(pname) == 0) {
	thError("%s: null or empty name for static variable", name);
	return(SH_GENERIC_ERROR);
}
if (isnan(value) || isinf(value)) {
	thError("%s: ERROR - no support for (value = %g)", name, (float) value);
	return(SH_GENERIC_ERROR);
}

if (!strcmp(pname, "SERSIC_INDEX_SMALL")) {
	SERSIC_INDEX_SMALL = value;
} else if (!strcmp(pname, "CORESERSIC_INDEX_SMALL")) {
	CORESERSIC_INDEX_SMALL = value;
} else if (!strcmp(pname, "SERSIC1_INDEX_SMALL")) {
	SERSIC1_INDEX_SMALL = value;
} else if (!strcmp(pname, "SERSIC2_INDEX_SMALL")) {
	SERSIC2_INDEX_SMALL = value;
} else if (!strcmp(pname, "SERSIC_INDEX_LARGE")) { 
	SERSIC_INDEX_LARGE = value;
} else if (!strcmp(pname, "CORESERSIC_INDEX_LARGE")) {
	CORESERSIC_INDEX_LARGE = value;
} else if (!strcmp(pname, "SERSIC1_INDEX_LARGE")) {
	SERSIC1_INDEX_LARGE = value;
} else if (!strcmp(pname, "SERSIC2_INDEX_LARGE")) {
	SERSIC2_INDEX_LARGE = value;
} else {
	thError("%s: ERROR - static variable '%s' not supported", name, pname);
	return(SH_GENERIC_ERROR);
}
return(SH_SUCCESS);
}


RET_CODE initCTransformPars(void *input) {
char *name = "initCTransformPars";

if (input == NULL) {
	thError("%s: WARNING - null parsed input passed", name);
	return(SH_SUCCESS);
}

char *input_name = "CTRANSFORM_PARSED_INPUT";
TYPE input_type = shTypeGetFromName(input_name);
if (input_type == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - schema support not available for '%s'", name, input_name);
	return(SH_GENERIC_ERROR);
}

SCHEMA *sin = shSchemaGetFromType(input_type);
int n = sin->nelem;

int i;
RET_CODE status;
for (i = 0; i < n; i++) {
	SCHEMA_ELEM *se = sin->elems + i;
	char *lpname = se->name;
	if (lpname != NULL && lpname[0] == 'l') {
		int *lvalue_ptr = shElemGet(input, se, NULL);
		int lvalue = 0;
		char *pname = lpname + 1;
		se = shSchemaElemGetFromType(input_type, pname);
		THPIX *value_ptr = NULL; 
		if ((lvalue_ptr != NULL) && ((lvalue = *lvalue_ptr) == 1) && 
		se != NULL && (value_ptr = shElemGet(input, se, NULL)) != NULL) {
			THPIX value = *value_ptr;
			printf("%s: setting '%s' = %g \n", name, pname, (float) value);	
			status = SetCTransformStaticPars(pname, value);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not set CTRANSFORM parameter '%s'", name, pname);
				return(status);
			}
		}
			
		
	}
}

return(SH_SUCCESS);
}
	
