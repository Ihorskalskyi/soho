#include "thDebug.h"
#include "thMGProfile.h"
#include "thPsf.h"
#include "math.h"

static const SCHEMA *schema_fabrication_flag = NULL;
static const SCHEMA *schema_production_flag = NULL;
static const SCHEMA *schema_thregion_type = NULL;

static THPIX GAUSSIAN_LUMCUT =  DEFAULT_GAUSSIAN_LUMCUT;
static THPIX DEV_LUMCUT = DEFAULT_DEV_LUMCUT;
static THPIX SERSIC_LUMCUT = DEFAULT_SERSIC_LUMCUT;
static THPIX EXP_LUMCUT =  DEFAULT_EXP_LUMCUT;
static THPIX PL_LUMCUT = DEFAULT_PL_LUMCUT;
static THPIX DEV_INNER_LUMCUT = DEFAULT_DEV_INNER_LUMCUT;
static THPIX EXP_INNER_LUMCUT = DEFAULT_EXP_INNER_LUMCUT;
static THPIX PL_INNER_LUMCUT = DEFAULT_PL_INNER_LUMCUT;
static THPIX SERSIC_INNER_LUMCUT = DEFAULT_SERSIC_INNER_LUMCUT;
static THPIX CORESERSIC_INNER_LUMCUT = DEFAULT_CORESERSIC_INNER_LUMCUT;
static THPIX DEV_OUTER_LUMCUT = DEFAULT_DEV_OUTER_LUMCUT; 
static THPIX EXP_OUTER_LUMCUT = DEFAULT_EXP_OUTER_LUMCUT; 
static THPIX PL_OUTER_LUMCUT = DEFAULT_PL_OUTER_LUMCUT; 
static THPIX SERSIC_OUTER_LUMCUT = DEFAULT_SERSIC_OUTER_LUMCUT; 
static THPIX CORESERSIC_OUTER_LUMCUT = DEFAULT_CORESERSIC_OUTER_LUMCUT; 

static REGION *wreg1 = NULL;
static REGION *wreg2 = NULL;

static void init_static_vars(void);	

static RET_CODE profile_maker(void *q, char *qtype, void *pixel_function, void *set_reg, REGION *reg);
/* setting region up for various profilers */
static RET_CODE set_area_for_region(REGION *reg, THPIX xc, THPIX yc, 
				int nrow, int ncol, 
				int *idr, int *idc);
static RET_CODE generic_get_reg_pars(void *q, void *qname, void *g_ptr, void *p_ptr, PSF_CONVOLUTION_INFO *psf_info, int *idr, int *idc, int *nrow, int *ncol, THPIX *xc, THPIX *yc);

static RET_CODE central_star_get_reg_pars(void *q, PSF_CONVOLUTION_INFO *psf_info, 
				int *idr, int *idc, 
				int *nrow, int *ncol, 
				THPIX *xc, THPIX *yc);
static RET_CODE wing_star_get_reg_pars(void *q, PSF_CONVOLUTION_INFO *psf_info,
				int *idr, int *idc, 
				int *nrow, int *ncol, 
				THPIX *xc, THPIX *yc);
static RET_CODE gaussian_get_reg_pars(void *q, PSF_CONVOLUTION_INFO *psf_info, int *idr, int *idc, 
				int *nrow, int *ncol, 
				THPIX *xc, THPIX *yc);
static RET_CODE deV_get_reg_pars(void *q, PSF_CONVOLUTION_INFO *psf_info, int *idr, int *idc, 
				int *nrow, int *ncol, 
				THPIX *xc, THPIX *yc);
static RET_CODE Exp_get_reg_pars(void *q, PSF_CONVOLUTION_INFO *psf_info, int *idr, int *idc, 
				int *nrow, int *ncol,
				THPIX *xc, THPIX *yc);
static RET_CODE Pl_get_reg_pars(void *q, PSF_CONVOLUTION_INFO *psf_info, int *idr, int *idc, 
				int *nrow, int *ncol,
				THPIX *xc, THPIX *yc);
static RET_CODE sersic_get_reg_pars(void *q, PSF_CONVOLUTION_INFO *psf_info, int *idr, int *idc, 
				int *nrow, int *ncol,
				THPIX *xc, THPIX *yc);

static RET_CODE gaussian_set_reg(void *q, PSF_CONVOLUTION_INFO *psf_info, REGION *reg, int *idr, int *idc);
static RET_CODE deV_set_reg(void *q, PSF_CONVOLUTION_INFO *psf_info, REGION *reg, int *idr, int *idc);
static RET_CODE Exp_set_reg(void *q, PSF_CONVOLUTION_INFO *psf_info, REGION *reg, int *idr, int *idc);
static RET_CODE Pl_set_reg(void *q, PSF_CONVOLUTION_INFO *psf_info, REGION *reg, int *idr, int *idc);

/* calculating any of the gaussians */
RET_CODE thregion_star_profile_maker(void *q, char *qtype, 
		void *f_pixel, THREGION *reg, int index);
RET_CODE thregion_galaxy_profile_maker(void *q, char *qtype, 
		void *f_pixel, THREGION *reg, int index);
static RET_CODE gaussian_profile_maker(void *q, void *gaussian_pixel, THREGION *reg, int index);
static RET_CODE deV_profile_maker(void *q, void *deV_pixel, THREGION *reg, int index);
static RET_CODE Exp_profile_maker(void *q, void *Exp_pixel, THREGION *reg, int index);
static RET_CODE Pl_profile_maker(void *q, void *Pl_pixel, THREGION *reg, int index);
static RET_CODE sersic_profile_maker(void *q, void *sersic_pixel, THREGION *reg, int index);
static RET_CODE coresersic_profile_maker(void *q, void *coresersic_pixel, THREGION *reg, int index);

/* all the nulls */
static RET_CODE new_null_profile_maker(void *q, char *qtype, void *pixel_function, REGION *reg);
static RET_CODE null_profile_maker(void *q, void *null_pixel, THREGION *reg, int index);
static RET_CODE Null_get_reg_pars(void *q, int *idr, int *idc, int *nrow, int *ncol, THPIX *xc, THPIX *yc);
static RET_CODE null_set_reg(void *q, PSF_CONVOLUTION_INFO *psf_info, REGION *reg, int *idr, int *idc);
static RET_CODE null_profile_maker(void *q, void *null_pixel, THREGION *reg, int index);
static THPIX null_function(PIXPOS *p, void *q);
static RET_CODE thregion_null_profile_maker(void *q, char *qtype, void *f_pixel, THREGION *reg, int index);


/* pixel functions */
static THPIX delta(PIXPOS *p, void *q);
static THPIX flat(PIXPOS *p, void *q);

/* 
static THPIX gaussian(PIXPOS *p, void *q);
*/
static THPIX DgaussianDa(PIXPOS *p, void *q);
static THPIX DgaussianDb(PIXPOS *p, void *q);
static THPIX DgaussianDc(PIXPOS *p, void *q);
static THPIX DgaussianDxc(PIXPOS *p, void *q);
static THPIX DgaussianDyc(PIXPOS *p, void *q);
static THPIX DgaussianDphi(PIXPOS *p, void *q);
static THPIX DgaussianDe(PIXPOS *p, void *q);
static THPIX DgaussianDre(PIXPOS *p, void *q);
static THPIX DgaussianDue(PIXPOS *p, void *q);
static THPIX DgaussianDE(PIXPOS *p, void *q);

/* 
static THPIX deV(PIXPOS *p, void *q);
*/
static THPIX DdeVDa(PIXPOS *p, void *q);
static THPIX DdeVDb(PIXPOS *p, void *q);
static THPIX DdeVDc(PIXPOS *p, void *q);
static THPIX DdeVDxc(PIXPOS *p, void *q);
static THPIX DdeVDyc(PIXPOS *p, void *q);
static THPIX DdeVDphi(PIXPOS *p, void *q);
static THPIX DdeVDe(PIXPOS *p, void *q);
static THPIX DdeVDre(PIXPOS *p, void *q);
static THPIX DdeVDue(PIXPOS *p, void *q);
static THPIX DdeVDE(PIXPOS *p, void *q);
static THPIX DdeVDv(PIXPOS *p, void *q);
static THPIX DdeVDw(PIXPOS *p, void *q);
static THPIX DdeVDV(PIXPOS *p, void *q);
static THPIX DdeVDW(PIXPOS *p, void *q);

static THPIX DDdeVDreDre(PIXPOS *p, void *q);
static THPIX DDdeVDueDue(PIXPOS *p, void *q);

/* 
static THPIX Exp(PIXPOS *p, void *q);
*/
static THPIX DExpDa(PIXPOS *p, void *q);
static THPIX DExpDb(PIXPOS *p, void *q);
static THPIX DExpDc(PIXPOS *p, void *q);
static THPIX DExpDxc(PIXPOS *p, void *q);
static THPIX DExpDyc(PIXPOS *p, void *q);
static THPIX DExpDphi(PIXPOS *p, void *q);
static THPIX DExpDe(PIXPOS *p, void *q);
static THPIX DExpDre(PIXPOS *p, void *q);
static THPIX DExpDue(PIXPOS *p, void *q);
static THPIX DExpDE(PIXPOS *p, void *q);
static THPIX DExpDv(PIXPOS *p, void *q);
static THPIX DExpDw(PIXPOS *p, void *q);
static THPIX DExpDV(PIXPOS *p, void *q);
static THPIX DExpDW(PIXPOS *p, void *q);

/* 
static THPIX Pl(PIXPOS *p, void *q);
*/
static THPIX DPlDa(PIXPOS *p, void *q);
static THPIX DPlDb(PIXPOS *p, void *q);
static THPIX DPlDc(PIXPOS *p, void *q);
static THPIX DPlDxc(PIXPOS *p, void *q);
static THPIX DPlDyc(PIXPOS *p, void *q);
static THPIX DPlDphi(PIXPOS *p, void *q);
static THPIX DPlDe(PIXPOS *p, void *q);
static THPIX DPlDre(PIXPOS *p, void *q);
static THPIX DPlDue(PIXPOS *p, void *q);
static THPIX DPlDE(PIXPOS *p, void *q);
static THPIX DPlDv(PIXPOS *p, void *q);
static THPIX DPlDw(PIXPOS *p, void *q);
static THPIX DPlDV(PIXPOS *p, void *q);
static THPIX DPlDW(PIXPOS *p, void *q);

static THPIX DDPlDreDre(PIXPOS *p, void *q);
static THPIX DDPlDueDue(PIXPOS *p, void *q);

/* 
 * static THPIX sersic(PIXPOS *p, void *q);
* static THPIX sersicStep(PIXPOS *p, void *q);
*/
static THPIX DsersicDa(PIXPOS *p, void *q);
static THPIX DsersicDb(PIXPOS *p, void *q);
static THPIX DsersicDc(PIXPOS *p, void *q);
static THPIX DsersicDxc(PIXPOS *p, void *q);
static THPIX DsersicDyc(PIXPOS *p, void *q);
static THPIX DsersicDre(PIXPOS *p, void *q);
static THPIX DsersicDue(PIXPOS *p, void *q);
static THPIX DsersicDe(PIXPOS *p, void *q);
static THPIX DsersicDphi(PIXPOS *p, void *q);
static THPIX DsersicDE(PIXPOS *p, void *q);
static THPIX DsersicDv(PIXPOS *p, void *q);
static THPIX DsersicDw(PIXPOS *p, void *q);
static THPIX DsersicDV(PIXPOS *p, void *q);
static THPIX DsersicDW(PIXPOS *p, void *q);
static THPIX DsersicDn(PIXPOS *p, void *q);
static THPIX DsersicDN(PIXPOS *p, void *q);
static THPIX DDsersicDreDre(PIXPOS *p, void *q);
static THPIX DDsersicDueDue(PIXPOS *p, void *q);

/* 
static THPIX coresersic(PIXPOS *p, void *q);
*/
static THPIX DcoresersicDre(PIXPOS *p, void *q);
static THPIX DcoresersicDue(PIXPOS *p, void *q);
static THPIX DcoresersicDrb(PIXPOS *p, void *q);
static THPIX DcoresersicDub(PIXPOS *p, void *q);
static THPIX DcoresersicDxc(PIXPOS *p, void *q);
static THPIX DcoresersicDyc(PIXPOS *p, void *q);
static THPIX DcoresersicDphi(PIXPOS *p, void *q);
static THPIX DcoresersicDe(PIXPOS *p, void *q);
static THPIX DcoresersicDE(PIXPOS *p, void *q);
static THPIX DcoresersicDgamma(PIXPOS *p, void *q);
static THPIX DcoresersicDG(PIXPOS *p, void *q);
static THPIX DcoresersicDdelta(PIXPOS *p, void *q);
static THPIX DcoresersicDD(PIXPOS *p, void *q);
static THPIX DcoresersicDn(PIXPOS *p, void *q);
static THPIX DcoresersicDN(PIXPOS *p, void *q);

static QUADFL coresersic_basic_function_w(QUADFL x, QUADFL w, QUADFL x0, QUADFL x0b, QUADFL a, QUADFL b, QUADFL c);

static void init_static_vars(void);
static THPIX record_get(void *q, TYPE t, char *r);
static void record_put(void *q, TYPE t, char *r, THPIX y);	

static RET_CODE adjust_lumcuts(void *q, char *qname, THPIX system_inner_lumcut, THPIX system_outer_lumcut);
static RET_CODE q_get_lumcut(void *q, char *qname, THPIX *inner_lumcut, THPIX *outer_lumcut);

static REGION *thRegIntShift(REGION *out, const REGION *in, REGION *scr, int filtsize, float dr, float dc);

static void  make_n_from_db(THPIX det, double db, int nmax, int nmin, int *n);

static void make_nrow_ncol_from_brow_bcol(THPIX det, double dbrow, double dbcol, int *nrow, int *ncol);
void init_static_vars(void) {
	TYPE t;
	t = shTypeGetFromName("FABRICATION_FLAG");
	schema_fabrication_flag = shSchemaGetFromType(t);
	shAssert(schema_fabrication_flag != NULL);
	t = shTypeGetFromName("PRODUCTION_FLAG");
	schema_production_flag = shSchemaGetFromType(t);
	shAssert(schema_production_flag != NULL);
	t = shTypeGetFromName("THREGION_TYPE");
	schema_thregion_type = shSchemaGetFromType(t);
	shAssert(schema_thregion_type != NULL);
	/* work space */
	if (wreg1 != NULL) shRegDel(wreg1);
	if (wreg2 != NULL) shRegDel(wreg2);
	wreg1 = shRegNew("thMGProfile work space 1", MAXNROW, MAXNCOL, TYPE_THPIX);	
	wreg2 = shRegNew("thMGProfile work space 2", MAXNROW, MAXNCOL, TYPE_THPIX);

	return;
}

void  make_n_from_db(THPIX det, double db, int nmax, int nmin, int *n) {
shAssert(n != NULL);
int n0 = -1;
if (det != (THPIX) 0.0 && db < (double) nmax) {
	n0 = (int) db;
	if (n0 < nmin) n0 = nmin;
	if (n0 % 2 == 0) n0++; 
} else {
	n0 = nmax;
	if ((n0 % 2) == 0) n0--;
}
*n = n0;
return;
}


void make_nrow_ncol_from_brow_bcol(THPIX det, double dbrow, double dbcol, int *nrow, int *ncol) {
shAssert(nrow != NULL);
shAssert(ncol != NULL);

make_n_from_db(det, dbrow, MAXNROW, MINNROW, nrow);
make_n_from_db(det, dbcol, MAXNCOL, MINNCOL, ncol);
return;
}

void unload_work_space(void) {
	if (wreg1 != NULL) shRegDel(wreg1);
	if (wreg2 != NULL) shRegDel(wreg2);
	wreg1 = NULL;
	wreg2 = NULL;
	return;
}

THPIX record_get(void *q, TYPE t, char *r) {
	shAssert(q != NULL);
	shAssert(t != UNKNOWN_SCHEMA);
	shAssert(r != NULL);
	shAssert(strlen(r) != 0);

	THPIX *x;
	SCHEMA_ELEM *se;
	se = shSchemaElemGetFromType(t, r);
	shAssert(se != NULL);
	x = shElemGet(q, se, NULL);
return(*x);
}

void record_put(void *q, TYPE t, char *r, THPIX y) {
	shAssert(q != NULL);
	shAssert(t != UNKNOWN_SCHEMA);
	shAssert(r != NULL);
	shAssert(strlen(r) != 0);
	
	THPIX *x;
	SCHEMA_ELEM *se;
	se = shSchemaElemGetFromType(t, r);
	shAssert(se != NULL);
	x = shElemGet(q, se, NULL);
	*x = y;
return;
}


RET_CODE MGInitProfile(CDESCRIPTION cdesc, void *input) {
char *name = "MGInitProfile";
RET_CODE status;
status = thCTransformInit(cdesc, input);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not initiate the (Coordinate Transformation) module", name);
	return(status);
}
status = InitQuad();
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not initiate (quadrature)", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE new_star_profile_maker(void *q, char *qtype, void *pixel_function, REGION *reg) {
char *name = "new_star_profile_maker";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL || pixel_function == NULL) {
	thError("%s: ERROR - null (q) or (f)", name);
	return(SH_GENERIC_ERROR);
}
if (qtype == NULL || strlen(qtype) == 0) {
	thError("%s: ERROR - empty patameter type string (qtype)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
int idr, idc;
idr = -(reg->row0);
idc = -(reg->col0);

int nQ;
QUADRATURE **Q;
Q = SetProfileQuadrature(&nQ);

status = CreateProfile(pixel_function, q, qtype, reg, idr, idc, Q, nQ);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not create profile", name);
	return(status);
}

return(SH_SUCCESS);
} 


RET_CODE new_null_profile_maker(void *q, char *qtype, void *pixel_function, REGION *reg) {
char *name = "new_null_profile_maker";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL || pixel_function == NULL) {
	thError("%s: ERROR - null (q) or (f)", name);
	return(SH_GENERIC_ERROR);
}
if (qtype == NULL || strlen(qtype) == 0) {
	thError("%s: ERROR - empty patameter type string (qtype)", name);
	return(SH_GENERIC_ERROR);
}

#if 0
RET_CODE status;
status = thCTransform(q, qtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - failed to transform elliptical description properly", name);
	return(status);
}

int idr, idc;
idr = -(reg->row0);
idc = -(reg->col0);

int nQ;
QUADRATURE **Q;
Q = SetProfileQuadrature(&nQ);

status = CreateProfile(pixel_function, q, qtype, reg, idr, idc, Q, nQ);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not create profile", name);
	return(status);
}

#else
int nrow, ncol;
nrow = reg->nrow;
ncol = reg->ncol;
if (nrow > 0 && ncol > 0) shRegClear(reg);
#endif

return(SH_SUCCESS);
} 






RET_CODE new_galaxy_profile_maker(void *q, char *qtype, void *pixel_function, REGION *reg) {
char *name = "new_galaxy_profile_maker";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL || pixel_function == NULL) {
	thError("%s: ERROR - null (q) or (f)", name);
	return(SH_GENERIC_ERROR);
}
if (qtype == NULL || strlen(qtype) == 0) {
	thError("%s: ERROR - empty patameter type string (qtype)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
status = thCTransform(q, qtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - failed to transform elliptical description properly", name);
	return(status);
}

int idr, idc;
idr = -(reg->row0);
idc = -(reg->col0);

int nQ;
QUADRATURE **Q;
Q = SetProfileQuadrature(&nQ);

status = CreateProfile(pixel_function, q, qtype, reg, idr, idc, Q, nQ);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not create profile", name);
	return(status);
}

return(SH_SUCCESS);
} 



RET_CODE profile_maker(void *q, char *qtype, void *pixel_function, void *set_reg, REGION *reg) {
char *name = "profile_maker";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL || pixel_function == NULL || set_reg == NULL) {
	thError("%s: ERROR - null (q), (f) or (set_reg)", name);
	return(SH_GENERIC_ERROR);
}
if (qtype == NULL || strlen(qtype) == 0) {
	thError("%s: ERROR - empty patameter type string (qtype)", name);
	return(SH_GENERIC_ERROR);
	}

THPIX (*f) (PIXPOS *, void *);
f = pixel_function;

RET_CODE status;
status = thCTransform(q, qtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - failed to transform elliptical description properly", name);
	return(status);
}

RET_CODE (*g) (void *, REGION *, int *, int *);
int idr, idc;
g = set_reg;
status = g(q, reg, &idr, &idc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not set up the region properly", name);
	return(status);
}

int nQ;
QUADRATURE **Q;
Q = SetProfileQuadrature(&nQ);

status = CreateProfile(f, q, qtype, reg, idr, idc, Q, nQ);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not create profile", name);
	return(status);
}

return(SH_SUCCESS);
} 


RET_CODE central_star_get_reg_pars(void *q, PSF_CONVOLUTION_INFO *psf_info, int *idr, int *idc, int *nrow, int *ncol, 
				THPIX *xc, THPIX *yc) {
char *name = "central_star_get_reg_pars";
if (q == NULL) {
	thError("%s: ERROR - null parameter (q)", name);
	return(SH_GENERIC_ERROR);
}
if (idr == NULL && idc == NULL && nrow == NULL && ncol == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}

int nrow0, ncol0;
nrow0 = OUTER_PSF_SIZE;
ncol0 = OUTER_PSF_SIZE;

STARPARS *qq = (STARPARS *) q;

if (nrow != NULL) *nrow = nrow0;
if (ncol != NULL) *ncol = ncol0;
/* changed to fit the common understanding that ROW is Y and COL is X */
if (idr != NULL) *idr = (int) (nrow0 / 2.0 - qq->xc);
if (idc != NULL) *idc = (int) (ncol0 / 2.0 - qq->yc);
if (xc != NULL) *xc = qq->xc;
if (yc != NULL) *yc = qq->yc;

return(SH_SUCCESS);
}

RET_CODE wing_star_get_reg_pars(void *q, PSF_CONVOLUTION_INFO *psf_info, int *idr, int *idc, int *nrow, int *ncol, THPIX *xc, THPIX *yc) {
char *name = "wing_star_get_reg_pars";
if (q == NULL) {
	thError("%s: ERROR - null parameter (q)", name);
	return(SH_GENERIC_ERROR);
}
if (idr == NULL && idc == NULL && nrow == NULL && ncol == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}


int nrow0, ncol0;
if (psf_info == NULL) {
	nrow0 = INNER_PSF_SIZE;
	ncol0 = INNER_PSF_SIZE;
} else {
	WPSF *wpsf = psf_info->wpsf;
	if (wpsf == NULL) {
		thError("%s: ERROR - null (wpsf) in (psf_info)", name);
		return(SH_GENERIC_ERROR);
	}
	PSFMODEL *psf_model = wpsf->psf_model;
	if (psf_model == NULL) {
		thError("%s: ERROR - null (psf_model) in (psf_info->wpsf)", name);
		return(SH_GENERIC_ERROR);
	}
	PSFWING *wing = psf_model->wing;
	if (wing == NULL) {
		thError("%s: ERROR - null (wing) in (psf_model)", name);
		return(SH_GENERIC_ERROR);
	}
	REGION *wreg = wing->reg;
	if (wreg == NULL) {
		thError("%s: ERROR - null wing shape in (wpsf)", name);
		return(SH_GENERIC_ERROR);
	}
	nrow0 = wreg->nrow;
	ncol0 = wreg->ncol;
	if (nrow0 == 0 || ncol0 == 0) {
		thError("%s: WARNING - wing image found with (nrow, ncol) = (%d, %d)", name, nrow0, ncol0);
	}
}

STARPARS *qq = (STARPARS *) q;

if (nrow != NULL) *nrow = nrow0;
if (ncol != NULL) *ncol = ncol0;
/* changed to fit the common understanding that ROW is Y and COL is X */
if (idr != NULL) *idr = (int) (nrow0 / 2.0 - qq->xc);
if (idc != NULL) *idc = (int) (ncol0 / 2.0 - qq->yc);
if (xc != NULL) *xc = qq->xc;
if (yc != NULL) *yc = qq->yc;

return(SH_SUCCESS);
}

RET_CODE gaussian_get_reg_pars(void *q, PSF_CONVOLUTION_INFO *psf_info, int *idr, int *idc, int *nrow, int *ncol,
				THPIX *xc, THPIX *yc) {
char *name = "gaussian_get_reg_pars";
if (q == NULL) {
	thError("%s: ERROR - null parameter (q)", name);
	return(SH_GENERIC_ERROR);
}
if (idr == NULL && idc == NULL && nrow == NULL && ncol == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}

GAUSSIANPARS *qq = (GAUSSIANPARS *) q;
THPIX det = qq->a * qq->c - pow(qq->b, 2);
if (det <= (THPIX) 0.0) {
	thError("%s: ERROR - unacceptable discriminant (D = %g)", name, det);
	return(SH_GENERIC_ERROR);
}
if (det != det) {
	thError("%s: ERROR - undefined discriminant for (a, b, c = %g, %g, %g)", name, qq->a, qq->b, qq->c);
	return(SH_GENERIC_ERROR);
} 
/* finding the truncation radius */
THPIX rho, g;
rho = -log(GAUSSIAN_LUMCUT);
g = pow(rho, 0.5);

#if DEBUG_MGPROFILE
printf("%s: luminosity cut for (%g) of the total light was made at (%g) of 're' \n", name, GAUSSIAN_LUMCUT, g);
#endif

double dbrow, dbcol;
dbrow = 1.5 + 2.0 * (GAUSSIAN_PSF_MARGIN + g * pow(fabs(qq->c / det), 0.5));
dbcol = 1.5 + 2.0 * (GAUSSIAN_PSF_MARGIN + g * pow(fabs(qq->a / det), 0.5));

int nrow0, ncol0;
make_nrow_ncol_from_brow_bcol(det, dbrow, dbcol, &nrow0, &ncol0);
/* 
if (det != (THPIX) 0.0 && dbrow <= MAXNROW) {
	nrow0 = (int) dbrow;
} else {
	nrow0 = MAXNROW;
}
if (det != (THPIX) 0.0 && dbcol <= MAXNCOL) {
	ncol0 = (int) dbcol;
} else {
	ncol0 = MAXNCOL;
}
*/

if (nrow != NULL) *nrow = nrow0;
if (ncol != NULL) *ncol = ncol0;
/* changed to fit the common understanding that ROW is Y and COL is X */
if (idr != NULL) *idr = (int) (nrow0 / 2.0 - qq->xc);
if (idc != NULL) *idc = (int) (ncol0 / 2.0 - qq->yc);
if (xc != NULL) *xc = qq->xc;
if (yc != NULL) *yc = qq->yc;

return(SH_SUCCESS);
}

RET_CODE null_get_reg_pars(void *q, void *qname, void *g_ptr, int *idr, int *idc, int *nrow, int *ncol, THPIX *xc, THPIX *yc) {
char *name = "null_get_reg_pars";
if (idr == NULL && idc == NULL && nrow == NULL && ncol == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
RET_CODE status;
int row0_in = 0, col0_in = 0, nrow_in = 0, ncol_in = 0, margin_in = 0;
int row0_out = 0, col0_out = 0, nrow_out = 0, ncol_out = 0, margin_out = 0;
#if DEEP_DEBUG_MGPROFILE
printf("%s: inner-region: cutoff = %10.2g, (nrow, ncol = %d, %d), (row0, col0 = %d, %d), margin = %d) \n", name, cutoff_in, nrow_in, ncol_in, row0_in, col0_in, margin_in);
printf("%s: outer-region: cutoff = %10.2g, (nrow, ncol = %d, %d), (row0, col0 = %d, %d), margin = %d) \n", name, cutoff_out, nrow_out, ncol_out, row0_out, col0_out, margin_out);
#endif
status = finalize_inner_region_chars(row0_in, col0_in, nrow_in, ncol_in,
				row0_out, col0_out, nrow_out, ncol_out,
				&row0_in, &col0_in, &nrow_in, &ncol_in);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not finalize inner region's parameters", name);
	return(status);
}
/* taking into account the PSF margins */
int row0_f, col0_f, nrow_f, ncol_f;
status = finalize_working_region_chars(row0_in, col0_in, nrow_in, ncol_in, margin_in,
				row0_out, col0_out, nrow_out, ncol_out, margin_out, 
				&row0_f, &col0_f, &nrow_f, &ncol_f);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not finalize working region characteristics", name);
	return(status);
}

#if DEEP_DEBUG_MGPROFILE
printf("%s: finalized inner region: cutoff = %10.2g, (nrow, ncol = %d, %d), (row0, col0 = %d, %d), margin = %d) \n", name, cutoff_in, nrow_in, ncol_in, row0_in, col0_in, margin_in);
printf("%s: finalized       region: (nrow, ncol = %d, %d), (row0, col0 = %d, %d) \n", name, nrow_f, ncol_f, row0_f, col0_f);
#endif

if (nrow != NULL) *nrow = nrow_f;
if (ncol != NULL) *ncol = ncol_f;
if (idr != NULL) *idr = -row0_f;
if (idc != NULL) *idc = -col0_f;

if (xc != NULL) *xc = (THPIX) XC_NULL;
if (yc != NULL) *yc = (THPIX) YC_NULL;

return(SH_SUCCESS);
}

RET_CODE generic_get_reg_pars(void *q, void *qname, void *g_ptr, void *p_ptr, PSF_CONVOLUTION_INFO *psf_info, int *idr, int *idc, int *nrow, int *ncol, THPIX *xc, THPIX *yc) {
char *name = "generic_get_reg_pars";
if (q == NULL || qname == NULL || strlen(qname) == 0 || g_ptr == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (idr == NULL && idc == NULL && nrow == NULL && ncol == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
TYPE t;
t = shTypeGetFromName(qname);
if (t == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - variable type '%s' is not supported by any schema", name, qname);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
THPIX cutoff_in, cutoff_out;
status = q_get_lumcut(q, qname, &cutoff_in, &cutoff_out);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get luminosity cuts from (%s)", name, qname);
	return(status);
}
int row0_in, col0_in, nrow_in, ncol_in, margin_in;
int row0_out, col0_out, nrow_out, ncol_out, margin_out;

THPIX cutoff_psf_in;
int margin_in_max, margin_in_min, margin_in_default;
margin_in_max = (INNER_PSF_SIZE + 1) / 2;
margin_in_min = (INNER_PSF_SIZE_LOW + 1) / 2;
margin_in_default = (INNER_PSF_SIZE + 1) / 2;
cutoff_psf_in = CUTOFF_INNER_PSF_DN;

status = thPsfConvInfoGetMargin(q, qname, p_ptr, psf_info, cutoff_psf_in, margin_in_min, margin_in_max, margin_in_default, &margin_in);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (margin) for (inner) part of the profile (cutoff = %g)", name, (float) cutoff_psf_in);
	return(status);
}

status = derive_profile_region_chars(q, qname, g_ptr, cutoff_in, 
			&row0_in, &col0_in, &nrow_in, &ncol_in);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not derive region characteristic for (inner) part of the profile (cutoff = %g)", name, cutoff_in);
	return(status);
}

THPIX cutoff_psf_out;
int margin_out_max, margin_out_min, margin_out_default;
margin_out_max = (OUTER_PSF_SIZE + 1) / 2;
margin_out_min = (OUTER_PSF_SIZE + 1) / 2;
margin_out_default = (OUTER_PSF_SIZE + 1) / 2;
cutoff_psf_out = CUTOFF_OUTER_PSF_DN;

status = thPsfConvInfoGetMargin(q, qname, p_ptr, psf_info, cutoff_psf_out, margin_out_min, margin_out_max, margin_out_default, &margin_out);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (margin) for (outer) part of the profile (cutoff = %g)", name, (float) cutoff_psf_out);
	return(status);
}

status = derive_profile_region_chars(q, qname, g_ptr, cutoff_out,
			&row0_out, &col0_out, &nrow_out, &ncol_out);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not derive region characteristic for (outer) part of the profile (cutoff = %g)", name, cutoff_out);
	return(status);
}
#if DEEP_DEBUG_MGPROFILE
printf("%s: inner-region: cutoff = %10.2g, (nrow, ncol = %d, %d), (row0, col0 = %d, %d), margin = %d) \n", name, cutoff_in, nrow_in, ncol_in, row0_in, col0_in, margin_in);
printf("%s: outer-region: cutoff = %10.2g, (nrow, ncol = %d, %d), (row0, col0 = %d, %d), margin = %d) \n", name, cutoff_out, nrow_out, ncol_out, row0_out, col0_out, margin_out);
#endif
status = finalize_inner_region_chars(row0_in, col0_in, nrow_in, ncol_in,
				row0_out, col0_out, nrow_out, ncol_out,
				&row0_in, &col0_in, &nrow_in, &ncol_in);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not finalize inner region's parameters", name);
	return(status);
}
/* taking into account the PSF margins */
int row0_f, col0_f, nrow_f, ncol_f;
status = finalize_working_region_chars(row0_in, col0_in, nrow_in, ncol_in, margin_in,
				row0_out, col0_out, nrow_out, ncol_out, margin_out, 
				&row0_f, &col0_f, &nrow_f, &ncol_f);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not finalize working region characteristics", name);
	return(status);
}

#if DEEP_DEBUG_MGPROFILE
printf("%s: finalized inner region: cutoff = %10.2g, (nrow, ncol = %d, %d), (row0, col0 = %d, %d), margin = %d) \n", name, cutoff_in, nrow_in, ncol_in, row0_in, col0_in, margin_in);
printf("%s: finalized       region: (nrow, ncol = %d, %d), (row0, col0 = %d, %d) \n", name, nrow_f, ncol_f, row0_f, col0_f);
#endif

if (nrow_in%2 != 1 || ncol_in%2 != 1) {	
	thError("%s: WARNING - found even in-size (nrow_in = %d, ncol_in = %d) for (qname = '%s')", name, nrow_in, ncol_in, qname);
}
if (nrow_out%2 != 1 || ncol_out%2 != 1) {	
	thError("%s: WARNING - found even out-size (nrow-out = %d, ncol-out = %d) for (qname = '%s')", name, nrow_out, ncol_out, qname);
}
if (nrow_f%2 != 1 || ncol_f%2 != 1) {
	thError("%s: WARNING - found even f-size (nrow-f = %d, ncol-f = %d) for (qname = '%s')", name, nrow_f, ncol_f, qname);
}

if (nrow != NULL) *nrow = nrow_f;
if (ncol != NULL) *ncol = ncol_f;
if (idr != NULL) *idr = -row0_f;
if (idc != NULL) *idc = -col0_f;

THPIX xc_val, yc_val;
xc_val = record_get(q, t, "xc");
yc_val = record_get(q, t, "yc");  
if (xc != NULL) *xc = xc_val;
if (yc != NULL) *yc = yc_val;

return(SH_SUCCESS);
}

RET_CODE Null_get_reg_pars(void *q, int *idr, int *idc, 
				int *nrow, int *ncol,
				THPIX *xc, THPIX *yc) {
char *name = "Null_get_reg_pars";
char *qname = "";
RET_CODE status;

status = null_get_reg_pars(q, qname, NULL, idr, idc, nrow, ncol, xc, yc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not specifiy region pars for (cutoff = %g, %g)", 
		name, (float) DEV_INNER_LUMCUT, (float) DEV_OUTER_LUMCUT);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE deV_get_reg_pars(void *q, PSF_CONVOLUTION_INFO *psf_info,
				int *idr, int *idc, 
				int *nrow, int *ncol,
				THPIX *xc, THPIX *yc) {
char *name = "deV_get_reg_pars";
char *qname = "DEVPARS";
RET_CODE status;

status = adjust_lumcuts(q, qname, DEV_INNER_LUMCUT, DEV_OUTER_LUMCUT);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not adjust lumcuts", name);
	return(status);
}
status = generic_get_reg_pars(q, qname, &g_deV, &p_deV, psf_info, idr, idc, nrow, ncol, xc, yc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not specifiy region pars for (cutoff = %g, %g)", 
		name, (float) DEV_INNER_LUMCUT, (float) DEV_OUTER_LUMCUT);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE Exp_get_reg_pars(void *q, PSF_CONVOLUTION_INFO *psf_info, 
				int *idr, int *idc, 
				int *nrow, int *ncol,
				THPIX *xc, THPIX *yc) {
char *name = "Exp_get_reg_pars";
char *qname = "EXPPARS";
RET_CODE status;
status = adjust_lumcuts(q, qname, EXP_INNER_LUMCUT, EXP_OUTER_LUMCUT);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not adjust lumcuts", name);
	return(status);
}
status = generic_get_reg_pars(q, qname, &g_exp, &p_exp, psf_info, idr, idc, nrow, ncol, xc, yc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not specifiy region pars for (cutoff = %g, %g)", 
		name, (float) EXP_INNER_LUMCUT, (float) EXP_OUTER_LUMCUT);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE Pl_get_reg_pars(void *q, PSF_CONVOLUTION_INFO *psf_info,
				int *idr, int *idc, 
				int *nrow, int *ncol,
				THPIX *xc, THPIX *yc) {
char *name = "Pl_get_reg_pars";
char *qname = "POWERLAWPARS";
RET_CODE status;
status = adjust_lumcuts(q, qname, PL_INNER_LUMCUT, PL_OUTER_LUMCUT);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not adjust lumcuts", name);
	return(status);
}
status = generic_get_reg_pars(q, qname, &g_pl, &p_pl, psf_info, idr, idc, nrow, ncol, xc, yc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not specifiy region pars for (cutoff = %g, %g)", 
		name, (float) PL_INNER_LUMCUT, (float) PL_OUTER_LUMCUT);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE sersic_get_reg_pars(void *q, PSF_CONVOLUTION_INFO *psf_info,
				int *idr, int *idc, 
				int *nrow, int *ncol,
				THPIX *xc, THPIX *yc) {
char *name = "sersic_get_reg_pars";
char *qname = "SERSICPARS";
RET_CODE status;

status = adjust_lumcuts(q, qname, (THPIX) SERSIC_INNER_LUMCUT, (THPIX) SERSIC_OUTER_LUMCUT);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not adjust lumcuts", name);
	return(status);
}
status = generic_get_reg_pars(q, qname, &g_sersic, &p_sersic, psf_info, idr, idc, nrow, ncol, xc, yc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not specifiy region pars for (cutoff = %g, %g)", 
		name, (float) SERSIC_INNER_LUMCUT, (float) SERSIC_OUTER_LUMCUT);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE coresersic_get_reg_pars(void *q, PSF_CONVOLUTION_INFO *psf_info,
				int *idr, int *idc, 
				int *nrow, int *ncol,
				THPIX *xc, THPIX *yc) {
char *name = "coresersic_get_reg_pars";
char *qname = "CORESERSICPARS";
RET_CODE status;

status = adjust_lumcuts(q, qname, (THPIX) CORESERSIC_INNER_LUMCUT, (THPIX) CORESERSIC_OUTER_LUMCUT);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not adjust lumcuts", name);
	return(status);
}
status = generic_get_reg_pars(q, qname, &g_coresersic, &p_coresersic, psf_info, idr, idc, nrow, ncol, xc, yc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not specifiy region pars for (cutoff = %g, %g)", 
		name, (float) CORESERSIC_INNER_LUMCUT, (float) CORESERSIC_OUTER_LUMCUT);
	return(status);
}

return(SH_SUCCESS);
}


RET_CODE set_area_for_region(REGION *reg, THPIX xc, THPIX yc, int nrow, int ncol,
				int *idr, int *idc) {

char *name = "set_area_for_region";
if (reg == NULL) {
	thError("%s: ERROR - null (region)", name);
	return(SH_GENERIC_ERROR);
}

double area1, area2;
area1 = (double) reg->nrow * (double) reg->ncol;
area2 = (double) nrow * (double) ncol;
if (reg->nrow < nrow || reg->ncol < ncol || 
	area1 > area2 * PROFILE_REG_AREA_FACTOR) {
	int i;
	thFree(reg->rows_thpix[0]);
	thFree(reg->rows_thpix);
	reg->rows_thpix = thCalloc(nrow, sizeof(THPIX *));
	reg->rows_thpix[0] = thCalloc(nrow * ncol, sizeof(THPIX));
	for (i = 1; i < nrow; i++) {
		reg->rows_thpix[i] = reg->rows_thpix[0] + i * ncol;
	}
	reg->nrow = nrow;
	reg->ncol = ncol;
}

if (idr != NULL) *idr = (int) (reg->nrow / 2.0 - xc);
if (idc != NULL) *idc = (int) (reg->ncol / 2.0 - yc);

return(SH_SUCCESS);
}

RET_CODE gaussian_set_reg(void *q, PSF_CONVOLUTION_INFO *psf_info, REGION *reg, int *idr, int *idc) { 
char *name = "gaussian_set_reg";
if (q == NULL || reg == NULL) {
	thError("%s: ERROR - null parameter (q) or region (reg)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
char *qtype = "GAUSSIANPARS";
status = thCTransform(q, qtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - failed to transform elliptical description properly", name);
	return(status);
}

THPIX xc, yc;
int nrow, ncol;
status = gaussian_get_reg_pars(q, psf_info, idr, idc, &nrow, &ncol, &xc, &yc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the (region) parameters", name);
	return(status);
}

status = set_area_for_region(reg, xc, yc, nrow, ncol, idr, idc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not set the area properly", name);
	return(status);
}

return(SH_SUCCESS);
}


RET_CODE null_set_reg(void *q, PSF_CONVOLUTION_INFO *psf_info, REGION *reg, int *idr, int *idc) { 
char *name = "null_set_reg";
if (q == NULL || reg == NULL) {
	thError("%s: ERROR - null parameter (q) or region (reg)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;

THPIX xc, yc;
int nrow, ncol;
status = Null_get_reg_pars(q, idr, idc, &nrow, &ncol, &xc, &yc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the (region) parameters", name);
	return(status);
}

status = set_area_for_region(reg, xc, yc, nrow, ncol, idr, idc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not set the area properly", name);
	return(status);
}

return(SH_SUCCESS);
}



RET_CODE deV_set_reg(void *q, PSF_CONVOLUTION_INFO *psf_info, REGION *reg, int *idr, int *idc) { 
char *name = "deV_set_reg";
if (q == NULL || reg == NULL) {
	thError("%s: ERROR - null parameter (q) or region (reg)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
char *qtype = "DEVPARS";
status = thCTransform(q, qtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - failed to transform elliptical description properly", name);
	return(status);
}

THPIX xc, yc;
int nrow, ncol;
status = deV_get_reg_pars(q, psf_info, idr, idc, &nrow, &ncol, &xc, &yc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the (region) parameters", name);
	return(status);
}

status = set_area_for_region(reg, xc, yc, nrow, ncol, idr, idc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not set the area properly", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE Exp_set_reg(void *q, PSF_CONVOLUTION_INFO *psf_info, REGION *reg, int *idr, int *idc) { 
char *name = "Exp_set_reg";
if (q == NULL || reg == NULL) {
	thError("%s: ERROR - null parameter (q) or region (reg)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
char *qtype = "EXPPARS";
status = thCTransform(q, qtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - failed to transform elliptical description properly", name);
	return(status);
}

THPIX xc, yc;
int nrow, ncol;
status = Exp_get_reg_pars(q, psf_info, idr, idc, &nrow, &ncol, &xc, &yc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the (region) parameters", name);
	return(status);
}

status = set_area_for_region(reg, xc, yc, nrow, ncol, idr, idc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not set the area properly", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE Pl_set_reg(void *q, PSF_CONVOLUTION_INFO *psf_info, REGION *reg, int *idr, int *idc) { 
char *name = "Pl_set_reg";
if (q == NULL || reg == NULL) {
	thError("%s: ERROR - null parameter (q) or region (reg)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
char *qtype = "POWERLAWPARS";
status = thCTransform(q, qtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - failed to transform elliptical description properly", name);
	return(status);
}

THPIX xc, yc;
int nrow, ncol;
status = Pl_get_reg_pars(q, psf_info, idr, idc, &nrow, &ncol, &xc, &yc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the (region) parameters", name);
	return(status);
}

status = set_area_for_region(reg, xc, yc, nrow, ncol, idr, idc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not set the area properly", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE gaussian_profile_maker(void *q, void *gaussian_pixel, THREGION *reg, int index) {
char *name = "gaussian_profile_maker";
char *qtype = "GAUSSIANPARS";
RET_CODE status;
status = thregion_galaxy_profile_maker(q, qtype, gaussian_pixel, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make the requested profile", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE null_profile_maker(void *q, void *null_pixel, THREGION *reg, int index) {
char *name = "null_profile_maker";
char *qtype = "DEVPARS";
RET_CODE status;
status = thregion_null_profile_maker(q, qtype, null_pixel, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make the requested profile", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE deV_profile_maker(void *q, void *deV_pixel, THREGION *reg, int index) {
char *name = "deV_profile_maker";
char *qtype = "DEVPARS";
RET_CODE status;
status = thregion_galaxy_profile_maker(q, qtype, deV_pixel, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make the requested profile", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE Exp_profile_maker(void *q, void *Exp_pixel, THREGION *reg, int index) {
char *name = "Exp_profile_maker";
char *qtype = "EXPPARS";
RET_CODE status;
status = thregion_galaxy_profile_maker(q, qtype, Exp_pixel, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make the requested profile", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE Pl_profile_maker(void *q, void *Pl_pixel, THREGION *reg, int index) {
char *name = "Pl_profile_maker";
char *qtype = "POWERLAWPARS";
RET_CODE status;
status = thregion_galaxy_profile_maker(q, qtype, Pl_pixel, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make the requested profile", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE sersic_profile_maker(void *q, void *sersic_pixel, THREGION *reg, int index) {
char *name = "sersic_profile_maker";
char *qtype = "SERSICPARS";
RET_CODE status;
status = thregion_galaxy_profile_maker(q, qtype, sersic_pixel, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make the requested profile", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE coresersic_profile_maker(void *q, void *coresersic_pixel, THREGION *reg, int index) {
char *name = "coresersic_profile_maker";
char *qtype = "CORESERSICPARS";
RET_CODE status;
status = thregion_galaxy_profile_maker(q, qtype, coresersic_pixel, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make the requested profile", name);
	return(status);
}

return(SH_SUCCESS);
}




/* new adaptation of profile maker after May 2012 suitable for THREGION handling */
RET_CODE thregion_star_profile_maker(void *q, char *qtype, 
		void *f_pixel, THREGION *reg, int index) {
char *name = "thregion_star_profile_maker";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL || f_pixel == NULL) {
	thError("%s: ERROR - null variable (q) or pixel function (f_pixel)", name);
	return(SH_GENERIC_ERROR);
}
if (qtype == NULL || strlen(qtype) == 0) {
	thError("%s: ERROR - null or empty type string for model variable", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
REGION *lreg;
FABRICATION_FLAG fab;

status = thRegGetFabFlag(reg, &fab);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract (fabrication_flag)", name);
	return(status);
}
if (fab == PREFAB) {
	status = thRegFab(reg, NULL, NULL, NULL, index);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not FAB component (%d) of (thregion)", 
			name, index);
		return(status);
	}
} else if (fab != FAB) {
	if (schema_fabrication_flag == NULL) init_static_vars ();
	thError("%s: ERROR - (fabrication_flag) '%s' not supported", 
		name, (schema_fabrication_flag->elems[fab]).name);
	return(SH_GENERIC_ERROR);
}

status = thRegGetRegMaskA(reg, &lreg, NULL, NULL, &fab, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get image component for index (%d)", name, index);
	return(status);
}
if (fab != FAB) {
	if (schema_fabrication_flag == NULL) init_static_vars();
	thError("%s: ERROR - component (%d) of (thregion) should be FAB'ed - received '%s' instead", name, index, (schema_fabrication_flag->elems[fab]).name);
	return(SH_GENERIC_ERROR);
}
status = new_star_profile_maker(q, qtype, f_pixel, lreg);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make the requested profile", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE thregion_null_profile_maker(void *q, char *qtype, 
		void *f_pixel, THREGION *reg, int index) {
char *name = "thregion_null_profile_maker";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null variable (q) or pixel function (f_pixel)", name);
	return(SH_GENERIC_ERROR);
}
if (qtype == NULL || strlen(qtype) == 0) {
	thError("%s: ERROR - null or empty type string for model variable", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
REGION *lreg;
FABRICATION_FLAG fab;

status = thRegGetFabFlag(reg, &fab);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract (fabrication_flag)", name);
	return(status);
}
if (fab == PREFAB) {
	status = thRegFab(reg, NULL, NULL, NULL, index);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not FAB component (%d) of (thregion)", 
			name, index);
		return(status);
	}
} else if (fab != FAB) {
	if (schema_fabrication_flag == NULL) init_static_vars ();
	thError("%s: ERROR - (fabrication_flag) '%s' not supported", 
		name, (schema_fabrication_flag->elems[fab]).name);
	return(SH_GENERIC_ERROR);
}

status = thRegGetRegMaskA(reg, &lreg, NULL, NULL, &fab, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get image component for index (%d)", name, index);
	return(status);
}
if (fab != FAB) {
	if (schema_fabrication_flag == NULL) init_static_vars ();
	thError("%s: ERROR - component (%d) of (thregion) expected to be FAB'ed, received '%s' instead", name, index, (schema_fabrication_flag->elems[fab]).name);
	return(SH_GENERIC_ERROR);
}
status = new_null_profile_maker(q, qtype, f_pixel, lreg);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make the requested profile", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE thregion_galaxy_profile_maker(void *q, char *qtype, 
		void *f_pixel, THREGION *reg, int index) {
char *name = "thregion_galaxy_profile_maker";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL || f_pixel == NULL) {
	thError("%s: ERROR - null variable (q) or pixel function (f_pixel)", name);
	return(SH_GENERIC_ERROR);
}
if (qtype == NULL || strlen(qtype) == 0) {
	thError("%s: ERROR - null or empty type string for model variable", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
REGION *lreg;
FABRICATION_FLAG fab;

status = thRegGetFabFlag(reg, &fab);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract (fabrication_flag)", name);
	return(status);
}
if (fab == PREFAB) {
	status = thRegFab(reg, NULL, NULL, NULL, index);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not FAB component (%d) of (thregion)", 
			name, index);
		return(status);
	}
} else if (fab != FAB && fab != PSFCONVOLVED) { 
/* inserted PSFCONVOLVED flag permission to allow for lprofile construct during runs */
	if (schema_fabrication_flag == NULL) init_static_vars ();
	thError("%s: ERROR - (fabrication_flag) '%s' not supported", 
		name, (schema_fabrication_flag->elems[fab]).name);
	return(SH_GENERIC_ERROR);
}

status = thRegGetRegMaskA(reg, &lreg, NULL, NULL, &fab, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get image component for index (%d)", name, index);
	return(status);
}
if (fab != FAB && fab != PSFCONVOLVED) { 
/* inserted PSFCONVOLVED flag permission to allow for lprofile construct during runs */
	if (schema_fabrication_flag == NULL) init_static_vars ();
	thError("%s: ERROR - component (%d) of (thregion) expected to be FAB'ed, received '%s' instead", name, index, (schema_fabrication_flag->elems[fab]).name);
	return(SH_GENERIC_ERROR);
} else if (fab == PSFCONVOLVED) {
	thError("%s: WARNING - found 'PSFCONVOLVED' for (thregion[%d] while 'FAB' was prefered", name, index);
}

status = new_galaxy_profile_maker(q, qtype, f_pixel, lreg);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make the requested profile", name);
	return(status);
}

return(SH_SUCCESS);
}


/* profiles */

/* 
this is equivalent to a star not convolved with PSF 
we don't use this function for star representation
*/

RET_CODE starProf(void *q, THREGION *reg, THPIX *mcount, int index) {
char *name = "starProf";

if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null variable (q)", name);
	return(SH_GENERIC_ERROR);
}

char *qtype = "STARPARS";

RET_CODE status;
FABRICATION_FLAG fab;

PSF_CONVOLUTION_INFO *psf_info = NULL;
status = thRegGetPsfConvInfo(reg, &psf_info);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (psf_info) from (thregion)", name);
	return(status);
}

STARPARS *qq = q;
status = thPsfInfoPutCenter(psf_info, qq->xc, qq->yc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update center position for (psf_info)", name);
	return(status);
}

psf_info->conv_type = STAR_CENTRAL_AND_WINGS;
status = thPsfInfoUpdateWpsf(psf_info);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update (wpsf) in (psf_info)", name);
	return(status);
}

FFTW_KERNEL *fftw_kern = NULL;
status = thPsfInfoGetFftwKernel(psf_info, index, &fftw_kern);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (fftw_ken) from (psf_info, index: %d)", name, index);
	return(status);
}

PRODUCTION_FLAG production = UNKNOWN_PRODUCTION_FLAG;
status = thRegGetProduction(reg, &production, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - failed to extract production flag for index (%d)", name, index);
	return(status);
}

REGION *lreg = NULL;
status = thFftwKernelGetDirReg(fftw_kern, &lreg);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not fetch direct image from (fftw_kern)", name);
	return(status);
}


#if DEBUG_MGPROFILE
static int debugcpy = 0;
if (debugcpy <= 1) {
	char *dbgfile;
	if (debugcpy == 0) {
		dbgfile = "./fit/dbg-psf-0.fits";
	} else {		
		dbgfile = "./fit/dbg-psf-1.fits";
	}
	printf("%s: outputting psf to '%s' \n", name, dbgfile);
	shRegWriteAsFits(lreg, dbgfile, STANDARD, 2, DEF_NONE, NULL, 0);
	debugcpy++;
}
#endif

/* inserting the component amplitude */
THPIX a = fftw_kern->psf_a;
THREGION2 *reg2 = reg->threg;
if (reg2 == NULL) {
	thError("%s: ERROR - improperly allocated (thregion)", name);
	return(SH_GENERIC_ERROR);
}
reg2->a[index] = a;
#if DEEP_DEBUG_MGPROFILE
printf("%s: a[index = %d] = %g \n", name, index, a);
#endif

/* will pix copy if the index is prefab-ed as runtime */
if (production == SAVED) {
	status = thRegFab(reg, lreg, NULL, NULL, index);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not fabricate (thregion) at index (%d)", name, index);
		return(status);
	}
} else if (production == RUNTIME) {
	status = thRegFab(reg, NULL, NULL, NULL, index);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not fabricate (thregion)", name);
		return(status);
	}
	REGION *treg = NULL;
	status = thRegGetRegMaskA(reg, &treg, NULL, NULL, NULL, index);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get fabricated region for index (%d)", name);
		return(status);
	}

	int hrow, hcol;
	hrow = fftw_kern->hrow;
	hcol = fftw_kern->hcol;
	int lrow0, lcol0, trow0, tcol0;
	trow0 = treg->row0;
	tcol0 = treg->col0;
	treg->row0 = - (treg->nrow / 2);
	treg->col0 = - (treg->ncol / 2);
	lrow0 = lreg->row0;
	lcol0 = lreg->col0;
	lreg->row0 = - (lreg->nrow / 2);
	lreg->col0 = - (lreg->ncol / 2);
	int mrow, mcol;
	mrow = MIN(treg->nrow, lreg->nrow);
	mcol = MIN(treg->ncol, lreg->ncol);
	NAIVE_MASK mask0, *mask, hmask0, *hmask;
	mask = &mask0; /* to avoid fragmentation */
	status = thNaiveMaskFromRect(mask, -(mrow / 2), mrow / 2, -(mcol / 2), mcol / 2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not generate (mask) for pixcopy", name);
		return(status);
	}
	if (hrow != 0 && hcol != 0) {
		hmask = &hmask0;
		status = thNaiveMaskFromRect(hmask, -(hrow / 2), hrow / 2, - (hcol / 2), hcol / 2);
		 if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not construct (hmask)", name);
			return(status);
		}
		status = thNaiveMaskAndNotNaiveMask(mask, hmask);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not subtract (hmask) from (mask)", name);
			return(status);
		}
	}
	#define SHIFT_STAR_CORRECTLY 1
	#if SHIFT_STAR_CORRECTLY
	/* now copy contents of lreg onto treg */
	if (wreg1 == NULL || wreg2 == NULL) init_static_vars();
	int wrow0, wcol0;
	REGION *subwreg1, *subwreg2;
	subwreg1 = shSubRegNew(name, wreg1, lreg->nrow, lreg->ncol, 0, 0, NO_FLAGS);
	subwreg2 = shSubRegNew(name, wreg2, lreg->nrow, lreg->ncol, 0, 0, NO_FLAGS);
	wrow0 = subwreg1->row0;
	wcol0 = subwreg1->col0;
	subwreg1->row0 = lreg->row0;
	subwreg1->col0 = lreg->col0;
	status = shRegPixCopyMask(lreg, subwreg1, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy (lreg) into (subwreg1) using (NULL mask)", name);
		return(status);
	}
	#define SHIFT_STAR_FRACTIONAL 1	
	#if SHIFT_STAR_FRACTIONAL
	float dr, dc;
	dr = (float) qq->xc - (float) 0.5 - (int) qq->xc; /* subtracting 0.5 creates a -0.5 pixel bias */
	dc = (float) qq->yc - (float) 0.5 - (int) qq->yc; /* need to subtract 0.5 to reduce the bias */
	if (dr < (float) 0.0) dr += (float) 1.0; /* added on october 16, 2015 after it was proven that it is only stars that cause trouble when fitting for centers */
	if (dc < (float) 0.0) dc += (float) 1.0; /* added on october 16, 2015 after it was proven that it is only stars that cause trouble when fitting for centers */
	int filtsize = 10;
	thRegIntShift(subwreg1, subwreg1, subwreg2, filtsize, dr, dc);
	#endif	
	status = shRegPixCopyMask(subwreg1, treg, mask);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy (subwreg1) into (treg) using mask", name);
		return(status);
	}
	shRegDel(subwreg1);
	shRegDel(subwreg2);

	#else 
	status = shRegPixCopyMask(lreg, treg, mask);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy pixels to index (%d)", name, index);
		return(status);
	}
	#endif
	/* reversing row0, col0 */
	treg->row0 = trow0;
	treg->col0 = tcol0;
	lreg->row0 = lrow0;
	lreg->col0 = lcol0;

} else {
	thError("%s: ERROR - unsupported production flag '%s' for index (%d)", name, shEnumNameGetFromValue("PRODUCTION_FLAG", production), index);
	return(SH_GENERIC_ERROR);
}

THPIX modelcount = 1.0;
if (mcount != NULL) *mcount = modelcount;
qq->mcount = modelcount;

psf_info->conv_type = NO_PSF_CONVOLUTION;
return(SH_SUCCESS);

static int init_star_wings = 0;
static REGION *init_reg = NULL;
static int init_nrow_wing = INNER_PSF_SIZE;
static int init_ncol_wing = INNER_PSF_SIZE;
if (index == 0) {
	status = thregion_star_profile_maker(q, qtype, &flat, reg, index);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not make the inner part of the star", name);	
		return(status);
	}
	return(SH_SUCCESS);
} else if (index == 1) {
	int nrow, ncol;
	REGION *lreg;	
	if (!init_star_wings) {
		init_reg = shRegNew("star-wing", init_nrow_wing, init_ncol_wing, TYPE_THPIX);
		init_star_wings = 1;
	}
	status = thRegFab(reg, init_reg, NULL, NULL, index);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not FAB (thregion) and assign it an existing image", name);
		return(status);
	}
	return(SH_SUCCESS);
} else {
	thError("%s: ERROR - (index) of value (%d) not supported", name, index);
	return(SH_GENERIC_ERROR);
}

}

RET_CODE deltaProf(void *q, REGION *reg, THPIX *mcount) {

char *name = "deltaProf";
if (reg == NULL && mcount == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}

STARPARS *qq = (STARPARS *)q;

if (reg != NULL) {

int nrow, ncol;
double dbrow, dbcol;
dbrow = 1.5 + 2.0 * STAR_PSF_MARGIN;
dbcol = 1.5 + 2.0 * STAR_PSF_MARGIN;

make_nrow_ncol_from_brow_bcol((THPIX) 1.0, dbrow, dbcol, &nrow, &ncol);

/* if (dbrow < MAXNROW) {
	nrow = (int) dbrow;
} else {
	nrow = MAXNROW;
}
if (dbcol < MAXNCOL) {
	ncol = (int) dbcol;
} else {
	ncol = MAXNCOL;
}
*/

int i, j;
double area1, area2;
area1 = (double) reg->nrow * (double) reg->ncol;
area2 = (double) nrow * (double) ncol;
if (reg->nrow < nrow || reg->ncol < ncol || 
	area1 > area2 * PROFILE_REG_AREA_FACTOR) {
	thFree(reg->rows_thpix[0]);
	thFree(reg->rows_thpix);
	reg->rows_thpix = thCalloc(nrow, sizeof(THPIX *));
	reg->rows_thpix[0] = thCalloc(nrow * ncol, sizeof(THPIX));
	for (i = 1; i < nrow; i++) {
		reg->rows_thpix[i] = reg->rows_thpix[0] + i * ncol;
	}
	reg->nrow = nrow;
	reg->ncol = ncol;
} else {
	/* now setting the values of the pixels */
	nrow = reg->nrow;
	ncol = reg->ncol;
	THPIX *row;
	for (i = 0; i < nrow; i++) {
		row = reg->rows_thpix[i];
		for (j = 0; j < ncol; j++) {
			row[j] = (THPIX) 0.0;
		}
	}
}

/* changed to fit the common understanding that ROW is Y and COL is X */
int idr, idc;
idr = (int) (reg->nrow / 2.0 - qq->xc);
idc = (int) (reg->ncol / 2.0 - qq->yc);

reg->row0 = -idr;
reg->col0 = -idc;

/* changed to fit the common understanding that ROW is Y and COL is X */
i = (int) (qq->xc + (THPIX) idr);
j = (int) (qq->yc + (THPIX) idc);

reg->rows_thpix[i][j] = (THPIX) 1.0;
}

if (mcount != NULL) *mcount = (THPIX) 1.0;

return(SH_SUCCESS);
}



RET_CODE gaussianProf(void *q, THREGION *reg, THPIX *mcount, int index) {

char *name = "gaussianProf";
if (reg == NULL && mcount == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}

if (reg != NULL) {
RET_CODE status;
status = gaussian_profile_maker(q, &gaussian, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
}

/* setting up the model count */
THPIX modelcount = THNAN;
GAUSSIANPARS *qq = (GAUSSIANPARS *) q;
THPIX det = qq->a * qq->c - pow(qq->b, 2);
if (det != 0.0) {
	modelcount = (THPIX) TWOPI * pow(det, -0.5);
}
qq->mcount = modelcount;
if (mcount != NULL) *mcount = modelcount;

return(SH_SUCCESS);
}


RET_CODE DgaussianDaProf(void *q, THREGION *reg, int index) {

char *name = "DgaussianDaProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = gaussian_profile_maker(q, &DgaussianDa, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}


RET_CODE DgaussianDbProf(void *q, THREGION *reg, int index) {

char *name = "DgaussianDbProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
status = gaussian_profile_maker(q, &DgaussianDb, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}


RET_CODE DgaussianDcProf(void *q, THREGION *reg, int index) {

char *name = "DgaussianDcProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = gaussian_profile_maker(q, &DgaussianDc, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DgaussianDxcProf(void *q, THREGION *reg, int index) {

char *name = "DgaussianDxcProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = gaussian_profile_maker(q, &DgaussianDxc, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DgaussianDycProf(void *q, THREGION *reg, int index) {

char *name = "DgaussianDycProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = gaussian_profile_maker(q, &DgaussianDyc, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DgaussianDphiProf(void *q, THREGION *reg, int index) {

char *name = "DgaussianDphiProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = gaussian_profile_maker(q, &DgaussianDphi, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DgaussianDeProf(void *q, THREGION *reg, int index) {

char *name = "DgaussianDeProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = gaussian_profile_maker(q, &DgaussianDe, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DgaussianDreProf(void *q, THREGION *reg, int index) {
char *name = "DgaussianDreProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = gaussian_profile_maker(q, &DgaussianDre, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DgaussianDueProf(void *q, THREGION *reg, int index) {
char *name = "DgaussianDueProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = gaussian_profile_maker(q, &DgaussianDue, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DgaussianDEProf(void *q, THREGION *reg, int index) {

char *name = "DgaussianDEProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = gaussian_profile_maker(q, &DgaussianDE, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE nullProf(void *q, THREGION *reg, int index) {

char *name = "nullProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = null_profile_maker(q, &null_function, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE deVProf(void *q, THREGION *reg, THPIX *mcount, int index) {

char *name = "deVProf";
if (reg == NULL && mcount == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}

if (reg != NULL) {
RET_CODE status;
status = deV_profile_maker(q, &deV, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
}

THPIX modelcount;
DEVPARS *qq = (DEVPARS *) q;
THPIX det = qq->a * qq->c - pow(qq->b, 2);
modelcount = (THPIX) DEV_MCOUNT * pow(det, -0.5); /* this was corrected on Sep 27, 2013 */
qq->mcount = modelcount;
if (mcount != NULL) *mcount = modelcount;

return(SH_SUCCESS);
}


RET_CODE DdeVDaProf(void *q, THREGION *reg, int index) {

char *name = "DdeVDaProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = deV_profile_maker(q, &DdeVDa, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}


RET_CODE DdeVDbProf(void *q, THREGION *reg, int index) {

char *name = "DdeVDbProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
status = deV_profile_maker(q, &DdeVDb, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}


RET_CODE DdeVDcProf(void *q, THREGION *reg, int index) {

char *name = "DdeVDcProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = deV_profile_maker(q, &DdeVDc, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DdeVDxcProf(void *q, THREGION *reg, int index) {

char *name = "DdeVDxcProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = deV_profile_maker(q, &DdeVDxc, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DdeVDycProf(void *q, THREGION *reg, int index) {

char *name = "DdeVDycProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = deV_profile_maker(q, &DdeVDyc, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DdeVDphiProf(void *q, THREGION *reg, int index) {

char *name = "DdeVDphiProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = deV_profile_maker(q, &DdeVDphi, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DdeVDeProf(void *q, THREGION *reg, int index) {
char *name = "DdeVDeProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = deV_profile_maker(q, &DdeVDe, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DdeVDreProf(void *q, THREGION *reg, int index) {
char *name = "DdeVDreProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = deV_profile_maker(q, &DdeVDre, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DdeVDueProf(void *q, THREGION *reg, int index) {
char *name = "DdeVDueProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = deV_profile_maker(q, &DdeVDue, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DdeVDEProf(void *q, THREGION *reg, int index) {

char *name = "DdeVDEProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = deV_profile_maker(q, &DdeVDE, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DdeVDvProf(void *q, THREGION *reg, int index) {
char *name = "DdeVDvProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = deV_profile_maker(q, &DdeVDv, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DdeVDwProf(void *q, THREGION *reg, int index) {
char *name = "DdeVDwProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = deV_profile_maker(q, &DdeVDw, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DdeVDVProf(void *q, THREGION *reg, int index) {
char *name = "DdeVDVProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = deV_profile_maker(q, &DdeVDV, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DdeVDWProf(void *q, THREGION *reg, int index) {
char *name = "DdeVDWProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = deV_profile_maker(q, &DdeVDW, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DDdeVDreDreProf(void *q, THREGION *reg, int index) {
char *name = "DDdeVDreDreProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = deV_profile_maker(q, &DDdeVDreDre, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DDdeVDueDueProf(void *q, THREGION *reg, int index) {
char *name = "DDdeVDueDueProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = deV_profile_maker(q, &DDdeVDueDue, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

/* profile creators */

RET_CODE ExpProf(void *q, THREGION *reg, THPIX *mcount, int index) {

char *name = "ExpProf";
if (reg == NULL && mcount == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}

if (reg != NULL) {
RET_CODE status;
status = Exp_profile_maker(q, &Exp, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
}

THPIX modelcount = THNAN;
EXPPARS *qq = (EXPPARS *) q;
THPIX det = qq->a * qq->c - pow(qq->b, 2);
if (det != 0.0) {
	 modelcount = (THPIX) EXP_MCOUNT * pow(det, -0.5); /* this was corrected on Sep 27, 2013 */
}
qq->mcount = modelcount;
if (mcount != NULL) *mcount = modelcount;

return(SH_SUCCESS);
}


RET_CODE DExpDaProf(void *q, THREGION *reg, int index) {

char *name = "DExpDaProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Exp_profile_maker(q, &DExpDa, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}


RET_CODE DExpDbProf(void *q, THREGION *reg, int index) {

char *name = "DExpDbProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
status = Exp_profile_maker(q, &DExpDb, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}


RET_CODE DExpDcProf(void *q, THREGION *reg, int index) {

char *name = "DExpDcProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Exp_profile_maker(q, &DExpDc, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DExpDxcProf(void *q, THREGION *reg, int index) {

char *name = "DExpDxcProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Exp_profile_maker(q, &DExpDxc, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DExpDycProf(void *q, THREGION *reg, int index) {

char *name = "DExpDycProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Exp_profile_maker(q, &DExpDyc, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DExpDphiProf(void *q, THREGION *reg, int index) {

char *name = "DExpDphiProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Exp_profile_maker(q, &DExpDphi, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DExpDeProf(void *q, THREGION *reg, int index) {

char *name = "DExpDeProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Exp_profile_maker(q, &DExpDe, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DExpDreProf(void *q, THREGION *reg, int index) {
char *name = "DExpDreProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Exp_profile_maker(q, &DExpDre, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DExpDueProf(void *q, THREGION *reg, int index) {
char *name = "DExpDueProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Exp_profile_maker(q, &DExpDue, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DExpDEProf(void *q, THREGION *reg, int index) {

char *name = "DExpDEProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Exp_profile_maker(q, &DExpDE, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DExpDvProf(void *q, THREGION *reg, int index) {
char *name = "DExpDvProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Exp_profile_maker(q, &DExpDv, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DExpDwProf(void *q, THREGION *reg, int index) {

char *name = "DExpDwProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Exp_profile_maker(q, &DExpDw, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DExpDVProf(void *q, THREGION *reg, int index) {
char *name = "DExpDVProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Exp_profile_maker(q, &DExpDV, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DExpDWProf(void *q, THREGION *reg, int index) {

char *name = "DExpDWProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Exp_profile_maker(q, &DExpDW, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

/* profile creators */

RET_CODE PlProf(void *q, THREGION *reg, THPIX *mcount, int index) {

char *name = "PlProf";
if (reg == NULL && mcount == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}

if (reg != NULL) {
RET_CODE status;
status = Pl_profile_maker(q, &Pl, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
}

THPIX modelcount = THNAN;
POWERLAWPARS *qq = (POWERLAWPARS *) q;
THPIX det = qq->a * qq->c - pow(qq->b, 2);
if (det != 0.0) {
/* profile definition:
 * I(r) = z / (1 + z ^ 2) ^ p
 * where z = r / r0
 * can also be calculated as 
 * z = sqrt([dx, dy] [[a, b], [c, d]] [dx, dy])
 */
	THPIX n = qq->n;
	THPIX r0 = pow(det, (THPIX) -0.25); /* we calculated this separately for debug purposes */
	#if SEMI_ORTHO_PL
	double gamma1, gamma2;
	gamma1 = thGamma((double) (n - 1.5));
	gamma2 = thGamma((double) n);
	#if APPLY_GAMMA_TOLERANCE
	double gamma1prime = Gamma_Function((double) (n - 1.5));
	double gamma2prime = Gamma_Function((double) n);
	if (fabs(gamma1 - gamma1prime) / (fabs(gamma1) + fabs(gamma1prime)) > GAMMA_TOLERANCE) {
	thError("%s: WARNING - mismatching gamma(%g) values (%G, %G)", name, (float) (n - 1.5), gamma1, gamma1prime);
	}
	if (fabs(gamma2 - gamma2prime) / (fabs(gamma2) + fabs(gamma2prime)) > GAMMA_TOLERANCE) {
	thError("%s: WARNING - mismatching gamma(%g) values (%G, %G)", name, (float) n, gamma2, gamma2prime);
	}
	#endif
	modelcount = pow(r0, (THPIX) 2.0) *  
	(THPIX) 0.5 * pow((THPIX) PI, (THPIX) 1.5) * (THPIX) (gamma1 / gamma2);
	#else
	modelcount = pow(r0, (THPIX) 2.0) * ((THPIX) PI) / (n - (THPIX) 1.0);
	#endif
}
qq->mcount = modelcount;
if (mcount != NULL) *mcount = modelcount;

return(SH_SUCCESS);
}


RET_CODE DPlDaProf(void *q, THREGION *reg, int index) {

char *name = "DPlDaProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Pl_profile_maker(q, &DPlDa, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}


RET_CODE DPlDbProf(void *q, THREGION *reg, int index) {

char *name = "DPlDbProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
status = Pl_profile_maker(q, &DPlDb, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}


RET_CODE DPlDcProf(void *q, THREGION *reg, int index) {

char *name = "DPlDcProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Pl_profile_maker(q, &DPlDc, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DPlDxcProf(void *q, THREGION *reg, int index) {

char *name = "DPlDxcProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Pl_profile_maker(q, &DPlDxc, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DPlDycProf(void *q, THREGION *reg, int index) {

char *name = "DPlDycProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Pl_profile_maker(q, &DPlDyc, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DPlDphiProf(void *q, THREGION *reg, int index) {

char *name = "DPlDphiProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Pl_profile_maker(q, &DPlDphi, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DPlDeProf(void *q, THREGION *reg, int index) {

char *name = "DPlDeProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Pl_profile_maker(q, &DPlDe, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DPlDreProf(void *q, THREGION *reg, int index) {
char *name = "DPlDreProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Pl_profile_maker(q, &DPlDre, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DPlDueProf(void *q, THREGION *reg, int index) {
char *name = "DPlDueProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Pl_profile_maker(q, &DPlDue, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DPlDEProf(void *q, THREGION *reg, int index) {

char *name = "DPlDEProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Pl_profile_maker(q, &DPlDE, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DPlDvProf(void *q, THREGION *reg, int index) {
char *name = "DPlDvProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Pl_profile_maker(q, &DPlDv, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DPlDwProf(void *q, THREGION *reg, int index) {

char *name = "DPlDwProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Pl_profile_maker(q, &DPlDw, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DPlDVProf(void *q, THREGION *reg, int index) {
char *name = "DPlDVProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Pl_profile_maker(q, &DPlDV, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DPlDWProf(void *q, THREGION *reg, int index) {

char *name = "DPlDWProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Pl_profile_maker(q, &DPlDW, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DDPlDreDreProf(void *q, THREGION *reg, int index) {

char *name = "DDPlDreDreProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Pl_profile_maker(q, &DDPlDreDre, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DDPlDueDueProf(void *q, THREGION *reg, int index) {

char *name = "DDPlDueDueProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = Pl_profile_maker(q, &DDPlDueDue, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE sersicProf(void *q, THREGION *reg, THPIX *mcount, int index) {

char *name = "sersicProf";
if (reg == NULL && mcount == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}

if (reg != NULL) {
RET_CODE status;
status = sersic_profile_maker(q, &sersic, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
}

THPIX modelcount;
SERSICPARS *qq = (SERSICPARS *) q;
THPIX det = qq->a * qq->c - pow(qq->b, 2);
THPIX n = qq->n;
THPIX k = qq->k;
THPIX Gamma_2n = qq->gamma_2n;
THPIX Ln_gamma_2n = qq->ln_gamma_2n;
#if 0
modelcount = (THPIX) (2.0 * PI * n * exp(k) * pow(k, -2.0 * n) * Gamma_2n * pow(det, -0.5)); /* based on Ciotti et al 1999 */ 
#else
modelcount = (THPIX) (2.0 * PI * n * exp(k) * exp(-2.0 * n * log(k) +  Ln_gamma_2n) * pow(det, -0.5)); /* based on Ciotti et al 1999 */ 
#endif
qq->mcount = modelcount;
if (mcount != NULL) *mcount = modelcount;

return(SH_SUCCESS);
}


RET_CODE DsersicDaProf(void *q, THREGION *reg, int index) {

char *name = "DsersicDaProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDa, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}


RET_CODE DsersicDbProf(void *q, THREGION *reg, int index) {

char *name = "DsersicDbProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
status = sersic_profile_maker(q, &DsersicDb, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}


RET_CODE DsersicDcProf(void *q, THREGION *reg, int index) {

char *name = "DsersicDcProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDc, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DsersicDxcProf(void *q, THREGION *reg, int index) {

char *name = "DsersicDxcProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDxc, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DsersicDycProf(void *q, THREGION *reg, int index) {

char *name = "DsersicDycProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDyc, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DsersicDphiProf(void *q, THREGION *reg, int index) {

char *name = "DsersicDphiProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDphi, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DsersicDeProf(void *q, THREGION *reg, int index) {
char *name = "DsersicDeProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDe, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DsersicDreProf(void *q, THREGION *reg, int index) {
char *name = "DsersicDreProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDre, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DsersicDueProf(void *q, THREGION *reg, int index) {
char *name = "DsersicDueProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDue, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DsersicDEProf(void *q, THREGION *reg, int index) {

char *name = "DsersicDEProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDE, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DsersicDvProf(void *q, THREGION *reg, int index) {
char *name = "DsersicDvProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDv, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DsersicDwProf(void *q, THREGION *reg, int index) {
char *name = "DsersicDwProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDw, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DsersicDVProf(void *q, THREGION *reg, int index) {
char *name = "DsersicDVProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDV, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DsersicDWProf(void *q, THREGION *reg, int index) {
char *name = "DsersicDWProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDW, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DsersicDnProf(void *q, THREGION *reg, int index) {
char *name = "DsersicDnProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDn, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DsersicDNProf(void *q, THREGION *reg, int index) {
char *name = "DsersicDNProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDN, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}



RET_CODE DDsersicDreDreProf(void *q, THREGION *reg, int index) {
char *name = "DDsersicDreDreProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DDsersicDreDre, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DDsersicDueDueProf(void *q, THREGION *reg, int index) {
char *name = "DDsersicDueDueProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DDsersicDueDue, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

/* core sersic function */

RET_CODE coresersicProf(void *q, THREGION *reg, THPIX *mcount, int index) {

char *name = "coresersicProf";
if (reg == NULL && mcount == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}

if (reg != NULL) {
RET_CODE status;
status = coresersic_profile_maker(q, &coresersic, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
}

THPIX modelcount;
CORESERSICPARS *qq = (CORESERSICPARS *) q;

RET_CODE status;
status = coresersic_mcount_calculator(q, &modelcount, 0);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - coult not calculate (modelcount)", name);
	qq->mcount = THNAN;
	if (mcount != NULL) *mcount = THNAN;
	return(status);
} else if (isnan(modelcount) || isinf(modelcount)) {
	thError("%s: ERROR - found (nan) in model count", name);
	qq->mcount = THNAN;
	if (mcount != NULL) *mcount = THNAN;
	return(SH_GENERIC_ERROR);
}

qq->mcount = modelcount;
if (mcount != NULL) *mcount = modelcount;

return(SH_SUCCESS);
}


RET_CODE DcoresersicDxcProf(void *q, THREGION *reg, int index) {

char *name = "DcoresersicDxcProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = coresersic_profile_maker(q, &DcoresersicDxc, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DcoresersicDycProf(void *q, THREGION *reg, int index) {

char *name = "DcoresersicDycProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = coresersic_profile_maker(q, &DcoresersicDyc, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DcoresersicDphiProf(void *q, THREGION *reg, int index) {

char *name = "DcoresersicDphiProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = coresersic_profile_maker(q, &DcoresersicDphi, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DcoresersicDeProf(void *q, THREGION *reg, int index) {
char *name = "DcoresersicDeProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = coresersic_profile_maker(q, &DcoresersicDe, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DcoresersicDreProf(void *q, THREGION *reg, int index) {
char *name = "DcoresersicDreProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = coresersic_profile_maker(q, &DcoresersicDre, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DcoresersicDueProf(void *q, THREGION *reg, int index) {
char *name = "DcoresersicDueProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = coresersic_profile_maker(q, &DcoresersicDue, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DcoresersicDEProf(void *q, THREGION *reg, int index) {

char *name = "DcoresersicDEProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = coresersic_profile_maker(q, &DcoresersicDE, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}


RET_CODE DcoresersicDnProf(void *q, THREGION *reg, int index) {
char *name = "DcoresersicDnProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = coresersic_profile_maker(q, &DcoresersicDn, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DcoresersicDNProf(void *q, THREGION *reg, int index) {
char *name = "DcoresersicDNProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = coresersic_profile_maker(q, &DcoresersicDN, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}


RET_CODE DcoresersicDdeltaProf(void *q, THREGION *reg, int index) {
char *name = "DcoresersicDdeltaProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = coresersic_profile_maker(q, &DcoresersicDdelta, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}


RET_CODE DcoresersicDDProf(void *q, THREGION *reg, int index) {
char *name = "DcoresersicDDProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = coresersic_profile_maker(q, &DcoresersicDD, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}


RET_CODE DcoresersicDgammaProf(void *q, THREGION *reg, int index) {
char *name = "DcoresersicDgammaProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = coresersic_profile_maker(q, &DcoresersicDgamma, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}


RET_CODE DcoresersicDGProf(void *q, THREGION *reg, int index) {
char *name = "DcoresersicDGProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = coresersic_profile_maker(q, &DcoresersicDG, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DcoresersicDrbProf(void *q, THREGION *reg, int index) {
char *name = "DcoresersicDrbProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = coresersic_profile_maker(q, &DcoresersicDrb, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DcoresersicDubProf(void *q, THREGION *reg, int index) {
char *name = "DcoresersicDubProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = coresersic_profile_maker(q, &DcoresersicDub, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}





/* pixel functions */
THPIX delta(PIXPOS *p, void *q) {
	/* this is never really used */
	STARPARS *qq = (STARPARS *) q;
	FL32 dx = (FL32) p->row - (FL32) qq->xc;
	FL32 dy = (FL32) p->col - (FL32) qq->yc;
	if (fabs(dx) < 0.5 && fabs(dy) < 0.5) return((THPIX) 1.0);
	if (dx == (THPIX) 0.5 && dy == (THPIX) 0.5) return((THPIX) 1.0);
	return((THPIX) 0.0);
}

THPIX flat(PIXPOS *p, void *q) {
	return((THPIX) 1.0);
}

THPIX gaussian(PIXPOS *p, void *q) {
	 GAUSSIANPARS *qq = (GAUSSIANPARS *) q;
	 MGFL dx = p->row - qq->xc;
	 MGFL dy = p->col - qq->yc;
	 MGFL z = dx * (qq->a * dx + (MGFL) 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	return((THPIX) exp(-z));
}

THPIX gaussianStep(PIXPOS *p, void *q) {
	 GAUSSIANPARS *qq = (GAUSSIANPARS *) q;
	 MGFL dx = p->row - qq->xc;
	 MGFL dy = p->col - qq->yc;
	 MGFL z = dx * (qq->a * dx + (MGFL) 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	return((THPIX) (exp(-z) * STEPFUNC(z)));
}


THPIX DgaussianDa(PIXPOS *p, void *q) {
	 GAUSSIANPARS *qq = (GAUSSIANPARS *) q;
	 MGFL dx = p->row - qq->xc;
	 MGFL dy = p->col - qq->yc;
	 MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	return(-(THPIX) ((MGFL) 2.0 * (pow(dx, 2)) * exp(-z)));
}
THPIX DgaussianDb(PIXPOS *p, void *q) {
	 GAUSSIANPARS *qq = (GAUSSIANPARS *) q;
	 MGFL dx = p->row - qq->xc;
	 MGFL dy = p->col - qq->yc;
	 MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	return(-(THPIX) ((MGFL) 2.0 * dx * dy * exp(-z)));
}
THPIX DgaussianDc(PIXPOS *p, void *q) {
	 GAUSSIANPARS *qq = (GAUSSIANPARS *) q;
	 MGFL dx = p->row - qq->xc;
	 MGFL dy = p->col - qq->yc;
	 MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	return(-(THPIX) ((MGFL) 2.0 * pow(dy, 2.0) * exp(-z)));
}
THPIX DgaussianDxc(PIXPOS *p, void *q) {
	 GAUSSIANPARS *qq = (GAUSSIANPARS *) q;
	 MGFL dx = p->row - qq->xc;
	 MGFL dy = p->col - qq->yc;
	 MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	return((THPIX) ((MGFL) 2.0 * (dx * (MGFL) qq->a + dy * (MGFL) qq->b) * exp(-z)));
}
THPIX DgaussianDyc(PIXPOS *p, void *q) {
	 GAUSSIANPARS *qq = (GAUSSIANPARS *) q;
	 MGFL dx = p->row - qq->xc;
	 MGFL dy = p->col - qq->yc;
	 MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	return((THPIX) ((MGFL) 2.0 * (dy * (MGFL) qq->c + dx * (MGFL) qq->b) * exp(-z)));
}
THPIX DgaussianDphi(PIXPOS *p, void *q) {
	GAUSSIANPARS *qq = (GAUSSIANPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);
	MGFL DzDphi = dx * (qq->aphi * dx + 2.0 * qq->bphi * dy) + qq->cphi * pow(dy, 2); 
	return((THPIX) (-DzDphi * exp(-z)));
}
THPIX DgaussianDe(PIXPOS *p, void *q) {
	 GAUSSIANPARS *qq = (GAUSSIANPARS *) q;
	 MGFL dx = p->row - qq->xc;
	 MGFL dy = p->col - qq->yc;
	 MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	 MGFL DzDe = dx * ((MGFL) qq->ae * dx + 2.0 * (MGFL) qq->be * dy) + (MGFL) qq->ce * pow(dy, 2);
	return((THPIX) (-DzDe * exp(-z)));
}
THPIX DgaussianDre(PIXPOS *p, void *q) {
	 GAUSSIANPARS *qq = (GAUSSIANPARS *) q;
	 MGFL dx = p->row - qq->xc;
	 MGFL dy = p->col - qq->yc;
	 MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);
	 MGFL DzDre = -2.0 * z / (MGFL) qq->re;
	return((THPIX) (-DzDre * exp(-z)));
}
THPIX DgaussianDue(PIXPOS *p, void *q) {
	 GAUSSIANPARS *qq = (GAUSSIANPARS *) q;
	 MGFL dx = p->row - qq->xc;
	 MGFL dy = p->col - qq->yc;
	 MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	 MGFL DzDue = -(MGFL) 2.0 * z / (MGFL) qq->re * (MGFL) qq->reUe;
	return((THPIX) (-DzDue * exp(-z)));
}
THPIX DgaussianDE(PIXPOS *p, void *q) {
	 GAUSSIANPARS *qq = (GAUSSIANPARS *) q;
	 MGFL dx = p->row - qq->xc;
	 MGFL dy = p->col - qq->yc;
	 MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);
	 MGFL DzDE = dx * ((MGFL) qq->aE * dx + (MGFL) 2.0 * (MGFL) qq->bE * dy) + (MGFL) qq->cE * pow(dy, 2);
	return((THPIX) (-DzDE * exp(-z)));
}

THPIX null_function(PIXPOS *p, void *q) {
	return((THPIX) 0.0);
}

/* deV law - pixel functions */


THPIX deV(PIXPOS *p, void *q) {
	DEVPARS *qq = (DEVPARS *)q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_DEV * (pow(z, 0.125) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) exp(zz));
	}
	#else
	return((THPIX) (exp(-NU_DEV * (pow(z, 0.125) - (MGFL) 1.0))));
	#endif
}

THPIX deVStep(PIXPOS *p, void *q) {
	DEVPARS *qq = (DEVPARS *)q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_DEV * (pow(z, 0.125) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (exp(zz) * STEPFUNC(z)));
	}
	#else
	return((THPIX) (exp(-NU_DEV * (pow(z, 0.125) - (MGFL) 1.0)) * STEPFUNC(z)));
	#endif
}

THPIX DdeVDa(PIXPOS *p, void *q) {
	DEVPARS *qq = (DEVPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);
	MGFL DzDa = (MGFL) -0.125 * NU_DEV * pow(dx, 2) * pow(z, -0.875); 
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_DEV * (pow(z, 0.125) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDa * exp(zz)));
	}
	#else
	return((THPIX) (DzDa * exp(-NU_DEV * (pow(z, 0.125) - (MGFL) 1.0))));
	#endif
}
THPIX DdeVDb(PIXPOS *p, void *q) {
	DEVPARS *qq = (DEVPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	MGFL DzDb = - 0.125 * NU_DEV * dx * dy * pow((double) z, -0.875); 
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_DEV * (pow(z, 0.125) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDb * exp(zz)));
	}
	#else
	return((THPIX) (DzDb * exp(-NU_DEV * (pow(z, 0.125) - (MGFL) 1.0))));
	#endif
}
THPIX DdeVDc(PIXPOS *p, void *q) {
	DEVPARS *qq = (DEVPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	MGFL DzDc = - 0.125 * NU_DEV * pow(dx, 2) * pow(z, -0.875); 
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_DEV * (pow(z, 0.125) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDc * exp(zz)));
	}
	#else
	return((THPIX) (DzDc * exp(-NU_DEV * (pow(z, 0.125) - (MGFL) 1.0))));
	#endif
}
THPIX DdeVDxc(PIXPOS *p, void *q) {
	DEVPARS *qq = (DEVPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	MGFL DzDxc = 0.25 * NU_DEV * (dx * qq->a + dy * qq->b) * pow(z, -0.875); 
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_DEV * (pow(z, 0.125) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDxc * exp(zz)));
	}
	#else
	return((THPIX) (DzDxc * exp(-NU_DEV * (pow(z, 0.125) - (MGFL) 1.0))));
	#endif
}
THPIX DdeVDyc(PIXPOS *p, void *q) {
	DEVPARS *qq = (DEVPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	MGFL DzDyc = 0.25 * NU_DEV * (dx * qq->b + dy * qq->c) * pow(z, -0.875); /* 0.875 = 7 / 8 */ 
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_DEV * (pow(z, 0.125) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDyc * exp(zz)));
	}
	#else
	return((THPIX) (DzDyc * exp(-NU_DEV * (pow(z, 0.125) - (MGFL) 1.0))));
	#endif
}
THPIX DdeVDre(PIXPOS *p, void *q) {
	DEVPARS *qq = (DEVPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	MGFL DzDre = (0.25 * NU_DEV * pow(z, 0.125) / (MGFL) qq->re);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_DEV * (pow(z, 0.125) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDre * exp(zz)));
	}
	#else
	return((THPIX) (DzDre * exp(-NU_DEV * (pow(z, 0.125) - (MGFL) 1.0))));
	#endif
}
THPIX DdeVDue(PIXPOS *p, void *q) {
	DEVPARS *qq = (DEVPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
/* 
(MAX exp(u_eff) + MIN) /  (exp(u_eff) + 1) = r_eff
exp(u_eff) = (r_eff - MIN) / (MAX - r_eff)
du_eff / dr_eff = (e(u) + 2 + e(-u)) / (max - min))
*/
	MGFL DzDue = (THPIX) (0.25 * NU_DEV * pow(z, 0.125) / (MGFL) qq->re * (MGFL) qq->reUe);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_DEV * (pow(z, 0.125) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDue * exp(zz)));
	}
	#else
	return((THPIX) (DzDue * exp(-NU_DEV * (pow(z, 0.125) - (MGFL) 1.0))));
	#endif
}

THPIX DdeVDe(PIXPOS *p, void *q) {
	DEVPARS *qq = (DEVPARS *) q;	
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDe = -0.125 * NU_DEV * 
	(dx * (qq->ae * dx + 2.0 * qq->be * dy) + qq->ce * pow(dy, 2)) * pow(z, -0.875);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_DEV * (pow(z, 0.125) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDe * exp(zz)));
	}
	#else
	return((THPIX) ((DzDe) * exp(-NU_DEV * (pow(z, 0.125) - (MGFL) 1.0))));
	#endif
}
THPIX DdeVDphi(PIXPOS *p, void *q) {
	DEVPARS *qq = (DEVPARS *) q;	
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDphi = -0.125 * NU_DEV * 
	(dx * (qq->aphi * dx + 2.0 * qq->bphi * dy) + qq->cphi * pow(dy, 2)) * pow(z, -0.875);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_DEV * (pow(z, 0.125) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDphi * exp(zz)));
	}
	#else
	return((THPIX) (DzDphi * exp(-NU_DEV * (pow(z, 0.125) - (MGFL) 1.0))));
	#endif
}
THPIX DdeVDE(PIXPOS *p, void *q) {
	DEVPARS *qq = (DEVPARS *) q;	
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDE = -0.125 * NU_DEV * 
	(dx * (qq->aE * dx + 2.0 * qq->bE * dy) + qq->cE * pow(dy, 2)) * pow(z, -0.875);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_DEV * (pow(z, 0.125) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDE * exp(zz)));
	}
	#else
	return((THPIX) ((DzDE) * exp(-NU_DEV * (pow(z, 0.125) - (MGFL) 1.0))));
	#endif
}
THPIX DdeVDv(PIXPOS *p, void *q) {
	DEVPARS *qq = (DEVPARS *) q;	
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDv = -0.125 * NU_DEV * 
	(dx * (qq->av * dx + 2.0 * qq->bv * dy) + qq->cv * pow(dy, 2)) * pow(z, -0.875);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_DEV * (pow(z, 0.125) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDv * exp(zz)));
	}
	#else
	return((THPIX) ((DzDv) * exp(-NU_DEV * (pow(z, 0.125) - (MGFL) 1.0))));
	#endif
}
THPIX DdeVDw(PIXPOS *p, void *q) {
	DEVPARS *qq = (DEVPARS *) q;	
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDw = -0.125 * NU_DEV * 
	(dx * (qq->aw * dx + 2.0 * qq->bw * dy) + qq->cw * pow(dy, 2)) * pow(z, -0.875);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_DEV * (pow(z, 0.125) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDw * exp(zz)));
	}
	#else
	return((THPIX) ((DzDw) * exp(-NU_DEV * (pow(z, 0.125) - (MGFL) 1.0))));
	#endif
}

THPIX DdeVDV(PIXPOS *p, void *q) {
	DEVPARS *qq = (DEVPARS *) q;	
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDV = -0.125 * NU_DEV * 
	(dx * (qq->aV * dx + 2.0 * qq->bV * dy) + qq->cV * pow(dy, 2)) * pow(z, -0.875);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_DEV * (pow(z, 0.125) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDV * exp(zz)));
	}
	#else
	return((THPIX) ((DzDV) * exp(-NU_DEV * (pow(z, 0.125) - (MGFL) 1.0))));
	#endif
}
THPIX DdeVDW(PIXPOS *p, void *q) {
	DEVPARS *qq = (DEVPARS *) q;	
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDW = -0.125 * NU_DEV * 
	(dx * (qq->aW * dx + 2.0 * qq->bW * dy) + qq->cW * pow(dy, 2)) * pow(z, -0.875);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_DEV * (pow(z, 0.125) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDW * exp(zz)));
	}
	#else
	return((THPIX) ((DzDW) * exp(-NU_DEV * (pow(z, 0.125) - (MGFL) 1.0))));
	#endif
}

THPIX DDdeVDreDre(PIXPOS *p, void *q) {
	DEVPARS *qq = (DEVPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	MGFL DzzDre = (0.25 * NU_DEV * pow(z, 0.125) / (MGFL) qq->re);
	MGFL DDzzDreDre = (-1.25 * DzzDre / (MGFL) qq->re);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_DEV * (pow(z, 0.125) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) ((DDzzDreDre + DzzDre * DzzDre) * exp(zz)));
	}
	#else
	return((THPIX) ((DDzzDreDre + DzzDre * DzzDre) * exp(-NU_DEV * (pow(z, 0.125) - (MGFL) 1.0))));
	#endif
}

THPIX DDdeVDueDue(PIXPOS *p, void *q) {
	DEVPARS *qq = (DEVPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	
	MGFL DzzDre = (0.25 * NU_DEV * pow(z, 0.125) / (MGFL) qq->re);	
	MGFL DDzzDreDre = (-1.25 * DzzDre / (MGFL) qq->re);
	MGFL DzzDue = (DzzDre * (MGFL) qq->reUe);
	MGFL DDzzDueDue = DzzDre * (MGFL) qq->reUeUe + pow((MGFL) qq->reUe, 2.0) * DDzzDreDre;
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_DEV * (pow(z, 0.125) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) ((DDzzDueDue + DzzDue * DzzDue) * exp(zz)));
	}
	#else
	return((THPIX) ((DDzzDueDue + DzzDue * DzzDue) * exp(-NU_DEV * (pow(z, 0.125) - (MGFL) 1.0))));
	#endif
}

/* Exp law - pixel functions */

THPIX Exp(PIXPOS *p, void *q) {
	EXPPARS *qq = (EXPPARS *)q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_EXP * (pow(z, 0.5) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) exp(zz));
	}
	#else
	return((THPIX) (exp(-NU_EXP * (pow(z, 0.5) - (MGFL) 1.0))));
	#endif
}

THPIX ExpStep(PIXPOS *p, void *q) {
	EXPPARS *qq = (EXPPARS *)q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_EXP * (pow(z, 0.5) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (exp(zz) * STEPFUNC(z)));
	}
	#else
	return((THPIX) (exp(-NU_EXP * (pow(z, 0.5) - (MGFL) 1.0)) * STEPFUNC(z)));
	#endif
}


THPIX DExpDa(PIXPOS *p, void *q) {
	EXPPARS *qq = (EXPPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	MGFL DzDa = - NU_EXP * pow(dx, 2) * pow(z, -0.5); 
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_EXP * (pow(z, 0.5) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDa * exp(zz)));
	}
	#else
	return((THPIX) (DzDa * exp(-NU_EXP * (pow(z, 0.5) - (MGFL) 1.0))));
	#endif
}
THPIX DExpDb(PIXPOS *p, void *q) {
	EXPPARS *qq = (EXPPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	MGFL DzDb = - NU_EXP * dx * dy * pow(z, -0.5); 
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_EXP * (pow(z, 0.5) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDb * exp(zz)));
	}
	#else
	return((THPIX) (DzDb * exp(-NU_EXP * (pow(z, 0.5) - (MGFL) 1.0))));
	#endif
}
THPIX DExpDc(PIXPOS *p, void *q) {
	EXPPARS *qq = (EXPPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	MGFL DzDc = - NU_EXP * pow(dx, 2) * pow(z, -0.5); 
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_EXP * (pow(z, 0.5) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDc * exp(zz)));
	}
	#else
	return((THPIX) (DzDc * exp(-NU_EXP * (pow(z, 0.5) - (MGFL) 1.0))));
	#endif
}
THPIX DExpDxc(PIXPOS *p, void *q) {
	EXPPARS *qq = (EXPPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	MGFL DzDxc = NU_EXP * (dx * qq->a + dy * qq->b) * pow(z, -0.5); 
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_EXP * (pow(z, 0.5) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDxc * exp(zz)));
	}
	#else
	return((THPIX) (DzDxc * exp(-NU_EXP * (pow(z, 0.5) - (MGFL) 1.0))));
	#endif
}
THPIX DExpDyc(PIXPOS *p, void *q) {
	EXPPARS *qq = (EXPPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	MGFL DzDyc = NU_EXP * (dx * qq->b + dy * qq->c) * pow(z, -0.5); /* 0.875 = 7 / 8 */ 
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_EXP * (pow(z, 0.5) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDyc * exp(zz)));
	}
	#else
	return((THPIX) (DzDyc * exp(-NU_EXP * (pow(z, 0.5) - (MGFL) 1.0))));
	#endif
}
THPIX DExpDre(PIXPOS *p, void *q) {
	EXPPARS *qq = (EXPPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	MGFL DzDre = (THPIX) (NU_EXP * pow(z, 0.5) / qq->re); /* according to valgrind this seems to depend on un-initialized values for some reason - generate by pow function*/
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_EXP * (pow(z, 0.5) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDre * exp(zz)));
	}
	#else
	return((THPIX) (DzDre * exp(-NU_EXP * (pow(z, 0.5) - (MGFL) 1.0))));
	#endif
}
THPIX DExpDue(PIXPOS *p, void *q) {
	EXPPARS *qq = (EXPPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	MGFL DzDue = (THPIX) (NU_EXP * pow(z, 0.5) / (MGFL) qq->re * (MGFL) qq->reUe); /* according to valgrind this seems to depend on un-initialized values for some reason - generate by pow function*/
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_EXP * (pow(z, 0.5) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDue * exp(zz)));
	}
	#else
	return((THPIX) (DzDue * exp(-NU_EXP * (pow(z, 0.5) - (MGFL) 1.0))));
	#endif
}


THPIX DExpDe(PIXPOS *p, void *q) {
	EXPPARS *qq = (EXPPARS *) q;	
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDe = -0.5 * NU_EXP * 
	(dx * (qq->ae * dx + 2.0 * qq->be * dy) + qq->ce * pow(dy, 2)) * pow(z, -0.5);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_EXP * (pow(z, 0.5) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDe * exp(zz)));
	}
	#else
	return((THPIX) (DzDe * exp(-NU_EXP * (pow(z, 0.5) - (MGFL) 1.0))));
	#endif
}
THPIX DExpDphi(PIXPOS *p, void *q) {
	EXPPARS *qq = (EXPPARS *) q;	
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDphi = -0.5 * NU_EXP * 
	(dx * (qq->aphi * dx + 2.0 * qq->bphi * dy) + qq->cphi * pow(dy, 2)) * pow(z, -0.5);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_EXP * (pow(z, 0.5) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDphi * exp(zz)));
	}
	#else
	return((THPIX) (DzDphi * exp(-NU_EXP * (pow(z, 0.5) - (MGFL) 1.0))));
	#endif
}
THPIX DExpDE(PIXPOS *p, void *q) {
	EXPPARS *qq = (EXPPARS *) q;	
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDE = -0.5 * NU_EXP * 
	(dx * (qq->aE * dx + 2.0 * qq->bE * dy) + qq->cE * pow(dy, 2)) * pow(z, -0.5);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_EXP * (pow(z, 0.5) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDE * exp(zz)));
	}
	#else
	return((THPIX) (DzDE * exp(-NU_EXP * (pow(z, 0.5) - (MGFL) 1.0))));
	#endif
}
THPIX DExpDv(PIXPOS *p, void *q) {
	EXPPARS *qq = (EXPPARS *) q;	
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDv = -0.5 * NU_EXP * 
	(dx * (qq->av * dx + 2.0 * qq->bv * dy) + qq->cv * pow(dy, 2)) * pow(z, -0.5);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_EXP * (pow(z, 0.5) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDv * exp(zz)));
	}
	#else
	return((THPIX) (DzDv * exp(-NU_EXP * (pow(z, 0.5) - (MGFL) 1.0))));
	#endif
}
THPIX DExpDw(PIXPOS *p, void *q) {
	EXPPARS *qq = (EXPPARS *) q;	
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDw = -0.5 * NU_EXP * 
	(dx * (qq->aw * dx + 2.0 * qq->bw * dy) + qq->cw * pow(dy, 2)) * pow(z, -0.5);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_EXP * (pow(z, 0.5) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDw * exp(zz)));
	}
	#else
	return((THPIX) (DzDw * exp(-NU_EXP * (pow(z, 0.5) - (MGFL) 1.0))));
	#endif
}

THPIX DExpDV(PIXPOS *p, void *q) {
	EXPPARS *qq = (EXPPARS *) q;	
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDV = -0.5 * NU_EXP * 
	(dx * (qq->aV * dx + 2.0 * qq->bV * dy) + qq->cV * pow(dy, 2)) * pow(z, -0.5);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_EXP * (pow(z, 0.5) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDV * exp(zz)));
	}
	#else
	return((THPIX) (DzDV * exp(-NU_EXP * (pow(z, 0.5) - (MGFL) 1.0))));
	#endif
}
THPIX DExpDW(PIXPOS *p, void *q) {
	EXPPARS *qq = (EXPPARS *) q;	
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDW = -0.5 * NU_EXP * 
	(dx * (qq->aW * dx + 2.0 * qq->bW * dy) + qq->cW * pow(dy, 2)) * pow(z, -0.5);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -NU_EXP * (pow(z, 0.5) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDW * exp(zz)));
	}
	#else
	return((THPIX) (DzDW * exp(-NU_EXP * (pow(z, 0.5) - (MGFL) 1.0))));
	#endif
}

/* Power Law - pixel functions */


THPIX Pl(PIXPOS *p, void *q) {
	POWERLAWPARS *qq = (POWERLAWPARS *)q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL n = (MGFL) qq->n;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);
	#if SEMI_ORTHO_PL
	return((THPIX) (sqrt(z) / pow((MGFL) 1.0 + z, n)));
	#else
	return((THPIX) ((MGFL) 1.0 / pow((MGFL) 1.0 + z, n)));
	#endif	
}

THPIX PlStep(PIXPOS *p, void *q) {
	POWERLAWPARS *qq = (POWERLAWPARS *)q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL n = (MGFL) qq->n;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);
	#if SEMI_ORTHO_PL
	return((THPIX) (sqrt(z) / pow((MGFL) 1.0 + z, n) * STEPFUNC(z)));
	#else
	return((THPIX) ((MGFL) 1.0 / pow((MGFL) 1.0 + z, n) * STEPFUNC(z)));
	#endif
}

THPIX DPlDa(PIXPOS *p, void *q) {
	POWERLAWPARS *qq = (POWERLAWPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);
	MGFL n = (MGFL) qq->n;
	#if SEMI_ORTHO_PL
	MGFL f = sqrt(z) / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = ((MGFL) 0.5 / z - n / ((MGFL) 1.0 + z)) * f;
	#else
	MGFL f = (MGFL) 1.0 / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = (- n / ((MGFL) 1.0 + z)) * f;
	#endif
	MGFL DzDa = dx * dx; 
	MGFL DfDa = DfDz * DzDa;
	return((THPIX) DfDa);
}
THPIX DPlDb(PIXPOS *p, void *q) {
	POWERLAWPARS *qq = (POWERLAWPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);
	MGFL n = (MGFL) qq->n;
	#if SEMI_ORTHO_PL
	MGFL f = sqrt(z) / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = ((MGFL) 0.5 / z - n / ((MGFL) 1.0 + z)) * f;
	#else
	MGFL f = (MGFL) 1.0 / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = (- n / ((MGFL) 1.0 + z)) * f;
	#endif
	MGFL DzDb = 2.0 * dx * dy; 
	MGFL DfDb = DfDz * DzDb;
	return((THPIX) DfDb);
}
THPIX DPlDc(PIXPOS *p, void *q) {
	POWERLAWPARS *qq = (POWERLAWPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);
	MGFL n = (MGFL) qq->n;
	#if SEMI_ORTHO_PL
	MGFL f = sqrt(z) / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = ((MGFL) 0.5 / z - n / ((MGFL) 1.0 + z)) * f;
	#else
	MGFL f = (MGFL) 1.0 / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = (- n / ((MGFL) 1.0 + z)) * f;
	#endif
	MGFL DzDc = dy * dy; 
	MGFL DfDc = DfDz * DzDc;
	return((THPIX) DfDc);
}
THPIX DPlDxc(PIXPOS *p, void *q) {
	POWERLAWPARS *qq = (POWERLAWPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);
	MGFL n = (MGFL) qq->n;
	#if SEMI_ORHTO_PL
	MGFL f = sqrt(z) / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = ((MGFL) 0.5 / z - n / ((MGFL) 1.0 + z)) * f;
	#else
	MGFL f = (MGFL) 1.0 / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = (- n / ((MGFL) 1.0 + z)) * f;
	#endif
	MGFL DzDxc = -2.0 * (dx * qq->a + dy * qq->b); 
	MGFL DfDxc = DfDz * DzDxc;
	return((THPIX) DfDxc);
}
THPIX DPlDyc(PIXPOS *p, void *q) {
	POWERLAWPARS *qq = (POWERLAWPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);
	MGFL n = (MGFL) qq->n;
	#if SEMI_ORTHO_PL
	MGFL f = sqrt(z) / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = ((MGFL) 0.5 / z - n / ((MGFL) 1.0 + z)) * f;
	#else
	MGFL f = (MGFL) 1.0 / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = (- n / ((MGFL) 1.0 + z)) * f;
	#endif
	MGFL DzDyc = -2.0 * (dx * qq->b + dy * qq->c);  
	MGFL DfDyc = DfDz * DzDyc;
	return((THPIX) DfDyc);
}
THPIX DPlDre(PIXPOS *p, void *q) {
	POWERLAWPARS *qq = (POWERLAWPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);
	MGFL n = (MGFL) qq->n;
	#if SEMI_ORTHO_PL
	MGFL f = sqrt(z) / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = ((MGFL) 0.5 / z - n / ((MGFL) 1.0 + z)) * f;
	#else
	MGFL f = (MGFL) 1.0 / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = (- n / ((MGFL) 1.0 + z)) * f;
	#endif
	MGFL DzDre = -2.0 * z / (MGFL) qq->re;
	MGFL DfDre = DfDz * DzDre;
	return((THPIX) DfDre);
}
THPIX DPlDue(PIXPOS *p, void *q) {
	POWERLAWPARS *qq = (POWERLAWPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);
	MGFL n = (MGFL) qq->n;
	#if SEMI_ORTHO_PL
	MGFL f = sqrt(z) / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = ((MGFL) 0.5 / z - n / ((MGFL) 1.0 + z)) * f;
	#else
	MGFL f = (MGFL) 1.0 / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = (- n / ((MGFL) 1.0 + z)) * f;
	#endif
	MGFL DzDue = -2.0 * z / (MGFL) qq->re * (MGFL) qq->reUe;
	MGFL DfDue = DfDz * DzDue;
	return((THPIX) DfDue);

}

THPIX DPlDe(PIXPOS *p, void *q) {
	POWERLAWPARS *qq = (POWERLAWPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);	
	MGFL n = (MGFL) qq->n;
	#if SEMI_ORTHO_PL
	MGFL f = sqrt(z) / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = ((MGFL) 0.5 / z - n / ((MGFL) 1.0 + z)) * f;
	#else
	MGFL f = (MGFL) 1.0 / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = (- n / ((MGFL) 1.0 + z)) * f;
	#endif
	MGFL DzDe = (MGFL) (dx * (qq->ae * dx + 2.0 * qq->be * dy) + qq->ce * pow(dy, 2));
	MGFL DfDe = DfDz * DzDe;
	return((THPIX) DfDe);
}
THPIX DPlDphi(PIXPOS *p, void *q) {
	POWERLAWPARS *qq = (POWERLAWPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);	
	MGFL n = (MGFL) qq->n;
	#if SEMI_ORTHO_PL
	MGFL f = sqrt(z) / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = ((MGFL) 0.5 / z - n / ((MGFL) 1.0 + z)) * f;
	#else
	MGFL f = (MGFL) 1.0 / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = (- n / ((MGFL) 1.0 + z)) * f;
	#endif
	MGFL DzDphi = (dx * (qq->aphi * dx + 2.0 * qq->bphi * dy) + qq->cphi * pow(dy, 2));
	MGFL DfDphi = DfDz * DzDphi;
	return((THPIX) DfDphi);
}
THPIX DPlDE(PIXPOS *p, void *q) {
	POWERLAWPARS *qq = (POWERLAWPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);	
	MGFL n = (MGFL) qq->n;
	#if SEMI_ORTHO_PL
	MGFL f = sqrt(z) / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = ((MGFL) 0.5 / z - n / ((MGFL) 1.0 + z)) * f;
	#else
	MGFL f = (MGFL) 1.0 / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = (- n / ((MGFL) 1.0 + z)) * f;
	#endif
	MGFL DzDE = (MGFL) (dx * (qq->aE * dx + 2.0 * qq->bE * dy) + qq->cE * pow(dy, 2));
	MGFL DfDE = DfDz * DzDE;
	return((THPIX) DfDE);
}
THPIX DPlDv(PIXPOS *p, void *q) {
	POWERLAWPARS *qq = (POWERLAWPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);	
	MGFL n = (MGFL) qq->n;
	#if SEMI_ORTHO_PL
	MGFL f = sqrt(z) / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = ((MGFL) 0.5 / z - n / ((MGFL) 1.0 + z)) * f;
	#else
	MGFL f = (MGFL) 1.0 / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = (- n / ((MGFL) 1.0 + z)) * f;
	#endif
	MGFL DzDv = (MGFL) (dx * (qq->av * dx + 2.0 * qq->bv * dy) + qq->cv * pow(dy, 2));
	MGFL DfDv = DfDz * DzDv;
	return((THPIX) DfDv);
}
THPIX DPlDw(PIXPOS *p, void *q) {
	POWERLAWPARS *qq = (POWERLAWPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);
	MGFL n = (MGFL) qq->n;
	#if SEMI_ORTHO_PL
	MGFL f = sqrt(z) / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = ((MGFL) 0.5 / z - n / ((MGFL) 1.0 + z)) * f;
	#else
	MGFL f = (MGFL) 1.0 / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = (- n / ((MGFL) 1.0 + z)) * f;
	#endif
	MGFL DzDw = (MGFL) (dx * (qq->aw * dx + 2.0 * qq->bw * dy) + qq->cw * pow(dy, 2));
	MGFL DfDw = DfDz * DzDw;
	return((THPIX) DfDw);
}

THPIX DPlDV(PIXPOS *p, void *q) {
	POWERLAWPARS *qq = (POWERLAWPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);	
	MGFL n = (MGFL) qq->n;
	#if SEMI_ORTHO_PL
	MGFL f = sqrt(z) / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = ((MGFL) 0.5 / z - n / ((MGFL) 1.0 + z)) * f;
	#else
	MGFL f = (MGFL) 1.0 / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = (- n / ((MGFL) 1.0 + z)) * f;
	#endif
	MGFL DzDV = (MGFL) (dx * (qq->aV * dx + 2.0 * qq->bV * dy) + qq->cV * pow(dy, 2));
	MGFL DfDV = DfDz * DzDV;
	return((THPIX) DfDV);
}
THPIX DPlDW(PIXPOS *p, void *q) {
	POWERLAWPARS *qq = (POWERLAWPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);	
	MGFL n = (MGFL) qq->n;
	#if SEMI_ORTHO_PL
	MGFL f = sqrt(z) / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = ((MGFL) 0.5 / z - n / ((MGFL) 1.0 + z)) * f;
	#else
	MGFL f = (MGFL) 1.0 / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = (- n / ((MGFL) 1.0 + z)) * f;
	#endif
	MGFL DzDW = (MGFL) (dx * (qq->aW * dx + 2.0 * qq->bW * dy) + qq->cW * pow(dy, 2));
	MGFL DfDW = DfDz * DzDW;
	return((THPIX) DfDW);
}

THPIX DDPlDreDre(PIXPOS *p, void *q) {
	POWERLAWPARS *qq = (POWERLAWPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);
	MGFL n = (MGFL) qq->n;
	#if SEMI_ORTHO_PL
	MGFL f = sqrt(z) / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = ((MGFL) 0.5 / z - n / ((MGFL) 1.0 + z)) * f;
	MGFL DDfDzDz = ((MGFL) 0.5 / z - n / ((MGFL) 1.0 + z)) * DfDz - ((MGFL) 0.5 / (z * z) - n / pow((MGFL) 1.0 + z, 2.0)) * f;
	#else
	MGFL f = (MGFL) 1.0 / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = (- n / ((MGFL) 1.0 + z)) * f;
	MGFL DDfDzDz = (- n / ((MGFL) 1.0 + z)) * DfDz - (- n / pow((MGFL) 1.0 + z, 2.0)) * f;
	#endif
	MGFL DzDre = -2.0 * z / qq->re;
	MGFL DDzDreDre = -3.0 * DzDre / qq->re;
	MGFL DDfDreDre = DDfDzDz * DzDre * DzDre + DfDz * DDzDreDre;
	return((THPIX) DDfDreDre);
}

THPIX DDPlDueDue(PIXPOS *p, void *q) {
	POWERLAWPARS *qq = (POWERLAWPARS *) q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);	
	MGFL n = (MGFL) qq->n;
	#if SEMI_ORTHO_PL
	MGFL f = sqrt(z) / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = ((MGFL) 0.5 / z - n / ((MGFL) 1.0 + z)) * f;
	MGFL DDfDzDz = ((MGFL) 0.5 / z - n / ((MGFL) 1.0 + z)) * DfDz - ((MGFL) 0.5 / (z * z) - n / pow((MGFL) 1.0 + z, 2.0)) * f;
	#else
	MGFL f = (MGFL) 1.0 / pow((MGFL) 1.0 + z, n);
	MGFL DfDz = (- n / ((MGFL) 1.0 + z)) * f;
	MGFL DDfDzDz = (- n / ((MGFL) 1.0 + z)) * DfDz - (- n / pow((MGFL) 1.0 + z, 2.0)) * f;
	#endif
	MGFL DzDre = -2.0 * z / qq->re;
	MGFL DDzDreDre = -3.0 * DzDre / qq->re;
	MGFL DzDue = (DzDre * (MGFL) qq->reUe);
	MGFL DDzDueDue = DzDre * (MGFL) qq->reUeUe + pow((MGFL) qq->reUe, 2.0) * DDzDreDre;
	MGFL DDfDueDue = DDfDzDz * DzDue * DzDue + DfDz * DDzDueDue;
	return((THPIX) DDfDueDue);
}

THPIX sersic(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *)q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (z < SERSIC_Z_TOLERANCE) {
                zz = ksersic;
        }
	#if ESTIMATE_SMALL_EXP
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) exp(zz));
	}
	#else
	MGFL pixvalue = exp(zz);
	shAssert(!isnan(pixvalue) && !isinf(pixvalue));
	return((THPIX) pixvalue);
	#endif
}

THPIX sersicStep(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *)q;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (exp(zz) * STEPFUNC(z)));
	}
	#else
	return((THPIX) (exp(-ksersic * (pow(z, exponentsersic) - (MGFL) 1.0)) * STEPFUNC(z)));
	#endif
}

THPIX DsersicDa(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * ((MGFL) qq->a * dx + (MGFL) 2.0 * (MGFL) qq->b * dy) + (MGFL) qq->c * pow(dy, 2);
	MGFL DzDa = (MGFL) -exponentsersic * ksersic * pow(dx, 2) * pow(z, exponentsersic - (MGFL) 1.0); 
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (z < SERSIC_Z_TOLERANCE) {
                DzDa = (MGFL) 0.0;
                zz = ksersic;
        }
	#if ESTIMATE_SMALL_EXP
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDa * exp(zz)));
	}
	#else
	return((THPIX) (DzDa * exp(zz)));
	#endif
}
THPIX DsersicDb(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	MGFL DzDb = - exponentsersic * ksersic * dx * dy * pow((double) z, exponentsersic - (MGFL) 1.0); 
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (z < SERSIC_Z_TOLERANCE) {
                DzDb = (MGFL) 0.0;
                zz = ksersic;
        }
	#if ESTIMATE_SMALL_EXP
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDb * exp(zz)));
	}
	#else
	return((THPIX) (DzDb * exp(zz)));
	#endif
}
THPIX DsersicDc(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	MGFL DzDc = - exponentsersic * ksersic * pow(dx, 2) * pow(z, exponentsersic - (MGFL) 1.0); 
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (z < SERSIC_Z_TOLERANCE) {
                DzDc = (MGFL) 0.0;
                zz = ksersic;
        }
	#if ESTIMATE_SMALL_EXP
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDc * exp(zz)));
	}
	#else
	return((THPIX) (DzDc * exp(zz)));
	#endif
}
THPIX DsersicDxc(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	MGFL DzDxc = exponentsersic * ksersic * (dx * qq->a + dy * qq->b) * pow(z, exponentsersic - (MGFL) 1.0); 
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (z < SERSIC_Z_TOLERANCE) {
                DzDxc = (MGFL) 0.0;
                zz = ksersic;
        }	
	#if ESTIMATE_SMALL_EXP
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDxc * exp(zz)));
	}
	#else
	return((THPIX) (DzDxc * exp(zz)));
	#endif
}
THPIX DsersicDyc(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	MGFL DzDyc = exponentsersic * ksersic * (dx * qq->b + dy * qq->c) * pow(z, exponentsersic - (MGFL) 1.0); /* 0.875 = 7 / 8 */ 
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (z < SERSIC_Z_TOLERANCE) {
                DzDyc = (MGFL) 0.0;
                zz = ksersic;
        }
	#if ESTIMATE_SMALL_EXP
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDyc * exp(zz)));
	}
	#else
	return((THPIX) (DzDyc * exp(zz)));
	#endif
}
THPIX DsersicDre(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	MGFL DzDre = (exponentsersic * ksersic * pow(z, exponentsersic) / (MGFL) qq->re);
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (z < SERSIC_Z_TOLERANCE) {
                DzDre = (MGFL) 0.0;
                zz = ksersic;
        }
	#if ESTIMATE_SMALL_EXP
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDre * exp(zz)));
	}
	#else
	return((THPIX) (DzDre * exp(zz)));
	#endif
}
THPIX DsersicDue(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
/* 
(MAX exp(u_eff) + MIN) /  (exp(u_eff) + 1) = r_eff
exp(u_eff) = (r_eff - MIN) / (MAX - r_eff)
du_eff / dr_eff = (e(u) + 2 + e(-u)) / (max - min))
*/
	MGFL DzDue = (THPIX) (exponentsersic * ksersic * pow(z, exponentsersic) / (MGFL) qq->re * (MGFL) qq->reUe);
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (z < SERSIC_Z_TOLERANCE) {
                DzDue = (MGFL) 0.0;
                zz = ksersic;
        }
	#if ESTIMATE_SMALL_EXP
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDue * exp(zz)));
	}
	#else
	return((THPIX) (DzDue * exp(zz)));
	#endif
}

THPIX DsersicDe(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;	
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDe = -exponentsersic * ksersic * 
	(dx * (qq->ae * dx + 2.0 * qq->be * dy) + qq->ce * pow(dy, 2)) * pow(z, exponentsersic - (MGFL) 1.0);
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (z < SERSIC_Z_TOLERANCE) {
                DzDe = (MGFL) 0.0;
                zz = ksersic;
        }
	#if ESTIMATE_SMALL_EXP
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDe * exp(zz)));
	}
	#else
	return((THPIX) ((DzDe) * exp(zz)));
	#endif
}
THPIX DsersicDphi(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;	
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDphi = -exponentsersic * ksersic * 
	(dx * (qq->aphi * dx + 2.0 * qq->bphi * dy) + qq->cphi * pow(dy, 2)) * pow(z, exponentsersic - (MGFL) 1.0);
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (z < SERSIC_Z_TOLERANCE) {
                DzDphi = (MGFL) 0.0;
                zz = ksersic;
        }
	#if ESTIMATE_SMALL_EXP
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDphi * exp(zz)));
	}
	#else
	return((THPIX) (DzDphi * exp(zz)));
	#endif
}
THPIX DsersicDE(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;	
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDE = -exponentsersic * ksersic * 
	(dx * (qq->aE * dx + 2.0 * qq->bE * dy) + qq->cE * pow(dy, 2)) * pow(z, exponentsersic - (MGFL) 1.0);
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (z < SERSIC_Z_TOLERANCE) {
		DzDE = (MGFL) 0.0;
		zz = ksersic;
	}
	#if ESTIMATE_SMALL_EXP
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDE * exp(zz)));
	}
	#else
	return((THPIX) ((DzDE) * exp(zz)));
	#endif
}
THPIX DsersicDv(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;	
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDv = -exponentsersic * ksersic * 
	(dx * (qq->av * dx + 2.0 * qq->bv * dy) + qq->cv * pow(dy, 2)) * pow(z, exponentsersic - (MGFL) 1.0);
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 		
	if (z < SERSIC_Z_TOLERANCE) {
		DzDv = (MGFL) 0.0;
		zz = ksersic;
	}
	#if ESTIMATE_SMALL_EXP
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDv * exp(zz)));
	}
	#else
	return((THPIX) ((DzDv) * exp(zz)));
	#endif
}
THPIX DsersicDw(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDw = -exponentsersic * ksersic * 
	(dx * (qq->aw * dx + 2.0 * qq->bw * dy) + qq->cw * pow(dy, 2)) * pow(z, exponentsersic - (MGFL) 1.0);
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (z < SERSIC_Z_TOLERANCE) {
		DzDw = (MGFL) 0.0;
		zz = ksersic;
	}
	#if ESTIMATE_SMALL_EXP
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDw * exp(zz)));
	}
	#else
	return((THPIX) ((DzDw) * exp(zz)));
	#endif
}

THPIX DsersicDV(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDV = -exponentsersic * ksersic * 
	(dx * (qq->aV * dx + 2.0 * qq->bV * dy) + qq->cV * pow(dy, 2)) * pow(z, exponentsersic - (MGFL) 1.0);
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
       if (z < SERSIC_Z_TOLERANCE) {
                DzDV = (MGFL) 0.0;
                zz = ksersic;
        }
	#if ESTIMATE_SMALL_EXP
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDV * exp(zz)));
	}
	#else
	return((THPIX) ((DzDV) * exp(zz)));
	#endif
}
THPIX DsersicDW(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;	
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzDW = -exponentsersic * ksersic * 
	(dx * (qq->aW * dx + 2.0 * qq->bW * dy) + qq->cW * pow(dy, 2)) * pow(z, exponentsersic - (MGFL) 1.0);
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (z < SERSIC_Z_TOLERANCE) {
                DzDW = (MGFL) 0.0;
                zz = ksersic;
        }
	#if ESTIMATE_SMALL_EXP
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DzDW * exp(zz)));
	}
	#else
	return((THPIX) ((DzDW) * exp(zz)));
	#endif
}

THPIX DsersicDn(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;	
	MGFL ksersic = (MGFL) qq->k;
	MGFL kn = (MGFL) qq->kn;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL DexponentsersicDn = -(MGFL) qq->inverse_2n / (MGFL) qq->n; 
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	shAssert(z >= (MGFL) 0.0);
	shAssert(qq->n >= (THPIX) 0.0);	
	shAssert(exponentsersic > (MGFL) 0.0);
	MGFL zz = (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (z < SERSIC_Z_TOLERANCE) zz = (MGFL) -1.0;
	MGFL ww = -ksersic * zz;
	MGFL DwwDn = -(kn  * zz + ksersic * log(z) * DexponentsersicDn * (zz + (MGFL) 1.0));  
	if (z <= SERSIC_Z_TOLERANCE) DwwDn = -(kn * zz);
	#if ESTIMATE_SMALL_EXP
	if (ww < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DwwDn * exp(ww)));
	}
	#else
	return((THPIX) ((DwwDn) * exp(ww)));
	#endif
}

THPIX DsersicDN(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;	
	MGFL ksersic = (MGFL) qq->k;
	MGFL kn = (MGFL) qq->kn;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL DexponentsersicDn = -(MGFL) qq->inverse_2n / (MGFL) qq->n; 
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	shAssert(z >= (MGFL) 0.0);
	shAssert(qq->n >= (THPIX) 0.0);	
	shAssert(exponentsersic > (MGFL) 0.0);
	MGFL nN = qq->nN;
	MGFL zz = (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (z < SERSIC_Z_TOLERANCE) zz = (MGFL) -1.0;
	MGFL ww = -ksersic * zz;
	MGFL DwwDn = -nN * (kn  * zz + ksersic * log(z) * DexponentsersicDn * (zz + (MGFL) 1.0));  
	if (z <= SERSIC_Z_TOLERANCE) DwwDn = -(nN * kn * zz);
	#if ESTIMATE_SMALL_EXP
	if (ww < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) (DwwDn * exp(ww)));
	}
	#else
	return((THPIX) ((DwwDn) * exp(ww)));
	#endif
}



THPIX DDsersicDreDre(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);
	MGFL DzzDre = (exponentsersic * ksersic * pow(z, exponentsersic) / (MGFL) qq->re);
	MGFL DDzzDreDre = (-(exponentsersic + (MGFL) 1.0) * DzzDre / (MGFL) qq->re);
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) ((DDzzDreDre + DzzDre * DzzDre) * exp(zz)));
	}
	#else
	return((THPIX) ((DDzzDreDre + DzzDre * DzzDre) * exp(-ksersic * (pow(z, exponentsersic) - (MGFL) 1.0))));
	#endif
}

THPIX DDsersicDueDue(PIXPOS *p, void *q) {
	SERSICPARS *qq = (SERSICPARS *) q;
	MGFL ksersic = (MGFL) qq->k;
	MGFL exponentsersic = (MGFL) qq->inverse_2n;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL z = dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2);	
	MGFL DzzDre = (exponentsersic * ksersic * pow(z, exponentsersic) / (MGFL) qq->re);	
	MGFL DDzzDreDre = (-(exponentsersic + (MGFL) 1.0) * DzzDre / (MGFL) qq->re);
	MGFL DzzDue = (DzzDre * (MGFL) qq->reUe);
	MGFL DDzzDueDue = DzzDre * (MGFL) qq->reUeUe + pow((MGFL) qq->reUe, 2.0) * DDzzDreDre;
	#if ESTIMATE_SMALL_EXP
	MGFL zz = -ksersic * (pow(z, exponentsersic) - (MGFL) 1.0); 
	if (zz < SMALL_EXPONENT) {
		return((THPIX) 0.0);
	} else {
		return((THPIX) ((DDzzDueDue + DzzDue * DzzDue) * exp(zz)));
	}
	#else
	return((THPIX) ((DDzzDueDue + DzzDue * DzzDue) * exp(-ksersic * (pow(z, exponentsersic) - (MGFL) 1.0))));
	#endif
}

/* core sersic pixel functions */

/* 
 *
 *
 *
	CORESERSICPARS *qq = (CORESERSICPARS *) q;
	MGFL b = (MGFL) qq->k;
	MGFL delta = (MGFL) qq->delta;
	MGFL n = (MGFL) qq->n;
	MGFL rb = (MGFL) qq->rb;
	MGFL re = (MGFL) qq->re;
	MGFL g1 = (MGFL) qq->gamma;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL r_re = pow(dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2), 0.5);
	MGFL rb_re = qq->rb / qq->re;
	MGFL rb_r = rb_re / r_re;
	MGFL r = r_re * re;

	MGFL w = (MGFL) 1.0 + pow(rb_r, delta);
	MGFL z = pow(r_re, delta) + pow(rb_re, delta);

	MGFL f = pow(w, g1 / delta) * exp(-b * pow(z, (MGFL) 1.0 / (n * delta)));

	MGFL DbDn = (MGFL) qq->kn;

	MGFL DfDw = g1 / delta * f / w;
	MGFL DfDz = - b * pow(z, (MGFL) 1.0 / (n * delta)) * f / (z * delta * n);
	MGFL DfDb = - f * pow(z, (MGFL) 1.0 / (delta * n));
	MGFL DfDgamma = f * log(w) / delta;
	MGFL DfDn = (f * b * log(z) * pow(z, (MGFL) 1.0 / (n * delta))) / (delta * n * n);
	MGFL DfDdelta = - ( g1 * log(w) - b * log(z) * pow(z, (MGFL) 1.0 / (n * delta)) / n) * f / (delta * delta);
	
	MGFL DwDdelta = (w - (MGFL) 1.0) * log(rb_r);
	MGFL DwDrb = (w - (MGFL) 1.0) * delta  / rb;
	MGFL DwDr = -(w - (MGFL) 1.0) * delta / r;

	MGFL DzDdelta = pow(r_re, delta) * log(r_re) + pow(rb_re, delta) * log(rb_re);
	MGFL DzDr = delta * pow(r_re, delta) / (r_re * re);
	MGFL DzDrb = delta * pow(rb_re, delta) / rb;
	MGFL DzDre = - delta * z / re;


	MGFL dfddelta = DfDdelta + DfDw * DwDdelta + DfDz * DzDdelta;
	MGFL dfdr = DfDw * DwDr + DfDz * DzDr;
	MGFL dfdrb = DfDw * DwDrb + DfDz * DzDrb;
	MGFL dfdre = DfDz * DzDre;
	MGFL dfdn = DfDb * DbDn + DfDn;
	MGFL dfdgamma = DfDgamma;

	MGFL re_sq_r = re * re / r;
	MGFL drdxc = -(qq->a * dx + qq->b * dy) * re_sq_r;
	MGFL drdyc = -(qq->b * dx + qq->c * dy) * re_sq_r;
	MGFL drdphi = (qq->aphi *dx*dx + 2.0 * qq->bphi * dx * dy + qq->cphi *dy*dy) * re_sq_r;
	MGFL drde = (qq->ae * dx * dx + 2.0 * qq->be * dx * dy + qq->ce * dy * dy) * re_sq_r;
		
	MGFL dfdxc = dfdr * drdxc;
	MGFL dfdyc = dfdr * drdyc;
	MGFL dfdphi = dfdr * drdphi;
	MGFL dfde = dfdr * drde;
	MGFL dfdE = dfdr * drde * (qq->eE);
	
	MGFL dfdUe = dfdre * (qq->reUe);
	MGFL dfdUb = dfdrb * (qq->rbUb);
	MGFL dfdN = dfdn * (qq->nN);
	MGFL dfdD = dfddelta * (qq->dD);
	MGFL dfdG = dfdgamma * (qq->gG);

*/	

THPIX coresersic(PIXPOS *p, void *q) {

	CORESERSICPARS *qq = (CORESERSICPARS *) q;
	MGFL b = (MGFL) qq->k;
	MGFL delta = (MGFL) qq->delta;
	/* 
	MGFL n = (MGFL) qq->n;
	*/
	MGFL g1 = (MGFL) qq->gamma;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL r_re = pow(dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2), 0.5);
	MGFL rb_re = qq->rb / qq->re;
	MGFL rb_r = rb_re / r_re;
	MGFL i_n_x_delta = (MGFL) 1.0 / (qq->n * qq->delta);
	
	MGFL w = (MGFL) 1.0 + pow(rb_r, delta);
	MGFL z = pow(r_re, delta) + pow(rb_re, delta);

	MGFL f = pow(w, g1 / delta) * exp(-b * pow(z, i_n_x_delta));
	
	return((THPIX) f);	
}

THPIX DcoresersicDre(PIXPOS *p, void *q) {

	CORESERSICPARS *qq = (CORESERSICPARS *) q;
	MGFL b = (MGFL) qq->k;
	MGFL delta = (MGFL) qq->delta;
	MGFL g1_delta = (MGFL) (qq->gamma / qq->delta);
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL r_re = pow(dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2), 0.5);
	MGFL rb_re = qq->rb / qq->re;
	MGFL rb_r = rb_re / r_re;
	MGFL i_n_x_delta = (MGFL) 1.0 / (qq->n * qq->delta);
	MGFL delta_re = (MGFL) (qq->delta / qq->re);
	MGFL w = (MGFL) 1.0 + pow(rb_r, delta);
	MGFL z = pow(r_re, delta) + pow(rb_re, delta);

	MGFL f = pow(w, g1_delta) * exp(-b * pow(z, i_n_x_delta));

	MGFL DfDz = - b * pow(z, i_n_x_delta) * f * i_n_x_delta / z;
	MGFL DzDre = - delta_re * z;

	MGFL dfdre = DfDz * DzDre;

	return((THPIX) dfdre);	
}


THPIX DcoresersicDue(PIXPOS *p, void *q) {

	CORESERSICPARS *qq = (CORESERSICPARS *) q;
	MGFL b = (MGFL) qq->k;
	MGFL delta = (MGFL) qq->delta;
	MGFL g1_delta = (MGFL) (qq->gamma / qq->delta);
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL r_re = pow(dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2), 0.5);
	MGFL rb_re = qq->rb / qq->re;
	MGFL rb_r = rb_re / r_re;
	MGFL i_n_x_delta = (MGFL) 1.0 / (qq->n * qq->delta);
        MGFL delta_re = (MGFL) (qq->delta / qq->re);
	MGFL w = (MGFL) 1.0 + pow(rb_r, delta);
	MGFL z = pow(r_re, delta) + pow(rb_re, delta);

	MGFL f = pow(w, g1_delta) * exp(-b * pow(z, i_n_x_delta));

	MGFL DfDz = - b * pow(z, i_n_x_delta) * f * i_n_x_delta / z;
	MGFL DzDre = - delta_re * z;
	MGFL dfdre = DfDz * DzDre;
	MGFL dfdUe = dfdre * (qq->reUe);
	return((THPIX) dfdUe);	
}




THPIX DcoresersicDrb(PIXPOS *p, void *q) {

	CORESERSICPARS *qq = (CORESERSICPARS *) q;
	MGFL b = (MGFL) qq->k;
	MGFL delta = (MGFL) qq->delta;
	MGFL g1_delta = (MGFL) (qq->gamma / qq->delta);
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL r_re = pow(dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2), 0.5);
	MGFL rb_re = qq->rb / qq->re;
	MGFL rb_r = rb_re / r_re;
	MGFL delta_rb = qq->delta / qq->rb;

	MGFL r_re_p_delta = pow(r_re, delta);
	MGFL rb_re_p_delta = pow(rb_re, delta);
	MGFL rb_r_p_delta = pow(rb_r, delta);

	MGFL i_n_x_delta = (MGFL) 1.0 / (qq->n * qq->delta);

	MGFL w = (MGFL) 1.0 + rb_r_p_delta;
	MGFL z = r_re_p_delta + rb_re_p_delta;

	MGFL f = pow(w, g1_delta) * exp(-b * pow(z, i_n_x_delta));

	MGFL DfDw = g1_delta * f / w;
	MGFL DfDz = - b * pow(z, i_n_x_delta) * f * i_n_x_delta / z;
	
	MGFL DwDrb = rb_r_p_delta * delta_rb;
	MGFL DzDrb = rb_re_p_delta * delta_rb;

	MGFL dfdrb = DfDw * DwDrb + DfDz * DzDrb;
	
	return((THPIX) dfdrb);	
}


THPIX DcoresersicDub(PIXPOS *p, void *q) {

	CORESERSICPARS *qq = (CORESERSICPARS *) q;
	MGFL b = (MGFL) qq->k;
	MGFL delta = (MGFL) qq->delta;
	MGFL g1_delta = (MGFL) (qq->gamma / qq->delta);
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL r_re = pow(dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2), 0.5);
	MGFL rb_re = qq->rb / qq->re;
	MGFL rb_r = rb_re / r_re;
	MGFL delta_rb = delta / qq->rb;

	MGFL r_re_p_delta = pow(r_re, delta);
	MGFL rb_re_p_delta = pow(rb_re, delta);
	MGFL rb_r_p_delta = pow(rb_r, delta);

	MGFL i_n_x_delta = (MGFL) 1.0 / (qq->n * qq->delta);

	MGFL w = (MGFL) 1.0 + rb_r_p_delta;
	MGFL z = r_re_p_delta + rb_re_p_delta;

	MGFL f = pow(w, g1_delta) * exp(-b * pow(z, i_n_x_delta));

	MGFL DfDw = g1_delta * f / w;
	MGFL DfDz = - b * pow(z, i_n_x_delta) * f * i_n_x_delta / z;
	
	MGFL DwDrb = rb_r_p_delta * delta_rb;
	MGFL DzDrb = rb_re_p_delta * delta_rb;

	MGFL dfdrb = DfDw * DwDrb + DfDz * DzDrb;
	MGFL dfdUb = dfdrb * (qq->rbUb);

	return((THPIX) dfdUb);	
}



THPIX DcoresersicDxc(PIXPOS *p, void *q) {

	CORESERSICPARS *qq = (CORESERSICPARS *) q;
	MGFL b = (MGFL) qq->k;
	MGFL delta = (MGFL) qq->delta;
	MGFL g1_delta = (MGFL) (qq->gamma / qq->delta);
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL r_re = pow(dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2), 0.5);
	MGFL rb_re = qq->rb / qq->re;
	MGFL rb_r = rb_re / r_re;
	MGFL re_sq_r = qq->re / r_re;
	MGFL delta_r = delta / (r_re * qq->re);

	MGFL rb_r_p_delta = pow(rb_r, delta);
	MGFL rb_re_p_delta = pow(rb_re, delta);
	MGFL r_re_p_delta = pow(r_re, delta);
	MGFL i_n_x_delta = (MGFL) 1.0 / (qq->n * qq->delta);

	MGFL w = (MGFL) 1.0 + rb_r_p_delta;
	MGFL z = r_re_p_delta + rb_re_p_delta;

	MGFL f = pow(w, g1_delta) * exp(-b * pow(z, i_n_x_delta));

	MGFL DfDw = g1_delta * f / w;
	MGFL DfDz = - b * pow(z, i_n_x_delta) * f * i_n_x_delta / z;

	MGFL DwDr = -rb_r_p_delta * delta_r;
	MGFL DzDr = r_re_p_delta * delta_r;

	MGFL dfdr = DfDw * DwDr + DfDz * DzDr;
	MGFL drdxc = -(qq->a * dx + qq->b * dy) * re_sq_r;
	MGFL dfdxc = dfdr * drdxc;

	return((THPIX) dfdxc);	
}


THPIX DcoresersicDyc(PIXPOS *p, void *q) {

	CORESERSICPARS *qq = (CORESERSICPARS *) q;
	MGFL b = (MGFL) qq->k;
	MGFL delta = (MGFL) qq->delta;
	MGFL g1_delta = (MGFL) (qq->gamma / qq->delta);
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL r_re = pow(dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2), 0.5);
	MGFL rb_re = qq->rb / qq->re;
	MGFL rb_r = rb_re / r_re;
	MGFL re_sq_r = qq->re / r_re;
	MGFL delta_r = delta / (r_re * qq->re);

	MGFL rb_r_p_delta = pow(rb_r, delta);
	MGFL rb_re_p_delta = pow(rb_re, delta);
	MGFL r_re_p_delta = pow(r_re, delta);
	MGFL i_n_x_delta = (MGFL) 1.0 / (qq->n * qq->delta);

	MGFL w = (MGFL) 1.0 + rb_r_p_delta;
	MGFL z = r_re_p_delta + rb_re_p_delta;

	MGFL f = pow(w, g1_delta) * exp(-b * pow(z, i_n_x_delta));

	MGFL DfDw = g1_delta * f / w;
	MGFL DfDz = - b * pow(z, i_n_x_delta) * f * i_n_x_delta / z;

	MGFL DwDr = -rb_r_p_delta * delta_r;
	MGFL DzDr = r_re_p_delta * delta_r;

	MGFL dfdr = DfDw * DwDr + DfDz * DzDr;
	MGFL drdyc = -(qq->b * dx + qq->c * dy) * re_sq_r;
	MGFL dfdyc = dfdr * drdyc;
	
	return((THPIX) dfdyc);	
}


THPIX DcoresersicDphi(PIXPOS *p, void *q) {

	CORESERSICPARS *qq = (CORESERSICPARS *) q;
	MGFL b = (MGFL) qq->k;
	MGFL delta = (MGFL) qq->delta;
	MGFL g1_delta = (MGFL) (qq->gamma / qq->delta);
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL r_re = pow(dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2), 0.5);
	MGFL rb_re = qq->rb / qq->re;
	MGFL rb_r = rb_re / r_re;
	MGFL re_sq_r = qq->re / r_re;

	MGFL rb_r_p_delta = pow(rb_r, delta);
	MGFL rb_re_p_delta = pow(rb_re, delta);
	MGFL r_re_p_delta = pow(r_re, delta);
	MGFL i_n_x_delta = (MGFL) 1.0 / (qq->n * qq->delta);
	MGFL delta_r = qq->delta / (r_re * qq->re);

	MGFL w = (MGFL) 1.0 + rb_r_p_delta;
	MGFL z = r_re_p_delta + rb_re_p_delta;

	MGFL f = pow(w, g1_delta) * exp(-b * pow(z, i_n_x_delta));

	MGFL DfDw = g1_delta * f / w;
	MGFL DfDz = - b * pow(z, i_n_x_delta) * f * i_n_x_delta / z;

	MGFL DwDr = -rb_r_p_delta * delta_r;
	MGFL DzDr = r_re_p_delta * delta_r;

	MGFL dfdr = DfDw * DwDr + DfDz * DzDr;
	MGFL drdphi = (qq->aphi *dx*dx + 2.0 * qq->bphi * dx * dy + qq->cphi *dy*dy) * re_sq_r;
	MGFL dfdphi = dfdr * drdphi;
	
	return((THPIX) dfdphi);	
}

THPIX DcoresersicDe(PIXPOS *p, void *q) {

	
	CORESERSICPARS *qq = (CORESERSICPARS *) q;
	MGFL b = (MGFL) qq->k;
	MGFL delta = (MGFL) qq->delta;
	MGFL g1_delta = (MGFL) (qq->gamma / qq->delta);
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL r_re = pow(dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2), 0.5);
	MGFL rb_re = qq->rb / qq->re;
	MGFL rb_r = rb_re / r_re;
	MGFL re_sq_r = qq->re / r_re;

	MGFL rb_r_p_delta = pow(rb_r, delta);
	MGFL rb_re_p_delta = pow(rb_re, delta);
	MGFL r_re_p_delta = pow(r_re, delta);
	MGFL i_n_x_delta = (MGFL) 1.0 / (qq->n * qq->delta);
	MGFL delta_r = qq->delta / (r_re * qq->re);

	MGFL w = (MGFL) 1.0 + rb_r_p_delta;
	MGFL z = r_re_p_delta + rb_re_p_delta;

	MGFL f = pow(w, g1_delta) * exp(-b * pow(z, i_n_x_delta));

	MGFL DfDw = g1_delta * f / w;
	MGFL DfDz = - b * pow(z, i_n_x_delta) * f * i_n_x_delta / z;

	MGFL DwDr = -rb_r_p_delta * delta_r;
	MGFL DzDr = r_re_p_delta * delta_r;

	MGFL dfdr = DfDw * DwDr + DfDz * DzDr;
	MGFL drde = (qq->ae * dx * dx + 2.0 * qq->be * dx * dy + qq->ce * dy * dy) * re_sq_r;		
	MGFL dfde = dfdr * drde;

	return((THPIX) dfde);	
}


THPIX DcoresersicDE(PIXPOS *p, void *q) {


	CORESERSICPARS *qq = (CORESERSICPARS *) q;
	MGFL b = (MGFL) qq->k;
	MGFL delta = (MGFL) qq->delta;
	MGFL g1_delta = (MGFL) (qq->gamma / qq->delta);
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL r_re = pow(dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2), 0.5);
	MGFL rb_re = qq->rb / qq->re;
	MGFL rb_r = rb_re / r_re;
	MGFL re_sq_r = qq->re / r_re;

	MGFL rb_r_p_delta = pow(rb_r, delta);
	MGFL rb_re_p_delta = pow(rb_re, delta);
	MGFL r_re_p_delta = pow(r_re, delta);
	MGFL i_n_x_delta = (MGFL) 1.0 / (qq->n * qq->delta);
	MGFL delta_r = qq->delta / (r_re * qq->re);

	MGFL w = (MGFL) 1.0 + rb_r_p_delta;
	MGFL z = r_re_p_delta + rb_re_p_delta;

	MGFL f = pow(w, g1_delta) * exp(-b * pow(z, i_n_x_delta));

	MGFL DfDw = g1_delta * f / w;
	MGFL DfDz = - b * pow(z, i_n_x_delta) * f * i_n_x_delta / z;

	MGFL DwDr = -rb_r_p_delta * delta_r;
	MGFL DzDr = r_re_p_delta * delta_r;

	MGFL dfdr = DfDw * DwDr + DfDz * DzDr;
	MGFL drde = (qq->ae * dx * dx + 2.0 * qq->be * dx * dy + qq->ce * dy * dy) * re_sq_r;		
	MGFL dfdE = dfdr * drde * (qq->eE);
	
	return((THPIX) dfdE);	
}


THPIX DcoresersicDgamma(PIXPOS *p, void *q) {

	CORESERSICPARS *qq = (CORESERSICPARS *) q;
	MGFL b = (MGFL) qq->k;
	MGFL delta = (MGFL) qq->delta;
	MGFL g1 = (MGFL) qq->gamma;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL r_re = pow(dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2), 0.5);
	MGFL rb_re = qq->rb / qq->re;
	MGFL rb_r = rb_re / r_re;
	MGFL i_n_x_delta = (MGFL) 1.0 / (qq->n * qq->delta);
	
	MGFL w = (MGFL) 1.0 + pow(rb_r, delta);
	MGFL z = pow(r_re, delta) + pow(rb_re, delta);

/*
	MGFL f = pow(w, g1 / delta) * exp(-b * pow(z, i_n_x_delta));
	MGFL DfDgamma = f * log(w) / delta;
	MGFL dfdgamma = DfDgamma;
	return((THPIX) dfdgamma);	

*/
	MGFL DfDgamma = pow(w, g1 / delta) * exp(-b * pow(z, i_n_x_delta)) * log(w) / delta;
	return((THPIX) DfDgamma);
}



THPIX DcoresersicDG(PIXPOS *p, void *q) {

	CORESERSICPARS *qq = (CORESERSICPARS *) q;
	MGFL b = (MGFL) qq->k;
	MGFL delta = (MGFL) qq->delta;
	MGFL g1 = (MGFL) qq->gamma;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL r_re = pow(dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2), 0.5);
	MGFL rb_re = qq->rb / qq->re;
	MGFL rb_r = rb_re / r_re;
	MGFL i_n_x_delta = (MGFL) 1.0 / (qq->n * qq->delta);
	
	MGFL w = (MGFL) 1.0 + pow(rb_r, delta);
	MGFL z = pow(r_re, delta) + pow(rb_re, delta);

/*
	MGFL f = pow(w, g1 / delta) * exp(-b * pow(z, i_n_x_delta));
	MGFL DfDgamma = f * log(w) / delta;
	MGFL dfdgamma = DfDgamma;
	MGFL DfDgamma = pow(w, g1 / delta) * exp(-b * pow(z, i_n_x_delta)) * log(w) / delta;
	return((THPIX) dfdgamma);	

*/

	MGFL dfdG = pow(w, g1 / delta) * exp(-b * pow(z, i_n_x_delta)) * log(w) / delta * (qq->gG);
	return((THPIX) dfdG);	
}


THPIX DcoresersicDdelta(PIXPOS *p, void *q) {

	CORESERSICPARS *qq = (CORESERSICPARS *) q;
	MGFL b = (MGFL) qq->k;
	MGFL delta = (MGFL) qq->delta;
	MGFL n = (MGFL) qq->n;
	MGFL g1 = (MGFL) qq->gamma;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;

	MGFL r_re = pow(dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2), 0.5);
	MGFL rb_re = qq->rb / qq->re;
	MGFL rb_r = rb_re / r_re;

	MGFL r_re_p_delta = pow(r_re, delta);
	MGFL rb_re_p_delta = pow(rb_re, delta);
	MGFL rb_r_p_delta = pow(rb_r, delta);


	MGFL w = (MGFL) 1.0 + rb_r_p_delta;
	MGFL z = r_re_p_delta + rb_re_p_delta;
	MGFL i_n_x_delta = (MGFL) 1.0 / (n * delta);

	MGFL f = pow(w, g1 / delta) * exp(-b * pow(z, i_n_x_delta));

	MGFL DfDw = g1 / delta * f / w;
	MGFL DfDz = - b * pow(z, i_n_x_delta) * f * i_n_x_delta / z;
	MGFL DfDdelta = - ( g1 * log(w) - b * log(z) * pow(z, i_n_x_delta) / n) * f / (delta * delta);
	
	MGFL DwDdelta = rb_r_p_delta * log(rb_r);
	MGFL DzDdelta = r_re_p_delta * log(r_re) + rb_re_p_delta * log(rb_re);

	MGFL dfddelta = DfDdelta + DfDw * DwDdelta + DfDz * DzDdelta;
	
	return((THPIX) dfddelta);	
}


THPIX DcoresersicDD(PIXPOS *p, void *q) {

	CORESERSICPARS *qq = (CORESERSICPARS *) q;
	MGFL b = (MGFL) qq->k;
	MGFL delta = (MGFL) qq->delta;
	MGFL n = (MGFL) qq->n;
	MGFL g1_delta = (MGFL) (qq->gamma / qq->delta);
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;

	MGFL r_re = pow(dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2), 0.5);
	MGFL rb_re = qq->rb / qq->re;
	MGFL rb_r = rb_re / r_re;

	MGFL r_re_p_delta = pow(r_re, delta);
	MGFL rb_re_p_delta = pow(rb_re, delta);
	MGFL rb_r_p_delta = pow(rb_r, delta);


	MGFL w = (MGFL) 1.0 + rb_r_p_delta;
	MGFL z = r_re_p_delta + rb_re_p_delta;
	MGFL i_n_x_delta = (MGFL) 1.0 / (n * delta);

	MGFL f = pow(w, g1_delta) * exp(-b * pow(z, i_n_x_delta));

	MGFL DfDw = g1_delta * f / w;
	MGFL DfDz = - b * pow(z, i_n_x_delta) * f * i_n_x_delta / z;
	MGFL DfDdelta = - ( g1_delta * log(w) - b * log(z) * pow(z, i_n_x_delta) * i_n_x_delta) * f / (delta);
	
	MGFL DwDdelta = rb_r_p_delta * log(rb_r);
	MGFL DzDdelta = r_re_p_delta * log(r_re) + rb_re_p_delta * log(rb_re);

/* 
	MGFL dfddelta = DfDdelta + DfDw * DwDdelta + DfDz * DzDdelta;
	MGFL dfdD = dfddelta * (qq->dD);
*/
	MGFL dfdD = (DfDdelta + DfDw * DwDdelta + DfDz * DzDdelta) * (qq->dD);

	return((THPIX) dfdD);	
}


THPIX DcoresersicDn(PIXPOS *p, void *q) {

	CORESERSICPARS *qq = (CORESERSICPARS *) q;
	MGFL b = (MGFL) qq->k;
	MGFL delta = (MGFL) qq->delta;
	MGFL g1 = (MGFL) qq->gamma;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL r_re = pow(dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2), 0.5);
	MGFL rb_re = qq->rb / qq->re;
	MGFL rb_r = rb_re / r_re;
	MGFL b_n = (MGFL) (qq->k / qq->n);

	MGFL i_n_x_delta = (MGFL) 1.0 / (qq->n * qq->delta);
	MGFL w = (MGFL) 1.0 + pow(rb_r, delta);
	MGFL z = pow(r_re, delta) + pow(rb_re, delta);

	MGFL f = pow(w, g1 / delta) * exp(-b * pow(z, i_n_x_delta));

	MGFL DbDn = (MGFL) qq->kn;

/* 
	MGFL DfDb = - f * pow(z,  i_n_x_delta);
	MGFL DfDn = (f * b * log(z) * pow(z, i_n_x_delta) * i_n_x_delta)  / n;
*/
	MGFL DfDb = - f * pow(z,  i_n_x_delta);
	MGFL DfDn = -DfDb * b_n * log(z) * i_n_x_delta;

	MGFL dfdn = DfDb * DbDn + DfDn;

	return((THPIX) dfdn);	
}

THPIX DcoresersicDN(PIXPOS *p, void *q) {

	CORESERSICPARS *qq = (CORESERSICPARS *) q;
	MGFL b = (MGFL) qq->k;
	MGFL delta = (MGFL) qq->delta;
	MGFL g1 = (MGFL) qq->gamma;
	MGFL dx = p->row - qq->xc;
	MGFL dy = p->col - qq->yc;
	MGFL r_re = pow(dx * (qq->a * dx + 2.0 * qq->b * dy) + qq->c * pow(dy, 2), 0.5);
	MGFL rb_re = qq->rb / qq->re;
	MGFL rb_r = rb_re / r_re;
	MGFL b_n = (MGFL) (qq->k / qq->n);

	MGFL i_n_x_delta = (MGFL) 1.0 / (qq->n * qq->delta);
	MGFL w = (MGFL) 1.0 + pow(rb_r, delta);
	MGFL z = pow(r_re, delta) + pow(rb_re, delta);

	MGFL f = pow(w, g1 / delta) * exp(-b * pow(z, i_n_x_delta));

/* 
	MGFL DbDn = (MGFL) qq->kn;
	MGFL DfDb = - f * pow(z,  i_n_x_delta);
	MGFL DfDn = (f * b * log(z) * pow(z, i_n_x_delta) * i_n_x_delta)  / n;
	MGFL dfdn = DfDb * DbDn + DfDn;
	MGFL dfdN = dfdn * (qq->nN);
*/
	MGFL DfDb = - f * pow(z,  i_n_x_delta);
	MGFL DfDn = -DfDb * b_n * log(z) * i_n_x_delta;

	MGFL dfdN = (DfDb * qq->kn + DfDn) * (qq->nN);
	return((THPIX) dfdN);	
}




/* THREGION specifics */

RET_CODE StarSpecifyRegion(void *q, PSF_CONVOLUTION_INFO *psf_info, THREGION *threg, int index, int reprefab) {
char *name = "StarSpecifyRegion";

if (q == NULL || threg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (index < 0 && index > 1) {
	thError("%s: ERROR - unsupported index (%d)", name, index);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
THREGION_TYPE regtype = threg->type;
if (regtype == UNKNOWN_THREGION_TYPE) {
	if (threg->threg != NULL) {
		thError("%s: ERROR - improperly allocated (thregion), type UNKNOWN while a non-null value found",
			name);
		return(SH_GENERIC_ERROR);
	}
	status = thRegRenew(threg, "", TYPE_THPIX, THREGION2_TYPE);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not renew (thregion)", name);
		return(status);
	}
} else if (regtype == THREGION1_TYPE) {
	thError("%s: ERROR - unsupported (thregion) type 'THREGION2_TYPE'", name);
	return(SH_GENERIC_ERROR);
} else if (regtype != THREGION2_TYPE) {
	thError("%s: ERROR - unsupported (thregion) type", name);
	return(SH_GENERIC_ERROR);
}
THREGION2 *threg2;
threg2 = threg->threg;

if (threg2 == NULL) {
	threg2 = thRegion2New("init", TYPE_THPIX);
	threg->threg = threg2;
	return(SH_SUCCESS);
	}

int nrow, ncol, idr, idc;
FABRICATION_FLAG fab = threg2->fab_i[index];
PRODUCTION_FLAG production;
if (index == OUTER_PSF) {
	production = RUNTIME;
} else if (index == INNER_PSF) {
	production = SAVED;
}
if (fab == INIT) {
	
	RET_CODE (*get_reg_pars) (void *, PSF_CONVOLUTION_INFO *, int *, int *, int *, int *, THPIX *, THPIX *);
	if (index == OUTER_PSF) {
		get_reg_pars = &central_star_get_reg_pars;
	} else if (index == INNER_PSF) {
		get_reg_pars = &wing_star_get_reg_pars;
	} else {
		thError("%s: ERROR - unsupported index (%d)", name, index);
		return(SH_GENERIC_ERROR);
	}
	PSF_CONVOLUTION_INFO *psf_info = NULL;
	status = thRegGetPsfConvInfo(threg, &psf_info);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (psf_info) from (thregion)", name);
		return(status);
	}
	status = get_reg_pars(q, psf_info, &idr, &idc, &nrow, &ncol, NULL, NULL);		
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get region parameters for model to prefab", 
			name);
		return(status);
	}
	#if DEEP_DEBUG_MGPROFILE
	printf("%s: prefab: (idr, idc) = (%d, %d), (nrow, ncol) = (%d, %d) \n", name, idr, idc, nrow, ncol);
	#endif
	status = thReg2Prefab(threg2, -idr, -idc, nrow, ncol, production, index);		if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not 'PREFAB' thregion", name);
		return(status);
	}
	return(SH_SUCCESS);
}

if (index == OUTER_PSF && fab == PREFAB) {
	status = thReg2Fab(threg2, NULL, index);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not FAB region for inner star", name);	
		return(status);
	}
	return(SH_SUCCESS);
} else if (index == INNER_PSF && fab == PREFAB) {
	return(SH_SUCCESS);
} else if (fab == FAB || fab == PSFCONVOLVED) {
	return(SH_SUCCESS);
}

if (schema_fabrication_flag == NULL) init_static_vars ();
thError("%s: ERROR - unsupported (fabrication_flag) '%s'",
	 name, (schema_fabrication_flag->elems[fab]).name);
return(SH_GENERIC_ERROR);

}

RET_CODE GalaxySpecifyRegion(void *q, PSF_CONVOLUTION_INFO *psf_info, THREGION *threg, int index, int reprefab,
				void *specify_reg_pars) {
char *name = "GalaxySpecifyRegion";
if (specify_reg_pars == NULL) {
	thError("%s: ERROR - null function for region parameter specification", name);
	return(SH_GENERIC_ERROR);
}

if (q == NULL || threg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (index != 0 && index != BAD_PSF_INDEX) {
	thError("%s: ERROR - unsupported index (%d)", name, index);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
THREGION_TYPE regtype = threg->type;
if (regtype == UNKNOWN_THREGION_TYPE) {
	if (threg->threg != NULL) {
		thError("%s: ERROR - improperly allocated (thregion), type UNKNOWN while a non-null value found",
			name);
		return(SH_GENERIC_ERROR);
	}
	status = thRegRenew(threg, "", TYPE_THPIX, THREGION1_TYPE);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not renew (thregion)", name);
		return(status);
	}
} else if (regtype == THREGION2_TYPE) {
	thError("%s: ERROR - unsupported (thregion) type 'THREGION2_TYPE'", name);
	return(SH_GENERIC_ERROR);
} else if (regtype != THREGION1_TYPE) {
	thError("%s: ERROR - unsupported (thregion) type", name);
	return(SH_GENERIC_ERROR);
}
THREGION1 *threg1;
threg1 = threg->threg;

if (threg1 == NULL) {
	threg1 = thRegion1New("init", TYPE_THPIX);
	threg->threg = threg1;
	return(SH_SUCCESS);
	}

int nrow = 0, ncol = 0, idr = 0, idc = 0;
FABRICATION_FLAG fab = threg1->fab;
if (fab == INIT || (fab == PREFAB && reprefab)) {
	if (fab == PREFAB) {
		status = thReg1Reinit(threg1);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not re-init prefab'ed (threg1)", name);
			return(status);
		}
	}
	RET_CODE (*get_reg_pars) (void *, PSF_CONVOLUTION_INFO *, int *, int *, int *, int *, THPIX *, THPIX *);
	get_reg_pars = specify_reg_pars;	
	status = get_reg_pars(q, psf_info, &idr, &idc, &nrow, &ncol, NULL, NULL);		
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get region parameters for model to prefab", 
			name);
		return(status);
	}
	status = thReg1Prefab(threg1, -idr, -idc, nrow, ncol, RUNTIME);		
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not 'PREFAB' thregion", name);
		return(status);
	}
	if (fab == INIT) return(SH_SUCCESS);
} 

if (fab == PREFAB) {
	status = thRegFab(threg, NULL, NULL, NULL, index);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not FAB region for inner star", name);	
		return(status);
	}
	return(SH_SUCCESS);
} else if (fab == FAB || fab == PSFCONVOLVED) {
	return(SH_SUCCESS);
}


if (schema_fabrication_flag == NULL) init_static_vars ();
thError("%s: ERROR - unsupported (fabrication_flag) '%s'",
	 name, (schema_fabrication_flag->elems[fab]).name);
return(SH_GENERIC_ERROR);
}

RET_CODE GaussianGalaxySpecifyRegion(void *q, PSF_CONVOLUTION_INFO *psf_info, THREGION *threg, int index, int reprefab) {
char *name = "GaussianGalaxySpecifyRegion";
RET_CODE status;
char *qtype = "GAUSSIANPARS";
status = thCTransform(q, qtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not transform elliptical coordinates into the required form", name);
	return(status);
}
status = GalaxySpecifyRegion(q, psf_info, threg, index, reprefab, &gaussian_get_reg_pars);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not specifiy region for model 'gaussian'", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE NullGalaxySpecifyRegion(void *q, PSF_CONVOLUTION_INFO *psf_info, THREGION *threg, int index, int reprefab) {
char *name = "NullGalaxySpecifyRegion";
RET_CODE status;

status = GalaxySpecifyRegion(q, psf_info, threg, index, reprefab, &Null_get_reg_pars);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not specifiy region for model 'deV'", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE deVGalaxySpecifyRegion(void *q, PSF_CONVOLUTION_INFO *psf_info, THREGION *threg, int index, int reprefab) {
char *name = "deVGalaxySpecifyRegion";
RET_CODE status;
char *qtype = "DEVPARS";
status = thCTransform(q, qtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not transform elliptical coordinates into the required form", name);
	return(status);
}
status = GalaxySpecifyRegion(q, psf_info, threg, index, reprefab, &deV_get_reg_pars);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not specifiy region for model 'deV'", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE ExpGalaxySpecifyRegion(void *q, PSF_CONVOLUTION_INFO *psf_info, THREGION *threg, int index, int reprefab) {
char *name = "ExpGalaxySpecifyRegion";
RET_CODE status;
char *qtype = "EXPPARS";
status = thCTransform(q, qtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not transform elliptical coordinates into the required form", name);
	return(status);
}
status = GalaxySpecifyRegion(q, psf_info, threg, index, reprefab, &Exp_get_reg_pars);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not specifiy region for model 'exp'", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE PlGalaxySpecifyRegion(void *q, PSF_CONVOLUTION_INFO *psf_info, THREGION *threg, int index, int reprefab) {
char *name = "PlGalaxySpecifyRegion";
RET_CODE status;
char *qtype = "POWERLAWPARS";
status = thCTransform(q, qtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not transform elliptical coordinates into the required form", name);
	return(status);
}
status = GalaxySpecifyRegion(q, psf_info, threg, index, reprefab, &Pl_get_reg_pars);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not specifiy region for model 'Pl'", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE sersicGalaxySpecifyRegion(void *q, PSF_CONVOLUTION_INFO *psf_info, THREGION *threg, int index, int reprefab) {
char *name = "sersicGalaxySpecifyRegion";
RET_CODE status;
char *qtype = "SERSICPARS";
status = thCTransform(q, qtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not transform elliptical coordinates into the required form", name);
	return(status);
}
status = GalaxySpecifyRegion(q, psf_info, threg, index, reprefab, &sersic_get_reg_pars);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not specifiy region for model 'sersic'", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE coresersicGalaxySpecifyRegion(void *q, PSF_CONVOLUTION_INFO *psf_info, THREGION *threg, int index, int reprefab) {
char *name = "coresersicGalaxySpecifyRegion";
RET_CODE status;
char *qtype = "CORESERSICPARS";
status = thCTransform(q, qtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not transform elliptical coordinates into the required form", name);
	return(status);
}
status = GalaxySpecifyRegion(q, psf_info, threg, index, reprefab, &coresersic_get_reg_pars);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not specifiy region for model 'coresersic'", name);
	return(status);
}

return(SH_SUCCESS);
}


/* psf_info_updaters */

RET_CODE p_exp() {
char *name = "p_exp";
thError("%s: ERROR - function not ready for dispatch", name);
return(SH_GENERIC_ERROR);
}

RET_CODE p_deV() {
char *name = "p_deV";
thError("%s: ERROR - function not ready for dispatch", name);
return(SH_GENERIC_ERROR);
}

RET_CODE p_pl() {
char *name = "p_pl";
thError("%s: ERROR - function not ready for dispatch", name);
return(SH_GENERIC_ERROR);
}

RET_CODE p_sersic() {
char *name = "p_sersic";
thError("%s: ERROR - function not ready for dispatch", name);
return(SH_GENERIC_ERROR);
}

RET_CODE p_coresersic() {
char *name = "p_coresersic";
thError("%s: ERROR - function not ready for dispatch", name);
return(SH_GENERIC_ERROR);
}


THPIX  g_exp(void *q, THPIX cutoff) {

/* using a quadratic fit, I got the following relationship between g and x= EXP_LUMCUT

The following needs adjustment due to the value of NU_EXP != 1

The following is a good approximation for 1.0E-1 > EXP_LUMCUT > 1.0E-4
log10(g) =   2.620459474717e-01
     	  +  -3.422881952480e-01 * log10(x)
          +  -3.574639246528e-02 * log10(x)^2

ln(g)    = 	 0.603383092
	      + -3.422881952480e-01 * ln(x)
	      + -1.5524461E-2 * ln(x) ^ 2

The following function is a good approximation for 1.0E-1 > EXP_LUMCUT > 1.0E-8

log10(g) =   9.192784942128e-02
     	+  -6.247320321302e-01 * log10(x)
     	+  -1.907318830331e-01 * log10(x)^2
     	+  -3.528212257159e-02 * log10(x)^3
     	+  -3.366571125961e-03 * log10(x)^4
     	+  -1.273977848097e-04 * log10(x)^5


The following estimate works for 3.0E-1 > EXP_LUMCUT > 1.0E-15

log10(g) =  -4.196230758468e-01
     +  -1.891615821894e+00 * log10(x)
     +  -1.419509477006e+00 * log10(x)^2
     +  -6.619607105524e-01 * log10(x)^3
     +  -1.928801149941e-01 * log10(x)^4
     +  -3.622037014080e-02 * log10(x)^5
     +  -4.448262729442e-03 * log10(x)^6
     +  -3.546799222649e-04 * log10(x)^7
     +  -1.767385830972e-05 * log10(x)^8
     +  -4.996805620695e-07 * log10(x)^9
     +  -6.115524392876e-09 * log10(x)^10

The following function works for 0.98 > EXP_LUMCUT > 0.1 with an accuracy better than 0.1

rho = -5.601631753e-1 log10(x)^4 - 2.541022794 log10(x) ^ 3 - 4.124364172 log10(x) ^ 2 - 5.732832911 log10(x) + 2.700742457e-1

(for definition of rho refer to g_deV)

*/

/* finding the truncation limit of the profile 
THPIX g = EXP_SIGMA_EXTENSION */
char *name = "g_exp";
double rho, x;
THPIX g;
x = log((double) cutoff) / log(10.0);
/* asserting that we are at the right range for cutoff value */
shAssert(cutoff <= 0.98 && cutoff >= 1.0E-8);
if (cutoff <= 0.1 && cutoff >= 1.0E-8) {
	rho =   9.192784942128e-02
	     +  -6.247320321302e-01 * x
	     +  -1.907318830331e-01 * pow(x, 2)
	     +  -3.528212257159e-02 * pow(x, 3)
	     +  -3.366571125961e-03 * pow(x, 4)
	     +  -1.273977848097e-04 * pow(x, 5);
	rho = exp(rho * log(10.0));
} else if (cutoff > 0.1 && cutoff <= 0.98) {
	rho = 	-5.601631753E-1 *pow(x, 4) 
		- 2.541022794 * pow(x, 3) 
		- 4.124364172 * pow(x, 2)
		- 5.732832911 * x 
		+ 2.700742457E-1;
} else {
	thError("%s: ERROR - unsupported value for (cutoff = %g)", name, (float) cutoff);
	return(THNAN);
}

g = (THPIX) (rho / NU_EXP);
return(g);
}

THPIX g_deV(void *q, THPIX cutoff) {

/* finding the truncation limit of the profile 
THPIX rho;
rho = invgamma(DEV_LUMCUT, 8.0)


the following is a polynomial estimator for invgamma(X, 8) where x = log10(X);

valid with a maximum error of 0.5 for 1.0E-8 < X < 0.1

y = -2.102730124·10-3 x5 - 5.392969934·10-2 x4 - 0.522210323 x3 - 2.427810097 x2 - 8.730759677 x + 5.019794975

valid with a maximum error of 0.2 for 0.1 < X < 0.96

y = -4.027418287 x5 - 21.48679921 x4 - 42.38465446 x3 - 39.1159198 x2 - 22.39261529 x + 3.579886741

*/
char *name = "g_deV";
double x, rho;
x = log((double) cutoff) / log((double) 10.0);
/* asserting that we are at the right range for the cutoff variable */
shAssert(cutoff <= 0.96 && cutoff > 1.0E-8);
if (cutoff <= 0.1 && cutoff >= 1.0E-8) {
rho =  	-2.102730124E-3 * pow(x, 5) 
	- 5.392969934E-2 * pow(x, 4) 
	- 0.522210323 * pow(x, 3) 
	- 2.427810097 * pow(x, 2) 
	- 8.730759677 * x 
	+ 5.019794975;  /* this is a poliynomial extension for invgamma(u, 8) with a maximum error of 0.5 for all  1.0E-8 < u < 0.1 */
} else if (cutoff > 0.1 && cutoff <= 0.96) {
	
rho = 	-4.027418287 * pow(x, 5) 
	- 21.48679921 * pow(x, 4) 
	- 42.38465446 * pow(x, 3) 
	- 39.1159198 * pow(x, 2) 
	- 22.39261529 * x 
	+ 3.579886741;

} else {
	thError("%s: ERROR - unsupported value for (cutoff = %g)", name, (float) cutoff);
	return(THNAN);
}

THPIX g = pow((THPIX) (rho / NU_DEV), 4.0);
return(g);
}

THPIX g_pl(void *q, THPIX cutoff) {

/* finding the truncation limit of the profile 
 *
 * 1. y =  0.0082 x ^ 5 - 0.0330 x ^ 4 - 0.0482 x ^ 3 + 0.4158 x ^ 2 - 1.0614 x + 0.7142
 * 2. y = -0.0025 x ^ 5 - 0.0136 x ^ 4 + 0.0122 x ^ 3 + 0.1790 x ^ 2 - 0.6629 x + 0.3136
 * 3. y = -0.4791 x + 0.152
 * 4. y = +6.0E-5 x ^ 5 + 0.0010 x ^ 4 + 0.0045 x ^ 3 - 0.0032 x ^ 2 - 0.3853 x + 0.0007
 * 5. y = +4.0E-4 x ^ 5 + 0.0040 x ^ 4 + 0.0084 x ^ 3 - 0.0278 x ^ 2 - 0.3454 x - 0.1317
 * 6. y = +3.0E-4 x ^ 5 + 0.0034 x ^ 4 + 0.0067 x ^ 3 - 0.0350 x ^ 2 - 0.3224 x - 0.2137
 * 7. y = +1.0E-4 x ^ 5 + 0.0013 x ^ 4 + 7.0E-4 x ^ 3 - 0.0414 x ^ 2 - 0.3056 x - 0.2711
 *
 * 1. p = 1.75
 * 2. p = 2
 * 3. p = 2.5
 * 4. p = 3
 * 5. p = 4
 * 6. p = 5
 * 7. p = 6
*/
char *name = "g_pl";
double x, rho;
x = log((double) cutoff / ((double) 1.0 - (double) cutoff)) / log((double) 10.0);

if (isinf(x) || isnan(x)) {
	thError("%s: ERROR - logarithmic conversion of (cutoff = %G) failed", name, (double) cutoff);
	return(SH_GENERIC_ERROR);
}

/* asserting that we are at the right range for the cutoff variable */
if (x < -6.0) {
	thError("%s: WARNING - adjusting (cutoff = %G, x = %G) to the domain", name, (double) cutoff, x);
	x = -6.0;
	}
if (x > 2.0) {	
	thError("%s: WARNING - adjusting (cutoff = %G, x = %G) to the domain", name, (double) cutoff, x);
	x = 2.0;
}
rho = -0.479 * x + 0.152;

THPIX g = pow(10.0, (THPIX) rho);
return(g);
}

THPIX g_sersic(void *q, THPIX cutoff) {

/* finding the truncation limit of the profile 
THPIX rho;
rho = invgamma(SERSIC_LUMCUT, 2.0 * n)

*/
char *name = "g_sersic";
double rho;
SERSICPARS *qq = (SERSICPARS *) q;
THPIX n = qq->n;
THPIX k = qq->k;
THPIX two_times_n = 2.0 * n;
rho = (double) qgamma_inv_asym((MATHDBLE) cutoff, (MATHDBLE) two_times_n);
if (isnan(rho) || isinf(rho)) {
	thError("%s: ERROR - divergent root of partial gamma function (n = %g, k = %g, cutoff = %g, rho = %g)", name, (float) n, (float) k, (float) cutoff, (float) rho);
}
if (k <= 0.0 || isinf(k) || isnan(k)) {
	thError("%s: ERROR - divergent (or zero) value of k_n found (n = %g, k = %g, cutoff = %g, rho = %g)", name, (float) n, (float) k, (float) cutoff, (float) rho);
	return(SH_GENERIC_ERROR);
}
THPIX g = pow((THPIX) (rho / k), n);
if (isnan(g) || isinf(g)) {
	thError("%s: ERROR - divergent power calculation (n = %g, k = %g, cutoff = %g, rho = %g, g_sersic = %g)", name, (float) n, (float) k, (float) cutoff, (float) rho, (float) g);
}
/*
printf("%s: (n = %g, N = %g, nN = %g, k = %g, g = %g, cutoff = %g \n", name, n, qq->N, qq->nN, k, g, cutoff);
*/
return(g);
}

THPIX g_coresersic(void *q, THPIX cutoff) {

/* finding the truncation limit of the profile 
THPIX rho;
rho = invgamma(SERSIC_LUMCUT, 2.0 * n)

*/
char *name = "g_coresersic";
double rho;
CORESERSICPARS *qq = (CORESERSICPARS *) q;
THPIX n = qq->n;
THPIX k = qq->k;
THPIX two_times_n = 2.0 * n;
rho = (double) qgamma_inv_asym((MATHDBLE) cutoff, (MATHDBLE) two_times_n);
if (isnan(rho) || isinf(rho)) {
	thError("%s: ERROR - divergent root of partial gamma function (n = %g, k = %g, cutoff = %g, rho = %g)", name, (float) n, (float) k, (float) cutoff, (float) rho);
}
if (k <= 0.0 || isinf(k) || isnan(k)) {
	thError("%s: ERROR - divergent (or zero) value of k_n found (n = %g, k = %g, cutoff = %g, rho = %g)", name, (float) n, (float) k, (float) cutoff, (float) rho);
	return(SH_GENERIC_ERROR);
}
THPIX g = pow((THPIX) (rho / k), n);
if (isnan(g) || isinf(g)) {
	thError("%s: ERROR - divergent power calculation (n = %g, k = %g, cutoff = %g, rho = %g, g_sersic = %g)", name, (float) n, (float) k, (float) cutoff, (float) rho, (float) g);
}
/*
printf("%s: (n = %g, N = %g, nN = %g, k = %g, g = %g, cutoff = %g \n", name, n, qq->N, qq->nN, k, g, cutoff);
*/
return(g);
}

RET_CODE derive_profile_region_chars(void *q, char *qname, void *g_ptr, 
			THPIX cutoff, 
			int *row0, int *col0, int *nrow, int *ncol) {
char *name = "derive_profile_region_chars";
if (q == NULL || qname == NULL || strlen(qname) == 0 || g_ptr == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
TYPE t;
t = shTypeGetFromName(qname);
if (t == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - variable type '%s' not supported by a schema", name, qname);
	return(SH_GENERIC_ERROR);
}

if (row0 == NULL && col0 == NULL && nrow == NULL && ncol == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
THPIX (*g_function)(void *, THPIX);
g_function = g_ptr;
THPIX g;
g = g_function(q, cutoff);
#if DEEP_DEBUG_MGPROFILE
printf("%s: g = %g, lum_cut =  %g\n", name, g, cutoff);
#endif

THPIX a, b, c, det;
a = record_get(q, t, "a");
b = record_get(q, t, "b");
c = record_get(q, t, "c");

if (a != a || b != b || c != c) {
	THPIX re, e, phi;
	re = record_get(q, t, "re");
	e = record_get(q, t, "e");
	phi = record_get(q, t, "phi");	
	thError("%s: ERROR - undefined algerbaic shape parameters (a, b, c, = %g, %g, %g), (re, e, e-1, phi = %g, %g, %g, %g)", name, a, b, c, re, e, e-1.0, phi);
	return(SH_GENERIC_ERROR);
}
det = a * c - pow(b, 2);
if (det <= (THPIX) 0.0) {
	THPIX re, e, phi;
	re = record_get(q, t, "re");
	e = record_get(q, t, "e");
	phi = record_get(q, t, "phi");	
	thError("%s: ERROR - unacceptable discriminant (D = %6.1g), (re, e, e-1, phi = %g, %g, %g, %g)", name, det, re, e, e-1.0, phi);
	return(SH_GENERIC_ERROR);
}
if (isnan(det) || isinf(det)) {
	THPIX re, e, phi;
	re = record_get(q, t, "re");
	e = record_get(q, t, "e");
	phi = record_get(q, t, "phi");
	thError("%s: ERROR - undefined discriminant for (a, b, c = %g, %g, %g), (re, e, e-1, phi = %g, %g, %g, %g)", name, a, b, c, re, e, e-1.0, phi);
	return(SH_GENERIC_ERROR);
} 

double dbrow, dbcol;
#if SET_SQUARE_REGION
dbrow = 1.5 + 2.0 * (g * pow(fabs(det), -0.25));
dbcol = 1.5 + 2.0 * (g * pow(fabs(det), -0.25));
#else 
dbrow = 1.5 + 2.0 * (g * pow(fabs(c / det), 0.5));
dbcol = 1.5 + 2.0 * (g * pow(fabs(a / det), 0.5));
#endif

#if VERY_DEEP_DEBUG_MGPROFILE
printf("%s: ellipse: (a = %12.2e, b = %12.2e, c = %12.2e, det = %12.2e \n", name, a, b, c, det);
printf("%s: before filtering for size (dbrow, dbcol = %12.4e, %12.4e) \n", name, (float) dbrow, (float) dbcol);
#endif

int nrow0, ncol0;	
make_nrow_ncol_from_brow_bcol(det, dbrow, dbcol, &nrow0, &ncol0);

#if DEEP_DEBUG_MGPROFILE 
printf("%s: after filtering for size (nrow, ncol = %d, %d) \n", name, nrow0, ncol0);
#endif

if (nrow != NULL) *nrow = nrow0;
if (ncol != NULL) *ncol = ncol0;
/* changed to fit the common understanding that ROW is Y and COL is X */
THPIX xc, yc;
xc = record_get(q, t, "xc");
yc = record_get(q, t, "yc");
if (row0 != NULL) *row0 = -(int) (nrow0 / 2.0 - xc);
if (col0 != NULL) *col0 = -(int) (ncol0 / 2.0 - yc);

return(SH_SUCCESS);
}



RET_CODE finalize_working_region_chars(int row0_in, int col0_in, int nrow_in, int ncol_in, int margin_in,
				int row0_out, int col0_out, int nrow_out, int ncol_out, int margin_out,
				int *row0_f, int *col0_f, int *nrow_f, int *ncol_f) {
char *name = "finalize_working_region_chars";

int d;
d = margin_in;
if (nrow_in + 2 * d >= MAXNROW) {
	d = (MAXNROW - nrow_in) / 2;
}
nrow_in += 2 * d;
row0_in -= d;

d = margin_in;
if (ncol_in + 2 * d >= MAXNCOL) {
	d = (MAXNROW - ncol_in) / 2;
}
ncol_in += 2 * d;
col0_in -= d;

d = margin_out;
if (nrow_out + 2 * d >= MAXNROW) {
	d = (MAXNROW - nrow_out) / 2;
}
nrow_out += 2 * d;
row0_out -= d;

d = margin_out;
if (ncol_out + 2 * d >= MAXNCOL) {
	d = (MAXNROW - ncol_out) / 2;
}
ncol_out += 2 * d;
col0_out -= d;

#if 1
int row0_w, col0_w, row1_w, col1_w, nrow_w, ncol_w;
row0_w = MIN(row0_in, row0_out);
col0_w = MIN(col0_in, col0_out);
row1_w = MAX((row0_in + nrow_in), (row0_out + nrow_out));
col1_w = MAX((col0_in + ncol_in), (col0_out + ncol_out));
nrow_w = row1_w - row0_w;
ncol_w = col1_w - col0_w;

#if DEEP_DEBUG_MGPROFILE
printf("%s: work pars (row0 = %d, col0 = %d) \n", name, row0_w, col0_w);
printf("%s: work pars (row1 = %d, col1 = %d) \n", name, row1_w, col1_w);
printf("%s: work pars (nrow = %d, ncol = %d) \n", name, nrow_w, ncol_w);
#endif

#else
int row0_w, col0_w, nrow_w, ncol_w;
if (nrow_out <= nrow_in) {
	nrow_w = nrow_in;
	row0_w = row0_in;
} else {
	nrow_w = nrow_out;
	row0_w = row0_out;
}

if (ncol_out <= ncol_in) {
	ncol_w = ncol_in;
	col0_w = col0_in;
} else {
	ncol_w = ncol_out;
	col0_w = col0_out;
}
#endif

if (row0_f != NULL) *row0_f = row0_w;
if (col0_f != NULL) *col0_f = col0_w;
if (nrow_f != NULL) *nrow_f = nrow_w;
if (ncol_f != NULL) *ncol_f = ncol_w;

if (row0_f == NULL && col0_f == NULL && nrow_f == NULL && ncol_f == NULL) {
	thError("%s: WARNING - null output placeholder", name);
}

return(SH_SUCCESS);
}

RET_CODE MGPsfInfoUpdate(void *q, char *qname, void *g_ptr, void *p_ptr, PSF_CONVOLUTION_INFO *psf_info) {
char *name = "MGPsfInfoUpdate";

if (q == NULL || qname == NULL || strlen(qname) == 0 || g_ptr == NULL || psf_info == NULL) {
	thError("%s: ERROR - null input or output", name);
	return(SH_GENERIC_ERROR);
}
TYPE t;
t = shTypeGetFromName(qname);
if (t == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - varibale type '%s' not supported by any schema", name, qname);
	return(SH_GENERIC_ERROR);
}

psf_info->rowc = record_get(q, t, "xc");
psf_info->colc = record_get(q, t, "yc");

RET_CODE status;
THPIX cutoff_in, cutoff_out;
status = q_get_lumcut(q, qname, &cutoff_in, &cutoff_out);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get luminosity cuts from (%s)", name, qname);
	return(status);
}

int row0_in, col0_in, nrow_in, ncol_in, margin_in;
int row0_out, col0_out, nrow_out, ncol_out, margin_out;

/* outer region */
THPIX cutoff_psf_out;
int margin_out_max, margin_out_min, margin_out_default;
margin_out_max = (OUTER_PSF_SIZE + 1) / 2;
margin_out_min = (OUTER_PSF_SIZE + 1) / 2;
margin_out_default = (OUTER_PSF_SIZE + 1) / 2;
cutoff_psf_out = CUTOFF_OUTER_PSF_DN;

status = thPsfConvInfoGetMargin(q, qname, p_ptr, psf_info, cutoff_psf_out, margin_out_min, margin_out_max, margin_out_default, &margin_out);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (margin) for (outer) part of the profile (cutoff = %g)", name, (float) cutoff_psf_out);
	return(status);
}

margin_out = (OUTER_PSF_SIZE + 1) / 2;
status = derive_profile_region_chars(q, qname, g_ptr, cutoff_out, 
		&row0_out, &col0_out, &nrow_out, &ncol_out); 
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not determine the characteristics of the outer region (cutoff = %g)", name, cutoff_out);
	return(status);
}

/* inner region */

THPIX cutoff_psf_in;
int margin_in_max, margin_in_min, margin_in_default;
margin_in_max = (INNER_PSF_SIZE + 1) / 2;
margin_in_min = (INNER_PSF_SIZE_LOW + 1) / 2;
margin_in_default = (INNER_PSF_SIZE + 1) / 2;
cutoff_psf_in = CUTOFF_INNER_PSF_DN;

status = thPsfConvInfoGetMargin(q, qname, p_ptr, psf_info, cutoff_psf_in, margin_in_min, margin_in_max, margin_in_default, &margin_in);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (margin) for (inner) part of the profile (cutoff = %g)", name, (float) cutoff_psf_in);
	return(status);
}

status = derive_profile_region_chars(q, qname, g_ptr, cutoff_in,  
		&row0_in, &col0_in, &nrow_in, &ncol_in);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not determine the characteristics of the inner region (cutoff = %g)", name, cutoff_in);
	return(status);
}

#if DEEP_DEBUG_MGPROFILE
printf("%s: pre-finalized in-mask  (nrow, ncol, row0, col0 = %d, %d, %d, %d) \n", name, nrow_in, ncol_in, row0_in, col0_in);
#endif
status = finalize_inner_region_chars(row0_in, col0_in, nrow_in, ncol_in,
				row0_out, col0_out, nrow_out, ncol_out,
				&row0_in, &col0_in, &nrow_in, &ncol_in);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not decide for the correct value of inner region mask", name);
	return(status);
}

#if DEEP_DEBUG_MGPROFILE || DEBUG_PSF_VARIABLE_SIZE
printf("%s: finalized     in-mask  (nrow, ncol, row0, col0 = %d, %d, %d, %d), margin = %d\n", name, nrow_in, ncol_in, row0_in, col0_in, margin_in);
printf("%s:              out-mask  (nrow, ncol, row0, col0 = %d, %d, %d, %d), margin = %d\n", name, nrow_out, ncol_out, row0_out, col0_out, margin_out);
#endif
status = thPsfInfoPutMask(psf_info, INNER_PSF, row0_in, col0_in, nrow_in, ncol_in, margin_in);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update (psf_info) with the inner region mask", name);
	return(status);
}
status = thPsfInfoPutMask(psf_info, OUTER_PSF, row0_out, col0_out, nrow_out, ncol_out, margin_out);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update (psf_info) with the outer region mask", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE MGPsfExp(void *q, PSF_CONVOLUTION_INFO *psf_info) {
char *name = "MGPsfExp";
char *qname = "EXPPARS";
RET_CODE status;
status = adjust_lumcuts(q, qname, EXP_INNER_LUMCUT, EXP_OUTER_LUMCUT);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not adjust lumcuts", name);
	return(status);
}
status = MGPsfInfoUpdate(q, qname, &g_exp, &p_exp, psf_info);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic (psf_info) updater for (cutoff = %g, %g)", name, (float) EXP_INNER_LUMCUT, (float) EXP_OUTER_LUMCUT);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE MGPsfDeV(void *q, PSF_CONVOLUTION_INFO *psf_info) {
char *name = "MGPsfDeV";
char *qname = "DEVPARS";
RET_CODE status;
status = adjust_lumcuts(q, qname, DEV_INNER_LUMCUT, DEV_OUTER_LUMCUT);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not adjust lumcuts", name);
	return(status);
}
status = MGPsfInfoUpdate(q, qname, &g_deV, &p_deV, psf_info);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic (psf_info) updater for (cutoff = %g, %g)", name, 
			(float) DEV_INNER_LUMCUT, (float) DEV_OUTER_LUMCUT);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE MGPsfPl(void *q, PSF_CONVOLUTION_INFO *psf_info) {
char *name = "MGPsfPl";
char *qname = "POWERLAWPARS";
RET_CODE status;
status = adjust_lumcuts(q, qname, PL_INNER_LUMCUT, PL_OUTER_LUMCUT);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not adjust lumcuts", name);
	return(status);
}
status = MGPsfInfoUpdate(q, qname, &g_pl, &p_pl, psf_info);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic (psf_info) updater for (cutoff = %g, %g)", name, 
			(float) PL_INNER_LUMCUT, (float) PL_OUTER_LUMCUT);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE MGPsfSersic(void *q, PSF_CONVOLUTION_INFO *psf_info) {
char *name = "MGPsfSersic";
char *qname = "SERSICPARS";
RET_CODE status;
status = adjust_lumcuts(q, qname, SERSIC_INNER_LUMCUT, SERSIC_OUTER_LUMCUT);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not adjust lumcuts", name);
	return(status);
}
status = MGPsfInfoUpdate(q, qname, &g_sersic, &p_sersic, psf_info);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic (psf_info) updater for (cutoff = %g, %g)", name, 
			(float) SERSIC_INNER_LUMCUT, (float) SERSIC_OUTER_LUMCUT);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE MGPsfCoresersic(void *q, PSF_CONVOLUTION_INFO *psf_info) {
char *name = "MGPsfCoreSersic";
char *qname = "CORESERSICPARS";
RET_CODE status;
status = adjust_lumcuts(q, qname, CORESERSIC_INNER_LUMCUT, CORESERSIC_OUTER_LUMCUT);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not adjust lumcuts", name);
	return(status);
}
status = MGPsfInfoUpdate(q, qname, &g_coresersic, &p_coresersic, psf_info);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic (psf_info) updater for (cutoff = %g, %g)", name, 
			(float) CORESERSIC_INNER_LUMCUT, (float) CORESERSIC_OUTER_LUMCUT);
	return(status);
}

return(SH_SUCCESS);
}


RET_CODE finalize_inner_region_chars(int row0_in, int col0_in, int nrow_in, int ncol_in,
				int row0_out, int col0_out, int nrow_out, int ncol_out,
				int *row0, int *col0, int *nrow, int *ncol) {
char *name = "finalize_inner_region_chars";
if (row0 == NULL && col0 == NULL && nrow == NULL && ncol == NULL) {
	thError("%s: WARNING - null output placeholder", name);	
	return(SH_GENERIC_ERROR);
}

if ((THPIX) nrow_out / (THPIX) nrow_in <= NROW_OUT_IN_RATIO || 
	nrow_out - nrow_in < NROW_OUT_IN_MARGINE || 
	(THPIX) ncol_out / (THPIX) ncol_in <= NCOL_OUT_IN_RATIO ||
	ncol_out - ncol_in < NCOL_OUT_IN_MARGINE) {
	if (row0 != NULL) *row0 = row0_out;
	if (col0 != NULL) *col0 = col0_out;
	if (nrow != NULL) *nrow = nrow_out;
	if (ncol != NULL) *ncol = ncol_out;
} else {
	if (row0 != NULL) *row0 = row0_in;
	if (col0 != NULL) *col0 = col0_in;
	if (nrow != NULL) *nrow = nrow_in;
	if (ncol != NULL) *ncol = ncol_in;
}

return(SH_SUCCESS);
}

RET_CODE adjust_lumcuts(void *q, char *qname, THPIX system_inner_lumcut, THPIX system_outer_lumcut){
char *name = "adjust_lumcuts";
if (q == NULL || qname == NULL) {
	thError("%s: ERROR - model parameter (q) and its type (qname) should both be passed", name);
	return(SH_GENERIC_ERROR);
}
TYPE qtype;
qtype = shTypeGetFromName(qname);
int change = 0;
THPIX inner_lumcut, outer_lumcut, *ptr_inner_lumcut, *ptr_outer_lumcut;
SCHEMA_ELEM *se1, *se2;
se1 = shSchemaElemGetFromType(qtype, "inner_lumcut");
se2 = shSchemaElemGetFromType(qtype, "outer_lumcut");
if (se1 == NULL) {
	thError("%s: ERROR - could not find (inner_lumcut) in (%s)", name, qname);
	return(SH_GENERIC_ERROR);
}
if (se2 == NULL) {
	thError("%s: ERROR - could not find (outer_lumcut) in (%s)", name, qname);
	return(SH_GENERIC_ERROR);
}
ptr_inner_lumcut = (THPIX *) shElemGet(q, se1, NULL);
ptr_outer_lumcut = (THPIX *) shElemGet(q, se2, NULL);
if (ptr_inner_lumcut == NULL) {
	thError("%s: ERROR - could not get (inner_lumcut) in (%s)", name, qname);
	return(SH_GENERIC_ERROR);
}
if (ptr_outer_lumcut == NULL) {
	thError("%s: ERROR - could not get (outer_lumcut) in (%s)", name, qname);
	return(SH_GENERIC_ERROR);
}
inner_lumcut = *ptr_inner_lumcut;
outer_lumcut = *ptr_outer_lumcut;

if (inner_lumcut != inner_lumcut) {
	inner_lumcut = system_inner_lumcut;
	change++;
}
if (outer_lumcut != outer_lumcut) {
	outer_lumcut = system_outer_lumcut;
	change++;
}
if (inner_lumcut <= (THPIX) 0.0 || inner_lumcut >= (THPIX) 1.0) {
	thError("%s: WARNING - correcting (inner_lumcut) in (%s) to (%g) from system", name, qname, (float) system_inner_lumcut);
	inner_lumcut = system_inner_lumcut;
	change++;
}
if (outer_lumcut <= (THPIX) 0.0 || outer_lumcut >= (THPIX) 1.0) {
	thError("%s: WARNING - correcting (outer_lumcut) in (%s) to (%g) from system", name, qname, (float) system_outer_lumcut);
	outer_lumcut = system_outer_lumcut;
	change++;
}
if (inner_lumcut > outer_lumcut) {
	thError("%s: WARNING - found (inner_lumcut > outer_lumcut) in (%s)", name, qname);
	outer_lumcut = inner_lumcut;
	change++;
}
if (change > 0) {
	ptr_inner_lumcut = &inner_lumcut;
	ptr_outer_lumcut = &outer_lumcut;
	shElemSet(q, se1, (void *) ptr_inner_lumcut);
	shElemSet(q, se2, (void *) ptr_outer_lumcut);
}

return(SH_SUCCESS);
}

RET_CODE q_get_lumcut(void *q, char *qname, THPIX *inner_lumcut, THPIX *outer_lumcut) {
char *name = "q_get_lumcut";
if (q == NULL || qname == NULL) {
	thError("%s: ERROR - variable and its name should be passed");
	return(SH_GENERIC_ERROR);
}
if (outer_lumcut == NULL && inner_lumcut == NULL) {
	thError("%s: WARNING - no ouput placeholder was passed", name);
	return(SH_SUCCESS);
}
TYPE qtype = shTypeGetFromName(qname);
if (qtype == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - could not find (%s) in schema library", name, qname);
	return(SH_GENERIC_ERROR);
}

SCHEMA_ELEM *se1, *se2;
se1 = shSchemaElemGetFromType(qtype, "inner_lumcut");
se2 = shSchemaElemGetFromType(qtype, "outer_lumcut");
if (se1 == NULL) {
	thError("%s: ERROR - could not find (inner_lumcut) in (%s)", name, qname);
	return(SH_GENERIC_ERROR);
}
if (se2 == NULL) {
	thError("%s: ERROR - could not find (outer_lumcut) in (%s)", name, qname);
	return(SH_GENERIC_ERROR);
}
THPIX *ptr_inner_lumcut, *ptr_outer_lumcut;
ptr_inner_lumcut = (THPIX *) shElemGet(q, se1, NULL);
ptr_outer_lumcut = (THPIX *) shElemGet(q, se2, NULL);
if (ptr_inner_lumcut == NULL) {
	thError("%s: ERROR - could not get (inner_lumcut) in (%s)", name, qname);
	return(SH_GENERIC_ERROR);
}
if (ptr_outer_lumcut == NULL) {
	thError("%s: ERROR - could not get (outer_lumcut) in (%s)", name, qname);
	return(SH_GENERIC_ERROR);
}
if (inner_lumcut != NULL) *inner_lumcut = *ptr_inner_lumcut;
if (outer_lumcut != NULL) *outer_lumcut = *ptr_outer_lumcut;

return(SH_SUCCESS);
}


/* the following is adapted from utils.c */
/*****************************************************************************/
/*
 * <AUTO EXTRACT>
 *
 * Return a shifted copy of a THPIX region
 *
 * If out is non-NULL, it must be the same size as in, and will
 * be the return value of the function. If it's NULL, a new region will
 * be allocated and returned.
 *
 * If the scratch region, scr, is NULL one will be allocated and freed for
 * you.
 */
#define MAXFILTSIZE (2*15 + 1)		/* max size of smoothing filters */

REGION *
thRegIntShift(REGION *out,		/* output region; may be NULL */
	      const REGION *in,		/* input region */
	      REGION *scr,		/* scratch space; may be NULL */
	      int filtsize,		/* size of sinc filter (must be odd) */
	      float dr,			/* shift by this far in row... */
	      float dc)			/*      and this far in column */
{
   float filtr[MAXFILTSIZE], filtc[MAXFILTSIZE]; /* sinc filters */
   int own_scr;				/* do we own the scr region? */
   int i;
   int idc, idr;			/* integral part of shift */
   int nrow, ncol;			/* == in->n{row,col} */
   THPIX **rows;			/* == out->rows_thpix */
   
   shAssert(in != NULL && in->type == TYPE_THPIX);

   if(filtsize%2 == 0) filtsize++;
   if(filtsize > MAXFILTSIZE) {
      shError("thRegIntShift: filter too large (max %d)", MAXFILTSIZE);
      filtsize = MAXFILTSIZE;
   }
   
   nrow = in->nrow; ncol = in->ncol;
/*
 * reduce desired shifts to the range [-0.5, 0.5]
 */
   idr = reduce_shift(dr, &dr);
   idc = reduce_shift(dc, &dc);
/*
 * Did we shift clean off the region?
 */
   if(idr <= -nrow || idr >= nrow || idc <= -ncol || idc >= ncol) {
      shErrStackPush("thRegIntShift: Attempting to shift by (%d,%d): "
		     " greater than region size (%dx%d)",
		     idr, idc, nrow, ncol);

      return(NULL);
   }
/*
 * Check/create regions
 */
   if(out == NULL) {
      out = shRegNew(in->name, nrow, ncol, in->type);
   }
   shAssert(out->type == in->type);
   shAssert(out->nrow == nrow && out->ncol == ncol);

   /* rows = out->ROWS; */
   rows = out->rows_thpix;

   own_scr = (scr == NULL) ? 1 : 0;
   if(own_scr) {
      scr = shRegNew("shift scratch", nrow, ncol, in->type);
   }
   shAssert(scr->type == in->type);
   shAssert(scr->nrow == nrow && scr->ncol == ncol);
/*
 * prepare sinc filters, then do the fractional shift. Note the convention
 * in naming filters passed to phConvolve(), the first extends across columns
 */
   get_sync_with_cosbell(filtr, (filtsize + 1)/2, -dr);
   get_sync_with_cosbell(filtc, (filtsize + 1)/2, -dc);

   phConvolve(out, in, scr, filtsize, filtsize, filtc, filtr, 0,CONVOLVE_MULT);
/*
 * do the non-fractional shifts. Rows first
 */
   if(idr < 0) {
      idr = -idr;
      for(i = idr;i < nrow - idr;i++) {
	 memcpy(rows[i], rows[i + idr], ncol*sizeof(THPIX));
      }
   } else if(idr == 0) {
      ;				/* nothing to do */
   } else {
      for(i = nrow - 1; i >= idr;i--) {
	 memcpy(rows[i], rows[i - idr], ncol*sizeof(THPIX));
      }
   }
/*
 * and now columns
 */
   if(idc < 0) {
      idc = -idc;
      for(i = 0; i < nrow; i++) {
	 memmove(&rows[i][0], &rows[i][idc], (ncol - idc)*sizeof(THPIX));
      }
   } else if(idc == 0) {
      ;				/* nothing to do */
   } else {
      for(i = 0; i < nrow; i++) {
	 memmove(&rows[i][idc], &rows[i][0], (ncol - idc)*sizeof(THPIX));
      }
   }
/*
 * cleanup
 */
   if(own_scr) {
      shRegDel(scr);
   }
   
   return(out);
}

RET_CODE coresersic_mcount_calculator(void *q, THPIX *mcount, int silent) {
char *name = "coresersic_mcount_calculator";
shAssert(q != NULL);
shAssert(mcount != NULL);

/* source:
 * https://keisan.casio.com/exec/system/1281279441
 * Nodes and Weights of Gauss-Laguerre Calculator
 */

QUADFL x[100] = {
0.014386146995419669464436032421084281688512050966,
0.075803612023357124642993170677619621890564537719,
0.186314102057187173711460039042000603546202143312,
0.345969180991429090805378304989176665916272749467,
0.554810937580915509598340862968475972057253331879,
0.812891284115668845038081302089693108916678219316,
1.120273835007540148567503208138516870987060387277,
1.4770343299238270697185615900119702038820488303,
1.883260826342394705801824648360138069613386116537,
2.339053849646034171844423329865789475081345368214,
2.844526542755359066517119246955045643779362386548,
3.399804827445711944287159094946990679966759356644,
4.005027581758652017464900403271417436365020069786,
4.6603468355689084595040280266228479367781522394339,
5.3659279855851170148835307002113124177735600130764,
6.121950030804019789061373794621522567393863248656,
6.928605829376173055020578689898725880965610515716,
7.786102377862517434336654189095964321122329875985,
8.69466111392216798920445606412688315055307201065,
9.654518243555080727805574157972236574607597645112,
10.665925094121675550534495350158743760803952603248,
11.729148494472225071619721912253497544647053283665,
12.844471183641030571565638150584109591717836412828,
14.012192249694274648964132308316289629464199959767,
15.23262760046669784302041550956226051576480325499,
16.506110468081989594185016055271767580375606202441,
17.832991949326387429842546114269676532212047749239,
19.213641584136066684901496749658987824327097324692,
20.648447974668349519654447808348566296686549680439,
22.137819447656704406552716077390543723572214396682,
23.682184763002376100171887646238319774521954316415,
25.281993871834041538775185811767894692106599605974,
26.937718727574264943554420821563356298284635311442,
28.64985415389129217109679221074678696489104285425,
30.418918773790942919595713617048104452697898639485,
32.245456004520665842270490683432751487625800550655,
34.130035123421516471287278120181406582083089466876,
36.07325241037997322620358475807515539192546120546,
38.07573237310709319104533518730924429731326579851,
40.138129062115545763233678737698828135667518753993,
42.261127482984794178411494933871743845349357073648,
44.445445114311806077478838491178111032087024891251,
46.691833540651539419116010770581151491879821535361,
49.001080210772436956028200619659191741327940749713,
51.37401033270399452135984751304564700967634136002,
53.811488918355660438291791945942420574999135872589,
56.314422991961708237686384235697436356533205469116,
58.883763978282090293706081105456788791980697773486,
61.520510288396142646799262555210088301307405134792,
64.225710123101560166967699113534070019919296344492,
67.000464516419311596764254278099751432705630830302,
69.845930644558374286446832951544745923856156790577,
72.763325428974586256338515260247046548202678094157,
75.753929465939936192709187121887367163951952654792,
78.81909131941147219953449711916410052905582603643,
81.960232219060124004406034433922871748963999777354,
85.17885121121890019965819425201152285085863371472,
88.47653081739461122166173313351549931817562325053,
91.85494326304930893124588349974424058207792737754,
95.31585734883172060708188049803719081708348511971,
98.861146047613527716885852139755277900476209073942,
102.49279492391656096335151490724171601197400288973,
106.21291148804681620553193500890842029564387560101,
110.02373561603091696044095567254651297506815777452,
113.92765118897162371111095615952688206853431374889,
117.92719913257322683566930153317598472314768078915,
122.0250920704416210770071192771802129977882374398,
126.22423084475038757481282514568521751638696104949,
130.52772320679941326555235618477155948740904346682,
134.93890504022740757717591916185365596931353150412,
139.46136455424015676483735784559273942265772179158,
144.0989699772127241580660709688897260659410534834,
148.85590139775824946113745249503385376533217593335,
153.73668754797303061104508345523147446924144789121,
158.74624851171310443390313484523534639226550232703,
163.88994558258723283156168509187096508290147957381,
169.17363981000302458051369125544383162038737281778,
174.60376118237662671452586450128250401592779770557,
180.18739094024569619468544567349296547541064513326,
185.93236023966697143161396472939190679919308665195,
191.84736937224832912467163514693758170098662143982,
197.94213310214325740605535167367820592237967945187,
204.22755956703050772080095334034491093971825741861,
210.71597286157694337066830974235388351439277499162,
217.42139327200148109611632514213232315501173179011,
224.35989478887460814536094305461044256748650339089,
231.550068025172484218832989049761250273162951332,
239.01362975131492446798893533792714879336168509609,
246.77624096724849045732253704645704530192569180271,
254.86862925704743027863842226837592454026841223628,
263.32816846915789310981244461061198767500418420254,
272.20117002409253682587551568880510333756002327315,
281.54632828389738879914802558930339720242714802028,
291.44013361637710726069724339449220378847267906566,
301.98585525163915366574517831621556837970610886064,
313.3295340040755243411836472127284809659746476805,
325.69126343702652000203534998873955161221810012847,
339.43510192344961653520496673232150915962334632322,
355.26131188853413247248270949708161897245257424477,
374.98411283434267870488403679649642050330116428623,
};
QUADFL w[100] = {
0.036392605883401356536582688652527281119113033552,
0.079676746212951398550297980175896805550340699323,
0.112115103342486944677820649354682741601226889249,
0.13035661297514618373772623535315878275986202453,
0.1340433397284623803995809996251625218909488932081,
0.125407090780663749967215024644281997071664303986,
0.108314112097260273553807010871384657371015248142,
0.087096638469959342034556667878760487784180518263,
0.0655510093123106143254924124226033774482397864853,
0.0463401335826442598743797831913033075124833971285,
0.0308463086276814456005144805637621254777161829491,
0.0193678281139787891103655436083071443453655927712,
0.0114854423601796916174017459536311883644243358432,
0.00643895100161042959051935142152561160765710441675,
0.00341497998969266259302227416611471017528500372381,
0.001714319740182208161909509870975297498589886209596,
8.1487159158783785328519095480230655513404379356E-4,
3.668548365994881507800688677137120388751883250338E-4,
1.56452074178106794718778123390308579080030530211E-4,
6.32108705288858076314343849789188882859881785535E-5,
2.419575226518929264427390417804516803513287272641E-5,
8.7743097637554872419172023169705136135739522551E-6,
3.01426748600094751874849862088636457233941687163E-6,
9.808335899345259767943230663476616757988265422807E-7,
3.022638743532254307981374378055013763695124537091E-7,
8.82005839529593861151560442517999225251665820163E-8,
2.436425856200673367434234553590401342366863768163E-8,
6.36971137390175702478571054606423744916749761943E-9,
1.575600320459668008004909551244819747286433003155E-9,
3.686329201346132668941019702874129021451690942534E-10,
8.15479892422461121192323202732336002405654615971E-11,
1.70506255682606540772815203850214730913571203354E-11,
3.368214170866725937785279046501309631122418066342E-12,
6.2835249553666573660283239383180375177680528767E-13,
1.106498015983306170674875796922964580328928541074E-13,
1.838350175454901383224783566281727650947124944829E-14,
2.880115057230585733874324239063165933766273547694E-15,
4.25261289733724140667331199961469941208475374776E-16,
5.91443246725224398376998088717593620285253182498E-17,
7.74308910250010893316637214278016083954018195699E-18,
9.53624944907479497426969322474983127737964459041E-19,
1.104094516986917145368922511287764632102915862643E-19,
1.200846970430497056918691203380694147426700780296E-20,
1.226007007490477403310383281349518328473558548389E-21,
1.1740217146467280968704426805502307001354827423604E-22,
1.05359406460873114069310308656802233093596012512E-23,
8.853231768474297509712764659858329078855775504419E-25,
6.9591987754834461443604211038526831591492867099452E-26,
5.11236981546583252408664221442917179811883483031E-27,
3.50627214881713874207856069526628461230851722362E-28,
2.24264627269392233958838316235683614146731672061E-29,
1.33620942344574962652804195969943810307672745214E-30,
7.40742895757575108926555935172583569236207979197E-32,
3.81586013713634469006194108706544286879293233177E-33,
1.82420019768285430389610326125712405237375360699E-34,
8.081632336064180577822393018962219905896955914E-36,
3.31306380186238467367921680762204151850652610727E-37,
1.2548329976876779940207099431868612775266563593E-38,
4.38379098016171658813718436375786200112433227904E-40,
1.41013922921734748500686879183207865260911065904E-41,
4.1688639996685860322335367357766752607692805674E-43,
1.13048190447714117256790039779181712291087950717E-44,
2.80603749678712394658486758799754883504463344485E-46,
6.3612705709821942695337863556955930929073953082E-48,
1.313981195978128907645410270101278171504897607102E-49,
2.46681069217039399917219141239664848636316423545E-51,
4.1977470228717237503549447883644804960332503578E-53,
6.45626910491200315334586305464160125223982623675E-55,
8.9473372486217303202807061616864155398675318972E-57,
1.11356697469976891840592731594274108094845269636E-58,
1.2402350422552966638800247206565346982286274274E-60,
1.23137657260836981505327640021163804472592328495E-62,
1.085367448905130903441673519712502805053078195643E-64,
8.4549564909552701837593458528340538484752738976E-67,
5.7926307566097949605715544589602950798356375536E-69,
3.4718547763506601611883382747346535444686342392E-71,
1.80985953356223935811577557330062874636838423683E-73,
8.15375260841082970347608650891209388975969478701E-76,
3.15247254879328804295935498448947030479425970932E-78,
1.03790058991526341831386870412959909092477584821E-80,
2.8848755233073495342561352512094368247369884903E-83,
6.70480122596859121471405942645685335060066048634E-86,
1.28896374643609165883972213698390778328935155243E-88,
2.02484834515290858240261046369078870406893637517E-91,
2.563392219966073272089616636460866840162736481357E-94,
2.5739615075159617883339090406763774502640998171E-97,
2.01265478748007766465945441250588994337180381773E-100,
1.19948441938232764373310215652481968717881191712E-103,
5.31213273153980343243328242012333410250356808655E-107,
1.695969259446745079458885406862115879157010972174E-110,
3.76209729863445115412921481269200517222252610744E-114,
5.53964175444960937376629578483318116166667995363E-118,
5.11064047709176055051054221326827185908294936545E-122,
2.73996546940034107527279319914529599922502214886E-126,
7.71361149263820042285378543489222173039763879901E-131,
9.882494600958826046252757696538767028233500669E-136,
4.6468630072942033151956807051337614289423890025E-141,
5.62603729501985300671527287500774071932725280212E-147,
8.9050314058891380744027560296217370118339857303E-154,
3.24656516343580907517363960444250060616629048679E-162,
};

CORESERSICPARS *qq = (CORESERSICPARS *) q;
QUADFL n = (QUADFL) (qq->n);
QUADFL k = (QUADFL) (qq->k);
QUADFL gamma = (QUADFL) (qq->gamma);
QUADFL alpha = (QUADFL) (qq->delta);
QUADFL rb = (QUADFL) (qq->rb);
QUADFL re = (QUADFL) (qq->re);

QUADFL a = n * (gamma + alpha) - (QUADFL) 1.0;
QUADFL b = n * alpha;
QUADFL x0 = k * pow((rb / re), ((QUADFL) 1.0) / n);
QUADFL x0b = pow(x0, b);
if (isinf(x0) || isnan(x0) || isinf(x0b) || isnan(x0b)) {
	QUADFL ln_x0 = log(k) + (log(rb) - log(re)) / (QUADFL) n;
	x0 = exp(x0);
	QUADFL ln_x0b = b * ln_x0;
	x0b = exp(ln_x0b);
	if (isinf(x0) || isnan(x0)) {
		thError("%s: ERROR - found (x0 = %G, inf) while (n = %g, k = %g, gamma = %g, alpha = %g, rb = %g, re = %g)", name, 
		(double) x0, (float) n, (float) k, (float) gamma, (float) alpha, (float) rb, (float) re);
		return(SH_GENERIC_ERROR);
	}
}

QUADFL c = ((QUADFL) 2.0 - gamma - alpha) / alpha;

QUADFL y_item, y, y2, y_error;
y_item = y = y2 = y_error = (QUADFL) 0.0;

int i;
for (i = 99; i >= 0; i--) {
	QUADFL wf_i = coresersic_basic_function_w(x[i], w[i], x0, x0b, a, b, c);
	if (isnan(wf_i) || isinf(wf_i)) {
		if (!silent) thError("%s: ERROR - found (nan) in return from function (x[%d] = %G, x0 = %G, x0b = %G, a = %G, b = %G, c = %G)", 
			name, i, (double) x[i], (double) x0, (double) x0b, (double) a, (double) b, (double) c);
		*mcount = THNAN;
		return(SH_GENERIC_ERROR);
	}
	y_item = wf_i - y_error;
	y2 = y + y_item;
	y_error = (QUADFL) (y2 - y) - (QUADFL) y_item;
	y = y2;
	if (isnan(y) || isinf(y)) {
		if (!silent) thError("%s: ERROR - found (nan) in integral at i = %d", name, i);
		*mcount = THNAN;
		return(SH_GENERIC_ERROR);	
	}

}

QUADFL mcount_qfl = (QUADFL) 2.0 * (QUADFL) PI * n * pow((re / pow(k, n)), (QUADFL) 2.0) * y;
if (isinf(mcount_qfl) || isnan(mcount_qfl)) {
	if (!silent) thError("%s: ERROR - found (nan) in (mcount_qfl)", name);
	*mcount = THNAN;
	return(SH_GENERIC_ERROR);
} else if (isinf((THPIX) mcount_qfl) || isnan((THPIX) mcount_qfl)) {
	if (!silent) thError("%s: ERROR - too big a value for (mcount_qfl = %G), n = %g, re = %g, rb = %g, gamma = %g, alpha = %g, k(n) = %g, y = %G", name, (double) mcount_qfl, (float) n, (float) re, (float) rb, (float) gamma, (float) alpha, (float) k, (double) y);
	if (!silent) thError("%s: ERROR - x0 = %g, x0 ^ b = %g, a = %g, b = %g, c = %g", name, (float) x0, (float) x0b, (float) a, (float) b, (float) c);
	*mcount = THNAN;
	return(SH_GENERIC_ERROR);
}
*mcount = (THPIX) mcount_qfl;
return(SH_SUCCESS);
}
 
QUADFL coresersic_basic_function_w(QUADFL x, QUADFL w, QUADFL x0, QUADFL x0b, QUADFL a, QUADFL b, QUADFL c) {

	#if 1
	if (isinf(x0) || isnan(x0)) return((QUADFL) 1.0 / (QUADFL) 0.0);

	if (fabs(x / x0) > (QUADFL) CORESERSIC_APPROXIMATE_XRATIO) {
		QUADFL exponent;
		QUADFL xpx0b = pow(x + x0, b);
		if (isinf(x0b) || isnan(x0b) || isinf(xpx0b) || isnan(xpx0b)) {
			exponent = -(x0) + a * log(x + x0) + c * b * log(x + x0) + log(w);
		} else {
			exponent = -(x0) + a * log(x + x0) + c * log(pow((x + x0), b) - x0b) + log(w);
		}
		return((QUADFL) exp(exponent));
	} else {
		QUADFL exponent = -(x0) + a * log(x + x0) + c * (log(x * b) + (b - (QUADFL) 1.0) * log(x0)) + log(w);
		return((QUADFL) exp(exponent));
	}
	#else
	return((QUADFL) (exp(-(x0)) * pow((x + x0), a) * pow(((pow((x + x0), b)) - x0b), c)));
	#endif
}



