#include "thClass.h"


static int strmatch(char *str1, char *str2);
static int match_ids(char **con1, char **con2);
static int match_class(PHPROPS *props, CHAIN *class, CHAIN *tree);
static int match_def(PHPROPS *props, OBJCDEF *def, CHAIN *tree);
static int match_sentence(PHPROPS *props, SENTENCE *sen);


static THOPERATOR get_operator_sen (SENTENCE *sen);
static int strmatch_nocase(char *str1, char *str2);
static void upper_case(char *str);

static RET_CODE cpy_class_id(CHAIN *class, char *id);
static THPIX dot_props(OBJC_IO *objc1, OBJC_IO *objc2);
static THPIX dot_prop_sen(OBJC_IO *objc1, SENTENCE *objc2);

RET_CODE thAddObjcDefTree(CHAIN *objcdefs, CHAIN *sens, CHAIN *tree) {

  char *name = "thAddObjcDefTree";
  
  if ((objcdefs == NULL) || (sens == NULL) || (tree == NULL)) {
    thError("%s: null input passed", name);
    return(SH_GENERIC_ERROR);
  }

  int i, ndef;
  ndef = shChainSize(objcdefs);

  CHAIN *class;
  int pos;
  RET_CODE status;

  OBJCDEF *def;
  for (i = 0; i < ndef; i++) {
    def = (OBJCDEF *) shChainElementGetByPos(objcdefs, i);
    /* 
       the last flag in the following command tells the machine to create a 
       class / subclass if the a node doesn't already exist in the tree 
    */
    class = thObjcClassGetFromTree(def, tree, 1); 
    pos = thObjcDefGetPosInClass(def, class);
    if (pos < 0) {
      /* the following inserts the pointers to 
	 the right sentences in the def */
      status = thSenInsertInObjcDef(def, sens);
      if (status != SH_SUCCESS) {
	thError("%s: problem assigning sentences to definition", name);
	return(status);
      }
      shChainElementAddByPos(class, def, "OBJCDEF", TAIL, AFTER);
    }
  }

  return(SH_SUCCESS);
}

RET_CODE thAddMaskDefTree(CHAIN *maskdefs, CHAIN *sens, CHAIN *tree) {

  char *name = "thAddMaskDefTree";
  
  if ((maskdefs == NULL) || (sens == NULL) || (tree == NULL)) {
    thError("%s: null input passed", name);
    return(SH_GENERIC_ERROR);
  }

  int i, ndef;
  ndef = shChainSize(maskdefs);

  RET_CODE status;

  int error = 0;
  MASKDEF *def;
  for (i = 0; i < ndef; i++) {
    def = (MASKDEF *) shChainElementGetByPos(maskdefs, i);
    /* 
       the last flag in the following command tells the machine to create a 
       class / subclass if the a node doesn't already exist in the tree 
    */
    status = thMaskDefInsertInTree(def, tree, 1);
    if (status != SH_SUCCESS) {
      thError("%s: could not insert (maskdef) in tree", 
		     name);
      error++;
    } else {
      status = thSenInsertInMaskDef(def, sens);
      if (status != SH_SUCCESS) {
	thError("%s: problem assigning sentences to definition", name);
	error++;
      }
    }    
  }

  if (error > 0) {
    return(SH_GENERIC_ERROR);
  }

  return(SH_SUCCESS);
}

RET_CODE thMaskDefInsertInTree(MASKDEF *def, CHAIN *tree, int addflag){
  
  char *name = "thMaskDefInsertInTree";

  if ((def == NULL) || (tree == NULL)) {
    thError("%s: null tree or null definition name passed", name);
    return(SH_GENERIC_ERROR);
  }
  
  int i, j, k, foundj, ntree;
  ntree = shChainSize(tree);
  
  MASKDEF *def2;
  
  for (i = 0; i < ntree; i++) {
    def2 = (MASKDEF *) shChainElementGetByPos(tree, i);
    for (j = 0; j < MX_STRING_ARR; j++) {
      if (def->objctype[j] == NULL || 
	  def->objctype[j][0] == '\0') break;
      foundj = 0;
      for (k = 0; k < MX_STRING_ARR; k++) {
	if (def2->objctype[k] == NULL || 
	    def2->objctype[k][0] == '\0') break;
	if (strmatch(def2->objctype[k], def->objctype[j])) {
	  foundj = 1;
	  break;
	}
      }
      if (foundj == 0) break;
    }
    if (foundj == 1 && 
	strmatch(def->arithmetic, def2->arithmetic)) {
      return(SH_SUCCESS);
    }
  }
  
  if (addflag) {
    shChainElementAddByPos(tree, def, "MASKDEF", TAIL, AFTER);
    return(SH_SUCCESS);
  }

  return(SH_SUCCESS);
}

RET_CODE thSenInsertInMaskDef(MASKDEF *def, CHAIN *sens) {

  char *name = "thSenInsertInMaskDef";
  
  char *id;
  int pos;
    
  id = def->arithmetic;
  pos = thChainSenGetPosById(sens, id);
  
  if (pos < 0) {
    thError("%s: could not locate senetence (%s) in the chain",
		   name, id);
    return(SH_GENERIC_ERROR);
  }
  
  def->sen = shChainElementGetByPos(sens, pos);
 
  return(SH_SUCCESS);
}


CHAIN *thObjcClassGetFromTree(OBJCDEF *def, CHAIN *tree, int addflag) {
  
  char *name = "thObjcClassGetFromTree";

  if ((def == NULL) || (tree == NULL)) {
    thError("%s: null tree or null definition name passed", name);
    return(NULL);
  }

  int i, ntree;
  ntree = shChainSize(tree);

  CHAIN *class;
  OBJCDEF *def2;

  for (i = 0; i < ntree; i++) {
    class = shChainElementGetByPos(tree, i);
    if (shChainSize(class) > 0) {
      def2 = (OBJCDEF *) shChainElementGetByPos(class, 0);
      if (strmatch(def2->name, def->name)) {
	return(class);
      }
    }
  }
  
  if (addflag) {
    class = shChainNew("OBJCDEF");
    shChainElementAddByPos(class, def, "OBJCDEF", TAIL, AFTER);
    shChainElementAddByPos(tree, class, "CHAIN", TAIL, AFTER);
    return(class);
  }

  return(NULL);
  
}

int thObjcClassGetPosFromTree(OBJCDEF *def, CHAIN *tree, int addflag) {
  
  char *name = "thObjcClassGetFromTree";

  if ((def == NULL) || (tree == NULL)) {
    thError("%s: null tree or null definition passed", name);
    return(-1);
  }

  int i, ntree;
  ntree = shChainSize(tree);

  CHAIN *class;
  OBJCDEF *def2;

  for (i = 0; i < ntree; i++) {
    class = shChainElementGetByPos(tree, i);
    if (shChainSize(class) > 0) {
      def2 = (OBJCDEF *) shChainElementGetByPos(class, 0);
      if (strmatch(def2->name, def->name)) {
	return(i);
      }
    }
  }
  
  if (addflag) {
    class = shChainNew("OBJCDEF");
    shChainElementAddByPos(class, def, "OBJCDEF", TAIL, AFTER);
    shChainElementAddByPos(tree, class, "CHAIN", TAIL, AFTER);
    return(ntree);
  }

  return(-1);
  
}

int thObjcDefGetPosInClass(OBJCDEF *def, CHAIN *class) {
  
  char *name = "thObjcDefGetPosInClass";

  int i, ndef, match;
  OBJCDEF *def2;

  for (i = 0; i < ndef; i++) {
    def2 = (OBJCDEF *) shChainElementGetByPos(class, i);
    match = match_ids(def2->conditions, def->conditions);
    if (match) {
      return(i);
    }
  }

  return(-1);
}

RET_CODE thSenInsertInObjcDef(OBJCDEF *def, CHAIN *sens) {
  
  char *name = "thSenInsertInObjcDef";
  
  char *id;
  int i, pos;
  
  i = 0;
  while ((i < MX_STRING_ARR) && (def->conditions[i][0] != '\0')) {
    
    id = def->conditions[i];
    pos = thChainSenGetPosById(sens, id);
    
    if (pos < 0) {
      thError("%s: could not locate senetence (%s) in the chain",
		     name, id);
      return(SH_GENERIC_ERROR);
    }
    
    def->sentences[i] = shChainElementGetByPos(sens, pos);
    i++;
  }

  return(SH_SUCCESS);
}

static int match_ids(char **con1, char **con2) {
  
  char *name = "match_ids";
  
  int i, j;
  int match1, match = 1;

  i = 0;
  while ((i < MX_STRING_ARR) && match) {
    match1 = 0;
    j = 0;
    while ((j < MX_STRING_ARR) && (~match1)) {
      match1 = strmatch(con1[i], con2[j]);
      j++;
    }
    i++;
    match = match1;
  }
  
  if (~match) {
    return(match);
  }
  
  i = 0;
  while ((i < MX_STRING_ARR) && match) {
    match1 = 0;
    j = 0;
    while ((j < MX_STRING_ARR) && (~match1)) {
      match1 = strmatch(con1[j], con2[i]);
      j++;
    }
    match = match1;
    i++;
  }
  
  return(match);
}

int thChainSenGetPosById(CHAIN *sens, char *id) {

  char *name = "thChainSenGetPosById";

  if ((id == NULL) || (sens == NULL)) {
    thError("%s: null senetence (id) or sentence chain", 
		   name);
    return(-1);
  }
  
  int i, nsens;
  nsens = shChainSize(sens);

  if ((id[0] == '\0') || (nsens == 0)) {
    thError("%s: empty (id) or empty sentence chain",
		   name);
    return(-1);
  }

  SENTENCE *sentence;
  for  (i = 0; i < nsens; i++) {
    sentence = (SENTENCE *) shChainElementGetByPos(sens, i);
    if (strmatch(id, sentence->thId)) {
      return(i);
    }
  }

  return(-1);
}
    
static int strmatch(char *str1, char *str2) {
  
  int l1, l2;
  l1 = strlen(str1);
  l2 = strlen(str2);

  if (l1 != l2) {
    return(0);
  }

  return(memcmp(str1, str2, l1));
}

RET_CODE thClassifyObjc(SINGLEOBJC *objc, CHAIN *tree) {

  char *name = "thClassifyObjc";
  
  if ((objc == NULL) || (tree == NULL)) {
    thError("%s: null input passed", name);
    return(SH_GENERIC_ERROR);
  }

  if (objc->thType == NULL) {
    thError("%s: object does not have specific type", name);
    return(SH_GENERIC_ERROR);
  }

  /* checking if the object is already classified */
  
  if (objc->thType[0][0] != '\0') {
    thError("%s: objc seem already classified, returning", name);
    return(SH_GENERIC_ERROR);
  }

  int ntree;
  ntree = shChainSize(tree);
  if (ntree == 0) {
    thError("%s: empty definition tree", name);
    return(SH_GENERIC_ERROR);
  }

  int match, i, itype = 0;
  CHAIN *class;
  for (i = 0; i < ntree; i++) {
    class = shChainElementGetByPos(tree, i);
    match = match_class(objc->phProps, class, tree);
    if (match) {
      cpy_class_id(class, objc->thType[itype]);
      itype++;
    }
  }
  
  if (itype > 0) {
    /* nullifying the n+1st element of thTypes */
    objc->thType[itype][0] = '\0';
    return(SH_SUCCESS); 
  } else {
    objc->thType[0] = "UNKNOWN";
    objc->thType[1][0] = '\0';

    return(SH_SUCCESS);
  }

}

static int match_class(PHPROPS *props, CHAIN *class, CHAIN *tree) {

  char *name = "match_class";

  OBJCDEF *def;
  int match, i, ndef;
  for (i = 0; i < ndef; i++) {
    def = (OBJCDEF *) shChainElementGetByPos(class, i);
    match = match_def(props, def, tree);
    if (match) {
      return(1);
    }
  }

  return(0);
}

static int match_def(PHPROPS *props, OBJCDEF *def, CHAIN *tree) {

  char *name = "match_def";

  int i, match, nsens, np;
  SENTENCE *sen;

  np = 0;
  for (i = 0; i < MX_STRING_ARR; i++) {
    if (def->parent[i][0] == '\0') {
      np = i;
      break;
    }
  }

  OBJCDEF *tempdef;
  tempdef = (OBJCDEF *) thCalloc(1, sizeof(OBJCDEF));

  CHAIN *class;
  for (i = 0; i < np; i++) {
    tempdef->name = def->parent[i];
    class = thObjcClassGetFromTree(tempdef, tree, 0); 
    match = match_class(props, class, tree);
    if (match == 0) {
      return(0);
    }
  }
  tempdef->name = NULL;
  thFree(tempdef);

  nsens = 0;
  for (i = 0; i < MX_STRING_ARR; i++) {
    if (def->sentences[i] == NULL) {
      nsens = i;
      break;
    }
  }

  for (i = 0; i < nsens; i++) {
    sen = def->sentences[i];
    match = match_sentence(props, sen);
    if (match == 0) {
      return(0);
    }
  }

  return(1);
}

static int match_sentence(PHPROPS *props, SENTENCE *sen) {
  
  char *name = "match_sentence";

  THPIX x;
  THOPERATOR operator;

  x = dot_prop_sen(props, sen); 
  operator = get_operator_sen(sen);
  
  switch (operator) {
  case GTOPERATOR:
    return((x > sen->value));
    break;
  case GEOPERATOR:
    return((x >= sen->value));
    break;
  case LTOPERATOR:
    return((x < sen->value));
    break;
  case LEOPERATOR:
    return((x <= sen->value));
    break;
  case EQOPERATOR:
    return((fabs(x - sen->value) <= sen->tolerance));
    break;
  case NEQOPERATOR:
    return((fabs(x - sen->value) > sen->tolerance));
    break;
  default:
    thError("%s: unknown operator type in sentence (%s)", 
		   name, sen->id);
    return(0);
  }

  return(0);
}

static THOPERATOR get_operator_sen (SENTENCE *sen) {

  char *opstr;
  int match;

  opstr = "GT";
  match = strmatch_nocase(sen->operator, opstr);
  if (match) {
    return(GTOPERATOR);
  }

  opstr = "GE";
  match = strmatch_nocase(sen->operator, opstr);
  if (match) {
    return(GEOPERATOR);
  }

  opstr = "LT";
  match = strmatch_nocase(sen->operator, opstr);
  if (match) {
    return(LTOPERATOR);
  }

  opstr = "LE";
  match = strmatch_nocase(sen->operator, opstr);
  if (match) {
    return(LEOPERATOR);
  }

  opstr = "EQ";
  match = strmatch_nocase(sen->operator, opstr);
  if (match) {
    return(EQOPERATOR);
  }

  opstr = "NEQ";
  match = strmatch_nocase(sen->operator, opstr);
  if (match) {
    return(NEQOPERATOR);
  }

  return(UNKNOWNOPERATOR);
 
}

static int strmatch_nocase(char *str1, char *str2) {

  int l1, l2;
  l1 = strlen(str1);
  l2 = strlen(str2);
  if (l1 != l2) {
    return(0);
  }
 
  char *x1;
  char *x2;
  
  x1 = (char *) thCalloc(l1 + 1, sizeof(char));
  x2 = (char *) thCalloc(l1 + 1, sizeof(char));


  strcpy(x1, str1);
  strcpy(x2, str2);
  
  upper_case(x1);
  upper_case(x2);
  
  
  int match;
  match = memcmp(x1, x2, l1);
  
  thFree(x1);
  thFree(x2);
  
  return(match);

}

static void upper_case(char *str) {

  if (str == NULL) return;

  int i, l;
  l = strlen(str);
  for (i = 0; i < l; i++) {
    if (islower(str[i])) str[i] = toupper(str[i]);
  }

  return;
}


static RET_CODE cpy_class_id(CHAIN *class, char *id) {

  char *name = "cpy_class_id";
  if (id == NULL || class == NULL) {
    thError("%s: null input passed", name);
    return(SH_GENERIC_ERROR);
  }

  if (shChainSize(class) == 0) {
    thError("%s: empty (class) passed", name);
    return(SH_GENERIC_ERROR);
  }

  OBJCDEF *def;
  def = (OBJCDEF * ) shChainElementGetByPos(class, 0);

  if (def->name == NULL) {
    thError("%s: object definition in class does not have a name", 
		   name);
    return(SH_GENERIC_ERROR);
  }

  if (def->name[0] == '\0') {
    thError("%s: object definition in class has an empty name string",
		   name);
    return(SH_GENERIC_ERROR);
  }
  
  strcpy(id, def->name);
  
  return(SH_SUCCESS);
}

static THPIX dot_props(OBJC_IO *objc1, OBJC_IO *objc2) {
  
  long double res = (long double) 0.0;

  res += (long double) (objc1->id) * (long double) (objc2->id);				/* id number for this objc */
  res += (long double) (objc1->parent) * (long double) (objc2->parent);			        /* id of parent for deblends */
  res += (long double) (objc1->ncolor)* (long double) (objc2->ncolor);			/* number of colours */
  res += (long double) (objc1->nchild)* (long double) (objc2->nchild);				/* number of children */
  res += (long double) (objc1->objc_type) * (long double) (objc2->objc_type);			/* overall classification */
  res += (long double) (objc1->objc_prob_psf)* (long double) (objc2->objc_prob_psf);			/* Bayesian probability of being PSF */
  res += (long double) (objc1->catID)* (long double) (objc2->catID);			        /* catalog id number */
  res += (long double) (objc1->objc_flags) * (long double) (objc2-> objc_flags);			/* flags from OBJC */
  res += (long double) (objc1->objc_flags2)* (long double) (objc2->objc_flags2);			/* flags2 from OBJC */
  res += (long double) (objc1->objc_rowc)* (long double) (objc2->objc_rowc); 
  res += (long double) (objc1->objc_rowcErr)* (long double) (objc2->objc_rowcErr);	/* row position and error of centre */
  res += (long double) (objc1->objc_colc)* (long double) (objc2->objc_colc); 
  res += (long double) (objc1->objc_colcErr)* (long double) (objc2->objc_colcErr);	/* column position and error */
  res += (long double) (objc1->rowv)* (long double) (objc2->rowv); 
  res += (long double) (objc1->rowvErr)* (long double) (objc2->rowvErr);			/* row velocity, in pixels/frame (NS)*/
  res += (long double) (objc1->colv)* (long double) (objc2->colv); 
  res += (long double) (objc1->colvErr)* (long double) (objc2->colvErr);			/* col velocity, in pixels/frame (NS)*/
  
  int i;
  for (i = 0; i < NCOLOR; i++) {
/*
 * Unpacked OBJECT1s
 */
    res += (long double) (objc1->rowc[i]) * (long double) (objc2-> rowc[i]);
    res += (long double) (objc1->rowcErr[i]) * (long double) (objc2-> rowcErr[i]);
    res += (long double) (objc1->colc[i]) * (long double) (objc2-> colc[i]);
    res += (long double) (objc1->colcErr[i]) * (long double) (objc2-> colcErr[i]);
    res += (long double) (objc1->sky[i]) * (long double) (objc2-> sky[i]);
    res += (long double) (objc1->skyErr[i]) * (long double) (objc2-> skyErr[i]);
/*
 * PSF and aperture fits/magnitudes
 */
    res += (long double) (objc1->psfCounts[i])* (long double) (objc2-> psfCounts[i]);
    res += (long double) (objc1->psfCountsErr[i])* (long double) (objc2->psfCountsErr[i]);
    res += (long double) (objc1->fiberCounts[i])* (long double) (objc2-> fiberCounts[i]);
    res += (long double) (objc1->fiberCountsErr[i])* (long double) (objc2->fiberCountsErr[i]);
    res += (long double) (objc1->petroCounts[i])* (long double) (objc2-> petroCounts[i]);
    res += (long double) (objc1->petroCountsErr[i])* (long double) (objc2->petroCountsErr[i]);
    res += (long double) (objc1->petroRad[i])* (long double) (objc2-> petroRad[i]);
    res += (long double) (objc1->petroRadErr[i])* (long double) (objc2->petroRadErr[i]);
    res += (long double) (objc1->petroR50[i])* (long double) (objc2->petroR50[i]);
    res += (long double) (objc1->petroR50Err[i])* (long double) (objc2->petroR50Err[i]);
    res += (long double) (objc1->petroR90[i])* (long double) (objc2-> petroR90[i]);
    res += (long double) (objc1->petroR90Err[i])* (long double) (objc2->petroR90Err[i]);
/*
 * Shape of object
 */
    res += (long double) (objc1->Q[i])* (long double) (objc2->Q[i]);
    res += (long double) (objc1->QErr[i])* (long double) (objc2-> QErr[i]);
    res += (long double) (objc1->U[i])* (long double) (objc2->U[i]);
    res += (long double) (objc1-> UErr[i])* (long double) (objc2-> UErr[i]);

    res += (long double) (objc1->M_e1[i])* (long double) (objc2-> M_e1[i]);
    res += (long double) (objc1->M_e2[i])* (long double) (objc2->M_e2[i]);
    res += (long double) (objc1->M_e1e1Err[i])* (long double) (objc2->M_e1e1Err[i]);
    res += (long double) (objc1->M_e1e2Err[i])* (long double) (objc2->M_e1e2Err[i]);
    res += (long double) (objc1->M_e2e2Err[i])* (long double) (objc2->M_e2e2Err[i]);
    res += (long double) (objc1->M_rr_cc[i])* (long double) (objc2->M_rr_cc[i]);
    res += (long double) (objc1->M_rr_ccErr[i])* (long double) (objc2->M_rr_ccErr[i]);
    res += (long double) (objc1->M_cr4[i])* (long double) (objc2->M_cr4[i]);
    res += (long double) (objc1->M_e1_psf[i])* (long double) (objc2->M_e1_psf[i]);
    res += (long double) (objc1->M_e2_psf[i])* (long double) (objc2->M_e2_psf[i]);
    res += (long double) (objc1->M_rr_cc_psf[i])* (long double) (objc2->M_rr_cc_psf[i]);
    res += (long double) (objc1->M_cr4_psf[i])* (long double) (objc2->M_cr4_psf[i]);
/*
 * Properties of a certain isophote.
 */
    res += (long double) (objc1->iso_rowc[i])* (long double) (objc2->iso_rowc[i]);
    res += (long double) (objc1->iso_rowcErr[i])* (long double) (objc2->iso_rowcErr[i]);
    res += (long double) (objc1->iso_rowcGrad[i])* (long double) (objc2->iso_rowcGrad[i]);
    res += (long double) (objc1->iso_colc[i])* (long double) (objc2->iso_colc[i]);
    res += (long double) (objc1->iso_colcErr[i])* (long double) (objc2->iso_colcErr[i]);
    res += (long double) (objc1->iso_colcGrad[i])* (long double) (objc2->iso_colcGrad[i]);
    res += (long double) (objc1->iso_a[i])* (long double) (objc2->iso_a[i]);
    res += (long double) (objc1->iso_aErr[i])* (long double) (objc2->iso_aErr[i]);
    res += (long double) (objc1->iso_aGrad[i])* (long double) (objc2->iso_aGrad[i]);
    res += (long double) (objc1->iso_b[i])* (long double) (objc2->iso_b[i]);
    res += (long double) (objc1->iso_bErr[i])* (long double) (objc2->iso_bErr[i]);
    res += (long double) (objc1->iso_bGrad[i])* (long double) (objc2->iso_bGrad[i]);
    res += (long double) (objc1->iso_phi[i])* (long double) (objc2->iso_phi[i]);
    res += (long double) (objc1->iso_phiErr[i])* (long double) (objc2->iso_phiErr[i]);
    res += (long double) (objc1->iso_phiGrad[i])* (long double) (objc2->iso_phiGrad[i]);
/*
 * Model parameters for deV and exponential models
 */
    res += (long double) (objc1->r_deV[i])* (long double) (objc2->r_deV[i]);
    res += (long double) (objc1->r_deVErr[i])* (long double) (objc2->r_deVErr[i]);
    res += (long double) (objc1->ab_deV[i])* (long double) (objc2-> ab_deV[i]);
    res += (long double) (objc1->ab_deVErr[i])* (long double) (objc2->ab_deVErr[i]);
    res += (long double) (objc1->phi_deV[i])* (long double) (objc2->phi_deV[i]);
    res += (long double) (objc1->phi_deVErr[i])* (long double) (objc2->phi_deVErr[i]);
    res += (long double) (objc1->counts_deV[i])* (long double) (objc2->counts_deV[i]);
    res += (long double) (objc1->counts_deVErr[i])* (long double) (objc2->counts_deVErr[i]);
    res += (long double) (objc1->r_exp[i])* (long double) (objc2->r_exp[i]);
    res += (long double) (objc1->r_expErr[i])* (long double) (objc2->r_expErr[i]);
    res += (long double) (objc1->ab_exp[i])* (long double) (objc2-> ab_exp[i]);
    res += (long double) (objc1->ab_expErr[i])* (long double) (objc2->ab_expErr[i]);
    res += (long double) (objc1->phi_exp[i])* (long double) (objc2-> phi_exp[i]);
    res += (long double) (objc1->phi_expErr[i])* (long double) (objc2->phi_expErr[i]);
    res += (long double) (objc1->counts_exp[i])* (long double) (objc2-> counts_exp[i]);
    res += (long double) (objc1->counts_expErr[i])* (long double) (objc2->counts_expErr[i]);

    res += (long double) (objc1->counts_model[i])* (long double) (objc2-> counts_model[i]);
    res += (long double) (objc1->counts_modelErr[i])* (long double) (objc2->counts_modelErr[i]);
/*
 * Measures of image structure
 */
    res += (long double) (objc1->texture[i])* (long double) (objc2->texture[i]);
/*
 * Classification information
 */
    res += (long double) (objc1->star_L[i])* (long double) (objc2->star_L[i]);
    res += (long double) (objc1->star_lnL[i])* (long double) (objc2->star_lnL[i]);
    res += (long double) (objc1->exp_L[i])* (long double) (objc2-> exp_L[i]);
    res += (long double) (objc1->exp_lnL[i])* (long double) (objc2->exp_lnL[i]);
    res += (long double) (objc1->deV_L[i])* (long double) (objc2->deV_L[i]);
    res += (long double) (objc1->deV_lnL[i])* (long double) (objc2->deV_lnL[i]);
    res += (long double) (objc1->fracPSF[i])* (long double) (objc2->fracPSF[i]);

    res += (long double) (objc1->flags[i])* (long double) (objc2->flags[i]);
    res += (long double) (objc1->flags2[i])* (long double) (objc2->flags2[i]);
    
    res += (long double) (objc1->type[i]) * (long double) (objc2->type[i])  ;
    res += (long double) (objc1->prob_psf[i])* (long double) (objc2->prob_psf[i]);
/*
 * Profile and extent of object
 */
    res += (long double) (objc1->nprof[i])* (long double) (objc2->nprof[i]);

    /* the following arrays are not mapped
    res += (long double) (objc1->profMean[i][NANN])* (long double) (objc2->profMean[i][NANN]);
    res += (long double) (objc1->profErr[i][NANN])* (long double) (objc2->profErr[i][NANN]);
    */
  }

  THPIX res_thpix;
  res_thpix = (THPIX) res;
  return(res_thpix);
}

static THPIX dot_prop_sen(OBJC_IO *objc1, SENTENCE *objc2) {
  
  long double res = (long double) 0.0;

  res += (long double) (objc1->id) * (long double) (objc2->id);				/* id number for this objc */
  res += (long double) (objc1->parent) * (long double) (objc2->parent);			        /* id of parent for deblends */
  res += (long double) (objc1->ncolor)* (long double) (objc2->ncolor);			/* number of colours */
  res += (long double) (objc1->nchild)* (long double) (objc2->nchild);				/* number of children */
  res += (long double) (objc1->objc_type) * (long double) (objc2->objc_type);			/* overall classification */
  res += (long double) (objc1->objc_prob_psf)* (long double) (objc2->objc_prob_psf);			/* Bayesian probability of being PSF */
  res += (long double) (objc1->catID)* (long double) (objc2->catID);			        /* catalog id number */
  res += (long double) (objc1->objc_flags) * (long double) (objc2-> objc_flags);			/* flags from OBJC */
  res += (long double) (objc1->objc_flags2)* (long double) (objc2->objc_flags2);			/* flags2 from OBJC */
  res += (long double) (objc1->objc_rowc)* (long double) (objc2->objc_rowc); 
  res += (long double) (objc1->objc_rowcErr)* (long double) (objc2->objc_rowcErr);	/* row position and error of centre */
  res += (long double) (objc1->objc_colc)* (long double) (objc2->objc_colc); 
  res += (long double) (objc1->objc_colcErr)* (long double) (objc2->objc_colcErr);	/* column position and error */
  res += (long double) (objc1->rowv)* (long double) (objc2->rowv); 
  res += (long double) (objc1->rowvErr)* (long double) (objc2->rowvErr);			/* row velocity, in pixels/frame (NS)*/
  res += (long double) (objc1->colv)* (long double) (objc2->colv); 
  res += (long double) (objc1->colvErr)* (long double) (objc2->colvErr);			/* col velocity, in pixels/frame (NS)*/
  
  int i;
  for (i = 0; i < NCOLOR; i++) {
/*
 * Unpacked OBJECT1s
 */
    res += (long double) (objc1->rowc[i]) * (long double) (objc2-> rowc[i]);
    res += (long double) (objc1->rowcErr[i]) * (long double) (objc2-> rowcErr[i]);
    res += (long double) (objc1->colc[i]) * (long double) (objc2-> colc[i]);
    res += (long double) (objc1->colcErr[i]) * (long double) (objc2-> colcErr[i]);
    res += (long double) (objc1->sky[i]) * (long double) (objc2-> sky[i]);
    res += (long double) (objc1->skyErr[i]) * (long double) (objc2-> skyErr[i]);
/*
 * PSF and aperture fits/magnitudes
 */
    res += (long double) (objc1->psfCounts[i])* (long double) (objc2-> psfCounts[i]);
    res += (long double) (objc1->psfCountsErr[i])* (long double) (objc2->psfCountsErr[i]);
    res += (long double) (objc1->fiberCounts[i])* (long double) (objc2-> fiberCounts[i]);
    res += (long double) (objc1->fiberCountsErr[i])* (long double) (objc2->fiberCountsErr[i]);
    res += (long double) (objc1->petroCounts[i])* (long double) (objc2-> petroCounts[i]);
    res += (long double) (objc1->petroCountsErr[i])* (long double) (objc2->petroCountsErr[i]);
    res += (long double) (objc1->petroRad[i])* (long double) (objc2-> petroRad[i]);
    res += (long double) (objc1->petroRadErr[i])* (long double) (objc2->petroRadErr[i]);
    res += (long double) (objc1->petroR50[i])* (long double) (objc2->petroR50[i]);
    res += (long double) (objc1->petroR50Err[i])* (long double) (objc2->petroR50Err[i]);
    res += (long double) (objc1->petroR90[i])* (long double) (objc2-> petroR90[i]);
    res += (long double) (objc1->petroR90Err[i])* (long double) (objc2->petroR90Err[i]);
/*
 * Shape of object
 */
    res += (long double) (objc1->Q[i])* (long double) (objc2->Q[i]);
    res += (long double) (objc1->QErr[i])* (long double) (objc2-> QErr[i]);
    res += (long double) (objc1->U[i])* (long double) (objc2->U[i]);
    res += (long double) (objc1-> UErr[i])* (long double) (objc2-> UErr[i]);

    res += (long double) (objc1->M_e1[i])* (long double) (objc2-> M_e1[i]);
    res += (long double) (objc1->M_e2[i])* (long double) (objc2->M_e2[i]);
    res += (long double) (objc1->M_e1e1Err[i])* (long double) (objc2->M_e1e1Err[i]);
    res += (long double) (objc1->M_e1e2Err[i])* (long double) (objc2->M_e1e2Err[i]);
    res += (long double) (objc1->M_e2e2Err[i])* (long double) (objc2->M_e2e2Err[i]);
    res += (long double) (objc1->M_rr_cc[i])* (long double) (objc2->M_rr_cc[i]);
    res += (long double) (objc1->M_rr_ccErr[i])* (long double) (objc2->M_rr_ccErr[i]);
    res += (long double) (objc1->M_cr4[i])* (long double) (objc2->M_cr4[i]);
    res += (long double) (objc1->M_e1_psf[i])* (long double) (objc2->M_e1_psf[i]);
    res += (long double) (objc1->M_e2_psf[i])* (long double) (objc2->M_e2_psf[i]);
    res += (long double) (objc1->M_rr_cc_psf[i])* (long double) (objc2->M_rr_cc_psf[i]);
    res += (long double) (objc1->M_cr4_psf[i])* (long double) (objc2->M_cr4_psf[i]);
/*
 * Properties of a certain isophote.
 */
    res += (long double) (objc1->iso_rowc[i])* (long double) (objc2->iso_rowc[i]);
    res += (long double) (objc1->iso_rowcErr[i])* (long double) (objc2->iso_rowcErr[i]);
    res += (long double) (objc1->iso_rowcGrad[i])* (long double) (objc2->iso_rowcGrad[i]);
    res += (long double) (objc1->iso_colc[i])* (long double) (objc2->iso_colc[i]);
    res += (long double) (objc1->iso_colcErr[i])* (long double) (objc2->iso_colcErr[i]);
    res += (long double) (objc1->iso_colcGrad[i])* (long double) (objc2->iso_colcGrad[i]);
    res += (long double) (objc1->iso_a[i])* (long double) (objc2->iso_a[i]);
    res += (long double) (objc1->iso_aErr[i])* (long double) (objc2->iso_aErr[i]);
    res += (long double) (objc1->iso_aGrad[i])* (long double) (objc2->iso_aGrad[i]);
    res += (long double) (objc1->iso_b[i])* (long double) (objc2->iso_b[i]);
    res += (long double) (objc1->iso_bErr[i])* (long double) (objc2->iso_bErr[i]);
    res += (long double) (objc1->iso_bGrad[i])* (long double) (objc2->iso_bGrad[i]);
    res += (long double) (objc1->iso_phi[i])* (long double) (objc2->iso_phi[i]);
    res += (long double) (objc1->iso_phiErr[i])* (long double) (objc2->iso_phiErr[i]);
    res += (long double) (objc1->iso_phiGrad[i])* (long double) (objc2->iso_phiGrad[i]);
/*
 * Model parameters for deV and exponential models
 */
    res += (long double) (objc1->r_deV[i])* (long double) (objc2->r_deV[i]);
    res += (long double) (objc1->r_deVErr[i])* (long double) (objc2->r_deVErr[i]);
    res += (long double) (objc1->ab_deV[i])* (long double) (objc2-> ab_deV[i]);
    res += (long double) (objc1->ab_deVErr[i])* (long double) (objc2->ab_deVErr[i]);
    res += (long double) (objc1->phi_deV[i])* (long double) (objc2->phi_deV[i]);
    res += (long double) (objc1->phi_deVErr[i])* (long double) (objc2->phi_deVErr[i]);
    res += (long double) (objc1->counts_deV[i])* (long double) (objc2->counts_deV[i]);
    res += (long double) (objc1->counts_deVErr[i])* (long double) (objc2->counts_deVErr[i]);
    res += (long double) (objc1->r_exp[i])* (long double) (objc2->r_exp[i]);
    res += (long double) (objc1->r_expErr[i])* (long double) (objc2->r_expErr[i]);
    res += (long double) (objc1->ab_exp[i])* (long double) (objc2-> ab_exp[i]);
    res += (long double) (objc1->ab_expErr[i])* (long double) (objc2->ab_expErr[i]);
    res += (long double) (objc1->phi_exp[i])* (long double) (objc2-> phi_exp[i]);
    res += (long double) (objc1->phi_expErr[i])* (long double) (objc2->phi_expErr[i]);
    res += (long double) (objc1->counts_exp[i])* (long double) (objc2-> counts_exp[i]);
    res += (long double) (objc1->counts_expErr[i])* (long double) (objc2->counts_expErr[i]);

    res += (long double) (objc1->counts_model[i])* (long double) (objc2-> counts_model[i]);
    res += (long double) (objc1->counts_modelErr[i])* (long double) (objc2->counts_modelErr[i]);
/*
 * Measures of image structure
 */
    res += (long double) (objc1->texture[i])* (long double) (objc2->texture[i]);
/*
 * Classification information
 */
    res += (long double) (objc1->star_L[i])* (long double) (objc2->star_L[i]);
    res += (long double) (objc1->star_lnL[i])* (long double) (objc2->star_lnL[i]);
    res += (long double) (objc1->exp_L[i])* (long double) (objc2-> exp_L[i]);
    res += (long double) (objc1->exp_lnL[i])* (long double) (objc2->exp_lnL[i]);
    res += (long double) (objc1->deV_L[i])* (long double) (objc2->deV_L[i]);
    res += (long double) (objc1->deV_lnL[i])* (long double) (objc2->deV_lnL[i]);
    res += (long double) (objc1->fracPSF[i])* (long double) (objc2->fracPSF[i]);

    res += (long double) (objc1->flags[i])* (long double) (objc2->flags[i]);
    res += (long double) (objc1->flags2[i])* (long double) (objc2->flags2[i]);
    
    res += (long double) (objc1->type[i]) * (long double) (objc2->type[i])  ;
    res += (long double) (objc1->prob_psf[i])* (long double) (objc2->prob_psf[i]);
/*
 * Profile and extent of object
 */
    res += (long double) (objc1->nprof[i])* (long double) (objc2->nprof[i]);

    /* the following arrays are not mapped
    res += (long double) (objc1->profMean[i][NANN])* (long double) (objc2->profMean[i][NANN]);
    res += (long double) (objc1->profErr[i][NANN])* (long double) (objc2->profErr[i][NANN]);
    */
  }

  THPIX res_thpix;
  res_thpix = (THPIX) res;
  return(res_thpix);
}
