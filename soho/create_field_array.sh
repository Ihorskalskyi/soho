#!/bin/bash

function check_data_exists_for_mle {

local name="check_data_exists_for_mle"
DATE=$(date +"%m-%d-%Y")
TIME=$(date +"%T") 

lband=no
lcamcol=no
lrun=no
lrerun=no
ldatadir=no

fit2fpCC=no
dband="r"
dcamcol="4"
drun=$DEFAULT_SDSS_RUN
drerun=$DEFAULT_SDSS_RERUN

while getopts ':-:' OPTION ; do
  case "$OPTION" in
    -  ) [ $OPTIND -ge 1 ] && optind=$(expr $OPTIND - 1 ) || optind=$OPTIND
         eval OPTION="\$$optind"
         OPTARG=$(echo $OPTION | cut -d'=' -f2)
         OPTION=$(echo $OPTION | cut -d'=' -f1)
         case $OPTION in
	     --fit2fpCC  ) fit2fpCC=yes		         ;;
     	     --run       ) lrun=yes; run="$OPTARG"	 ;;
	     --rerun     ) lrerun=yes; rerun="$OPTARG"    ;; 
	     --camcol    ) lcamcol=yes; camcol="$OPTARG" ;;
	     --band      ) lband=yes; band="$OPTARG"	 ;;
	     --datadir   ) ldatadir=yes; datadir="$OPTARG"     ;;
	* )  echo "$name: invalid options (long) - optind = $OPTARG, option = $OPTION" ;;
         esac
       OPTIND=1
       shift
      ;;
    ? )  echo "$name: invalid options (short) "  ;;
  esac
done

if [ $ldatadir == no ]; then
	echo "$name: ERROR - argument (datadir) should be set"
	exit
fi
if [ $lrun == no ]; then
	run=$drun
	echo "$name: argument (run)    set to '$run'"
fi
if [ $lrerun == no ]; then
	rerun=$drerun;
	echo "$name: argument (rerun)  set to '$rerun'"
fi
if [ $lcamcol == no ]; then
	camcol=$dcamcol
	echo "$name: argument (camcol) set to '$camcol'"
fi
if [ $lband == no ]; then
	band=$dband
	echo "$name: argument (band)   set to '$band'"
fi

fieldstr=`printf "%04d" $field`
runstr=`printf "%06d" $run`

srcparfile="$datadir/parfiles/ff-$runstr-$band$camcol-$fieldstr.par"
fpCfile="$datadir/photo/$fpCprefix-$runstr-$band$camcol-$fieldstr.fit"	
fpCCfile="$datadir/photo/$fpCCprefix-$runstr-$band$camcol-$fieldstr.fit"

if [[ $fit2fpCC == yes ]]; then
	desired_fpCfile=$fpCCfile
else
	desired_fpCfile=$fpCfile
fi
desired_fpCfile_gz="$desired_fpCfile.gz"
if [[ -e $srcparfile ]]; then 
#  && [[ [ -e $desired_fpCfile] || [ -e $desired_fpCfile_gz ] ]] ]]; then
	exists=1
else
	exists=0
fi
#since the value is an integer you can return it using the returned command
return $exists

}

function create_field_array {

local name="create_field_array"
local DATE=$(date +"%m-%d-%Y")
local TIME=$(date +"%T") 


local debug=no
local verbose=no
local fit2fpCC=no

local lband=no
local lcamcol=no
local lrun=no
local lrerun=no
local lfstep=no
local lfield0=no
local lfield1=no
local ldatadir=no
local lexcept=no

local dband="r"
local dcamcol="4"
local drun=$DEFAULT_SDSS_RUN
local drerun="137"
local dfstep=68
local dfield0=100
local dfield1=5000
local -a dexcept=()

while getopts ':-:' OPTION ; do
  case "$OPTION" in
    -  ) [ $OPTIND -ge 1 ] && optind=$(expr $OPTIND - 1 ) || optind=$OPTIND
         eval OPTION="\$$optind"
         OPTARG=$(echo $OPTION | cut -d'=' -f2)
         OPTION=$(echo $OPTION | cut -d'=' -f1)
         case $OPTION in
	     --fit2fpCC  ) fit2fpCC=yes		         ;;
	     --field0	 ) lfield0=yes; field0="$OPTARG"   ;;
	     --field1    ) lfield1=yes; field1="$OPTARG" ;;
	     --fstep     ) lfstep=yes; fstep="$OPTARG" ;;
     	     --run       ) lrun=yes; run="$OPTARG"	 ;;
	     --rerun     ) lrerun=yes; rerun="$OPTARG"    ;; 
	     --camcol    ) lcamcol=yes; camcol="$OPTARG" ;;
	     --band      ) lband=yes; band="$OPTARG"	 ;;
	     --datadir   ) ldatadir=yes; datadir="$OPTARG"     ;;
	     --except    ) lexcept=yes; except="$OPTARG" ;;
	* )  echo "$name: invalide options (long)" ;;
         esac
       OPTIND=1
       shift
      ;;
    ? )  echo "$name: invalid options (short) "  ;;
  esac
done

if [ $lfield0 == no ]; then
	field0=$dfield0
fi
if [ $lfield1 == no ]; then
	field1=$dfield1
fi
if [ $lfstep == no ]; then
	fstep=$dfstep
fi
if [ $lrun == no ]; then
	run=$drun
fi
if [ $lrerun == no ]; then
	rerun=$drerun;
fi
if [ $lcamcol == no ]; then
	camcol=$dcamcol
fi
if [ $lband == no ]; then
	band=$dband
fi
if [ $ldatadir == no ]; then
	echo "$name: ERROR - argument (datadir) should be passed" 
	exit
fi
if [ $lexcept == no ]; then 
	except=${dexcept[@]}	
fi
fpCCflag=""
if [[ $fit2fpCC == yes ]]; then 
fpCCflag="--fit2fpCC"
fi
nexcept=${#except[@]}
parser=","

local data=""

if [ 1 == 0 ]; then 

ifield=$field0
while [ $ifield -le $field1 ] 
do
	excepted=false
	for (( iexcept=0; iexcept<$nexcept; iexcept++ )) 
	do 
		if [[ ${except[$iexcept]} -eq $ifield ]]; then
			excepted=true
		fi
	done
	if [[ $excepted == false ]]; then
		dataexists=$(check_data_exists_for_mle --datadir="$datadir" --field="$ifield" --run=$run --camcol="$camcol"  --band=$band)
		if [[ $dataexists ]]; then
			if [[ $ndata -eq 0 ]]; then
				data="$ifield"
			else
				data="$data,$ifield"
			fi
			let ndata++
		fi
	fi
	let "ifield = $ifield + $fstep"
done

fi

local ndata=0
local file_list=($(find $datadir/parfiles/*par  -printf "%f\n"))
local nfile=${#file_list[@]}

for (( ifile=0; ifile<$nfile; ifile++ )) 
do 

local filename=${file_list[$ifile]}	
local filename_len=${#filename}
local start_loc=$((filename_len-8))
local field=${filename:$start_loc:4}
field=$((10#$field))
if [[ $ndata -eq 0 ]]; then
	data="$field"
else
	data="$data,$field"
fi
let ndata++

done

echo $data

}


function create_field_array_mledata {

local name="create_field_array_mledata"
local DATE=$(date +"%m-%d-%Y")
local TIME=$(date +"%T") 


local debug=no
local verbose=no
local fit2fpCC=no

local lband=no
local lcamcol=no
local lrun=no
local lrerun=no
local lfstep=no
local lfield0=no
local lfield1=no
local ldatadir=no
local lexcept=no

local dband="r"
local dcamcol="4"
local drun=$DEFAULT_SDSS_RUN
local drerun="137"
local dfstep=68
local dfield0=100
local dfield1=5000
local -a dexcept=()

while getopts ':-:' OPTION ; do
  case "$OPTION" in
    -  ) [ $OPTIND -ge 1 ] && optind=$(expr $OPTIND - 1 ) || optind=$OPTIND
         eval OPTION="\$$optind"
         OPTARG=$(echo $OPTION | cut -d'=' -f2)
         OPTION=$(echo $OPTION | cut -d'=' -f1)
         case $OPTION in
	     --band      ) lband=yes; band="$OPTARG"	 ;;
	     --datadir   ) ldatadir=yes; datadir="$OPTARG"     ;;
	* )  echo "$name: invalide options (long)" ;;
         esac
       OPTIND=1
       shift
      ;;
    ? )  echo "$name: invalid options (short) "  ;;
  esac
done

if [ $lband == no ]; then
	band=$dband
fi
if [ $ldatadir == no ]; then
	echo "$name: ERROR - argument (datadir) should be passed" 
	exit
fi
fpCCflag=""
parser=","

local data=""

#frame-r-001000-1-0095.fits.bz2
local ndata=0
local file_list=($(find $datadir/frame-$band*fits.bz2  -printf "%f\n"))
local nfile=${#file_list[@]}

for (( ifile=0; ifile<$nfile; ifile++ )) 
do 

local filename=${file_list[$ifile]}	
local filename_len=${#filename}
local start_loc=$((filename_len-13))
local field=${filename:$start_loc:4}
field=$((10#$field))
if [[ $ndata -eq 0 ]]; then
	data="$field"
else
	data="$data,$field"
fi
let ndata++

done

echo $data

}


