
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "strings.h"
#include "dervish.h"
#include "sohoEnv.h"
#include "thDebug.h"

#define FpmFileType STANDARD              /* FITS file Type: STANDARD, NONSTANDARD, IMAGE */
#define FpmFileDef  DEF_DEFAULT /* = DEF_NUM_OF_ELEMS    /* = DEF_NONE: thCFitsIo.h */
#define NROW_FPM 1489
#define NCOLUMN_FPM 2048
#define REGION_FLAGS_FPM NO_FLAGS

MASK *thMaskReadFpm(char *file,  
		     const int row0,  const int column0,  
		     const int row1,  const int column1, 
		     RET_CODE *thStatus);
RET_CODE thWriteFpm(char *file, 
		    MASK *mask);

RET_CODE thCpFpm(char *infile, char *outfile, /* input and output filenames */
		       const int row0, const int column0, 
		       const int row1, const int column1 /* corners of the image in INFILE */);

/*****************************************************************************/

MASK *thMaskReadFpm(char *file,  
		   const int row0,  const int column0,  
		   const int row1,  const int column1, 
		   RET_CODE *thStatus)
{
  char *name = "thMaskReadFpm";
  char *image="im";
  int  nrow,  ncolumn;
  MASK *sub = NULL, *fullmask = NULL, *mask = NULL;

  nrow = row1 - row0 + 1;
  ncolumn = column1 - column0 + 1;

  /*RET_CODE thMaskReadAsFits
    (
    MASK   *a_maskPtr,       // IN: Pointer to mask //
    char   *a_file,          // IN: Name of FITS file //
    DEFDIRENUM a_FITSdef,    // IN: Directory/file extension default type //
    FILE   *maskFilePtr,     // IN:  If != NULL then is a pipe to read from //
    int     a_readtape       // IN:  Flag to indicate that we must fork/exec an
    instance of "dd" to read a tape drive //
    ) */
  mask = thMaskNew(image, nrow, ncolumn);
  fullmask = thMaskNew(image, NROW_FPM, NCOLUMN_FPM);

  *thStatus = shMaskReadAsFits(fullmask, file, FpmFileDef, NULL, 0);
  if (*thStatus != SH_SUCCESS) {
    thMaskDel(mask);
    thMaskDel(fullmask);
    thErro/u/khosrow/lib/Linux/photo/src/phSpanUtil.cr("%s: %d \n", name, *thStatus);
    shError("%s: cannot read the MASK in %s\n", name, file);
    *thStatus = SH_GENERIC_ERROR;
    return(NULL);
    }
  
  /*C SYNTAX: 
    REGION * thSubMaskNew ( char *name,  IN : Address of name 
    MASK *mask // IN : Address of parent region/ 
    int nrow, // IN : Number of rows in image /
    int ncol, // IN : Number of columns in image /
    int row0, // IN : smallest row number in parent /
    int col0, // IN : smallest col number in parent /
    REGION_FLAGS flags // IN : Described above/ )
  */
    /* defining a sub-image and making a new, separate, image from it */
  if ((sub = thSubMaskNew(image, fullmask, nrow, ncolumn, row0, column0, REGION_FLAGS_FPM)) == NULL){
    shError("%s: cannot trim region %s\n", name, file);
    *thStatus = SH_GENERIC_ERROR;
    return(NULL);
  }
  thMaskCopy(sub, mask); 

  if (sub != NULL) {thMaskDel(sub);}
  if (fullmask != NULL) {thMaskDel(fullmask);}

  if (*thStatus != SH_SUCCESS) {
    return(NULL);
  } 
  
  /* successful run */
  *thStatus = SH_SUCCESS;
  return(mask);
}


RET_CODE thWriteFpm(char *file, 
		    MASK *mask)
{

  char *name = "thWriteFpc";
  RET_CODE thStatus;
  
  /* Write the MASK */

  /*
    RET_CODE thMaskWriteAsFits
    (
    MASK   *a_maskPtr,       // IN: Pointer to mask //
    char   *a_file,          // IN: Name of FITS file //
    DEFDIRENUM a_FITSdef,    // IN: Directory/file extension default type //
    FILE   *maskFilePtr,     // IN:  If != NULL then is a pipe to read from //
    int     a_writetape      // IN:  Flag to indicate that we must fork/exec an
    instance of "dd" to write a tape drive //
				      )
  */
  if (mask != NULL){
    thStatus = shMaskWriteAsFits (mask, file, FpmFileDef, NULL, 0);
  }
  return(thStatus);
}
  

RET_CODE thCpFpm(char *infile, char *outfile, /* input and output filenames */
		 const int row0, const int column0, 
		 const int row1, const int column1 /* corners of the image in INFILE */) 
{
  
  char *name = "thCpFpm";
  MASK *mask = NULL;
  RET_CODE thStatus;

  mask = thMaskReadFpm(infile, row0, column0, row1, column1, &thStatus);
  if (thStatus != SH_SUCCESS) {
    thMaskDel(mask);
    return(thStatus);
  }
  thStatus = thWriteFpm(outfile, mask);

  if (mask != NULL) {
    thMaskDel(mask);
  }
  return(thStatus);
}
