#ifndef CAPTURETYPES_H
#define CAPTURETYPES_H

#include "dervish.h"
#include "thConsts.h"
#include "thMleTypes.h"
#include "thProcTypes.h"
#include "thIoTypes.h"

#define CAPTURE_MODE int
#define UNKNOWN_CMODE  (0)
#define GCAPTURE_MODEL (1 << 0)
#define GCAPTURE_OBJC  (1 << 1)
#define GCAPTURE_SKY   (1 << 2)
#define GCAPTURE_RES   (1 << 3)

#define PCAPTURE_MODEL (1 << 4)
#define PCAPTURE_OBJC  (1 << 5)
#define PCAPTURE_SKY   (1 << 6)
#define PCAPTURE_RES   (1 << 7)
#define PCAPTURE_PAR   (1 << 8)

#define GCAPTURE_ALL (GCAPTURE_MODEL | GCAPTURE_OBJC | GCAPTURE_SKY | GCAPTURE_RES)
#define PCAPTURE_ALL (PCAPTURE_MODEL | PCAPTURE_OBJC | PCAPTURE_SKY | PCAPTURE_RES | PCAPTURE_PAR)
#define CAPTURE_ALL (GCAPTURE_ALL | PCAPTURE_ALL)

typedef enum capture_sector {
	CAPTURE_MODEL, CAPTURE_RES, CAPTURE_OBJC, CAPTURE_SKY, 
	N_CAPTURE_SECTOR, UNKNOWN_CAPTURE
} CAPTURE_SECTOR;

typedef struct capture_stat {
	int n, nrow, ncol;
	THPIX min, max, median, mean, mode;
	THPIX sigma, iqr, q1, q3, nout1, nout3, nsuspect1, nsuspect2;
	THPIX nsigma; /* clipping info */
	char *title;
	THPIX bmax;
} CAPTURE_STAT;


typedef enum capture_flag {
	CAPTURE_INIT, CAPTURE_FINI, 
	CAPTURE_DONE, CAPTURE_UNKNWON
	} CAPTURE_FLAG; 

typedef struct capture {

	LSTRUCT *lstruct;
	FRAME *frame;
	FRAMEID *id;

	CAPTURE_MODE cmode; /* mode of graphical and parametric text capturing */
	char *fpar; /* filename for parameteric text representation */

	char **fmts; /* of size [N_CAPTURE_MODEL]; */
	char **sfiles; /* of size [N_CAPTURE_MODEL]; */
	char **rfiles; /* of size [N_CAPTURE_MODEL]; */
	char **u8fmts; /* size of [NCAPTURE_MODEL]; */
	char **u8files; /* size of [NCAPTURE_MODEL]; */
	char **subfiles; /* size of [NCAPTURE_MODEL]; */
	FILE **substreams; /* size of [NCAPTURE_MODEL]; */
	REGION **regs; /* of size [N_CAPTURE_MODEL]; */
	LSTRUCT **ls; /* of size [N_CAPTURE_MODEL] ; */
	CHAIN **chains; /* of size [N_CAPTURE_SECTOR] */
	CHAIN *pChainOfChain; 

 	LINITMODE initmode; /* the method for choise of initial value SDSS, SDSS+error, CORRECT VALUE, CORRECT VALUE+error */
	LIMAGEMODE imagemode; /* if the fit image is poisson grained or not CONTINUOUS, POISSON */
	REGION *fimage; /* the address of the image for which a fit is being determined - should tell poisson vs. continuous image */	
	REGION *ireg;
	REGION **luts;
	int gbin; /* graphical binning size */
	int growc, gcolc, gnrow, gncol; /* subregion for graphical output */
	int ncapture, nmle;
	CONVERGENCE convergence;

	CAPTURE_FLAG cflag;
} CAPTURE; /* pragma IGNORE */	

#define thCapture_statNew thCaptureStatNew
#define thCapture_statDel thCaptureStatDel

CAPTURE_STAT *thCaptureStatNew();
void thCaptureStatDel(void *x);

CAPTURE *thCaptureNew();
void thCaptureDel(CAPTURE *capture);

RET_CODE thCaptureGetCmode(CAPTURE *capture, CAPTURE_MODE *cmode);
RET_CODE thCaptureGetLstruct(CAPTURE *capture, LSTRUCT **lstruct);
RET_CODE thCaptureGetPChainOfChain(CAPTURE *capture, CHAIN **pChainOfChain);
#if 0
RET_CODE thCaptureGetSourceRegions(CAPTURE *capture, REGION **rmodel_src, REGION **robjc_src, REGION **rsky_src, REGION **rres_src);
RET_CODE thCaptureGetRegions(CAPTURE *capture, REGION **rmodel, REGION **robjc, REGION **rsky, REGION **rres);
RET_CODE thCaptureGetCaptureStatChain(CAPTURE *capture, CHAIN **rmodelStatChain, CHAIN **rskyStatChain, CHAIN **robjcStatChain, CHAIN **rresStatChain);
#endif

RET_CODE thCaptureGetData(CAPTURE *cap, REGION **data);
RET_CODE thCaptureGetLBySector(CAPTURE *cap, CAPTURE_SECTOR sector, LSTRUCT **ldump);
RET_CODE thCaptureGetRegionBySector(CAPTURE *capture, CAPTURE_SECTOR sector, REGION **reg);

RET_CODE thCaptureGetFmtBySector(CAPTURE *cap, CAPTURE_SECTOR sector, char **fmt);
RET_CODE thCaptureGetSFileBySector(CAPTURE *cap, CAPTURE_SECTOR sector, char **file);
RET_CODE thCaptureGetRFileBySector(CAPTURE *cap, CAPTURE_SECTOR sector, char **file);
RET_CODE thCaptureGetStatChainBySector(CAPTURE *cap, CAPTURE_SECTOR sector, CHAIN **chain);

RET_CODE thCapturePutRegBySector(CAPTURE *cap, CAPTURE_SECTOR sector, REGION *reg);
RET_CODE thCaptureGetIntReg(CAPTURE *capture, REGION **ireg);

RET_CODE thCaptureGetU8FileBySector(CAPTURE *cap, CAPTURE_SECTOR sector, char **file);
RET_CODE thCaptureGetU8FmtBySector(CAPTURE *cap, CAPTURE_SECTOR sector, char **fmt);

RET_CODE thCaptureGetSubfileBySector(CAPTURE *cap, CAPTURE_SECTOR sector, char **file);
RET_CODE thCaptureGetSubStreamBySector(CAPTURE *cap, CAPTURE_SECTOR sector, FILE **fil);
RET_CODE thCapturePutSubStreamBySector(CAPTURE *cap, CAPTURE_SECTOR sector, FILE *fil);

RET_CODE thCaptureGetLUTBySector(CAPTURE *cap, CAPTURE_SECTOR sector, REGION **lut);
RET_CODE thCaptureGetGInfo(CAPTURE *cap, int *gbin, int *growc, int *gcolc, int *gnrow, int *gncol);

#endif
