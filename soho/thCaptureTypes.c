#include "thCaptureTypes.h"


CAPTURE_STAT *thCaptureStatNew() {
CAPTURE_STAT *cs = thCalloc(1, sizeof(CAPTURE_STAT));
cs->title = thCalloc(MX_STRING_LEN, sizeof(char));
return(cs);
}

void thCaptureStatDel(void *x) {
if (x == NULL) return;
CAPTURE_STAT *cs = (CAPTURE_STAT *) x;
thFree(cs->title);
thFree(cs);
return;
}


CAPTURE *thCaptureNew() {
CAPTURE *cap = thCalloc(1, sizeof(CAPTURE));

/* format strings and filenames */
int i;
cap->fmts = thCalloc(N_CAPTURE_SECTOR, sizeof(char *));
cap->sfiles = thCalloc(N_CAPTURE_SECTOR, sizeof(char *));
cap->rfiles = thCalloc(N_CAPTURE_SECTOR, sizeof(char *));
cap->u8fmts = thCalloc(N_CAPTURE_SECTOR, sizeof(char *));
cap->u8files = thCalloc(N_CAPTURE_SECTOR, sizeof(char *));
cap->subfiles = thCalloc(N_CAPTURE_SECTOR, sizeof(char *));
cap->substreams = thCalloc(N_CAPTURE_SECTOR, sizeof(FILE *));
for (i = 0; i < N_CAPTURE_SECTOR; i++) {
	cap->fmts[i] = thCalloc(MX_STRING_LEN, sizeof(char));
	cap->sfiles[i] =  thCalloc(MX_STRING_LEN, sizeof(char));
	cap->rfiles[i] =  thCalloc(MX_STRING_LEN, sizeof(char));
	cap->u8fmts[i] = thCalloc(MX_STRING_LEN, sizeof(char));
	cap->u8files[i] = thCalloc(MX_STRING_LEN, sizeof(char));
	cap->subfiles[i] = thCalloc(MX_STRING_LEN, sizeof(char));
}
cap->fpar=thCalloc(1, MX_STRING_LEN * sizeof(char));

/* regions */
cap->regs = thCalloc(N_CAPTURE_SECTOR, sizeof(REGION *));
cap->luts = thCalloc(N_CAPTURE_SECTOR, sizeof(REGION *));
for (i = 0; i < N_CAPTURE_SECTOR; i++) {
	char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", i);
	char lutname[MX_STRING_LEN];
	lutname[0] = '\0';
	sprintf(lutname, "lut region for sector = %s", sname);
	cap->luts[i] = shRegNew(lutname, (int) 1, (int) (MAX_U16 + 1), TYPE_U8);
}
/* stat chains */
cap->chains = thCalloc(N_CAPTURE_SECTOR, sizeof(CHAIN *));
for (i = 0; i < N_CAPTURE_SECTOR; i++) {
	cap->chains[i] = shChainNew("CAPTURE_STAT");
}
/* par chain */
cap->pChainOfChain=shChainNew("CHAIN");
/* source data  - flat image */
/* this should be simple pointer to the location in (lstruct) */

/* ldump's */
cap->ls = thCalloc(N_CAPTURE_SECTOR, sizeof(LSTRUCT *));
for (i = 0; i < N_CAPTURE_SECTOR; i++) {
	cap->ls[i] = thLstructNew();
}

return(cap);
}

void thCaptureDel(CAPTURE *cap) {
char *name = "thCaptureDel";
if (cap == NULL) return;

int i;
if (cap->fmts == NULL) {
	thError("%s: WARNING - null (fmt)s array - improperly allocated (capture)", name);
}
if (cap->sfiles == NULL) {
	thError("%s: WARNING - null (sfiles) array - improperly allocated (capture)", name);
}
if (cap->rfiles == NULL) {
	thError("%s: WARNING - null (rfiles) array - improperly allocated (capture)", name);
}
for (i = 0; i < N_CAPTURE_SECTOR; i++) {
	if (cap->fmts != NULL) thFree(cap->fmts[i]);
	if (cap->sfiles != NULL) thFree(cap->sfiles[i]);
	if (cap->rfiles != NULL) thFree(cap->rfiles[i]);
	if (cap->u8files != NULL) thFree(cap->u8files[i]);
	if (cap->u8fmts != NULL) thFree(cap->u8fmts[i]);
	if (cap->subfiles != NULL) thFree(cap->subfiles[i]);
	if (cap->substreams != NULL && cap->substreams[i] != NULL) fclose(cap->substreams[i]);
}
thFree(cap->fmts);
thFree(cap->sfiles);
thFree(cap->rfiles);
thFree(cap->u8fmts);
thFree(cap->u8files);
thFree(cap->subfiles);
thFree(cap->substreams);
thFree(cap->fpar);
thFree(cap->regs);
if (cap->luts != NULL) {
	for (i = 0; i < N_CAPTURE_SECTOR; i++) {
		if (cap->luts[i] != NULL) shRegDel(cap->luts[i]);
	}
	thFree(cap->luts);
}

if (cap->chains == NULL) {
	thError("%s: WARNING - null (chain)s array - improperly allocated (capture)", name);
} else {
	for (i = 0; i < N_CAPTURE_SECTOR; i++) {
		shChainDestroy(cap->chains[i], &thCaptureStatDel);
	}
	thFree(cap->chains);
}
int n = 0;
if (cap->pChainOfChain != NULL) n = shChainSize(cap->pChainOfChain);
for (i = 0; i < n; i++) {
	CHAIN *pchain = shChainElementGetByPos(cap->pChainOfChain, i);
	shChainDestroy(pchain, &thObjcDel);
}
shChainDel(cap->pChainOfChain);

if (cap->ls == NULL) {
	thError("%s: WARNIG - null (ldump)s array - improperly allocated (capture)", name);
} else {
	for (i = 0; i < N_CAPTURE_SECTOR; i++) {
		thLstructDel(cap->ls[i]);
	}
	thFree(cap->ls);
}
if (cap->ireg != NULL) shRegDel(cap->ireg);

thFree(cap);
return;
}


RET_CODE thCaptureGetCmode(CAPTURE *capture, CAPTURE_MODE *cmode) {
char *name = "thCaptureGetCmode";
if (cmode == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (capture == NULL) {
	thError("%s: ERROR - null input", name);
	*cmode = UNKNOWN_CMODE;
	return(SH_GENERIC_ERROR);
}
*cmode = capture->cmode;
return(SH_SUCCESS);
}


RET_CODE thCaptureGetLstruct(CAPTURE *capture, LSTRUCT **lstruct) {
char *name = "thCaptureGetLstruct";
if (lstruct == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (capture == NULL) {
	thError("%s: ERROR - null input", name);
	*lstruct = NULL;
	return(SH_GENERIC_ERROR);
}
*lstruct = capture->lstruct;
if (*lstruct == NULL) {
	thError("%s: WARNING - null (lstruct) - improperly allocated (capture)", name);
}
return(SH_SUCCESS);
}

RET_CODE thCaptureGetPChainOfChain(CAPTURE *capture, CHAIN **pChainOfChain) {
char *name = "thCaptureGetPChainOfChain";
if (pChainOfChain == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (capture == NULL) {
	thError("%s: ERROR - null input", name);
	*pChainOfChain = NULL;
	return(SH_GENERIC_ERROR);
}
*pChainOfChain = capture->pChainOfChain;
if (*pChainOfChain == NULL) {
	thError("%s: WARNING - null (pChainOfChain) - improperly allocated (capture)", name);
}
return(SH_SUCCESS);
}


#if 0
RET_CODE thCaptureGetSourceRegions(CAPTURE *capture, REGION **rmodel_src, REGION **robjc_src, REGION **rsky_src, REGION **rres_src) {
char *name = "thCaptureGetSourceRegions";
if (rmodel_src == NULL && rsky_src == NULL && rres_src == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (capture == NULL) {
	thError("%s: ERROR - null input", name);
	if (rmodel_src != NULL) *rmodel_src = NULL;
	if (rsky_src != NULL) *rsky_src = NULL;
	if (rres_src != NULL) *rres_src = NULL;
	return(SH_GENERIC_ERROR);
}
if (rmodel_src != NULL) {
	*rmodel_src = capture->rmodel_src;
	if (*rmodel_src == NULL) {
		thError("%s: WARNING - null (rmodel_src) - improperly allocated (capture)", name);
	}
}
if (robjc_src != NULL) {
	*robjc_src = capture->robjc_src;
	if (*robjc_src == NULL) {
		thError("%s: WARNING - null (robjc_src) - improperly allocated (capture)", name);
	}
}
if (rsky_src != NULL) {
	*rsky_src = capture->rsky_src;
	if (*rsky_src == NULL) {
		thError("%s: WARNING - null (rsky_src) - improperly allocated (capture)", name);
	}
}
if (rres_src != NULL) {
	*rres_src = capture->rres_src;
	if (*rres_src == NULL) {
		thError("%s: WARNING - null (rres_src) - improperly allocated (capture)", name);
	}
}
return(SH_SUCCESS);
}

RET_CODE thCaptureGetRegions(CAPTURE *capture, REGION **rmodel, REGION **robjc, REGION **rsky, REGION **rres) {
char *name = "thCaptureGetRegions";
if (rmodel == NULL && rsky == NULL && rres == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (capture == NULL) {
	thError("%s: ERROR - null input", name);
	if (rmodel != NULL) *rmodel = NULL;
	if (rsky != NULL) *rsky = NULL;
	if (rres != NULL) *rres = NULL;
	return(SH_GENERIC_ERROR);
}
if (rmodel != NULL) {
	*rmodel = capture->rmodel;
	if (*rmodel == NULL) {
		thError("%s: WARNING - null (rmodel) - improperly allocated (capture)", name);
	}
}
if (robjc != NULL) {
	*robjc = capture->robjc;
	if (*robjc == NULL) {
		thError("%s: WARNING - null (robjc) - improperly allocated (capture)", name);
	}
}

if (rsky != NULL) {
	*rsky = capture->rsky;
	if (*rsky == NULL) {
		thError("%s: WARNING - null (rsky) - improperly allocated (capture)", name);
	}
}
if (rres != NULL) {
	*rres = capture->rres;
	if (*rres == NULL) {
		thError("%s: WARNING - null (rres) - improperly allocated (capture)", name);
	}
}
return(SH_SUCCESS);
}

RET_CODE thCaptureGetCaptureStatChain(CAPTURE *capture, CHAIN **rmodelStatChain, CHAIN **rskyStatChain, CHAIN **robjcStatChain, CHAIN **rresStatChain) {
char *name = "thCaptureGetCaptureStatChain";
if (rmodelStatChain == NULL && rskyStatChain == NULL && robjcStatChain == NULL && rresStatChain == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (capture == NULL) {
	thError("%s: ERROR - null input", name);
	if (rmodelStatChain != NULL) *rmodelStatChain = NULL;
	if (rskyStatChain != NULL) *rskyStatChain = NULL;
	if (robjcStatChain != NULL) *robjcStatChain = NULL;
	if (rresStatChain != NULL) *rresStatChain = NULL;
	return(SH_GENERIC_ERROR);
}
if (rmodelStatChain != NULL) {
	*rmodelStatChain = capture->rmodelStatChain;
	if (*rmodelStatChain == NULL) {
		thError("%s: WARNING - null (rmodelStatChain) - improperly allocated (capture)", name);
	}
}
if (rskyStatChain != NULL) {
	*rskyStatChain = capture->rskyStatChain;
	if (*rskyStatChain == NULL) {
		thError("%s: WARNING - null (rskyStatChain) - improperly allocated (capture)", name);
	}
}
if (robjcStatChain != NULL) {
	*robjcStatChain = capture->robjcStatChain;
	if (*robjcStatChain == NULL) {
		thError("%s: WARNING - null (robjcStatChain) - improperly allocated (capture)", name);
	}
}
if (rresStatChain != NULL) {
	*rresStatChain = capture->rresStatChain;
	if (*rresStatChain == NULL) {
		thError("%s: ERROR - WARNING - null (rresStatChain) - improperly allocated (capture)", name);
	}
}
return(SH_SUCCESS);
}
#endif

RET_CODE thCaptureGetData(CAPTURE *cap, REGION **data) {
char *name = "thCaptureGetData";
if (data == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (cap == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
*data = cap->fimage;
if (*data == NULL) {
	thError("%s: WARNING - null (data) region in capture - is (capture) properly initialized?", name);
}
return(SH_SUCCESS);
}

RET_CODE thCaptureGetLBySector(CAPTURE *cap, CAPTURE_SECTOR sector, LSTRUCT **ldump) {
char *name = "thCaptureGetLBySector";
if (ldump == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (cap == NULL) {
	thError("%s: ERROR - null input", name);
	*ldump = NULL;
	return(SH_GENERIC_ERROR);
}
char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", sector); 
if (sector >= N_CAPTURE_SECTOR) {
	thError("%s: ERROR - unsupported (sector = '%s')", name, sname);
	*ldump = NULL;
	return(SH_GENERIC_ERROR);
}
LSTRUCT **ls = cap->ls;
if (ls == NULL) {
	thError("%s: ERROR - null (dump lstruct) array - improperly allocated (capture)", name);
	*ldump = NULL;
	return(SH_GENERIC_ERROR);
}
*ldump = ls[sector];
if (*ldump == NULL) {
	thError("%s: WARNING - null (ldump) - did you really mean to ask for (ldump) in (sector = '%s')?", name, sname);
}
return(SH_SUCCESS);
}

RET_CODE thCaptureGetRegionBySector(CAPTURE *capture, CAPTURE_SECTOR sector, REGION **reg) {
char *name = "thCaptureGetRegonBySector";
if (reg == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (capture == NULL) {
	thError("%s: ERROR - null input", name);
	*reg = NULL;
	return(SH_GENERIC_ERROR);
}
char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", sector); 
if (sector >= N_CAPTURE_SECTOR) {
	thError("%s: ERROR - unsupported (sector = '%s')", name, sname);
	*reg = NULL;
	return(SH_GENERIC_ERROR);
}
REGION **regs = capture->regs;
if (regs == NULL) {
	thError("%s: ERROR - null (reg) array in (capture) - improperly allocated or initialized (capture)", name);
	*reg = NULL;
	return(SH_GENERIC_ERROR);
}
*reg = regs[sector];
if (sector != CAPTURE_RES) {
	LSTRUCT *ldump = NULL;
	thCaptureGetLBySector(capture, sector, &ldump);
	thLstructGetMN(ldump, reg);
}
if (*reg == NULL) {
	thError("%s: WARNING - null (reg) for (sector = '%s') - did you mean to ask for this (region)?", name, sname);
}
return(SH_SUCCESS);
}

RET_CODE thCaptureGetFmtBySector(CAPTURE *cap, CAPTURE_SECTOR sector, char **fmt) {
char *name = "thCaptureGetFmtBySector";
if (fmt == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (cap == NULL) {
	thError("%s: ERROR - null input", name);
	*fmt = NULL;
	return(SH_GENERIC_ERROR);
}
char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", sector);
if (sector >= N_CAPTURE_SECTOR) {
	thError("%s: ERROR - (sector = '%s') not supported", name, sname);
	*fmt = NULL;
	return(SH_GENERIC_ERROR);
}
*fmt = cap->fmts[sector];
if (*fmt == NULL) {
	thError("%s: WARNING - null (fmt) string - did you really mean to get (fmt) for (sector = '%s'", name, sname);
}
return(SH_SUCCESS);
}

RET_CODE thCaptureGetSFileBySector(CAPTURE *cap, CAPTURE_SECTOR sector, char **file) {
char *name = "thCaptureGetSFileBySector";
if (file == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (cap == NULL) {
	thError("%s: ERROR - null input", name);
	*file = NULL;
	return(SH_GENERIC_ERROR);
}
char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", sector);
if (sector >= N_CAPTURE_SECTOR) {
	thError("%s: ERROR - unsupported (sector = '%s')", name, sname);
	*file = NULL;
	return(SH_GENERIC_ERROR);
}
*file = cap->sfiles[sector];
if (*file == NULL) {
	thError("%s: WARNING - null (sfile) for (sector = '%s') - improperly allocated (capture)", name, sname);
}
return(SH_SUCCESS);
}

RET_CODE thCaptureGetRFileBySector(CAPTURE *cap, CAPTURE_SECTOR sector, char **file) {
char *name = "thCaptureGetRFileBySector";
if (file == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (cap == NULL) {
	thError("%s: ERROR - null input", name);
	*file = NULL;
	return(SH_GENERIC_ERROR);
}
char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", sector);
if (sector >= N_CAPTURE_SECTOR) {
	thError("%s: ERROR - unsupported (sector = '%s')", name, sname);
	*file = NULL;
	return(SH_GENERIC_ERROR);
}
*file = cap->rfiles[sector];
if (*file == NULL) {
	thError("%s: WARNING - null (rfile) for (sector = '%s') - improperly allocated (capture)", name, sname);
}
return(SH_SUCCESS);
}

RET_CODE thCaptureGetStatChainBySector(CAPTURE *cap, CAPTURE_SECTOR sector, CHAIN **chain) {
char *name = "thCaptureGetStatChainBySector";
if (chain == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (cap == NULL) {
	thError("%s: ERROR - null input", name);
	*chain = NULL;
	return(SH_GENERIC_ERROR);
}
char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", sector);
if (sector >= N_CAPTURE_SECTOR) {
	thError("%s: ERROR - unsupported (sector = '%s')", name, sname);
	*chain = NULL;
	return(SH_GENERIC_ERROR);
}
CHAIN **chains = cap->chains;
if (chains == NULL) {
	thError("%s: ERROR - null (chains) array - improperly allocated (capture)", name);
	*chain = NULL;
	return(SH_GENERIC_ERROR);
}
*chain = chains[sector];
if (*chain == NULL) {
	thError("%s: WARNING - null (chain) for (sector = '%s') - improperly allocated (capture)", name, sname);
}
return(SH_SUCCESS);
}

RET_CODE thCapturePutRegBySector(CAPTURE *cap, CAPTURE_SECTOR sector, REGION *reg) {
char *name = "thCapturePutRegBySector";
if (cap == NULL) {
	thError("%s: ERROR - null input (capture)", name);
	return(SH_GENERIC_ERROR);
}
char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", sector);
if (sector >= N_CAPTURE_SECTOR) {
	thError("%s: ERROR - unsupported (sector = '%s')", name, sname);
	return(SH_GENERIC_ERROR);
}
if (cap->regs == NULL) {
	thError("%s: ERROR - null (reg)s array - improperly allocated (capture)", name);
	return(SH_GENERIC_ERROR);
}
if (reg == cap->regs[sector]) return(SH_SUCCESS);
if (reg == NULL) thError("%s: WARNING - putting null (reg) in place for (sector = '%s') in (capture)", name, sname);
if (sector == CAPTURE_RES && cap->regs[sector] != NULL) {
	thError("%s: WARNING - deleting previous (reg) for (sector = '%s') in (capture)", name, sname);
	shRegDel(cap->regs[sector]);
	cap->regs[sector] = NULL;
} else if (sector != CAPTURE_RES && cap->regs[sector] != NULL) {
	thError("%s: WARNING - a non-null (reg) was found for (sector = '%s') - user should take control of memory", name, sname);
}
cap->regs[sector] = reg;
return(SH_SUCCESS);
}

RET_CODE thCaptureGetIntReg(CAPTURE *capture, REGION **ireg) {
char *name = "thCaptureGetIntReg";
if (ireg == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (capture == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
*ireg = capture->ireg;
if (*ireg == NULL) {
	thError("%s: WARNING - null (ireg) in (capture)", name);
}
return(SH_SUCCESS);
}

RET_CODE thCaptureGetU8FileBySector(CAPTURE *cap, CAPTURE_SECTOR sector, char **file) {
char *name = "thCaptureGetU8FileBySector";
if (file == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (cap == NULL) {
	thError("%s: ERROR - null input", name);
	*file = NULL;
	return(SH_GENERIC_ERROR);
}
char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", sector);
if (sector >= N_CAPTURE_SECTOR) {
	thError("%s: ERROR - unsupported (sector = '%s')", name, sname);
	*file = NULL;
	return(SH_GENERIC_ERROR);
}
if (cap->u8files == NULL) {
	thError("%s: ERROR - improperly allocated (capture)", name);
	return(SH_GENERIC_ERROR);
}
*file = cap->u8files[sector];
if (*file == NULL) {
	thError("%s: WARNING - null (u8file) for (sector = '%s') - improperly allocated (capture)", name, sname);
}
return(SH_SUCCESS);
}

RET_CODE thCaptureGetU8FmtBySector(CAPTURE *cap, CAPTURE_SECTOR sector, char **fmt) {
char *name = "thCaptureGetU8FmtBySector";
if (fmt == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (cap == NULL) {
	thError("%s: ERROR - null input", name);
	*fmt = NULL;
	return(SH_GENERIC_ERROR);
}
char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", sector);
if (sector >= N_CAPTURE_SECTOR) {
	thError("%s: ERROR - unsupported (sector = '%s')", name, sname);
	*fmt = NULL;
	return(SH_GENERIC_ERROR);
}
if (cap->u8fmts == NULL) {
	thError("%s: ERROR - improperly allocated (capture)", name);
	*fmt = NULL;
	return(SH_GENERIC_ERROR);
}
*fmt = cap->u8fmts[sector];
if (*fmt == NULL) {
	thError("%s: WARNING - null (u8file) for (sector = '%s') - improperly allocated (capture)", name, sname);
}
return(SH_SUCCESS);
}

RET_CODE thCaptureGetSubfileBySector(CAPTURE *cap, CAPTURE_SECTOR sector, char **file) {
char *name = "thCaptureGetSubfileBySector";
if (file == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (cap == NULL) {
	thError("%s: ERROR - null input", name);
	*file = NULL;
	return(SH_GENERIC_ERROR);
}
char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", sector);
if (sector >= N_CAPTURE_SECTOR) {
	thError("%s: ERROR - unsupported (sector = '%s')", name, sname);
	*file = NULL;
	return(SH_GENERIC_ERROR);
}
if (cap->subfiles == NULL) {
	thError("%s: ERROR - improperly allocated (capture)", name);
	*file = NULL;
	return(SH_GENERIC_ERROR);
}
*file = cap->subfiles[sector];
if (*file == NULL) {
	thError("%s: WARNING - null (u8file) for (sector = '%s') - improperly allocated (capture)", name, sname);
}
return(SH_SUCCESS);
}

RET_CODE thCaptureGetSubStreamBySector(CAPTURE *cap, CAPTURE_SECTOR sector, FILE **fil) {
char *name = "thCaptureGetSubStreamBySector";
if (fil == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (cap == NULL) {
	thError("%s: ERROR - null input", name);
	*fil = NULL;
	return(SH_GENERIC_ERROR);
}
char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", sector);
if (sector >= N_CAPTURE_SECTOR) {
	thError("%s: ERROR - unsupported (sector = '%s')", name, sname);
	*fil = NULL;
	return(SH_GENERIC_ERROR);
}
if (cap->substreams == NULL) {
	thError("%s: ERROR - improperly allocated (capture)", name);
	*fil = NULL;
	return(SH_GENERIC_ERROR);
}
*fil = cap->substreams[sector];
return(SH_SUCCESS);
}

RET_CODE thCapturePutSubStreamBySector(CAPTURE *cap, CAPTURE_SECTOR sector, FILE *fil) {
char *name = "thCapturePutSubStreamBySector";
if (cap == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", sector);
if (sector >= N_CAPTURE_SECTOR) {
	thError("%s: ERROR - unsupported (sector = '%s')", name, sname);
	return(SH_GENERIC_ERROR);
}
if (cap->substreams == NULL) {
	thError("%s: ERROR - improperly allocated (capture)", name);
	return(SH_GENERIC_ERROR);
}
cap->substreams[sector] = fil;
return(SH_SUCCESS);
}

RET_CODE thCaptureGetLUTBySector(CAPTURE *cap, CAPTURE_SECTOR sector, REGION **lut) {
char *name = "thCaptureGetLUTBySector";
if (lut == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (cap == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_SUCCESS);
}
char *sname = shEnumNameGetFromValue("CAPTURE_SECTOR", sector);
if (sector >= N_CAPTURE_SECTOR) {
	thError("%s: ERROR - unsupported (sector = '%s')", name, sname);
	*lut = NULL;
	return(SH_GENERIC_ERROR);
}
if (cap->luts == NULL) {
	thError("%s: ERROR - improperly allocated (capture)", name);
	*lut = NULL;
	return(SH_GENERIC_ERROR);
}
*lut = cap->luts[sector];
if (*lut == NULL) {
	thError("%s: WARNING - null (lut) found in (capture) for (sector = '%s')", name, sname);
}
return(SH_SUCCESS);
}


RET_CODE thCaptureGetGInfo(CAPTURE *cap, int *gbin, int *growc, int *gcolc, int *gnrow, int *gncol) {
char *name = "thCaptureGetGInfo";
if (cap == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (gbin != NULL) *gbin = cap->gbin;
if (growc != NULL) *growc = cap->growc;
if (gcolc != NULL) *gcolc = cap->gcolc;
if (gncol != NULL) *gncol = cap->gncol;
if (gnrow != NULL) *gnrow = cap->gnrow;
return(SH_SUCCESS);
}


