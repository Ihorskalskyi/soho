 
#include "thJobs.h"

void thProcDoSky (PROCESS *proc) {

  char *name = "thProcDoSky";

  char *fname = proc->files->thframefile;

  FILE *fil;
  fil = fopen(fname, "r");
  
  CHAIN *fchain;
  fchain = thAsciiChainRead(fil);

  int i, nframe;
  nframe = shChainSize(fchain);
  
  if (nframe == 0) {
    thError("%s: no structures was properly read from the par file", 
		   name);
    return;
  } else {
    printf("%s: number of frames - %d \n", name, nframe);
  }  
  
  FRAME *frame;

  FRAMEFILES *ff;
  for (i = 0; i < nframe; i++) {
   
    printf("%s: working with frame (%d) \n", name, i);
    
    ff = (FRAMEFILES *) shChainElementGetByPos(fchain, i);

    if (ff == NULL) {
      thError("%s: NULL returned from chain - check source code", name);
    } 
  
    frame = thFrameNew();
    if (frame->files != NULL) {
      thFramefilesDel(frame->files);
    }
    
    frame->files = ff;
    frame->proc = proc;
      
    printf("%s: loading fpC file\n", name);
    thFrameLoadImage(frame);
    printf("%s: loading amplifier information\n", name);
    thFrameLoadAmpl(frame); 
    printf("%s: loading PHOTO masks\n", name);
    thFrameLoadPhMask(frame);
    printf("%s: loading OBJC's \n", name);
    thFrameLoadPhObjc(frame);
    printf("%s: loading PHOTO psf \n", name);
    thFrameLoadPhPsf(frame);
    printf("%s: loading sky basis set \n", name);
    thFrameLoadSkybasisset(frame);
    printf("%s: loading poisson weights \n", name);
    thFrameLoadWeight(frame);
    printf("%s: loading SOHO masks \n", name);
    thFrameLoadThMask(frame);
    printf("%s: fitting sky \n", name);
    thFrameFitSky(frame);
    printf("%s: dumping sky model \n", name);
    thFrameDumpSkymodel(frame);
    
    /* deleting to free unused memory */
    /* this is likely to cause memory error */
    /*
    thFrameDel(frame);
    */
  }

  return;

}
  
