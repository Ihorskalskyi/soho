#include "dervish.h"
#include "sohoEnv.h"

REGION *thRegReadFpcAll(const char *file,   
		     RET_CODE *thStatus);
REGION *thRegReadFpc(const char *file,  
		     const int row0,  const int column0,  
		     const int row1,  const int column1, 
		     RET_CODE *thStatus);

RET_CODE thWriteFpc(const char *file, 
		    const REGION *reg);

RET_CODE thCpFpc(const char *infile, const char *outfile, /* input and output filenames */
		       const int row0, const int column0, 
		       const int row1, const int column1 /* corners of the image in INFILE */);
