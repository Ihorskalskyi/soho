#include "thConsts.h"
#include "thFuncmodelIo.h"

const char* const FMHDU_HDR_KEYWORD = "FMHDU";
const char* const FMIMAGE_HDR_KEYWORD = "FMIMAGE";
const int  FMHDU_HDR_VAL = 2;
const int  FMIMAGE_HDR_VAL = 1;
const char* const FMHDU_HDR_COMMENT = "STORAGE EXTENSION FOR FMHDU";
const char* const FMIMAGE_HDR_COMMENT = "STORAGE EXTENSION FOR FUNC VALUES";
#if !IO_COMPRESSED
const char* const IOREGION = "REGION";
#else
const char *const IOREGION = "EXTREGION";
#endif

/* gets the related row number in the table of par-elements */
int thGetIndexFromPardef (PARELEM *pe, PARDEF *pd, RET_CODE *status) {

char *name = "thGetIndexFromPardef";

if (pe == NULL || pd == NULL) {
	thError("%s: ERROR - null input", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(-1);
	}

if (pe->ptype != pd->ptype) {
	thError("%s: ERROR - mistamtching type in pardef and parelem", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(-1);
	}

if (pe->ptype == UNKNOWN_PARTYPE) {
	thError("%s: ERROR - uknown partype", name);	
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(-1);
	}

if (pe->ptype == INTEGER_PARTYPE) {
	int index;	
	/* this function should be written */
	index = thGetIndexIntPardiv(*(int*)pd->pval, pe->pdiv, pe->npdiv, pe->divtype, status);
	if (status != NULL && *status != SH_SUCCESS) {
		thError("%s: ERROR - could not find index in division", name);
	}
	return(index);
	}

if (pe->ptype == PIX_PARTYPE) {
	int index;
	/* this function should be written */
	index = thGetIndexThpixPardiv(*(THPIX*)pd->pval, pe->pdiv, pe->npdiv, pe->divtype, status);
	if (status != NULL && *status != SH_SUCCESS) {
		thError("%s: ERROR - could not find index in division", name);
	}
	return(index);
	}

thError("%s: ERROR - unsupported partype", name);
if (status != NULL) *status = SH_GENERIC_ERROR;
return(-1);
}

int thGetFitsRowFromParlist (PARTABLE *pt, PARLIST *pl, RET_CODE *status) {

char *name = "thGetFitsRowFromParlist";

if (pt == NULL || pl == NULL) {
	thError("%s: ERROR - null input", name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(-1);
	}

int nt;
nt = shChainSize(pt->table);
int *tmaplist;
tmaplist = thCalloc(nt, sizeof(int)); 

int corr;
/* this function should be written */
corr = thCorrPartableParlist(pt, pl, tmaplist, status);

if (corr != 0 || (status != NULL && *status != SH_SUCCESS)) {
	thError("%s: member mismatch between partable and parlist", name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(-1);
	}


int i, *divindex, *npdiv;
divindex = thCalloc(nt, sizeof(int));
npdiv = thCalloc(nt, sizeof(int));
PARELEM *pe;
PARDEF *pd;
for (i = 0; i < nt; i++) {
	pe = shChainElementGetByPos(pt->table, i);
	pd = shChainElementGetByPos(pl->pars, tmaplist[i]);
	/* this function should be written */
	divindex[i] = thGetDivindexFromPardef(pe, pd, status);
	npdiv[i] = pe->npdiv;
	if (divindex[i] < 0 || (status != NULL && *status != SH_SUCCESS)) {
		thError("%s: the given parameter was not in the table range", name);
		thInsertStatus(status, SH_GENERIC_ERROR);
		thFree(index);
		thFree(tmaplist);
		return(-1);
	}	
}

int row;  
/* this function should be written */
row = thGetRowFromDivindex(divindex, npdiv, nt, status);
if (row < 0 || (status != NULL && *status != SH_SUCCESS)) {
	thError("%s: could not find the fits file row for the given index list", name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	}

thFree(index);
thFree(tmaplist);
return(row);

}

RET_CODE thGetParlistFromPartable (PARTABLE *pt, int row, PARLIST *pl) {

char *name = "thGetParlistFromPartable";

if (pt == NULL || pl == NULL) {
	thError("%s: null placeholer input or output", name);
	return(SH_GENERIC_ERROR);
	}	

if (row < 0) {
	thError("%s: unacceptable (negative) row number", name);
	return(SH_GENERIC_ERROR);
	}

RET_CODE status;
int nt, *npdiv, *divindex;
/* this function should be written */
npdiv = thGetNpardivFromPartable(pt, &nt, &status);
/* this function should be written */
divindex = thGetDivindexFromRow(row, npdiv, nt, &status);
int maxrow;
maxrow = thGetMaxrowFromNpardiv(npdiv, nt);
if (row >= maxrow) {
	thError("%s: unacceptable row number, row too big", name);
	return(SH_GENERIC_ERROR);
	}
thFree(npdiv);

int np, *pindex;
/* this function should be written */
pindex = thGetPartableIndexFromParlist(pt, pl, &np, &status);
if (status != SH_SUCCESS) {
	thError("%s: could not specify par element index for parlist", name);
	return(status);
	}
PARELEM *pe;
PARDEF *pd;

int i;
for (i = 0; i < np; i++) {
	pd = shChainElementGetByPos(pl->pars, i);
	pe = shChainElementGetByPos(pt->table, pindex[i]);
	/* should be written */
	status = thGetPardefFromParelem(pe, divindex[pindex[i]], pd);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not generate pardef from parelem", name);
		thFree(pindex); thFree(divindex);
		return(status);
	}
}

return(SH_SUCCESS);
}

RET_CODE thGetPardefFromPartable(PARTABLE *pt, int row, PARDEF *pd) {
	
char *name = "thGetParlistFromPardef";

if (pt == NULL || pd == NULL) {
	thError("%s: null placeholer input or output", name);
	return(SH_GENERIC_ERROR);
	}	

if (row < 0) {
	thError("%s: unacceptable (negative) row number", name);
	return(SH_GENERIC_ERROR);
	}

RET_CODE status;
int nt, *npdiv, *divindex;
/* this function should be written */
npdiv = thGetNpardivFromPartable(pt, &nt, &status);
/* this function should be written */
divindex = thGetDivindexFromRow(row, npdiv, nt, &status);

/* checking the upper bound of row */
int maxrow;
maxrow = thGetMaxrowFromNpardiv(npdiv, nt);
if (row >= maxrow) {
	thError("%s: unacceptable row number, row too big", name);
	return(SH_GENERIC_ERROR);
	}
thFree(npdiv);

/* now deciding which element in table belongs to that specific pardef */

/* this function should be written */
int pindex;
pindex = thGetPartableIndexFromPardef(pt, pd, &status);
if (status != SH_SUCCESS) {
	thError("%s: could not specify par element index for parlist", name);
	return(status);
	}
PARELEM *pe;

/* now fetching the specific parelement in partable which belongs to pardef */
pe = shChainElementGetByPos(pt->table, pindex);
/* this function should be written */
status = thGetPardefFromParelem(pe, divindex[pindex], pd);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not generate pardef from parelem", name);
	thFree(index);
	return(status);
}


return(SH_SUCCESS);
}

RET_CODE thFmhduReadFits(PHFITSFILE *fits, CHAIN *fmhdu) {

char *name = "thFmhduReadFits";

if (fits == NULL || fmhdu == NULL) {
	thError("%s: ERROR - null input or output", name);
	return(SH_GENERIC_ERROR);
	}

int phstatus, nfitsrow, quite = 0;
phstatus = phFitsBinTblHdrRead(fits, "FMHDU", NULL, &nfitsrow, quite);

int i;
FMHDU *hdu;

for (i = 0; i < nfitsrow; i++) {
	hdu = thFmhduNew();
	if (phFitsBinTblRowRead(fits, hdu) < 0) {
		phFitsBinTblEnd(fits);
		thError("%s: ERROR - problem writing element (%d) of the chain", name, i);	
		return(SH_GENERIC_ERROR);
	}
	shChainElementAddByPos(fmhdu, hdu, "FMHDU", TAIL, AFTER);
	}

phFitsBinTblEnd(fits);

return(SH_SUCCESS);
}

RET_CODE thFmhduWriteFits(PHFITSFILE *fits, CHAIN *fmhdu) {

char *name = "thFmhduWriteFits";
if (fits == NULL || fmhdu == NULL) {
	thError("%s: ERROR - null input or output", name);
	return(SH_GENERIC_ERROR);
	}

/* this empties the possibly existing hdu and fills it back */

phFitsBinTblHdrWrite(fits, "FMHDU", NULL);
if (phFitsBinTblChainWrite(fits, fmhdu) < 0) {
	phFitsBinTblEnd(fits);
	thError("%s: ERROR - problem writing chain to file", name);
	return(SH_GENERIC_ERROR);
	}

phFitsBinTblEnd(fits);
return(SH_SUCCESS);
}

RET_CODE thPartableReadFits(PHFITSFILE *fits, PARTABLE *pt) {
 char *name = "thPartableReadFits";
 if (fits == NULL || pt == NULL) {
	thError("%s: ERROR - null input or output");
	return(SH_GENERIC_ERROR);
	}

if (pt->table == NULL) pt->table = shChainNew("PARELEMIO");
if (shChainSize(pt->table) != 0) {
	thError("%s: ERROR - the chain already allocated", name);
	return(SH_GENERIC_ERROR);
	}

int phstatus, nfitsrow, quite = 0;
phstatus = phFitsBinTblHdrRead(fits, "PARELEMIO", NULL, &nfitsrow, quite);

int i;
PARELEMIO *peio;
PARELEM *pe;

peio = thParelemioNew();
for (i = 0; i < nfitsrow; i++) {
	if (phFitsBinTblRowRead(fits, peio) < 0) {
		phFitsBinTblEnd(fits);
		thError("%s: ERROR - problem reading element (%d) of the chain", name, i);	
		return(SH_GENERIC_ERROR);
	}
	pe = thParelemConvertParelemio(peio);
	shChainElementAddByPos(pt->table, pe, "PARELEM", TAIL, AFTER);
	}

thParelemioDel(peio);
phFitsBinTblEnd(fits);

return(SH_SUCCESS);
}

RET_CODE thPartableWriteFits(PHFITSFILE *fits, PARTABLE *pt) {

char *name = "thPartableWriteFits";
if (fits == NULL || pt == NULL || pt->table == NULL) {
	thError("%s: ERROR - null input or output", name);
	return(SH_GENERIC_ERROR);
	}

/* this empties the possibly existing hdu and fills it back */

PARTABLE *ptio;
ptio = thPartableioConvertPartable(pt);

phFitsBinTblHdrWrite(fits, "PARELEMIO", NULL);
if (phFitsBinTblChainWrite(fits, ptio->table) < 0) {
	phFitsBinTblEnd(fits);
	thPartableioDestroy(ptio);
	thError("%s: ERROR - problem writing chain to file", name);
	return(SH_GENERIC_ERROR);
	}

thPartableioDestroy(ptio);
phFitsBinTblEnd(fits);
return(SH_SUCCESS);
}


RET_CODE thImageReadFitsFromRow(PHFITSFILE *fits, REGION *reg, int fitsrow, int *wfitsrow) {

char *name = "thImageReadFitsFromRow";

if (fits == NULL || wfitsrow == NULL || reg == NULL || fitsrow < 0) {
	thError("%s: ERROR - null or problematic input or output", name);
	return(SH_GENERIC_ERROR);
	}

/* check see if the file is already open */

int totrow, quiet = 0;

if (*wfitsrow < 0) {
	phFitsBinTblHdrRead(fits, IOREGION, NULL, &totrow, quiet);
	*wfitsrow = 0;
}

if (fitsrow >= totrow) {
	thError("%s: ERROR - row number (%d) too large (total rows = %d)", name, fitsrow, totrow);
	return(SH_GENERIC_ERROR);
	}

int i;
if (fitsrow > *wfitsrow) {
	for (i = 0; i < fitsrow - *wfitsrow; i++) {
		phFitsBinTblRowUnread(fits);
		}
} else {
	for (i = 0; i < *wfitsrow - fitsrow; i++) {
		phFitsBinTblRowRead(fits, NULL);
	}
}
*wfitsrow = fitsrow;

#if !IO_COMPRESSED
	phFitsBinTblRowRead(fits, reg);
	(*wfitsrow)++;
	return(SH_SUCCESS);
#else 
        EXTREGION *extreg;
	extreg = thExtregionNew("", 0, 0, THIOPIXTYPE);
	phFitsBinTblRowRead(fits, extreg);
	(*wfitsrow)++;
	if (reg->nrow == 0 || reg->ncol == 0) {
		reg->rows_thpix = thCalloc(extreg->reg->nrow, sizeof(THPIX));
		for (i = 0; i < extreg->reg->nrow; i++) {
			reg->rows_thpix[i] = thCalloc(extreg->reg->ncol, sizeof(THPIX));
		}
	}
	reg->nrow = extreg->reg->nrow;
	reg->ncol = extreg->reg->ncol;
	if (reg->nrow < extreg->reg->nrow || reg->ncol < extreg->reg->ncol) {
		thError("%s: ERROR - space allocated in region is smaller than the one saved on file", name);
		return(SH_GENERIC_ERROR);
	}
	if (extreg->conversion) {
		thRegConvertPixType(extreg->reg, reg, extreg->bzero, extreg->bscale);
	} else {
		shRegPixCopy(extreg->reg, reg);
	}
	reg->row0 = extreg->reg->row0;
	reg->col0 = extreg->reg->col0;
	/* zeroing pixels that lie outside the saved portion */	
	THPIX *rows;
	int j;
	for (i = extreg->reg->nrow; i < reg->nrow; i++) {
		rows = reg->rows_thpix[i];
		for (j = 0; j < reg->ncol; j++) {
			rows[j] = (THPIX) 0.0;
		}
	}
	for (i = 0; i < extreg->reg->nrow; i++) {
		rows = reg->rows_thpix[i];
		for (j = extreg->reg->ncol; j < reg->ncol; j++) {
			rows[j] = (THPIX) 0.0;
		}
	}
	/* now getting rid of extreg */
	thExtregionDel(extreg);
	return(SH_SUCCESS);
#endif

thError("%s: ERROR - source code problem", name);
return(SH_GENERIC_ERROR);
}
	
RET_CODE thImageReadFitsFromParlist(PHFITSFILE *fits, REGION *reg, PARTABLE *partable, PARLIST *parlist, int *wfitsrow) {
char *name = "thImageWriteFitsInParlist";

if (fits == NULL || partable == NULL || partable->table == NULL || parlist == NULL || parlist->pars == NULL || reg == NULL || wfitsrow == NULL) {
	thError("%s: ERROR - null input or output", name);
	return(SH_GENERIC_ERROR);
}

int fitsrow;
RET_CODE status;
fitsrow = thGetFitsRowFromParlist (partable, parlist, &status);

if (fitsrow < 0 || status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the proper fits row from par list and partable", name);
	return(SH_GENERIC_ERROR);
	}

status = thImageReadFitsFromRow(fits, reg, fitsrow, wfitsrow);
return(status);
}

RET_CODE thFuncmodelReadFits(PHFITSFILE *fits, PARTABLE *partable, FUNCMODEL *funcmodel, FMHDU *fmhdu, int *wfitsrow) {

char *name = "thFuncmodelReadFits";

if (fits == NULL || partable == NULL || funcmodel == NULL || funcmodel->parlist == NULL || 
	fmhdu == NULL || fmhdu->ncomp <= 0 || fmhdu->compindex == NULL ||  wfitsrow == NULL ) {
	thError("%s: ERROR - null input or output", name);
	return(SH_GENERIC_ERROR);
	}
RET_CODE status;
int i, fitsrow;

fitsrow = thGetFitsRowFromParlist (partable, funcmodel->parlist, &status);

REGION **regs;
if (funcmodel->regs == NULL) {
	funcmodel->regs == (REGION **) thCalloc(N_FUNCMODEL_COMP_INDEX, sizeof(REGION*));
}
regs = funcmodel->regs;

for (i = 0; i < fmhdu->ncomp; i++) {

if (fmhdu->compindex[i] < 0) {
	thError("%s: ERROR - problem indexing the funcmodel components at %d", name, i);
	return(SH_GENERIC_ERROR);
	}
if (regs[fmhdu->compindex[i]] == NULL) {
	regs[fmhdu->compindex[i]] = shRegNew("", 0, 0, TYPE_THPIX);
}
status = thImageReadFitsFromRow(fits, regs[fmhdu->compindex[i]], fitsrow, wfitsrow);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not read image from parlist", name);
	return(status);
	}
fitsrow++; 
}

return(status);
}

RET_CODE thImageWriteFitsInRow(PHFITSFILE *fits, REGION *reg, int *wfitsrow) {
char *name = "thImageWriteFitsInRow";

#if IO_COMPRESSED

static int init = 0;
static REGION *wioreg = NULL;


if (init && fits == NULL && reg == NULL && wfitsrow == NULL) {
	shRegDel(wioreg);
	wioreg = NULL;
	init = 0;
	return(SH_SUCCESS);
}
#endif

if (fits == NULL || reg == NULL) {
	thError("%s: ERROR - null input or output", name);
	return(SH_GENERIC_ERROR);
	}

#if !IO_COMPRESSED 
if (phFitsBinTblRowWrite(fits, reg) < 0) {
	thError("%s: ERROR - could not write the image to file", name);
	return(SH_GENERIC_ERROR);
	}
#else
	
if (!init) {
	wioreg = shRegNew(name, MAXIONROW, MAXIONCOL, THIOPIXTYPE);
	init = 1;
	}
EXTREGION *extreg;
extreg = thExtregionNew("", 0, 0, THIOPIXTYPE);
shRegDel(extreg->reg);
extreg->reg = shSubRegNew(reg->name, wioreg, reg->nrow, reg->ncol, 0, 0, NO_FLAGS);
thRegTransExtreg(reg, extreg);
extreg->reg->row0 = reg->row0;
extreg->reg->col0 = reg->col0;
if (phFitsBinTblRowWrite(fits, extreg) < 0) {
	thError("%s: ERROR - could not write the extended region to file", name);
	return(SH_GENERIC_ERROR);
	}
thExtregionDel(extreg);
#endif
(*wfitsrow)++;
return(SH_SUCCESS);
}

RET_CODE thFuncmodelWriteFits(PHFITSFILE *fits, FUNCMODEL *funcmodel, int *wfitsrow) {

char *name = "thFuncmodelWriteFits";
if (fits == NULL ||  funcmodel == NULL || funcmodel->ncomp <= 0 || funcmodel->compindex == NULL ||
	wfitsrow == NULL) {
	thError("%s: ERROR - null input or output", name);
	return(SH_GENERIC_ERROR);
}

REGION **regs;
if (funcmodel->regs == NULL) {
	funcmodel->regs = (REGION **) thCalloc(funcmodel->ncomp, sizeof(REGION*));
	}
regs = funcmodel->regs;

RET_CODE status;
int i, j;

for (i = 0; i < funcmodel->ncomp; i++) {

	j = funcmodel->compindex[i];
	if (j < 0) {
		thError("%s: ERROR - detected on component %d (index = %d)", name, i, j);
		return(SH_GENERIC_ERROR);
	}
	if (regs[j] == NULL) {
		regs[j] == shRegNew("", 0, 0, TYPE_THPIX);
		thError("%s: WARNING - outputting null image for %d (index = %d)", name, i, j);
	}
	status = thImageWriteFitsInRow(fits, regs[j], wfitsrow);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not write component %d (index = %d) of the funcmodel", name, i, j);
		return(status);
	}
}

return(status);
}

FMFITSFILE *thFmfitsOpen(char *filename, int mode, HDR *hdr, RET_CODE *status) {
	
char *name = "thFmfitsOpen";
if (filename == NULL) {
	thError("%s: ERROR - null filename", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(NULL);
}

/* does not support append mode */
if (mode > 1 || mode < 0) {
	thError("%s: ERROR - unsupported IO mode", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(NULL);
}

PHFITSFILE *fits;
HDR *hdr1;
RET_CODE status1;
int i, fmhdu_loc;

/* place holder for special keywords, the difference is that the memory is allocated 
with dervish */
char *key, *comment;
key = thCalloc(SIZE, sizeof(char));
comment = thCalloc(SIZE, sizeof(char));

/* if the mode is READ then 
   1. open the file as read 
   2. input FMHDUs and PARTABLEs
   3. go to the first row of the image hdu
*/

if (mode == 0) {

fits = phFitsBinTblOpen(filename, mode, hdr1);

strcpy(key, FMHDU_HDR_KEYWORD);
status1 = shHdrGetInt(hdr1, key, &fmhdu_loc);

if (status1 != SH_SUCCESS) {
	fmhdu_loc = FMHDU_HDR_VAL;
	thError("%s: WARNING - could not locate FMHDU location keyword in th header, will check hdu %d", name, fmhdu_loc);
	} 

int image_loc;
strcpy(key, FMIMAGE_HDR_KEYWORD);
status1 = shHdrGetInt(hdr1, key, &image_loc);

if (status1 != SH_SUCCESS) {
	image_loc = FMIMAGE_HDR_VAL;
	thError("%s: WARNING - could not locate IMAGE location keyword in the header, will check the hdu %d", name, image_loc);
	}

if (image_loc <= 0 || fmhdu_loc <= 0 || image_loc == fmhdu_loc) {
	thError("%s: ERROR - impossible values for FMHDU and IMAGE extensions (%d, %d)", name, fmhdu_loc, image_loc);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(NULL);
	}

for (i = 1; i < fmhdu_loc; i++) {
	phFitsBinTblHdrRead(fits, NULL, NULL, NULL, 0);	
	phFitsBinTblEnd(fits);
}

CHAIN *fmhdu;
fmhdu = shChainNew("FMHDU");
status1 = thFmhduReadFits(fits, fmhdu);
if (status1 != SH_SUCCESS) {
	thError("%s: ERROR - could not read FMHDUs from extension %d", name, fmhdu_loc);
	thInsertStatus(status, status1);
	return(NULL);
	}

/* now that you have read FMHDUs, go to each HDU and read the partables */

/* first order HDUs in terms of their extension number
Note that the intrinsic order doesn't need to be kept since FMHDUs naturally contain a FIRSTROW keywrod that 
tells the location of the function image in the image extension.  
it denotes the order in which value images are put in the image extension */

int *order;
order = thOrderFmhdu(fmhdu, &status1);

if (status1 != SH_SUCCESS) {
	thError("%s: could not order FMHDUs based on extension number", name);
	thInsertStatus(status, status1);
	return(NULL);
}

int first_pt_loc, current_loc, next_loc, j;
first_pt_loc = ((FMHDU *)shChainElementGetByPos(fmhdu, order[0]))->pthdu;

if (first_pt_loc <= fmhdu_loc) {
	phFitsBinTblClose(fits);
	phFitsBinTblOpen(filename, mode, NULL);
	current_loc = 1;
} else {
	current_loc = fmhdu_loc + 1;
}

PARTABLE *pt;
CHAIN *pt_chain;
pt_chain = shChainNew("PARTABLE");

for (i = 0; i < shChainSize(fmhdu); i++) {
	next_loc = ((FMHDU *) shChainElementGetByPos(fmhdu, order[i]))->pthdu;
	for (j = current_loc; j < next_loc; j++) {
		phFitsBinTblHdrRead(fits, NULL, NULL, NULL, 0);
		current_loc++;
	}
	pt = thPartableNew();
	status1 = thPartableReadFits(fits, pt);
	if (status1 != SH_SUCCESS) {
		thError("%s: ERROR reading PARTABLE from hdu %d", name, current_loc);
		thInsertStatus(status, status1);
	}
	current_loc++;
	shChainElementAddByPos(pt_chain, pt, "PARTABLE", TAIL, AFTER);
	}

/* now re order partable to conform with the given order */

thChainReorder(pt_chain, order);

/* now go to the image location */

if (image_loc < current_loc) {
	phFitsBinTblClose(fits);
	fits = phFitsBinTblOpen(filename, mode, NULL);
	current_loc = 1;
}
for (i = current_loc; i < image_loc; i++) {
	if  (phFitsBinTblHdrRead(fits, NULL, NULL, NULL, 0) < 0) {
		thError("%s: ERROR - could not pass beyond extension %d", name, i);
		phFitsBinTblClose(fits);
		thInsertStatus(status, SH_GENERIC_ERROR);
		return(NULL);
	}
	phFitsBinTblEnd(fits);
	current_loc++;
}
	
/* now open the image table */
int nfitsrow, quiet = 0;
if (phFitsBinTblHdrRead(fits, IOREGION, NULL, &nfitsrow, quiet) < 0) {
	thError("%s: ERROR - could not open the image extension %d", name, image_loc);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(NULL);
	}
	
/* now insert everything in the fmfitsfile holder */

FMFITSFILE *f;
f = thFmfitsfileNew();

int current_row = 0;
thFmfitsfilePut(f, filename, fits, &mode, hdr1, fmhdu, pt_chain,  &current_row, &image_loc, &fmhdu_loc);

thInsertStatus(status, SH_SUCCESS);
return(f);
}

/* if the mode is WRITE -- does not support the APPEND mode
   1. set up the header 
   2. go to the last row of the image extension 
*/

if (mode == 1) {

hdr1 = hdr;
if (hdr == NULL) hdr1 = shHdrNew();

int image_loc, fmhdu_loc, loc1;
image_loc = 1;
fmhdu_loc = image_loc + 1;

strcpy(key, FMIMAGE_HDR_KEYWORD);
if (shHdrGetInt(hdr1, key, &loc1) == SH_SUCCESS) { 
	if (loc1 != image_loc) {
	thError("%s: ERROR - duplicate keyword (%s=%d) exists in the header, unsupported value", name, FMIMAGE_HDR_KEYWORD, loc1);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(NULL);
	}
} else {
	strcpy(comment, FMIMAGE_HDR_COMMENT);
	shHdrInsertInt(hdr1, key, image_loc, comment);
}

strcpy(key, FMHDU_HDR_KEYWORD);
if (shHdrGetInt(hdr1, key, &loc1) == SH_SUCCESS) {
	if (loc1 != fmhdu_loc) {
		thError("%s: ERROR - duplicate keyword (%s=%d) exists in the header, unsupported value", name, FMHDU_HDR_KEYWORD, loc1);
		thInsertStatus(status, SH_GENERIC_ERROR);
		return(NULL);
		}
} else {
	strcpy(comment, FMHDU_HDR_COMMENT);
	shHdrInsertInt(hdr1, key, fmhdu_loc, comment);
}

fits = phFitsBinTblOpen(filename, mode, hdr1);
phFitsBinTblHdrWrite(fits, IOREGION, NULL);

int current_row = 0;
FMFITSFILE *f;
f = thFmfitsfileNew();
thFmfitsfilePut(f, filename, fits, &mode, hdr1, NULL, NULL,  &current_row, &image_loc, &fmhdu_loc);


thInsertStatus(status, SH_SUCCESS);
return(f);

}

/* you should never reach here */

thError("%s: ERROR - source code fatal error", name);
thInsertStatus(status, SH_GENERIC_ERROR);
return(NULL);
}

REGION *thFmfitsReadImageFromRow(FMFITSFILE *fmfits, int fitsrow, RET_CODE *status) {
char *name = "thFmfitsReadImageFromRow";

if (fmfits == NULL) {
	thError("%s: ERROR - null input", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(NULL);
	}

REGION *reg;
reg = shRegNew("", 0, 0, TYPE_THPIX);

RET_CODE status1;
status1 = thImageReadFitsFromRow(fmfits->fits, reg, fitsrow, &(fmfits->row));
thInsertStatus(status, status1);

if (status1 != SH_SUCCESS) {
	thError("%s: ERROR - problem reading the row %d", name, fitsrow);
	shRegDel(reg);
	return(NULL);
	}

return(reg);
}

RET_CODE thFmfitsReadFuncmodel(FMFITSFILE *fmfits, FUNCMODEL *fm) {
char *name = "thFmfitsReadFuncmodel";

if (fmfits == NULL || fmfits->fmhdus == NULL || fmfits->pts == NULL || fm == NULL) {
	thError("%s: ERROR - null input or output", name);
	return(SH_GENERIC_ERROR);
	}

int i;
RET_CODE status1;
i = thLocateFuncInFmhdu(fmfits->fmhdus, fm->funcname, &status1);

if (status1 != SH_SUCCESS || i < 0) {
	thError("%s: ERROR - could not locate the function (%s) in FMHDU list in file (%s)", name, fm->funcname, fmfits->fname);
	return(status1);
	}

FMHDU *fmhdu;
PARTABLE *pt;

fmhdu = shChainElementGetByPos(fmfits->fmhdus, i);
pt = shChainElementGetByPos(fmfits->pts, i);
int *wfitsrow;

wfitsrow = &(fmfits->row);
status1 = thFuncmodelReadFits(fmfits->fits, pt, fm, fmhdu, wfitsrow); 

if (status1 != SH_SUCCESS) {
	thError("%s: ERROR - could not read function from fits file", name);
	}

return(status1);
}

RET_CODE thFmfitsWriteImageToRow(FMFITSFILE *fmfits, REGION *reg) {
char *name = "thFmfitsWriteImageToRow";

if (fmfits == NULL || fmfits->fits == NULL || reg == NULL) {
	thError("%s: ERROR - null input or output", name);
	return(SH_GENERIC_ERROR);
	}

RET_CODE status;
status = thImageWriteFitsInRow(fmfits->fits, reg, &(fmfits->row));

return(status);
}
	
RET_CODE thFmfitsAddPartable(FMFITSFILE *fmfits, PARTABLE *pt) {
char *name = "FmfitsAddPartable";
if (fmfits == NULL || pt == NULL) {
	thError("%s: ERROR - null input or output", name);
	return(SH_GENERIC_ERROR);
	}

FMHDU *fmhdu;
RET_CODE status;
fmhdu = thFmhduNewFromPartable(pt, fmfits->fmhdus, 
	FMIMAGE_HDR_VAL, FMHDU_HDR_VAL, &status);

if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not generate a FMHDU", name);	
	return(status);
	}

if (fmfits->pts == NULL) fmfits->pts = shChainNew("PARTABLE");
if (fmfits->fmhdus == NULL) fmfits->fmhdus = shChainNew("FMHDU");

shChainElementAddByPos(fmfits->pts, pt, "PARTABLE", TAIL, AFTER);
shChainElementAddByPos(fmfits->fmhdus, fmhdu, "FMHDU", TAIL, AFTER);

return(SH_SUCCESS);
}

RET_CODE thFmfitsClose(FMFITSFILE *fmfits) {
char *name = "thFmfitsClose";

if (fmfits == NULL || fmfits->fits == NULL || fmfits->fmhdus == NULL) {
	thError("%s: ERROR - null input or outpur", name);
	return(SH_GENERIC_ERROR);
	}
if (fmfits->mode > 1 || fmfits->mode < 0) {
	thError("%s: ERROR - unsupported IO mode (%d)", name, fmfits->mode);
	return(SH_GENERIC_ERROR);
	}

/* if the file was openned to be READ */
if  (fmfits->mode == 0) {
	phFitsBinTblEnd(fmfits->fits);
	phFitsBinTblClose(fmfits->fits);
	return(SH_SUCCESS);
}

/* in the WRITE mode, you should go through hdus one by one */

if (fmfits->mode == 1) {

RET_CODE status;

int *order;
order = thOrderFmhdu(fmfits->fmhdus, &status);	
if (status != SH_SUCCESS || order == NULL) {
	thError("%s: could not order FMHDU chain", name);
	return(status);
}

if (phFitsBinTblEnd(fmfits->fits) < 0) {
	thError("%s: ERROR - could not close the IMAGE extension", name);
	return(SH_GENERIC_ERROR);
	}

/* now writing the FMHDUs and the PARTABLEs */

int i, j, k, n, k0;

/* first find where w.r.t partable does the fmhdu lie */
j = 0;
i = 0;
n = shChainSize(fmfits->fmhdus);
while (((FMHDU *)shChainElementGetByPos(fmfits->fmhdus, order[i]))->pthdu  < fmfits->fmhdu_loc && (i < n)) i++;

j = i - 1;

/* then do the ouputing in two parts */
FMHDU *fmhdu;
PARTABLE *pt;

/* first output the partables before fmhdu */

k0 = fmfits->image_loc;
for (i = 0; i < j; i++) {
	if (i > 0) k0 = fmhdu->pthdu;
	fmhdu = shChainElementGetByPos(fmfits->fmhdus, order[i]);
	for (k = k0+1; k < fmhdu->pthdu; k++) {
		phFitsBinTblHdrRead(fmfits->fits, NULL, NULL, NULL, 0);
		phFitsBinTblEnd(fmfits->fits);
	}
	pt = shChainElementGetByPos(fmfits->pts, order[i]);
	status = thPartableWriteFits(fmfits->fits, pt);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not write PARTABLE to extension %d", name, fmhdu->pthdu);
		return(status);
	}
}

/* then output fmhdu's */

status = thFmhduWriteFits(fmfits->fits, fmfits->fmhdus);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not write FMHDUs to extension %d", fmfits->fmhdu_loc);
	return(status);
	}

/* then output the rest of the partables */

if (j < 0) j = 0;
k0 = fmfits->fmhdu_loc;
for (i = j; i < n; i++) {
	if (i > 0) k0 = fmhdu->pthdu;
	fmhdu = shChainElementGetByPos(fmfits->fmhdus, order[i]);
	for (k = k0+1; k < fmhdu->pthdu; k++) {
		phFitsBinTblHdrRead(fmfits->fits, NULL, NULL, NULL, 0);
		phFitsBinTblEnd(fmfits->fits);
	}
	pt = shChainElementGetByPos(fmfits->pts, order[i]);
	status = thPartableWriteFits(fmfits->fits, pt);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not write PARTABLE to extension %d", name, fmhdu->pthdu);
		return(status);
	}
}


/* close the file at the end */
if (phFitsBinTblClose(fmfits->fits) < 0) {
	thError("%s: ERROR - could not close PHFITSFILE", name);
	return(SH_GENERIC_ERROR);
	}

/* does not empty the memory, allows the used space to be carried around */

return(SH_SUCCESS);
}

/* you should never reach here */

thError("%s: ERROR - source code is faulty", name);
return(SH_GENERIC_ERROR);
}

/* manipulating chains of PT and FMHDU */

int *thOrderFmhdu(CHAIN *fmhdu_chain, RET_CODE *status) {

/* this function finds the order of fmhdu's for pt's. it is assumed that there are not repetative hdu numbers in the fmhdus for two different pt's */
char *name = "thOrderFmhdu";
if (fmhdu_chain == NULL || shChainSize(fmhdu_chain) == 0) {
	thError("%s: ERROR - null or empty input", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(NULL);
}

int i, n, *order, *hdu;
n = shChainSize(fmhdu_chain);

order = (int *) thCalloc(n, sizeof(int));
hdu = (int *) thCalloc(n, sizeof(int));

FMHDU *fmhdu;
for (i = 0; i < n; i++) {
	fmhdu = shChainElementGetByPos(fmhdu_chain, i);
	hdu[i] = fmhdu->pthdu;
	if (hdu[i] < 0) {
		thError("%s: ERROR - negative HDU discovered for function %s at index = %d", name, fmhdu->funcname, i);
		thFree(order);
		thFree(hdu);
		thInsertStatus(status, SH_GENERIC_ERROR);
		return(NULL);
	}
}



int j, this_index, this_hdu, max_hdu = -1, prev_hdu = -1;

for (i = 0; i < n; i++) {
	if (hdu[i] > max_hdu) max_hdu = hdu[i];
}
for (i = 0; i < n; i++) {
	this_hdu = max_hdu + 1;
	for (j = 0; j < n; j++) {
		if (hdu[j] > prev_hdu && hdu[j] < this_hdu) {
		this_hdu = hdu[j];
		this_index = j;
		}
	}
	prev_hdu = this_hdu;
	order[i] = this_index;
}

thFree(hdu);
thInsertStatus(status, SH_SUCCESS);
return(order);
}


FMHDU *thFmhduNewFromPartable(PARTABLE *pt, CHAIN *fmhdus, int fmhdu_loc, int image_loc, RET_CODE *status) {
	
char *name = "thFmhduNewFromPartable";

if (pt == NULL) {
	thError("%s: ERROR - null input", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(NULL);
	}

int n = 0;
if (fmhdus != NULL) {
	n = shChainSize(fmhdus);
}

int i, *hdu;
hdu = thCalloc(n+2, sizeof(int));

FMHDU *fmhdu1;
for (i = 0; i < n; i++) {
	fmhdu1 = shChainElementGetByPos(fmhdus, i);
	hdu[i] = fmhdu1->pthdu;
	}
hdu[n] = image_loc;
hdu[n+1] = fmhdu_loc;

for (i = 0; i < n + 2; i++) {
	if (hdu[i] <= 0) {
		thError("%s: ERROR - negative or zero hdu detected", name);
		thInsertStatus(status, SH_GENERIC_ERROR);
		thFree(hdu);	
		return(NULL);
}
}

int this_hdu = 1;
for (i = 0; i < n + 2; i++) {
	if (hdu[i] == this_hdu) hdu++;
}

thFree(hdu);

FMHDU *fmhdu;
fmhdu = thFmhduNew();
memcpy(fmhdu->funcname, pt->funcname, strlen(pt->funcname));
fmhdu->pthdu = this_hdu;
thInsertStatus(status, SH_SUCCESS);
return(fmhdu);
}


RET_CODE thChainReorder(CHAIN *c, int *order) {
char *name = "thChainReorder";

if (c == NULL || order == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

int n;
n = shChainSize(c);
if (n == 0) return(SH_SUCCESS);

int i, j;
for (i = 0; i < n; i++) {
	j = 0;
	while (j < n && order[j] != i) j++;
	if (j == n) {
		thError("%s: ERROR - order array not safe", name);
		return(SH_GENERIC_ERROR);
	}
}

CHAIN *c2;
c2 = shChainNew(shNameGetFromType(shChainTypeGet(c)));

void *e;
TYPE etype;
for (i = 0; i < n; i++) {
	e = shChainElementGetByPos(c, order[i]);
	etype = shChainElementTypeGetByPos(c, order[i]);
	shChainElementAddByPos(c2, e, shNameGetFromType(etype), TAIL, AFTER);
	}

for (i = 0; i < n; i++) {
	shChainElementRemByPos(c, i);
}

for (i = 0; i < n; i++) {
	e = shChainElementGetByPos(c2, i);
	etype = shChainElementTypeGetByPos(c2, i);
	shChainElementAddByPos(c, e, shNameGetFromType(etype), TAIL, AFTER);
}

shChainDel(c2);
return(SH_SUCCESS);
}

int thLocateFuncInFmhdu(CHAIN *fmhdus, char *fname, RET_CODE *status) {
char *name = "thLocateFuncInFmhdu";

if (fmhdus == NULL || shChainSize(fmhdus) == 0 || fname == NULL || strlen(fname) == 0) {
	thError("%s: ERROR - null or trivial input", name);
	thInsertStatus(status, SH_GENERIC_ERROR);
	return(-1);
}

int i, n, match = 0, l, l1;
n = shChainSize(fmhdus);
FMHDU *fmhdu;
l = strlen(fname);

for (i = 0; i < n; i++) {
	fmhdu = shChainElementGetByPos(fmhdus, i);
	l1 = strlen(fmhdu->funcname);
	if (l1 == l && memcmp(fmhdu->funcname, fname, l) == 0) match = match * n + i + 1;
}

if (match > n) {
	thError("%s: WARNING - multiple matches found, returning the last match", name);
	thInsertStatus(status, SH_SUCCESS);
	match = match %n - 1;
	if (match < 0) match = match + n;
	return(match);
}

thInsertStatus(status, SH_SUCCESS);
return(match - 1);

}

int thCountFuncInFmfits(FMFITSFILE *fmfits, char *fname) {

char *name = "thLocateFuncInFmfits";
if (fmfits == NULL || fmfits->fmhdus == NULL || fname == NULL || strlen(fname) == 0) {
	thError("%s: ERROR - null input", name);
	return(0);
}

int i, n, match = 0;
CHAIN *fmhdus;
fmhdus = fmfits->fmhdus;
n = shChainSize(fmhdus);
FMHDU *fmhdu;

for (i = 0; i < n; i++) {
	fmhdu = shChainElementGetByPos(fmhdus, i);
	if (strcmp(fmhdu->funcname, fname) == 0) match++;
}

return(match);

}

/* this is to upload the header */
RET_CODE thFmfitsAddNewPartable(FMFITSFILE *fmfits, PARTABLE *pt, FUNCMODEL *wf) {
char *name = "thFmfitsUploadNewPartable";
if (fmfits == NULL || pt == NULL || pt->funcname == NULL || strlen(pt->funcname) == 0) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
	}

FMHDU *fmhdu;
fmhdu = thFmhduNew();

if (pt->funcname == NULL || strlen(pt->funcname) == 0) {
	thError("%s: WARNING - null or empty function name in partable", name);
	} else {
	strcpy(fmhdu->funcname, pt->funcname);
	}

if ((fmhdu->firstrow = fmfits->row) < 0) {
	thError("%s: ERROR - negative starting row number", name);
	thFmhduDel(fmhdu);
	return(SH_GENERIC_ERROR);
	}

int n;
FMHDU *fmhdu2;
fmhdu->pthdu = 3;
if (fmfits->fmhdus != NULL && (n = shChainSize(fmfits->fmhdus)) != 0) {
	fmhdu2 = shChainElementGetByPos(fmfits->fmhdus, n - 1);
	fmhdu->pthdu = 1 + fmhdu2->pthdu;
	}

fmhdu->ncomp = wf->ncomp;
if (fmhdu->ncomp > N_FUNCMODEL_COMP_INDEX) {
	thError("%s: ERROR - too many func indexes, source code needs be recompiled", name);
	thFmhduDel(fmhdu);
	return(SH_GENERIC_ERROR);
}
n = wf->ncomp < N_FUNCMODEL_COMP_INDEX ? wf->ncomp : N_FUNCMODEL_COMP_INDEX;
if (n <= 0) {
	thError("%s: ERROR - unacceptable number of components for function model", name);
	thFmhduDel(fmhdu);
	return(SH_GENERIC_ERROR);
	}

if (wf->compindex == NULL) {
	thError("%s: ERROR - component index not set in the sample function model", name);
	thFmhduDel(fmhdu);
	return(SH_GENERIC_ERROR);
}
memcpy(fmhdu->compindex, wf->compindex, n * sizeof(int));

if (fmfits->fmhdus == NULL) fmfits->fmhdus = shChainNew("FMHDU");
if (fmfits->pts == NULL) fmfits->pts = shChainNew("PARTABLE");

shChainElementAddByPos(fmfits->fmhdus, fmhdu, "FMHDU", TAIL, AFTER);
shChainElementAddByPos(fmfits->pts, pt, "PARTABLE", TAIL, AFTER);
return(SH_SUCCESS);

}

/* the following function is used to seal the header */
RET_CODE TH_SUCCESS (void *a, void *b, int c) {
	return(SH_SUCCESS);
}

int thGetRowMaxFromPartable(PARTABLE *pt) {
char *name = "thGetRowMaxFromPartable";
if (pt == NULL) {
	thError("%s: ERROR - null input", name);
	return(-1);
}

RET_CODE status;
int nt, *npdiv;
/* this function should be written */
npdiv = thGetNpardivFromPartable(pt, &nt, &status);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get division properties from partable", name);
	return(-1);
}

int maxrow;
maxrow = thGetMaxrowFromNpardiv(npdiv, nt);
return(maxrow);
}


RET_CODE thFmfitsWriteFuncmodel(FMFITSFILE *fmfits, FUNCMODEL *wfm) {
char *name = "thFmfitsWriteFuncmodel";
if (fmfits == NULL || fmfits->fits == NULL || wfm == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
	}

RET_CODE status;
status = thFuncmodelWriteFits(fmfits->fits, wfm, &(fmfits->row));

if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not write to the fits file", name);
	}

return(status);
}

PARLIST *thParlistNewFromPartable(PARTABLE *pt) {
char *name = "thParlistNewFromPartable";
if (pt == NULL || pt->table == NULL) {
	thError("%s: ERROR - null input", name);
	return(NULL);
	}

PARLIST*pl;
pl = thParlistNew(pt->tablename);

int i, n;
n = shChainSize(pt->table);

PARELEM *pe; PARDEF *pd;
for (i = 0; i < n; i++) {
	pd = thPardefNew(NULL);
	pe = shChainElementGetByPos(pt->table, i);
	thPardefPut(pd, NULL, NULL, pe->pname, &(pe->index), &pe->ptype, pe->pdiv);
	shChainElementAddByPos(pl->pars, pd, "PARDEF", TAIL, AFTER);
}
return(pl);
}

int thParlistGetNPardef(PARLIST *pl) {
char *name = "thParlistGetNPardef";
if (pl == NULL || pl->pars == NULL) {
	thError("%s: ERROR - null parlist", name);
	return(-1);
	}
return(shChainSize(pl->pars));
}

PARDEF *thParlistGetPardefByPos(PARLIST *pl, int pos){
char *name = "thParlistGetPardefByPos";
if (pl == NULL || pl->pars == NULL) {
	thError("%s: ERROR - null input", name);
	return(NULL);
}
int n = shChainSize(pl->pars);
if (pos >= n) {
	thError("%s: ERROR - position (%d) beyond pardef chain size (%d)", name, pos, n);
	return(NULL);
}
PARDEF *pd;
pd = shChainElementGetByPos(pl->pars, pos);
return(pd);
}

PARDEF *thParlistGetPardefByName(PARLIST *pl, char *pname) {
char *name = "thParlistGetPardefByName";
if (pl == NULL || pl->pars == NULL || pname == NULL) {
	thError("%s: ERROR - null input", name);
	return(NULL);
}
int i = 0, n;
n = shChainSize(pl->pars);
PARDEF *pd = NULL, *pdx = NULL;
while (pdx == NULL && i < n) {
	pd = shChainElementGetByPos(pl->pars, i);
	if (pd != NULL && pd->pname != NULL && !strcmp(pd->pname, pname)) pdx = pd;
	i++;
}
return(pdx);
}

char *thPardefGetName(PARDEF *pd) {
char *name = "thPardefGetName";
if (pd == NULL) {
	thError("%s: ERROR - null input", name);
	return(NULL);
}
return(pd->pname);
}

void thPardefCopy(PARDEF *pd1, PARDEF *pd2) {
char *name = "thPardefCopy";
if (pd1 == NULL || pd2 == NULL) {
	thError("%s: ERROR - null input or output", name);
	return;
}
if (pd1->pname == NULL || pd1->pval == NULL) {
	thError("%s: ERROR - null name or value in input", name);
	return;	
}
if (pd2->pname == NULL) {
	pd2->pname = thCalloc(SIZE, sizeof(char));
	}
strcpy(pd2->pname, pd1->pname);

if (pd1->ptype != pd2->ptype && pd2->pval != NULL) {
	switch(pd2->ptype) {
	case  PIX_PARTYPE:
	thFree((THPIX *) pd2->pval);
	pd2->pval = NULL;
	break;
	case INTEGER_PARTYPE: 
	thFree((int *) pd2->pval);	
	pd2->pval = NULL;
	break;
	default:
	pd2->pval = NULL;
	break;
	}
	}
pd2->ptype = pd1->ptype;
if (pd2->pval == NULL) { 
	switch(pd2->ptype) {
	case INTEGER_PARTYPE: 
	pd2->pval = thCalloc(1, sizeof(int));
	break;
	case PIX_PARTYPE:
	pd2->pval = thCalloc(1, sizeof(THPIX));
	break;
	default:
	thError("%s: ERROR - unsupported type for (%s)", name, pd1->pname);
	return;
	break;
	}
}

switch(pd2->ptype) {
	case INTEGER_PARTYPE: 
	memcpy(pd2->pval, pd1->pval, sizeof(int));
	break;
	case PIX_PARTYPE:
	memcpy(pd2->pval, pd1->pval,  sizeof(THPIX));
	break;
	default:
	thError("%s: ERROR - unsupported type for (%s)", name, pd1->pname);
	return;
	}

return;
}

void thValuePutInPardef(void *q, char *type, PARDEF *pd) {
char *name = "thValuePutInPardef";
if (q == NULL || type == NULL || pd == NULL) {
	thError("%s: ERROR - null input or output", name);
	return;
}

TYPE qtype;
if ((qtype = shTypeGetFromName(type)) == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - unrecognized type %s", name, type);
	return;
}

static int init = 0;
static TYPE UCHAR;
static TYPE SCHAR;
static TYPE CHAR;
static TYPE INT;
static TYPE UINT;
static TYPE LONG;
static TYPE ULONG;
static TYPE FLOAT;
static TYPE DOUBLE;

if (!init) {
	UCHAR = shTypeGetFromName("UCHAR");
	CHAR = shTypeGetFromName("CHAR");
	SCHAR = shTypeGetFromName("SCHAR");
	INT = shTypeGetFromName("INT");
	UINT = shTypeGetFromName("UINT");
	LONG = shTypeGetFromName("LONG");
	ULONG = shTypeGetFromName("ULONG");
	FLOAT = shTypeGetFromName("FLOAT");
	DOUBLE = shTypeGetFromName("DOUBLE");
	init = 1;
	}
if (pd->ptype == UNKNOWN_PARTYPE && pd->pval != NULL) {
	pd->pval = NULL;
}
if (qtype == FLOAT) {
	if (pd->ptype != PIX_PARTYPE) {
	 	if (pd->pval != NULL) thFree((int *) pd->pval);
		pd->pval = thCalloc(1, sizeof(THPIX));
	}
		pd->ptype = PIX_PARTYPE;
		*(THPIX *) pd->pval = (THPIX) *(float *) pd->pval;	
} else if (qtype == DOUBLE) {	
	if (pd->ptype != PIX_PARTYPE) {
	 	if (pd->pval != NULL) thFree((int *) pd->pval);
		pd->pval = thCalloc(1, sizeof(THPIX));
	}
		pd->ptype = PIX_PARTYPE;
		*(THPIX *) pd->pval = (THPIX) *(double *) pd->pval;
} else if (qtype == UCHAR) {
	if (pd->ptype != INTEGER_PARTYPE) {
	 	if (pd->pval != NULL) thFree((THPIX *) pd->pval);
		pd->pval = thCalloc(1, sizeof(int));
	}
		pd->ptype = INTEGER_PARTYPE;
		*(int *) pd->pval = (int) *(unsigned char *) pd->pval;
} else if (qtype == SCHAR) {
	if (pd->ptype != INTEGER_PARTYPE) {
	 	if (pd->pval != NULL) thFree((THPIX *) pd->pval);
		pd->pval = thCalloc(1, sizeof(int));
	}	
		pd->ptype = INTEGER_PARTYPE;
		*(int *) pd->pval = (int) *(signed char *) pd->pval;
} else if (qtype == CHAR) {
	if (pd->ptype != INTEGER_PARTYPE) {
	 	if (pd->pval != NULL) thFree((THPIX *) pd->pval);
		pd->pval = thCalloc(1, sizeof(int));
	}	
		pd->ptype = INTEGER_PARTYPE;
		*(int *) pd->pval = (int) *(char *) pd->pval;
} else if (qtype == UINT) {
	if (pd->ptype != INTEGER_PARTYPE) {
	 	if (pd->pval != NULL) thFree((THPIX *) pd->pval);
		pd->pval = thCalloc(1, sizeof(int));
	}	
		pd->ptype = INTEGER_PARTYPE;
		*(int *) pd->pval = (int) *(unsigned int *) pd->pval;
} else if (qtype == INT) {
	if (pd->ptype != INTEGER_PARTYPE) {
	 	if (pd->pval != NULL) thFree((THPIX *) pd->pval);
		pd->pval = thCalloc(1, sizeof(int));
	}	
		pd->ptype = INTEGER_PARTYPE;
		*(int *) pd->pval = (int) *(int *) pd->pval;
} else if (qtype == ULONG) {
	if (pd->ptype != INTEGER_PARTYPE) {
	 	if (pd->pval != NULL) thFree((THPIX *) pd->pval);
		pd->pval = thCalloc(1, sizeof(int));
	}	
		pd->ptype = INTEGER_PARTYPE;
		*(int *) pd->pval = (int) *(unsigned long *) pd->pval;
} else if (qtype == LONG) {
	if (pd->ptype != INTEGER_PARTYPE) {
	 	if (pd->pval != NULL) thFree((THPIX *) pd->pval);
		pd->pval = thCalloc(1, sizeof(int));
	}	
		pd->ptype = INTEGER_PARTYPE;
		*(int *) pd->pval = (int) *(long *) pd->pval;
} else {
	thError("%s: ERROR - unsupported type (%s)", name, type);
	return;
}

return;
}

void thPardefValuePutInChar(PARDEF *pd, char *cval) {
char *name = "thPardefValuePutInChar";
if (pd == NULL || pd->pval == NULL || cval == NULL) {
	thError("%s: NULL input or output", name);
	return;
}
if (pd->ptype == PIX_PARTYPE) {
	sprintf(cval, "%f", *(THPIX *) pd->pval);
} else if (pd->ptype == INTEGER_PARTYPE) {
	sprintf(cval, "%d", *(int *) pd->pval);
} else {
	thError("%s: ERROR - unsupported type", name);
	return;
}
return;
}

RET_CODE thRegTransExtreg(REGION *reg, EXTREGION  *extreg) {
char *name = "thRegTransExtreg";
if (reg == NULL || extreg == NULL) {
	thError("%s: ERROR - null input or output", name);
	}

REGION *subreg;
subreg = reg;
	
int nrow, ncol;
nrow = extreg->reg->nrow;
ncol = extreg->reg->ncol;
if (extreg->reg == NULL) extreg->reg = shRegNew(reg->name, reg->nrow, reg->ncol, THIOPIXTYPE);
if (nrow == 0 || ncol == 0) {
	int i;
	extreg->reg->rows_io = thCalloc(subreg->nrow, sizeof(THIOTYPE*));
	for (i = 0; i < reg->nrow; i++) {
		extreg->reg->rows_io[i] = thCalloc(reg->ncol, sizeof(THIOTYPE));
	}
	extreg->reg->nrow = reg->nrow;
	extreg->reg->ncol = reg->ncol;
}
FL64 bzero, bscale;
if (extreg->reg->ncol != reg->ncol || extreg->reg->nrow != reg->nrow) {
	thError("%s: ERROR - mismatching sizes for region and extended region", name);
	return(SH_GENERIC_ERROR);
	}
RET_CODE status;
status = thCompressRegion(reg, THIOPIXTYPE, &bscale, &bzero);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - problem calculating bzero and bscale", name);
	return(status);
}

status = thRegConvertPixType(reg, extreg->reg, bzero, bscale);
/* altering bscale and bzero such that it can be directly used when reading */
extreg->bscale = ((FL64) 1.0) / bscale;
extreg->bzero = - bzero / bscale;

if (status != SH_SUCCESS) {
	thError("%s: ERROR - problem converting type", name);
	return(status);
}
extreg->conversion = 1;
return(SH_SUCCESS);
}

