#include "thMask.h"

RET_CODE thMaskUpdateFromSpanmask(MASK *mask, SPANMASK *sm, 
			    const int nbp) {

  char *name = "thMaskUpdateFromSpanmask";
 
  if (sm == NULL) {
    thError("%s: ERROR - null input (spanmask)", name);
    return(SH_GENERIC_ERROR);
  }
  if (mask == NULL) {
	thError("%s: ERROR - null input (mask)", name);
	return(SH_GENERIC_ERROR);
	}
  int nrow, ncol;
  nrow = sm->nrow; ncol = sm->ncol;

  if ((sm->nrow <= 0) || (sm->ncol <= 0)) {
    thError("%s: ERROR - spanmask nrow (%d) or ncol (%d) inappropriate", 
		   name, sm->nrow, sm->ncol);
    return(SH_GENERIC_ERROR);
  }

  if (nbp <= 0) {
    thError("%s: ERORR - unrealistic number of bitplanes(%d)", name, nbp);
    return(SH_GENERIC_ERROR);
  }

  if (nbp > (CHAR_BIT * sizeof(unsigned char))) {
    thError("%s: ERROR - mask pixel doesn't have enough bits (%d) to represent all the bitplanes of the spanmask (%d)",
		   name, CHAR_BIT * sizeof(unsigned char), nbp);
    return(SH_GENERIC_ERROR);
  }

  if ((mask->nrow != nrow) || (mask->ncol != ncol)) {
	thError("%s: ERROR - (nrow, ncol) from (mask) and (spanmask) do not match", name);
	return(SH_GENERIC_ERROR);
	}
  thMaskClear(mask);

  REGION *reg;
  reg = shRegNew("", nrow, ncol, TYPE_PIX); /* TYPE_PIX is defined PHOTO */

  int im, row, col;
  PIX *rrows;
  MASKBIT *mrows;
  MASKBIT uval = 1;
  int mval;

  for (im = 0; im < nbp; im++) {

    mval = (int) uval;
    mval = 1;
   
    shRegClear(reg);
    phRegionSetValFromObjmaskChain(reg, sm->masks[im], mval);
    
    for (row = 0; row < nrow; row++) {
      mrows = mask->rows[row];
      rrows = reg->ROWS[row];  /* ROWS is defined in PHOTO */
      for (col = 0; col < ncol; col++) {
	if (rrows[col]) {
	  mrows[col] |= uval;
	}
      }
    }
    /* shift the value by one bit */
    uval <<= 1;

  }

  shRegDel(reg);

  /* debug

  reg = shRegNew("debug-mask", nrow, ncol, TYPE_PIX);
  for (row = 0; row < nrow; row++) {
      mrows = mask->rows[row];
      rrows = reg->ROWS[row]; 
      for (col = 0; col < ncol; col++) {
	rrows[col] = (mrows[col]);
      }
  }
  
  if (shRegWriteAsFits(reg, "./fit/fpctest2.fit", 
		       STANDARD, 2, DEF_DEFAULT,
		       NULL, 0) != SH_SUCCESS) {
    thError("%s: problem outputting debug mode mask file", 
		   name);
  }

  shRegDel(reg);

  shMaskWriteAsFits(mask, "./fit/mask-test.fit",
  DEF_DEFAULT, NULL, 0);

  end of debugging commands 
  */
  return(SH_SUCCESS);
}

MASK *thMaskGenFromSpanmask(SPANMASK *sm, 
			    const int nbp, 
			    RET_CODE *status) {

  char *name = "thMaskGenFromSpanmask";
 
  if (sm == NULL) {
    thError("%s: no spanmask passed", name);
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
  }

  int nrow, ncol;
  nrow = sm->nrow; ncol = sm->ncol;

  if ((sm->nrow <= 0) || (sm->ncol <= 0)) {
    thError("%s: spanmask nrow (%d) or ncol (%d) inappropriate", 
		   name, sm->nrow, sm->ncol);
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
  }

  if (nbp <= 0) {
    thError("%s: unrealistic number of bitplanes(%d)", name, nbp);
    return(NULL);
  }

  if (nbp > (CHAR_BIT * sizeof(unsigned char))) {
    thError("%s: mask pixel doesn't have enough bits (%d) to represent all the bitplanes of the spanmask (%d)",
		   name, CHAR_BIT * sizeof(unsigned char), nbp);
    if (status != NULL) {
      *status = SH_GENERIC_ERROR;
    }
    return(NULL);
  }

  THMASK *mask;
  mask = shMaskNew(name, nrow, ncol);
  
  RET_CODE status1;
  status1 = thMaskUpdateFromSpanmask(mask, sm, nbp);
  if (status1 != SH_SUCCESS) {
	thError("%s: ERROR - could not update (mask) from (spanmask)", name);
	if (status != NULL) *status = status1;
	shMaskDel(mask);
	return(NULL);
	} 

   if (status != NULL) *status = SH_SUCCESS;
   return(mask);
}

RET_CODE thObjmaskChainAddToSpanmask(CHAIN *mchain, 
				 SPANMASK *sm, 
				 S_MASKTYPE which) {
  char *name = "thObjmaskChainAddToSpanmask";
 
  if (mchain == NULL || shChainSize(mchain) == 0) {
	thError("%s: ERROR - null or empty mask chain", name);
	return(SH_GENERIC_ERROR);
  }
  if (sm == NULL) {
	thError("%s: ERROR - null (spanmask)", name);
	return(SH_GENERIC_ERROR);
  }
  if (which >= S_NMASK_TYPES) {
	thError("%s: ERROR - found (which = %d) >= (S_NMASK_TYPES = %d)", name, which, (int) S_NMASK_TYPES);
	return(SH_GENERIC_ERROR);
	}
  OBJMASK *objmask;
  int i;
  for(i = 0; i < shChainSize(mchain); i++) {
    objmask = (OBJMASK *) shChainElementGetByPos(mchain, i);
    if (objmask != NULL) {
      phObjmaskAddToSpanmask(objmask, sm, which);
    }
  }

  return(SH_SUCCESS);
}

OBJMASK *thObjmaskChainMerge(CHAIN *mc, float *dx, float *dy) {
  
  int nc;

  if (mc == NULL || (nc = shChainSize(mc)) == 0) {
    return(NULL);
  }


  int init_dx = 0, init_dy = 0;
  if (dx == NULL) {
    init_dx = 1;
    dx = (float *) thCalloc(nc, sizeof(float));
  }
  if (dy == NULL) {
  	init_dy = 1;
	dy = (float *) thCalloc(nc, sizeof(float));
  }
  
  OBJMASK *om, *om2;
  int i;

  om = phObjmaskCopy((OBJMASK *) shChainElementGetByPos(mc, 0), (float) 0.0, (float) 0.0);
  for (i = 1; i < shChainSize(mc); i++) {
    om2 = (OBJMASK *) shChainElementGetByPos(mc, i);
    phObjmaskMerge(om, om2, dx[i], dy[i]);
  }

  /* the result is already canonized */
  if (init_dx) thFree(dx);
  if (init_dy) thFree(dy);
  return(om);
}


RET_CODE thExpandPhSpanmask(SPANMASK *sm) {

  char *name = "thExpandPhSpanmask";
  int nrow, ncol;
  nrow = sm->nrow;
  ncol = sm->ncol;
  
  REGION *reg;
  reg = shRegNew("",  nrow, ncol, TYPE_PIX);
  
  int i, im, nom;
  OBJMASK *om;

  RET_CODE status;
  for (im = 0; im < S_NMASK_TYPES; im++) {

    nom = shChainSize(sm->masks[im]);

    if (nom > 0) {
      
      for (i = 0; i < nom; i++) {
      
	om = (OBJMASK*) shChainElementGetByPos(sm->masks[im], i);	
	if (om != NULL) {
		status = thCanonizeObjmask(om, 1);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not canonize (om) at (im = %d, i = %d)", name, im, i);
			return(status);
		}
	}
	om = phObjmaskGrow(om, reg, THMASKADDPIX);
	om = shChainElementChangeByPos(sm->masks[im], i, om);
	phObjmaskDel(om);
	
      } 
    }
  }

  shRegDel(reg);
  return(SH_SUCCESS);
  
}


