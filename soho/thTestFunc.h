#ifndef THTESTFUNC_H
#define THTESTFUNC_H

#include "thBasic.h"
#include "thMath.h"
#include "thFuncLoad.h"
#include "thFuncmodelTypes.h"
#include "thProfile.h"

typedef struct testpar {
	/* test parameters */
	THPIX a;
	THPIX b;

	/* galaxy profiles */
	THPIX r_e;
	THPIX n_sersic;
	THPIX epsilon;
	THPIX phi;
	THPIX rowc, colc;
		
	/* the following is private */
	THPIX b_n;
	THPIX a_xx, a_xy, a_yy;	

	/* sky basis */	
	int irow, icol;
	int mrow, mcol;
	int nrow, ncol;
} TESTPAR;

TESTPAR *thTestparNew();
void thTestparDel(TESTPAR *q);

RET_CODE sinxcosy(TESTPAR *q, FUNCMODEL *f);
RET_CODE sinxsiny(TESTPAR *q, FUNCMODEL *f);
#if 0
RET_CODE deV(TESTPAR *q, FUNCMODEL *f);
#endif
RET_CODE SersicProfile(TESTPAR *q, FUNCMODEL *f);
RET_CODE deVprofile(TESTPAR *q, FUNCMODEL *f);

THPIX deVpixel(PIXPOS *pixpos, void *q);
THPIX SersicPixel(PIXPOS *pixpos, TESTPAR *q);

RET_CODE BsplineXTcheb(TESTPAR *q, FUNCMODEL *f);


#endif
