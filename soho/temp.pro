if (objc_info.I_sersic1 ne 0.0) then begin
	mag_sersic1 = objc_info.mag_sersic1 
	r_sersic1 = objc_info.re_sersic1 * !PIXELSIZE
	n_sersic1 = objc_info.n_sersic1
	comment = [comment, $
	"mag_sersic1 = " + strtrim(string(mag_sersic1, format = format), 2), $
	"r_sersic1 = " + strtrim(string(r_sersic1, format = format), 2) + ", n1 = " + strtrim(string(n_sersic1, format =  format), 2)]
endif
if (objc_info.I_sersic2 ne 0.0) then begin
	mag_sersic2 = objc_info.mag_sersic2
	r_sersic2 = objc_info.re_sersic2 * !PIXELSIZE
	n_sersic2 = objc_info.n_sersic2
	comment = [comment, $ 
	"mag_sersic2 = " + strtrim(string(mag_sersic2, format =  format), 2), $
	"r_sersic2 = " + strtrim(string(r_sersic2, format =  format), 2) + ", n2 = " + strtrim(string(n_sersic2, format =  format), 2)]
endif
if (objc_info.I_sersic ne 0.0) then begin
	mag_sersic = objc_info.mag_sersic
	r_sersic = objc_info.re_sersic * !PIXELSIZE
	n_sersic = objc_info.n_sersic
	comment = [comment, $
	"mag_sersic = " + strtrim(string(mag_sersic, format =  format), 2), $
	"r_sersic = " + strtrim(string(r_sersic, format =  format), 2) + ", n = " + strtrim(string(n_sersic, format =  format), 2)]
endif
if (objc_info.I_deV ne 0.0) then begin
	mag_deV = objc_info.mag_deV
	r_deV = objc_info.re_deV * !PIXELSIZE
	comment = [comment, $
	"mag_deV = " + strtrim(string(mag_deV, format =  format), 2), $
	"r_deV = " + strtrim(string(r_deV, format =  format), 2)]
endif
if (objc_info.I_deV1 ne 0.0) then begin
	mag_deV1 = objc_info.mag_deV1
	r_deV1 = objc_info.re_deV1 * !PIXELSIZE
	comment = [comment, $
	"mag_deV1 = " + strtrim(string(mag_deV1, format =  format), 2), $
	"r_deV1 = " + strtrim(string(r_deV1, format =  format), 2)]
endif
if (objc_info.I_deV2 ne 0.0) then begin
	mag_deV2 = objc_info.mag_deV2
	r_deV2 = objc_info.re_deV2 * !PIXELSIZE
	comment = [comment, $
	"mag_deV2 = " + strtrim(string(mag_deV2, format =  format), 2), $
	"r_deV2 = " + strtrim(string(r_deV2, format =  format), 2)]
endif
if (objc_info.I_exp ne 0.0) then begin
	mag_exp = objc_info.mag_exp
	r_exp = objc_info.re_exp * !PIXELSIZE
	comment = [comment, $
	"mag_exp = " + strtrim(string(mag_exp, format =  format), 2), $
	"r_exp = " + strtrim(string(r_exp, format =  format), 2)]
endif
if (objc_info.I_exp1 ne 0.0) then begin
	mag_exp1 = objc_info.mag_exp1
	r_exp1 = objc_info.re_exp1 * !PIXELSIZE
	comment = [comment, $
	"mag_exp1 = " + strtrim(string(mag_exp1, format =  format), 2), $
	"r_exp1 = " + strtrim(string(r_exp1, format =  format), 2)]
endif
if (objc_info.I_exp2 ne 0.0) then begin
	mag_exp2 = objc_info.mag_exp2
	r_exp2 = objc_info.re_exp2 * !PIXELSIZE
	comment = [comment, $
	"mag_exp2 = " + strtrim(string(mag_exp2, format =  format), 2), $
	"r_exp2 = " + strtrim(string(r_exp2, format =  format), 2)]
endif


