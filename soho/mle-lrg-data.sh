#!/bin/bash

source "./generate_temporary_file.bash"
source "./run_commands_in_file.bash"

name="mle-lrg-data"
rerun=$DEFAULT_SDSS_RERUN

# generate a temporary command file
commandfile=$(generate_temporary_file --root="./" --prefix="temp-mle-lrg-data-submit-batch-" --suffix=".cmd")


declare -a camcol_a=("1" "2" "3" "4" "5" "6" "5" "2" "4" "6" "5")
n0=6

declare -a source_model_a=("sdss-data") 
declare -a source_class_a=("DEVEXP_GCLASS") 
n1=1

declare -a source_a=("fpC")
declare -a fit2fpCC_flag_a=("")
n2=1

declare -a target_model_a=("deV-exp") 
declare -a target_class_a=("DEVEXP_GCLASS") 
n3=1

declare -a nobjc_flag_a=("" "") #"-nobjc 1" "-nobjc 1")
declare -a nobjcstr_a=("multiple-init-6-pcost-8-DD1-Rlog-stage-1" "multiple-init-6-pcost-8-DD1-Rlog-stage-2")
declare -a multistageflag_a=("" "-multistage")
declare -a stage_a=("stage-1" "stage-2")
n4=1

#stripe 82
#4797
declare -a run_a=(2709 2700 2703 4797 2708 4797 2708 4207 2700 2703 2708 2709 2960 3565 3434 3437 2968 1742 4247 4252 4253 4263 4288 1863 1887 2570 2578 2579 109 125 211 240 241 250 251 256 259 273 287 297 307 94 5036 5042 5052 2873 21
24 1006 1009 1013 1033 1040 1055 1056 1057 4153 4157 4158 4184 4187 4188 4191 4192 2336 2886 2955 3325 3354 3355 3360 3362 3368 2385 2506 2728 4849 4858 4868 4874 4894 4895 4899 4905 4927 4930 4933 4948 2854 2855 2856
 2861 2662 3256 3313 3322 4073 4198 4203 3384 3388 3427 3430 4128 4136 4145 2738 2768 2820 1752 1755 1894 2583 2585 2589 2591 2649 2650 2659 2677 3438 3460 3458 3461 3465 7674 7183)
n5=120
n5=1

declare -a executable_a=(
"do-mle-lrg-data-HLM10"
"do-mle-lrg-data-RSV8"
"do-mle-lrg-data-RSV7"
"do-mle-lrg-data-RSV6"
"do-mle-lrg-data-RSV8"
"do-mle-lrg-data-RSV"
"do-mle-lrg-data-PGD-c1-eta-delta-2-secondary"
"do-mle-lrg.NAG.momentum-040.lr5E-3-nstep-1000zg"
"do-mle-lrg.NAG.momentum-040.lr2E-4-nstep-1000zg"
"do-mle-lrg.NAG.momentum-040.lrE-3-nstep-1000zg"
"do-mle-lrg.NAG.momentum-080.lr5E-3-nstep-1000zg"
"do-mle-lrg.NAG.momentum-080.lr2E-4-nstep-1000zg"
"do-mle-lrg.NAG.momentum-080.lrE-3-nstep-1000zg"
"do-mle-lrg.NAG.momentum-060.lr5E-3-nstep-1000zg"
"do-mle-lrg.NAG.momentum-060.lr2E-4-nstep-1000zg"
"do-mle-lrg.NAG.momentum-060.lrE-3-nstep-1000zg"
"do-mle-lrg-data-PGD-c1-eta-delta-2"
"do-mle-lrg-data-double-lmrecord-first-and-last-psf-variable-multi-init-flex-adv-masking-v4-hqn"
"do-mle-lrg-data-double-lmrecord-first-and-last-psf-variable-multi-init-flex-adv-masking-v4-hqn-extended-Gamma"
"do-mle-lrg-data-double-lmrecord-first-and-last-psf-variable-multi-init-flex-adv-masking-v4-lm"
"do-mle-lrg-data-double-lmrecord-first-and-last-psf-variable-multi-init-flex-adv-masking-v4"
"do-mle-lrg-data-double-lmrecord-first-and-last-psf-variable-multi-init-flex-adv-masking-v4"
"do-mle-lrg-double-lockre-seleted-pix-maskA"
"do-mle-lrg-double-lockre-seleted-pix-maskB"
"do-mle-lrg-double-lockre-seleted-pix-maskC"
"do-mle-lrg-double-lockre-seleted-pix-maskD"
"do-mle-lrg-double-lockre-seleted-pix-maskE"
"do-mle-lrg-data-apsector-double-lockre-improved-LRG-full-sky-good-pix-memory-limited-2Gk"
"do-mle-lrg-data-apsector-long-double-lockre-improved-LRG-full-sky-good-pix-memory-limited"
"do-mle-lrg-data-apsector-float-lockre-improved-LRG-full-sky-good-pix-memory-limited")
declare -a fitflags_a=(
"-fitindex -fitsize" 
"-fitindex -fitsize" 
"-fitindex -fitsize" 
"-fitindex -fitsize" 
"-fitindex -fitsize" 
"-fitindex -fitsize" 
"-fitindex -fitsize" 
"-fitindex -fitsize" 
"-fitindex -fitsize" 
"-fitindex -fitsize -fitshape -fitcenter" 
"-fitsize" "-fitsize" "-fitsize" "-fitsize")
declare -a dirflags_a=(
"flag-fitindex-fitsize-unconstrained-multi-init-HLM10"
"flag-fitindex-fitsize-unconstrained-multi-init-RSV8"
"flag-fitindex-fitsize-unconstrained-multi-init-RSV7"
"flag-fitindex-fitsize-unconstrained-multi-init-RSV6"
"flag-fitindex-fitsize-unconstrained-multi-init-RSV8"
"flag-fitindex-fitsize-unconstrained-multi-init-RSV"
"flag-fitindex-fitsize-unconstrained-multi-init-PGD-c1-eta-delta-2-secondary"
"flag-fitindex-fitsize-unconstrained-multi-init-NAG-momentum-040-lr5E-3-nstep-1000zg"
"flag-fitindex-fitsize-unconstrained-multi-init-NAG-momentum-040-lr2E-4-nstep-1000zg"
"flag-fitindex-fitsize-unconstrained-multi-init-NAG-momentum-040-lrE-3-nstep-1000zg"
"flag-fitindex-fitsize-unconstrained-multi-init-NAG-momentum-080-lr5E-3-nstep-1000zg"
"flag-fitindex-fitsize-unconstrained-multi-init-NAG-momentum-080-lr2E-4-nstep-1000zg"
"flag-fitindex-fitsize-unconstrained-multi-init-NAG-momentum-080-lrE-3-nstep-1000zg"
"flag-fitindex-fitsize-unconstrained-multi-init-NAG-momentum-060-lr5E-3-nstep-1000zg"
"flag-fitindex-fitsize-unconstrained-multi-init-NAG-momentum-060-lr2E-4-nstep-1000zg"
"flag-fitindex-fitsize-unconstrained-multi-init-NAG-momentum-060-lrE-3-nstep-1000zg"
"flag-fitindex-fitsize-unconstrained-multi-init-PGD-c1-eta-delta-2"
"flag-fitindex-fitsize-unconstrained-multi-init-flex-adv-masking-v4-hqn-extended-Gamma"
"flag-fitindex-fitsize-unconstrained-multi-init-flex-adv-masking-v4-lm"
"flag-fitindex-fitsize-unconstrained-multi-init-flex-adv-masking-v4"
"flag-fitindex-fitsize-fitshape-fitcenter-unconstrained-multi-init-5-adv-masking-v4"
"flag-fitsize-unconstrained-multi-init-1-nstep-9000-double-apsector-lockre-seleted-pix-maskA"
"flag-fitsize-unconstrained-multi-init-1-nstep-9000-double-apsector-lockre-seleted-pix-maskB"
"flag-fitsize-unconstrained-multi-init-1-nstep-9000-double-apsector-lockre-seleted-pix-maskC"
"flag-fitsize-unconstrained-multi-init-1-nstep-9000-double-apsector-lockre-seleted-pix-maskD"
"flag-fitsize-unconstrained-multi-init-1-nstep-9000-double-apsector-lockre-seleted-pix-maskE"
"flag-fitsize-unconstrained-multi-init-1-nstep-9000-double-apsector-lockre-full-sky-good-pix-memory-limited-selected5k"
"flag-fitsize-unconstrained-multi-init-1-nstep-9000-long-double-apsector-lockre-full-sky-good-pix-memory-limited-selected4"
"flag-fitsize-unconstrained-multi-init-1-nstep-9000-float-apsector-lockre-full-sky-good-pix-memory-limited-selected4") 
n6=4

declare -a cd_model_a=("sersic-sersic-n-0.2" "sersic-sersic-n-0.5" "sersic-sersic-n-1" "sersic-sersic-n-2" "sersic-sersic-n-3" "coresersic" "sersic-sersic" "sersic-Exp" "sersic" "deV" "deV-deV" "deV-Pl" "deV" "deV-deV" "deV")
declare -a cd_class_a=("SERSICSERSIC_GCLASS" "SERSICSERSIC_GCLASS" "SERSICSERSIC_GCLASS" "SERSICSERSIC_GCLASS" "SERSICSERSIC_GCLASS" "CORESERSIC_GCLASS" "SERSICSERSIC_GCLASS" "SERSICEXP_GCLASS" "SERSIC_GCLASS" "DEV_GCLASS"  "DEVDEV_GCLASS" "DEVPL_GCLASS" "DEV_GCLASS" "DEVDEV_GCLASS" "DEV_GCLASS")
declare -a transform_flag_a=("'-SERSIC2_INDEX_SMALL 0.2 -SERSIC2_INDEX_LARGE 0.201'" "'-SERSIC2_INDEX_SMALL 0.5 -SERSIC2_INDEX_LARGE 0.501'" "'-SERSIC2_INDEX_SMALL 1.0 -SERSIC2_INDEX_LARGE 1.01'" "'-SERSIC2_INDEX_SMALL 2.0 -SERSIC2_INDEX_LARGE 2.01'" "'-SERSIC2_INDEX_SMALL 3.0 -SERSIC2_INDEX_LARGE 3.01'" "''" "''" "''" "''" "''" "''" "''" "''" "''" "''" "''" "''")
n7=5

declare -a cd_model_a=("sersic-sersic-n-1" "sersic-sersic-n-2" "sersic-sersic-n-3" "coresersic" "sersic-sersic" "sersic-Exp" "sersic" "deV" "deV-deV" "deV-Pl" "deV" "deV-deV" "deV")
declare -a cd_class_a=("SERSICSERSIC_GCLASS" "SERSICSERSIC_GCLASS" "SERSICSERSIC_GCLASS" "SERSICSERSIC_GCLASS" "SERSICSERSIC_GCLASS" "CORESERSIC_GCLASS" "SERSICSERSIC_GCLASS" "SERSICEXP_GCLASS" "SERSIC_GCLASS" "DEV_GCLASS"  "DEVDEV_GCLASS" "DEVPL_GCLASS" "DEV_GCLASS" "DEVDEV_GCLASS" "DEV_GCLASS")
declare -a transform_flag_a=("'-SERSIC2_INDEX_SMALL 1.0 -SERSIC2_INDEX_LARGE 1.01'" "'-SERSIC2_INDEX_SMALL 2.0 -SERSIC2_INDEX_LARGE 2.01'" "'-SERSIC2_INDEX_SMALL 3.0 -SERSIC2_INDEX_LARGE 3.01'" "''" "''" "''" "''" "''" "''" "''" "''" "''" "''" "''" "''")
n7=1

declare -a cd_model_a=("sersic-sersic" "sersic-Exp" "sersic" "deV" "deV-deV" "deV-Pl" "deV" "deV-deV" "deV")
declare -a cd_class_a=("SERSICSERSIC_GCLASS" "SERSICEXP_GCLASS" "SERSIC_GCLASS" "DEV_GCLASS"  "DEVDEV_GCLASS" "DEVPL_GCLASS" "DEV_GCLASS" "DEVDEV_GCLASS" "DEV_GCLASS")
declare -a transform_flag_a=("''" "''" "''" "''" "''" "''" "''" "''" "''" "''" "''" "''")
n7=4



for (( i7=0; i7<$n7; i7++ ))
do

for (( i0=0; i0<$n0; i0++ ))
do

for (( i1=0; i1<$n1; i1++ ))
do

for (( i2=0; i2<$n2; i2++ ))
do

for (( i3=0; i3<$n3; i3++ ))
do

for (( i4=0; i4<$n4; i4++ ))
do

for (( i5=0; i5<$n5; i5++ ))
do

for (( i6=0; i6<$n6; i6++ ))
do

camcol=${camcol_a[$i0]}

source_model=${source_model_a[$i1]}

source=${source_a[$i2]}
fit2fpCC_flag=${fit2fpCC_flag_a[$i2]}

target_model=${target_model_a[$i3]}
target_class=${target_class_a[$i3]}

nobjc_flag=${nobjc_flag_a[$i4]}
nobjcstr=${nobjcstr_a[$i4]}
multistage_flag=${multistageflag_a[$i4]}
stage_name=${stage_a[$i4]}

run=${run_a[$i5]}

fitflag=${fitflags_a[$i6]}
dirflag=${dirflags_a[$i6]}
executable=${executable_a[$i6]} # for optimization of multi-init runs

cdmodel=${cd_model_a[$i7]}
cdclass=${cd_class_a[$i7]}
transformflag=${transform_flag_a[$i7]}

	root="/u/khosrow/thesis/opt/soho/multi-object/images"
	outdir="$root/mle/$source_model/CD-$cdmodel-G-$target_model-$source/$stage_name/$dirflag/$rerun/$run/$camcol"
	datadir="$root/$rerun/$run/$camcol"
	source_objcsdir="$CPHOTO_OBJCS/$rerun/$run/$camcol"
	source_framesdir="$CPHOTO_FRAMES/$rerun/$run/$camcol"
	source_psfdir="$CPHOTO_REDUX/$rerun/$run/objcs/$camcol"

	field0="0001"
	field1="5100"

	#init_flag="SDSSpERROR_INIT -error 2"
	init_flag="SDSS_INIT"
	maxseed=4294967295 # the biggest unsigned int that can be passed as a seed to simulator 
	seed=$(python -S -c "import random; print(random.randrange(1,$maxseed))")

	extra_flags="-nolockre $fitflag $multistage_flag -gclass $target_class -init $init_flag $fit2fpCC_flag -cD_classify -cdclass $cdclass -ctransform $transformflag"

	mle-data-script.sh --run=$run --rerun=$rerun --outdir="$outdir" --source_objcsdir="$source_objcsdir" --source_framesdir="$source_framesdir" --source_psfdir="$source_psfdir" --model=$source_model --EXTRA_FLAGS="$extra_flags" --executable=$executable --camcol=$camcol --commandfile=$commandfile --multiband

done
done
done
done
done
done
done
done


echo "$name: NOT randomly running commands in file '$commandfile'"
#run_commands_in_file --commandfile=$commandfile --random
#grep deV-Pl $commandfile > "$commandfile.deV-Pl"
#grep -v deV-Pl $commandfile > "$commandfile.deV"
#paste -d ':' "$commandfile.deV-Pl" "$commandfile.deV" > "$commandfile.deV-Pl.deV"
#shuf "$commandfile.deV-Pl.deV" --output "$commandfile.deV-Pl.deV.shuf"
#sed 's/:/\n/g' "$commandfile.deV-Pl.deV.shuf" > "$commandfile.deV-Pl.deV.shuf.sep"
#chmod u+x "$commandfile.deV-Pl.deV.shuf.sep"
#echo "$name: NOT running randomdized pairs at '$commandfile.deV-Pl.deV.shuf.sep'"
#bash "./$commandfile.deV-Pl.deV.shuf.sep"

pastecommand="paste -d ':'"
for (( i7=0; i7<$n7; i7++ ))
do

cdmodel=${cd_model_a[$i7]}
searchstring="CD-$cdmodel-G"

grep "$searchstring" $commandfile > "$commandfile.$searchstring"
pastecommand="$pastecommand $commandfile.$searchstring"
done

pastecommand="$pastecommand > $commandfile.all"
eval "$pastecommand"
#paste -d ':' "$commandfile.CD-*-G" > "$commandfile.all"
shuf "$commandfile.all" --output "$commandfile.all.shuf"
sed 's/:/\n/g' "$commandfile.all.shuf" > "$commandfile.all.shuf.sep"
chmod u+x "$commandfile.all.shuf.sep"
echo "$name: NOT running randomdized pairs at '$commandfile.all.shuf.sep'"
#bash "./$commandfile.all.shuf.sep"

#grep fitshape $commandfile > "$commandfile.fitshape"
#grep -v fitshape $commandfile > "$commandfile.fitsize"
#paste -d ':' "$commandfile.fitshape" "$commandfile.fitsize" > "$commandfile.fitshape.fitsize"
#shuf "$commandfile.fitshape.fitsize" --output "$commandfile.fitshape.fitsize.shuf"
#sed 's/:/\n/g' "$commandfile.fitshape.fitsize.shuf" > "$commandfile.fitshape.fitsize.shuf.sep"
#chmod u+x "$commandfile.fitshape.fitsize.shuf.sep"
#echo "$name: NOT running randomdized pairs at '$commandfile.fitshape.fitsize.shuf.sep'"
#bash "./$commandfile.deV-Pl.deV.shuf.sep"




#shuf "$commandfile" --output "$commandfile.shuf"
#chmod u+x "$commandfile.shuf"
#echo "$name: running randomdized commands at '$commandfile.shuf"
#bash "./$commandfile.shuf"




echo "$name: not deleting temp-file = $commandfile"
#rm $commandfile

