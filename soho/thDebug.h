#ifndef THDEBUG_H
#define THDEBUG_H



#include "strings.h"
#include "sohoEnv.h"
#include "dervish.h"

#define DEBUG_DUMP_LM 1

#define DEBUG_PROC_IO 0
#define DEBUG_PROC_LOAD_DATA 1
#define DEBUG_PROC_PRINT_WEIGHT_STATS 0 /* print statistics of the image and weight pixels */
#define DEBUG_PROC_ATLAS_LOAD 0
#define DEEP_DEBUG_PROC_ATLAS_LOAD 0
#define DEBUG_PROC_PHOBJC_INFO 0
#define DEEP_DEBUG_PROC_PHOBJC_INFO 0
#define DEBUG_IO_OBJC_IO 0 /* whether to output list of objects read in IO */
#define DEBUG_IO_CALIB_PHOBJC_IO 0 /* whether to output list of objects read in IO */

#define DEBUG_CTRANSFORM 0
#define DEEP_VERBOSE_CTRANSFORM 0

#define DEBUG_MAPTYPES 0

#define VERBOSE_CAPTURE 1
#define DEBUG_CAPTURE 0
#define DEEP_DEBUG_CAPTURE 0
#define VERY_DEEP_DEBUG_CAPTURE 0
#define INTRUSIVE_DEBUG_CAPTURE 0

#define DEEP_DEBUG_MATH 0
#define DEBUG_MATH 0
#define DEBUG_MATH_2 0
#define DEBUG_MATH_SINGULAR 0

#define DEBUG_MLETYPES 0
#define DEEP_DEBUG_MLETYPES 0
#define VERY_DEEP_DEBUG_MLETYPES 0
#define VERBOSE_MLETYPES 0
#define DEBUG_MLETYPES_HIGHER_ORDER 1
#define DEBUG_BRACKET 0
#define DEBUG_MLETYPES_LPROFILE 0

#define DEBUG_QQ_TRANS 0

#define DEBUG_MLE 1
#define DEEP_DEBUG_MLE 0
#define VERY_DEEP_DEBUG_MLE 0
#define VERBOSE_MLE 0
#define DEBUG_MLE_HIGHER_ORDER 1
#define DEBUG_CHISQ 1
#define DEBUG_CHISQ_KAHAN  0
#define DEEP_DEBUG_CHISQ 0
#define VERY_DEEP_DEBUG_CHISQ 0
#define DEBUG_MLE_MEMORY 0
#define DEBUG_MLE_LPROFILE 1
#define DEBUG_MLE_COV 0
#define DEBUG_MLE_ALGORITHM 0
#define DEBUG_MLE_PRINT_MIMJ 0
#define DEBUG_MLE_QN 0
#define DEBUG_MLE_SINGULAR 0

#define DEBUG_THFITMASKTYPES 0

#define DEBUG_MEMORY 0
#define DEBUG_MEMORY_VERBOSE 0

#define DEBUG_PROFILE 0
#define DEEP_DEBUG_PROFILE 0
#define VERY_DEEP_DEBUG_PROFILE 0

#define DEBUG_MSKY 0
#define DEEP_DEBUG_MSKY 1

#define DEBUG_MODELTYPES 0

#define DEBUG_MGPROFILE 0
#define DEEP_DEBUG_MGPROFILE 0

#define DEBUG_MGALAXY 0
#define DEEP_DEBUG_MGALAXY 0

#define DEBUG_PSFTYPES 0
#define DEEP_DEBUG_PSFTYPES 0

#define DEBUG_PSF 0
#define DEEP_DEBUG_PSF 0
#define DEBUG_PSF_2 0
#define DEBUG_PSF_3 0
#define VERBOSE_PSF 0

#define DEBUG_ALGORITHM 1
#define DEEP_DEBUG_ALGORITHM 0 
#define VERBOSE_ALGORITHM 0

#define DEBUG_REGION_TYPES 1

#define VERBOSE_REGION 0

#define VERBOSE_DUMP 1

#define DEBUG_MAIN_PSF 0

#define DEBUG_PCOST 1

#define DEBUG_LRG 0

RET_CODE thIoFpcDebug(char *name, 
		      HDR *header, REGION *reg);

RET_CODE thPrintHdr(HDR *header);

RET_CODE thPrintReg(REGION *reg);

RET_CODE thErrShowStack();

RET_CODE thDebugWriteCalibPhObjcIo(char *filename, HDR *hdr);

#endif
