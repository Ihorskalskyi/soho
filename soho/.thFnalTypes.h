#ifndef THFNALTYPES_H
#define THFNALTYPES_H                                                                                                                              
#include "dervish.h"                                            
#include "phPhotoFitsIO.h"
#include "phSpanUtil.h"        
#include "phObjc.h"
                                  
#include "thConsts.h"                                                          

#define thFnalObjcIoNew thFnal_objc_ioNew
#define thFnalObjcIoDel thFnal_objc_ioDel

typedef struct fnal_objc_io {
   int id;				/* id number for this objc */
   int parent;			        /* id of parent for deblends */
   const int ncolor;			/* number of colours */
   int nchild;				/* number of children */
   OBJ_TYPE objc_type;			/* overall classification */
   float objc_prob_psf;			/* Bayesian probability of being PSF */
   int catID;			        /* catalog id number */
   int objc_flags;			/* flags from OBJC */
   int objc_flags2;			/* flags2 from OBJC */
   float objc_rowc, objc_rowcErr;	/* row position and error of centre */
   float objc_colc, objc_colcErr;	/* column position and error */
   float rowv, rowvErr;			/* row velocity, in pixels/frame (NS)*/
   float colv, colvErr;			/* col velocity, in pixels/frame (NS)*/

   ATLAS_IMAGE *aimage;			/* the atlas image */
   struct test_info *test;		/* information for testers */
/*
 * Unpacked OBJECT1s
 */
   float rowc[NCOLOR], rowcErr[NCOLOR];
   float colc[NCOLOR], colcErr[NCOLOR];
   float sky[NCOLOR], skyErr[NCOLOR];
/*
 * PSF and aperture fits/magnitudes
 */
   float psfCounts[NCOLOR], psfCountsErr[NCOLOR];
   float fiberCounts[NCOLOR], fiberCountsErr[NCOLOR];
   float fiber2Counts[NCOLOR], fiber2CountsErr[NCOLOR]; /* these two fields are missing in Princeton copy */
   float petroCounts[NCOLOR], petroCountsErr[NCOLOR];
   float petroRad[NCOLOR], petroRadErr[NCOLOR];
   float petroR50[NCOLOR], petroR50Err[NCOLOR];
   float petroR90[NCOLOR], petroR90Err[NCOLOR];
/*
 * Shape of object
 */
   float Q[NCOLOR], QErr[NCOLOR], U[NCOLOR], UErr[NCOLOR];

   float M_e1[NCOLOR], M_e2[NCOLOR];
   float M_e1e1Err[NCOLOR], M_e1e2Err[NCOLOR], M_e2e2Err[NCOLOR];
   float M_rr_cc[NCOLOR], M_rr_ccErr[NCOLOR];
   float M_cr4[NCOLOR];
   float M_e1_psf[NCOLOR], M_e2_psf[NCOLOR];
   float M_rr_cc_psf[NCOLOR];
   float M_cr4_psf[NCOLOR];
/*
 * Properties of a certain isophote.
 */
   float iso_rowc[NCOLOR], iso_rowcErr[NCOLOR], iso_rowcGrad[NCOLOR];
   float iso_colc[NCOLOR], iso_colcErr[NCOLOR], iso_colcGrad[NCOLOR];
   float iso_a[NCOLOR], iso_aErr[NCOLOR], iso_aGrad[NCOLOR];
   float iso_b[NCOLOR], iso_bErr[NCOLOR], iso_bGrad[NCOLOR];
   float iso_phi[NCOLOR], iso_phiErr[NCOLOR], iso_phiGrad[NCOLOR];
/*
 * Model parameters for deV and exponential models
 */
   float r_deV[NCOLOR], r_deVErr[NCOLOR];
   float ab_deV[NCOLOR], ab_deVErr[NCOLOR];
   float phi_deV[NCOLOR], phi_deVErr[NCOLOR];
   float counts_deV[NCOLOR], counts_deVErr[NCOLOR];
   float r_exp[NCOLOR], r_expErr[NCOLOR];
   float ab_exp[NCOLOR], ab_expErr[NCOLOR];
   float phi_exp[NCOLOR], phi_expErr[NCOLOR];
   float counts_exp[NCOLOR], counts_expErr[NCOLOR];

   float counts_model[NCOLOR], counts_modelErr[NCOLOR];

/* the deV2, exp2 and pl (power law) models are added to model cD's. not available originally in the PHOTO-dispatch */
   float r_deV2[NCOLOR], r_deV2Err[NCOLOR];
   float ab_deV2[NCOLOR], ab_deV2Err[NCOLOR];
   float phi_deV2[NCOLOR], phi_deV2Err[NCOLOR];
   float counts_deV2[NCOLOR], counts_deV2Err[NCOLOR];

   float r_exp2[NCOLOR], r_exp2Err[NCOLOR];
   float ab_exp2[NCOLOR], ab_exp2Err[NCOLOR];
   float phi_exp2[NCOLOR], phi_exp2Err[NCOLOR];
   float counts_exp2[NCOLOR], counts_exp2Err[NCOLOR];

   float r_pl[NCOLOR], r_plErr[NCOLOR];
   float ab_pl[NCOLOR], ab_plErr[NCOLOR];
   float phi_pl[NCOLOR], phi_plErr[NCOLOR];
   float n_pl[NCOLOR], n_plErr[NCOLOR];
   float counts_pl[NCOLOR], counts_plErr[NCOLOR];

   float r_csersic[NCOLOR], r_csersicErr[NCOLOR];
   float rb_csersic[NCOLOR], rb_csersicErr[NCOLOR];
   float ab_csersic[NCOLOR], ab_csersicErr[NCOLOR];
   float phi_csersic[NCOLOR], phi_csersicErr[NCOLOR];
   float n_csersic[NCOLOR], n_csersicErr[NCOLOR];
   float g_csersic[NCOLOR], g_csersicErr[NCOLOR];
   float d_csersic[NCOLOR], d_csersicErr[NCOLOR];
   float counts_csersic[NCOLOR], counts_csersicErr[NCOLOR];

   float counts_model2[NCOLOR], counts_model2Err[NCOLOR];
/*
 * Measures of image structure
 */
   float texture[NCOLOR];
/*
 * Classification information
 */
   float star_L[NCOLOR], star_lnL[NCOLOR];
   float exp_L[NCOLOR], exp_lnL[NCOLOR];
   float deV_L[NCOLOR], deV_lnL[NCOLOR];
   float fracPSF[NCOLOR];

   int flags[NCOLOR];
   int flags2[NCOLOR];

   OBJ_TYPE type[NCOLOR];
   float prob_psf[NCOLOR];
/*
 * Profile and extent of object
 */
   int nprof[NCOLOR];
   float profMean[NCOLOR][NANN];
   float profErr[NCOLOR][NANN];

/* the following flag was added by Khosrow Akbari on August 2016 */
   int flagLRG;

} FNAL_OBJC_IO;				/* pragma SCHEMA */



FNAL_OBJC_IO *thFnalObjcIoNew(int ncolor);
void thFnalObjcIoDel(FNAL_OBJC_IO *fnal_objc_io, int deep);
RET_CODE thFnalObjcIoCopyToObjcIo(OBJC_IO *new, FNAL_OBJC_IO *obj1);
RET_CODE thObjcIoCopyToFnalObjcIo(FNAL_OBJC_IO *new, OBJC_IO *obj1);

#endif
