RET_CODE MSkyInit06( SBINDEX *sbindex) {
char *name = "MSkyInit06";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 0;
sbindex->mcol = SKY_N2; sbindex->icol = 6;
return(SH_SUCCESS);
}

RET_CODE MSkyInit16( SBINDEX *sbindex) {
char *name = "MSkyInit16";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 1;
sbindex->mcol = SKY_N2; sbindex->icol = 6;
return(SH_SUCCESS);
}

RET_CODE MSkyInit26( SBINDEX *sbindex) {
char *name = "MSkyInit26";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 2;
sbindex->mcol = SKY_N2; sbindex->icol = 6;
return(SH_SUCCESS);
}

RET_CODE MSkyInit36( SBINDEX *sbindex) {
char *name = "MSkyInit36";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 3;
sbindex->mcol = SKY_N2; sbindex->icol = 6;
return(SH_SUCCESS);
}

RET_CODE MSkyInit46( SBINDEX *sbindex) {
char *name = "MSkyInit46";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 4;
sbindex->mcol = SKY_N2; sbindex->icol = 6;
return(SH_SUCCESS);
}

RET_CODE MSkyInit56( SBINDEX *sbindex) {
char *name = "MSkyInit56";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 5;
sbindex->mcol = SKY_N2; sbindex->icol = 6;
return(SH_SUCCESS);
}

RET_CODE MSkyInit66( SBINDEX *sbindex) {
char *name = "MSkyInit66";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 6;
sbindex->mcol = SKY_N2; sbindex->icol = 6;
return(SH_SUCCESS);
}



