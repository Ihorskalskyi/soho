#include "thNaiveMaskTypes.h"

static const SCHEMA *schema_naive_mask_type = NULL;
static NAIVE_MASK *wmask = NULL;

static void init_static_vars (void);
static int compare_int(const void *x1, const void *x2);

void init_static_vars (void) {
	TYPE t;
	t = shTypeGetFromName("NAIVE_MASK");
	shAssert(t != UNKNOWN_SCHEMA);
	schema_naive_mask_type = shSchemaGetFromType(t);
	if (wmask == NULL) {
		wmask = thNaiveMaskNew();
	}
	return;
}

int compare_int(const void *x1, const void *x2) {
	/* ascending order if used with qsort */
	int i1, i2;
	i1 = * (int *) x1;
	i2 = * (int *) x2;
	return(i1 - i2);
}


/* basic handlers  */

NAIVE_MASK *thNaiveMaskNew() {
	NAIVE_MASK *m = thCalloc(1, sizeof(NAIVE_MASK));
	m->type = UNKNOWN_NAIVE_MASK;
	return(m);
}

void thNaiveMaskDel(NAIVE_MASK *m) {
	if (m == NULL) return;
	thFree(m);
	return;
}

RET_CODE thNaiveMaskCopy(NAIVE_MASK *t, NAIVE_MASK *s) {
char *name = "thNaiveMaskCopy";
if (s == NULL && t == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
}
if (t == NULL) {
	thError("%s: ERROR - null target to copy", name);
	return(SH_GENERIC_ERROR);
}
if (s == NULL) {
	thError("%s: ERROR - null source to copy", name);
	return(SH_GENERIC_ERROR);
}
memcpy(t, s, sizeof(NAIVE_MASK));
return(SH_SUCCESS);
}

RET_CODE thNaiveMaskReset(NAIVE_MASK *m) {
char *name = "thNaiveMaskReset";
if (m == NULL) {
	thError("%s: WARNING - null input", name);
	return(SH_SUCCESS);
}

m->type = UNKNOWN_NAIVE_MASK;
m->nspan = 0;
memset(m->x0, 0, N_ELEM_NAIVE_MASK * sizeof(int));
memset(m->x1, 0, N_ELEM_NAIVE_MASK * sizeof(int));
memset(m->y0, 0, N_ELEM_NAIVE_MASK * sizeof(int));
memset(m->y1, 0, N_ELEM_NAIVE_MASK * sizeof(int));

return(SH_SUCCESS);
}

RET_CODE thNaiveMaskGetNspan(NAIVE_MASK *m, int *nspan) {
char *name = "thNaiveMaskGetNspan";
if (m == NULL && nspan == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
}
if (m == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_SUCCESS);
}
if (nspan == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (m->type != RECTANGLE && m->type != RING) {
	if (schema_naive_mask_type == NULL) init_static_vars();
	thError("%s: ERROR - unsupported (naive_mask_type) '%s'", name, (schema_naive_mask_type->elems[m->type]).name);
	return(SH_GENERIC_ERROR);
}
if (m->nspan < 0 || m->nspan > N_ELEM_NAIVE_MASK) {
	thError("%s: ERROR - problematic (nspan = %d) discovered", name, m->nspan);
	return(SH_GENERIC_ERROR);
}
if (m->type == RECTANGLE && m->nspan > 1) {
	thError("%s: WARNING - discovered a rectangular naive mask where (nspan = %d)", name, m->nspan);
}

*nspan = m->nspan;
return(SH_SUCCESS);
}

RET_CODE thNaiveMaskGetElement(NAIVE_MASK *m, int s, int *x0, int *x1, int *y0, int *y1) {
char *name = "thNaiveMaskGetElement";
if (m == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
int nspan = m->nspan;
if (nspan < 0 || nspan > N_ELEM_NAIVE_MASK) {
	thError("%s:E RROR - problematic (nspan = %d) discovered", name, nspan);
	return(SH_GENERIC_ERROR);
}
if (s >= nspan || s < 0) {
	thError("%s: ERROR - span number (s = %d) out of range for (nspan = %d)", name, s, nspan);
	return(SH_GENERIC_ERROR);
}
if (x0 != NULL) *x0 = m->x0[s];
if (x1 != NULL) *x1 = m->x1[s];
if (y0 != NULL) *y0 = m->y0[s];
if (y1 != NULL) *y1 = m->y1[s];

if (x0 == NULL && x1 == NULL && y0 == NULL && y1 == NULL) {
	thError("%s: WARNING - null output placeholder", name);
}
return(SH_SUCCESS);
}

/* upper level manipulations */

RET_CODE thNaiveMaskFromRect(NAIVE_MASK *m, int x0, int x1, int y0, int y1) {
char *name = "thNaiveMaskFromRect";
if (m == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status = thNaiveMaskReset(m);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not reset mask", name);
	return(status);
}
m->nspan = 1;
m->type = RECTANGLE;
m->x0[0] = MIN(x0, x1);
m->x1[0] = MAX(x1, x0);
m->y0[0] = MIN(y0, y1);
m->y1[0] = MAX(y1, y0);

if (x0 > x1) {
	thError("%s: WARNING - switched the order of x-component", name);
} 
if (y0 > y1) {
	thError("%s: WARNING - switched the order of y-component", name);
}
return(SH_SUCCESS);
}

RET_CODE thNaiveMaskAndNaiveMask(NAIVE_MASK *t, NAIVE_MASK *m) {
char *name = "thNaiveMaskAndNaiveMask";
if (m == NULL && t == NULL) {
	thError("%s: WARNING - null input", name);
	return(SH_SUCCESS);
}
if (t == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (m == NULL) {
	thError("%s: WARNING - null secondary input mask", name);
	return(SH_SUCCESS);
}
NAIVE_MASK_TYPE t_type, m_type;
t_type = t->type;
m_type = m->type;

int condition1, condition2, condition3;
condition1 = (t_type == RECTANGLE && m_type == RECTANGLE);
condition2 = (t_type == RING && m_type == RECTANGLE);
condition3 = (t_type == RECTANGLE && m_type == RING);

if (!condition1 && !condition2 && !condition3) {
	if (schema_naive_mask_type == NULL) init_static_vars ();
	thError("%s: ERROR - current version doesn't support two mask types (t, m) '%s', '%s' together", name,
		(schema_naive_mask_type->elems[t_type]).name, (schema_naive_mask_type->elems[m_type]).name);
	return(SH_GENERIC_ERROR);
}
static int x[4], y[4];
static int span[N_ELEM_NAIVE_MASK];
int mdx, mdy, tdx, tdy, dx, dy;
int s, mnspan, tnspan, nspan = 0;
mnspan = m->nspan;
tnspan = t->nspan;
if (mnspan == 0 || tnspan == 0) {
	t->nspan = 0;
	return(SH_SUCCESS);
}

if (condition1) {

	x[0] = m->x0[0];
	x[1] = m->x1[0];
	x[2] = t->x0[0];
	x[3] = t->x1[0];
	mdx = x[1] - x[0] + 1;
	tdx = x[3] - x[2] + 1;

	y[0] = m->y0[0];
	y[1] = m->y1[0];
	y[2] = t->y0[0];
	y[3] = t->y1[0];
	mdy = y[1] - y[0] + 1;
	tdy = y[3] - y[2] + 1;	

	qsort(x, 4, sizeof(int), &compare_int);
	qsort(y, 4, sizeof(int), &compare_int);
	dx = x[3] - x[0] + 1;
	dy = y[3] - y[0] + 1;
	if (dx < mdx + tdx && dy < mdy + tdy) {
		t->x0[0] = x[1];
		t->x1[0] = x[2];
		t->y0[0] = y[1];
		t->y1[0] = y[2];
		t->nspan = 1;
		t->type = RECTANGLE;
	} else {
		t->nspan = 0;
		t->type = RECTANGLE;
	}
	return(SH_SUCCESS);
}

RET_CODE status;
if (wmask == NULL) {
	init_static_vars();
} else {
	status = thNaiveMaskReset(wmask);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not reset working naive mask (wmask)", name);
		return(status);
	}
}

if (condition2) {
	mdx = m->x1[0] - m->x0[0] + 1;
	mdy = m->y1[0] - m->y0[0] + 1;	

	for (s = 0; s < tnspan; s++) {

		x[2] = m->x0[0];
		x[3] = m->x1[0];
		y[2] = m->y0[0];
		y[3] = m->y1[0];

		x[0] = t->x0[s];
		x[1] = t->x1[s];
		y[0] = t->y0[s];		
		y[1] = t->y1[s];
		tdx = x[1] - x[0] + 1;
		tdy = y[1] - y[0] + 1;
	
		qsort(x, 4, sizeof(int), &compare_int);
		qsort(y, 4, sizeof(int), &compare_int);
		dx = x[3] - x[0] + 1;
		dy = y[3] - y[0] + 1;
			
		if (dx < mdx + tdx && dy < mdy + tdy) {
			wmask->x0[s] = x[1];
			wmask->x1[s] = x[2];
			wmask->y0[s] = y[1];
			wmask->y1[s] = y[2];
			nspan++;
			span[s] = 1;	
		} else { 
			span[s] = 0;
		}
	}
	t->nspan = nspan;
	if (nspan <= 1) {
		t->type = RECTANGLE;
	} else {
		t->type = RING;
	}
	int ss = 0;
	for (s = 0; s < tnspan; s++) {
		if (span[s]) {
			t->x0[ss] = wmask->x0[s];
			t->x1[ss] = wmask->x1[s];
			t->y0[ss] = wmask->y0[s];
			t->y1[ss] = wmask->y1[s];
			ss++;
		}			
	}
	if (ss != nspan) {
		thError("%s: ERROR - source code problem", name);
		return(SH_GENERIC_ERROR);
	}
	return(SH_SUCCESS);
}

if (condition3) {
	tdx = t->x1[0] - t->x0[0] + 1;
	tdy = t->y1[0] - t->y0[0] + 1;	

	for (s = 0; s < mnspan; s++) {
		x[2] = t->x0[0];
		x[3] = t->x1[0];
		y[2] = t->y0[0];
		y[3] = t->y1[0];


		x[0] = m->x0[s];
		x[1] = m->x1[s];
		y[0] = m->y0[s];		
		y[1] = m->y1[s];
		mdx = x[1] - x[0] + 1;
		mdy = y[1] - y[0] + 1;
	
		qsort(x, 4, sizeof(int), &compare_int);
		qsort(y, 4, sizeof(int), &compare_int);
		dx = x[3] - x[0] + 1;
		dy = y[3] - y[0] + 1;
			
		if (dx < mdx + tdx && dy < mdy + tdy) {
			wmask->x0[s] = x[1];
			wmask->x1[s] = x[2];
			wmask->y0[s] = y[1];
			wmask->y1[s] = y[2];
			nspan++;
			span[s] = 1;	
		} else { 
			span[s] = 0;
		}
	}
	t->nspan = nspan;
	if (nspan <= 1) {
		t->type = RECTANGLE;
	} else {
		t->type = RING;
	}
	int ss = 0;
	for (s = 0; s < mnspan; s++) {
		if (span[s]) {
			t->x0[ss] = wmask->x0[s];
			t->x1[ss] = wmask->x1[s];
			t->y0[ss] = wmask->y0[s];
			t->y1[ss] = wmask->y1[s];
			ss++;
		}			
	}
	if (ss != nspan) {
		thError("%s: ERROR - source code problem", name);
		return(SH_GENERIC_ERROR);
	}

	return(SH_SUCCESS);
}


thError("%s: ERROR - source code error - conditions not exhausted", name);
return(SH_GENERIC_ERROR);

}

RET_CODE thNaiveMaskNotIntersectionNaiveMask(NAIVE_MASK *t, NAIVE_MASK *m1, NAIVE_MASK *m2) {
char *name = "thNaiveMaskNotIntersectionNaiveMask";
if (t == NULL && m1 == NULL && m2 == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
}
if (t == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_SUCCESS);
}
RET_CODE status;
status = thNaiveMaskReset(t);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not reset (t)", name);
	return(status);
}
if (m1 == NULL) {
	t->nspan = 0;
	t->type = RECTANGLE;
	return(SH_SUCCESS);
}
if (m2 == NULL) {
	status = thNaiveMaskCopy(t, m1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy (m1) onto (t)", name);
		return(status);
	}
	return(SH_SUCCESS);
}
int nspan1, nspan2;
nspan1 = m1->nspan;
nspan2 = m2->nspan;
if (nspan1 == 0) {
	t->nspan = 0;
	t->type = RECTANGLE;
	return(SH_SUCCESS);
}
if (nspan2 == 0) {
	t->nspan = 0;
	t->type = RECTANGLE;
	return(SH_SUCCESS);
}
NAIVE_MASK_TYPE type1, type2;
type1 = m1->type;
type2 = m2->type;
int condition;
condition = (type1 == RECTANGLE && type2 == RECTANGLE);
if (!condition) {
	if (schema_naive_mask_type == NULL) init_static_vars();
	thError("%s: ERROR - mask type combination for (m1, m2) '%s', '%s' not supported", name,
		(schema_naive_mask_type->elems[type1]).name, (schema_naive_mask_type->elems[type2]).name);
	return(SH_GENERIC_ERROR);
}
if (nspan1 != 1 || nspan2 != 1) {
	thError("%s: ERROR - unaaceptable number of spans for (m1, m2) = (%d, %d)", name, nspan1, nspan2);
	return(SH_GENERIC_ERROR);
}
int nspan = 0;
int wy = MIN(m2->y0[0] - 1, m1->y1[0]);
if (wy >= m1->y0[0]) {
	t->x0[nspan] = m1->x0[0];
	t->x1[nspan] = m1->x1[0];
	t->y0[nspan] = m1->y0[0];
	t->y1[nspan] = wy;
	nspan++;
} 
wy++;
wy = MAX(wy, m1->y0[0]);
int wwy = m2->y1[0];
wwy = MIN(wwy, m1->y1[0]);
if (wwy >= wy) {
	int wx = MIN(m2->x0[0] - 1, m1->x1[0]);
	if (wx >= m1->x0[0]) {
		t->x0[nspan] = m1->x0[0];
		t->x1[nspan] = wx;
		t->y0[nspan] = wy;
		t->y1[nspan] = wwy;
		nspan++;
	}
	wx = MIN(m2->x1[0] + 1, m1->x1[0]);
	if (wx >= m1->x0[0]) {
		t->x0[nspan] = wx;
		t->x1[nspan] = m1->x1[0];
		t->y0[nspan] = wy;
		t->y1[nspan] = wwy;
		nspan++;
	}
}
wwy++;
wwy = MAX(wwy, m1->y0[0]);
if (wwy <= m1->y1[0]) {
	t->x0[nspan] = m1->x0[0];
	t->x1[nspan] = m1->x1[0];
	t->y0[nspan] = wwy;
	t->y1[nspan] = m1->y1[0];
	nspan++;
}
t->nspan = nspan;
if (nspan <= 1) {
	t->type = RECTANGLE;
} else {
	t->type = RING;
}

return(SH_SUCCESS);
}


RET_CODE thNaiveMaskAndNotNaiveMask(NAIVE_MASK *t, NAIVE_MASK *m) {
char *name = "thNaiveMaskAndNotNaiveMask";
RET_CODE status;
if (wmask == NULL) {
	init_static_vars();
} else {
	status = thNaiveMaskReset(wmask);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not reset working naive mask (wmask)", name);
		return(status);
	}
}
status = thNaiveMaskNotIntersectionNaiveMask(wmask, t, m);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (wmask = t - m)", name);
	return(status);
}
status = thNaiveMaskCopy(t, wmask);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not copt result (wmask) back onto (t)", name);
	return(status);
}
return(SH_SUCCESS);
}

REGION *shRegNewFromRegMask(REGION *reg, NAIVE_MASK *mask, int margine) {
char *name = "shRegNewFromReg";

if (reg == NULL) {
	thError("%s: ERROR - null input (region)", name);
	return(NULL);
}
if (margine < 0) {
	thError("%s: ERROR - doesn't support negative (margine = %d)", name, margine);
	return(NULL);
}

RET_CODE status;
REGION *out;
NAIVE_MASK *mask_t = NULL;
if (mask == NULL) {
	int row0, col0, nrow, ncol;
	row0 = reg->row0;
	col0 = reg->col0;
	nrow = reg->nrow;
	ncol = reg->ncol;
	mask_t = thNaiveMaskNew();
	status = thNaiveMaskFromRect(mask_t, row0, row0 + nrow - 1, col0, col0 + ncol - 1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not make a rectangular mask", name);
		return(NULL);
	}
	row0 -= margine;
	col0 -= margine;
	nrow += 2 * margine;
	ncol += 2 * margine;
	out = shRegNew(reg->name, nrow, ncol, reg->type);
	/* now copy values */		
	status = shRegPixCopyMask(reg, out, mask_t);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy pixels from source to target (reg)", name);	
		return(NULL);
	}
	thNaiveMaskDel(mask_t);
	return(out);
} else {
	if (mask->type != RECTANGLE) {
		thError("%s: ERROR - only (mask) of (type = 'RECATNGLE') supported", name);
		return(NULL);
	}
	mask_t = thNaiveMaskNew();
	status = thNaiveMaskExpand(mask_t, mask, margine);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not expand (mask) by margine", name);
		return(NULL);
	}
	int row0, row1, col0, col1;
	status = thNaiveMaskGetElement(mask_t, 0, &row0, &row1, &col0, &col1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get the corner coordinates of the expanded rectangle", name);
		return(NULL);
	}
	int nrow, ncol;
	nrow = row1 - row0 + 1;
	ncol = col1 - col0 + 1;
	out = shRegNew(reg->name, nrow, ncol, reg->type);
	status = shRegPixCopyMask(reg, out, mask_t);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy pixels from source to target (reg)", name);
		return(NULL);
	}
	thNaiveMaskDel(mask_t);
	return(out);
}	

thError("%s: ERROR - source code problem", name);
return(NULL);
}

RET_CODE shRegPixCopyMask(REGION *a, REGION *b, NAIVE_MASK *mask) {
char *name = "shRegPixCopyMask";
if (a == NULL || b == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}

NAIVE_MASK *amask;
int arow0, acol0, anrow, ancol;
arow0 = a->row0;
acol0 = a->col0;
anrow = a->nrow;
ancol = a->ncol;
if (anrow == 0 || ancol == 0) {
	thError("%s: empty (in) region", name);
	return(SH_SUCCESS);
}
NAIVE_MASK *bmask;
int brow0, bcol0, bnrow, bncol;
brow0 = b->row0;
bcol0 = b->col0;
bnrow = b->nrow;
bncol = b->ncol;
if (bnrow == 0 || bncol == 0) {
	thError("%s: empty (out) region", name);
	return(SH_SUCCESS);
}

RET_CODE status;
amask = thNaiveMaskNew();
status = thNaiveMaskFromRect(amask, arow0, arow0 + anrow - 1, acol0, acol0 + ancol - 1);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not form mask for (in) region", name);
	return(status);
}
bmask = thNaiveMaskNew();
status = thNaiveMaskFromRect(bmask, brow0, brow0 + bnrow - 1, bcol0, bcol0 + bncol - 1);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not form mask for (out) region", name);
	return(status);
}
status = thNaiveMaskAndNaiveMask(amask, bmask);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not find (in .and. out) naive mask", name);
	return(status);
}
if (mask != NULL) {
	status = thNaiveMaskAndNaiveMask(amask, mask);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not find (in .and. out .and. mask)", name);
		return(status);
	}
}
if (amask->type == UNKNOWN_NAIVE_MASK) {
	shRegClear(b);
	thNaiveMaskDel(amask);
	thNaiveMaskDel(bmask);
	return(SH_SUCCESS);
} else {
	shRegClear(b);
	int nspan, s, mrow0, mcol0, mrow1, mcol1, nmcol;
	nspan = amask->nspan;
	for (s = 0; s < nspan; s++) {
		status = thNaiveMaskGetElement(amask, s, &mrow0, &mrow1, &mcol0, &mcol1);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get mask properties for (s = %d)", name, s);
			return(status);
		}
		nmcol = mcol1 - mcol0 + 1;
		int i, ia, ib;
		THPIX *row_ia, *row_ib;
		for (i = mrow0; i < mrow1; i++) {
			ia = i - arow0;
			ib = i - brow0;
			row_ia = a->rows_thpix[ia] + (mcol0 - acol0);
			row_ib = b->rows_thpix[ib] + (mcol0 - bcol0);
			memcpy(row_ib, row_ia, nmcol * sizeof(THPIX));
		}
	}
	thNaiveMaskDel(amask);
	thNaiveMaskDel(bmask);
	return(SH_SUCCESS);
}
	
thError("%s: ERROR - source code problem", name);
return(SH_GENERIC_ERROR);
}

RET_CODE thNaiveMaskExpand(NAIVE_MASK *mask_t, NAIVE_MASK *mask_s, int margine) {
char *name = "thNaiveMaskExpand";
if (margine < 0) {
	thError("%s: ERROR - doesn't support negative margine (%d) values", name, margine);
	return(SH_GENERIC_ERROR);
}
if (mask_s == NULL || mask_t == NULL) {
	thError("%s: ERROR - null input or output", name);
	return(SH_GENERIC_ERROR);
}
if (mask_s->type != RECTANGLE) {
	thError("%s: ERROR - only supports masks of (type = 'RECTANGLE')", name);
	return(SH_GENERIC_ERROR);
}
mask_t->type = RECTANGLE;
mask_t->nspan = 1;
mask_t->x0[0] = mask_s->x0[0] - margine;
mask_t->x1[0] = mask_s->x1[0] + margine;
int nrow = mask_t->x1[0] - mask_t->x0[0] + 1;
if (nrow > MAXNROW) {
	/* correcting the margine */
	int d = margine - (nrow - MAXNROW + 1) / 2;
	mask_t->x0[0] = mask_s->x0[0] - d;
	mask_t->x1[0] = mask_s->x1[0] + d;
	#if WARN_HUGE_REGION
	thError("%s: WARNING - target mask exceeding (MAXNROW = %d), altering row-margine (%d) to %d", name, (int) MAXNROW, margine, d);
	#endif
}

mask_t->y0[0] = mask_s->y0[0] - margine;
mask_t->y1[0] = mask_s->y1[0] + margine;
int ncol = mask_t->y1[0] - mask_t->y0[0] + 1;
if (ncol > MAXNCOL) {
	/* correcting the margine */
	int d = margine - (ncol - MAXNCOL + 1) / 2;
	mask_t->y0[0] = mask_s->y0[0] - d;
	mask_t->y1[0] = mask_s->y1[0] + d;
	#if WARN_HUGE_REGION
	thError("%s: WARNING - target mask exceeding (MAXNCOL = %d), altering col-margine (%d) to %d", name, (int) MAXNCOL, margine, d);
	#endif
}


return(SH_SUCCESS);
}

REGION *shSubRegNewFromRegNaiveMask(const char *regname, REGION *reg, const NAIVE_MASK *mask) {
char *name = "shSubRegNewFromRegNaiveMask";
if (reg == NULL) {
	thError("%s: ERROR - null parent region", name);
	return(NULL);
}
if (mask == NULL) {
	thError("%s: ERROR - null naive mask", name);
	return(NULL);
}
if (mask->type != RECTANGLE) {
	thError("%s: ERROR - only rectangular naive masks are supported", name);
	return(NULL);
} else if (mask->nspan != 1) {
	thError("%s: ERROR - expected (nspan = 1) while received (nspan = %d) in the rectangular naive mask", name, mask->nspan);
	return(NULL);
}

int nrow, ncol, row0, col0;
nrow = reg->nrow;
ncol = reg->ncol;
row0 = reg->row0;
col0 = reg->col0;
reg->row0 = 0;
reg->col0 = 0;

int mrow0, mcol0, mrow1, mcol1, mnrow, mncol;
mrow0 = mask->x0[0];
mcol0 = mask->y0[0];
mrow1 = mask->x1[0];
mcol1 = mask->y1[0];
mnrow = mrow1 - mrow0 + 1;
mncol = mcol1 - mcol0 + 1;
int srow0, scol0;
srow0 = mrow0 - row0;
scol0 = mcol0 - col0;

if (srow0 < 0 || scol0 < 0 || srow0 + mnrow > nrow || scol0 + mncol > ncol) {
	thError("%s: ERROR - mask (nrow, ncol, row0, col0 = %d, %d, %d, %d) does not fit in region (nrow, ncol, row0, col0 = %d, %d, %d, %d)", name, mnrow, mncol, mrow0, mcol0, nrow, ncol, row0, col0);
	return(NULL);
}

REGION *subreg;
subreg = shSubRegNew(regname, reg, mnrow, mncol, srow0, scol0, NO_FLAGS);
reg->row0 = row0;
reg->col0 = col0;
if (subreg == NULL) {
	thError("%s: ERROR - dervish function constructed a null subregion (nrow, ncol, row0, col0 = %d, %d, %d, %d) to region (nrow, ncol, row0, col0 = %d, %d, %d, %d)", 
	name, mnrow, mncol, mrow0, mcol0, nrow, ncol, row0, col0);
	return(NULL);
}
subreg->row0 = mrow0;
subreg->col0 = mcol0;
return(subreg);
}

