/*
 * Read fpC images (infile), and copy its content to Outfile.
 */
/* PHOTO libraries */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ftcl.h"
#include "dervish.h"
#include "strings.h"

#include "phUtils.h"

#include "phPhotoFitsIO.h"

#include "thProc.h"
#include "thMath.h"
#include "thDebug.h"
#include "thIo.h"

int verbose = 0;

static int nrow = 1489;
static int ncol = 2048; 

static void usage(void);


int
main(int ac, char *av[])
{

  char *name = "gen_skybasis";
  /* Arguments */
  char *fname, *fname2;		/* input and output filenames */
  int n_tr, n_tc; 
  THPIX r0 = -1.0, r1 = 1.0, c0 = -1.0, c1 = 1.0;

  while(ac > 1 && (av[1][0] == '-' || av[1][0] == '+')) {
    switch (av[1][1]) {
    case '?':
    case 'h':
      usage();
      exit(0);
      break;
    case 'v':
      verbose++;
      break;
    default:
      shError("Unknown option %s\n",av[1]);
      break;
    }
    ac--;
    av++;
  }
  if(ac <= 3) {
    shError("You must specify a file name to throw the basis functions in \n");
    exit(1);
  }
  fname = av[1]; 
  n_tr = atoi(av[2]); n_tc = atoi(av[3]);
  
  if (ac <= 7 && ac > 4) {
    shError("r0, r1, c0, and c1 should be supplied all together");
    exit(1);
  }

  if (ac > 7) {
    r0 = atoi(av[4]);
    r1 = atoi(av[5]);
    c0 = atoi(av[6]);
    c1 = atoi(av[7]);
  }

  if (ac > 8) {
    nrow = atoi(av[8]);
    ncol = atoi(av[9]);
  }
  
  CHAIN *tcheb;
  tcheb = gen_Bspline_tcheb_chain_func(nrow, ncol, 
				       n_tr, n_tc,  
				       r0, r1, c0, c1);     
			       
  int nchain;
  nchain = shChainSize(tcheb);

  FUNC *func;
  func = (FUNC *) shChainElementGetByPos(tcheb, HEAD);
 
  PHFITSFILE *fits;
  
  thInitIo();
  
  fits = phFitsBinTblOpen(fname, 1, NULL);
  
  phFitsBinTblHdrWrite(fits, "FUNC", NULL);

  phFitsBinTblChainWrite(fits, tcheb); 
  
  phFitsBinTblClose(fits);

  /* now showing the error stack to ensure that everything went fine */

  thErrShowStack();

  /* successfull run */
  return(0);
}

/*****************************************************************************/

static void
usage(void)
{
   char **line;

   static char *msg[] = {
      "Usage: read_atlas_image [options] output-file n_tr n_tc",
      "Your options are:",
      "       -?      This message",
      "       -h      This message",
      "       -i      Print an ID string and exit",
      "       -v      Turn up verbosity (repeat flag for more chatter)",
      NULL,
   };

   for(line = msg;*line != NULL;line++) {
      fprintf(stderr,"%s\n",*line);
   }
}

