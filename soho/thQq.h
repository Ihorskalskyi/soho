#ifndef THQQ_H
#define THQQ_H

#include "string.h"
#include "dervish.h"
#include "thBasic.h"

void * (*thQNew)(void *);
void (*thQDel)(void *);

RET_CODE thQQTrans(void *Qin, void *Qout);
RET_CODE thQqTrans(void *Q, void *q,  char *type);
RET_CODE thqQTrans(void *q,  char *type, void *Q);
RET_CODE thqqTrans(void *qin,  char *typein, void *qout,  char*typeout);

RET_CODE thQJoin(void **Qin, int n, void *Qout);
RET_CODE thqJoin(void **qin,  char **typein, int n, void *qout,  char *typeout);

RET_CODE thQDisjoin(void *Qin, void **Qout, int n);
RET_CODE thqDisjoin(void *qin,  char *typein, void **qout,  char **typeout, int n);

void *thqNew(char *type);
void thqDel(void *q, char *type);

#endif



