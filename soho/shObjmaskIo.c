
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "strings.h"
#include "dervish.h"
#include "phPhotoFitsIO.h"
#include "phSpanUtil.h"
#include "phUtils.h"
#include "phObjc.h"
#include "sohoEnv.h"
#include "shDebug.h"
#include "shFpcIo.h"


#define FpmFileType STANDARD              /* FITS file Type: STANDARD, NONSTANDARD, IMAGE */
#define FpmFileDef  DEF_DEFAULT /* = DEF_NUM_OF_ELEMS    /* = DEF_NONE: shCFitsIo.h */
#define NROW_FPM 1489
#define NCOLUMN_FPM 2048
#define REGION_FLAGS_FPM NO_FLAGS

REGION *shObjmaskReadFpm(char *file,  
			 const int row0,  const int column0,  
			 const int row1,  const int column1, 
			 RET_CODE *shStatus);
RET_CODE shWriteFpm(char *file, 
		    MASK *mask);

RET_CODE shCpFpm(char *infile, char *outfile, /* input and output filenames */
		       const int row0, const int column0, 
		       const int row1, const int column1 /* corners of the image in INFILE */);

/*****************************************************************************/

REGION *shObjmaskReadFpm(char *file,  
		   const int row0,  const int column0,  
		   const int row1,  const int column1, 
		   RET_CODE *shStatus)
{
  char *name = "shObjmaskReadFpm";
  int  nrow,  ncolumn;
  //  MASK *sub = NULL, *fullmask = NULL, *mask = NULL;
  OBJMASK *Objmask; CHAIN *ObjmaskChain;
  int j, nspans;

  nrow = row1 - row0 + 1;
  ncolumn = column1 - column0 + 1;

  /* open the file as a PHFITSFILE */

  /*PHFITSFILE *
    phFitsBinTblOpen(char *file,		// file to open //
    int flag,		// open for read (flag == 0),
    write (flag == 1; => creat)
    append (flag == 2; => creat) //
    HDR *hdr)		// additional header info //
  */
  
  PHFITSFILE *fits;
  HDR *hdr;

  fits = phFitsBinTblOpen(file,	0, hdr);

  initSpanObjmask();
  ObjmaskChain = shChainNew("OBJMASK");
 
  /* 
     Objmask = phObjmaskNew(
     int nspans			// number of spans //
     ); */

  /* determine the real nspan */
  nspans = 10;
  Objmask = phObjmaskNew(nspans);
  phFitsBinTblRowRead(fits, Objmask);
  shErrStackClear();
  phFitsBinTblRowUnread(fits);
  //nspans = Objmask->nspans; 
  phObjmaskDel(Objmask);
  
  
  for (j = 0; j < fits->nrow; j++) {
	Objmask = phObjmaskNew(nspans); 
	/* read  Objmask */

	if (phFitsBinTblRowRead(fits,Objmask) < 0){
	  shError("%s: problem reading fits file %s at row %d", name, file, j);
		*shStatus = SH_GENERIC_ERROR;    
		return(NULL);
	}
    /* add it to chain */
    
    shChainElementAddByPos(ObjmaskChain, Objmask, "OBJMASK", TAIL, AFTER);

    /* renew object mask */
    phObjmaskDel(Objmask);
  }
  
  /* closing the openned file */
  if (phFitsBinTblClose(fits) != 0) {
    shError("%s: problem closing the fits file %s", name, file);
    *shStatus = SH_GENERIC_ERROR; 
    return(NULL);
  }
  
// Converting Objmask Chain into REGION

  /* void
     phRegionSetValFromObjmaskChain(REGION *reg, // the region to set //
     const CHAIN *chain, // the CHAIN of OBJMASKs specifying pixels //
     int val)	// to set to this value //
  */

  REGION *ObjmaskReg;
  /* debugging mode */
  int maskval = 1; 
  
  phRegionSetValFromObjmaskChain(ObjmaskReg, ObjmaskChain, maskval);

  /* successful run */
  *shStatus = SH_SUCCESS;
  return(ObjmaskReg);
}


RET_CODE shWriteFpm(char *file, 
		    MASK *mask)
{

  char *name = "shWriteFpc";
  RET_CODE shStatus;
  
  /* Write the MASK */

  /*
    RET_CODE shMaskWriteAsFits
    (
    MASK   *a_maskPtr,       // IN: Pointer to mask //
    char   *a_file,          // IN: Name of FITS file //
    DEFDIRENUM a_FITSdef,    // IN: Directory/file extension default type //
    FILE   *maskFilePtr,     // IN:  If != NULL then is a pipe to read from //
    int     a_writetape      // IN:  Flag to indicate that we must fork/exec an
    instance of "dd" to write a tape drive //
				      )
  */
  if (mask != NULL){
    shStatus = shMaskWriteAsFits (mask, file, FpmFileDef, NULL, 0);
  }
  return(shStatus);
}
  

RET_CODE shCpFpm(char *infile, char *outfile, /* input and output filenames */
		 const int row0, const int column0, 
		 const int row1, const int column1 /* corners of the image in INFILE */) 
{
  
  char *name = "shCpFpm";
  REGION *ObjmaskReg = NULL;
  RET_CODE shStatus;

  ObjmaskReg = shObjmaskReadFpm(infile, row0, column0, row1, column1, &shStatus);
  if (shStatus != SH_SUCCESS) {
    shRegDel(ObjmaskReg);
    return(shStatus);
  }
  //shStatus = shWriteFpm(outfile, mask);
  shWriteFpc(outfile, ObjmaskReg);
  if (ObjmaskReg != NULL) {
    shRegDel(ObjmaskReg);
  }
  return(shStatus);
}
