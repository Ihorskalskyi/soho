/* 
   the following are the environmetal parameters that thould be defined properly before compiling SOHO
*/
#define $PHOTO_REDUX .
#define $PHOTO_SWEEP .
#define $PHOTO_CALIB .
#define $PHOTO_DATA .
#define $PHOTO_RESOLVE .
#define $PHOTO_SKY .

#define debug 1
#define RUNTIME_ERROR 1
#define RUNTIME_WARNING 0
#define STACK_ERROR 1
#define STACK_WARNING 1
