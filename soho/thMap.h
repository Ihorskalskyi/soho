#ifndef THMAP_H
#define THMAP_H

#include "thProcTypes.h"
#include "thMapTypes.h"
#include "thObjcTypes.h"
#include "thModelTypes.h"
#include "thCTransform.h"

/* add object-models to the list of amplitude searchs */
RET_CODE thMAddObjcAllModels(MAPMACHINE *map, THOBJC *objc, int band, MFLAG flag);
RET_CODE thMAddObjc(MAPMACHINE *map, THOBJC *objc, char **models, int nmodel, int band, MFLAG flag);

/* take object-models out of the amplitude search list */
RET_CODE thMDeclaimObjc(MAPMACHINE *map, THOBJC *objc, char **models, int nmodel);

/*  Include Object-model-record in the list of parameters for non-linear search */
RET_CODE thMClaimObjcVar(MAPMACHINE *map, THOBJC *objc, char *mname, char **rlist, int nrecord);
RET_CODE thMDeclaimObjcVar(MAPMACHINE *map, THOBJC *objc, char *mname, char **rlist, int nrecord);
 
/* declare two object-model-records as equivalent in the search */
RET_CODE thMEquivObjcVars(MAPMACHINE *map, 
			THOBJC *objc1, char *mname1, char **rlist1,
			THOBJC *objc2,  char *mname2, char **rlist2,
			int nrecord);
RET_CODE thMEquivObjcVar(MAPMACHINE *map, 
			THOBJC *objc1, char *mname1, char *rname1,
			THOBJC *objc2,  char *mname2, char *rname2);

/* separate already equated object-model-records */
RET_CODE thMSepObjcVars(MAPMACHINE *map, THOBJC *objc, char *mname, char **rnames, int nrecord);
RET_CODE thMSepObjcVar(MAPMACHINE *map, THOBJC *objc, char *mname, char *rname);

/* compiler a mapmachine under construction, generate the array of model-parameters void **ps */
RET_CODE thMCompile(MAPMACHINE *map);
RET_CODE thMapPrint(MAPMACHINE *map);
/* to be constructed */
int thMrecCmp(MREC *mrec1, MREC *mrec2);

RET_CODE thMAddObjcChainToParFit(MAPMACHINE *map, CHAIN *objclist, int fitflag, char **classnames, int nclass, MCDCOMP component, int *nfound);

RET_CODE thMUpdateObjclist(MAPMACHINE *map, MASK_UPDATE mask_flag);

RET_CODE thMDecideDeclaimObjc(THOBJC *objc, THOBJC **upobjcs, int npobjc, int *declaim_decision);

#endif

