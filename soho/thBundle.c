
/* --- algorithm compilation ---- */

RET_CODE design_algorithm (CHAIN *objclist, MEMORY_USAGE *memory) {
	char *name = "design_algorithm";
	if (memory == NULL) {
		thError("%s: ERROR - memory amount (%ld) not supported", name, memory);
		return(SH_GENERIC_ERROR);
	}  
	if (objclist == NULL || shChainSize(objclist) == 0) {
		thError("%s: no object in the list", name);
		return(SH_GENERIC_ERROR);
	}

/* The following are the steps to come up with an algorithm for optimal use of memory during construction of the image:


====== ALGORITHM  ======

1. Clustering parameter (n)
2. Get Clusters
3. Choose the best Cluster (i = 1)
4. Register the objects in cluster (i) not registered in previous session (i-1)
5. Delete all edges inside cluster(i)
6. Delete lonely vertices
7. Redo clustering calculation for (m) in {n, n-1, …}
8. Choose the best cluster (i <-- i+1) based on criteria X
9. Check memory -- Flag objects based on Criteria Y
10. Delete objects from previous stages that are not in the new best bundle
11. If any object is left in the graph go to (4)

=======================

Criteria X:

A best cluster at stage (i) is one which:
a. Has the largest interaction with remnants of previous stages
b. Has the largest inclusion of remnants of previous stages


Largest inclusion / interaction can be defined in two based
a. Number of objects interacting or included
b. Number of pixels in objects interacting or included
c. Number of shared pixels between objects and the new bundle
*/
	THGRAPH *g, *wg;
	/* g is the plan-graph, this is the graph that is finally used to report the algorithm */
	g = thGraphNew();
	/* wg is the work-graph, which is used for clustering calculation */
	wg = thGraphNew();

	RET_CODE status;
	status = make_plan_graph(objclist, g);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not make the plan graph (g)", name);
		return(status);
	}
	status = make_work_graph(objclist, wg);	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not make the work graph (wg)", name);
		return(status);
	}	
	
	/* this is the data structure that hold clustering information */
	BUNDLE *clustering;
	clustering = thBundleNew();

	/* this is the data structure that holds the algorithm designed */
	ALGORITHM *algorithm;
	algorithm = thAlgorithmNew();

	nodesleft = count_nodes(wg);
	while (nodesleft) {
		/* this created clusters, or interprets the already performed clustering structure 
		to be suitable for discovery of the best bundle */	
		status = do_clustering_calculation(wg, clustering, memory);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not do the clustering calculation", name);
			return(status);
		}
		/* clustering is usually performed in a hierarchical fashion, 
		the following uses memory restrictions to find the optimal stage of clustering, 
		it also keeps in place the definition of bundles in this stage of clustering */
		status = choose_clustering(g, wg, clustering, memory);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not choose appropriate level of clustering", name);
			return(status);
		}
		/* once the suitable stage of clustering is determined, the following choose one bundle out
		of all the bundles determined by the clustering algorithm. note that the objects available 
		in memory from the previous stage can / should play a role in choosing this bundle */
		status = get_bundle(g, wg, clustering);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not choose the working bundle", name);
			return(status);
		}
		/* once you get the best bundle, clean the memory from the objects not in the bundle -- only if necessary --
		this should take into account not only memory but also if the objects proposed for deletion have any overlap
		with any of the objects in the bundle */
		status = clean_memory(g, wg, clustering, algorithm, memory);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not clean memory before bundle calculation", name);
			return(status);
		}
		/* once the best bundle is determined and the memory is cleaned, compute the objects in the best bundle
		if not already in the memory, this only works with vertices and not edges, flags them as created at this stage */	
		status = compute_the_best_bundle(g, wg, clustering, algorithm); 
		/* this deletes the edges inside the bundle in wg and adds a comment to the same edges on g */
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not compute what the chosen bundle", name);
			return(status);
		}
		/* once the objects are loaded in the memory, calculate the dot product of images that have overlap with eachother 
		this works with edges and not with vertices, and deletes them in (wg) graph once calculated */
		status = compute_overlaps(g, wg, clustering, algorithm);	
		/* at this stage, all interactions with some objects might be exhausted, 
		i.e. there might be no object with overlap with that object whose dot product with that object is not
		calculated. these objects will appear as 'lonely' vertices in the (wg) graph. 
		at this point, you should delete them from (wg) and empty the memory from them
		*/
		status = delete_lonely_vertices(g, wg, clustering, algorithm); /* this deletes the lonely vertices in the bundle and adds a comment to the same vertext in g */
		if (status != SH_SUCCESS) {	
			thError("%s: ERROR - could not delete lonely vertices", name);
			return(status);
		}	
		/* once the lonely vertices are deleted, one needs to counts the total number of 
		vertices remaining in (wg) graph to see if there is any need to continue with the algorithm*/
		nodesleft = count_nodes(wg);
		
	}	
	
	status = output_usage(g, algorithm);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not output resource usage properly", name);
		return(status);
	}
	status = output_algorithm(g, algorithm);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not output algorithm properly", name);
		return(status);
	}

return(SH_SUCCESS);
}

/* --- initiation of plan and work graphs --- */

RET_CODE make_plan_graph(CHAIN *objclist, THGRAPH *g) {
char *name = "make_plan_graph";
	if (objclist == NULL || shChainSize(objclist) == 0) {
		thError("%s: ERROR - no objects in the (objclist)", name);
		return(SH_GENERIC_ERROR);
	}
	if (g == NULL) {
		thError("%s: ERROR - null plan graph data structure (g) passed", name);
		return(SH_GENERIC_ERROR);
	}
	RET_CODE status;
	status = create_graph_from_objclist(objclist, g);		
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not create graph from object list", name);
		return(status);
	}
	static int init = 0;
	static n_vattr_string = 5, n_vattr_numeric = 5, n_eattr_string = 5, n_eattr_numeric = 5; 
	static m_vattr_string = 0, m_vattr_numeric = 0, m_eattr_string = 0, m_eattr_numeric = 0; 
	static char **vattr_string = NULL, **vattr_numeric = NULL, **eattr_string = NULL, **eattr_numeric = NULL; 
	if (!init) {
		if (n_vattr_string > 0) vattr_string = thStringArrNew(n_vattr_string);
		if (n_eattr_string > 0) eattr_string = thStringArrNew(n_eattr_string);
		if (n_vattr_numeric > 0) vattr_numeric = thStringArrNew(n_vattr_numeric);
		if (n_eattr_numeric > 0) eattr_numeric = thStringArrNew(n_eattr_numeric);
		i = 0;
		strcpy(vattr_numeric[i], "loneliness-stage"); i++; /* the stage at which this object got 'lonely' */
		strcpy(vattr_numeric[i], "pixcount"); i++;
		strcpy(vattr_numeric[i], "ngen"); i++; /* the number of generation of the profile of this object */
		strcpy(vattr_numeric[i], "objcid"); i++;
		m_vattr_numeric = i; 
		i = 0;
		strcpy(eattr_numeric[i], "weight"); i++; /* weight of interaction between two objects */
		strcpy(eattr_numeric[i], "calc-stage"); i++; /* stage at which this edge (dot) product was calculated */
		m_eattr_numeric = i;
		/* now make sure that you have initialized the function */
		init++;	
	}

	status = add_attr_to_graph(g, vattr_string, m_vattr_string,
				 vattr_numeric, m_vattr_numeric,
				 eattr_string, m_eattr_string,
 				 eattr_numeric, m_eattr_numeric);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not assign attributes to the graph", name);
		return(status);
	}

	status = assign_objcid_to_vertices(objclist, g);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not assign (objcid) to vertices", name);
		return(status);
	}
	status = assign_pixcount_to_vertices(objclist, g);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not assign 'pixcount' to vertices", name);
		return(status);
	}
	status = assign_weight_to_edges(objclist, g);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not assign 'weight' to edges", name);
		return(status);
	}	
return(SH_SUCCESS);
}

RET_CODE make_work_graph(CHAIN *objclist, THGRAPH *g) {	
char *name = "make_work_graph";
	static int init = 0;
	static n_vattr_string = 6, n_vattr_numeric = 6, n_eattr_string = 6, n_eattr_numeric = 6; 
	static m_vattr_string = 0, m_vattr_numeric = 0, m_eattr_string = 0, m_eattr_numeric = 0; 
	static char **vattr_string = NULL, **vattr_numeric = NULL, **eattr_string = NULL, **eattr_numeric = NULL; 
	
	if (objclist == NULL && g == NULL) {
		if (!init) {
			thError("%s: WARNING - can't release statically allocated memory", name);
			return(SH_SUCCESS);
		}
	
		thError("%s: WARNING - releasing statically allocated memory", name);	
	
		thStrigArrFree(vattr_string, n_attr_string);
		thStringArrFree(vattr_numeric, n_attr_numeric);
		thStringArrFree(eattr_string, n_attr_string);
		thStringArrFree(eattr_numeric, n_attr_numeric);
		
		m_vattr_string = 0; 
		m_vattr_numeric = 0; 
		m_eattr_string = 0; 
		m_eattr_numeric = 0; 
		
		vattr_string = NULL;
		vattr_numeric = NULL;
		eattr_string = NULL;
		eattr_numeric = NULL; 
	
		init = 0;
	}
	if (objclist == NULL || shChainSize(objclist) == 0) {
		thError("%s: ERROR - no objects in the (objclist)", name);
		return(SH_GENERIC_ERROR);
	}
	if (g == NULL) {
		thError("%s: ERROR - null plan graph data structure (g) passed", name);
		return(SH_GENERIC_ERROR);
	}
	RET_CODE status;
	status = create_graph_from_objclist(objclist, g);		
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not create graph from object list", name);
		return(status);
	}
	if (!init) {
		if (n_vattr_string > 0) vattr_string = thStringArrNew(n_vattr_string);
		if (n_eattr_string > 0) eattr_string = thStringArrNew(n_eattr_string);
		if (n_vattr_numeric > 0) vattr_numeric = thStringArrNew(n_vattr_numeric);
		if (n_eattr_numeric > 0) eattr_numeric = thStringArrNew(n_eattr_numeric);
		i = 0;
		strcpy(vattr_numeric[i], "pixcount"); i++;
		strcpy(vattr_numeric[i], "ngen"); i++;
		strcpy(vattr_numeric[i], "objcid"); i++;
		strcpy(vattr_numeric[i], "memstat"); i++;
		strcpy(vattr_numeric[i], "memrank"); i++;
		m_vattr_numeric = i;
		i = 0;
		strcpy(eattr_numeric[i], "weight"); i++;
		m_eattr_numeric = i;
		/* now make sure that you have initialized the function */
		init++;	
	}

	status = add_attr_to_graph(g, vattr_string, m_vattr_string,
				 vattr_numeric, m_vattr_numeric,
				 eattr_string, m_eattr_string,
 				 eattr_numeric, m_eattr_numeric);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not assign attributes to the graph", name);
		return(status);
	}
	
	status = assign_objcid_to_vertices(objclist, g);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not assign (objcid) to vertices", name);
		return(status);
	}
	status = assign_pixcount_to_vertices(objclist, g);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not assign 'pixcount' to vertices", name);
		return(status);
	}
	status = assign_weight_to_edges(objclist, g);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not assign 'weight' to edges", name);
		return(status);
	}	

	status = assign_mem_stat_rank_to_vertices(g);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not assign memory allocation status to vertices", name);
		return(SH_GENERIC_ERROR);
	}

return(SH_SUCCESS);
}


RET_CODE add_attr_to_graph(THGRAPH *g, char **vattr_string, int n_vattr_string,
				 char **vattr_numeric, int n_vattr_numeric,
				 char **eattr_string, int n_eattr_string,
 				 char **eattr_numeric, int n_eattr_numeric) {
char *name = "add_attr_to_graph";
	if (g == NULL) {
		thError("%s: WARNING - null graph, cannot add any attributes", name);
		return(SH_SUCCESS);
	}
	if (n_vattr_string != 0) {
		status = add_vertex_attr_to_graph_string(g, vattr_string, n_vattr_string);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not generate vertex attributes", name);
			return(status);
		}
	}
	if (n_vattr_numeric != 0) {
		status = add_vertex_attr_to_graph_numeric(g, vattr_numeric, n_vattr_numeric);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not generate vertex attributes", name);
			return(status);
		}
	}
	if (n_eattr_string != 0) {
		status = add_edge_attr_to_graph_string(g, eattr_string, n_eattr_string);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not generate edge attributes", name);
			return(status);
		}
	}
	if (n_eattr_numeric != 0) {
		status = add_edge_attr_to_graph_numeric(g, eattr_numeric, n_eattr_numeric);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not generate edge attributes", name);
			return(status);
		}
	}
	
return(SH_SUCCESS);
}

/* --- clustering calculation --- */

/* this created clusters, or interprets the already
performed clustering structure to be suitable for 
discovery of the best bundle */	

RET_CODE do_clustering_calculation(THGRAPH *wg, THBUNDLE *clustering, 
				MEMORY_USAGE *memory) {
char *name = "do_clustering_calculation";
if (wg == NULL) {
	thError("%s: ERROR - null work graph (wg)", name);
	return(SH_GENERIC_ERROR);
}
if (clustering == NULL) {
	thError("%s: ERROR - null (clustering)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;

igraph_t *wgg;
status = thGraphGet(wg, &wgg, &weights);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get arguments from 'THGRAPH'", name);
	return(status);
}

status = thBundleGet(clustering, &merges, &membership, &nodes, &csize, &stage1, &tree_status);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get arguments from 'THBUNDLE'", name);
	return(status);
}
if (tree_status == NOT_DONE) {
	istatus = igraph_community_walktrap(wgg, weights, L_WALKTRAP, 
					merges, modularity);
	if (istatus != 0) {
		thError("%s: ERROR - could not perform walk-trap on the graph", name);
		return(SH_GENERIC_ERROR);
	}

	
	while (repeat) {	
		steps = n_vertex - stage1;
		istatus = igraph_community_to_membership(merges, nodes, 
						steps, membership, csize);
		if (istatus != 0) {
			thError("%s: ERROR - could not extract community membership", 
				name);
			return(SH_GENERIC_ERROR);
		}
		
		/* the following returns 0 if the clusters fit in the memory constraint */
		repeat = check_clustering_fits_memory(clustering, memory);
		if (repeat) stage1++;
	
	}	
	/* this saves the membership in the form readable from outside igraph_t */
	status = fix_membership(clustering, stage1);
	
	for (stage = stage1 - 1; stage <= 0; stage--) {
		steps = n_vertex - stage;
		istatus = igraph_community_to_membership(merges, nodes, 
						steps, membership, csize);
		if (istatus != 0) {
			thError("%s: ERROR - could not extract community membership", 
				name);
			return(SH_GENERIC_ERROR);
		}
		/* the following fills the structure of inclusion of smaller clusters in bigger ones */	
		status = assert_clustering_in_this_stage(clustering, stage);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not assert clustering at stage (%d)", name, (int) stage);
			return(status);
		}
	
	}
	
	tree_status = DONE;
	status = thBundlePut(clustering, merges, membership, nodes, csize, stage1, tree_status);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put updated arguments in 'THBUNDLE'", name);
		return(status);
	}
}

return(SH_SUCCESS);
}

/* the following function gets the next best bundle */
RET_CODE get_bundle(THGRAPH *g, THGRAPH *wg, THBUNDLE *clustering) {
char *name = "get_bundle";
if (g == NULL || wg == NULL) {
	thError("%s: ERROR - null (g) or (wg) graph", name);
	return(SH_GENERIC_ERROR);
}
if (clustering == NULL) {
	thError("%s: ERROR - null (clustering) 'THBUNDLE'", name);
	return(SH_GENERIC_ERROR);
}


status = thBundleGet(algorithm, &working_bundle);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (working_bundle)", name);
	return(status);
}

if (working_bundle == BAD_BUNDLE) {
	/* this means we are at the fist call of best bundle */
	status = get_top_most_bundle(clustering, &adj_bundle);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get the topmost bundle id", name);
		return(status);
	}
	status = find_best_leaf(clustering, adj_bundle, &best_bundle);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not find the best lead to the topmost cluster", name);
		return(status);
	} 

} else {	
	status = get_adjacent_bundle(clustering, working_bundle, &adj_bundle);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get the bundle adjacent to (working_bundle)", name);
		return(status);
	}
	is_calculated = is_bundle_calculated(g, wg, clustering, algorithm, clustering, adj_bundle, &status);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not evaluate the calculation status of (adj_bundle)", name);
		return(status);
	}
	if (!is_calculated) best_bundle = adj_bundle;
	while (is_calculated) {
		status = get_parent_bundle(clustering, working_bundle, &working_bundle);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get the parent bundle for (working_bundle))", name);
			return(status);
		}
		status = get_adjacent_bundle(clustering, working_bundle, &adj_bundle);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get the (adj_bundle) to (working_bundle)", name);
			return(status);
		}
		is_calculated = is_bundle_calculated(g, wg, clustering, algorithm, clustering, adj_bundle, &status);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not evaluate the calculation status of (adj_bundle)", name);
			return(status);
		}
		if (!is_calculated) {
			status = find_the_best_leaf(clustering, adj_bundle, &best_bundle);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not find the best leaf for (adj_bundle)", name);
				return(status);
			}
		}
	}
}
status = insert_best_bundle(clustering, algorithm, best_bundle);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not insert the (best_bundle) id back in 'THBUNDLE'", name);
	return(status);
}

return(SH_SUCCESS);
}	
	
		

RET_CODE clean_memory(THGRAPH *g, THGRAPH *wg, THBUNDLE *clustering, ALGORITHM *algorithm, MEMORY_USAGE *memory) {
char *name = "clean_memory";
if (g == NULL || wg == NULL || clustering == NULL || algorithm == NULL || memory == NULL) {
	thError("%s: ERROR - null input discovered", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
status = thGraphGet(wg, &wgg);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (working graph)", name);
	return(status);
}
status = thGraphGet(g, &gg);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (planning graph)", name);
	return(status);
}

