#ifndef THCAPTURE_H
#define THCAPTURE_H

#include "thCaptureTypes.h"

RET_CODE thCaptureInit(CAPTURE *capture, FRAME *f, int fitid, CAPTURE_MODE cmode);
RET_CODE thCapture(CAPTURE *capture);
RET_CODE thCaptureFini(CAPTURE *capture);
RET_CODE thCaptureExport(CAPTURE *capture);

RET_CODE thCaptureStatWrite(FILE *fil, CAPTURE_STAT *stat);
RET_CODE pChainWrite(FILE *fil, CHAIN *chain);

RET_CODE thCaptureMakeSectorModel(CAPTURE *capture, CAPTURE_SECTOR sector);
RET_CODE make_ldump_by_sector(LSTRUCT *l, CAPTURE_SECTOR sector, LSTRUCT *ldump);

RET_CODE thU16RegMakeLUT(REGION *ireg, REGION *lut);
RET_CODE thCaptureWriteSubtitle(FILE *fil, int frame, CAPTURE_STAT *stat, THOBJC *objc);
RET_CODE thCaptureFlush(CAPTURE *cap);

RET_CODE thCaptureBinU8Reg(REGION *reg, int gbin, int growc, int gcolc, int gnrow, int gncol, REGION **outreg);

#endif
