#include "thStatTypes.h"
#include "thDebug.h"

BASIC_STAT *thBasic_statNew() {
BASIC_STAT *stat = thCalloc(1, sizeof(BASIC_STAT));
return(stat);
}
void thBasic_statDel (BASIC_STAT *x) {
if (x == NULL) return;
thFree(x);
return;
}

void thBasicStatInit(BASIC_STAT *x) {
	if (x == NULL) return;
	x->n = VALUE_IS_BAD;
	x->mean = VALUE_IS_BAD;  x->s = VALUE_IS_BAD;
	x->median = VALUE_IS_BAD; x->iqr = VALUE_IS_BAD; 
	x->q1 = VALUE_IS_BAD; x->q3 = VALUE_IS_BAD;
	x->min = VALUE_IS_BAD; x->max = VALUE_IS_BAD; x->range = VALUE_IS_BAD;
	return;
}

/* rewriting BASIC_STAT to pass schema obstacles */
BASICSTAT *thBasicstatNew() {
BASICSTAT *stat = thCalloc(1, sizeof(BASICSTAT));
return(stat);
}
void thBasicstatDel (BASICSTAT *x) {
if (x == NULL) return;
thFree(x);
return;
}

