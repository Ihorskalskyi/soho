#include "thConsts.h"
#include "thBasic.h"
#include "thMGalaxyTypes.h"

GMODELPARS *thGmodelparsNew() {
	GMODELPARS *q;
	q = thCalloc(1, sizeof(GMODELPARS));
	q->band = DEFAULT_BAND;
	q->lumcut = DEFAULT_LUMCUT;
	q->inner_lumcut = DEFAULT_INNER_LUMCUT;
	q->outer_lumcut = DEFAULT_OUTER_LUMCUT;
	return(q);
}
void thGmodelparsDel(GMODELPARS *q) {
	if (q != NULL) thFree(q);
	return;
}

STARPARS *thStarparsNew() {
	STARPARS *q;
	q = thCalloc(1, sizeof(STARPARS));
	q->band = DEFAULT_BAND;
	return(q);
	q->lumcut = DEFAULT_STAR_LUMCUT;
	q->inner_lumcut = DEFAULT_STAR_INNER_LUMCUT;
	q->outer_lumcut = DEFAULT_STAR_OUTER_LUMCUT;
	
}
void thStarparsDel(STARPARS *q) {
	if (q != NULL) thFree(q);
	return;
}

DEVPARS *thDevparsNew() {
	DEVPARS *dp;
	dp = thCalloc(1, sizeof(DEVPARS));
	dp->band = DEFAULT_BAND;
	dp->lumcut = DEFAULT_DEV_LUMCUT;
	dp->inner_lumcut = DEFAULT_DEV_INNER_LUMCUT;
	dp->outer_lumcut = DEFAULT_DEV_OUTER_LUMCUT;
	return(dp);
}
void thDevparsDel(DEVPARS *dp){
	if (dp != NULL) thFree(dp);
	return;
}

EXPPARS *thExpparsNew() {
	EXPPARS *q;
	q = thCalloc(1, sizeof(EXPPARS));
	q->band = DEFAULT_BAND;
	q->lumcut = DEFAULT_EXP_LUMCUT;
	q->inner_lumcut = DEFAULT_EXP_INNER_LUMCUT;
	q->outer_lumcut = DEFAULT_EXP_OUTER_LUMCUT;
	return(q);
}
void thExpparsDel(EXPPARS *q) {
	if (q != NULL) thFree(q);
	return;
}

SERSICPARS *thSersicparsNew() {
	SERSICPARS *q;
	q = thCalloc(1, sizeof(SERSICPARS));
	q->band = DEFAULT_BAND;
	q->lumcut = DEFAULT_SERSIC_LUMCUT;
	q->inner_lumcut = DEFAULT_SERSIC_INNER_LUMCUT;
	q->outer_lumcut = DEFAULT_SERSIC_OUTER_LUMCUT;
	return(q);
}
void thSersicparsDel(SERSICPARS *q) {
	if (q != NULL) thFree(q);
	return;
}

CORESERSICPARS *thCoresersicparsNew() {
	CORESERSICPARS *q;
	q = thCalloc(1, sizeof(CORESERSICPARS));
	q->band = DEFAULT_BAND;
	q->lumcut = DEFAULT_CORESERSIC_LUMCUT;
	q->inner_lumcut = DEFAULT_CORESERSIC_INNER_LUMCUT;
	q->outer_lumcut = DEFAULT_CORESERSIC_OUTER_LUMCUT;
	return(q);
}

void thCoresersicparsDel(CORESERSICPARS *q) {
	if (q != NULL) thFree(q);
	return;
}

POWERLAWPARS *thPowerlawparsNew() {
	POWERLAWPARS *q;
	q = thCalloc(1, sizeof(POWERLAWPARS));
	q->band = DEFAULT_BAND;
	q->lumcut = DEFAULT_PL_LUMCUT;
	q->inner_lumcut = DEFAULT_PL_INNER_LUMCUT;
	q->outer_lumcut = DEFAULT_PL_OUTER_LUMCUT;
	return(q);
}
void thPowerlawparsDel(POWERLAWPARS *q) {
	if (q != NULL) thFree(q);
	return;
}

GAUSSIANPARS *thGaussianparsNew() {
	GAUSSIANPARS *q;
	q = thCalloc(1, sizeof(GAUSSIANPARS));
	q->band = DEFAULT_BAND;
	q->lumcut = DEFAULT_GAUSSIAN_LUMCUT;
	q->inner_lumcut = DEFAULT_GAUSSIAN_INNER_LUMCUT;
	q->outer_lumcut = DEFAULT_GAUSSIAN_OUTER_LUMCUT;
	return(q);
}
void thGaussianparsDel(GAUSSIANPARS *q) {
	if (q != NULL) thFree(q);
	return;
}


OBJCPARS *thObjcparsNew() {
	OBJCPARS *q;
	q = thCalloc(1, sizeof(OBJCPARS));
	q->band = DEFAULT_BAND;
	return(q);
}
void thObjcparsDel(OBJCPARS *q) {
	if (q != NULL) thFree((OBJCPARS *) q);
	return;
}

RET_CODE thDumpObjcparsWrite(FILE *fil, OBJCPARS *p, OBJ_TYPE sdsstype) {
char *name = "thDumpObjcparsWrite";
if (fil == NULL) {
	thError("%s: ERROR - null output file", name);
	return(SH_GENERIC_ERROR);
}
if (p == NULL) {
	thError("%s: ERROR - null (objcpars) to write", name);
	return(SH_GENERIC_ERROR);
}

char *fmt;
if (sdsstype == OBJ_STAR) {
fmt = "#star component \nI_star  = %9.4g, J_star  = %9.4g \nxc_star = %9.4g, yc_star = %9.4g \nmcount_star = %9.4g \n";
fprintf(fil, fmt, p->I_star, p->J_star, p->xc_star, p->yc_star, p->mcount_star);	
} else {
fmt = "#deV component \nI_deV  = %9.4g, J_deV  = %9.4g \nxc_deV = %9.4g, yc_deV = %9.4g \nre_deV = %9.4g, e_deV  = %9.4g, phi_deV = %9.4g \na_deV  = %9.4g, b_deV  = %9.4g, c_deV   = %9.4g \nE_deV  = %9.4g \nmcount_deV = %9.4g \n";
fprintf(fil, fmt, 
		p->I_deV, p->J_deV,
		p->xc_deV, p->yc_deV,
		p->re_deV, p->e_deV, p->phi_deV,
		p->a_deV, p->b_deV, p->c_deV,
		p->E_deV,
		p->mcount_deV);

fmt = "#second (outer) deV component \nI_deV2  = %9.4g, J_deV2  = %9.4g \nxc_deV2 = %9.4g, yc_deV2 = %9.4g \nre_deV2 = %9.4g, e_deV2  = %9.4g, phi_deV2  = %9.4g \na_deV2  = %9.4g, b_deV2  = %9.4g, c_deV2    = %9.4g \nE_deV2  = %9.4g \nmcount_deV2 = %9.4g \n";
fprintf(fil, fmt, 
	p->I_deV2, p->J_deV2,
	p->xc_deV2, p->yc_deV2,
	p->re_deV2, p->e_deV2, p->phi_deV2,
	p->a_deV2, p->b_deV2, p->c_deV2,
	p->E_deV2,
	p->mcount_deV2);

fmt = "#exponential component \nI_Exp  = %9.4g, J_Exp  = %9.4g \nxc_Exp = %9.4g, yc_Exp = %9.4g \nre_Exp = %9.4g, e_Exp  = %9.4g, phi_Exp = %9.4g \na_Exp   = %9.4g, b_Exp  = %9.4g, c_Exp   = %9.4g \nE_Exp  = %9.4g \nmcount_Exp = %9.4g \n";
fprintf(fil, fmt, 
	p->I_Exp, p->J_Exp,
	p->xc_Exp, p->yc_Exp,
	p->re_Exp, p->e_Exp, p->phi_Exp,
	p->a_Exp, p->b_Exp, p->c_Exp,
	p->E_Exp,
	p->mcount_Exp);


fmt = "#second (outer) exponential component \nI_Exp2  = %9.4g, J_Exp2  = %9.4g \nxc_Exp2 = %9.4g, yc_Exp2 = %9.4g \nre_Exp2 = %9.4g, e_Exp2  = %9.4g, phi_Exp2 = %9.4g \na_Exp2  = %9.4g, b_Exp2  = %9.4g, c_Exp2   = %9.4g \nE_Exp2  = %9.4g \nmcount_Exp2 = %9.4g \n";
fprintf(fil, fmt, 
	p->I_Exp2, p->J_Exp2,
	p->xc_Exp2, p->yc_Exp2,
	p->re_Exp2, p->e_Exp2, p->phi_Exp2,
	p->a_Exp2, p->b_Exp2, p->c_Exp2,
	p->E_Exp2,
	p->mcount_Exp2);


fmt = "#sersic component \nI_sersic  = %9.4g, J_sersic  = %9.4g \nxc_sersic = %9.4g, yc_sersic = %9.4g \nre_sersic = %9.4g, e_sersic  = %9.4g, phi_sersic = %9.4g, n_sersic = %9.4g \na_sersic  = %9.4g, b_sersic  = %9.4g, c_sersic   = %9.4g \nE_sersic  = %9.4g \nmcount_sersic = %9.4g \n";
fprintf(fil, fmt, 
	p->I_sersic, p->J_sersic,
	p->xc_sersic, p->yc_sersic,
	p->re_sersic, p->e_sersic, p->phi_sersic, p->n_sersic,
	p->a_sersic, p->b_sersic, p->c_sersic,
	p->E_sersic,
	p->mcount_sersic);

/* 	
fmt="#gaussian \nI_gaussian  = %9.4g, J_gaussian  = %9.4g \na_gaussian  = %9.4g, b_gaussian  = %9.4g, c_gaussian   = %9.4g \nxc_gaussian = %9.4g, yc_gaussian = %9.4g \nre_gaussian = %9.4g, e_gaussian  = %9.4g, phi_gaussian = %9.4g \nE_gaussian  = %9.4g \nmcount_gaussian = %9.4g \n";
fprintf(fil, fmt, 
	p->I_gaussian, p->J_gaussian,
	p->a_gaussian, p->b_gaussian, p->c_gaussian,
	p->xc_gaussian, p->yc_gaussian,
	p->re_gaussian, p->e_gaussian, p->phi_gaussian,
	p->E_gaussian, 
	p->mcount_gaussian);
*/
}
return(SH_SUCCESS);
}

void simple_print_model_pars(void *q, char *qname){
char *name = "simple_print_model_pars";
shAssert(qname != NULL);
shAssert(q != NULL);
TYPE qtype = shTypeGetFromName(qname);
shAssert(qtype != UNKNOWN_SCHEMA);
SCHEMA *s = shSchemaGetFromType(qtype);
shAssert(s != NULL);
int n = s->nelem;
int i;
printf("%s: variable[type '%s'] = {", name, qname);
for (i = 0; i < n; i++) {
	SCHEMA_ELEM *se = s->elems + i;
	if (se != NULL) {
		void *e = shElemGet(q, se, NULL);
		char *ename = se->name;
		THPIX evalue = * (THPIX *) e;
		if (strcmp(ename, "band")) {
			printf("%s = %20.10G, ", ename, (double) evalue);
		}
	}
}
printf("}\n");
return;
}
