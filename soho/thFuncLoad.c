#include "thFuncLoad.h"

static CHAIN *fs = NULL;
FUNCSCHEMA *thFuncschemaNew() {
FUNCSCHEMA *s;
s = thCalloc(1, sizeof(FUNCSCHEMA));
s->fname = thCalloc(SIZE, sizeof(char));
return(s);
}

void thFuncschemaDel(FUNCSCHEMA *s) {
if (s == NULL) return;
if (s->fname != NULL) thFree(s->fname);
thFree(s);
return;
}


void thFuncLoadInit() {

char *name = "thFuncLoadInit";
static int init = 0;
if (!init) {
	fs = shChainNew("GENERIC");
	return;
	}

thError("%s: WARNING - module already initialized", name);
return;
}

RET_CODE thFuncLoadAddEntry(char *fname, const void *fptr) {

char *name = "thFuncLoadAddEntry";
if (fname == NULL || strlen(fname) == 0 || fptr == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
FUNCSCHEMA *s;
s = thFuncLoadFindEntryByName(fname, NULL);
if (s != NULL) {
	thError("%s: ERROR - entry for function %s already exists", name, fname);
	return(SH_GENERIC_ERROR);
	}

s = thFuncschemaNew();
strcpy(s->fname, fname);
s->fptr = fptr;
shChainElementAddByPos(fs, s, "FUNCSCHEMA", TAIL, AFTER);

return(SH_SUCCESS);
}

RET_CODE thFuncLoadDelEntry(char *fname) {
char *name = "thFuncLoadDelEntry";
if (fname == NULL || strlen(fname) == 0) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
	}
FUNCSCHEMA *s;
int i;
s = thFuncLoadFindEntryByName(fname, &i);
if (i < 0) {
	thError("%s: ERROR - no entry was found for function %s", name, fname);
	return(SH_GENERIC_ERROR);
	}
shChainElementRemByPos(fs, i);
thFuncschemaDel(s);
return(SH_SUCCESS);
}

RET_CODE thFuncLoadReplaceEntry(char *fname, const void *fptr) {
char *name = "thFuncLoadReplaceEntry";
if (fname == NULL || strlen(fname) == 0 || fptr == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
thFuncLoadDelEntry(fname);
if (thFuncLoadAddEntry(fname, fptr) != SH_SUCCESS) {
	thError("%s: ERROR - problem adding function %s to database", name, fname);
	return(SH_GENERIC_ERROR);
	}
return(SH_SUCCESS);
}

FUNCSCHEMA *thFuncLoadFindEntryByName(char *fname, int *pos) {
char *name = "thFuncLoadFindEntryByName";
if (fs == NULL) {
	thError("%s: ERROR - module not initiated", name);
	if (pos != NULL) *pos = -1;
	return(NULL);
	}
FUNCSCHEMA *s;
int i = 0, n, nfound = 1;
n = shChainSize(fs);
while (nfound && i < n) {
	s = shChainElementGetByPos(fs, i);
	nfound = strcmp(s->fname, fname);
	i++;
}
i--;
if (pos != NULL) {
	if (0 <= i  && i< n) {*pos = i;} else {*pos = -1;}
}
if (!nfound) return(s);
return(NULL);
}

FUNCSCHEMA *thFuncLoadFindEntryByPtr(const void *fptr, int *pos) {
char *name = "thFuncLoadFindEntryByPtr";
if (fs == NULL) {
	thError("%s: ERROR - module not initialized", name);
	}
FUNCSCHEMA *s;
int i = 0, n, found = 0;
n = shChainSize(fs);
while (!found && i < n) {
	s = shChainElementGetByPos(fs, i);
	found = (s->fptr == fptr);
	i++;
}
i--;
if (pos != NULL) {
	if (i < n) {*pos = i;} else {*pos = -1;}
}
if (found) return(s);
return(NULL);
}


const void *thFuncPtrGetFromName(char *fname) {
FUNCSCHEMA *s;
s = thFuncLoadFindEntryByName(fname, NULL);
if (s == NULL) return(NULL);
return(s->fptr);
}

const char *thFuncNameGetFromPtr(const void *fptr) {
FUNCSCHEMA *s;
s = thFuncLoadFindEntryByPtr(fptr, NULL);
if (s == NULL) return(NULL);
return(s->fname);
}
