#include "thIoTypes.h"

static int static_ncolor = NCOLOR;

CHAIN *thChainObjcIoNewBlock(int ncolor, int nchain) {
	static_ncolor = ncolor;
	CHAIN *this_chain = shChainNewBlock("MY_OBJC_IO", nchain);
	return(this_chain);
}

SKYMODELIO *thSkymodelioNew(int nrow, int ncol) {
  SKYMODELIO *sm;
  sm = thCalloc(1, sizeof(SKYMODELIO));

  if (nrow >= 0 && ncol >= 0) {
    sm->reg = shRegNew("packed-sky-model", nrow, ncol, TYPE_THPIX);
  }

  return(sm);
}

void thSkymodelioDel(SKYMODELIO *sm) {
  
  if (sm != NULL) thFree(sm);

  return;

}

SKYMODELIO *thSkymodelIoPackFrame(FRAME *f) {

  char *name = "thSkymodelIoPack";

  if (f == NULL) {
    thError("%s: null (frame) passed", name);
    return(NULL);
  }


  if (f->sky == NULL) {
    thError("%s: no (sky) information available",
		   name);
    return(NULL);
  }

  if (f->sky->sc == NULL) {
    thError("%s: no (skycoeffs) available",
		   name);
    return(NULL);
  }

  
  SKYMODELIO *sm;
  sm = thSkymodelioNew(-1, -1);

  if (f->id == NULL) {
    thError("%s: no (id) available, proceeding",
		   name);
  } else {
    sm->run = f->id->run;
    sm->rerun = f->id->rerun;
    sm->field = f->id->field;
    sm->camcol = f->id->camcol;
    sm->camrow = f->id->camrow;
    sm->filter = f->id->filter;
    sm->mjd    = f->id->mjd;
  }

  
  /* packing SKYCOEFFS */
  
  SKYCOEFFS *sc;
  sc = f->sky->sc;

  sm->bsize = sc->bsize;

  THPIX *smc, *scc, *smcov, *sccov, *sminvcov, *scinvcov;
  int i, j;
  smc = sm->c;
  scc = sc->c;
  for (i = 0; i < sm->bsize; i++) {
    smc[i] = scc[i];
    smcov = sm->cov[i];
    sccov = sc->cov[i];
    sminvcov = sm->cov[i];
    scinvcov = sc->invcov[i];
    for (j = 0; j < sm->bsize; j++) {
      smcov[j] = sccov[j];
      sminvcov[j] = scinvcov[j];
    }
  }
  
  sm->reg = shRegNew(f->sky->reg->name, 
		     f->sky->reg->nrow, f->sky->reg->ncol,
		     THIOPIXTYPE);
  
  if (thCompressRegion(f->sky->reg, THIOPIXTYPE,
		       &(sm->bscale), &(sm->bzero)) != SH_SUCCESS){ 
    thError("%s: could not find suitable bzero and bscale conversion",
		   name);
    return(NULL);
  }

  if (thRegConvertPixType(f->sky->reg, sm->reg, 
			  sm->bzero, sm->bscale) != SH_SUCCESS) {
    thError("%s: could not convert pixel type for sky reg", name);
    return(NULL);
  }
      

  /* these are the staistics used to compare the goodness of fit */
  sm->loglikelihood = f->sky->loglikelihood;
  sm->npixel = f->sky->npixel;

  if (f->files == NULL) {
    thError("%s: Warning - no (frmaefiles) available for packing, proceeding", 
		   name);
  } else {
    sm->thmaskselfile = f->files->thmaskselfile;
    sm->thobjcselfile = f->files->thobjcselfile;
  }

  return(sm);

}
      

CHAIN *thSkymodelIoPackProc(PROCESS *proc) {
  
  char *name = "thSkymodelIoPackProc";
  
  if (proc == NULL) {
    thError("%s: null (proc) passed",
		   name);
    return(NULL);
  }

  if (proc->frames == NULL) {
    thError("%s: no (frames) available in (proc)",
		   name);
    return(NULL);
  }

  int i, nf;
  nf = shChainSize(proc->frames);

  CHAIN *smchain;
  smchain = shChainNew("SKYMODELIO");

  FRAME *f;
  SKYMODELIO *sm;
  for (i = 0; i < nf; i++) {
    f = (FRAME *) shChainElementGetByPos(proc->frames, i);
    sm = thSkymodelIoPackFrame(f);
    shChainElementAddByPos(smchain, sm, "SKYMODELIO", TAIL, AFTER);
  }

  return(smchain);
}




