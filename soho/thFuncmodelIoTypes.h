#ifndef THFUNCMODELIOTYPES_H
#define THFUNCMODELIOTYPES_H

#include "dervish.h"
#include "phPhotoFitsIO.h"
#include "thConsts.h"
#include "thBasic.h"
#include "thPardiv.h"
#include "thMapTypes.h"
#include "thFuncmodelTypes.h"

/* NDIVPAR shows the number of parameters needed to specify a prototype parameter in a division */
#define NDIVPAR 2

typedef struct parelem {

	int index;
	char *pname;
	void *pdiv; /* this will be used if the parameter is a real variable */
	THPARDIV_TYPE divtype; /* logarithmic, linear or else */
	THPAR_TYPE ptype; 
	int n, npdiv;
	
	} PARELEM;

typedef struct parelemio {

	int index;
	char *pname;
	THPIX pdiv1[NDIVPAR]; /* this will be used if the parameter is a real variable */
	int pdiv2[NDIVPAR]; /* this will be used if the parameter is an intereger variable */
	THPARDIV_TYPE divtype; /* logarithmic, linear or else */
	THPAR_TYPE ptype; 
	int n, npdiv;
	
} PARELEMIO;



typedef struct partable {
	/* name of the file and the table read */
	char *tablename;
	/* function name - the function that is suggested to be described by this table */
	char *funcname;
	/* the description of parameter ranges and divisions */
	CHAIN *table;
	/* image resolution - ccd description */
	int nrow, ncol;
} PARTABLE;

typedef struct parstatus {
	/* this is a status structure that shows the whereabouts of a parlist in a partable */
	int exists;
	/* this is the list of n closest neighbors to the point */
	CHAIN *neighbors;
	/* there should be information on the method of finding these neighbors, currently there
	   is none */
	/* reference to the original list and table */
	PARLIST *pl;
	PARTABLE *pt;
} PARSTATUS;

typedef struct funcmodelhdu {

char *funcid;
char *funcname;

U32 stat;

/* the following is used to locate the function values and the table */

int ncomp; /* number of components, 1, 2, 3 */
int compindex[N_FUNCMODEL_COMP_INDEX]; /* index for each component */
int firstrow; /* the first row in the binary table */
int pthdu;

} FMHDU;

typedef struct fmfitsfile {

	/* file information */
	char *fname;
	PHFITSFILE *fits;
	int mode;

	/* function information */
	CHAIN *fmhdus;
	CHAIN *pts;

	/* location info */
	int row, fmhdu_loc, image_loc;
	/* hdr */
	HDR *hdr;
	
} FMFITSFILE;


/* extended region type for I/O of funcmodels. 
   EXTREGION includes records on the positioning of 
   the subregion representing the function in the 
   main (parent) region, the central position of the 
   object, and the scaling parameters for the I/O 
  (bscale and bzero).
*/

typedef struct extregion {
	REGION *reg; /* representation of the image */
	/* positioning of the object in the parent region are
	   given by row0 and col0 in REGION above */
	int nrowp, ncolp; /* parent nrow, ncol - i am not sure if this should ever be used  */

	FL64 bzero, bscale; /* scaling / conversion factors */
	int conversion; /* is conversion necessary */
} EXTREGION;

/* handlers */

PARELEM *thParelemNew();
void thParelemDel(PARELEM *pe);

PARELEMIO *thParelemioNew();
void thParelemioDel(PARELEMIO *pe);

PARTABLE *thPartableNew();
void thPartableDel(PARTABLE *pt);
void thPartableDestroy(PARTABLE *pt);
void thPartableioDestroy(PARTABLE *pt); 

PARSTATUS *thParstatusNew();
void thParstatusDel(PARSTATUS *ps);
void thParstatusDestroy(PARSTATUS *ps);


FMHDU *thFmhduNew();
void thFmhduDel(FMHDU *fmhdu);


FMFITSFILE *thFmfitsfileNew();
void thFmfitsfileDel(FMFITSFILE *f);
void thFmfitsfileDestroy(FMFITSFILE *f);
RET_CODE thFmfitsfilePut(FMFITSFILE *f, char *filename, PHFITSFILE *fits, int *mode, HDR *hdr, CHAIN *fmhdus, CHAIN *pts, int *current_row, int *image_loc, int *fmhdu_loc);


EXTREGION *thExtregionNew(char *rname, int nrow, int ncol, PIXDATATYPE type);
void thExtregionDel(EXTREGION *extreg);

/* conversion between io types and non-io types */

PARELEMIO *thParelemioConvertParelem (PARELEM *pe);
PARELEM *thParelemConvertParelemio(PARELEMIO *pe);
PARTABLE *thPartableioConvertPartable(PARTABLE *pt);
PARTABLE *thPartableConvertPartableio(PARTABLE *pt);


/* pardef-parlist relations */

int thMatchPardefParelem (PARDEF *pd, PARELEM *pe);
/* basic handling of par tables and parelements and relating them to pardef and parlist */

int thGetIndexThpixPardiv (THPIX pval, THPIX *pdiv, int npdiv, THPARDIV_TYPE divtype, RET_CODE *status);	
int thGetIndexIntPardiv (int pval, int *pdiv, int npdiv, THPARDIV_TYPE divtype, RET_CODE *status);
int thCorrPartableParlist(PARTABLE *pt, PARLIST *pl, int *ctable, RET_CODE *status);
int thCorrParlistPartable(PARLIST *pl, PARTABLE *pt, int *ctable, RET_CODE *status);

int thGetDivindexFromPardef (PARELEM *pe, PARDEF *pd, RET_CODE *status);
int thGetRowFromDivindex(int *divindex, int *npdiv, int nt, RET_CODE *status);

int *thGetNpardivFromPartable (PARTABLE *pt, int *nt, RET_CODE *status);
int *thGetDivindexFromRow(int row, int *npdiv, int nt, RET_CODE *status);
int thGetMaxrowFromNpardiv(int *npdiv, int nt);

int *thGetPartableIndexFromParlist (PARTABLE *pt, PARLIST *pl, int *np, RET_CODE *status);
int thGetPartableIndexFromPardef (PARTABLE *pt, PARDEF *pd, RET_CODE *status);

RET_CODE thGetPardefFromParelem (PARELEM *pe, int divindex, PARDEF *pd);
 
#endif
