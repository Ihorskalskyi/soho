
dir = "/u/khosrow/dss/data/1033/137/objcs/4/"
fpcfile =  dir+"fpC-001033-g4-0024.fit.gz"
fpcreg = mrdfits(fpcfile,  0,  fpchdr)

tchebfile =  "./fit/tcheb-new-1489-2048-5-5.fit"
t =  mrdfits(tchebfile, 1, tchebhdr,  row = 12);
simreg =  t[0].rrows_fl32
ncol =  t[0].rncol
nrow =  t[0].rnrow
simreg =  reform(simreg, ncol,  nrow)
simreg = simreg * 20 + 100

g0 = 3.1
g1 = 4.9
col0 = 1023L

rndreg = replicate(0L, ncol, nrow)

for i = 0L, col0, 1L do begin
for j = 0L, (nrow - 1L), 1L do begin
rndreg[i, j] = (1.0E0 / g0) * randomn(seed, poisson = (g0 * simreg[i, j]))
endfor
endfor

for i = col0 + 1L, ncol - 1L, 1L do begin
for j = 0L, (nrow - 1L), 1L do begin
rndreg[i, j] = (1.0E0 / g1) *  randomn(seed, poisson = (g1 * simreg[i, j]))
endfor
endfor


simfile =  "./fit/simfpc.fit"
simregint = rndreg

simhdr = [fpchdr[7:46], fpchdr[48:73]]
mwrfits, simregint, simfile, simhdr, /create


END
