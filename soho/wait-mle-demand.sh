#!/bin/bash

name="wait-mle-demand"

count="$(condor_q | grep sim | grep -c khosrow)"
echo "$name: number of simulation jobs remaining = $count"

while [ $count -ne 0 ]; do	
	echo "$name: number of simulation jobs remaining = $count"
	echo "$name: waiting for the next condor statistics"
	sleep 2m
	count="$(condor_q | grep sim | grep -c khosrow)"
done

echo "$name: end of the wait"
./mle-lrg.sh

