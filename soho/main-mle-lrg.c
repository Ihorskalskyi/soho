/* PHOTO libraries */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ftcl.h"
#include "dervish.h"
#include "strings.h"
#include "time.h"

#include "phUtils.h"

#include "phPhotoFitsIO.h"
#include "phSpanUtil.h"

#include "thMath.h" 
#include "thDebug.h" 
#include "thIo.h" 
#include "thAscii.h" 
#include "thProc.h" 
#include "thMask.h" 
#include "thMSky.h" 
#include "thObjcTypes.h"
#include "thMGalaxy.h"
#include "thAlgorithm.h"
#include "thMap.h"
#include "thMle.h"
#include "thResidual.h"
#include "thParse.h"
#include "thDump.h"

typedef enum tweakflag {
	ABSOLUTE, FRACTIONAL, N_TWEAKFLAG, UNKNOWN_TWEAKFLAG
	} TWEAKFLAG;

#define SIM_SKY 1
#define CLASS_BAND R_BAND
#define TABLEROW 5
#define N_RANDOM_FRAMES 2
#define N_OUTPUT_OBJC 2
#define NMLEOBJC 1000000 /*2, 10, */
#define INIT_NDEV 0.0
#define WORK_BAND R_BAND
/* image clustering and memory constants */
#define THMEMORY 1300
#define DO_ALGORITHM 1
#define RUN_MLE_FOR_LSTRUCT2 1 /* damp the weight in core of cD and run to fit for halo better */
#define CONDUCT_MLE_LRG_ONLY_CD 1

typedef struct strategy {
	THPIX dev_adj_factor, exp_adj_factor;
	char *adj_method;
	int l_walktrap;
	int hierarchy_step;
	
	char *gfile_prefix, *clusterfile_prefix, *clusterlog_prefix;
	} STRATEGY;
 
int verbose = 0;

#if 0
static int nrow = 1489;
static int ncol = 2048; 
#endif
static int CLASS_INDEX = UNKNOWN_BNDEX;

#if 0
static void usage(void);
#endif
void chain_model_output(char *job, CHAIN *objclist, THOBJCTYPE otype);
void model_output(char *job, THOBJC *objc);
int compare_re(const void *x1, const void *x2);
int compare_count_galaxy(const void *x1, const void *x2);
int compare_count_star(const void *x1, const void *x2);

extern double ddot_(int *, double [], int *, double [], int *);
extern float sdot_(int *, float [], int *, float [], int *);

static RET_CODE mle_frame(FRAME *frame, FRAME *frame_init, MLE_PARSED_INPUT *input, int init, int *attempt_boolean);
static int compare_count(const void *x1, const void *x2);
static RET_CODE tweak_wobjc(WOBJC_IO *objc, float tweakf, int fitflag, unsigned int seed);
static void output_fitflag_info(char *comment, int fitflag);

int debug_pass_objc_to_mle(THOBJC *objc, CHAIN *objclist);

int
main(int ac, char *av[]) {

  char *name = "do-mle-lrg";
  /* Arguments */

#if THCLOCK 
clock_t clk1;
clk1 = clock();
#endif

printf("%s: *** DEBUG *** : NMLEOBJC = %d \n", name, (int) NMLEOBJC);
printf("%s: *** DEBUG *** : AMPLITUDE_CV  = %g \n", name, (float) AMPLITUDE_CV);
fflush(stderr);
fflush(stdout);

printf("%s: starting to run '%s' \n", name, name);

phSchemaLoadFromPhoto();

thSchemaLoadFromSoho();


  /* Arguments */

MLE_PARSED_INPUT *input = thMleParsedInputNew();
RET_CODE status = mleparser(ac, av, input);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not parse input", name);
	fflush(stdout);
	fflush(stderr);
	return(-1);
}

#if 0
printf("%s: *** input *** \n", name);
printf("%s: setupfile = %s\n", name, input->setupfile);
printf("%s: camcol    = %d\n", name, input->camcol);
printf("%s: framefile = %s\n", name, input->framefile); /* this should include input and output objc file and sky file */
printf("%s: tweak f   = %g\n", name, input->smearf);
printf("%s: objclist  = %p\n", name, input->objclist); 
printf("%s: *** end of input *** \n", name);
#endif

char *fname;
int noobjcfile = 0, nrow, ncol, camcol, camrow, band, run, rerun, field;
unsigned int seed;
#if 1
int stripe;
nrow = input->nrow;
ncol = input->ncol;
camcol = input->camcol;
run = input->run;
rerun = input->rerun;
stripe = input->stripe;
field = input->field;
fname = input->framefile;
seed = input->seed;
GCLASS cdclass = input->cdclass;
int mle_on_demand = (input->demand != NO_DEMAND_CODE);
RUN_MODE run_mode = MLE_MODE;
if (mle_on_demand) run_mode = MLE_ON_DEMAND_MODE;

#else
thError("%s: ERROR - binary probelm - ensure input parameters are properly translated", name);
fflush(stdout);
fflush(stderr);
return(-1);
#endif

printf("%s: initiating modules \n", name);

printf("%s: initiating IO \n", name); 
thInitIo();
printf("%s: initiating Tank \n", name);
thTankInit();
printf("%s: initiating Ascii \n", name);
thAsciiInit();  
printf("%s: initiating SpanObjmask \n", name);
initSpanObjmask();
printf("%s: initiating MGalaxy \n", name);
status = initMGalaxy(input->mgalaxy_input);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not initiate MGalaxy from parsed input", name);
	return(-1);
}
printf("%s: initiating coordinate transform CTransform \n", name);
status = MGInitProfile(DEFAULT_ELL_DESC, input->ctransform_input);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate profile package", name);
		return(status);
	}

FRAMEFILES *ff;
CHAIN *fchain;
char *strutype = "FRAMEFILES";	

if (input->ff != NULL) {
	fchain = shChainNew(strutype);
	shChainElementAddByPos(fchain, input->ff, strutype, TAIL, AFTER);
} else {
	printf("%s: reading framefile \n", name);
	  /* get the related schema */
  	ASCIISCHEMA *schema = NULL;
  	schema = thTankSchemaGetByType(strutype);
  	if (schema == NULL) {
    		printf("%s: ERROR - No schema exists for structure %s", name, strutype);
		fflush(stdout);
		fflush(stderr);
		return(-1);
  	}
  	/* get the structure constructor */
  	void *(*strunew)();
 	strunew = schema->constructor;
  	if (strunew == NULL) {
    		printf("%s: ERROR - structure constructor for %s not available in tank", name, strutype);
		fflush(stdout);
		fflush(stderr);
		return(-1);
  	}
  	/* construct a structure */
  	void *stru;
  	stru = (*strunew)();
  
	ff = (FRAMEFILES *) stru;

  	FILE *fil;
  	fil = fopen(fname, "r");
	if (fil == NULL) {
		thError("%s: ERROR - could not read framefile '%s'", name, fname);
		fflush(stdout);
		fflush(stderr);
		return(-1);
	}  
  	fchain = thAsciiChainRead(fil);
}

if (shChainSize(fchain) == 0) {
	thError("%s: no structures was properly read", name);
	}
  printf("%s: (%d) pieces of frame info read \n",
	 name, shChainSize(fchain));

  ff = (FRAMEFILES *) shChainElementGetByPos(fchain, 0);
  if (noobjcfile) strcpy(ff->inphobjcfile, "");

  if (ff == NULL) {
    thError("%s: NULL returned from chain - check source code", name);
  } else {  
	#if DEBUG_MAIN_THWART 
    /* fields of the ff structure refer to outbound memory locations */
    printf(">>> phconfigfile = %s \n", ff->phconfigfile);
    printf(">>> phecalibfile = %s \n", ff->phecalibfile);
    printf(">>> out-phflatframefile = %s \n", ff->outphflatframefileformat);
    printf("phmaskfile = %s \n", ff->phmaskfile);
    printf(">>> in-phsmfile = %s \n", ff->inphsmfileformat); /* input fpBIN file format - band to be inserted */
    printf(">>> out-phsmfile = %s \n", ff->outphsmfileformat);  /* output fpBIN file format - band to be inserted */
    printf(">>> phpsfile = %s \n", ff->phpsfile);
    printf(">>> in- phobjcfile  = %s \n", ff->inphobjcfile); /* input fpObjc file */
    printf(">>> out-phobjcfile = %s \n", ff->outphobjcfile); /* output fpObjc file */ 
    printf("thsbfile = %s \n", ff->thsbfile);
    printf("thobfile = %s \n", ff->thobfile);
    printf("thmakselfile = %s \n", ff->thmaskselfile);
    printf("thobjcselfile = %s \n", ff->thobjcselfile);
    printf(">>> out-thsmfile = %s \n", ff->outthsmfileformat); /* output gpBIN file format - band to be inserted */
    printf("thpsfile = %s \n", ff->thpsfile);
    printf(">>> out-thobjcfile = %s \n", ff->outthobjcfileformat); /* ouput gpObjc file format - band to be inserted */
    printf(">>> out-thflatframefile  = %s \n", ff->outthflatframefileformat); /* output fpC file format - band to be inserted */
	#endif

  }

#if 1	
printf("%s: initiating random numbers \n", name);
RANDOM *rand;
char *randstr;
int nrandom;
randstr = thCalloc(MX_STRING_LEN, sizeof(char));
nrandom = (int) (nrow * ncol * (float) N_RANDOM_FRAMES);
sprintf(randstr, "%d:2", nrandom);
printf("%s: generating %d random numbers (random frames= %g) \n", name, nrandom, (float) N_RANDOM_FRAMES);
rand = phRandomNew(randstr, 1);
printf("%s: setting seed to %u \n", name, seed);
unsigned int seed0 = phRandomSeedSet(rand, seed);
printf("%s: old seed was %u \n", name, seed0);
if (!phRandomIsInitialised()) {
	thError("%s: ERROR - random package is not initialized", name);
	fflush(stdout);
	fflush(stderr);
	return(-1);
}
#else
printf("%s: random numbers not to be initiated \n", name);
#endif

 printf("%s: process and frame \n", name);
 PROCESS *proc;
 proc = thProcessNew();
 int i;
 int bndex[NCOLOR];
 for (i = 0; i < NCOLOR; i++) {
	bndex[i] = i;
}

int iband = 0;
int band_list[NCOLOR] = {2, 3, 1, 4, 0};
int success_list[NCOLOR] = {-1, -1, -1, -1, -1};
int success_list2[NCOLOR] = {-1, -1, -1, -1, -1};

FRAME *frames[NCOLOR];
FRAME *frames2[NCOLOR];

for (iband = 0; iband < NCOLOR; iband++) { 

	band = band_list[iband];
	status = thCamrowGetFromBand(band, &camrow);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not retrieve camrow", name);
		fflush(stdout);
		fflush(stderr);
		continue;
	}
	printf("%s: frame for band %d \n", name, band);
	status = thFramefilesPutBand(ff, band, run_mode);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not insert band in framefile", name);
		fflush(stdout);
		fflush(stderr);
		continue;
	}
	
   	FRAME *frame = thFrameNew();
	frames[band] = frame;
  	REGION *im = NULL;
      	if (frame->files != NULL) {
		thFramefilesDel(frame->files);
      	}
      
      	frame->files = ff; 
      	frame->proc = proc;
      
      	printf("%s: loading fpC file\n", name);
      	thFrameLoadImage(frame);
      	/* this part is only used during simulation */
      	frame->data->image = shRegNew("simulated image", nrow, ncol, TYPE_THPIX);
      	im = frame->data->image;

	FRAME *frame2 = thFrameNew();
	frames2[band] = frame2;
  	REGION *im2 = NULL;
      	if (frame2->files != NULL) {
		thFramefilesDel(frame2->files);
      	}

      	frame2->files = ff; 
      	frame2->proc = proc;
      
      	printf("%s: loading fpC file\n", name);
      	thFrameLoadImage(frame2);
      	/* this part is only used during simulation */
      	frame2->data->image = shRegNew("simulated image", nrow, ncol, TYPE_THPIX);
      	im2 = frame2->data->image;



      /* note that because this is only a simulation no fpC file exists prior to this run */
	frame->id->camcol = camcol;
	frame->id->camrow = camrow;
	frame->id->nrow = nrow;
	frame->id->ncol = ncol;
	frame->id->rerun = rerun;
	frame->id->run = run;
	frame->id->field = field;
	frame->id->filter = band;

	input->band = band;
	int init;
	init = 0;
	if (iband == 0) init = 1;
	if (iband == NCOLOR - 1) init = -1;
	FRAME *frame_init;
	if (init == 1) frame_init = NULL;

	if (mle_on_demand) {

		input->dump_mode = DUMP_OVERWRITE;
		input->galaxyclass = DEVEXP_GCLASS;
		int attempt_boolean = 0;
		status = mle_frame(frame, frame_init, input, init, &attempt_boolean);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not process frame for band = %d, iband = %d - proceeding to the next", name, band, iband);
			success_list[iband] = 0;
		} else if (attempt_boolean) {
			success_list[iband] = 1;
		}
		if (init == 1) {
			frame_init = frame;
			init = 0;
		}

		#if 0
		input->dump_mode = DUMP_APPEND;
		input->cdclass = DEV_GCLASS;
		input->galaxyclass = DEVEXP_GCLASS;
		status = mle_frame(frame2, frame_init, input, init);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not process frame for band = %d, iband = %d - proceeding to the next", 
			name, band, iband);
			success_list2[iband] = 0;
		} else {
			success_list2[iband] = 1;
		}
		#endif

	} else {
	
		input->dump_mode = DUMP_OVERWRITE;
		input->cdclass = cdclass;
		input->galaxyclass = DEVEXP_GCLASS;
		int attempt_boolean = 0;
		status = mle_frame(frame, frame_init, input, init, &attempt_boolean);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not process frame for band = %d, iband = %d - proceeding to the next", 
			name, band, iband);
			success_list[iband] = 0;
		} else if (attempt_boolean) {
			success_list[iband] = 1;
		}
		if (init == 1) {
			frame_init = frame;
			init = 0;
		}

		#if 0
		/* causes memory error for some reason */
		input->dump_mode = DUMP_APPEND;
		input->cdclass = DEV_GCLASS;
		input->galaxyclass = DEVEXP_GCLASS;
		status = mle_frame(frame2, frame_init, input, init);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not process frame for band = %d, iband = %d - proceeding to the next", 
			name, band, iband);
			success_list2[iband] = 0;
		} else {
			success_list2[iband] = 1;
		}
		#endif

	}


	printf("%s: WARNING - binary issue - only running MLE on the first band \n", name);
	if (iband == 0) break;
}

/* for unknown reason, this returns an error - distribution of the variates were checked and the results produced satisfy the 
randomness condition.
phRandomDel(rand);
*/

fflush(stderr);
fflush(stdout);

printf("%s: reporting on MLE-1 success for bands \n", name);
for (iband = 0; iband < NCOLOR; iband++) {
	band = band_list[iband];
	char cband = '\0';
	thNameGetFromBand(band, &cband);
	char *successreport; 
	if (success_list[iband] == 1) {
		successreport = "successful";
	} else if (success_list[iband] == 0) {
		successreport = "failed";
	} else if (success_list[iband] == -1) {
		successreport = "did not attempt";
	} else {
		successreport = "unknown";
	}
	printf("%s: report - band = '%c': %s \n", name, cband, successreport);
}

printf("%s: reporting on MLE-2 success for bands \n", name);
for (iband = 0; iband < NCOLOR; iband++) {
	band = band_list[iband];
	char cband = '\0';
	thNameGetFromBand(band, &cband);
	char *successreport; 
	if (success_list2[iband] == 1) {
		successreport = "successful";
	} else if (success_list2[iband] == 0) {
		successreport = "failed";
	} else if (success_list2[iband] == -1) {
		successreport = "did not attempt";
	} else {
		successreport = "unknown";
	}
	printf("%s: report - band = '%c': %s \n", name, cband, successreport);
}	
printf("%s: end of MLE report \n", name);
	
 /* no fit to be done by this program */
printf("%s: end of the program \n", name);

thErrShowStack();
fflush(stderr);
fflush(stdout);  
/* successfull run */
  return(0);
}

/*****************************************************************************/

void chain_model_output(char *job, CHAIN *objclist, THOBJCTYPE otype) {

THOBJC *objc;
int iobjc, nobjc;
nobjc = shChainSize(objclist);
for (iobjc = 0; iobjc < MIN(nobjc, N_OUTPUT_OBJC); iobjc++) {
	objc = shChainElementGetByPos(objclist, iobjc);
	model_output(job, objc);
}
printf("--- %s: end of output --- \n", job);
return;
}


void model_output(char *job, THOBJC *objc) {

CHAIN *thprops;
int nmodel, imodel;
static char *line = NULL;
if (line == NULL) line = thCalloc(MX_STRING_LEN, sizeof(char));

thprops = objc->thprop;
nmodel = shChainSize(thprops);
for (imodel = 0; imodel < nmodel; imodel++) {
	SCHEMA *s;
	SCHEMA_ELEM *se;
	int n, i;
	char *rname;
	THPIX *value;
	THPROP *prop;
	prop = shChainElementGetByPos(thprops, imodel);	 		
	s = shSchemaGetFromType(shTypeGetFromName(prop->pname));
	n = s->nelem;
	printf("--- %s parameters for (objc: %d) (model: '%s') --- \n", 
		job, objc->thid, prop->mname);
	for  (i = 0; i < n; i++) {
		se = s->elems + i;
		rname = se->name;
		value = shElemGet(prop->value, se, NULL);
		if (strlen(line) != 0) {
			sprintf(line, "%s, %15s = %.5e", line, rname, *value);
		} else {
			sprintf(line,"%15s = %.5e", rname, *value);
		}
		if (((i + 1)%TABLEROW == 0) || i == n - 1) {
			printf("%s\n", line);
			strcpy(line, "");
		}
	}
}
return;
}

/* note that if x1 > x2 then compare should result in 1 if you want things to be assending */

int compare_re(const void *x1, const void *x2) {
	const OBJC_IO *y1, *y2;
	shAssert(x1 != NULL);
	shAssert(x2 != NULL);
	y1 = *(OBJC_IO **)x1;
	y2 = *(OBJC_IO **)x2;
	float z1 = -200.0, z2 = -200.0;
	float c1 = 1.0, c2 = 1.0;
	if (y1->objc_type == OBJ_GALAXY) {
		if (y1->deV_L[CLASS_INDEX] > y1->exp_L[CLASS_INDEX]) {
			z1 = (float) y1->r_deV[CLASS_INDEX]; 
			c1 = (float) y1->counts_deV[CLASS_INDEX];
		} else {
			z1 = (float) y1->r_exp[CLASS_INDEX];	
			c1 = (float) y1->counts_exp[CLASS_INDEX];
		}
	} else if (y1->objc_type == OBJ_STAR) {
		z1 = -100.0;
		c1 = 1000.00;
	}
	if (y2->objc_type == OBJ_GALAXY) {
		if (y2->deV_L[CLASS_INDEX] > y2->exp_L[CLASS_INDEX]) {
			z2 = (float) y2->r_deV[CLASS_INDEX];
			c2 = (float) y2->counts_deV[CLASS_INDEX];
		} else {
			z2 = (float) y2->r_exp[CLASS_INDEX];
			c2 = (float) y2->counts_exp[CLASS_INDEX];
		}
	} else if (y2->objc_type == OBJ_STAR) {
		z2 = -100.0;
		c2 = 1000.0;
	}
	int res = 0;
	double dbres;
	dbres = (double) c2 * (double) z2 - (double) c1 * (double) z1;
	if (dbres > 0.0) {
		res = 1.0;
	} else if (dbres < 0.0) {
		res = -1.0;
	}
	
	/* 
	if (y1->objc_type == OBJ_GALAXY && 
		y2->objc_type == OBJ_GALAXY) {
		if (z1 > z2) {
			res = -1;
		} else if (z1 < z2) {
			res = 1;
		} else {
			res = 0;
		}
	} else if (y1->objc_type == OBJ_GALAXY && y2->objc_type != OBJ_GALAXY) {
		res = -1;
	} else if (y1->objc_type != OBJ_GALAXY && y2->objc_type == OBJ_GALAXY) {
		res = 1;
	} else {
		res = 0;
	}
	*/
	return(res);
}

int compare_count_galaxy(const void *x1, const void *x2) {
	WOBJC_IO *y1, *y2;
	shAssert(x1 != NULL);
	shAssert(x2 != NULL);
	y1 = *(WOBJC_IO **) x1;
	y2 = *(WOBJC_IO **) x2;
	int res;
	if (y1->type[CLASS_INDEX] == OBJ_GALAXY && y2->type[CLASS_INDEX] == OBJ_GALAXY) {
		float z1, z2;
		z1 = y1->petroCounts[CLASS_INDEX];
		z2 = y2->petroCounts[CLASS_INDEX];
		if (z1 > z2) {
			res = -1;
		} else if (z1 < z2) {
			res = 1;
		} else {
			res = 0;
		}
	} else if (y1->type[CLASS_INDEX] == OBJ_GALAXY) {
		res = -1;
	} else if (y2->type[CLASS_INDEX] == OBJ_GALAXY) {
		res = 1;
	} else {
		res = 0;
	}
	return(res);
}

int compare_count_star(const void *x1, const void *x2) {
	WOBJC_IO *y1, *y2;
	shAssert(x1 != NULL);
	shAssert(x2 != NULL);
	y1 = *(WOBJC_IO **) x1;
	y2 = *(WOBJC_IO **) x2;
	int res;
	if (y1->type[CLASS_INDEX] == OBJ_STAR && y2->type[CLASS_INDEX] == OBJ_STAR) {
		float z1, z2;
		z1 = y1->psfCounts[CLASS_INDEX];
		z2 = y2->psfCounts[CLASS_INDEX];
		if (z1 > z2) {
			res = -1;
		} else if (z1 < z2) {
			res = 1;
		} else {
			res = 0;
		}
	} else if (y1->type[CLASS_INDEX] == OBJ_STAR) {
		res = -1;
	} else if (y2->type[CLASS_INDEX] == OBJ_STAR) {
		res = 1;
	} else {
		res = 0;
	}
	return(res);
}

void test_sorted_chain(CHAIN *chain, int (*compare)(const void *, const void *)) {
char *name = "test_sorted_chain";
if (chain == NULL || compare == NULL) {
	thError("%s: ERROR - null input", name);
	return;
}
int n = shChainSize(chain);
int i, error = 0;

printf("%s: listing chain elements (chain size = %d) \n", name, n);
for (i = 0; i < n; i++) {
	WOBJC_IO *x1;
	x1 = shChainElementGetByPos(chain, i);
	if (x1->objc_type == OBJ_STAR) {
		printf("%02d STAR  : (xc, yc) = (%g, %g), counts = %g \n", i, x1->rowc[CLASS_INDEX], x1->colc[CLASS_INDEX], x1->psfCounts[CLASS_INDEX]); 	} else {
		printf("%02d GALAXY: (xc, yc) = (%g, %g), (counts_deV, re_deV, ab_deV, phi_deV) = (%g, %g, %g, %g), (counts_exp, re_exp, ab_exp, phi_exp) = (%g, %g, %g, %g) \n", 
	i, (float) x1->rowc[CLASS_INDEX], (float) x1->colc[CLASS_INDEX], 
	(float) x1->counts_deV[CLASS_INDEX], (float) x1->r_deV[CLASS_INDEX], (float) x1->ab_deV[CLASS_INDEX], (float) x1->phi_deV[CLASS_INDEX],
	(float) x1->counts_exp[CLASS_INDEX], (float) x1->r_exp[CLASS_INDEX], (float) x1->ab_exp[CLASS_INDEX], (float) x1->phi_exp[CLASS_INDEX]);	
	}
}

for (i = 0; i < n - 1; i++) {
	const void *x1, *x2;
	x1 = shChainElementGetByPos(chain, i);
	x2 = shChainElementGetByPos(chain, i+1);
	int res = compare(&x1, &x2);
	if (res == -1) {
		thError("%s: ERROR - chain descends at (i = %d)", i);
		error++;
	}
}
if (error) {
	thError("%s: ERROR - found at least (%d) misplaced elements in chain", name, error);
}
return;
}


RET_CODE mle_frame(FRAME *frame, FRAME *frame_init, MLE_PARSED_INPUT *input, int init, int *attempt_boolean) {

char *name = "mle_frame";
shAssert(frame != NULL);
shAssert(input != NULL);
shAssert(init == 0 || init == 1 || init == -1);
shAssert(attempt_boolean != NULL);

*attempt_boolean = 0;
RET_CODE status; 
char *fname;
unsigned int seed;
int nrow, ncol, camcol, camrow, band, run, rerun, stripe, field;
int force_cD_classify = 0, do_cD_classify = 0;
float tweakf;
DUMP_WRITE_MODE dump_mode;

nrow = input->nrow;
ncol = input->ncol;
camcol = input->camcol;
band = input->band;
run = input->run;
rerun = input->rerun;
stripe = input->stripe;
field = input->field;
fname = input->framefile;
seed = input->seed;
do_cD_classify = input->do_cD_classify;
force_cD_classify = input->force_cD_classify;
dump_mode = input->dump_mode;
tweakf = input->error;

int fitflag = MFIT_NULL;
fitflag |= (input->fitshape) | (input->fitsize) | (input->fitindex) | (input->fitcenter);
if (input->lockcenter) fitflag |= MFIT_LOCK_CENTER;
if (input->lockre) fitflag |= MFIT_LOCK_RE;

printf("%s: fitflag = %x \n", name, fitflag);

#if ALLOW_MAX_NOBJC
int nobjc_max = input->nobjc;
#endif

CHAIN *myobjclist = input->objclist;
int mle_on_demand = (input->demand != NO_DEMAND_CODE);
int demand_code = input->demand;
int initn = input->initn;
int do_fit_sky = input->do_fit_sky;
RUN_MODE run_mode = MLE_MODE;
if (mle_on_demand) run_mode = MLE_ON_DEMAND_MODE;

if (mle_on_demand) {
	PSF_CONVOLUTION_TYPE galaxypsf = input->galaxypsf;
	status = thMGalaxyPsfSet(galaxypsf);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not set Galaxy Psf Convolution method to '%s'", name, shEnumNameGetFromValue("PSF_CONVOLUTION_TYPE", galaxypsf));
		fflush(stderr);
		fflush(stdout);
		return(status);
	}
}

FRAMEFILES *ff = frame->files;
status = thCamrowGetFromBand(band, &camrow);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not retrieve camrow", name);
	fflush(stderr);
	fflush(stdout);
	return(status);
}


GC_SOURCE gcsource = input->gcsource;
if (init != 1 && gcsource == SWEEP_FILE_AND_WRITE) {
	gcsource = SWEEP_FILE;
	printf("%s: changing gcsource to 'SWEEP_FILE' \n", name);
}

int fit2fpCC = input->fit2fpCC;
if (fit2fpCC) {
	printf("%s: switching fpC and fpCC files \n", name);
	char *temp;
	temp = ff->phflatframefile;
	ff->phflatframefile=ff->thflatframefile;
	ff->thflatframefile=temp;
	printf("%s: loading fpCC file \n", name);
} else { 
	printf("%s: loading fpC file\n", name);
}
status = thFrameLoadImage(frame); 
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not load fpC image", name);
	fflush(stderr);
	fflush(stdout);
	return(status);
}

printf("%s: loading data for frame \n", name);
status = thFrameLoadData(frame, gcsource);
if (status != SH_SUCCESS && !mle_on_demand) {
	thError("%s: ERROR - could not load data for frame", name);
	fflush(stderr);
	fflush(stdout);
	return(status);
} else if (status != SH_SUCCESS && mle_on_demand) {
	thError("%s: WARNING - faced I/O errors while loading data on demand (code = %d), proceeding anyways", name, demand_code);
}
 
printf("%s: loading weight matrix -- space saver \n", name); 
status = thFrameLoadWeight(frame);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not load weights", name);
	fflush(stderr);
	fflush(stdout);
	return(status);
}

if (init != 1 && frame_init != NULL) {
	printf("%s: copying object data from initial frame \n", name);
	status = thFrameCopyDataObjcs(frame, frame_init, 0);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy data from frame", name);
		fflush(stderr);
		fflush(stdout);
		return(status);
	}
}

thPhpropChainPrintMatchStat(frame->data->phprop);

/* initiating the dump package */
status = thDumpInit(dump_mode);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not initiate the dump package", name);
	fflush(stderr);
	fflush(stdout);
	return(status);
}

/* initializing model and object bank */
if (init != 1) {
	status = thModelTerminate();
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not terminate previously initiated model banks", name);
		return(status);
	}
}
status = thModelInit();
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not initate model banks", name);
	return(status);
}

fflush(stderr);
fflush(stdout);
/* introducing sky to the object bank */
printf("%s: initiating all models and object types  ... \n", name);
printf("%s: initiating sky model and object type\n", name);
thSkyObjcInit(nrow, ncol, frame->data->ampl, &g0sky);
printf("%s: initiating other models \n", name);
status = MGModelsInit();
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not initiate galaxy related models", name);
	return(status);
}

int classflag = (GALAXY_CLASSIFICATION | STAR_CLASSIFICATION);
if (do_cD_classify) classflag |= CD_CLASSIFICATION;
GCLASS cdclass = input->cdclass;
GCLASS galaxyclass = input->galaxyclass;
int multistage = input->multistage;
status = thMGInitAllObjectTypes(classflag, cdclass, galaxyclass, run_mode);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not initialize all the necessary object types after initializing models", name);
	return(status);
}
int max_fitid = -1;
if (multistage == 1) {
	printf("%s: multi-stage flag set (= %d) \n", name, multistage);
} else if (multistage == 0) {
	printf("%s: multi-stage flag not set (= %d) \n", name, multistage);
} else {
	printf("%s: multi-stage flag not supported (= %d) \n", name, multistage);
}
if (cdclass == DEVDEV_GCLASS || cdclass == DEVPL_GCLASS || cdclass == SERSICEXP_GCLASS || cdclass == SERSICSERSIC_GCLASS || cdclass == CORESERSIC_GCLASS) {
	if (multistage == 1) {
		max_fitid = MAX_FITID;
	} else if (multistage == 0) {
		max_fitid = 1;
	} else {
		thError("%s: ERROR - (multistage = %d) flag not supported", name, multistage);
		return(SH_GENERIC_ERROR);
	}
} else if (cdclass == DEV_GCLASS || cdclass == SERSIC_GCLASS) {
	max_fitid = 1;
} else {
	thError("%s: ERROR - (cdclass = '%s') not supported", name, shEnumNameGetFromValue("GCLASS", cdclass));
	return(SH_GENERIC_ERROR);
}

int i, nm_max = 2;
char *mname, **mnames = thCalloc(nm_max, sizeof(char *));
for (i = 0; i < nm_max; i++) {
	mnames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
} 
mname = mnames[0];

printf("%s: models and object types initiated \n", name);	
/* define THOBJC with type SKY
construct a MAP
*/

int nsimobjc = 0;

THOBJCID id = 0;
THOBJC *objc = NULL;
CHAIN *objclist = NULL;
objclist = shChainNew("THOBJC");

WOBJC_IO *phobjc;
CHAIN *phobjcs, *cphobjcs, *cwphobjcs, *phprops;
int iobjc, nobjc = 0;	

phobjcs = frame->data->phobjc;
int *bndex = NULL;
status = thFrameGetPhObjcs(frame, &phobjcs, &bndex);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (phobjc chain) and (bndex array) from (frame)", name);
	return(status);
}
if (phobjcs != NULL) nobjc = shChainSize(phobjcs);
printf("%s: (%d) objects read from SDSS file \n", name, nobjc);

fflush(stderr);
fflush(stdout);
/* add all the models in the object to the map */
char **rnames, **iornames;
int nrname = 10;
rnames = thCalloc(nrname, sizeof(char*));
iornames = thCalloc(nrname, sizeof(char *));
for (i = 0; i < nrname; i++) {
	rnames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
	iornames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
}

CHAIN *robjclist = NULL;
int fitid = 0;
printf("%s: entering the fit loop, planning to run (%d) times \n", name, max_fitid);

for (fitid = 0; fitid < max_fitid; fitid++) {
printf("%s: starting MLE run for (fitid = %d) \n", name, fitid); 

status = thFrameSetFitid(frame, fitid);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not set (fitid = %d) in (frame)", name, fitid);
	return(status);
}

MAPMACHINE *map;
printf("%s: loading models into (mapmachine) \n", name);
map = thMapmachineNew();
/* inserting map into frame */
status = thFrameworkPutMap(frame->work, map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not put (map) in (framework)", name);
	return(status);
}

LSTRUCT *l = NULL;
LWORK *lwork = NULL;

int pos = 0;
/* introducing sky to the simulated set of objects  */
if (do_fit_sky) {
	printf("%s: creating sky object \n", name);
	objc = thSkyObjcNew();
	if (objc == NULL) {
		thError("%s: ERROR - could no create object sample", name);
		return(SH_GENERIC_ERROR);
	}
	printf("%s: loading sky object onto (mapmachine) \n", name);
	thMAddObjcAllModels(map, objc, band, SEPARATE);
	objc->thid = id++;
	shChainElementAddByPos(objclist, objc, "THOBJC", TAIL, AFTER);
	printf("%s: compiling map with (sky) \n", name);
	status = thMCompile(map);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not compile the map", name);
		return(status);
	}	
	/* constructing LSTRUCT */
	printf("%s: constructing likelihood data structure (lstruct) \n", name);
	status = thFrameLoadLstruct(frame, NULL, DEFAULT_CALIBTYPE, DEFAULT_LFITRECORD);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not load (lstruct)", name);
		return(status);
	}
	status = thFrameworkGetLstruct(frame->work, &l);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (lstruct) from (framework)", name);
		return(status);
	}
	l->lmodel->lmachine = map;
	lwork = l->lwork;

	thLDumpAmp(l, IOTOX);
	thLDumpPar(l, IOTOX);
	thLDumpPar(l, IOTOXN);

	thLDumpAmp(l, IOTOMODEL);
	thLDumpPar(l, IOTOMODEL);
	thLDumpPar(l, IOTOMODEL);

} else {
	printf("%s: WARNING - requested to exclude sky from fit \n", name);
	status = thFrameSubtractSky(frame);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not subtract sky from (frame)", name);
		return(status);
	}
}

int ndeleted = 0;

if (fitid == 0) {

	printf("%s: proceeding to the rest of the objects (non-sky)... \n", name);
	phobjcs = frame->data->phobjc;
	cwphobjcs = frame->data->cwphobjc;
	cphobjcs = frame->data->cphobjc;
	phprops = frame->data->phprop;

	int n_phobjcs = 0, n_cphobjcs = 0, n_cwphobjcs = 0, n_phprops = 0;
	if (phobjcs != NULL) n_phobjcs = shChainSize(phobjcs);
	if (cphobjcs != NULL) n_cphobjcs = shChainSize(cphobjcs);
	if (cwphobjcs != NULL) n_cwphobjcs = shChainSize(cwphobjcs);
	if (phprops != NULL) n_phprops = shChainSize(phprops);

	/* 
	shAssert(n_phobjcs == n_cphobjcs == n_cwphobjcs == n_phprops);
	*/
	shAssert(n_phobjcs == n_cphobjcs || n_phobjcs == 0 || n_cphobjcs == 0);
	shAssert(n_phobjcs == n_cwphobjcs || n_phobjcs == 0 || n_cwphobjcs == 0);
	shAssert(n_phobjcs == n_phprops || n_phobjcs == 0 || n_phprops == 0);

	/* 
	shAssert(shChainSize(phobjcs) == shChainSize(cphobjcs));
	shAssert(shChainSize(phobjcs) == shChainSize(cwphobjcs));
	shAssert(shChainSize(phprops) == shChainSize(phobjcs));
	*/
	nobjc = n_phobjcs;
	printf("%s: (%d) objects read from SDSS file \n", name, nobjc);

	if (mle_on_demand) {
		printf("%s: over-riding SDSS list on demand (code = %d) \n", name, demand_code);
		printf("%s: sorting the list made on demand to late force cD classification \n", name);
		shChainQsort(myobjclist, &compare_count);
		printf("%s: preparing object list for simulation on demand \n", name);
		status = thFrameReplacePhObjc(frame, myobjclist);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not replace (phobjc) list in the frame", name);
			return(status);
		}
		myobjclist = NULL;
		status = thFrameGetPhObjcs(frame, &myobjclist, &bndex);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (phobjc) list from (frame)", name);
			return(status);
		}
		int demand_nobjc = shChainSize(myobjclist);
		PHCALIB *calibData = frame->data->phcalib;
		CHAIN *demand_phprop_list = shChainNew("PHPROPS");
	
		for (iobjc = 0; iobjc < demand_nobjc; iobjc++) {
			WOBJC_IO *demand_objc = shChainElementGetByPos(myobjclist, iobjc);
			status = tweak_wobjc(demand_objc, tweakf, fitflag, seed);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not tweak (objc = %p, iobjc = %d)", name, demand_objc, iobjc);	
			}
			PHPROPS *demand_phprop = thPhpropsNew();
			CALIB_WOBJC_IO *cwphobjc = NULL;
			CALIB_PHOBJC_IO *cphobjc = NULL;
			OBJMASK *objmask = NULL;
			status = thPhpropsPut(demand_phprop, demand_objc, bndex, cwphobjc, cphobjc, calibData, objmask);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not make (phprop) from object data on demand inside for (iobjc = %d)", name, iobjc);
				return(status);
			}
			shChainElementAddByPos(demand_phprop_list, demand_phprop, "PHPROPS", TAIL, AFTER);
		}
		nobjc = shChainSize(demand_phprop_list);
		phprops = demand_phprop_list;
		printf("%s: made a list of (%d) objects based on demand (code = %d) - replaced with the original list \n", name, nobjc, demand_code);
		force_cD_classify = 1;
		do_cD_classify = 1;
	}  
	
	/* introducing a galaxy */
	printf("%s: getting crude-calib from frame \n", name);
	CRUDE_CALIB *cc = NULL;
	status = thFrameGetCrudeCalib(frame, &cc);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get 'crude_calib' from (frame)", name);
		return(status);
		}

	printf("%s: looping over phprops chain \n", name);
	ndeleted = 0;
	for (iobjc = 0; iobjc < nobjc; iobjc++) {
	
		/* initiating the object */
		PHPROPS *phprop = shChainElementGetByPos(phprops, iobjc);
		status = thPhpropsGet(phprop, &phobjc, NULL, NULL, NULL, NULL);
		shAssert(status == SH_SUCCESS);	
		if (iobjc == 0 && force_cD_classify == 1 && do_cD_classify == 1) { /* this naturally allows for mle init on demand for simulation on deman */
			thError("%s: WARNING - forcing classification of first object on demand", name);
			objc = thObjcNewFromPhprop(phprop, band, FORCECLASSIFY, "CDCANDIDATE");
		} else {
			objc = thObjcNewFromPhprop(phprop, band, CLASSIFY, NULL);
		}
		if (objc == NULL) {
			thError("%s: ERROR - could not create object for object (%d) in the sdss list", name, iobjc);
			return(SH_GENERIC_ERROR);
		}
		objc->thid = (THOBJCID) phobjc->id;
		THOBJCTYPE objctype;
		char *objcname;
		objctype = objc->thobjctype;
		status = thObjcNameGetFromType(objctype, &objcname);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get type name from object type (iobjc = %d)", name, iobjc);
			return(status);
		}
		if (objctype != SKY_OBJC && objctype != PHOTO_OBJC) {
			/* one should create the ioprop of the object after it is properly classified */
			char *tname = NULL;
			thObjcNameGetFromType(objc->thobjctype, &tname);
 			status = thObjcInitMpars(objc);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not initiate model parameters for (objc: %d) of class '%s'", name, objc->thid, tname);
				return(status);
			}
			status = thObjcTransIo(objc, MODELTOIO); 
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not translate model parameter to io parameter in (objc: %d) of class '%s'", name, objc->thid, tname);
			}
 			status = thObjcDumpCounts(objc);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not dump counts for (objc: %d) fo class '%s'", name, objc->thid, tname);
				return(status);
			}
			status = thObjcCrudeCalib(cc, objc);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not calibrate (objc: %d) of class '%s'", name, objc->thid, tname);
				return(status);
			}
			/* 
			if (debug_pass_objc_to_mle(objc, objclist)) {
				shChainElementAddByPos(objclist, objc, "THOBJC", TAIL, AFTER);		
			}
			*/
			if ((shChainSize(objclist) < NMLEOBJC) || (strstr(tname, "cD") != NULL) || (strstr(tname, "CD") != NULL)) {
				shChainElementAddByPos(objclist, objc, "THOBJC", TAIL, AFTER);		
			}
			/* thMAddObjcAllModels(map, objc, band, SEPARATE); */
			nsimobjc++;
			/* 
			if (nsimobjc == NMLEOBJC) break;
			*/
			#if ALLOW_MAX_NOBJC
			if (nsimobjc == nobjc_max) break;
			#endif
			
		} else if (objctype == PHOTO_OBJC) {
			ndeleted++; 
			thObjcDel(objc);
		}		
	}

	printf("%s: (%d) objects were deleted due to lack of classification \n", name, ndeleted);

	robjclist = objclist;
	nsimobjc=0;
	ndeleted=0;
}

fflush(stderr);
fflush(stdout);

nsimobjc = 0;
nobjc = shChainSize(robjclist);
for (iobjc = 0; iobjc < nobjc; iobjc++) {
	objc = shChainElementGetByPos(robjclist, iobjc);
	THOBJCTYPE objctype;
	char *objcname;
	objctype = objc->thobjctype;
	status = thObjcNameGetFromType(objctype, &objcname);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get type name from object type (iobjc = %d)", name, iobjc);
		return(status);
	}
	if (objctype != SKY_OBJC && objctype != PHOTO_OBJC) {
		thMAddObjcAllModels(map, objc, band, SEPARATE);
		if (nobjc < THRESHOLD_OBJCLIST_PRINT) output_objc_checklist(name, objc);
		nsimobjc++;
		/* 
		if (nsimobjc >  NMLEOBJC) {
			thError("%s: (NMLEOBJC = %d) requested but found (%d) in the list", name, (int) NMLEOBJC, nsimobjc);
			return(SH_GENERIC_ERROR);
		}
		*/
		#if ALLOW_MAX_NOBJC
		if (nsimobjc > nobjc_max) {
			thError("%s: (nobjc = %d) requested but found (%d) in the list", name, nobjc_max, nsimobjc);
			return(SH_GENERIC_ERROR);
		}
		#endif

		
	} else if (objctype == PHOTO_OBJC) {
		ndeleted++; 
	}	
}

printf("%s: (%d) objects added to the simulation from the random list \n", name, nsimobjc);
printf("%s: (%d) objects deleted from the random list due to lack of clear classification \n", name, ndeleted);

if (fitid == 0) {
	printf("%s: initiating model galaxy profiles \n", name);
	status = MGInitProfile(DEFAULT_ELL_DESC, input->ctransform_input);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate profile package", name);
		return(status);
	}
}

fitflag = MFIT_NULL;
fitflag |= (input->fitshape) | (input->fitsize) | (input->fitindex);

if (fitid == 0) {
	if (input->fitcenter) {
		fitflag |= input->fitcenter;
		if (input->lockcenter) fitflag |= MFIT_LOCK_CENTER;
	}
	if (input->fitsize) {
		fitflag |= input->fitsize;
		if (input->lockre) fitflag |= MFIT_LOCK_RE;
	}
}

printf("%s: fitflag = %x \n", name, fitflag);

char **targetclasses = thCalloc(1, sizeof(char *));
targetclasses[0] = "CDCANDIDATE";
MCDCOMP map_cd_component = UNKNOWN_MCDCOMP;
if (fitid == 0) {
	map_cd_component = HALO_AND_CORE;
} else {
	map_cd_component = HALO_ONLY;
}
if (fitflag != MFIT_NULL) {
	int n_cd_added = 0;
	status = thMAddObjcChainToParFit(map, robjclist, fitflag, targetclasses, 1, map_cd_component, &n_cd_added);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add object model parameters to non-linear fit", name);
		return(status);
	}
	if (n_cd_added == 0 && CONDUCT_MLE_LRG_ONLY_CD) {
		printf("%s: no cD-candidate was found in the (objc) list. Will not run MLE at all (even the linear part) \n", name);
		return(SH_SUCCESS);
	}
}
fflush(stderr);
fflush(stdout);
printf("%s: compiling map for simulation... \n", name);
status = thMCompile(map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not compile the mapmachine", name);
	return(status);
}
printf("%s: outputting (mapmachine) info \n", name);
status = thMapPrint(map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not print the compiled map", name);
	return(status);
}	
printf("%s: n(model) = %d, n(par) = %d, n(objc) = %d \n", name, map->namp, map->npar, map->nobjc);

MASK_UPDATE mask_flag = DEFAULT_MASK_UPDATE_FLAG;
printf("%s: updating mask definition to mask inner parts of bright objects \n", name);
status = thFrameUpdateThMask(frame, map, mask_flag);
if (status != SH_SUCCESS && !mle_on_demand) {
	thError("%s: ERROR - could not update mask definitions in frame", name);
	return(status);
} else if (status != SH_SUCCESS && mle_on_demand) {
	thError("%s: WARNING - WARNING - (thsm) errors while MLE on demand (code = %d), proceeding anyways", name, demand_code);
}
printf("%s: updating object list in map machine to exclude affected objects \n", name);
status = thMUpdateObjclist(map, mask_flag);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update (map)", name);
	return(status);
}
printf("%s: re-compiling map \n", name);
status = thMCompile(map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not re-compile (map)", name);
	return(status);
}

/* 
thMInfoPrint(map, EXTENSIVE);
*/

nobjc = shChainSize(robjclist);
printf("%s: total of (%d) objects added to (mapmachine) \n", name, nobjc);
if (nobjc == 0) {
	thError("%s: ERROR - no objects loaded from SDSS", name);
	return(SH_GENERIC_ERROR);
}

/* constructing LSTRUCT */
printf("%s: constructing likelihood data structure (lstruct) \n", name);
status = thFrameLoadLstruct(frame, NULL, DEFAULT_CALIBTYPE, DEFAULT_LFITRECORD);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not load (lstruct)", name);
	return(status);
}
status = thFrameGetLstruct(frame, &l);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lstruct) out of (frame)", name);
	return(status);
}
status = thLstructPutLmachine(l, map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmachine) out of (lstruct)", name);
	return(status);
}
status = thLstructGetLwork(l, &lwork);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwork) out of (lstruct)", name);
	return(status);
}
if (fitid > 0) {
	printf("%s: maninpulating weight matrix \n", name);
	status = thLstructManipulateWeight(l, targetclasses, 1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not manipulate the weight matrix in (lstruct)", name);
		return(status);
	}
}
/* simulating the galaxy */ 

pos++;	
printf("%s: assigining parameters and amplitude arrays in (mapmachine) from SDSS model parameters\n", 
name);

#if DEBUG_MEMORY
printf("%s: memory statistics before dumping X", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#endif

status = thLDumpAmp(l, MODELTOIO); 
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not transform amplitudes from (model) onto (IO)", name);
	return(status);
}
status = thLDumpPar(l, MODELTOIO);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not transform parameters from (model) onto (IO)", name);
	return(status);
}
status = thLDumpAmp(l, MODELTOX);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not transform amplitudes from (model) onto (X-array)", name);
	return(status);
}
status = thLDumpPar(l, MODELTOX);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not transform parameters from (model) onto (X-array)", name);
	return(status);
}
status = thLDumpPar(l, MODELTOXN);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not transform parameters from (model) onto (XN-array)", name);
	return(status);
}

/* 
printf("%s: assigining variance in (lmodvar) from IO parameters\n", 
name);	
thLDumpCov(l)
*/

#if DEBUG_MEMORY
printf("%s: memory statistics after dumping X", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#else
fflush(stderr);
fflush(stdout);
#endif

/* calculation needs proper initiation of quadrature methods and elliptical description */
MEMDBLE memory = input->memory;
if ((memory <= MEMORY_MIN || memory >= MEMORY_MAX) && (memory != INDEFINITE_MEMORY)) {
	memory = MEMORY_DEFAULT;
}
printf("%s: algorithms will be generated for a total memory of (%g MB) \n", name, (float) memory / (float) MEGABYTE);
fflush(stdout);
fflush(stderr);

/* creating the simulated / fake image */
printf("%s: * memory estimates \n", name);
#if DEBUG_MEMORY
printf("%s: memory statistics before estimating memory", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#endif

status = MakeImageAndMatrices(l, MEMORY_ESTIMATE);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not estimate the memory for the model elements", name);
	return(status);
}

#if DEBUG_MEMORY
printf("%s: memory statistics after estimating memory", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#else
fflush(stderr);
fflush(stdout);
#endif

clock_t clk1, clk2;
printf("%s: fit algorithm \n", name);
printf("%s: l-compilation \n", name);	
clk1 = clock();
status = thLCompile(l, memory, SIMFINALFIT);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not compile (l)", name);
	return(status);
}
clk2 = clock();
printf("%s: l-compilation time: %g sec \n", name, ((float) (clk2 - clk1)) / (float) CLOCKS_PER_SEC);
fflush(stderr);
fflush(stdout);

if (fitid == 0) {
	/* dumper call - note that if their is a need to calibrate the init counts, it should be done before initializing the dumpers */
	printf("%s: initilizing dump \n", name);
	status = thFrameInitIo(frame);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not init dumpers for this frame", name);
		return(status);
	}
}

fflush(stderr);
fflush(stdout);

double dif = 0.0; double difs[MAX_INIT_COUNT]; /* for timing */
printf("%s: running MLE with (%d) random initial points based on PHOTO findings \n", name, initn);
int initid = 0, true_init_count = 0, initk = EASY_INIT_COUNT;

while (initid < initk && true_init_count < MAX_TRUE_INIT_COUNT) {

	printf("%s: running mleframe for (initid = %d, attempt_id = %d) \n", name, initid, true_init_count);
	output_fitflag_info(name, fitflag);
	fflush(stderr);
	fflush(stdout);
	true_init_count++;
	init_psf_warning_count();

	if (true_init_count > 1) {

		printf("%s: tweaking PHOTO information for fit objects (initid = %d) \n", name, initid);
		fflush(stderr);
		fflush(stdout);
		status = thFrameTweakFitWobjcPar(frame, fitflag);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not tweak input fit parameter to a new initial guess (initid = %d)", name, initid);
			continue;
		}

		printf("%s: renewing fit objects based on altered PHOTO description (initid = %d) \n", name, initid);
		fflush(stderr);
		fflush(stdout);
		status = thLUPObjcRenewFromPhprop(l);
		if (status != SH_SUCCESS) {
			thError("%s: could not renew objects from altered PHOTO information (initid = %d)", name);
			continue;
		}

		printf("%s: re-assigning parameter and amplitude arrays in map machines from altred PHOTO model parameters (initid = %d) \n", name, initid);
		fflush(stderr);
		fflush(stdout);
		thLDumpAmp(l, MODELTOIO); 
		thLDumpPar(l, MODELTOIO);
 
		thLDumpAmp(l, MODELTOX);
		thLDumpPar(l, MODELTOX);
		thLDumpPar(l, MODELTOXN);

		printf("%s: refreshing (lstruct) - all algorithms (runtime correction, initid = %d) \n", name, initid);
		fflush(stderr);
		fflush(stdout);
		status = thLRefresh(l);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not refresh algorithms in (lstruct, initid = %d)", name, initid);
			continue;
		}

		printf("%s: initiating a new (io) unit in (frame) \n", name);
		fflush(stderr);
		fflush(stdout);
		status = thFrameInitIo(frame);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not initiation (io) in (frame)", name);
			continue;
		}
	} else {
		printf("%s: refreshing (lstruct) - all algorithms (runtime correction, initid = %d) - just in case \n", name, initid);
		fflush(stderr);
		fflush(stdout);
		status = thLRefresh(l);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not refresh algorithms in (lstruct, initid = %d)", name, initid);
			continue;
		}

	}
	fflush(stderr);
	fflush(stdout);

	time_t time1, time2;
	printf("%s: l-run \n", name);
	fflush(stderr);
	fflush(stdout);
	time(&time1);
	status = thLRun(l);
	if (status != SH_SUCCESS) {
		thError("%s: WARNING - could not run (l) at (init[id = %d, n = %d, k = %d], attempt = %d, ERROR)", name, initid, initn, initk, true_init_count);
		continue;
	}
	time(&time2);
	difs[initid] = difftime(time2, time1);	
	printf("%s: run time for (initid = %d): %G sec \n", name, initid, (double) difs[initid]);
	dif += difs[initid];
	fflush(stderr);
	fflush(stdout);

	printf("%s: finalizing frame dumpers \n", name);
	fflush(stderr);
	fflush(stdout);
	/* during finalizing steps, it should be determined which one of the initiation attempts is more acceptable */
	status = thFrameFiniIo(frame);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not finalize dumpers for frame", name);
		continue;
	}

	status = thFrameAssessTerrainComplexity(frame, initid, initn, &initk);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not assess terrain complexity for this fit", name);
		continue;
	}
	printf("%s: terrain complexity assessed, suggested initiation attempts (initk = %d) while (initid = %d) \n", name, initk, initid); 
	fflush(stdout);
	fflush(stderr);

	initid++;

}

initn = initid;
printf("%s: number of all initiation attemps = %d, successful attempts = %d \n", name, true_init_count, initn);

if (initn >= 1) {
	printf("%s: list of runtimes: ", name);
	for (initid = 0; initid < initn; initid++) {
		printf("%G ", (double) difs[initid]);	
		if (initid < initn - 1) printf(",");
	}
	printf("\n");
}

printf("%s: total attempt time: %G sec \n", name, (double) dif);
fflush(stderr);
fflush(stdout);


if (initn >= 1) {
	printf("%s: dumping log, sdss pars, init pars and fit findings\n", name);
	status = thFrameDumpIo(frame, 1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump frame information", name);
		return(status);
	}
	*attempt_boolean = 1;
} else {
	thError("%s: WARNING - no successful attempts - nothing to dump", name);
	*attempt_boolean = 0;
}

fflush(stderr);
fflush(stdout);
}

printf("%s: end of MLE fit \n", name);
fflush(stderr);
fflush(stdout);

return(SH_SUCCESS);

}

int compare_count(const void *x1, const void *x2) {
	WOBJC_IO *y1, *y2;
	shAssert(x1 != NULL);
	shAssert(x2 != NULL);
	y1 = *(WOBJC_IO **) x1;
	y2 = *(WOBJC_IO **) x2;
	int res;
	if (y1->objc_type == OBJ_GALAXY && y2->objc_type == OBJ_GALAXY) {
		float z1, z2;
		z1 = (y1->counts_exp[R_BAND]) * (1.0 - y1->fracPSF[R_BAND]) + y1->counts_deV[R_BAND] * y1->fracPSF[R_BAND];	
		z2 = (y2->counts_exp[R_BAND]) * (1.0 - y2->fracPSF[R_BAND]) + y2->counts_deV[R_BAND] * y2->fracPSF[R_BAND];
		if (z1 > z2) {
			res = -1;
		} else if (z1 < z2) {
			res = 1;
		} else {
			res = 0;
		}
	} else if (y1->objc_type == OBJ_GALAXY) {
		res = -1;
	} else if (y2->objc_type == OBJ_GALAXY) {
		res = 1;
	} else {
		res = 0;
	}
	return(res);
}


RET_CODE tweak_wobjc(WOBJC_IO *objc, float tweakf, int fitflag, unsigned int seed) {
char *name = "tweak_wobjc";
if (objc == NULL) {
	thError("%s: ERROR - null objc", name);
	return(SH_GENERIC_ERROR);
}

float epsilon1, epsilon2, epsilon3;
int bndex;
int pass = 0;

while (!pass) {

	pass = 1;
	if (fitflag & (MFIT_RE | MFIT_RB)) {
		epsilon1 = (float) parameter_epsilon_random_draw() * tweakf;
		epsilon2 = (float) parameter_epsilon_random_draw() * tweakf;
		epsilon3 = (float) parameter_epsilon_random_draw() * tweakf;
		for (bndex = 0; bndex < NCOLOR; bndex++) {
			if (objc->r_deV[bndex] != VALUE_IS_BAD) {
				objc->r_deV[bndex] += objc->r_deVErr[bndex] * epsilon1;
				pass = pass && (objc->r_deV[bndex] >= 0.0);
			}
			#if USE_FNAL_OBJC_IO
			if (objc->r_deV2[bndex] != VALUE_IS_BAD) {
				objc->r_deV2[bndex] += objc->r_deV2Err[bndex] * epsilon2;
				pass = pass && (objc->r_deV2[bndex] >= 0.0);
			}
			if (objc->r_pl[bndex] != VALUE_IS_BAD) {
				objc->r_pl[bndex] += objc->r_plErr[bndex] * epsilon3;
				pass = pass && (objc->r_pl[bndex] >= 0.0);	
			}
			#endif
		}
		epsilon2 = (float) parameter_epsilon_random_draw() * tweakf;
		epsilon1 = (float) parameter_epsilon_random_draw() * tweakf;
		for (bndex = 0; bndex < NCOLOR; bndex++) {
			if (objc->r_exp[bndex] != VALUE_IS_BAD) {
				objc->r_exp[bndex] += objc->r_expErr[bndex] * epsilon1;
				pass = pass && (objc->r_exp[bndex] >= 0.0);
			}
			#if USE_FNAL_OBJC_IO
			if (objc->r_exp2[bndex] != VALUE_IS_BAD) {
				objc->r_exp2[bndex] += objc->r_exp2Err[bndex] * epsilon2;
				pass = pass && (objc->r_exp2[bndex] >= 0.0);
			}
			#endif
		}
	}
	if (fitflag & MFIT_INDEX) {
		epsilon1 = (float) parameter_epsilon_random_draw() * tweakf;
		epsilon2 = (float) parameter_epsilon_random_draw() * tweakf;
		epsilon3 = (float) parameter_epsilon_random_draw() * tweakf;
		for (bndex = 0; bndex < NCOLOR; bndex++) {
			objc->fracPSF[bndex] = 0.5 + epsilon1;
			pass = pass && ((objc->fracPSF[bndex] >= 0.0) && (objc->fracPSF[bndex] <=  1.0));
			#if USE_FNAL_OBJC_IO
			/* 
			objc->fracPSF2[bndex] = 0.5 + epsilon2;
			*/
			#endif
		}
	}
	if (fitflag & MFIT_PHI) {
		epsilon1 = (float) parameter_epsilon_random_draw() * tweakf;
		epsilon2 = (float) parameter_epsilon_random_draw() * tweakf;
		epsilon3 = (float) parameter_epsilon_random_draw() * tweakf;
		for (bndex = 0; bndex < NCOLOR; bndex++) {
			objc->phi_deV[bndex] += objc->phi_deVErr[bndex] * epsilon1;	
			#if USE_FNAL_OBJC_IO
			objc->phi_deV2[bndex] += objc->phi_deV2Err[bndex] * epsilon2;
			objc->phi_pl[bndex] += objc->phi_plErr[bndex] * epsilon3;
			#endif
		}
		epsilon2 = (float) parameter_epsilon_random_draw() * tweakf;
		epsilon1 = (float) parameter_epsilon_random_draw() * tweakf;
		for (bndex = 0; bndex < NCOLOR; bndex++) {
			objc->phi_exp[bndex] += objc->phi_expErr[bndex] * epsilon1;	
			#if USE_FNAL_OBJC_IO
			objc->phi_exp2[bndex] += objc->phi_exp2Err[bndex] * epsilon2;
			#endif
		}
	}
	if (fitflag & MFIT_CENTER) {
		epsilon1 = (float) parameter_epsilon_random_draw() * tweakf;
		epsilon2 = (float) parameter_epsilon_random_draw() * tweakf;
		objc->objc_rowc += objc->objc_rowcErr * epsilon1;
		objc->objc_colc += objc->objc_colcErr * epsilon2;
		for (bndex = 0; bndex < NCOLOR; bndex++) {
			objc->rowc[bndex] += objc->rowcErr[bndex] * epsilon1;
			objc->colc[bndex] += objc->colcErr[bndex] * epsilon2;
		}
	}
	if (fitflag & MFIT_E) {
		epsilon1 = (float) parameter_epsilon_random_draw() * tweakf;
		epsilon2 = (float) parameter_epsilon_random_draw() * tweakf;
		epsilon3 = (float) parameter_epsilon_random_draw() * tweakf;
		for (bndex = 0; bndex < NCOLOR; bndex++) {
			if (objc->ab_deV[bndex] != VALUE_IS_BAD) {
				objc->ab_deV[bndex] += objc->ab_deVErr[bndex] * epsilon1;
				pass = pass && (objc->ab_deV[bndex] >= 0.0);
			}
			#if USE_FNAL_OBJC_IO
			if (objc->ab_deV2[bndex] != VALUE_IS_BAD) {
				objc->ab_deV2[bndex] += objc->ab_deV2Err[bndex] * epsilon2;
				pass = pass && (objc->ab_deV2[bndex] >= 0.0);
			}
			if (objc->ab_pl[bndex] != VALUE_IS_BAD) {
				objc->ab_pl[bndex] += objc->ab_plErr[bndex] * epsilon3;
				pass = pass && (objc->ab_pl[bndex] >= 0.0);
			}
			#endif
		}
		epsilon2 = (float) parameter_epsilon_random_draw() * tweakf;
		epsilon1 = (float) parameter_epsilon_random_draw() * tweakf;
		for (bndex = 0; bndex < NCOLOR; bndex++) {
			if (objc->ab_exp[bndex] != VALUE_IS_BAD) {
				objc->ab_exp[bndex] += objc->ab_expErr[bndex] * epsilon1;
				pass = pass && (objc->ab_exp[bndex] >= 0.0);
			}
			#if USE_FNAL_OBJC_IO
			if (objc->ab_exp2[bndex] != VALUE_IS_BAD) {
				objc->ab_exp2[bndex] += objc->ab_exp2Err[bndex] * epsilon2;
				pass = pass && (objc->ab_exp2[bndex] >= 0.0);
			}
			#endif
		}
	}
}
return(SH_SUCCESS);
}

int debug_pass_objc_to_mle(THOBJC *objc, CHAIN *objclist) {

/* 
 * do-mle-lrg --framefile /u/khosrow/thesis/opt/soho/multi-object/images/mle/sdss-data/CD-deV-Pl-G-deV-exp-fpC/stage-1/flag-fitsize-unconstrained-init-1-nstep-9000-nearby-10/301/4207/6/parfiles/ff-004207-6-0113.par -seed 4290660709 -fitsize -gclass DEVEXP_GCLASS -init SDSS_INIT -cD_classify -cdclass DEVPL_GCLASS
 *
 */

char *tname = NULL;
thObjcNameGetFromType(objc->thobjctype, &tname);
if (!strcmp(tname, "CDCANDIDATE")) return((int) 1);
if (shChainSize(objclist) > NMLEOBJC) return((int) 0);
if (!thObjcHasModel(objc, "Exp")) return((int) 0);
THPROP *prop = NULL;
RET_CODE status = thObjcGetPropByMname(objc, "Exp", &prop);
shAssert(status == SH_SUCCESS);
EXPPARS *value = NULL;
char *mname = NULL, *pname = NULL;
status = thPropGet(prop, &mname, &pname, (void **) &value);
shAssert(status == SH_SUCCESS);
shAssert(value != NULL);
int pass = 0;
if (shChainSize(objclist) == 3) {
	pass = (value->xc > 1140.0 && value->xc < 1150.0 && value->yc > 640.0 && value->yc < 650.0);
} else if (shChainSize(objclist) == 4) {
	pass = (value->xc > 1160.0 && value->xc < 1170.0 && value->yc > 530.0 && value->yc < 540.0);
} else if (shChainSize(objclist) == 5) {
	pass = (value->xc > 1060.0 && value->xc < 1070.0 && value->yc > 700.0 && value->yc < 710.0);
} else if (shChainSize(objclist) == 6) {
	pass = (value->xc > 950.0 && value->xc < 960.0 && value->yc > 2000.0 && value->yc < 2010.0);
} else if (shChainSize(objclist) == 7) {
	pass = (value->xc > 1410.0 && value->xc < 1420.0 && value->yc > 340.0 && value->yc < 350.0);
} else if (shChainSize(objclist) == 8) {
	pass = (value->xc > 950.0 && value->xc < 960.0 && value->yc > 690.0 && value->yc < 700.0);
} else if (shChainSize(objclist) == 9) {
	pass = (value->xc > 1040.0 && value->xc < 1050.0 && value->yc > 1020.0 && value->yc < 1030.0);
} else if (shChainSize(objclist) == 10) {
	pass = (value->xc > 1380.0 && value->xc < 1390.0 && value->yc > 360.0 && value->yc < 370.0);
} else if (shChainSize(objclist) == 11) {
	pass = (value->xc > 500.0 && value->xc < 510.0 && value->yc > 960.0 && value->yc < 970.0);
} else if (shChainSize(objclist) == 12) {
	pass = (value->xc > 1430.0 && value->xc < 1440.0 && value->yc > 610.0 && value->yc < 620.0);
} else if (shChainSize(objclist) > 6) {
	pass = 1;

}
return(pass);
}

void output_fitflag_info(char *comment, int fitflag) {
char *name = "output_fitflag_info";

if (fitflag == MFIT_NULL) {
	printf("%s: fitflag = MFIT_NULL \n", name);
	return;
}
if (fitflag) {
	printf("%s fitflag = ", comment); 
	int prev_flag = 0;
	if (fitflag & MFIT_RE) {
		printf("MFIT_RE ");
		prev_flag = 1;
	} else {
		prev_flag = 0;
	}
	if (fitflag & MFIT_RB) {
		printf("MFIT_RB ");
		prev_flag = 1;
	} else {
		prev_flag = 0;
	}

	if (fitflag & MFIT_INDEX) {	
		if (prev_flag) printf("& ");
		printf("MFIT_INDEX ");
		prev_flag = 1;
	} else {
		prev_flag = 0;
	}

	if (fitflag & MFIT_E) {
		if (prev_flag) printf("& ");
		printf("MFIT_E ");
		prev_flag = 1;
	}
	if (fitflag & MFIT_PHI) {
		if (prev_flag) printf("& ");
		printf("MFIT_PHI ");
		prev_flag = 1;
	}
	if (fitflag & MFIT_CENTER) {
		if (prev_flag) printf("& ");
		printf("MFIT_CENTER ");
		prev_flag = 1;
	}
	if (fitflag & MFIT_RE1) {
		printf("MFIT_RE1 ");
		prev_flag = 1;
	} else {
		prev_flag = 0;
	}
	if (fitflag & MFIT_RB1) {
		printf("MFIT_RB1 ");
		prev_flag = 1;
	} else {
		prev_flag = 0;
	}

	if (fitflag & MFIT_INDEX1) {	
		if (prev_flag) printf("& ");
		printf("MFIT_INDEX1 ");
		prev_flag = 1;
	} else {
		prev_flag = 0;
	}

	if (fitflag & MFIT_E1) {
		if (prev_flag) printf("& ");
		printf("MFIT_E1 ");
		prev_flag = 1;
	}
	if (fitflag & MFIT_PHI1) {
		if (prev_flag) printf("& ");
		printf("MFIT_PHI1 ");
		prev_flag = 1;
	}
	if (fitflag & MFIT_CENTER1) {
		if (prev_flag) printf("& ");
		printf("MFIT_CENTER1 ");
		prev_flag = 1;
	}
	if (fitflag & MFIT_RE2) {
		printf("MFIT_RE2 ");
		prev_flag = 1;
	} else {
		prev_flag = 0;
	}
	if (fitflag & MFIT_RB2) {
		printf("MFIT_RB2 ");
		prev_flag = 1;
	} else {
		prev_flag = 0;
	}

	if (fitflag & MFIT_INDEX2) {	
		if (prev_flag) printf("& ");
		printf("MFIT_INDEX2 ");
		prev_flag = 1;
	} else {
		prev_flag = 0;
	}

	if (fitflag & MFIT_E2) {
		if (prev_flag) printf("& ");
		printf("MFIT_E2 ");
		prev_flag = 1;
	}
	if (fitflag & MFIT_PHI2) {
		if (prev_flag) printf("& ");
		printf("MFIT_PHI2 ");
		prev_flag = 1;
	}
	if (fitflag & MFIT_CENTER2) {
		if (prev_flag) printf("& ");
		printf("MFIT_CENTER2 ");
		prev_flag = 1;
	}

	if (fitflag & MFIT_LOCK_CENTER) {
		if (prev_flag) printf("& ");
		printf("MFIT_LOCK_CENTER");
		prev_flag = 1;
	}
	if (fitflag & MFIT_LOCK_RE) {
		if (prev_flag) printf("& ");
		printf("MFIT_LOCK_RE");
		prev_flag = 1;
	}
	printf("\n");
} else {
	printf("%s: fitflag = %x (did not recognize fitflag) \n", comment, fitflag);
	return;
}

return;
}
