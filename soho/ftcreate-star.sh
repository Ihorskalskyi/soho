#!/bin/bash
#stripe 82
#4797
declare -a run_a=(4797 2700 2703 2708 2709 2960 3565 3434 3437 2968 4207 1742 4247 4252 4253 4263 4288 1863 1887 2570 2578 2579 109 125 211 240 241 250 251 256 259 273 287 297 307 94 5036 5042 5052 2873 21
24 1006 1009 1013 1033 1040 1055 1056 1057 4153 4157 4158 4184 4187 4188 4191 4192 2336 2886 2955 3325 3354 3355 3360 3362 3368 2385 2506 2728 4849 4858 4868 4874 4894 4895 4899 4905 4927 4930 4933 4948 2854 2855 2856
 2861 2662 3256 3313 3322 4073 4198 4203 3384 3388 3427 3430 4128 4136 4145 2738 2768 2820 1752 1755 1894 2583 2585 2589 2591 2649 2650 2659 2677 3438 3460 3458 3461 3465 7674 7183)
n4=120
n4=2

for (( i4=0; i4<$n4; i4++ ))
do

run=${run_a[$i4]}
runstr=`printf "%06d" $run`
outdir="/u/khosrow/thesis/opt/soho/single-galaxy/images/sims/circular/deV/301/$run"
outdir="/u/khosrow/thesis/opt/soho/single-galaxy/images/sims/star/301/$run"

simreport_filename="sim-data-$runstr.fits"
coldesc_lisfilename="coldesc-$runstr.lis"
keyword_lisfilename="keyword-$runstr.lis"
simreport_file="$outdir/$simreport_filename"
coldesc_lisfile="$outdir/$coldesc_lisfilename"
keyword_lisfile="$outdir/$keyword_lisfilename"

datalis_filename="sim-data-$runstr.lis"
datalis_file="$outdir/$datalis_filename"

ftcreate_exec="/u/khosrow/lib/heasoft-6.16/x86_64-unknown-linux-gnu-libc2.12/bin/ftcreate"
ftcreate_command="$ftcreate_exec cdfile=$coldesc_lisfile datafile=$datalis_file outfile=$simreport_file headfile=$keywords_lisfile"
($ftcreate_command);

done
