print, "*** START *** "

.compile plot_sdss_mle_sim.pro
.compile plot_sdss_mle_sim.pro
.compile plot_batch_sdss_mle_sim.pro
plot_batch_sdss_mle_sim, /star, /bias

.reset_session
.compile plot_sdss_mle_sim.pro
.compile plot_sdss_mle_sim.pro
.compile plot_batch_sdss_mle_sim.pro
plot_batch_sdss_mle_sim, /circular, /bias, /deV


.reset_session
.compile plot_sdss_mle_sim.pro
.compile plot_sdss_mle_sim.pro
.compile plot_batch_sdss_mle_sim.pro
plot_batch_sdss_mle_sim, /ciruclar, /bias, /exp


.reset_session
.compile plot_sdss_mle_sim.pro
.compile plot_sdss_mle_sim.pro
.compile plot_batch_sdss_mle_sim.pro
plot_batch_sdss_mle_sim, /elliptical, /bias, /deV

.reset_session
.compile plot_sdss_mle_sim.pro
.compile plot_sdss_mle_sim.pro
.compile plot_batch_sdss_mle_sim.pro
plot_batch_sdss_mle_sim, /elliptical, /bias, /exp

print, "*** END ***"
