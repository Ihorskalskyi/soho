#ifndef THSKY_H
#define THSKY_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dervish.h"
#include "phPhotoFitsIO.h"
#include "phSpanUtil.h"
#include "phUtils.h"
#include "phObjc.h"
#include "phDataIo.h"
#include "prvt/region_p.h"

#include "thConsts.h"
#include "thMath.h"
#include "thReg.h"
#include "thSkyTypes.h"
#include "thMask.h"

#ifndef MAX
#define MAX(a,b) \
       ({ typeof (a) _a = (a); \
           typeof (b) _b = (b); \
         _a > _b ? _a : _b; })
#endif
#ifndef MIN
#define MIN(a,b) \
       ({ typeof (a) _a = (a); \
           typeof (b) _b = (b); \
         _a < _b ? _a : _b; })
#endif


RET_CODE thSkycoeffsFitSkybasisset(SKYCOEFFS *sc, /* sky basis coefficients and their covariance */ 
				   SKYBASISSET *sb,  /* sky function basis set */
				   REGION *wp, REGION *data, MASK *mask /* input poisson weight, image, mask */
				   );

RET_CODE thSkycoeffsRead(SKYCOEFFS *sc, PHFITSFILE *fits);
RET_CODE thSkyCoeffWrite(PHFITSFILE *fits, SKYCOEFFS *sc);


RET_CODE thSkymodelFit(SKYMODEL *sm, 
		       SKYBASISSET *sbs,
		       REGION *image, MASK *mask);
RET_CODE thSkymodelReconstruct(SKYMODEL *sm,  /* structure containing the  Sky Model */
			       SKYBASISSET *sbs, /* structure containing the sky basis set */ 
			       SKYCOEFFS *sc /* structure containing the sky expansion coefficients 
							and their covarinace */
			       );
RET_CODE thSkymodelRead(SKYMODEL *sm, PHFITSFILE *fits);
RET_CODE thSkymodelWrite(PHFITSFILE *fits, SKYMODEL *SkyModel);



THPIX thSkybasisDotReg(SKYBASIS *b, REGION *reg, 
		       REGION *wp, MASK *mask, /* wp and mask are optional
						    will be 1 if NULL */
		       unsigned char maskbit);


THPIX thSkybasisDotSkyBasis(SKYBASIS *sb1, SKYBASIS *sb2, /* input sky basis */
			    REGION *wp, MASK *mask, /* input mask */
			    unsigned char maskbit);

THPIX **thSkybasissetMetric(SKYBASISSET *sbs, /* input sky basis */ 
			  REGION *wp, MASK *mask /* poisson statistical weights and the mask */
			  );
THPIX *thSkybasissetDotReg(SKYBASISSET *sb,
			    REGION *reg, 
			   REGION *wp, MASK *mask,
			   unsigned char maskbit);
RET_CODE thSkybasissetAddFuncChain(SKYBASISSET *sbs, /* Basis Function Chain 
								  to be Altered */ 
				   CHAIN *FuncChain /* Func Chain to be Added to 
						       the Original Basis Function Chain */ 
				   );
RET_CODE thSkybasissetAddFunc(SKYBASISSET *sbs, /* Basis Function Chain to be Altered */ 
			      FUNC *func /* Func Chain to be Added to 
					       the Original Basis Function Chain */ 
			      );
RET_CODE thSkybasissetAddAmplChain(SKYBASISSET *sbs, /* Basis Function Set to be Altered */ 
				   CHAIN *AmplChain /* Ampl Chain to be Added to 
						       the Original Basis Function Chain */ 
				   );
RET_CODE thSkybasissetAddAmpl(SKYBASISSET *sbs, /* Basis Function Set to be Altered */ 
			       AMPL *ampl /* Amplifier region info to be Added to 
					     the Original Basis Function Set */ 
			       );

SKYBASIS *thSkybasisGenAmpl(AMPL *Ampl);
SKYBASIS *thSkybasisGenFunc(FUNC *func);

RET_CODE thSse(REGION *model,
	       REGION *data, REGION *wp, MASK *mask,
	       unsigned char maskbit,
	       THPIX *res);


BINREGION *phBinregionFromThBinregion(THBINREGION *source);

#endif
