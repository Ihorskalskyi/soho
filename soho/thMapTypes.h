#ifndef MAPTYPES_H
#define MAPTYPES_H

#include "string.h"
#include "dervish.h"
#include "thConsts.h"
#include "thBasic.h"
#include "thObjcTypes.h"
#include "thFitobjcTypes.h"
#include "thEnumParTypes.h"

typedef enum mobjc_sector {
	ALL_MOBJCS, NONLINEAR_MOBJCS, LINEAR_MOBJCS, ALL_MSKY, 
	N_MOBJC_SECTOR, UNKNOWN_MOBJC_SECTOR
} MOBJC_SECTOR; /* pragma SCHEMA */

typedef enum mflag {
	COALESCE = 0, SEPARATE, 
	N_MFLAG
} MFLAG; /* pragma SCHEMA */

typedef enum mcflag {
	ALTERED = 0, COMPILED,
	N_MCFLAG, UNKNOWN_MCFLAG 
} MCFLAG; /* pragma SCHEMA */

typedef enum mcdcomp {
	HALO_ONLY, HALO_AND_CORE, UNKNOWN_MCDCOMP, N_MCDCOMP
} MCDCOMP; /* pragma SCHEMA */

/* this structure is used to save the parameter characterists in the working space for mapmachine */
typedef struct mrec {
	THOBJCID objcid;
	THOBJCTYPE objctype;
	char *mname; /* model name */
	int band; /* model band */
	char *rname; /* record name */
	char *pname; /* object type in string */

	/* source object or parameter */
	void *ps; /* if map doesn't have schema info embedded, 
		this record is only used when referring to the whole parameter (not thobjc) */
	TYPE ptype; /* record or object type in TYPE */	
	#if MAP_HAS_SCHEMA_INFO
	char *etype; /* record type as STRING */
	#endif
	void *objc;
} MREC; /* pragma IGNORE */

typedef struct mapmachine {

  /* characterization of the map, the source id and the targert id */
  char *mapid; 
  char *mapsource;
  char *maptarget;

  /* the following is being developed to address the needs in the L-package (Mle)  */
  char **mnames; /* a string array of size [namp] - the name of the models */
  int *bands; /* a string showing band for each model */
  char **pnames; /* a string array of size [namp] - the name (type) of the input parameter for the models */
  char ***enames; /* a string array of size [namp][nmpar] */

  int *nmpar; /* real number of records for the parameter specifying model i */
  int **accnmpar; /* location of parameter j in model i in a list made of rnames[i] */
  int **ploc; /* mapping between record number and parameter number of the list */
  int **invploc; /* if invpploc[j][i] then derivate of model[i] with respect to parameter [j] is needed */

  /* mapping between object pars (parname_modelname) and parameter index 
 *  particularly useful for parameter cost calculation */

  int *nobjcpar;
  char ***robjcnames; 
  int **pobjcloc;
  int ***invpobjcloc;

  int namp;
  int npar;
  int nobjc;
  int npobjc; 
  int naobjc; 
  int nacc; /* this is the ceiling for the accnmpar */
  
/* second order fitting */ 

  int *DDnmpar; /* real number of records for the parameter specifying model i */
  int ***DDaccnmpar; /* location of parameter j in model i in a list made of rnames[i] */
  int ***DDploc; /* mapping between record number and parameter number of the list */
  int ***DDinvploc; /* if invpploc[j][i] then derivate of model[i] with respect to parameter [j] is needed */

  /* mapping between object pars (parname_modelname) and parameter index 
 *  particularly useful for parameter cost calculation */

  int *DDnobjcpar;
  char ***DDrobjcnames; 
  int **DDpobjcloc;
  int ***DDinvpobjcloc;

  int npp;
  int DDnamp;
  int DDnpar;
  int DDnobjc;
  int DDnpobjc; 
  int DDnacc; /* this is the ceiling for the DDaccnmpar */
  
/* 
     source and target parameters - they should be saved elsewhere only referred to here
     void **ps is an array of objects (data structures) of size [namp] where ps[i] is the input for model i
     THPIX *pt is an array of real parameters of size [npar] which are used by the L-package (Mle)
  */

  void **uobjcs; /* [nobjc] unique object list of type THOBJC */ 
  void **upobjcs; /* [npobjc] object list with non linear fit */
  void **uaobjcs; /* [naobjc] object list within linear fit only */
  void **objcs;  /* [namp] objects of type THOBJC */
  void **ps;   /* [namp] parameters of the models */
  #if MAP_HAS_SCHEMA_INFO
  void ***rs;  /* [namp][nmpar] records within the objects */
  char ***etypes; /* [namp][nmpar] type name of the records within the object */
  TYPE **rtypes; /* [namp][nmpar] type of the records within the objects */
  #endif
  THPIX *pt;
  
  /* work space before M-compilations */
  CHAIN *alist;
  CHAIN *plist;
 
  /* working band - this is set for single band fits */
  int band;
  /* alteration - compilation flag */
  MCFLAG cflag;

  /* private stuff */
  void *parent;
} MAPMACHINE; /* pragma IGNORE */ 


typedef struct pardef {
  
  char *id; /* id of the object for which this parameter is defined */
  char *type; /* type of the object for which this parameter is defined */

  /* parameter specification */
  char *pname; /* name of this parameter */
  int pindex; /* index of this parameter in the type given above */

  /* value */
  THPAR_TYPE ptype;  
  void *pval;

} PARDEF; /* pragma IGNORE */

typedef struct parlist {

  char *id; /* id of this par list */
  CHAIN *pars; /* list of par defs */
  
} PARLIST; /* pragma IGNORE */

typedef struct mfit_rname_loc {
	int isize, icenter, nsize, ncenter;
} MFIT_RNAME_LOC; /* pragma IGNORE */

void thPardefPut(PARDEF *pd, char *id, char *type, char *pname, int *pindex, THPAR_TYPE *ptype, void *pval);

/* map machine basic handlers */

#if 0 /* old version - isn't supported anymore */
MAPMACHINE *thMapmachineNew(int nsource, int ntarget);
RET_CODE thMapmachineDel(MAPMACHINE *mm);
RET_CODE thMapmachinePut(MAPMACHINE *mm, 
			 char *mid, char *msource, char *mtarget, 
			 THPIX **map, THPIX *t0);
#endif

/* transform parameters using the map machine */
RET_CODE thMapmachineDoBasic(MAPMACHINE *mm, THPIX **s, THPIX **t);

/* the parameter list handlers */
PARDEF *thPardefNew(char *id);
void thPardefDel(PARDEF *pardef);

PARLIST *thParlistNew(char *id);
void thParlistDel(PARLIST *parlist);
RET_CODE thParlistAddPardef(PARLIST *parlist, PARDEF *pardef);

THPIX *thFetchObjcPar(FITOBJC *objc, PARDEF *pardef, RET_CODE *status);
THPIX **thFetchObjcParlist(FITOBJC *objc, PARLIST *parlist, 
			   int *npar, RET_CODE *status);



/* mapmachine constructor(s) and deconstructor */
MAPMACHINE *thMapmachineNew();
RET_CODE thMapmachineDel(MAPMACHINE *map);
RET_CODE thMapmachineCopy(MAPMACHINE *s, MAPMACHINE *t);

/* parameter list operation of maps on FITOBJCs */

RET_CODE thMapmachineDoFitobjc(MAPMACHINE *mm, FITOBJC *objc1, FITOBJC *objc2);

/* the following is being developed based on the need from the L-package */
RET_CODE thMapmachineGetNpar(MAPMACHINE *map, int *npar);
RET_CODE thMapmachineGetNamp(MAPMACHINE *map, int *namp);
RET_CODE thMapmachinePutNmodel(MAPMACHINE *map, int nmodel);
RET_CODE thMapmachinePutNpar(MAPMACHINE *map, int npar);
RET_CODE thMapmachinePutNobjc(MAPMACHINE *map, int nobjc);

RET_CODE thMapmachineGetModelPars(MAPMACHINE *map, int i, int *nmpar, int **ploc);
RET_CODE thMapmachineGetMname(MAPMACHINE *map, int i, char **mname);
RET_CODE thMapmachineGetPname(MAPMACHINE *map, int i, char **pname);

RET_CODE thMapmachinePutPs(MAPMACHINE *map, void **ps);
RET_CODE thMapmachineGetPs(MAPMACHINE *map, void ***ps);
RET_CODE thMapmachinePutPt(MAPMACHINE *map, THPIX *pt);
RET_CODE thMapmachineGetPt(MAPMACHINE *map, THPIX **pt);
RET_CODE thMapmachineGetObjcs(MAPMACHINE *map, void ***objcs, int *nm);
RET_CODE thMapmachineGetUObjcs(MAPMACHINE *map, void ***uobjcs, int *nobjc);
RET_CODE thMapmachineGetUPObjcs(MAPMACHINE *map, void ***upobjcs, int *npobjc);
RET_CODE thMapmachineGetUAObjcs(MAPMACHINE *map, void ***uaobjcs, int *naobjc);

RET_CODE thMapmachineGetCflag(MAPMACHINE *map, MCFLAG *cflag);
RET_CODE thMapmachinePutCflag(MAPMACHINE *map, MCFLAG cflag);

RET_CODE thMAccnmparGet(MAPMACHINE *map, int i, int j, int *a);
RET_CODE thDDMAccnmparGet(MAPMACHINE *map, int i, int j, int k, int *a);

/* MREC handlers */
MREC *thMrecNew(THOBJC *objc, char *mname, char *rname, int band);
void thMrecDel(MREC *mrec);
MREC *thMrecDup(MREC *s);

RET_CODE thMrecGetObjc(MREC *mrec, THOBJC **objc);
#if MAP_HAS_SCHEMA_INFO
RET_CODE thMrecGetPtypePs(MREC *mrec, TYPE *ptype, char **etype, void **ps);
#else
RET_CODE thMrecGetPtypePs(MREC *mrec, TYPE *ptype, void **ps);
#endif

RET_CODE thMrecGetPname(MREC *mrec, char **pname);

RET_CODE thMGetInModelMLocOfP(MAPMACHINE *map, int i, int j, int *k);
RET_CODE thMGetInModelMLocOfPP(MAPMACHINE *map, int imodel, int jpar1, int jpar2, int *k);

RET_CODE thMapGetObjcArr(MAPMACHINE *map, MOBJC_SECTOR sector, THOBJC ***objcs, int *nobjc);
RET_CODE thMapGetLpars(MAPMACHINE *map, int *nm, int *np, int *npp, int *nc, int *nacc, int *nj);

RET_CODE thMapGetObjcRnamesPndex(MAPMACHINE *map, void *objc, char ***rnames, int *nrnames,int **pndex, int *npndex);

RET_CODE thMapmachineModelIndexGetFromThprop(MAPMACHINE *map, THPROP *prop, int *imodel);

RET_CODE thMapmachineGetModelInfo(MAPMACHINE *map, int i, char **mname, void **p, void **objc);

MFIT_RNAME_LOC *thMfitRnameLocNew();
void thMfitRnameLocDel(MFIT_RNAME_LOC *x);

#endif

