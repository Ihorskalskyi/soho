#!/bin/bash
#export comment="sersic-Exp long runs"
#export start_id=5129999
#export end_id=5131983

#echo "START - holding jobs with id between $start_id, $end_id"
#eval condor_hold {$start_id..$end_id}.0
#echo "END - held jobs with id between $start_id, $end_id"

#export comment="sersic-Exp long runs"
#export start_id=5131900
#export end_id=5131983
#
#echo "START - releasing jobs with id between $start_id, $end_id"
#eval condor_release {$start_id..$end_id}.0
#echo "END - released jobs with id between $start_id, $end_id"

#export comment="sersic-Exp long runs"
#export start_id=5131600
#export end_id=5131700
#
#echo "START - releasing jobs with id between $start_id, $end_id"
#eval condor_release {$start_id..$end_id}.0
#echo "END - released jobs with id between $start_id, $end_id"

#export comment="sersic-Exp long runs"
#export start_id=5131612
#export end_id=5132767
#
#echo "START - releasing jobs with id between $start_id, $end_id"
#eval condor_hold {$start_id..$end_id}.0
#echo "END - released jobs with id between $start_id, $end_id"

#export comment="sersic long runs"
#export start_id=5132777
#export end_id=5133616

#echo "START - releasing jobs with id between $start_id, $end_id"
#eval condor_hold {$start_id..$end_id}.0
#echo "END - released jobs with id between $start_id, $end_id"

export comment="sersic-sersic long runs"
export start_id=5133622
export end_id=5134753

echo "START - releasing jobs with id between $start_id, $end_id"
eval condor_hold {$start_id..$end_id}.0
echo "successfuly held: $comment"
echo "END - released jobs with id between $start_id, $end_id"


