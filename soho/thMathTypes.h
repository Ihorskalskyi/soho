#ifndef THMATHTYPES_H
#define THMATHTYPES_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "dervish.h"
#include "prvt/region_p.h"

#include "phMathUtils.h"

#include "thConsts.h"
#include "thBasic.h"

#ifndef MAX
#define MAX(a,b) \
       ({ typeof (a) _a = (a); \
           typeof (b) _b = (b); \
         _a > _b ? _a : _b; })
#endif
#ifndef MIN
#define MIN(a,b) \
       ({ typeof (a) _a = (a); \
           typeof (b) _b = (b); \
         _a < _b ? _a : _b; })
#endif

#ifndef STEPFUNC
#define STEPFUNC(a) \
       ({ typeof (a) _a = 0.5 * (1.0 + tanh(STEPSHARPNESS * (1.0 - a))); \
         _a; })
#endif


/* function handling */
#define FUNC_N_INDICES 2
#define FUNC_N_PARS 5

/* mathematical constants */
#ifndef THPI
#define THPI 3.14159265
#endif

typedef struct Vec {
  int dim;
  int r; /* index */ 
  THPIX *vec;
  int child;
} VECT;

typedef struct Func {

  char *name;
  int row0, col0; /* 
		     used when you want to represent 
		     a subregion within a bigger region
		  */
  
  THPIX rowc, colc;

  /* 
     the following arrays of pointer are the ones that are
     referred to throuought the code. 
     the previous explicit tags are used only for FitsIO.
  */

  int **indices; /*[FUNC_N_INDICES]; */
  THPIX **pars; /*[FUNC_N_PARS]; */
  
  

  REGION *reg;

  /* 
     the following are explicitly defined because photoFitsIo
     is unable to write heaps with arbitrary type. So I am using non-heap
     elements and refer to them inside the arrays of pointers
     defined above.
  */

   
  int index0, index1, index2, index3, index4, index5, 
    index6, index7, index8, index9, index10;

  THPIX par0, par1, par2, par3, par4, par5, 
    par6, par7, par8, par9, par10;  

} FUNC;

VECT *thVectNew(int dim);
RET_CODE thVectDel(VECT *vec);

FUNC *thFuncNew();
RET_CODE thFuncDel(FUNC *func);
RET_CODE thFuncPut(FUNC *func, 
		   int row0, int col0, 
		   int rowc, int colc,
		   int *indices, THPIX *pars,  
		   REGION *reg);

#endif

