#!/bin/bash

source "./write_parfile.sh"
source "./write_condorjob.sh"
source "./find_npsf.sh"

any_with_ext () ( 

local mydir="$1"
local mypattern="$2"
local any=false
if test -n "$(find $mydir -maxdepth 1 -name $mypattern -print -quit)"
then
any=true
fi
echo $any
)

function count_files_in_dir {
local name="count_files_in_dir"

local Options=$@
local Optnum=$#

local lpath=no
local path=empty

while getopts ':-:' OPTION ; do
  case "$OPTION" in
    -  ) [ $OPTIND -ge 1 ] && optind=$(expr $OPTIND - 1 ) || optind=$OPTIND
         eval OPTION="\$$optind"
         OPTARG=$(echo $OPTION | cut -d'=' -f2)
         OPTION=$(echo $OPTION | cut -d'=' -f1)
         case $OPTION in
	     --path       ) lpath=yes; path="$OPTARG"	 ;;
	     * )  echo "$name: invalide options (long)" ;;
         esac
       OPTIND=1
       shift
      ;;
    ? )  echo "$name: invalid options (short) "  ;;
  esac
done

if [ $lpath == no ]; then
	echo "$name: not enough arguments set"
	echo -1
	exit
fi
if [ "$path" == empty ]; then
	echo "$name: path not set"
	echo -1
	exit
fi
file=0
if [ -e $path ]; then
	for fd in `ls -A $path` ; do
		if [ -e "$fd" ]; then
			((file++))
		fi
	done
fi
echo $file
}

name="sim-script-single"

SOHO_DIR="/u/khosrow/thesis/opt/soho"

debug=no
double=no
verbose=no

TODAY=$(date +"%m-%d-%Y")
NOW=$(date +"%T") 

logdir="$SOHO_DIR/log"

Options=$@
Optnum=$#


nrow=1489
ncol=2048

xc=`echo "$nrow/2 + 0.570" | bc`
yc=`echo "$ncol/2 + 0.570" | bc`

lroot=no
lfield=no
lfield0=no
lrun=no
lrerun=no
lcamcol=no
lbands=no
lLOGFILE=no
lcondordir=no
lnall=no

root=empty
field=empty
field0=empty
run=empty
camcol=empty
bands=empty
condordir=empty
nall=empty

droot="$SOHO_DIR/single-galaxy/images"
dfield=101
drun=$DEFAULT_SDSS_RUN
drerun="137"
dcamcol=4
dbands="ugriz"
dLOGFILE="$SOHO_DIR/single-galaxy-process-list.txt"
dcondordir="$SOHO_DIR/condor"
dnall=5000
tempfile="./temp-condor-submit.output"

while getopts ':-:' OPTION ; do
  case "$OPTION" in
    -  ) [ $OPTIND -ge 1 ] && optind=$(expr $OPTIND - 1 ) || optind=$OPTIND
         eval OPTION="\$$optind"
         OPTARG=$(echo $OPTION | cut -d'=' -f2)
         OPTION=$(echo $OPTION | cut -d'=' -f1)
         case $OPTION in
	     --debug     ) debug=yes			 ;;
	     --double    ) double=yes                    ;;
     	     --verbose   ) verbose=yes			 ;;
	     --root      ) lroot=yes; root="$OPTARG"     ;;
	     --field	 ) lfield=yes; field="$OPTARG"   ;;
	     --field0    ) lfield0=yes; field0="$OPTARG" ;;
	     --run       ) lrun=yes; run="$OPTARG"	 ;;
	     --rerun     ) lrerun=yes; rerun="$OPTARG"    ;;
	     --camcol    ) lcamcol=yes; camcol="$OPTARG" ;;
	     --bands      ) lbands=yes; bands="$OPTARG"	 ;;
	     --logfile   ) lLOGFILE=yes; LOGFILE="$OPTARG" ;;
             --condordir ) lcondordir=yes; condordir="$OPTARG" ;;     
             --nall      ) lnall=yes; nall="$OPTARG"	;;
	* )  echo "$name: invalide options (long)" ;;
         esac
       OPTIND=1
       shift
      ;;
    ? )  echo "$name: invalid options (short) "  ;;
  esac
done

if [ $lroot == no ]; then
	root=$droot
	echo "$name: argument (root)   set to '$root'"
fi
if [ $lfield == no ]; then
	field=$dfield
	echo "$name: argument (field)  set to '$field'"
fi
if [ $lrun == no ]; then
	run=$drun
	echo "$name: argument (run)    set to '$run'"
fi
if [ $lrerun == no ]; then
	rerun=$drerun;
	echo "$name: argument (rerun)  set to '$rerun'"
fi
if [ $lcamcol == no ]; then
	camcol=$dcamcol
	echo "$name: argument (camcol) set to '$camcol'"
fi
if [ $lbands == no ]; then
	bands=$dbands
	echo "$name: argument (bands)   set to '$bands'"
fi
if [ $lLOGFILE == no ]; then
	LOGFILE=$dLOGFILE
	echo "$name: argument (logfile) set to '$LOGFILE'"
fi
if [ $lfield0 == no ]; then
	field0=$field
	echo "$name: argument (field0) set to '$field0'"
fi
if [ $lcondordir == no ]; then
	condordir=$dcondordir
	echo "$name: argument (condordir) set to '$condordir'"
fi
if [ $lnall == no ]; then
	nall=$dnall
	echo "$name: argument (nall) set to '$nall'"
fi
if [ $nall -lt 0 ]; then
	echo "$name: argument (nall) set to invalid value '$nall'"
	exit
fi


echo "$name: debug    = $debug"
echo "$name: verbose  = $verbose"
echo "$name: EXTRA_FLAGS = '$EXTRA_FLAGS'"

if [ ! -d $condordir ]; then
	echo "$name: making (condordir) '$condordir'"
	mkdir -p $condordir
fi
clogdir="$condordir/log"
cerrordir="$condordir/error"
coutputdir="$condordir/output"
csubmitdir="$condordir/submit"
objcdir="$outdir/objc"

if [ ! -d $clogdir ]; then
	echo "$name: making (clogdir) '$clogdir'"
	mkdir -p $clogdir
fi
if [ ! -d $cerrordir ]; then
	echo "$name: making (cerrordir) '$cerrordir'"
	mkdir -p $cerrordir
fi
if [ ! -d $coutputdir ]; then
	echo "$name: making (coutputdir) '$coutputdir'"
	mkdir -p $coutputdir
fi
if [ ! -d $csubmitdir ]; then
	echo "$name: making (csubmitdir) '$csubmitdir'"
	mkdir -p $csubmitdir
fi
if [ ! -d $parfiledir ]; then
	echo "$name: making (parfiledir) '$parfiledir'"
	mkdir -p $parfiledir
fi
if [ ! -d $datadir ]; then
	echo "$name: making (datadir) '$datadir'"
	mkdir -p $datadir
fi
if [ ! -d $outdir ]; then
	echo "$name: making (outdir) '$outdir'"
	mkdir -p $outdir
fi
if [ ! -d $objcdir ]; then
	echo "$name: making (objcdir) '$objcdir'"
	mkdir -p $objcdir
fi

srcdir_redux="$PHOTO_REDUX/$DEFAULT_SDSS_RERUN/$run/objcs/$camcol"
srcdir_data="$PHOTO_DATA/$DEFAULT_SDSS_RERUN/$run/objcs/$camcol"
srcdir_calib="$PHOTO_CALIB/$DEFAULT_SDSS_RERUN/$run/objcs/$camcol"
if [ ! -d $srcdir_redux ]; then 
	echo "$name: SDSS source directory '$srcdir_redux' does not exist"
	exit
fi


nband=${#bands}
for (( iband=0; iband<$nband; iband++ ))
do
	band=${bands:$iband:1}
	if [[ $band != "u" && $band != "g" && $band != "r" && $band != "i" && $band != "z" ]]; then
		echo "$name: undefined band ($band) found in (bands = $bands)"
		exit
	fi
done

echo "DATE: $TODAY, TIME: $NOW" >>$LOGFILE
echo "run, camcol, field, sky1, sky2, model1, counts1, re1, e1, phi1, modelname2, counts2, re2, e2, phi2, dir" >>$LOGFILE

declare -a usky_a=(35 26 30 27 27)
declare -a gsky_a=(85 91 85 84 87)
declare -a rsky_a=(60 56 58 57 55)
declare -a isky_a=(37 48 40 35 38)
declare -a zsky_a=(31 30 26 28 26)

runstr=`printf "%06d" $run` 

declare -a psf_a=(9999 999 99 90 80 70 60 50 40 30 20 10)
##nband=5 # no reason to be here Feb 2014
npsf=1
nfield=5

#defining simulation parameters for re-counts simulations
declare -a sky_a=(50 100 150 200)
declare -a model_a=("deV" "exp" "sersic")
declare -a counts1_a=(50 100 200 300 500 750 1000 1500 2000 3000 5000 7500 10000 15000 20000 50000 100000 150000 200000 500000 750000 1000000)
declare -a counts2_a=$counts1_a
declare -a counts21_ratio_a=("0.001" "0.005" "0.01" "0.05" "0.1" "0.2" "0.5" "0.75" "1.0" "1.5" "2.0" "3.0" "4.0" "10.0")
declare -a re_a=(2.0 3.0 4.0 5.0 7.0 10.0 15.0 20.0 25.0 30.0 50.0 75.0 100.0 150.0 200.0 250.0 500.0)
declare -a e_a=(0.0 0.05 0.1 0.2 0.5 0.7 0.01 0.02 0.05 0.075 0.1 0.2 0.3 0.5)
declare -a phi_a=(0.0 10.0 20.0 30.0 40.0 50.0 60.0 70.0 80.0 90.0)
declare -a n_a=(0.5 0.75 1.0 2.0 3.0 4.0 5.0 7.5 10.0 12.0)


nsky=2
nmodel=2
ncounts1=20
ncounts2=$ncounts1
ncounts21_ratio=14
nre=17
ne=4
nphi=1
nn=10



#defining simulation parameters for ellipticity simulations
declare -a sky_a=(50 100 150 200)
declare -a model_a=("deV" "exp" "sersic")
declare -a counts1_a=(5000 15000 30000 80000 200000)
declare -a counts2_a=$counts1_a
declare -a counts21_ratio_a=(0.0)
declare -a re_a=(2.0 4.0 7.5 15.0 30.0 55.0 100.0 200.0)
declare -a e_a=(0.0 0.01 0.02 0.03 0.04 0.05 0.07 0.10 0.12 0.15 0.17 0.20 0.22 0.25 0.27 0.30 0.32 0.35 0.37 0.40 0.45 0.50 0.55 0.60 0.65 0.70 0.75 0.80 0.85 0.9 0.95)
declare -a phi_a=(0.0 30.0 90.0)
declare -a n_a=(0.5 0.75 1.0 2.0 3.0 4.0 5.0 7.5 10.0 12.0)

nsky=2 ##
nmodel=2 ##
ncounts1=5 ##
ncounts2=$ncounts1
ncounts21_ratio=1
nre=8 ##
ne=31 ##
nphi=3 ##
nn=10


COUNTS2_CUT="1.0" #cut for excluding underluminous objects
#end of definition 

maxseed=4294967295 # the biggest unsigned int that can be passed as a seed to simulator 

if [ $double == yes ]; then
ntotal=$(perl -e "printf "%d", $nsky*$ncounts1*$ncounts21_ratio*$nre*$nre*$ne*$ne*$nphi*$nphi")
else
ntotal=$(perl -e "printf "%d", $nsky*$ncounts1*$nre*$ne*$nphi")
fi

echo "$name: n-total     = $ntotal"
echo "$name: n-simulated = $nall"

fillfactor=$(perl -e "printf "%g", $nall/$ntotal")

#echo "$name: corrected n-total = $ntotal"
#echo "$name: corrected n-all   = $nall"
echo "$name: parameter space filling factor = $fillfactor"

ifield=$field
ifield0=$field0
field0=`printf "%04d" $ifield0`

ipsf=0
f1=${psf_a[$ipsf]}
f2=$f1
executable="$SOHO_DIR/do-sim-$f1-$f2"

npsf=$(find_npsf --run=$run --camcol=$camcol --field0=$field0 --band=$band)
echo "$name: npsf = $npsf"
nsim=$nall
njob=0 # number of jobs submitted so far 
if [ $double == yes ]; then

	for (( imodel1=0; imodel1 <$nmodel; imodel1++ ))
	do
	modelname1=${model_a[$imodel1]}

	for (( imodel2=0; imodel2 <$nmodel; imodel2++ ))
	do
	modelname2=${model_a[$imodel2]}

	modelstr="$modelname1-$modelname2"
	echo ""
	echo -n "$modelstr: "

	#first see if the directory has been handled before (some multiple runs are necessary due to disk space problem)
	dir="$root/$modelname1-$modelname2/$f1-$f2"
	pattern1="fpC*.fit"
	pattern2="fpC*.fit.gz"
	found1=$( any_with_ext $dir $pattern1 ); 
	found2=$( any_with_ext $dir $pattern2 ); 
	if $found1 || $found2 ; then
		echo "skipping model - seems to have been handled before - found files of type $pattern1, $pattern2"
	else
	isim=0
	nsim=$nall
	ifield=$ifield0

	while [[ $isim -lt $nsim ]]
	do
	nrand=$(python -S -c "import random; print random.randrange(1,$ntotal)")	
	
	let "isky = $nrand % $nsky"
	sky1=${sky_a[$isky]}
	sky2=${sky_a[$isky]}

	let "nrand = $nrand / $nsky"
	let "icounts1 = $nrand % $ncounts1"
	counts1=${counts1_a[$icounts1]}

	let "nrand = $nrand / $ncounts1"
	let "ire1 = $nrand % $nre"
	re1=${re_a[$ire1]}

	let "nrand = $nrand / $nre"
	let "ie1 = $nrand % $ne"
	e1=${e_a[$ie1]}

	let "nrand = $nrand / $ne"
	let "iphi1 = $nrand % $nphi"
	phi1=${phi_a[$iphi1]}

	let "nrand = $nrand / $nphi"
	let "icounts21 = $nrand % $ncounts21_ratio"
	counts21=${counts21_ratio_a[$icounts21]}
	counts2=$(perl -e "printf "%g", $counts1*$counts21")

	let "nrand = $nrand / $ncounts21_ratio"
	let "ire2 = $nrand % $nre"
	re2=${re_a[$ire2]}

	let "nrand = $nrand / $nre"
	let "ie2= $nrand % $ne"
	e2=${e_a[$ie2]}

	let "nrand = $nrand / $ne"
	let "iphi2 = $nrand % $nphi"
	phi2=${phi_a[$iphi2]}

	lumcondition=$(echo "$counts2 > $COUNTS2_CUT" | bc) 

	if [ $lumcondition -ne 0 ]; then

		for (( iband=0; iband<$nband; iband++ )) 
		do

		band=${bands:$iband:1}
	
		skyname=$band."sky_a"
		sky1=${sky_a[$isky]}
		sky2=${sky_a[$isky]}


		let isim++
		modelskystring="-sky '-s00 $sky1 -z00 $sky2'"
		modelstring1="-$modelname1 '-counts $counts1 -xc $xc -yc $yc -re $re1 -e $e1 -phi $phi1'"
		modelstring2="-$modelname2 '-counts $counts2 -xc $xc -yc $yc -re $re2 -e $e2 -phi $phi2'"

		seed=$(python -S -c "import random; print random.randrange(1,$maxseed)")

		#designing the file names #

		modelstr="$modelname1-$modelname2"
		field=`printf "%04d" $ifield`
		let "ipsffield = $field0 + ( $ifield - $field0 ) % $npsf"
		psffield=`printf "%04d" $ipsffield`
		dir="$root/$modelname1-$modelname2/$f1-$f2"
		framefile="$dir/ff-$runstr-$field-$band$camcol.par"
		
		condor_prefix="$condordir/condor-sim-$modelstr-$runstr-$field-$band$camcol"
		condor_outputfile="$condor_prefix.out"	
		condor_submitfile="$condor_prefix.submit"
		condor_errorfile="$condor_prefix.error"
		condor_logfile="$condor_prefix.log"
		condor_argument="\"-band $band -camcol $camcol -run $run -rerun $rerun -field $field -framefile $framefile -noobjcfile  -seed $seed $modelstring1 $modelstring2 $modelskystring\""	
		condor_comment="date: $DATE, time: $TIME"
	
		write_parfile --root=$dir --band=$band --camcol=$camcol --run=$run --field=$field --field0=$field0 --psffield=$psffield --output=$framefile

		write_condorjob --restrict --submitfile=$condor_submitfile --errorfile=$condor_errorfile --logfile=$condor_logfile --outputfile=$condor_outputfile --executable="$executable" --arguments="$condor_argument" --comment="$condor_comment"	
	
		if [ $verbose == yes ]; then

			echo "band=$band camcol=$camcol run=$run field=$field psf=$f1"
			echo "model1: $modelstring1"
			echo "model2: $modelstring2"
			echo "sky:    $modelskystring"
			echo "condor: $condor_submitfile"

		else 

			echo -n "."

		fi

		if [ $debug == yes ]; then 
			echo "condor: $condor_submitfile"
		else
			let njob++
			eval "condor_submit $condor_submitfile >> $tempfile"
		fi
		done # for band loop 

		let ifield++	
		echo "$run, $camcol, $field, $sky1, $sky2, $modelname1, $counts1, $re1, $e1, $phi1, $modelname2, $counts2, $re2, $e2, $phi2, $dir" >> $LOGFILE

		
	
	fi

	done

	fi

	#freeing disk space by zipping
	if [ $debug == noi ]; then

		echo ""
		echo "$name: zipping fpC and fpCC files in $dir"
		pattern="fpC*fit"
		path="$dir/$pattern"
		zipcmd="gzip -f $path"
		while $( any_with_ext $dir $pattern ) ; do
			echo -n "-"
			($zipcmd);
			echo -n "."
		done
		echo " "
	
	fi


done
done

else

	for (( imodel1=0; imodel1 <$nmodel; imodel1++ ))
	do
	#ntotal=$ntotal0
	modelname1=${model_a[$imodel1]}
	ifield=$(( 10#$field0 ))
	echo ""
	echo -n "$modelname1: "

	for (( isky=0; isky<$nsky; isky++ ))
	do
	sky1=${sky_a[$isky]}
	sky2=${sky_a[$isky]}
	modelskystring="-sky '-s00 $sky1 -z00 $sky2'"

	for (( icounts1=0; icounts1<$ncounts1; icounts1++ ))
	do
	counts1=${counts1_a[$icounts1]}

	for (( ire1=0; ire1<$nre; ire1++ ))
	do
	re1=${re_a[$ire1]}

	for (( ie1=0; ie1<$ne; ie1++ ))
	do
	e1=${e_a[$ie1]}

	for (( iphi1=0; iphi1<$nphi; iphi1++ ))
	do
	phi1=${phi_a[$iphi1]}
	modelstring1="-$modelname1 '-counts $counts1 -xc $xc -yc $yc -re $re1 -e $e1 -phi $phi1'"

	randport=$(python -S -c "import random; print random.randrange(1,$ntotal)")
	echo "$randport, $nall, $ntotal"
	if [ $randport -le $nall ]; then

		for (( iband=0; iband<$nband; iband++ )) 
		do

		band=${bands:$iband:1}
	
		sky1=${sky_a[$isky]}
		sky2=${sky_a[$isky]}
		
		modelskystring="-sky '-s00 $sky1 -z00 $sky2'"
		modelstring1="-$modelname1 '-counts $counts1 -xc $xc -yc $yc -re $re1 -e $e1 -phi $phi1'"
		modelstring2="-$modelname2 '-counts $counts2 -xc $xc -yc $yc -re $re2 -e $e2 -phi $phi2'"


		seed=$(python -S -c "import random; print random.randrange(1,$maxseed)")
		
		#designing the file names #
		modelstr=$modelname1	
		field=`printf "%04d" $ifield`
		let "ipsffield = $field0 + ($ifield - $field0) % $npsf"
		psffield=`printf "%04d" $ipsffield`
		dir="$root/$modelname1/$f1-$f2"
		framefile="$dir/ff-$runstr-$field-$band$camcol.par"

		condorprefix="condor-sim-$modelstr-$runstr-$fieldstr-$band$camcol"
		condor_submitfile="$csubmitdir/$condorprefix.submit"
		condor_errorfile="$cerrordir/$condorprefix.error"
		condor_logfile="$clogdir/$condorprefix.log"
		condor_outputfile="$coutputdir/$condorprefix.out"

		condor_argument="\"-band $band -camcol $camcol -run $run -rerun $rerun -field $field -framefile $framefile -noobjcfile -seed $seed $modelstring1 $modelskystring\""
		condor_comment="date: $TODAY, time: $NOW"

		if [ $verbose == yes ]; then 

			echo "... $name ..."
			echo "condor_arguments: $condor_argument"
			echo "... ..... ..."	

		else

			echo -n "."

		fi

		write_parfile --root=$dir --band=$band --camcol=$camcol --run=$run --field=$field --field0=$field0 --psffield=$psffield --output=$framefile

		write_condorjob --restrict --submitfile=$condor_submitfile --errorfile=$condor_errorfile --logfile=$condor_logfile --outputfile=$condor_outputfile --executable="$executable" --arguments="$condor_argument" --comment="$condor_comment"
	
		if [ $verbose == yes ]; then

			echo "model1: $modelstring1"
			echo "sky:    $modelskystring"
			echo "condor: $condor_submitfile"

		else

			echo -n "."

		fi
	
		if [ $debug == yes ]; then 
			echo "condor: band = $band, ifield = $ifield, field = $field, field0 = $field0, $condor_submitfile"
		else
			let njob++
			eval "condor_submit $condor_submitfile >> $tempfile"
		fi

		if [[ $verbose == yes ]]; then 
			echo "$name: $condor_submitfile"
		else 
		if [ $debug == yes ]; then
			echo -n 'e'
		else 

			if [ $docondor == yes ]; then
 				if [ $lcommandfile == yes ]; then
					echo "condor_submit $condor_submitfile >> $tempfile" >> $commandfile
				else
					eval "condor_submit $condor_submitfile >> $tempfile"
				fi
			else
				echo "$name: $executable $condor_argument >> $tempfile"
				eval "$executable $condor_argument >> $tempfile"

			fi

			echo -n "."
		fi




		done #for band loop	
		let ifield++;

		echo "$run, $camcol, $field, $sky1, $sky2, $modelname1, $counts1, $re1, $e1, $phi1, $modelname2, $counts2, $re2, $e2, $phi2, $dir" >> $LOGFILE
		
	fi

done
done
done
done
done
	#freeing disk space by zipping
	if [ $debug == no ]; then
	
		echo ""
		echo "$name: zipping fpC and fpCC files in $dir"
		pattern="fpC*fit"
		path="$dir/$pattern"
		zipcmd="gzip -f $path"
		while $( any_with_ext $dir $pattern ) ; do
			echo -n "-"
			($zipcmd);
			echo -n "."
		done
		echo " "

	fi
done

fi

echo "."

echo "$name: ($njob) jobs submitted"

