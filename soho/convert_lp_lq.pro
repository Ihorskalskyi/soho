pro convert_lp_lq, root = root, force = force

	if (root eq "") then begin
		root = "/u/khosrow/thesis/opt/soho/multi-object/images/mle/sdss-data/CD-sersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-RSV/301/2703/4"
	endif

	undefined_data = [0]
	undefine, undefined_data
	print, "DEBUG - root director = ", root
	lp_list = file_search(root, "lP-*.fit")
	n = n_elements(lp_list)
	print, "DEBUG - number of lP-files found = ", n

	for i = 0, n - 1L, 1 do begin

		lp_file_address = lp_list[i]
		address_array = STRSPLIT(lp_file_address,'/',/EXTRACT)
		lp_file = address_array[-1]

		lq_file_address = repstr(lp_file_address, "lP-", "lQ-")
		address_array = STRSPLIT(lq_file_address,'/',/EXTRACT)
		lq_file = address_array[-1]

		lp_exists = file_test(lp_file_address)
		lq_exists = file_test(lq_file_address)
		if (lq_exists and ~keyword_set(force)) then begin
			print, "WARNING - skipping because lQ file exists = ", lq_file
		endif else if (~lp_exists) then begin
			print, "WARNING - skipping because lP file does not exist = ", lp_file
		endif else begin
			print, "DEBUG - reading lP-file = ", lp_file
			header = headfits(lp_file_address, exten = 0)
			x = mrdfits(lp_file_address, 1, /silent)
			print, "DEBUG - writing lQ-file = ", lq_file
			WRITEFITS, lq_file_address, undefine_data, header
			mwrfits, x, lq_file_address
			lp_zip = "gzip -f " + lp_file_address
			lq_zip = "gzip -f " + lq_file_address
			print, "DEBUG - zipping lP and lQ file"
			spawn, lp_zip
			spawn, lq_zip
		endelse

	endfor

end

pro convert_lp_lq_all, force = force

	root = "/u/khosrow/thesis/opt/soho/multi-object/images/mle/sdss-data/CD-sersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-RSV/301/2703/"
	root = "/u/khosrow/thesis/opt/soho/multi-object/images/mle/sdss-data/CD-sersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-PGD-c1-eta-delta-2-secondary/301/2703/"
	root = "/u/khosrow/thesis/opt/soho/multi-object/images/mle/sdss-data/CD-sersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-HLM/301/2703/"
	root = "/u/khosrow/thesis/opt/soho/multi-object/images/mle/sdss-data/CD-sersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-RSV8/301/2703/"
	root = "/u/khosrow/thesis/opt/soho/multi-object/images/mle/sdss-data/CD-sersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-HLM10/301/2703/"
	root = "/u/khosrow/thesis/opt/soho/multi-object/images/mle/sdss-data/CD-sersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-RSV6/301/2703/"
	
	root_prefix_list = $
	["/u/khosrow/thesis/opt/soho/multi-object/images/mle/sdss-data/CD-sersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-RSV6/301/", $	
	"/u/khosrow/thesis/opt/soho/multi-object/images/mle/sdss-data/CD-sersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-RSV7/301/", $
	"/u/khosrow/thesis/opt/soho/multi-object/images/mle/sdss-data/CD-sersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-RSV8/301/", $
	"/u/khosrow/thesis/opt/soho/multi-object/images/mle/sdss-data/CD-sersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-HLM10/301/"]	

	run_list = [2700] ;; [2703, 2700, 2708, 4797]
	subfolder_all = [1, 2, 3, 4, 5, 6]
	for i_run = 0, n_elements(run_list) - 1L, 1L do begin
		run = strtrim(string(run_list[i_run]), 2)
		print, "DEBUG - working with run = ", run
		for i_root = 0, n_elements(root_prefix_list) - 1L, 1L do begin
			root = root_prefix_list[i_root] + "/" + run + "/"
			n = n_elements(subfolder_all)
			for i = 0, n - 1L, 1 do begin
				subfolder = strtrim(string(subfolder_all[i]), 2)
				folder = root + subfolder
				convert_lp_lq, root = folder, force = keyword_set(force)
			endfor
		endfor
	endfor
end
