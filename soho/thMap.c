#include "thMap.h"
#include "thMSky.h"

static int check_string_in_string_list(char *string, char **list, int n);

static RET_CODE get_parfit_rnames_for_model(char *mname, int fitflag, char **rnames, int *nrecord, MFIT_RNAME_LOC *loc);

RET_CODE thMAddObjcAllModels(MAPMACHINE *map, THOBJC *objc, int band, MFLAG flag) {
char *name = "thMAddObjcAllModels";
if (map == NULL) {
	thError("%s: ERROR - null (map)", name);
	return(SH_GENERIC_ERROR);
}
if (objc == NULL) {
	thError("%s: ERROR - null (objc)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
OBJC_ELEM *oe;
status = thObjcElemGetByType(objc->thobjctype, &oe);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not derive (objc_elem) for (objc: %ld, type: %ld) from object bank", name, (long int) objc->thid, (long int) objc->thobjctype);
	return(status);
}

CHAIN *ms;
ms = oe->ms;
int n;
if (ms == NULL || (n = shChainSize(ms)) ==  0) {
	thError("%s: ERROR - no specific model seems to be upload to (objc_elem) for (objc: %d)", name, objc->thid);
	return(SH_GENERIC_ERROR);
}
MODEL_ELEM *me;
int i;
char *mname; //n = MIN(2, n);
for (i = 0; i < n; i++) {
	me = shChainElementGetByPos(ms, i);
	if (me == NULL) {
		thError("%s: ERROR - null (model_elem) detected at location (%d) in definition of (objc: %d) of type '%s'", name, i, objc->thid, oe->classname);
		return(SH_GENERIC_ERROR);
	} 
	mname = me->mname;
	status = thMAddObjc(map, objc, &mname, 1, band, flag);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add model (%d) for (objc: %d) to (mapmachine)", name, i, objc->thid);
		return(status);
	}
}

return(SH_SUCCESS);
}

/* map construction */
RET_CODE thMAddObjc(MAPMACHINE *map, THOBJC *objc, char **models, int nmodel, int band, MFLAG flag) {
char *name = "thMAddObjc";
if (map == NULL && objc == NULL && models == NULL) {
	thError("%s: WARNING - null input, performing null", name);
	return(SH_SUCCESS);
}
if (map == NULL || objc == NULL) {
	thError("%s: ERROR - improperly allocated (mapmachine) or (thobjc)", name);
	return(SH_GENERIC_ERROR);
}
if (models != NULL && nmodel <= 0) {
	thError("%s: ERROR - non-null list of models with length (%d)", name, nmodel);
	return(SH_GENERIC_ERROR);
}
if (band == UNKNOWN_BAND) band = map->band;
/* cannot use single amplitude for multiple models - should think of it for the future */

if (flag == COALESCE && nmodel > 1) {
	thError("%s: ERROR - single amplitude for multiple models is not supported", name);
	return(SH_GENERIC_ERROR);
}

/* check if the models exist in the object */

int i;
int err = 0;
char *mname;

for (i = 0; i < nmodel; i++) {
	mname = models[i];
	if (mname == NULL) {
		thError("%s: ERROR - null model detected at (%d) in the model list", name);
		err++;
	} else if (!thObjcHasModel(objc, mname)) {
		thError("%s: ERROR - model %d in the list (%s) is not part of the object standard definition", 
		name, i, mname);
		err++;
	}
}
if (err) {
	thError("%s: ERROR - %d problematic models were detected in the model list", name, err);
	return(SH_GENERIC_ERROR);
}


/* loop over models */

if (map->alist == NULL) map->alist = shChainNew("MREC");
CHAIN *alist;
alist = map->alist;
MREC *mrec, *mrec2;

int k;
for (k = 0; k < nmodel; k++) {

	mrec = thMrecNew(objc, models[k], NULL, band);

	int i = 0;
	for (i = 0; i < shChainSize(alist); i++) {
		mrec2 = shChainElementGetByPos(alist, i);
		if (!thMrecCmp(mrec, mrec2)) {
			thMrecDel(mrec);
			thError("%s: WARNING - object %d already in the list", name, mrec->objcid);
			return(SH_SUCCESS);
		}
	}
	shChainElementAddByPos(alist, mrec, "MREC", TAIL, AFTER);

}
thMapmachinePutCflag(map, ALTERED);  
return(SH_SUCCESS);
}


RET_CODE thMDeclaimObjc(MAPMACHINE *map, THOBJC *objc, char **models, int nmodel) {
char *name = "thMdeClaimObjc";
if (map == NULL && objc == NULL && models == NULL) {
	thError("%s: WARNING - null input, performing null", name);
	return(SH_SUCCESS);
}
if (map == NULL || objc == NULL) {
	thError("%s: ERROR - improperly allocated (mapmachine) or (thobjc)", name);
	return(SH_GENERIC_ERROR);
}
if (models != NULL && nmodel <= 0) {
	thError("%s: ERROR - non-null list of models with length (%d)", name, nmodel);
	return(SH_GENERIC_ERROR);
}

int i, j, i2, j2, k;

MREC *mrec, *mrec2;

CHAIN *alist;
alist = map->alist;

CHAIN *plist, *clist;
plist = map->plist;

for (k = 0; k < nmodel; k++) {

	if (models[k] == NULL) {
		thError("%s: WARNING - null model name detected in the list (%d), declaiming all models for (objc: %d)",
			name, k, mrec->objcid);
	}
	mrec = thMrecNew(objc, models[k], NULL, UNKNOWN_BAND);

	/* declaim from a-list */
	if (alist != NULL) {
		i2 = 0;
		for (i = 0; i < shChainSize(alist); i++) {
			mrec2 = shChainElementGetByPos(alist, i2);
			if (!thMrecCmp(mrec, mrec2)) {
				shChainElementRemByPos(alist, i2);
				thMrecDel(mrec2);
				thMapmachinePutCflag(map, ALTERED);  
			} else {
			i2++;
			}
		}
	}

	/* declaim from p-list */
	if (plist != NULL) {
		i2 = 0;
		for (i = 0; i < shChainSize(plist); i++) {
			clist = shChainElementGetByPos(plist, i2);
			if (clist != NULL) {
				j2 = 0;
				for (j = 0; j < shChainSize(clist); j++) {
					mrec2 = shChainElementGetByPos(clist, j2);
					if (!thMrecCmp(mrec, mrec2)) {
						shChainElementRemByPos(clist, j2);
						thMrecDel(mrec2);
						thMapmachinePutCflag(map, ALTERED);  
					} else {
					j2++;
					}	
				}
			}
			if (clist == NULL || shChainSize(clist) == 0) {
				shChainElementRemByPos(plist, i2);
				if (clist != NULL) {
					shChainDel(clist);
					thMapmachinePutCflag(map, ALTERED);  
				}	
			} else {
			i2++;
			}
		}
	}		

}


return(SH_SUCCESS);
}

RET_CODE thMClaimObjcVar(MAPMACHINE *map, THOBJC *objc, char *mname, char **rlist, int nrecord) {
char *name = "thMClaimObjcVar";
if (map == NULL && objc == NULL && mname == NULL && rlist == NULL) {
	thError("%s: WARNING - null input, performing null", name);
	return(SH_SUCCESS);
}
if (map == NULL || objc == NULL || (mname == NULL && rlist != NULL)) {
	thError("%s: ERROR - improperly allocated (mapmachine), (objc), (mname, rlist)", name);
	return(SH_GENERIC_ERROR);
}
if (rlist != NULL && nrecord <= 0) {
	thError("%s: ERROR - non-null list of records with length (%d)", name, nrecord);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
int i, j, k;
MREC *mrec, *mrec2;
mrec = thMrecNew(objc, mname, NULL, UNKNOWN_BAND);
if (mrec == NULL) {
	thError("%s: ERROR- could not create a new 'MREC' for (mname = '%s')", 
	name, mname);
	return(SH_GENERIC_ERROR);
}

/* claim into a-list */
if (map->alist == NULL) map->alist = shChainNew("MREC");
CHAIN *alist;
alist = map->alist;

int found = 0;
for (i = 0; i < shChainSize(alist); i++) {
	mrec2 = shChainElementGetByPos(alist, i);
	if (!thMrecCmp(mrec, mrec2)) found = 1;
	if (found) break;
}
if (!found) {
	shChainElementAddByPos(alist, mrec, "MREC", TAIL, AFTER);
	thMapmachinePutCflag(map, ALTERED);  
} else {
	thMrecDel(mrec);
}

/* claim into p-list */

CHAIN *plist, *clist;
if (map->plist == NULL) map->plist = shChainNew("void");
plist = map->plist;

found = 0;
for (k = 0; k <nrecord; k++) {

	char *rname = rlist[k];
	int exists_in_model_par = 0, exists_in_deriv_list = 0;
	status = thModelTestForRecordName(mname, rname, &exists_in_model_par, &exists_in_deriv_list);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not check model '%s' parameter for existence of record '%s'", name, mname, rname);
		return(status);
	}
	if (exists_in_model_par) {

		if (!exists_in_deriv_list) {
			thError("%s: WARNING - record '%s' is available in the parameter for model '%s' but the derivative function is not supplied", name, rname, mname);
		}
		mrec = thMrecNew(objc, mname, rname, UNKNOWN_BAND);
		if (mrec == NULL) {
			thError("%s: ERROR - could not create an (MREC) for (mname = %s, rname = %s, k = %d)", name, mname, rlist[k], k);
			return(SH_GENERIC_ERROR);
		}
		i = 0;
		for (i = 0; i < shChainSize(plist); i++) {
			clist = shChainElementGetByPos(plist, i);
			if (clist != NULL) {
				j = 0;
				for (j = 0; j < shChainSize(clist); j++) {
					mrec2 = shChainElementGetByPos(clist, j);
					if (!thMrecCmp(mrec, mrec2)) found = 1;
					if (found) break;
				}
			}
			if (clist != NULL && found) break;
		}

		if (!found) {
			clist = shChainNew("MREC");
			shChainElementAddByPos(clist, mrec, "MREC", TAIL, AFTER);
			shChainElementAddByPos(plist, clist, "void", TAIL, AFTER);
			thMapmachinePutCflag(map, ALTERED);  
		} else {
			thError("%s: WARNING - record already claimed (objc %d, mname = %s, rname = %s, k = %d)", 
				name, mrec->objcid, mname, rname, k);
			thMrecDel(mrec);
		}
	} else {		
		thError("%s: WARNING - record '%s' does not exist in parameter for model '%s'", name, rname, mname);
	}
}

return(SH_SUCCESS);
}



RET_CODE thMDeclaimObjcVar(MAPMACHINE *map, THOBJC *objc, char *mname, char **rlist, int nrecord) {
char *name = "thMDeclaimObjcVar";
if (map == NULL && objc == NULL && mname == NULL && rlist == NULL) {
	thError("%s: WARNING - null input, performing null", name);
	return(SH_SUCCESS);
}
if (map == NULL || objc == NULL || (mname == NULL && rlist != NULL)) {
	thError("%s: ERROR - improperly allocated (mapmachine), (objc), (mname, rlist)", name);
	return(SH_GENERIC_ERROR);
}
if (rlist != NULL && nrecord <= 0) {
	thError("%s: ERROR - non-null list of records with length (%d)", name, nrecord);
	return(SH_GENERIC_ERROR);
}

/* declaim from plist only */

int i, j, k, i2, j2;
CHAIN *plist, *clist;
plist = map->plist;


if (plist != NULL) {
	MREC *mrec, *mrec2;

	for (k = 0; k < nrecord; k++) {
		mrec = thMrecNew(objc, mname, rlist[k], UNKNOWN_BAND);
		i2 = 0;
		for (i = 0; i < shChainSize(plist); i++) {
			clist = shChainElementGetByPos(plist, i2);
			if (clist != NULL) {
				j2 = 0;
				for (j = 0; j < shChainSize(clist); j++) {
					mrec2 = shChainElementGetByPos(clist, j2);
					if (!thMrecCmp(mrec, mrec2)) {
						shChainElementRemByPos(clist, j2);
						thMrecDel(mrec2);
						thMapmachinePutCflag(map, ALTERED);  
					} else {
					j2++;
					}	
				}
			}
			if (clist == NULL || shChainSize(clist) == 0) {
				shChainElementRemByPos(plist, i2);
				if (clist != NULL) {
					shChainDel(clist);
					thMapmachinePutCflag(map, ALTERED);  
				}
			} else {
			i2++;
			}
		}
	}
}

return(SH_SUCCESS);
}

RET_CODE thMEquivObjcVars(MAPMACHINE *map, 
			THOBJC *objc1, char *mname1, char **rlist1,
			THOBJC *objc2,  char *mname2, char **rlist2,
			int nrecord) {
char *name = "thMEquivObjcVars";
if (map == NULL && objc1 == NULL && mname1 == NULL && rlist1 == NULL 
	&& objc2 == NULL && mname2 == NULL && rlist2 == NULL) {
	thError("%s: WARNING - null input, performing null", name);
	return(SH_SUCCESS);
	}
if (map == NULL || (objc1 == NULL && objc2 == NULL) || (mname1 == NULL && mname2 == NULL)  || (rlist1 == NULL && rlist2 == NULL)) {
	thError("%s: ERROR - improperly allocated (mapmachine), (objc), (mname), or (rlist)", name); 
	return(SH_GENERIC_ERROR);
}

if (objc1 == NULL) objc1 = objc2;
if (objc2 == NULL) objc2 = objc1;
if (mname1 == NULL) mname1 = mname2;
if (mname2 == NULL) mname2 = mname1;
if (rlist1 == NULL) rlist1 = rlist2;
if (rlist2 == NULL) rlist2 = rlist1;

if (objc1 == objc2 && mname1 == mname2 && rlist1 == rlist2) {
	thError("%s: WARNING - equating records with themselves, null performance", name);
	return(SH_SUCCESS);
}

if (nrecord <= 0) {
	thError("%s: ERROR - non-null record list with length (%d)", name, nrecord);
	return(SH_GENERIC_ERROR);
}

MREC *mrec, *mrec1, *mrec2;
CHAIN *plist, *clist, *clist1, *clist2;
plist = map->plist;

if (plist == NULL || shChainSize(plist) == 0) {
	thError("%s: ERROR - empty plist in (mapmachine) - cannot look for any records", name);
	return(SH_GENERIC_ERROR);
	}

/* locate record1 and record2 separately */
	
/* see if either of them is in an equiv chain of length 1 */

/* if yes, then join */

/* if not the give an error message */

int i, j, k, i1, j1, i2, j2, err = 0;
for (k = 0; k < nrecord; k++) {
	if (rlist1[k] != NULL && rlist2[k] != NULL && 
		(strlen(rlist1[k]) != 0) && (strlen(rlist2[k]) != 0)) {

		mrec1 = thMrecNew(objc1, mname1, rlist1[k], UNKNOWN_BAND);
		mrec2 = thMrecNew(objc2, mname2, rlist2[k], UNKNOWN_BAND);
		clist1 = NULL; clist2 = NULL;
		i1 = -1; j1 = -1; i2 = -1; j2 = -1;
		i = 0;
		/* finding the respective records in the plist */
		for (i = 0; i < shChainSize(plist); i++) {
			clist = shChainElementGetByPos(plist, i);
			if (clist != NULL) {
				j = 0;
				for (j = 0; j < shChainSize(clist); j++) {	
					mrec = shChainElementGetByPos(clist, j);
					if (!thMrecCmp(mrec, mrec1)) {
						clist1 = clist;
						j1 = j;
						i1 = i;
					}
					if (!thMrecCmp(mrec, mrec2)) {
						clist2 = clist;
						j2 = j;
						i2 = i;
					}
					if (clist1 != NULL && clist2 != NULL) break;
				}
			}
			if (clist1 != NULL && clist2 != NULL) break;
		}
		
		if (clist1 != clist2 && clist1 != NULL && clist2 != NULL) {

			if (shChainSize(clist1) != 1 && shChainSize(clist2) != 1) {
				thError("%s: ERROR - both object models have extesive equivalence classes already - (objc: %d, model: %s, record: %s) (objc: %d, model: %s, record: %s)", 
				name, mrec1->objcid, mrec1->mname, mrec1->rname, 
				mrec2->objcid, mrec2->mname, mrec2->rname);
				err++;
			} else if (shChainSize(clist1) == 1) {
				mrec = shChainElementGetByPos(clist1, 0);
				shChainElementRemByPos(plist, i1);
				shChainDel(clist1);
				shChainElementAddByPos(clist2, mrec, "MREC", TAIL, AFTER);
				thMapmachinePutCflag(map, ALTERED);  
			} else if (shChainSize(clist2) == 1) {
				mrec = shChainElementGetByPos(clist2, 0);
				shChainElementRemByPos(plist, i2);
				shChainDel(clist2);
				shChainElementAddByPos(clist1, mrec, "MREC", TAIL, AFTER);
				thMapmachinePutCflag(map, ALTERED);  
			} else {
				thError("%s: ERROR - source code problem", name);
				return(SH_GENERIC_ERROR);
			}

		} else if (clist1 == NULL) {
			thError("%s: ERROR - could not locate (objc1: %d, mname1: %s, rname1: %s)", 
			name, mrec1->objcid, mrec1->mname, mrec1->rname);
			err++;
		} else if (clist2 == NULL) {
			thError("%s: ERROR - could not locate (objc2: %d, mname2: %s, rname2: %s)", 
			name, mrec2->objcid, mrec2->mname, mrec2->rname);
			err++;
		} else if (clist1 == clist2) {
			thError("%s: WARNING - the two records are already in one equivalence class - matching (objc: %d, model: %s, record: %s) (objc: %d, model: %s, record: %s)", 
			name, mrec1->objcid, mrec1->mname, mrec1->rname, mrec2->objcid, mrec2->mname, mrec2->rname);
		} else {
			thError("%s: ERROR - source code error", name);
			return(SH_GENERIC_ERROR);
		}
	
		thMrecDel(mrec1);
		thMrecDel(mrec2);

	} else {

		thError("%s: ERROR - empty record name detected for record %d", name, k);
		err++;

	}
}

if (err) {
	thError("%s: ERROR - total of %d equivalence orders could not be executed", name);
	return(SH_GENERIC_ERROR);
	}

return(SH_SUCCESS);
}

RET_CODE thMEquivObjcVar(MAPMACHINE *map, 
			THOBJC *objc1, char *mname1, char *rname1,
			THOBJC *objc2,  char *mname2, char *rname2) {
char *name = "thMEquateObjcVars";
#if 0
if (map == NULL && objc1 == NULL && mname1 == NULL && rname1 == NULL 
	&& objc2 == NULL && mname2 == NULL && rname2 == NULL) {
	thError("%s: WARNING - null input, performing null", name);
	return(SH_SUCCESS);
	}
if (map == NULL || (objc1 == NULL && objc2 == NULL) || 
	((mname1 == NULL || strlen(mname1) == 0) && (mname2 == NULL || strlen(mname2) == 0))  || 
	((rname1 == NULL || strlen(rname1) == 0) && (rname2 == NULL || strlen(rname2) == 0))) {
	thError("%s: ERROR - improperly allocated (mapmachine), (objc), (mname), or (rlist)", name);
	return(SH_GENERIC_ERROR);
}

if (objc1 == NULL) objc1 = objc2;
if (objc2 == NULL) objc2 = objc1;
if (mname1 == NULL) mname1 = mname2;
if (mname2 == NULL) mname2 = mname1;
if (rname1 == NULL || strlen(rname1) == 0) rname1 = rname2;
if (rname2 == NULL || strlen(rname2) == 0) rname2 = rname1;

if (objc1 == objc2 && mname1 == mname2 && !strcmp(rname1, rname2)) {
	thError("%s: WARNING - equating records with themselves, null performance", name);
	return(SH_SUCCESS);
}
#endif

RET_CODE status;
status = thMEquivObjcVars(map, objc1, mname1, &rname1, objc2, mname2, &rname2, 1);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not create equivalence class", name);
}
return(status);
}

RET_CODE thMSepObjcVars(MAPMACHINE *map, THOBJC *objc, char *mname, char **rnames, int nrecord) {
char *name = "thMSepObjcVars";
if (map == NULL && objc == NULL && mname == NULL && rnames == NULL) {
	thError("%s: WARNING - null input, performing null", name);
	return(SH_SUCCESS);
}
if (map == NULL || objc == NULL) {
	thError("%s: ERROR - improperly allocated (mapmachine) or (thobjc)", name);
	return(SH_GENERIC_ERROR);
}

thError("%s: ERROR - under construction", name);
return(SH_GENERIC_ERROR);
}

RET_CODE thMSepObjcVar(MAPMACHINE *map, THOBJC *objc, char *mname, char *rname) {
char *name = "thMSepObjcVar";
if (map == NULL && objc == NULL && mname == NULL && rname == NULL) {
	thError("%s: WARNING - null input, performing null", name);
	return(SH_SUCCESS);
}
if (map == NULL || objc == NULL 
	|| mname == NULL || strlen(mname) == 0 
	|| rname == NULL || strlen(rname) == 0) {
	thError("%s: ERROR - improperly allocated (mapmachine), (thobjc), (mname), or (rname)", name);
	return(SH_GENERIC_ERROR);
}

/* find the location for the record */

/* check if it is an equiv object already */

/* if it is, remove it from clist */

/* create a new clist containing the record */

/* put the clist at the end of the plist */

CHAIN *plist, *clist, *clist1;
plist = map->plist;
if (plist == NULL || shChainSize(plist) == 0) {
	thError("%s: ERROR - null plist in (mapmachine)", name);
	return(SH_GENERIC_ERROR);
	}


int i, j, i1, j1;

MREC *mrec, *mrec2;
mrec = thMrecNew(objc, mname, rname, UNKNOWN_BAND);

for (i = 0; i < shChainSize(plist); i++) {
	clist = shChainElementGetByPos(plist, i);
	if (clist != NULL) {
		clist1 = NULL; i1 = -1; j1 = -1;
		for (j = 0; j < shChainSize(clist); j++) {
			mrec2 = shChainElementGetByPos(clist, j);	
			if (!thMrecCmp(mrec, mrec2)) {
				clist1 = clist;
				j1 = j; i1 = i;
				break;
			}
		}
	}
	if (clist1 != NULL) break;
}

if (clist1 != NULL) {
	if (shChainSize(clist1) == 1) {
		thError("%s: WARNING - record has no equivalent (objc: %d, mname: %s, rname: %s)",
			name, mrec->objcid, mrec->mname, mrec->rname);
		thMrecDel(mrec);
		return(SH_SUCCESS);
	}
	shChainElementRemByPos(clist1, j1);
	clist = shChainNew("MREC");
	shChainElementAddByPos(clist, mrec2, "MREC", TAIL, AFTER);
	shChainElementAddByPos(plist, clist, "void", TAIL, AFTER);
	thMrecDel(mrec);
 	thMapmachinePutCflag(map, ALTERED);	
	return(SH_SUCCESS);
}

thError("%s: ERROR - could not find the record (objc: %d, mname: %s, rname: %s)",
	name, mrec->objcid, mrec->mname, mrec->rname);
thMrecDel(mrec);
return(SH_GENERIC_ERROR);
}

RET_CODE thMCompile(MAPMACHINE *map) {
char *name = "thMCompile";
if (map == NULL) {
	thError("%s: WARNING - null (mapmachine)", name);
	return(SH_SUCCESS);
}

if (map->alist == NULL) {
	thError("%s: ERROR - no (alist) in (mapmachine)", name);
	return(SH_GENERIC_ERROR);
}
CHAIN *alist, *plist, *clist;
if (map->plist == NULL) map->plist = shChainNew("void");

alist = map->alist;
plist = map->plist;

/* 
   check for empty elements and empty clists  and delete them if they exist

   extract npar, namp, nmpar

   deallocate mname, pname, and ename

   alloctae and fill mname, pname, and ename

   deallocate ploc

   allocate and fill ploc

   nullify *pt

   fill *ps

*/

/* 
   check for empty elements and empty clists  and delete them if they exist
*/

#if DEBUG_MEMORY
printf("%s: memory statistics before compiling map", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#endif



int err = 0;
RET_CODE status;

int i, j, i1, j1;

i1 = 0;
for (i = 0; i < shChainSize(alist); i++) {
	if (shChainElementGetByPos(alist, i1) == NULL) {
		shChainElementRemByPos(alist, i1);
	} else {
		i1++;
	}
}

i1 = 0;
for (i = 0; i < shChainSize(plist); i++) {
	clist = shChainElementGetByPos(plist, i1);
	if (clist != NULL) {
		j1 = 0;
		for (j = 0; j < shChainSize(clist); j++) {
			if (shChainElementGetByPos(clist, j1) == NULL) {
				shChainElementRemByPos(clist, j1);
			} else {
				j1++;
			}
		}
	}
	if (clist == NULL || shChainSize(clist) == 0) {
		shChainElementRemByPos(plist, i1);
		if (clist != NULL) shChainDel(clist);
	} else {
		i1++;
	}
}

/* constructing mnames[namp] and nmpar[namp] accnmpar[namp][npar] */

int npar, nmodel, nacc, *nmpar, **accnmpar;
nmodel = shChainSize(alist);
npar = shChainSize(plist);

if (nmodel == 0) {
	thError("%s: WARNING - map doesn't contain any models", name);
	return(SH_GENERIC_ERROR);
}

nmpar = (int *) thCalloc(nmodel, sizeof(int));
accnmpar = (int **) thCalloc(nmodel, sizeof(int*));
for (i = 0; i < nmodel; i++) {
	accnmpar[i] = (int *) thCalloc(npar + 1, sizeof(int));
	int *accnmpari;
	accnmpari = accnmpar[i];
	for (j = 0; j < npar + 1; j++) {
		accnmpari[j] = BAD_INDEX;  /* value (-1) is for models which do not have derivative with respect to a certain parameter */
	}
}

if (map->mnames != NULL) thFree(map->mnames);
map->mnames = (char **) thCalloc(nmodel, sizeof(char *));
if (map->bands != NULL) thFree(map->bands);
map->bands = (int *) thCalloc(nmodel, sizeof(int));

int k, base = -1;
MREC *mrec, *mrec2;
for (i = 0; i < nmodel; i++) {
	base++;
	accnmpar[i][0] = base;
	mrec = shChainElementGetByPos(alist, i);
	map->mnames[i] = mrec->mname;
	map->bands[i] = mrec->band;
	for (k = 0; k < npar; k++) {
		clist = shChainElementGetByPos(plist, k);
		for (j = 0; j < shChainSize(clist); j++) {
			mrec2 = shChainElementGetByPos(clist, j);
			if (!thMrecCmp(mrec, mrec2)) {
				base++;
				nmpar[i]++;
				accnmpar[i][k + 1] = base;
			} 
		}
	}
}

nacc = base + 1;

int *DDnmpar = NULL;
int npp = 0;
int ***DDaccnmpar = NULL;
#if MLE_CONDUCT_HIGHER_ORDER
DDnmpar = thCalloc(nmodel, sizeof(int));
for (i = 0; i < nmodel; i++) {
	DDnmpar[i] = (nmpar[i] * (nmpar[i] + 1)) / 2;
	npp += DDnmpar[i];
}
DDaccnmpar = thCalloc(nmodel, sizeof(int **));
int DDbase = 0;
for (i = 0; i < nmodel; i++) {
	int nmpar_i = nmpar[i];
	if (nmpar_i > 0) {
		DDaccnmpar[i] = thCalloc(nmpar_i, sizeof(int *));
		int **DDaccnmpar_i = DDaccnmpar[i];
		DDaccnmpar_i[0] = thCalloc(nmpar_i * nmpar_i, sizeof(int));
		for (j = 0; j < nmpar_i; j++) {
			DDaccnmpar_i[j] = DDaccnmpar_i[0] + j * nmpar_i;
		}
		for (j = 0; j < nmpar_i; j++) {
			for (k = 0; k <= j; k++) {
				DDaccnmpar_i[j][k] = DDbase;
				if (j != k) DDaccnmpar_i[k][j] = DDbase;
				DDbase++;	
			}	
		}		
	}
}

shAssert(npp == DDbase);	
#endif

/* constructing enames and ploc */

if (map->enames != NULL) {
	for (i = 0; i < map->namp; i++) {
		if (map->enames[i] != NULL) thFree(map->enames[i]);
	}
	thFree(map->enames);
	map->enames = NULL;
}

if (map->ploc != NULL) {
	for (i = 0; i < map->namp; i++) {
		if (map->ploc[i] != NULL) thFree(map->ploc[i]);
	}
	thFree(map->ploc);
	map->ploc = NULL;
}

if (map->invploc != NULL) {
	for (i = 0; i < map->npar; i++) if (map->invploc[i] != NULL) thFree(map->invploc[i]);
	thFree(map->invploc);
	map->invploc = NULL;
}

#if MAP_HAS_SCHEMA_INFO

if (map->rs != NULL) {
	for (i = 0; i < map->namp; i++) {
		if (map->rs[i] != NULL) thFree(map->rs[i]);
	}
	thFree(map->rs);
	map->rs = NULL;
}

if (map->rtypes != NULL) {
	for (i = 0; i < map->namp; i++) {
		if (map->rtypes[i] != NULL) thFree(map->rtypes[i]);
	}
	thFree(map->rtypes);
	map->rtypes = NULL;
}	

if (map->etypes != NULL) {
	for (i = 0; i < map->namp; i++) {
		if (map->etypes[i] != NULL) thFree(map->etypes[i]);
	}
	thFree(map->etypes);
	map->etypes = NULL;
}

#endif

/* object mapping of record names */

if (map->nobjcpar != NULL) {
	thFree(map->nobjcpar);
	map->nobjcpar = NULL;
}
if (map->robjcnames != NULL) {
	for (i = 0; i < map->npobjc; i++) {
		if (map->robjcnames[i] != NULL) {
			for (j = 0; j < map->npar; j++) {
				if (map->robjcnames[i][j] != NULL) thFree(map->robjcnames[i][j]);
			}
		}
	}
	if (map->robjcnames[0] != NULL) thFree(map->robjcnames[0]);
	thFree(map->robjcnames);
	map->robjcnames = NULL;
}

if (map->pobjcloc != NULL) {
	if (map->pobjcloc[0] != NULL) thFree(map->pobjcloc[0]);
	thFree(map->pobjcloc);
	map->pobjcloc = NULL;			
}

if (map->invpobjcloc != NULL) {
	if (map->invpobjcloc[0] != NULL) {
		if (map->invpobjcloc[0][0] != NULL) thFree(map->invpobjcloc[0][0]);
		thFree(map->invpobjcloc[0]);
	}
	thFree(map->invpobjcloc);
	map->invpobjcloc = NULL;
}


map->nacc = nacc;
if (map->nmpar != NULL) thFree(map->nmpar);
map->nmpar = nmpar;
if (map->accnmpar != NULL) {
	for (i = 0; i < map->namp; i++) {
		if (map->accnmpar[i] != NULL) thFree(map->accnmpar[i]);
	}
	thFree(map->accnmpar);
}
map->accnmpar = accnmpar;

if (map->DDnmpar != NULL) thFree(map->DDnmpar);
map->DDnmpar = DDnmpar;
map->npp = npp;
if (map->DDaccnmpar != NULL) {
	for (i = 0; i < map->namp; i++) {
		int **DDaccnmpar_i = map->DDaccnmpar[i];
		if (DDaccnmpar_i != NULL) {
			if (DDaccnmpar_i[0] != NULL) thFree(DDaccnmpar_i[0]);
			thFree(DDaccnmpar_i);
		}
	}
	thFree(map->DDaccnmpar);
}
map->DDaccnmpar = DDaccnmpar;

map->enames = (char ***) thCalloc(nmodel, sizeof(char **));
map->ploc = (int **) thCalloc(nmodel, sizeof(int *));
if (npar > 0) map->invploc= (int **) thCalloc(npar, sizeof(int *));
for (j = 0; j < npar; j++) {
	map->invploc[j] = thCalloc(nmodel, sizeof(int));
}	


#if MAP_HAS_SCHEMA_INFO
map->rs = (void ***) thCalloc(nmodel, sizeof(void **));
map->rtypes = (TYPE **) thCalloc(nmodel, sizeof(TYPE *));
map->etypes = (char ***) thCalloc(nmodel, sizeof(char **));
#endif

int l;
char **ename;
int *pploc, **invploc;
#if MAP_HAS_SCHEMA_INFO
void **r;
TYPE *rtype;
char **etype;
#endif

invploc = map->invploc;

for (i = 0; i < nmodel; i++) {
	mrec = shChainElementGetByPos(alist, i);
	/* allocating space */
	map->enames[i] = (char **) thCalloc(nmpar[i], sizeof(char *));
	map->ploc[i] = (int *) thCalloc(nmpar[i], sizeof(int));
	#if MAP_HAS_SCHEMA_INFO
	map->rs[i] = (void **) thCalloc(nmpar[i], sizeof(void *));
	map->rtypes[i] = (TYPE *) thCalloc(nmpar[i], sizeof(TYPE));
	map->etypes[i] = (char **) thCalloc(nmpar[i], sizeof(char *));
	#endif
	/* extracting row */
	ename = map->enames[i];
	pploc = map->ploc[i];
	#if MAP_HAS_SCHEMA_INFO
	r = map->rs[i];
	rtype = map->rtypes[i];
	etype = map->etypes[i];
	#endif
	l = 0;
	for (j = 0; j < npar; j++) {
		invploc[j][i] = BAD_INDEX; /* initiation */
		clist = shChainElementGetByPos(plist, j);
		for (k = 0; k < shChainSize(clist); k++) {
			mrec2 = shChainElementGetByPos(clist, k);
			if (!thMrecCmp(mrec, mrec2)) {
				ename[l] = mrec2->rname;				
				pploc[l] = j;
				invploc[j][i] = l; /* used to be a boolean but now it returns bad index instead */
				#if MAP_HAS_SCHEMA_INFO
				status = thMrecGetPtypePs(mrec2, rtype + l, etype + l, r + l);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - %s (objc: %d, model: %s, record: %s)", 
					name, "could not extract the memory location or type for",
					mrec2->objcid, mrec2->mname, mrec2->rname);
					err++;
				} 
				#endif 
				l++;
			} 
		}
	}
}
/* getting pnames, and source parameters */

if (map->pnames != NULL) thFree(map->pnames);
map->pnames = (char **) thCalloc(nmodel, sizeof(char *));

if (map->ps != NULL) thFree(map->ps);
map->ps = (void **) thCalloc(nmodel, sizeof(void *));

if (map->objcs != NULL) thFree(map->objcs);
map->objcs = (void **) thCalloc(nmodel, sizeof(void *));

for (i = 0; i < nmodel; i++) {
	mrec = shChainElementGetByPos(alist, i);
	status = thMrecGetObjc(mrec, (THOBJC **) (map->objcs + i));
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract source (objc) from (mrec) for model (%d)", name, i);
		err++;
	}
	status = thMrecGetPname(mrec, map->pnames + i);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract the parameter type name (pname) for model (%d)", name, i);
		err++;
	}
	#if MAP_HAS_SCHEMA_INFO
	status = thMrecGetPtypePs(mrec, NULL, NULL, map->ps + i); /* note: ptype is not extracted */
	#else
	status = thMrecGetPtypePs(mrec, NULL, map->ps + i); /* note: ptype is not extracted */
 	#endif	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get the value of parameter for model (%d)", name, i);
		err++;
	}
}


map->pt = NULL;

/* now find out how many unique objects are in objcs and put them in an array uobjcs */

int *wflag;
wflag = thCalloc(nmodel, sizeof(int));

void  **objcs;
objcs = map->objcs;
for (i = 0; i < nmodel; i++) wflag[i] = 1;
for (i = 0; i < nmodel; i++) {
	if (wflag[i] == 1) {
		for (j = i + 1; j < nmodel; j++) {
		if (((void *) objcs[j]) == ((void *) objcs[i])) wflag[j] = 0;
		}
	}
}
int nobjc = 0;
for (i = 0; i < nmodel; i++) {
	if (wflag[i]) {	
		nobjc++;
		wflag[i] = nobjc;
	}
}

if (map->uobjcs != NULL) {
	thFree(map->uobjcs);
}
void **uobjcs;
if (nobjc != 0) {
	map->uobjcs = thCalloc(nobjc, sizeof(THOBJC *));
	uobjcs = map->uobjcs;
	objcs = map->objcs;
	for (i = 0; i < nmodel; i++) {
		if ((j = wflag[i]) != 0) {
			uobjcs[j-1] = objcs[i];
		}
	}
}

if (map->upobjcs != NULL) thFree(map->upobjcs);
map->upobjcs = thCalloc(nobjc, sizeof(void *));
if (map->uaobjcs != NULL) thFree(map->uaobjcs);
map->uaobjcs = thCalloc(nobjc, sizeof(void *));
void **upobjcs = map->upobjcs;
void **uaobjcs = map->uaobjcs;
int pi = 0, ai = 0;
for (i = 0; i < nobjc; i++) {
	void *myobjc = uobjcs[i];
	int objcfound = 0;
	for (j = 0; j < shChainSize(map->plist); j++) {
		CHAIN *clist = shChainElementGetByPos(plist, j);
		for (k = 0; k < shChainSize(clist); k++) {
			MREC *mrec = shChainElementGetByPos(clist, k);
			if (mrec != NULL && mrec->objc == myobjc) {
				objcfound = 1;
				break;
			}
		}
		if (objcfound) break;
	}
	if (objcfound) {
		upobjcs[pi] = myobjc;
		pi++;
	} else {
		uaobjcs[ai] = myobjc;
		ai++;
	}
}

map->npobjc = pi;
map->naobjc = ai;


thFree(wflag);

/* create the objects record names combined with model, such as re_deV
 * and their mapping rule onto fit parameter
 */
int npobjc = map->npobjc;
if (npobjc > 0) {
	map->nobjcpar = thCalloc(npobjc, sizeof(int));

	map->robjcnames = thCalloc(npobjc, sizeof(char **));
	int nallpar = shChainSize(map->plist);
	if (nallpar > 0) {
		map->robjcnames[0] = thCalloc(npobjc * nallpar, sizeof(char *));
		for (j = 0; j < npobjc; j++) {
			map->robjcnames[j] = map->robjcnames[0] + j * nallpar;
		}
	}

	map->pobjcloc = thCalloc(npobjc, sizeof(int *));
	if (nallpar > 0) {	
		map->pobjcloc[0] = thCalloc(npobjc * nallpar, sizeof(int));
		for (j = 0; j < npobjc; j++) {
			map->pobjcloc[j] = map->pobjcloc[0] + j * nallpar;
		}
	}
	map->invpobjcloc = thCalloc(npobjc, sizeof(int **));
	if (nallpar > 0) {
		map->invpobjcloc[0] = thCalloc(npobjc * nallpar, sizeof(int *));
		map->invpobjcloc[0][0] = thCalloc(npobjc * nallpar * nallpar, sizeof(int *));
		for (j = 0; j < npobjc; j++) {
			map->invpobjcloc[j] = map->invpobjcloc[0] + j * nallpar;
			map->invpobjcloc[j][0] = map->invpobjcloc[0][0] + j * nallpar * nallpar;   
			int **invpobjcloc = map->invpobjcloc[j];
			for (k = 0; k < nallpar; k++) {
				invpobjcloc[k] = invpobjcloc[0] + k * nallpar;
			}
		}

	}
}

for (i = 0; i < npobjc; i++) {
	void *myobjc = upobjcs[i];
	char **robjcnames = map->robjcnames[i];
	int *pobjcloc = map->pobjcloc[i];
	int **invpobjcloc = map->invpobjcloc[i];
	int nobjcpar = 0;
	for (j = 0; j < shChainSize(map->plist); j++) {
		CHAIN *clist = shChainElementGetByPos(plist, j);
		for (k = 0; k < shChainSize(clist); k++) {
			MREC *mrec = shChainElementGetByPos(clist, k);
			if (mrec != NULL && mrec->objc == myobjc) {
				char *mname = mrec->mname;
				char *rname = mrec->rname;
				char *robjcname = robjcnames[nobjcpar];
				if (robjcname == NULL) {
					robjcname = thCalloc(MX_STRING_LEN, sizeof(char));
					robjcnames[nobjcpar] = robjcname;
				}
				sprintf(robjcname, "%s_%s", rname, mname);
				pobjcloc[nobjcpar] = j;
				invpobjcloc[j][nobjcpar] = 1;
				nobjcpar++;
			}
		}
	}
	map->nobjcpar[i] = nobjcpar;
}


	
/* now that things are fixed, settle value of nmodel, npar and nobjc */

thMapmachinePutNmodel(map, nmodel);
thMapmachinePutNpar(map, npar);
thMapmachinePutNobjc(map, nobjc);

/* now set flag as COMPILED */

#if DEBUG_MEMORY
printf("%s: memory statistics after compiling map", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#endif

if (!err) {
	thMapmachinePutCflag(map, COMPILED);
	return(SH_SUCCESS);
} else {
	if (err > 1) thError("%s: ERROR - (%d) error messages generated while trying to compile (mapmachine)", name, err);
	return(SH_GENERIC_ERROR);
}

}

RET_CODE thMapPrint(MAPMACHINE *map) {
char *name = "thMapPrint";
if (map == NULL) {
	thError("%s: WARNING - null map", name);
	return(SH_SUCCESS);
}

printf("%s: -------------------------- \n", name);

int npar = map->npar;
int nmodel = map->namp;
int nobjc = map->nobjc;
#if 0
int npobjc = map->npobjc;
#endif

int **invploc = map->invploc;
int ** ploc = map->ploc;
int *nmpar = map->nmpar;
char ***enames = map->enames;
char **mnames = map->mnames;
#if MAP_HAS_SCHEMA_INFO
#if 0
void ***rs = map->rs;
TYPE **rtypes = map->rtypes;
char ***etypes = map->etypes;
#endif
#endif
void **objcs = map->objcs;
void **uobjcs = map->uobjcs;


int band = map->band;
MCFLAG cflag = map->cflag;

int i, j, l; 
printf("** map: %s, band = %d, npar = %d, nmodel = %d, nobjc = %d \n", 
	shEnumNameGetFromValue("MCFLAG", cflag), band, npar, nmodel, nobjc);

if (nobjc < THRESHOLD_OBJCLIST_PRINT) {
	printf("** objc list (%d)", nobjc);
	for (i = 0; i < nobjc; i++) {
		printf("%p, ", uobjcs[i]);
	}
	printf("\n");
}


if (nmodel < THRESHOLD_OBJCLIST_PRINT) {
	for (i = 0; i < nmodel; i++) {

	int nmpar_i = nmpar[i];
	void *objc = objcs[i];
	char *mname = mnames[i];
	char **enames_i = enames[i];

	printf("** model (%d), objc (%p), mname ='%s', nmpar(model) = %d \n", i, objc, mname, nmpar_i);
	for (j = 0; j < nmpar_i; j++) {
		printf("%s, ", enames_i[j]);
	}
	if (nmpar_i > 0) printf("\n");
	}

} else {

	for (i = 0; i < nmodel; i++) {

	int nmpar_i = nmpar[i];
	void *objc = objcs[i];
	char *mname = mnames[i];
	char **enames_i = enames[i];

	if (nmpar_i > 0) {
	printf("** model (%d), objc (%p), mname ='%s', nmpar(model) = %d \n", i, objc, mname, nmpar_i);
	for (j = 0; j < nmpar_i; j++) {
		printf("%s, ", enames_i[j]);
	}
	printf("\n");
	}

	}
}
printf("** n_model = %d \n", nmodel);
printf("** ploc matrix ploc[1:n_model][1:n_model_par] \n");
for (i = 0; i < nmodel; i++) {	
	int nmpar_i = nmpar[i];
	if (nmpar_i > 0) {
		printf("[%d]: ", i);
		int *ploc_i = ploc[i];
		for (l = 0; l < nmpar_i; l++) {
			printf("%d ,", ploc_i[l]);
		}
		printf("\n");
	}
}
printf("** n_par = %d, n_model = %d \n", npar, nmodel);
printf("** invploc matrix invploc[1:n_par][1:n_model] \n");
for (j = 0; j < npar; j++) {	
	int *invploc_j = invploc[j];
	printf("[%d]: ", j);
	for (i = 0; i < nmodel; i++) {
		int invploc_j_i = invploc_j[i];
		if (invploc_j_i != BAD_INDEX) printf("%d(%d), ", invploc_j_i, i);
	
	}
	printf("\n");
}

printf("%s: -------------------------- \n", name);
return(SH_SUCCESS);
}

int thMrecCmp(MREC *mrec1, MREC *mrec2) {
char *name = "thMrecCmp";
if (mrec1 == NULL && mrec2 == NULL) {
	thError("%s: ERROR - fully null input", name);
	return(0);
}
if (mrec1 == NULL || mrec2 == NULL) {
	thError("%s: ERROR - partially null input, cannot compare", name);
	return(0);
}

/* 
	THOBJCID objcid;
	THOBJCTYPE objctype;
	char *mname; 
	char *rname; 
	char *pname;

	void *ps;
	TYPE ptype;
	#if MAP_HAS_SCHEMA_INFO
	char *etype;
	#endif
} MREC; 

*/

if (mrec1->objc != mrec2->objc) return(1);

if (mrec1->objctype == UNKNOWN_OBJC || mrec2->objctype == UNKNOWN_OBJC) return(0);
if (mrec1->objctype != mrec2->objctype) return(2);

if (mrec1->mname == NULL || mrec2->mname == NULL || strlen(mrec1->mname) == 0 || strlen(mrec2->mname) == 0) return(0);
if (strcmp(mrec1->mname, mrec2->mname)) return(3);

if (mrec1->rname == NULL || mrec2->rname == NULL || strlen(mrec1->rname) == 0 || strlen(mrec2->rname) == 0) return(0);
if (strcmp(mrec1->rname, mrec2->rname)) return(4);

if (mrec1->pname == NULL || mrec2->pname == NULL || strlen(mrec1->pname) == 0 || strlen(mrec2->pname) == 0) return(0);
if (strcmp(mrec1->pname, mrec2->pname)) return(5);


if (mrec1->ptype == UNKNOWN_SCHEMA || mrec2->ptype == UNKNOWN_SCHEMA) return(0);
if (mrec1->ptype != mrec2->ptype) return(6);
#if MAP_HAS_SCHEMA_INFO
if (mrec1->etype == NULL || mrec2->etype == NULL || strlen(mrec1->etype) == 0 || strlen(mrec2->etype) == 0) return(0);
if (strcmp(mrec1->etype, mrec2->etype)) return(7);
#endif
return(0);

}

RET_CODE thMAddObjcChainToParFit(MAPMACHINE *map, CHAIN *objclist, int fitflag, char **classnames, int nclass, MCDCOMP component, int *nfound) {
char *name = "thMAddObjcChainToParFit";
if (fitflag == MFIT_NULL) thError("%s: WARNING - null fit flag - only linear fit", name);
shAssert(nclass >= 0);
if (component != HALO_ONLY && component != HALO_AND_CORE) {
	thError("%s: ERROR - usupported (mcdcomp) flag '%s'", name, shEnumNameGetFromValue("MCDCOMP", component));
	return(SH_GENERIC_ERROR);
}

char ***rnames, **iornames;
int nrname = 50, nrmodel = 2;
rnames = thCalloc(nrmodel, sizeof(char **));
rnames[0] = thCalloc(nrname * nrmodel, sizeof(char*));
iornames = thCalloc(nrname, sizeof(char *));
rnames[0][0] = thCalloc(nrmodel * nrname * MX_STRING_LEN, sizeof(char));
iornames[0] = thCalloc(nrname * MX_STRING_LEN, sizeof(char));
int i, i_model;
for (i_model = 0; i_model < nrmodel; i_model++) {
	rnames[i_model] = rnames[0] + i_model * nrname;
	rnames[i_model][0] = rnames[0][0] + i_model * nrname * MX_STRING_LEN;
	for (i = 1; i < nrname; i++) {
		rnames[i_model][i] = rnames[i_model][0] + i * MX_STRING_LEN;
	}
}
for (i = 1; i < nrname; i++) {
	iornames[i] = iornames[0] + i * MX_STRING_LEN; 
}

int icenter[nrmodel], isize[nrmodel], ncenter[nrmodel], nsize[nrmodel];

int nm_max = 2;
char *mname, **mnames = thCalloc(nm_max, sizeof(char *));
mnames[0] = thCalloc(nm_max * MX_STRING_LEN, sizeof(char));
for (i = 1; i < nm_max; i++) {
	mnames[i] = mnames[0] + i * MX_STRING_LEN;
} 
mname = mnames[0];

#if 0
#endif


RET_CODE status;	
int k, kmax, kfit = 0;
printf("%s: adding variables to nonlinear fit \n", name);
kmax = shChainSize(objclist);
printf("%s: number of objects for nonlinear fit = %d \n", name, kmax); 
for (k = 0; k < kmax; k++) {
	THOBJCTYPE objctype;
	char *objcname = NULL;
	THOBJC *objc = shChainElementGetByPos(objclist, k);
	objctype = objc->thobjctype;
	status = thObjcNameGetFromType(objctype, &objcname);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (objcname) from (objctype)", name);
		return(status);
	}
	int match_fit_request = check_string_in_string_list(objcname, classnames, nclass);
	if (match_fit_request) {
	if (!strcmp(objcname, "DEVGALAXY") || !strcmp(objcname, "EXPGALAXY") || 
		!strcmp(objcname, "GAUSSIAN") || !strcmp(objcname, "GALAXY") || 
		!strcmp(objcname, "CDCANDIDATE")) {

		kfit++;
		strcpy(mnames[1], "");
		if (!strcmp(objcname, "DEVGALAXY")) strcpy(mnames[0], "deV");
		if (!strcmp(objcname, "EXPGALAXY")) strcpy(mnames[0], "Exp");
		if (!strcmp(objcname, "GAUSSIAN")) strcpy(mnames[0], "gaussian");
		if (!strcmp(objcname, "GALAXY")) {
			strcpy(mnames[0], "deV");
			strcpy(mnames[1], "Exp");
		}
		if (!strcmp(objcname, "CDCANDIDATE")) {
			if (component == HALO_ONLY) {
				if (thObjcHasModel(objc, "deV2")) {
					strcpy(mnames[0], "deV2");
				} else if (thObjcHasModel(objc, "Exp2")) {
					strcpy(mnames[0], "Exp2");
				} else if (thObjcHasModel(objc, "Pl")) {
					strcpy(mnames[0], "Pl");
				} else if (thObjcHasModel(objc, "sersic2")) {
					strcpy(mnames[0], "sersic2");
				}  else if (thObjcHasModel(objc, "coresersic")) {
					strcpy(mnames[0], "coresersic"); 
				} else {
					strcpy(mnames[0], "");
					thError("%s: WARNING - halo model could not be found for '%s' (mcdcomp = '%s')", name, objcname, shEnumNameGetFromValue("MCDCOMP", component));
				} 
				strcpy(mnames[1], "");
			} else if (component == HALO_AND_CORE) {
				if (thObjcHasModel(objc, "deV1")) {
					strcpy(mnames[0], "deV1");
				} else if (thObjcHasModel(objc, "sersic")) {
					strcpy(mnames[0], "sersic");
				} else if (thObjcHasModel(objc, "sersic1")) {
					strcpy(mnames[0], "sersic1");
				} else if (thObjcHasModel(objc, "Pl1")) {
					strcpy(mnames[0], "Pl1");
				} else if (thObjcHasModel(objc, "exp1")) {
					strcpy(mnames[0], "exp1");
				} else if (thObjcHasModel(objc, "deV")) {
					strcpy(mnames[0], "deV"); 
				} else if (thObjcHasModel(objc, "exp")) {
					strcpy(mnames[0], "exp"); 
				} else if (thObjcHasModel(objc, "Pl")) {
					strcpy(mnames[0], "Pl");
				} else if (thObjcHasModel(objc, "coresersic")) {
					strcpy(mnames[0], "coresersic"); 
				} else { 
					thError("%s: ERROR - object does not have any supported core model", name);
					return(SH_GENERIC_ERROR);
				}
				if (thObjcHasModel(objc, "deV2")) {
					strcpy(mnames[1], "deV2");
				} else if (thObjcHasModel(objc, "Exp2")) {
					strcpy(mnames[1], "Exp2");
				} else if (thObjcHasModel(objc, "Pl")) {
					strcpy(mnames[1], "Pl");
				} else if (thObjcHasModel(objc, "sersic2")) {
					strcpy(mnames[1], "sersic2");
				} else {
					strcpy(mnames[1], "");
					thError("%s: WARNING - halo model could not be found for '%s' (mcdcomp = '%s')", name, objcname, shEnumNameGetFromValue("MCDCOMP", component));
				} 

			} else {
				thError("%s: ERROR - usupported (mcdcomp) flag '%s'", name, shEnumNameGetFromValue("MCDCOMP", component));
				return(SH_GENERIC_ERROR);
			}
		}	

		#if 1
		int i_model;	
		MFIT_RNAME_LOC *loc = thMfitRnameLocNew();
		
		for (i_model = 0; i_model < 2; i_model++) {
			int nrecord = 0;
			mname = mnames[i_model];
			if (mname != NULL && strlen(mname) != 0 && thObjcHasModel(objc, mname) == 1) {
				nrecord = -1;
				status = get_parfit_rnames_for_model(mname, fitflag, rnames[i_model], &nrecord, loc);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not get non-linear fit parameter list for (mname = '%s')", name, mname);
					return(status);
				}
				status = thMClaimObjcVar(map, objc, mname, rnames[i_model], nrecord);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not add model '%s' in (objc: %d) to fit", name, mname, objc->thid);
					return(status);	
				}
				icenter[i_model] = loc->icenter;
				isize[i_model] = loc->isize;
				ncenter[i_model] = loc->ncenter;
				nsize[i_model] = loc->nsize;

				int j;
				printf("%s: added (%d) records to model '%s' of (objc = %p), fitflag = %x \n", name, nrecord, mname, objc, fitflag);
				for (j = 0; j < nrecord; j++) {
					printf("%s: added '%s' to model '%s' fit list of (objc = %p) \n", name, rnames[i_model][j], mname, objc);
				}
			}
		}
		thMfitRnameLocDel(loc);
		mname = mnames[1];
		if (mname != NULL && strlen(mname) != 0 && thObjcHasModel(objc, mname) == 1) {
				int fit_center = (MFIT_CENTER | MFIT_CENTER1 | MFIT_CENTER2);
				int fit_lock_center = (MFIT_LOCK_CENTER);
				int fit_re = (MFIT_RE | MFIT_RE1 | MFIT_RE2);
				int fit_lock_re = (MFIT_LOCK_RE);
				if ((fitflag & fit_center) && (fitflag & fit_lock_center)) {
					if (!strcmp(objcname, "GALAXY") || !strcmp(objcname, "CDCANDIDATE")) {
						status = thMEquivObjcVars(map, objc, mnames[0], rnames[0] + icenter[0], 
						objc, mnames[1], rnames[1] + icenter[1], 1);
					}	
					if (status != SH_SUCCESS) {
						thError("%s: ERROR - could not claim centers as equiv", name);
						return(status);
					}
				}
				if ((fitflag & fit_re) && (fitflag & fit_lock_re)) {
					if (!strcmp(objcname, "GALAXY") || !strcmp(objcname, "CDCANDIDATE")) {
						status = thMEquivObjcVars(map, objc, mnames[0], rnames[0] + isize[0], 
						objc, mnames[1], rnames[1] + isize[1], 1);
					}	
					if (status != SH_SUCCESS) {
						thError("%s: ERROR - could not claim radii as equiv", name);
						return(status);
					}
				}
		}
		
		#else
		#endif
	}
	}	

if (kfit >= DEBUG_NFIT) break;
} /* end of for loop */

thFree(rnames[0][0]);
thFree(rnames[0]);
thFree(rnames);
thFree(iornames[0]);
thFree(iornames);

if (nfound != NULL) *nfound = kfit;

return(SH_SUCCESS);
}



int check_string_in_string_list(char *string, char **list, int n) {
shAssert(n >= 0);
shAssert(n == 0 || list != NULL);
int res = 0;
int i;
for (i = 0; i < n; i++) {
	shAssert(list[i] != NULL);
	res = res || (!strcmp(list[i], string));
}
return(res);
}


RET_CODE thMUpdateObjclist(MAPMACHINE *map, MASK_UPDATE mask_flag) {
char *name = "thMUpdateObjclist";
if (map == NULL) {
	thError("%s: ERROR - null input (map)", name);
	return(SH_GENERIC_ERROR);
}
if (mask_flag == MASK_NO_UPDATE) {
	thError("%s: WARNING - no update was made to mask", name);
	return(SH_SUCCESS);
}

THOBJCTYPE SKY_OBJC = UNKNOWN_OBJC;
MSkyGetType(&SKY_OBJC);

RET_CODE status;
if (mask_flag & MASK_BAD_LINEAR_OBJC) {
	int declaim_count = 0, claim_count = 0;
	char **mnames = shCalloc(100, sizeof(char *));
	/* 
	get_linear_objc_list
	if the object is not sky
	get_model_list_for each_object
	declaim the object with all the models of the object
	*/
	THOBJC **pobjcs = NULL, **objcs = NULL;
	int npobjc = 0, nobjc = 0;
	status = thMapmachineGetUAObjcs(map, (void ***) &objcs, &nobjc);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - coul not get linear (objc) list from (map)", name);
		return(status);
	}
	if (nobjc <= 0) {
		thError("%s: WARNING - (nobjc = %d) while (mask_flag) suggests linear object masks has been marked bad", name, nobjc);
		return(SH_SUCCESS);
	}
	status = thMapmachineGetUPObjcs(map, (void ***) &pobjcs, &npobjc);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get nonlinear (objc) list from (map)", name);
		return(status);
	}
	if (npobjc <= 0) {
		thError("%s: WARNING - (n_p_objc = %d) found for nonlinear objects in (map)", name, npobjc);
	}
	
	int i;
	for (i = 0; i < nobjc; i++) {
		THOBJC *objc = objcs[i];
		OBJC_ELEM *oe = NULL;
		THOBJCTYPE type = objc->thobjctype;
		int declaim_decision = MASK_BAD_UNKNOWN_FLAG;
		status = thMDecideDeclaimObjc(objc, pobjcs, npobjc, &declaim_decision);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not make declaim decision for (iobjc = %d)", name, i);
			return(status);
		}
		if (declaim_decision & MASK_BAD_AGGRESSIVE_FLAG) {
			declaim_count++;
		} else {
			claim_count++;
		}
		if ((type != SKY_OBJC) && (type != UNKNOWN_OBJC) && (declaim_decision & MASK_BAD_AGGRESSIVE_FLAG)) {

			status = thObjcElemGetByType(objc->thobjctype, &oe);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not derive (objc_elem) for (objc: %ld, type: %ld) from object bank", name, (long int) objc->thid, (long int) objc->thobjctype);
				return(status);
			}

			CHAIN *ms = oe->ms;
			int nmodel;
			if (ms == NULL || (nmodel = shChainSize(ms)) ==  0) {
				thError("%s: ERROR - no specific model seems to be upload to (objc_elem) for (objc: %d)", name, objc->thid);
				return(SH_GENERIC_ERROR);
			}

			MODEL_ELEM *me = NULL;
			int i;
			for (i = 0; i < nmodel; i++) {
				me = shChainElementGetByPos(ms, i);
				if (me == NULL) {
					thError("%s: ERROR - null (model_elem) detected at location (%d) in definition of (objc: %d) of type '%s'", name, i, objc->thid, oe->classname);
					return(SH_GENERIC_ERROR);
				} 
				mnames[i] = me->mname;
			}	
			status = thMDeclaimObjc(map, objc, mnames, nmodel);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not declaim for (objc: %d) from (mapmachine)", name, objc->thid);
				return(status);
			}
		}
	}

	printf("%s: amplitude claim_count = %d, amplitude declaim_count = %d \n", name, claim_count, declaim_count);

	shFree(mnames);
} else {
	thError("%s: WARNING - ineffective (mask_flag)", name);
	return(SH_SUCCESS);
}

return(SH_SUCCESS);
}


RET_CODE thMDecideDeclaimObjc(THOBJC *objc, THOBJC **upobjcs, int npobjc, int *declaim_decision) {
char *name = "thMDecideDeclaimObjc";
shAssert(declaim_decision != NULL);
shAssert(npobjc >= 0);
if (upobjcs == NULL && npobjc > 0) {
	thError("%s: ERROR - null nonlinear (objc) list passed while (npobjc = %d)", name, npobjc);
	*declaim_decision = MASK_BAD_UNKNOWN_FLAG;
	return(SH_GENERIC_ERROR);
} else if (upobjcs != NULL && npobjc == 0) {
	thError("%s: ERROR - nonlinear (objc) list passed while (npobjc = %d)", name, npobjc);
	*declaim_decision = MASK_BAD_UNKNOWN_FLAG;
	return(SH_GENERIC_ERROR);
}
if (objc == NULL) {
	thError("%s: WARNING - null (objc) passed", name);
	*declaim_decision = MASK_BAD_FLAG;
	return(SH_SUCCESS);
}
THOBJCTYPE type = objc->thobjctype;
if (type == SKY_OBJC) {
 	*declaim_decision = MASK_BAD_FLAG;
	return(SH_SUCCESS);
}

RET_CODE status;
THPIX xc, yc;
status = thObjcGetPhCenter(objc, &xc, &yc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get center coordinates for (objc)", name);
	*declaim_decision = MASK_BAD_UNKNOWN_FLAG;
	return(status);
}
/* 
printf("%s: object under analysis (%g, %g) \n", name, (float) xc, (float) yc);
*/

int i;
THPIX distance = 1.0E20;
for (i = 0; i < npobjc; i++) {
	THOBJC *pobjc = upobjcs[i];
	if (pobjc->thobjctype != SKY_OBJC) {
		THPIX pxc = 0.0, pyc = 0.0;
		status = thObjcGetPhCenter(pobjc, &pxc, &pyc);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get center coordinates for (pobjc)", name);
			*declaim_decision = MASK_BAD_UNKNOWN_FLAG;
			return(status);
		}
		THPIX distance_i = pow((pxc - xc), 2.0) + pow((pyc - yc), 2.0);
		distance_i = pow(distance_i, 0.5);
		/* 
		printf("%s: non-linear object (%g, %g), distance = %g \n", name, (float) pxc, (float) pyc, (float) distance_i);
		*/
		distance = MIN(distance, distance_i);
	}
}

int decision = MASK_BAD_UNKNOWN_FLAG;
if (distance > (THPIX) MASK_BAD_AGGRESSIVE_DISTANCE) {
	decision = MASK_BAD_AGGRESSIVE_FLAG;
} else if (distance > (THPIX) MASK_BAD_DISTANCE) {
	decision = MASK_BAD_FLAG;
} else if (distance <= (THPIX) MASK_BAD_DISTANCE) {
	decision = MASK_BAD_MILD_FLAG;
} else {
	thError("%s: ERROR - binary error - (distance = %g) does not match any criteria", name, (float) distance);
	*declaim_decision = MASK_BAD_UNKNOWN_FLAG;
	return(SH_GENERIC_ERROR);
}	 

/* 
printf("%s: distance = %g, decision = %d \n", name, (float) distance, (int) decision);
*/

*declaim_decision = decision;
return(SH_SUCCESS);
}

RET_CODE get_parfit_rnames_for_model(char *mname, int fitflag, char **rnames, int *nrecord, MFIT_RNAME_LOC *loc) {
char *name = "get_parfit_rnames_for_model";

if (mname == NULL || strlen(mname) == 0) {
	thError("%s: ERROR - null or empty (mname)", name);
	return(SH_GENERIC_ERROR);
}
if (rnames == NULL || nrecord == NULL || loc == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}

CDESCRIPTION tdesc = UNKNOWN_CDESCRIPTION;
RET_CODE status = thCTransformGetDesc(&tdesc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (description)", name);
	return(status);
}

int len = strlen(mname);
char c = mname[len - 1];
int fit_re = MFIT_RE, fit_rb = MFIT_RB, fit_center = MFIT_CENTER, fit_e = MFIT_E, fit_phi = MFIT_PHI, fit_index = MFIT_INDEX;
if (c == '1') {
	fit_re |= MFIT_RE1;
	fit_rb |= MFIT_RB1;
	fit_center |= MFIT_CENTER1;
	fit_e |= MFIT_E1;
	fit_phi |= MFIT_PHI1;
	fit_index |= MFIT_INDEX1;
} else if (c == '2') {
	fit_re |= MFIT_RE2;
	fit_rb |= MFIT_RB2;
	fit_center |= MFIT_CENTER2;
	fit_e |= MFIT_E2;
	fit_phi |= MFIT_PHI2;
	fit_index |= MFIT_INDEX2;
}

int j = 0;
int isize = -1, nsize = 0;
int ksize = -1;
if (tdesc & ALGEBRAIC) {
	strcpy(rnames[j++], "a");
	strcpy(rnames[j++], "b");
	strcpy(rnames[j++], "c");
} else if (tdesc & CANONICAL) {
	if (fitflag & fit_re) {
		isize = j;
		strcpy(rnames[j++], "re");
		nsize++;
	}
	if (fitflag & fit_rb) {
		ksize = j;
		strcpy(rnames[j++], "rb");
		nsize++;
	}
	if (fitflag & fit_e) strcpy(rnames[j++], "e");
	if (fitflag & fit_phi) strcpy(rnames[j++], "phi");	
} else if (tdesc & (SUPERCANONICAL | UBERCANONICAL)) {
	if (fitflag & fit_re) {
		isize = j;
		strcpy(rnames[j++], "re");
		nsize++;
	}
	if (fitflag & fit_rb) {
		ksize = j;
		strcpy(rnames[j++], "rb");
		nsize++;
	}
	if (fitflag & fit_e) strcpy(rnames[j++], "E");
	if (fitflag & fit_phi) strcpy(rnames[j++], "phi");
} else if (tdesc & SUPERCANONICAL_RSUPERLOG) {
	
	if (fitflag & fit_re) {
		isize = j;
		strcpy(rnames[j++], "ue");
		nsize++;
	}
	  
	if (fitflag & fit_rb) {
		ksize = j;
		strcpy(rnames[j++], "ub");
		nsize++;
	}	
	
	if (fitflag & fit_e) strcpy(rnames[j++], "E");
	if (fitflag & fit_phi) strcpy(rnames[j++], "phi");
} else {
	thError("%s: ERROR - unsupported coordinate description '%s'", name, shEnumNameGetFromValue("CDESCRIPTION", tdesc));
	return(SH_GENERIC_ERROR);
}

if (fitflag & fit_index) {
	if (tdesc & DIRECT_SERSIC_INDEX) {
		strcpy(rnames[j++], "n");
		strcpy(rnames[j++], "delta");
		strcpy(rnames[j++], "gamma");
	} else if (tdesc & INDIRECT_SERSIC_INDEX) {
		strcpy(rnames[j++], "N");
		strcpy(rnames[j++], "D");
		strcpy(rnames[j++], "G");
	}
}

int jshape = -1;
int nshape = -1;	
jshape = j;
nshape = j;

int icenter, jcenter, ncenter;
if (fitflag & MFIT_CENTER) {
	icenter = j;
	strcpy(rnames[j++], "xc");  
	strcpy(rnames[j++], "yc");
	jcenter = j;
	ncenter = (jcenter - icenter);
}

*nrecord = j;

printf("%s: records added for model '%s': ", name, mname);
for (j = 0; j < *nrecord; j++) {
	printf("'%s,' ", rnames[j]);
}
printf("\n"); 
loc->icenter = icenter;
loc->isize = isize;
loc->ncenter = ncenter;
loc->nsize = nsize;
return(SH_SUCCESS);
}

