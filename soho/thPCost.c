#include "thDebug.h"
#include "thPCost.h"

static int init = 0;
static PCOST_GENERIC *wpcost_all = NULL;



static int is_valid_core_name(char *mname);
static int is_valid_halo_name(char *mname);
static RET_CODE get_halo_core_properties(THOBJC *objc, void **halo, char **phalo, void **core, char **pcore);

void thPCostInit() {
	if (init == 1) return;
	init = 1;
	wpcost_all = thPCostGenericNew();
	return;
}

void thPCostFini () {
	if (init == 0) return;
	init = 0;
	thPCostGenericDel(wpcost_all);
	wpcost_all = NULL;
	return;
}

int is_valid_core_name(char *mname) {
int res = 0;
if (mname == NULL || strlen(mname) == 0) {
	res = 0;
} else {
	res = (!strcmp(mname, "deV1") || !strcmp(mname, "Exp1") || !strcmp(mname, "deV"));
}
return(res);
}

int is_valid_halo_name(char *mname) {
int res = 0;
if (mname == NULL || strlen(mname) == 0) {
	res = 0;
} else {
	res = (!strcmp(mname, "deV2") || !strcmp(mname, "Exp2") || !strcmp(mname, "Pl") || !strcmp(mname, "pl"));
}
return(res);
}


RET_CODE get_halo_core_properties(THOBJC *objc, void **halo, char **phalo, void **core, char **pcore) {
char *name = "get_halo_core_properties";
shAssert(objc != NULL);
shAssert(halo != NULL);
shAssert(core != NULL);
char *classname = NULL;
RET_CODE status;
status = thObjcGetClassName(objc, &classname);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get classname for (objc = %p)", name, (void *) objc);
	*halo = NULL;
	*core = NULL;
	return(status);
}
if (classname == NULL || strlen(classname) == 0) {
	thError("%s: ERROR - null or empty classname for (objc = %p)", name, (void *) objc);
	*halo = NULL;
	*core = NULL;
	return(SH_GENERIC_ERROR);
}
char *cdclassname = "CDCANDIDATE";
if (strcmp(classname, cdclassname)) {
	thError("%s: ERROR - expected class = '%s', found '%s' for (objc = %p)", name, classname, cdclassname);
	*halo = NULL;
	*core = NULL;
	return(SH_GENERIC_ERROR);
}

CHAIN *props = objc->thprop;
if (props == NULL || shChainSize(props) == 0) {
	thError("%s: ERROR - model properties not set for cD (objc = %p)", name, (void *) objc);
	*halo = NULL;
	*core = NULL;
	return(SH_GENERIC_ERROR);
	}
int n = shChainSize(props);
if (n > 2 || n < 1) {
	thError("%s: ERROR - cD models are expected to have (1 or 2) models, found (%d) instead", name, n);
	*core = NULL;
	*halo = NULL;
	return(SH_GENERIC_ERROR);
}

int nprops = shChainSize(props);

if (nprops == 1) {
	THPROP *prop1 = shChainElementGetByPos(props, 0);
	shAssert(prop1 != NULL);
	char *mname1 = prop1->mname;
	shAssert(mname1 != NULL && strlen(mname1) != 0);
	THPROP *cprop = NULL;
	if (is_valid_core_name(mname1)) {
		cprop = prop1;
	} else {
		thError("%s: ERROR - core model name '%s' not supported in (objc = %p)", name, mname1, (void *) objc);
		*core = NULL;
	}
	if (halo != NULL) *halo = NULL;
	if (phalo != NULL) *phalo = NULL;
	if (core != NULL) *core = cprop->value;
	if (pcore != NULL) *pcore = cprop->pname;
} else if (nprops == 2) {
	THPROP *prop1 = shChainElementGetByPos(props, 0);
	THPROP *prop2 = shChainElementGetByPos(props, 1);
	shAssert(prop1 != NULL && prop2 != NULL);
	char *mname1 = prop1->mname;
	char *mname2 = prop2->mname;
	shAssert(mname1 != NULL && strlen(mname1) != 0);
	shAssert(mname2 != NULL && strlen(mname2) != 0);
	THPROP *cprop = NULL, *hprop = NULL;
	if (is_valid_core_name(mname1) && is_valid_halo_name(mname2)) {
		cprop = prop1;
		hprop = prop2;
	} else if (is_valid_core_name(mname2) && is_valid_halo_name(mname1)) {
		cprop = prop2;
		hprop = prop1;
	} else {
		thError("%s: ERROR - cannot tell halo and core from model names '%s', '%s' in (objc = %p)", name, mname1, mname2, (void *) objc);
		*halo = NULL;
		*core = NULL;
		*phalo = NULL;
		*pcore = NULL;
		return(SH_GENERIC_ERROR);
	}
	if (halo != NULL) *halo = hprop->value;
	if (phalo != NULL) *phalo = hprop->pname;
	if (core != NULL) *core = cprop->value;
	if (pcore != NULL) *pcore = cprop->pname;
} else {
	thError("%s: ERROR - found (%d) models in the cD object", name, nprops);
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}


RET_CODE cD_pcost_generic(THOBJC *objc, PCOST_GENERIC *pcost_all) {
char *name = "cD_pcost_generic";
shAssert(pcost_all != NULL);
RET_CODE status;
void *core = NULL, *halo = NULL;
char *phalo = NULL, *pcore = NULL;
status = get_halo_core_properties(objc, (void **) &halo, &phalo, (void **) &core, &pcore);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get core and halo properties for the object (%x)", name, objc);
	pcost_all->pcost = CHISQFLNAN;
	pcost_all->DpcostDhalo = CHISQFLNAN;
	pcost_all->DpcostDcore = CHISQFLNAN;
	pcost_all->DDpcostDhaloDhalo = CHISQFLNAN;
	pcost_all->DDpcostDcoreDcore = CHISQFLNAN;
	pcost_all->DDpcostDhaloDcore = CHISQFLNAN;
	pcost_all->DpcostDuhalo = CHISQFLNAN;
	pcost_all->DpcostDucore = CHISQFLNAN;
	pcost_all->DDpcostDuhaloDuhalo = CHISQFLNAN;
	pcost_all->DDpcostDucoreDucore = CHISQFLNAN;
	pcost_all->DDpcostDuhaloDucore = CHISQFLNAN;
	
	return(status);
}

shAssert(phalo != NULL && strlen(phalo) != 0);
shAssert(pcore != NULL && strlen(pcore) != 0);

CHISQFL r1, u1, e1, E1, De1DE1, DDe1DE1DE1, Dr1Du1, DDr1Du1Du1;
CHISQFL r2, u2, e2, E2, De2DE2, DDe2DE2DE2, Dr2Du2, DDr2Du2Du2;


if (!strcmp(pcore, "DEVPARS")) {
	DEVPARS *deVcore = (DEVPARS *) core;
	r1 = (CHISQFL) deVcore->re;
	u1 = (CHISQFL) deVcore->ue;
	e1 = (CHISQFL) deVcore->e;
	E1 = (CHISQFL) deVcore->E;
	De1DE1 = (CHISQFL) deVcore->eE;
	DDe1DE1DE1 = (CHISQFL) deVcore->eEE;
	Dr1Du1 = (CHISQFL) deVcore->reUe;  
	DDr1Du1Du1 = (CHISQFL) deVcore->reUeUe;  
} else {
	thError("%s: ERROR - unsupported parameter type '%s' for core", name, pcore);
	return(SH_GENERIC_ERROR);
}
if (!strcmp(phalo, "DEVPARS")) {
	DEVPARS *deVhalo = (DEVPARS *) halo;
	r2 = (CHISQFL) deVhalo->re;
	u2 = (CHISQFL) deVhalo->ue;
	e2 = (CHISQFL) deVhalo->e;
	E2 = (CHISQFL) deVhalo->E;
	De2DE2 = (CHISQFL) deVhalo->eE;
	DDe2DE2DE2 = (CHISQFL) deVhalo->eEE;
	Dr2Du2 = (CHISQFL) deVhalo->reUe;  
	DDr2Du2Du2 = (CHISQFL) deVhalo->reUeUe;  
} else if (!strcmp(phalo, "POWERLAWPARS")) {
	POWERLAWPARS *Plhalo = (POWERLAWPARS *) halo;
	r2 = (CHISQFL) Plhalo->re;
	u2 = (CHISQFL) Plhalo->ue;
	e2 = (CHISQFL) Plhalo->e;
	E2 = (CHISQFL) Plhalo->E;
	De2DE2 = (CHISQFL) Plhalo->eE;
	DDe2DE2DE2 = (CHISQFL) Plhalo->eEE;
	Dr2Du2 = (CHISQFL) Plhalo->reUe;  
	DDr2Du2Du2 = (CHISQFL) Plhalo->reUeUe;  
} else {
	thError("%s: ERRRO - unsupported parameter type '%s' for halo", name, phalo);
	return(SH_GENERIC_ERROR);
}



shAssert((CHISQFL) PCOST_LOWER_THRESHOLD <= (CHISQFL) PCOST_UPPER_THRESHOLD);
CHISQFL z = 0.0, pcost = 0.0, DpcostDz = 0.0, DzDr1 = 0.0, DzDr2 = 0.0;
CHISQFL DpcostDr1 = 0.0, DpcostDr2 = 0.0;
CHISQFL DpcostDu1 = 0.0, DpcostDu2 = 0.0;
CHISQFL DDpcostDzDz = 0.0, DDzDr1Dr1 = 0.0, DDzDr2Dr2 = 0.0, DDzDr1Dr2 = 0.0;
CHISQFL DDpcostDr1Dr1 = 0.0, DDpcostDr2Dr2 = 0.0, DDpcostDr1Dr2 =  0.0;
CHISQFL DDpcostDu1Du1 = 0.0, DDpcostDu2Du2 = 0.0, DDpcostDu1Du2 =  0.0;
CHISQFL DpcostDe1 = 0.0, DpcostDe2 = 0.0, DDpcostDe1De1 = 0.0, DDpcostDe2De2 = 0.0;

CHISQFL DpcostDE1 = 0.0, DDpcostDE1DE1 = 0.0, DpcostDE2 = 0.0, DDpcostDE2DE2 = 0.0;

if ((r2 / r1) < (CHISQFL) PCOST_LOWER_THRESHOLD) {
	z = -(log(r2 / r1) - (CHISQFL) LNPCOST_LOWER_THRESHOLD) / (CHISQFL) LNPCOST_LOWER_THRESHOLD_MARGIN;
	pcost = (CHISQFL) CHISQ_PCOST_LOWER_THESHOLD * pow(z, PCOST_LOWER_INDEX);
	if (pcost < 0) {
		thError("%s: WARNING - negative pcost for halo/core lower threshold: (pcost, z, rcore, rhalo) = (%g, %g, %g, %g)", 
		name, (float) pcost, (float) z, (float) r1, (float) r2);
	} 
	DpcostDz = PCOST_LOWER_INDEX * pcost / z;
	DzDr1 = ((CHISQFL) 1.0) / r1 / (CHISQFL) LNPCOST_LOWER_THRESHOLD;
	DzDr2 = ((CHISQFL) -1.0) / r2 / (CHISQFL) LNPCOST_LOWER_THRESHOLD;  
	DpcostDr1 = DpcostDz * DzDr1;
	DpcostDr2 = DpcostDz * DzDr2;
	DDpcostDzDz = ((CHISQFL) PCOST_LOWER_INDEX - (CHISQFL) 1.0) * DpcostDz / z;
	DDzDr1Dr1 = -DzDr1 / r1;
	DDzDr2Dr2 = -DzDr2 / r2;
	DDzDr1Dr2 = 0.0;
	DDpcostDr1Dr1 = DDpcostDzDz *  pow(DzDr1, 2.0) + DpcostDz * DDzDr1Dr1;
	DDpcostDr2Dr2 = DDpcostDzDz *  pow(DzDr2, 2.0) + DpcostDz * DDzDr2Dr2;
	DDpcostDr1Dr2 = DDpcostDzDz * DzDr1 * DzDr2 + DpcostDz * DDzDr1Dr2;
} else if ((r2 / r1) > (CHISQFL) PCOST_UPPER_THRESHOLD) {
	z = (log(r2 / r1) - (CHISQFL) LNPCOST_UPPER_THRESHOLD) / (CHISQFL) LNPCOST_UPPER_THRESHOLD_MARGIN;
	pcost = (CHISQFL) CHISQ_PCOST_UPPER_THRESHOLD * pow(z, PCOST_UPPER_INDEX);
	if (pcost < 0) {
		thError("%s: WARNING - negative pcost for halo/core upper threshold: (pcost, z, rcore, rhalo) = (%g, %g, %g, %g)", 
		name, (float) pcost, (float) z, (float) r1, (float) r2);
	} 
	DpcostDz = ((CHISQFL) PCOST_UPPER_INDEX) * pcost / z;
	DzDr1 = ((CHISQFL) -1.0) / r1 / (CHISQFL) LNPCOST_UPPER_THRESHOLD_MARGIN;
	DzDr2 = ((CHISQFL) 1.0) / r2 / (CHISQFL) LNPCOST_UPPER_THRESHOLD_MARGIN;  
	DpcostDr1 = DpcostDz * DzDr1;
	DpcostDr2 = DpcostDz * DzDr2;
	DDpcostDzDz = ((CHISQFL) PCOST_UPPER_INDEX - (CHISQFL) 1.0) * DpcostDz / z;
	DDzDr1Dr1 = -DzDr1 / r1;
	DDzDr2Dr2 = -DzDr2 / r2;
	DDzDr1Dr2 = 0.0;
	DDpcostDr1Dr1 = DDpcostDzDz *  pow(DzDr1, 2.0) + DpcostDz * DDzDr1Dr1;
	DDpcostDr2Dr2 = DDpcostDzDz *  pow(DzDr2, 2.0) + DpcostDz * DDzDr2Dr2;
	DDpcostDr1Dr2 = DDpcostDzDz * DzDr1 * DzDr2 + DpcostDz * DDzDr1Dr2;

}

if (r1 < (CHISQFL) PCOST_RCORE_LOWER_THRESHOLD) {
	z = log(r1 / (CHISQFL) PCOST_RCORE_LOWER_THRESHOLD) / (CHISQFL) LNPCOST_RCORE_LOWER_THRESHOLD_MARGIN; 
	CHISQFL pcost_rcore = (CHISQFL) CHISQ_PCOST_RCORE * pow(z, PCOST_RCORE_INDEX);
	if (pcost_rcore < 0) {
		thError("%s: WARNING - negative pcost for r_core threshold: (pcost, z, rcore) = (%g, %g, %g) \n", 
		name, (float) pcost_rcore, (float) z, (float) r1);
	}
	CHISQFL Dpcost_rcoreDz = (CHISQFL) PCOST_RCORE_INDEX * pcost_rcore / z;
	CHISQFL DDpcost_rcoreDzDz = ((CHISQFL) PCOST_RCORE_INDEX -  (CHISQFL) 1.0) * Dpcost_rcoreDz / z;
	DzDr1 = (CHISQFL) 1.0 / r1 / (CHISQFL) LNPCOST_RCORE_LOWER_THRESHOLD_MARGIN; 
 	DDzDr1Dr1 = -DzDr1 / r1;
	CHISQFL Dpcost_rcoreDr1 = Dpcost_rcoreDz * DzDr1;
	CHISQFL DDpcost_rcoreDr1Dr1 = DDpcost_rcoreDzDz * pow(DzDr1, 2.0) + Dpcost_rcoreDz * DDzDr1Dr1; 	 
	pcost += pcost_rcore;
	DpcostDr1 += Dpcost_rcoreDr1;
	DDpcostDr1Dr1 += DDpcost_rcoreDr1Dr1;	
}

DpcostDu1 = DpcostDr1 * Dr1Du1;
DpcostDu2 = DpcostDu2 * Dr2Du2;
DDpcostDu1Du1 = DDpcostDr1Dr1 * Dr1Du1 * Dr1Du1 + DpcostDr1 * DDr1Du1Du1;
DDpcostDu2Du2 = DDpcostDr2Dr2 * Dr2Du2 * Dr2Du2 + DpcostDr2 * DDr2Du2Du2;
DDpcostDu1Du2 = DDpcostDr1Dr2 * Dr1Du1 * Dr2Du2;



CHISQFL ab1 = (CHISQFL) 1.0 / sqrt((CHISQFL) 1.0 - e1 * e1);
CHISQFL ab2 = (CHISQFL) 1.0 / sqrt((CHISQFL) 1.0 - e2 * e2);

if (ab1 > (CHISQFL) PCOST_AB_LOWER_THRESHOLD) {

	CHISQFL Dab1De1 = e1 * pow(ab1, 3.0);
	CHISQFL DDab1De1De1 = pow(ab1, 3.0) + e1 * Dab1De1 * 3.0 * pow(ab1, 2.0);

	z = log(ab1 / (CHISQFL) PCOST_AB_LOWER_THRESHOLD) / (CHISQFL) LNPCOST_AB_MARGIN;
	CHISQFL DzDab1 = (CHISQFL) 1.0 / ab1 / (CHISQFL) LNPCOST_AB_MARGIN;
	CHISQFL DDzDab1Dab1 = - DzDab1 / ab1;
	CHISQFL DzDe1 = DzDab1 * Dab1De1;
	CHISQFL DDzDe1De1 = DDzDab1Dab1 * pow(Dab1De1, 2.0) + DzDab1 * DDab1De1De1;
	CHISQFL pcost1 = (CHISQFL) CHISQ_PCOST_E * pow(z, PCOST_E_INDEX);
	CHISQFL Dpcost1Dz = (CHISQFL) PCOST_E_INDEX * pcost1 / z;
	CHISQFL DDpcost1DzDz = (CHISQFL) (PCOST_E_INDEX - 1.0) * Dpcost1Dz / z;
	CHISQFL Dpcost1De1 = Dpcost1Dz * DzDe1;
	CHISQFL DDpcost1De1De1 = DDpcost1DzDz * pow(DzDe1, 2.0) + Dpcost1Dz * DDzDe1De1;

	pcost += pcost1;
	DpcostDe1 = Dpcost1De1;
	DDpcostDe1De1 = DDpcost1De1De1; 

	CHISQFL Dpcost1DE1 = Dpcost1De1 * De1DE1;
	CHISQFL DDpcost1DE1DE1 = DDpcost1De1De1 * pow(De1DE1, 2.0) + Dpcost1De1 * DDe1DE1DE1;
	DpcostDE1 = Dpcost1DE1;
	DDpcostDE1DE1 = DDpcost1DE1DE1;
}

if (ab2 > (CHISQFL) PCOST_AB_LOWER_THRESHOLD) {

	CHISQFL Dab2De2 = e2 * pow(ab2, 3.0);
	CHISQFL DDab2De2De2 = pow(ab2, 3.0) + e2 * Dab2De2 * 3.0 * pow(ab2, 2.0);

	z = log(ab2 / (CHISQFL) PCOST_AB_LOWER_THRESHOLD) / (CHISQFL) LNPCOST_AB_MARGIN;
	CHISQFL DzDab2 = (CHISQFL) 1.0 / ab2 / (CHISQFL) LNPCOST_AB_MARGIN;
	CHISQFL DDzDab2Dab2 = - DzDab2 / ab2;
	CHISQFL DzDe2 = DzDab2 * Dab2De2;
	CHISQFL DDzDe2De2 = DDzDab2Dab2 * pow(Dab2De2, 2.0) + DzDab2 * DDab2De2De2;
	CHISQFL pcost2 = (CHISQFL) CHISQ_PCOST_E * pow(z, PCOST_E_INDEX);
	CHISQFL Dpcost2Dz = (CHISQFL) PCOST_E_INDEX * pcost2 / z;
	CHISQFL DDpcost2DzDz = (CHISQFL) (PCOST_E_INDEX - 1.0) * Dpcost2Dz / z;
	CHISQFL Dpcost2De2 = Dpcost2Dz * DzDe2;
	CHISQFL DDpcost2De2De2 = DDpcost2DzDz * pow(DzDe2, 2.0) + Dpcost2Dz * DDzDe2De2;

	pcost += pcost2;
	DpcostDe2 = Dpcost2De2;
	DDpcostDe2De2 = DDpcost2De2De2; 

	CHISQFL Dpcost2DE2 = Dpcost2De2 * De2DE2;
	CHISQFL DDpcost2DE2DE2 = DDpcost2De2De2 * pow(De2DE2, 2.0) + Dpcost2De2 * DDe2DE2DE2;
	DpcostDE2 = Dpcost2DE2;
	DDpcostDE2DE2 = DDpcost2DE2DE2;

}

if (e1 < (CHISQFL) 0.0) {

	z = e1 / (CHISQFL) PCOST_E;
	CHISQFL DzDe1 = (CHISQFL) 1.0 / (CHISQFL) PCOST_E;
	CHISQFL DDzDe1De1 = 0.0;
	CHISQFL pcost1 = (CHISQFL) CHISQ_PCOST_E * pow(z, PCOST_E_INDEX);
	CHISQFL Dpcost1Dz = (CHISQFL) PCOST_E_INDEX * pcost1 / z;
	CHISQFL DDpcost1DzDz = (CHISQFL) (PCOST_E_INDEX - 1.0) * Dpcost1Dz / z;
	CHISQFL Dpcost1De1 = Dpcost1Dz * DzDe1;
	CHISQFL DDpcost1De1De1 = DDpcost1DzDz * pow(DzDe1, 2.0) + Dpcost1Dz * DDzDe1De1;

	pcost += pcost1;
	DpcostDe1 += Dpcost1De1;
	DDpcostDe1De1 += DDpcost1De1De1; 

	CHISQFL Dpcost1DE1 = Dpcost1De1 * De1DE1;
	CHISQFL DDpcost1DE1DE1 = DDpcost1De1De1 * pow(De1DE1, 2.0) + Dpcost1De1 * DDe1DE1DE1;
	DpcostDE1 += Dpcost1DE1;
	DDpcostDE1DE1 += DDpcost1DE1DE1;
}

if (e2 < (CHISQFL) 0.0) {

	z = e2 / (CHISQFL) PCOST_E;
	CHISQFL DzDe2 = (CHISQFL) 1.0 / (CHISQFL) PCOST_E;
	CHISQFL DDzDe2De2 = 0.0;
	CHISQFL pcost2 = (CHISQFL) CHISQ_PCOST_E * pow(z, PCOST_E_INDEX);
	CHISQFL Dpcost2Dz = (CHISQFL) PCOST_E_INDEX * pcost2 / z;
	CHISQFL DDpcost2DzDz = (CHISQFL) (PCOST_E_INDEX - 1.0) * Dpcost2Dz / z;
	CHISQFL Dpcost2De2 = Dpcost2Dz * DzDe2;
	CHISQFL DDpcost2De2De2 = DDpcost2DzDz * pow(DzDe2, 2.0) + Dpcost2Dz * DDzDe2De2;

	pcost += pcost2;
	DpcostDe2 += Dpcost2De2;
	DDpcostDe2De2 += DDpcost2De2De2; 

	CHISQFL Dpcost2DE2 = Dpcost2De2 * De2DE2;
	CHISQFL DDpcost2DE2DE2 = DDpcost2De2De2 * pow(De2DE2, 2.0) + Dpcost2De2 * DDe2DE2DE2;
	DpcostDE2 += Dpcost2DE2;
	DDpcostDE2DE2 += DDpcost2DE2DE2;
}



pcost_all->r1 = r1;
pcost_all->r2 = r2;
pcost_all->pcost = pcost;
pcost_all->DpcostDhalo = DpcostDr2;
pcost_all->DpcostDcore = DpcostDr1;
pcost_all->DDpcostDhaloDhalo = DDpcostDr2Dr2;
pcost_all->DDpcostDcoreDcore = DDpcostDr1Dr1;
pcost_all->DDpcostDhaloDcore = DDpcostDr1Dr2;

pcost_all->u1 = u1;
pcost_all->u2 = u2;
pcost_all->DpcostDuhalo = DpcostDu2;
pcost_all->DpcostDucore = DpcostDu1;
pcost_all->DDpcostDuhaloDuhalo = DDpcostDu2Du2;
pcost_all->DDpcostDucoreDucore = DDpcostDu1Du1;
pcost_all->DDpcostDuhaloDucore = DDpcostDu1Du2;

pcost_all->e1 = e1;
pcost_all->e2 = e2;
pcost_all->DpcostDecore = DpcostDe1;
pcost_all->DpcostDehalo = DpcostDe2;
pcost_all->DDpcostDehaloDecore = 0.0;
pcost_all->DDpcostDecoreDecore = DDpcostDe1De1;
pcost_all->DDpcostDehaloDehalo = DDpcostDe2De2;

pcost_all->E1 = E1;
pcost_all->E2 = E2;
pcost_all->DpcostDEcore = DpcostDE1;
pcost_all->DpcostDEhalo = DpcostDE2;
pcost_all->DDpcostDEhaloDEcore = 0.0;
pcost_all->DDpcostDEcoreDEcore = DDpcostDE1DE1;
pcost_all->DDpcostDEhaloDEhalo = DDpcostDE2DE2;




return(SH_SUCCESS);
}

RET_CODE cD_pcost(THOBJC *objc, CHISQFL *pcost) {
char *name = "cD_pcost";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status;
status = cD_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->pcost;

#if DEBUG_PCOST
float r1 = (float) wpcost_all->r1;
float r2 = (float) wpcost_all->r2;
float u1 = (float) wpcost_all->u1;
float u2 = (float) wpcost_all->u2;
float e1 = (float) wpcost_all->e1;
float e2 = (float) wpcost_all->e2;
float E1 = (float) wpcost_all->E1;
float E2 = (float) wpcost_all->E2;

printf("%s: (1:c,2:h) r1 = %20.10g, r2 = %20.10g, u1 = %20.10g, u2 = %20.10g, e1 = %20.10g, e2 = %20.10g, E1 = %20.10g, E2 = %20.10g, pcost = %20.10g \n", name, r1, r2, u1, u2, e1, e2, E1, E2, (float) wpcost_all->pcost);
#endif

return(SH_SUCCESS);
}

RET_CODE cD_DpcostDhalo(THOBJC *objc, CHISQFL *pcost) {
char *name = "cD_DpcostDhalo";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cD_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DpcostDhalo;
return(SH_SUCCESS);
}

RET_CODE cD_DpcostDcore(THOBJC *objc, CHISQFL *pcost) {
char *name = "cD_DpcostDcore";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cD_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DpcostDcore;
return(SH_SUCCESS);
}

RET_CODE cD_DDpcostDcoreDcore(THOBJC *objc, CHISQFL *pcost) {
char *name = "cD_DDpcostDcoreDcore";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cD_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DDpcostDcoreDcore;
return(SH_SUCCESS);
}

RET_CODE cD_DDpcostDhaloDhalo(THOBJC *objc, CHISQFL *pcost) {
char *name = "cD_DDpcostDhaloDhalo";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cD_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DDpcostDhaloDhalo;
return(SH_SUCCESS);
}

RET_CODE cD_DDpcostDhaloDcore(THOBJC *objc, CHISQFL *pcost) {
char *name = "cD_DDpcostDhaloDcore";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cD_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DDpcostDhaloDcore;
return(SH_SUCCESS);
}

RET_CODE cD_DpcostDuhalo(THOBJC *objc, CHISQFL *pcost) {
char *name = "cD_DpcostDuhalo";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cD_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DpcostDuhalo;
return(SH_SUCCESS);
}

RET_CODE cD_DpcostDucore(THOBJC *objc, CHISQFL *pcost) {
char *name = "cD_DpcostDucore";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cD_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DpcostDucore;
return(SH_SUCCESS);
}

RET_CODE cD_DDpcostDucoreDucore(THOBJC *objc, CHISQFL *pcost) {
char *name = "cD_DDpcostDucoreDucore";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cD_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DDpcostDucoreDucore;
return(SH_SUCCESS);
}

RET_CODE cD_DDpcostDuhaloDuhalo(THOBJC *objc, CHISQFL *pcost) {
char *name = "cD_DDpcostDuhaloDuhalo";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cD_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DDpcostDuhaloDuhalo;
return(SH_SUCCESS);
}

RET_CODE cD_DDpcostDuhaloDucore(THOBJC *objc, CHISQFL *pcost) {
char *name = "cD_DDpcostDuhaloDucore";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cD_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DDpcostDuhaloDucore;
return(SH_SUCCESS);
}

RET_CODE cD_DpcostDehalo(THOBJC *objc, CHISQFL *pcost) {
char *name = "cD_DpcostDehalo";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cD_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DpcostDehalo;
return(SH_SUCCESS);
}

RET_CODE cD_DpcostDecore(THOBJC *objc, CHISQFL *pcost) {
char *name = "cD_DpcostDecore";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cD_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DpcostDecore;
return(SH_SUCCESS);
}

RET_CODE cD_DDpcostDecoreDecore(THOBJC *objc, CHISQFL *pcost) {
char *name = "cD_DDpcostDecoreDecore";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cD_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DDpcostDecoreDecore;
return(SH_SUCCESS);
}

RET_CODE cD_DDpcostDehaloDehalo(THOBJC *objc, CHISQFL *pcost) {
char *name = "cD_DDpcostDehaloDehalo";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cD_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DDpcostDehaloDehalo;
return(SH_SUCCESS);
}

RET_CODE cD_DDpcostDehaloDecore(THOBJC *objc, CHISQFL *pcost) {
char *name = "cD_DDpcostDehaloDecore";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cD_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DDpcostDehaloDecore;
return(SH_SUCCESS);
}

RET_CODE cD_DDpcostDEcoreDEcore(THOBJC *objc, CHISQFL *pcost) {
char *name = "cD_DDpcostDEcoreDEcore";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cD_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DDpcostDEcoreDEcore;
return(SH_SUCCESS);
}

RET_CODE cD_DDpcostDEhaloDEhalo(THOBJC *objc, CHISQFL *pcost) {
char *name = "cD_DDpcostDEhaloDEhalo";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cD_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DDpcostDEhaloDEhalo;
return(SH_SUCCESS);
}

RET_CODE cD_DDpcostDEhaloDEcore(THOBJC *objc, CHISQFL *pcost) {
char *name = "cD_DDpcostDEhaloDEcore";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cD_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DDpcostDEhaloDEcore;
return(SH_SUCCESS);
}

RET_CODE cD_DpcostDEhalo(THOBJC *objc, CHISQFL *pcost) {
char *name = "cD_DpcostDEhalo";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cD_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DpcostDEhalo;
return(SH_SUCCESS);
}

RET_CODE cD_DpcostDEcore(THOBJC *objc, CHISQFL *pcost) {
char *name = "cD_DpcostDEcore";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cD_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DpcostDEcore;
return(SH_SUCCESS);
}


/* single model cD-deV cost functions */

RET_CODE cDeV_pcost_generic(THOBJC *objc, PCOST_GENERIC *pcost_all) {
char *name = "cDeV_pcost_generic";
shAssert(pcost_all != NULL);
RET_CODE status;
DEVPARS *core = NULL;
char *pcore = NULL;
DEVPARS *none_halo = NULL;
char *none_phalo = NULL;

status = get_halo_core_properties(objc, (void **) &none_halo, &none_phalo, (void **) &core, &pcore);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get core properties for the object (%x)", name, objc);
	pcost_all->pcost = CHISQFLNAN;
	pcost_all->DpcostDhalo = CHISQFLNAN;
	pcost_all->DpcostDcore = CHISQFLNAN;
	pcost_all->DDpcostDhaloDhalo = CHISQFLNAN;
	pcost_all->DDpcostDcoreDcore = CHISQFLNAN;
	pcost_all->DDpcostDhaloDcore = CHISQFLNAN;
	return(status);
}

shAssert(pcore != NULL && strlen(pcore) != 0);
shAssert(!strcmp(pcore, "DEVPARS"));

CHISQFL r1 = (CHISQFL) core->re;
CHISQFL e1 = (CHISQFL) core->e;
CHISQFL E1 = (CHISQFL) core->E;
CHISQFL De1DE1 = (CHISQFL) core->eE;
CHISQFL DDe1DE1DE1 = (CHISQFL) core->eEE;

shAssert((CHISQFL) PCOST_LOWER_THRESHOLD <= (CHISQFL) PCOST_UPPER_THRESHOLD);
CHISQFL z = 0.0, pcost = 0.0, DzDr1 = 0.0, DpcostDr1 = 0.0;
CHISQFL DDzDr1Dr1 = 0.0, DDpcostDr1Dr1 = 0.0, DpcostDe1 = 0.0, DDpcostDe1De1 = 0.0;

CHISQFL DpcostDE1 = 0.0, DDpcostDE1DE1 = 0.0;

if (r1 < (CHISQFL) PCOST_RCORE_LOWER_THRESHOLD) {
	z = log(r1 / (CHISQFL) PCOST_RCORE_LOWER_THRESHOLD) / (CHISQFL) LNPCOST_RCORE_LOWER_THRESHOLD_MARGIN; 
	CHISQFL pcost_rcore = (CHISQFL) CHISQ_PCOST_RCORE * pow(z, PCOST_RCORE_INDEX);
	if (pcost_rcore < 0) {
		thError("%s: WARNING - negative pcost for r_core threshold: (pcost, z, rcore) = (%g, %g, %g) \n", 
		name, (float) pcost_rcore, (float) z, (float) r1);
	}
	CHISQFL Dpcost_rcoreDz = (CHISQFL) PCOST_RCORE_INDEX * pcost_rcore / z;
	CHISQFL DDpcost_rcoreDzDz = ((CHISQFL) PCOST_RCORE_INDEX -  (CHISQFL) 1.0) * Dpcost_rcoreDz / z;
	DzDr1 = (CHISQFL) 1.0 / r1 / (CHISQFL) LNPCOST_RCORE_LOWER_THRESHOLD_MARGIN; 
 	DDzDr1Dr1 = -DzDr1 / r1;
	CHISQFL Dpcost_rcoreDr1 = Dpcost_rcoreDz * DzDr1;
	CHISQFL DDpcost_rcoreDr1Dr1 = DDpcost_rcoreDzDz * pow(DzDr1, 2.0) + Dpcost_rcoreDz * DDzDr1Dr1; 	 
	pcost += pcost_rcore;
	DpcostDr1 += Dpcost_rcoreDr1;
	DDpcostDr1Dr1 += DDpcost_rcoreDr1Dr1;	
}

CHISQFL ab1 = (CHISQFL) 1.0 / sqrt((CHISQFL) 1.0 - e1 * e1);

if (ab1 > (CHISQFL) PCOST_AB_LOWER_THRESHOLD) {

	CHISQFL Dab1De1 = e1 * pow(ab1, 3.0);
	CHISQFL DDab1De1De1 = pow(ab1, 3.0) + e1 * Dab1De1 * 3.0 * pow(ab1, 2.0);

	z = log(ab1 / (CHISQFL) PCOST_AB_LOWER_THRESHOLD) / (CHISQFL) LNPCOST_AB_MARGIN;
	CHISQFL DzDab1 = (CHISQFL) 1.0 / ab1 / (CHISQFL) LNPCOST_AB_MARGIN;
	CHISQFL DDzDab1Dab1 = - DzDab1 / ab1;
	CHISQFL DzDe1 = DzDab1 * Dab1De1;
	CHISQFL DDzDe1De1 = DDzDab1Dab1 * pow(Dab1De1, 2.0) + DzDab1 * DDab1De1De1;
	CHISQFL pcost1 = (CHISQFL) CHISQ_PCOST_E * pow(z, PCOST_E_INDEX);
	CHISQFL Dpcost1Dz = (CHISQFL) PCOST_E_INDEX * pcost1 / z;
	CHISQFL DDpcost1DzDz = (CHISQFL) (PCOST_E_INDEX - 1.0) * Dpcost1Dz / z;
	CHISQFL Dpcost1De1 = Dpcost1Dz * DzDe1;
	CHISQFL DDpcost1De1De1 = DDpcost1DzDz * pow(DzDe1, 2.0) + Dpcost1Dz * DDzDe1De1;

	pcost += pcost1;
	DpcostDe1 = Dpcost1De1;
	DDpcostDe1De1 = DDpcost1De1De1; 

	CHISQFL Dpcost1DE1 = Dpcost1De1 * De1DE1;
	CHISQFL DDpcost1DE1DE1 = DDpcost1De1De1 * pow(De1DE1, 2.0) + Dpcost1De1 * DDe1DE1DE1;
	DpcostDE1 = Dpcost1DE1;
	DDpcostDE1DE1 = DDpcost1DE1DE1;
}

if (e1 < (CHISQFL) 0.0) {

	z = e1 / (CHISQFL) PCOST_E;
	CHISQFL DzDe1 = (CHISQFL) 1.0 / (CHISQFL) PCOST_E;
	CHISQFL DDzDe1De1 = 0.0;
	CHISQFL pcost1 = (CHISQFL) CHISQ_PCOST_E * pow(z, PCOST_E_INDEX);
	CHISQFL Dpcost1Dz = (CHISQFL) PCOST_E_INDEX * pcost1 / z;
	CHISQFL DDpcost1DzDz = (CHISQFL) (PCOST_E_INDEX - 1.0) * Dpcost1Dz / z;
	CHISQFL Dpcost1De1 = Dpcost1Dz * DzDe1;
	CHISQFL DDpcost1De1De1 = DDpcost1DzDz * pow(DzDe1, 2.0) + Dpcost1Dz * DDzDe1De1;

	pcost += pcost1;
	DpcostDe1 += Dpcost1De1;
	DDpcostDe1De1 += DDpcost1De1De1; 

	CHISQFL Dpcost1DE1 = Dpcost1De1 * De1DE1;
	CHISQFL DDpcost1DE1DE1 = DDpcost1De1De1 * pow(De1DE1, 2.0) + Dpcost1De1 * DDe1DE1DE1;
	DpcostDE1 += Dpcost1DE1;
	DDpcostDE1DE1 += DDpcost1DE1DE1;
}

pcost_all->r1 = r1;
pcost_all->r2 = CHISQFLNAN;
pcost_all->pcost = pcost;
pcost_all->DpcostDhalo = CHISQFLNAN;
pcost_all->DpcostDcore = DpcostDr1;
pcost_all->DDpcostDhaloDhalo = CHISQFLNAN;
pcost_all->DDpcostDcoreDcore = DDpcostDr1Dr1;
pcost_all->DDpcostDhaloDcore = CHISQFLNAN;

pcost_all->e1 = e1;
pcost_all->e2 = CHISQFLNAN;
pcost_all->DpcostDecore = DpcostDe1;
pcost_all->DpcostDehalo = CHISQFLNAN;
pcost_all->DDpcostDehaloDecore = 0.0;
pcost_all->DDpcostDecoreDecore = DDpcostDe1De1;
pcost_all->DDpcostDehaloDehalo = CHISQFLNAN;

pcost_all->E1 = E1;
pcost_all->E2 = CHISQFLNAN;
pcost_all->DpcostDEcore = DpcostDE1;
pcost_all->DpcostDEhalo = CHISQFLNAN;
pcost_all->DDpcostDEhaloDEcore = 0.0;
pcost_all->DDpcostDEcoreDEcore = DDpcostDE1DE1;
pcost_all->DDpcostDEhaloDEhalo = CHISQFLNAN;

return(SH_SUCCESS);
}

RET_CODE cDeV_pcost(THOBJC *objc, CHISQFL *pcost) {
char *name = "cDeV_pcost";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status;
status = cDeV_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->pcost;

#if DEBUG_PCOST
float r1 = (float) wpcost_all->r1;
float r2 = (float) wpcost_all->r2;
float e1 = (float) wpcost_all->e1;
float e2 = (float) wpcost_all->e2;
float E1 = (float) wpcost_all->E1;
float E2 = (float) wpcost_all->E2;
float u1 = (float) wpcost_all->u1;
float u2 = (float) wpcost_all->u2;

printf("%s: (1:c, 2:h) r1 = %20.10g, r2 = %20.10g, u1 = %20.10g, u2 = %20.10g, e1 = %20.10g, e2 = %20.10g, E1 = %20.10g, E2 = %20.10g, pcost = %20.10g \n", name, r1, r2, u1, u2, e1, e2, E1, E2, (float) wpcost_all->pcost);
#endif

return(SH_SUCCESS);
}

RET_CODE cDeV_DpcostDcore(THOBJC *objc, CHISQFL *pcost) {
char *name = "cDeV_DpcostDcore";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cDeV_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DpcostDcore;
return(SH_SUCCESS);
}

RET_CODE cDeV_DDpcostDcoreDcore(THOBJC *objc, CHISQFL *pcost) {
char *name = "cDeV_DDpcostDcoreDcore";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cDeV_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DDpcostDcoreDcore;
return(SH_SUCCESS);
}

RET_CODE cDeV_DpcostDucore(THOBJC *objc, CHISQFL *pcost) {
char *name = "cDeV_DpcostDucore";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cDeV_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DpcostDucore;
return(SH_SUCCESS);
}

RET_CODE cDeV_DDpcostDucoreDucore(THOBJC *objc, CHISQFL *pcost) {
char *name = "cDeV_DDpcostDucoreDucore";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cDeV_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DDpcostDucoreDucore;
return(SH_SUCCESS);
}

RET_CODE cDeV_DpcostDecore(THOBJC *objc, CHISQFL *pcost) {
char *name = "cDeV_DpcostDecore";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cDeV_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DpcostDecore;
return(SH_SUCCESS);
}

RET_CODE cDeV_DDpcostDecoreDecore(THOBJC *objc, CHISQFL *pcost) {
char *name = "cDeV_DDpcostDecoreDecore";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cDeV_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DDpcostDecoreDecore;
return(SH_SUCCESS);
}

RET_CODE cDeV_DDpcostDEcoreDEcore(THOBJC *objc, CHISQFL *pcost) {
char *name = "cD_DDpcostDEcoreDEcore";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cDeV_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DDpcostDEcoreDEcore;
return(SH_SUCCESS);
}

RET_CODE cDeV_DpcostDEcore(THOBJC *objc, CHISQFL *pcost) {
char *name = "cDeV_DpcostDEcore";
shAssert(pcost != NULL);
if (wpcost_all == NULL) {
	thPCostInit();
}
shAssert(wpcost_all != NULL);
RET_CODE status = cDeV_pcost_generic(objc, wpcost_all);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run generic pcost calculate on objc (%x)", name, objc);
	return(status);
}
*pcost = wpcost_all->DpcostDEcore;
return(SH_SUCCESS);
}



