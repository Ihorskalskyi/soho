#ifndef THCALIB_H
#define THCALIB_H

#include "thCalibTypes.h"
#include "thObjcTypes.h"
#include "thProbability.h"

RET_CODE thPhpropsCalibrate(PHPROPS *p, MAGUNIT magUnit);

RET_CODE thBandGetFromName(char bname, int *band);
RET_CODE thNameGetFromBand(int band, char *bname);
RET_CODE thBandGetFromCamrow(int camrow, int *band);
RET_CODE thCamrowGetFromBand(int band, int *camrow);

RET_CODE thCrudeCalibInsertBValue(CRUDE_CALIB *cc);

RET_CODE thCrudeCalibVariable(CRUDE_CALIB *cc, void *x, TYPE type);
RET_CODE thCrudeCalibGetMag(CRUDE_CALIB *cc, THPIX counts, THPIX *mag);
RET_CODE thCrudeCalibGetMagErr(CRUDE_CALIB *cc, THPIX counts, THPIX mag, THPIX countsErr, THPIX *magErr);

RET_CODE thCrudeCalibWObjcIo(CHAIN *ccs, WOBJC_IO *objc, CALIB_WOBJC_IO *cobjc);

RET_CODE thCrudeCalibWObjcIoChain(CHAIN *ccs, CHAIN *objc, CHAIN*cobjc);

RET_CODE thMuFromMagR(float mag, float magErr, float r, float rErr, float *mu, float *muErr);
RET_CODE thMuFromMagTheta(float mag, float magErr, float r, float rErr, float *mu, float *muErr);

void regress(MATHFL **x, MATHFL *y, MATHFL *beta, int n, int p, REGRESSION_INFO *info);
void regress_with_const(MATHFL **x, MATHFL *y, MATHFL *beta, int n, int p, REGRESSION_INFO *info);
void regress_nonlinear_with_pos(MATHFL **pos, MATHFL *y, MATHFL *beta, int n, int deg, int *df, REGRESSION_INFO *info);

RET_CODE thCalibPhObjcIoGetFieldInfo(CALIB_PHOBJC_IO *objc, int *run, char *rerun, int *camcol, int *field);

void regress_extinction(CHAIN *objcs, MATHFL **beta, int deg, int *df, REGRESSION_INFO **infos);
CHAIN *thGalacticCalibMakeFromObjcs(CHAIN *objcs, int deg);
RET_CODE thGCChainGetExtinction(CHAIN *gcs, float rowc, float colc, float *extinction);
RET_CODE thCalibPhObjcIoFromGC(CALIB_PHOBJC_IO *cphobjc, CALIB_WOBJC_IO *cwphobjc, WOBJC_IO *phobjc, CHAIN *gcs);

#endif
