#include "thDefTypes.h"

OBJCDEF *thObjcdefNew() {

  int i;
  OBJCDEF *def;
  def = (OBJCDEF *) thCalloc(1, sizeof(OBJCDEF));
  
  def->name = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  def->parent = (char **) thCalloc(MX_STRING_ARR, sizeof(char));
  for (i = 0; i < MX_STRING_ARR; i++) {
    def->parent[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }

  def->phId = ~0;
  def->comment = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  
  def->conditions = (char **) thCalloc(MX_STRING_ARR, sizeof(char));
  for (i = 0; i < MX_STRING_ARR; i++) {
    def->conditions[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }
  
  def->sentences = (SENTENCE **) thCalloc(MX_STRING_ARR, sizeof(SENTENCE *));
  return(def);
}

void thObjcdefDel(OBJCDEF *def) {
  
  char *name = "thObjcdefDel";
  int i;

  if (def == NULL) return;

  if (def->name != NULL) thFree(def->name);
  if (def->parent != NULL) {
    for (i = 0; i < MX_STRING_ARR; i++) {
      if (def->parent[i] != NULL) thFree(def->parent[i]);
    }
    thFree(def->parent);
  }
  
  thFree(def->comment);
  if (def->conditions != NULL) {
    for (i = 0; i < MX_STRING_ARR; i++) {
      if (def->conditions[i] != NULL) thFree(def->conditions[i]);
    }
    thFree(def->conditions);
  }
   
  if (def->sentences != NULL) thFree(def->sentences);

  thFree(def);
  return;

}

MASKDEF *thMaskdefNew () {

  char *name = "thMaskdefNew";

  int i;
  MASKDEF* def;
  def = (MASKDEF *) thCalloc(1, sizeof(MASKDEF));

  def->objctype = (char **) thCalloc(MX_STRING_ARR, sizeof(char *));
  for (i = 0; i < MX_STRING_ARR; i++) {
    def->objctype[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }
  
  def->phMaskbit = 0;
  def->phFlag = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  def->unit = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  def->arithmetic = (char *) thCalloc(MX_STRING_LEN, sizeof(char));

  return(def);
}

void thMaskdefDel (MASKDEF* def) {
  
  char *name = "thMaskdefDel";
  int i;

  if (def == NULL) return;

  if (def->objctype != NULL) {
    for (i = 0; i < MX_STRING_ARR; i++) {
      thFree(def->objctype);
    }
    thFree(def->objctype);
  }

  if (def->phFlag != NULL) thFree(def->phFlag);
  if (def->unit != NULL) thFree(def->unit);
  if (def->arithmetic != NULL) thFree(def->arithmetic);

  thFree(def);
  return;

}

SENTENCE *thSentenceNew () {

  
  SENTENCE *sen;
  sen = (SENTENCE *) thCalloc(1, sizeof(SENTENCE));
  sen->thId = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  sen->comment = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  sen->operator = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  sen->tolerance = (THPIX) 0.0;

  return(sen);
}

void thSentenceDel(SENTENCE *sen) {

  if (sen == NULL) return;

  if (sen->thId != NULL) thFree(sen->thId);
  if (sen->comment != NULL) thFree(sen->comment);
  if (sen->operator != NULL) thFree(sen->operator);

  thFree(sen);
}
			      
