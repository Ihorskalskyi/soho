#ifndef THDUMP_H
#define THDUMP_H

#include "thDumpTypes.h"
#include "thProcTypes.h"
#include "thIo.h"
#include "thMle.h"
#include "thMSky.h"

RET_CODE thDumpInit(DUMP_WRITE_MODE mode);
int thDumpGetFlag();

/* dumpers and all */

RET_CODE thFrameInitLogDump(FRAME *f);
RET_CODE thFrameFiniLogDump(FRAME *f);
RET_CODE thFrameInitSkyDump(FRAME *f);
RET_CODE thFrameFiniSkyDump(FRAME *f);
RET_CODE thFrameInitObjcDump(FRAME *f);
RET_CODE thFrameFiniObjcDump(FRAME *f);

RET_CODE thFrameInitIo(FRAME *f); /* does the objcs ini, log init, ccd info, run info, stamp, file info */
RET_CODE thFrameFiniIo(FRAME *f); /* does the objcs, maps, log fini, sky */

RET_CODE thDumpLog(LOGDUMP *log);
RET_CODE thDumpSky(SKYDUMP *sky);
RET_CODE thDumpObjc(OBJCDUMP *objc);
RET_CODE thFrameDumpIo(FRAME *f, int load_best);
RET_CODE thDumpLM(LMDUMP *lm);
RET_CODE thDumpLP(LPDUMP *lp);
RET_CODE thDumpLRZ(LRZDUMP *lrz);
RET_CODE thDumpPsf(PSFDUMP *dump);

/* initializers and finalizers */
RET_CODE thFrameGetSkyobjc(FRAME *f, THOBJC **objc);
RET_CODE thFrameGetSdssObjc(FRAME *f, CHAIN *sdssobjc, CHAIN *sdssobjc2);
RET_CODE thFrameGetFIObjc(FRAME *f, CHAIN *initobjc);

/* initiators and finilizers */
RET_CODE thFrameInitCcdinfo(FRAME *f, CCDINFO *ccdinfo);
RET_CODE thFrameInitRuninfo(FRAME *f, RUNINFO *runinfo);
RET_CODE thFrameInitAlginfo(FRAME *f, ALGINFO *alginfo);
RET_CODE thFrameInitFieldinfo(FRAME *f, FIELDINFO *finfo);
RET_CODE thFrameInitIoinfo(FRAME *f, IOINFO *ioinfo);
RET_CODE thFrameFiniRuninfo(FRAME *f, RUNINFO *runinfo);
RET_CODE thFrameFiniAlginfo(FRAME *f, ALGINFO *alginfo);
RET_CODE thFrameFiniFieldinfo(FRAME *f, FIELDINFO *finfo);

RET_CODE thLMDumpGetMinChisq(LMDUMP *lmdump, CHISQFL *chisq, int *nstep, int *nstep_all, CHISQFL *pcost);
RET_CODE thFrameOrderIo(FRAME *f);
RET_CODE thFrameRevertObjcDump(FRAME *f);
RET_CODE thFrameRevertFIObjc(FRAME *f, CHAIN *fitobjc);
RET_CODE thFrameAddLprofileLastOfBest(FRAME *f);

RET_CODE thFrameUpdateFitmask(FRAME *f);
#endif

