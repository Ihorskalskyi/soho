#include "thCalib.h"

static char *counts_keyword = "counts";
static char *countsErr_keyword = "countsErr";
static char *mag_keyword = "mag";
static char *magErr_keyword = "magErr";

static RET_CODE calibrate(THPIX count, THPIX countErr, 
			THPIX aterm, THPIX kterm, THPIX airmass, THPIX kdot, int band,  
			MAGUNIT magUnit, 
			THPIX *magnitude, THPIX *magnitudeErr);

static void copy_wobjc_io_onto_calib_wobjc_io (WOBJC_IO *objc, CALIB_WOBJC_IO *cobjc);
static void copy_calib_wobjc_io_onto_calib_phobjc_io (CALIB_WOBJC_IO *s, CALIB_PHOBJC_IO *t);

static MATHFL sumproduct(MATHFL *a, MATHFL *b, int n);

static RET_CODE calibrate(THPIX count, THPIX countErr, 
			THPIX aterm, THPIX kterm, THPIX airmass, THPIX kdot, int band,  
			MAGUNIT magUnit, 
			THPIX *magnitude, THPIX *magnitudeErr) {
char *name = "calibrate";
if (magUnit != LUPTITUDE && magUnit != nMGY) {
	thError("%s: ERROR - magnitude unit not supported");
	return(SH_GENERIC_ERROR);
}


/*
http://data.sdss3.org/datamodel/files/PHOTO_CALIB/RERUN/RUN/nfcalib/calibPhotomGlobal.html

 nmgypercount = aterm × 10 ^ (0.4 × [kterm+kdot× (t-t0)]× airmass) × flat */

/* 
http://www.sdss.org/dr7/algorithms/fluxcal.html#counts2mag

f/f0 = counts/exptime * 10 ^ (0.4*(aa + kk * airmass))
	
	
Pogson

    mag = -2.5 * log10(f/f0)
    error(mag) = 2.5 / ln(10) * error(counts) / counts
    To get the error on the counts, see the note on computing count errors below.

asinh

    mag = -(2.5/ln(10))*[asinh((f/f0)/2b)+ln(b)]
    error(mag) = 2.5 / ln(10) * error(counts)/exptime * 1/2b *
            (10 ^ (0.4*(aa + kk * airmass))) / sqrt(1 + [(f/f0)/2b]2),
*/

double ff0, pow_term, mag, magErr;
pow_term = pow((double) 10.0, (double) (0.4 * (kterm+kdot *EXPTIME) * airmass));
ff0 = (double) count * (double) aterm * pow_term;

if (magUnit == nMGY) {
	mag = - (double) 2.5 * log(ff0) / log((double) 10.0);
	magErr = (double) 2.5 / log((double) 10.0) * (double) countErr / (double) count;
} else if (magUnit == LUPTITUDE) {
	double b;	
	if (band == 0) {
		b = B_U;
	} else if (band == 1) {
		b = B_G;
	} else if (band == 2) {
		b = B_R;
	} else if (band == 3) {
		b = B_I;
	} else if (band == 4) {
		b = B_Z;
	} else {
		thError("%s: ERROR - (b) constant unavailable for (band = %d)", 
			name, band);
		return(SH_GENERIC_ERROR);
	}

	mag = -(double) 2.5 / log((double) 10.0) * (asinh(ff0 / (double) 2.0 / (double) b) + log((double) b));
	magErr = (double) 2.5 / log((double) 10.0) * (double) countErr * (double) aterm / (double) 2.0 / (double) b * pow_term / sqrt((double) 1.0 + pow(ff0 / (double) 2.0 / (double) b, 2));
} else {
	thError("%s: ERROR - unsupported (magUnit)", name);
	return(SH_GENERIC_ERROR);
}

if (magnitude != NULL) *magnitude = (THPIX) mag;
if (magnitudeErr != NULL) *magnitudeErr = (THPIX) magErr;

return(SH_SUCCESS);
}

RET_CODE thPhpropsCalibrate(PHPROPS *p, MAGUNIT magUnit) {
char *name = "thPhpropsCalibrate";
if (p == NULL) {
	thError("%s: WARNING - null input", name);
	return(SH_GENERIC_ERROR);
}
WOBJC_IO *phobjc;
if ((phobjc = p->phObjc) == NULL) {
	thError("%s: ERROR - improperly allocated input (p) 'PHPROPS' - PHOTO properties not available", name);
	return(SH_GENERIC_ERROR);
	}
PHCALIB *calibData;
if ((calibData = p->calibData) == NULL) {
	thError("%s: ERROR - improperly allocated input (p) 'PHPROPS' - PHOTO calibration not available", name);
	return(SH_GENERIC_ERROR);
}
CALIB_WOBJC_IO *cwphobjc;
if ((cwphobjc = p->cwphobjc) == NULL) {
	thError("%s: WARNING - loading calibrated object properties onto (p) 'PHPROPS'", name);
	p->cwphobjc = thCalibWObjcIoNew();
	cwphobjc = p->cwphobjc;
}

copy_wobjc_io_onto_calib_wobjc_io (phobjc, cwphobjc);
cwphobjc->magUnit = magUnit;

/* now loop over all bands */
/*
   WBJC_IO:
 
   float psfCounts[NCOLOR], psfCountsErr[NCOLOR];
   float fiberCounts[NCOLOR], fiberCountsErr[NCOLOR];
   float petroCounts[NCOLOR], petroCountsErr[NCOLOR];

   float counts_deV[NCOLOR], counts_deVErr[NCOLOR];
   float counts_exp[NCOLOR], counts_expErr[NCOLOR];
   float counts_model[NCOLOR], counts_modelErr[NCOLOR];

   CALIB_PHOBJC_IO

   float psfMag[NCOLOR], psfMagErr[NCOLOR];
   float fiberMag[NCOLOR], fiberMagErr[NCOLOR];
   float petroMag[NCOLOR], petroMagErr[NCOLOR];
   float deVMag[NCOLOR], deVMagErr[NCOLOR];	
   float expMag[NCOLOR], expMagErr[NCOLOR];
*/

int i, band;
static char **phobjc_names = NULL;
static char **phobjc_namesErr = NULL;
static char **calib_phobjc_names = NULL;
static char **calib_phobjc_namesErr = NULL;
static int n_names = 0;
static char *name1 = NULL, *name2 = NULL;
static TYPE t1, t2;
static int init = 0;
if (!init) {
 name1 = thCalloc(MX_STRING_LEN, sizeof(char));
 name2 = thCalloc(MX_STRING_LEN, sizeof(char));
#if USE_FNAL_OBJC_IO
 strcpy(name1, "FNAL_OBJC_IO");
#else
 strcpy(name1, "OBJC_IO");
#endif
 strcpy(name2, "CALIB_WOBJC_IO");
 t1 = shTypeGetFromName(name1);
 t2 = shTypeGetFromName(name2);
 if (t1 == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - data type '%s' not known - initiation failed", name, name1);
	return(SH_GENERIC_ERROR);
 }
 if (t2 == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - data type '%s' not known - initiation failed", name, name2);
	return(SH_GENERIC_ERROR);
 }
 n_names = 6;
 phobjc_names = thCalloc(n_names, sizeof(char *));
 phobjc_namesErr = thCalloc(n_names, sizeof(char *));	
 calib_phobjc_names = thCalloc(n_names, sizeof(char *));
 calib_phobjc_namesErr = thCalloc(n_names, sizeof(char *));
 for (i = 0; i < n_names; i++) {
	phobjc_names[i] = thCalloc(MX_STRING_LEN, sizeof(char));
	phobjc_namesErr[i] = thCalloc(MX_STRING_LEN, sizeof(char));
	calib_phobjc_names[i] = thCalloc(MX_STRING_LEN, sizeof(char));
	calib_phobjc_namesErr[i] = thCalloc(MX_STRING_LEN, sizeof(char));
 } 
 
	i = 0;
   	strcpy(phobjc_names[i], "psfCounts");
   	strcpy(phobjc_namesErr[i], "psfCountsErr");
   	strcpy(calib_phobjc_names[i], "psfMag");
   	strcpy(calib_phobjc_namesErr[i], "psfMagErr");
	i++;

   	strcpy(phobjc_names[i], "fiberCounts");
   	strcpy(phobjc_namesErr[i], "fiberCountsErr");
	strcpy(calib_phobjc_names[i], "fiberMag");
   	strcpy(calib_phobjc_namesErr[i],  "fiberMagErr");
	i++;

   	strcpy(phobjc_names[i], "petroCounts");
   	strcpy(phobjc_namesErr[i], "petroCountsErr");
	strcpy(calib_phobjc_names[i], "petroMag");
	strcpy(calib_phobjc_namesErr[i], "petroMagErr");
	i++;

   	strcpy(phobjc_names[i], "counts_deV");
   	strcpy(phobjc_namesErr[i], "counts_deVErr");
	strcpy(calib_phobjc_names[i], "deVMag");
	strcpy(calib_phobjc_namesErr[i], "deVMagErr");	
	i++;

    	strcpy(phobjc_names[i], "counts_exp");
	strcpy(phobjc_namesErr[i], "counts_expErr");
  	strcpy(calib_phobjc_names[i], "expMag");
	strcpy(calib_phobjc_namesErr[i], "expMagErr");
	i++;

    	strcpy(phobjc_names[i], "counts_model");
	strcpy(phobjc_namesErr[i], "counts_modelErr");
        strcpy(calib_phobjc_names[i], "modelMag");
	strcpy(calib_phobjc_namesErr[i],  "modelMagErr");
	i++;
	if (i != n_names) {
		thError("%s: ERROR - source code error during initiation of the function", name);
		return(SH_GENERIC_ERROR);
	}
	init = 1;
}
SCHEMA_ELEM *se;

THPIX *count, *countErr, *magnitude, *magnitudeErr;
THPIX kterm, aterm, airmass, kdot;
RET_CODE status;
for (i = 0; i < n_names; i++) {

	se = shSchemaElemGetFromType(t1, phobjc_names[i]);
	if (se == NULL) {
		thError("%s: WARNING - '%s' is not available in '%s' - cannot calibrate this quantity", 
			name, phobjc_names[i], name1);
		count = NULL;
	} else {
		count = (THPIX *) shElemGet(phobjc, se, NULL);
	}
	se = shSchemaElemGetFromType(t1, phobjc_namesErr[i]);
	if (se == NULL) {
		thError("%s: WARNING - '%s' is not available in '%s' - cannot calibrate this quantity", 
			name, phobjc_namesErr[i], name1);
		countErr = NULL;
	} else {
		countErr = (THPIX *) shElemGet(phobjc, se, NULL);
	}
	se = shSchemaElemGetFromType(t2, calib_phobjc_names[i]);
	if (se == NULL) {
		thError("%s: WARNING - '%s' is not available in '%s' - cannot calibrate this quantity", 
		name, calib_phobjc_names[i], name2);
		magnitude = NULL;
	} else {
		magnitude = (THPIX *) shElemGet(cwphobjc, se, NULL);
	}
	se = shSchemaElemGetFromType(t2, calib_phobjc_namesErr[i]);
	if (se == NULL) {
		thError("%s: WARNING - '%s' is not available in '%s' - cannot calibrate this quantity", 
		name, calib_phobjc_namesErr[i], name2);
		magnitudeErr = NULL;
	} else {
		magnitudeErr = (THPIX *) shElemGet(cwphobjc, se, NULL);
	}
	if (count != NULL && countErr != NULL) {

		for (band = 0; band < NCOLOR; band++) {
			kterm = calibData->kterm[band];
			aterm = calibData->aterm[band];
			airmass = calibData->airmass[band];
			kdot = calibData->kdot[band];
	
			/* various model counts */
			status = calibrate(count[band], countErr[band], 
					aterm, kterm, airmass, kdot, band, magUnit, 
					magnitude + band, magnitudeErr + band);	
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not calculate calibrated quantities for '%s' in (band = %d)", 
				name, phobjc_names[i], band);
				return(status);
			}
		
		}
	}
}

/* now copy other records */
/* this function doesn't copy other records */

return(SH_SUCCESS);
}

RET_CODE thBandGetFromName(char bname, int *band) {
char *name = "thBandGetFromName";

if (band == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}

switch (bname) {
	case 'u':
		*band = U_BAND;
		break;
	case 'g': 
		*band = G_BAND;
		break;
	case 'r':
		*band = R_BAND;
		break;
	case 'i':
		*band = I_BAND;
		break;
	case 'z':
		*band = Z_BAND;
		break;
	default:
		*band = UNKNOWN_BAND;
		if (bname != '\000') {
			thError("%s: ERROR - unsupported band ('%c')", name, bname);
		} else {
			thError("%s: ERROR - unidentified band ('')", name);
		}
		return(SH_GENERIC_ERROR);
	}

return(SH_SUCCESS);
}

RET_CODE thNameGetFromBand(int band, char *bname) {
char *name = "thNameGetFromBand";

if (bname == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}

switch (band) {
	case U_BAND:
		*bname = 'u';
		break;
	case G_BAND: 
		*bname = 'g';
		break;
	case R_BAND:
		*bname = 'r';
		break;
	case I_BAND:
		*bname = 'i';
		break;
	case Z_BAND:
		*bname = 'z';
		break;
	default:
		*bname = '\0';
		thError("%s: ERROR - unsupported band ('%d')", name, band);
		return(SH_GENERIC_ERROR);
	}

return(SH_SUCCESS);
}


RET_CODE thBandGetFromCamrow(int camrow, int *band) {
char *name = "thBandGetFromCamrow";
if (band == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}

switch (camrow) {
	case U_ROW:
		*band = U_BAND;
		break;
	case G_ROW: 
		*band = G_BAND;
		break;
	case R_ROW:
		*band = R_BAND;
		break;
	case I_ROW:
		*band = I_BAND;
		break;
	case Z_ROW:
		*band = Z_BAND;
		break;
	default:
		*band = UNKNOWN_BAND;
		thError("%s: ERROR - unsupported camrow ('%d')", name, camrow);
		return(SH_GENERIC_ERROR);
	}

return(SH_SUCCESS);
}

RET_CODE thCamrowGetFromBand(int band, int *camrow) {
char *name = "thCamrowGetFromBand";

if (camrow == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}

switch (band) {
	case U_BAND:
		*camrow = U_ROW;
		break;
	case G_BAND: 
		*camrow = G_ROW;
		break;
	case R_BAND:
		*camrow = R_ROW;
		break;
	case I_BAND:
		*camrow = I_ROW;
		break;
	case Z_BAND:
		*camrow = Z_ROW;
		break;
	default:
		*camrow = UNKNOWN_CAMROW;
		thError("%s: ERROR - unsupported band ('%d')", name, band);
		return(SH_GENERIC_ERROR);
	}

return(SH_SUCCESS);
}

RET_CODE thCrudeCalibInsertBValue(CRUDE_CALIB *cc) {
char *name = "thCrudeCalibInsertBValue";
if (cc == NULL) {
	thError("%s: ERROR - null input and output placeholder", name);
	return(SH_GENERIC_ERROR);
}
switch (cc->filter) {
	case 'u':
		cc->b = (float) B_U;
		break;
	case 'g':
		cc->b = (float) B_G;
		break;
	case 'r':
		cc->b = (float) B_R;
		break;
	case 'i':
		cc->b = (float) B_I;
		break;
	case 'z':
		cc->b = (float) B_Z;
		break;
	default:
		thError("%s: ERROR - unsupported filter '%s'", name, cc->filter);
		return(SH_GENERIC_ERROR);
		break;
	}
return(SH_SUCCESS);
}

RET_CODE thCrudeCalibVariable(CRUDE_CALIB *cc, void *x, TYPE type) {
char *name = "thCrudeCalibVariable";
if (cc == NULL) {
	thError("%s: ERROR - null (crude_calib)", name);
	return(SH_GENERIC_ERROR);
}
if (x == NULL) {
	thError("%s: ERROR - null variable to calibrate", name);
	return(SH_GENERIC_ERROR);
}
if (type == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - unknown variable type", name);
	return(SH_GENERIC_ERROR);
}
char *vname = shNameGetFromType(type);
/* get objects and io structures */
SCHEMA *s = shSchemaGetFromType(type);
if (s == NULL) {
	thError("%s: ERROR - variable type not supported by a schema", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
int nelem = s->nelem;
int i;
for (i = 0; i < nelem; i++) {

SCHEMA_ELEM *se = s->elems + i;
char *ename = se->name;
char *locOfCname = strstr(ename, counts_keyword);
char *locOfCErrName = strstr(ename, countsErr_keyword);
if (ename != NULL && locOfCname == ename && locOfCErrName == NULL) { /* enames is a counts keyword */

	char tname1[MX_STRING_LEN], tname2[MX_STRING_LEN];
	SCHEMA_ELEM *se1 = NULL, *se2 = NULL;
	memset(tname1, '\0', MX_STRING_LEN);
	strcpy(tname1, countsErr_keyword);
	if (strlen(ename) > strlen(counts_keyword)) {
		strcpy(tname1 + strlen(countsErr_keyword), ename + strlen(counts_keyword));
	}
	se2 = shSchemaElemGetFromType(type, tname1);
	THPIX *counts = shElemGet(x, se, NULL);	
	char *cname = se->name;
	#if DO_STD_ERRORS
	THPIX *countsErr = shElemGet(x, se2, NULL);
	char *cErrname = se2->name;
	#endif

	memset(tname1, '\0', MX_STRING_LEN);
	memset(tname2, '\0', MX_STRING_LEN);
	strcpy(tname1, mag_keyword);
	if (strlen(ename) > strlen(counts_keyword)) {
		strcpy(tname1 + strlen(mag_keyword), ename + strlen(counts_keyword));
	}
	strcpy(tname2, magErr_keyword);
	if (strlen(ename) > strlen(countsErr_keyword)) {
		strcpy(tname2 + strlen(magErr_keyword), ename + strlen(countsErr_keyword));
	}
	se1 = shSchemaElemGetFromType(type, tname1);
	se2 = shSchemaElemGetFromType(type, tname2);

	THPIX *mag = shElemGet(x, se1, NULL);	
	char *magname = se1->name;
	#if DO_STD_ERRORS
	THPIX *magErr = shElemGet(x, se2, NULL);
	char *magErrname = se2->name;
	#endif
	/* see if is a counts keywrod */
	if (counts != NULL && mag != NULL) {
		/* if it does convert it */
		status = thCrudeCalibGetMag(cc, (*counts), mag);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not crude_calib '%s' to '%s' in variable '%s'", name, cname, magname, vname);
			return(status);
		}
		#if DO_STD_ERRORS
		/* now check if error keywords exist */
		if (countsErr != NULL && magErr != NULL) {
			status = thCrudeCalibGetMagErr(cc, (*counts), (*mag), (*countsErr), magErr);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not calculate '%s' from '%s' in variable '%s'", 
					name, cErrname, magErrname, objcid);
				return(status);
			}
		} else if (countsErr == NULL) {
			thError("%s: WARNING - countsErr keyword not found in '%s'", name, ioname);
		} else if (magErr == NULL) {
			thError("%s: WARNING - magErr keyword not found in '%s'", name, ioname);
		}
		/* end of error calculation */
		#endif
	}
}
} /* for loop */
return(SH_SUCCESS);
}


RET_CODE thCrudeCalibGetMag(CRUDE_CALIB *cc, THPIX counts, THPIX *mag) {
char *name = "thCrudeCalibGetMag";
if (cc == NULL) {
	thError("%s: ERROR - null (crude_calib)", name);
	return(SH_GENERIC_ERROR);
}
if (mag == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}

double A = (double) 1.0E8 * (double) cc->flux20;
double ff0 = (double) counts/ A;
double B = (double) 2.0 * (double) cc->b;
double x = ff0 / B;
double C = (double) 2.5 / log((double) 10.0);
double D = log((double) cc->b);

double magdble = -C*(asinh(x)+D); 
*mag = (THPIX) magdble;

return(SH_SUCCESS);
}

RET_CODE thCrudeCalibGetMagErr(CRUDE_CALIB *cc, THPIX counts, THPIX mag, THPIX countsErr, THPIX *magErr) {
char *name = "thCrudeCalibGetMagErr";
if (cc == NULL) {
	thError("%s: ERROR - null (crude_calib)", name);
	return(SH_GENERIC_ERROR);
}
if (magErr == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
double A = ((double) 1.0E8 * (double) cc->flux20);
double ff0 = (double) counts/ A;
double B = (double) 2.0 * (double) cc->b;
double x = ff0 / B;
double C = (double) 2.5 / log((double) 10.0);

double magErrdble = fabs(C / sqrt((double) 1.0 + x * x) / B / A) * (double) countsErr;
*magErr = (THPIX) magErrdble;

return(SH_SUCCESS);
}


RET_CODE thCrudeCalibMagFromCounts(CRUDE_CALIB *cc, float counts, float countsErr, float *mag, float *magErr) {

shAssert(cc != NULL);

double A = (double) 1.0E8 * (double) cc->flux20;
double ff0 = (double) counts/ A;
double B = (double) 2.0 * (double) cc->b;
double x = ff0 / B;
double C = (double) 2.5 / log((double) 10.0);
double D = log((double) cc->b);

double magdble = -C*(asinh(x)+D); 
double magErrdble = fabs(C / sqrt((double) 1.0 + x * x) / B / A) * (double) countsErr;

if (counts == (float) VALUE_IS_BAD) {
	magdble = (double) VALUE_IS_BAD;
	magErrdble = (double) VALUE_IS_BAD;
}
if (countsErr == (float) VALUE_IS_BAD) magErrdble = (double) VALUE_IS_BAD;

if (mag != NULL) *mag = (float) magdble;
if (magErr != NULL) *magErr = (float) magErrdble;

return(SH_SUCCESS);
}


RET_CODE thMuFromMagR(float mag, float magErr, float r, float rErr, float *mu, float *muErr) {
/*
 * mu = mag + 2.5 log10(2 pi R ^ 2)
 */

if (r == (float) 0.0) r = VALUE_IS_BAD;
if (mag == VALUE_IS_BAD || r == VALUE_IS_BAD ) {
	if (mu != NULL) *mu = VALUE_IS_BAD;
	if (muErr != NULL) *muErr = VALUE_IS_BAD;
	return(SH_SUCCESS);
}

double rdble = r * PIXARCSEC;
double rErrdble = rErr * PIXARCSEC;

double mudble = (double) mag + 2.5 * log10((double) 2.0 * PI * pow(rdble, 2.0));
double muErrdble = pow((double) magErr, 2.0) + 25.0 * pow(rErrdble / rdble, 2.0);
muErrdble = sqrt(muErrdble);

if (magErr == VALUE_IS_BAD || rErr == VALUE_IS_BAD) muErrdble = (double) VALUE_IS_BAD;
if (mu != NULL) *mu = (float) mudble;
if (muErr != NULL) *muErr = (float) muErrdble;

return(SH_SUCCESS);
}

RET_CODE thMuFromMagTheta(float mag, float magErr, float r, float rErr, float *mu, float *muErr) {
/*
 * mu = mag + 2.5 log10(2 pi R ^ 2)
 */

if (r == (float) 0.0) r = (float) VALUE_IS_BAD;
if (mag == VALUE_IS_BAD || r == VALUE_IS_BAD ) {
	if (mu != NULL) *mu = VALUE_IS_BAD;
	if (muErr != NULL) *muErr = VALUE_IS_BAD;
	return(SH_SUCCESS);
}

double rdble = (double) r;
double rErrdble = (double) rErr;

double mudble = (double) VALUE_IS_BAD, muErrdble = (double) VALUE_IS_BAD;

if (r != VALUE_IS_BAD && mag != VALUE_IS_BAD) {
	mudble  = (double) mag + 2.5 * log10((double) 2.0 * PI * pow(rdble, 2.0));
	if (rErr != VALUE_IS_BAD && magErr != VALUE_IS_BAD) {
	muErrdble = pow((double) magErr, 2.0) + 25.0 * pow(rErrdble / rdble, 2.0);
	muErrdble = sqrt(muErrdble);
	}
}

if (mu != NULL) *mu = (float) mudble;
if (muErr != NULL) *muErr = (float) muErrdble;

return(SH_SUCCESS);
}



void copy_wobjc_io_onto_calib_wobjc_io (WOBJC_IO *objc, CALIB_WOBJC_IO *cobjc) {
shAssert(objc != NULL);
shAssert(cobjc != NULL);

cobjc->id = objc->id;
cobjc->parent = objc->parent;
cobjc->ncolor = objc->ncolor;
cobjc->nchild = objc->nchild;
cobjc->objc_type = objc->objc_type;
cobjc->catID = objc->catID;
cobjc->objc_flags = objc->objc_flags;
cobjc->objc_rowc = objc->objc_rowc;
cobjc->objc_colc = objc->objc_colc;
cobjc->objc_rowcErr = objc->objc_rowcErr;
cobjc->objc_colcErr = objc->objc_colcErr;
cobjc->rowv = objc->rowv;
cobjc->colv = objc->colv;
cobjc->rowvErr = objc->rowvErr;
cobjc->colvErr = objc->colvErr;

memcpy(cobjc->rowc, objc->rowc, NCOLOR * sizeof(float));
memcpy(cobjc->colc, objc->colc, NCOLOR * sizeof(float));
memcpy(cobjc->sky, objc->sky, NCOLOR * sizeof(float));

memcpy(cobjc->rowcErr, objc->rowcErr, NCOLOR * sizeof(float));
memcpy(cobjc->colcErr, objc->colcErr, NCOLOR * sizeof(float));
memcpy(cobjc->skyErr, objc->skyErr, NCOLOR * sizeof(float));

memcpy(cobjc->petroRad, objc->petroRad, NCOLOR * sizeof(float));
memcpy(cobjc->petroR50, objc->petroR50, NCOLOR * sizeof(float));
memcpy(cobjc->petroR90, objc->petroR90, NCOLOR * sizeof(float));
memcpy(cobjc->petroCounts, objc->petroCounts, NCOLOR * sizeof(float));

memcpy(cobjc->petroRadErr, objc->petroRadErr, NCOLOR * sizeof(float));
memcpy(cobjc->petroR50Err, objc->petroR50Err, NCOLOR * sizeof(float));
memcpy(cobjc->petroR90Err, objc->petroR90Err, NCOLOR * sizeof(float));
memcpy(cobjc->petroCountsErr, objc->petroCountsErr, NCOLOR * sizeof(float));

memcpy(cobjc->r_deV, objc->r_deV, NCOLOR * sizeof(float));
memcpy(cobjc->ab_deV, objc->ab_deV, NCOLOR * sizeof(float));
memcpy(cobjc->phi_deV, objc->phi_deV, NCOLOR * sizeof(float));
memcpy(cobjc->counts_deV, objc->counts_deV, NCOLOR * sizeof(float));

memcpy(cobjc->r_deVErr, objc->r_deVErr, NCOLOR * sizeof(float));
memcpy(cobjc->ab_deVErr, objc->ab_deVErr, NCOLOR * sizeof(float));
memcpy(cobjc->phi_deVErr, objc->phi_deVErr, NCOLOR * sizeof(float));
memcpy(cobjc->counts_deVErr, objc->counts_deVErr, NCOLOR * sizeof(float));

memcpy(cobjc->r_exp, objc->r_exp, NCOLOR * sizeof(float));
memcpy(cobjc->ab_exp, objc->ab_exp, NCOLOR * sizeof(float));
memcpy(cobjc->phi_exp, objc->phi_exp, NCOLOR * sizeof(float));
memcpy(cobjc->counts_exp, objc->counts_exp, NCOLOR * sizeof(float));

memcpy(cobjc->r_expErr, objc->r_expErr, NCOLOR * sizeof(float));
memcpy(cobjc->ab_expErr, objc->ab_expErr, NCOLOR * sizeof(float));
memcpy(cobjc->phi_expErr, objc->phi_expErr, NCOLOR * sizeof(float));
memcpy(cobjc->counts_expErr, objc->counts_expErr, NCOLOR * sizeof(float));

memcpy(cobjc->counts_model, objc->counts_model, NCOLOR * sizeof(float));
memcpy(cobjc->counts_modelErr, objc->counts_modelErr, NCOLOR * sizeof(float));

memcpy(cobjc->star_L, objc->star_L, NCOLOR * sizeof(float));
memcpy(cobjc->exp_L, objc->exp_L, NCOLOR * sizeof(float));
memcpy(cobjc->deV_L, objc->deV_L, NCOLOR * sizeof(float));

memcpy(cobjc->star_lnL, objc->star_lnL, NCOLOR * sizeof(float));
memcpy(cobjc->exp_lnL, objc->exp_lnL, NCOLOR * sizeof(float));
memcpy(cobjc->deV_lnL, objc->deV_lnL, NCOLOR * sizeof(float));
memcpy(cobjc->fracPSF, objc->fracPSF, NCOLOR * sizeof(float));

memcpy(cobjc->flags, objc->flags, NCOLOR * sizeof(int));
memcpy(cobjc->flags2, objc->flags2, NCOLOR * sizeof(int));

cobjc->source = objc;

return;
}

RET_CODE thCrudeCalibWObjcIo(CHAIN *ccs, WOBJC_IO *objc, CALIB_WOBJC_IO *cobjc) {
char *name = "thCrudeCalibWObjcIo";
shAssert(objc != NULL);
shAssert(ccs != NULL);
shAssert(cobjc != NULL);
shAssert(objc->ncolor == NCOLOR);

copy_wobjc_io_onto_calib_wobjc_io (objc, cobjc);

int i;
for (i = 0; i < NCOLOR; i++) {
	RET_CODE status;
	float counts, mag, countsErr, magErr, r, rErr, mu, muErr;
	CRUDE_CALIB *cc = shChainElementGetByPos(ccs, i);

	counts = objc->psfCounts[i];
	countsErr = objc->psfCountsErr[i];
	status = thCrudeCalibMagFromCounts(cc, counts, countsErr, &mag, &magErr);
	shAssert(status == SH_SUCCESS);
	cobjc->psfMag[i] = mag;
	cobjc->psfMagErr[i] = magErr;

	counts = objc->fiberCounts[i];
	countsErr = objc->fiberCountsErr[i];
	status = thCrudeCalibMagFromCounts(cc, counts, countsErr, &mag, &magErr);
	shAssert(status == SH_SUCCESS);
	cobjc->fiberMag[i] = mag;
	cobjc->fiberMagErr[i]= magErr;

#if USE_FNAL_OBJC_IO
	counts = objc->fiber2Counts[i];
	countsErr = objc->fiber2CountsErr[i];
	status = thCrudeCalibMagFromCounts(cc, counts, countsErr, &mag, &magErr);
	shAssert(status == SH_SUCCESS);
	cobjc->fiber2Mag[i] = mag;
	cobjc->fiber2MagErr[i]= magErr;
#endif

	counts = objc->petroCounts[i];
	countsErr = objc->petroCountsErr[i];
	status = thCrudeCalibMagFromCounts(cc, counts, countsErr, &mag, &magErr);
	shAssert(status == SH_SUCCESS);
	cobjc->petroMag[i] = mag;
	cobjc->petroMagErr[i]= magErr;

	r = objc->petroR50[i];
	rErr = objc->petroR50Err[i];
	status = thMuFromMagR(mag, magErr, r, rErr, &mu, &muErr);
	shAssert(status == SH_SUCCESS);
	cobjc->petroMu[i] = mu;
	cobjc->petroMuErr[i] = muErr;
	
	counts = objc->counts_exp[i];
	countsErr = objc->counts_expErr[i];
	status = thCrudeCalibMagFromCounts(cc, counts, countsErr, &mag, &magErr);
	shAssert(status == SH_SUCCESS);
	cobjc->expMag[i] = mag;
	cobjc->expMagErr[i]= magErr;
	
	r = objc->r_exp[i];
	rErr = objc->r_expErr[i];
	status = thMuFromMagR(mag, magErr, r, rErr, &mu, &muErr);
	shAssert(status == SH_SUCCESS);
	cobjc->expMu[i] = mu;
	cobjc->expMuErr[i] = muErr;

	counts = objc->counts_deV[i];
	countsErr = objc->counts_deVErr[i];
	status = thCrudeCalibMagFromCounts(cc, counts, countsErr, &mag, &magErr);
	shAssert(status == SH_SUCCESS);
	cobjc->deVMag[i] = mag;
	cobjc->deVMagErr[i]= magErr;

	r = objc->r_deV[i];
	rErr = objc->r_deVErr[i];
	status = thMuFromMagR(mag, magErr, r, rErr, &mu, &muErr);
	shAssert(status == SH_SUCCESS);
	cobjc->deVMu[i] = mu;
	cobjc->deVMuErr[i] = muErr;

	counts = objc->counts_model[i];
	countsErr = objc->counts_modelErr[i];
	status = thCrudeCalibMagFromCounts(cc, counts, countsErr, &mag, &magErr);
	shAssert(status == SH_SUCCESS);
	cobjc->modelMag[i] = mag;
	cobjc->modelMagErr[i]= magErr;

}

return(SH_SUCCESS);
}



RET_CODE thCrudeCalibWObjcIoChain(CHAIN *ccs, CHAIN *objc, CHAIN*cobjc) {
char *name = "thCrudeCalibWObjcIoChain";
shAssert(objc != NULL && cobjc != NULL);
RET_CODE status;
int i, n;
n = shChainSize(objc);
if (shChainSize(cobjc) != 0) {
	thError("%s: WARNING - unempty calibrated chain passed as output placeholder", name);
	}

for (i = 0; i < n; i++) {
	WOBJC_IO *objc0 = shChainElementGetByPos(objc, i);
	CALIB_WOBJC_IO *cobjc0 = thCalibWObjcIoNew();
	status = thCrudeCalibWObjcIo(ccs, objc0, cobjc0);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not calibrate object found at position (%d) in the input chain", name, i);
		return(status);
	}
	shChainElementAddByPos(cobjc, cobjc0, "CALIB_WOBJC_IO", TAIL, AFTER);
}
return(SH_SUCCESS);
}


MATHFL sumproduct(MATHFL *a, MATHFL *b, int n) {
shAssert(a != NULL);
shAssert(b != NULL);
shAssert(n > 0);
MATHFL res = 0.0;
int i;
for (i = 0; i < n; i++) {
	res += (*a) * (*b);
	a++; b++;
}
return(res);
}

MATHFL sum(MATHFL *a, int n) {
shAssert(a != NULL);
shAssert(n > 0);
MATHFL res = 0.0;
int i;
for (i = 0; i < n; i++) {
	res += (*a);
	a++;
}
return(res);
}


RET_CODE thCalibPhObjcIoGetFieldInfo(CALIB_PHOBJC_IO *objc, int *run, char *rerun, int *camcol, int *field) {
shAssert(objc != NULL);
if (run != NULL) *run = (int) objc->run;
if (rerun != NULL) memcpy(rerun, objc->rerun, 3 * sizeof(char));
if (camcol != NULL) *camcol = (int) objc->camcol;
if (field != NULL) *field = (int) objc->field;
return(SH_SUCCESS);
}

void regress(MATHFL **x, MATHFL *y, MATHFL *beta, int n, int p, REGRESSION_INFO *info) {
/* p is the size of x  vector
 * n is the number of data points
 */
shAssert(sizeof(MATHFL) == sizeof(double));

shAssert(n > 0);
shAssert(p > 0);
shAssert(x != NULL);
shAssert(y != NULL);

MATHFL *xy, **xx;
xy = thCalloc(p, sizeof(MATHFL));
xx = thCalloc(p, sizeof(MATHFL *));
xx[0] = thCalloc(p * p, sizeof(MATHFL));

int j, k;
for (j = 0; j < p; j++) {
	xx[j] = xx[0] + p * j;
}
for (j = 0; j < p; j++) {
	xy[j] = sumproduct(x[j], y, n);
	for (k = 0; k <= j; k++) {
		xx[j][k] = sumproduct(x[j], x[k], n);
		xx[k][j] = xx[j][k];
	}
}

phDMatrixInvert(xx, xy, p);
memcpy(beta, xy, p * sizeof(MATHFL));
thFree(xy);
thFree(xx[0]);
thFree(xx);

if (info != NULL) {
MATHFL **xtrans;
xtrans = thCalloc(n, sizeof(MATHFL *));
xtrans[0] = thCalloc(n * p, sizeof(MATHFL));
int i;
for (i = 0; i < n; i++) {
	xtrans[i] = xtrans[0] + p * i;
	MATHFL *xtrans_i = xtrans[i];
	for (j = 0; j < p; j++) {
		xtrans_i[j] = x[j][i];
	}
}
MATHFL yfit = 0.0, rss = 0.0, ytot = 0.0;
for (i = 0; i < n; i++) {
	MATHFL *xtrans_i = xtrans[i];
	yfit = sumproduct(beta, xtrans_i, p);
	rss += pow((y[i] - yfit), 2.0);
	ytot += y[i];
	}
thFree(xtrans[0]);
thFree(xtrans);

MATHFL s = sqrt(rss / (MATHFL) (n - p));
MATHFL ymean = ytot / (MATHFL) n;
MATHFL tss = 0.0;
for (i = 0; i < n; i++) {
	tss += pow((y[i] - ymean), 2.0);
	}
MATHFL R2 = (MATHFL) 1.0 - rss / tss;
MATHFL R2adj = (MATHFL) 1.0 - (rss / (MATHFL) (n - 1)) / (tss / (MATHFL) (n - p)); 
MATHFL F = ((tss - rss) / (MATHFL) (p - 1)) / (rss / (MATHFL) (n - p));  
int df1 = p - 1, df2 = n - p;
MATHFL pvalue = (MATHFL) 1.0 - (MATHFL) F_Distribution((double) F, (int) df1, (int) df2);

info->s = s;
info->rss = rss;
info->tss = tss;
info->R2 = R2;
info->R2adj = R2adj;
info->F = F;
info->df1 = df1;
info->df2 = df2;
info->pvalue = pvalue;

}

return;
}

void regress_with_const(MATHFL **x, MATHFL *y, MATHFL *beta, int n, int p, REGRESSION_INFO *info) {
shAssert(n > 0);
shAssert(p > 0);
shAssert(x != NULL);
shAssert(y != NULL);
MATHFL **X;
X = thCalloc(p+1, sizeof(MATHFL *));
X[0] = thCalloc((p+1) * n, sizeof(MATHFL));

int i;
for (i = 0; i < p+1; i++) {
	X[i] = X[0] + i * n;
}
MATHFL *X_0 = X[0];
for (i = 0; i < n; i++) {
	X_0[i] = (MATHFL) 1.0;
}
for (i = 0; i < p; i++) {
	MATHFL *X_i = X[i+1];
	memcpy(X_i, x[i], n * sizeof(MATHFL));
}
regress(X, y, beta, n, p+1, info);
thFree(X[0]);
thFree(X);

return;
}

void regress_nonlinear_with_pos(MATHFL **pos, MATHFL *y, MATHFL *beta, int n, int deg, int *df, REGRESSION_INFO *info) {
shAssert(pos != NULL);
shAssert(y != NULL);
shAssert(beta != NULL);
shAssert(n > 0);
shAssert(deg > 0);

int p = (deg * (deg + 3)) / 2;
int d, d1, d2, i, q = 0;
MATHFL **X; 
X = thCalloc(p, sizeof(MATHFL *));
X[0] = thCalloc(p * n, sizeof(MATHFL));
for (i = 0; i < p; i++) {
	X[i] = X[0] + i * n;
}
MATHFL *x1 = pos[0], *x2 = pos[1];
shAssert(x1 != NULL && x2 != NULL);
for (d = 1; d <= deg; d++) {
	for (d1 = 0; d1 <= d; d1++) {
		d2 = d - d1;
		MATHFL *X_q = X[q];
		for (i = 0; i < n; i++) X_q[i] = pow(x1[i], d1) * pow(x2[i], d2);	
		q++;
		}
	}
shAssert(p == q);
regress_with_const(X, y, beta, n, p, info);
if (df != NULL) *df = p + 1;
thFree(X[0]);
thFree(X);
return;
}

void regress_extinction(CHAIN *objcs, MATHFL **beta, int deg, int *df, REGRESSION_INFO **infos) {
shAssert(objcs != NULL);
shAssert(shChainSize(objcs) != 0);
shAssert(beta != NULL);
shAssert(deg > 0);

int n = shChainSize(objcs);
MATHFL **pos;
pos = thCalloc(2, sizeof(MATHFL *));
pos[0] = thCalloc(2 * n, sizeof(MATHFL));
pos[1] = pos[0] + n;
MATHFL *x1 = pos[0], *x2 = pos[1];
int i;
for (i = 0; i < n; i++) {
	CALIB_PHOBJC_IO *objc;
	objc = shChainElementGetByPos(objcs, i);
	x1[i] = (MATHFL) objc->objc_rowc;
	x2[i] = (MATHFL) objc->objc_colc;
}
int band;
REGRESSION_INFO *info;
for (band = 0; band < NCOLOR; band++) {
	MATHFL y[n];
	for (i = 0; i < n; i++) {
		CALIB_PHOBJC_IO *objc = shChainElementGetByPos(objcs, i);
		y[i] = (MATHFL) objc->extinction[band];
	}
	MATHFL *beta_band = beta[band];
	shAssert(beta_band != NULL);
	if (infos != NULL) {
		info = infos[band];
	} else {
		info = NULL;
	}
	regress_nonlinear_with_pos(pos, y, beta_band, n, deg, df, info);
}
thFree(pos[0]);
thFree(pos);

return;
}

CHAIN *thGalacticCalibMakeFromObjcs(CHAIN *objcs, int deg) {
char *name = "thGalacticCalibMakeFromObjcs";
if (objcs == NULL || shChainSize(objcs) == 0) {
	thError("%s: ERROR - null or empty objcs chain", name);
	return(NULL);
}
if (deg <= 0) {
	thError("%s: ERROR expected positive deg, received '%d'", name, deg);
	return(NULL);
}
CALIB_PHOBJC_IO *cphobjc = shChainElementGetByPos(objcs, 0);
int run, camcol, field;
char rerun[3];
memset(rerun, '\0', 3 * sizeof(char));
RET_CODE status = thCalibPhObjcIoGetFieldInfo(cphobjc, &run, rerun, &camcol, &field);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the field information from object", name);
	return(NULL);
}

MATHFL **beta;
beta = thCalloc(NCOLOR, sizeof(MATHFL *));
beta[0] = thCalloc(NCOLOR * NBETA, sizeof(MATHFL));
REGRESSION_INFO **infos = thCalloc(NCOLOR, sizeof(REGRESSION_INFO *));
int i;
for (i = 0; i < NCOLOR; i++) {
	beta[i] = beta[0] + i * NBETA;
	infos[i] = thRegressionInfoNew();
}
int df = 0;
regress_extinction(objcs, beta, deg, &df, infos);
CHAIN *gc_chain = shChainNew("GALACTIC_CALIB");
int band;
for (band = 0; band < NCOLOR; band++) {
	REGRESSION_INFO *info = infos[band];
	GALACTIC_CALIB *gc = thGalacticCalibNew();
	/* field information */	
	gc->field = (int) field;
	gc->run = (int) run;
	memcpy(gc->rerun, rerun, 3 * sizeof(char));
	gc->camcol = (int) camcol;
	/* regression information */
	gc->sigma = (float) info->s;
	gc->R2 = (float) info->R2;
	gc->R2adj = (float) info->R2adj;
	gc->F = (float) info->F;
	gc->rss = (float) info->rss;
	gc->tss = (float) info->tss;
	gc->df1 = info->df1;
	gc->df2 = info->df2;
	gc->pvalue = (float) info->pvalue;

	float *gc_beta = gc->beta; 
	MATHFL *beta_band = beta[band];
	for (i = 0; i < NBETA; i++) {
		gc_beta[i] = (float) beta_band[i];
	}
	gc->band = band;
	char bname = '\0';
	status = thNameGetFromBand(band, &bname);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get band name for (band = %d)", name, band);
		shChainDestroy(gc_chain, &thGalacticCalibDel);
		return(NULL);
	}
	gc->bname = bname;
	gc->df = df;
	gc->poldeg = deg;
	strcpy(gc->pos1, "ROWC");
	strcpy(gc->pos2, "COLC");
	gc->n = shChainSize(objcs);
	shChainElementAddByPos(gc_chain, gc, "GALACTIC_CALIB", TAIL, AFTER);


	thRegressionInfoDel(info);
}

thFree(infos);
thFree(beta[0]);
thFree(beta);
return(gc_chain);
}

RET_CODE thGCGetExtinction(GALACTIC_CALIB *gc, float rowc, float colc, float *extinction) {
char *name = "thGCChainGetExtinction";
shAssert(gc != NULL);
if (extinction == NULL) {
	thError("%s: WARNING - null placeholder for extinction", name);
	return(SH_SUCCESS);
}

int df = gc->df;
MATHFL x1, x2;
char *pos1 = gc->pos1, *pos2 = gc->pos2;
if (strcmp(pos1, "ROWC") == 0 && strcmp(pos2, "COLC") == 0) {
	x1 = (MATHFL) rowc;
	x2 = (MATHFL) colc;
	} else if (strcmp(pos2, "ROWC") == 0 && strcmp(pos1, "COLC") == 0) {
	x1 = (MATHFL) colc;
	x2 = (MATHFL) rowc;
	} else {
	thError("%s: ERROR - unsupported (pos1, pos2) = ('%s', '%s')", name, pos1, pos2);
	return(SH_GENERIC_ERROR);
	}
int d, d1, d2, deg, q = 0;
float *beta = gc->beta; 
MATHFL yfit = 0.0;
deg = gc->poldeg;
for (d = 0; d <= deg; d++) {
	for (d1 = 0; d1 <= d; d1++) {
		shAssert(q < NBETA);
		d2 = d - d1;
		MATHFL X_q = pow(x1, d1) * pow(x2, d2);
		MATHFL beta_q = (MATHFL) beta[q];	
		yfit += X_q * beta_q;
		q++;
	}
}
shAssert(q == df);
*extinction = (float) yfit;
return(SH_SUCCESS);
}


RET_CODE thGCChainGetExtinction(CHAIN *gcs, float rowc, float colc, float *extinction) {
char *name = "thGCChainGetExtinction";
shAssert(gcs != NULL);
shAssert(extinction != NULL);
shAssert(shChainSize(gcs) == NCOLOR);
int i;
for (i = 0; i < NCOLOR; i++) {
	GALACTIC_CALIB *gc = shChainElementGetByPos(gcs, i);
	shAssert(gc != NULL);
	RET_CODE status = thGCGetExtinction(gc, rowc, colc, extinction + i);
	if (status != SH_SUCCESS) {
		int band = gc->band;
		char cband = gc->bname;
		thError("%s: ERROR - could not get extinction value for (band = %d = '%c')", name, band, cband);
		return(status);
	}
}

return(SH_SUCCESS);
}

void copy_calib_wobjc_io_onto_calib_phobjc_io (CALIB_WOBJC_IO *s, CALIB_PHOBJC_IO *t) {
shAssert(s != NULL);
shAssert(t != NULL);

t->id = s->id;
t->parent = s->parent;
t->nchild = s->nchild;
t->objc_type = s->objc_type;
t->objc_flags = s->objc_flags;
t->objc_flags2 = s->objc_flags2;
t->objc_rowc = s->objc_rowc;
t->objc_colc = s->objc_colc;
t->objc_rowcErr = s->objc_rowcErr;
t->objc_colcErr = s->objc_colcErr;
/* fix the following to degrees */
t->rowvDeg = s->rowv * (float) PIXARCSEC; 
t->colvDeg = s->colv * (float) PIXARCSEC;
t->rowvDegErr = s->rowvErr * (float) PIXARCSEC;
t->colvDegErr = s->colvErr * (float) PIXARCSEC;

memcpy(t->rowc, s->rowc, NCOLOR * sizeof(float));
memcpy(t->colc, s->colc, NCOLOR * sizeof(float));

memcpy(t->rowcErr, s->rowcErr, NCOLOR * sizeof(float));
memcpy(t->colcErr, s->colcErr, NCOLOR * sizeof(float));

memcpy(t->ab_deV, s->ab_deV, NCOLOR * sizeof(float));
memcpy(t->ab_deVErr, s->ab_deVErr, NCOLOR * sizeof(float));
memcpy(t->ab_exp, s->ab_exp, NCOLOR * sizeof(float));
memcpy(t->ab_expErr, s->ab_expErr, NCOLOR * sizeof(float));

/* should ensure that the input is in degrees */
memcpy(t->phi_deV_deg, s->phi_deV, NCOLOR * sizeof(float));
memcpy(t->phi_exp_deg, s->phi_exp, NCOLOR * sizeof(float));

memcpy(t->star_lnL, s->star_lnL, NCOLOR * sizeof(float));
memcpy(t->exp_lnL, s->exp_lnL, NCOLOR * sizeof(float));
memcpy(t->deV_lnL, s->deV_lnL, NCOLOR * sizeof(float));

memcpy(t->fracdeV, s->fracPSF, NCOLOR * sizeof(float));
memcpy(t->flags, s->flags, NCOLOR * sizeof(int));
memcpy(t->flags2, s->flags2, NCOLOR * sizeof(int));


memcpy(t->psfMag, s->psfMag, NCOLOR * sizeof(float));
memcpy(t->fiberMag, s->fiberMag, NCOLOR * sizeof(float));
memcpy(t->petroMag, s->petroMag, NCOLOR * sizeof(float));
memcpy(t->deVMag, s->deVMag, NCOLOR * sizeof(float));
memcpy(t->expMag, s->expMag, NCOLOR * sizeof(float));
memcpy(t->cmodelMag, s->modelMag, NCOLOR * sizeof(float));

(s->exp_lnL[R_BAND] > s->deV_lnL[R_BAND]) ? 
memcpy(t->modelMag, s->expMag, NCOLOR * sizeof(float)):
memcpy(t->modelMag, s->deVMag, NCOLOR * sizeof(float));

memcpy(t->psfMagErr, s->psfMagErr, NCOLOR * sizeof(float));
memcpy(t->fiberMagErr, s->fiberMagErr, NCOLOR * sizeof(float));
memcpy(t->petroMagErr, s->petroMagErr, NCOLOR * sizeof(float));
memcpy(t->deVMagErr, s->deVMagErr, NCOLOR * sizeof(float));
memcpy(t->expMagErr, s->expMagErr, NCOLOR * sizeof(float));
memcpy(t->cmodelMagErr, s->modelMagErr, NCOLOR * sizeof(float));

(s->exp_lnL[R_BAND] > s->deV_lnL[R_BAND]) ? 
memcpy(t->modelMagErr, s->expMagErr, NCOLOR * sizeof(float)):
memcpy(t->modelMagErr, s->deVMagErr, NCOLOR * sizeof(float));

/* should convert to angle measures rather from pixel */
memcpy(t->theta_deV, s->r_deV, NCOLOR * sizeof(float));
memcpy(t->theta_deVErr, s->r_deVErr, NCOLOR * sizeof(float));
memcpy(t->theta_exp, s->r_exp, NCOLOR * sizeof(float));
memcpy(t->theta_expErr, s->r_expErr, NCOLOR * sizeof(float));
memcpy(t->petroTheta, s->petroRad, NCOLOR * sizeof(float));
memcpy(t->petroThetaErr, s->petroRadErr, NCOLOR * sizeof(float));
memcpy(t->petroTh50, s->petroR50, NCOLOR * sizeof(float));
memcpy(t->petroTh50Err, s->petroR50Err, NCOLOR * sizeof(float));
memcpy(t->petroTh90, s->petroR90, NCOLOR * sizeof(float));
memcpy(t->petroTh90Err, s->petroR90Err, NCOLOR * sizeof(float));
int i;
for (i = 0; i < NCOLOR; i++) {
	t->theta_deV[i] *= (float) PIXARCSEC;
	t->theta_deVErr[i] *= (float) PIXARCSEC;
	t->theta_exp[i] *=  (float) PIXARCSEC;
	t->theta_expErr[i] *=  (float) PIXARCSEC;
	t->petroTheta[i] *=  (float) PIXARCSEC;
	t->petroThetaErr[i] *=  (float) PIXARCSEC;
	t->petroTh50[i] *=  (float) PIXARCSEC;
	t->petroTh50Err[i] *=  (float) PIXARCSEC;
	t->petroTh90[i] *=  (float) PIXARCSEC;
	t->petroTh90Err[i] *=  (float) PIXARCSEC;
}

return;
}


RET_CODE thCalibPhObjcIoFromGC(CALIB_PHOBJC_IO *cphobjc, CALIB_WOBJC_IO *cwphobjc, WOBJC_IO *phobjc, CHAIN *gcs) {
char *name = "thCalibPhObjcIoFromGC";

copy_calib_wobjc_io_onto_calib_phobjc_io (cwphobjc, cphobjc);
float rowc, colc;
rowc = cphobjc->objc_rowc;
colc = cphobjc->objc_colc;
RET_CODE status;
status = thGCChainGetExtinction(gcs, rowc, colc, cphobjc->extinction);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calculate galactic extinction for this object", name);
	return(status);
}
return(SH_SUCCESS);
}


