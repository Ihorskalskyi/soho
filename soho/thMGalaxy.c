#include "thMGalaxy.h"
#include "thMGalaxy-aux.h"
#include "thMath.h"

static THPIX magnitude(THPIX f, int band);
static THPIX ellipticity(THPIX ab);
static THPIX effectiveradius(THPIX r, THPIX ab, THPIX small, THPIX large);

static RET_CODE canonize_gaussian(GAUSSIANPARS *q);
static RET_CODE derive_class_info(PHPROPS *p, 
	THPIX *psf_L, THPIX *deV_L, THPIX *exp_L, 
	THPIX *psfMag, THPIX *deVMag, THPIX *expMag, 
	OBJ_TYPE *objcType);

/* the following static functions are re-iteration of ModelElemNew and ModelElemAddDeriv, but the extra parameters are specific to this package  */
static THPIX magnitude(THPIX f, int band);
static MODEL_ELEM *StarModelElemNew (char *mname, void *f0, char *pname, void *init, void *reg_generator);
static MODEL_ELEM *GalaxyModelElemNew (char *mname, void *f0, char *pname, void *init, void *reg_generator, void *psf_info_update);
static RET_CODE GalaxyModelElemAddDeriv(MODEL_ELEM *me, char *rname, void *Df);
static RET_CODE GalaxyModelElemAddDeriv2(MODEL_ELEM *me, char *rname1, char *rname2, void *DDf);

static RUN_MODE MGalaxyMode = MLE_MODE;
static PSF_CONVOLUTION_TYPE MGalaxyPsfConvolution = DEFAULT_GALAXY_PSF_CONVOLUTION;

static RET_CODE get_n_sersic(THPIX w1, THPIX w2, THPIX *n);
static RET_CODE get_n_sersic1(THPIX w1, THPIX w2, THPIX *n);
static RET_CODE get_n_sersic2(THPIX w1, THPIX w2, THPIX *n);
static RET_CODE get_n_coresersic(THPIX w1, THPIX w2, THPIX *n);
static RET_CODE get_d_coresersic(THPIX w1, THPIX w2, THPIX *d);
static RET_CODE get_g_coresersic(THPIX w1, THPIX w2, THPIX *g);

static RET_CODE draw_re_sersic(THPIX *n);
static RET_CODE draw_n_sersic(THPIX *n);
static RET_CODE draw_n_sersic1(THPIX *n);
static RET_CODE draw_n_sersic2(THPIX *n);
static RET_CODE draw_n_coresersic(THPIX *n);
static RET_CODE draw_d_coresersic(THPIX *d);
static RET_CODE draw_g_coresersic(THPIX *g);

static THPIX N_SERSIC_1 = 4.0;
static THPIX N_SERSIC_2 = 1.0;

static THPIX N_SERSIC1_1 = 4.0;
static THPIX N_SERSIC1_2 = 1.0;

static THPIX N_SERSIC2_1 = 4.0;
static THPIX N_SERSIC2_2 = 1.0;

static THPIX RE_SERSIC_DRAW_1 = (THPIX) RE_SERSIC_DRAW1;
static THPIX RE_SERSIC_DRAW_2 = (THPIX) RE_SERSIC_DRAW2;

static THPIX N_SERSIC_DRAW_1 = (THPIX) N_SERSIC_DRAW1;
static THPIX N_SERSIC_DRAW_2 = (THPIX) N_SERSIC_DRAW2;

static THPIX N_SERSIC_DRAW1_1 = (THPIX) N_SERSIC_DRAW1;
static THPIX N_SERSIC_DRAW1_2 = (THPIX) N_SERSIC_DRAW2;

static THPIX N_SERSIC_DRAW2_1 = (THPIX) N_SERSIC_DRAW1;
static THPIX N_SERSIC_DRAW2_2 = (THPIX) N_SERSIC_DRAW2;

static THPIX N_CORESERSIC_1 = 4.0;
static THPIX N_CORESERSIC_2 = 1.0;
static THPIX N_CORESERSIC = NN_CORESERSIC;

static THPIX DELTA_CORESERSIC_1 = D_CORESERSIC;
static THPIX DELTA_CORESERSIC_2 = D_CORESERSIC;
static THPIX DELTA_CORESERSIC = D_CORESERSIC;

static THPIX GAMMA_CORESERSIC_1 = G_CORESERSIC;
static THPIX GAMMA_CORESERSIC_2 = G_CORESERSIC;
static THPIX GAMMA_CORESERSIC = G_CORESERSIC;

static THPIX N_CORESERSIC_DRAW_1 = N_CORESERSIC_DRAW1;
static THPIX N_CORESERSIC_DRAW_2 = N_CORESERSIC_DRAW2;

static THPIX DELTA_CORESERSIC_DRAW_1 = D_CORESERSIC_DRAW1;
static THPIX DELTA_CORESERSIC_DRAW_2 = D_CORESERSIC_DRAW2;

static THPIX GAMMA_CORESERSIC_DRAW_1 = G_CORESERSIC_DRAW1;
static THPIX GAMMA_CORESERSIC_DRAW_2 = G_CORESERSIC_DRAW2;

RET_CODE thMGalaxyPsfSet(PSF_CONVOLUTION_TYPE psf_convolution_type) {
char *name = "thMGalaxyPsfSet";
thError("%s: WARNING - this interface is reserved for expert user - should only be used during simulations", name);
printf("%s: previous galaxy convolution type = '%s' \n", name, 
	shEnumNameGetFromValue("PSF_CONVOLUTION_TYPE", MGalaxyPsfConvolution));
MGalaxyPsfConvolution = psf_convolution_type;
printf("%s: current convolution type = '%s' \n", name, 	
	shEnumNameGetFromValue("PSF_CONVOLUTION_TYPE", MGalaxyPsfConvolution));
return(SH_SUCCESS);
}

RET_CODE thMGalaxyPsfGet(PSF_CONVOLUTION_TYPE *psf_convolution_type) {
shAssert(psf_convolution_type != NULL);
*psf_convolution_type = MGalaxyPsfConvolution;
return(SH_SUCCESS);
}

RET_CODE thMGalaxyModeSet(RUN_MODE mode) {
char *name = "thMGalaxyModeSet";
if (MGalaxyMode == mode) {
	thError("%s: WARNING - no mode change in MGalaxy module (%s)", name, shEnumNameGetFromValue("RUN_MODE", mode));
	return(SH_SUCCESS);
}
if (mode != SIM_MODE && mode != MLE_MODE && mode != MLE_MODE_PLUS && mode != MLE_ON_DEMAND_MODE) {
	thError("%s: ERROR - unsupported run mode ('%s')", name, shEnumNameGetFromValue("RUN_MODE", mode));
	return(SH_GENERIC_ERROR);
}

int nf = 2;
char **fnames = thCalloc(nf, sizeof(char *));
fnames[0] = "MGInitDeV1";
fnames[1] = "MGInitDeV2";
MGalaxyMode = mode;
thError("%s: WARNING - run mode changed into '%s'", name, shEnumNameGetFromValue("RUN_MODE", mode));
thFree(fnames);
return(SH_SUCCESS);
}

RET_CODE thMGalaxyModeGet(RUN_MODE *mode) {
char *name = "thMGalaxyModeGet";
if (mode == NULL) {
	thError("%s: ERROR - null (mode) placeholder", name);
	return(SH_GENERIC_ERROR);
}

*mode = MGalaxyMode;
if (MGalaxyMode != SIM_MODE && MGalaxyMode != MLE_MODE && MGalaxyMode != MLE_MODE_PLUS && MGalaxyMode != MLE_ON_DEMAND_MODE) {
	thError("%s: WARNING - unsupported run mode ('%s')", name, shEnumNameGetFromValue("RUN_MODE", MGalaxyMode));
}
return(SH_SUCCESS);
}

MODEL_ELEM *StarModelElemNew(char *mname, void *f0, char *pname, void *init, void *reg_generator) {
return(thModelElemNew(mname, f0, pname, 
		WHOLEREGION, THREGION2_TYPE, NO_PSF_CONVOLUTION, 2, 
		init, reg_generator, NULL, NULL, NULL));
}

MODEL_ELEM *GalaxyModelElemNew(char *mname, void *f0, char *pname, void *init, void *reg_generator, void *psf_info_update) {
/* 
PSF_CONVOLUTION_TYPE conv_type = NO_PSF_CONVOLUTION;
*/
/* CENTRAL_AND_WINGS_PSF */

return(thModelElemNew(mname, f0, pname, 
		WHOLEREGION, THREGION1_TYPE, MGalaxyPsfConvolution, 1, 
		init, reg_generator, psf_info_update, NULL, NULL));
}

RET_CODE GalaxyModelElemAddDeriv(MODEL_ELEM *me, char *rname, void *Df) {
return(thModelElemAddDeriv(me, rname, Df));
}

RET_CODE GalaxyModelElemAddDeriv2(MODEL_ELEM *me, char *rname1, char *rname2, void *DDf) {
return(thModelElemAddDeriv2(me, rname1, rname2, DDf));
}




/* --- end of static functions --- */

RET_CODE MGObjcInit(char *tname, char *pname, 
		THOBJCTYPE ctype, char *cname, 
		void *classifier, void *compactor, 
		char **mnames, int nm) {

char *name = "MGObjcInit";
RET_CODE status;

if (mnames == NULL || nm <= 0) {
	thError("%s: ERROR - no component model provided", name);
	return(SH_GENERIC_ERROR);
}

if (ctype == UNKNOWN_OBJC && (cname == NULL || strlen(cname) == 0)) {
	thError("%s: ERROR - no proper candidate type provided to define object type '%s'", name, tname);
	return(SH_GENERIC_ERROR);
}

if (cname == NULL || strlen(cname) == 0) {
	status = thObjcNameGetFromType(ctype, &cname);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - PHOTO_OBJC needs to be defined before defining '%s'", name, tname);
		return(status);
	}
} else if (ctype != UNKNOWN_OBJC) {
	char *ccname;
	status = thObjcNameGetFromType(ctype, &ccname);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not check the candidate type (%d) in the object bank", name, ctype);
		return(status);
	}
	if (strcmp(cname, ccname)) {
		thError("%s: ERROR - candidate type (ctype = %d) belongs to object type '%s' while (cname = '%s')", name, ctype, ccname, cname);
		return(SH_GENERIC_ERROR);
	}
} else if (ctype == UNKNOWN_OBJC) {
	status = thObjcTypeGetFromName(cname, &ctype);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - cannot obtain an object type for candidate '%s')", name, cname);
		return(status);
	}
}

TYPE ptype;
if (pname == NULL) pname = "";
if (strlen(pname) == 0) {
	ptype = UNKNOWN_SCHEMA;
	thError("%s: WARNING - only amplitudes will be open to fit for objects of type '%s' - suggested only acceptable for stars and previously measured objects", name, tname);
	}
if (strlen(pname) != 0 && (ptype = shTypeGetFromName(pname)) == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - cannot recognize variable type '%s'", name, pname);
	return(SH_GENERIC_ERROR);
}

int i;

THOBJCTYPE t;
status = thObjcTypeGetFromName(tname, &t);
OBJC_ELEM *oe;
char *mname;
if (t != UNKNOWN_OBJC) {
	thError("%s: WARNING - redefining object type '%s'", name, tname);
	status = thObjcElemGetByName(tname, &oe);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not obtain the old definition of '%s')", name, tname);
		return(status);
	}
	CHAIN *oms;
 	if (oe->ms != NULL) {
		oms = oe->ms;
		oe->ms = shChainNew("MODEL_ELEM");
	}	
	int err = 0;
	for (i = 0; i < nm; i++) {
		mname = mnames[i];
		if (mname == NULL || strlen(mname) == 0) {
			thError("%s: WARNING - the %d-th model name for object type '%s' is null", name, i, tname);
			err++;
			} else {
			status = thObjcElemAddModel(oe, mname);
		}
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not add models to the object definition '%s')", name, tname);
			shChainDel(oe->ms);
			oe->ms = oms;
		}
	}
	if (err == nm) {
		thError("%s: ERROR - no model could be uploaded to definition of object '%s'", name, tname);
		shChainDel(oe->ms);
		oe->ms = oms;
		return(SH_GENERIC_ERROR);
	}
	if (err) {
		thError("%s: WARNING - only (%d) models out of (%d) could be uploaded to the definition of object '%s'", 
		name, nm - err, err, tname);
		} 
	if (oms != NULL) shChainDel(oms);
	if (oe->candidatename == NULL) oe->candidatename = thCalloc(MX_STRING_LEN, sizeof(char));
	if (oe->pname == NULL) oe->pname = thCalloc(MX_STRING_LEN, sizeof(char));
	strcpy(oe->candidatename, cname);
	strcpy(oe->pname, pname);
	oe->ptype = ptype;
	oe->classifier = classifier;
	oe->compactor = compactor;
	return(SH_SUCCESS);
}	


oe = thObjcElemNew(cname, tname, classifier, pname, compactor);
if (oe == NULL) {
	thError("%s: ERROR - could not properly define object type '%s'", name, tname);
	return(SH_GENERIC_ERROR);
}
int err = 0;
for (i = 0; i < nm; i++) {
	mname = mnames[i];
	if (mname == NULL || strlen(mname) == 0) {
		thError("%s: WARNING - the %d-th model name for object type '%s' is null", name, i, tname);
		err++;
	} 
	status = thObjcElemAddModel(oe, mnames[i]);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add model to definition of object '%s'", name, tname);
		thObjcElemDel(oe);
		return(status);
	}
}
if (err == nm) {
	thError("%s: ERROR - no model could be uploaded to definition of object '%s'", name, tname);
	thObjcElemDel(oe);
	return(SH_GENERIC_ERROR);
	}
if (err) {
	thError("%s: WARNING - only (%d) models out of (%d) could be uploaded to the definition of object '%s'", 
		name, nm - err, err, tname);
	} 

status = thObjcTypeGetNextAvailable(&oe->classtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not find a suitable (type) for object '%s'", name, tname);
	thObjcElemDel(oe);
	return(status);
}	
status = thObjcElemAddToObjcBank(oe);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add object type '%s' to the object bank", name, tname);
	thObjcElemDel(oe);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE MGalaxyObjcInit(char **mnames, int nm) {
char *name = "MGalaxyObjcInit";

void *classifier, *compactor;

classifier = (void *) &MGalaxyClassifier;
compactor = NULL;

char *pname = "OBJCPARS";
char *tname = "GALAXY";

RET_CODE status;
status = MGObjcInit(tname, pname, PHOTO_OBJC, NULL, classifier, compactor, mnames, nm);

if (status != SH_SUCCESS) {
	thError("%s: ERROR - object type definition failed for '%s'", name, tname);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE McDCandidateObjcInit(char **mnames, int nm) {
char *name = "McDCandidateObjcInit";

void *classifier, *compactor;
classifier = (void *) &McDCandidateClassifier;
compactor = NULL;

char *pname = "OBJCPARS";
char *tname = "CDCANDIDATE";

RET_CODE status;
status = MGObjcInit(tname, pname, PHOTO_OBJC, NULL, classifier, compactor, mnames, nm);

if (status != SH_SUCCESS) {
	thError("%s: ERROR - object type definition failed for '%s'", name, tname);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE MdeVGalaxyObjcInit(char **mnames, int nm) {
char *name = "MdeVGalaxyObjcInit";

void *classifier, *compactor;
classifier = (void *) &MdeVGalaxyClassifier;
compactor = NULL;

char *pname = "OBJCPARS";
char *tname = "DEVGALAXY";

RET_CODE status;
status = MGObjcInit(tname, pname, PHOTO_OBJC, NULL, classifier, compactor, mnames, nm);

if (status != SH_SUCCESS) {
	thError("%s: ERROR - object type definition failed for '%s'", name, tname);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE MExpGalaxyObjcInit(char **mnames, int nm) {
char *name = "MExpGalaxyObjcInit";

void *classifier, *compactor;

classifier = (void *) &MExpGalaxyClassifier;
compactor = NULL;

char *pname = "OBJCPARS";
char *tname = "EXPGALAXY";

RET_CODE status;
status = MGObjcInit(tname, pname, PHOTO_OBJC, NULL, classifier, compactor, mnames, nm);

if (status != SH_SUCCESS) {
	thError("%s: ERROR - object type definition failed for '%s'", name, tname);
	return(status);
}

return(SH_SUCCESS);
}

 
RET_CODE MSersicGalaxyObjcInit(char **mnames, int nm) {
char *name = "MSersicGalaxyObjcInit";

void *classifier, *compactor;

classifier = (void *) &MSersicGalaxyClassifier;
compactor = NULL;

char *pname = "OBJCPARS";
char *tname = "SERSICGALAXY";

RET_CODE status;
status = MGObjcInit(tname, pname, PHOTO_OBJC, NULL, classifier, compactor, mnames, nm);

if (status != SH_SUCCESS) {
	thError("%s: ERROR - object type definition failed for '%s'", name, tname);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE MSersicExpGalaxyObjcInit(char **mnames, int nm) {
char *name = "MSersicExpGalaxyObjcInit";

void *classifier, *compactor;

classifier = (void *) &MSersicExpGalaxyClassifier;
compactor = NULL;

char *pname = "OBJCPARS";
char *tname = "SERSICEXPGALAXY";

RET_CODE status;
status = MGObjcInit(tname, pname, PHOTO_OBJC, NULL, classifier, compactor, mnames, nm);

if (status != SH_SUCCESS) {
	thError("%s: ERROR - object type definition failed for '%s'", name, tname);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE MSersicSersicGalaxyObjcInit(char **mnames, int nm) {
char *name = "MSersicSersicGalaxyObjcInit";

void *classifier, *compactor;

classifier = (void *) &MSersicSersicGalaxyClassifier;
compactor = NULL;

char *pname = "OBJCPARS";
char *tname = "SERSICSERSICGALAXY";

RET_CODE status;
status = MGObjcInit(tname, pname, PHOTO_OBJC, NULL, classifier, compactor, mnames, nm);

if (status != SH_SUCCESS) {
	thError("%s: ERROR - object type definition failed for '%s'", name, tname);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE MCoresersicGalaxyObjcInit(char **mnames, int nm) {
char *name = "MCoresersicGalaxyObjcInit";

void *classifier, *compactor;

classifier = (void *) &MCoresersicGalaxyClassifier;
compactor = NULL;

char *pname = "OBJCPARS";
char *tname = "CORESERSICGALAXY";

RET_CODE status;
status = MGObjcInit(tname, pname, PHOTO_OBJC, NULL, classifier, compactor, mnames, nm);

if (status != SH_SUCCESS) {
	thError("%s: ERROR - object type definition failed for '%s'", name, tname);
	return(status);
}

return(SH_SUCCESS);
}


RET_CODE MStarObjcInit(char *mname) {
char *name = "MStarObjcInit";

void *classifier, *compactor;

classifier = (void *) &MStarClassifier;
compactor = NULL;

char *pname = "OBJCPARS";
char *tname = "STAR";

RET_CODE status;
status = MGObjcInit(tname, pname, PHOTO_OBJC, NULL, classifier, compactor, &mname, 1);

if (status != SH_SUCCESS) {
	thError("%s: ERROR - object type definition failed for '%s'", name, tname);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE MStarGalaxyObjcInit(char **mnames, int nm) {
char *name = "MStarGalaxyObjcInit";

void *classifier, *compactor;

classifier = (void *) &MStarGalaxyClassifier;
compactor = NULL;

char *pname = "OBJCPARS";
char *tname = "STARGALAXY";

RET_CODE status;
status = MGObjcInit(tname, pname, 
		PHOTO_OBJC, NULL, 
		classifier, compactor, 	
		mnames, 1);

if (status != SH_SUCCESS) {
	thError("%s: ERROR - object type definition failed for '%s'", name, tname);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE MGaussianObjcInit(char **mnames, int nm) {
char *name = "MGaussianObjcInit";

void *classifier, *compactor;

classifier = (void *) &PHGalaxyClassifier;
compactor = NULL;

char *pname = "OBJCPARS";
char *tname = "GAUSSIAN";

RET_CODE status;
status = MGObjcInit(tname, pname, 
		PHOTO_OBJC, NULL, 
		classifier, compactor, 	
		mnames, nm);

if (status != SH_SUCCESS) {
	thError("%s: ERROR - object type definition failed for '%s'", name, tname);
	return(status);
}

return(SH_SUCCESS);
}

MODEL_ELEM *thStarModelElemNewInBank(char *mname, void *f0, char *pname, void *g0, void *h0, RET_CODE *status) {
char *name = "thModelElemNewInBank";
RET_CODE status2;
MODEL_ELEM *me;

me = StarModelElemNew(mname, f0, pname, g0, h0);
if (me == NULL) {
	thError("%s: ERROR - model element for '%s' could not be created", name, mname);
	return(NULL);
}

status2 = thModelElemAddToMBank(me);
if (status2 != SH_SUCCESS) {
	thModelElemDel(me);
	if (status != NULL) *status = status2;
	return(NULL);
}

if (status != NULL) *status = SH_SUCCESS;
return(me);
} 


MODEL_ELEM *thGalaxyModelElemNewInBank(char *mname, void *f0, char *pname, void *g0, void *h0, void *k0, RET_CODE *status) {
char *name = "thModelElemNewInBank";
RET_CODE status2;
MODEL_ELEM *me;

me = GalaxyModelElemNew(mname, f0, pname, g0, h0, k0);
if (me == NULL) {
	thError("%s: ERROR - model element for '%s' could not be created", name, mname);
	return(NULL);
}

status2 = thModelElemAddToMBank(me);
if (status2 != SH_SUCCESS) {
	thModelElemDel(me);
	if (status != NULL) *status = status2;
	return(NULL);
}

if (status != NULL) *status = SH_SUCCESS;
return(me);
} 


RET_CODE MGModelsInit() {
char *name = "MGObjcModelInit";

char *mname, *pname, *rname;
void *f0, *g0, *h0, *k0, *Df;
MODEL_ELEM *me;
RET_CODE status;

#if MLE_CONDUCT_HIGHER_ORDER 
void *DDf;
char *rname1, *rname2;
#endif
 
mname = "star";
pname = "STARPARS";
f0 = (void *) &starProf;
g0 = (void *) &MGInitStar;
h0 = (void *) &StarSpecifyRegion;
me = thStarModelElemNewInBank(mname, f0, pname, g0, h0, &status);
if (status != SH_SUCCESS || me == NULL) {
	thError("%s: ERROR - could not introduce model '%s' to the bank", name, mname);
	if (status != SH_SUCCESS) return(status);
	return(SH_GENERIC_ERROR);
}


mname = "deV";
pname = "DEVPARS";
f0 = (void *) &deVProf;
g0 = (void *) &MGInitDeV;
h0 = (void *) &deVGalaxySpecifyRegion;
k0 = (void *) &MGPsfDeV;
me = thGalaxyModelElemNewInBank(mname, f0, pname, g0, h0, k0, &status);
if (status != SH_SUCCESS || me == NULL) {
	thError("%s: ERROR - could not introduce model '%s' to the bank", name, mname);
	if (status != SH_SUCCESS) return(status);
	return(SH_GENERIC_ERROR);
}
rname = "xc";
Df = (void *) &DdeVDxcProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "yc";
Df = (void *) &DdeVDycProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "re";
Df = (void *) &DdeVDreProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "ue";
Df = (void *) &DdeVDueProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "phi";
Df = (void *) &DdeVDphiProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "e";
Df = (void *) &DdeVDeProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "E";
Df = (void *) &DdeVDEProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "v";
Df = (void *) &DdeVDvProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "w";
Df = (void *) &DdeVDwProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "V";
Df = (void *) &DdeVDVProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "W";
Df = (void *) &DdeVDWProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "a";
Df = (void *) &DdeVDaProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "b";
Df = (void *) &DdeVDbProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "c";
Df = (void *) &DdeVDcProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

#if MLE_CONDUCT_HIGHER_ORDER
rname1 = "re";
rname2 = "re";
DDf = (void *) &DDdeVDreDreProf;
status = GalaxyModelElemAddDeriv2(me, rname1, rname2, DDf);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add DDf for '%s' wrt '%s','%s' to the bank", name, mname, rname1, rname2);
	return(status);
}
rname1 = "ue";
rname2 = "ue";
DDf = (void *) &DDdeVDueDueProf;
status = GalaxyModelElemAddDeriv2(me, rname1, rname2, DDf);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add DDf for '%s' wrt '%s','%s' to the bank", name, mname, rname1, rname2);
	return(status);
}
#endif
/* initiating first component deV */
mname = "deV1";
pname = "DEVPARS";
f0 = (void *) &deVProf;
g0 = (void *) &MGInitDeV1;
h0 = (void *) &deVGalaxySpecifyRegion;
k0 = (void *) &MGPsfDeV;
me = thGalaxyModelElemNewInBank(mname, f0, pname, g0, h0, k0, &status);
if (status != SH_SUCCESS || me == NULL) {
	thError("%s: ERROR - could not introduce model '%s' to the bank", name, mname);
	if (status != SH_SUCCESS) return(status);
	return(SH_GENERIC_ERROR);
}
rname = "xc";
Df = (void *) &DdeVDxcProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "yc";
Df = (void *) &DdeVDycProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "re";
Df = (void *) &DdeVDreProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "ue";
Df = (void *) &DdeVDueProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "phi";
Df = (void *) &DdeVDphiProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "e";
Df = (void *) &DdeVDeProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "E";
Df = (void *) &DdeVDEProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "v";
Df = (void *) &DdeVDvProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "w";
Df = (void *) &DdeVDwProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "V";
Df = (void *) &DdeVDVProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "W";
Df = (void *) &DdeVDWProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "a";
Df = (void *) &DdeVDaProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "b";
Df = (void *) &DdeVDbProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "c";
Df = (void *) &DdeVDcProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

#if MLE_CONDUCT_HIGHER_ORDER
rname1 = "re";
rname2 = "re";
DDf = (void *) &DDdeVDreDreProf;
status = GalaxyModelElemAddDeriv2(me, rname1, rname2, DDf);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add DDf for '%s' wrt '%s','%s' to the bank", name, mname, rname1, rname2);
	return(status);
}
rname1 = "ue";
rname2 = "ue";
DDf = (void *) &DDdeVDueDueProf;
status = GalaxyModelElemAddDeriv2(me, rname1, rname2, DDf);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add DDf for '%s' wrt '%s','%s' to the bank", name, mname, rname1, rname2);
	return(status);
}
#endif

/* initiating second component deV */
mname = "deV2";
pname = "DEVPARS";
f0 = (void *) &deVProf;
g0 = (void *) &MGInitDeV2;
h0 = (void *) &deVGalaxySpecifyRegion;
k0 = (void *) &MGPsfDeV;
me = thGalaxyModelElemNewInBank(mname, f0, pname, g0, h0, k0, &status);
if (status != SH_SUCCESS || me == NULL) {
	thError("%s: ERROR - could not introduce model '%s' to the bank", name, mname);
	if (status != SH_SUCCESS) return(status);
	return(SH_GENERIC_ERROR);
}
rname = "xc";
Df = (void *) &DdeVDxcProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "yc";
Df = (void *) &DdeVDycProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "re";
Df = (void *) &DdeVDreProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "ue";
Df = (void *) &DdeVDueProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "phi";
Df = (void *) &DdeVDphiProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "e";
Df = (void *) &DdeVDeProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "E";
Df = (void *) &DdeVDEProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "v";
Df = (void *) &DdeVDvProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "w";
Df = (void *) &DdeVDwProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "V";
Df = (void *) &DdeVDVProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "W";
Df = (void *) &DdeVDWProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "a";
Df = (void *) &DdeVDaProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "b";
Df = (void *) &DdeVDbProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "c";
Df = (void *) &DdeVDcProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
#if MLE_CONDUCT_HIGHER_ORDER
rname1 = "re";
rname2 = "re";
DDf = (void *) &DDdeVDreDreProf;
status = GalaxyModelElemAddDeriv2(me, rname1, rname2, DDf);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add DDf for '%s' wrt '%s','%s' to the bank", name, mname, rname1, rname2);
	return(status);
}
rname1 = "ue";
rname2 = "ue";
DDf = (void *) &DDdeVDueDueProf;
status = GalaxyModelElemAddDeriv2(me, rname1, rname2, DDf);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add DDf for '%s' wrt '%s','%s' to the bank", name, mname, rname1, rname2);
	return(status);
}
#endif
/* initiating Exp model */
mname = "Exp";
pname = "EXPPARS";
f0 = (void *) &ExpProf;
g0 = (void *) &MGInitExp;
h0 = (void *) &ExpGalaxySpecifyRegion;
k0 = (void *) &MGPsfExp;
me = thGalaxyModelElemNewInBank(mname, f0, pname, g0, h0, k0, &status);
if (status != SH_SUCCESS || me == NULL) {
	thError("%s: ERROR - could not introduce model '%s' to the bank", name, mname);
	if (status != SH_SUCCESS) return(status);
	return(SH_GENERIC_ERROR);
}
rname = "xc";
Df = (void *) &DExpDxcProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "yc";
Df = (void *) &DExpDycProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "re";
Df = (void *) &DExpDreProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "ue";
Df = (void *) &DExpDueProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "phi";
Df = (void *) &DExpDphiProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "e";
Df = (void *) &DExpDeProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "E";
Df = (void *) &DExpDEProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "v";
Df = (void *) &DExpDvProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "w";
Df = (void *) &DExpDwProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "V";
Df = (void *) &DExpDVProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "W";
Df = (void *) &DExpDWProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "a";
Df = (void *) &DExpDaProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "b";
Df = (void *) &DExpDbProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "c";
Df = (void *) &DExpDcProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

/* initiating Exp1 (interior) model */
mname = "Exp1";
pname = "EXPPARS";
f0 = (void *) &ExpProf;
g0 = (void *) &MGInitExp1;
h0 = (void *) &ExpGalaxySpecifyRegion;
k0 = (void *) &MGPsfExp;
me = thGalaxyModelElemNewInBank(mname, f0, pname, g0, h0, k0, &status);
if (status != SH_SUCCESS || me == NULL) {
	thError("%s: ERROR - could not introduce model '%s' to the bank", name, mname);
	if (status != SH_SUCCESS) return(status);
	return(SH_GENERIC_ERROR);
}
rname = "xc";
Df = (void *) &DExpDxcProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "yc";
Df = (void *) &DExpDycProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "re";
Df = (void *) &DExpDreProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "ue";
Df = (void *) &DExpDueProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "phi";
Df = (void *) &DExpDphiProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "e";
Df = (void *) &DExpDeProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "E";
Df = (void *) &DExpDEProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "v";
Df = (void *) &DExpDvProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "w";
Df = (void *) &DExpDwProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "V";
Df = (void *) &DExpDVProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "W";
Df = (void *) &DExpDWProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}



rname = "a";
Df = (void *) &DExpDaProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "b";
Df = (void *) &DExpDbProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "c";
Df = (void *) &DExpDcProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}




/* initiating Exp2 (exterior) model */
mname = "Exp2";
pname = "EXPPARS";
f0 = (void *) &ExpProf;
g0 = (void *) &MGInitExp2;
h0 = (void *) &ExpGalaxySpecifyRegion;
k0 = (void *) &MGPsfExp;
me = thGalaxyModelElemNewInBank(mname, f0, pname, g0, h0, k0, &status);
if (status != SH_SUCCESS || me == NULL) {
	thError("%s: ERROR - could not introduce model '%s' to the bank", name, mname);
	if (status != SH_SUCCESS) return(status);
	return(SH_GENERIC_ERROR);
}
rname = "xc";
Df = (void *) &DExpDxcProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "yc";
Df = (void *) &DExpDycProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "re";
Df = (void *) &DExpDreProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "ue";
Df = (void *) &DExpDueProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "phi";
Df = (void *) &DExpDphiProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "e";
Df = (void *) &DExpDeProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "E";
Df = (void *) &DExpDEProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "v";
Df = (void *) &DExpDvProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "w";
Df = (void *) &DExpDwProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "V";
Df = (void *) &DExpDVProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "W";
Df = (void *) &DExpDWProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}



rname = "a";
Df = (void *) &DExpDaProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "b";
Df = (void *) &DExpDbProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "c";
Df = (void *) &DExpDcProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

/* initiating power law model */

mname = "Pl";
pname = "POWERLAWPARS";
f0 = (void *) &PlProf;
g0 = (void *) &MGInitPl;
h0 = (void *) &PlGalaxySpecifyRegion;
k0 = (void *) &MGPsfPl;
me = thGalaxyModelElemNewInBank(mname, f0, pname, g0, h0, k0, &status);
if (status != SH_SUCCESS || me == NULL) {
	thError("%s: ERROR - could not introduce model '%s' to the bank", name, mname);
	if (status != SH_SUCCESS) return(status);
	return(SH_GENERIC_ERROR);
}
rname = "xc";
Df = (void *) &DPlDxcProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "yc";
Df = (void *) &DPlDycProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "re";
Df = (void *) &DPlDreProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "ue";
Df = (void *) &DPlDueProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "phi";
Df = (void *) &DPlDphiProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "e";
Df = (void *) &DPlDeProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "E";
Df = (void *) &DPlDEProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "v";
Df = (void *) &DPlDvProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "w";
Df = (void *) &DPlDwProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "V";
Df = (void *) &DPlDVProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "W";
Df = (void *) &DPlDWProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "a";
Df = (void *) &DPlDaProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "b";
Df = (void *) &DPlDbProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "c";
Df = (void *) &DPlDcProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

#if MLE_CONDUCT_HIGHER_ORDER
rname1 = "re";
rname2 = "re";
DDf = (void *) &DDPlDreDreProf;
status = GalaxyModelElemAddDeriv2(me, rname1, rname2, DDf);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add DDf for '%s' wrt '%s','%s' to the bank", name, mname, rname1, rname2);
	return(status);
}
rname1 = "ue";
rname2 = "ue";
DDf = (void *) &DDPlDueDueProf;
status = GalaxyModelElemAddDeriv2(me, rname1, rname2, DDf);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add DDf for '%s' wrt '%s','%s' to the bank", name, mname, rname1, rname2);
	return(status);
}
#endif
 
mname = "sersic";
pname = "SERSICPARS";
f0 = (void *) &sersicProf;
g0 = (void *) &MGInitSersic;
h0 = (void *) &sersicGalaxySpecifyRegion;
k0 = (void *) &MGPsfSersic;
me = thGalaxyModelElemNewInBank(mname, f0, pname, g0, h0, k0, &status);
if (status != SH_SUCCESS || me == NULL) {
	thError("%s: ERROR - could not introduce model '%s' to the bank", name, mname);
	if (status != SH_SUCCESS) return(status);
	return(SH_GENERIC_ERROR);
}
rname = "xc";
Df = (void *) &DsersicDxcProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "yc";
Df = (void *) &DsersicDycProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "n";
Df = (void *) &DsersicDnProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "N";
Df = (void *) &DsersicDNProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "re";
Df = (void *) &DsersicDycProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "e";
Df = (void *) &DsersicDeProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "ue";
Df = (void *) &DsersicDueProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "phi";
Df = (void *) &DsersicDphiProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "E";
Df = (void *) &DsersicDEProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "v";
Df = (void *) &DsersicDvProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "w";
Df = (void *) &DsersicDwProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "V";
Df = (void *) &DsersicDVProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "W";
Df = (void *) &DsersicDWProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "a";
Df = (void *) &DsersicDaProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "b";
Df = (void *) &DsersicDbProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "c";
Df = (void *) &DsersicDcProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

mname = "sersic1";
pname = "SERSICPARS";
f0 = (void *) &sersicProf;
g0 = (void *) &MGInitSersic1;
h0 = (void *) &sersicGalaxySpecifyRegion;
k0 = (void *) &MGPsfSersic;
me = thGalaxyModelElemNewInBank(mname, f0, pname, g0, h0, k0, &status);
if (status != SH_SUCCESS || me == NULL) {
	thError("%s: ERROR - could not introduce model '%s' to the bank", name, mname);
	if (status != SH_SUCCESS) return(status);
	return(SH_GENERIC_ERROR);
}
rname = "xc";
Df = (void *) &DsersicDxcProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "yc";
Df = (void *) &DsersicDycProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "n";
Df = (void *) &DsersicDnProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "N";
Df = (void *) &DsersicDNProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "re";
Df = (void *) &DsersicDycProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "e";
Df = (void *) &DsersicDeProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "ue";
Df = (void *) &DsersicDueProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "phi";
Df = (void *) &DsersicDphiProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "E";
Df = (void *) &DsersicDEProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "v";
Df = (void *) &DsersicDvProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "w";
Df = (void *) &DsersicDwProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "V";
Df = (void *) &DsersicDVProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "W";
Df = (void *) &DsersicDWProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "a";
Df = (void *) &DsersicDaProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "b";
Df = (void *) &DsersicDbProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "c";
Df = (void *) &DsersicDcProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}


mname = "sersic2";
pname = "SERSICPARS";
f0 = (void *) &sersicProf;
g0 = (void *) &MGInitSersic2;
h0 = (void *) &sersicGalaxySpecifyRegion;
k0 = (void *) &MGPsfSersic;
me = thGalaxyModelElemNewInBank(mname, f0, pname, g0, h0, k0, &status);
if (status != SH_SUCCESS || me == NULL) {
	thError("%s: ERROR - could not introduce model '%s' to the bank", name, mname);
	if (status != SH_SUCCESS) return(status);
	return(SH_GENERIC_ERROR);
}
rname = "xc";
Df = (void *) &DsersicDxcProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "yc";
Df = (void *) &DsersicDycProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "n";
Df = (void *) &DsersicDnProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "N";
Df = (void *) &DsersicDNProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "re";
Df = (void *) &DsersicDreProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "e";
Df = (void *) &DsersicDeProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "ue";
Df = (void *) &DsersicDueProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "phi";
Df = (void *) &DsersicDphiProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "E";
Df = (void *) &DsersicDEProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "v";
Df = (void *) &DsersicDvProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "w";
Df = (void *) &DsersicDwProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "V";
Df = (void *) &DsersicDVProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "W";
Df = (void *) &DsersicDWProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "a";
Df = (void *) &DsersicDaProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "b";
Df = (void *) &DsersicDbProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "c";
Df = (void *) &DsersicDcProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}



mname = "coresersic";
pname = "CORESERSICPARS";
f0 = (void *) &coresersicProf;
g0 = (void *) &MGInitCoresersic;
h0 = (void *) &coresersicGalaxySpecifyRegion;
k0 = (void *) &MGPsfCoresersic;
me = thGalaxyModelElemNewInBank(mname, f0, pname, g0, h0, k0, &status);
if (status != SH_SUCCESS || me == NULL) {
	thError("%s: ERROR - could not introduce model '%s' to the bank", name, mname);
	if (status != SH_SUCCESS) return(status);
	return(SH_GENERIC_ERROR);
}
rname = "xc";
Df = (void *) &DcoresersicDxcProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "yc";
Df = (void *) &DcoresersicDycProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "n";
Df = (void *) &DcoresersicDnProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "N";
Df = (void *) &DcoresersicDNProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "delta";
Df = (void *) &DcoresersicDdeltaProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "D";
Df = (void *) &DcoresersicDDProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "gamma";
Df = (void *) &DcoresersicDgammaProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "G";
Df = (void *) &DcoresersicDGProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "re";
Df = (void *) &DcoresersicDreProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "rb";
Df = (void *) &DcoresersicDrbProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "e";
Df = (void *) &DcoresersicDeProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "ue";
Df = (void *) &DcoresersicDueProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "ub";
Df = (void *) &DcoresersicDubProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "phi";
Df = (void *) &DcoresersicDphiProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "E";
Df = (void *) &DcoresersicDEProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

/* 

mname = "pl2";
g0 = (void *) pl2init;
me = thGalaxyModelElemNewInBank(mname, f0, pname, g0, h0, &status);
if (status != SH_SUCCESS || me == NULL) {
	thError("%s: ERROR - could not introduce model '%s' to the bank", name, mname);
	if (status != SH_SUCCESS) return(status);
	return(SH_GENERIC_ERROR);
}
rname = "xc";
Df = (void *) DplDxcProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "yc";
Df = (void *) DplDycProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "n";
Df = (void *) DplDnProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "phi";
Df = (void *) DplDphiProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "e";
Df = (void *) DplDeProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
*/
 
mname = "gaussian";
pname = "GAUSSIANPARS";
#if DEBUG_MGALAXY
printf("%s: initiating model '%s' with parameter type '%s' \n", name, mname, pname);
#endif
f0 = (void *) &gaussianProf;
g0 = (void *) &MGInitGaussian;
h0 = (void *) &GaussianGalaxySpecifyRegion;
/* no psf_info update for gaussian models */
me = thGalaxyModelElemNewInBank(mname, f0, pname, g0, h0, NULL, &status);
rname = "xc";
Df = (void *) &DgaussianDxcProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "yc";
Df = (void *) &DgaussianDycProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "a";
Df = (void *) &DgaussianDaProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "b";
Df = (void *) &DgaussianDbProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "c";
Df = (void *) &DgaussianDcProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "phi";
Df = (void *) &DgaussianDphiProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "e";
Df = (void *) &DgaussianDeProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "re";
Df = (void *) &DgaussianDreProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "ue";
Df = (void *) &DgaussianDueProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "E";
Df = (void *) &DgaussianDEProf;
status = GalaxyModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}



return(SH_SUCCESS);
}

/* Classifiers */
 
RET_CODE derive_class_info(PHPROPS *p, 
	THPIX *psf_L, THPIX *deV_L, THPIX *exp_L, 
	THPIX *psfMag, THPIX *deVMag, THPIX *expMag,
	OBJ_TYPE *objcType) {

char *name = "derive_class_info";
if (psf_L == NULL && deV_L == NULL && exp_L == NULL &&
	psfMag == NULL && deVMag == NULL && expMag == NULL) {
	thError("%s: WARNING - no placeholder available", name);
	return(SH_SUCCESS);
}
if (p == NULL) {
	thError("%s: ERROR - null PHOTO properties", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
WOBJC_IO *pobjc = NULL;
CALIB_WOBJC_IO *pcalibobjc = NULL;
int *bndex = NULL;
status = thPhpropsGet(p, &pobjc, &bndex, &pcalibobjc, NULL, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (pobjc) from (p)", name);
	return(status);
}
int ncolor;
status = thPhpropsGetNcolor(p, &ncolor);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not obtain (ncolor) from 'PHPROPS'", name);
	return(status);
}
if (OBJC_CLASS_BAND < 0 || OBJC_CLASS_BAND >= NCOLOR) {
	thError("%s: ERROR - classification band should be in [0, %d] while it is set to [%d]", name, NCOLOR - 1, OBJC_CLASS_BAND);
	return(SH_GENERIC_ERROR);
}

int objc_class_index = bndex[OBJC_CLASS_BAND];
if (objc_class_index < 0 || objc_class_index >= NCOLOR) {
	thError("%s: ERROR - index for classification band (%d) is out of bounds", name, objc_class_index);
	return(SH_GENERIC_ERROR);
}

if (psf_L != NULL) *psf_L = (THPIX) (pobjc->star_L[objc_class_index]);
if (deV_L != NULL) *deV_L = (THPIX) (pobjc->deV_L[objc_class_index]);
if (exp_L != NULL) *exp_L = (THPIX) (pobjc->exp_L[objc_class_index]);

if (psfMag != NULL) *psfMag = pcalibobjc->psfMag[objc_class_index];
if (deVMag != NULL) *deVMag = pcalibobjc->deVMag[objc_class_index];
if (expMag != NULL) *expMag = pcalibobjc->expMag[objc_class_index];

if (objcType != NULL) *objcType = pobjc->objc_type;
return(SH_SUCCESS);
}		
	
int MGalaxyClassifier(PHPROPS *p) {
char *name = "MGalaxyClassifier";

	if (p == NULL) {
		thError("%s: ERROR - null input, cannot classify", name);
		return(0);
	}

	THPIX psf_L, deV_L, exp_L;
	THPIX psfMag, deVMag, expMag;
	OBJ_TYPE objcType;
	RET_CODE status;
 	status = derive_class_info(p, 
				&psf_L, &deV_L, &exp_L, 
				&psfMag, &deVMag, &expMag, 
				&objcType);
	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not derive classification information", name);
		return(status);
	}

	int class;
	/* 
	class = (((psfMag - (deV_L>exp_L)?deVMag:expMag) > PHOTO_GALAXY_CLASS_LIMIT) && 
		((expMag - deVMag) < PHOTO_DEV_CLASS_LIMIT) &&
		((deVMag - expMag) < PHOTO_EXP_CLASS_LIMIT));
	class = class && (objcType == OBJ_GALAXY);
	*/
	class = (objcType == OBJ_GALAXY);
	#if CDCANDIDATE_CLASSIFICATION
	if (class) class = !MSuperLRGClassifier(p);
	#endif
	return(class);
}

int MSuperLRGClassifier(PHPROPS *p) {
/* 
char *name = "MSuperLRGClassifier";
*/

int res = (is_LRG(p) != LRG_NOT);
return(res);

}

int McDCandidateClassifier(PHPROPS *p) {
char *name = "McDCandidateClassifier";
	WOBJC_IO *phobjc = NULL;
	int *bndex = NULL;
	RET_CODE status;
	status = thPhpropsGet(p, &phobjc, &bndex, NULL, NULL, NULL);
	if (status != SH_SUCCESS || phobjc == NULL) {
		thError("%s: ERROR - could not get (phobjc) from (p) 'PHPROPS'", name);
		return(0);
	}
	
	if (phobjc->objc_type != OBJ_GALAXY) return(0);
	return(MSuperLRGClassifier(p));
}

int MdeVGalaxyClassifier(PHPROPS *p) {
char *name = "MdeVGalaxyClassifier";
	if (p == NULL) {
		thError("%s: ERROR - null input, cannot classify", name);
		return(0);
	}

	THPIX psf_L, deV_L, exp_L;
	THPIX psfMag, deVMag, expMag;
	OBJ_TYPE objcType;
	RET_CODE status;
 	status = derive_class_info(p, 
				&psf_L, &deV_L, &exp_L, 
				&psfMag, &deVMag, &expMag,
				&objcType);
	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not derive classification information", name);
		return(status);
	}

	int class;
	class = ((psfMag - ((deV_L>exp_L)?deVMag:expMag) > PHOTO_GALAXY_CLASS_LIMIT) && 
		(deV_L > PHOTO_DEV_EXP_FRACTION * exp_L));
	class = class && (objcType == OBJ_GALAXY);
	/* 
		((expMag - deVMag) > PHOTO_DEV_CLASS_LIMIT));
	*/
	class = (objcType == OBJ_GALAXY);
	return(class);

}

int MExpGalaxyClassifier(PHPROPS *p) {
char *name = "MExpGalaxyClassifier";
	if (p == NULL) {
		thError("%s: ERROR - null input, cannot classify", name);
		return(0);
	}

	THPIX psf_L, deV_L, exp_L;
	THPIX psfMag, deVMag, expMag;
	OBJ_TYPE objcType;
	RET_CODE status;
 	status = derive_class_info(p, 
				&psf_L, &deV_L, &exp_L, 
				&psfMag, &deVMag, &expMag,
				&objcType);
	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not derive classification information", name);
		return(status);
	}


	int class;
	class = ((psfMag - ((deV_L>exp_L)?deVMag:expMag) > PHOTO_GALAXY_CLASS_LIMIT) && 
		(exp_L > PHOTO_EXP_DEV_FRACTION * deV_L));	
	class = class && (objcType == OBJ_GALAXY);
	class = (objcType == OBJ_GALAXY);
	return(class);

}


int MSersicGalaxyClassifier(PHPROPS *p) {
char *name = "MSersicGalaxyClassifier";
	if (p == NULL) {
		thError("%s: ERROR - null input, cannot classify", name);
		return(0);
	}

	THPIX psf_L, deV_L, exp_L;
	THPIX psfMag, deVMag, expMag;
	OBJ_TYPE objcType;
	RET_CODE status;
 	status = derive_class_info(p, 
				&psf_L, &deV_L, &exp_L, 
				&psfMag, &deVMag, &expMag,
				&objcType);
	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not derive classification information", name);
		return(status);
	}


	int class;
	class = ((psfMag - ((deV_L>exp_L)?deVMag:expMag) > PHOTO_GALAXY_CLASS_LIMIT));
	class = class && (objcType == OBJ_GALAXY);
	class = (objcType == OBJ_GALAXY);
	return(class);

}

int MSersicExpGalaxyClassifier(PHPROPS *p) {
char *name = "MSersicExpGalaxyClassifier";
	if (p == NULL) {
		thError("%s: ERROR - null input, cannot classify", name);
		return(0);
	}

	THPIX psf_L, deV_L, exp_L;
	THPIX psfMag, deVMag, expMag;
	OBJ_TYPE objcType;
	RET_CODE status;
 	status = derive_class_info(p, 
				&psf_L, &deV_L, &exp_L, 
				&psfMag, &deVMag, &expMag,
				&objcType);
	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not derive classification information", name);
		return(status);
	}


	int class;
	class = ((psfMag - ((deV_L>exp_L)?deVMag:expMag) > PHOTO_GALAXY_CLASS_LIMIT));
	class = class && (objcType == OBJ_GALAXY);
	class = (objcType == OBJ_GALAXY);
	return(class);

}

int MSersicSersicGalaxyClassifier(PHPROPS *p) {
char *name = "MSersicSersicGalaxyClassifier";
	if (p == NULL) {
		thError("%s: ERROR - null input, cannot classify", name);
		return(0);
	}

	THPIX psf_L, deV_L, exp_L;
	THPIX psfMag, deVMag, expMag;
	OBJ_TYPE objcType;
	RET_CODE status;
 	status = derive_class_info(p, 
				&psf_L, &deV_L, &exp_L, 
				&psfMag, &deVMag, &expMag,
				&objcType);
	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not derive classification information", name);
		return(status);
	}


	int class;
	class = ((psfMag - ((deV_L>exp_L)?deVMag:expMag) > PHOTO_GALAXY_CLASS_LIMIT));
	class = class && (objcType == OBJ_GALAXY);
	class = (objcType == OBJ_GALAXY);
	return(class);

}

int MCoresersicGalaxyClassifier(PHPROPS *p) {
char *name = "MCoresersicGalaxyClassifier";
	if (p == NULL) {
		thError("%s: ERROR - null input, cannot classify", name);
		return(0);
	}

	THPIX psf_L, deV_L, exp_L;
	THPIX psfMag, deVMag, expMag;
	OBJ_TYPE objcType;
	RET_CODE status;
 	status = derive_class_info(p, 
				&psf_L, &deV_L, &exp_L, 
				&psfMag, &deVMag, &expMag,
				&objcType);
	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not derive classification information", name);
		return(status);
	}

	int class;
	class = ((psfMag - ((deV_L>exp_L)?deVMag:expMag) > PHOTO_GALAXY_CLASS_LIMIT));
	class = class && (objcType == OBJ_GALAXY);
	class = (objcType == OBJ_GALAXY);
	return(class);

}

int MStarClassifier(PHPROPS *p) {
char *name = "MStarClassifier";
	if (p == NULL) return(0);
	RET_CODE status;
	WOBJC_IO *pobjc = NULL;
	int *bndex = NULL;
	status = thPhpropsGet(p, &pobjc, &bndex, NULL, NULL, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (pobjc) from (p)", name);
		return(0);
	}
	return(pobjc->objc_type == OBJ_STAR);
}

int MStarGalaxyClassifier(PHPROPS *p) {
char *name = "MStarGalaxyClassifier";
	if (p == NULL) {
		thError("%s: ERROR - null input, cannot classify", name);
		return(0);
	}

	THPIX psf_L, deV_L, exp_L;
	THPIX psfMag, deVMag, expMag;
	OBJ_TYPE objcType;
	RET_CODE status;
 	status = derive_class_info(p, 
				&psf_L, &deV_L, &exp_L, 
				&psfMag, &deVMag, &expMag, 
				&objcType);
	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not derive classification information", name);
		return(status);
	}

	int class;
	class = (((psfMag - (deV_L>exp_L)?deVMag:expMag) > PHOTO_STAR_CLASS_LIMIT) && 
		((psfMag - (deV_L>exp_L)?deVMag:expMag) < PHOTO_GALAXY_CLASS_LIMIT));
	class = class && (objcType == OBJ_GALAXY || objcType == OBJ_STAR);
	return(class);
}

int MIdentityClassifier(PHPROPS *p) {
	return(p != NULL);
}

int PHGalaxyClassifier(PHPROPS *p) {
	char *name = "PHGalaxyClassifier";
	if (p == NULL) return(0);
	RET_CODE status;
	WOBJC_IO *pobjc = NULL;
	int *bndex = NULL;
	status = thPhpropsGet(p, &pobjc, &bndex, NULL, NULL, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (pobjc) from (p)", name);
		return(0);
	}
	return(pobjc->objc_type == OBJ_GALAXY);
}
/* initiators */

THPIX magnitude(THPIX f, int band) {
char *name = "magnitude";
THPIX m;
double f0, b;
shAssert(band >= 0 && band <= 4);
if (band == 0) { /* u-band */
 	f0 = F0_U; 
	b = B_U;
} else if (band == 1) {
	f0 = F0_G;
	b = B_G;
} else if (band == 2) {	
	f0 = F0_R;
	b = B_R;
} else if (band == 3) {
	f0 = F0_I;
	b = B_I;
} else if (band == 4) {
	f0 = F0_Z;
	b = B_Z;
} else {
	thError("%s: ERROR - unsupported bandi (%d)", name, band);
	return(THNAN);
}
m = (THPIX) (-((double) 2.5/log((double) 10.0))*(asinh((f/(f0 * 2.0 * b)))+log(b)));
return(m);
}

static THPIX ellipticity(THPIX ab) {

if (ab == VALUE_IS_BAD) return((THPIX) DEFAULT_ELLIPTICITY);

THPIX e = ((ab >= (THPIX) 1.0 || ab <= (THPIX) -1.0)?(sqrt(ab * ab - 1.0) / ab):(sqrt(1.0 - ab * ab)));
if (-e > (THPIX) NARROWEST_ELLIPTICITY) {
	e = - (THPIX) NARROWEST_ELLIPTICITY;
} else if (e > NARROWEST_ELLIPTICITY) {
	e = (THPIX) NARROWEST_ELLIPTICITY;
}
#if DEBUG_MGALAXY
char *name = "ellipticity";
printf("%s: ab = %g, e = %g \n", name, ab, e);
#endif

return(e);
}

static THPIX effectiveradius(THPIX r, THPIX ab, THPIX small, THPIX large) {
#if DEEP_DEBUG_MGALAXY
char *name = "effectiveradius";
printf("%s: r = %7.2g, small = %7.2g, large = %7.2g \n", name, r, small, large);
#endif
if (ab == VALUE_IS_BAD) ab = 1.0;
if (ab > 1.0) ab = 1.0 / ab;
shAssert(ab >= 0.0 && ab <= 1.0);
shAssert(r == r && small == small && large == large);
shAssert(large > small);

THPIX reduced = r * sqrt(ab); /* added on June 26, 2016 */
if (reduced < small) return(small);
if (reduced > large) return(large);
return(reduced);
}

static RET_CODE canonize_gaussian(GAUSSIANPARS *q) {
char *name = "canonize_gaussian";
if (q == NULL) {
	thError("%s: WARNING - null (gaussianpars)", name);
	return(SH_SUCCESS);
}
THPIX c, s, sigmax, sigmay, f, r;
r = q->re;
c = cos(q->phi);
s = sin(q->phi);
f = pow(1.0 - pow(q->e, 2), 0.25);
sigmay = r / f;
sigmax = r * f;
q->a = 0.5 * (c * c / sigmax / sigmax + s * s / sigmay / sigmay);
q->b = 0.5 * c * s * (-1.0 / sigmax / sigmax + 1.0 / sigmay / sigmay);
q->c = 0.5 * (s * s / sigmax / sigmax + c * c / sigmay / sigmay);
q->sigmax = sigmax;
q->sigmay = sigmay;

return(SH_SUCCESS);
}


RET_CODE MGInitStar(PHPROPS *p, void *q, int band) {
char *name = "MGInitStar";
	if (q == NULL) {
		thError("%s: WARNING - null placeholder", name);
		return(SH_SUCCESS);
	}
	if (p == NULL) {
		thError("%s: ERROR - null PHOTO property list", name);
		return(SH_GENERIC_ERROR);
	}
	WOBJC_IO *pobjc = NULL;
	int *bndex = NULL;
	RET_CODE status;
	status = thPhpropsGet(p, &pobjc, &bndex, NULL, NULL, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (pobjc) from (p)", name);
		return(status);
	}
	if (bndex == NULL) {
		thError("%s: ERROR - null (bndex array) from (phprops)", name);
		return(SH_GENERIC_ERROR);
	}
	STARPARS *t;
	t = (STARPARS *) q;
	int ncolor;
	t->band = band;
	status = thPhpropsGetNcolor(p, &ncolor);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract (ncolor) from 'PHPROPS'", name);
		return(status);
	}
	if (ncolor <= 0) {
		thError("%s: ERROR - unacceptable (ncolor = %d) value in 'PHPROPS'",
		name, ncolor);
		return(SH_GENERIC_ERROR);
	} else if (band < 0 || band >= NCOLOR) {
		thError("%s: ERROR - PHOTO properties are only listed for (%d) bands, while searching for band (%d)", name, NCOLOR, band);
		return(SH_GENERIC_ERROR);
	}
	int index = bndex[band];
	if (index < 0 || index >= NCOLOR) {
		thError("%s: ERROR - index listed in (bndex array) for (band = %d) is out of bound", name, band);
		return(SH_GENERIC_ERROR);
	}
	t->I = (THPIX) (pobjc->psfCounts[index]);
	t->xc = (THPIX) (pobjc->objc_rowc);
	t->yc = (THPIX) (pobjc->objc_colc);
	t->mcount = (THPIX) 1.0;
	t->counts = (THPIX) (pobjc->psfCounts[index]);

	/* COMMENT: added on Oct 1, 2015 to test the bias in position of stars observed in PHOTO ouput created for my sims 
	t->xc = t->xc + 0.5;
	Deleted on Oct 1, 2015
	*/


	return(SH_SUCCESS);
}

RET_CODE MGInitExp(PHPROPS *p, void *q, int band) {
char *name = "MGInitExp";
	if (q == NULL) {
		thError("%s: WARNING - null placeholder", name);
		return(SH_SUCCESS);
	}
	if (p == NULL) {
		thError("%s: ERROR - null PHOTO property list", name);
		return(SH_GENERIC_ERROR);
	}
	WOBJC_IO *pobjc = NULL;
	int *bndex = NULL;
	RET_CODE status;
	status = thPhpropsGet(p, &pobjc, &bndex, NULL, NULL, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (pobjc) from (p)", name);
		return(status);
	}
	EXPPARS *t;
	t = (EXPPARS *) q;
	int ncolor;
	t->band = band;
	status = thPhpropsGetNcolor(p, &ncolor);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract (ncolor) from 'PHPROPS'", name);
		return(status);
	}
	if (ncolor <= 0) {
		thError("%s: ERROR - unacceptable (ncolor = %d) value in 'PHPROPS'",
		name, ncolor);
		return(SH_GENERIC_ERROR);
	} else if (band < 0 || band >= NCOLOR) {
		thError("%s: ERROR - PHOTO properties are only listed for (%d) bands, while searching for band (%d)", name, NCOLOR, band);
		return(SH_GENERIC_ERROR);
	}
	int index = bndex[band];
	if (index < 0 || index >= NCOLOR) {
		thError("%s: ERROR - index (%d) found in (bndex array) for (band = %d) is out of bounds", name, index, band);
		return(SH_GENERIC_ERROR);
	}
	THPIX ab_exp = pobjc->ab_exp[index];
	t->xc = (THPIX) pobjc->objc_rowc;
	t->yc = (THPIX) pobjc->objc_colc;
	t->re = effectiveradius((THPIX) pobjc->r_exp[index], ab_exp, EXP_RE_SMALL, EXP_RE_LARGE);
	#if INIT_MODEL_WITH_DEV_FRACTION	
	t->counts = (THPIX) (pobjc->counts_exp[index] * (1.0 - pobjc->fracPSF[index]));
	#else
	t->counts = (THPIX) (pobjc->counts_exp[index]);
	#endif
	t->mcount =  ((THPIX) EXP_MCOUNT * pow(t->re, 2.0)); /* readjusted on june 26, 2016  * ab_exp); */
	t->I = t->counts / t->mcount;
	t->e = (THPIX) ellipticity(ab_exp);
	if (t->e >= (THPIX) 1.0 || t->e <= (THPIX) -1.0) {
		thError("%s: ERROR - problematic ellipticity (e = %g) for (ab = %g)", name, t->e, pobjc->ab_exp[index]);
		return(SH_GENERIC_ERROR);
	}
	t->phi = RADIAN_DEGREE_RATIO * ((THPIX) pobjc->phi_exp[index]);	
	status = thCTransformFromPhoto(t, "EXPPARS");
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not perform generic coordinate transform on 'EXPPARS'", name);
		return(status);
	}
	return(SH_SUCCESS);

}

#if 1

RET_CODE MGInitExp1(PHPROPS *p, void *q, int band) {
char *name = "MGInitExp1";
	if (q == NULL) {
		thError("%s: WARNING - null placeholder", name);
		return(SH_SUCCESS);
	}
	if (p == NULL) {
		thError("%s: ERROR - null PHOTO property list", name);
		return(SH_GENERIC_ERROR);
	}
	WOBJC_IO *pobjc = NULL;
	int *bndex = NULL;
	RET_CODE status;
	status = thPhpropsGet(p, &pobjc, &bndex, NULL, NULL, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (pobjc) from (p)", name);
		return(status);
	}
	EXPPARS *t;
	t = (EXPPARS *) q;
	int ncolor;
	t->band = band;
	status = thPhpropsGetNcolor(p, &ncolor);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract (ncolor) from 'PHPROPS'", name);
		return(status);
	}
	if (ncolor <= 0) {
		thError("%s: ERROR - unacceptable (ncolor = %d) value in 'PHPROPS'",
		name, ncolor);
		return(SH_GENERIC_ERROR);
	} else if (band < 0 || band >= NCOLOR) {
		thError("%s: ERROR - PHOTO properties are only listed for (%d) bands, while searching for band (%d)", name, NCOLOR, band);
		return(SH_GENERIC_ERROR);
	}
	THPIX expMag, deVMag;
	status = derive_class_info(p, NULL, NULL, NULL, NULL, &deVMag, &expMag, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not derive classification information", name);
		return(status);
	}
	int index = bndex[band];
	t->xc = (THPIX) pobjc->objc_rowc;
	t->yc = (THPIX) pobjc->objc_colc;
	/* note that the effective radius that PHOTO finds in semi major effective radius */
	if (MGalaxyMode == MLE_MODE || MGalaxyMode == MLE_MODE_PLUS || MGalaxyMode == MLE_ON_DEMAND_MODE) {

	THPIX ab_deV = pobjc->ab_deV[index];
	if (ab_deV > 1.0) ab_deV = 1.0 / ab_deV;
	THPIX ab_exp = pobjc->ab_exp[index];
	if (ab_exp > 1.0) ab_exp = 1.0 / ab_exp;
	THPIX re_from_dev = effectiveradius(EXP1_DEV_RATIO * pobjc->r_deV[index], pobjc->ab_deV[index], EXP1_RE_SMALL, EXP1_RE_LARGE);
	THPIX re_from_exp = effectiveradius(EXP1_EXP_RATIO * pobjc->r_exp[index], pobjc->ab_exp[index], EXP1_RE_SMALL, EXP1_RE_LARGE);
	THPIX re_big = MAX(re_from_dev, re_from_exp);
	THPIX re_small = MIN(re_from_dev, re_from_exp);
	THPIX ratio = MAX(sqrt(re_big / re_small), EXP2_EXP1_INIT_RATIO);
	re_big = re_big * ratio;
	re_small = re_small / ratio;
	if (re_big > EXP1_RE_LARGE) re_big = EXP1_RE_LARGE;
	if (re_big < EXP1_RE_SMALL) re_big = EXP1_RE_SMALL;
	if (re_small > EXP1_RE_LARGE) re_small = EXP1_RE_LARGE;
	if (re_small < EXP1_RE_SMALL) re_small = EXP1_RE_SMALL;
	shAssert(re_small < re_big);
	t->re = re_small;
	if (re_from_dev <= re_from_exp) {
		t->e = ellipticity(pobjc->ab_deV[index]);
		t->phi = RADIAN_DEGREE_RATIO * (pobjc->phi_deV[index]);
	} else {
		t->e = ellipticity(pobjc->ab_exp[index]);
		t->phi = RADIAN_DEGREE_RATIO * (pobjc->phi_exp[index]);
	}

	#if INIT_MODEL_WITH_DEV_FRACTION	
	if (re_from_dev <= re_from_exp) {
		t->counts = (THPIX) (pobjc->counts_deV[index] * pobjc->fracPSF[index]);
	} else {	
		t->counts = (THPIX) (pobjc->counts_exp[index] * ((float) 1.0 - pobjc->fracPSF[index]));
	}
	#else
	if (re_from_dev >= re_from_exp) {
		t->counts = (THPIX) (pobjc->counts_deV[index]);
	} else {	
		t->counts = (THPIX) (pobjc->counts_exp[index]);
	}
	#endif
	} else if (MGalaxyMode == SIM_MODE) {
	#if USE_FNAL_OBJC_IO	
	t->re = effectiveradius(pobjc->r_exp[index], pobjc->ab_exp[index], EXP1_RE_SMALL, EXP1_RE_LARGE);
	t->counts = (THPIX) (pobjc->counts_exp[index]);
	t->e = ellipticity(pobjc->ab_exp[index]);
	t->phi = RADIAN_DEGREE_RATIO * (pobjc->phi_exp[index]); 
	#else
	thError("%s: ERROR - 'OBJC_IO' not supported in 'SIM_MODE'", name);
	return(SH_GENERIC_ERROR);
	#endif

	} else {
		thError("%s: ERROR - unsupported run mode for MGalaxy module", name);
		return(SH_GENERIC_ERROR);
	}



t->mcount =  (THPIX) EXP_MCOUNT * pow(t->re, 2.0); /* readjusted on june 26, 2016 * pobjc->ab_deV[index]; */
t->I = (t->counts) / t->mcount;
status = thCTransformFromPhoto(t, "EXPPARS");
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not perform generic coordinate transform on 'EXPPARS'", name);
	return(status);
}

return(SH_SUCCESS);

}

RET_CODE MGInitExp2(PHPROPS *p, void *q, int band) {
char *name = "MGInitExp2";
	if (q == NULL) {
		thError("%s: WARNING - null placeholder", name);
		return(SH_SUCCESS);
	}
	if (p == NULL) {
		thError("%s: ERROR - null PHOTO property list", name);
		return(SH_GENERIC_ERROR);
	}
	WOBJC_IO *pobjc = NULL;
	int *bndex = NULL;
	RET_CODE status;
	status = thPhpropsGet(p, &pobjc, &bndex, NULL, NULL, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (pobjc) from (p)", name);
		return(status);
	}
	EXPPARS *t;
	t = (EXPPARS *) q;
	int ncolor;
	t->band = band;
	status = thPhpropsGetNcolor(p, &ncolor);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract (ncolor) from 'PHPROPS'", name);
		return(status);
	}
	if (ncolor <= 0) {
		thError("%s: ERROR - unacceptable (ncolor = %d) value in 'PHPROPS'",
		name, ncolor);
		return(SH_GENERIC_ERROR);
	} else if (band < 0 || band >= NCOLOR) {
		thError("%s: ERROR - PHOTO properties are only listed for (%d) bands, while searching for band (%d)", name, NCOLOR, band);
		return(SH_GENERIC_ERROR);
	}
	THPIX expMag, deVMag;
	status = derive_class_info(p, NULL, NULL, NULL, NULL, &deVMag, &expMag, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not derive classification information", name);
		return(status);
	}
	int index = bndex[band];
	t->xc = (THPIX) pobjc->objc_rowc;
	t->yc = (THPIX) pobjc->objc_colc;
	/* note that the effective radius that PHOTO finds in semi major effective radius */
	if (MGalaxyMode == MLE_MODE || MGalaxyMode == MLE_MODE_PLUS || MGalaxyMode == MLE_ON_DEMAND_MODE) {

	THPIX ab_deV = pobjc->ab_deV[index];
	if (ab_deV > 1.0) ab_deV = 1.0 / ab_deV;
	THPIX ab_exp = pobjc->ab_exp[index];
	if (ab_exp > 1.0) ab_exp = 1.0 / ab_exp;
	THPIX re_from_dev = effectiveradius(EXP2_DEV_RATIO * pobjc->r_deV[index], pobjc->ab_deV[index], EXP2_RE_SMALL, EXP2_RE_LARGE);
	THPIX re_from_exp = effectiveradius(EXP2_EXP_RATIO * pobjc->r_exp[index], pobjc->ab_exp[index], EXP2_RE_SMALL, EXP2_RE_LARGE);
	THPIX re_big = MAX(re_from_dev, re_from_exp);
	THPIX re_small = MIN(re_from_dev, re_from_exp);
	THPIX ratio = MAX(sqrt(re_big / re_small), EXP2_EXP1_INIT_RATIO);
	re_big = re_big * ratio;
	re_small = re_small / ratio;
	if (re_big > EXP2_RE_LARGE) re_big = EXP2_RE_LARGE;
	if (re_big < EXP2_RE_SMALL) re_big = EXP2_RE_SMALL;
	if (re_small > EXP2_RE_LARGE) re_small = EXP2_RE_LARGE;
	if (re_small < EXP2_RE_SMALL) re_small = EXP2_RE_SMALL;
	shAssert(re_small < re_big);
	t->re = re_big;
	if (re_from_dev >= re_from_exp) {
		t->e = ellipticity(pobjc->ab_deV[index]);
		t->phi = RADIAN_DEGREE_RATIO * (pobjc->phi_deV[index]);
	} else {
		t->e = ellipticity(pobjc->ab_exp[index]);
		t->phi = RADIAN_DEGREE_RATIO * (pobjc->phi_exp[index]);
	}

	#if INIT_MODEL_WITH_DEV_FRACTION	
	if (re_from_dev >= re_from_exp) {
		t->counts = (THPIX) (pobjc->counts_deV[index] * pobjc->fracPSF[index]);
	} else {	
		t->counts = (THPIX) (pobjc->counts_exp[index] * ((float) 1.0 - pobjc->fracPSF[index]));
	}
	#else
	if (re_from_dev >= re_from_exp) {
		t->counts = (THPIX) (pobjc->counts_deV[index]);
	} else {	
		t->counts = (THPIX) (pobjc->counts_exp[index]);
	}
	#endif
	} else if (MGalaxyMode == SIM_MODE) {
	#if USE_FNAL_OBJC_IO	
	t->re = effectiveradius(pobjc->r_exp2[index], pobjc->ab_exp2[index], EXP2_RE_SMALL, EXP2_RE_LARGE);
	t->counts = (THPIX) (pobjc->counts_deV2[index]);
	t->e = ellipticity(pobjc->ab_deV2[index]);
	t->phi = RADIAN_DEGREE_RATIO * (pobjc->phi_deV2[index]); 
	#else
	thError("%s: ERROR - 'OBJC_IO' not supported in 'SIM_MODE'", name);
	return(SH_GENERIC_ERROR);
	#endif

	} else {
		thError("%s: ERROR - unsupported run mode for MGalaxy module", name);
		return(SH_GENERIC_ERROR);
	}



t->mcount =  (THPIX) EXP_MCOUNT * pow(t->re, 2.0); /* readjusted on june 26, 2016 * pobjc->ab_deV[index]; */
t->I = (t->counts) / t->mcount;
status = thCTransformFromPhoto(t, "EXPPARS");
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not perform generic coordinate transform on 'EXPPARS'", name);
	return(status);
}

return(SH_SUCCESS);

}


#else

RET_CODE MGInitExp2(PHPROPS *p, void *q, int band) {
char *name = "MGInitExp2";
	if (q == NULL) {
		thError("%s: WARNING - null placeholder", name);
		return(SH_SUCCESS);
	}
	if (p == NULL) {
		thError("%s: ERROR - null PHOTO property list", name);
		return(SH_GENERIC_ERROR);
	}
	WOBJC_IO *pobjc = NULL;
	int *bndex = NULL;
	RET_CODE status;
	status = thPhpropsGet(p, &pobjc,&bndex,  NULL, NULL, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (pobjc) from (p)", name);
		return(status);
	}
	EXPPARS *t;
	t = (EXPPARS *) q;
	int ncolor;
	t->band = band;
	status = thPhpropsGetNcolor(p, &ncolor);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract (ncolor) from 'PHPROPS'", name);
		return(status);
	}
	if (ncolor <= 0) {
		thError("%s: ERROR - unacceptable (ncolor = %d) value in 'PHPROPS'",
		name, ncolor);
		return(SH_GENERIC_ERROR);
	} else if (band < 0 || band >= NCOLOR) {
		thError("%s: ERROR - PHOTO properties are only listed for (%d) bands, while searching for band (%d)", name, NCOLOR, band);
		return(SH_GENERIC_ERROR);
	}
	THPIX expMag, deVMag;
	status = derive_class_info(p, NULL, NULL, NULL, NULL, &deVMag, &expMag, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not derive classification information", name);
		return(status);
	}
	int index = bndex[band];
	if (index < 0 || index >= NCOLOR) {
		thError("%s: ERROR - index (%d) found in (bndex array) for (band = %d) is out of bounds", name, band, index);
		return(SH_GENERIC_ERROR);
	}
	t->xc = (THPIX) pobjc->objc_rowc;
	t->yc = (THPIX) pobjc->objc_colc;
	/* note that the effective radius that PHOTO finds in semi major effective radius */
	THPIX ab_deV = pobjc->ab_deV[index];
	if (ab_deV > 1.0) ab_deV = 1.0 / ab_deV;
	THPIX ab_exp = pobjc->ab_exp[index];
	if (ab_exp > 1.0) ab_exp = 1.0 / ab_exp;
	t->re = effectiveradius((expMag > deVMag)?(EXP2_DEV_RATIO * pobjc->r_deV[index]):(EXP2_EXP_RATIO * pobjc->r_exp[index]), 
		(expMag > deVMag)? (pobjc->ab_deV[index]):(pobjc->ab_exp[index]), 
		EXP2_RE_SMALL, EXP2_RE_LARGE);
	t->mcount = ((THPIX) EXP_MCOUNT * pow(t->re, 2.0)); /* readjusted on june 24, 2016 * pobjc->ab_exp[index]); */	
	t->I = ((THPIX) pobjc->counts_exp[index]) / t->mcount; 
	t->e = (expMag > deVMag)?
		(ellipticity(pobjc->ab_deV[index])):
		(ellipticity(pobjc->ab_exp[index]));

	t->phi = RADIAN_DEGREE_RATIO * ((THPIX) ((expMag > deVMag)?
		(pobjc->phi_deV[index]):
		(pobjc->phi_exp[index])));
	status = thCTransformFromPhoto(t, "EXPPARS");
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not perform generic coordinate transform on 'EXPPARS'", name);
		return(status);
	}
	t->counts = (THPIX) ((expMag > deVMag)?
		(pobjc->counts_deV[index]):
		(pobjc->counts_exp[index]));

return(SH_SUCCESS);

}

#endif

RET_CODE MGInitGaussian(PHPROPS *p, void *q, int band) {
char *name = "MGInitGaussian";
	if (q == NULL) {
		thError("%s: WARNING - null placeholder", name);
		return(SH_SUCCESS);
	}
	if (p == NULL) {
		thError("%s: ERROR - null PHOTO property list", name);
		return(SH_GENERIC_ERROR);
	}
	WOBJC_IO *pobjc = NULL;
	int *bndex = NULL;
	RET_CODE status;
	status = thPhpropsGet(p, &pobjc, &bndex, NULL, NULL, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (pobjc) from (p)", name);
		return(status);
	}

	GAUSSIANPARS *t;
	t = (GAUSSIANPARS *) q;
	int ncolor;
	t->band = band;
	status = thPhpropsGetNcolor(p, &ncolor);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract (ncolor) from 'PHPROPS'", name);
		return(status);
	}
	if (ncolor <= 0) {
		thError("%s: ERROR - unacceptable (ncolor = %d) value in 'PHPROPS'",
		name, ncolor);
		return(SH_GENERIC_ERROR);
	} else if (band < 0 || band >= NCOLOR) {
		thError("%s: ERROR - PHOTO properties are only listed for (%d) bands, while searching for band (%d)", name, NCOLOR, band);
		return(SH_GENERIC_ERROR);
	}
	THPIX expMag, deVMag;
	status = derive_class_info(p, NULL, NULL, NULL, NULL, &deVMag, &expMag, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not derive classification information", name);
		return(status);
	}
	int index = bndex[band];
	if (index < 0 || index >= NCOLOR) {
		thError("%s: ERROR - index (%d) found in (bndex array) for (band = %d) is out of bounds", name, index, band);
		return(SH_GENERIC_ERROR);
	}
	t->xc = pobjc->objc_rowc;
	t->yc = pobjc->objc_colc;
	/* note that the effective radius that PHOTO finds in semi major effective radius */
	THPIX ab_deV = pobjc->ab_deV[index];
	if (ab_deV > 1.0) ab_deV = 1.0 / ab_deV;
	THPIX ab_exp = pobjc->ab_exp[index];
	if (ab_exp > 1.0) ab_exp = 1.0 / ab_exp;
	t->re = effectiveradius((expMag > deVMag)?(GAUSSIAN_DEV_RATIO * pobjc->r_deV[index]):(GAUSSIAN_EXP_RATIO * pobjc->r_exp[index]), 
		(expMag > deVMag)?(pobjc->ab_deV[index]):(pobjc->ab_exp[index]), 
		GAUSSIAN_RE_SMALL, GAUSSIAN_RE_LARGE);
 	t->e = (expMag > deVMag)?
		(ellipticity(pobjc->ab_deV[index])):
		(ellipticity(pobjc->ab_exp[index]));
	t->phi = (expMag > deVMag)?
		(pobjc->phi_deV[index]):
		(pobjc->phi_exp[index]);
	/* this produces a, b, c parameters from re, phi, e */
	canonize_gaussian(t);
	status = thCTransformFromPhoto(t, "GAUSSIANPARS");
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not perform generic coordinate transform on 'GAUSSIANPARS'", name);
		return(status);
	}
	t->counts = (THPIX) ((expMag > deVMag)?
		(pobjc->counts_deV[index]):
		(pobjc->counts_exp[index]));

	return(SH_SUCCESS);
}

RET_CODE MGInitDeV(PHPROPS *p, void *q, int band) {
char *name = "MGInitDeV";
	if (q == NULL) {
		thError("%s: WARNING - null placeholder", name);
		return(SH_SUCCESS);
	}
	if (p == NULL) {
		thError("%s: ERROR - null PHOTO property list", name);
		return(SH_GENERIC_ERROR);
	}
	WOBJC_IO *pobjc = NULL;
	int *bndex = NULL;
	RET_CODE status;
	status = thPhpropsGet(p, &pobjc, &bndex, NULL, NULL, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (pobjc) from (p)", name);
		return(status);
	}
	DEVPARS *t;
	t = (DEVPARS *) q;
	int ncolor;
	t->band = band;
	status = thPhpropsGetNcolor(p, &ncolor);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract (ncolor) from 'PHPROPS'", name);
		return(status);
	}
	if (ncolor <= 0) {
		thError("%s: ERROR - unacceptable (ncolor = %d) value in 'PHPROPS'",
		name, ncolor);
		return(SH_GENERIC_ERROR);
	} else if (band < 0 || band >= NCOLOR) {
		thError("%s: ERROR - PHOTO properties are only listed for (%d) bands, while searching for band (%d)", name, NCOLOR, band);
		return(SH_GENERIC_ERROR);
	}
	int index = bndex[band];
	if (index < 0 || index >= NCOLOR) {
		thError("%s: ERROR - index (%d) found in (bndex array) for (band = %d) is out of bounds", name, index, band);
		return(SH_GENERIC_ERROR);
	}
	t->xc = (THPIX) pobjc->objc_rowc;
	t->yc = (THPIX) pobjc->objc_colc;
	/* note that the effective radius that PHOTO finds in semi major effective radius */
	THPIX ab_deV = pobjc->ab_deV[index];
	if (ab_deV > 1.0) ab_deV = 1.0 / ab_deV;
	t->re = effectiveradius((THPIX) pobjc->r_deV[index], ab_deV, DEV_RE_SMALL, DEV_RE_LARGE);
	t->mcount =  ((THPIX) DEV_MCOUNT * pow(t->re, 2.0)); /* readjusted on june 26, 2016  * pobjc->ab_deV[index]);	 */
	#if INIT_MODEL_WITH_DEV_FRACTION	
	t->counts = (THPIX) (pobjc->counts_deV[index] * pobjc->fracPSF[index]);
	#else
	t->counts = (THPIX) (pobjc->counts_deV[index]);
	#endif
	t->I = t->counts / t->mcount;
	t->e = (THPIX) ellipticity(pobjc->ab_deV[index]);
	t->phi = RADIAN_DEGREE_RATIO * ((THPIX) pobjc->phi_deV[index]);
	status = thCTransformFromPhoto(t, "DEVPARS");
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not perform generic coordinate transform on 'DEVPARS'", name);
		return(status);
	}
	return(SH_SUCCESS);
}

RET_CODE MGInitDeV1(PHPROPS *p, void *q, int band) {
char *name = "MGInitDeV1";
	if (q == NULL) {
		thError("%s: WARNING - null placeholder", name);
		return(SH_SUCCESS);
	}
	if (p == NULL) {
		thError("%s: ERROR - null PHOTO property list", name);
		return(SH_GENERIC_ERROR);
	}
	WOBJC_IO *pobjc = NULL;
	int *bndex = NULL;
	RET_CODE status;
	status = thPhpropsGet(p, &pobjc, &bndex, NULL, NULL, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (pobjc) from (p)", name);
		return(status);
	}
	DEVPARS *t;
	t = (DEVPARS *) q;
	int ncolor;
	t->band = band;
	status = thPhpropsGetNcolor(p, &ncolor);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract (ncolor) from 'PHPROPS'", name);
		return(status);
	}
	if (ncolor <= 0) {
		thError("%s: ERROR - unacceptable (ncolor = %d) value in 'PHPROPS'",
		name, ncolor);
		return(SH_GENERIC_ERROR);
	} else if (band < 0 || band >= NCOLOR) {
		thError("%s: ERROR - PHOTO properties are only listed for (%d) bands, while searching for band (%d)", name, NCOLOR, band);
		return(SH_GENERIC_ERROR);
	}
	THPIX expMag, deVMag;
	status = derive_class_info(p, NULL, NULL, NULL, NULL, &deVMag, &expMag, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not derive classification information", name);
		return(status);
	}
	int index = bndex[band];
	t->xc = (THPIX) pobjc->objc_rowc;
	t->yc = (THPIX) pobjc->objc_colc;
	/* note that the effective radius that PHOTO finds in semi major effective radius */
	if (MGalaxyMode == MLE_MODE || MGalaxyMode == MLE_MODE_PLUS) { /* MLE_ON_DEMAND_MODE addressed separately */

	THPIX ab_deV = pobjc->ab_deV[index];
	if (ab_deV > 1.0) ab_deV = 1.0 / ab_deV;
	THPIX ab_exp = pobjc->ab_exp[index];
	if (ab_exp > 1.0) ab_exp = 1.0 / ab_exp;
	THPIX re_from_dev = effectiveradius(DEV1_DEV_RATIO * pobjc->r_deV[index], pobjc->ab_deV[index], DEV1_RE_SMALL, DEV1_RE_LARGE);
	THPIX re_from_exp = effectiveradius(DEV1_EXP_RATIO * pobjc->r_exp[index], pobjc->ab_exp[index], DEV1_RE_SMALL, DEV1_RE_LARGE);
	THPIX re_big = MAX(re_from_dev, re_from_exp);
	THPIX re_small = MIN(re_from_dev, re_from_exp);
	THPIX ratio = MAX(sqrt(re_big / re_small), DEV2_DEV1_INIT_RATIO);
	re_big = re_big * ratio;
	re_small = re_small / ratio;
	if (re_big > DEV1_RE_LARGE) re_big = DEV1_RE_LARGE;
	if (re_big < DEV1_RE_SMALL) re_big = DEV1_RE_SMALL;
	if (re_small > DEV1_RE_LARGE) re_small = DEV1_RE_LARGE;
	if (re_small < DEV1_RE_SMALL) re_small = DEV1_RE_SMALL;
	re_small = MIN(re_small, DEV1_INIT_RADIUS);
	re_big = MAX(re_big, DEV2_INIT_RADIUS);
	shAssert(re_small < re_big);
	#if INIT_MLE_DEV1_WITH_EXACT_PHOTO
		t->re = re_from_dev;
		t->e = ellipticity(pobjc->ab_deV[index]);
		t->phi = RADIAN_DEGREE_RATIO * (pobjc->phi_deV[index]);
		#if INIT_MODEL_WITH_DEV_FRACTION	
		t->counts = (THPIX) (pobjc->counts_deV[index] * pobjc->fracPSF[index]);
		#else
		t->counts = (THPIX) (pobjc->counts_deV[index]);
		#endif
	#else
		t->re = re_small;
		if (re_from_dev <= re_from_exp) {
			t->e = ellipticity(pobjc->ab_deV[index]);
			t->phi = RADIAN_DEGREE_RATIO * (pobjc->phi_deV[index]);
		} else {
			t->e = ellipticity(pobjc->ab_exp[index]);
			t->phi = RADIAN_DEGREE_RATIO * (pobjc->phi_exp[index]);
		}
		#if INIT_MODEL_WITH_DEV_FRACTION	
		if (re_from_dev <= re_from_exp) {
			t->counts = (THPIX) (pobjc->counts_deV[index] * pobjc->fracPSF[index]);
		} else {	
			t->counts = (THPIX) (pobjc->counts_exp[index] * ((float) 1.0 - pobjc->fracPSF[index]));
		}
		#else
		if (re_from_dev <= re_from_exp) {
			t->counts = (THPIX) (pobjc->counts_deV[index]);
		} else {	
			t->counts = (THPIX) (pobjc->counts_exp[index]);
		}
		#endif
	#endif
	} else if (MGalaxyMode == SIM_MODE) {
	#if USE_FNAL_OBJC_IO
	if (pobjc->r_deV[index] >= CD_RCORE_RANDOMIZE_THRESH) {
		#if CD_RCORE_LOGNORMAL_TAIL
		int ntry = 0;
		float r_dev_io = pobjc->r_deV[index];
		while (ntry >= 0 && ntry <= CD_NTRY_MAX) {
			ntry++;
			pobjc->r_deV[index] = r_dev_io * exp(CD_RCORE_RANDOMIZE_LOGSIGMA * phGaussdev()); 	
			if (pobjc->r_deV[index] <= (float) CD_R_MAX) ntry = -1;
		}
		#else
		pobjc->r_deV[index] += pobjc->r_deVErr[index] * CD_RCORE_RANDOMIZE_NSIGMA * phGaussdev(); 	
		if (pobjc->r_deV[index]  <= 0.0) pobjc->r_deV[index] = DEV1_RE_SMALL;
		#endif
	}
	t->re = effectiveradius(pobjc->r_deV[index], pobjc->ab_deV[index], DEV1_RE_SMALL, DEV1_RE_LARGE);
	t->counts = (THPIX) (pobjc->counts_deV[index]);
	t->e = ellipticity(pobjc->ab_deV[index]);
	t->phi = RADIAN_DEGREE_RATIO * (pobjc->phi_deV[index]); 
	#else
	thError("%s: ERROR - 'OBJC_IO' not supported in 'SIM_MODE'", name);
	return(SH_GENERIC_ERROR);
	#endif

	} else if (MGalaxyMode == MLE_ON_DEMAND_MODE) {
		t->re = effectiveradius(pobjc->r_deV[index], pobjc->ab_deV[index], DEV1_RE_SMALL, DEV1_RE_LARGE);
		t->counts = (THPIX) (pobjc->counts_deV[index]);
		t->e = ellipticity(pobjc->ab_deV[index]);
		t->phi = RADIAN_DEGREE_RATIO * (pobjc->phi_deV[index]); 
	} else {
		thError("%s: ERROR - unsupported run mode for MGalaxy module", name);
		return(SH_GENERIC_ERROR);
	}



t->mcount =  (THPIX) DEV_MCOUNT * pow(t->re, 2.0); /* readjusted on june 26, 2016 * pobjc->ab_deV[index]; */
t->I = (t->counts) / t->mcount;
status = thCTransformFromPhoto(t, "DEVPARS");
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not perform generic coordinate transform on 'DEVPARS'", name);
	return(status);
}

return(SH_SUCCESS);

}

RET_CODE MGInitDeV2(PHPROPS *p, void *q, int band) {
char *name = "MGInitDeV2";
	if (q == NULL) {
		thError("%s: WARNING - null placeholder", name);
		return(SH_SUCCESS);
	}
	if (p == NULL) {
		thError("%s: ERROR - null PHOTO property list", name);
		return(SH_GENERIC_ERROR);
	}
	WOBJC_IO *pobjc = NULL;
	int *bndex = NULL;
	RET_CODE status;
	status = thPhpropsGet(p, &pobjc, &bndex, NULL, NULL, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (pobjc) from (p)", name);
		return(status);
	}
	DEVPARS *t;
	t = (DEVPARS *) q;
	int ncolor;
	t->band = band;
	status = thPhpropsGetNcolor(p, &ncolor);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract (ncolor) from 'PHPROPS'", name);
		return(status);
	}
	if (ncolor <= 0) {
		thError("%s: ERROR - unacceptable (ncolor = %d) value in 'PHPROPS'",
		name, ncolor);
		return(SH_GENERIC_ERROR);
	} else if (band < 0 || band >= NCOLOR) {
		thError("%s: ERROR - PHOTO properties are only listed for (%d) bands, while searching for band (%d)", name, NCOLOR, band);
		return(SH_GENERIC_ERROR);
	}
	THPIX expMag, deVMag;
	status = derive_class_info(p, NULL, NULL, NULL, NULL, &deVMag, &expMag, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not derive classification information", name);
		return(status);
	}
	int index = bndex[band];
	t->xc = (THPIX) pobjc->objc_rowc;
	t->yc = (THPIX) pobjc->objc_colc;
	/* note that the effective radius that PHOTO finds in semi major effective radius */
	if (MGalaxyMode == MLE_MODE || MGalaxyMode == MLE_MODE_PLUS) {

	THPIX ab_deV = pobjc->ab_deV[index];
	if (ab_deV > 1.0) ab_deV = 1.0 / ab_deV;
	THPIX ab_exp = pobjc->ab_exp[index];
	if (ab_exp > 1.0) ab_exp = 1.0 / ab_exp;
	THPIX re_from_dev = effectiveradius(DEV2_DEV_RATIO * pobjc->r_deV[index], pobjc->ab_deV[index], DEV2_RE_SMALL, DEV2_RE_LARGE);
	THPIX re_from_exp = effectiveradius(DEV2_EXP_RATIO * pobjc->r_exp[index], pobjc->ab_exp[index], DEV2_RE_SMALL, DEV2_RE_LARGE);
	THPIX re_big = MAX(re_from_dev, re_from_exp);
	THPIX re_small = MIN(re_from_dev, re_from_exp);
	THPIX ratio = MAX(sqrt(re_big / re_small), DEV2_DEV1_INIT_RATIO);
	re_big = re_big * ratio;
	re_small = re_small / ratio;
	if (re_big > DEV2_RE_LARGE) re_big = DEV2_RE_LARGE;
	if (re_big < DEV2_RE_SMALL) re_big = DEV2_RE_SMALL;
	if (re_small > DEV2_RE_LARGE) re_small = DEV2_RE_LARGE;
	if (re_small < DEV2_RE_SMALL) re_small = DEV2_RE_SMALL;
        re_small = MIN(re_small, DEV1_INIT_RADIUS);
        re_big = MAX(re_big, DEV2_INIT_RADIUS);
	shAssert(re_small < re_big);
	#if INIT_MLE_DEV1_WITH_EXACT_PHOTO
	#if INIT_MLE_DEV2_RANDOM
	float puniform = (float) phRandomUniformdev();	
	ratio = (THPIX) (CD_R_RATIO0 * pow((float) CD_R_RATIO1 / (float) CD_R_RATIO0, puniform));
	#endif
	t->re = re_from_dev * ratio;
	t->e = ellipticity(pobjc->ab_deV[index]);
	t->phi = RADIAN_DEGREE_RATIO * (pobjc->phi_deV[index]);
	#else
	t->re = re_big;
	if (re_from_dev >= re_from_exp) {
		t->e = ellipticity(pobjc->ab_deV[index]);
		t->phi = RADIAN_DEGREE_RATIO * (pobjc->phi_deV[index]);
	} else {
		t->e = ellipticity(pobjc->ab_exp[index]);
		t->phi = RADIAN_DEGREE_RATIO * (pobjc->phi_exp[index]);
	}
	#endif
	#if INIT_MODEL_WITH_DEV_FRACTION	
	if (re_from_dev >= re_from_exp) {
		t->counts = (THPIX) (pobjc->counts_deV[index] * pobjc->fracPSF[index]);
	} else {	
		t->counts = (THPIX) (pobjc->counts_exp[index] * ((float) 1.0 - pobjc->fracPSF[index]));
	}
	#else
	if (re_from_dev >= re_from_exp) {
		t->counts = (THPIX) (pobjc->counts_deV[index]);
	} else {	
		t->counts = (THPIX) (pobjc->counts_exp[index]);
	}
	#endif

	} else if (MGalaxyMode == SIM_MODE) {
	#if USE_FNAL_OBJC_IO	
	t->re = effectiveradius(pobjc->r_deV2[index], pobjc->ab_deV2[index], DEV2_RE_SMALL, DEV2_RE_LARGE);
	t->counts = (THPIX) (pobjc->counts_deV2[index]);
	t->e = ellipticity(pobjc->ab_deV2[index]);
	t->phi = RADIAN_DEGREE_RATIO * (pobjc->phi_deV2[index]); 
	#else
	thError("%s: ERROR - 'OBJC_IO' not supported in 'SIM_MODE'", name);
	return(SH_GENERIC_ERROR);
	#endif

	} else if (MGalaxyMode == MLE_ON_DEMAND_MODE) {

	#if USE_FNAL_OBJC_IO	
	t->re = effectiveradius(pobjc->r_deV2[index], pobjc->ab_deV2[index], DEV2_RE_SMALL, DEV2_RE_LARGE);
	t->counts = (THPIX) (pobjc->counts_deV2[index]);
	t->e = ellipticity(pobjc->ab_deV2[index]);
	t->phi = RADIAN_DEGREE_RATIO * (pobjc->phi_deV2[index]); 
	#else
	t->re = effectiveradius(pobjc->r_exp[index], pobjc->ab_exp[index], DEV2_RE_SMALL, DEV2_RE_LARGE);
	t->counts = (THPIX) (pobjc->counts_exp[index]);
	t->e = ellipticity(pobjc->ab_exp[index]);
	t->phi = RADIAN_DEGREE_RATIO * (pobjc->phi_exp[index]); 
	#endif

	} else {
		thError("%s: ERROR - unsupported run mode for MGalaxy module", name);
		return(SH_GENERIC_ERROR);
	}



t->mcount =  (THPIX) DEV_MCOUNT * pow(t->re, 2.0); /* readjusted on june 26, 2016 * pobjc->ab_deV[index]; */
t->I = (t->counts) / t->mcount;
status = thCTransformFromPhoto(t, "DEVPARS");
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not perform generic coordinate transform on 'DEVPARS'", name);
	return(status);
}

return(SH_SUCCESS);

}

RET_CODE MGInitPl(PHPROPS *p, void *q, int band) {
char *name = "MGInitPl";
	if (q == NULL) {
		thError("%s: WARNING - null placeholder", name);
		return(SH_SUCCESS);
	}
	if (p == NULL) {
		thError("%s: ERROR - null PHOTO property list", name);
		return(SH_GENERIC_ERROR);
	}
	WOBJC_IO *pobjc = NULL;
	int *bndex = NULL;
	RET_CODE status;
	status = thPhpropsGet(p, &pobjc, &bndex, NULL, NULL, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (pobjc) from (p)", name);
		return(status);
	}
	POWERLAWPARS *t = (POWERLAWPARS *) q;
	int ncolor;
	t->band = band;
	status = thPhpropsGetNcolor(p, &ncolor);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract (ncolor) from 'PHPROPS'", name);
		return(status);
	}
	if (ncolor <= 0) {
		thError("%s: ERROR - unacceptable (ncolor = %d) value in 'PHPROPS'",
		name, ncolor);
		return(SH_GENERIC_ERROR);
	} else if (band < 0 || band >= NCOLOR) {
		thError("%s: ERROR - PHOTO properties are only listed for (%d) bands, while searching for band (%d)", name, NCOLOR, band);
		return(SH_GENERIC_ERROR);
	}
	THPIX expMag, deVMag;
	status = derive_class_info(p, NULL, NULL, NULL, NULL, &deVMag, &expMag, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not derive classification information", name);
		return(status);
	}
	int index = bndex[band];
	t->xc = (THPIX) pobjc->objc_rowc;
	t->yc = (THPIX) pobjc->objc_colc;
	/* note that the effective radius that PHOTO finds in semi major effective radius */
	if (MGalaxyMode == MLE_MODE || MGalaxyMode == MLE_MODE_PLUS) {

	THPIX ab_deV = pobjc->ab_deV[index];
	if (ab_deV > 1.0) ab_deV = 1.0 / ab_deV;
	THPIX ab_exp = pobjc->ab_exp[index];
	if (ab_exp > 1.0) ab_exp = 1.0 / ab_exp;
	THPIX re_from_dev = effectiveradius(PL_DEV_RATIO * pobjc->r_deV[index], pobjc->ab_deV[index], PL_RE_SMALL, PL_RE_LARGE);
	THPIX re_from_exp = effectiveradius(PL_EXP_RATIO * pobjc->r_exp[index], pobjc->ab_exp[index], PL_RE_SMALL, PL_RE_LARGE);
	THPIX re_big = MAX(re_from_dev, re_from_exp);
	THPIX re_small = MIN(re_from_dev, re_from_exp);
	THPIX ratio = MAX(sqrt(re_big / re_small), PL_DEV_INIT_RATIO);
	re_big = re_big * ratio;
	re_small = re_small / ratio;
	if (re_big > PL_RE_LARGE) re_big = PL_RE_LARGE;
	if (re_big < PL_RE_SMALL) re_big = PL_RE_SMALL;
	if (re_small > PL_RE_LARGE) re_small = PL_RE_LARGE;
	if (re_small < PL_RE_SMALL) re_small = PL_RE_SMALL;
        re_small = MIN(re_small, DEV1_INIT_RADIUS);
        re_big = MAX(re_big, PL_INIT_RADIUS);
	shAssert(re_small < re_big);
	#if INIT_MLE_DEV1_WITH_EXACT_PHOTO
	#if INIT_MLE_PL_RANDOM
	float puniform = (float) phRandomUniformdev();	
	ratio = (THPIX) (CD_R_RATIO0 * pow((float) CD_R_RATIO1 / (float) CD_R_RATIO0, puniform));
	#endif
	t->re = re_from_dev * ratio;
	t->e = ellipticity(pobjc->ab_deV[index]);
	t->phi = RADIAN_DEGREE_RATIO * (pobjc->phi_deV[index]);
	#else
	t->re = re_big;
	if (re_from_dev >= re_from_exp) {
		t->e = ellipticity(pobjc->ab_deV[index]);
		t->phi = RADIAN_DEGREE_RATIO * (pobjc->phi_deV[index]);
	} else {
		t->e = ellipticity(pobjc->ab_exp[index]);
		t->phi = RADIAN_DEGREE_RATIO * (pobjc->phi_exp[index]);
	}
	#endif
	#if INIT_MODEL_WITH_DEV_FRACTION	
	if (re_from_dev >= re_from_exp) {
		t->counts = (THPIX) (pobjc->counts_deV[index] * pobjc->fracPSF[index]);
	} else {	
		t->counts = (THPIX) (pobjc->counts_exp[index] * ((float) 1.0 - pobjc->fracPSF[index]));
	}
	#else
	if (re_from_dev >= re_from_exp) {
		t->counts = (THPIX) (pobjc->counts_deV[index]);
	} else {	
		t->counts = (THPIX) (pobjc->counts_exp[index]);
	}
	#endif
	t->n = (THPIX) DEFAULT_PL_INDEX; 
	} else if (MGalaxyMode == SIM_MODE) {
	#if USE_FNAL_OBJC_IO	
	t->re = effectiveradius(pobjc->r_pl[index], pobjc->ab_pl[index], PL_RE_SMALL, PL_RE_LARGE);
	t->counts = (THPIX) (pobjc->counts_pl[index]);
	t->e = ellipticity(pobjc->ab_pl[index]);
	t->phi = RADIAN_DEGREE_RATIO * (pobjc->phi_pl[index]);
	t->n = pobjc->n_pl[index];
	#else
	thError("%s: ERROR - 'OBJC_IO' not supported in 'SIM_MODE'", name);
	return(SH_GENERIC_ERROR);
	#endif

	} else if (MGalaxyMode == MLE_ON_DEMAND_MODE) {

	#if USE_FNAL_OBJC_IO	
	t->re = effectiveradius(pobjc->r_pl[index], pobjc->ab_pl[index], PL_RE_SMALL, PL_RE_LARGE);
	t->counts = (THPIX) (pobjc->counts_pl[index]);
	t->e = ellipticity(pobjc->ab_pl[index]);
	t->phi = RADIAN_DEGREE_RATIO * (pobjc->phi_pl[index]); 
	t->n = pobjc->n_pl[index];
	#else
	t->re = effectiveradius(pobjc->r_exp[index], pobjc->ab_exp[index], PL_RE_SMALL, PL_RE_LARGE);
	t->counts = (THPIX) (pobjc->counts_exp[index]);
	t->e = ellipticity(pobjc->ab_exp[index]);
	t->phi = RADIAN_DEGREE_RATIO * (pobjc->phi_exp[index]);
	t->n = (THPIX) DEFAULT_PL_INDEX; 
	#endif

	} else {
		thError("%s: ERROR - unsupported run mode for MGalaxy module", name);
		return(SH_GENERIC_ERROR);
	}

	THPIX n = t->n;
	double gamma1, gamma2;
	gamma1 = thGamma((double) (n - 1.5));
	gamma2 = thGamma((double) n);
	/* 
	if (gamma1 < (n - 2.5)) {
		thError("%s: ERROR - unacceptable gamma(%g) = %g", name, (float) (n - 1.5), (float) gamma1);
	}
	if (gamma2 < (n - 1.0)) {
		thError("%s: ERROR - unaccepatble gamma(%g) = %g", name, (float) n, (float) gamma2);
	}
	*/
	#if APPLY_GAMMA_TOLERANCE
	double gamma1prime = Gamma_Function((double) (n - 1.5));
	double gamma2prime = Gamma_Function((double) n);
	if (fabs(gamma1 - gamma1prime) / (fabs(gamma1) + fabs(gamma1prime)) > GAMMA_TOLERANCE) {
	thError("%s: WARNING - mismatching gamma(%g) values (%G, %G)", name, (float) (n- 1.5), gamma1, gamma1prime);
	}
	if (fabs(gamma2 - gamma2prime) / (fabs(gamma2) + fabs(gamma2prime)) > GAMMA_TOLERANCE) {
	thError("%s: WARNING - mismatching gamma(%g) values (%G, %G)", name, (float) n, gamma2, gamma2prime);
	}
	#endif
t->mcount = pow(t->re, (THPIX) 2.0) *  (THPIX) 0.5 * pow((THPIX) PI, (THPIX) 1.5) * (THPIX) (gamma1 / gamma2);
t->I = (t->counts) / t->mcount;
status = thCTransformFromPhoto(t, "DEVPARS");
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not perform generic coordinate transform on 'DEVPARS'", name);
	return(status);
}

return(SH_SUCCESS);

}



RET_CODE MGInitSersic(PHPROPS *p, void *q, int band) {
char *name = "MGInitSersic";
	if (q == NULL) {
		thError("%s: WARNING - null placeholder", name);
		return(SH_SUCCESS);
	}
	if (p == NULL) {
		thError("%s: ERROR - null PHOTO property list", name);
		return(SH_GENERIC_ERROR);
	}
	WOBJC_IO *pobjc = NULL;
	int *bndex = NULL;
	RET_CODE status;
	status = thPhpropsGet(p, &pobjc, &bndex, NULL, NULL, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (pobjc) from (p)", name);
		return(status);
	}
	SERSICPARS *t;
	t = (SERSICPARS *) q;
	int ncolor;
	t->band = band;
	t->prv_cid = 0;
	status = thPhpropsGetNcolor(p, &ncolor);
	if (status != SH_SUCCESS) {	
		thError("%s: could not get (ncolor) from (p)", name);
		return(status);
	}
	if (band < 0 || band >= NCOLOR) {
		thError("%s: ERROR - PHOTO properties are only listed for (%d) bands, while searching for band (%d)", name, NCOLOR, band);
		return(SH_GENERIC_ERROR);
	}
	THPIX expMag, deVMag;
	status = derive_class_info(p, NULL, NULL, NULL, NULL, &deVMag, &expMag, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not derive classification information", name);
		return(status);
	}
	int index = bndex[band];
	t->xc = pobjc->objc_rowc;
	t->yc = pobjc->objc_colc;
	/* note that the effective radius that PHOTO finds in semi major effective radius */
	THPIX ab_deV = pobjc->ab_deV[index];
	if (ab_deV > 1.0) ab_deV = 1.0 / ab_deV;
	THPIX ab_exp = pobjc->ab_exp[index];
	if (ab_exp > 1.0) ab_exp = 1.0 / ab_exp;
	THPIX phi_deV = RADIAN_DEGREE_RATIO * pobjc->phi_deV[index];
	THPIX phi_exp = RADIAN_DEGREE_RATIO * pobjc->phi_exp[index];

	THPIX w_deV = (fabs(pobjc->r_deV[index]) * pobjc->fracPSF[index]) / 
	(fabs(pobjc->r_deV[index]) * pobjc->fracPSF[index] + fabs(pobjc->r_exp[index]) * (1.0 - pobjc->fracPSF[index]));
	THPIX w_exp = 1.0 - w_deV;

	if (MGalaxyMode == MLE_MODE || MGalaxyMode == SIM_MODE || MGalaxyMode == MLE_ON_DEMAND_MODE) {
		THPIX re_deV = effectiveradius(SERSIC_DEV_RATIO * pobjc->r_deV[index], pobjc->ab_deV[index], SERSIC_RE_SMALL, SERSIC_RE_LARGE);
		THPIX re_exp = effectiveradius(SERSIC_EXP_RATIO * pobjc->r_exp[index], pobjc->ab_exp[index], SERSIC_RE_SMALL, SERSIC_RE_LARGE);
		t->re = w_deV * re_deV + w_exp * re_exp;
	} else if (MGalaxyMode == MLE_MODE_PLUS) {
		status = draw_re_sersic(&t->re);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not draw (re_sersic)", name);
			return(status);
		}
	} else {
		thError("%s: ERROR - unsupported run mode = '%s'", name, shEnumNameGetFromValue("RUN_MODE", MGalaxyMode));
		return(SH_GENERIC_ERROR);
	}

	THPIX ab_sersic = w_deV * ab_deV + w_exp * ab_exp;
	t->e = ellipticity(ab_sersic);
	t->phi = w_deV * phi_deV + w_exp * phi_exp;
	#if DRAW_RANDOM_SERSIC_INDEX
	status = draw_n_sersic(&t->n);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not draw (n_sersic)", name);
		return(status);
	}
	#else
	status = get_n_sersic(w_deV, w_exp, &(t->n));
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (n_sersic)", name);
		return(status);
	}
	#endif
	t->counts = pobjc->counts_deV[index] * pobjc->fracPSF[index] + pobjc->counts_exp[index] * (1.0 - pobjc->fracPSF[index]);
	status = thCTransformFromPhoto(t, "SERSICPARS");
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not perform generic coordinate transform on 'SERSICPARS'", name);
		return(status);
	}
	#if 0
	THPIX modelcount = (THPIX) (2.0 * PI * t->n * exp(t->k) * pow(t->k, -2.0 * t->n) * t->gamma_2n * pow(t->re, 2.0));
	#else	
	THPIX modelcount = (THPIX) (2.0 * PI * t->n * exp(t->k) * exp(-2.0 * t->n * log(t->k) + t->ln_gamma_2n) * pow(t->re, 2.0));
	#endif
	t->mcount = modelcount;
	t->I = t->counts / t->mcount;	
	return(SH_SUCCESS);
}

RET_CODE MGInitSersic1(PHPROPS *p, void *q, int band) {
char *name = "MGInitSersic1";
	if (q == NULL) {
		thError("%s: WARNING - null placeholder", name);
		return(SH_SUCCESS);
	}
	if (p == NULL) {
		thError("%s: ERROR - null PHOTO property list", name);
		return(SH_GENERIC_ERROR);
	}
	WOBJC_IO *pobjc = NULL;
	int *bndex = NULL;
	RET_CODE status;
	status = thPhpropsGet(p, &pobjc, &bndex, NULL, NULL, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (pobjc) from (p)", name);
		return(status);
	}
	SERSICPARS *t;
	t = (SERSICPARS *) q;
	int ncolor;
	t->prv_cid = 1;
	t->band = band;
	status = thPhpropsGetNcolor(p, &ncolor);
	if (status != SH_SUCCESS) {	
		thError("%s: could not get (ncolor) from (p)", name);
		return(status);
	}
	if (band < 0 || band >= NCOLOR) {
		thError("%s: ERROR - PHOTO properties are only listed for (%d) bands, while searching for band (%d)", name, NCOLOR, band);
		return(SH_GENERIC_ERROR);
	}
	THPIX expMag, deVMag;
	status = derive_class_info(p, NULL, NULL, NULL, NULL, &deVMag, &expMag, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not derive classification information", name);
		return(status);
	}
	int index = bndex[band];
	t->xc = pobjc->objc_rowc;
	t->yc = pobjc->objc_colc;
	/* note that the effective radius that PHOTO finds in semi major effective radius */
	THPIX ab_deV = pobjc->ab_deV[index];
	if (ab_deV > 1.0) ab_deV = 1.0 / ab_deV;
	THPIX ab_exp = pobjc->ab_exp[index];
	if (ab_exp > 1.0) ab_exp = 1.0 / ab_exp;
	THPIX phi_deV = RADIAN_DEGREE_RATIO * pobjc->phi_deV[index];
	THPIX phi_exp = RADIAN_DEGREE_RATIO * pobjc->phi_exp[index];

	THPIX w_deV = (fabs(pobjc->r_deV[index]) * pobjc->fracPSF[index]) / 
	(fabs(pobjc->r_deV[index]) * pobjc->fracPSF[index] + fabs(pobjc->r_exp[index]) * (1.0 - pobjc->fracPSF[index]));
	THPIX w_exp = 1.0 - w_deV;

 	THPIX re_deV = effectiveradius(SERSIC1_DEV_RATIO * pobjc->r_deV[index], pobjc->ab_deV[index], SERSIC_RE_SMALL, SERSIC_RE_LARGE);
	THPIX re_exp = effectiveradius(SERSIC1_EXP_RATIO * pobjc->r_exp[index], pobjc->ab_exp[index], SERSIC_RE_SMALL, SERSIC_RE_LARGE);
	t->re = w_deV * re_deV + w_exp * re_exp;
	THPIX ab_sersic = w_deV * ab_deV + w_exp * ab_exp;
	t->e = ellipticity(ab_sersic);
	t->phi = w_deV * phi_deV + w_exp * phi_exp;
	#if DRAW_RANDOM_SERSIC1_INDEX
	status = draw_n_sersic1(&t->n);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not draw (n_sersic)", name);
		return(status);
	}
	#else
	status = get_n_sersic1(w_deV, w_exp, &(t->n));
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (n_sersic)", name);
		return(status);
	}
	#endif
	t->counts = pobjc->counts_deV[index] * pobjc->fracPSF[index] + pobjc->counts_exp[index] * (1.0 - pobjc->fracPSF[index]);
	status = thCTransformFromPhoto(t, "SERSICPARS");
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not perform generic coordinate transform on 'SERSICPARS'", name);
		return(status);
	}
	#if 0
	THPIX modelcount = (THPIX) (2.0 * PI * t->n * exp(t->k) * pow(t->k, -2.0 * t->n) * t->gamma_2n * pow(t->re, 2.0));
	#else
	THPIX modelcount = (THPIX) (2.0 * PI * t->n * exp(t->k) * exp(-2.0 * t->n * log(t->k) + t->ln_gamma_2n) * pow(t->re, 2.0));
	#endif
	t->mcount = modelcount;
	t->I = t->counts / t->mcount;	
	return(SH_SUCCESS);
}

RET_CODE MGInitSersic2(PHPROPS *p, void *q, int band) {
char *name = "MGInitSersic2";
	if (q == NULL) {
		thError("%s: WARNING - null placeholder", name);
		return(SH_SUCCESS);
	}
	if (p == NULL) {
		thError("%s: ERROR - null PHOTO property list", name);
		return(SH_GENERIC_ERROR);
	}
	WOBJC_IO *pobjc = NULL;
	int *bndex = NULL;
	RET_CODE status;
	status = thPhpropsGet(p, &pobjc, &bndex, NULL, NULL, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (pobjc) from (p)", name);
		return(status);
	}
	SERSICPARS *t;
	t = (SERSICPARS *) q;
	int ncolor;
	t->prv_cid = 2;
	t->band = band;
	status = thPhpropsGetNcolor(p, &ncolor);
	if (status != SH_SUCCESS) {	
		thError("%s: could not get (ncolor) from (p)", name);
		return(status);
	}
	if (band < 0 || band >= NCOLOR) {
		thError("%s: ERROR - PHOTO properties are only listed for (%d) bands, while searching for band (%d)", name, NCOLOR, band);
		return(SH_GENERIC_ERROR);
	}
	THPIX expMag, deVMag;
	status = derive_class_info(p, NULL, NULL, NULL, NULL, &deVMag, &expMag, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not derive classification information", name);
		return(status);
	}
	int index = bndex[band];
	t->xc = pobjc->objc_rowc;
	t->yc = pobjc->objc_colc;
	/* note that the effective radius that PHOTO finds in semi major effective radius */
	THPIX ab_deV = pobjc->ab_deV[index];
	if (ab_deV > 1.0) ab_deV = 1.0 / ab_deV;
	THPIX ab_exp = pobjc->ab_exp[index];
	if (ab_exp > 1.0) ab_exp = 1.0 / ab_exp;
	THPIX phi_deV = RADIAN_DEGREE_RATIO * pobjc->phi_deV[index];
	THPIX phi_exp = RADIAN_DEGREE_RATIO * pobjc->phi_exp[index];

	THPIX w_deV = (fabs(pobjc->r_deV[index]) * pobjc->fracPSF[index]) / 
	(fabs(pobjc->r_deV[index]) * pobjc->fracPSF[index] + fabs(pobjc->r_exp[index]) * (1.0 - pobjc->fracPSF[index]));
	THPIX w_exp = 1.0 - w_deV;

 	THPIX re_deV = effectiveradius(SERSIC2_DEV_RATIO * pobjc->r_deV[index], pobjc->ab_deV[index], SERSIC_RE_SMALL, SERSIC_RE_LARGE);
	THPIX re_exp = effectiveradius(SERSIC2_EXP_RATIO * pobjc->r_exp[index], pobjc->ab_exp[index], SERSIC_RE_SMALL, SERSIC_RE_LARGE);
	t->re = w_deV * re_deV + w_exp * re_exp;
	THPIX ab_sersic = w_deV * ab_deV + w_exp * ab_exp;
	t->e = ellipticity(ab_sersic);
	t->phi = w_deV * phi_deV + w_exp * phi_exp;
	#if DRAW_RANDOM_SERSIC2_INDEX
	status = draw_n_sersic2(&t->n);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not draw (n_sersic)", name);
		return(status);
	}
	#else
	status = get_n_sersic2(w_deV, w_exp, &(t->n));
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (n_sersic)", name);
		return(status);
	}
	#endif
	t->counts = pobjc->counts_deV[index] * pobjc->fracPSF[index] + pobjc->counts_exp[index] * (1.0 - pobjc->fracPSF[index]);
	status = thCTransformFromPhoto(t, "SERSICPARS");
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not perform generic coordinate transform on 'SERSICPARS'", name);
		return(status);
	}
	#if 0
	THPIX modelcount = (THPIX) (2.0 * PI * t->n * exp(t->k) * pow(t->k, -2.0 * t->n) * t->gamma_2n * pow(t->re, 2.0));
	#else
	THPIX modelcount = (THPIX) (2.0 * PI * t->n * exp(t->k) * exp(-2.0 * t->n * log(t->k) + t->ln_gamma_2n) * pow(t->re, 2.0));
	#endif
	t->mcount = modelcount;
	t->I = t->counts / t->mcount;	
	return(SH_SUCCESS);
}


RET_CODE MGInitCoresersic(PHPROPS *p, void *q, int band) {
char *name = "MGInitCoresersic";
	if (q == NULL) {
		thError("%s: WARNING - null placeholder", name);
		return(SH_SUCCESS);
	}
	if (p == NULL) {
		thError("%s: ERROR - null PHOTO property list", name);
		return(SH_GENERIC_ERROR);
	}
	WOBJC_IO *pobjc = NULL;
	int *bndex = NULL;
	RET_CODE status;
	status = thPhpropsGet(p, &pobjc, &bndex, NULL, NULL, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (pobjc) from (p)", name);
		return(status);
	}
	CORESERSICPARS *t;
	t = (CORESERSICPARS *) q;
	int ncolor;
	t->band = band;
	status = thPhpropsGetNcolor(p, &ncolor);
	if (status != SH_SUCCESS) {	
		thError("%s: could not get (ncolor) from (p)", name);
		return(status);
	}
	if (band < 0 || band >= NCOLOR) {
		thError("%s: ERROR - PHOTO properties are only listed for (%d) bands, while searching for band (%d)", name, NCOLOR, band);
		return(SH_GENERIC_ERROR);
	}
	THPIX expMag, deVMag;
	status = derive_class_info(p, NULL, NULL, NULL, NULL, &deVMag, &expMag, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not derive classification information", name);
		return(status);
	}
	
	THPIX modelcount;
	#if DRAW_RANDOM_CORESERSIC_INDEX 
	int found = 0;
	int trial_count = 0;
	while (trial_count < CORESERSIC_MAX_NDRAW && !found) {
	trial_count++;
	#endif
	int index = bndex[band];
	t->xc = pobjc->objc_rowc;
	t->yc = pobjc->objc_colc;
	if (MGalaxyMode == MLE_MODE || MGalaxyMode == MLE_MODE_PLUS) {
	/* note that the effective radius that PHOTO finds in semi major effective radius */
	THPIX ab_deV = pobjc->ab_deV[index];
	if (ab_deV > 1.0) ab_deV = 1.0 / ab_deV;
	THPIX ab_exp = pobjc->ab_exp[index];
	if (ab_exp > 1.0) ab_exp = 1.0 / ab_exp;
	THPIX phi_deV = RADIAN_DEGREE_RATIO * pobjc->phi_deV[index];
	THPIX phi_exp = RADIAN_DEGREE_RATIO * pobjc->phi_exp[index];

	THPIX w_deV = (fabs(pobjc->r_deV[index]) * pobjc->fracPSF[index]) / 
	(fabs(pobjc->r_deV[index]) * pobjc->fracPSF[index] + fabs(pobjc->r_exp[index]) * (1.0 - pobjc->fracPSF[index]));
	THPIX w_exp = 1.0 - w_deV;

 	THPIX re_deV = effectiveradius(CORESERSIC_DEV_RATIO * pobjc->r_deV[index], pobjc->ab_deV[index], SERSIC_RE_SMALL, SERSIC_RE_LARGE);
	THPIX re_exp = effectiveradius(CORESERSIC_EXP_RATIO * pobjc->r_exp[index], pobjc->ab_exp[index], SERSIC_RE_SMALL, SERSIC_RE_LARGE);
	t->re = MAX(re_deV, re_exp);
	t->rb = MIN(re_deV, re_exp);
	THPIX ab_sersic = w_deV * ab_deV + w_exp * ab_exp;
	t->e = ellipticity(ab_sersic);
	t->phi = w_deV * phi_deV + w_exp * phi_exp;
	#if DRAW_RANDOM_CORESERSIC_INDEX
	status = draw_n_coresersic(&(t->n));
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not draw (n_coresersic)", name);
		return(status);
	}
	status = draw_d_coresersic(&(t->delta));
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not draw (d_coresersic)", name);
		return(status);	
	}
	t->delta = (THPIX) D_CORESERSIC_DRAW1 + ((THPIX) D_CORESERSIC_DRAW2 - (THPIX) D_CORESERSIC_DRAW1) * (THPIX) phRandomUniformdev();
	status = draw_g_coresersic(&(t->gamma));
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not draw (g_coresersic)", name);
		return(status);
	}
	t->gamma = (THPIX) G_CORESERSIC_DRAW1 + ((THPIX) G_CORESERSIC_DRAW2 - (THPIX) G_CORESERSIC_DRAW1) * (THPIX) phRandomUniformdev();
	#else
	THPIX d_coresersic = D_CORESERSIC;
	THPIX g_coresersic = N_CORESERSIC;
	status = get_n_coresersic(w_deV, w_exp, &(t->n));
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (n_coresersic)", name);
		return(status);
	}
	status = get_d_coresersic(w_deV, w_exp, &(t->delta));
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (d_coresersic)", name);
		return(status);
	}
	t->delta = MAX(d_coresersic, (THPIX) D_CORESERSIC);
	status = get_g_coresersic(w_deV, w_exp, &(t->gamma));
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (d_coresersic)", name);
		return(status);
	}
	t->gamma = MAX(g_coresersic, (THPIX) G_CORESERSIC);
	
	#endif
	t->counts = pobjc->counts_deV[index] * pobjc->fracPSF[index] + pobjc->counts_exp[index] * (1.0 - pobjc->fracPSF[index]);

	} else if (MGalaxyMode == SIM_MODE) { 

	#if USE_FNAL_OBJC_IO	
	t->rb = effectiveradius(pobjc->r_csersic[index], pobjc->ab_csersic[index], CORESERSIC_RB_SMALL, CORESERSIC_RB_LARGE);
	t->re = effectiveradius(pobjc->r_csersic[index], pobjc->ab_csersic[index], CORESERSIC_RE_SMALL, CORESERSIC_RE_LARGE);
	t->counts = (THPIX) (pobjc->counts_csersic[index]);
	t->e = ellipticity(pobjc->ab_csersic[index]);
	t->phi = RADIAN_DEGREE_RATIO * (pobjc->phi_csersic[index]); 
	t->n = pobjc->n_csersic[index];
	t->gamma = pobjc->g_csersic[index];
	t->delta = pobjc->d_csersic[index];

	#else
	thError("%s: ERROR - 'OBJC_IO' not supported in 'SIM_MODE'", name);
	return(SH_GENERIC_ERROR);
	#endif


	} else if (MGalaxyMode == MLE_ON_DEMAND_MODE) { 

	#if USE_FNAL_OBJC_IO	
	t->rb = effectiveradius(pobjc->rb_csersic[index], pobjc->ab_csersic[index], CORESERSIC_RB_SMALL, CORESERSIC_RB_LARGE);
	t->re = effectiveradius(pobjc->r_csersic[index], pobjc->ab_csersic[index], CORESERSIC_RE_SMALL, CORESERSIC_RE_LARGE);
	t->counts = (THPIX) (pobjc->counts_csersic[index]);
	t->e = ellipticity(pobjc->ab_csersic[index]);
	t->phi = RADIAN_DEGREE_RATIO * (pobjc->phi_csersic[index]); 
	t->n = pobjc->n_csersic[index];
	t->gamma = pobjc->g_csersic[index];
	t->delta = pobjc->d_csersic[index];

	#else

	float rb = pobjc->r_deV[index] > pobjc->r_exp[index] ? pobjc->r_exp[index] : pobjc->r_deV[index]
	float re = pobjc->r_deV[index] > pobjc->r_exp[index] ? pobjc->r_deV[index] : pobjc->r_exp[index]
	float abb = pobjc->r_deV[index] > pobjc->r_exp[index] ? pobjc->ab_exp[index] : pobjc->ab_deV[index]

	float abe = pobjc->r_deV[index] > pobjc->r_exp[index] ? pobjc->ab_deV[index] : pobjc->ab_exp[index]
	t->rb = effectiveradius(rb, abb, CORESERSIC_RB_SMALL, CORESERSIC_RB_LARGE);
	t->re = effectiveradius(re, abe, CORESERSIC_RE_SMALL, CORESERSIC_RE_LARGE);
	t->counts = (THPIX) (pobjc->counts_deV[index]);
	t->e = ellipticity(pobjc->ab_exp[index]);
	t->phi = RADIAN_DEGREE_RATIO * (pobjc->phi_exp[index]);
	status = get_default_n_coresersic(&(t->n));
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get default (n_coresersic)", name);
		return(status);
	}
	t->gamma = (THPIX) DEFAULT_CORESERSIC_GAMMA;
	t->delta = (THPIX) DEFAULT_CORESERSIC_DELTA;
	#endif

	} else {
		thError("%s: ERROR - unsupported run mode for MGalaxy module", name);
		return(SH_GENERIC_ERROR);
	}

	status = thCTransformFromPhoto(t, "CORESERSICPARS");
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not perform generic coordinate transform on 'SERSICPARS'", name);
		return(status);
	}
	#if DRAW_RANDOM_CORESERSIC_INDEX 
	int silent = 1;
	status = coresersic_mcount_calculator(t, &modelcount, silent);
	if (status == SH_SUCCESS && !isnan(modelcount) && !isinf(modelcount))found = 1;

	} /* while loops */
	if (!found) {
		thError("%s: ERROR - conducted (%d) trials but could not find working indices", name, trial_count);
		modelcount = THNAN;
		t->mcount = modelcount;
		t->I = THNAN;
	}	
	#else
	status = coresersic_mcount_calculator(t, &modelcount);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (mcount)", name);
		return(status);
	}
	if (isnan(modelcount) || isinf(modelcount)) {
		thError("%s: ERROR - found (nan) in (mcount)", name);
		return(SH_GENERIC_ERROR);
	}
	#endif
	t->mcount = modelcount;
	t->I = t->counts / t->mcount;	
	return(SH_SUCCESS);
}

RET_CODE thMGInitAllObjectTypes(int classflag, GCLASS cdclass, GCLASS galaxyclass, RUN_MODE runmode) {
char *name = "thMGInitAllObjectTypes";

shAssert((classflag & ALL_CLASSIFICATION) != 0)
static int init_all_object_types_counts = 0;
static int class_flag = 0;
static GCLASS galaxy_class = UNKNOWN_GCLASS;
static RUN_MODE run_mode = MLE_MODE;

if (init_all_object_types_counts > 0 && class_flag == classflag && galaxy_class == galaxyclass && runmode == run_mode) {
	thError("%s: WARNING - all object types already initiated with the same flags - reinitiating", name);
	/* return(SH_SUCCESS); */
}

RET_CODE status;
status = thMGalaxyModeSet(runmode);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not set the mode for module MGalaxy", name);
	return(status);
}

int i, nm_max = 2;
char *mname, **mnames = thCalloc(nm_max, sizeof(char *));
mnames[0] = thCalloc(nm_max, MX_STRING_LEN * sizeof(char));
for (i = 0; i < nm_max; i++) {
	mnames[i] = mnames[0] + i * MX_STRING_LEN;
} 
mname = mnames[0];
if (classflag & STAR_CLASSIFICATION) {
	strcpy(mnames[0], "star");
	printf("%s: initiating object type 'STAR' \n", name);
	status = MStarObjcInit(mnames[0]);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object type", name);
		return(status);
	}
}

if (classflag & CD_CLASSIFICATION) {
	if (cdclass == DEVDEV_GCLASS) {

	strcpy(mnames[0], "deV1");
	strcpy(mnames[1], "deV2");
	char *classname = "CDCANDIDATE";
	printf("%s: initiating object type '%s' for the cdclass = '%s' \n", name, classname, shEnumNameGetFromValue("GCLASS", cdclass));
	printf("%s: adding two models '%s', '%s' \n", name, mnames[0], mnames[1]);
	status = McDCandidateObjcInit(mnames, 2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object type", name);
		return(status);
	}
	/* add pcost functions */
	#if MLE_APPLY_CONSTRAINTS 
	char *rname1, *rname2;
	rname1 = thCalloc(MX_STRING_LEN, sizeof(char));
	rname2 = thCalloc(MX_STRING_LEN, sizeof(char));
	sprintf(rname1, "re_%s", mnames[0]);
	sprintf(rname2, "re_%s", mnames[1]);

	void *pf = &cD_pcost;
	void *dpf1 = &cD_DpcostDcore;
	void *dpf2 = &cD_DpcostDhalo;
	void *ddpf11 = &cD_DDpcostDcoreDcore;
	void *ddpf22 = &cD_DDpcostDhaloDhalo;
	void *ddpf12 = &cD_DDpcostDhaloDcore;

	status = thObjcClassAddPCostFunc(classname, NULL, NULL, (void *) pf);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add pcost function for classname = '%s'", name, classname);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, NULL, (void *) dpf1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, NULL, rname2, (void *) dpf2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname2);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname1, (void *) ddpf11);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname2, rname2, (void *) ddpf22);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname2, rname2);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname2, (void *) ddpf12);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname2);
		return(status);
	}

	sprintf(rname1, "ue_%s", mnames[0]);
	sprintf(rname2, "ue_%s", mnames[1]);

	dpf1 = &cD_DpcostDucore;
	dpf2 = &cD_DpcostDuhalo;
	ddpf11 = &cD_DDpcostDucoreDucore;
	ddpf22 = &cD_DDpcostDuhaloDuhalo;
	ddpf12 = &cD_DDpcostDuhaloDucore;

	status = thObjcClassAddPCostFunc(classname, rname1, NULL, (void *) dpf1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, NULL, rname2, (void *) dpf2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname2);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname1, (void *) ddpf11);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname2, rname2, (void *) ddpf22);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname2, rname2);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname2, (void *) ddpf12);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname2);
		return(status);
	}

	sprintf(rname1, "e_%s", mnames[0]);
	sprintf(rname2, "e_%s", mnames[1]);

	dpf1 = &cD_DpcostDecore;
	dpf2 = &cD_DpcostDehalo;
	ddpf11 = &cD_DDpcostDecoreDecore;
	ddpf22 = &cD_DDpcostDehaloDehalo;

	status = thObjcClassAddPCostFunc(classname, rname1, NULL, (void *) dpf1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, NULL, rname2, (void *) dpf2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname2);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname1, (void *) ddpf11);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname2, rname2, (void *) ddpf22);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname2, rname2);
		return(status);
	}

	sprintf(rname1, "E_%s", mnames[0]);
	sprintf(rname2, "E_%s", mnames[1]);

	dpf1 = &cD_DpcostDEcore;
	dpf2 = &cD_DpcostDEhalo;
	ddpf11 = &cD_DDpcostDEcoreDEcore;
	ddpf22 = &cD_DDpcostDEhaloDEhalo;

	status = thObjcClassAddPCostFunc(classname, rname1, NULL, (void *) dpf1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, NULL, rname2, (void *) dpf2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname2);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname1, (void *) ddpf11);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname2, rname2, (void *) ddpf22);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname2, rname2);
		return(status);
	}


	thFree(rname1);
	thFree(rname2);
	#endif

	} else if (cdclass == DEVPL_GCLASS) {

	strcpy(mnames[0], "deV");
	strcpy(mnames[1], "Pl");
	char *classname = "CDCANDIDATE";
	printf("%s: initiating object type '%s' in cdclass '%s' \n", name, classname, shEnumNameGetFromValue("GCLASS", cdclass));
	printf("%s: adding two models '%s', '%s' \n", name, mnames[0], mnames[1]);
	status = McDCandidateObjcInit(mnames, 2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object type", name);
		return(status);
	}
	/* add pcost functions */
	#if MLE_APPLY_CONSTRAINTS 
	char *rname1, *rname2;
	rname1 = thCalloc(MX_STRING_LEN, sizeof(char));
	rname2 = thCalloc(MX_STRING_LEN, sizeof(char));
	sprintf(rname1, "re_%s", mnames[0]);
	sprintf(rname2, "re_%s", mnames[1]);

	void *pf = &cD_pcost;
	void *dpf1 = &cD_DpcostDcore;
	void *dpf2 = &cD_DpcostDhalo;
	void *ddpf11 = &cD_DDpcostDcoreDcore;
	void *ddpf22 = &cD_DDpcostDhaloDhalo;
	void *ddpf12 = &cD_DDpcostDhaloDcore;

	status = thObjcClassAddPCostFunc(classname, NULL, NULL, (void *) pf);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add pcost function for classname = '%s'", name, classname);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, NULL, (void *) dpf1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, NULL, rname2, (void *) dpf2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname2);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname1, (void *) ddpf11);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname2, rname2, (void *) ddpf22);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname2, rname2);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname2, (void *) ddpf12);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname2);
		return(status);
	}

	sprintf(rname1, "ue_%s", mnames[0]);
	sprintf(rname2, "ue_%s", mnames[1]);

	dpf1 = &cD_DpcostDucore;
	dpf2 = &cD_DpcostDuhalo;
	ddpf11 = &cD_DDpcostDucoreDucore;
	ddpf22 = &cD_DDpcostDuhaloDuhalo;
	ddpf12 = &cD_DDpcostDuhaloDucore;

	status = thObjcClassAddPCostFunc(classname, rname1, NULL, (void *) dpf1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, NULL, rname2, (void *) dpf2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname2);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname1, (void *) ddpf11);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname2, rname2, (void *) ddpf22);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname2, rname2);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname2, (void *) ddpf12);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname2);
		return(status);
	}

	sprintf(rname1, "e_%s", mnames[0]);
	sprintf(rname2, "e_%s", mnames[1]);

	dpf1 = &cD_DpcostDecore;
	dpf2 = &cD_DpcostDehalo;
	ddpf11 = &cD_DDpcostDecoreDecore;
	ddpf22 = &cD_DDpcostDehaloDehalo;

	status = thObjcClassAddPCostFunc(classname, rname1, NULL, (void *) dpf1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, NULL, rname2, (void *) dpf2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname2);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname1, (void *) ddpf11);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname2, rname2, (void *) ddpf22);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname2, rname2);
		return(status);
	}

	sprintf(rname1, "E_%s", mnames[0]);
	sprintf(rname2, "E_%s", mnames[1]);

	dpf1 = &cD_DpcostDEcore;
	dpf2 = &cD_DpcostDEhalo;
	ddpf11 = &cD_DDpcostDEcoreDEcore;
	ddpf22 = &cD_DDpcostDEhaloDEhalo;

	status = thObjcClassAddPCostFunc(classname, rname1, NULL, (void *) dpf1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, NULL, rname2, (void *) dpf2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname2);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname1, (void *) ddpf11);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname2, rname2, (void *) ddpf22);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname2, rname2);
		return(status);
	}


	thFree(rname1);
	thFree(rname2);
	#endif

	} else if (cdclass == DEV_GCLASS) {
	strcpy(mnames[0], "deV");
	char *classname = "CDCANDIDATE";
	printf("%s: initiating object type '%s' \n", name, classname);
	printf("%s: adding one model '%s' \n", name, mnames[0]);
	status = McDCandidateObjcInit(mnames, 1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object type", name);
		return(status);
	}
	/* add pcost functions */
	#if MLE_APPLY_CONSTRAINTS 
	char *rname1;
	rname1 = thCalloc(MX_STRING_LEN, sizeof(char));
	sprintf(rname1, "re_%s", mnames[0]);

	void *pf = &cDeV_pcost;
	void *dpf1 = &cDeV_DpcostDcore;
	void *ddpf11 = &cDeV_DDpcostDcoreDcore;

	status = thObjcClassAddPCostFunc(classname, NULL, NULL, (void *) pf);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add pcost function for classname = '%s'", name, classname);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, NULL, (void *) dpf1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname1, (void *) ddpf11);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname1);
		return(status);
	}

	sprintf(rname1, "ue_%s", mnames[0]);

	dpf1 = &cDeV_DpcostDucore;
	ddpf11 = &cDeV_DDpcostDucoreDucore;

	status = thObjcClassAddPCostFunc(classname, rname1, NULL, (void *) dpf1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname1, (void *) ddpf11);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname1);
		return(status);
	}

	
	sprintf(rname1, "e_%s", mnames[0]);

	dpf1 = &cDeV_DpcostDecore;
	ddpf11 = &cDeV_DDpcostDecoreDecore;

	status = thObjcClassAddPCostFunc(classname, rname1, NULL, (void *) dpf1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname1, (void *) ddpf11);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname1);
		return(status);
	}
	sprintf(rname1, "E_%s", mnames[0]);

	dpf1 = &cDeV_DpcostDEcore;
	ddpf11 = &cDeV_DDpcostDEcoreDEcore;

	status = thObjcClassAddPCostFunc(classname, rname1, NULL, (void *) dpf1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname1, (void *) ddpf11);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname1);
		return(status);
	}

	thFree(rname1);
	#endif

	} else if (cdclass == SERSIC_GCLASS) {
	strcpy(mnames[0], "sersic");
	char *classname = "CDCANDIDATE";
	printf("%s: initiating object type '%s' \n", name, classname);
	printf("%s: adding one model '%s' \n", name, mnames[0]);
	status = McDCandidateObjcInit(mnames, 1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object type", name);
		return(status);
	}
	/* add pcost functions */
	#if MLE_APPLY_CONSTRAINTS 
	char *rname1;
	rname1 = thCalloc(MX_STRING_LEN, sizeof(char));
	sprintf(rname1, "re_%s", mnames[0]);

	void *pf = &cDeV_pcost;
	void *dpf1 = &cDeV_DpcostDcore;
	void *ddpf11 = &cDeV_DDpcostDcoreDcore;

	status = thObjcClassAddPCostFunc(classname, NULL, NULL, (void *) pf);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add pcost function for classname = '%s'", name, classname);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, NULL, (void *) dpf1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname1, (void *) ddpf11);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname1);
		return(status);
	}

	sprintf(rname1, "ue_%s", mnames[0]);

	dpf1 = &cDeV_DpcostDucore;
	ddpf11 = &cDeV_DDpcostDucoreDucore;

	status = thObjcClassAddPCostFunc(classname, rname1, NULL, (void *) dpf1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname1, (void *) ddpf11);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname1);
		return(status);
	}

	
	sprintf(rname1, "e_%s", mnames[0]);

	dpf1 = &cDeV_DpcostDecore;
	ddpf11 = &cDeV_DDpcostDecoreDecore;

	status = thObjcClassAddPCostFunc(classname, rname1, NULL, (void *) dpf1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname1, (void *) ddpf11);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname1);
		return(status);
	}
	sprintf(rname1, "E_%s", mnames[0]);

	dpf1 = &cDeV_DpcostDEcore;
	ddpf11 = &cDeV_DDpcostDEcoreDEcore;

	status = thObjcClassAddPCostFunc(classname, rname1, NULL, (void *) dpf1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname1, (void *) ddpf11);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname1);
		return(status);
	}

	thFree(rname1);
	#endif

	} else if (cdclass == SERSICEXP_GCLASS) {
	strcpy(mnames[0], "sersic1");
	strcpy(mnames[1], "Exp2");
	char *classname = "CDCANDIDATE";
	printf("%s: initiating object type '%s' \n", name, classname);
	printf("%s: adding two model '%s' '%s' \n", name, mnames[0], mnames[1]);
	status = McDCandidateObjcInit(mnames, 2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object type", name);
		return(status);
	}
	/* add pcost functions */
	#if MLE_APPLY_CONSTRAINTS 
	char *rname1;
	rname1 = thCalloc(MX_STRING_LEN, sizeof(char));
	sprintf(rname1, "re_%s", mnames[0]);

	void *pf = &cDeV_pcost;
	void *dpf1 = &cDeV_DpcostDcore;
	void *ddpf11 = &cDeV_DDpcostDcoreDcore;

	status = thObjcClassAddPCostFunc(classname, NULL, NULL, (void *) pf);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add pcost function for classname = '%s'", name, classname);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, NULL, (void *) dpf1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname1, (void *) ddpf11);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname1);
		return(status);
	}

	sprintf(rname1, "ue_%s", mnames[0]);

	dpf1 = &cDeV_DpcostDucore;
	ddpf11 = &cDeV_DDpcostDucoreDucore;

	status = thObjcClassAddPCostFunc(classname, rname1, NULL, (void *) dpf1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname1, (void *) ddpf11);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname1);
		return(status);
	}

	
	sprintf(rname1, "e_%s", mnames[0]);

	dpf1 = &cDeV_DpcostDecore;
	ddpf11 = &cDeV_DDpcostDecoreDecore;

	status = thObjcClassAddPCostFunc(classname, rname1, NULL, (void *) dpf1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname1, (void *) ddpf11);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname1);
		return(status);
	}
	sprintf(rname1, "E_%s", mnames[0]);

	dpf1 = &cDeV_DpcostDEcore;
	ddpf11 = &cDeV_DDpcostDEcoreDEcore;

	status = thObjcClassAddPCostFunc(classname, rname1, NULL, (void *) dpf1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname1, (void *) ddpf11);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname1);
		return(status);
	}

	thFree(rname1);
	#endif

	} else if (cdclass == SERSICSERSIC_GCLASS) {
	strcpy(mnames[0], "sersic1");
	strcpy(mnames[1], "sersic2");
	char *classname = "CDCANDIDATE";
	printf("%s: initiating object type '%s' \n", name, classname);
	printf("%s: adding two model '%s' '%s' \n", name, mnames[0], mnames[1]);
	status = McDCandidateObjcInit(mnames, 2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object type", name);
		return(status);
	}
	/* add pcost functions */
	#if MLE_APPLY_CONSTRAINTS 
	char *rname1;
	rname1 = thCalloc(MX_STRING_LEN, sizeof(char));
	sprintf(rname1, "re_%s", mnames[0]);

	void *pf = &cDeV_pcost;
	void *dpf1 = &cDeV_DpcostDcore;
	void *ddpf11 = &cDeV_DDpcostDcoreDcore;

	status = thObjcClassAddPCostFunc(classname, NULL, NULL, (void *) pf);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add pcost function for classname = '%s'", name, classname);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, NULL, (void *) dpf1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname1, (void *) ddpf11);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname1);
		return(status);
	}

	sprintf(rname1, "ue_%s", mnames[0]);

	dpf1 = &cDeV_DpcostDucore;
	ddpf11 = &cDeV_DDpcostDucoreDucore;

	status = thObjcClassAddPCostFunc(classname, rname1, NULL, (void *) dpf1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname1, (void *) ddpf11);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname1);
		return(status);
	}

	
	sprintf(rname1, "e_%s", mnames[0]);

	dpf1 = &cDeV_DpcostDecore;
	ddpf11 = &cDeV_DDpcostDecoreDecore;

	status = thObjcClassAddPCostFunc(classname, rname1, NULL, (void *) dpf1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname1, (void *) ddpf11);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname1);
		return(status);
	}
	sprintf(rname1, "E_%s", mnames[0]);

	dpf1 = &cDeV_DpcostDEcore;
	ddpf11 = &cDeV_DDpcostDEcoreDEcore;

	status = thObjcClassAddPCostFunc(classname, rname1, NULL, (void *) dpf1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add Dpcost function for classname = '%s' w.r.t. '%s'", name, classname, rname1);
		return(status);
	}
	status = thObjcClassAddPCostFunc(classname, rname1, rname1, (void *) ddpf11);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add DDpcost function for classname = '%s' w.r.t. '%s', '%s'", name, classname, rname1, rname1);
		return(status);
	}

	thFree(rname1);
	#endif

	}  else if (cdclass == CORESERSIC_GCLASS) {
	strcpy(mnames[0], "coresersic");
	char *classname = "CDCANDIDATE";
	printf("%s: initiating object type '%s' \n", name, classname);
	printf("%s: adding one model '%s' \n", name, mnames[0]);
	status = McDCandidateObjcInit(mnames, 1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object type", name);
		return(status);
	}
	/* add pcost functions */
	#if MLE_APPLY_CONSTRAINTS 
	thError("%s: WARNING - binary not prepared to apply constraints for 'coresersic' model", name);
	#endif

	} else {
	thError("%s: ERROR - unsupported galaxy class ('%s') for cD classification", name, shEnumNameGetFromValue("GCLASS", cdclass));
	return(SH_GENERIC_ERROR);
	}
}
if (classflag & GALAXY_CLASSIFICATION) {
	if (galaxyclass == DEVEXP_GCLASS) {
		strcpy(mnames[0], "deV");
		strcpy(mnames[1], "Exp");
		printf("%s: initiating object type 'GALAXY' as 'deV-Exp' \n", name);
		printf("%s: adding two models '%s', '%s' \n", name, mnames[0], mnames[1]);
		status = MGalaxyObjcInit(mnames, 2);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not initiate object type", name);
			return(SH_GENERIC_ERROR);
		}
	} else if (galaxyclass == DEV_GCLASS) {
		strcpy(mnames[0], "deV");
		printf("%s: initiating object type 'DEVGALAXY' \n", name);
		printf("%s: adding one model '%s' \n", name, mnames[0]);
		status = MdeVGalaxyObjcInit(mnames, 1);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not initiate object type", name);
			return(SH_GENERIC_ERROR);
		}
	} else if (galaxyclass == EXP_GCLASS) {
		strcpy(mnames[0], "Exp");
		printf("%s: initiating object type 'EXPGALAXY' \n", name);
		printf("%s: adding one model '%s' \n", name, mnames[0]);
		status = MExpGalaxyObjcInit(mnames, 1);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not initiate object type", name);
			return(SH_GENERIC_ERROR);
		}
	} else if (galaxyclass == SERSIC_GCLASS) {
		strcpy(mnames[0], "sersic");
		printf("%s: initiating object type 'SERSICGALAXY' \n", name);
		printf("%s: adding one model '%s' \n", name, mnames[0]);
		status = MSersicGalaxyObjcInit(mnames, 1);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not initiate object type", name);
			return(SH_GENERIC_ERROR);
		}
	} else if (galaxyclass == SERSICEXP_GCLASS) {
		strcpy(mnames[0], "sersic1");
		strcpy(mnames[0], "Exp2");
		printf("%s: initiating object type 'SERSICEXPGALAXY' \n", name);
		printf("%s: adding one model '%s' \n", name, mnames[0]);
		status = MSersicExpGalaxyObjcInit(mnames, 2);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not initiate object type", name);
			return(SH_GENERIC_ERROR);
		}
	} else if (galaxyclass == SERSICSERSIC_GCLASS) {
		strcpy(mnames[0], "sersic1");
		strcpy(mnames[1], "sersic2");
		printf("%s: initiating object type 'SERSICSERSICGALAXY' \n", name);
		printf("%s: adding two models '%s' '%s' \n", name, mnames[0], mnames[1]);
		status = MSersicSersicGalaxyObjcInit(mnames, 2);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not initiate object type", name);
			return(SH_GENERIC_ERROR);
		}	
	} else if (galaxyclass == CORESERSIC_GCLASS) {
		strcpy(mnames[0], "coresersic");
		printf("%s: initiating object type 'CORESERSICGALAXY' \n", name);
		printf("%s: adding one model '%s' \n", name, mnames[0]);
		status = MCoresersicGalaxyObjcInit(mnames, 1);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not initiate object type", name);
			return(SH_GENERIC_ERROR);
		}	
	} else if (galaxyclass == DEVDEV_GCLASS) {	
		strcpy(mnames[0], "deV1");
		strcpy(mnames[1], "deV2");
		printf("%s: initiating object type 'GALAXY' as 'deV-deV' \n", name);
		printf("%s: adding two models '%s', '%s' \n", name, mnames[0], mnames[1]);
		status = MGalaxyObjcInit(mnames, 2);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not initiate object type", name);
			return(SH_GENERIC_ERROR);
		}
	} else if (galaxyclass == DEVPL_GCLASS) {	
		strcpy(mnames[0], "deV1");
		strcpy(mnames[1], "deV2");
		printf("%s: initiating object type 'GALAXY' as 'deV-Pl' \n", name);
		printf("%s: adding two models '%s', '%s' \n", name, mnames[0], mnames[1]);
		status = MGalaxyObjcInit(mnames, 2);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not initiate object type", name);
			return(SH_GENERIC_ERROR);
		}
	} else if (galaxyclass == EXPEXP_GCLASS) {	
		strcpy(mnames[0], "Exp1");
		strcpy(mnames[1], "Exp2");
		printf("%s: initiating object type 'GALAXY' as 'Exp-Exp' \n", name);
		printf("%s: adding two models '%s', '%s' \n", name, mnames[0], mnames[1]);
		status = MGalaxyObjcInit(mnames, 2);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not initiate object type", name);
			return(SH_GENERIC_ERROR);
		}
	} else if (galaxyclass == EXPDEV_GCLASS) {
		strcpy(mnames[0], "Exp1");
		strcpy(mnames[1], "deV2");
		printf("%s: initiating object type 'GALAXY' as 'Exp-deV' \n", name);
		printf("%s: adding two models '%s', '%s' \n", name, mnames[0], mnames[1]);
		status = MGalaxyObjcInit(mnames, 2);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not initiate object type", name);
			return(SH_GENERIC_ERROR);
		}
	} else {
		char *cname = shEnumNameGetFromValue("GCLASS", galaxyclass);
		thError("%s: ERROR - galaxy class '%s' not supported", name, cname);
		return(SH_GENERIC_ERROR);
	}

}

init_all_object_types_counts++;
class_flag = classflag;
galaxy_class = galaxyclass;
run_mode = runmode;

thFree(mnames[0]);
thFree(mnames);
return(SH_SUCCESS);
}

RET_CODE get_n_sersic(THPIX w1, THPIX w2, THPIX *n) {
char *name = "get_n_sersic";
if (n == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (isnan(w1) || isinf(w1) || isnan(w2) || isinf(w2)) {
	thError("%s: ERROR - (nan) or (inf) discovered in (w1) or (w2)", name);
	*n = THNAN;
	return(SH_GENERIC_ERROR);
}

THPIX n1 = w1 * N_SERSIC_1 + w2 * N_SERSIC_2;
*n = MIN(n1, (THPIX) N_SERSIC);  

return(SH_SUCCESS);
}

RET_CODE get_n_sersic1(THPIX w1, THPIX w2, THPIX *n) {
char *name = "get_n_sersic1";
if (n == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (isnan(w1) || isinf(w1) || isnan(w2) || isinf(w2)) {
	thError("%s: ERROR - (nan) or (inf) discovered in (w1) or (w2)", name);
	*n = THNAN;
	return(SH_GENERIC_ERROR);
}

THPIX n1 = w1 * N_SERSIC1_1 + w2 * N_SERSIC1_2;
*n = MIN(n1, (THPIX) N_SERSIC1);  

return(SH_SUCCESS);
}

RET_CODE get_n_sersic2(THPIX w1, THPIX w2, THPIX *n) {
char *name = "get_n_sersic2";
if (n == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (isnan(w1) || isinf(w1) || isnan(w2) || isinf(w2)) {
	thError("%s: ERROR - (nan) or (inf) discovered in (w1) or (w2)", name);
	*n = THNAN;
	return(SH_GENERIC_ERROR);
}

THPIX n2 = w1 * N_SERSIC2_1 + w2 * N_SERSIC2_2;
*n = MIN(n2, (THPIX) N_SERSIC2);  

return(SH_SUCCESS);
}

RET_CODE draw_re_sersic(THPIX *re) {
char *name = "draw_re_sersic";
if (re == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}

*re = (THPIX) RE_SERSIC_DRAW_1 * pow(((THPIX) RE_SERSIC_DRAW_2 / (THPIX) RE_SERSIC_DRAW_1), (THPIX) phRandomUniformdev());
return(SH_SUCCESS);
}





RET_CODE draw_n_sersic(THPIX *n) {
char *name = "draw_n_sersic";
if (n == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}

*n = (THPIX) N_SERSIC_DRAW_1 + ((THPIX) N_SERSIC_DRAW_2 - (THPIX) N_SERSIC_DRAW_1) * (THPIX) phRandomUniformdev();
return(SH_SUCCESS);
}

RET_CODE draw_n_sersic1(THPIX *n) {
char *name = "draw_n_sersic1";
if (n == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}

*n = (THPIX) N_SERSIC_DRAW1_1 + ((THPIX) N_SERSIC_DRAW1_2 - (THPIX) N_SERSIC_DRAW1_1) * (THPIX) phRandomUniformdev();
return(SH_SUCCESS);
}

RET_CODE draw_n_sersic2(THPIX *n) {
char *name = "draw_n_sersic2";
if (n == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}

*n = (THPIX) N_SERSIC_DRAW2_1 + ((THPIX) N_SERSIC_DRAW2_2 - (THPIX) N_SERSIC_DRAW2_1) * (THPIX) phRandomUniformdev();
return(SH_SUCCESS);
}

RET_CODE draw_n_coresersic(THPIX *n) {
char *name = "draw_n_coresersic";
if (n == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}

*n = (THPIX) N_CORESERSIC_DRAW_1 + ((THPIX) N_CORESERSIC_DRAW_2 - (THPIX) N_CORESERSIC_DRAW_1) * (THPIX) phRandomUniformdev();
return(SH_SUCCESS);
}

RET_CODE draw_d_coresersic(THPIX *d) {
char *name = "draw_d_coresersic";
if (d == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}

*d = (THPIX) DELTA_CORESERSIC_DRAW_1 + ((THPIX) DELTA_CORESERSIC_DRAW_2 - (THPIX) DELTA_CORESERSIC_DRAW_1) * (THPIX) phRandomUniformdev();
return(SH_SUCCESS);
}

RET_CODE draw_g_coresersic(THPIX *g) {
char *name = "draw_g_coresersic";
if (g == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}

*g = (THPIX) GAMMA_CORESERSIC_DRAW_1 + ((THPIX) GAMMA_CORESERSIC_DRAW_2 - (THPIX) GAMMA_CORESERSIC_DRAW_1) * (THPIX) phRandomUniformdev();
return(SH_SUCCESS);
}

RET_CODE get_n_coresersic(THPIX w1, THPIX w2, THPIX *n) {
char *name = "get_n_coresersic";
if (n == NULL) {
	thError("%s: ERROR - null output place holder", name);
	return(SH_GENERIC_ERROR);
}

THPIX n_coresersic = w1 * N_CORESERSIC_1 + w2 * N_CORESERSIC_2;
*n = MAX(n_coresersic, (THPIX) N_CORESERSIC);
return(SH_SUCCESS);
}

RET_CODE get_d_coresersic(THPIX w1, THPIX w2, THPIX *d) {
char *name = "get_d_coresersic";
if (d == NULL) {
	thError("%s: ERROR - null output place holder", name);
	return(SH_GENERIC_ERROR);
}

THPIX d_coresersic = w1 * DELTA_CORESERSIC_1 + w2 * DELTA_CORESERSIC_2;
*d = MAX(d_coresersic, (THPIX) DELTA_CORESERSIC);
return(SH_SUCCESS);
}

RET_CODE get_g_coresersic(THPIX w1, THPIX w2, THPIX *g) {
char *name = "get_g_coresersic";
if (g == NULL) {
	thError("%s: ERROR - null output place holder", name);
	return(SH_GENERIC_ERROR);
}

THPIX g_coresersic = w1 * GAMMA_CORESERSIC_1 + w2 * GAMMA_CORESERSIC_2;
*g = MAX(g_coresersic, (THPIX) GAMMA_CORESERSIC);
return(SH_SUCCESS);
}



RET_CODE get_default_n_coresersic(THPIX *n) {
char *name = "get_default_n_coresersic";
if (n == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}

if (N_CORESERSIC < MIN(N_CORESERSIC_DRAW2, N_CORESERSIC_DRAW2)) {
	*n = MIN(N_CORESERSIC_DRAW2, N_CORESERSIC_DRAW2);
} else if (N_CORESERSIC > MAX(N_CORESERSIC_DRAW2, N_CORESERSIC_DRAW2)) {
	*n = MAX(N_CORESERSIC_DRAW1, N_CORESERSIC_DRAW2);
} else {
	*n = N_CORESERSIC;
}
return(SH_SUCCESS);
}

RET_CODE SetMGalaxyStaticPars(char *pname, THPIX value) {
char *name = "SetMGalaxyStaticPars";
if (pname == NULL || strlen(pname) == 0) {
	thError("%s: null or empty name for static variable", name);
	return(SH_GENERIC_ERROR);
}
if (isnan(value) || isinf(value)) {
	thError("%s: ERROR - no support for (value = %g)", name, (float) value);
	return(SH_GENERIC_ERROR);
}
if (!strcmp(pname, "N_SERSIC_1")) {
	 N_SERSIC_1 = value;
} else if (!strcmp(pname, "N_SERSIC_2")) {
	N_SERSIC_2 = value;
} else if (!strcmp(pname, "N_SERSIC1_1")) {	
	N_SERSIC1_1 = value;
} else if (!strcmp(pname, "N_SERSIC1_2")) {
	N_SERSIC1_2 = value;
} else if (!strcmp(pname, "N_SERSIC2_1")) {	
	N_SERSIC2_1 = value;
} else if (!strcmp(pname, "N_SERSIC2_2")) {
	N_SERSIC2_2 = value;
} else if (!strcmp(pname, "N_SERSIC_DRAW_1")) {
	N_SERSIC_DRAW_1 = value;
} else if (!strcmp(pname, "N_SERSIC_DRAW_2")) {
	N_SERSIC_DRAW_2 = value;
} else if (!strcmp(pname, "N_SERSIC_DRAW1_1")) {
	N_SERSIC_DRAW1_1 = value;
} else if (!strcmp(pname, "N_SERSIC_DRAW1_2")) {
	N_SERSIC_DRAW1_2 = value;
} else if (!strcmp(pname, "N_SERSIC_DRAW2_1")) {
	N_SERSIC_DRAW2_1 = value;
} else if (!strcmp(pname, "N_SERSIC_DRAW2_2")) {
	N_SERSIC_DRAW2_2 = value;
} else if (!strcmp(pname, "N_CORESERSIC_1")) { 
	N_CORESERSIC_1 = value;
} else if (!strcmp(pname, "N_CORESERSIC_2")) {  
	N_CORESERSIC_2 = value;
} else if (!strcmp(pname, "DELTA_CORESERSIC_1")) { 
	DELTA_CORESERSIC_1 = value;
} else if (!strcmp(pname, "DELTA_CORESERSIC_2")) {  
	DELTA_CORESERSIC_2 = value;
} else if (!strcmp(pname, "GAMMA_CORESERSIC_1")) { 
	GAMMA_CORESERSIC_1 = value;
} else if (!strcmp(pname, "GAMMA_CORESERSIC_2")) {  
	GAMMA_CORESERSIC_2 = value;
} else if (!strcmp(pname, "N_CORESERSIC")) {  
	N_CORESERSIC = value;
} else if (!strcmp(pname, "DELTA_CORESERSIC")) {  
	DELTA_CORESERSIC = value;
} else if (!strcmp(pname, "GAMMA_CORESERSIC")) {  
	GAMMA_CORESERSIC = value;
} else if (!strcmp(pname, "N_CORESERSIC_DRAW_1")) { 
	N_CORESERSIC_DRAW_1 = value;
} else if (!strcmp(pname, "N_CORESERSIC_DRAW_2")) {  
	N_CORESERSIC_DRAW_2 = value;
} else if (!strcmp(pname, "DELTA_CORESERSIC_DRAW_1")) { 
	DELTA_CORESERSIC_DRAW_1 = value;
} else if (!strcmp(pname, "DELTA_CORESERSIC_DRAW_2")) {  
	DELTA_CORESERSIC_DRAW_2 = value;
} else if (!strcmp(pname, "GAMMA_CORESERSIC_DRAW_1")) { 
	GAMMA_CORESERSIC_DRAW_1 = value;
} else if (!strcmp(pname, "GAMMA_CORESERSIC_DRAW_2")) {  
	GAMMA_CORESERSIC_DRAW_2 = value;
} else {
	thError("%s: ERROR - static variable '%s' not supported", name, pname);
	return(SH_GENERIC_ERROR);
}
return(SH_SUCCESS);
}


RET_CODE initMGalaxy(void *input) {
char *name = "initMGalaxy";

if (input == NULL) {
	thError("%s: WARNING - null parsed input passed", name);
	return(SH_SUCCESS);
}

char *input_name = "MGALAXY_PARSED_INPUT";
TYPE input_type = shTypeGetFromName(input_name);
if (input_type == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - schema support not available for '%s'", name, input_name);
	return(SH_GENERIC_ERROR);
}

SCHEMA *sin = shSchemaGetFromType(input_type);
int n = sin->nelem;

int i;
RET_CODE status;
for (i = 0; i < n; i++) {
	SCHEMA_ELEM *se = sin->elems + i;
	char *lpname = se->name;
	if (lpname != NULL && lpname[0] == 'l') {
		int *lvalue_ptr = shElemGet(input, se, NULL);
		int lvalue = 0;
		char *pname = lpname + 1;
		se = shSchemaElemGetFromType(input_type, pname);
		THPIX *value_ptr = NULL; 
		if ((lvalue_ptr != NULL) && ((lvalue = *lvalue_ptr) == 1) && 
		se != NULL && (value_ptr = shElemGet(input, se, NULL)) != NULL) {
			THPIX value = *value_ptr;
			printf("%s: setting '%s' = %g \n", name, pname, (float) value);	
			status = SetMGalaxyStaticPars(pname, value);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not set MGALAXY parameter '%s'", name, pname);
				return(status);
			}
		}
			
		
	}
}

return(SH_SUCCESS);
}
