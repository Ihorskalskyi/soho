#ifndef THFITMASK_H
#define THFITMASK_H

#include "thFitmaskTypes.h"
#include "thObjcTypes.h"
#include "thMSky.h"

RET_CODE thObjcMakeFitmask(THOBJC *objc, OBJMASK *goodmask, THOBJC **objc_list, int nobjc);
RET_CODE thSpanmaskSumSq(REGION *reg, OBJMASK *sp, MLEFL *ss, int *npix);
RET_CODE thFitmaskSumSq(FITMASK *mask, THPIX rPetro, REGION *r_image, REGION *z_image);
RET_CODE thObjcUpdateFitMask(THOBJC *objc, REGION *r_image, REGION *z_image);
RET_CODE thFitmaskGetNmask(FITMASK *fitmask, int *nmask);

RET_CODE thFitmaskTrans(FITMASK *mask, void *p, char *pname);

OBJMASK *thObjcMakeObjmaskHighSN(THOBJC *objc, WPSF *wpsf, THPIX sfactor, int aggressive, RET_CODE *status);

#endif
