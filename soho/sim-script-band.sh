#!/bin/bash

run="001033"
root="./simfit"
fpCprefix="fpC"
fpCCprefix="fpCC"

iband=$1
camcol=4

declare -a band_a=("u" "g" "r" "i" "z")
band=${band_a[$iband]}
declare -a field_a=("0101" "0102" "0103" "0104" "0105")
if [ $band == "u" ] 
	then
	declare -a sky_a=(35 26 30 27 27)
elif [ $band == "g" ]
	then
	declare -a sky_a=(85 91 85 84 87)
elif [ $band == "r" ]
	then
	declare -a sky_a=(60 56 58 57 55)
elif [ $band == "i" ]
	then
	declare -a sky_a=(37 48 40 35 38)
elif [ $band == "z" ]
	then
	declare -a sky_a=(31 30 26 28 26)
else
	echo "ERROR - does not support unrecognize band($band)"
	exit
fi

declare -a psf_a=(9999 999 99 90 80 70 60 50 40 30 20 10)
nband=5
npsf=12
nfield=5


for (( ifield=0; ifield<$nfield; ifield++ ))
do
	for (( ipsf=0; ipsf<$npsf; ipsf++ ))
	do		
		sky=${sky_a[$ifield]}
		sky1=$sky
		sky2=$sky
		field=${field_a[$ifield]}
		f1=${psf_a[$ipsf]}
		f2=$f1

		dir="$f1-$f2"
		
		executable="do-sim-w21-w301-$f1-$f2"
		parfile="./sim-$run-$camcol-$field.par"
		modelfile="$root/$dir/$fpCCprefix-$run-$band$camcol-$field.fit"
		poissonfile="$root/$dir/$fpCprefix-$run-$band$camcol-$field.fit"
		logfile="nohup.do-sim-w21-w301-$f1-$f2-$run-$field-$band$camcol.out";

		command="nohup $executable $iband $sky1 $sky2 $parfile $modelfile $poissonfile > $logfile"
		echo "band=$band camcol=$camcol run=$run field=$field psf=$f1 sky=$sky"
		#echo $command
		eval $command

	done
done

echo "end of all simulations for band=$band"
