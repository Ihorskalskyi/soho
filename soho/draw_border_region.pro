pro draw_border_region, rin = rin,  akl = akl, epsfile = epsfile, arcsec = arcsec
name = "draw_border_region"
if (0 eq 1) then begin
if (n_elements(akl) eq 0) then begin
	print, name, ": ERROR - a_kl should be provided"
	return
endif
if (n_elements(rin) eq 0) then begin
	print, name, ": ERROR - rin should be provided"
	return
endif
if (akl le 0.0) then begin
	print, name, ": ERROR - a(kl) should be positive"
	return
endif
if (rin le 0.0) then begin
	print, name, ": ERROR - r(in) should be positive"
	return
endif
endif

akl = 31.0
rin0 = 7.0
rout0 = 14.0
if (n_elements(epsfile) eq 0) then epsfile = "./sketches/sketch-boundary-region.eps"

xsize = 16.0
ysize = 12.0
my_font_size = 12

print, name, ": writing to file = ", epsfile
olddevice = !d.name ; store the current device name in a variable
set_plot, 'PS' ; switch to the postscript device
Device, DECOMPOSED=decomposed, COLOR=1, BITS_PER_PIXEL=8
device, /encapsulated, LANGUAGE_LEVEL = 2, file=epsfile, font_size = my_font_size ; specify some details, give the file a name
ct = 0
loadct, ct

print, name, ": setting up graphic device, xsize = ", xsize, ", ysize = ", ysize
device, xsize=xsize, ysize=ysize


xsize = !D.X_SIZE
ysize = !D.Y_SIZE

xmin = 0
xmax = xsize
ymin = 0
ymax = ysize

center = [(xmax + xmin) / 2.0, (ymax + ymin) / 2.0]
a = (ymax - ymin) * 0.75
rin = (rin0 / akl) * a
rout = (rout0 / akl) * a
x_square = center[0] + 0.5 * [-a, a, a, -a]
y_square = center[1] + 0.5 * [-a, -a, a, a]

npoints = 100
angles = 2 * !PI * dindgen(npoints) / npoints
x_incircle = center[0] + rin * cos(angles)
y_incircle = center[1] + rin * sin(angles)
x_outcircle = center[0] + rout * cos(angles)
y_outcircle = center[1] + rout * sin(angles)

;;POLYFILL, x_square, y_square, /Device, /line_fill,spacing=0.1,orient=45

POLYFILL, x_outcircle, y_outcircle, /Device, /line_fill,spacing=0.1,orient=45
POLYFILL, x_incircle, y_incircle, /Device, color = 255

plots, [x_square, x_square[0]], [y_square, y_square[0]], /device, thick = 3.0
plots, [x_incircle, x_incircle[0]], [y_incircle, y_incircle[0]], /device
plots, [x_outcircle, x_outcircle[0]], [y_outcircle, y_outcircle[0]], /device

npix = 31
pixcolor = 200
if (npix gt 1) then begin
	for i = 1, npix - 1, 1 do begin
	hline_x = center[0] + 0.5 * [-a, a] 
	hline_y = center[1] + 0.5 * [-a, -a] + a * i / npix
	plots, hline_x, hline_y, /device, color = pixcolor
	vline_x = center[0] + 0.5 * [-a, -a] + a * i / npix
	vline_y = center[1] + 0.5 * [-a, a] 
	plots, vline_x, vline_y, /device, color = pixcolor
	endfor
endif

;; arrow indicating the inner radius

arrow_angle = -!pi / 4
xarrow_end = center[0] + rin * cos(arrow_angle)
yarrow_end = center[1] + rin * sin(arrow_angle)
xarrow_start = center[0]
yarrow_start = center[1]

arrow, xarrow_start, yarrow_start, xarrow_end, yarrow_end, /solid, thick = 3.0

title_angle = arrow_angle + 1.0 / 6.0
pos_title = center + rin / 2.0 * [cos(title_angle), sin(title_angle)]
rin_title = textoidl("r_{in}")
xyouts, pos_title[0], pos_title[1], rin_title, charsize = 2.0, /device

;; arrow indicating the out radius

arrow_angle = !pi / 3
xarrow_end = center[0] + rout * cos(arrow_angle)
yarrow_end = center[1] + rout * sin(arrow_angle)
xarrow_start = center[0]
yarrow_start = center[1]

arrow, xarrow_start, yarrow_start, xarrow_end, yarrow_end, /solid, thick = 3.0

title_angle = arrow_angle - 1.0 / 6.0
pos_title = center + (rin + rout) / 2.0 * [cos(title_angle), sin(title_angle)]
rout_title = textoidl("r_{out}")
xyouts, pos_title[0], pos_title[1], rout_title, charsize = 2.0, /device





;; arrow indicating the boundary region
xarrow_start = center[0] - a / 2.0 + a / 15.0
yarrow_start = center[1] + a  / 2.0 + a / 15.0
arrow_size = a / 3.0
arrow_angle = -!pi / 3
xarrow_end = xarrow_start + arrow_size * cos(arrow_angle)
yarrow_end = yarrow_start + arrow_size * sin(arrow_angle)

arrow, xarrow_start, yarrow_start, xarrow_end, yarrow_end, /solid, thick = 2.0

x_title = xarrow_start
y_title = yarrow_start + a / 40.0
boundary_title = textoidl("boundary region")
xyouts, x_title, y_title, boundary_title, charsize = 1.0, /device

;; arrow indicating the KL region

xarrow_end = center[0] - a / 2.0 + a / 13.0
yarrow_end = center[1] - a / 2.0
arrow_size = a / 10.0
arrow_angle = -!pi / 2.5
xarrow_start = xarrow_end + arrow_size * cos(arrow_angle)
yarrow_start = yarrow_end + arrow_size * sin(arrow_angle)

arrow, xarrow_start, yarrow_start, xarrow_end, yarrow_end, /solid, thick = 2.0
x_title = xarrow_start + a / 40.0
y_title = yarrow_start - a / 50.0
if (keyword_set(arcsec)) then  $
	boundary_title = textoidl('KL region (12"\times12")')	$
	else $
	boundary_title = textoidl("KL region (31\times31 pixels)")

xyouts, x_title, y_title, boundary_title, charsize = 1.0, /device




print
print, name, ": closing device"
device, /close ; close the file
set_plot, olddevice ; go back to the graphics device
command = "epstopdf "+epsfile
spawn, command

return
end
