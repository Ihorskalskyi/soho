#include "thDumpTypes.h"

CCDINFO *thCcdinfoNew() {
CCDINFO *x = thCalloc(1, sizeof(CCDINFO));
return(x);
}

void thCcdinfoDel(CCDINFO *x) {
if (x == NULL) return;
if (x->rerun != NULL) thFree(x->rerun);
thFree(x);
return;
}

RUNINFO *thRuninfoNew() {
RUNINFO *x = thCalloc(1, sizeof(RUNINFO));
x->machine = thCalloc(MX_STRING_LEN, sizeof(char));
return(x);
}

void thRuninfoDel(RUNINFO *x) {
if (x == NULL) return;
if (x->machine != NULL) thFree(x->machine);
thFree(x);
return;
}

ALGINFO *thAlginfoNew() {
ALGINFO *x = thCalloc(1, sizeof(ALGINFO));
return(x);
}
void thAlginfoDel(ALGINFO *x) {
if (x == NULL) return;
thFree(x);
return;
}

FIELDINFO *thFieldinfoNew() {
FIELDINFO *x = thCalloc(1, sizeof(FIELDINFO));
return(x);
}
void thFieldinfoDel(FIELDINFO *x) {
if (x == NULL) return;
thFree(x);
}

FITINFO *thFitinfoNew() {
FITINFO *x = thCalloc(1, sizeof(FITINFO));
x->model1 = thCalloc(MX_STRING_LEN, sizeof(char));
x->model2 = thCalloc(MX_STRING_LEN, sizeof(char));
x->model3 = thCalloc(MX_STRING_LEN, sizeof(char));
x->model4 = thCalloc(MX_STRING_LEN, sizeof(char));
return(x);
}

void thFitinfoDel(FITINFO *x) {
if (x == NULL) return;
if (x->model1 != NULL) thFree(x->model1);
if (x->model2 != NULL) thFree(x->model2);
if (x->model3 != NULL) thFree(x->model3);
if (x->model4 != NULL) thFree(x->model4);
thFree(x);
return;
}

IOINFO *thIoinfoNew() {
IOINFO *x = thCalloc(1, sizeof(IOINFO));
x->fpCfile = thCalloc(MX_STRING_LEN, sizeof(char));
x->fpObjcfile = thCalloc(MX_STRING_LEN, sizeof(char));
x->psFieldfile = thCalloc(MX_STRING_LEN, sizeof(char));
x->gpBINfile = thCalloc(MX_STRING_LEN, sizeof(char));
x->gpObjc = thCalloc(MX_STRING_LEN, sizeof(char));
return(x);
}

void thIoinfoDel(IOINFO *x) {
if (x == NULL) return;
if (x->fpCfile != NULL) thFree(x->fpCfile);
if (x->fpObjcfile != NULL) thFree(x->fpObjcfile);
if (x->psFieldfile != NULL) thFree(x->psFieldfile);
if (x->gpBINfile != NULL) thFree(x->gpBINfile);
if (x->gpObjc != NULL) thFree(x->gpObjc);
thFree(x);
return;
}

LOGDUMP *thLogdumpNew() {
LOGDUMP *x = thCalloc(1, sizeof(LOGDUMP));
x->filename = thCalloc(MX_STRING_LEN, sizeof(char));
x->stamp = thCalloc(MX_STRING_LEN, sizeof(char));
x->ccdinfo = thCcdinfoNew();
x->runinfo = thRuninfoNew();
x->alginfo = thAlginfoNew();
x->fieldinfo = thFieldinfoNew();
x->fitinfo = thFitinfoNew();
x->ioinfo = thIoinfoNew();
return(x);
}

void thLogdumpDel(LOGDUMP *x) {
if (x == NULL) return;
if (x->filename != NULL) thFree(x->filename);
if (x->stamp != NULL) thFree(x->stamp);
if (x->ccdinfo != NULL) thCcdinfoDel(x->ccdinfo);
if (x->runinfo != NULL) thRuninfoDel(x->runinfo);
if (x->alginfo != NULL) thAlginfoDel(x->alginfo);
if (x->fieldinfo != NULL) thFieldinfoDel(x->fieldinfo);
if (x->fitinfo != NULL) thFitinfoDel(x->fitinfo);
if (x->ioinfo != NULL) thIoinfoDel(x->ioinfo);
thFree(x);
return;
}


SKYDUMP *thSkydumpNew() {
SKYDUMP *x = thCalloc(1, sizeof(SKYDUMP));
x->filename = thCalloc(MX_STRING_LEN, sizeof(char));
x->skypars = thSkyparsNew();
x->skycov = thSkycovNew();
return(x);
}

void thSkydumpDel(SKYDUMP *x) {
if (x == NULL) return;
if (x->filename != NULL) thFree(x->filename);
if (x->skypars != NULL) thSkyparsDel(x->skypars);
if (x->skycov != NULL) thSkycovDel(x->skycov);
thFree(x);
return;
}

OBJCDUMP *thObjcdumpNew(char *pname) {
OBJCDUMP *x = thCalloc(1, sizeof(OBJCDUMP));
x->filename = thCalloc(MX_STRING_LEN, sizeof(char));
#if USE_FNAL_OBJC_IO
x->sdssobjc = shChainNew("FNAL_OBJC_IO");
#else
x->sdssobjc = shChainNew("OBJC_IO");
#endif
x->sdssobjc2 = shChainNew("CALIB_PHOBJC_IO");
x->initobjc = shChainNew(pname);
x->fitobjc = shChainNew(pname);
x->lm = shChainNew("LMETHOD");
x->lp_init = shChainNew("LPROFILE");

return(x);
}

void thObjcdumpDel(OBJCDUMP *x) {
if (x == NULL) return;
if (x->filename != NULL) thFree(x->filename);
if (x->sdssobjc != NULL) shChainDel(x->sdssobjc);
if (x->sdssobjc2 != NULL) shChainDel(x->sdssobjc2);
if (x->initobjc != NULL) shChainDel(x->initobjc);
if (x->fitobjc != NULL) shChainDel(x->fitobjc);
if (x->lm != NULL) shChainDel(x->lm);
if (x->lp_init != NULL) shChainDel(x->lp_init);
if (x->lp_fit != NULL) shChainDel(x->lp_fit);
thFree(x);
return;
}

FRAMEIO *thFrameioNew(char *pname) {
FRAMEIO *x = thCalloc(1, sizeof(FRAMEIO));
x->log = thLogdumpNew();
x->sky = thSkydumpNew();
x->objcs = thObjcdumpNew(pname);
x->lms = thLmdumpNew();
x->lps = thLpdumpNew();
x->lrz = thLrzdumpNew();
return(x);
}
void thFrameioDel(FRAMEIO *x) {
if (x == NULL) return;
thLogdumpDel(x->log);
thSkydumpDel(x->sky);
thObjcdumpDel(x->objcs);
thLmdumpDel(x->lms);
thLpdumpDel(x->lps);
thLrzdumpDel(x->lrz);
thFree(x);
return;
}

LMDUMP *thLmdumpNew() {
LMDUMP *x = thCalloc(1, sizeof(LMDUMP));
x->filename = thCalloc(MX_STRING_LEN, sizeof(char));
x->lm = shChainNew("LMETHOD");
return(x);
}

void thLmdumpDel(LMDUMP *x) {
if (x == NULL) return;
if (x->filename != NULL) thFree(x->filename);
if (x->lm != NULL) shChainDel(x->lm);
thFree(x);
return;
}

LPDUMP *thLpdumpNew() {
LPDUMP *x = thCalloc(1, sizeof(LPDUMP));
x->filename = thCalloc(MX_STRING_LEN, sizeof(char));
x->lp = shChainNew("LPROFILE");
return(x);
}

void thLpdumpDel(LPDUMP *x) {
if (x == NULL) return;
if (x->filename != NULL) thFree(x->filename);
if (x->lp != NULL) shChainDel(x->lp);
thFree(x);
return;
}

LRZDUMP *thLrzdumpNew() {
LRZDUMP *x = thCalloc(1, sizeof(LRZDUMP));
x->rfilename = thCalloc(MX_STRING_LEN, sizeof(char));
x->zfilename = thCalloc(MX_STRING_LEN, sizeof(char));
x->wfilename = thCalloc(MX_STRING_LEN, sizeof(char));
x->r_image = shRegNew("residual image = data - model", 0, 0, TYPE_THPIX);
x->z_image = shRegNew("z-score for residual image", 0, 0, TYPE_THPIX);
x->w_image = shRegNew("weight matrix image", 0, 0, TYPE_THPIX);
return(x);
}

void thLrzdumpDel(LRZDUMP *x) {
if (x == NULL) return;
if (x->rfilename != NULL) thFree(x->rfilename);
if (x->zfilename != NULL) thFree(x->zfilename);
if (x->wfilename != NULL) thFree(x->wfilename);
if (x->r_image != NULL) shRegDel(x->r_image);
if (x->z_image != NULL) shRegDel(x->z_image);
if (x->w_image != NULL) shRegDel(x->w_image);
thFree(x);
return;
}

RET_CODE thObjcDumpGetLMChain(OBJCDUMP *objc, CHAIN **lm) {
char *name = "thObjcDumpGetLMChain";
if (lm == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (objc == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
*lm = objc->lm;
return(SH_SUCCESS);
}

RET_CODE thObjcDumpGetLPChain(OBJCDUMP *objc, CHAIN **lp_init, CHAIN **lp_fit) {
char *name = "thObjcDumpGetLPChain";
if (lp_init == NULL && lp_fit == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (objc == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (lp_init != NULL) *lp_init = objc->lp_init;
if (lp_fit != NULL) *lp_fit = objc->lp_fit;
return(SH_SUCCESS);
}

PSFDUMP *thPsfdumpNew() {
PSFDUMP *dump = thCalloc(1, sizeof(PSFDUMP));
dump->statfile = thCalloc(MX_STRING_LEN, sizeof(char));
dump->reportfile = thCalloc(MX_STRING_LEN, sizeof(char));
dump->statchain = shChainNew("BRIEFPSFREPORT");
dump->reportchain = shChainNew("PSFREPORT");
return(dump);
}

void thPsfdumpDel(PSFDUMP *x) {
if (x == NULL) return;
if (x->statfile != NULL) thFree(x->statfile);
if (x->reportfile != NULL) thFree(x->reportfile);
if (x->statchain != NULL) shChainDel(x->statchain);
if (x->reportchain != NULL) shChainDel(x->reportchain);
thFree(x);
return;
}

RET_CODE thPsfDumpPutFilename(PSFDUMP *dump, char *reportfile, char *statfile) {
char *name = "thPsfDumpPutFilename";
shAssert(dump != NULL);
shAssert(dump->reportfile != NULL);
shAssert(dump->statfile != NULL);

if (statfile != NULL) strcpy(dump->statfile, statfile);
if (reportfile != NULL) strcpy(dump->reportfile, reportfile);
return(SH_SUCCESS);
}

RET_CODE thPsfDumpPutData(PSFDUMP *dump, CHAIN *reportchain, CHAIN *statchain, int deep) {
char *name = "thPsfDumpPutData";
shAssert(dump != NULL);
if (dump->reportchain == NULL) dump->reportchain = shChainNew("PSFREPORT");
if (dump->statchain == NULL) dump->statchain = shChainNew("BRIEFPSFREPORT");
if (reportchain != NULL) shChainJoin(dump->reportchain, reportchain);
if (statchain != NULL) shChainJoin(dump->statchain, statchain);

return(SH_SUCCESS);
}

RET_CODE thPsfDumpGetFilename(PSFDUMP *dump, char **reportfile, char **statfile) {
char *name = "thPsfDumpGetFilename";
shAssert(dump != NULL);
if (reportfile != NULL) *reportfile = dump->reportfile;
if (statfile != NULL) *statfile = dump->statfile;
return(SH_SUCCESS);
}

RET_CODE thPsfDumpGetData(PSFDUMP *dump, CHAIN **reportchain, CHAIN **statchain) {
char *name = "thPsfDumpGetData";
shAssert(dump != NULL);
if (reportchain != NULL) *reportchain = dump->reportchain;
if (statchain != NULL) *statchain = dump->statchain;
return(SH_SUCCESS);
}

RET_CODE thPsfDumpGetHdr(PSFDUMP *dump, HDR **hdr) {
char *name = "thPsfDumpGetHdr";
shAssert(dump != NULL);
if (hdr != NULL) *hdr = dump->hdr;
return(SH_SUCCESS);
}

RET_CODE thLRZDumpGetRZWImage(LRZDUMP *lrzdump, REGION **r_image, REGION **z_image, REGION **w_image) {
char *name = "thLRZDumpGetRZWImage";
if (r_image == NULL && z_image == NULL && w_image == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (lrzdump == NULL) {
	thError("%s: ERROR - null input placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (r_image != NULL) *r_image = lrzdump->r_image;
if (z_image != NULL) *z_image = lrzdump->z_image;
if (w_image != NULL) *w_image = lrzdump->w_image;
return(SH_SUCCESS);
}

