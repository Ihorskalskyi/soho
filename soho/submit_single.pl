#!/usr/bin/perl

use lib '/u/khosrow/lib/CPAN/';

use warnings;
#use strict;
use Data::Dumper;
use FileHandle;
use File::Path;
use File::Copy;
use File::Glob;
use String::Scanf;


my $flatformat = "%s-%0.6d-%s%d-%0.4d.fit";
my $flatformat2 = "%s-%0.6d-%d-%0.4d.fit";
my $procfilefmt = "%-20.20s = %-80s   #%s\n";

sub check_file_exists {
    
    my $name = "check_file_exists";

    if (scalar(@_) == 0) {
	return(1);
    }

    if (scalar(@_) > 1) {
	printf("%s: too many filenames passed \n", $name);
	return(0);
    }

    $filename = $_[0];

    if (-e $filename) {
	return(1);
    }

    $filename2 = $filename.'.Z';
    if (-e $filename2) {
	return(1);
    }

    $filename2 = $filename.'.gz';
    if (-e $filename2) {
	return(1);
    }

    return(0);
}

sub construct_flat_filename {

    my $name = "construct_flat_filename";

    my ($type, $run, $camcol, $field, $rerun, $band, $dir) = @_;
    
    if ($type eq 'psField' || $type eq 'qsField' || $type eq 'fpObjc' || $type eq 'gpObjc') {
	$band = '';
    }

    my $filename = sprintf($flatformat, $type, $run, $band, $camcol, $field);

    my $rundir = sprintf("%d/", $run);
    my $objcsdir =  $dir.$rundir.$rerun."/objcs/".$camcol."/";

    my $fileadd = $objcsdir.$filename;

    return($fileadd);

}

sub deconstruct_flat_filename {

    my $name = "deconstruct_flat_filename";
    
    if (scalar(@_) != 1) {
	printf("%s: call sequence -- %s \$address \n", $name, $name);
	return(-1);
    }
    
    my $add = $_[0];

    my $dec = {};
    $dec->{field} = -1;
    $dec->{run} = -1;
    $dec->{camcol} = -1;
    $dec->{rerun} = -1;
    $dec->{band} =  -1;
    $dec->{objcsdir} = -1;
    $dec->{dir} = -1;

    my $pos = 1 + rindex($add, '/');
    my $filename = substr($add, $pos);
    $add = substr($add, 0, $pos-1);

    if (substr($filename, length($filename) - 2) eq '.Z') {
	$filename = substr($filename, 0, length($filename) - 2);
    } elsif (substr($filename, length($filename) - 3) eq '.gz') {
	$filename = substr($filename, 0, length($filename) - 3);
    }

    my ($type, $run, $camcol, $band, $field);
    ($type) = sscanf("%s-%s-%s-%s", $filename);

    if ($type eq 'fpObjc' || $type eq 'qpObjc' || $type eq 'psField' || $type eq 'qsField') {
        ($type, $run, $camcol, $field) = sscanf("%s-%d-%d-%d.%s", $filename);
	$band = -1;
    } else {
	($type, $run, my $bandcamcol, $field) = sscanf("%s-%d-%s-%d.%s", $filename);
	$band = substr($bandcamcol, 0, 1);
	$camcol = substr($bandcamcol, 1, 1);
    }

    # replacing all the leading 0s
    $field =~ s/^0+(.)/"$1"/e;
    $run =~ s/^0+(.)/"$1"/e;

    $dec->{field} = $field;
    $dec->{run} = $run;
    $dec->{camcol} = $camcol;
    $dec->{band} = $band;

    $add = substr($add, 0, $pos);
    while (substr($add, length($add) -1, 1) eq '/') {
	$add = substr($add, 0, length($add) - 1);
    }
    
    if ($add eq "") {
	return($dec);
    }

    $pos = 1 + rindex($add, '/');
    my $camcolstr = substr($add, $pos);
    my ($camcol2) = sscanf("%d", $camcolstr);

    if ($camcol2 != $camcol) {
	printf("%s: nonmatch detected in camcol between directory and filename\n", 
	       $name);
	return($dec);
    }

    $add = substr($add, 0, $pos);
    $dec->{objcsdir} = $add;

    while (substr($add, length($add) -1, 1) eq '/') {
	$add = substr($add, 0, length($add) - 1);
    }
    
    if ($add eq "") {
	return($dec);
    }


    $pos = 1 + rindex($add, '/');
    my $objcsdirstr = substr($add, $pos);

    if ($objcsdirstr ne 'objcs') {
	printf("%s: non match detected for objcs in dirname $add\n", $name);
	return($dec);
    }

    $add = substr($add, 0, $pos);
    while (substr($add, length($add) -1, 1) eq '/') {
	$add = substr($add, 0, length($add) - 1);
    }

    if ($add eq "") {
	return($dec);
    }
    
    $pos = 1 + rindex($add, '/');
    my $rerunstr = substr($add, $pos);

    my  ($rerun) = sscanf("%s", $rerunstr);
    $dec->{rerun} = $rerun;

    $add = substr($add, 0, $pos);
    while (substr($add, length($add) -1, 1) eq '/') {
	$add = substr($add, 0, length($add) - 1);
    }

    if ($add eq "") {
	return($dec);
    }

    $pos = 1 + rindex($add, '/');
    my $runstr = substr($add, $pos);
    $runstr =~ s/^0+(.)/"$1"/e;

    
    my  ($run2) = sscanf("%d", $runstr);
    if ($run != $run2) {
	printf("%s: nonmatch detected for run between dir and filename \n", $name);
	return($dec);
    }

    $add = substr($add, 0, $pos);
    $dec->{dir} = $add;

    return($dec);
}

sub collapse_flat_deconstruction {
    
    my $name = "collapse_flat_deconstruction";

    if (scalar(@_) != 1) {
	printf("%s: call sequence -- %s \\\@deconstruction \n", 
	       $name, $name);
	return(-1);
    }

    my $des = $_[0];
    my $info = {};

    my $n = scalar(@$des);
    if ($n == 0) {
	return(-1);
    }

    my @index = ();
    for (my $i = 0; $i < $n; $i++) {
	push(@index, $i);
    }

    for my $key (keys %{$des->[0]}) {
	my @vals = grep {!'-1'} ($des->[@index]->{$key});
	if (scalar(@vals) > 1) {
	    $val = $vals[0];
	    my @diff = grep {!$val} @vals;
	    if (scalar(@diff) != 0) {
		return(-1);
	    }
	}
    }

    $des->[0];

}


sub create_framefiles {

    my $files = {};
    my $name = "create_framefiles";

    if (scalar(@_) != 6) {
	printf("%s: calling sequence -- %s \$run, \$camcol, \$band, \$field, \$srcrerun, \$trgrerun \n", $name, $name);
	return($files);
    }

    my ($run, $camcol, $band, $field, $srcrerun, $trgrerun) = @_;
    # debug
    # print "$run, $camcol, $band, $field, $srcrerun, $trgrerun \n";

    my $rundir = sprintf("%d/", $run);
    my $rawdir = $rundir."fields/";
    my $logsdir = $rundir.$srcrerun."/logs/";
    my $objcsdir =  $rundir.$srcrerun."/objcs/".$camcol."/";
    my $trgobjcdir =  $rundir.$trgrerun."/objcs/".$camcol."/";
    my $modeldir = 'sohofiles/'.$trgrerun."/";
    my $seldir = 'sohofiles/'.$trgrerun."/";

    my ($filehandle, $dir, $fileadd, $comment);

#---------------------------------------------------------------------------
# raw files

    $filehandle = "phrawfile";
    $dir = $rawdir.$camcol."/";
    $temp = sprintf($flatformat, 
		    "idR", $run, $band, $camcol, $field);
    $fileadd = $dir.$temp;
    $comment = "address of the raw image";

    $files->{$filehandle} = {
	add => $fileadd,
	comment => $comment
	};
    
#---------------------------------------------------------------------------
# input */
    $filehandle = "phconfigfile";
    $dir = $logsdir;
    $fileadd = $dir."opConfig-*.par";
    $comment = "address of the opConfig parfile";
    
# search and find the matching file and replace the address

    $files->{$filehandle} = {
	add => $fileadd,
	comment => $comment
	};
    
#--------------------------------------------------------------------------
    $filehandle = "phecalibfile"; 
    $dir = $logsdir;
    $fileadd =  $dir."opECalib-*.par";
    $comment = "address of the opECalib parfile";
    
# search and find the matching file and replace the address
    
    $files->{$filehandle} = {
	add => $fileadd,
	comment => $comment
	};

#--------------------------------------------------------------------------
    $filehandle = "phflatframefile"; 
    $dir =  $objcsdir ;
    my $temp = sprintf($flatformat, 
		       "fpC", $run, $band, $camcol, $field);
    $fileadd =  $dir.$temp;
    $comment = "address of fpC file generated by PHOTO";
    
    $files->{$filehandle} = {
	 
	add => $fileadd,
	comment => $comment
	};
    
#--------------------------------------------------------------------------
    $filehandle = "phmaskfile"; 
    $dir =  $objcsdir ;
    $temp = sprintf($flatformat, 
		    "fpM", $run, $band, $camcol, $field);
    $fileadd =  $dir.$temp;
    $comment = "address of fpM file generated by PHOTO";
    
    $files->{$filehandle} = {
	 
	add => $fileadd,
	comment => $comment
	};
    
#--------------------------------------------------------------------------
    $filehandle = "phsmfile"; 
    $dir = $objcsdir;
    $temp = sprintf($flatformat, 
		    "fpBIN", $run, $band, $camcol, $field);
    $fileadd =  $objcsdir.$temp;
    $comment = "address of fpBin file generated by PHOTO";
    
    $files->{$filehandle} = {
	 
	add => $fileadd,
	comment => $comment
	};
    
#--------------------------------------------------------------------------
    $filehandle = "phpsfile";
    $dir = $objcsdir;
    $temp = sprintf($flatformat, 
		    "psField", $run, '', $camcol, $field);
    $fileadd =  $objcsdir.$temp;
    $comment = "address of psField file generated bt PHOTO";
    
    $files->{$filehandle} = {
	add => $fileadd,
	comment => $comment
	};
    
#--------------------------------------------------------------------------
    $filehandle = "phobjcfile";
    $dir = $objcsdir;
    $temp = sprintf($flatformat, 
		     "fpObjc", $run, '', $camcol, $field);
    $fileadd =  $objcsdir.$temp;
    $comment = "address of fpObjc file generated by PHOTO";
    
    $files->{$filehandle} = {
	add => $fileadd,
	comment => $comment
	};
    
#--------------------------------------------------------------------------
# the following two files should be imported once for all */
    $filehandle = "thsbfile"; 
    $dir = $modeldir;
    $fileadd = $modeldir."th-sky-basis-model.fit";
    $comment = "address of the sky basis functions to be read";
    
    $files->{$filehandle} = {
	 
	add => $fileadd,
	comment => $comment
	};
#--------------------------------------------------------------------------
    $filehandle = "thobfile";
    $dir = $modeldir;
    $fileadd = $modeldir."th-object-basis-model.fit";
    $comment = "address of the object basis models to be read";
    
    $files->{$filehandle} = {
	add => $fileadd,
	comment => $comment
	};
#--------------------------------------------------------------------------
# selection and creation rules */
    $filehandle = "thmaskselfile";
    $dir = $seldir;
    $fileadd = $seldir."th-mask-selfile.par";
    $comment = "address of file containing the rule to make each mask";
    
    $files->{$filehandle} = {
	add => $fileadd,
	comment => $comment
	};
#--------------------------------------------------------------------------
    $filehandle = "thobjcselfile";
    $dir = $seldir;
    $fileadd = $seldir."th-objc-selfile.par"; 
    $comment = "address of the object selection rules for fitting";
    
    $files->{$filehandle} = {
	add => $fileadd,
	comment => $comment
	};
#--------------------------------------------------------------------------
# /* output and input */
    $filehandle = "thsmfile";
    $dir = $trgobjcdir;
    $temp = sprintf($flatformat, 
		    "gpSKY", $run, $band, $camcol, $field);
    $fileadd = $trgobjcdir.$temp;
    $comment = "address of the sky model file for SOHO";
    
    $files->{$filehandle} = {
	add => $fileadd,
	comment => $comment
    };
#--------------------------------------------------------------------------
    $filehandle = "thpsfile"; 
    $dir = $trgobjcdir;
    $temp = sprintf($flatformat, 
		    "qsField", $run, $band, $camcol, $field);
    $fileadd = $trgobjcdir.$temp;
    $comment = "address of the PSF model file for SOHO";
    
    $files->{$filehandle} = {
	add => $fileadd,
	comment => $comment
	};
#--------------------------------------------------------------------------
    $filehandle = "thobjcfile"; 
    $dir = $trgobjcdir;
    $temp = sprintf($flatformat, 
		    "gpObjc", $run, $band, $camcol, $field);
    $fileadd =  $trgobjcdir.$temp;
    $comment = "address of object model parameter file for SOHO";
    
    $files->{$filehandle} = {
	add => $fileadd,
	comment => $comment
	};
#--------------------------------------------------------------------------
    $filehandle = "thlmfile"; 
    $dir = $trgobjcdir;
    $temp = sprintf($flatformat, 
		    "lM", $run, $band, $camcol, $field);
    $fileadd =  $trgobjcdir.$temp;
    $comment = "address of LM dump location";
    
    $files->{$filehandle} = {
	add => $fileadd,
	comment => $comment
	};
#--------------------------------------------------------------------------
 
    $files;
    
}

sub disk_copy_framefiles {
    

    my $proc = "disk_copy_framefiles";

    if (scalar(@_) != 3) {
	printf("%s: call sequence: %s \\%%framefiles \$srcdir \$trgdir \n",
	       $proc, $proc);
	return(-1);
    }

    my ($framefiles, $srcdir, $trgdir) = @_;
    
    my $dir;
    my $name;

    while ( my ($key, $value) = each(%$framefiles) ){
	# debug
        #print "disk_copy: value = ", Dumper $value->{add};
	my $position = 1 + rindex($value->{add}, '/');
	if ($position < 0) {
	    $dir = '';
	    $name = $value->{add}
	} else {
	    $dir = substr($value->{add}, 0, $position);
	    $name = substr($value->{add}, $position);
	}
	
	my $dir1 = $srcdir.$dir;
	my $dir2 = $trgdir.$dir;
	if (!(-d $dir2)) {
	    &mkpath($dir2);
	}
  
    
	if (opendir(my $DIR1, $dir1)) {
	    
	    my $namesearch = $dir1.$name."*";
	    my @thisfile = glob($namesearch);

	    if (scalar(@thisfile) != 0) {
		my $file1 = $thisfile[0];

	
		if (-e $file1) {
		    
		    if ($key eq 'phrawfile') {
			$framefiles->{$key}->{add} = $file1;
		    } else {
			my $file2;
			if (substr($file1, length($file1)-3) eq '.gz') {
			    
			    $position = 1 + rindex($file1, '/');
			    my $namelength = length($file1) - 3 - $position;
			    my $name2 = substr($file1, $position, $namelength);
			    my $file2 = $dir2.$name2;
			    
			    if (!(-e $file2)) {
				#gunzip $file1 =>$file2  ||
				copy($file1, $dir2) || 
				    printf("%s: could not copy (%s)\n", 
					   $proc, $file1);
				# debug
				# printf("%s: gunzipping %s.gz \n", $proc, $file2);
				$gunzipcommand = "gunzip $file2.gz";
				!(system($gunzipcommand)) || 
				    printf("%s: could not gunzip (%s)\n", 
					   $proc, $file1);
			    }
			    $framefiles->{$key}->{add} = $file2;
		   
			} elsif (substr($file1, length($file1)-2) eq '.Z') {
			    
			    $position = 1 + rindex($file1, '/');
			    my $namelength = length($file1) - 2 - $position;
			    my $name2 = substr($file1, $position, $namelength);
			    my $file2 = $dir2.$name2;
			    
			    # debug
			    printf("%s: searching for %s \n", $proc, $file2);
			    if (!(-e $file2)) {
				#gunzip $file1 =>$file2  ||
				copy($file1, $dir2) || 
				    printf("%s: could not copy (%s)\n", 
					   $proc, $file1);
				$gunzipcommand = "gunzip $file2.Z";
				# debug
				# printf("%s: gunzipping %s.Z \n", $proc, $file2);
				!(system($gunzipcommand)) || 
				    printf("%s: could not gunzip (%s)\n", 
					   $proc, $file1);
			    }
			    $framefiles->{$key}->{add} = $file2;
		   
			} else {
			    
			    $position = 1 + rindex($file1, '/');
			    my $namelength = length($file1) - $position;
			    my $name2 = substr($file1, $position, $namelength);
			    my $file2 = $dir2.$name2;
			    
			    if (!(-e $file2)) {
				&copy($file1, $dir2);
			    }
			    $framefiles->{$key}->{add} = $file2;
			}
		    }
		} else {
		    printf("%s: could not locate file (%s) \n", 
			   $proc, $file1);
		    if ($key eq 'phrawfile') {
			$framefiles->{$key}->{add} = $srcdir.$value->{add};
		    } else {
			$framefiles->{$key}->{add} = $trgdir.$value->{add};
		    }
		}
	    } else {
		if ($key eq 'phrawfile') {
		    $framefiles->{$key}->{add} = $srcdir.$value->{add};
		} else {
		    $framefiles->{$key}->{add} = $trgdir.$value->{add};
		}
	    }
	    
	    closedir($DIR1);
	    
	} else {
	    if ($key eq 'phrawfile') {
		$framefiles->{$key}->{add} = $srcdir.$value->{add};
	    } else {
		$framefiles->{$key}->{add} = $trgdir.$value->{add};
	    }
	}
    }

    $framefiles;

}

sub gunzip_file {

    my $name = "gunzip_file";
    
    if (scalar(@_) != 1) {
	printf("%s: calling sequence -- %s, $filename \n",
	       $name, $name);
    }

    $filename = $_[0];
    
    my $namesearch = $filename."*";
    my @thisfile = glob($namesearch);

    if (scalar(@thisfile) == 0) {
	return(0);
    }
    
    my $file1 = $thisfile[0];

    if ((substr($file1, length($file1)-3) eq '.gz') || 
	(substr($file1, length($file1)-2) eq '.Z')) {
	my $gunzipcommand = "gunzip $file1";
	
	!(system($gunzipcommand)) || 
	    printf("%s: could not gunzip %s \n", $name, $file1);
    }

    return(0);
}
	

sub gunzip_framefiles {

    my $name = "gunzip_framefiles";

    if (scalar(@_) != 1) {
	printf("%s: calling sequence -- %s, \\\%ff \n",
	       $name, $name);
    }

    $ff = $_[0];

    while ( my ($key, $value) = each(%$ff) ){
	if ($key ne 'phrawfile') {
	    if (&check_file_exists($value->{add})) {
		&gunzip_file($value->{add})
		};
	}
    }

    return(0);

}
	

sub gunzip_proc {

    my $name = "gunzip_proc";

    if (scalar(@_) != 1) {
	printf("%s: calling sequence -- %s, \\\@ff \n",
	       $name, $name);
    }
    
    my $procs = $_[0];
    my $nprocs = scalar(@$procs);

    for (my $iproc = 0; $iproc < $nprocs; $iproc++) {
	$nframe = scalar(@{$procs->[$iproc]});
	for (my $iframe = 0; $iframe < $nframe; $iframe++) {
	    &gunzip_framefiles($procs->[$iproc]->[$iframe]);
	}
    }

    return(0);
}
    

sub read_overlap_proc {
    

    my $name = "read_overlap_proc";

    my $allff = [];
    local @allframefiles = ();

    # this subroutine reads the list of overlapping frames 
    # from FILE and returns an array of framefiles 

    # this subroutine doesn't check for the correctness of the 
    # overlap information as it comes in FILE
    
    if ((scalar(@_) < 3) || (scalar(@_) > 4)) {
	printf("%s: calling sequence -- %s, \$PROCFILE \$trgrerun \n",
	       $name, $name);
	return(@allframefiles);
    }


    #my $MYPROCFILE;

    my ($srcrerun, $trgrerun, @bands);
    if (scalar(@_) == 4) {
	$MYPROCFILE = shift;
	my ($srcrerun, $trgrerun, @bands) =
	    ($_[0], $_[1], @{$_[2]});
	# debug
	#printf("%s: initialized 4 variables \n", $name);
    } elsif (scalar(@_) == 3) {
	$MYPROCFILE = shift;
	($srcrerun, $trgrerun) = @_; 
	@bands = ("r", "u", "g", "i", "z"); 
	# debug
	#printf("%s: initialized 3 variables: %s, %s, %s \n", 
	#       $name, $MYPROCFILE, $srcrerun, $trgrerun);
	# print "band = ", Dumper @bands, "\n";
    }
   
    my $overlapfmt = "Run: %d Camcol: %d Field: %d";

    my $iframe;
    my $iproc = 0;
    my $procs = [];
    my $frames = [];

    while (my $line = $MYPROCFILE->getline) {
	# dropping the new line character from the end of line
	$line = substr($line, 0, length($line)-1);
	# now deciding if we are at the beginning or at the end of
	# a series of overlapping fields

	if (substr($line, 0, 1) eq '{') {
	    $frames = [];
	    $iframe = 0;
	    $procs->[$iproc] = $frames;

	} elsif ((substr($line, 0, 1)) eq "}") {
	    $iproc++;
	} else {
	    split / /, $line, 6;

	    if (scalar(@_) == 6) {
		my ($run, $camcol, $field) = 
		    ($_[1], $_[3], $_[5]);
		# debug
		# printf("%s: Run: %d Camcol: %d Field: %d \n", 
		#	$name, $run, $camcol, $field);
		# print "bands = ", Dumper @bands, "\n";

		# printf("nb = %s %s \n", scalar(@bands), Dumper @bands);
 
		for (my $ib = 0; $ib < scalar(@bands); $ib++) {
		    my $band = $bands[$ib];
		    my $ff = &create_framefiles($run, $camcol, $band, 
						$field, $srcrerun, $trgrerun);
		    $frames->[$iframe] = $ff;
		    $iframe++;
			 
		}
	    } else {
		printf("%s: could not transform line = %s \n", $name, $line);
	    }
	}
    }

    $procs;
}

sub write_overlap_proc {

    my $name = "write_overlap_proc";

    if (scalar(@_) != 2) {
	printf("%s: calling sequence -- %s \$PROCFILE, \@proc \n",
	       $name, $name);
	return(-1);
    }
        
    my($OVRPROCFILE, $proc) = ($_[0], $_[1]); 

    my  $fmt = $procfilefmt;
    #"%-20.20s = %-100.150s   #%s\n";
	
	# print "proc = ", Dumper $proc;
    
	for (my $ffi = 0; $ffi < scalar(@$proc); $ffi++) {
	    
	    my $ff = $proc->[$ffi];

	    # writing the structre header 
	    my $key = "structure-type";
	    my $value = "FRAMEFILES";
	    my $comment = "";
	    my $line = sprintf($fmt, $key, $value, $comment);
	    $OVRPROCFILE->print($line);
	    
	foreach $key (keys %$ff) {

	    $value = $ff->{$key}{add};
	    $comment = $ff->{$key}{comment};
	    $line = sprintf($fmt, $key, $value, $comment);
	    $OVRPROCFILE->print($line);
	    # debug
            # print $line;
	}
    }

    0;

}
    
sub write_proc_job {

    my $name = "write_proc_job";

    my %job = ();

    if (scalar(@_) != 4) {
	printf("%s: calling sequence -- %s \\\@procs, \$dir, \$rerun \\\@photocmd\n",
	       $name, $name);
	return(%job);
    }

    my(@procfiles) = (@{$_[0]});
    my($dir, $rerun, $photocmd) = ($_[1], $_[2], $_[3]);
       
    my $nprocs = scalar(@procfiles);
    if ($nprocs == 0) {
	printf("%s: empty procs array passed - returning \n",
	       $name);
	return(%job);
    }
    
    # debug
    # printf("%s: %d, procfiles = %d, dir = %s, rerun = %s \n", 
    # $name, scalar(@_), scalar(@procfiles), $dir, $rerun);

    my $jobdir = $dir."jobs/";
    if (!(-d $jobdir)) {
	&mkpath($jobdir);
    }

    my ($range, $temp, $mapfile, $jobfile);
    while (1) {
	$range = 1000000;
	$temp = int(rand($range));
	$mapfile = $jobdir."filemap-$rerun-$temp.mp";
	$jobfile = $jobdir."submit-$rerun-$temp.cmd";
	$photofile = $jobdir."photo-$rerun-$temp.pl";
	if ((!(-e $mapfile)) && (!(-e $jobfile))) {
	    last;
	}
    }

    my $FILEMAP;
    if (!(open($FILEMAP, ">> $mapfile"))) {
	printf("%s: could not open the mapfile \n", $name);
	return(%job);
	};
    print $FILEMAP "#FILEMAP between temporary jobs and SOHO processes\n";
    print $FILEMAP "#temp      = $temp \n";
    print $FILEMAP "#rerun     = $rerun \n";
    print $FILEMAP "#jobfile   = $jobfile \n";
    print $FILEMAP "#mapfile   = $mapfile \n";
    print $FILEMAP "#photofile = $photofile \n";
    print $FILEMAP "\n";

    my ($inputfile, $tempfile, $INPUTFILE, $inputfilecontents);
    for (my $proc = 0; $proc < $nprocs; $proc++) {
	$inputfile = $jobdir."input-$rerun-$temp.$proc";
	$fffile = $jobdir."ff-$rerun-$temp.$proc";
	print $FILEMAP $inputfile, "\n";
	print $FILEMAP $procfiles[$proc], "\n";
	print $FILEMAP $fffile, "\n";
	print $FILEMAP "\n";

	#
	#$tempfile = $procfiles[$proc].".temp";
	copy($procfiles[$proc], $fffile);
	#File::Copy::mv $tempfile, $inputfile;

	# the new version of do-sky requires that you submit the 
	# name of framesfile par inside another procfile

	if (!(open($INPUTFILE, "> $inputfile"))) {
	    printf("%s: could not open the process file \n", $name);
	    return(%job);
	};

	$inputfilecontents = <<INPUT_CONT;

	    structure-type = PROCFILES \# no comments
	    thframefile = $fffile # the frames file
INPUT_CONT

print $INPUTFILE $inputfilecontents;
	close $INPUTFILE;

	
    }
    
    close $FILEMAP;
    
    my $job_contents = <<END_PROC_JOB;
	
	\# job submission batch file for SOHO
	\# temp  = $temp
	\# rerun = $rerun 
	\# jobfile = $jobfile
	\# mapfile = $mapfile

	Executable = $dir\lexec/do-sky
	Universe = vanilla
	Arguments = $jobdir\linput-$rerun-$temp.\$(Process)
	Output = $jobdir\loutput-$rerun-$temp.\$(Process)
	error = $jobdir\lerr-$rerun-$temp.\$(Process)
	Log = $jobdir\llog-$rerun-$temp.\$(Process)
	
	Getenv = True
	Requirements  = ((Arch == \"INTEL\" && OpSys == \"LINUX\") || \\
	(Arch ==\"X86_64\" && OpSys == \"LINUX\")) && \\
	(machine != \"glenn.astro.Princeton.EDU\") && \\
	(machine != \"osca.astro.Princeton.EDU\") 
	
	\# glenn belongs to Yanfei Jiang
	\# osca belongs to Fergal Mullaly

	Arguments = $jobdir\linput-$rerun-$temp.\$(Process)

	Queue $nprocs 

	\#end of the job submission script
END_PROC_JOB

    if (!(open(JOBFILE, ">$jobfile"))) {
	printf("%s: could not open jobfile (%s) \n", 
	       $name, $jobfile);
	
	return(%job);
    }

    print JOBFILE $job_contents;

    close JOBFILE;

    if (!(open(PHOTOFILE, ">$photofile"))) {
	printf("%s: could not open photofile (%s) \n", 
	       $name, $photofile);
	
	return(%job);
    }

    print PHOTOFILE "#  Photo commandline file to create missing files \n";
    print PHOTOFILE "#temp      = $temp \n";
    print PHOTOFILE "#rerun     = $rerun \n";
    print PHOTOFILE "#jobfile   = $jobfile \n";
    print PHOTOFILE "#mapfile   = $mapfile \n";
    print PHOTOFILE "#photofile = $photofile \n";
    print PHOTOFILE "\n";

    for (my $i = 0; $i < scalar(@$photocmd); $i++) {
	for (my $j = 0; $j < scalar(@{$photocmd->[$i]}); $j++) {
	    print PHOTOFILE $photocmd->[$i]->[$j];
	}
    }

    close PHOTOFILE;

    my $job = {
	jobfile => $jobfile,
	mapfile => $mapfile, 
	photofile => $photofile
	};
    
    %{$job};

}


sub create_overlap_job {
    

    my $name = "create_overlap_job";
    my %job = ();

    if ((scalar(@_)) != 5) {
	printf("%s: call sequence -- %s \$overlapfile \$rerun \$dir \n", 
	       $name, $name);
	return(\%job);
    }

    my ($overlapfile, $srcrerun, $trgrerun, $srcdir, $trgdir) = @_;

    my $overlapdir = $trgdir."overlap/";

    if (!(-d $overlapdir))  {
	&mkpath($overlapdir);
    }
    
    my @procs = ();
    
    my $allprocs = &read_overlap_proc($overlapfile, $srcrerun, $trgrerun);

    my $nprocs = scalar(@$allprocs);

    my $allphotocmd = [];
    my $iproc;
    for ($iproc = 0; $iproc < $nprocs; $iproc++) {

	$allff = $allprocs->[$iproc];

	my $nff = scalar(@$allff);

	for ($i = 0; $i < $nff; $i++) {

	    my $oldff = $allff->[$i];

	   
	    
	    $allff->[$i] = &disk_copy_framefiles($oldff, $srcdir, $trgdir);
	}
	
	$allprocs->[$iproc] = $allff;

	$PROCFILE = new FileHandle;
	
	my $procfile = $overlapdir."overlap-$trgrerun.ovp.".$iproc;
	
	$PROCFILE->open(">$procfile");
	&write_overlap_proc($PROCFILE, $allff);
	$PROCFILE->close;
	
	push(@procs, $procfile);

	$allphotocmd->[$iproc] = &create_photo_proc($srcdir, $trgdir, $allff);
	
    }

    printf("* generated (%d) process info \n", $nprocs);

    %job = &write_proc_job(\@procs, $trgdir, $trgrerun, $allphotocmd);
 
    if (exists($job{jobfile})) {
	printf("%s: job file = %s \n", $name, $job{jobfile});
    }

    if (exists($job{mapfile})) {
	printf("%s: map file = %s \n", $name, $job{mapfile});
    }
    
    if (exists($job{photofile})) {
	printf("%s: photo file = %s \n", $name, $job{photofile});
    }

    $job{proc} = $allprocs;

    \%job;
}

sub submit_overlap_job {
    

    my $name = "submit_overlap_job";
    
    if (scalar(@_) != 5) {
	printf("%s: calling sequence -- %s \$overlapfile, \$srcrerun, \$trgrerun, \$srcdir \$trgdir \n",
	       $name, $name);
	return(-1);
    }

    my ($overlapfile, $srcrerun, $trgrerun, $srcdir, $trgdir) = @_;
   
    if (!(-e $overlapfile)) {
	printf("%s: could not locate overlap file: %s \n", 
	       $name, $overlapfile);
	return(-1);
    }
    
    $OVERLAPFILE = new FileHandle;
    
    if (!($OVERLAPFILE->open("<$overlapfile"))) {
	printf("%s: could not open overlap file %s \n",
	       $name, $overlapfile);
	return(-1);
    }

    my $job = &create_overlap_job($OVERLAPFILE, $srcrerun, $trgrerun, 
				  $srcdir, $trgdir);

    if (!exists($job->{jobfile})) {
	printf("%s: no jobfile created \n", 
	       $name);
	return(-1);
    }

    # now submit the photo command
    if (!exists($job->{photofile})) {
	printf("%s: no photo command file created \n",
	       $name);
	return(-1);
    }
    
    # submitting photo commands
    my $photofile = $job->{photofile};
    my $command = "photo -file $photofile";
    printf("%s: submitting photo commands to generate missing files \n", 
	   $name);
    system($command);

    # now gunzip the files that have been recently generated
    printf("%s: gunzipping newly generated files \n", $name);
    &gunzip_proc($job->{proc});

    # submitting condor jobs;
    #$job = &Condor::Submit($job{jobfile});
    my $jobfile = $job->{jobfile};
    $command = "condor_submit $jobfile";
    printf("%s: submitting condor jobs to generate missing files \n", 
	   $name);
    my $result = system($command);

    
    $OVERLAPFILE->close;

    $result;

}

sub create_photo_command {

    my $name = "create_photo_command";
    my $command = "";
    
    if (scalar(@_) != 3) {
	printf("%s: calling sequence -- create_photo_command \$srcdir, \$trgdir, \$ff \n",
	       $name);
	return($command);
    }

    my ($srcdir, $trgdir, $ff) = @_;

    my $idRfile = $ff->{'phrawfile'}->{add};
    my $fpCfile = $ff->{'phflatframefile'}->{add};
    my $fpMfile = $ff->{'phmaskfile'}->{add};
    my $fpBINfile = $ff->{'phsmfile'}->{add};
    my $fpObjcfile = $ff->{'phobjcfile'}->{add};

    # check of the raw data exists. if it doesn't return a comment message
    # which will pass through photo.
    my $eidRfile = check_file_exists($idRfile);
    if (!$eidRfile) {
	$command = <<PHOTO_COMMAND;

\# no raw data was found under the following address (requested flat files also listed)
\# idRfile = $idRfile
\# fpCfile = $fpCfile
\# fpMfile = $fpMfile
\# fpBINfile = $fpBINfile
\# fpObjcfile = $fpObjcfile
\# Warning - you are likely to encounter some problems if you need 
\# flat frame files. 
\# error message generated by $name;
PHOTO_COMMAND

    return($command);
}

    # check if fpC file exists -- generate flagw
    # check if fpM file exists -- generate flag
    # check if fpBIN file exists
    # check if fpObjc file exists
    # check if they exist in the Zip format
    # if all if them exist simply exit with a null command;

    my $efpC = check_file_exists($fpCfile);
    my $efpM = check_file_exists($fpMfile);
    my $efpBIN = check_file_exists($fpBINfile);
    my $efpObjc = check_file_exists($fpObjcfile);

    my ($fpCflag, $fpMflag);

    if ($efpC && $efpM && $efpBIN && $efpObjc) {
	$command = "";
	return($command);
    }

    if (!$efpC) {
	$fpCflag = '-fpC';
    } else {
	$fpCflag = "";
    }

    if (!$efpM) {
	$fpMflag = '-fpM';
    } else {
	$fpMflag = "";
    }

    # extract ($run, $camcol, $field, $band, $rerun, $objcsdir) from
    # fpC filename
    # fpM filename
    # fpBIN filename
    # check if these parameters are the same

    my $defpC = &deconstruct_flat_filename($fpCfile);
    my $defpM = &deconstruct_flat_filename($fpMfile);
    my $defpBIN = &deconstruct_flat_filename($fpBINfile);
    my $defpObjc = &deconstruct_flat_filename($fpObjcfile);

    @des =($defpC, $defpM, $defpBIN, $defpObjc); 
    $info = collapse_flat_deconstruction(\@des);

    if ($info eq "-1") {
	printf("%s: nonmatch discovered between photo filenames \n", $name);
	$command = <<PHOTO_COMMAND;

\# Warning - nonmatch discovered between the following file addresses
\# these file addresses do not point to the same
\# run, camcol, field, rerun and band information
\# fpCfile = $fpCfile
\# fpMfile = $fpMfile
\# fpBINfile = $fpBINfile
\# fpObjcfile = $fpObjcfile
\# Warning - you are likely to encounter some problems if you need 
\# flat frame files. 
\# error message generated by $name;
PHOTO_COMMAND

	return($command);
    }
    
    my $run = $info->{run};
    my $rerun = $info->{rerun};
    my $camcol = $info->{camcol};
    my $field = $info->{field};
    my $band = $info->{band};
    my $objcsdir = $info->{objcsdir};
    my $filterlist = "all";
    my $executable = "do-frames";

    # we assume that the derivation in $command1 is done based on 
    # $PHOTO_DATA database. so the following is not necessary.
    # 
    # determine the baseDir for photo
    # get the address of idR, psField, psBB, and psFF files

    # check if idR file exists
    # check if psField file exists
    # check if psBB file exists
    # check if psFF file exists
    # check if fpPlan.par file exists
    # 
    
    

    # the e-flags are set to be true even if the file addresses are empty
    # so check if you need to reconstruct fpC or fpM files based on the
    # e-flags on the fpBIN and fpObjc files;
    
    my $command1 = <<PHOTO_COMMAND;
	
$executable $run $camcol $field $field -filterlist $filterlist -rerun $rerun \\
$fpCflag $fpMflag -v 2 \\
-plan \{fpCDir $objcsdir$camcol\};
PHOTO_COMMAND
    
    
    if ((!$efpC || !$efpM) && $efpBIN && $efpObjc) {
	return($command1);
    }
    
    if (!$efpBIN || !$efpObjc) {
	$fpCfile = &construct_flat_filename("fpC", $run, $camcol, $field, $rerun, $band, $trgdir);
	$fpMfile = &construct_flat_filename("fpM", $run, $camcol, $field, $rerun, $band, $trgdir);
	
	$efpC = &check_file_exists($fpCfile);
	$efpM = &check_file_exists($fpMfile);
	
	if (!$efpC) {
	    $fpCflag = '-fpC';
	}
	
	if (!$efpM) {
	    $fpMflag = '-fpM';
	}
	
	if (!$efpC || !$efpM) {
	    
	    $command1 = <<PHOTO_COMMAND;
	    
	    $executable $run $camcol $field $field -filterlist $filterlist -rerun $rerun $fpCflag $fpMflag -v 2 -plan \{fpCDir $objcsdir$camcol\};
PHOTO_COMMAND
    
} else {
    $command1 = "";
}
    }
    
    
    if (!$efpBIN || !$efpObjc) {
	
	$command2 = <<PHOTO_COMMAND;
	
	$executable $run $camcol $field $field -filterlist $filterlist $-rerun $rerun -read_fpC -noFpAtlas -v 2 -plan \{outputDir $objcsdir$camcol imageDir $objcsdir$camcol\};
PHOTO_COMMAND
} else {
    $command2 = "";
}
    
    $command = <<PHOTO_COMMAND;
    
    $command1
    $command2
PHOTO_COMMAND
    
$command;

}

sub create_photo_proc  {

    my $name = "create_photo_command";
    my $allcmd = [];
    
    if (scalar(@_) != 3) {
	printf("%s: calling sequence -- %s \$srcdir, \$trgdir, \$allff \n",
	       $name, $name);
	return($allcmd);
    }

    my ($srcdir, $trgdir, $allff) = @_;

    my $nff = scalar(@$allff);
    if ($nff == 0) {
	return($allcmd);
    }

    for (my $i = 0; $i < $nff; $i++) {
	$ff = $allff->[$i];
	my $command =  &create_photo_command($srcdir, $trgdir, $ff);
	$allcmd->[$i] = $command;
    }
    
    $allcmd;
}


&submit_overlap_job($ARGV[0], $ARGV[1], $ARGV[2], $ARGV[3], $ARGV[4]);
