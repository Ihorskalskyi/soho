pro create_data_lrg_match_plots
name = "create_data_lrg_match_plots"

rejection_distance = 3.0
prefix0 = "nearby-"
boxplots = 0
camcol_list = [2, 4, 5, 6]

suffix = "all-"
mag_thresh = 100
sas1 = 1
sim1 = 0
mle2 = 1 ;; set to 0 when analyzing photo results 
sas3 = 1
locmap = 1

dolrg_list = [1, 0, 0]
dogalaxy_list = [0, 1, 0]
dostar_list = 1 - dogalaxy_list - dolrg_list
objctype_list=["all-lrg-", "all-galaxy-", "all-star-"]

;; /u/khosrow/thesis/opt/soho/multi-object/images/mle/sdss-g0-s0-cd1-extended/deV-exp-fpC-multiple-corrected/301/4797/2/objc

dir_list = ["sdss-data"]
mle_type_list = ["CD-deV-Pl-G-deV-exp-fpC/stage-1/flag-fitsize-unconstrained-multi-init-10-nstep-9000-full-sky"]
mle_type_list = [ $
"CD-sersic-sersic-n-1-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-flex-adv-masking-v4-hqn", $
"CD-sersic-sersic-n-0.5-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-flex-adv-masking-v4-hqn", $
"CD-sersic-sersic-n-0.2-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-flex-adv-masking-v4-hqn", $
"CD-sersic-sersic-n-2-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-flex-adv-masking-v4-hqn", $
"CD-sersic-sersic-n-3-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-flex-adv-masking-v4-hqn", $
"CD-coresersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-flex-adv-masking-v4-hqn", $
"CD-coresersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-flex-adv-masking-v4-hqn-extended-Gamma", $
"CD-sersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-flex-adv-masking-v4-hqn", $
"CD-sersic-sersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-flex-adv-masking-v4-hqn", $
"CD-sersic-Exp-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-flex-adv-masking-v4-hqn", $
"CD-deV-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-flex-adv-masking-v4-hqn", $
"CD-deV-deV-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-flex-adv-masking-v4-hqn", $
"CD-deV-deV-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-flex-adv-masking-v4-lm", $
"CD-deV-deV-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-flex-adv-masking-v4-hqn", $
"CD-sersic-sersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-5-psf-variable", $
"CD-sersic-sersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-fitshape-fitcenter-unconstrained-multi-init-5-psf-variable", $
"CD-sersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-5-psf-variable", $
"CD-sersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-fitshape-fitcenter-unconstrained-multi-init-5-psf-variable", $
"CD-sersic-Exp-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-5-psf-variable", $
"CD-sersic-Exp-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-fitshape-fitcenter-unconstrained-multi-init-5-psf-variable", $
"CD-deV-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-5-psf-variable", $
"CD-deV-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-fitshape-fitcenter-unconstrained-multi-init-5-psf-variable", $
"CD-deV-deV-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-5-psf-variable", $
"CD-deV-deV-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-fitshape-fitcenter-unconstrained-multi-init-5-psf-variable", $
"CD-sersic-sersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-5-classic-pl-psf-variable", $
"CD-sersic-sersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-fitshape-fitcenter-unconstrained-multi-init-5-classic-pl-psf-variable", $
"CD-sersic-Exp-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-5a-classic-pl-psf-variable", $
"CD-sersic-Exp-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-fitshape-fitcenter-unconstrained-multi-init-5a-classic-pl-psf-variable", $
"CD-sersic-Exp-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-10-classic-pl-psf-variable", $
"CD-sersic-Exp-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-fitshape-fitcenter-unconstrained-multi-init-10-classic-pl-psf-variable", $
"CD-sersic-Exp-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-5-classic-pl-psf-variable", $
"CD-sersic-Exp-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-fitshape-fitcenter-unconstrained-multi-init-5-classic-pl-psf-variable", $
"CD-sersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-10-classic-pl-psf-variable", $
"CD-sersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-fitshape-fitcenter-unconstrained-multi-init-10-classic-pl-psf-variable", $
"CD-sersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-5-classic-pl-psf-variable", $
"CD-sersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-fitshape-fitcenter-unconstrained-multi-init-5-classic-pl-psf-variable", $
"CD-sersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-1-classic-pl-psf-variable", $
"CD-sersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-fitshape-fitcenter-unconstrained-multi-init-1-classic-pl-psf-variable", $
"CD-deV-G-deV-exp-fpC/stage-1/flag-fitsize-unconstrained-multi-init-1-nstep-9000-double-apsector-lockre-updated-mask-lmrecord-all-classic-pl-psf-variable", $
"CD-deV-Pl-G-deV-exp-fpC/stage-1/flag-fitsize-unconstrained-multi-init-1-nstep-9000-double-apsector-lockre-updated-mask-lmrecord-all-classic-pl-psf-variable", $
"CD-deV-G-deV-exp-fpC/stage-1/flag-fitsize-fitshape-fitcenter-unconstrained-multi-init-1-lockre-classic-pl-psf-variable", $
"CD-deV-Pl-G-deV-exp-fpC/stage-1/flag-fitsize-fitshape-fitcenter-unconstrained-multi-init-1-lockre-classic-pl-psf-variable", $
"CD-deV-Pl-G-deV-exp-fpC/stage-1/flag-fitsize-unconstrained-multi-init-1-nstep-9000-double-apsector-lockre-updated-mask-lmrecord-all", $
"CD-deV-G-deV-exp-fpC/stage-1/flag-fitsize-unconstrained-multi-init-1-nstep-9000-apsector-lockre-full-sky-all-pix", $
"CD-deV-Pl-G-deV-exp-fpC/stage-1/flag-fitsize-unconstrained-multi-init-1-nstep-9000-apsector-lockre-full-sky-all-pix", $
"CD-deV-G-deV-exp-fpC/stage-1/flag-fitsize-unconstrained-multi-init-1-nstep-9000-double-apsector-lockre-improved-LRG-full-sky-good-pix", $
"CD-deV-Pl-G-deV-exp-fpC/stage-1/flag-fitsize-unconstrained-multi-init-1-nstep-9000-double-apsector-lockre-improved-LRG-full-sky-good-pix", $
"CD-deV-G-deV-exp-fpC/stage-1/flag-fitsize-unconstrained-multi-init-1-nstep-9000-long-double-apsector-lockre-improved-LRG-full-sky-good-pix", $
"CD-deV-Pl-G-deV-exp-fpC/stage-1/flag-fitsize-unconstrained-multi-init-1-nstep-9000-long-double-apsector-lockre-improved-LRG-full-sky-good-pix", $
"CD-deV-G-deV-exp-fpC/stage-1/flag-fitsize-unconstrained-multi-init-1-nstep-9000-float-apsector-lockre-improved-LRG-full-sky-good-pix", $
"CD-deV-Pl-G-deV-exp-fpC/stage-1/flag-fitsize-unconstrained-multi-init-1-nstep-9000-float-apsector-lockre-improved-LRG-full-sky-good-pix", $
"CD-deV-G-deV-exp-fpC/stage-1/flag-fitsize-unconstrained-multi-init-1-nstep-9000-apsector-lockre-full-sky-good-pix", $
"CD-deV-Pl-G-deV-exp-fpC/stage-1/flag-fitsize-unconstrained-multi-init-1-nstep-9000-apsector-lockre-full-sky-good-pix",$
"CD-deV-G-deV-exp-fpC/stage-1/flag-fitsize-unconstrained-multi-init-1-nstep-9000-apsector-double-full-sky-good-pix", $
"CD-deV-Pl-G-deV-exp-fpC/stage-1/flag-fitsize-unconstrained-multi-init-1-nstep-9000-apsector-double-full-sky-good-pix"]

mle_type_index = lindgen(5) ; lindgen(5) + 1L ;; [4, 3] ;; 1, 3, 6, 4, 2, 5] ; lindgen(6) ;; lindgen(10) ;; [0, 1] ;; , 2, 3, 4, 5] ;[0, 1, 2, 3, 4, 5] ;; , 0, 1]

mle_type_list = mle_type_list[mle_type_index]

flagLRG_list = [-1, 1, 2] 
flagLRG_list = [-1]

distance_match_flag_list = [1, 0]
dm_prefix_list = ["pos-match-", "pos-shape-match-"]
distance_match_flag_list = [distance_match_flag_list[0:0]]
dm_prefix_list = [dm_prefix_list[0:0]]

chisq_list = [5.0D10, 15.0D6]
chisq_prefix_list = ["chisq-5D10-", "chisq-15D6-"]
chisq_list = [chisq_list[0:0]]
chisq_prefix_list = [chisq_prefix_list[0:0]]

do_fake_cd2 = 0

root3="/u/dss/sas/dr12/boss/photo/redux"


for i_objctype = 0L, n_elements(objctype_list) - 1L, 1L do begin
for i_mle_type = 0L, n_elements(mle_type_list) - 1L, 1L do begin
for i_flagLRG = 0L, n_elements(flagLRG_list) - 1L, 1L do begin
for i_dm_list = 0L, n_elements(distance_match_flag_list) - 1L, 1L do begin
for i_chisq = 0L, n_elements(chisq_list) - 1L, 1L do begin
for i_dir = 0L, n_elements(dir_list) - 1L, 1L do begin

dolrg = dolrg_list[i_objctype]
dogalaxy = dogalaxy_list[i_objctype]
dostar = dostar_list[i_objctype]
objc_prefix = objctype_list[i_objctype]

dir = dir_list[i_dir]
mle_type = mle_type_list[i_mle_type]

root1="/u/khosrow/thesis/opt/soho/multi-object/images/sims/"+dir
root1 = root3
if (mle2 eq 0) then begin
	root2 = root1 ;; used when analyzing photo results
endif else begin
	root2="/u/khosrow/thesis/opt/soho/multi-object/images/mle/"+dir+"/"+mle_type
endelse
flagLRG = flagLRG_list[i_flagLRG]
distance_match = distance_match_flag_list[i_dm_list]
dm_prefix = dm_prefix_list[i_dm_list]
rejection_chisq = chisq_list[i_chisq]
chisq_prefix = chisq_prefix_list[i_chisq]

prefix = dir+"-"+prefix0 + objc_prefix + dm_prefix + chisq_prefix

print, "root1 = ", root1
print, "root2 = ", root2
print, "flagLRG = ", flagLRG
print, "match style = ", dm_prefix
print, "rejection chisq = ", rejection_chisq

report = draw_matched_pairs_for_run_list(root1 = root1, root2 = root2, root3 = root3, $
	camcol_list = camcol_list, $
	sas1 = sas1, sim1 = sim1, sas3 = sas3, mle2 = mle2, $
	fake_cd2 = do_fake_cd2, boxplots = boxplots, $
	galaxy = dogalaxy, star = dostar, lrg = dolrg, flagLRG = flagLRG, $
	distance_match = distance_match, $
	locmap = locmap, mag_thresh = mag_thresh, prefix = prefix, suffix = suffix, $
	rejection_distance = rejection_distance, rejection_chisq = rejection_chisq, /data)

endfor
endfor
endfor
endfor
endfor
endfor

return
end
