#ifndef THPCOSTTYPES_H
#define THPCOSTTYPES_H

#include <string.h>
#include <stdio.h>
#include "thConsts.h"
#include "thBasic.h"

typedef struct thpcost_generic {
	CHISQFL r1, r2;
	CHISQFL pcost;
	CHISQFL DpcostDhalo;
	CHISQFL DpcostDcore;
	CHISQFL DDpcostDhaloDhalo;
	CHISQFL DDpcostDcoreDcore;
	CHISQFL DDpcostDhaloDcore;

	CHISQFL u1, u2;
	CHISQFL DpcostDuhalo;
	CHISQFL DpcostDucore;
	CHISQFL DDpcostDuhaloDuhalo;
	CHISQFL DDpcostDucoreDucore;
	CHISQFL DDpcostDuhaloDucore;

	CHISQFL e1, e2;
	CHISQFL DpcostDehalo, DpcostDecore;
	CHISQFL DDpcostDehaloDehalo, DDpcostDecoreDecore, DDpcostDehaloDecore; 
	CHISQFL E1, E2;
	CHISQFL DpcostDEhalo, DpcostDEcore;
	CHISQFL DDpcostDEhaloDEhalo, DDpcostDEcoreDEcore, DDpcostDEhaloDEcore; 	
} PCOST_GENERIC;

PCOST_GENERIC *thPCostGenericNew();
void thPCostGenericDel(PCOST_GENERIC *pcost_generic);

#endif
