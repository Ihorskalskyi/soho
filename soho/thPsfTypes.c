#include "thPsfTypes.h"
#include "thDebug.h"

static const SCHEMA *schema_psf_convolution_type = NULL;

static void init_static_vars(void);
static RET_CODE psf_get_default_ncomponent(PSF_CONVOLUTION_TYPE type, int ncomponent, int *ncomponent_default);

void init_static_vars(void) {
	TYPE t;
	t = shTypeGetFromName("PSF_CONVOLUTION_TYPE");
	schema_psf_convolution_type = shSchemaGetFromType(t);
	shAssert(schema_psf_convolution_type != NULL);
	return;
}

void free_psf_types_static_memory(void) {
	if (schema_psf_convolution_type != NULL) schema_psf_convolution_type = NULL;
	return;
}

PHPSFMODEL *thPhpsfmodelNew() {

  char *name = "thPhpsfmodelNew";

  PHPSFMODEL *phpsf;
  phpsf = (PHPSFMODEL*) thCalloc(1, sizeof(PHPSFMODEL));

  thError("%s: WARNING - temporary constructor - should not be present in dispatched binary", name);
  
  return(phpsf);

}

void thPhpsfmodelDel(PHPSFMODEL *phpsf) {
  if (phpsf == NULL) return;
  
  if (phpsf->basis != NULL) phPsfBasisDel(phpsf->basis);
  if (phpsf->wing != NULL) phPsfWingDel(phpsf->wing);
  if (phpsf->cc != NULL) thCrudeCalibDel(phpsf->cc); 
  phpsf->basis = NULL;
  phpsf->wing = NULL;
  phpsf->cc = NULL;
  thFree(phpsf);

  return;
}


PSFWING *thPsfwingNew() {
  
  PSFWING *wing;
  wing = (PSFWING *) thCalloc(1, sizeof(PSFWING));
  
  return(wing);
}

void thPsfwingDel(PSFWING *wing) {
  
  if (wing == NULL) return;
  
  shRegDel(wing->reg);
  wing->reg = NULL;
  thFree(wing);
  
  return;
}

FFTW_KERNEL * thFftwKernelNew() {
FFTW_KERNEL *fftw_kern = thCalloc(1, sizeof(FFTW_KERNEL));
fftw_kern->source = UNKNOWN_PSF_SOURCE;
return(fftw_kern);
}

void thFftwKernelDel(FFTW_KERNEL *fftw_kern) {
if (fftw_kern == NULL) return;
char *name = "thFftwKernelDel";
int err = 0;
int prow, pcol;
prow = fftw_kern->prow;
pcol = fftw_kern->pcol;
if (prow > 0 && pcol > 0) {
	if (fftw_kern->fftw_kern != NULL) {
		thFree(fftw_kern->fftw_kern[0]);
		thFree(fftw_kern->fftw_kern);
	} else {
		thError("%s: ERROR - improperly allocated (fftw_kern) : (fftw_kern) 2d-matrix is empty while (prow, pcol) = (%d, %d)", name, prow, pcol);
	err++;
	}
} else if (fftw_kern->fftw_kern != NULL) {
	thError("%s: ERROR - improperly allocated (fftw_kern) : non-null (fftw_kern) 2d-matrix while (prow, pcol) = (%d, %d)", name, prow, pcol);
	err++;
}
prow = fftw_kern->planrow;
pcol = fftw_kern->plancol;
#if NO_FFTW_DURING_TEST
#else
if (prow > 0 && pcol > 0) {
	fftwnd_destroy_plan(fftw_kern->plan_fwd);
	fftwnd_destroy_plan(fftw_kern->plan_bck);
}
#endif
if (err == 0) {
	thFree(fftw_kern);
} else {
	thError("%s: ERROR - (%d) errors while deleting (fftw_kernel), cannot free memory", name, err);
}
return;
}

RET_CODE thFftwKernelPutPsfA(FFTW_KERNEL *fftw_kern, THPIX psf_a) {
char *name = "thFftwKernelPutPsfA";
if (fftw_kern == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (psf_a <= 0.0) {
	thError("%s: WARNING - possible problem with (psf_a = %g <= 0.0) detected", name, (float) psf_a);
}
fftw_kern->psf_a = psf_a;
return(SH_SUCCESS);
}

RET_CODE thFftwKernelGetDirReg(FFTW_KERNEL *fftw_kern, REGION **reg) {
char *name = "thFftwKernelGetDirReg";
if (fftw_kern == NULL && reg == NULL) {	
	thError("%s: WARNING - null input and output placeholder", name);
	return(SH_SUCCESS);
} 
if (reg == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (fftw_kern == NULL) {
	thError("%s: ERROR - null input", name);
	*reg = NULL;
	return(SH_GENERIC_ERROR);
}
*reg = fftw_kern->reg;
return(SH_SUCCESS);
}


RET_CODE thFftwKernelGetCounts(FFTW_KERNEL *fftw_kern, THPIX *counts, THPIX *hcounts) {
char *name = "thFftwKernelGetCounts";
if (fftw_kern == NULL && counts == NULL && hcounts == NULL) {
	thError("%s: WARNING - null input and output placeholder", name);
	return(SH_SUCCESS);
} else if (fftw_kern == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
} else if (counts == NULL && hcounts == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}

if (counts != NULL) *counts = fftw_kern->counts;
if (hcounts != NULL) *hcounts = fftw_kern->hcounts;
return(SH_SUCCESS);
}

PSFMODEL *thPsfmodelNew() {
  
  PSFMODEL *psf;
  psf = (PSFMODEL *) thCalloc(1, sizeof(PSFMODEL));

  return(psf);
}

void thPsfmodelDel(PSFMODEL *psf) {
 
  if (psf == NULL) return;

  PHPSFMODEL *parent = psf->parent;
  if (parent == NULL || parent->basis != psf->basis) {
	phPsfBasisDel(psf->basis); 
	psf->basis = NULL;
  }
  thPsfwingDel(psf->wing);
  psf->wing = NULL;  
  if (parent == NULL || parent->cc != psf->cc) {
  	thCrudeCalibDel(psf->cc); 
	psf->cc = NULL;
  }
  thFree(psf);
  
  return;
}

RET_CODE psf_check_type_ncomponent_consistency(PSF_CONVOLUTION_TYPE type, int ncomponent) {
	if (ncomponent == DEFAULT_NCOMPONENT) return(SH_SUCCESS);
	if (ncomponent <= 0) return(SH_GENERIC_ERROR);
	if (ncomponent > N_PSF_COMPONENT) return(SH_GENERIC_ERROR);
	if (type == NO_PSF_CONVOLUTION) return(SH_SUCCESS);
	if (type == CENTRAL_PSF && ncomponent > 0) return(SH_SUCCESS);
	if (type == RADIAL_PSF && ncomponent > 0) return(SH_SUCCESS);
	if (type == CENTRAL_AND_WINGS_PSF && ncomponent == 1) return(SH_SUCCESS);
	if (type == STAR_CENTRAL_AND_WINGS && ncomponent == 1) return(SH_SUCCESS); /* should never be invoked */	
	return(SH_GENERIC_ERROR);
}
RET_CODE psf_get_default_ncomponent(PSF_CONVOLUTION_TYPE type, int ncomponent, int *ncomponent_default) {
	shAssert(ncomponent_default != NULL);
	if (type == NO_PSF_CONVOLUTION) {
		if (ncomponent > 0 && ncomponent <= N_PSF_COMPONENT) {
			*ncomponent_default = ncomponent;
			return(SH_SUCCESS);
		} else if (ncomponent == DEFAULT_NCOMPONENT) {
			*ncomponent_default = DEFAULT_NCOMPONENT;
			return(SH_SUCCESS);
		} else {
			return(SH_GENERIC_ERROR);
		}	
	} else if (type == CENTRAL_PSF || type == RADIAL_PSF) {
		*ncomponent_default = DEFAULT_NCOMPONENT;
		return(SH_SUCCESS);
	} else if (type == CENTRAL_AND_WINGS_PSF || type == STAR_CENTRAL_AND_WINGS) {
		*ncomponent_default = DEFAULT_NCOMPONENT;
		return(SH_SUCCESS);
	} else {
		return(SH_GENERIC_ERROR);
	}
}

WPSF *thWpsfNew() {
WPSF *wpsf;
wpsf = thCalloc(1, sizeof(WPSF));
wpsf->psf_reg = phPsfRegNew(0, 0, TYPE_THPIX);
int i;
for (i = 0; i < N_PSF_COMPONENT; i++) {
	wpsf->fftw_kernel[i] = thFftwKernelNew();
}
return(wpsf);
}

void thWpsfDel(WPSF *wpsf) {
if (wpsf == NULL) return;
if (wpsf->psf_reg != NULL) phPsfRegDel(wpsf->psf_reg);
wpsf->psf_reg = NULL;
int i;
for (i = 0; i < N_PSF_COMPONENT; i++) {
	if (wpsf->fftw_kernel[i] != NULL) thFftwKernelDel(wpsf->fftw_kernel[i]);
	wpsf->fftw_kernel[i] = NULL;
}
thFree(wpsf);
return;
}

RET_CODE thWpsfUpdate(WPSF *wpsf, PSFMODEL *psf_model) {
char *name = "thWpsfUpdate";
if (wpsf == NULL || psf_model == NULL) {
	thError("%s:ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

PSFWING *psfwing;
if ((psfwing = psf_model->wing) == NULL) {
	thError("%s: ERROR - null (wing): improperly allocated (psf_model)", name);
	return(SH_GENERIC_ERROR);
}
REGION *reg;
if ((reg = psfwing->reg) == NULL) {
	thError("%s: ERROR - null (reg) in (psf_wing): improperly compiled or allocated (psf_model)", name);
	return(SH_GENERIC_ERROR);
}
if (reg->nrow == 0 || reg->ncol == 0) {
	thError("%s: WARNING - (psfwing) image in (reg) not properly loaded (nrow, ncol) = (%d, %d)", name, reg->nrow, reg->ncol);
	return(SH_GENERIC_ERROR);
}

#if 1 /* finding a memory bug - jan 23, 2018 */
wpsf->psf_model = psf_model;
#else
wpsf->psf_model = NULL;
#endif
return(SH_SUCCESS);
}

PSF_CONVOLUTION_REPORT *thPsf_convolution_reportNew() {
PSF_CONVOLUTION_REPORT *report = thCalloc(1, sizeof(PSF_CONVOLUTION_REPORT));
return(report);
}
void thPsf_convolution_reportDel(PSF_CONVOLUTION_REPORT *report) {
if (report == NULL) return;
thFree(report);
return;
}

RET_CODE thPsfConvReportUpdate(PSF_CONVOLUTION_REPORT *report, THPIX *alpha_inner, 
				THPIX *alpha_outer, THPIX *bkg_outer, THPIX *lost_light, THPIX *halo_frac) {
char *name = "thPsfConvReportUpdate";
if (report == NULL) {
	thError("%s: ERROR - null input and output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (alpha_outer != NULL) report->alpha_outer = *alpha_outer;
if (alpha_inner != NULL) report->alpha_inner = *alpha_inner;
if (bkg_outer != NULL) report->bkg_outer = *bkg_outer;
if (lost_light != NULL) report->lost_light = *lost_light;
if (halo_frac != NULL) report->halo_frac = *halo_frac;

if (lost_light != NULL && *lost_light < (THPIX) 0.0) thError("%s: WARNING - (lost light = %g) found to be negative", name, (float) *lost_light);
if (halo_frac != NULL && *halo_frac < (THPIX) 0.0) thError("%s: WARNING - (halo frac = %g) found to be negative", name, (float) *halo_frac);


return(SH_SUCCESS);
}

RET_CODE thPsfConvReportGet(PSF_CONVOLUTION_REPORT *report, THPIX *alpha_inner, THPIX *alpha_outer, THPIX *bkg_outer) {
char *name = "thPsfConvReportGet";
if (report == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (alpha_inner != NULL) *alpha_inner = report->alpha_inner;
if (alpha_outer != NULL) *alpha_outer = report->alpha_outer;
if (bkg_outer != NULL) *bkg_outer = report->bkg_outer;
return(SH_SUCCESS);
}

PSF_CONVOLUTION_INFO *thPsf_convolution_infoNew(PSF_CONVOLUTION_TYPE type, int ncomponent) {
	char *name = "thPsfConvInfoNew";
	if (psf_check_type_ncomponent_consistency(type, ncomponent) != SH_SUCCESS) {
		if (schema_psf_convolution_type == NULL) init_static_vars ();
		thError("%s: ERROR - (psf_convolution_type) '%s' and (ncomponent) (%d) don't seem consistent", 
			name, (schema_psf_convolution_type->elems[type]).name, ncomponent);
		return(NULL);
	}
	if (psf_get_default_ncomponent(type, ncomponent, &ncomponent) != SH_SUCCESS) {
		if (schema_psf_convolution_type == NULL) init_static_vars ();
		 thError("%s: ERROR - could not get the default (ncomponent) for (psf_convolution_type) '%s'",
			name, (schema_psf_convolution_type->elems[type]).name);
		return(NULL);
	}
	PSF_CONVOLUTION_INFO *x = thCalloc(1, sizeof(PSF_CONVOLUTION_INFO));
	x->conv_type = type;
	x->ncomponent = ncomponent;

	/* for reporting purposed only - has no calculation value */
	x->report = thPsfConvReportNew();
	return(x);
}

void thPsf_convolution_infoDel(PSF_CONVOLUTION_INFO *psf_conv_info) {
	if (psf_conv_info == NULL) return;
	thPsfConvReportDel(psf_conv_info->report);
	thFree(psf_conv_info);
	return;
}

RET_CODE thPsfConvInfoGetNComponent(PSF_CONVOLUTION_INFO *psf_info, int *n) {
char *name = "thPsfConvInfoGetNComponent";
if (psf_info == NULL && n == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
}
if (psf_info == NULL) {
	thError("%s: ERROR - null input, expecting output", name);
	return(SH_GENERIC_ERROR);
}
if (n == NULL) {
	thError("%s: WARNING - null output placeholder, process terminated", name);
	return(SH_SUCCESS);
}

PSF_CONVOLUTION_TYPE type = psf_info->conv_type;
if (type == UNKNOWN_PSF_CONVOLUTION) {
	thError("%s: ERROR - unknown PSF convotluion type", name);
	return(SH_GENERIC_ERROR);
} else if (type == NO_PSF_CONVOLUTION) {
	*n = psf_info->ncomponent;
	return(SH_SUCCESS);
} else if (type == CENTRAL_PSF) {
	*n = 1; /* psf_info->ncomponent; */
	return(SH_SUCCESS);
} else if (type == RADIAL_PSF) {
	*n = 1; /* psf_info->ncomponent; */
	return(SH_SUCCESS);
} else if (type == CENTRAL_AND_WINGS_PSF) {
	*n = 2;
	return(SH_SUCCESS);
} else if (type == STAR_CENTRAL_AND_WINGS) {
	*n = 2;
	return(SH_SUCCESS);
}

if (schema_psf_convolution_type == NULL) init_static_vars();
thError("%s: ERROR - unsupported PSF convolution type '%s'", 
	name, (schema_psf_convolution_type->elems[type]).name);
return(SH_GENERIC_ERROR);
}

RET_CODE thPsfConvInfoGetPsfConvType(PSF_CONVOLUTION_INFO *psf_info, PSF_CONVOLUTION_TYPE *type) {
char *name = "thPsfConvInfoGetPsfConvType";
if (psf_info == NULL && type == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
}
if (psf_info == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (type == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}

*type = psf_info->conv_type;
return(SH_SUCCESS);
}

RET_CODE thPsfInfoGetPsfConvReport(PSF_CONVOLUTION_INFO *psf_info, PSF_CONVOLUTION_REPORT **report) {
char *name = "thPsfInfoGetPsfConvReport";
if (psf_info == NULL && report == NULL) {
	thError("%s: WARNING - null input and output placeholder", name);
	return(SH_SUCCESS);
}
if (psf_info == NULL) {
	thError("%s: ERROR - null input", name);
	*report = NULL;
	return(SH_GENERIC_ERROR);
}
if (report == NULL) {
	thError("%s: WARNING - null output placeholder");
	return(SH_SUCCESS);
}
*report = psf_info->report;
return(SH_SUCCESS);
}

RET_CODE thPsfConvReportCopy(PSF_CONVOLUTION_REPORT *treport, PSF_CONVOLUTION_REPORT *sreport) {
shAssert(treport != NULL);
shAssert(sreport != NULL);
memcpy(treport, sreport, sizeof(PSF_CONVOLUTION_REPORT));
return(SH_SUCCESS);
}


RET_CODE thPsfConvInfoUpdate(PSF_CONVOLUTION_INFO *psf_info, 
		PSF_CONVOLUTION_TYPE type, int ncomponent) {
char *name = "thPsfConvInfoUpdate";
RET_CODE status;
status = psf_check_type_ncomponent_consistency(type, ncomponent);
if (status != SH_SUCCESS) {
	if (schema_psf_convolution_type == NULL) init_static_vars ();
	thError("%s: ERROR - (psf_convolution_type) '%s' and (ncomponent) (%d) don't seem consistent", 
	name, (schema_psf_convolution_type->elems[type]).name, ncomponent);
	return(status);
}
status = psf_get_default_ncomponent(type, ncomponent, &ncomponent);
if (status != SH_SUCCESS) {
	if (schema_psf_convolution_type == NULL) init_static_vars ();
	thError("%s: ERROR - could not get the default (ncomponent) for (psf_convolution_type) '%s'",
	name, (schema_psf_convolution_type->elems[type]).name);
	return(status);
}
psf_info->conv_type = type;
psf_info->ncomponent = ncomponent;
return(SH_SUCCESS);
}

RET_CODE thPsfInfoGetPsfReg(PSF_CONVOLUTION_INFO *psf_info, PSF_REG **psf_reg) {
char *name = "thPsfInfoGetPsfReg";
if (psf_info == NULL && psf_reg == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
}
if (psf_info == NULL || psf_info->wpsf == NULL) {
	thError("%s: ERROR - null input or improperly allocated (wpsf)", name);
	return(SH_GENERIC_ERROR);
}
if (psf_reg == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (psf_info->wpsf->psf_reg == NULL) {
	thError("%s: ERROR - null (psf_reg): improperly allocated (psf_info)", name);
	return(SH_GENERIC_ERROR);
}
*psf_reg = psf_info->wpsf->psf_reg;
return(SH_SUCCESS);
}

RET_CODE thPsfInfoGetCenter(PSF_CONVOLUTION_INFO *psf_info, THPIX *rowc, THPIX *colc) {
char *name = "thPsfInfoGetCenter";
if (psf_info == NULL && rowc == NULL && colc == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
}
if (psf_info == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (rowc == NULL && colc == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (rowc != NULL) *rowc = psf_info->rowc;
if (colc != NULL) *colc = psf_info->colc;
return(SH_SUCCESS);
}

RET_CODE thPsfInfoPutCenter(PSF_CONVOLUTION_INFO *psf_info, THPIX rowc, THPIX colc) {
char *name = "thPsfInfoGetCenter";
if (psf_info == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (rowc != rowc || colc != colc) {
	thError("%s: ERROR - (center) didn't pass NAN test", name);
	return(SH_GENERIC_ERROR);
}

psf_info->rowc = rowc;
psf_info->colc = colc;
return(SH_SUCCESS);
}


RET_CODE thPsfInfoGetPsfModel(PSF_CONVOLUTION_INFO *psf_info, PSFMODEL **psf_model) {
char *name = "thPsfInfoGetPsfModel";
if (psf_model == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
WPSF *wpsf;
if (psf_info == NULL || (wpsf = psf_info->wpsf) == NULL) {
	thError("%s: ERROR - null or improperly allocated (psf_info)", name);
	return(SH_GENERIC_ERROR);
}
if (wpsf->psf_model == NULL) {
	thError("%s: WARNING - null (psf_model)", name);
}

*psf_model = wpsf->psf_model;
return(SH_SUCCESS);
}

RET_CODE thPsfInfoGetPsfWing(PSF_CONVOLUTION_INFO *psf_info, PSFWING **psf_wing) {
char *name = "thPsfInfoGetPsfWing";
if (psf_wing == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (psf_info == NULL) {
	thError("%s: ERROR - null (psf_info)", name);
	*psf_wing = NULL;
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
PSFMODEL *psf_model;
status = thPsfInfoGetPsfModel(psf_info, &psf_model);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not obtain (psf_model) from (psf_info)", name);
	*psf_wing = NULL;
	return(status);
}
if (psf_model == NULL) {
	thError("%s: ERROR - null (psf_model): improperly allocated (psf_info)", name);
	*psf_wing = NULL;
	return(SH_GENERIC_ERROR);
}
if (psf_model->wing == NULL) {
	thError("%s: WARNING - null (wing) in (psf_model)", name);
}

*psf_wing = psf_model->wing;
return(SH_SUCCESS);
}
 
RET_CODE thPsfInfoGetPsfBasis(PSF_CONVOLUTION_INFO *psf_info, PSF_BASIS **psf_basis) {
char *name = "thPsfInfoGetPsfBasis";
if (psf_basis == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (psf_info == NULL || psf_info->wpsf == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (psf_info->wpsf->psf_model == NULL) {
	thError("%s: ERROR - null (psf_model): improperly allocated (psf_info)", name);
	*psf_basis = NULL;
	return(SH_SUCCESS);
}
*psf_basis = psf_info->wpsf->psf_model->basis;
if (*psf_basis == NULL) {
	thError("%s: WARNING - improperly allocated (psf_model) in (psf_info)", name);
	return(SH_SUCCESS);
}
return(SH_SUCCESS);
}

RET_CODE thPsfInfoPutMask(PSF_CONVOLUTION_INFO *psf_info, PSF_COMPONENT psf_c, 
	int row0, int col0, int nrow, int ncol, int margin) {
char *name = "thPsfInfoPutMask";
if (psf_info == NULL) {
	thError("%s: ERROR - null (psf_info)", name);
	return(SH_GENERIC_ERROR);
}
if (psf_c >= N_PSF_COMPONENT) {
	thError("%s: ERROR - unsupported (psf component)", name);
	return(SH_GENERIC_ERROR);
}
if (nrow <= 0 || ncol <= 0) {
	thError("%s: ERROR - unacceptable (nrow, ncol) = (%d, %d)", name, nrow, ncol);
	return(SH_GENERIC_ERROR);
}
if (margin < 0) {
	thError("%s: ERROR - not supporting (margin = %d < 0)", name, margin);
	return(SH_GENERIC_ERROR);
}
NAIVE_MASK *inmask, *outmask;
inmask = psf_info->inmask + psf_c;
outmask = psf_info->outmask + psf_c;

RET_CODE status;
status = thNaiveMaskFromRect(inmask, row0, row0 + nrow - 1, col0, col0 + ncol - 1);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not set the mask for component (%d) of (psf_info)", 
		name, psf_c);
	return(status);
}
status = thNaiveMaskExpand(outmask, inmask, margin);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extended (inmask) by margin (%d)", name, margin);
	return(status);
}
#if DEEP_DEBUG_PSFTYPES || DEBUG_PSF_VARIABLE_SIZE
int trow0, tcol0, tnrow, tncol;
trow0 = inmask->x0[0];
tcol0 = inmask->y0[0];
tnrow = inmask->x1[0] - trow0 + 1;
tncol = inmask->y1[0] - tcol0 + 1;
printf("%s: unpadded mask - (nrow, ncol, row0, col0 = %d, %d, %d, %d) \n", name, tnrow, tncol, trow0, tcol0);

trow0 = outmask->x0[0];
tcol0 = outmask->y0[0];
tnrow = outmask->x1[0] - trow0 + 1;
tncol = outmask->y1[0] - tcol0 + 1;
printf("%s: padded mask - (nrow, ncol, row0, col0 = %d, %d, %d, %d), margin = %d \n", name, tnrow, tncol, trow0, tcol0, margin);
#endif

psf_info->margin[psf_c] = margin;

return(SH_SUCCESS);
}

RET_CODE thPsfInfoGetMask(PSF_CONVOLUTION_INFO *psf_info, PSF_COMPONENT psf_c, NAIVE_MASK **inmask, NAIVE_MASK **outmask, int *margin) {

char *name = "thPsfInfoGetMask";
if (psf_info == NULL) {
	thError("%s: ERROR - null input (psf_info)", name);
	return(SH_GENERIC_ERROR);
}
if (inmask == NULL && outmask == NULL && margin == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (psf_c >= N_PSF_COMPONENT) {
	thError("%s: ERROR - unsupported value for (psf component = %d)", name, (int) psf_c);
	return(SH_GENERIC_ERROR);
}

if (inmask != NULL) *inmask = psf_info->inmask + psf_c;
if (outmask != NULL) *outmask = psf_info->outmask + psf_c;
if (margin != NULL) *margin = psf_info->margin[psf_c];

return(SH_SUCCESS);
}


RET_CODE thPsfInfoGetFftwKernel(PSF_CONVOLUTION_INFO *psf_info, PSF_COMPONENT psf_c, FFTW_KERNEL **fftw_kernel) {
char *name = "thPsfInfoGetFftwKernel";	
if (psf_info == NULL || psf_info->wpsf == NULL) {
	thError("%s: ERROR - null or improperly allocated input (psf_info)", name);
	return(SH_GENERIC_ERROR);
}
if (fftw_kernel == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (psf_c >= N_PSF_COMPONENT) {
	thError("%s: ERROR - unsupported value for (psf component = %d)", name, (int) psf_c);
	return(SH_GENERIC_ERROR);
}

*fftw_kernel = psf_info->wpsf->fftw_kernel[psf_c];
return(SH_SUCCESS);
}

RET_CODE thPsfInfoGetBorderMask(PSF_CONVOLUTION_INFO *psf_info, WMASK **wmask) {
char *name = "thPsfInfoGetBorderMask";
shAssert(psf_info != NULL);
shAssert(wmask != NULL);

RET_CODE status;
FFTW_KERNEL *fftw_kern;
status = thPsfInfoGetFftwKernel(psf_info, OUTER_PSF, &fftw_kern);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (fftw_kern) for 'OUTER_PSF'", name);
	*wmask = NULL;
	return(status);
}
if (fftw_kern->border == NULL) {
	thError("%s: WARNING - null (border) mask", name);
}
*wmask = fftw_kern->border;
return(SH_SUCCESS);
}

MYCALIB_IO *thMyCalibIoNew() {
MYCALIB_IO *x = thCalloc(1, sizeof(MYCALIB_IO));
int i;
for (i = 0; i < NCOLOR; i++) {
	x->nann_ap_frame[i] = MYNANN;
}

return(x);
}

void thMyCalibIoDel(MYCALIB_IO *x) {
if (x == NULL) return;
thFree(x);
}

RET_CODE phPsfModelGetCrudeCalib(PHPSFMODEL *psf, CRUDE_CALIB **cc) {
char *name = "phPsfModelGetCrudeCalib";
if (cc == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (psf == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
*cc = psf->cc;
return(SH_SUCCESS);
}

RET_CODE phPsfModelPutCrudeCalib(PHPSFMODEL *psf, CRUDE_CALIB *cc) {
char *name = "phPsfModelPutCrudeCalib";
if (psf == NULL) {
	thError("%s: ERROR - null (psf)", name);
	return(SH_GENERIC_ERROR);
}
if (cc != NULL && psf->cc != NULL) {
	thError("%s: WARNING - replaceing (cc) in (psf)", name);
}
psf->cc = cc;
return(SH_SUCCESS);
}

RET_CODE thPsfModelGetCrudeCalib(PSFMODEL *psf, CRUDE_CALIB **cc) {
char *name = "thPsfModelGetCrudeCalib";
if (cc == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (psf == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
*cc = psf->cc;
return(SH_SUCCESS);
}

RET_CODE thPsfModelPutCrudeCalib(PSFMODEL *psf, CRUDE_CALIB *cc) {
char *name = "thPsfModelPutCrudeCalib";
if (psf == NULL) {
	thError("%s: ERROR - null (psf)", name);
	return(SH_GENERIC_ERROR);
}
if (cc != NULL && psf->cc != NULL) {
	thError("%s: WARNING - replaceing (cc) in (psf)", name);
}
psf->cc = cc;
return(SH_SUCCESS);
}

PSFREPORT *thPsfreportNew() {
PSFREPORT *report = thCalloc(1, sizeof(PSFREPORT));
return(report);
}
void thPsfreportDel(PSFREPORT *x) {
if (x == NULL) return;
thFree(x);
return;
}

BRIEFPSFREPORT *thBriefpsfreportNew() {
BRIEFPSFREPORT *report = thCalloc(1, sizeof(BRIEFPSFREPORT));

report->name_c1 = thCalloc(MX_STRING_LEN, sizeof(char));
report->name_w1 = thCalloc(MX_STRING_LEN, sizeof(char));
report->name_c2 = thCalloc(MX_STRING_LEN, sizeof(char));
report->name_w2 = thCalloc(MX_STRING_LEN, sizeof(char));

return(report);
}

void thBriefpsfreportDel(BRIEFPSFREPORT *x) {
if (x == NULL) return; 

if (x->name_c1 != NULL) thFree(x->name_c1);
if (x->name_w1 != NULL) thFree(x->name_w1);
if (x->name_c2 != NULL) thFree(x->name_c2);
if (x->name_w2 != NULL) thFree(x->name_w2);
thFree(x);

return;
}

PSFDOTPRODUCT *thPsfDotProductNew() {
PSFDOTPRODUCT *prod = thCalloc(1, sizeof(PSFDOTPRODUCT));
return(prod);
}

void thPsfDotProductDel(PSFDOTPRODUCT *prod) {
if (prod == NULL) return;
thFree(prod);
return;
}

RET_CODE thPsfDotProductPut(PSFDOTPRODUCT *prod, MLEFL in_in, MLEFL out_out, MLEFL in_out, MLEFL in_one, MLEFL out_one, MLEFL one_one) {
char *name = "thPsfDotProductPut";
if (prod == NULL) {
	thError("%s: ERROR - output placeholder is null", name);
	return(SH_GENERIC_ERROR);
}
prod->in_in = in_in;
prod->out_out = out_out;
prod->in_out = in_out;
prod->in_one = in_one;
prod->out_one = out_one;
prod->one_one = one_one;
return(SH_SUCCESS);
}

RET_CODE thPsfDotProductGet(PSFDOTPRODUCT *prod, MLEFL *in_in, MLEFL *out_out, MLEFL *in_out, MLEFL *in_one, MLEFL *out_one, MLEFL *one_one) {
char *name = "thPsfDotProductGet";
if (in_in == NULL && in_out == NULL && out_out == NULL && in_one == NULL && out_one == NULL && one_one == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (prod == NULL) {
	thError("%s: ERROR - null input placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (in_in != NULL) *in_in = prod->in_in;
if (out_out != NULL) *out_out = prod->out_out;
if (in_out != NULL) *in_out = prod->in_out;
if (in_one != NULL) *in_one = prod->in_one;
if (out_one != NULL) *out_one = prod->out_one;
if (one_one != NULL) *one_one = prod->one_one;

return(SH_SUCCESS);
}

RET_CODE thPsfWingGetIntegLP(PSFWING *psfwing, THPIX **IntegLP, THPIX **rIntegLP, int *nr) {
char *name = "thPsfWingGetIntegLP";
if (psfwing == NULL || IntegLP == NULL || rIntegLP == NULL || nr == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (IntegLP != NULL) *IntegLP = psfwing->IntegLP;
if (rIntegLP != NULL) *rIntegLP = psfwing->rIntegLP;
if (nr != NULL) *nr = psfwing->nr;
return(SH_SUCCESS);
}

RET_CODE thPsfModelGetIntegLP(PSFMODEL *psfmodel, THPIX **IntegLP, THPIX **rIntegLP, int *nr) {
char *name = "thPsfModelGetIntegLP";
RET_CODE status;
PSFWING *psfwing = NULL;
status = thPsfModelGetPsfWing(psfmodel, &psfwing);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (psfwing) from (psfmodel)", name);
	if (IntegLP != NULL) *IntegLP = NULL;
	if (rIntegLP != NULL) *rIntegLP = NULL;
	if (nr != NULL) *nr = -1;
	return(status);
}
status = thPsfWingGetIntegLP(psfwing, IntegLP, rIntegLP, nr);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (IntegLP) from (psfwing)", name);
	if (IntegLP != NULL) *IntegLP = NULL;
	if (rIntegLP != NULL) *rIntegLP = NULL;
	if (nr != NULL) *nr = -1;
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE thWpsfGetIntegLP(WPSF *wpsf, THPIX **IntegLP, THPIX **rIntegLP, int *nr) {
char *name = "thWpsfGetIntegLP";
RET_CODE status;
PSFMODEL *psfmodel = NULL;
status = thWpsfGetPsfModel(wpsf, &psfmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could noget get (psfmodel) from (wpsf)", name);
	if (IntegLP != NULL) *IntegLP = NULL;
	if (rIntegLP != NULL) *rIntegLP = NULL;
	if (nr != NULL) *nr = -1;
	return(status);
}
status = thPsfModelGetIntegLP(psfmodel, IntegLP, rIntegLP, nr); 
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (IntegLP) from (psfmodel)", name);
	if (IntegLP != NULL) *IntegLP = NULL;
	if (rIntegLP != NULL) *rIntegLP = NULL;
	if (nr != NULL) *nr = -1;
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE thPsfModelGetPsfWing(PSFMODEL *psfmodel, PSFWING **psfwing) {
char *name = "thPsfModelGetPsfWing";
if (psfmodel == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	if (psfwing != NULL) *psfwing = NULL;
	return(SH_GENERIC_ERROR);
}
*psfwing = psfmodel->wing;
return(SH_SUCCESS);
}

RET_CODE thWpsfGetPsfModel (WPSF *wpsf, PSFMODEL **psfmodel) {
char *name = "thWpsfGetPsfModel";
if (wpsf == NULL || psfmodel == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
*psfmodel = wpsf->psf_model;
return(SH_SUCCESS);
}

RET_CODE thPsfConvInfoGetIntegLP(PSF_CONVOLUTION_INFO *psfinfo, THPIX **IntegLP, THPIX **rIntegLP, int *nr) {
char *name = "thPsfInfoGetIntegLP";
RET_CODE status;
PSFMODEL *psfmodel = NULL;
status = thPsfInfoGetPsfModel(psfinfo, &psfmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (psfmodel) from (psf_info)", name);
	if (IntegLP != NULL) *IntegLP = NULL;
	if (rIntegLP != NULL) *rIntegLP = NULL;
	if (nr != NULL) *nr = -1;
	return(status);
}
status = thPsfModelGetIntegLP(psfmodel, IntegLP, rIntegLP, nr);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (IntegLP) from (psfmodel)", name);
	if (IntegLP != NULL) *IntegLP = NULL;
	if (rIntegLP != NULL) *rIntegLP = NULL;
	if (nr != NULL) *nr = -1;
	return(status);
}
return(SH_SUCCESS);
}



