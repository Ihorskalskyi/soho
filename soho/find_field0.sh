function find_field0 {
local name="find_field0"

local Options=$@
local Optnum=$#

local lfield0=yes
local lrun=no
local lcamcol=no
local lband=no

local field0=0000
local run=empty
local camcol=empty
local band=empty

while getopts ':-:' OPTION ; do
  case "$OPTION" in
    -  ) [ $OPTIND -ge 1 ] && optind=$(expr $OPTIND - 1 ) || optind=$OPTIND
         eval OPTION="\$$optind"
         OPTARG=$(echo $OPTION | cut -d'=' -f2)
         OPTION=$(echo $OPTION | cut -d'=' -f1)
         case $OPTION in
	     --run       ) lrun=yes; run="$OPTARG"	 ;;
	     --camcol    ) lcamcol=yes; camcol="$OPTARG" ;;
	     --band      ) lband=yes; band="$OPTARG"	 ;;
	     * )  echo "$name: invalide options (long)" ;;
         esac
       OPTIND=1
       shift
      ;;
    ? )  echo "$name: invalid options (short) "  ;;
  esac
done

if [ $lfield0 == no ] || [ $lrun == no ] || [ $lcamcol == no ] || [ $lband == no ]; then
	echo "$name: not enough arguments set"
	exit
fi
if [ $field0 == empty ] || [ $run == empty ] || [ $camcol == empty ] || [ $band == empty ]; then
	echo "$name: some arguments are invoked but not set"
	exit
fi

psFieldprefix="psField"
runstr=`printf "%06d" $run`
#psfdir="/u/dss/data/$run/137/objcs/$camcol"
#psfdir="PHOTO_REDUX/$run/137/objcs/$camcol"
psfdir="$PHOTO_REDUX/$DEFAULT_SDSS_RERUN/$run/objcs/$camcol"
if [ ! -d $psfdir ]; then 
	echo "$name: '$psfdir' does not exist"
	exit
fi

startfield=0
maxfield=9999
filefound=0;
ifield=$(( 10#$field0 ))
while (( filefound == 0 &&  ifield <= maxfield )); do
	field=`printf "%04d" $ifield`	
	psFieldfile="$psfdir/$psFieldprefix-$runstr-$camcol-$field.fit"
	if [ -e "$psFieldfile" ]; then
		startfield=$field
		filefound=1
		((ifield++))
	else
		filefound=0
	fi
done
echo $startfield
}

function find_f0 {
local name="find_f0"

local Options=$@
local Optnum=$#

local lrun=no
local lcamcol=no
local lband=no

local run=empty
local camcol=empty
local band=empty

while getopts ':-:' OPTION ; do
  case "$OPTION" in
    -  ) [ $OPTIND -ge 1 ] && optind=$(expr $OPTIND - 1 ) || optind=$OPTIND
         eval OPTION="\$$optind"
         OPTARG=$(echo $OPTION | cut -d'=' -f2)
         OPTION=$(echo $OPTION | cut -d'=' -f1)
         case $OPTION in
	     --run       ) lrun=yes; run="$OPTARG"	 ;;
	     --camcol    ) lcamcol=yes; camcol="$OPTARG" ;;
	     --band      ) lband=yes; band="$OPTARG"	 ;;
	     * )  echo "$name: invalide options (long)" ;;
         esac
       OPTIND=1
       shift
      ;;
    ? )  echo "$name: invalid options (short) "  ;;
  esac
done

if [ $lrun == no ] || [ $lcamcol == no ] || [ $lband == no ]; then
	echo "$name: not enough arguments set"
	exit
fi
if [ $run == empty ] || [ $camcol == empty ] || [ $band == empty ]; then
	echo "$name: some arguments are invoked but not set"
	exit
fi

psFieldprefix="psField"
runstr=`printf "%06d" $run`
# the following line was replaced after FAHL crash
#psfdir="PHOTO_REDUX/$run/137/objcs/$camcol"
psfdir="$PHOTO_REDUX/$DEFAULT_SDSS_RERUN/$run/objcs/$camcol"
if [ ! -d $psfdir ]; then 
	echo "$name: '$psfdir' does not exist"
	exit
fi

field0=0
filefound=0;
ifield=$(( 10#$field0 ))

pattern="$psFieldprefix-$runstr-$camcol-*.fit"
numfiles=$(find "$psfdir" -maxdepth 1 -type f -name "$pattern" 2>/dev/null|wc -l)
if [ $numfiles -eq 0 ]; then
	#echo "$name: '$psfdir' does not contain any psField files"
	echo "-1"
	exit
fi

while [ $filefound -eq 0 ] 
do
	field=`printf "%04d" $ifield`	
	psFieldfile="$psfdir/$psFieldprefix-$runstr-$camcol-$field.fit"
	if [ -e "$psFieldfile" ]; then
		filefound=1
	else	
		((ifield++))
		filefound=0
	fi
done
echo $ifield
}


