
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "strings.h"
#include "dervish.h"
#include "phPhotoFitsIO.h"
#include "phSpanUtil.h"
#include "phUtils.h"
#include "phObjc.h"
#include "sohoEnv.h"
#include "shDebug.h"
#include "shFpcIo.h"


#define FpmFileType STANDARD              /* FITS file Type: STANDARD, NONSTANDARD, IMAGE */
#define FpmFileDef  DEF_DEFAULT /* = DEF_NUM_OF_ELEMS    /* = DEF_NONE: shCFitsIo.h */
#define NROW_FPM 1489
#define NCOLUMN_FPM 2048
#define REGION_FLAGS_FPM NO_FLAGS

REGION *shObjmaskReadFpm(char *file,  
			 const int row0,  const int column0,  
			 const int row1,  const int column1, 
			 RET_CODE *shStatus);
RET_CODE shWriteFpm(char *file, 
		    MASK *mask);

RET_CODE shCpFpm(char *infile, char *outfile, /* input and output filenames */
		       const int row0, const int column0, 
		       const int row1, const int column1 /* corners of the image in INFILE */);

