
/* SDSS construction */
#include "dervish.h"
#include "phObjc.h"
#include "phPhotoFitsIO.h"
/* SOHO Libs */
#include "sohoEnv.h"
#include "thDebug.h"

RET_CODE thCpFpObjc(char *infile, char *outfile);
OBJC *thReadFpObjc(char *file, RET_CODE *status);
RET_CODE thWriteFpObjc(char *file, OBJC *objc);

