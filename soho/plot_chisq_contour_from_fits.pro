pro create_sim_chisq_plots

name = "plot_chisq_contour_from_fits.pro"

define_globals

fitsfile = "/u/khosrow/thesis/opt/soho/multi-object/images/mle/float/sim-demand-52a//mle-demand-112/flag-amplitude-unconstrained-init-1-nstep-9000-nearby-10-full-sky/301/4207/plots/match-sim-mle-gal-sim-demand-52a-mle-demand-112-nearby-pos-match-chisq-5D10-flag-amplitude-unconstrained-init-1-nstep-9000-nearby-10-full-sky-acc-camcols-acc-fields-all-004207.fits"

report_all = mrdfits(fitsfile, 1, hdr)

chisq_all_filename = "/u/khosrow/thesis/opt/soho/plots/plot-contour-sim-demand-52a-mle-demand-112-full-sky-CHISQ-all.eps"
cost_all_filename = "/u/khosrow/thesis/opt/soho/plots/plot-contour-sim-demand-52a-mle-demand-112-full-sky-COST-all.eps"


tps = 0
print, name, ": creating contour plots "
print, name, ": chi-squared value output to: ", chisq_all_filename
do_chisq_contour, output = chisq_all_filename, data = report_all, tps = tps
print, name, ": cost value output to: ", cost_all_filename
do_chisq_contour, output = cost_all_filename, data = report_all, /docost

return
end
