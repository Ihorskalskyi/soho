mname = "deV";
pname = "DEVPARS";
/* 
#if DEBUG_MGALAXY
printf("%s: initiating model '%s' with parameter type '%s' \n", name, mname, pname);
#endif
*/
f0 = (void *) &deVProf;
g0 = (void *) &MGInitDeV;
me = thModelElemNewInBank(mname, f0, pname, g0, &status);
if (status != SH_SUCCESS || me == NULL) {
	thError("%s: ERROR - could not introduce model '%s' to the bank", name, mname);
	if (status != SH_SUCCESS) return(status);
	return(SH_GENERIC_ERROR);
}
rname = "xc";
Df = (void *) &DdeVDxcProf;
status = thModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "yc";
Df = (void *) &DdeVDycProf;
status = thModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "re";
Df = (void *) &DdeVDreProf;
status = thModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "phi";
Df = (void *) &DdeVDphiProf;
status = thModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}
rname = "e";
Df = (void *) &DdeVDeProf;
status = thModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "E";
Df = (void *) &DdeVDEProf;
status = thModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "a";
Df = (void *) &DdeVDaProf;
status = thModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "b";
Df = (void *) &DdeVDbProf;
status = thModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}

rname = "c";
Df = (void *) &DdeVDcProf;
status = thModelElemAddDeriv(me, rname, Df);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add the derivative of model '%s' with respect to '%s' to the bank", name, mname, rname);
	return(status);
}


