#include "thFitobjcTypes.h"
#include "thProcTypes.h"
#include "thFuncmodelTypes.h"
#include "thMapTypes.h"
#include "thReg.h"

/* handling derivatives */
int thFitobjcGetNDeriv(FITOBJC *objc, RET_CODE *status);
FITOBJC *thFitobjcDerive(FITOBJC *objc, int i, RET_CODE *status);

/* 
   handling and calculating models, 
   i.e. x-y represnetation of the fit-object
*/

RET_CODE thFitobjcModel(FITOBJC *objc, REGION *dirreg, REGION *fftwreg);
RET_CODE thFitobjcModelAdd(FITOBJC *objc, 
			   REGION *dirreg, REGION *fftwreg,
			   REGION *dirtemp, REGION *fftwtemp,
			   long *FitobjcStat);
RET_CODE thSimpleFuncModelAdd(FUNCMODEL *objc, 
			      REGION *dirreg, REGION *fftwreg, 
			      REGION *dirtemp, REGION *fftwtemp,
			      long *FitobjcStat);
