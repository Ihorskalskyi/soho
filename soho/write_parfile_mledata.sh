function write_parfile_mledata {
local name="write_parfile_mledata"

local Options=$@
local Optnum=$#


local framenew=no
local lfield=no
local lrun=no
local lcamcol=no
local lband=no
local lmleparfile=no
local loutdir=no
local lcomment=no
local multibandmle=no
local lsource_framesdir=no
local lsource_psfdir=no
local lsource_fpobjcdir=no
local lsource_photoobjcdir=no
local lsource_fpBINdir=no
local lsource_fpMdir=no
local lsource_fpAtlasdir=no
local ltarget_objcdir=no


local field=empty
local run=empty
local camcol=empty
local band=empty
local mleparfile=empty
local outdir=empty
local comment=empty
local source_framesdir=empty
local tempdir=empty
local source_psfdir=empty
local source_fpobjcdir=empty
local source_photoobjcdir=empty
local source_fpBINdir=empty
local source_fpMdir=empty
local target_objcdir=empty

while getopts ':-:' OPTION ; do
  case "$OPTION" in
    -  ) [ $OPTIND -ge 1 ] && optind=$(expr $OPTIND - 1 ) || optind=$OPTIND
         eval OPTION="\$$optind"
         OPTARG=$(echo $OPTION | cut -d'=' -f2)
         OPTION=$(echo $OPTION | cut -d'=' -f1)
         case $OPTION in
	     
	     --framenew  ) framenew=yes	;;
	     --source_framesdir ) lsource_framesdir=yes; source_framesdir="$OPTARG" ;;
	     --source_psfdir	 ) lsource_psfdir=yes; source_psfdir="$OPTARG"	;;
	     --source_fpobjcdir ) lsource_fpobjcdir=yes; source_fpobjcdir="$OPTARG"  ;;
             --source_photoobjcdir ) lsource_photoobjcdir=yes; source_photoobjcdir="$OPTARG"  ;;
             --source_fpBINdir ) lsource_fpBINdir=yes; source_fpBINdir="$OPTARG"  ;;
             --source_fpMdir ) lsource_fpMdir=yes; source_fpMdir="$OPTARG"  ;;
	     --source_fpAtlasdir  )  lsource_fpAtlasdir=yes; source_fpAtlasdir="$OPTARG"  ;;
             --target_objcdir ) ltarget_objcdir=yes; target_objcdir="$OPTARG"  ;;
	     --field	 ) lfield=yes; field="$OPTARG"   ;;
	     --run       ) lrun=yes; run="$OPTARG"	 ;;
	     --camcol    ) lcamcol=yes; camcol="$OPTARG" ;;
	     --band      ) lband=yes; band="$OPTARG"	 ;;
	     --output    ) lmleparfile=yes; mleparfile="$OPTARG" 	;;
	     --outdir    ) loutdir=yes; outdir="$OPTARG"	;;
	     --comment   ) lcomment=yes; comment="$OPTARG"	;;
	     --multibandmle  ) multibandmle=yes			;;
	     * )  echo "$name: invalide options ($OPTION)" ;;
         esac
       OPTIND=1
       shift
      ;;
    ? )  echo "$name: invalid options (short) "  ;;
  esac
done

#echo "$name: root = $root, field = $field, field0 = $field0, run = $run, camcol = $camcol, band = $band, output = $mleparfile"

if [ $lfield == no ] || [ $lrun == no ] || [ $lcamcol == no ] || [ $lband == no ] || [ $lmleparfile == no ]; then
	echo "$name: not enough arguments set"
	exit
fi
if [ $field == empty ] || [ $run == empty ] || [ $camcol == empty ] || [ $band == empty ] || [ $mleparfile == empty ]; then
	echo "$name: some arguments are invoked but not set"
	exit
fi
if [ $loutdir == no ]; then
	echo "$name: (outdir) is not set"
	exit
fi
if [ $lsource_framesdir == no ]; then 
	source_framesdir="$outdir/temp"
fi
if [ $lsource_fpobjcdir == no ]; then
	source_fpobjcdir=
fi
if [ $lsource_photoobjcdir == no ]; then
	source_photoobjcdir=
fi
if [ $lsource_fpBINdir == no ]; then
	source_fpBINdir=
fi
if [ $lsource_fpMdir == no ]; then
	source_fpMdir=
fi
if [ $lsource_fpAtlasdir == no ]; then
	source_fpAtlasdir="$PHOTO_REDUX/$DEFAULT_SDSS_RERUN/$run/objcs/$camcol"
fi
if [ $ltarget_objcdir == no ]; then
	target_objcdir="$outdir/objc"
fi
if [ $lsource_psfdir == no ]; then
	source_psfdir="$PHOTO_REDUX/$DEFAULT_SDSS_RERUN/$run/objcs/$camcol"
fi

#echo "$name: outdir = $outdir"
#echo "$name: target_objcdir = $target_objcdir"

local fpCprefix="fpC"
local fpCCprefix="fpCC"
local frames_prefix="frame"
local psFieldprefix="psField"
local fpObjcprefix="fpObjc"
local photoObjcprefix="photoObj"
local gcprefix="gC"
local fpBINprefix="fpBIN"
local fpMprefix="fpM"
local fpAtlasprefix="fpAtlas"
local gpObjcprefix="gpObjc"
local lMprefix="lM"
local lPprefix="lP"
local lRprefix="lR"
local lZprefix="lZ"
local lWprefix="lW"
local psfRprefix="psfR"
local psfSprefix="psfS"


run=$(( 10#$run ))
psffield=$(( 10#$psffield ))
field=$(( 10#$field ))

runstr=`printf "%06d" $run`
psffieldstr=`printf "%04d" $psffield`
fieldstr=`printf "%04d" $field`

#the following lined was changed after FAHL's crash
#source_psfdir="/u/dss/data/$run/137/objcs/$camcol"
source_psfdir="$PHOTO_REDUX/$DEFAULT_SDSS_RERUN/$run/objcs/$camcol"
source_psField="$source_psfdir/psField-$runstr-$camcol-$fieldstr.fit"

if [ ! -e $source_psField ]; then
	echo -n "x"
fi
psFieldfile=$source_psField

skyobjc_datadir="$datadir"
skyobjc_datadir2="$datadir2"

if [[ $framenew == yes ]]; then
	fpCfile="$source_framesdir/frame-$band-$runstr-$camcol-$fieldstr.fits"
	fpCCfile="$source_framesdir/file-does-not-exist.fits"
else
	fpCfile="$source_framesdir/$fpCprefix-$runstr-$band$camcol-$fieldstr.fit"
	fpCCfile="$source_framesdir/$fpCCprefix-$runstr-$band$camcol-$fieldstr.fit"
fi
#psFieldfile=$target_psField #done above; differentiate between copy and non-copy
fpObjcfile="$source_fpobjcdir/$fpObjcprefix-$runstr-$camcol-$fieldstr.fit"
photoObjcfile="$source_photoobjcdir/$photoObjcprefix-$runstr-$camcol-$fieldstr.fits"
gcfile="$target_objcdir/$gcprefix-$runstr-$camcol-$fieldstr.fit"
fpBINfile="$source_fpBINdir/$fpBINprefix-$runstr-$band$camcol-$fieldstr.fit"
fpMfile="$source_fpMdir/$fpMprefix-$runstr-$band$camcol-$fieldstr.fit"
fpAtlasfile="$source_fpAtlasdir/$fpAtlasprefix-$runstr-$camcol-$fieldstr.fit"
gpObjcfile="$target_objcdir/$gpObjcprefix-$runstr-$band$camcol-$fieldstr.fit"
lMfile="$target_objcdir/$lMprefix-$runstr-$band$camcol-$fieldstr.fit"
lPfile="$target_objcdir/$lPprefix-$runstr-$band$camcol-$fieldstr.fit"
lRfile="$target_objcdir/$lRprefix-$runstr-$band$camcol-$fieldstr.fit"
lZfile="$target_objcdir/$lZprefix-$runstr-$band$camcol-$fieldstr.fit"
lWfile="$target_objcdir/$lWprefix-$runstr-$band$camcol-$fieldstr.fit"
psfRfile="$target_objcdir/$psfRprefix-$runstr-$band$camcol-$fieldstr.fit"
psfSfile="$target_objcdir/$psfSprefix-$runstr-$band$camcol-$fieldstr.fit"

phcalibfile="/u/dss/redux/resolve/full_02apr06/calibs/default2/137/$run/nfcalib/calibPhotomGlobal-$runstr-$camcol.fits"

logsdir="$PHOTO_REDUX/$DEFAULT_SDSS_RERUN/$run/logs"
phconfigpattern="opConfig-*.par"
phecalibpattern="opECalib-*.par"

phconfigfile=$(find "$logsdir" -maxdepth 1 -type f -name "$phconfigpattern")
phecalibfile=$(find "$logsdir" -maxdepth 1 -type f -name "$phecalibpattern")

if [ $multibandmle == yes ]; then
	if [[ $framenew == yes ]]; then
	inphflatframefileformat="$source_framesdir/frame-%c-$runstr-$camcol-$fieldstr.fits"
	inthflatframefileformat="$source_framesdir/file-for-%c-does-not-exist.fits"
	else
	inphflatframefileformat="$source_framesdir/$fpCprefix-$runstr-%c$camcol-$fieldstr.fit"
	inthflatframefileformat="$source_framesdir/$fpCCprefix-$runstr-%c$camcol-$fieldstr.fit"
	fi
inphsmfileformat="$source_fpBINdir/$fpBINprefix-$runstr-%c$camcol-$fieldstr.fit"
inphmaskfileformat="$source_fpMdir/$fpMprefix-$runstr-%c$camcol-$fieldstr.fit"
outthobjcfileformat="$target_objcdir/$gpObjcprefix-$runstr-%c$camcol-$fieldstr.fit"
outlMfileformat="$target_objcdir/$lMprefix-$runstr-%c$camcol-$fieldstr.fit"
outlPfileformat="$target_objcdir/$lPprefix-$runstr-%c$camcol-$fieldstr.fit"
outlRfileformat="$target_objcdir/$lRprefix-$runstr-%c$camcol-$fieldstr.fit"
outlZfileformat="$target_objcdir/$lZprefix-$runstr-%c$camcol-$fieldstr.fit"
outlWfileformat="$target_objcdir/$lWprefix-$runstr-%c$camcol-$fieldstr.fit"
outpsfreportfileformat="$target_objcdir/$psfRprefix-$runstr-%c$camcol-$fieldstr.fit"
outpsfstatsfileformat="$target_objcdir/$psfSprefix-$runstr-%c$camcol-$fieldstr.fit"
fi

if [ $lcomment == yes ]; then
echo "# $comment" > $mleparfile
echo "structure-type     = FRAMEFILES" >> $mleparfile
else
echo "structure-type     = FRAMEFILES" > $mleparfile
fi
#echo "phconfigfile       = /u/dss/data/$run/137/logs/opConfig-50000.par" >> $mleparfile
#echo "phecalibfile       = /u/dss/data/$run/137/logs/opECalib-51081.par" >> $mleparfile
echo "phconfigfile       = $phconfigfile #address after FAHL's failure" >> $mleparfile
echo "phecalibfile       = $phecalibfile #address after FAHL's failure" >> $mleparfile
echo "phpsfile           = $psFieldfile # PHOTO PSF (KL) (I)" >> $mleparfile
echo "phobjcfile         = $fpObjcfile  # PHOTO object list and properties (I)" >> $mleparfile
echo "photoObjcfile      = $photoObjcfile # PHOTO object list and properties - caliberated (I)" >> $mleparfile
echo "phcalibfile        = #$phcalibfile #PHOTOOP calibration data for the $run, $camcol (I) " >> $mleparfile
echo "gcfile		 = $gcfile	#GALACTIC_CALIB regression file to be generated or to be used (I / O)" >> $mleparfile

echo "phflatframefile    = $fpCfile     # PHOTO Flat-frame (I)" >> $mleparfile
echo "phmaskfile	 = $fpMfile     # PHOTO masks (I)" >> $mleparfile
echo "phatlasfile	 = $fpAtlasfile     # PHOTO atlas catalog for the field (I)" >> $mleparfile
echo "phsmfile		 = $fpBINfile   # PHOTO estimation of sky (I)" >> $mleparfile
echo "thflatframefile    = $fpCCfile    #continuous fpC image for simulation or fit (O)" >> $mleparfile
echo "thobjcfile         = $gpObjcfile  #object properties as found by mle (O)" >> $mleparfile		 
echo "thlmfile		 = $lMfile      #location of LM dump (O)" >> $mleparfile
echo "thlpfile		 = $lPfile      #location of LP dump (O)" >> $mleparfile
echo "thlrfile		 = $lRfile      #location of LR dump (O)" >> $mleparfile
echo "thlzfile		 = $lZfile      #location of LZ dump (O)" >> $mleparfile
echo "thlwfile		 = $lWfile      #location of LW dump (O)" >> $mleparfile
echo "thpsfreportfile    = $psfRfile    # location of PSF (report) file (O)" >> $mleparfile
echo "thpsfstatfile      = $psfSfile    # location of PSF (stat) file (O)" >> $mleparfile


if [ $multibandmle == yes ]; then

echo "inphflatframefileformat = $inphflatframefileformat #input fpC file format" >> $mleparfile
echo "inthflatframefileformat = $inthflatframefileformat #input fpCC file format" >> $mleparfile
echo "inphmaskfileformat      = $inphmaskfileformat #input fpM file format" >> $mleparfile
echo "inphsmfileformat        = $inphsmfileformat #input fpBIN file format" >> $mleparfile
echo "outthobjcfileformat     = $outthobjcfileformat #output gpObjc file format" >> $mleparfile
echo "outlMfileformat         = $outlMfileformat #output lM file format (chisq tracking)" >> $mleparfile
echo "outlPfileformat         = $outlPfileformat #output lP file format (1D profile for objects and their component models)" >> $mleparfile
echo "outlRfileformat         = $outlRfileformat #output lR file format (residual = fit model - data)" >> $mleparfile
echo "outlZfileformat         = $outlZfileformat #output lZ file format (z-score of residual image)" >> $mleparfile
echo "outlWfileformat         = $outlWfileformat #output lW file format (weight image)" >> $mleparfile
echo "outpsfreportfileformat  = $outpsfreportfileformat #output psf report file format" >> $mleparfile
echo "outpsfstatsfileformat   = $outpsfstatsfileformat #output psf statistics file format" >> $mleparfile

fi

echo "outdir             = $outdir #output directory for all other products (O)" >> $mleparfile
}

