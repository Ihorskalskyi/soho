#ifndef THMASKTYPES_H
#define THMASKTYPES_H

#include "dervish.h"
#include "phPhotoFitsIO.h"
#include "phSpanUtil.h"
#include "phUtils.h"
#include "phObjc.h"
#include "phDataIo.h"
#include "prvt/region_p.h"
#include "thConsts.h"

/* mask convention */
#define THMASK MASK
#define THMASKisMASK 1
#define THMASKisSPANMASK 0
#define THMASKisSUPERMASK 0

#if 0
typedef struct th_mask {
  SPANMASK *phspanmask;
  CHAIN *phobjmask;

  REGION *main_mask;
  REGION *sky_mask;
  REGION *objc_mask; 
  REGION *psf_mask;
  
}  SUPERMASK; /* pragma IGNORE */
#else
#define SUPERMASK REGION
#endif

#undef thMaskDel
#undef thMaskNew 
#if THMASKisMASK
#define thMaskDel shMaskDel
#define thMaskNew shMaskNew
#define MASKBIT U8
#define rows_mask rows
#endif
#if THMASKisSUPERMASK
#define thMaskDel shRegDel
#define thMaskNew shRegNew
#define MASKBIT U16
#define rows_mask rows_u16
#endif

typedef enum {
   TH_MASK_BAD = 0,             /* pixel's value should not be used in
				   fitting to any object or for sky */
   TH_MASK_GOOD, 		/* all the good pixels in the sky */
   TH_MASK_ALLOBJC,             /* all objects */
   TH_MASK_SKY,                 /* pixel part of the sky */
   TH_MASK_FITOBJC,             /* to be used for current fits */
   TH_NMASK_TYPES               /* number of types, MUST BE LAST */
} TH_MASKTYPE; /* pragma SCHEMA */

#define THBADBIT    (1 << TH_MASK_BAD)
#define THGOODBIT   (1 << TH_MASK_GOOD)
#define THALLOBJBIT (1 << TH_MASK_ALLOBJC)
#define THSKYBIT    (1 << TH_MASK_SKY)
#define THFITOBJBIT (1 << TH_MASK_FITOBJC)

typedef struct index_value_pair {
	int index;
	float value;
	void *objc1;
	void *objc2;
} INDEX_VALUE_PAIR; /*pragma IGNORE */
	
typedef enum {
	WMASK_INACTIVE = 0, 
	WMASK_ACTIVE = 1, 
	UNKNOWN_WMASK_ACTIVATION_FLAG, 
	N_WMASK_ACTIVATION_FLAG
} WMASK_ACTIVATION_FLAG; /* pragma SCHEMA */

typedef struct wmask {
	REGION *W;
	THMASK *mask;
  	#if (THMASKisMASK | THMASKisSUPERMASK)
  	MASKBIT maskbit; 
  	#endif
	
	#if 0	
	REGION *W_active[WMASK_NACTIVE];
	THMASK *mask_active[WMASK_NACTIVE];
	THMASK *mini_mask_active;
	int index_active, index_active_prev;
	int mini_index_active, mini_index_active_prev;
	int mask_nactive, mini_mask_nactive;
	int mini_batch_size_active, mini_batch_size_active_prev;
	WMASK_ACTIVATION_FLAG active_flag;
	#else
	REGION *W_active;
	THMASK *mask_active;
	REGION *random_active;
	int index_active, mask_nactive;
	int *index_active_list, index_active_index; 
	int mask_size, mask_active_size;
	#endif
	/* private */
	void *parent;
	void **wshuffle;
} WMASK; /* pragma IGNORE */

WMASK *thWmaskNew();
void thWmaskDel(WMASK *lwmask);
void thWmaskDeepDel(WMASK *wmask);
RET_CODE thWmaskCopy(WMASK *s, WMASK *t);

RET_CODE thMaskGetSize(THMASK *mask, int *nrow, int *ncol);
RET_CODE thLwmaskPrintInfo(WMASK *wmask, char *comment);
void thMaskClear(THMASK *mask);
RET_CODE thMaskSetPix(THMASK *mask, MASKBIT maskbit);

RET_CODE thLwmaskPrintInfo(WMASK *wmask, char *comment);
RET_CODE thLwmaskPutW(WMASK *lwmask, REGION *W);
RET_CODE thLwmaskPutMask(WMASK *lwmask, THMASK *mask);
RET_CODE thLwmaskPutMaskbit(WMASK *lwmask, MASKBIT maskbit);
RET_CODE thLwmaskGetW(WMASK *lwmask, REGION **W);
RET_CODE thLwmaskGetMask(WMASK *lwmask, THMASK **mask);
RET_CODE thLwmaskGetMaskbit(WMASK *lwmask, MASKBIT *maskbit);
RET_CODE thLwmaskGetWActive(WMASK *lwmask, REGION **W);
RET_CODE thLwmaskGetMaskActive(WMASK *lwmask, THMASK **mask);
RET_CODE thLwmaskGetIndexActive(WMASK *lwmask, int *index_active, int *mini_index_active);

#if 0
RET_CODE thLwmaskSample(WMASK *lwmask, int nactive, int mini_batch_nactive);
#else
RET_CODE thLwmaskSample(WMASK *lwmask, int nactive);
#endif
RET_CODE thLwmaskFree(WMASK *lwmask);
RET_CODE thLwmaskShuffle(WMASK *lwmask);
RET_CODE thLwmaskNextActive(WMASK *lwmask);

INDEX_VALUE_PAIR *thIndexValuePairNew();
void thIndexValuePairDel(INDEX_VALUE_PAIR *x);

RET_CODE thLwmaskGetMiniBatchSize(WMASK *lwmask, int *mini_batch_size);
#endif
