#ifndef THMGALAXYTYPES_H
#define THMGALAXYTYPES_H

#include "dervish.h"
#include "phObjc.h"
#include "thConsts.h"

typedef enum gclass {
	DEV_GCLASS, EXP_GCLASS, 
	DEVEXP_GCLASS, DEVDEV_GCLASS, DEVPL_GCLASS,  
	EXPDEV_GCLASS, EXPEXP_GCLASS,
	SERSIC_GCLASS, SERSICPL_GCLASS, SERSICEXP_GCLASS, SERSICSERSIC_GCLASS, CORESERSIC_GCLASS,  
	STAR_GCLASS, /* I suggest that STAR_GCLASS should not be supported in the final binary */
	N_GCLASS, UNKNOWN_GCLASS
} GCLASS;

/* generic model parameter type */
typedef struct gmodelpars {
	FL32 xc, yc; /* center location */
	FL32 re, ue; /* radial shape parameters */
	FL32 e, phi, E; /* canonical shape parameters */
	FL32 v, w; /* hyperbolic shape parameters */
	FL32 V, W; /* klog-hyperbolic shape paremeters */
	FL32 n, n1, n2; /* auxiliary shape parameters */
	FL32 a, b, c;   /* auxiliary shape parameters */
	FL32 sigma, sigma1, sigma2; /* auxiliary shape parameters */
	FL32 aphi, bphi, cphi;
	FL32 ae, be, ce;
	FL32 aE, bE, cE, eE, eEE;
	FL32 av, bv, cv;
	FL32 aw, bw, cw;
	FL32 aV, bV, cV;
	FL32 aW, bW, cW;
	FL32 reUe, reUeUe;
	FL32 alpha, beta, s2phi, c2phi;
	int band;	
	/* added on April 27, 2015 */
	FL32 lumcut, inner_lumcut, outer_lumcut;
} GMODELPARS;

typedef struct starpars {
	FL32 I; 
	FL32 xc, yc;
	FL32 mcount, counts;
	int band;
	/* added on April 27, 2015 */
	FL32 lumcut, inner_lumcut, outer_lumcut;
} STARPARS;

typedef struct devpars {
	FL32 I;
	FL32 xc, yc;
	FL32 re, ue;
	FL32 e, phi, E;
	FL32 v, w;
	FL32 V, W;
	FL32 a, b, c;
	FL32 aphi, bphi, cphi;
	FL32 ae, be, ce;
	FL32 aE, bE, cE;
	FL32 av, bv, cv;
	FL32 aw, bw, cw;
	FL32 aV, bV, cV;
	FL32 aW, bW, cW;
	FL32 reUe, reUeUe, eE, eEE;
	FL32 alpha, beta, s2phi, c2phi;
	FL32 mcount, counts;
	int band;
	/* added on April 27, 2015 */
	FL32 lumcut, inner_lumcut, outer_lumcut;
} DEVPARS;

typedef struct exppars {
	FL32 I;
	FL32 xc, yc;
	FL32 re, ue;
	FL32 e, phi, E;
	FL32 v, w;
	FL32 V, W;
	FL32 a, b, c;
	FL32 aphi, bphi, cphi;
	FL32 ae, be, ce;
	FL32 aE, bE, cE;
	FL32 av, bv, cv;
	FL32 aw, bw, cw;
	FL32 aV, bV, cV;
	FL32 aW, bW, cW;
	FL32 reUe, reUeUe, eE, eEE;
	FL32 alpha, beta, s2phi, c2phi;
	FL32 mcount, counts;
	int band;
	/* added on April 27, 2015 */
	FL32 lumcut, inner_lumcut, outer_lumcut;
} EXPPARS;

typedef struct sersicpars {
	FL32 I;
	FL32 xc, yc;
	FL32 re, ue;
	FL32 e, phi, E;
	FL32 n, N, k, inverse_2n, gamma_2n, ln_gamma_2n;
	FL32 v, w;
	FL32 V, W;
	FL32 a, b, c;
	FL32 aphi, bphi, cphi;
	FL32 ae, be, ce;
	FL32 aE, bE, cE;
	FL32 av, bv, cv;
	FL32 aw, bw, cw;
	FL32 aV, bV, cV;
	FL32 aW, bW, cW;
	FL32 reUe, reUeUe, eE, eEE, kn, nN;
	FL32 alpha, beta, s2phi, c2phi;
	FL32 mcount, counts;
	int band;
	/* added on April 27, 2015 */
	FL32 lumcut, inner_lumcut, outer_lumcut;
	int prv_cid;
} SERSICPARS;

typedef struct coresersicpars {
	FL32 I;
	FL32 xc, yc;
	FL32 re, ue;
	FL32 rb, ub;
	FL32 e, phi, E;
	FL32 n, N, k, inverse_2n, gamma_2n, ln_gamma_2n;
	FL32 delta, D, gamma, G;
	FL32 v, w;
	FL32 V, W;
	FL32 a, b, c;
	FL32 aphi, bphi, cphi;
	FL32 ae, be, ce;
	FL32 aE, bE, cE;
	FL32 av, bv, cv;
	FL32 aw, bw, cw;
	FL32 aV, bV, cV;
	FL32 aW, bW, cW;
	FL32 reUe, reUeUe, eE, eEE, kn, nN;
	FL32 rbUb, rbUbUb; 
	FL32 dD, gG;
	FL32 alpha, beta, s2phi, c2phi;
	FL32 mcount, counts;
	FL32 r50, k50; 
	int band;
	/* added on April 27, 2015 */
	FL32 lumcut, inner_lumcut, outer_lumcut;
} CORESERSICPARS;




typedef struct powerlawpars {
	FL32 I;
	FL32 xc, yc;
	FL32 re, ue;
	FL32 e, phi, E;
	FL32 v, w;
	FL32 V, W;
	FL32 a, b, c;
	FL32 aphi, bphi, cphi;
	FL32 ae, be, ce;
	FL32 aE, bE, cE;
	FL32 av, bv, cv;
	FL32 aw, bw, cw;
	FL32 aV, bV, cV;
	FL32 aW, bW, cW;
	FL32 reUe, reUeUe, eE, eEE;
	FL32 alpha, beta, s2phi, c2phi;
	FL32 n; /* index */
	FL32 mcount, counts;
	int band;
	/* added on April 27, 2015 */
	FL32 lumcut, inner_lumcut, outer_lumcut;
} POWERLAWPARS;

typedef struct gaussianpars {
	FL32 xc, yc;	
	FL32 a, b, c;	
	FL32 re, ue;
	FL32 phi, e, E;
	FL32 v, w;
	FL32 V, W;
	FL32 sigmax, sigmay; /* sigmax > sigmay */
	FL32 aphi, bphi, cphi; /* derivatives w.r.t. phi */
	FL32 ae, be, ce; /* derivatives w.r.t. e */
	FL32 aE, bE, cE, eE, eEE; /* derivatives w.r.t. E */
	FL32 av, bv, cv; /* derivatives w.r.t. shape parameters v, w */
	FL32 aw, bw, cw;
	FL32 aV, bV, cV; /* derivatives w.r.t. shape parameters V, W */
	FL32 aW, bW, cW;
	FL32 reUe, reUeUe; /* derivative w.r.t. super-logarithmic radial scale */
	FL32 alpha, beta;
	FL32 c2phi, s2phi;
	FL32 mcount, counts;
	int band;
	/* added on April 27, 2015 */
	FL32 lumcut, inner_lumcut, outer_lumcut;
} GAUSSIANPARS;

typedef struct objcpars {

	/* star component */
	FL32 I_star, J_star;
	FL32 xc_star, yc_star;
 	FL32 mcount_star;
	FL32 counts_star;
	FL32 flux_star, mag_star;

	/* standard errors */
	FL32 IErr_star, JErr_star;
	FL32 xcErr_star, ycErr_star;
	FL32 countsErr_star;
	FL32 fluxErr_star;
	FL32 magErr_star;

	/* accumulated galaxy models */
	FL32 counts_model;
	FL32 xc_model, yc_model;	
	FL32 flux_model, mag_model;
	FL32 chisq_model;
	int namp_model;

	/*standard errors */
	FL32 countsErr_model;
	FL32 xcErr_model, ycErr_model;	
	FL32 fluxErr_model, magErr_model;

	/* deV component */
	FL32 I_deV, J_deV;
	FL32 xc_deV, yc_deV;
	FL32 re_deV, e_deV, phi_deV;
	FL32 a_deV, b_deV, c_deV;
	FL32 ue_deV, E_deV;
	FL32 v_deV, w_deV;
	FL32 V_deV, W_deV;
	FL32 mcount_deV;
	FL32 counts_deV;
	FL32 flux_deV, mag_deV;

	/* standard errors */
	FL32 IErr_deV, JErr_deV;
	FL32 xcErr_deV, ycErr_deV;
	FL32 reErr_deV, eErr_deV, phiErr_deV;
	FL32 aErr_deV, bErr_deV, cErr_deV;
	FL32 ueErr_deV, EErr_deV;
	FL32 vErr_deV, wErr_deV;
	FL32 VErr_deV, WErr_deV;
	FL32 countsErr_deV;
	FL32 fluxErr_deV, magErr_deV;

	/* first (inner) deV component */
	FL32 I_deV1, J_deV1;
	FL32 xc_deV1, yc_deV1;
	FL32 re_deV1, e_deV1, phi_deV1;
	FL32 a_deV1, b_deV1, c_deV1;
	FL32 ue_deV1, E_deV1;
	FL32 v_deV1, w_deV1;
	FL32 V_deV1, W_deV1;
	FL32 mcount_deV1;
	FL32 counts_deV1;
	FL32 flux_deV1, mag_deV1;
	
	/* standard errors */
	FL32 IErr_deV1, JErr_deV1;
	FL32 xcErr_deV1, ycErr_deV1;
	FL32 reErr_deV1, eErr_deV1, phiErr_deV1;
	FL32 aErr_deV1, bErr_deV1, cErr_deV1;
	FL32 ueErr_deV1, EErr_deV1;
	FL32 vErr_deV1, wErr_deV1;
	FL32 VErr_deV1, WErr_deV1;
	FL32 countsErr_deV1;
	FL32 fluxErr_deV1, magErr_deV1;

	/* second (outer) deV component */
	FL32 I_deV2, J_deV2;
	FL32 xc_deV2, yc_deV2;
	FL32 re_deV2, e_deV2, phi_deV2;
	FL32 a_deV2, b_deV2, c_deV2;
	FL32 ue_deV2, E_deV2;
	FL32 v_deV2, w_deV2;
	FL32 V_deV2, W_deV2;
	FL32 mcount_deV2;
	FL32 counts_deV2;
	FL32 flux_deV2, mag_deV2;
	
	/* standard errors */
	FL32 IErr_deV2, JErr_deV2;
	FL32 xcErr_deV2, ycErr_deV2;
	FL32 reErr_deV2, eErr_deV2, phiErr_deV2;
	FL32 aErr_deV2, bErr_deV2, cErr_deV2;
	FL32 ueErr_deV2, EErr_deV2;
	FL32 vErr_deV2, wErr_deV2;
	FL32 VErr_deV2, WErr_deV2;
	FL32 countsErr_deV2;
	FL32 fluxErr_deV2, magErr_deV2;

	/* exponential component */
	FL32 I_Exp, J_Exp;
	FL32 xc_Exp, yc_Exp;
	FL32 re_Exp, e_Exp, phi_Exp;
	FL32 a_Exp, b_Exp, c_Exp;
	FL32 ue_Exp, E_Exp;
	FL32 v_Exp, w_Exp;
	FL32 V_Exp, W_Exp;
	FL32 mcount_Exp;
	FL32 counts_Exp;
	FL32 flux_Exp, mag_Exp;

	/* standard errors */
	FL32 IErr_Exp, JErr_Exp;
	FL32 xcErr_Exp, ycErr_Exp;
	FL32 reErr_Exp, eErr_Exp, phiErr_Exp;
	FL32 aErr_Exp, bErr_Exp, cErr_Exp;
	FL32 ueErr_Exp, EErr_Exp;
	FL32 vErr_Exp, wErr_Exp;
	FL32 VErr_Exp, WErr_Exp;
	FL32 countsErr_Exp;	
	FL32 fluxErr_Exp, magErr_Exp;

	/* first (inner) exponential component */

	FL32 I_Exp1, J_Exp1;
	FL32 xc_Exp1, yc_Exp1;
	FL32 re_Exp1, e_Exp1, phi_Exp1;
	FL32 a_Exp1, b_Exp1, c_Exp1;
	FL32 ue_Exp1, E_Exp1;
	FL32 v_Exp1, w_Exp1;
	FL32 V_Exp1, W_Exp1;
	FL32 mcount_Exp1;
	FL32 counts_Exp1;
	FL32 flux_Exp1, mag_Exp1;

	/* standard errors */
	FL32 IErr_Exp1, JErr_Exp1;
	FL32 xcErr_Exp1, ycErr_Exp1;
	FL32 reErr_Exp1, eErr_Exp1, phiErr_Exp1;
	FL32 aErr_Exp1, bErr_Exp1, cErr_Exp1;
	FL32 ueErr_Exp1, EErr_Exp1;
	FL32 vErr_Exp1, wErr_Exp1;
	FL32 VErr_Exp1, WErr_Exp1;
	FL32 countsErr_Exp1;
	FL32 fluxErr_Exp1, magErr_Exp1;

	/* second (outer) exponential component */

	FL32 I_Exp2, J_Exp2;
	FL32 xc_Exp2, yc_Exp2;
	FL32 re_Exp2, e_Exp2, phi_Exp2;
	FL32 a_Exp2, b_Exp2, c_Exp2;
	FL32 ue_Exp2, E_Exp2;
	FL32 v_Exp2, w_Exp2;
	FL32 V_Exp2, W_Exp2;
	FL32 mcount_Exp2;
	FL32 counts_Exp2;
	FL32 flux_Exp2, mag_Exp2;

	/* standard errors */
	FL32 IErr_Exp2, JErr_Exp2;
	FL32 xcErr_Exp2, ycErr_Exp2;
	FL32 reErr_Exp2, eErr_Exp2, phiErr_Exp2;
	FL32 aErr_Exp2, bErr_Exp2, cErr_Exp2;
	FL32 ueErr_Exp2, EErr_Exp2;
	FL32 vErr_Exp2, wErr_Exp2;
	FL32 VErr_Exp2, WErr_Exp2;
	FL32 countsErr_Exp2;
	FL32 fluxErr_Exp2, magErr_Exp2;

	/* powerlaw component */
/* 
	FL32 I_Pl, J_Pl;
	FL32 xc_Pl, yc_Pl;
	FL32 re_Pl, e_Pl, phi_Pl, n_Pl;
	FL32 a_Pl, b_Pl, c_Pl;
	FL32 ue_Pl, E_Pl;
	FL32 v_Pl, w_Pl;
	FL32 V_Pl, W_Pl;
	FL32 mcount_Pl;
	FL32 counts_Pl;
	FL32 flux_Pl, mag_Pl;
*/
	/* standard errors */
/* 
	FL32 IErr_Pl, JErr_Pl;
	FL32 xcErr_Pl, ycErr_Pl;
	FL32 reErr_Pl, eErr_Pl, phiErr_Pl, nErr_Pl;
	FL32 aErr_Pl, bErr_Pl, cErr_Pl;
	FL32 ueErr_Pl, EErr_Pl;
	FL32 vErr_Pl, wErr_Pl;
	FL32 VErr_Pl, WErr_Pl;
	FL32 countsErr_Pl;
	FL32 fluxErr_Pl, magErr_Pl;
*/
	/* sersic component */
	FL32 I_sersic, J_sersic;
	FL32 xc_sersic, yc_sersic;
	FL32 re_sersic, e_sersic, phi_sersic;
	FL32 k_sersic, n_sersic, N_sersic;
	FL32 a_sersic, b_sersic, c_sersic;
	FL32 ue_sersic, E_sersic;
	FL32 v_sersic, w_sersic;
	FL32 V_sersic, W_sersic;
	FL32 mcount_sersic;
	FL32 counts_sersic;
	FL32 flux_sersic, mag_sersic;

	/* standard errors */
	FL32 IErr_sersic, JErr_sersic;
	FL32 xcErr_sersic, ycErr_sersic;
	FL32 reErr_sersic, eErr_sersic, phiErr_sersic;
	FL32 kErr_sersic, nErr_sersic, NErr_sersic;
	FL32 aErr_sersic, bErr_sersic, cErr_sersic;
	FL32 ueErr_sersic, EErr_sersic;
	FL32 vErr_sersic, wErr_sersic;
	FL32 VErr_sersic, WErr_sersic;
	FL32 countsErr_sersic;
	FL32 fluxErr_sersic, magErr_sersic;

	/* first (inner) sersic component */
	FL32 I_sersic1, J_sersic1;
	FL32 xc_sersic1, yc_sersic1;
	FL32 re_sersic1, e_sersic1, phi_sersic1;
	FL32 k_sersic1, n_sersic1, N_sersic1;
	FL32 a_sersic1, b_sersic1, c_sersic1;
	FL32 ue_sersic1, E_sersic1;
	FL32 v_sersic1, w_sersic1;
	FL32 V_sersic1, W_sersic1;
	FL32 mcount_sersic1;
	FL32 counts_sersic1;
	FL32 flux_sersic1, mag_sersic1;

	/* standard errors */
	FL32 IErr_sersic1, JErr_sersic1;
	FL32 xcErr_sersic1, ycErr_sersic1;
	FL32 reErr_sersic1, eErr_sersic1, phiErr_sersic1;
	FL32 kErr_sersic1, nErr_sersic1, NErr_sersic1;
	FL32 aErr_sersic1, bErr_sersic1, cErr_sersic1;
	FL32 ueErr_sersic1, EErr_sersic1;
	FL32 vErr_sersic1, wErr_sersic1;
	FL32 VErr_sersic1, WErr_sersic1;
	FL32 countsErr_sersic1;
	FL32 fluxErr_sersic1, magErr_sersic1;

	
	/* second (outer) sersic component */
	FL32 I_sersic2, J_sersic2;
	FL32 xc_sersic2, yc_sersic2;
	FL32 re_sersic2, e_sersic2, phi_sersic2;
	FL32 k_sersic2, n_sersic2, N_sersic2;
	FL32 a_sersic2, b_sersic2, c_sersic2;
	FL32 ue_sersic2, E_sersic2;
	FL32 v_sersic2, w_sersic2;
	FL32 V_sersic2, W_sersic2;
	FL32 mcount_sersic2;
	FL32 counts_sersic2;
	FL32 flux_sersic2, mag_sersic2;

	/* standard errors */
	FL32 IErr_sersic2, JErr_sersic2;
	FL32 xcErr_sersic2, ycErr_sersic2;
	FL32 reErr_sersic2, eErr_sersic2, phiErr_sersic2;
	FL32 kErr_sersic2, nErr_sersic2, NErr_sersic2;
	FL32 aErr_sersic2, bErr_sersic2, cErr_sersic2;
	FL32 ueErr_sersic2, EErr_sersic2;
	FL32 vErr_sersic2, wErr_sersic2;
	FL32 VErr_sersic2, WErr_sersic2;
	FL32 countsErr_sersic2;
	FL32 fluxErr_sersic2, magErr_sersic2;

	/* coresersic component */
 
	FL32 I_coresersic, J_coresersic;
	FL32 xc_coresersic, yc_coresersic;
	FL32 re_coresersic, rb_coresersic;
	FL32 e_coresersic, phi_coresersic;
	FL32 k_coresersic, n_coresersic, N_coresersic;
	FL32 gamma_coresersic, delta_coresersic;
	FL32 G_coresersic, D_coresersic;
	FL32 a_coresersic, b_coresersic, c_coresersic;
	FL32 ue_coresersic, ub_coresersic, E_coresersic;
	FL32 v_coresersic, w_coresersic;
	FL32 V_coresersic, W_coresersic;
	FL32 mcount_coresersic;
	FL32 counts_coresersic;
	FL32 r50_coresersic, k50_coresersic; 
	FL32 flux_coresersic, mag_coresersic;

	/* standard errors */
	FL32 IErr_coresersic, JErr_coresersic;
	FL32 xcErr_coresersic, ycErr_coresersic;
	FL32 reErr_coresersic, rbErr_corsersic;
	FL32 eErr_coresersic, phiErr_coresersic;
	FL32 kErr_coresersic, nErr_coresersic, NErr_coresersic;
	FL32 gammaErr_coresersic, deltaErr_coresersic;
	FL32 GErr_coresersic, DErr_coresersic; 
	FL32 aErr_coresersic, bErr_coresersic, cErr_coresersic;
	FL32 ueErr_coresersic, ubErr_coresersic, EErr_coresersic;
	FL32 vErr_coresersic, wErr_coresersic;
	FL32 VErr_coresersic, WErr_coresersic;
	FL32 countsErr_coresersic;
	FL32 r50Err_coresersic, k50Err_coresersic; 
	FL32 fluxErr_coresersic, magErr_coresersic;
	
	/* second (outer) power-law component */
	/* 
	FL32 I_pl2, J_pl2;
	FL32 xc_pl2, yc_pl2;
	FL32 n_pl2;
	FL32 mcount_pl2;

	*/
	/* gaussian */

/* 
	FL32 I_gaussian, J_gaussian;
	FL32 a_gaussian, b_gaussian, c_gaussian;
	FL32 xc_gaussian, yc_gaussian;
	FL32 re_gaussian, e_gaussian, phi_gaussian;
	FL32 E_gaussian; 
	FL32 v_gaussian, w_gaussian;
	FL32 V_gaussian, W_gaussian;
	FL32 mcount_gaussian;
	FL32 counts_gaussian;
	FL32 flux_gaussian, mag_gaussian;
*/
	/* standard errors */	
 
/* 
	FL32 IErr_gaussian, JErr_gaussian;
	FL32 aErr_gaussian, bErr_gaussian, cErr_gaussian;
	FL32 xcErr_gaussian, ycErr_gaussian;
	FL32 reErr_gaussian, eErr_gaussian, phiErr_gaussian;
	FL32 EErr_gaussian; 
	FL32 vErr_gaussian, wErr_gaussian;
	FL32 VErr_gaussian, WErr_gaussian;
	FL32 countsErr_gaussian;
	FL32 fluxErr_gaussian, magErr_gaussian;
*/
	/* information for all moderls */
	/* accurate and crude calibration info
	FL32 kk, aa;
	FL32 flux20; 
	*/

	/* goodness of fit and deviation masures */
	int npar; /* number of object parameters */
	/*
  	number of masks initiated
	number of pixels used in each mask
  	radius of each mask 
  	rms count deviation as defined by Seiger, Graham, Jeijer 2007
  	portion of chi-squared value calculated in the respective mask 
	*/
 
	int nmask; 
	int npix_mask[NMASK_FITOBJC];
	FL32 radius_mask[NMASK_FITOBJC];
	FL32 delta_rms_mask[NMASK_FITOBJC]; 
	FL32 chisq_mask[NMASK_FITOBJC]; 

	/* chosen object specific chi-squared information */

	/* 
	FL32 rPetro_objc, rmask_objc;
	int npix_objc;
	FL32 chisq_objc, delta_rms_objc;
	*/

	/* 
	FL32 rPetroCoeff_objc;
	int indexPetro_objc;
	FL32 chisq_nu_objc;
	*/

	int  band;
} OBJCPARS;

GMODELPARS *thGmodelparsNew();
void thGmodelparsDel(GMODELPARS *q);
	
STARPARS *thStarparsNew();
void thStarparsDel(STARPARS *q);

DEVPARS *thDevparsNew();
void thDevparsDel(DEVPARS *dp);

EXPPARS *thExpparsNew();
void thExpparsDel(EXPPARS *q);

SERSICPARS *thSersicparsNew();
void thSersicparsDel(SERSICPARS *q);

CORESERSICPARS *thCoresersicparsNew();
void thCoresersicparsDel(CORESERSICPARS *q);

POWERLAWPARS *thPowerlawparsNew();
void thPowerlawparsDel(POWERLAWPARS *q);

GAUSSIANPARS *thGaussianparsNew();
void thGaussianparsDel(GAUSSIANPARS *q);

OBJCPARS *thObjcparsNew();
void thObjcparsDel(OBJCPARS *gp);

RET_CODE thDumpObjcparsWrite(FILE *fil, OBJCPARS *p, OBJ_TYPE sdsstype);
void simple_print_model_pars(void *q, char *qname);

#endif
