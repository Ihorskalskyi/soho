
if (xT != NULL) {
		if (lwork != NULL) {
			*xT = lwork->aT;
		} else {
			thError("%s: ERROR - null (lwork) in (lstruct)", name);
			return(SH_GENERIC_ERROR);
		}
	}





if (lwork->aT != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (aT) is allocated while (nap = %d)", name, lwork->nap);
	return(SH_GETERIC_ERROR);
}
if (lwork->aT != NULL) {
	thFree(lwork->aT);
	lwork->aT = NULL;
}
if (lwork->pT != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (pT) is allocated while (np = %d)", name, lwork->np);
	return(SH_GETERIC_ERROR);
}
if (lwork->pT != NULL) {
	lwork->pT = NULL;
}





if (lwork->wVeca != NULL) phVecDel(lwork->wVeca);
lwork->wVeca = NULL;
if (lwork->wVecp != NULL) phVecDel(lwork->wVecp);
lwork->wVecp = NULL;
if (lwork->wVecap != NULL) phVecDel(lwork->wVecap);
lwork->wVecap = NULL;



void copy_calib_wobjc_io_onto_calib_phobjc_io (CALIB_WOBJC_IO *s, CALIB_PHOBJC_IO *t) {
shAssert(s != NULL);
shAssert(t != NULL);

t->id = s->id;
t->parent = s->parent;
t->nchild = s->nchild;
t->objc_type = s->s_type;
t->objc_flags = s->s_flags;
t->objc_flags2 = s->s_flags2;
t->objc_rowc = s->s_rowc;
t->objc_colc = s->s_colc;
t->objc_rowcErr = s->s_rowcErr;
t->objc_colcErr = s->s_colcErr;
/* fix the following to degrees */
t->rowvDeg = s->rowv * (float) PIXARCSEC; 
t->colvDeg = s->colv * (float) PIXARCSEC;
t->rowvDegErr = s->rowvErr * (float) PIXARCSEC;
t->colvDegErr = s->colvErr * (float) PIXARCSEC;

memcpy(t->rowc, s->rowc, TCOLOR * sizeof(float));
memcpy(t->colc, s->colc, TCOLOR * sizeof(float));

memcpy(t->rowcErr, s->rowcErr, TCOLOR * sizeof(float));
memcpy(t->colcErr, s->colcErr, TCOLOR * sizeof(float));

memcpy(t->ab_deV, s->ab_deV, TCOLOR * sizeof(float));
memcpy(t->ab_deVErr, s->ab_deVErr, TCOLOR * sizeof(float));
memcpy(t->ab_exp, s->ab_exp, TCOLOR * sizeof(float));
memcpy(t->ab_expErr, s->ab_expErr, TCOLOR * sizeof(float));

/* should ensure that the input is in degrees */
memcpy(t->phi_deV_deg, s->phi_deV, TCOLOR * sizeof(float));
memcpy(t->phi_exp_deg, s->phi_exp, TCOLOR * sizeof(float));

memcpy(t->star_lnL, s->star_lnL, TCOLOR * sizeof(float));
memcpy(t->exp_lnL, s->exp_lnL, TCOLOR * sizeof(float));
memcpy(t->deV_lnL, s->deV_lnL, TCOLOR * sizeof(float));

memcpy(t->fracdeV, s->fracPSF, TCOLOR * sizeof(float));
memcpy(t->flags, s->flags, TCOLOR * sizeof(int));
memcpy(t->flags2, s->flags2, TCOLOR * sizeof(int));


memcpy(t->psfMag, s->psfMag, TCOLOR * sizeof(float));
memcpy(t->fiberMag, s->fiberMag, TCOLOR * sizeof(float));
memcpy(t->petroMag, s->petroMag, TCOLOR * sizeof(float));
memcpy(t->deVMag, s->deVMag, TCOLOR * sizeof(float));
memcpy(t->expMag, s->expMag, TCOLOR * sizeof(float));
memcpy(t->cmodelMag, s->modelMag, TCOLOR * sizeof(float));

(s->exp_lnL[R_BATD] > s->deV_lnL[R_BATD]) ? 
memcpy(t->modelMag, s->expMag, TCOLOR * sizeof(float)):
memcpy(t->modelMag, s->deVMag, TCOLOR * sizeof(float));

memcpy(t->psfMagErr, s->psfMagErr, TCOLOR * sizeof(float));
memcpy(t->fiberMagErr, s->fiberMagErr, TCOLOR * sizeof(float));
memcpy(t->petroMagErr, s->petroMagErr, TCOLOR * sizeof(float));
memcpy(t->deVMagErr, s->deVMagErr, TCOLOR * sizeof(float));
memcpy(t->expMagErr, s->expMagErr, TCOLOR * sizeof(float));
memcpy(t->cmodelMagErr, s->modelMagErr, TCOLOR * sizeof(float));

(s->exp_lnL[R_BATD] > s->deV_lnL[R_BATD]) ? 
memcpy(t->modelMagErr, s->expMagErr, TCOLOR * sizeof(float)):
memcpy(t->modelMagErr, s->deVMagErr, TCOLOR * sizeof(float));

/* should convert to angle measures rather from pixel */
memcpy(t->theta_deV, s->r_deV, TCOLOR * sizeof(float));
memcpy(t->theta_deVErr, s->r_deVErr, TCOLOR * sizeof(float));
memcpy(t->theta_exp, s->r_exp, TCOLOR * sizeof(float));
memcpy(t->theta_expErr, s->r_expErr, TCOLOR * sizeof(float));
memcpy(t->petroTheta, s->petroRad, TCOLOR * sizeof(float));
memcpy(t->petroThetaErr, s->petroRadErr, TCOLOR * sizeof(float));
memcpy(t->petroTh50, s->petroR50, TCOLOR * sizeof(float));
memcpy(t->petroTh50Err, s->petroR50Err, TCOLOR * sizeof(float));
memcpy(t->petroTh90, s->petroR90, TCOLOR * sizeof(float));
memcpy(t->petroTh90Err, s->petroR90Err, TCOLOR * sizeof(float));
int i;
for (i = 0; i < TCOLOR; i++) {
	t->theta_deV[i] *= (float) PIXARCSEC;
	t->theta_deVErr[i] *= (float) PIXARCSEC;
	t->theta_exp[i] *=  (float) PIXARCSEC;
	t->theta_expErr[i] *=  (float) PIXARCSEC;
	t->petroTheta[i] *=  (float) PIXARCSEC;
	t->petroThetaErr[i] *=  (float) PIXARCSEC;
	t->petroTh50[i] *=  (float) PIXARCSEC;
	t->petroTh50Err[i] *=  (float) PIXARCSEC;
	t->petroTh90[i] *=  (float) PIXARCSEC;
	t->petroTh90Err[i] *=  (float) PIXARCSEC;
}

return;
}

