#ifndef THPSF_H
#define THPSF_H

#include "phVariablePsf.h"
#include "thBasic.h"
#include "thMath.h"
#include "thMleTypes.h"
#include "thPsfTypes.h"

void init_psf_warning_count();

PSFMODEL *thConvertPhPsfToThPsf (PHPSFMODEL *phpsf, RET_CODE *status);
PSFWING *thPsfWingGetFromAnnulus(PHPSF_WING *phwing, int filter, 
				 RET_CODE *status);
RET_CODE thPsfRegGetFromAnnulus(PSFWING *psfwing);
REGION *thRegFromWingInfo(int maxr, THPIX *fI, 
			  THPIX *t, int dfI, int nrad, 
			  RET_CODE *status);
THPIX *thIntegLPFromWingInfo(int maxr, THPIX *fI, 
			  THPIX *t, int dfI, int nrad, 
			  RET_CODE *status);

/* ====================================*/

RET_CODE shRegConvolve(REGION *reg, REGION *outreg, PSF_CONVOLUTION_INFO *psf_info);


RET_CODE shRegConvCentralPsf(REGION *reg, REGION *outreg, PSF_CONVOLUTION_INFO *psf_info);
RET_CODE thPsfInfoUpdatePsfReg(PSF_CONVOLUTION_INFO *psf_info, PSF_REG **psf_reg_out);
RET_CODE thPsfKLReconstruct(const PSF_BASIS *basis, FFTW_KERNEL *fftw_kernel, 
				THPIX rowc, THPIX colc);

RET_CODE thConvolve(REGION *in, REGION *out, FFTW_KERNEL *fftw_kern);
RET_CODE thDirectConvolve(REGION *in, REGION *out, FFTW_KERNEL *fftw_kern, int margin);
RET_CODE thFFTWConvolve(REGION *in, REGION *out, FFTW_KERNEL *kern, int margin);

RET_CODE shRegConvWingPsf(REGION *reg, REGION *outreg, PSF_CONVOLUTION_INFO *psf_info);
RET_CODE thPsfInfoUpdateWingKern(PSF_CONVOLUTION_INFO *psf_info, FFTW_KERNEL **wing_kern);

FFTW_COMPLEX **thFftwComplexNewFromReg(REGION *in, FFTW_KERNEL *kern, int margin, int prow, int pcol, RET_CODE *status);
FFTW_COMPLEX **thFftwKernelGetComplex(FFTW_KERNEL *kern, int margin, int prow, int pcol, RET_CODE *status);
RET_CODE thFftwKernelUpdatePlan(FFTW_KERNEL *kern, int margin, int prow, int pcol);

FFTW_COMPLEX fftw_multiply(FFTW_COMPLEX a, FFTW_COMPLEX b);
RET_CODE thFftwComplex2dMultiply(FFTW_COMPLEX **in1, FFTW_COMPLEX **in2, FFTW_COMPLEX **out, int prow, int pcol);

FFTW_COMPLEX **thFftwComplex2dNew(int nrow, int ncol);
void thFftwComplex2dDel(FFTW_COMPLEX **arr);

RET_CODE shRegFromFftwComplex(FFTW_COMPLEX **carr, int prow, int pcol, FFTWFL scale, REGION *out);
RET_CODE thFftwGetPadSize(REGION *reg, FFTW_KERNEL *kern, int margin, int *prow, int *pcol);

/* ===== */

RET_CODE thPsfInfoUpdateWpsf(PSF_CONVOLUTION_INFO *psf_info);
RET_CODE thPsfInfoUpdateCounts(PSF_CONVOLUTION_INFO *psf_info);
RET_CODE thPsfInfoUpdatePsfA(PSF_CONVOLUTION_INFO *psf_info);
RET_CODE thPsfInfoUpdateFftwKernel(PSF_CONVOLUTION_INFO *psf_info, PSF_COMPONENT psf_c, PSF_SOURCE source, CONV_METHOD conv_method, int hrow, int hcol);

RET_CODE thFftwKernelCleanup(FFTW_KERNEL *fftw_kern);
RET_CODE thPsfInfoCleanup(PSF_CONVOLUTION_INFO *psf_info);
RET_CODE thConvTypeGetComponent(PSF_CONVOLUTION_TYPE conv_type, WPSF *wpsf, PSF_COMPONENT psf_c, PSF_SOURCE *source, CONV_METHOD *conv_method, int *hrow, int *hcol);


RET_CODE thWpsfGetSizeOfCentral(WPSF *wpsf, int *hrow, int *hcol);

RET_CODE thFftwKernelAddConst(FFTW_KERNEL *fftw_kern, THPIX bkg);
RET_CODE thAnnulusGetFromReg(PSFWING *psfwing);
THPIX *thISplineExpandAnnulus(THPIX *t, THPIX *fI, int nrad,  int *dfI); 


RET_CODE thPsfReportInit(PSFREPORT *report, THPIX xc, THPIX yc, int inboundary, int outboundary);
RET_CODE thPsfReportFinalize(PSFREPORT *report, PSF_CONVOLUTION_REPORT *creport); 
RET_CODE thPsfStatFromPsfReport(CHAIN *report_chain, BRIEFPSFREPORT *breport);

RET_CODE thPsfReportCompileFromPsfWing(PSFWING *psfwing, PSFREPORT *psfreport, int component);
RET_CODE thFWHMGetFromRadial(THPIX *rad, THPIX *a, int nrad, THPIX *FWHM);

RET_CODE thPsfReplaceKLPixels(REGION *klreg, REGION *wingreg, THPIX rin, THPIX alpha, THPIX gamma);
RET_CODE thPsfGetDotProduct(REGION *reg_inner, REGION *reg_outer, WMASK *w, PSFDOTPRODUCT *product);

RET_CODE thPsfGetAnndexVer(int *version);
RET_CODE thPsfSetAnndexVer(int version);

RET_CODE  thPsfConvInfoGetMarginFromFraction(THPIX fraction, void *p_ptr, PSF_CONVOLUTION_INFO *psf_info, int margin_min, int margin_max, int  margin_default, int *margin);
RET_CODE thPsfConvInfoGetMargin(void *q, char *qname, void *p_ptr, PSF_CONVOLUTION_INFO *psf_info, THPIX cutoff_psf, int margin_min, int margin_max, int margin_default, int *margin);
RET_CODE  thPsfConvInfoGetMarginFromWObjcIo(WOBJC_IO *q, void *p_ptr, PSF_CONVOLUTION_INFO *psf_info, THPIX cutoff_psf, int margin_min, int margin_max, int  margin_default, int *margin);

RET_CODE  thWpsfGetMarginFromFraction(THPIX fraction, void *p_ptr, WPSF *wpsf, int margin_min, int margin_max, int  margin_default, int *margin);
RET_CODE  thWpsfGetMarginFromWObjcIo(WOBJC_IO *q, void *p_ptr, WPSF *wpsf, THPIX cutoff_psf, int margin_min, int margin_max, int  margin_default, int *margin);

RET_CODE thFftwArrayCountBadPixels(FFTW_COMPLEX **fftw_arr, int prow, int pcol, int *nbadpixel);
#endif
