#ifndef THCALIBTYPES_H
#define THCALIBTYPES_H

#include <string.h>
#include "dervish.h"
#include "phConsts.h"
#include "phObjc.h"
#include "thConsts.h"
#include "thBasic.h"

#define thCrudeCalibNew thCrude_calibNew
#define thCrudeCalibDel thCrude_calibDel

#define thCrudeCalibIoNew thCrude_calib_ioNew
#define thCrudeCalibIoDel thCrude_calib_ioDel

#define PHCALIB PHCALIB
#define phCalibNew thPhcalibNew
#define phCalibDel thPhcalibDel
#define phCalibCopy thPhcalibCopy
#define thFitsFrameCalibRead thFitsPhCalibRead

#define thCalibWObjcIoNew thCalib_wobjc_ioNew
#define thCalibWObjcIoDel thCalib_wobjc_ioDel

#define thCalibPhObjcIoNew thCalib_phobjc_ioNew
#define thCalibPhObjcIoDel thCalib_phobjc_ioDel

#define thGalacticCalibNew thGalactic_calibNew
#define thGalacticCalibDel thGalactic_calibDel

#define thRegressionInfoNew thRegression_infoNew
#define thRegressionInfoDel thRegression_infoDel

/* the following structure is inspired by the webpage

http://data.sdss3.org/datamodel/files/PHOTO_CALIB/RERUN/RUN/nfcalib/calibPhotomGlobal.html

which illustrates the contents of the file

calibPhotomGlobal-$RUN-$CAMCOL.fits

*/
typedef enum calibtype {
	UNKNOWN_CALIBTYPE, NOCALIB, CRUDECALIB, FULLCALIB, N_CALIBTYPE
	} CALIBTYPE;

typedef struct crude_calib {
	char  filter;
	float flux20;
	float flux20Err;
	float b; /* b value structure added by K.A. but not present in SDSS products */
} CRUDE_CALIB;

typedef struct crude_calib_io {
	char filter[NCOLOR];
	float flux20[NCOLOR];
	float flux20Err[NCOLOR];
	int ncolor;
} CRUDE_CALIB_IO;

typedef struct phcalib {
	
    int run; /* (int32): drift scan run number */
    char *rerun; /* (string): reprocessing rerun number */
    int camcol; /* (int32): drift scan camcol number*/
    int field; /* (int32): drift scan field number */
    int image_status[NCOLOR]; /* (int32): bitmask describing image status in each band */
    int calib_status[NCOLOR]; /* (int32): bitmask describing calibration status in each band */
    double tai[NCOLOR]; /* (float64): TAI-format time each band was observed in (ugriz) */
    double ref_tai[NCOLOR]; /* (float64): reference time in TAI-format for time variation (kdot) */
    float airmass[NCOLOR]; /* (float32): airmass at time of observation in each batch */
    float aterm[NCOLOR]; /* (float32): "a-term" in calibration equations in each band */
    float kterm[NCOLOR]; /* (float32): "k-term" in calibration equations in each band (at reference time) */
    float kdot[NCOLOR]; /* (float32): time variation of "k-term" in calibration equations in each band */
    int nstars_offset[NCOLOR]; /* (int32): number of stars used in calculating field offset */
    float field_offset[NCOLOR]; /* (float32): magnitude offset of ubercalibration relative to input (???) */
    float nmgypercount[NCOLOR]; /* (float32): nanomaggies per count in each band */
    float nmgypercount_ivar[NCOLOR]; /* (float32): inverse variance of nanomaggies per count in each band */

} PHCALIB;

typedef enum magunit {
	LUPTITUDE, nMGY, UNKNOWN_UNIT, N_MAGUNIT
	} MAGUNIT;

/* the following structure is inspired by 
http://photo.astro.princeton.edu/#dm_calibobj
*/
typedef struct calib_wobjc_io {

	int id;				/* id number for this objc */
   	int parent;			        /* id of parent for deblends */
   	int ncolor;			/* number of colours */
   	int nchild;				/* number of children */
   	OBJ_TYPE objc_type;			/* overall classification */
   	float objc_prob_psf;			/* Bayesian probability of being PSF */
   	int catID;			        /* catalog id number */
   	int objc_flags;			/* flags from OBJC */
   	int objc_flags2;			/* flags2 from OBJC */
   	float objc_rowc, objc_rowcErr;	/* row position and error of centre */
	float objc_colc, objc_colcErr;	/* column position and error */
   	float rowv, rowvErr;			/* row velocity, in pixels/frame (NS)*/
   	float colv, colvErr;			/* col velocity, in pixels/frame (NS)*/

/*
 * Unpacked OBJECT1s
 */
	float rowc[NCOLOR], rowcErr[NCOLOR];
   	float colc[NCOLOR], colcErr[NCOLOR];
   	float sky[NCOLOR], skyErr[NCOLOR];

/* magnitudes instead of counts */

	float psfMag[NCOLOR], psfMagErr[NCOLOR];
	float fiberMag[NCOLOR], fiberMagErr[NCOLOR];
#if USE_FNAL_OBJC_IO
	float fiber2Mag[NCOLOR], fiber2MagErr[NCOLOR];
#endif
	float petroMag[NCOLOR], petroMagErr[NCOLOR];
	float deVMag[NCOLOR], deVMagErr[NCOLOR];	
	float expMag[NCOLOR], expMagErr[NCOLOR];
	float modelMag[NCOLOR], modelMagErr[NCOLOR];
/*
 * Classification information
 */
 	 float star_L[NCOLOR], star_lnL[NCOLOR];
  	 float exp_L[NCOLOR], exp_lnL[NCOLOR];
  	 float deV_L[NCOLOR], deV_lnL[NCOLOR];
	
/* magnitude units - Luptitude vs. nMGY */

	MAGUNIT magUnit;

/* surface brightness - added on July 14, 2016 */
	float petroMu[NCOLOR], petroMuErr[NCOLOR];
	float deVMu[NCOLOR], deVMuErr[NCOLOR];
	float expMu[NCOLOR], expMuErr[NCOLOR];

/*
 * Model parameters for deV and exponential models
 * copies from WOBJC_IO on July 14, 2016 
 */

   float petroRad[NCOLOR], petroRadErr[NCOLOR];
   float petroR50[NCOLOR], petroR50Err[NCOLOR];
   float petroR90[NCOLOR], petroR90Err[NCOLOR];
   float petroCounts[NCOLOR], petroCountsErr[NCOLOR];

   float r_deV[NCOLOR], r_deVErr[NCOLOR];
   float ab_deV[NCOLOR], ab_deVErr[NCOLOR];
   float phi_deV[NCOLOR], phi_deVErr[NCOLOR];
   float counts_deV[NCOLOR], counts_deVErr[NCOLOR];
   float r_exp[NCOLOR], r_expErr[NCOLOR];
   float ab_exp[NCOLOR], ab_expErr[NCOLOR];
   float phi_exp[NCOLOR], phi_expErr[NCOLOR];
   float counts_exp[NCOLOR], counts_expErr[NCOLOR];
   float counts_model[NCOLOR], counts_modelErr[NCOLOR];
  
   float fracPSF[NCOLOR]; 
/* 
 *  Classification information - added on July 14, 2016 
 *
 */
   int flags[NCOLOR];
   int flags2[NCOLOR];

/* this particular flag is to identify the result of LRG cut analysis - added by Khosrow Akbari on August 2016 */
   int flagLRG;

/* pointing to the object from which this was created */
   /* private */
   void *source;
} CALIB_WOBJC_IO; /* pragma IGNORE */


typedef struct calib_phobjc_io {
   char objid[19];
   char parentid[19];
   char fieldid[19];  
   unsigned char skyversion;
   unsigned char mode;
   unsigned char clean;
   short int run;
   char rerun[3];
   unsigned char camcol;
   short int field;

   short int id;				/* id number for this objc */
   short int parent;			        /* id of parent for deblends */
   short int nchild;				/* number of children */
   int objc_type;			/* overall classification */
   float objc_prob_psf;			/* Bayesian probability of being PSF */
   int objc_flags;			/* flags from OBJC */
   int objc_flags2;			/* flags2 from OBJC */
   float objc_rowc, objc_rowcErr;	/* row position and error of centre */
   float objc_colc, objc_colcErr;	/* column position and error */
   float rowvDeg, rowvDegErr;			/* row velocity, in pixels/frame (NS)*/
   float colvDeg, colvDegErr;			/* col velocity, in pixels/frame (NS)*/

/*
 * Unpacked OBJECT1s
 */
   float rowc[NCOLOR], rowcErr[NCOLOR];
   float colc[NCOLOR], colcErr[NCOLOR];

/*
 * PSF and aperture fits/magnitudes
 */
   float petroTheta[NCOLOR], petroThetaErr[NCOLOR];
   float petroTh50[NCOLOR], petroTh50Err[NCOLOR];
   float petroTh90[NCOLOR], petroTh90Err[NCOLOR];

/*
 * Shape of object
 */

   float Q[NCOLOR], QErr[NCOLOR], U[NCOLOR], UErr[NCOLOR];

   float M_e1[NCOLOR], M_e2[NCOLOR];
   float M_e1e1Err[NCOLOR], M_e1e2Err[NCOLOR], M_e2e2Err[NCOLOR];
   float M_rr_cc[NCOLOR], M_rr_ccErr[NCOLOR];
   float M_cr4[NCOLOR];
   float M_e1_psf[NCOLOR], M_e2_psf[NCOLOR];
   float M_rr_cc_psf[NCOLOR];
   float M_cr4_psf[NCOLOR];

/*
 * Model parameters for deV and exponential models
 */
   float theta_deV[NCOLOR], theta_deVErr[NCOLOR];
   float ab_deV[NCOLOR], ab_deVErr[NCOLOR];
   float theta_exp[NCOLOR], theta_expErr[NCOLOR];
   float ab_exp[NCOLOR], ab_expErr[NCOLOR];

   float fracdeV[NCOLOR];
   int flags[NCOLOR];
   int flags2[NCOLOR];
   OBJ_TYPE type[NCOLOR];
   float prob_psf[NCOLOR];
/*
 * Profile and extent of object
 */
   int nprof[NCOLOR];
   float profMean_nMgy[NCOLOR][NANN];
   float profErr_nMgy[NCOLOR][NANN];
/*
 * Classification information
 */
   float star_lnL[NCOLOR];
   float exp_lnL[NCOLOR];
   float deV_lnL[NCOLOR];
   int psp_status[NCOLOR];
   float pixscale[NCOLOR];
   double ra, dec; 
   double cx, cy, cz;
   double raErr, decErr;
   double L, B;
   float offsetRa[NCOLOR], offsetDec[NCOLOR];
   float psf_fwhm[NCOLOR];
   int   mjd;
   float airmass[NCOLOR];

   float phi_offset[NCOLOR];
   float phi_deV_deg[NCOLOR];
   float phi_exp_deg[NCOLOR];

   float extinction[NCOLOR];
   float skyflux[NCOLOR], skyflux_ivar[NCOLOR];
   float psfflux[NCOLOR], psfflux_ivar[NCOLOR];
   float psfMag[NCOLOR], psfMagErr[NCOLOR];
   float fiberflux[NCOLOR], fiberflux_ivar[NCOLOR];
   float fiberMag[NCOLOR], fiberMagErr[NCOLOR];
   float fiber2flux[NCOLOR], fiber2flux_ivar[NCOLOR];
   float fiber2Mag[NCOLOR], fiber2MagErr[NCOLOR];
   float cmodelflux[NCOLOR], cmodelflux_ivar[NCOLOR];
   float cmodelMag[NCOLOR], cmodelMagErr[NCOLOR];
   float modelflux[NCOLOR], modelflux_ivar[NCOLOR];
   float modelMag[NCOLOR], modelMagErr[NCOLOR];
   float petroflux[NCOLOR], petroflux_ivar[NCOLOR];
   float petroMag[NCOLOR], petroMagErr[NCOLOR];
   float deVflux[NCOLOR], deVflux_ivar[NCOLOR];
   float deVMag[NCOLOR], deVMagErr[NCOLOR];
   float expflux[NCOLOR], expflux_ivar[NCOLOR];
   float expMag[NCOLOR], expMagErr[NCOLOR];
   float aperflux[NCOLOR][8], aperflux_ivar[NCOLOR][8];
 
/* the following involves some enum types which is not currentlt defined within my code */ 
   int cloudcam[NCOLOR];
   int calib_status[NCOLOR];
   float nMgyperCount[NCOLOR], nMgyperCount_ivar[NCOLOR];
   double tai[NCOLOR];
   int resolve_status;
   int thing_id;
   int ifield, balkan_id;
   int nobserve, ndetect, nedge;
   float score;

/* the following was added by Khosrow Akbari on August 2016 */
   int flagLRG;

} CALIB_PHOBJC_IO;				/* pragma SCHEMA */


typedef struct regression {
	MATHFL s;
	MATHFL rss, tss;
	MATHFL R2, R2adj;
	MATHFL F;
	int df1, df2;
	MATHFL pvalue;
} REGRESSION_INFO;  /* pragma IGNORE */

typedef struct galactic_calib {
	int field;
	int camcol;
	int band;
	char bname;
	int run;
	char rerun[3];
	
	float beta[20];
	int df;
	int poldeg;
	char pos1[19], pos2[19];
	
	int n;
	float sigma, R2, R2adj, F;
	float rss, tss;
	float pvalue;
	int df1, df2;
} GALACTIC_CALIB;	/* pragma SCHEMA */


CRUDE_CALIB *thCrudeCalibNew();
void thCrudeCalibDel(CRUDE_CALIB *cc);

CRUDE_CALIB_IO *thCrudeCalibIoNew();
void thCrudeCalibIoDel(CRUDE_CALIB_IO *ccio);

CRUDE_CALIB *thCrudeCalibGetFromCCIo(CRUDE_CALIB_IO *ccio, int band);

PHCALIB *thPhcalibNew();
void thPhcalibDel(PHCALIB *fc);
RET_CODE thPhcalibCopy(PHCALIB *t, PHCALIB *s);

CALIB_WOBJC_IO *thCalib_wobjc_ioNew();
void thCalib_wobjc_ioDel(CALIB_WOBJC_IO *x);
void thCalibWObjcIoCopy(CALIB_WOBJC_IO *t, CALIB_WOBJC_IO *s);

CALIB_PHOBJC_IO *thCalib_phobjc_ioNew();
void thCalib_phobjc_ioDel(CALIB_PHOBJC_IO *x);
RET_CODE thCalibPhObjcIoCopy(CALIB_PHOBJC_IO *t, CALIB_PHOBJC_IO *s);

GALACTIC_CALIB *thGalactic_calibNew();
void thGalactic_calibDel(GALACTIC_CALIB *gc);

REGRESSION_INFO *thRegression_infoNew();
void thRegression_infoDel(REGRESSION_INFO *x);

#endif 
