#!/bin/bash

source "./generate_temporary_file.bash"
source "./run_commands_in_file.bash"

name="sim-sdss-batch"

rootdir="/u/khosrow/thesis/opt/soho/mutli-object/images/sims/sdss"
ppsf=0.001
run=$DEFAULT_SDSS_RUN
rerun=$DEFAULT_SDSS_RERUN

# generate a temporary command file
commandfile=$(generate_temporary_file --root="./" --prefix="temp-sim-sdss-submit-batch-" --suffix=".cmd")

bands="ugriz"
camcols="123456"
camcols="2456"
camcols="2"
nfield=5000
nfield=200
nstar=0
ngalaxy=3000
nlrg=1
cdflag="--cD"

declare -a exec_a=("do-thwart-const-noise")
declare -a dest_a=("")
declare -a model_a=("")
n2=1

declare -a psf_a=("9999")
declare -a psfdir_a=("9999-9999")
declare -a inner_lumcut_a=("0.0001")
declare -a outer_lumcut_a=("0.0001")
n3=1

#stripe 82
#4797
#declare -a run_a=(4797 2700 2703 2708 2709 2960 3565 3434 3437 2968 4207 1742 4247 4252 4253 4263 4288 1863 1887 2570 2578 2579 109 125 211 240 241 250 251 256 259 273 287 297 307 94 5036 5042 5052 2873 2124 1006 1009 1013 1033 1040 1055 1056 1057 4153 4157 4158 4184 4187 4188 4191 4192 2336 2886 2955 3325 3354 3355 3360 3362 3368 2385 2506 2728 4849 4858 4868 4874 4894 4895 4899 4905 4927 4930 4933 4948 2854 2855 2856 2861 2662 3256 3313 3322 4073 4198 4203 3384 3388 3427 3430 4128 4136 4145 2738 2768 2820 1752 1755 1894 2583 2585 2589 2591 2649 2650 2659 2677 3438 3460 3458 3461 3465 7674 7183)
n4=10
n4=${#run_a[@]}
n4=1
declare -a run_a=(4207 2700 2703 4797 2708 2709 2960 3565 3434 3437 2968 4207 1742)
n4=12
n4=1 

#declare -a dir_a=("sdss-g0-s0-lrg1" "sdss-g0-s0-lrgA" "sdss-gA-s0-lrg1" "sdss-gA-sA-lrg1" "sdss-gA-s0-lrgA" "sdss-gA-sA-lrgA")
#declare -a nstar_a=(0 0 0 5000 0 5000)
#declare -a ngalaxy_a=(0 0 5000 5000 5000 5000)
#declare -a nlrg_a=(1 5000 1 1 5000 5000)
#declare -a cD_a=(0 0 0 0 0 0)
#n5=6

#declare -a dir_a=("sdss-g0-s0-lrg1-aligned-6-poisson-noise"  "sdss-g0-s0-lrg1-aligned-6-variable-gaussian-noise" "sdss-gA-sA-cd1-aligned-2" "sdss-g0-sA-cd1" "sdss-gA-s0-cd1" "sdss-gA-sA-cd1" "sdss-g0-s0-cd1")
#declare -a dir_a=("demand-1"  "demand-2" "demand-3" "demand-4" "demand-11" "demand-12" "demand-13" "demand-14" "demand-6"  "demand-7" "demand-8" "demand-9" "demand-16" "demand-17" "demand-18" "demand-19")
#declare -a dir_a=("demand-21"  "demand-22" "demand-23" "demand-24" "demand-31" "demand-32" "demand-33" "demand-34" "demand-26"  "demand-27" "demand-28" "demand-29" "demand-36" "demand-37" "demand-38" "demand-39")
declare -a dir_a=("demand-41" "demand-42" "demand-43" "demand-44" "demand-51" "demand-52" "demand-53" "demand-54" "demand-46"  "demand-47" "demand-48" "demand-49" "demand-56" "demand-57" "demand-58" "demand-59" "demand-61" "demand-62" "demand-63" "demand-64" "demand-71" "demand-72" "demand-73" "demand-74" "demand-66"  "demand-67" "demand-68" "demand-69" "demand-76" "demand-77" "demand-78" "demand-79")
declare -a nstar_a=(0 5000 5000 0 5000 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
declare -a ngalaxy_a=(0 5000 0 5000 5000 0 0 0 00 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
declare -a nlrg_a=(1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1)
declare -a cD_a=(1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1)
#declare -a demand_a=("-demand 1" "-demand 2" "-demand 3" "-demand 4" "-demand 11" "-demand 12" "-demand 13" "-demand 14" "-demand 6"  "-demand 7" "-demand 8" "-demand 9" "-demand 16" "-demand 17" "-demand 18" "-demand 19")
#declare -a demand_a=("-demand 21" "-demand 22" "-demand 23" "-demand 24" "-demand 31" "-demand 32" "-demand 33" "-demand 34" "-demand 26"  "-demand 27" "-demand 28" "-demand 29" "-demand 36" "-demand 37" "-demand 38" "-demand 39")
declare -a demand_a=("-demand 41" "-demand 42" "-demand 43" "-demand 44" "-demand 51" "-demand 52" "-demand 53" "-demand 54" "-demand 46"  "-demand 47" "-demand 48" "-demand 49" "-demand 56" "-demand 57" "-demand 58" "-demand 59" "-demand 61" "-demand 62" "-demand 63" "-demand 64" "-demand 71" "-demand 72" "-demand 73" "-demand 74" "-demand 66"  "-demand 67" "-demand 68" "-demand 69" "-demand 76" "-demand 77" "-demand 78" "-demand 79")
n5=32

declare -a gaussian_noise_dn_a=(5.0 0.5 0.05, 0.005)
n_gaussian_dn=${#gaussian_noise_dn_a[@]}
m_gaussian_dn=$((n_gaussian_dn-1))

for (( i2=0; i2<$n2; i2++ ))
do

for (( i3=0; i3<$n3; i3++ ))
do

for (( i4=0; i4<$n4; i4++ ))
do

for (( i5=0; i5<$n5; i5++ ))
do



model=${model_a[$i2]}
destination=${dest_a[$i2]}
executable=${exec_a[$i2]}

fpsf=${psf_a[$i3]}
psfdir=${psfdir_a[$i3]}
inner_lumcut=${inner_lumcut_a[$i3]}
outer_lumcut=${outer_lumcut_a[$i3]}

run=${run_a[$i4]}

dir=${dir_a[$i5]}
nlrg=${nlrg_a[$i5]}
ngalaxy=${ngalaxy_a[$i5]}
nstar=${nstar_a[$i5]}
cD=${cD_a[$i5]}
demandflag=${demand_a[$i5]}

i_gaussian=$(python -S -c "import random; print random.randint(0,$m_gaussian_dn)")
gaussian_noise_dn=${gaussian_noise_dn_a[$i_gaussian]}
extraflags="$demandflag" # -gaussian_noise $gaussian_noise_dn" NOTE: gaussian noise should be chosen in a per field basis

if [ $cD == 1 ]; then
	cdflag="--cD"
else
	cdflag=""
fi

rootdir="/u/khosrow/thesis/opt/soho/multi-object/images/sims/float/$dir"
workdir="$rootdir/$destination"
outdir="$workdir/$rerun/$run"
sim-sdss-script.sh --unzip --executable=$executable --camcols=$camcols --nfield=$nfield --run=$run --rerun=$rerun --commandfile=$commandfile --docondor  --outdir=$outdir --nfield=$nfield --nstar=$nstar --ngalaxy=$ngalaxy --nlrg=$nlrg $cdflag --inner_lumcut=$inner_lumcut --outer_lumcut=$outer_lumcut --extraflags="$extraflags"

done
done
done
done 

echo
echo "$name: randomly running commands in file '$commandfile'"
run_commands_in_file --commandfile=$commandfile --random

echo
echo "$name: not deleting temp-file = $commandfile"
#rm $commandfile
