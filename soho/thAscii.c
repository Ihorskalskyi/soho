
#include "thAscii.h"

static const char *strutypefield = "structure-type";

typedef void (*readerfunc) (FILE *, void *);
typedef void (*readerfunc_heap) (FILE *, void *, const int);

static readerfunc *thfieldreaders_noheap;
static readerfunc_heap *thfieldreaders_heap;

static int tell_size_from_name (char *vname, int *size);

void thAsciiInit() {


  thfieldreaders_noheap = (readerfunc *) thCalloc(16, sizeof(readerfunc));
  
  thfieldreaders_noheap[(int) THASCIII] = &read_i;
  thfieldreaders_noheap[(int) THASCIIL] = &read_l;
  thfieldreaders_noheap[(int) THASCIILL] = &read_ll;
  
  thfieldreaders_noheap[(int) THASCIIF] = &read_f;
  thfieldreaders_noheap[(int) THASCIID] = &read_d;
  thfieldreaders_noheap[(int) THASCIILD] = &read_ld;
  
  thfieldreaders_noheap[(int) THASCIIS] = &read_s;

  thfieldreaders_heap = (readerfunc_heap *) thCalloc(16, sizeof(readerfunc_heap));
  
  thfieldreaders_heap[(int) THASCIII] = &read_i_heap;
  thfieldreaders_heap[(int) THASCIIL] = &read_l_heap;
  thfieldreaders_heap[(int) THASCIILL] = &read_ll_heap;
  
  thfieldreaders_heap[(int) THASCIIF] = &read_f_heap;
  thfieldreaders_heap[(int) THASCIID] = &read_d_heap;
  thfieldreaders_heap[(int) THASCIILD] = &read_ld_heap;

  /* this returns a warning due to function prototype incompatibility */
  thfieldreaders_noheap[(int) THASCIIS] = NULL; /*&read_s_heap;*/
}

int thAsciiNameRead(FILE *fil, char *vname, int *size) {

  char *name = "thAsciiNameRead";

  if (feof(fil)) {
    vname[0] = '\0';
    return(-1);
  }

  long floc;
  floc = ftell(fil);
  
  check_ln_delimiter(fil);
  
  char sign = '\0';
  fscanf(fil, "%s %c", vname, &sign);
  
  switch (sign) {
  case '=': case ':': case '~':
    if (size != NULL) {
      /* the following cuts the brackets at the end of vname and returns
	 size; will return -1 if there is no size information
      */
      tell_size_from_name(vname, size);
    }
    return(0);
    break;
  default:
    vname[0] = '\0';
    fseek(fil, floc, SEEK_SET);
    thError("%s: wrong field ascription sign (%c) at floc = %d", 
		   name, sign, floc);
    return (-1);
    break;
  }
 
}

int tell_size_from_name (char *vname, int *size) {
  
  char *name = "tell_size_from_name";

  if (size == NULL) {
    thError("%s: null (size) pointer", name);
  }

  int count;
  char *xname;
  xname = (char *) thCalloc(strlen(vname) + 1, sizeof(char));

  count = sscanf(vname, "%s[%d]", xname, size);
  if (count == 1) {
    *size = -1;
    strcpy(xname, vname);
  } else if (count == 0) {
    thError("%s: could not convert full name to name and size", name);
    thFree(xname);
      return(-1);
  } else if (count == 2) {
    strcpy(xname, vname);
  }

  thFree(xname);
  return(0);
}

int thAsciiValRead(FILE *fil, void *fieldval, 
		   THASCIITYPE eltype, THASCIIHEAPTYPE elheaptype,
		   int elsize)

{

  char *name = "thAsciiValRead";

  int nfields = 0;
  readerfunc fieldreader;
  readerfunc_heap fieldreader_heap;

  /* note: this should not be used to read heaps of string type */
  if ((eltype == THASCIIS) && (elheaptype == THASCIIHEAP)) return(0);

  /* now try to read the field value */
  if (elheaptype == THASCIINOHEAP) {

    /* The array in thAsciiInit doesn't seem to be 
       properly initialized 

    fieldreader = thfieldreaders_noheap[(int) eltype];
    if (fieldreader != NULL) {
      fieldreader(fil, fieldval);
      nfields++;
    } else {
      thError("%s: unsupported field type for I/O", 
		     name);
    }
    */

    switch (eltype) {
    case (THASCIIS): 
      read_s(fil, fieldval);
      nfields++;
      break;
    case (THASCIII): 
      read_i(fil, fieldval);
      nfields++;
      break;
    case (THASCIIL): 
      read_l(fil, fieldval);
      nfields++;
      break;
    case (THASCIILL): 
      read_ll(fil, fieldval);
      nfields++;
      break;
    case (THASCIIF): 
      read_f(fil, fieldval);
      nfields++;
      break;
    case (THASCIID): 
      read_d(fil, fieldval);
      nfields++;
      break;
    case (THASCIILD): 
      read_ld(fil, fieldval);
      nfields++;
      break;
    default:
      thError("%s: Unsupported data type for I/O");
      break;
    }
  } else {

    /*
    if (eltype != THASCIIS) {
    fieldreader_heap = thfieldreaders_heap[(int) eltype];
      if (fieldreader != NULL) {
	fieldreader_heap(fil, fieldval, elsize);
	nfields++;
      } else {
	thError("%s: unsupported field type for I/O", 
		       name);
      }
    } else {
    
      NOTE: the following line should not work because of 
	incompatibility of function prototypes 
     
	fieldreader_heap = thfieldreaders_heap[(int) eltype];
      if (fieldreader != NULL) {
	fieldreader(fil, fieldval);
	nfields++;
      } else {
	thError("%s: unsupported field type for I/O", 
		       name);
      }
       
      read_s_heap(fil, (void **) fieldval, elsize);
      nfields++;
    */

    switch (eltype) {
    case (THASCIIS): 
      read_s_heap(fil, (void **) fieldval, elsize);
      nfields++;
      break;
    case (THASCIII): 
      read_i_heap(fil, fieldval, elsize);
      nfields++;
      break;
    case (THASCIIL): 
      read_l_heap(fil, fieldval, elsize);
      nfields++;
      break;
    case (THASCIILL): 
      read_ll_heap(fil, fieldval, elsize);
      nfields++;
      break;
    case (THASCIIF): 
      read_f_heap(fil, fieldval, elsize);
      nfields++;
      break;
    case (THASCIID): 
      read_d_heap(fil, fieldval, elsize);
      nfields++;
      break;
    case (THASCIILD): 
      read_ld_heap(fil, fieldval, elsize);
      nfields++;
      break;
    default:
      thError("%s: Unsupported data type for I/O");
      break;
    }
    
  }
  /* by now, field value is read and inserted in the given position */

  return(nfields);
}

int thAsciiCommentRead(FILE *fil, char *comment) {

  check_ln_delimiter(fil);

  if (feof(fil)) return(-1);

  long floc;
  floc = ftell(fil);

  char c;
  c = fgetc(fil);

  if (c != '#') {
    fseek(fil, floc, SEEK_SET);
    return(-1);
  }

  char *comm;
  char temp;
  if (comment == NULL) {
    comm = &temp;
  } else {
    comm = comment;
  }

  int linejump = 0;
  int endofline = 0;
  
  char cnewline = '\n', cspace = ' ', ctab = '\t', cbackslash = '\\';
  char clast = '\0';

  int i = 0;

  while (!endofline && !feof(fil)) {
    c = fgetc(fil);
    if (c == cbackslash) {
      linejump = 1;
      c = '\0';
    } else if (c == cspace || c == ctab) {
      if (clast == cspace || clast == '\0') {
	c = '\0';
      } else {
	c = cspace;
      }
    } else if (c == cnewline) {
      c = '\0';
      if (!linejump) endofline = 1;
      linejump = 0;
    } else {
      linejump = 0;
    }

    if (clast == cspace && c == cspace) c = '\0';
    if (c != '\0') {
      clast = c;
      *comm = c;
      if (comment != NULL) comm++;
    }
   
  }
  
  /* successful run */
  return(0);

}

void thAsciiFieldSkip(FILE *fil) {

  if (feof(fil)) return;

  long floc;
  floc = ftell(fil);

  char c;
  char *comm;
  char temp;
  
  comm = &temp;

  int linejump = 0;
  int endofline = 0;
  
  char cnewline = '\n', cspace = ' ', ctab = '\t', cbackslash = '\\';
  char clast = '\0';

  int i = 0;

  while (!endofline && !feof(fil)) {
    c = fgetc(fil);
    if (c == cbackslash) {
      linejump = 1;
      c = '\0';
    } else if (c == cspace || c == ctab) {
      if (clast == cspace || clast == '\0') {
	c = '\0';
      } else {
	c = cspace;
      }
    } else if (c == cnewline) {
      c = '\0';
      if (!linejump) endofline = 1;
      linejump = 0;
    } else {
      linejump = 0;
    }

    if (clast == cspace && c == cspace) c = '\0';
    if (c != '\0') {
      clast = c;
      *comm = c;
      
    }
   
  }
  
  /* successful run */
  return;

}

void *thAsciiStructRead(FILE *fil, char *type) {

  char *name = "thAsciiStructRead";

  char *strutypeindic;
  strutypeindic = (char *) thCalloc(MX_STRING_LEN, sizeof(char));

  char *strutype;
  strutype = (char *) thCalloc(MX_STRING_LEN, sizeof(char));

  int fieldsize;

  /* pass the comments and read the name */
  while (thAsciiCommentRead(fil, NULL) == 0) {}
  if (feof(fil)) {
    type[0] = '\0'; 
    return(NULL);
  }

  while (thAsciiNameRead(fil, strutypeindic, NULL) != 0 && !feof(fil)) {
    thError("%s: empty of improperly formatted line, skipping", name);
    thAsciiFieldSkip(fil);
  }
  
  if strmatch(strutypeindic, strutypefield) {
    if (thAsciiCommentRead(fil, NULL) != 0) {
      thAsciiValRead(fil, strutype, THASCIIS, THASCIINOHEAP, 1);
    } /* otherwise strutype = empty string */
    thAsciiCommentRead(fil, NULL);
  } else {
    thError("%s: You are in the middle of another structure", name);
    return(NULL);
  }

  /* get the related schema */
  ASCIISCHEMA *asciischema = NULL;
  asciischema = thTankSchemaGetByType(strutype);

  if (asciischema == NULL) {
    thError("%s: No schema exists for structure %s", 
		   name, strutype);
    return(NULL);
  }

  /* get the structure constructor */
  void *(*strunew)();
  strunew = asciischema->constructor;
  if (strunew == NULL) {
    thError("%s: structure constructor for %s not available in tank", 
		   name, strutype);
    return(NULL);
  }

  /* construct a structure */
  void *stru;
  stru = (*strunew)();

  ASCIISCHEMAELEM *el;
  void *fieldval, *heapval;

  long floc;

  char *fieldindic;
  fieldindic = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  //fieldindic = "1";

  int nfields = 0;
  while (!strmatch(fieldindic, strutypefield) && !feof(fil)) {

  /* save the location of the beginning of the field
     to come back if it is determined later that it
     is the beginning of the structure.
  */


    while (thAsciiCommentRead(fil, NULL) == 0) {}
    
    floc = ftell(fil);

    fieldindic[0] = '\0';
    while(strlen(fieldindic) == 0 && !feof(fil)) {
      skip_empty_space(fil);
      while (thAsciiNameRead(fil, fieldindic, &fieldsize) != 0 && !feof(fil)) {
	thError("%s: empty or improperly formatted line, skipping",
		       name);
	thAsciiFieldSkip(fil);
	skip_empty_space(fil);
	floc = ftell(fil);

      }
    }

    /* check if we are at the beginning of another structure */
    if (!strmatch(fieldindic, strutypefield) && 
	!feof(fil) && strlen(fieldindic) != 0) {

      el = thAsciischemaelemGetByName(asciischema, fieldindic);
      
      if (el == NULL) {
	thError("%s: field (%s) unavailable in schema (%s)", 
		       name, fieldindic, strutype);
	/* skip this specific line */
	skip_line(fil, 1);

      } else {	
	
	if (thAsciiCommentRead(fil, NULL) != 0) {

	  if ((el->type == THASCIIS) && (el->heaptype == THASCIIHEAP)) {
	    heapval = (void **) (*( (void ***) (stru + el->offset)));
	    read_s_heap(fil, heapval, fieldsize);
	    nfields++;
	  } else {
	    /* reading anything that is not an array of strings */
	    if (el->heaptype == THASCIIHEAP || el->type == THASCIIS) {
	      /* if the variable is a 1D heap -- string is a heap */
	      fieldval = (void *)(*( (void **)(stru + el->offset)));
	    } else {
	      /* if the variable is really a scalar */
	      fieldval = (void *) (stru + el->offset);
	    }
	    nfields += thAsciiValRead(fil, fieldval, 
				      el->type, el->heaptype, fieldsize);
	    
	  }
	} /* if there is a comment after the equality sign
	     no new value is read and the structure value
	     is left untouched 
	  */
	thAsciiCommentRead(fil, NULL);
      } 
    /* by now, if this is a good field, the full line is read */
      skip_empty_space(fil);
    }
  }
    /* by now, the structure is read and we are 
       at the beginning of the next structure */

    /* you should go back to the beginning of the field
       if the filedindic shows that you are at the beginning of 
       another structure
    */
  if (strmatch(fieldindic, strutypefield)) {
    fseek(fil, floc, SEEK_SET);
  } 
  
  if (nfields == 0) {
    thError("%s: no fields read for the structure %s, returning defaults", name, strutype);
  }
  
  if (type != NULL) memcpy(type, strutype, strlen(strutype) + 1);
  return(stru);

}
  

CHAIN *thAsciiChainRead(FILE *fil) {

  char *type;
  type = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  
  void *var;

  CHAIN *chain;
  chain = shChainNew("GENERIC");

  while (!feof(fil)) {
    var = thAsciiStructRead(fil, type);
    if (var != NULL) shChainElementAddByPos(chain, var, type, TAIL, AFTER);

    var = NULL;
    type[0] = '\0';

  }

  return(chain);

}
