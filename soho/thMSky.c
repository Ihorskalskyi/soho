#include "thDebug.h"
#include "thMSky.h"

/* global variable */
THOBJCTYPE SKY_OBJC = UNKNOWN_OBJC;
static int init_sky = 0;
static int init_data = 0;
static int init_mrow = UNDO_MROW, init_mcol = UNDO_MCOL;
static int init_nrow = UNDO_NROW, init_ncol = UNDO_NCOL;

static SCHEMA *schema_thregion_type = NULL;
static SCHEMA *schema_fabrication_flag = NULL;
static SCHEMA *schema_production_flag = NULL;
static CHAIN *stored_data = NULL, *stored_frow = NULL, *stored_fcol = NULL;
#if SKY_FULL_REGION
static void *sky_reg_generator = (void *) &SkySpecifyRegion_old;
#else
static void *sky_reg_generator = (void *) &SkySpecifyRegion;
#endif

static void init_static_vars (void);
static RET_CODE sky_get_nbasis(int *n_basis_row, int *n_basis_col);
static RET_CODE SkyDataInit(int mrow, int mcol, int nrow, int ncol);
static MODEL_ELEM *SkyModelElemNew (char *mname, void *f0, char *pname, void *init);
static RET_CODE sky_do_amplifier_delta(SBINDEX *sb, REGION *reg);
static RET_CODE sky_fetch_saved_basis_function(SBINDEX *sb, REGION **reg, THPIX **frow, THPIX **fcol);
static int sky_is_this_an_amplifier_delta(SBINDEX *sb);


static void dbgSky_stored_frowfcol(CHAIN *stored_x, int nx, int mx, char *comment);

void init_static_vars (void) {
	TYPE t;
	t = shTypeGetFromName("FABRICATION_FLAG");
	schema_fabrication_flag = shSchemaGetFromType(t);
	shAssert(schema_fabrication_flag != NULL);
	t = shTypeGetFromName("PRODUCTION_FLAG");
	schema_production_flag = shSchemaGetFromType(t);
	shAssert(schema_production_flag != NULL);
	t = shTypeGetFromName("THREGION_TYPE");
	schema_thregion_type = shSchemaGetFromType(t);
	shAssert(schema_thregion_type != NULL);
	return;
}
	
void MSkyGetType(THOBJCTYPE *type) {
shAssert(type != NULL);
*type = SKY_OBJC;
return;
}

MODEL_ELEM *SkyModelElemNew(char *mname, void *f0, char *pname, void *init) {
#if SKY_FULL_REGION
return(thModelElemNew(mname, f0, pname, 
			WHOLEREGION, THREGION1_TYPE, NO_PSF_CONVOLUTION, 1, 
			init, sky_reg_generator, NULL, NULL, NULL));
#else
return(thModelElemNew(mname, f0, pname, 
			WHOLEREGION, THREGION3_TYPE, NO_PSF_CONVOLUTION, 1, 
			init, sky_reg_generator, NULL, NULL, NULL));
#endif
}


RET_CODE MSkyObjcInit(int nrow, int ncol, CHAIN *ampl, void *f0) {
char *name = "MSkyObjcInit";

if (nrow <= 0 || ncol <= 0) {
	thError("%s: ERROR - could not (re-)initiate sky module for negative or zero values of (nrow, ncol)", name);
	return(SH_GENERIC_ERROR);
	}

RET_CODE status;
if (init_sky) {	
	thError("%s: WARNING - reinitiating sky module for (nrow, ncol) = (%d, %d)", name, nrow, ncol);
	status = SkyDataInit(UNDO_MROW, UNDO_MCOL, UNDO_NROW, UNDO_NCOL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not de-initiate function data", name);	
		return(status);
	}
} 

int nampl;
if (ampl == NULL) {
	nampl = 1;
} else {
	nampl = shChainSize(ampl);
	if (nampl < 1 || nampl > 2) {
		thError("%s: ERROR - the number of amplifiers can be 1 or 2, provided with (%d) instead", name, nampl);
		return(SH_GENERIC_ERROR);
	}
}

status = MSkyPutAmpCharInIndex(NULL, ampl);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not initiate due to problematic AMPL structure", name);
	return(status);
}


int i1, i2;
/* discovered in Nov 2017 that if m1 shows the number of basis functions it should be one bigger than the value below
int m1 = SKY_N1 + BSPLINE_ORDER - 2, m2 = SKY_N2;
*/
int m1 = SKY_N1 + BSPLINE_ORDER - 1, m2 = SKY_N2;

if (m1 > 6 || m2 > 5) {
	thError("%s: ERROR - source code problem - (n1, n2) must be smaller than or eq. to (6, 5), received (%d, %d)", name, m1, m2);	
	return(SH_GENERIC_ERROR);
}
/* initiating data */
status = SkyDataInit(m1, m2, nrow, ncol);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not initiate the sky data", name);	
	return(status);
}

int n_basis_row = -1, n_basis_col = -1;
status = sky_get_nbasis(&n_basis_row, &n_basis_col);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (n_basis) for (row and column) basis functions", name);
	return(status);
	}
shAssert(n_basis_row > 0);
shAssert(n_basis_col > 0);

if (n_basis_row > MAX_MSKY1 || n_basis_col > MAX_MSKY2) {
	thError("%s: ERROR - (n_basis_row, n_basis_col) = (%d, %d) while max allowed is (%d, %d)", name, n_basis_row, n_basis_col, (int) MAX_MSKY1, (int) MAX_MSKY2);
	return(SH_GENERIC_ERROR);
}


/* initiating models */

void ***fs = NULL;
fs = (void ***) thCalloc(MAX_MSKY1, sizeof(void **));
fs[0] = thCalloc(MAX_MSKY1 * MAX_MSKY2, sizeof(void *));
for (i1 = 0; i1 < MAX_MSKY1; i1++) {
	fs[i1] = fs[0] + i1 * MAX_MSKY2;;
	}

#if DEFINE_FLAT_MSKY
fs[0][0] = (void *) &MSkyInitAmp1;
#else
fs[0][0] = (void *) &MSkyInit00;
#endif
fs[1][0] = (void *) &MSkyInit10;
fs[2][0] = (void *) &MSkyInit20;
fs[3][0] = (void *) &MSkyInit30;
fs[4][0] = (void *) &MSkyInit40;
fs[5][0] = (void *) &MSkyInit50;
fs[6][0] = (void *) &MSkyInit60;

fs[0][1] = (void *) &MSkyInit01;
fs[1][1] = (void *) &MSkyInit11;
fs[2][1] = (void *) &MSkyInit21;
fs[3][1] = (void *) &MSkyInit31;
fs[4][1] = (void *) &MSkyInit41;
fs[5][1] = (void *) &MSkyInit51;
fs[6][1] = (void *) &MSkyInit61;


fs[0][2] = (void *) &MSkyInit02;
fs[1][2] = (void *) &MSkyInit12;
fs[2][2] = (void *) &MSkyInit22;
fs[3][2] = (void *) &MSkyInit32;
fs[4][2] = (void *) &MSkyInit42;
fs[5][2] = (void *) &MSkyInit52;
fs[6][2] = (void *) &MSkyInit62;

fs[0][3] = (void *) &MSkyInit03;
fs[1][3] = (void *) &MSkyInit13;
fs[2][3] = (void *) &MSkyInit23;
fs[3][3] = (void *) &MSkyInit33;
fs[4][3] = (void *) &MSkyInit43;
fs[5][3] = (void *) &MSkyInit53;
fs[6][3] = (void *) &MSkyInit63;

fs[0][4] = (void *) &MSkyInit04;
fs[1][4] = (void *) &MSkyInit14;
fs[2][4] = (void *) &MSkyInit24;
fs[3][4] = (void *) &MSkyInit34;
fs[4][4] = (void *) &MSkyInit44;
fs[5][4] = (void *) &MSkyInit54;
fs[6][4] = (void *) &MSkyInit64;

fs[0][5] = (void *) &MSkyInit05;
fs[1][5] = (void *) &MSkyInit15;
fs[2][5] = (void *) &MSkyInit25;
fs[3][5] = (void *) &MSkyInit35;
fs[4][5] = (void *) &MSkyInit45;
fs[5][5] = (void *) &MSkyInit55;
fs[6][5] = (void *) &MSkyInit65;

fs[0][6] = (void *) &MSkyInit06;
fs[1][6] = (void *) &MSkyInit16;
fs[2][6] = (void *) &MSkyInit26;
fs[3][6] = (void *) &MSkyInit36;
fs[4][6] = (void *) &MSkyInit46;
fs[5][6] = (void *) &MSkyInit56;
fs[6][6] = (void *) &MSkyInit66;

#if DEFINE_FLAT_MSKY
m1 = 1; m2 = 1;
n_basis_row = 1; n_basis_col = 1;
printf("%s: WARNING - re-setting (m1, m2) to (%d, %d) for debug purposes \n", name, m1, m2);
#endif

#if DEEP_DEBUG_MSKY
printf("%s: ", name);
#endif

MODEL_ELEM *me;
if (nampl == 2) {
	#if DEEP_DEBUG_MSKY
	printf("'z00' ");
	#endif
	me = SkyModelElemNew("z00", f0, "sbindex", (void *) &MSkyInitAmp2);	
	status = thModelElemAddToMBank(me);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add the second amplifier region description to model bank", name);
		return(status);
	}
}

char *mname = (char *) thCalloc(MX_STRING_LEN, sizeof(char));

for (i1 = 0; i1 < n_basis_row; i1++) {
	for (i2 = 0; i2 < n_basis_col; i2++) {
		sprintf(mname, "s%d%d", i1, i2);
		#if DEEP_DEBUG_MSKY
		printf("'%s' ", mname);
		#endif 
		me = SkyModelElemNew(mname, f0, "sbindex", fs[i1][i2]);
		status = thModelElemAddToMBank(me);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR could not add the sky basis model (%s) to the bank model", name, mname);
			return(status);
		}
	}
}


#if DEEP_DEBUG_MSKY
printf("\n");
#endif

thFree(fs[0]);
thFree(fs);

OBJC_ELEM *oe;
oe = thObjcElemNew(NULL, "SKY", NULL, "SKYPARS", NULL);
if (oe == NULL) {
	thError("%s: ERROR - could not make (objc_elem) for 'SKY'", name);
	return(SH_GENERIC_ERROR);
}

if (nampl == 2) {
	status = thObjcElemAddModel(oe, "z00");
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add model (z00) to the sky object definition", name);
		return(status);
	}
}

for (i1 = 0; i1 < n_basis_row; i1++) {
	for (i2 = 0; i2 < n_basis_col; i2++) {
		sprintf(mname, "s%d%d", i1, i2);
		status = thObjcElemAddModel(oe, mname);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not add model (%s) to the sky object definition", name, mname);
			return(status);
		}
	}
}
status = thObjcTypeGetNextAvailable(&oe->classtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not find an available class type for sky object", name);
	return(status);
}
SKY_OBJC = oe->classtype; /* setting the global variable */
status = thObjcElemAddToObjcBank(oe);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add (SKY) as an object to the bank", name);
	return(status);
}

init_sky++;

thFree(mname);
mname = NULL;
return(SH_SUCCESS);
}

RET_CODE MSkyObjcFini(void) {
char *name = "MSkyObjcFini";
RET_CODE status;
status = thObjcElemDelFromBankByType(SKY_OBJC);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not delete sky object from object bank", name);
	return(status);
}

/* discovered in Nov 2017 that if m1 shows the number of basis functions it should be one bigger than the value below
int m1 = SKY_N1 + BSPLINE_ORDER - 2, m2 = SKY_N2;
*/
int m1 = SKY_N1 + BSPLINE_ORDER - 1, m2 = SKY_N2;

int n_basis_row = -1, n_basis_col = -1;
status = sky_get_nbasis(&n_basis_row, &n_basis_col);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (n_basis_row,  n_basis_col)", name);
	return(status);
}
shAssert(n_basis_row > 0);
shAssert(n_basis_col > 0);

#if DEFINE_FLAT_MSKY
m1 = 1; m2 = 1;
n_basis_row = 1; n_basis_col = 1;
printf("%s: WARNING - re-setting (m1, m2) to (%d, %d) for debug purposes \n", name, m1, m2);
#endif

#if DEEP_DEBUG_MSKY
printf("%s: deleting models from bank: ", name);
#endif

int i1, i2;
char mname[20];
for (i1 = 0; i1 < n_basis_row; i1++) {
	for (i2 = 0; i2 < n_basis_col; i2++) {
		sprintf(mname, "s%d%d", i1, i2);
		#if DEEP_DEBUG_MSKY
		printf("'%s' ", mname);
		#endif 
		status = thModelElemDelFromBank(mname);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR could not delete sky basis model (%s) from model bank", name, mname);
			return(status);
		}
	}
}

strcpy(mname, "z00");
#if DEEP_DEBUG_MSKY
	printf("'%s' \n", mname);
#endif 
status = thModelElemDelFromBank(mname);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not delete sky basis model (%s) from model bank", name, mname);
	return(status);
}

status = MSkyPutAmpCharInIndex(NULL, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not de-init amplifier info", name);
	return(status);
	}

#if 0
if (init_data) {
	status = SkyDataInit(UNDO_MROW, UNDO_MCOL, UNDO_NROW, UNDO_NCOL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not de-init sky related data", name);
		return(status);
	}
}
#endif

return(SH_SUCCESS);
}




RET_CODE MSkyPutAmpCharInIndex(SBINDEX *sbindex, CHAIN *ampl) {
static CHAIN *ampl_saved = NULL;
static int init = 0;
char *name = "MSkyPutAmpCharInIndex";

if (ampl == NULL && sbindex == NULL) {
	init = 0;
	ampl_saved = NULL;
	return(SH_SUCCESS);
}

if (ampl != NULL) {
	ampl_saved = ampl;
	init++;
}

if (init > 1) {
	thError("%s: WARNING - multiple initiation attempts", name, init);
	}

if (sbindex != NULL && (!init || ampl_saved == NULL)) {
	thError("%s: ERROR - no amplifier information to work with", name);
	return(SH_GENERIC_ERROR);
}

if (sbindex != NULL) {
	sbindex->ampl = ampl_saved;
}

return(SH_SUCCESS);
}

RET_CODE MSkyInitAmp1(SBINDEX *sbindex) {
char *name = "MSkyInitAmp1";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
	return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 0;
sbindex->mcol = SKY_N2; sbindex->icol = 0;
return(SH_SUCCESS);
}

RET_CODE MSkyInitAmp2( SBINDEX *sbindex) {
char *name = "MSkyInitAmp2";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

if (shChainSize(sbindex->ampl) == 1) {
	thError("%s: ERROR - this chip has only one amplifier", name);
	return(SH_GENERIC_ERROR);
}

sbindex->mrow = SKY_N1; sbindex->irow = -1;
sbindex->mcol = SKY_N2; sbindex->icol = 0;
return(SH_SUCCESS);
}

RET_CODE MSkyInit00( SBINDEX *sbindex) {
char *name = "MSkyInit00";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 0;
sbindex->mcol = SKY_N2; sbindex->icol = 0;
return(SH_SUCCESS);
}



RET_CODE MSkyInit10( SBINDEX *sbindex) {
char *name = "MSkyInit10";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 1;
sbindex->mcol = SKY_N2; sbindex->icol = 0;
return(SH_SUCCESS);
}

RET_CODE MSkyInit20(SBINDEX *sbindex) {
char *name = "MSkyInit20";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 2;
sbindex->mcol = SKY_N2; sbindex->icol = 0;
return(SH_SUCCESS);
}

RET_CODE MSkyInit30( SBINDEX *sbindex) {
char *name = "MSkyInit30";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 3;
sbindex->mcol = SKY_N2; sbindex->icol = 0;
return(SH_SUCCESS);
}

RET_CODE MSkyInit40( SBINDEX *sbindex) {
char *name = "MSkyInit40";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 4;
sbindex->mcol = SKY_N2; sbindex->icol = 0;
return(SH_SUCCESS);
}

RET_CODE MSkyInit50( SBINDEX *sbindex) {
char *name = "MSkyInit50";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 5;
sbindex->mcol = SKY_N2; sbindex->icol = 0;
return(SH_SUCCESS);
}

RET_CODE MSkyInit60( SBINDEX *sbindex) {
char *name = "MSkyInit60";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 6;
sbindex->mcol = SKY_N2; sbindex->icol = 0;
return(SH_SUCCESS);
}

RET_CODE MSkyInit01( SBINDEX *sbindex) {
char *name = "MSkyInit01";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 0;
sbindex->mcol = SKY_N2; sbindex->icol = 1;
return(SH_SUCCESS);
}



RET_CODE MSkyInit11( SBINDEX *sbindex) {
char *name = "MSkyInit11";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 1;
sbindex->mcol = SKY_N2; sbindex->icol = 1;
return(SH_SUCCESS);
}

RET_CODE MSkyInit21( SBINDEX *sbindex) {
char *name = "MSkyInit21";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 2;
sbindex->mcol = SKY_N2; sbindex->icol = 1;
return(SH_SUCCESS);
}

RET_CODE MSkyInit31( SBINDEX *sbindex) {
char *name = "MSkyInit31";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 3;
sbindex->mcol = SKY_N2; sbindex->icol = 1;
return(SH_SUCCESS);
}

RET_CODE MSkyInit41( SBINDEX *sbindex) {
char *name = "MSkyInit41";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 4;
sbindex->mcol = SKY_N2; sbindex->icol = 1;
return(SH_SUCCESS);
}

RET_CODE MSkyInit51( SBINDEX *sbindex) {
char *name = "MSkyInit51";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 5;
sbindex->mcol = SKY_N2; sbindex->icol = 1;
return(SH_SUCCESS);
}

RET_CODE MSkyInit61( SBINDEX *sbindex) {
char *name = "MSkyInit61";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 6;
sbindex->mcol = SKY_N2; sbindex->icol = 1;
return(SH_SUCCESS);
}

RET_CODE MSkyInit02( SBINDEX *sbindex) {
char *name = "MSkyInit02";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 0;
sbindex->mcol = SKY_N2; sbindex->icol = 2;
return(SH_SUCCESS);
}

RET_CODE MSkyInit12( SBINDEX *sbindex) {
char *name = "MSkyInit12";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 1;
sbindex->mcol = SKY_N2; sbindex->icol = 2;
return(SH_SUCCESS);
}

RET_CODE MSkyInit22( SBINDEX *sbindex) {
char *name = "MSkyInit22";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 2;
sbindex->mcol = SKY_N2; sbindex->icol = 2;
return(SH_SUCCESS);
}

RET_CODE MSkyInit32( SBINDEX *sbindex) {
char *name = "MSkyInit32";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 3;
sbindex->mcol = SKY_N2; sbindex->icol = 2;
return(SH_SUCCESS);
}

RET_CODE MSkyInit42( SBINDEX *sbindex) {
char *name = "MSkyInit42";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 4;
sbindex->mcol = SKY_N2; sbindex->icol = 2;
return(SH_SUCCESS);
}

RET_CODE MSkyInit52( SBINDEX *sbindex) {
char *name = "MSkyInit52";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 5;
sbindex->mcol = SKY_N2; sbindex->icol = 2;
return(SH_SUCCESS);
}

RET_CODE MSkyInit62( SBINDEX *sbindex) {
char *name = "MSkyInit62";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 6;
sbindex->mcol = SKY_N2; sbindex->icol = 2;
return(SH_SUCCESS);
}

RET_CODE MSkyInit03( SBINDEX *sbindex) {
char *name = "MSkyInit03";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 0;
sbindex->mcol = SKY_N2; sbindex->icol = 3;
return(SH_SUCCESS);
}

RET_CODE MSkyInit13( SBINDEX *sbindex) {
char *name = "MSkyInit13";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 1;
sbindex->mcol = SKY_N2; sbindex->icol = 3;
return(SH_SUCCESS);
}

RET_CODE MSkyInit23( SBINDEX *sbindex) {
char *name = "MSkyInit23";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 2;
sbindex->mcol = SKY_N2; sbindex->icol = 3;
return(SH_SUCCESS);
}

RET_CODE MSkyInit33( SBINDEX *sbindex) {
char *name = "MSkyInit33";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 3;
sbindex->mcol = SKY_N2; sbindex->icol = 3;
return(SH_SUCCESS);
}

RET_CODE MSkyInit43( SBINDEX *sbindex) {
char *name = "MSkyInit43";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 4;
sbindex->mcol = SKY_N2; sbindex->icol = 3;
return(SH_SUCCESS);
}

RET_CODE MSkyInit53( SBINDEX *sbindex) {
char *name = "MSkyInit53";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 5;
sbindex->mcol = SKY_N2; sbindex->icol = 3;
return(SH_SUCCESS);
}

RET_CODE MSkyInit63( SBINDEX *sbindex) {
char *name = "MSkyInit63";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 6;
sbindex->mcol = SKY_N2; sbindex->icol = 3;
return(SH_SUCCESS);
}



RET_CODE MSkyInit04( SBINDEX *sbindex) {
char *name = "MSkyInit04";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 0;
sbindex->mcol = SKY_N2; sbindex->icol = 4;
return(SH_SUCCESS);
}

RET_CODE MSkyInit14( SBINDEX *sbindex) {
char *name = "MSkyInit14";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 1;
sbindex->mcol = SKY_N2; sbindex->icol = 4;
return(SH_SUCCESS);
}

RET_CODE MSkyInit24( SBINDEX *sbindex) {
char *name = "MSkyInit24";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 2;
sbindex->mcol = SKY_N2; sbindex->icol = 4;
return(SH_SUCCESS);
}

RET_CODE MSkyInit34( SBINDEX *sbindex) {
char *name = "MSkyInit34";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 3;
sbindex->mcol = SKY_N2; sbindex->icol = 4;
return(SH_SUCCESS);
}

RET_CODE MSkyInit44( SBINDEX *sbindex) {
char *name = "MSkyInit44";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 4;
sbindex->mcol = SKY_N2; sbindex->icol = 4;
return(SH_SUCCESS);
}

RET_CODE MSkyInit54( SBINDEX *sbindex) {
char *name = "MSkyInit54";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 5;
sbindex->mcol = SKY_N2; sbindex->icol = 4;
return(SH_SUCCESS);
}

RET_CODE MSkyInit64( SBINDEX *sbindex) {
char *name = "MSkyInit64";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 6;
sbindex->mcol = SKY_N2; sbindex->icol = 4;
return(SH_SUCCESS);
}

RET_CODE MSkyInit05( SBINDEX *sbindex) {
char *name = "MSkyInit05";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 0;
sbindex->mcol = SKY_N2; sbindex->icol = 5;
return(SH_SUCCESS);
}

RET_CODE MSkyInit15( SBINDEX *sbindex) {
char *name = "MSkyInit15";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 1;
sbindex->mcol = SKY_N2; sbindex->icol = 5;
return(SH_SUCCESS);
}

RET_CODE MSkyInit25( SBINDEX *sbindex) {
char *name = "MSkyInit25";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 2;
sbindex->mcol = SKY_N2; sbindex->icol = 5;
return(SH_SUCCESS);
}

RET_CODE MSkyInit35( SBINDEX *sbindex) {
char *name = "MSkyInit35";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 3;
sbindex->mcol = SKY_N2; sbindex->icol = 5;
return(SH_SUCCESS);
}

RET_CODE MSkyInit45( SBINDEX *sbindex) {
char *name = "MSkyInit45";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 4;
sbindex->mcol = SKY_N2; sbindex->icol = 5;
return(SH_SUCCESS);
}

RET_CODE MSkyInit55( SBINDEX *sbindex) {
char *name = "MSkyInit55";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 5;
sbindex->mcol = SKY_N2; sbindex->icol = 5;
return(SH_SUCCESS);
}

RET_CODE MSkyInit65( SBINDEX *sbindex) {
char *name = "MSkyInit65";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 6;
sbindex->mcol = SKY_N2; sbindex->icol = 5;
return(SH_SUCCESS);
}

RET_CODE MSkyInit06( SBINDEX *sbindex) {
char *name = "MSkyInit06";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 0;
sbindex->mcol = SKY_N2; sbindex->icol = 6;
return(SH_SUCCESS);
}

RET_CODE MSkyInit16( SBINDEX *sbindex) {
char *name = "MSkyInit16";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 1;
sbindex->mcol = SKY_N2; sbindex->icol = 6;
return(SH_SUCCESS);
}

RET_CODE MSkyInit26( SBINDEX *sbindex) {
char *name = "MSkyInit26";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 2;
sbindex->mcol = SKY_N2; sbindex->icol = 6;
return(SH_SUCCESS);
}

RET_CODE MSkyInit36( SBINDEX *sbindex) {
char *name = "MSkyInit36";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 3;
sbindex->mcol = SKY_N2; sbindex->icol = 6;
return(SH_SUCCESS);
}

RET_CODE MSkyInit46( SBINDEX *sbindex) {
char *name = "MSkyInit46";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 4;
sbindex->mcol = SKY_N2; sbindex->icol = 6;
return(SH_SUCCESS);
}

RET_CODE MSkyInit56( SBINDEX *sbindex) {
char *name = "MSkyInit56";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 5;
sbindex->mcol = SKY_N2; sbindex->icol = 6;
return(SH_SUCCESS);
}

RET_CODE MSkyInit66( SBINDEX *sbindex) {
char *name = "MSkyInit66";

RET_CODE status;
status = MSkyPutAmpCharInIndex(sbindex, NULL);
if (status != SH_SUCCESS) {
        thError("%s: ERROR - could not place amplifier information in Sky Basis Index", name);
        return(status);
}

sbindex->mrow = SKY_N1; sbindex->irow = 6;
sbindex->mcol = SKY_N2; sbindex->icol = 6;
return(SH_SUCCESS);
}


/* calculating the sky basis */

RET_CODE sky_get_nbasis(int *n_basis_row, int *n_basis_col) {
char *name = "sky_get_nbasis";
#if SKY_FULL_REGION
thError("%s: ERROR - this function is not supported in (sky_full_region) mode", name);
return(SH_GENERIC_ERROR);
#else
if (n_basis_row != NULL) {
	*n_basis_row = 0;
	if (stored_frow != NULL) *n_basis_row = shChainSize(stored_frow);
}
if (n_basis_col != NULL) {
	*n_basis_col = 0;
	if (stored_fcol != NULL) *n_basis_col = shChainSize(stored_fcol);
}
return(SH_SUCCESS);
#endif
}

RET_CODE SkyDataInit(int mrow, int mcol, int nrow, int ncol) {
char *name = "SkyDataInit";

if (nrow == UNDO_NROW && ncol == UNDO_NCOL && 
	mrow == UNDO_MROW && mcol == UNDO_MCOL && init_data) {
	thError("%s: WARNING - reinitiating sky basis function data", name);
	if (stored_data != NULL) {
		printf("%s: destroying (stored_data) \n", name);
		shChainDestroy(stored_data, &thFuncDel);
	}
	if (stored_frow != NULL) {
		printf("%s: destroying (stored_frow) \n", name);
		shChainDestroy(stored_frow, &thVectDel);
	}
	if (stored_fcol != NULL) {
		printf("%s: destroying (stored_fcol) \n", name);
		shChainDestroy(stored_fcol, &thVectDel);
	}
	stored_data = NULL;
	stored_frow = NULL;
	stored_fcol = NULL;
	init_mrow = UNDO_MROW;
	init_mcol = UNDO_MCOL;
	init_nrow = UNDO_NROW;
	init_ncol = UNDO_NCOL;
	init_data = 0;
	return(SH_SUCCESS);
}
if (nrow <= 0 || ncol <= 0) {
	thError("%s: ERROR - unacceptable region size (nrow, ncol) = (%d, %d)",
		name, nrow, ncol);
	return(SH_GENERIC_ERROR);
}

if (!init_data) {

	if (stored_data != NULL) shChainDestroy(stored_data, &thFuncDel);
	if (stored_frow != NULL) shChainDestroy(stored_frow, &thVectDel);
	if (stored_fcol != NULL) shChainDestroy(stored_fcol, &thVectDel);	
	#if SKY_FULL_REGION
	stored_data = gen_Bspline_tcheb_chain_func(nrow, ncol, mrow, mcol, -1.0, 1.0, -1.0, 1.0);
	#else
	stored_frow = gen_Bspline_vec(nrow, mrow, -1.0, 1.0);
	stored_fcol = gen_tcheb_vec(ncol, mcol, -1.0, 1.0);

	#if DEBUG_MSKY
	printf("%s: outputting stored_frow (Bspline) \n", name);
	dbgSky_stored_frowfcol(stored_frow, nrow, mrow, name);
	printf("%s: outputting stored_fcol (Tcheb) \n", name);
	dbgSky_stored_frowfcol(stored_fcol, ncol, mcol, name);
	#endif

	#endif

	init_mrow = mrow;
	init_mcol = mcol;
	init_nrow = nrow;
	init_ncol = ncol;

	init_data = 1;

	return(SH_SUCCESS);
} else if (init_nrow != nrow || init_ncol != ncol || 
		init_mrow != mrow || init_mcol != mcol) {

	thError("%s: ERROR - function already initiated with (mrow, mcol) = (%d, %d), (nrow, ncol) = (%d, %d)",  name, init_mrow, init_mcol, init_nrow, init_ncol); 
	return(SH_GENERIC_ERROR);
} else {
	thError("%s: WARNING - no need to initiate sky functions multiple times", name);
	return(SH_SUCCESS);
}

}

RET_CODE f0sky(void *p, REGION *reg) {
char *name = "f0sky";

if (p == NULL && reg != NULL) {
	thError("%s: ERROR - null input, cannot return any output", name);
	return(SH_GENERIC_ERROR);
}

SBINDEX *sb;
sb = (SBINDEX *) p;

/* re-initiating */
if (p == NULL && reg == NULL) {
	if (init_data) thError("%s: WARNING - re-initiation requested", name);
	if (stored_data != NULL) shChainDestroy(stored_data, &thFuncDel);
	stored_data = NULL;
	init_mrow = UNDO_MROW; init_mcol = UNDO_MCOL;
	init_nrow = UNDO_NROW; init_ncol = UNDO_NCOL;
	init_data = 0;
	return(SH_SUCCESS);
}

int mrow, mcol, nrow, ncol;
mrow = sb->mrow;
mcol = sb->mcol;
nrow = reg->nrow;
ncol = reg->ncol;

if (mrow != init_mrow || mcol != init_mcol) { 

	if (!init_data) {
	if (stored_data != NULL) shChainDestroy(stored_data, &thFuncDel);
	if (stored_frow != NULL) shChainDestroy(stored_frow, &thVectDel);
	if (stored_fcol != NULL) shChainDestroy(stored_fcol, &thVectDel);
	#if SKY_FULL_REGION
	stored_data = gen_Bspline_tcheb_chain_func(init_nrow, init_ncol, mrow, mcol, -1.0, 1.0, -1.0, 1.0);
	#else
	stored_frow = gen_Bspline_vec(init_nrow, mrow, -1.0, 1.0);
	stored_fcol = gen_tcheb_vec(init_ncol, mcol, -1.0, 1.0); 
	#endif
	init_mrow = mrow;
	init_mcol = mcol;
	init_data = 1;

	} else {
		thError("%s: ERROR - function initiated with (mrow, mcol) = (%d, %d) while asking for (%d, %d)", 
		name, init_mrow, init_mcol, mrow, mcol);
		return(SH_GENERIC_ERROR);
	}
}

if (nrow != init_nrow || ncol != init_ncol) {
	int i;
	thFree(reg->rows_thpix[0]);
	thFree(reg->rows_thpix);
	reg->rows_thpix = thCalloc(init_nrow, sizeof(THPIX *));
	reg->rows_thpix[0] = thCalloc(init_nrow * init_ncol, sizeof(THPIX));
	for (i = 1; i < init_nrow; i++) {
		reg->rows_thpix[i] = reg->rows_thpix[0] + i * init_ncol;
	}
	reg->nrow = init_nrow;
	reg->ncol = init_ncol;
	
}

reg->row0 = 0;
reg->col0 = 0;
nrow = init_nrow;
ncol = init_ncol;

int irow, icol;
irow = sb->irow;
icol = sb->icol;

static char *regname = NULL;
if (regname == NULL) regname = thCalloc(MX_STRING_LEN, sizeof(char));
sprintf(regname, "sky basis function for (irow, icol, mrow, mcol) = (%d, %d, %d, %d)", irow, icol, mrow, mcol);
strcpy(regname, reg->name);

static int indices[2];
indices[0] = irow;
indices[1] = icol;

CHAIN *campl;
campl = sb->ampl;
int n = 0;
if (campl != NULL) n = shChainSize(campl);


if (irow <= 0 && irow > -n && icol == 0) {
	AMPL *ampl;
	ampl = shChainElementGetByPos(campl, (-irow));

	int row0, col0, row1, col1;
	row0 = ampl->row0;
	row1 = ampl->row1;
	col0 = ampl->col0;	
	col1 = ampl->col1;
	
	int i, j;
	THPIX *row;
	for (i = 0; i < row0; i++) {
		row = reg->rows_thpix[i];
		for (j = 0; j < ncol; j++) {
			row[j] = 0.0;
		}
	}
	for (i = row1; i < nrow; i++) {
		row = reg->rows_thpix[i];
		for (j = 0; j < ncol; j++) {
			row[j] = 0.0;
		}
	}
	
	for (i = row0; i < row1; i++) {
		row = reg->rows_thpix[i];
		for (j = 0; j < col0; j++) {
			row[j] = 0.0;
		}
		for (j = col0; j < col1; j++) {
			row[j] = 1.0;
		}
		for (j = col1; j < ncol; j++) {
			row[j] = 0.0;
		}
	}

	return(SH_SUCCESS);
}

/* relaxing this condition to enable situations where the number of nodes / knots
   and the number of basis fuctions is not the same, e.g. bspline

if (irow < 0 || irow > mrow) {
		thError("%s: ERROR - cannot search for index (irow = %d) when (mrow = %d)", name, irow, mrow);
		return(SH_GENERIC_ERROR);
	}
if (icol < 0 || icol > mcol) {
		thError("%s: ERROR - cannot search for index (icol = %d) when (mcol = %d)", name, icol, mcol);
		return(SH_GENERIC_ERROR);
}
*/
#if SKY_FULL_REGION
int pos;
FUNC *sfunc;
pos = thGetFuncPosByPar(stored_data, indices, NULL);
if (pos <0) {
	thError("%s: ERROR - database search for (irow, icol) = (%d, %d) while (mrow, mcol) = (%d, %d) failed", name, irow, icol, mrow, mcol);
	return(SH_GENERIC_ERROR);
	}
sfunc = shChainElementGetByPos(stored_data, pos); 
if (sfunc == NULL) {
	thError("%s: ERROR - problematic data based item for (irow, icol) = (%d, %d) while (mrow, mcol) = (%d, %d)", name, irow, icol, mrow, mcol);
	return(SH_GENERIC_ERROR);
}
shRegPixCopy(sfunc->reg, reg);
return(SH_SUCCESS);
#else
thError("%s: ERROR - this function is only designed for full region simulation of sky", name);
return(SH_GENERIC_ERROR);
#endif

}

RET_CODE sky_get_amplifier_region_info(SBINDEX *sb, int *row0, int *row1, int *nrow, int *col0, int *col1, int *ncol) {
char *name = "sky_get_amplifier_region_info";
if (sb == NULL && row0 == NULL && row1 == NULL && nrow == NULL && col0 == NULL && col1 == NULL && ncol == NULL) {
	thError("%s: ERROR - null input and output", name);
	return(SH_SUCCESS);
}
if (sb == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (row0 == NULL && row1 == NULL && nrow == NULL && col0 == NULL && col1 == NULL && ncol == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}

int irow, icol;
irow = sb->irow;
icol = sb->icol;

CHAIN *campl;
campl = sb->ampl;
int n = 0;
if (campl != NULL) n = shChainSize(campl);

if (sky_is_this_an_amplifier_delta(sb)) {
	AMPL *ampl;
	ampl = shChainElementGetByPos(campl, (-irow));
	if (ampl == NULL) {
		thError("%s: ERROR - null (ampl) for (irow = %d)", name, irow);
		return(SH_GENERIC_ERROR);
	}	

	if (row0 != NULL) *row0 = ampl->row0;
	if (row1 != NULL) *row1 = ampl->row1;
	if (nrow != NULL) *nrow = ampl->row1 - ampl->row0 + 1;
	if (col0 != NULL) *col0 = ampl->col0;	
	if (col1 != NULL) *col1 = ampl->col1;
	if (ncol != NULL) *ncol = ampl->col1 - ampl->col0 + 1;
	return(SH_SUCCESS);

} 
	
thError("%s: ERROR - unacceptable (irow, icol) pair (%d, %d) - not an amplifier region", name, irow, icol);
return(SH_GENERIC_ERROR);
}

RET_CODE sky_do_amplifier_delta(SBINDEX *sb, REGION *reg) {	
char *name = "sky_do_amplifier_delta";

if (sb == NULL && reg == NULL) {
	thError("%s: WARNING - null input and output", name);	
	return(SH_SUCCESS);
}
if (sb == NULL && reg != NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (sb != NULL && reg == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}

int irow, icol;
irow = sb->irow;
icol = sb->icol;

CHAIN *campl;
campl = sb->ampl;
int n = 0;
if (campl != NULL) n = shChainSize(campl);

if (sky_is_this_an_amplifier_delta(sb)) {
	AMPL *ampl;
	ampl = shChainElementGetByPos(campl, (-irow));
	if (ampl == NULL) {
		thError("%s: ERROR - null (ampl) for (irow = %d)", name, irow);
		return(SH_GENERIC_ERROR);
	}	

	int nrow, ncol;
	nrow = reg->nrow;
	ncol = reg->ncol;
	int row0, col0, row1, col1;
	row0 = ampl->row0;
	row1 = ampl->row1;
	col0 = ampl->col0;	
	col1 = ampl->col1;
	
	int i, j;
	THPIX *row;
	for (i = 0; i < row0; i++) {
		row = reg->rows_thpix[i];
		for (j = 0; j < ncol; j++) {
			row[j] = 0.0;
		}
	}
	for (i = row1; i < nrow; i++) {
		row = reg->rows_thpix[i];
		for (j = 0; j < ncol; j++) {
			row[j] = 0.0;
		}
	}
	
	for (i = row0; i < row1; i++) {
		row = reg->rows_thpix[i];
		for (j = 0; j < col0; j++) {
			row[j] = 0.0;
		}
		for (j = col0; j < col1; j++) {
			row[j] = 1.0;
		}
		for (j = col1; j < ncol; j++) {
			row[j] = 0.0;
		}
	}

	return(SH_SUCCESS);

} 
	
thError("%s: ERROR - unacceptable (irow, icol) pair (%d, %d)", 
	name, irow, icol);
return(SH_GENERIC_ERROR);
}

int sky_is_this_an_amplifier_delta(SBINDEX *sb) {
char *name = "sky_is_this_an_amplifier_delta";
if (sb == NULL) {
	thError("%s: ERROR - null input", name);
	return(0);
}

int irow, icol;
irow = sb->irow;
icol = sb->icol;

CHAIN *campl;
campl = sb->ampl;
int n = 0;
if (campl != NULL) n = shChainSize(campl);

#if DEFINE_FLAT_MSKY
return((irow <= 0 && irow > -n && icol == 0));
#else
return((irow < 0 && irow > -n && icol == 0));
#endif
}


RET_CODE sky_fetch_saved_basis_function(SBINDEX *sb, REGION **reg, THPIX **frow, THPIX **fcol) {
char *name = "sky_fetch_saved_basis_function";
if (sb == NULL && reg == NULL && frow == NULL && fcol == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
}
if (sb != NULL && reg == NULL && frow == NULL && fcol == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (sb == NULL && (reg != NULL || frow != NULL || fcol != NULL))  {
	*reg = NULL;
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

/* re-initiating */
int pos, mrow, mcol;
mrow = sb->mrow;
mcol = sb->mcol;

int error = 0;

#if SKY_FULL_REGION

int indices[2];
indices[0] = sb->irow;
indices[1] = sb->icol;

FUNC *sfunc;
pos = thGetFuncPosByPar(stored_data, indices, NULL);
if (pos <0) {
	thError("%s: ERROR - database search for (irow, icol) = (%d, %d) while (mrow, mcol) = (%d, %d) failed", name, sb->irow, sb->icol, sb->mrow, sb->mcol);
	return(SH_GENERIC_ERROR);
	}
sfunc = shChainElementGetByPos(stored_data, pos); 
if (sfunc == NULL) {
	thError("%s: ERROR - problematic data based item for (irow, icol) = (%d, %d) while (mrow, mcol) = (%d, %d)", name, sb->irow, sb->icol, sb->mrow, sb->mcol);
	return(SH_GENERIC_ERROR);
}

if (sfunc->reg == NULL) {
	thError("%s: ERROR - null region found in stored data", name);
	*reg = NULL;
	return(SH_GENERIC_ERROR);
}

if (reg != NULL) *reg = sfunc->reg;
if (frow != NULL || fcol != NULL) {
	thError("%s: WARNING - full region representation of sky basis functions cannot return f(x) or g(y)", name);
	if (frow != NULL) *frow = NULL;
	if (fcol != NULL) *fcol = NULL;
}
#else 

int irow, icol;
irow = sb->irow;
icol = sb->icol;
VECT *rvect, *cvect;

#if DEBUG_MSKY
printf("%s: DEBUG - fetching row and column for (irow = %d, icol = %d) \n", name, irow, icol);
#endif

pos = thGetVectPosByIndex(stored_frow, irow);
if (pos != BAD_POS) {
	rvect = shChainElementGetByPos(stored_frow, pos);
} else {
	thError("%s: ERROR - bad position found for (irow = %d)", name, irow);
	error++;
}
#if DEBUG_MSKY
printf("%s: DEBUG - found (irow = %d) at (pos = %d) \n", name, irow, pos);
#endif

pos = thGetVectPosByIndex(stored_fcol, icol);
if (pos != BAD_POS) {
	cvect = shChainElementGetByPos(stored_fcol, pos);
} else {
	thError("%s: ERROR - bad position found for (icol = %d)", name, icol);
	error++;
}

#if DEBUG_MSKY
printf("%s: DEBUG - found (icol = %d) at (pos = %d) \n", name, icol, pos);
#endif

if (reg != NULL) {
	thError("%s: WARNING - f(x)g(y) representation of sky basis functions cannot return (region)", name);
	*reg = NULL;
}

if (frow != NULL) {
	if (rvect != NULL) {
		*frow = rvect->vec;
	} else {
		*frow = NULL;
	}
}
if (fcol != NULL) {
	if (cvect != NULL) {
		*fcol = cvect->vec;
	} else {
		*fcol = NULL;
	}
}
#endif

if (error != 0) return(SH_GENERIC_ERROR);
return(SH_SUCCESS);
}

/* THREGION adaptation of f0sky */
RET_CODE g0sky(void *p, THREGION *threg, THPIX *mcount, int index) {
char *name = "g0sky";

if (p == NULL && threg != NULL) {
	thError("%s: ERROR - null input, cannot return any output", name);
	return(SH_GENERIC_ERROR);
}
if (p == NULL && threg == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
}

if (p != NULL && threg == NULL) {
	thError("%s: ERROR - null output", name);
	return(SH_GENERIC_ERROR);
}
if (index != 0) {
	thError("%s: ERROR - received (index = %d) while expected (index = 0)", 
		name, index);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;

SBINDEX *sb;
sb = (SBINDEX *) p;
int irow, icol;
irow = sb->irow;
icol = sb->icol;

CHAIN *campl;
campl = sb->ampl;
int n = 0;
if (campl != NULL) n = shChainSize(campl);

REGION *lreg;

/* amplifier delta function */
if (sky_is_this_an_amplifier_delta(p)) {
	status = thRegFab(threg, NULL, NULL, NULL, 0);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not FAB (thregion)", name);
		return(status);
	}
	#if SKY_FULL_REGION
	status = thRegGetRegMaskA(threg, &lreg, NULL, NULL, NULL, 0);	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not fetch (region) from (thregion)", name);
		return(status);
	}
	status = sky_do_amplifier_delta(p, lreg);	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not create amplifier delta function", name);
		return(status);
	}
	#endif
	status = thRegFabFinalize(threg);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not finalize fabrication of (threg)", name);
		return(status);
	}
	return(SH_SUCCESS);
}

THPIX *frow = NULL, *fcol = NULL;
#if SKY_FULL_REGION
status = sky_fetch_saved_basis_function(p, &lreg, NULL, NULL);	
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not fetch saved functions for sky basis", name);
	return(status);
}
status = thRegFab(threg, lreg, NULL, NULL, 0);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not 'FAB' (thregion)", name);
	return(SH_SUCCESS);
}
#else 
status = sky_fetch_saved_basis_function(p, NULL, &frow, &fcol);	
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not fetch saved functions for sky basis", name);
	return(status);
}
#if DEBUG_MSKY
printf("%s: DEBUG - fetched row = %p, fetched column = %p \n", name, frow, fcol);
#endif

status = thRegFab(threg, NULL, frow, fcol, 0);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not 'FAB' (thregion)", name);
	return(SH_SUCCESS);
}
#endif

status = thRegFabFinalize(threg);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not finalize fabrication of (thregion)", name);
	return(status);
}

return(SH_SUCCESS);
}

/* object creation */

THOBJC *thSkyObjcNew() {
char *name = "thSkyObjcNew";

THOBJC *objc;
objc = thObjcNew();

objc->thobjctype = SKY_OBJC;

OBJC_ELEM *oe;
MODEL_ELEM *me;
THPROP *prop;
void *v;
RET_CODE status;
status = thObjcElemGetByType(SKY_OBJC, &oe);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - 'SKY' doesn't seem to be defined in the object bank", name);
	thObjcDel(objc);
	return(NULL);
}
CHAIN *ms;
int i, nm;
ms = oe->ms;
nm = shChainSize(ms);
void (*init)(SBINDEX *);
char *mname;

if (objc->thprop == NULL) objc->thprop = shChainNew("THPROP");
for (i = 0; i < nm; i++) {
	me = shChainElementGetByPos(ms, i);
	mname = me->mname;
	v = thSbindexNew();
	init = me->init;
	init(v);
	prop = thPropNew(mname, "SBINDEX", v);
	shChainElementAddByPos(objc->thprop, prop, "THPROP", TAIL, AFTER);
}

objc->phtype = UNKNOWN_SCHEMA;
objc->ioprop = thSkyparsNew();
strcpy(objc->ioname, "SKYPARS");
objc->iotype = shTypeGetFromName(objc->ioname);

return(objc);
}

RET_CODE SkySpecifyRegion(void *q, PSF_CONVOLUTION_INFO *psf_info, THREGION *threg, int index, int reprefab) {
char *name = "SkySpecifyRegion";

#if SKY_FULL_REGION
thError("%s: ERROR - this function is meant for short sky place holding", name);
return(SH_GENERIC_ERROR);
#endif

if (q == NULL || threg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (index != 0 && index != BAD_PSF_INDEX) {
	thError("%s: ERROR - unsupported index (%d)", name, index);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
THREGION_TYPE regtype = threg->type;
if (regtype == UNKNOWN_THREGION_TYPE) {
	if (threg->threg != NULL) {
		thError("%s: ERROR - improperly allocated (thregion), type UNKNOWN while a non-null value found",
			name);
		return(SH_GENERIC_ERROR);
	}
	status = thRegRenew(threg, "", TYPE_THPIX, THREGION3_TYPE);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not renew (thregion)", name);
		return(status);
	}
} else if (regtype != THREGION3_TYPE) {
	if (schema_thregion_type == NULL) init_static_vars ();
	thError("%s: ERROR - unsupported (thregion) type '%s'", 
		name,(schema_thregion_type->elems[regtype]).name);
	return(SH_GENERIC_ERROR);
}

THREGION3 *threg3;
threg3 = threg->threg;

if (threg3 == NULL) {
	threg3 = thRegion3New("sky basis function", TYPE_THPIX);
	threg->threg = threg3;
	return(SH_SUCCESS);
	}

FABRICATION_FLAG fab = threg3->fab;
PRODUCTION_FLAG production;

if (sky_is_this_an_amplifier_delta(q)) {
	#if SKY_FULL_REGION
	production = RUNTIME;
	#else
	production = SAVED;
	#endif
	if (fab == INIT) {
		int row0, col0, nrow_delta, ncol_delta;
		status = sky_get_amplifier_region_info(q, &row0, NULL, &nrow_delta, &col0, NULL, &ncol_delta);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get amplifier region info", name);
			return(status);
		}
		status = thReg3Prefab(threg3, row0, col0, nrow_delta, ncol_delta, production);		
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not 'PREFAB' thregion", name);
			return(status);
		}
		return(SH_SUCCESS);
	}
} else {
	production = SAVED;
	if (fab == INIT) {
		status = thReg3Prefab(threg3, 0, 0, init_nrow, init_ncol, production);		
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not 'PREFAB' thregion", name);
			return(status);
		}
		return(SH_SUCCESS);
	}
}

return(SH_SUCCESS);
}




RET_CODE SkySpecifyRegion_old(void *q, PSF_CONVOLUTION_INFO *psf_info, THREGION *threg, int index, int reprefab) {
char *name = "SkySpecifyRegion_old";

#if SKY_FULL_REGION
#else
thError("%s: ERROR - this function is meant for full-region sky place holding", name);
return(SH_GENERIC_ERROR);
#endif

if (q == NULL || threg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (index != 0 && index != BAD_PSF_INDEX) {
	thError("%s: ERROR - unsupported index (%d)", name, index);
	return(SH_GENERIC_ERROR);
}


RET_CODE status;
THREGION_TYPE regtype = threg->type;
if (regtype == UNKNOWN_THREGION_TYPE) {
	if (threg->threg != NULL) {
		thError("%s: ERROR - improperly allocated (thregion), type UNKNOWN while a non-null value found",
			name);
		return(SH_GENERIC_ERROR);
	}
	status = thRegRenew(threg, "", TYPE_THPIX, THREGION1_TYPE);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not renew (thregion)", name);
		return(status);
	}
} else if (regtype != THREGION1_TYPE) {
	if (schema_thregion_type == NULL) init_static_vars ();
	thError("%s: ERROR - unsupported (thregion) type '%s'", 
		name,(schema_thregion_type->elems[regtype]).name);
	return(SH_GENERIC_ERROR);
}
THREGION1 *threg1;
threg1 = threg->threg;

if (threg1 == NULL) {
	threg1 = thRegion1New("sky basis function", TYPE_THPIX);
	threg->threg = threg1;
	return(SH_SUCCESS);
	}

FABRICATION_FLAG fab = threg1->fab;
PRODUCTION_FLAG production = SAVED;

if (sky_is_this_an_amplifier_delta(q)) {
	#if SKY_FULL_REGION
	production = RUNTIME;
	#else
	production = SAVED;
	#endif
}

if (fab == INIT) {
	status = thReg1Prefab(threg1, 0, 0, init_nrow, init_ncol, production);		
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not 'PREFAB' thregion", name);
		return(status);
	}
	return(SH_SUCCESS);
}

return(SH_SUCCESS);

thError("%s: ERROR - unsupported (fabrication_flag)", name);
return(SH_GENERIC_ERROR);
}


void dbgSky_stored_frowfcol(CHAIN *stored_x, int nx, int mx, char *comment) {
char *name = "dbgSky_stored_frowfcol";
if (stored_x == NULL || shChainSize(stored_x) == 0) {
	printf("%s: DEBUG - null or empty stored (frow / fcol) chain while (nx = %d), (mx = %d) \n", comment, nx, mx);
	return;
}
printf("%s: DEBUG - frow / fcol (nx = %d), (mx = %d) \n", comment, nx, mx);
int nchain = shChainSize(stored_x);
printf("%s: DEBUG - frow / fcol chain size = %d \n", comment, nchain);
int i;
for (i = 0; i < nchain; i++) {
	VECT *v = shChainElementGetByPos(stored_x, i);
	if (v == NULL) {
		printf("%s: vector[%d] = null \n", comment, i);
	} else {
		THPIX *x = v->vec;
		int dim = v->dim;
		int r = v->r;
		int j;
		printf("%s: x[%d, r = %d, dim = %d] = ", comment, i, r, dim);
		for (j = 0; j < dim; j=j+100) {
			printf("%10.5g, ", x[j]);
		}
		printf("\n");
	}
}

return;
}

