#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "strings.h"
#include "dervish.h"

#include "sohoEnv.h"

RET_CODE shIoFpcDebug(char *name, HDR *header, REGION *reg);
RET_CODE shPrintHdr(HDR *header);
RET_CODE shPrintReg(REGION *reg);

/* **************************************************** */
RET_CODE shIoFpcDebug(char *name, 
		      HDR *header, REGION *reg)
{ RET_CODE shStatus;

  printf("**** %s **** \n", name);
  shStatus = shPrintHdr(header);
  shStatus = shPrintReg(reg);
  
  return(shStatus);
}
    

RET_CODE shPrintHdr(HDR *header)
{
  int LineNo;
  RET_CODE shStatus;
  char *Line;

  if (header == NULL) {
    printf("HEADER Pointer is NULL \n");
    return(SH_SUCCESS);
  } else {
    printf("HEADER = %d \n", sizeof(*header));
    
    shStatus = SH_SUCCESS;
    LineNo = 1;
    while (shStatus == SH_SUCCESS) {
      shStatus = shHdrGetLineCont (header, &LineNo, Line);
      if (shStatus == SH_SUCCESS) {
	printf("%d: %s \n", LineNo, Line);
	LineNo++;
      } else {
	LineNo--;
      }
      if (shStatus == SH_HEADER_IS_NULL) {
	printf("%d: Header is Null \n", LineNo);
      }
    }
    printf("Number of Lines in the header: %d \n", LineNo);
    return(shStatus);
  }
}
  
RET_CODE shPrintReg(REGION *reg)
{
  RET_CODE shStatus;

    // printing the header information
  // Program received signal SIGSEGV, Segmentation fault. //

  if (reg == NULL) {
    printf("REGION pointer is NULL \n");
    return(SH_SUCCESS);
  } else {
    printf("REGION = %d \n", sizeof(*reg));
    shStatus = SH_SUCCESS;
    if (reg != NULL) {
      printf("REG: (NROW, NCOLUMN), NTYPE: (%d, %d), %d \n", reg->nrow, reg->ncol, reg->type);
      shPrintHdr(&reg->hdr);
      return(SH_SUCCESS);
    } else {
      printf("REG: Null \n");
      return(SH_GENERIC_ERROR);
    }}
  
}
  
