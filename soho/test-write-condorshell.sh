#!/bin/bash

source "./write_parfile.sh"
source "./write_condorjob.sh"
source "./write_condorshell.sh"
source "./find_npsf.sh"
source "./counts_files_in_dir.sh"

name="test-write-condorshell"

condor_shellfile="MY_SHELL_FILE.sh"
condor_comment="MY_COMMENT"
executable="MY_FOLDER/MY_EXECUTABLE"
condor_argument="--MY_CONDOR_FLAG MY_CONDOR_FLAG_VALUE"
dataskydir="MY_LOCATION_FOR_STORED_ZIP_FILES"
inskydir="MY_LOCATION_FOR_UNZIPPED_FILES"
fpCDir="MY_LOCATION_FOR_FPC_FILES"
fpCCDir="MY_LOCATION_FOR_FPCC_FILES"

run="4797"
field="120"
camcol="3"

write_condorshell --field=$field --run=$run --camcol=$camcol --shellfile=$condor_shellfile --comment="$condor_comment" --executable="$executable" --argument="$condor_argument" --inskydir="$dataskydir" --outskydir="$inskydir" --zipfpC --fpCDir="$fpCDir" --zipfpCC --fpCCDir="$fpCCDir" 

echo "$name: file written to $condor_shellfile"

