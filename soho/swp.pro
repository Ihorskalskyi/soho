pro create_data_lrg_match_goodness_plots
name = "create_data_lrg_match_goodness_plots"

define_globals
sample_report = define_goodness_report()

camcol_list = [2, 4, 5, 6]

dir_list = ["sdss-data"]
mle_type_list = [$
"CD-sersic-sersic-G-deV-exp-fpC", $
"CD-deV-G-deV-exp-fpC", $
"CD-sersic-G-deV-exp-fpC", $
"CD-deV-deV-G-deV-exp-fpC", $
"CD-sersic-Exp-G-deV-exp-fpC"]
mle_legend_list = [$
"CD-sersic-sersic", $
"CD-deV", $
"CD-sersic", $
"CD-deV-deV", $
"CD-sersic-Exp"]

mle_type_extension = [$
"/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-5-psf-variable", $
"/stage-1/flag-fitindex-fitsize-fitshape-fitcenter-unconstrained-multi-init-5-psf-variable"]


for i_dir = 0L, n_elements(dir_list) - 1L, 1L do begin
for i_mle_type_extension = 0L, n_elements(mle_type_extension) - 1L, 1L do begin

dir = dir_list[i_dir]

roots="/u/khosrow/thesis/opt/soho/multi-object/images/mle/"+dir+"/"+mle_type_list + mle_type_extension[i_mle_type_extension]
outdir = "/u/khosrow/thesis/opt/soho/multi-object/images/mle/"+dir+"/" + mle_type_extension[i_mle_type_extension]

runlist_draw_matched_tuples_from_mle, $
	roots = roots, band = band, root_titles = mle_legend_list, $
	run_list = run_list, rerun = rerun, camcol_list = camcol_list, outdir = outdir, $
	star = keyword_set(star), galaxy = keyword_set(galaxy), LRG = keyword_set(LRG), flagLRG = flagLRG, $
	locmap = locmap, mag_thresh = mag_thresh, suffix = suffix, status = status, $
	sample_report = sample_report, $
	distance_match = distance_match, data = data


endfor
endfor

return
end
