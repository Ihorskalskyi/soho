#ifndef THPROBABILITY_H
#define THPROBABILITY_H

double Beta_Distribution(double x, double a, double b);
double Beta_Function(double a, double b);
double Ln_Beta_Function(double a, double b);
double Gamma_Function(double x);
double Ln_Gamma_Function(double x);
double F_Distribution(double f, int v1, int v2);


#endif
