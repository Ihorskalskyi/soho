#ifndef THPSFTYPES_H
#define THPSFTYPES_H


#include "phCalib1.h"
#include "phVariablePsf.h"

#include "thConsts.h"
#include "thBasic.h"
#include "thMathTypes.h"

#include "thMaskTypes.h"
#include "thNaiveMaskTypes.h"
#include "thCalibTypes.h"

#include "fftw.h"

#define PHPSF_WING CALIB_IO
#define phPsfWingDel phCalibIoDel
#define phPsfWingNew phCalibIoNew

#define thFftwKernelNew thFftw_kernelNew
#define thFftwKernelDel thFftw_kernelDel

#define thPsfConvReportNew thPsf_convolution_reportNew
#define thPsfConvReportDel thPsf_convolution_reportDel

#define thPsfConvInfoNew thPsf_convolution_infoNew
#define thPsfConvInfoDel thPsf_convolution_infoDel

#define thBasicStatNew thBasic_statNew
#define thBasicStatDel thBasic_statDel

#define thPsfReportNew thPsfreportNew
#define thPsfReportDel thPsfreportDel

#define thBriefPsfReportNew thBriefpsfreportNew
#define thBriefPsfReportDel thBriefpsfreportDel

#define thPsfDotProductNew thPsfdotproductNew
#define thPsfDotProductDel thPsfdotproductDel


typedef struct psfwing {
  FL32 a[MYNANN2 + 1];
  FL32 rad[MYNANN2 + 1];
  int nrad;

  FL32 rIntegLP[MAXNROW];
  FL32 IntegLP[MAXNROW];
  int nr;

  REGION *reg;
  FL32 totlum;
  
} PSFWING; 

typedef enum conv_method {
	DIRECT_CONV, FFTW_CONV, N_CONV_METHOD, UNKNOWN_CONV_METHOD
} CONV_METHOD;

typedef enum psf_source {
	WING, KL, N_PSF_SOURCE, UNKNOWN_PSF_SOURCE
} PSF_SOURCE;

typedef enum modification_flag {
	MODIFIED = 0, BUILT, N_MODIFICATION_FLAG, UNKNOWN_MODIFICATION_FLAG
} MODIFICATION_FLAG;

typedef struct fftw_kernel {
	FL32 rowc, colc; /* so that the structure can replace PSF_REG */
	MODIFICATION_FLAG aflag; /* has this kernel been alterened since it was last time calc-ed ? */ 
	PSF_SOURCE source;
	/* the image of the PSF kernel */
	int nrow, ncol; /* this is the nrow and ncol for the original, unpadded kernel image */
	REGION *reg;
	REGION *hreg; /* this is a subreg of the source or null, never modify the contents */
	REGION *padded_reg;
	WMASK *border;
	FL32 totlum;
	FL32 counts, hcounts;
	FL32 psf_a;
	/* mathematical method used for convolution */	
	CONV_METHOD conv_method;
	/* central hole */
	int hrow, hcol;
	
	/* FFTW and its plan - if needed*/	
	FFTW_COMPLEX **fftw_kern, *fftw_kern_pix;
	int prow, pcol, pmax;

	fftwnd_plan plan_fwd, plan_bck;
	int planrow, plancol;

} FFTW_KERNEL;	/* pragma IGNORE */

typedef struct phpsfmodel {
  int filter;
  PSF_BASIS *basis;
  PHPSF_WING *wing;
  CRUDE_CALIB *cc;
  CHAIN *ccs; /* CHAIN of crude_calibs for all filters - added  on July 14, 2016 */
} PHPSFMODEL; /* pragma IGNORE */


typedef struct thpsfmodel {
  PSF_BASIS *basis;
  PSFWING *wing;
  CRUDE_CALIB *cc;
  CHAIN *ccs; /* chain of crude_calibs for all filters - added on July 14, 2016 */
  /* private */
  void *parent;
} PSFMODEL; /* pragma IGNORE */

typedef enum psf_convolution_type {
	UNKNOWN_PSF_CONVOLUTION, 
	NO_PSF_CONVOLUTION, /* no psf convolution */ 
	CENTRAL_PSF, /* constant PSF over image, derived at the center */
	CENTRAL_AND_WINGS_PSF, /* constat PSF derivet at the center with wings */
	STAR_CENTRAL_AND_WINGS, 
	VARYING_CENTRAL_PSF, /* spatially variable PSF from PHOTO */
	VARYING_CENTRAL_AND_WINGS_PSF, /* spatially variable PSF with wings */
	RADIAL_PSF, /* the azimuthally symmetric PSF */ 
	WINGS_ONLY_PSF,  /* the outer part  */
	DG_PSF, /* double gaussian psf */
	G_PSF,  /* gaussian psf */
	N_PSF_CONVOLUTION_TYPE
} PSF_CONVOLUTION_TYPE;

typedef enum psf_component {
	OUTER_PSF, INNER_PSF, N_PSF_COMPONENT, UNKNOWN_PSF_COMPONENT
	} PSF_COMPONENT;

typedef struct wpsf {
	/* working space for PSF to make LWORK references concise */
	PSFMODEL *psf_model;
	PSF_REG *psf_reg;
	FFTW_KERNEL *fftw_kernel[N_PSF_COMPONENT];
} WPSF;	/* pragma IGNORE */

typedef struct psf_convolution_report {
	int inbound, outbound;
	FL32 bkg_outer, alpha_outer, alpha_inner, lost_light, halo_frac;
} PSF_CONVOLUTION_REPORT; /* pragma IGNORE */

typedef struct psf_convolution_info {
	PSF_CONVOLUTION_TYPE conv_type;
	FL32 rowc, colc;
 
	/* substantive PSF info */
	WPSF *wpsf; /* this is a link to a structure of the same name in LWORK */
 
	/* component information */
	NAIVE_MASK inmask[N_PSF_COMPONENT];
	NAIVE_MASK outmask[N_PSF_COMPONENT];
	int margin[N_PSF_COMPONENT];
	int ncomponent; /* this record has a particular meaning 
				when convolution type is set
				to NO_PSF_CONVOLUTION in THREGION2
				it shows the number of components
				for the image
			*/
	/* this is a brief report of some parameters that are only available for certain types of convolution */
	PSF_CONVOLUTION_REPORT *report; 
} PSF_CONVOLUTION_INFO;	/* pragma IGNORE */	

typedef struct {
    const int ncolor;
    int field;
    double mjd;
    PSP_STATUS psp_status;
    float node;
    float incl;
    PSP_STATUS status[NCOLOR];
    float sky[NCOLOR];
    float skysig[NCOLOR];
    float skyerr[NCOLOR];
    float skyslope[NCOLOR];
    float lbias[NCOLOR];
    float rbias[NCOLOR];
    float flux20[NCOLOR];
    float flux20Err[NCOLOR];
    int psf_nstar[NCOLOR];
    float psf_ap_correctionErr[NCOLOR];
    int nann_ap_frame[NCOLOR]; 
    int nann_ap_run[NCOLOR]; 
    float ap_corr_run[NCOLOR]; 
    float ap_corr_runErr[NCOLOR]; 
    float psf_sigma1[NCOLOR];
    float psf_sigma2[NCOLOR];
    float psf_sigmax1[NCOLOR];
    float psf_sigmax2[NCOLOR];
    float psf_sigmay1[NCOLOR];
    float psf_sigmay2[NCOLOR]; 
    float psf_b[NCOLOR];
    float psf_p0[NCOLOR];
    float psf_beta[NCOLOR];
    float psf_sigmap[NCOLOR];
    float psf_width[NCOLOR];
    float psf_psfCounts[NCOLOR];
    float psf_sigma1_2G[NCOLOR];
    float psf_sigma2_2G[NCOLOR]; 
    float psf_b_2G[NCOLOR];
    float psfCounts[NCOLOR];		 
    int prof_nprof[NCOLOR];			 
    float prof_mean[NCOLOR][MYNANN2];		 
    float prof_med[NCOLOR][MYNANN2];		 
    float prof_sig[NCOLOR][MYNANN2];			 
    float gain[NCOLOR];
    float dark_variance[NCOLOR];
} MYCALIB_IO;                  /* pragma SCHEMA */

/* PSF report structures */

typedef struct psfreport {
	FL32 xc, yc;
	int inboundary, outboundary;
	FL32 background, amplitude, lostlight, halofrac;
	/* descriptive stat for each component */
	FL32 sigma[N_PSF_REPORT_TYPE], sigma1[N_PSF_REPORT_TYPE], sigma2[N_PSF_REPORT_TYPE];
	FL32 FWHM[N_PSF_REPORT_TYPE], sum[N_PSF_REPORT_TYPE];
	/* more extensive info */
	FL32 rad[PSFNRAD];
	FL32 average[N_PSF_REPORT_TYPE][PSFNRAD];
	int nrad[N_PSF_REPORT_TYPE];
	FL32 bzero[N_PSF_REPORT_TYPE], bscale[N_PSF_REPORT_TYPE];
	REGION *image[N_PSF_REPORT_TYPE];
} PSFREPORT; /* pragma SCHEMA */


typedef struct briefpsfreport {
	int inboundary, outboundary;
	int n; /* sample size */
	
	FL32 inboundary_c1;
	FL32 inboundary_w1;
	FL32 inboundary_c2;
	FL32 inboundary_w2;
	
	FL32 outboundary_c1;
	FL32 outboundary_w1;
	FL32 outboundary_c2;
	FL32 outboundary_w2;

	FL32 background_c1;
	FL32 background_w1;
	FL32 background_c2;
	FL32 background_w2;

	FL32 amplitude_c1;
	FL32 amplitude_w1;
	FL32 amplitude_c2;
	FL32 amplitude_w2;

	FL32 lostlight_c1;
	FL32 lostlight_w1;
	FL32 lostlight_c2;
	FL32 lostlight_w2;

	FL32 halofrac_c1;
	FL32 halofrac_w1;
	FL32 halofrac_c2;
	FL32 halofrac_w2;

	FL32 sigma_c1[N_PSF_REPORT_TYPE];
	FL32 sigma_w1[N_PSF_REPORT_TYPE];
	FL32 sigma_c2[N_PSF_REPORT_TYPE];
	FL32 sigma_w2[N_PSF_REPORT_TYPE];

	FL32 sigma1_c1[N_PSF_REPORT_TYPE];
	FL32 sigma1_w1[N_PSF_REPORT_TYPE];
	FL32 sigma1_c2[N_PSF_REPORT_TYPE];
	FL32 sigma1_w2[N_PSF_REPORT_TYPE];

	FL32 sigma2_c1[N_PSF_REPORT_TYPE];
	FL32 sigma2_w1[N_PSF_REPORT_TYPE];
	FL32 sigma2_c2[N_PSF_REPORT_TYPE];
	FL32 sigma2_w2[N_PSF_REPORT_TYPE];

	FL32 FWHM_c1[N_PSF_REPORT_TYPE];
	FL32 FWHM_w1[N_PSF_REPORT_TYPE];
	FL32 FWHM_c2[N_PSF_REPORT_TYPE];
	FL32 FWHM_w2[N_PSF_REPORT_TYPE];

	FL32 sum_c1[N_PSF_REPORT_TYPE];
	FL32 sum_c2[N_PSF_REPORT_TYPE];
	FL32 sum_w1[N_PSF_REPORT_TYPE];
	FL32 sum_w2[N_PSF_REPORT_TYPE];
	
	char *name_c1;
	char *name_w1;
	char *name_c2;
	char *name_w2;

} BRIEFPSFREPORT; /*pragma SCHEMA */

typedef struct  psfdotproduct {
	MLEFL in_in, out_out, in_out, in_one, out_one, one_one;
} PSFDOTPRODUCT; /* pragma IGNORE */	

RET_CODE psf_check_type_ncomponent_consistency(PSF_CONVOLUTION_TYPE type, int ncomponent);

PHPSFMODEL *thPhpsfmodelNew();
void thPhpsfmodelDel(PHPSFMODEL *phpsf);

PSFWING *thPsfwingNew();
void thPsfwingDel(PSFWING *wing);

FFTW_KERNEL *thFftwKernelNew();
void thFftwKernelDel(FFTW_KERNEL *fftw_kern);
RET_CODE thFftwKernelPutPsfA(FFTW_KERNEL *fftw_kern, FL32 psf_a);
RET_CODE thFftwKernelGetDirReg(FFTW_KERNEL *fftw_kern, REGION **reg);
RET_CODE thFftwKernelGetCounts(FFTW_KERNEL *fftw_kern, FL32 *counts, FL32 *hcounts);

PSFMODEL *thPsfmodelNew();
void thPsfmodelDel(PSFMODEL *psf);

WPSF *thWpsfNew();
void thWpsfDel(WPSF *wpsf);
RET_CODE thWpsfUpdate(WPSF *wpsf, PSFMODEL *psf_model);

PSF_CONVOLUTION_REPORT *thPsf_convolution_reportNew();
void thPsf_convoluton_reportDel(PSF_CONVOLUTION_REPORT *report);
RET_CODE thPsfConvReportCopy(PSF_CONVOLUTION_REPORT *treport, PSF_CONVOLUTION_REPORT *sreport);
RET_CODE thPsfConvReportUpdate(PSF_CONVOLUTION_REPORT *report, FL32 *alpha_inner, FL32 *alpha_outer, FL32 *bkg_outer, FL32 *lost_light, FL32 *halo_frac);
RET_CODE thPsfConvReportGet(PSF_CONVOLUTION_REPORT *report, FL32 *alpha_inner, FL32 *alpha_outer, FL32 *bkg_outer);

PSF_CONVOLUTION_INFO *thPsf_convolution_infoNew();
void thPsf_convolution_infoDel(PSF_CONVOLUTION_INFO *psf_conv_info);

RET_CODE thPsfConvInfoGetNComponent(PSF_CONVOLUTION_INFO *psf_info, int *n);
RET_CODE thPsfConvInfoGetPsfConvType(PSF_CONVOLUTION_INFO *psf_info, 
					PSF_CONVOLUTION_TYPE *type);
RET_CODE thPsfConvInfoUpdate(PSF_CONVOLUTION_INFO *psf_info, 
		PSF_CONVOLUTION_TYPE type, int ncomponent);

RET_CODE thPsfInfoGetCenter(PSF_CONVOLUTION_INFO *psf_info, FL32 *rowc, FL32 *colc);
RET_CODE thPsfInfoPutCenter(PSF_CONVOLUTION_INFO *psf_info, FL32 rowc, FL32 colc);

RET_CODE thPsfInfoGetPsfReg(PSF_CONVOLUTION_INFO *psf_info, PSF_REG **psf_reg);
RET_CODE thPsfInfoGetPsfModel(PSF_CONVOLUTION_INFO *psf_info, PSFMODEL **psf_model);
RET_CODE thPsfInfoGetPsfWing(PSF_CONVOLUTION_INFO *psf_info, PSFWING **psf_wing);
RET_CODE thPsfInfoGetPsfBasis(PSF_CONVOLUTION_INFO *psf_info, PSF_BASIS **psf_basis);
RET_CODE thPsfInfoGetPsfConvReport(PSF_CONVOLUTION_INFO *psf_info, PSF_CONVOLUTION_REPORT **report);

RET_CODE thPsfInfoPutMask(PSF_CONVOLUTION_INFO *psf_info, PSF_COMPONENT psf_c, 
	int row0, int col0, int nrow, int ncol, int margin);
RET_CODE thPsfInfoGetMask(PSF_CONVOLUTION_INFO *psf_info, PSF_COMPONENT psf_c, NAIVE_MASK **inmask, NAIVE_MASK **outmask, int *margin);
RET_CODE thPsfInfoGetFftwKernel(PSF_CONVOLUTION_INFO *psf_info, PSF_COMPONENT psf_c, FFTW_KERNEL **fftw_kernel);
RET_CODE thPsfInfoGetBorderMask(PSF_CONVOLUTION_INFO *psf_info, WMASK **wmask);

MYCALIB_IO *thMyCalibIoNew();
void thMyCalibIoDel(MYCALIB_IO *x);

RET_CODE phPsfModelGetCrudeCalib(PHPSFMODEL *psf, CRUDE_CALIB **cc);
RET_CODE phPsfModelPutCrudeCalib(PHPSFMODEL *psf, CRUDE_CALIB *cc);
RET_CODE thPsfModelGetCrudeCalib(PSFMODEL *psf, CRUDE_CALIB **cc);
RET_CODE thPsfModelPutCrudeCalib(PSFMODEL *psf, CRUDE_CALIB *cc);

PSFREPORT *thPsfreportNew();
void thPsfreportDel(PSFREPORT *x);
RET_CODE thPsfReportPutData(PSFREPORT *report, CHAIN *report_chain, CHAIN *stat_chain, int deep);

BRIEFPSFREPORT *thBriefpsfreportNew();
void thBriefpsfreportDel(BRIEFPSFREPORT *x);

PSFDOTPRODUCT *thPsfdotproductNew();
void thPsfdotproductDel(PSFDOTPRODUCT *prod);
RET_CODE thPsfDotProductPut(PSFDOTPRODUCT *prod, MLEFL in_in, MLEFL out_out, MLEFL in_out, MLEFL in_one, MLEFL out_one, MLEFL one_one);
RET_CODE thPsfDotProductGet(PSFDOTPRODUCT *prod, MLEFL *in_in, MLEFL *out_out, MLEFL *in_out, MLEFL *in_one, MLEFL *out_one, MLEFL *one_one);

RET_CODE thPsfWingGetIntegLP(PSFWING *psfwing, FL32 **IntegLP, FL32 **rIntegLP, int *nr);
RET_CODE thPsfModelGetIntegLP(PSFMODEL *psfmodel, FL32 **IntegLP, FL32 **rIntegLP, int *nr);
RET_CODE thWpsfGetIntegLP(WPSF *wpsf, FL32 **IntegLP, FL32 **rIntegLP, int *nr);
RET_CODE thPsfModelGetPsfWing(PSFMODEL *psfmodel, PSFWING **psfwing);
RET_CODE thWpsfGetPsfModel (WPSF *wpsf, PSFMODEL **psfmodel);
RET_CODE thPsfConvInfoGetIntegLP(PSF_CONVOLUTION_INFO *psfinfo, FL32 **IntegLP, FL32 **rIntegLP, int *nr);
#endif
