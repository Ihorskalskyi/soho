#include "thFuncmodelTypes.h"

FUNCMODEL *thFuncmodelNew () {
  
  FUNCMODEL *func;
  func = (FUNCMODEL *) thCalloc(1, sizeof(FUNCMODEL));
  func->stat = UNKNOWN_FUNCMODEL;
  func->regs = thCalloc(N_FUNCMODEL_COMP_INDEX, sizeof(REGION*));
  return(func);

}

void thFuncmodelDel(FUNCMODEL *func) {

  if (func == NULL) {
    return;
  }

  if (func->funcid != NULL) thFree(func->funcid);
  if (func->funcname != NULL) thFree(func->funcname);
  if (func->parlist != NULL) thParlistDel(func->parlist);
  /* if the function is complex, then the components are not deleted
     only the place holders to the components are destroyed */
  if (func->components != NULL) shChainDel(func->components);
  if (func->regs != NULL) {
	int i;
	REGION *reg;
	for (i = 0; i < N_FUNCMODEL_COMP_INDEX; i++) {
		reg = func->regs[i];
		if (reg != NULL) shRegDel(reg);
		}
		thFree(func->regs);
	}

  thFree(func);
  return;

}

int thFuncmodelGetNComponent (FUNCMODEL *func, RET_CODE *status) {
  
  if (func == NULL) {
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(-1);
  }

  if (func->stat & COMPLEX_FUNCMODEL) {    
    if (status != NULL) *status = SH_SUCCESS;
    if (func->components == NULL) return(0);
    return(shChainSize(func->components));
  }
  
  /* a simple function */
  if (func->stat & UNKNOWN_FUNCMODEL) {
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(-1);
  }

  if (status != NULL) *status = SH_SUCCESS;
  return(1);
}


OBJCMODEL *thObjcmodelNew () {

  OBJCMODEL *objc;
  objc = (OBJCMODEL *) thCalloc(1, sizeof(OBJCMODEL));
  objc->stat = UNKNOWN_OBJCMODEL;
  objc->regs = thCalloc(N_FUNCMODEL_COMP_INDEX, sizeof(REGION*));
  return(objc);

}

void thObjcmodelDel (OBJCMODEL *objc) {

  if (objc == NULL) return;
  if (objc->objcid != NULL) thFree(objc->objcid);
  if (objc->objctype != NULL) thFree(objc->objctype);
  if (objc->parlist != NULL) thParlistDel(objc->parlist);
  if (objc->funcs != NULL) shChainDel(objc->funcs);
  if (objc->regs != NULL) {
	REGION *reg;
	int i;
	for (i = 0; i < N_FUNCMODEL_COMP_INDEX; i++) {
		 reg = objc->regs[i];
		if (reg != NULL) shRegDel(reg);
		}
	thFree(objc->regs);
	}

  thFree(objc);
  return;

}

int thObjcmodelGetNFuncmodel (OBJCMODEL *objc, RET_CODE *status) {

  if (objc == NULL) {
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(-1);
  }

  if (status != NULL) *status = SH_SUCCESS;
  if (objc->funcs == NULL) return(0);
  return(shChainSize(objc->funcs));

}
  
