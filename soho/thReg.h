#ifndef THREG_H
#define THREG_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dervish.h"
#include "thConsts.h"
#include "thMath.h"

#ifndef MAX
#define MAX(a,b) \
       ({ typeof (a) _a = (a); \
           typeof (b) _b = (b); \
         _a > _b ? _a : _b; })
#endif
#ifndef MIN
#define MIN(a,b) \
       ({ typeof (a) _a = (a); \
           typeof (b) _b = (b); \
         _a < _b ? _a : _b; })
#endif

THPIX thRegDotReg(REGION *reg1, REGION *reg2, 
		  REGION *wp, MASK *mask, /* wp and mask are optional will be 1 if NULL */
		  unsigned char maskbit);

RET_CODE thRegAddScalar(THPIX c, REGION *reg, REGION *res,
			int row0, int row1, int col0, int col1);

RET_CODE thRegCrossScalar(THPIX c, REGION *reg,  /* input scalar and region */
			  REGION *res /* output region */
			  );
RET_CODE thRegAddReg(REGION *reg1, REGION *reg2, /* input regions */
		     REGION *reg12 /* output region */
		     );

RET_CODE thRegCrossReg(REGION *reg1, REGION *reg2, /* input regions */
		       REGION *reg12 /*output region */
		       );

THPIX thRegSum(REGION *reg, 
	       int row0, int row1, int col0, int col1,
	       REGION *wp, MASK *mask, unsigned char maskbit);

int thCountMaskPix(const int row0, const int row1, 
		   const int col0, const int col1, 
		   MASK *mask, unsigned char maskbit);

RET_CODE thRegMakeZero(REGION *reg);

RET_CODE thRegInplaceAddRegCrossScalar (REGION *res, REGION *reg, THPIX x);

RET_CODE thRegConvertPixType (REGION *reg,REGION  *res, 
			      FL64 bzero, FL64 bscale);

RET_CODE thCompressRegion(REGION *reg, PIXDATATYPE restype,
			  FL64 *bscale, FL64 *bzero);

RET_CODE thRegMinusReg(REGION *mmd, REGION *m, REGION *d);

RET_CODE thRegPixCopy(REGION *regin, REGION *regout);

RET_CODE shRegReshape(REGION *reg, int nrow, int ncol);

#endif
