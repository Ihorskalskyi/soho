#ifndef THNAIVE_MASK_TYPES_H
#define THNAIVE_MASK_TYPES_H

#include "dervish.h"
#include "thConsts.h"
#include "thBasic.h"
#include "thMath.h"

#define thNaiveMaskNew thNaive_maskNew
#define thNaiveMaskDel thNaive_maskDel

typedef enum naive_mask_type {
	RECTANGLE, RING, 
	UNKNOWN_NAIVE_MASK, N_NAIVE_MASK_TYPE
} NAIVE_MASK_TYPE;

typedef struct naive_mask {
	int x0[N_ELEM_NAIVE_MASK];
	int x1[N_ELEM_NAIVE_MASK];
	int y0[N_ELEM_NAIVE_MASK];
	int y1[N_ELEM_NAIVE_MASK];
	int nspan;
	NAIVE_MASK_TYPE type;
} NAIVE_MASK;

NAIVE_MASK *thNaiveMaskNew();
void thNaiveMaskDel();

RET_CODE thNaiveMaskCopy(NAIVE_MASK *t, NAIVE_MASK *s);
RET_CODE thNaiveMaskReset(NAIVE_MASK *m);
RET_CODE thNaiveMaskGetNspan(NAIVE_MASK *m, int *nspan);
RET_CODE thNaiveMaskGetElement(NAIVE_MASK *m, int s, int *x0, int *x1, int *y0, int *y1);

RET_CODE thNaiveMaskFromRect(NAIVE_MASK *m, int x0, int x1, int y0, int y1);
RET_CODE thNaiveMaskAndNaiveMask(NAIVE_MASK *t, NAIVE_MASK *m);
RET_CODE thNaiveMaskNotIntersectionNaiveMask(NAIVE_MASK *t, NAIVE_MASK *m1, NAIVE_MASK *m2);
RET_CODE thNaiveMaskAndNotNaiveMask(NAIVE_MASK *t, NAIVE_MASK *m);
REGION *shRegNewFromRegMask(REGION *reg, NAIVE_MASK *mask, int margine);
RET_CODE shRegPixCopyMask(REGION *a, REGION *b, NAIVE_MASK *mask);
RET_CODE thNaiveMaskExpand(NAIVE_MASK *mask_t, NAIVE_MASK *mask_s, int margine);
REGION *shSubRegNewFromRegNaiveMask(const char *regname, REGION *reg, const NAIVE_MASK *mask);
#endif
