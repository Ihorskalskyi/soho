; based on the following: 
; source:
; http://www.sdss3.org/svn/repo/photoop/trunk/pro/image/read_frame.pro
;+
; NAME:
;   read_frame
; PURPOSE:
;   Read in corrected frame and apply attached calibrations
; CALLING SEQUENCE:
;   img= read_frame(run, camcol, field, rerun=, filter=, hdr=, $
;                   simg=, cimg=, /nosky, /nocalib)
;
; INPUTS:
;   run    - run number
;   camcol - camera column
;   field - field
;   rerun  - rerun name
;   filter - filter name ('u', 'g', 'r', 'i', 'z')
; OPTIONAL KEYWORDS:
;   nosky - restore the image to un-skysubtracted version
;   nocalib - restore the image to units of fpC (counts)
; OUTPUTS:
;   img - [2048, 1489] output image in nmgy per pixel, except
;         if /nocalib set, in which case of counts per pixel
;   hdr - header, including astrometry
;   simg - [2048, 1489] sky image (counts)
;   cimg - [2048, 1489] calibration image (nmgy/counts)
; COMMENTS:
;   Assumes that the appropriate image has been created by SDSS_FRAME, 
;     in the directory $BOSS_PHOTOOBJ/frames/[rerun]/[camcol]/[field]
;   Output image has calibrations, flats and sky-subtraction applied.
;   Note that these images have been
; REVISION HISTORY:
;   2009-Jun-03  Written by Mike Blanton, NYU
;
;----------------------------------------------------------------------
pro convert_frame_to_fpC, source, destination

name = "convert_frame_to_fpC"
print, name, ": source = ", source
print, name, ": destination = ", destination

;; read in image
img= mrdfits(source,0,hdr)
nrowc= (size(img,/dim))[1]
softbias = SXPAR(hdr, "SOFTBIAS")

;; read in calibrations and restore
calib= mrdfits(source,1)
cimg= calib#replicate(1.,nrowc)
;; restore to uncalibrated state if so desired
if(keyword_set(nocalib) gt 0) then img= img/cimg

;; handle sky
sky= mrdfits(source,2)
simg= interpolate(sky.allsky, sky.xinterp, sky.yinterp, /grid)

;; converting back to DN      
img=img/cimg+simg
img += softbias
mwrfits, img, destination, hdr, /create

print, name, ": destination file successfully written = ", destination
end

pro print_error_for_frame_process, n

name = "convert_frame_to_fpC_batch.pro"
print, ": ERROR - not enough argument passed to 'convert_frame_to_fpC_batch' - found (", n, ") expected (2)"

end 
