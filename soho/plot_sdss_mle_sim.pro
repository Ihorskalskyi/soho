;This is an IDL macro which puts the init (SDSS), (MLE) and the true shape parameter underlying the simulation in a plot

function smooth_log, z, n = n
if (n lt 1) then return, z
if (n lt 2 and n ge 1) then return, asinh(z)
return, smooth_log(z, n = n - 1)
end

function smooth_alog, z, n = n
if (n lt 1) then return, z
if (n lt 2 and n ge 1) then return, sinh(z)
return, smooth_alog(z, n = n - 1)
end

function smooth_data_for_plotting, x, y, z, bounds = bounds, $
	start = start, delta = delta, nx = nx, ny = ny, $
	mcs = mcs, ts = ts, gd = gd, verbose = verbose
name = "smooth_data_for_plotting"

n_smooth = 5
my_z = smooth_log(z, n = n_smooth)

use_min_curve_surface = 0
use_tri_surface = 0
use_grid_data = 1
if ((keyword_set(mcs) + keyword_set(ts) + keyword_set(gd)) gt 1) then begin
	print, name, ": ERROR - only one smoothing option can be set"
	return, [-1]
endif

if keyword_set(mcs) then begin
	use_min_curve_surface = 1
	use_tri_surface = 0
	use_grid_data = 0
endif
if keyword_set(ts) then begin
	use_min_curve_surface = 0
	use_tri_surface = 1
	use_grid_data = 0
endif
if keyword_set(gd) then begin
	use_min_curve_surface = 0
	use_tri_surface = 0
	use_grid_data = 1
end

if (use_min_curve_surface eq 1) then begin
	if (keyword_set(verbose)) then print, name, ": using min-curve-surf"
	my_smooth_z = MIN_CURVE_SURF(my_z, x, y, $
	/double, /tps, bounds = bounds, nx = nx, ny = ny)
endif
if (use_tri_surface eq 1) then begin
	if (keyword_set(verbose)) then print, name, ": using tri-surf"
	my_smooth_z = TRI_SURF(my_z, x, y, $
	/linear, /extrapolate, bounds = bounds, nx = nx, ny = ny)
endif
if (use_grid_data eq 1) then begin
	if (keyword_set(verbose)) then print, name, ": using grid-data"
	triangulate, x, y, tr
	my_smooth_z = GRIDDATA(x, y, my_z,  $
	/quintic, triangles = tr, dimension = [nx, ny], start = start, delta = delta)
endif 

smooth_z = smooth_alog(my_smooth_z, n = n_smooth)
return, smooth_z

end

function get_contour_levels, data, n_level = n_level, range = range, dolog = dolog, dohisto = dohisto, fracrange = fracrange
name = "get_contour_levels"
n_smooth = 1
npix = n_elements(data)
if (npix lt 2) then begin
	print, name, ": ERROR - not enough data points"
	return, -1
endif
row = reform(data, [npix])
row = row[sort(row)]
if (n_elements(range) eq 0) then begin
	range = [min(row), max(row)]
	if (n_elements(fracrange) ne 0) then range = quantile(fracrange, row, range = range, /nosort)
endif
if (n_elements(n_level) eq 0) then begin
	n_level = 10
endif
if (keyword_set(dolog) and keyword_set(dohisto)) then begin
	print, name, ": ERROR - cannot do histogram and logarithmic division at the same time"
	return, -1
endif
if (keyword_set(dolog)) then begin
	if (range[0] * range[1] le 0.0) then begin
		print, name, ": ERROR - cannot do logarithmic division if data changes sign"
		return, -1
	endif
	initial = range[0]
	ratio = (range[1] / range[0]) ^ (1.0 / (n_level - 1))
	levels = ratio ^ (findgen(n_level))
	levels = levels * initial
	return, levels
endif
if (keyword_set(dohisto)) then begin
	smooth_row = smooth_log(row, n = n_smooth)
	if (n_elements(fracrange) ne 2) then fracrange = [0.05, 0.95]
	fracs = findgen(n_level) * (fracrange[1] - fracrange[0]) / (n_level - 1.0) + fracrange[0]
	levels = quantile(fracs, smooth_row, range = range, /nosort)
	levels = smooth_alog(levels, n = n_smooth)
	if (keyword_set(verbose)) then begin
		print, name, ": COMMENT - fracs  = ", fracs[0: min([10, n_elements(fracs) - 1])]
		print, name, ": COMMENT - levels = ", levels[0:min([10, n_elements(levels) - 1])]
	endif
	uindex = uniq(levels, sort(levels))
	if (n_elements(uindex) ne n_elements(fracs)) then begin
		print, name, ": adjusting for uniqueness of levels"
		fracs = fracs[uindex]
		levels = levels[uindex]
		n_level = n_elements(levels)
	endif
	return, levels
endif
step = (range[1] - range[0]) / (n_level - 1)
initial = range[0]
if (step eq 0) then begin
	step = 2.0 / (n_level - 1)
	initial -= 1.0
endif
levels = findgen(n_level)
levels = levels * step
levels = levels + initial
return, levels
end


pro do_contour_legend, levels = levels, position = position, horizontal = horizontal, font = font, tickformat = tickformat, RGB_TABLE = RGB_TABLE
name = "do_contour_legend"
;; setting arguments necessary for continuation of the procedure
if (n_elements(font) eq 0) then font = 2
if (n_elements(tickformat) eq 0) then tickformat = "(f7.2)"
;; creating the color / grey-scale bar
n1 = 100
n2 = 100
max_levels = max(levels)
min_levels = min(levels)
range_levels = max_levels - min_levels

;; vin = levels
;; xin = findgen(n_level) / (n_level - 1.0)
;; xout = findgen(n1) / (n1 - 1.0)
;; vout = interpol(vin, xin, xout, /spline)

bar1 = findgen(n1) / (n1 - 1) * range_levels + min_levels 
;; bar1 = vout
bar2 = replicate(1.0, n2)
if (keyword_set(horizontal)) then begin
	bar = bar1 # bar2
endif else begin
	bar = bar2 # bar1
endelse
;; drawing the bar
if (keyword_set(horizontal)) then begin
	xrange = [min_levels, max_levels]
	yrange = [0.0, 1.0]
	xstyle = 1
	ystyle = 4
endif else begin
	xrange = [0.0, 1.0]
	yrange = [min_levels, max_levels]
	xstyle = 4
	ystyle = 1
endelse
;; labeling the axes and then creating the filled contour plot
font=1
nticks=11
plot, [1], [1], xrange = xrange, yrange = yrange, font = font, $
	xtickformat = tickformat, ytickformat = tickformat, $
	xstyle = xstyle, ystyle = ystyle, position = position, /nodata
;; if(keyword_set(horizontal)) then begin
;;	axis, xaxis = 1, xrange = xrange, xstyle = 1, xtitle = "secondary X", xticks = nticks, font = font, /data, /save
;; endif else begin
;;	axis, yaxis = 1, yrange = yrange, ystyle = 1, yticks = nticks, font = font, /data, /save
;; endelse
contour, bar, position = position, levels = levels, xstyle = 4, ystyle = 4, /fill ;;, RGB_TABLE = RGB_TABLE
end

pro get_contour_position, position = position, leg_position = leg_position
name = "get_contour_position"
pos0 = !my_cposition ;; [0.0, 0.0, 0.80, 1.0]
leg0 = !my_lposition ;;[0.90, 0.0, 1.0, 1.0]
wpos = !p.position
xscale = wpos[2] - wpos[0]
yscale = wpos[3] - wpos[1]
xmove =  wpos[0]
ymove =  wpos[1]
;; doing contour plot position
position = replicate(0.0, 4)
position[0] = pos0[0] * xscale + xmove
position[2] = pos0[2] * xscale + xmove
position[1] = pos0[1] * yscale + ymove
position[3] = pos0[3] * yscale + ymove
;; doing the legend position
leg_position = replicate(0.0, 4)
leg_position[0] = leg0[0] * xscale + xmove
leg_position[2] = leg0[2] * xscale + xmove
leg_position[1] = leg0[1] * yscale + ymove
leg_position[3] = leg0[3] * yscale + ymove

end

pro read_gpobjc, filename = filename, lmfile = lmfile, rnames = rnames, init = init, final = final, $
	doarcsec = doarcsec, error = error, verbose = verbose
name = "read_gpobjc"
error = 0
; This macro reads the particular keywords for SDSS and MLE from a gpObjc file
status = 0
if (not file_test(filename)) then begin
	if (keyword_set(verbose)) then print, name, ": ERROR - gpfile does not exist: ", filename
	error = -1
	print, "X", format='(A, $)' 
	return
endif

hdu = mrdfits(filename, 0, hdr, /silent, status = status)
if (status ne 0) then begin
	if (keyword_set(verbose)) then print, name, ": could not properly read (status = ", status, ") the header of the file ", filename
	error = status
	print, "B", format='(A, $)' 
	return
endif
sdssinit0=mrdfits(filename, 1, /silent, status = status); ;; OBJC_IO structure read from fpObjc file */
if (status ne 0) then begin
	if (keyword_set(verbose)) then print, name, ": could not properly read (status = ", status, ") ext [1] of the file ", filename
	error = status
	print, "B", format='(A, $)' 
	return
endif

init0 = mrdfits(filename, 2, /silent, status = status)
if (status ne 0) then begin
	if (keyword_set(verbose)) then print, name, ": could not properly read (status = ", status, ") the ext [2] of the file ", filename
	error = status
	print, "B", format='(A, $)' 
	return
endif
init0 = init0[0];
final0 = mrdfits(filename, 3, /silent, status = status)
if (status ne 0) then begin
	if (keyword_set(verbose)) then print, name, ": could not properly read (status = ", status, ") the ext [3] of the file ", filename
	error = status
	print, "B", format='(A, $)' 
	return
endif
final0 = final0[0]
if (n_elements(lmfile) eq 0) then begin
	lm = mrdfits(filename, 4, /silent, /dscale, status = status)
endif else begin
	lm = mrdfits(lmfile, 1, /silent, /dscale, status = status)
endelse
if (status ne 0) then begin
	if (keyword_set(verbose)) then print, name, ": could not properly read (status = ", status, ") the ext [3] of the file ", filename
	print, "B", format='(A, $)' 
	;;error = status
	;;return
endif

init_type = size(init0[0], /type)
final_type = size(final0[0], /type)
lm_type = size(lm[0], /type)
if (init_type ne 8) then begin
	if (keyword_set(verbose)) then print, name, ": ERROR - init0 is not a structure - problem with file ", filename
	print, "i", format='(A, $)' 
	error = -3
	return
endif
if (final_type ne 8) then begin
	if (keyword_set(verbose)) then print, name, ": ERROR - final0 is not a structure - problem with file ", filename
	print, "f", format='(A, $)' 
	error = -3
	return
endif
if (lm_type ne 8) then begin
	if (keyword_set(verbose)) then print, name, ": ERROR - lm is not a structure - problem with file ", filename
	;; print, "w", format='(A, $)' 
	;; error = -3
	;; return
endif
TWOPI = 2.0 * !PI
; see if the needed tags exist
all_tagnames = ['COUNTS', 'COUNTS_STAR', 'COUNTS_DEV', 'COUNTS_EXP', 'COUNTS_DEV2', 'COUNTS_EXP2', 'COUNTS_GAUSSIAN', 'COUNTS_SERSIC', $
		'XC', 'XC_STAR', 'XC_DEV', 'XC_EXP', 'XC_DEV2', 'XC_EXP2', 'XC_GAUSSIAN', 'XC_SERSIC', $
		'YC', 'YC_STAR', 'YC_DEV', 'YC_EXP', 'YC_DEV2', 'YC_EXP2', 'YC_GAUSSIAN', 'YC_SERSIC', $
		'DXC', 'DYC', 'DRC', $	
		'MAG', 'MAG_STAR', 'MAG_DEV', 'MAG_EXP', 'MAG_DEV2', 'MAG_EXP2', 'MAG_GAUSSIAN', 'MAG_SERSIC', $
		'CHISQ', 'NSTEP', 'FLUX20', 'BFILTER', "FRACPSF"]

tagsdss0 = tag_names(sdssinit0) 
tags0 = tag_names(init0)
tagsf0 = tag_names(final0)
if (n_elements(tags0) ne n_elements(tagsf0)) then begin
        if (keyword_set(verbose)) then	print, name, ": ERROR - old stype gpObjc file found: ", filename
	print, "b", format='(A, $)' 
	error = -4
	return
endif
ntag = 0
for i = 0, n_elements(all_tagnames) - 1, 1 do begin
	tag = all_tagnames[i]
	counts = 0
	tagindex = where(strlowcase(tags0) eq strlowcase(tag), counts)
	if (counts eq 0) then begin
		if (ntag eq 0) then tagnames = [tag] else tagnames = [tagnames, tag]
		ntag++
	endif	
endfor
if (ntag ne 0) then begin
	values = replicate('0.0', n_elements(tagnames))
	add_tags, init0, tagnames, values, init
	add_tags, final0, tagnames, values, final
endif
if (ntag eq 0) then begin
	init = init0
	final = final0
endif

;; init.counts = init.counts_dev + init.counts_exp + $
;; 	init.counts_dev2 + init.counts_exp2 + $
;; 	init.counts_gaussian + init.counts_sersic
fracpsf = sdssinit0.fracpsf[0]
if (sdssinit0.OBJC_TYPE  eq 6) then begin
	init.counts = sdssinit0.PsfCounts[0]
endif else begin
	init.counts = sdssinit0.counts_model[0]; sdssinit0.counts_deV[0] * fracpsf + sdssinit0.counts_exp[0] * (1.0 - fracpsf)
endelse
final.counts = final.counts_star + final.counts_dev + final.counts_exp + $
	final.counts_dev2 + final.counts_exp2 + $
	final.counts_gaussian + final.counts_sersic
init.fracpsf = fracpsf

;; print, name, ": ", filename, " : dss.counts_dev = ", sdssinit0.counts_dev[0], ", init.counts_dev = ", init.counts_dev, ", final.counts = ", final.counts_dev, ", frac = ", fracpsf, ", dss.r_dev = ", sdssinit0.r_dev[0]

if (ntag ne 0) then begin
	init0 = init
	final0 = final	
endif
if (n_elements(lm)  gt 0 and lm_type eq 8) then begin
	if (tag_exist(lm[0], "CHISQ") eq 1b) then begin
		init.chisq = lm[0].chisq
		final.chisq = min(lm[*].chisq)
	endif else begin
		error = -1
		init.chisq = !BAD_VALUE
		final.chisq = !BAD_VALUE
		print, name, ": ERROR - tag 'CHISQ' does not exist in [lm] in file ", filename
	endelse
	if (tag_exist(lm[0], "K") eq 1b) then begin
		init.nstep = lm[0].k
		final.nstep = lm[n_elements(lm) - 1L].k
	endif else begin
		error = -1
		init.nstep = !BAD_VALUE
		final.nstep = !BAD_VALUE
		print, name, ": ERROR - tag 'K'     does not exist in [lm] in file ", filename
	endelse
	if (init.chisq lt final.chisq) then begin
		print, name, ": WARNING - increase in CHISQ value found in file ", filename
		print, name, ": NSTEP = ", final.nstep, ", CHISQ: init = ", init.chisq, ", final = ", final.chisq, ", final - init = ", final.chisq - init.chisq
	endif

endif else begin
	if (keyword_set(verbose)) then print, name, ": WARNING - could not find LM chain in ", filename
	;; error = -1
endelse

if (tag_exist(init, "FLUX20") eq 1b) then begin
	init.flux20 = sxpar(hdr, 'FLUX20', /silent)
	init.bfilter = sxpar(hdr, 'BFILTER', /silent)
	final.flux20 = sxpar(hdr, 'FLUX20', /silent)	
	final.bfilter = sxpar(hdr, 'BFILTER', /silent)
endif else begin
	error = -1
	init.flux20 = !BAD_VALUE
	init.bfilter = !BAD_VALUE
	final.flux20 = !BAD_VALUE
	final.bfilter = !BAD_VALUE
	print, name, ": ERROR - tag 'FLUX20' does not exist in file ", filename
endelse

if (keyword_set(doarcsec)) then begin
	init.re_deV *= !PIXELSIZE
	init.re_exp *= !PIXELSIZE
	init.re_deV2 *= !PIXELSIZE
	init.re_exp2 *= !PIXELSIZE
	init.re_gaussian *= !PIXELSIZE
	init.re_sersic *= !PIXELSIZE
	
	final.re_deV *= !PIXELSIZE
	final.re_exp *= !PIXELSIZE
	final.re_deV2 *= !PIXELSIZE
	final.re_exp2 *= !PIXELSIZE
	final.re_gaussian *= !PIXELSIZE
	final.re_sersic *= !PIXELSIZE
endif

init.dxc = init.xc_exp - init.xc_dev
init.dyc = init.yc_exp - init.yc_dev
init.drc = sqrt(init.dxc ^ 2 + init.dyc ^ 2)

final.dxc = final.xc_exp - final.xc_dev
final.dyc = final.yc_exp - final.yc_dev
final.drc = sqrt(final.dxc ^ 2 + final.dyc ^ 2)

if (keyword_set(verbose) and abs(final.drc) ge 2.0) then print, "c", format = '(A, $)'

if (keyword_set(doarcsec)) then begin
	init.xc *= !PIXELSIZE
	init.yc *= !PIXELSIZE
	final.xc *= !PIXELSIZE
	final.yc *= !PIXELSIZE

	init.dxc *= !PIXELSIZE
	init.dyc *= !PIXELSIZE
	init.drc *= !PIXELSIZE
	final.dxc *= !PIXELSIZE
	final.dyc *= !PIXELSIZE
	final.drc *= !PIXELSIZE
endif

print, ".", format='(A, $)' 
end

function create_cat_variable, data, ncat = ncat, quants = quants, error = error
name = "create_cat_variable"
if (n_elements(data) eq 0) then begin
	print, name, ": ERROR - empty data"
	error = -1
	return, [-1]
endif
if (ncat le 0) then begin
	print, name, ": ERROR - ncat has to be positive while found ncat = ", ncat
	error = -2
	return, replicate(-1.0, n_elements(data))
endif
type = datatype(data[0], 2)
if ((type ge 6) and (type le 11)) then begin
	print, name, ": ERROR - type is not supported : '", datatype(data[0], 1), "'"
	error = -3
	return, replicate(-1.0, n_elements(data))
endif

n = n_elements(data)
cat_array = fltarr(n)

if (keyword_set(quants)) then begin

	frac = findgen(ncat + 1) / ncat
	min_data = min(data)
	max_data = max(data)
	range = [min_data, max_data]
	qt = quantile(frac, data, range = range, /nosort)
	if (qt[0] ne min_data) then begin 
		print, name, ": WARNING - placing minimum at q[0]"
		qt[0] = min_data
	endif
	if (qt[ncat] ne max_data) then begin
		print, name, ": WARNING - placing maximum at q[end]"
		qt[ncat] = max_data 
	endif
	if (keyword_set(verbose)) then begin
		print, name, ": COMMENT - variable classes = ", qt
	endif
	for icat = 0, ncat - 1L, 1L do begin
		lq = qt[icat]
		uq = qt[icat + 1L]
		if (icat lt (ncat-1L)) then $
		index = where(data ge lq and data lt uq) $
		else $
		index = where(data ge lq and data le uq)
		if (index[0] ne -1) then begin
			cat_array[index] = icat
		endif else begin
			print, name, ": WARNING - no data found for icat = ", icat, $
			", range = [ ", lq, ", ", uq, " ]"
		endelse
	endfor

endif else begin

	ndata = n_elements(data)
	data_order = sort(data)
	u1 = -1
	for icat = 0, ncat - 1L, 1L do begin
		l = u1 + 1
		u = floor(((icat + 1) * ndata) / ncat) - 1
		cat_array[data_order[l:u]] = icat
		l1 = l
		u1 = u
	endfor

endelse
return, cat_array
end


function combine_array, array1 = array1, suffix1 = suffix1, array2 = array2, suffix2 = suffix2, addid = addid
name = "combine_array"
n1 = n_elements(array1)
if (n1 eq 0) then begin
	print, name, ": ERROR - array1 is empty"
	stop
endif
if (n_elements(suffix1) eq 0) then begin
	print, name, ": ERROR - suffix1 not supplied"
	stop
endif
n2 = n_elements(array2)
if (n2 eq 0) then begin
	print, name, ": ERROR - array2 is empty"
	stop
endif
if (n_elements(suffix2) eq 0) then begin
	print, name, ": ERROR - suffix2 is not supplied"
	stop
endif
if (n1 ne n2) then begin
	print, name, ": ERROR - array1 and array2 should be of the same size, received (", n1, ", ", n2, ")"
	stop
endif

struct1 = array1[0]
struct2 = array2[0]

type1 = tag_names(struct1, /STRUCTURE_NAME)
type2 = tag_names(struct2, /STRUCTURE_NAME)
prefix="HEX"+to_hex(string(randomu(seed, /long)))
type = prefix+type1+suffix1+type2+suffix2+"_combined_by_"+name
tags1 = tag_names(struct1)
tags2 = tag_names(struct2)
tags1_added = tags1 + suffix1
tags2_added = tags2 + suffix2
tags = [tags1_added, tags2_added]

if (keyword_set(addid)) then tags = [["ID"], tags]
id_adjustment = keyword_set(addid)

ntag1 = n_elements(tags1)
ntag2 = n_elements(tags2)
ntag = ntag1 + ntag2

;; create the proper value string
values = replicate('', ntag + 1)
values[0] = '-1L'
for itag = 0, ntag - 1L, 1 do begin
	if (itag lt ntag1) then begin
		field_type = size(struct1.(itag), /type)
	endif else begin
		field_type = size(struct2.(itag - ntag1), /type)
	endelse
	value_string = ''
	case field_type of 
		1L: value_string = '1B'
		2L: value_string = '1'
		3L: value_string = '1L'
		4L: value_string = '1.0'
		5L: value_string = '1.0D'
		6L: value_string = '(1.0, 1.0)'
		7L: value_string = "'x'"
	else: value_string = ''
	endcase
	values[itag+id_adjustment] = value_string
endfor

result_arr = MRD_STRUCT(TAGS, VALUES, n1, STRUCTYP=type)	
elem = result_arr[0]

for j1 = 0, ntag1 - 1L, 1L do begin
	j = j1 + id_adjustment
	result_arr[*].(j) = array1.(j1)
endfor
for j2 = 0, ntag2 - 1L, 1L do begin
	j = j2 + ntag1 + id_adjustment
	result_arr[*].(j) = array2[*].(j2)
endfor

if (keyword_set(addid)) then begin
	id = lindgen(n1)
	result_arr.ID = id
	id = -1
endif
	
return, result_arr
end

function create_rep_struct, data = data, seed = seed, random = random, bias = bias, rnderror = rnderror, prob = prob
name = "create_rep_struct"
ndata = n_elements(data)
if (ndata eq 0) then begin
	print, name,  ": ERROR - data array is empty"
	return, -1
endif

out_struct = data[0]
tags = tag_names(out_struct)
ntags = n_elements(tags)
if (keyword_set(random)) then begin
	random_index = floor(ndata * randomu(seed))
	data_item = data[random_index]
	for i = 0, ntags - 1L, 1L do begin
		out_struct.(i) = data_item.(i)
	endfor
endif else begin
	if (ndata gt 1) then begin
		if (keyword_set(bias)) then begin
			for i = 0, ntags - 1L, 1L do begin
				out_struct.(i) = mean(data[*].(i))
			endfor
		endif
		if (keyword_set(rnderror)) then begin
			for i = 0, ntags - 1L, 1L do begin
				out_struct.(i) = sqrt(variance(data[*].(i)))
			endfor
		endif
		if (keyword_set(prob)) then begin
			for i = 0, ntags - 1L, 1L do begin
				dev = stddev(data[*].(i))
				if (dev gt 0.0) then begin
					t = abs(mean(data[*].(i))) * sqrt(ndata) / dev
					out_struct.(i) = 1.0 - 2.0 * (1.0 - t_pdf(t, ndata - 1))
				endif else begin
					out_struct.(i) = 0.0
				endelse	
			endfor
		endif				
	endif else begin
		for i = 0, ntags - 1L, 1L do begin
		out_struct.(i) = !Values.F_NAN
		endfor
	endelse
endelse

return, out_struct
end


function create_rankarray, data, innames = innames, nunique = nunique
name = "create_rankarray"
if (n_elements(data) eq 0) then begin
	print, name, ": WARNING - empty array of structures, cannot create rank array"
	nunique = -1	
	return, [-1]
endif
n = n_elements(data)
tags = tag_names(data[0])
ntags = n_elements(tags)

rank_array = dblarr(n)
rank_array[*] = 0.0
rank_base = 5.0d * (n + 5.0d)
rank_array_tag = dblarr(n)
rank_array_tag[*] = 0.0

;; print, name, ": ranking over tags, # (tags) = ", ntags

wformat = "(F20.10)"
parse_char = "$"
string_array = replicate("", n)
for itag = 0, ntags - 1L, 1 do begin	
	;; determine if tag is in innames
	which_tag = where(strlowcase(innames) eq strlowcase(tags[itag]))
	if (which_tag[0] ne -1) then begin

		data_array = float(data.(itag))
		sort_index = sort(data_array)
		rank_array_temp = ranks(data_array[sort_index])
	
		rank_array_tag[*] = 0.0
		rank_array_tag[sort_index[*]] = rank_array_temp[*]
	
		max_rank = max(rank_array_tag)
		min_rank = min(rank_array_tag)
		diff_rank = max_rank - min_rank
		if (diff_rank eq 0.0) then diff_rank = 1.0
		rank_array = rank_array + rank_base * rank_array_tag
		sort_index = sort(rank_array)
		rank_rankarray = ranks(rank_array(sort_index))
		rank_array[sort_index[*]] = rank_rankarray[*]
	endif
endfor

sort_index = sort(rank_array)
rank_rankarray_temp = float(ranks(rank_array[sort_index]))
rank_rankarray = fltarr(n)
rank_rankarray[sort_index[*]] = rank_rankarray_temp[*]

return, rank_rankarray
end

function create_rep_data, data = data, seed = seed, random = random, bias = bias, rnderror  = rnderror, prob = prob
ndata = n_elements(data)
if (keyword_set(random)) then begin
	random_index = floor(ndata * randomu(seed))
	rep_data = data[random_index]
endif else begin
	if (ndata gt 1) then begin
		if (keyword_set(bias)) then rep_data = mean(data)
		if (keyword_set(rnderror)) then rep_data = sqrt(variance(data))
		if (keyword_set(prob)) then begin
			dev = sqrt(variance(data))
			mu = mean(data)
			t = abs(mu / dev) * sqrt(ndata)
			if (dev gt 0.0) then begin
				rep_data = 1.0 - 2.0 * (1.0 - T_PDF(t, ndata - 1)) 
			endif else begin
				rep_data = 0.0
			endelse
		endif 
	endif else begin
			rep_data = !Values.F_NAN
	endelse
endelse
return, rep_data
end

function collapse_array_with_index, data = data, collapse_index = collapse_index, seed = seed, $
	random = random, bias = bias, rnderror = rnderror, prob = prob


nunique = -1
unique_ranks = collapse_index[UNIQ(collapse_index, SORT(collapse_index))]
unique_ranks = unique_ranks[sort(unique_ranks)]
nunique = n_elements(unique_ranks)
if (nunique le 0) then begin
	print, name, ": ERROR - rank array created from data seems to have ", nunique, "unique elements"
	stop
endif

outdata = replicate(data[0], nunique)
for irank = 0, nunique - 1L, 1L do begin
	rank = unique_ranks[irank]
	nwithrank = 0
	index = where(collapse_index eq rank, nwithrank)
	class_array = data[index]
	rep_data = create_rep_data(data = class_array, seed = seed, $
	random = keyword_set(random), bias = keyword_set(bias), rnderror = keyword_set(rnderror), prob = keyword_set(prob))
	class_array = -1
	outdata[irank] = rep_data
endfor

return, outdata
end


function collapse_array, data = data, innames = innames, outnames = outnames, $
	random = random, bias = bias, rnderror = rnderror, prob = prob, $
	collapse_index = collapse_index, seed = seed
name = "collapse_array"
ndata = n_elements(data)
if (ndata eq 0) then begin
	print, name, ": ERROR - data array is empty"
	stop
endif
n_in = n_elements(innames)
if (n_in eq 0) then begin
	print, name, ": ERROR - 'innames' array not supplied"
	stop
endif
n_out = n_elements(outnames)
if (n_out eq 0)then begin
	print, name, ": ERROR - 'outnames' array not supplied"
	stop
endif

instruct = data[0]
tags1 = tag_names(instruct)

;; check that in-tags exist
;; print, name, ": tags1 = ", tags1
innames_found = []
for i = 0, n_in - 1L, 1L do begin
	intag = innames[i]
	nfound = 0
	index = where(STRLOWCASE(tags1) eq STRLOWCASE(intag), nfound)
	if (nfound eq 0) then begin
		print, name, ": WARNING - inname = ", intag, " does not exist in the data structure"
		;; stop
	endif else begin
		innames_found = [innames_found, [intag]]
	endelse
endfor
innames = innames_found
;; check that out-tags exist
;; print, name, ": tags2 = ", tags2
outnames_found = []
for i = 0, n_out - 1L, 1L do begin
	outtag = outnames[i]
	nfound = 0
	index = where(STRLOWCASE(tags1) eq STRLOWCASE(outtag), nfound)
	if (index[0] eq -1) then begin
		print, name, " # list of tags = ", strlowcase(tags1)
		print, name, ": WARNING - outname = '", outtag, "' does not exist in the data structure"
		;; stop
	endif else begin
		outnames_found = [outnames_found, [outtag]]
	endelse
endfor
outnames = outnames_found
;; creating and output structure
alltags = [innames, outnames]
nX = n_elements(alltags)
alltags = alltags(UNIQ(alltags, SORT(alltags)))
nY = n_elements(alltags)
if (nX gt nY) then begin
	print, name, ": WARNING - 'innames' and 'outnames' share some elements"
endif else if (nY gt nX) then begin
	print, name, ": ERROR - number of joined names is bigger than innames and outnames"
	stop
endif
ntags = n_elements(alltags)
;; creating the outdata
outstruct = struct_selecttags(instruct, select_tags = alltags)
usefuldata = replicate(outstruct, ndata)
data_tags = tag_names(instruct)
out_tags = tag_names(outstruct)
inj = lindgen(ntags)
outj = lindgen(ntags)
for j = 0, ntags - 1L, 1L do begin
	;; inj[j] = (where(data_tags eq outnames[j]))[0]
	this_tag = strlowcase(alltags[j])
	inj[j] = (where(strlowcase(data_tags) eq this_tag))[0]
	outj[j] = (where(strlowcase(out_tags) eq this_tag))[0]
endfor

bad_tags = where(inj eq -1)
if (bad_tags[0] ne -1) then begin
	print, name, ":# $of tags not found among data tags = ", n_elements(bad_tags)
	print, name, ": tags not found in data tags = ", alltags[bad_tags]
endif
for j = 0, ntags - 1L, 1L do begin
	j1 = inj[j]
	j2 = outj[j]
	if (j1 ne -1 and j2 ne -1) then begin
		usefuldata.(j2) = data.(j1)
	endif

endfor
;; debug
;; help, usefuldata, /stru

nunique = -1
rank_array = create_rankarray(usefuldata, innames = innames)
unique_ranks = rank_array[UNIQ(rank_array, SORT(rank_array))]
unique_ranks = unique_ranks[sort(unique_ranks)]
nunique = n_elements(unique_ranks)
if (nunique le 0) then begin
	print, name, ": ERROR - rank array created from data seems to have ", nunique, "unique elements"
	stop
endif

outdata = replicate(usefuldata[0], nunique)
for irank = 0, nunique - 1L, 1L do begin
	rank = unique_ranks[irank]
	nwithrank = 0
	index = where(rank_array eq rank, nwithrank)
	class_array = usefuldata[index]
	rep_struct = create_rep_struct(data = class_array, seed = seed, $
	random = keyword_set(random), bias = keyword_set(bias), rnderror = keyword_set(rnderror), prob = keyword_set(prob)) 
	class_array = -1
	outdata[irank] = rep_struct
endfor
class_array = -1

collapse_factor = n_elements(usefuldata) / (float(n_elements(outdata)))
if (keyword_set(random)) then begin
	print, name, ": COMMENT - collapsed input array by random selection from ", n_elements(usefuldata), " to ", n_elements(outdata), ", collapse factor = ", collapse_factor
endif else begin
	print, name, ": COMMENT - collapsed input array without random selection from ", n_elements(usefuldata), ", to ", n_elements(outdata), ", collapse factor = ", collapse_factor
endelse

collapse_index = rank_array
return, outdata
end

pro read_simpars_array_from_fits, filename = filename, rnames = rnames, siminfo = siminfo, $
	docorrect = docorrect, doarcsec = doarcsec, error = error

name = "read_simpars_array_from_fits"
; making sure the output array is set
if (n_elements(siminfo) eq 0) then begin
	print, name, " ERROR - output 'simlist' should be set"
	stop
endif
if (arg_present(siminfo) eq 0) then begin
	print, name, " ERROR - output 'simlist should be passed by reference"
	stop
endif

; This macro finds  the parameters in a particular simulation parameter in a simulation log
table_header='N/A'
header=['10']
if (not file_test(filename)) then begin
	print, name, ": ERROR - could not find simpar array fits file: ", filename	
	siminfo = []
	error = -1
	return
endif
fitsinfo = mrdfits(filename, 1, header, /silent)
if (n_elements(fitsinfo) eq 0) then begin
	print, name, ": ERROR - empty array read from FITS file"
	error = -2
	return
endif
if (keyword_set(verbose)) then print, name, ": structure of the array read from file '", filename, "'"
if (keyword_set(verbose)) then begin
print, name, ": COMMENT - (fitsinfo) phi1 ", median(fitsinfo.phi1), min(fitsinfo.phi1), max(fitsinfo.phi1)
print, name, ": COMMENT - (fitsinfo) phi2 ", median(fitsinfo.phi2), min(fitsinfo.phi2), max(fitsinfo.phi2)
print, name, ": file = ", filename
help, fitsinfo, /stru
endif

if (tag_exist(fitsinfo[0], "phi1")) then fitsinfo[*].phi1 = 90.0 - fitsinfo[*].phi1
if (tag_exist(fitsinfo[0], "phi2")) then fitsinfo[*].phi2 = 90.0 - fitsinfo[*].phi2

;; information about the tags and the array
n = n_elements(fitsinfo)
m = n_tags(fitsinfo[0])
;; header is an array of strings containing the tags
n_added = 0
;; print, name, ": descriptor = ", descriptor
;; create_struct, simitem0, "SIMITEMY", header, descriptor
simitem0 = fitsinfo[0]
all_tagnames = ['COUNTS', 'COUNTS1', 'COUNTS2', $ 
		'MAG', 'MAG1', 'MAG2', $
		'DXC', 'DYC', 'DRC', $
		'CAT', 'CAT_LUM', $
		'CAT_COUNTS', 'CAT_COUNTS1', 'CAT_COUNTS2', $
		'CAT_MAG', 'CAT_MAG1', 'CAT_MAG2', $
		'CAT_RE', 'CAT_RE1', 'CAT_RE2', $
		'CAT_E', 'CAT_E1', 'CAT_E2']

tags0 = tag_names(simitem0)
ntag = 0
for i = 0, n_elements(all_tagnames) - 1, 1 do begin
	tag = all_tagnames[i]
	counts = 0
	tagindex = where(strlowcase(tags0) eq strlowcase(tag), counts)
	if (counts eq 0) then begin
		if (ntag eq 0) then tagnames = [tag] else tagnames = [tagnames, tag]
		ntag++
	endif	
endfor
if (ntag ne 0) then begin
	values = replicate('0.0', n_elements(tagnames))
	add_tags, simitem0, tagnames, values, simitem
endif
if (ntag eq 0) then begin
	init = simitem0
	final = simitem0
endif



if (tag_exist(simitem0, "MAG1") eq 0b) then begin
	tagnames = ['MAG1', 'MAG2', 'MAG', 'COUNTS', 'DXC', 'DYC', 'DRC']
	values = replicate('0.0', n_elements(tagnames))
	add_tags, simitem0, tagnames, values, simitem1
	n_added += n_elements(tagnames)
	if (keyword_set(verbose)) then print, name, ": adding 'mag' tags to structure - ", tagnames
	
endif else begin
	simitem1 = simitem0
endelse
;adding category variables which are startfied version of luminosity, re, e
if (tag_exist(simitem1, 'CAT') eq 0b) then begin
	tagnames = ['CAT', 'CAT_LUM', $
		'CAT_COUNTS', 'CAT_COUNTS1', 'CAT_COUNTS2', $
		'CAT_MAG', 'CAT_MAG1', 'CAT_MAG2', $
		'CAT_RE', 'CAT_RE1', 'CAT_RE2', $
		'CAT_E', 'CAT_E1', 'CAT_E2']
	values = replicate ('0.0', n_elements(tagnames))
	add_tags, simitem1, tagnames, values, simitem2
	n_added += n_elements(tagnames)
	if (keyword_set(verbose)) then print, name, ": adding 'cat' tags to structure - ", tagnames
endif else begin
	simitem2 = simitem1
endelse
if (tag_exist(simitem2, 'SNRATIO') eq 0b) then begin
	tagnames = ['SNRATIO']
	values = replicate ('0.0', n_elements(tagnames))
	add_tags, simitem2, tagnames, values, simitem3
	n_added += n_elements(tagnames)
	if (keyword_set(verbose)) then print, name, ": adding 'S/N' tags to structure - ", tagnames
endif else begin
	simitem3 = simitem2
endelse

copy_tag_from_xc_yc = 0
if (tag_exist(simitem3, "XC1") eq 0b) then begin
	tagnames = ['XC1', 'YC1', 'XC2', 'YC2']
	if (tag_exist(simitem3, "XC")) then begin
		xc = simitem3.xc
		yc = simitem3.yc
		print, name, ": adding 'xc1, yc1, xc2, yc2' tags to structure from 'xc, yc' tags - ", tagnames
		copy_tag_from_xc_yc = 1
	endif else begin
		;; default values used in simulations (center of the field)
		nrow=1489
		ncol=2048
		xc = 1.0 * (nrow / 2) + 0.5
		yc = 1.0 * (ncol / 2) + 0.5
		copy_tag_from_xc_yc = 0
		print, name, ": adding 'xc1, yc1, xc2, yc2' tags to structure from mid FOV - ", tagnames
	endelse
	xc_str= strtrim(string(xc, format = "(F10.4)"), 2)
	yc_str = strtrim(string(yc, format = "(F10.4)"), 2)
	values = [xc_str, yc_str, xc_str, yc_str]
	add_tags, simitem3, tagnames, values, simitem4
	n_added += n_elements(tagnames)
endif else begin
	simitem4 = simitem3
endelse

if (tag_exist(simitem4, "RE1") eq 0b) then begin
	tagnames = ['RE1', 'E1', 'PHI1', 'RE2', 'E2', 'PHI2']

	;; default values used in simulations (center of the field)
	re1 = 0.0001
	e1 = 0.0
	phi1 = 0.0
	re2 = 0.0
	e2 = 0.0
	phi2 = 0.0
	re1_str= strtrim(string(re1, format = "(F15.9)"), 2)
	e1_str = strtrim(string(e1, format = "(F15.9)"), 2)
	phi1_str = strtrim(string(phi1, format = "(F15.9)"), 2)
	re2_str= strtrim(string(re2, format = "(F15.9)"), 2)
	e2_str = strtrim(string(e2, format = "(F15.9)"), 2)
	phi2_str = strtrim(string(phi2, format = "(F15.9)"), 2)
	values = [re1_str, e1_str, phi1_str, re2_str, e2_str, phi2_str]
	add_tags, simitem4, tagnames, values, simitem5
	n_added += n_elements(tagnames)
	if (keyword_set(verbose)) then print, name, ": adding 'mag' tags to structure - ", tagnames
	
endif else begin
	simitem5 = simitem4
endelse

if (tag_exist(simitem5, "MODEL2") eq 0b) then begin
	tagnames = ['MODEL2']

	;; default values used in simulations (center of the field)
	model2=""
	model2_str = "''"
	values = [model2_str]
	add_tags, simitem5, tagnames, values, simitem
	n_added += n_elements(tagnames)
	if (keyword_set(verbose)) then print, name, ": adding 'mag' tags to structure - ", tagnames
	
endif else begin
	simitem = simitem5
endelse



if (keyword_set(verbose)) then print, name, ": # of tags added = ", n_added
siminfo=replicate(simitem, n);
index = lindgen(n)
for i = 0, m-1, 1 do begin
siminfo[index].(i) = fitsinfo[index].(i)
endfor
if (tag_exist(siminfo[0], "counts1")) then begin
	if (tag_exist(siminfo[0], "counts2")) then begin
		siminfo[index].counts = siminfo[index].counts1 + siminfo[index].counts2
	endif else begin
		siminfo[index].counts = siminfo[index].counts1
	endelse
endif

if (copy_tag_from_xc_yc) then begin
	siminfo[index].xc1 = siminfo[index].xc
	siminfo[index].xc2 = siminfo[index].xc
	siminfo[index].yc1 = siminfo[index].yc
	siminfo[index].yc2 = siminfo[index].yc
endif

if (keyword_set(docorrect)) then begin
	print, name, ": no counts correction is needed in the runs after oct 13, 2013"
	if (1 eq 0) then begin
		;; added on Sep 13, 2013
		DEV_MCOUNT = 22.6652331786
		EXP_MCOUNT = 11.9484947415

		print, name, ": correcting simulation counts"
		deVindex = where(siminfo.modelname1 eq 'deV')
		if (deVindex[0] ne -1) then begin
			siminfo[deVindex].counts1 *= DEV_MCOUNT
		endif
		deVindex = where(siminfo.modelname2 eq 'deV')
		if (deVindex[0] ne -1) then begin
			siminfo[deVindex].counts2 *= DEV_MCOUNT
		endif
		deVindex = -1
		expindex = where(siminfo.modelname1 eq 'exp')
		if (expindex[0] ne -1) then begin
			siminfo[expindex].counts1 *= EXP_MCOUNT
		endif
		expindex = where(siminfo.modelname2 eq 'exp')
		if (expindex[0] ne -1) then begin
				siminfo[expindex].counts2 *= EXP_MCOUNT
		endif
		expindex=-1
	endif
end
if (keyword_set(doarcsec)) then begin
	if (tag_exist(siminfo[0], "re1") and tag_exist(siminfo[0], "re2")) then begin
		siminfo[*].re1 *= !PIXELSIZE
		siminfo[*].re2 *= !PIXELSIZE
	endif else begin
		print, name, ": WARNING - keywords 're1' and 're2' not found in siminfo read from csv file"
	endelse
endif

;; add signal to noise ratio
if (tag_exist(siminfo[0], "re1")) then siminfo.SNratio = siminfo.counts / (sqrt(siminfo.sky1) * 2 * !PI * siminfo.re1 ^ 2)

if (keyword_set(verbose)) then print, name, ": structure read. # of elements = ", n_elements(siminfo)
if (keyword_set(verbose)) then help, siminfo, /stru
if (tag_exist(siminfo[0], "phi1") and keyword_set(verbose)) then $
	print, name, ": COMMENT - (sim) phi1: ", median(siminfo.phi1), min(siminfo.phi1), max(siminfo.phi1)
if (tag_exist(siminfo[0], "phi2") and keyword_set(verbose)) then $
	print, name, ": COMMENT - (sim) phi2: ", median(siminfo.phi2), min(siminfo.phi2), max(siminfo.phi2)

print, name, ": xc1 = ", min(siminfo[*].xc1), max(siminfo[*].xc1), median(siminfo[*].xc1)
print, name, ": yc1 = ", min(siminfo[*].yc1), max(siminfo[*].yc1), median(siminfo[*].yc1)

end



pro read_simpars_array, filename = filename, rnames = rnames, siminfo = siminfo, docorrect = docorrect, doarcsec = doarcsec
name = "read_simpars_array"
; making sure the output array is set
if (n_elements(siminfo) eq 0) then begin
	print, name, " ERROR - output 'simlist' should be set"
	stop
endif
if (arg_present(siminfo) eq 0) then begin
	print, name, " ERROR - output 'simlist should be passed by reference"
	stop
endif

; This macro finds  the parameters in a particular simulation parameter in a simulation log
table_header='N/A'
header=['10']
csvinfo = read_csv(filename, header=header, n_table_header=0, table_header = table_header)
if (n_elements(csvinfo) eq 0) then begin
	print, name, ": ERROR - empty array read from CSV file"
	stop
endif
if (keyword_set(verbose)) then print, name, ": structure of the array read from file '", filename, "'"
;; help, csvinfo, /stru
if (keyword_set(verbose)) then begin 
print, name, ": header       = ", header
print, name, ": table header = ", table_header
endif
;; information about the tags and the array
n = n_elements(csvinfo.(0))
m = n_elements(header)
;; making field descriptor
for i = 0, m-1, 1 do begin
	type = datatype(csvinfo.(i), 1)
	case type of
	'String': tchar = "A"
	'Byte': tchar = "L"
	'Integer': tchar = "I"
	'Long': tchar = "J"
	'Double': tchar = "F"
	'Float': tchar = "D"
	'Complex': tchar = "C"
	'DComplex': tchar = "M"
	else: stop
	endcase
	if (i gt 0) then begin
		descriptor=descriptor+","+tchar
	endif else begin
		descriptor=tchar
	endelse
endfor
;; header is an array of strings containing the tags
n_added = 0
print, name, ": descriptor = ", descriptor
create_struct, simitem0, "SIMITEMY", header, descriptor

all_tagnames = ['COUNTS', 'COUNTS1', 'COUNTS2', $ 
		'MAG', 'MAG1', 'MAG2', $
		'CAT', 'CAT_LUM', $
		'CAT_COUNTS', 'CAT_COUNTS1', 'CAT_COUNTS2', $
		'CAT_MAG', 'CAT_MAG1', 'CAT_MAG2', $
		'CAT_RE', 'CAT_RE1', 'CAT_RE2', $
		'CAT_E', 'CAT_E1', 'CAT_E2']

tags0 = tag_names(simitem0)
ntag = 0
for i = 0, n_elements(all_tagnames) - 1, 1 do begin
	tag = all_tagnames[i]
	counts = 0
	tagindex = where(strlowcase(tags0) eq strlowcase(tag), counts)
	if (counts eq 0) then begin
		if (ntag eq 0) then tagnames = [tag] else tagnames = [tagnames, tag]
		ntag++
	endif	
endfor
if (ntag ne 0) then begin
	values = replicate('0.0', n_elements(tagnames))
	add_tags, simitem0, tagnames, values, simitem
endif
if (ntag eq 0) then begin
	init = simitem0
	final = simitem0
endif



if (tag_exist(simitem0, "MAG1") eq 0b) then begin
	tagnames = ['MAG1', 'MAG2', 'MAG', 'COUNTS']
	values = replicate('0.0', n_elements(tagnames))
	add_tags, simitem0, tagnames, values, simitem1
	n_added += n_elements(tagnames)
	if (keyword_set(verbose)) then print, name, ": adding 'mag' tags to structure - ", tagnames
	
endif else begin
	simitem1 = simitem0
endelse
;adding category variables which are startfied version of luminosity, re, e
if (tag_exist(simitem1, 'CAT') eq 0b) then begin
	tagnames = ['CAT', 'CAT_LUM', $
		'CAT_COUNTS', 'CAT_COUNTS1', 'CAT_COUNTS2', $
		'CAT_MAG', 'CAT_MAG1', 'CAT_MAG2', $
		'CAT_RE', 'CAT_RE1', 'CAT_RE2', $
		'CAT_E', 'CAT_E1', 'CAT_E2']
	values = replicate ('0.0', n_elements(tagnames))
	add_tags, simitem1, tagnames, values, simitem2
	n_added += n_elements(tagnames)
	if (keyword_set(verbose)) then print, name, ": adding 'cat' tags to structure - ", tagnames
endif else begin
	simitem2 = simitem1
endelse
if (tag_exist(simitem1, 'SNRATIO') eq 0b) then begin
	tagnames = ['SNRATIO']
	values = replicate ('0.0', n_elements(tagnames))
	add_tags, simitem2, tagnames, values, simitem
	n_added += n_elements(tagnames)
	if (keyword_set(verbose)) then print, name, ": adding 'S/N' tags to structure - ", tagnames
endif else begin
	simitem = simitem2
endelse

if (keyword_set(verbose)) then print, name, ": # of tags added = ", n_added
;; help, simitem, /stru
siminfo=replicate(simitem, n);
index = lindgen(n)
for i = 0, m-1, 1 do begin
siminfo[index].(i) = csvinfo.(i)[index]
endfor
siminfo[index].counts = siminfo[index].counts1 + siminfo[index].counts2

if (keyword_set(docorrect)) then begin

	if (1 eq 1) then begin
		print, name, ": no counts correction is needed in the runs after oct 13, 2013"
	endif else begin
		;; added on Sep 13, 2013
		DEV_MCOUNT = 22.6652331786
		EXP_MCOUNT = 11.9484947415

		print, name, ": correcting simulation counts"
		deVindex = where(siminfo.modelname1 eq 'deV')
		if (deVindex[0] ne -1) then begin
			siminfo[deVindex].counts1 *= DEV_MCOUNT
		endif
		deVindex = where(siminfo.modelname2 eq 'deV')
		if (deVindex[0] ne -1) then begin
			siminfo[deVindex].counts2 *= DEV_MCOUNT
		endif
		deVindex = -1
		expindex = where(siminfo.modelname1 eq 'exp')
		if (expindex[0] ne -1) then begin
			siminfo[expindex].counts1 *= EXP_MCOUNT
		endif
		expindex = where(siminfo.modelname2 eq 'exp')
		if (expindex[0] ne -1) then begin
				siminfo[expindex].counts2 *= EXP_MCOUNT
		endif
		expindex=-1
	endelse
end
if (keyword_set(doarcsec)) then begin
	if (tag_exist(siminfo[0], "re1") and tag_exist(siminfo[0], "re2")) then begin
		siminfo[*].re1 *= !PIXELSIZE
		siminfo[*].re2 *= !PIXELSIZE
	endif else begin
		print, name, ": ERROR - keywords 're1' and 're2' not found in siminfo read from csv file"
		return
	endelse
endif

;; add signal to noise ratio
siminfo.SNratio = siminfo.counts / (sqrt(siminfo.sky1) * 2 * !PI * siminfo.re1 ^ 2)

print, name, ": structure read. # of elements = ", n_elements(siminfo)
;; help, siminfo, /stru
end

function index_sim_in_array, simarray = simarray, run = run, Lfield = Lfield, Ufield = Ufield, target_model = target_model, source_model = source_model, exists = exists, nfound = nfound

name = "index_sim_in_array"
if (n_elements(Ufield) ne 1) then Ufield=5000
if (n_elements(Lfield) ne 1) then Lfield=1
if (n_elements(run) ne 1) then run = LONG(GETENV('DEFAULT_SDSS_RUN'))
if (n_elements(source_model) eq 0) then source_model = "exp"


switch source_model of
	"deV-exp": begin
		model1 = "deV"
		model2 = "exp"
		break
		end
	"exp": begin
		model1 = source_model
		model2 = ""
		break
		end 
	"deV": begin 
		model1 = source_model
		model2 = ""
		break
		end
	"exp-deV": begin
		model1 = "exp"
		model2 = "deV"
		break;
		end
	"exp-exp": begin
		model1 = "exp"
		model2 = "exp"
		break
		end
	"deV-deV": begin
		model1 = "deV"
		model2 = "deV"
		break
		end
	"star": begin
		model1 = source_model
		model2 = ""
		break
		end
	else: begin
		print, name, ": ERROR - unsupported source model - ", source_model
		stop
		end
endswitch

if (keyword_set(verbose)) then begin
print, name, ": COMMENT - Lfield = ", Lfield, ", Ufield = ", Ufield, ", run = ", run, ", model1 = ", model1, ", model2 = ", model2
;; help, simarray, /stru
endif
if (strcmp(strtrim(model2, 2), "") and (not tag_exist(simarray[0], "model2"))) then begin

	if (tag_exist(simarray[0], 'field') and tag_exist(simarray[0], 'run') and $
	tag_exist(simarray[0], "model1")) then begin

		index = where(simarray.field ge Lfield and simarray.field le Ufield and simarray.run eq run and $
		strtrim(simarray.model1, 2) eq model1)

	endif else begin

		print, name, ": ERROR - could not find appropriate tags in the siminfo, problematic read from csv file"
		help, simarray, /stru
		index = [-1]

	endelse

endif else begin

	if (tag_exist(simarray[0], 'field') and tag_exist(simarray[0], 'run') and $
	tag_exist(simarray[0], "model1") and tag_exist(simarray[0], "model2")) then begin

		index = where(simarray.field ge Lfield and simarray.field le Ufield and simarray.run eq run and $
		strtrim(simarray.model1, 2) eq model1 and strtrim(simarray.model2, 2) eq model2)

	endif else begin

		print, name, ": ERROR - could not find appropriate tags in the siminfo, problematic read from csv file"
		help, simarray, /stru
		index = [-1]

	endelse

endelse

if (index[0] eq -1) then begin
	print, name, ": WARNING - no simulation found with info: run = ", run, $
		", Lfield = ", Lfield, ", Ufield = ", Ufield, $
		", source_model = ", source_model, ", target_model = ", target_model
	print, name, ": total # of simulations = ", n_elements(simarray)
	exists = 0
	nfound = 0
	return, index
endif
exists = 1
nfound = n_elements(index)
return, index
end

pro startify, x, n = n, cat = cat, average = average, med = med, min = min, max = max, s = s
name = "stratify"

m = n_elements(x)
if (m eq 0) then begin
	print, name, ": ERROR - cannot stratify an empty array"
	return
endif
;; adjusting the number of categories
if ((n_elements(n) eq 0) or (n le 0)) then begin
	n = 5
	print, name, ": WARNING - setting number of categories to default value = ", n
endif
z = uniq(x, sort(x))
if (n_elements(z) gt n) then begin
	n = n_elements(z)
	print, name, ": WARNING - number of categories too large, setting back to", n
endif
z = -1

if (n_elements(cat) ne m) then begin
	print, name, ": ERROR - category variable should have the same size as the data"
	return
endif
if ((n_elements(average) ne m) and (n_elements(average) ne 0)) then begin 
	print, name, ": ERROR - if provided, average variable should have the same size as the data"
	return
endif
if ((n_elements(med) ne 0) and (n_elements(med) ne 0)) then begin
	print, name, ": ERROR - if provided, median variable should have the same size as the data"
	return
endif
if ((n_elements(min) ne m) and (n_elements(min) ne 0)) then begin
	print, name, ": ERROR - if provided, min variable should have the same size as the data"
	return
endif
if ((n_elements(max) ne m) and (n_elements(max) ne 0)) then begin
	print, name, ": ERROR - if provided, max variable should have the same size as the data"
	return
endif

class_sample = {particular_category, cat: 0L, average: 0.0D, med: 0.0D, min: 0.0D, max: 0.0D, s: 0.0D, p: 0.0D}

class = replicate(class_sample, n)
fracs = findgen(n + 1) * 1.0 / n
levels = quantile(fracs, x, /nosort)
;; finding the percentiles
for i = 0, n - 1, 1 do begin
	class1 = class[i]
	class1.cat = i + 1
	class1.min = levels[i]
	class1.max = levels[i + 1]
	class1.p = fracs[i + 1]
	if (i lt n - 1) then gndex = where((x ge class1.min) and (x lt class1.max)) else $
		gndex = where((x ge class1.min) and (x le class1.max))

	class1.average = mean(x[gndex])
	class1.s = stddev(x[gndex])
	class1.median = median(x[gndex])

	cat[gndex] = class1.cat
	if (n_elements(min) ne 0) then	min[gndex] = class1.min
	if (n_elements(max) ne 0) then max[gndex] = class1.max
	if (n_elements(average) ne 0) then average[gndex] = class1.average
	if (n_elements(med) ne 0) then med[gndex] = class1.med
	if (n_elements(s) ne 0) then s[gndex] = class1.s

endfor

print, name, ": class information after stratification"
for i = 0, n - 1, 1 do begin
	if (i eq 0) then begin
		print_struct, class[i]
	endif else begin
	 	print_struct, class[i], /no_title
	endelse
endfor
class = -1

return
end

pro stratify_target, y, med = med, cat = cat
name = "stratify_target"
m = n_elements(y)
if (m eq 0) then begin
	print, name, ": ERROR - empty input and output array (y)"
	return
endif
if (n_elements(cat) ne m) then begin
	print, name, ": ERROR - category variable should have the same size as the data"
	return
endif
class = uniq(cat, sort(cat))
n = n_elements(class)
for i = 0, n - 1, 1 do begin
	mycat = class[i]
	gndex = where(cat eq mycat)
	if (keyword_set(med)) then center = median(y[gndex]) else center = mean(y[gindex])
	y[gndex] = center
endfor

return
end



pro stratify_simarray, simarray = simarray, magcat = magcat, nmag = nmag, $
	countscat = countscat, ncounts = ncounts, $
	recat = recat, nre = nre, $
	ellcat = ellcat, nell = nell

name = "stratify_simarray"
n = n_elements(simarray)
simindex = lindgen(n)
if (n eq 0) then begin
	print, name, ": ERROR - empty simulation array"
	return
endif
if (keyword_set(magcat)) then begin
	startify, simarray.mag1, n = nmag, cat = simarray.lumcat, average = simarray.lumcat_av, min = simarray.lumcat_min, max = simarray.lumcat_max, s = simarray.lumcat_s
endif
if (keyword_set(countscat)) then begin
	startify, simarray.counts1, n = ncounts, cat = simarray.lumcat, average = simarray.lumcat_av, min = simarray.lumcat_min, max = simarray.lumcat_max, s = simarray.lumcat_s
endif
if (keyword_set(recat)) then begin
	startify, simarray.re1, n = nre, cat = simarray.recat, average = simarray.recat_av, min = simarray.recat_min, max = simarray.recat_max, s = simarray.recat_s
endif
if (keyword_set(ellcat)) then begin
	startify, simarray.e1, n = nell, cat = simarray.ellcat, average = simarray.ellcat_av, min = simarray.ellcat_min, max = simarray.ellcat_max, s = simarray.ellcat_s
endif

return
end

; This should generate the list of the files
pro create_gpobjc_filename, root = root, run = run, rerun = rerun, field = field, band = band, camcol = camcol, $
	source_model = source_model, target_model = target_model, psf_cutoff = psf_cutoff, $
	fit2fpCC = fit2fpCC, domultiple = domultiple, gpobjc = gpobjc, lm = lm
;; note: camcol should be passes as string or character to this procedure
;; name = "create_gpobjc_filename"
fieldstr=string(field, format = "(I4.4)")
runstr=string(run, format = "(I6.6)")
rundir=strtrim(string(run), 2)
camcolstr=strtrim(string(camcol), 2)
multiplicity="-single"
if (keyword_set(domultiple)) then multiplicity="-multiple"
fpCCstr = "-fpC"
if (keyword_set(fit2fpCC)) then fpCCstr = "-fpCC"

dir=root+"/"+source_model+"/"+psf_cutoff+"/"+target_model+fpCCstr+multiplicity+"/"+rerun+"/"+rundir+"/"+camcolstr+"/"+"objc"
gpobjc=dir+"/"+"gpObjc-"+runstr+"-"+band+camcolstr+"-"+fieldstr+".fit"
lm=dir+"/"+"lM-"+runstr+"-"+band+camcolstr+"-"+fieldstr+".fit"
end


pro create_gpobjc_files, root = root, run = run, rerun = rerun, field = field, band = band, camcol = camcol, $
	source_model = source_model, target_model = target_model, psf_cutoff = psf_cutoff, $
	fit2fpCC = fit2fpCC, domultiple = domultiple, gpobjcs = gpobjcs, lms = lms


for iroot = 0, n_elements(root) - 1L, 1 do begin
for isource = 0, n_elements(source_model) - 1L, 1 do begin
for itarget = 0, n_elements(target_model) - 1L, 1 do begin

	gpobjc = ""
	lm = ""
	create_gpobjc_filename(root = root[iroot], run = run, rerun = rerun, field = field, band = band, camcol = camcol, $
	source_model = source_model[isource], target_model = target_model[itarget], psf_cutoff = psf_cutoff, $
	fit2fpCC = keyword_set(fit2fpCC), domultiple = keyword_set(domultiple), gpobjc = gpobjc, lm = lm)
	if (n_elements(lm_array) gt 0) then begin
		lm_array = [lm_array, lm]
	endif else begin
		lm_array = [lm]
	endelse
	if (n_elements(gpobjc_array) gt 0) then begin
		gp_array = [gp_array, gpobjc]
	endif else begin
		gp_array = [gpobjc]
	endelse

endfor
endfor
endfor

gpobjcs = gp_array
lms = lm_array

end

pro enforce_range, array, range = range, error = error
name = "enforce_range"
if (n_elements(array) eq 0) then begin 
	print, name, ": ERROR - a data array should be passed"
	error = -1
	return
endif
if (n_elements(range) ne 2) then begin
	print, name, ": ERROR - a range array of size 2 should be passed"
	error = -2
	return
endif

lower = min(range)
upper = max(range)
nupper = 0
upper_index = where(array gt upper, nupper)
if (nupper ne 0) then begin
	print, name, ": fixing upper limits"
	array[upper_index] = upper
endif
nlower = 0
lower_index = where(array lt lower, nlower)
if (nlower ne 0) then begin
	print, name, ": fixing lower limits"
	array[lower_index] = lower
endif

error = 0
end


function get_tick_format, range1 = range1, range2 = range2, error = error
name = "get_tick_format"
if (n_elements(range1) eq 0 and n_elements(range2) eq 0) then begin
	print, name, ": ERROR - some range should be passed"
	error = -1
	return, ""
endif

tick_format = !my_tick_format_large
if (n_elements(range1) ne 0) then begin
	max_tick = abs(max(range1) - min(range1))
	if (max_tick ge 1.0E4) then tick_format = !my_tick_format_very_large
	if (max_tick lt 1.0E4) then tick_format = !my_tick_format_large	
	if (max_tick lt 2.0) then tick_format = !my_tick_format_small
	if (max_tick lt 1.0) then tick_format = !my_tick_format_very_small
endif
if (n_elements(range2) ne 0) then begin
	max_tick = abs(max(range2) - min(range2))
	if (max_tick ge 1.0E4) then tick_format = !my_tick_format_very_large
	if (max_tick lt 1.0E4) then tick_format = !my_tick_format_large	
	if (max_tick lt 2.0) then tick_format = !my_tick_format_small
	if (max_tick lt 1.0) then tick_format = !my_tick_format_very_small
endif

return, tick_format
end


function get_range, array, frange = frange, fixdata = fixdata
name = "get_range"

npix = n_elements(array)
work_array0 = reform(array, [npix])
finite_index = where(finite(work_array0))
work_array = work_array0[finite_index]

work_array = work_array[sort(work_array)]
low = min(work_array)
high = max(work_array)
range=[low, high]
if (n_elements(frange) ne 0) then begin
	print, name, ": WARNING - cutting range to frac = ", frange
	frange = frange[UNIQ(frange, sort(frange))]
	frange = frange[sort(frange)]
	qs = quantile(frange, work_array, range = range, /nosort)
	range = qs
endif else begin
	outlier_scale = 1.5
	if (keyword_set(verbose)) then print, name, ": cutting outliers off"
	frange = [0.25, 0.75]
	qs = quantile(frange, work_array, range = range, /nosort)
	iqr = qs[1] - qs[0]
	upper = qs[1] + outlier_scale * iqr
	lower = qs[0] - outlier_scale * iqr
	noutlier = 0
	outlier_index = where(work_array gt upper or work_array lt lower, noutlier)
	if (noutlier ne 0 and keyword_set(verbose)) then $
	print, name, ": WARNING - # of outliers found = ", noutlier, " (out of ", npix, ", fraction = ", (noutlier * 1.0 / npix), ")"
	ninlier = 0
	inlier_index = where(work_array le upper and work_array ge lower, ninlier)
	if (ninlier gt 0) then begin
		max_value = max(work_array[inlier_index])
		min_value = min(work_array[inlier_index])
		range = [min_value, max_value]
	endif else begin
		print, name, ": ERROR - (n_inlier = 0) for quartiles = ", qs
	endelse	
endelse
upper = range[1]
lower = range[0]

if (keyword_set(fixdata)) then begin
	nupper = 0
	upper_index = where(array gt upper, nupper)
	if (nupper ne 0) then begin
		if (keyword_set(verbose)) then print, name, ": WARNING - fixing upper outliers (", nupper, ")"
		array[upper_index] = upper
	endif
	nlower = 0
	lower_index = where(array lt lower, nlower)
	if (nlower ne 0) then begin
		if (keyword_set(verbose)) then print, name, ": WARNING - fixing lower outliers (", nlower, ")"
		array[lower_index] = lower
	endif
endif

work_array = [-1]
return, range
end

function adjust_array_to_range, data, range = range, error = error

name = "adjust_array_to_range"

if (n_elements(data) eq 0) then begin
	print, name, ": ERROR - non-empty array should be passed"
	error = -1
	return, [-1]
endif
if (n_elements(range) eq 0) then begin
	print, name, ": ERROR - non-empty range should be passed"
	error = -1
	return, [-1]
endif

max_range = max(range)
min_range = min(range)

nsmall = 0
nlarge = 0

small_index = where(data lt min_range, nsmall)
large_index = where(data gt max_range, nlarge)

pdata = data
if (nsmall gt 0) then pdata[small_index] = min_range
if (nlarge gt 0) then pdata[large_index] = max_range

return, pdata
end

pro fix_range, data, range = range, error = error
name = "fix_range"
error = 0
if (n_elements(range) ne 2) then begin
	print, name, ": ERROR - range should be passed as an array with two elements"
	error = -1
	return
endif
if (range[0] eq range[1]) then begin
	print, name, ": WARNING - range has width zero"
endif
max_range = max(range)
min_range = min(range)
upcount = 0
lowcount = 0
up_index = where(data gt max_range, upcount)
low_index = where(data lt min_range, lowcount)
if (upcount ne 0) then data[up_index] = max_range
if (lowcount ne 0) then data[low_index] = min_range
return
end

pro calibrate_sim, sim_arr = sim_arr, calib_info = calib_info, error = error, verbose = verbose
name = "calibrate_sim"

if (n_elements(calib_info) ne n_elements(sim_arr)) then begin
	print, name, ": ERROR - number of elements in simulation information and calibratio info arrays should be the same, #(calib_info) = ", $
		n_elements(calib_info), ", #(sim_arr) = ", n_elements(sim_arr)
	error = -1
	return
endif
if (n_elements(calib_info) eq 0) then begin
	print, name, ": ERROR - no calibration information provided"
	error = -2
	return
endif

A = 1.0E8 * calib_info[*].flux20;

ff0_1 = sim_arr[*].counts1 / A[*]
if (tag_exist(sim_arr[0], "counts2")) then ff0_2 = sim_arr[*].counts2 / A[*] else ff0_2 = 0.0 * ff0_1
ff0 = sim_arr[*].counts / A[*]

B = 2.0 * calib_info[*].bfilter

x1 = ff0_1[*] / B[*]
x2 = ff0_2[*] / B[*]
x = ff0[*] / B[*]

C = 2.5 / alog(10.0);
D = alog(calib_info[*].bfilter);

sim_arr[*].mag1 = -C*(asinh(x1[*]) +D[*]); 
sim_arr[*].mag2 = -C*(asinh(x2[*])+ D[*]);
sim_arr[*].mag = -C*(asinh(x[*])+ D[*]);

if (keyword_set(verbose)) then begin
	print, name, ": FLUX20 = ", calib_info[0].flux20, ", B = ", calib_info[0].bfilter
	print, name, ": A = ", A[0], ", B = ", B[0], ", C = ", C[0], ", D = ", D[0]
endif

x1 = -1
x2 = -1
ff0_1 = -1
ff0_2 = -1
ff0 = -1
A = -1
B = -1
C = -1
D = -1

;; sim_arr[*].phi1 = 0.0
;; sim_arr[*].phi2 = 0.0

error = 0
end

pro calibrate_mle, mle_arr = mle_arr, calib_info = calib_info, error = error, all = all, verbose = verbose
name = "calibrate_mle"

if (n_elements(calib_info) ne n_elements(mle_arr)) then begin
	print, name, ": ERROR - number of elements in simulation information and calibratio info arrays should be the same, #(calib_info) = ", $
		n_elements(calib_info), ", #(mle_arr) = ", n_elements(mle_arr)
	error = -1
	return
endif
if (n_elements(calib_info) eq 0) then begin
	print, name, ": ERROR - no calibration information provided"
	error = -2
	return
endif

A = 1.0E8 * calib_info[*].flux20;
B = 2.0 * calib_info[*].bfilter
C = 2.5 / alog(10.0);
D = alog(calib_info[*].bfilter);

if (keyword_set(all)) then begin
	ff0_dev = mle_arr[*].counts_dev / A[*]
	ff0_exp = mle_arr[*].counts_exp / A[*]
	ff0_dev2 = mle_arr[*].counts_dev2 / A[*]
	ff0_exp2 = mle_arr[*].counts_exp2 / A[*]
	ff0_gaussian = mle_arr[*].counts_gaussian / A[*]
	ff0_sersic = mle_arr[*].counts_sersic / A[*]

	x_dev = ff0_dev[*] / B[*]
	x_exp = ff0_exp[*] / B[*]
	x_dev2 = ff0_dev2[*] / B[*]
	x_exp2 = ff0_exp2[*] / B[*]
	x_gaussian = ff0_gaussian[*] / B[*]
	x_sersic = ff0_sersic[*] / B[*]

	mle_arr[*].mag_dev = -C*(asinh(x_dev[*]) +D[*]); 
	mle_arr[*].mag_exp = -C*(asinh(x_exp[*])+ D[*]);
	mle_arr[*].mag_dev2 = -C*(asinh(x_dev2[*]) +D[*]); 
	mle_arr[*].mag_exp2 = -C*(asinh(x_exp2[*])+ D[*]);
	mle_arr[*].mag_gaussian = -C*(asinh(x_gaussian[*]) +D[*]); 
	mle_arr[*].mag_sersic = -C*(asinh(x_sersic[*])+ D[*]);
endif

ff0 = mle_arr[*].counts / A[*]
x = ff0[*] / B[*]
mle_arr[*].mag = -C*(asinh(x[*])+ D[*]);

if (keyword_set(verbose)) then begin 
	print, name, ": FLUX20 = ", calib_info[0].flux20, ", B = ", calib_info[0].bfilter
	print, name, ": A = ", A[0], ", B = ", B[0], ", C = ", C[0], ", D = ", D[0]
endif

x1 = -1
x2 = -1
ff0_1 = -1
ff0_2 = -1
ff0 = -1
A = -1
B = -1
C = -1
D = -1

error = 0
end



function where_cat, data, catname = catname, catvalue = catvalue
name = "where_cat"
index = [-1]
if (n_elements(catname) ne 1) then begin
	print, name, ": ERROR - category name should be passed along"
	return, index
endif
if (n_elements(catvalue) ne 1) then begin
	print, name, ": ERROR - category value should be passed along"
	return, index
endif
if (n_elements(data) eq 0) then begin
	print, name, ": WARNING - empty array passed no search can be done"
	return, index
endif

tags = strlowcase(tag_names(data[0]))
pos = where(tags eq strlowcase(catname))
if (pos[0] eq -1) then begin
	print, name, ": ERROR - data structure does not contain a tag name '", catname, "'"
	help, data, /stru
	return, index
endif
index = where(data[*].(pos) eq catvalue)
if (index[0] eq -1) then begin
	print, name, ": WARNING - no array element of catname = '", catname, "', and catvalue = ", catvalue, " was found"
endif

return, index
end

function add_cat_to_filename, file, catname = catname, catvalue = catvalue
name = "add_cat_to_filename"
if (n_elements(file) eq 0) then begin
	print, name, ": ERROR - 'file' string should not be empty"
	return, -1
endif
if (n_elements(catname) eq 0) then begin
	print, name, ": ERROR - 'catname' string should not be empty"
	return, -1
endif
if (strlen(catname) eq 0) then begin
	print, name, ": ERROR - 'catname' should not be an empty string"
	return, -1
endif
if (n_elements(catvalue) eq 0) then begin
	print, name, ": ERROR - 'catvalue' string should not be empty"
	return, -1
endif
if (strlen(catvalue) eq 0) then begin
	print, name, ": ERROR - 'catvalue' string should not be empty"
	return, -1
endif

pos=strpos(file, '.', /REVERSE_SEARCH)
file_no_extension = file
extension = ""
if (pos ne -1 and pos ne 0) then begin
	len = strlen(file)
	len_extension = len - pos
	file_no_extension= strmid(file, 0, pos)
	extension = strmid(file, pos, len_extension)
endif
if (pos eq 0) then begin
	file_no_extension = ""
	extension = file
endif

catfile = file_no_extension+"-"+catname+"-"+string(catvalue, FORMAT='(I2.2)')+extension
return, catfile
end

function create_error_filename, file, uname = uname, dopositive = dopositive
name = "add_cat_to_filename"
if (n_elements(file) eq 0) then begin
	print, name, ": ERROR - 'file' string should not be empty"
	return, -1
endif
if (n_elements(uname) eq 0) then begin
	print, name, ": ERROR - 'uname' string should not be empty"
	return, -1
endif
if (strlen(uname) eq 0) then begin
	print, name, ": ERROR - 'uname' should not be an empty string"
	return, -1
endif

pos=strpos(file, '.', /REVERSE_SEARCH)
file_no_extension = file
extension = ""
if (pos ne -1 and pos ne 0) then begin
	len = strlen(file)
	len_extension = len - pos
	file_no_extension= strmid(file, 0, pos)
	extension = strmid(file, pos, len_extension)
endif
if (pos eq 0) then begin
	file_no_extension = ""
	extension = file
endif

if (keyword_set(dopositive)) then error_indic = "-UE-" else error_indic = "-E-"
efile = file_no_extension+error_indic+uname+extension
return, efile
end


function create_log_filename, file, dographicallog = dographicallog, dotxt = dotxt
name = "create_log_filename"
if (n_elements(file) eq 0) then begin
	print, name, ": ERROR - 'file' string should not be empty"
	return, -1
endif


pos=strpos(file, '.', /REVERSE_SEARCH)
file_no_extension = file
extension = ""

if (pos ne -1 and pos ne 0) then begin
	len = strlen(file)
	len_extension = len - pos
	file_no_extension= strmid(file, 0, pos)
	extension = strmid(file, pos, len_extension)
endif
if (pos eq 0) then begin
	file_no_extension = ""
	extension = file
endif

log_extension = ".log"
if (keyword_set(dographicallog)) then log_extension = "-log.eps"
if (keyword_set(dotxt)) then log_extension = ".txt"

logfile = file_no_extension+log_extension

return, logfile
end

pro output_log, file, addcomment = addcomment, stat_catrecord = stat_catrecord, $
		draw_x = draw_x, draw_y = draw_y, smooth_x = smooth_x, smooth_y = smooth_y, $
		xrange = xrange, yrange = yrange, xtitle = xtitle, ytitle = ytitle, xlog = xlog, ylog = ylog, $
		name1 = name1, name2 = name2, row1 = row1, row2 = row2, smooth_row1 = smooth_row1, smooth_row2 = smooth_row2, $
		dographicallog = dographicallog, error = error
		
name = "output_log"
error = 0

if (n_elements(draw_x) ne n_elements(draw_y)) then begin
	print, name, ": ERROR - size of draw_x and draw_y arrays should be the same"
	error = -1
	return
endif
if (n_elements(smooth_x) ne n_elements(smooth_y)) then begin
	print, name, ": ERROR - size of smooth_x and smooth_y arrays should be the same"
	error = -1
	return
endif
if (n_elements(row1) ne n_elements(row2)) then begin
	print, name, ": ERROR - size of row1 and row2 arrays should be the same"
	error = -1	
	return
endif
if (n_elements(smooth_row1) ne n_elements(smooth_row2)) then begin
	print, name, ": ERROR - size of smooth_row1 and smooth_row2 arrays should be the same"
	error = -1
	return
endif

doxlog = keyword_set(xlog)
doylog = keyword_set(ylog)

if (n_elements(row1) eq 0) then mark_draw_points = 0 else mark_draw_points = 1
if (n_elements(smooth_row1) eq 0) then mark_smooth_points = 0 else mark_smooth_points = 1
if (n_elements(name1) eq 0) then name1 = 'row1'
if (n_elements(name2) eq 0) then name2 = 'row2'
if (n_elements(xtitle) eq 0) then xtitle = 'x'
if (n_elements(ytitle) eq 0) then ytitle = 'y'

if (mark_draw_points) then begin
	rho = r_correlate(row1, row2)
	tau = r_correlate(row1, row2, /kendall)
	df = n_elements(row1)
endif
if (mark_smooth_points) then begin
	smooth_rho = r_correlate(smooth_row1, smooth_row2)
	smooth_tau = r_correlate(smooth_row1, smooth_row2, /kendall)
	smooth_df = n_elements(smooth_row1)
endif

logfile = create_log_filename(file, dographicallog = keyword_set(dographicallog))

;; extracting statistics
catrecord = stat_catrecord.name
n_catrecord = stat_catrecord.n
max_catrecord = stat_catrecord.max
min_catrecord = stat_catrecord.min
median_catrecord = stat_catrecord.med
s_catrecord = stat_catrecord.std_dev

comments = replicate("", 100)
;; information on categorical variable
i = 0
if (n_elements(addcomment) eq 1) then comments[i++] = addcomment
comments[i++] = "CATEGORICAL VARIABLE = '"+catrecord+"'"
comments[i++] = "number of unique category variables = "+strtrim(n_catrecord, 2)	
comments[i++] = "max     = "+string(max_catrecord, format=fformat)
comments[i++] = "min     = "+string(min_catrecord, format=fformat)
comments[i++] = "median  = "+string(median_catrecord, format=fformat)
comments[i++] = "std dev = "+string(s_catrecord, format=fformat)

comments[i++] = "DATA AND SMOOTHING"
comments[i++] = "# of data points         = " + strtrim(n_elements(draw_x), 2)
comments[i++] = "# of interpolated points = " + strtrim(n_elements(smooth_x), 2)

if (mark_draw_points) then begin
	comments[i++] = "RANK CORRELATIONS between row1 and row2"
	comments[i++] = "Spearman rho: "+strtrim(string(rho[0], format = fformat), 2)+$
		" (p  = "+strtrim(string(rho[1], format=fformat), 2)+")"
	comments[i++] = "Kendall  tau: "+strtrim(string(tau[0], format = fformat), 2)+ $
		" (df = "+strtrim(df, 2)+")"
endif
if (mark_smooth_points) then begin
	comments[i++] = "RANK CORRELATIONS between row1 (smooth) and row2 (smooth)"
	comments[i++] = "Spearman rho: "+strtrim(string(rho[0], format = fformat), 2)+$
		" (p  = "+strtrim(string(smooth_rho[1], format=fformat), 2)+")"
	comments[i++] = "Kendall  tau: "+strtrim(string(smooth_tau[0], format = fformat), 2)+ $
		" (df = "+strtrim(smooth_df, 2)+")"
endif
n_comments = i - 1L
comments = comments[0:i-1L]

;; print, name, ": outputting to logfile = '", logfile, "'"

if (keyword_set(dographicallog)) then begin

	ct = 0 ;; color table
	decomposed = 0 ;; color decomposition
	n1 = 3
	n2 = 1
	aspect_ratio=(1.0 * n1) / n2
	xsize= !my_xsize * n1 / 2.0 ;; 45
	ysize= !my_ysize * n2 / 2.0 ;; 35
	symsize=1.0 / 20.0 * xsize
	gap = !my_gap
	
	tfmt = get_tick_format(range1 = xrange, range2 = yrange)
	print, name, ": writing to file = ", logfile
	
	olddevice = !d.name ; store the current device name in a variable
	set_plot, 'PS' ; switch to the postscript device
	Device, DECOMPOSED=decomposed, COLOR=1, BITS_PER_PIXEL=8
	device, /encapsulated, file=logfile, font_size = !my_font_size ;; specify some details, give the file a name
	loadct, ct, /silent

	;; size of the region
	ymargin_size = (!Y.Margin[0] + !Y.Margin[1]) * !D.Y_CH_SIZE/ !D.Y_PX_CM 
	my_xgap = !my_xgap / (n1)
	my_ygap = !my_ygap / (n2)
	xsize = !my_xsize
	ysize = (!my_xsize * (1.0 - (n1) * my_xgap) * n2) / (n1 * !my_xyratio) + !my_lygap * (n2) + ymargin_size
	my_ygap = !my_lygap / ysize

	if (keyword_set(verbose)) then $
	print, name, ": setting up graphic device, xsize = ", xsize, ", ysize = ", ysize, ", [n1, n2] = ", n1, n2
	device, xsize=xsize, ysize=ysize
	multiplot, /reset
	multiplot, [n1, n2], xgap = my_xgap, ygap = my_ygap, $
	mTitSize = !my_font_size, mxTitSize = !my_font_size, myTitSize = !my_font_size, $
	myTitOffset = !myTitOffset, mxTitOffset = !mxTitOffset	

	xtfmt = get_tick_format(range1 = xrange)
	ytfmt = get_tick_format(range1 = yrange)

	if (mark_draw_points) then begin
		
		if (keyword_set(verbose)) then print, name, ": marking the points (1)"
		get_contour_position, position = position, leg_position = leg_position
		title="database: available points("+strtrim(n_elements(draw_x), 2)+")"
		xstyle = !my_xstyle
		ystyle = !my_ystyle
		plot, draw_x, draw_y, xrange = xrange, yrange = yrange, $
			xlog = doxlog, ylog = doylog, $
			title = title, font = font, $
			xtickformat = xtfmt, ytickformat = ytfmt, xtitle = xtitle, ytitle = ytitle, $
		 	position = position, xstyle = xstyle, ystyle = ystyle, /nodata
		xstyle += 4
		ystyle += 4
		plot, draw_x, draw_y, psym = 5, symsize = 1.0, $
			xlog = doxlog, ylog = doylog, $
			xstyle = xstyle, ystyle = ystyle, xrange = xrange, yrange = yrange, $
			position = position
		multiplot
	endif

	if (mark_smooth_points) then begin
		if (keyword_set(verbose)) then print, name, ": marking the points (2)"
		get_contour_position, position = position, leg_position = leg_position
		title="interpolation: available points("+strtrim(n_elements(smooth_x_grid), 2)+")"
		xstyle = !my_xstyle
		ystyle = !my_ystyle
		plot, smooth_x, smooth_y, xrange = xrange, yrange = yrange, $
			xlog = doxlog, ylog = doylog, $
		 	title = title, font = font, $
			xtickformat = xtfmt, ytickformat = ytfmt, xtitle = xtitle, ytitle = ytitle, $
		 	position = position, xstyle = 1, ystyle = 1, /nodata
		xstyle += 4
		ystyle += 4
		plot, smooth_x, smooth_y, psym = 5, symsize = 0.02, $
			xlog = doxlog, ylog = doylog, $
			xstyle = 5, ystyle = 5, xrange = xrange, yrange = pyrange, $
			position = position
		multiplot
	endif

	my_charsize = 1.0
	my_title_size = 3.0
	plot, [1], [1], xrange = [0.0, 100.0], yrange = [0.0, 100.0], xstyle = 4, ystyle = 4, charsize = my_title_size
	y0 = 95.0
	ydelta = -7.5
	for i = 0, n_comments - 1, 1 do begin
		yposition = y0 + ydelta * i 
		xyouts, 0.0, yposition, comments[i], charsize = my_charsize
	endfor

	if (keyword_set(verbose)) then print, name, ": closing device"
	device, /close ; close the file
	set_plot, olddevice ; go back to the graphics device
	command = "epstopdf "+logfile
	spawn, command
	
endif else begin

	get_lun, ufile
	openw, ufile, logfile
	printf, ufile, comments, format = "(A)"
	close, ufile
	free_lun, ufile

endelse

txtfile = create_log_filename(file, /dotxt)
print, name, ": outputting contour plot to txt file '", txtfile, "'"

get_lun, ufile
openw, ufile, txtfile
output_data = [draw_x, draw_y, row1, row2]

n = min([n_elements(draw_x), n_elements(draw_y), n_elements(row1), n_elements(row2)])
if (keyword_set(verbose)) then print, name, ": outputting n = ", n, " elements to the file"
printf, ufile, [xtitle, ytitle, name1, name2], format = '(4(A, :, "; "))'
for i = 0L, n - 1L, 1L do begin
	printf, ufile, [draw_x[i], draw_y[i], row1[i], row2[i]], format='(4(F10.2, :, "; "))' 
endfor
close, ufile
free_lun, ufile

;; print, name, ": log written to '", logfile, "'"
return
end

function extract_cat_stat, catname = catname, catvalue = catvalue, icat = icat, cat_records = catrecords, error = error
name = "extract_cat_stat"
error = 0
if (n_elements(catrecords) eq 0) then begin
	print, name, ": ERROR - no list to derive statistics"
	error = -1
	return, -1
endif
if (n_elements(catname) eq 0) then catname = "* UNKNOWN *"
if (n_elements(catvalue) eq 0) then catvalue = 1.0 / 0.0
if (n_elements(icat) eq 0) then icat = -1L

cat_stat = {name: "", value: 0.0, index: 0, min: 0.0, max: 0.0, med: 0.0, var: 0.0, std_dev: 0.0, n: 0L}
cat_stat.name = catname	
cat_stat.value = catvalue
cat_stat.index = icat
cat_stat.min = min(catrecords)
cat_stat.max = max(catrecords)
cat_stat.med = median(catrecords)
cat_stat.var = variance(catrecords)
cat_stat.std_dev = sqrt(cat_stat.var)
cat_stat.n = n_elements(UNIQ(catrecords, SORT(catrecords)))

return, cat_stat
end

pro do_chisq_contour_plots, file = file, source_model = source_model, target_model = target_model, $
	sim_arr = sim_arr, sdss_arr = sdss_arr, mle_arr = mle_arr, $
	n_level = n_level, nbin1 = nbin1, nbin2 = nbin2, $
	domag = domag, doarcsec = doarcsec, doell = doell, $
	catell = catell, catre = catre, catlum = catlum, ncat = ncat, $
	fit2fpCC = fit2fpCC, comment_string = comment_string, $
	seed = seed, random = random, bias = bias, rnderror = rnderror, prob = prob, $
	docolor = docolor, doraw = doraw, dosmooth = dosmooth, dohisto = dohisto, doshisto = doshisto, $
	force_nstep_max = force_nstep_max, dolog = dolog, dographicallog = dographicallog
name = "do_chisq_contour_plots"
 
min_number_points_for_plot = 10

if (n_elements(sim_arr) eq 0 or n_elements(sdss_arr) eq 0 or n_elements(mle_arr) eq 0) then begin
	print, name, ": ERROR - null input"
	return
endif
if (n_elements(sim_arr) ne n_elements(sdss_arr) or n_elements(sim_arr) ne n_elements(mle_arr)) then begin
	print, name, ": ERROR - SDSS, MLE and SIM arrays should be of the same size"
	return
endif
if (n_elements(file) eq 0) then begin
	print, name, ": ERROR - null filename - no export can be done"
	return
endif
if (n_elements(n_level) eq 0) then n_level = 10
if (n_elements(nbin1) eq 0) then nbin1 = 50
if (n_elements(nbin2) eq 0) then nbin2 = 50

if (keyword_set(random) + keyword_set(bias) + keyword_set(rnderror) + keyword_set(prob) ne 1) then begin
	print, name, ": ERROR - one of the keywords random, bias, rnderror, prob, should be set"
	return
endif
if (keyword_set(rnderror) + keyword_set(prob)) then begin
	rnderror = 0
	prob = 0
	bias = 1
	print, name, ": WARNING - working with the average CHISQ and NSTEP (bias) keyword"
endif

;; counting the categorization keywords set
cat_keyword_num = 0
cat_keyword_num += keyword_set(catell)
cat_keyword_num += keyword_set(catre)
cat_keyword_num += keyword_set(catlum)
if (cat_keyword_num ne 1) then begin
	print, name, ": ERROR - expecting only 1 categorization keyword to be set while ", cat_keyword_num, " was set"
	stop
endif
;; deciding which plots to make
dolum = 1 - keyword_set(catlum)
dore = 1 - keyword_set(catre)
doell = 1 - keyword_set(catell)

;; combining structures all together
print, name, ": combining input arrays into one structure"
print, name, ": # (sim_arr) = ", n_elements(sim_arr)
print, name, ": # (mle_arr) = ", n_elements(mle_arr)
print, name, ": # (sdss_arr)= ", n_elements(sdss_arr)
print, name, ": info on chisq (mle_arr ) : min = ", min(mle_arr.chisq), ", max = ", max(mle_arr.chisq)
print, name, ": info on chisq (sdss_arr) : min = ", min(sdss_arr.chisq), ", max = ", max(sdss_arr.chisq)

comb_array1 = combine_array(array1 = sim_arr, suffix1 = "_sim", array2 = mle_arr, suffix2 = "_mle")
print, name, ": # (sim_arr ^ mle_arr)    = ", n_elements(comb_array1)
rawdata = combine_array(array1 = comb_array1, suffix1 = "", array2 = sdss_arr, suffix2 = "_sdss")
print, name, ": # (comb_arr1 ^ sdss_arr) = ", n_elements(rawdata)
comb_array1 = -1

error = 0
if (keyword_set(catell)) then begin
	print, name, ": creating cat variable: 'cat_e1_sim'"
	rawdata.cat_e1_sim = create_cat_variable(rawdata.e1_sim, ncat = ncat, error = error)
	inrecords = ["counts1_sim", "counts2_sim", "cat_e1_sim", "re1_sim", "re2_sim"]
	outrecords = ["mag1_sim", "mag2_sim", "e1_sim", "e2_sim", "chisq_mle", "nstep_mle", "chisq_sdss", "snratio_sim"]
	catrecord = "e1_sim"
endif
if (keyword_set(catre)) then begin
	print, name, ": creating cat variable: 'cat_re_sim'"
	rawdata.cat_re1_sim = create_cat_variable(rawdata.re1_sim, ncat = ncat, error = error)
	inrecords = ["counts1_sim", "counts2_sim", "e1_sim", "e2_sim", "cat_re1_sim"]
	outrecords = ["mag1_sim", "mag2_sim", "re1_sim", "re2_sim", "chisq_mle", "nstep_mle", "chisq_sdss", "snratio_sim"]
	catrecord = "re1_sim"
endif
if (keyword_set(catlum)) then begin	
	if (keyword_set(domag)) then begin
		print, name, ": creating cat variable: 'cat_mag_sim'"
		rawdata.cat_mag1_sim = create_cat_variable(rawdata.mag1_sim, ncat = ncat, error = error)
		inrecords = ["cat_mag1_sim", "e1_sim", "e2_sim", "re1_sim", "re2_sim"] 
		outrecords = ["counts1_sim", "counts2_sim", "mag1_sim", "mag2_sim", "chisq_mle", "nstep_mle", "chisq_sdss", "snratio_sim"]
		catrecord = "mag1_sim"	
	endif else  begin
		print, name, ": creating cat variable: 'cat_counts_sim'"
		rawdata.cat_counts1_sim = create_cat_variable(rawdata.counts1_sim, ncat = ncat, error = error)
		inrecords = ["cat_counts1_sim", "e1_sim", "e2_sim", "re1_sim", "re2_sim"]
		outrecords = ["counts1_sim", "counts2_sim", "mag1_sim", "mag2_sim", "chisq_mle", "nstep_mle", "chisq_sdss", "snratio_sim"]
		catrecord = "counts1_sim"
	endelse
endif
if (error ne 0) then begin
	print, name, ": ERROR - problem categorizing '", catrecord, "'"
	return
endif

;; the following command collapses the array of structures on a certain number of its records
if (keyword_set(verbose)) then begin
	print, name, ": COMMENT - collapsing the combined array"
	print, name, ": COMMENT - # (raw data) = ", n_elements(rawdata)
endif
data = collapse_array(data = rawdata, innames = inrecords, outnames = outrecords, seed = seed, $
	random = keyword_set(random), bias = keyword_set(bias), rnderror = keyword_set(rnderror), prob = keyword_set(prob))
if (keyword_set(verbose)) then begin
	print, name, ": COMMENT - # (data)     = ", n_elements(data)
endif
rawdata = -1


;; the ranges should be that of the whole simulations including the good fits so that 
;; this test seems to fail
if (keyword_set(verbose)) then begin

print, name, ": COMMENT - testing collapsed array"
catname = "cat_"+catrecord
my_tagnames = tag_names(data[0])
rcat = where(strlowcase(my_tagnames) eq strlowcase(catname))
rrcat = where(strlowcase(my_tagnames) eq strlowcase(catrecord))
for icat = 1, ncat - 1L, 1L do begin
	catindex = where(data.(rcat) eq float(icat))
	if (catindex[0] ne -1) then begin
		min_catrecord = min(data[catindex].(rrcat))
		max_catrecord = max(data[catindex].(rrcat))
		print, name, ": catvalue = ", icat, ", catrecord = '", catrecord, $
			"', range = [", min_catrecord, ", ", max_catrecord, "]"
	endif
endfor

endif

;; the bad fit scatter plot shows how much of the phases space is not reachable
;; help, sim_arr, /stru

mag_sim = data.mag1_sim
re_sim = data.re1_sim
lgre_sim = alog10(re_sim)
lgre_sim = re_sim ;; doing log by the plot command itself
counts_sim = data.counts1_sim
lgcounts_sim = alog10(counts_sim)
lgcounts_sim = counts_sim ;; doing log by the plot command itself
e_sim = data.e1_sim


;; debugging
mag_range = get_range(mag_sim)
counts_range = get_range(counts_sim)
lgcounts_range = get_range(lgcounts_sim)
re_range = get_range(re_sim)
lgre_range = get_range(lgre_sim)
e_range = get_range(e_sim)

chisq_sdss = data.chisq_sdss
chisq_mle = data.chisq_mle
nstep_mle = data.nstep_mle
snratio_sim = data.snratio_sim

n1 = 0
if (keyword_set(doraw)) then n1++
if (keyword_set(dosmooth)) then n1++
if (keyword_set(dohisto)) then n1++
if (keyword_set(doshisto)) then n1++
if (keyword_set(allcats_in_one_file)) then n1 = ncat * n1
n2 = 3 ;2 (it used to be 2 when SN ratio was not plotted ) 

aspect_ratio=(1.0 * n1) / n2
xsize= !my_xsize * n1 / 2.0 ;; 45
ysize= !my_ysize * n2 / 2.0 ;; 35
symsize=1.0 / 20.0 * xsize
gap = !my_gap

if (keyword_set(verbose)) then begin

print, name, ": choosing valid chisq values for plotting"
print, name, ": # chisq(MLE) = ", n_elements(chisq_mle)
print, name, ": # chisq(SDSS)= ", n_elements(chisq_sdss)

print, name, ": input chisq MLE  : min = ", min(chisq_mle), ", max = ", max(chisq_mle)
print, name, ": input chisq SDSS : min = ", min(chisq_sdss), ", max = ", max(chisq_sdss)

endif

badchisq = where(chisq_sdss le 0.0 or chisq_mle le 0.0)
goodchisq = where(chisq_sdss gt 0.0 and chisq_mle gt 0.0)

if (n_elements(goodchisq) le 10) then begin
	print, name, ": ERROR - all or most of the chisq values are bad"
	return
endif else begin
	print, name, ": # of bad chisq values = ", n_elements(chisq_sdss) - n_elements(goodchisq)
endelse

if (goodchisq[0] eq -1) then begin
	print, name, ": ERROR - no good chisq value was found"
	print, name, ": info about chisq arrays"
	print, name, ": #(chisq_sdss) = ", n_elements(chisq_sdss)
	print, name, ": #(chisq_mle)  = ", n_elements(chisq_mle)
	return
endif 
;;restraiing to good values
lgcounts_sim = lgcounts_sim[goodchisq]
mag_sim = mag_sim[goodchisq]
lgre_sim = lgre_sim[goodchisq]
e_sim = e_sim[goodchisq]
nstep_mle = nstep_mle[goodchisq]
snratio_sim = snratio_sim[goodchisq]

data = data[goodchisq] ;; data should be screened too
;; fixing max_nstep
if (n_elements(force_nstep_max) eq 1) then begin
	print, name, ": forcing nstep max = ", force_nstep_max
	nstep_max_index = where(nstep_mle ge force_nstep_max)
	if (nstep_max_index[0] ne -1) then nstep_mle[nstep_max_index] = force_nstep_max
endif

;; due to the large number of sky pixels, chisq value is much larger and dominated by poisson noise in sky pixels. the absolute difference and not the ratio is a better indicator of improvement in the fit
chisq_diff = chisq_mle[goodchisq] - chisq_sdss[goodchisq]
chisq_mle = alog10(chisq_mle[goodchisq])
chisq_sdss = alog10(chisq_sdss[goodchisq])


;; getting ranges for plots
;; calculating the range and adopting a symmetric one
print, name, ": compiling ranges"
chisq_diff_range = get_range(chisq_diff)
nstep_mle_range = get_range(nstep_mle)		
snratio_sim_range = get_range(snratio_sim)

chisq_diff_color_range = [-50.0, 50.0]
nstep_mle_color_range = 1.0E0 * [-max(abs(nstep_mle_range)), max(abs(nstep_mle_range))]
snratio_sim_color_range = [-1.0, 1.0]

if (keyword_set(docolor)) then begin
	old_chisq_diff_range = chisq_diff_range
	new_max_range = max(abs(chisq_diff_range))
	chisq_diff_range = [-new_max_range, new_max_range]

	old_nstep_mle_range = nstep_mle_range
	new_max_range = max(abs(nstep_mle_range))
	nstep_mle_range = [-new_max_range, new_max_range]

	old_snratio_sim_range = snratio_sim_range
	new_max_range = max(abs(snratio_sim_range))
	snratio_sim_range = [-new_max_range, new_max_range]

	print, name, ": using color pallet"
	ct = 16
endif else begin
	print, name, ": using greyscale"
	ct = 0
endelse
 decomposed = 0 ;; color decomposition

my_fracs = findgen(n_level+1) / (n_level)
;; getting levels from function get_contour_levels
if (keyword_set(verbose)) then begin
print, name, ": invoking 'get_contour_levels' function"
print, name, ": color levels for chisq_diff"
endif
chisq_diff_levels = get_contour_levels(chisq_diff, range = chisq_diff_color_range, n_level = n_level)
if (keyword_set(verbose)) then begin
print, name, ": COMMENT - chisq_diff_levels = ", chisq_diff_levels[0: min([5, n_elements(chisq_diff_levels) - 1])]
print, name, ": color levels for nstep_mle"
endif
nstep_mle_levels = get_contour_levels(nstep_mle, range = nstep_mle_color_range, n_level = n_level)
if (keyword_set(verbose)) then begin
print, name, ": COMMENT - nstep_mle_levels  = ", nstep_mle_levels[0: min([5, n_elements(nstep_mle_levels) - 1])]
endif
snratio_sim_levels = get_contour_levels(snratio_sim, range = snratio_sim_color_range, n_level = n_level)
if (keyword_set(verbose)) then begin
print, name, ": COMMENT - snratio_sim_levels = ", snratio_sim_levels[0: min([5, n_elements(snratio_sim_levels) - 1])]
endif

smooth_chisq_diff_range = chisq_diff_range
smooth_nstep_mle_range = smooth_nstep_mle_range
smooth_snratio_sim_range = smooth_snratio_sim_range

;; printing the ranges
if (keyword_set(verbose)) then begin
	print, name, ": chisq diff plotting range = ", chisq_diff_range
	print, name, ": nstep      plotting range = ", nstep_mle_range
endif

if (keyword_set(catell)) then begin
	catname = "cat_e1_sim"
	print, name, ": cat-name = ", catname
	;; setting the x-axis
	print, name, ": x-axis shows brightness"
	if (keyword_set(domag)) then begin
		x = mag_sim
		doxlog = 0
		xrange = get_range(mag_sim)
		xtitle = "MAG (maggies)"
	endif else begin
		x = lgcounts_sim
		doxlog = 1
		xrange = get_range(counts_sim)
		xtitle = "COUNTS (photons)"
	endelse
	;; setting the y-axis
	print, name, ": y-axis shows size"
	y = lgre_sim
	yrange = get_range(re_sim)
	if (keyword_set(doarcsec)) then begin
		ytitle = "RE (arcsec)"
	endif else begin
		ytitle = "RE (pixel)"
	endelse
	doylog = 1
endif
if (keyword_set(catre)) then begin
	catname = "cat_re1_sim"
	print, name, ": cat-name = ", catname
	;; setting the x-axis
	print, name, ": x-axis shows ellipticity"
	x = e_sim
	xrange = get_range(e_sim)
	xtitle = "ellipticity"
	doxlog = 0
	;; setting the y-axis	
	print, name, ": y-axis shows brightness"
	if (keyword_set(domag)) then begin
		y = mag_sim
		doylog = 0
		yrange = get_range(mag_sim)
		ytitle = "MAG (maggies)"
	endif else begin
		y = lgcounts_sim
		doylog = 1
		yrange = get_range(counts_sim)
		ytitle = "COUNTS (photons)"
	endelse
endif
if (keyword_set(catlum)) then begin
	if (keyword_set(domag)) then catname = "cat_mag1_sim" else catname = "cat_counts1_sim"
	print, name, ": cat-name = ", catname
	;; setting the x-axis
	print, name, ": x-axis shows ellipticity"
	x = e_sim
	xrange = get_range(e_sim)
	xtitle = "ellipticity"
	doxlog = 0
	;; setting the y-axis
	print, name, ": y-axis shows size"
	y = lgre_sim
	yrange = get_range(re_sim)
	if (keyword_set(doarcsec)) then begin
		ytitle = "RE (arcsec)"
	endif else begin
		ytitle = "RE (pixel)"
	endelse
	doylog = 1
endif

;; now loop over cats
tags = tag_names(data[0])
rcat = where(strlowcase(tags) eq strlowcase(catname))
if (rcat lt 0) then begin
	print, name, ": ERROR - could not find the categorical variable '", catname, "' in the data structure"
	return
endif
all_catvalues = data.(rcat)
catvalues = all_catvalues[uniq(all_catvalues, sort(all_catvalues))]
ncat_unique = n_elements(catvalues)
if (keyword_set(verbose)) then begin
	print, name, ": finding unique cat values for catname = ", catname, "'"
	print, name, ": unique catvalues = ", catvalues
endif
rrcat=where(strlowcase(tags) eq strlowcase(catrecord))
if (rrcat[0] lt 0) then begin
	print, name, ": ERROR - could not find category record '", catrecord, "' in the data structure"
	return
endif
catrecords = data.(rrcat)
;; selecting categories with enough data points
if (keyword_set(verbose)) then begin
print, name, ": finding dense categories, catname = '", catname, "'"
endif
mcat = intarr(ncat_unique)
for icat = 0, ncat_unique - 1L, 1 do begin
	catvalue = catvalues[icat]
	mcat[icat] = n_elements(where(all_catvalues eq catvalue))
endfor
dense_cat_index = where(mcat gt min_number_points_for_plot)
if (dense_cat_index[0] eq -1) then begin
	print, name, ": ERROR - no category with # of data points bigger than ", $
	 min_number_points_for_plot
	return
endif
dense_catvalues = catvalues[dense_cat_index]
ncat_dense = n_elements(dense_catvalues)
print, name, ": dense catvalues = ", dense_catvalues

;; all_catvalues = -1
mcat = -1

for icat = 0, ncat_dense - 1L, 1 do begin

	;; extracting simulations belonging to this category
	catvalue = dense_catvalues[icat]
	print, name, ": catname = ", catname, ", icat = ", icat, ", catvalue = ", catvalue
	;; catindex = where_cat(data, catname = catname, catvalue = catvalue)
	catindex = where(all_catvalues eq catvalue)
	draw_x = x[catindex]
	draw_y = y[catindex]
	draw_chisq_mle = chisq_mle[catindex]
	draw_chisq_sdss = chisq_sdss[catindex]
	draw_chisq_diff = chisq_diff[catindex]
	draw_nstep_mle = nstep_mle[catindex]
	draw_snratio_sim = snratio_sim[catindex]

	;; extracting catrecord information for this category
	error = 0
	cat_stat = extract_cat_stat(catname = catname, catvalue = catvalue, icat = icat, cat_records = catrecords[catindex], error = error)
	if (error ne 0) then begin
		print, name, ": ERROR - could not create statistics for '", catname, "' = ", catvalue
		return
	endif

	catfile = add_cat_to_filename(file, catname = catname, catvalue = catvalue)
	print, name, ": writing to file = ", catfile
	olddevice = !d.name ; store the current device name in a variable
	set_plot, 'PS' ; switch to the postscript device
	Device, DECOMPOSED=decomposed, COLOR=1, BITS_PER_PIXEL=8
	device, /encapsulated, file=catfile, font_size = !my_font_size ; specify some details, give the file a name

	loadct, ct, file = "./colors/colors1.tbl", /silent
	;; size of the region
	ymargin_size = (!Y.Margin[0] + !Y.Margin[1]) * !D.Y_CH_SIZE/ !D.Y_PX_CM 
	my_xgap = !my_xgap / (n1)
	my_ygap = !my_ygap / (n2)
	xsize = !my_xsize
	ysize = (!my_xsize * (1.0 - (n1) * my_xgap) * n2) / (n1 * !my_xyratio) + !my_lygap * (n2) + ymargin_size
	my_ygap = !my_lygap / ysize
	if (keyword_set(verbose)) then $
	print, name, ": setting up graphic device, xsize = ", xsize, ", ysize = ", ysize, ", [n1, n2] = ", n1, n2
	device, xsize=xsize, ysize=ysize
	multiplot, /reset
	multiplot, [n1, n2], xgap = my_xgap, ygap = my_ygap,  $
		mTitSize = !my_font_size, $
		mxTitSize = !my_font_size, myTitSize = !my_font_size, $	
		myTitOffset = !myTitOffset, mxTitOffset = !mxTitOffset

	nx = 200
	ny = 200

	print, name, ": determining the ranges and the bounds"
	xrange = get_range(draw_x)
	yrange = get_range(draw_y)
	fractions = [0.01, 0.99]
	smooth_xrange0 = get_range(draw_x)
	smooth_yrange0 = get_range(draw_y)
	smooth_xrange = quantile(fractions, draw_x, range = smooth_xrange0, /nosort)
	smooth_yrange = quantile(fractions, draw_y, range = smooth_yrange0, /nosort)
	smooth_xrange = xrange
	smooth_yrange = yrange
	if (keyword_set(verbose)) then begin
	print, name, ": fractions = ", fractions
	print, name, ": smooth x-range = ", smooth_xrange
	print, name, ": smooth y-range = ", smooth_yrange
	endif
	;; end of quantile calculation
	nstep_range = get_range(nstep_mle)
	if (keyword_set(verbose)) then	print, name, ": nstep range = ", nstep_range

	bounds = [min(x), min(y), max(x), max(y)]
	if (keyword_set(verbose)) then begin
		print, name, ": bounds = ", bounds
		print, name, ": creating smooth x- and y-grid"
	endif

	x_start = smooth_xrange[0]
	x_end = smooth_xrange[1]
	if (doxlog eq 1) then begin
		x_start = alog(x_start)
		x_end = alog(x_end)
	endif
	y_start = smooth_yrange[0]
	y_end = smooth_yrange[1]
	if (doylog eq 1) then begin
		y_start = alog(y_start)
		y_end = alog(y_end)
	endif
	x_delta = (x_end - x_start) / (nx - 1.0)
	y_delta = (y_end - y_start) / (ny - 1.0)
	dimension = [nx, ny]
	start = [x_start, y_start]
	delta = [x_delta, y_delta]
	;;; creating x-grid and y-grid
	smooth_x_grid = replicate(0.0, [nx, ny])
	smooth_y_grid = replicate(0.0, [nx, ny])
	for i = 0, nx - 1, 1 do begin
		smooth_x_grid[i, *] = i * x_delta + x_start
	endfor
	if (doxlog eq 1) then begin
		smooth_x_grid = exp(smooth_x_grid)
		sdraw_x = alog(draw_x)
	endif else begin
		sdraw_x = draw_x
	endelse
	for i = 0, ny - 1, 1 do begin
		smooth_y_grid[*, i] = i * y_delta + y_start
	endfor
	if (doylog eq 1) then begin
		smooth_y_grid = exp(smooth_y_grid)
		sdraw_y = alog(draw_y)
	endif else begin
		sdraw_y = draw_y
	endelse

 	print, name, ": smoothing data"

	;; note: the new version of the plotting macro does not show the mle or the SDSS chisq value but only their difference
	smooth_chisq_diff = smooth_data_for_plotting(sdraw_x, sdraw_y, draw_chisq_diff, $
	bounds = bounds, start = start, delta = delta, nx = nx, ny = ny)
	smooth_nstep_mle = smooth_data_for_plotting(sdraw_x, sdraw_y, draw_nstep_mle, $
	bounds = bounds, start = start, delta = delta, nx = nx, ny = ny)
	smooth_snratio_sim = smooth_data_for_plotting(sdraw_x, sdraw_y, draw_snratio_sim, bounds = bounds, start = stat, delta = delta, nx = nx, ny = ny)

	;; note that the new version of the plotting macro does not show chisq contour plots separately for MLE or SDSS but only the difference
	if (keyword_set(verbose)) then begin
	print, name, ": raw    chisq DIFF : min = ", min(draw_chisq_diff), ", max = ", max(draw_chisq_diff)
	print, name, ": smooth chisq DIFF : min = ", min(smooth_chisq_diff), ", max = ", max(smooth_chisq_diff)
	endif

	dolabels = replicate(1, n_level);
	cell_fill = 0
	fill = 1
	charsize = 0.25
	font = 2
	position = !my_cposition
	leg_position =  !my_lposition
	;; plotting the box
	fpCCstr = "(fpC)"
	if (keyword_set(fit2fpCC)) then fpCCstr = "(fpCC)"
	ctkfmt=!my_ctick_format

	pxrange = xrange
	pyrange = yrange
	print, name, ": plotting differences in CHISQ (1)"
	xtfmt = get_tick_format(range1 = pxrange)
	ytfmt = get_tick_format(range1 = pyrange)
	ctkfmt=!my_ctick_format

	title="CHISQ (MLE-SDSS): src("+source_model+"), trg("+target_model+")"

	error = 0
	uname = "CHISQ"
	plot_contour_and_hist, $
	draw_x = draw_x, draw_y = draw_y, draw_u = draw_chisq_diff, $
	smooth_x = smooth_x_grid, smooth_y = smooth_y_grid, $
	smooth_u = smooth_chisq_diff, $
	pxrange = pxrange, pyrange = pyrange, $
	xtfmt = xtfmt, ytfmt = ytfmt, xlog = doxlog, ylog = doylog, $
	xtitle = xtitle, ytitle = ytitle, $
	u_color_range = chisq_diff_color_range, u_levels = chisq_diff_levels, $
	u_range = chisq_diff_range, $
	uname = uname, ctkfmt = ctkfmt, title = title, font = font, nbin = nbin1, $
	doraw = keyword_set(doraw), dosmooth = keyword_set(dosmooth), $
	dohisto = keyword_set(dohisto), doshisto = keyword_set(doshisto), $	
	error = error

	nxticks = 3
	htfmt = "(g7.2)"

	xtfmt = get_tick_format(range1 = pxrange)
	ytfmt = get_tick_format(range1 = pyrange)
	ctkfmt="(i7)"
	title="NSTEP (MLE): src("+source_model+"), trg("+target_model+")"	
	title="NSTEP (MLE)"

	error = 0
	uname = "NSTEP"
	plot_contour_and_hist, $
	draw_x = draw_x, draw_y = draw_y, draw_u = draw_nstep_mle, $
	smooth_x = smooth_x_grid, smooth_y = smooth_y_grid, $
	smooth_u = smooth_nstep_mle, $
	pxrange = pxrange, pyrange = pyrange, $
	xtfmt = xtfmt, ytfmt = ytfmt, xlog = doxlog, ylog = doylog, $
	xtitle = xtitle, ytitle = ytitle, $
	u_color_range = nstep_mle_color_range, u_levels = nstep_mle_levels, $
	u_range = nstep_mle_range, $
	uname = uname, ctkfmt = ctkfmt, title = title, font = font, nbin = nbin1, $
	doraw = keyword_set(doraw), dosmooth = keyword_set(dosmooth), $
	dohisto = keyword_set(dohisto), doshisto = keyword_set(doshisto), $	
	error = error

	xtfmt = get_tick_format(range1 = pxrange)
	ytfmt = get_tick_format(range1 = pyrange)
	ctkfmt="(i7)"
	title="S/N ratio: src("+source_model+"), trg("+target_model+")"	
	title="S/N ratio"

	error = 0
	uname = "S/N"
	plot_contour_and_hist, $
	draw_x = draw_x, draw_y = draw_y, draw_u = draw_snratio_sim, $
	smooth_x = smooth_x_grid, smooth_y = smooth_y_grid, $
	smooth_u = smooth_snratio_sim, $
	pxrange = pxrange, pyrange = pyrange, $
	xtfmt = xtfmt, ytfmt = ytfmt, xlog = doxlog, ylog = doylog, $
	u_color_range = snratio_sim_color_range, u_levels = snratio_sim_levels, $
	u_range = snratio_sim_range, $
	uname = uname, ctkfmt = ctkfmt, title = title, font = font, nbin = nbin1, $
	doraw = keyword_set(doraw), dosmooth = keyword_set(dosmooth), $
	dohisto = keyword_set(dohisto), doshisto = keyword_set(doshisto), $	
	error = error

	if (keyword_set(verbose)) then print, name, ": closing device"
	device, /close ; close the file
	set_plot, olddevice ; go back to the graphics device
	command = "epstopdf "+catfile
	spawn, command

	if (keyword_set(dolog)) then begin

		name1 = "dCHISQ"
		name2 = "NSTEP"
		error = 0	
		output_log, catfile, addcomment = comment, stat_catrecord = cat_stat, $
			draw_x = draw_x, draw_y = draw_y, smooth_x = smooth_x_grid, smooth_y = smooth_y_grid, $
			xrange = pxrange, yrange = pyrange, xtitle = xtitle, ytitle = ytitle, $
			xlog = doxlog, ylog = doylog, $
			name1 = name1, name2 = name2, row1 = draw_chisq_diff, row2 = draw_nstep_mle, $
			dographicallog = keyword_set(dographicallog), error = error
		if (error ne 0) then begin
			print, name, ": ERROR - could not write to the logfile '", logfile, "'"
			return
		endif

	endif

endfor

end


pro create_simlist_filename, root = root, source_model = source_model, target_model = target_model, simprefix = simprefix, $
	run = run, rerun = rerun, simlistfile=simlistfile
name = "create_simlist_filename"

runstr =  STRING(run,  format =  '(I06)')
runfolder=strtrim(STRING(run), 2)

dir=root+"/"+source_model+"/"+rerun+"/"+runfolder
filename=simprefix+"-"+runstr+".fits"
simlistfile=dir+"/"+filename

return
end
	


pro select_good_objects, sim_arr = sim_arr, mle_arr = mle_arr, sdss_arr = sdss_arr, index = index, count = count, replace = replace
name = "select_good_objects"
if (n_elements(sim_arr) eq 0) then begin
	print, name, ": ERROR - 'sim_arr' should be passed"
	index = [-1]
	count = 0
endif
index = where(sim_arr.re1 le 4.1212 ^ (21 - sim_arr.mag), count)
bcount = 0
bndex = where(sim_arr.re1 gt 4.1212 ^ (21 - sim_arr.mag), bcount)
if (count eq 0) then begin
	print, name, ": WARNING - could not find good fits"
endif 

if (bcount gt 0 and keyword_set(replace)) then begin

	print, name, ": replacing elements - bcount = ", bcount
	mle_arr[bndex].counts_dev = sim_arr[bndex].counts1
	mle_arr[bndex].counts_exp = sim_arr[bndex].counts1
	mle_arr[bndex].counts = sim_arr[bndex].counts1

	mle_arr[bndex].mag_dev = sim_arr[bndex].mag1
	mle_arr[bndex].mag_exp = sim_arr[bndex].mag1
	mle_arr[bndex].mag = sim_arr[bndex].mag1

	mle_arr[bndex].re_dev = sim_arr[bndex].re1
	mle_arr[bndex].re_exp = sim_arr[bndex].re1

	mle_arr[bndex].e_dev = sim_arr[bndex].e1
	mle_arr[bndex].e_exp = sim_arr[bndex].e1

	mle_arr[bndex].phi_dev = sim_arr[bndex].phi1
	mle_arr[bndex].phi_exp = sim_arr[bndex].phi1

	mle_arr[bndex].xc_dev = sim_arr[bndex].xc1
	mle_arr[bndex].xc_exp = sim_arr[bndex].xc1
	mle_arr[bndex].yc_dev = sim_arr[bndex].yc1
	mle_arr[bndex].yc_exp = sim_arr[bndex].yc1

	sdss_arr[bndex].counts_dev = sim_arr[bndex].counts1
	sdss_arr[bndex].counts_exp = sim_arr[bndex].counts1
	sdss_arr[bndex].counts = sim_arr[bndex].counts1

	sdss_arr[bndex].mag_dev = sim_arr[bndex].mag1
	sdss_arr[bndex].mag_exp = sim_arr[bndex].mag1
	sdss_arr[bndex].mag = sim_arr[bndex].mag1

	sdss_arr[bndex].re_dev = sim_arr[bndex].re1
	sdss_arr[bndex].re_exp = sim_arr[bndex].re1

	sdss_arr[bndex].e_dev = sim_arr[bndex].e1
	sdss_arr[bndex].e_exp = sim_arr[bndex].e1

	sdss_arr[bndex].phi_dev = sim_arr[bndex].phi1
	sdss_arr[bndex].phi_exp = sim_arr[bndex].phi1

	sdss_arr[bndex].xc_dev = sim_arr[bndex].xc1
	sdss_arr[bndex].xc_exp = sim_arr[bndex].xc1
	sdss_arr[bndex].yc_dev = sim_arr[bndex].yc1
	sdss_arr[bndex].yc_exp = sim_arr[bndex].yc1


endif



return
end

pro plot_sdss_mle_sim, mleroot = mleroot, runlist = runlist, rerun = rerun, $
	frange = frange,band = band, camcollist = camcollist, $
	target_model=target_model, source_model = source_model, $
	psf_cutoff = psf_cutoff, $
	fit2fpCC = fit2fpCC, domultiple = domultiple, $
	simroot = simroot, simprefix = simprefix, rnames = rnames, tolfactor = tolfactor, $
	seed = seed, random = random, bias = bias, rnderror = rnderror, prob = prob, $
	domle = domle, dophoto = dophoto, doseparate = doseparate, $
	docorrect = docorrect, dovs = dovs, dopoormle = dopoormle, $
	doerror = doerror, docomparerr = docomparerr, $
	dopoorsdss = dopoorsdss, dochisq = dochisq, domag = domag, $
	doarcsec = doarcsec, doell = doell, $
	epsfilename =epsfilename, poorfilename = poorfilename, $
	chisqfilename = chisqfilename, errorfilename = errorfilename, $
	n_level = n_level, nbin1 = nbin1, nbin2 = nbin2, force_nstep_max = force_nstep_max, $
	catlum = catlum, catre = catre, catell = catell, ncat = ncat, $
	unames = unames, uranges = uranges, $
	doraw = doraw, dosmooth = dosmooth, dohisto = dohisto, doshisto = doshisto, $
	nerrorbin = nerrorbin, dosigned = dosigned, dounsigned = dounsigned, dogood = dogood, $
	docolor = docolor, dolog = dolog, dographicallog = dographicallog, debugio = debugio, $
	error = error	
name="plot_sdss_mle_sim"
drun = LONG(GETENV('DEFAULT_SDSS_RUN'))
drerun= STRING(GETENV('DEFAULT_SDSS_RERUN'))
dfrange = [1, 5000]
dtarget_model="deV-exp"
dsource_model="deV"
dpsf_cutoff="9999-9999"
dsimlist="single-galaxy-Oct2.txt"
dsimprefix="sim-data"
dband = "r"
dcamcollist=['1', '2', '3', '4', '5', '6']
dmleroot="/u/khosrow/thesis/opt/soho/single-galaxy/images/mle"
dsimroot="/u/khosrow/thesis/opt/soho/single-galaxy/images/sims"
depsfilename="./mle-sdss.eps"
dpoorfilename = "./mle-sdss-poor.eps"
dchisqfilename = "./chisq-mle-sdss.eps"
derrorfilename = "./error-mle-sdss-sim.eps"
drnames=["XC_STAR", "YC_STAR", "COUNTS_STAR", "MAG_STAR", "XC_DEV", "YC_DEV", "MAG_DEV", "RE_DEV", "PHI_DEV", "XC_EXP", "YC_EXP", "MAG_EXP", "RE_EXP", "PHI_EXP"]
dtolfactor = 5.0
dn_level = 5
dnbin1 = 20
dnbin2 = 30
dnerrorbin = 20
; setting the default values for arguments not set
if (n_elements(mleroot) eq 0) then begin
	mleroot = dmleroot
	print, name, ": mle-root set to ", mleroot
endif
if (n_elements(simroot) eq 0) then begin
	simroot = dsimroot
	print, name, ": sim-root set to ", simroot
endif
if (n_elements(runlist) eq 0) then begin
	print, name, ": run set to", drun
	runlist=[drun]
endif
if (n_elements(frange) lt 2) then begin
	frange = dfrange
	print, name, ": field range set to ", frange
endif
if (n_elements(source_model) eq 0) then begin
	source_model = dsource_model
	print, name, ": source_model set to ", source_model
endif
if (n_elements(target_model) eq 0) then begin
	target_model = dtarget_model
	print, name, ": target_model set to ", target_model
endif
if (n_elements(psf_cutoff) eq 0) then begin
	psf_cutoff = dpsf_cutoff
	print, name, ": psf_cutoff set to ", psf_cutoff
endif
if (n_elements(simprefix) eq 0) then begin
	simprefix = dsimprefix
	print, name, ": simlist set to ", simprefix
endif
if (n_elements(band) eq 0) then begin
	band = dband
	print, name, ": band set to ", band
endif
if (n_elements(camcollist) eq 0) then begin
	camcollist = dcamcollist
	print, name, ": camcollist set to ", camcollist
endif
if (n_elements(epsfilename) eq 0) then begin
	epsfilename = depsfilename
	print, name, ": epsfilename set to ", epsfilename
endif
if (n_elements(poorfilename) eq 0) then begin
	poorfilename = dpoorfilename
	print, name, ": poor filename set to ", poorfilename
endif
if (n_elements(chisqfilename) eq 0) then begin
	chisqfilename = dchisqfilename
	print, name, ": chisq filename set to", chisqfilename
endif
if (n_elements(rnames) eq 0) then begin
	rnames = drnames
	print, name, ": rnames set to ", rnames
endif
if (n_elements(tolfactor) eq 0) then begin
	tolfactor = dtolfactor
	print, name, ": tolerance factor set to ", tolfactor
endif
if (n_elements(n_level) eq 0) then begin
	n_level = dn_level
	print, name, ": n_level for chisq plot set to ", n_level
endif
if (n_elements(nbin1) eq 0) then begin
	nbin1 = dnbin1
	print, name, ": nbin for CHISQ histogram plots set to ", nbin1
endif
if (n_elements(nbin2) eq 0) then begin
	nbin2 = dnbin2
	print, name, ": nbin for NSTEP histogram plots set to ", nbin2
endif
if (n_elements(nerrorbin) eq 0) then begin
	nerrorbin = dnerrorbin
	print, name, ": nbin for error histogram plots set to ", nerrorbin
endif


if (keyword_set(doseparate) and keyword_set(dophoto) and keyword_set(domle)) then begin
	sepfactor = 2
	separate = 1
endif else begin
	sepfactor = 1
	separate = 0
endelse
if (keyword_set(debugio)) then begin
	nread_io = 600
	print, name, ": WARNING - trial run, only checking about ", nread_io, " fields"
	max_io_p = nread_io / (frange[1] - frange[0] + 1.0)
endif else begin
	max_io_p = 2.0
endelse
if (keyword_set(random) + keyword_set(rnderror) + keyword_set(bias) + keyword_set(prob) ne 1) then begin
	print, name, ": ERROR - only one keyword (random, rnderror, bias, prob) should be set"
	error = -1
	return
endif
if (n_elements(seed) eq 0) then seed = total(10 ^ (lindgen(6) * 2) * bin_date(systime(0)))
rerunstring=""

;; SDSS specific information

DEFSYSV, '!RADIANTODEGREE', 180 / !PI, 1 ;; pixel size in arcmin
DEFSYSV, '!PIXELSIZE', 0.396, 1 ;; pixel size in arcmin
DEFSYSV, '!BAD_VALUE', -9999, 1 ;; bad value indicator
;; plot setting
DEFSYSV, '!MY_XSIZE', 16.0; 14.0
DEFSYSV, '!MY_YSIZE', 10.3; 9.0
DEFSYSV, '!MY_XYRATIO', 1.75
DEFSYSV, '!MY_FONT_SIZE', 8
DEFSYSV, '!MY_XGAP', 0.09
DEFSYSV, '!MY_YGAP', 0.09
DEFSYSV, '!MY_LYGAP', 0.09 * 21.0 / 2
DEFSYSV, '!MY_GAP', 0.05

DEFSYSV, '!MY_TICK_FORMAT_VERY_SMALL', '(f8.2)'
DEFSYSV, '!MY_TICK_FORMAT_SMALL', '(f7.1)'
DEFSYSV, '!MY_TICK_FORMAT_LARGE', '(f7.0)'
DEFSYSV, '!MY_TICK_FORMAT_VERY_LARGE', '(e9.1)'

DEFSYSV, '!MY_TICK_FORMAT_INT', '(i7)'
DEFSYSV, '!MY_CTICK_FORMAT', '(f7.1)'
DEFSYSV, '!MY_CPOSITION', [0.0, 0.0, 0.75, 1.0]
DEFSYSV, '!MY_LPOSITION', [0.90, 0.0, 1.0, 1.0]
DEFSYSV, '!MY_XSTYLE', 1
DEFSYSV, '!MY_YSTYLE', 1
DEFSYSV, '!myTitOffset', 10
DEFSYSV, '!mxTitOffset', 0

!Y.MARGIN = [2, 2]
!X.MARGIN = [6, 2]
nrec = n_elements(rnames)
nrun = n_elements(runlist)
ncamcol=n_elements(camcollist)
field0=frange[0]
field1=frange[1]
nfield=field1-field0 + 1
nsize=nfield * nrun * ncamcol

; read the contents of the sim file 
print, name, ": reading simulation array" 
exist1=indgen(nsize)
exist1[*] = 0
;; help, simarray, /stru
icounter=0
print, name, ": assessing the simulation array for"
print, name, ": run = ", runlist
print, name, ": field = ", field0, " , ... , ", field1 
found_counter=0
for irun = 0, nrun-1, 1 do begin
	run = runlist[irun]
	exists=0
	nfound=0
	simlistfile=""
	create_simlist_filename, root = simroot, source_model = source_model, target_model = target_model, simprefix = simprefix, $
	run = run, rerun = rerun, simlistfile=simlistfile
	
	simarray_run = [0, 1, 2]
	error = 0 
	read_simpars_array_from_fits, filename = simlistfile, rnames = rnames, siminfo = simarray_run, $
	docorrect = keyword_set(docorrect), doarcsec = keyword_set(doarcsec), error = error

	if (error eq 0) then begin

	print, name, ": finding sim in siminfo array, run = ", run
	found_index_run = index_sim_in_array(simarray = simarray_run, run = run, Lfield = field0, Ufield = field1, $
		target_model = target_model, source_model = source_model, exists = exists, nfound = nfound)
	if (exists eq 1) then begin
		if (keyword_set(verbose)) then begin
			print, name, ": COMMENT - indexing existing simulations for run = ", run, ", 'exists' = ", exists
			print, name, ": COMMENT - found_index_run: min = ", min(found_index_run), ", max = ", max(found_index_run)
		endif
		;;found_index_run = (irun) * nfield + found_index_run	
		if (keyword_set(verbose)) then print, name, ": COMMENT - found_index_run: min = ", min(found_index_run), ", max = ", max(found_index_run)
		if (found_counter eq 0) then simarray = simarray_run[found_index_run] $
			else simarray = [simarray, simarray_run[found_index_run]]
		;; exist1[found_index_run]=exists
	endif
	found_counter += nfound
	if (keyword_set(verbose)) then print, name, ": # of simulations found = ", nfound

	endif else begin
		print, name, ": could not find siminfo array for run = ", run
	endelse


endfor

if (keyword_set(verbose)) then begin 
print, name, ": # of suitable simulations found (total) = ", found_counter
endif
if (found_counter eq 0) then begin
	print, name, ": ERROR - no proper simulation was found in the list"
	stop
endif

;;if (keyword_set(verbose)) then print, name, ": COMMENT - filtering for simulations in the list of fields and runs"
;; sim_arr = simarray[found_index]
sim_arr = simarray

; make the array and tracer array for the gpobjc file contents
print, name, ": reading gpobjc files"

nsim = n_elements(sim_arr)
exist2 = replicate(0, nsim)
icounter = 0
ncounter = 0

for isim = 0, nsim - 1, 1 do begin

	simelem = sim_arr[isim]
	run = simelem.run
	ifield = simelem.field
	camcol = simelem.camcol


	run_checked = 0
	field_checked = 0
	camcol_checked = 0
	member_checked = 0
	temp_run_info = where(runlist eq run, run_checked)
	field_checked = (ifield le field1) and (ifield ge field0)
	temp_camcol_info = where(camcollist eq camcol, camcol_checked)
	member_checked = (run_checked gt 0) and (field_checked gt 0) and (camcol_checked gt 0)


	if (member_checked) then begin 

	gpobjc = ""
	lmfile= ""

	create_gpobjc_filename, root = mleroot, run = run, rerun = rerun, field = ifield, band = band, camcol = camcol, $
	source_model = source_model, target_model = target_model, $
	psf_cutoff = psf_cutoff, fit2fpCC = keyword_set(fit2fpCC), domultiple=keyword_set(domultiple), $
	gpobjc = gpobjc, lm = lmfile
	
	if (keyword_set(verbose)) then begin
	if (icounter eq 0) then begin
		print, name, ": COMMENT - ifield = ", ifield, ", run = ", run
		print, name, ": COMMENT - gpobjc = ", gpobjc
		print, name, ": COMMENT - lMfile = ", lmfile
	endif
	endif

	filecount = -1
	
	exists = 0
	filecount = 0
	io_this_field = (randomu(seed) le max_io_p)
	if (io_this_field) then begin	
		filelist = file_search(gpobjc, count = filecount)
	endif
	if (filecount gt 0) then begin
		exists = 1
	endif
	error = 0
	if (exists eq 1) then begin
		read_gpobjc, filename = gpobjc, lmfile = lmfile, rnames = rnames, $
		init = init, final = final, doarcsec = keyword_set(doarcsec), error = error
		if (error eq 0) then begin
			if (ncounter eq 0) then begin
				sdss_arr = replicate(init, nsize)
				mle_arr = replicate(final, nsize)
			endif else begin
				sdss_arr[icounter] = init
				mle_arr[icounter]=  final
			endelse
			ncounter++
		endif else begin
			if (keyword_set(verbose)) then print, name, ": ERROR - encountered (error = ", error, " ) at ", gpobjc
			exists = 0
		endelse
	endif
	exist2[icounter] = exists	
	icounter++

	endif ;; membership checked

endfor
print, ""

print, name, ": information gathered for plotting"
print, name, ": gpObjc files found: ", ncounter
index = where(exist2 eq 1)
if (index[0] eq -1) then begin
	print, name, ": no proper gpObjc file was found"
	error = -1
	return
endif
if (keyword_set(verbose)) then begin
print, name, ": COMMENT - step 2: filtering for 'exist2'"
endif

sim_arr = sim_arr[index];
sdss_arr = sdss_arr[index]
mle_arr = mle_arr[index];

if (keyword_set(verbose)) then begin
	print, name, ": COMMENT - step 3: post filter products"
	print, name, ": COMMENT - sim_arr  size = ", n_elements(sim_arr)
	print, name, ": COMMENT - sdss_arr size = ", n_elements(sdss_arr)
	print, name, ": COMMENT - mle_arr  size = ", n_elements(mle_arr)

	camcol_array = sim_arr.camcol
	uniq_camcol_array = camcol_array[uniq(camcol_array, sort(camcol_array))]
	print, name, ": COMMENT - list of camcols found (", n_elements(uniq_camcol_array), "): ", uniq_camcol_array
	for icamcol = 0, n_elements(uniq_camcol_array) - 1L, 1 do begin
		nfiles_camcol = 0
		this_camcol = uniq_camcol_array[icamcol]
		where_camcol = where(camcol_array eq this_camcol, nfiles_camcol)
		print, name, ": COMMENT - camcol = ", this_camcol, ", nfiles = ", nfiles_camcol
	endfor

	where_camcol = [-1]
	nfiles_camcol = 0
	this_camcol = -1
	camcol_array = [-1]
	print, name, "----------------------------"
endif

; now calibrating the simulation information
print, name, ": calibrating simulation information"
calibrate_sim, sim_arr = sim_arr, calib_info = mle_arr, error = error
if (error ne 0) then begin
	print, name, ": ERROR - could not calibrate simulation information, error = ", error
	error = -2
	return
endif
print, name, ": calibrating MLE information"
;; mle_arr[*].phi_dev = !pi / 2.0 + mle_arr[*].phi_dev
;; mle_arr[*].phi_exp = !pi / 2.0 + mle_arr[*].phi_exp
mle_arr[*].phi_dev *= !RADIANTODEGREE
mle_arr[*].phi_exp *= !RADIANTODEGREE

print, name, ": COMMENT - (mle) phi_dev: ", median(mle_arr.phi_dev), min(mle_arr.phi_dev), max(mle_arr.phi_dev)
print, name, ": COMMENT - (mle) phi_exp: ", median(mle_arr.phi_exp), min(mle_arr.phi_exp), max(mle_arr.phi_exp)

calibrate_mle, mle_arr = mle_arr, calib_info = mle_arr, error = error
if (error ne 0) then begin
	print, name, ": ERROR - could not calibrate MLE results, error = ", error
	error = -2
	return
endif
;; sdss_arr[*].phi_dev = !pi / 2.0 - sdss_arr[*].phi_dev
;; sdss_arr[*].phi_exp = !pi / 2.0 - sdss_arr[*].phi_exp
sdss_arr[*].phi_dev *= !RADIANTODEGREE
sdss_arr[*].phi_exp *= !RADIANTODEGREE
print, name, ": COMMENT - (sdss) phi_dev: ", median(sdss_arr.phi_dev), min(sdss_arr.phi_dev), max(sdss_arr.phi_dev)
print, name, ": COMMENT - (sdss) phi_exp: ", median(sdss_arr.phi_exp), min(sdss_arr.phi_exp), max(sdss_arr.phi_exp)


calibrate_mle, mle_arr = sdss_arr, calib_info = mle_arr, error = error, /all
if (error ne 0) then begin
	print, name, ": ERROR - could not calibrate MLE results, error = ", error
	error = -2
	return
endif


;; now detect where magnitude is missing
;; if (keyword_set(domag)) then begin
	print, name, ": deleteing NaN and Inf values in magnitude"
	finite_index = where(finite(sim_arr.mag1) and finite(sim_arr.mag2))
	n_finite = n_elements(finite_index)
	n_nan = n_elements(sim_arr) - n_finite
	print, name, ": finite index = ", n_finite
	print, name, ": NaN    index = ", n_nan
	sim_arr = sim_arr[finite_index]
	sdss_arr = sdss_arr[finite_index]
	mle_arr = mle_arr[finite_index]
;; endif


print, name, ": selecting good objects"
if (keyword_set(dogood)) then begin
	gndex = [-1]
	ngood = 0
	select_good_objects, sim_arr = sim_arr, mle_arr = mle_arr, sdss_arr = sdss_arr, index = gndex, count = ngood, /replace
	if (ngood le 0) then begin
		print, name, ": ERROR - could not select good fits, ngood = ", ngood
	endif else begin
		print, name, ": good objects selected (counts = ", ngood, ")"
		
		;; sim_arr = sim_arr[gndex]
		;; mle_arr = mle_arr[gndex]
		;; sdss_arr = sdss_arr[gndex]	
	endelse 
endif else begin
	gndex = lindgen(n_elements(sim_arr))
endelse

print, name, ": information sorted for plotting"
print, name, ": setting up device at file: ", epsfilename

allpoorindex_mle = [-1L] ; this will be later used to investigate the criteria for goodness of fit
allpoorindex_sdss = [-1L]

if (keyword_set(dovs)) then $
	plot_mle_vs_sdss, rnames = rnames, file = epsfilename, $
	sim_arr = sim_arr[gndex], mle_arr = mle_arr[gndex], sdss_arr = sdss_arr[gndex], sepfactor = sepfactor, $
	domle = keyword_set(domle), dophoto = keyword_set(dophoto), separate = separate, tolfactor = tolfactor

if (keyword_set(dopoormle) or keyword_set(dopoorsdss)) then $
	plot_poor_fits, sim_arr = sim_arr, sdss_arr = sdss_arr, mle_arr = mle_arr, $
	dopoormle = keyword_set(dopoormle), dopoorsdss = keyword_set(dopoorsdss), file = poorfilename, $
	rnames = rnames, source_model = source_model, target_model = target_model, tolfactor = tolfactor

comment_string = "sim address = '"+simroot+"'"
if (keyword_set(dochisq)) then $
	do_chisq_contour_plots, file = chisqfilename, source_model = source_model, target_model = target_model, $
	n_level = n_level, nbin1 = nbin1, nbin2 = nbin2, $
	sim_arr = sim_arr, mle_arr = mle_arr, sdss_arr = sdss_arr, $
	domag = keyword_set(domag), doarcsec = keyword_set(doarcsec), doell = keyword_set(doell), fit2fpCC = keyword_set(fit2fpCC), $
	catlum = keyword_set(catlum), catell = keyword_set(catell), catre = keyword_set(catre), ncat = ncat, $
	comment_string = comment_string, force_nstep_max = force_nstep_max, $
	doraw = keyword_set(doraw), dosmooth = keyword_set(dosmooth), $
	dohisto = keyword_set(dohisto), doshisto = keyword_set(doshisto), $
	dolog = keyword_set(dolog), dographicallog = keyword_set(dographicallog), docolor = keyword_set(docolor), $
	random = keyword_set(random), bias = keyword_set(bias), rnderror = keyword_set(rnderror), prob = keyword_set(prob)

print, name, ": successful end of plotting"

comment_string = "sim address = '"+simroot+"'"
if (keyword_set(doerror)) then begin
	n_u = n_elements(unames)
	for i_u = 0, n_u - 1, 1 do begin
		uname = unames[i_u]
		if (n_elements(uranges) ne 0) then begin
			urange = uranges[*, i_u]
		endif else begin
			urange = !NULL
		endelse
		if (keyword_set(dounsigned)) then begin
			ufilename = create_error_filename(errorfilename, uname = uname, /dopositive)
			do_error_contour_plots, file = ufilename, source_model = source_model, target_model = target_model, $
			sim_arr = sim_arr, sdss_arr = sdss_arr, mle_arr = mle_arr, $
			n_level = n_level, nbin = nerrorbin, $
			random = keyword_set(random), bias = keyword_set(bias), rnderror = keyword_set(rnderror), prob = keyword_set(prob), $
			gndex = gndex, dogood = keyword_set(dogood), $
			domag = keyword_set(domag), doarcsec = keyword_set(doarcsec), doell = keyword_set(doell), $
			catell = keyword_set(catell), catre = keyword_set(catre), catlum = keyword_set(catlum), ncat = ncat, $
			fit2fpCC = keyword_set(fit2fpCC), comment_string = comment_string, $
			uname = uname, urange = urange, seed = seed, doraw = keyword_set(doraw), dosmooth = keyword_set(dosmooth), $
			dohisto = keyword_set(dohisto), doshisto = keyword_set(doshisto), $
			/dopositive, dolog = keyword_set(dolog), dographicallog = keyword_set(dographicallog), $
			docolor = keyword_set(docolor), docompare = keyword_set(docomparerr)
		endif
		if (keyword_set(dosigned)) then begin
			ufilename = create_error_filename(errorfilename, uname = uname)
			do_error_contour_plots, file = ufilename, source_model = source_model, target_model = target_model, $
			sim_arr = sim_arr, sdss_arr = sdss_arr, mle_arr = mle_arr, $
			n_level = n_level, nbin = nerrorbin, $
			random = keyword_set(random), bias = keyword_set(bias), rnderror = keyword_set(rnderror), prob = keyword_set(prob), $
			gndex = gndex, dogood = keyword_set(dogood),  $
			domag = keyword_set(domag), doarcsec = keyword_set(doarcsec), doell = keyword_set(doell), $
			catell = keyword_set(catell), catre = keyword_set(catre), catlum = keyword_set(catlum), ncat = ncat, $
			fit2fpCC = keyword_set(fit2fpCC), comment_string = comment_string, $
			uname = uname, urange = urange, seed = seed, doraw = keyword_set(doraw), dosmooth = keyword_set(dosmooth), $
			dohisto = keyword_set(dohisto), doshisto = keyword_set(doshisto), $
			dolog = keyword_set(dolog), dographicallog = keyword_set(dographicallog), $
			docolor = keyword_set(docolor), docompare = keyword_set(docomparerr)
		endif

	endfor
endif



end

	
pro plot_mle_vs_sdss, rnames = rnames, file = file, $
	sim_arr = sim_arr, mle_arr = mle_arr, sdss_arr = sdss_arr, sepfactor = sepfactor, $ 
	domle = domle, dophoto = dophoto, separate = separate, tolfactor = tolfactor, $
	clustercat = clustercat, ncluster = ncluster

name = "plot_mle_vs_sdss"

;; nrec = n_elements(rnames)
;; finding nrec
nrec = 0
sim_arr0 = sim_arr[0]
for irec=0, n_elements(rnames) - 1, 1 do begin
	rname = rnames[irec];
	;; number of the index in each of the structures 
	if (tag_exist(sim_arr0, "model1")) then sim_arr0.model1 = strtrim(sim_arr0.model1, 2)
	if (tag_exist(sim_arr0, "model2")) then sim_arr0.model2 = strtrim(sim_arr0.model2, 2)
	error_message =  ": ERROR - rname = "+rname+" not supported for sim ("+sim_arr0.model1+")" ;; +", "+sim_arr0.model2+")"
	error = 0	

	case rname of
		"MAG": begin ;; total magnitude 
			nrec++
			end
		"COUNTS": begin ;; total counts
			nrec++
			end
		"MAG_DEV": begin
			if (strcmp(sim_arr0.model1, "deV")) then begin
				nrec++
			endif else begin
				if ((not strcmp(sim_arr0.model1, "exp")) and (strcmp(sim_arr0.model2, "deV"))) then begin
					nrec++
				endif else begin
					print, name, error_message
				error = 1
				endelse
			endelse 
			end
		"COUNTS_DEV": begin
			if (strcmp(sim_arr0.model1, "deV")) then begin
				nrec++
			endif else begin
			if ((not strcmp(sim_arr0.model1, "exp")) and (strcmp(sim_arr0.model2, "deV"))) then begin
				nrec++
			endif else begin
					print, name, error_message
				error = 1
				endelse
			endelse
			end
		"RE_DEV": begin
			if (strcmp(sim_arr0.model1, "deV")) then begin
				nrec++
			endif else begin
			if ((not strcmp(sim_arr0.model1, "exp")) and (strcmp(sim_arr0.model2, "deV"))) then begin
				nrec++
			endif else begin
					print, name, error_message
				error = 1
				endelse
			endelse
			end
		"PHI_DEV": begin
			if (strcmp(sim_arr0.model1, "deV")) then begin
				nrec++
			endif else begin
			if ((not strcmp(sim_arr0.model1, "exp")) and (strcmp(sim_arr0.model2, "deV"))) then begin
				nrec++
			endif else begin
					print, name, error_message
				error = 1
				endelse
			endelse
			end

		"MAG_EXP": begin
			if (strcmp(sim_arr0.model1, "exp")) then begin
				nrec++
			endif else begin
			if ((strcmp(sim_arr0.model1, "deV")) and (strcmp(sim_arr0.model2, "exp"))) then begin
				nrec++
			endif else begin
					print, name, error_message
				error = 1
				endelse
			endelse
			end
		"COUNTS_EXP": begin
			if (strcmp(sim_arr0.model1, "exp")) then begin
				nrec++
			endif else begin
			if ((strcmp(sim_arr0.model1, "deV")) and (strcmp(sim_arr0.model2, "exp"))) then begin
				nrec++
			endif else begin
					print, name, error_message
				error = 1
				endelse
			endelse
			end
		"RE_EXP": begin
			if (strcmp(sim_arr0.model1, "exp")) then begin
				nrec++
			endif else begin
			if ((strcmp(sim_arr0.model1, "deV")) and (strcmp(sim_arr0.model2, "exp"))) then begin
				nrec++
			endif else begin
					print, name, error_message
				error = 1
				endelse
			endelse
			end
		"PHI_EXP": begin
			if (strcmp(sim_arr0.model1, "exp")) then begin
				nrec++
			endif else begin
			if ((strcmp(sim_arr0.model1, "deV")) and (strcmp(sim_arr0.model2, "exp"))) then begin
				nrec++
			endif else begin
					print, name, error_message
				error = 1
				endelse
			endelse
			end
		"MAG_STAR": begin
			if (strcmp(sim_arr0.model1, "star")) then begin
				nrec++
			endif else begin
			if (strcmp(sim_arr0.model2, "star")) then begin
				nrec++
			endif else begin
					print, name, error_message
				error = 1
				endelse
			endelse
			end
		"COUNTS_STAR": begin
			if (strcmp(sim_arr0.model1, "star")) then begin
				nrec++
			endif else begin
			if ((strcmp(sim_arr0.model2, "star"))) then begin
				nrec++
			endif else begin
					print, name, error_message
				error = 1
				endelse
			endelse
			end
		"XC_STAR": begin
			if (strcmp(sim_arr0.model1, "star")) then begin
				nrec++
			endif else begin
			if ((strcmp(sim_arr0.model2, "star"))) then begin
				nrec++
			endif else begin
					print, name, error_message
				error = 1
				endelse
			endelse
			end
		"XC_DEV":  begin
			if (strcmp(sim_arr0.model1, "deV")) then begin
				nrec++
			endif else begin
			if ((strcmp(sim_arr0.model2, "deV"))) then begin
				nrec++
			endif else begin
					print, name, error_message
				error = 1
				endelse
			endelse
			end
		"XC_EXP": begin
			if (strcmp(sim_arr0.model1, "exp")) then begin
				nrec++
			endif else begin
			if ((strcmp(sim_arr0.model2, "exp"))) then begin
				nrec++
			endif else begin
					print, name, error_message
				error = 1
				endelse
			endelse
			end
		"YC_STAR": begin
			if (strcmp(sim_arr0.model1, "star")) then begin
				nrec++
			endif else begin
			if ((strcmp(sim_arr0.model2, "star"))) then begin
				nrec++
			endif else begin
					print, name, error_message
				error = 1
				endelse
			endelse
			end
		 "YC_DEV":  begin
			if (strcmp(sim_arr0.model1, "deV")) then begin
				nrec++
			endif else begin
			if ((strcmp(sim_arr0.model2, "deV"))) then begin
				nrec++
			endif else begin
					print, name, error_message
				error = 1
				endelse
			endelse
			end
		"YC_EXP": begin
			if (strcmp(sim_arr0.model1, "exp")) then begin
				nrec++
			endif else begin
			if ((strcmp(sim_arr0.model2, "exp"))) then begin
				nrec++
			endif else begin
					print, name, error_message
				error = 1
				endelse
			endelse
			end
		else: begin
			print, name, ": unsupported plot option ", rname
			error = 1
			end
	endcase
	if (error eq 0) then begin
		if (nrec eq 1) then begin
			good_rindex = [irec]
			good_rnames = [rname]
		endif else begin
			good_rindex = [good_rindex, irec]
			good_rnames = [good_rnames, rname]
		endelse
	endif
endfor

if (nrec eq 0) then begin
	print, name, ": ERROR - could not find any matching record name to the simulated array provided"
	error = -1
	return
endif

clustercat="re1"
catclass = "re1"
ncluster = 100
niterations = 5
if (n_elements(catclass) ne 1) then begin
	catresult = replicate(1L, n_elements(sim_arr))
	ncluster = 1
	catindex = [1L]
endif else begin
	catrecord = -1L
	does_cat_tag_exist = tag_exist(sim_arr0, clustercat, index = catrecord)	
	if (does_cat_tag_exist eq 0 or catrecord lt 0) then begin
		print, name, ": ERROR - could not find tag ", catclass, " in simulation info"
		error = -3
		return
	endif
	catarray = transpose(sim_arr[*].(catrecord))
	if (n_elements(ncluster) eq 1) then begin
		catweights = CLUST_WTS(catarray, N_CLUSTERS = ncluster, N_ITERATIONS = niterations) 
		catresult = CLUSTER(catarray, catweights, N_CLUSTERS = ncluster)
		catindex = catresult[UNIQ(catresult, SORT(catresult))]

		min_catarray = min(catarray)
		dcat = (max(catarray) - min(catarray)) / ncluster
		catresult = floor((catarray - min_catarray) / dcat)	
		catindex = catresult[UNIQ(catresult, SORT(catresult))]
		ntoobig = 0
		max_catindex = where(catresult eq ncluster, ntoobig)
		if (ntoobig gt 0) then catresult[max_catindex] = ncluster - 1
		if (ncluster ne n_elements(catindex)) then begin
			print, name, ": WARNING - looking for ", ncluster, "clusters in the data but found ", n_elements(catindex), "instead"
			ncluster = n_elements(catindex)
		endif
	endif else begin
		catweights = CLUST_WTS(catarray, N_ITERATIONS = niterations) 
		catresult = CLUSTER(catarray, catweights)
		catindex = catresult[UNIQ(catresult, SORT(catresult))]
		ncluster = n_elements(catindex)
	endelse
	print, name, ": COMMENT - clustercat    = ", clustercat
	print, name, ": COMMENT - ncluster      = ", ncluster	
	print, name, ": COMMENT - catarray size = ", n_elements(catarray)
endelse


print, name, ": COMMENT - ", nrec, " applicable records found"
print, name, ": COMMENT - indices = ", good_rindex
print, name, ": COMMENT - records = ", good_rnames

n1 = sepfactor;
n2 = nrec

;; n1 = ceil(sqrt(nrec))
;; n2 = ceil((1.0 * nrec) / (1.0 * n1)) * sepfactor
print, name, ": sepfactor = ", sepfactor

aspect_ratio=(1.0 * n1) / n2
xsize= !my_xsize * n1 / 2.0
ysize= !my_ysize * n2 / 2.0
symsize=1.0 / 20.0 * xsize

ct = 0 ;; color table
decomposed = 0 ;; color decomposition

print, name, ": writing to file = ", file
olddevice = !d.name ; store the current device name in a variable
set_plot, 'PS' ; switch to the postscript device
Device, DECOMPOSED=decomposed, COLOR=1, BITS_PER_PIXEL=8
device, /encapsulated, file=file, font_size = !my_font_size; specify some details, give the file a name
ct = 16
loadct, ct, file = "./colors/colors1.tbl";

;; size of the region
ymargin_size = (!Y.Margin[0] + !Y.Margin[1]) * !D.Y_CH_SIZE/ !D.Y_PX_CM 
my_xgap = !my_xgap / (n1)
my_ygap = !my_ygap / (n2)
xsize = !my_xsize
ysize = (!my_xsize * (1.0 - (n1) * my_xgap) * n2) / (n1 * !my_xyratio) + !my_lygap * (n2) + ymargin_size
my_ygap = !my_lygap / ysize

;; if (keyword_set(verbose)) then $
print, name, ": setting up graphic device, xsize = ", xsize, ", ysize = ", ysize, "[n1, n2] = ", n1, n2
device, xsize=xsize, ysize=ysize
gap = !my_gap
multiplot, /reset
multiplot, [n1, n2], xgap = my_xgap, ygap = my_ygap, $
	mTitSize = !my_font_size, $
	mxTitSize = !my_font_size, myTitSize = !my_font_size, $
	myTitOffset = !myTitOffset, mxTitOffset = !mxTitOffset

; now add this to the plots
sim_arr0 = sim_arr[0];
;; help, sim_arr0, /stru
for irec=0, nrec - 1, 1 do begin
	rname = good_rnames[irec];

	;; number of the index in each of the structures 
	if (tag_exist(sim_arr0, "model1")) then sim_arr0.model1 = strtrim(sim_arr0.model1, 2)
	if (tag_exist(sim_arr0, "model2")) then sim_arr0.model2 = strtrim(sim_arr0.model2, 2)
	error_message =  ": ERROR - rname = "+rname+" not supported for sim ("+sim_arr0.model1+")" ;; +", "+sim_arr0.model2+")"
	error = 0	
	case rname of
		"MAG": begin ;; total magnitude 
			xrname = "MAG"
			arname = xrname
			brname = rname
			xunit = " ( )"
			aunit = xunit
			dolog = 0
			doalog = 0	
			end
		"COUNTS": begin ;; total counts
			xrname = "COUNTS"	
			arname = xrname
			brname = rname
			xunit = " (photons)"	
			aunit = xunit
			dolog = 1
			doalog = 1
			end
		"MAG_DEV": begin
			if (strcmp(sim_arr0.model1, "deV")) then begin
				xrname = "MAG1"
			endif else begin
				if ((not strcmp(sim_arr0.model1, "exp")) and (strcmp(sim_arr0.model2, "deV"))) then begin
					xrname = "MAG2"
				endif else begin
					print, name, error_message
				error = 1
				endelse
			endelse 
			arname = xrname
			brname = rname
			xunit = " ( )"	
			aunit = xunit
			dolog = 0
			doalog = 0
			end
		"COUNTS_DEV": begin
			if (strcmp(sim_arr0.model1, "deV")) then begin
				xrname = "COUNTS1"
			endif else begin
			if ((not strcmp(sim_arr0.model1, "exp")) and (strcmp(sim_arr0.model2, "deV"))) then begin
				xrname = "COUNTS2"
			endif else begin
					print, name, error_message
				error = 1
				endelse
			endelse
			arname = xrname
			brname = rname
			xunit = " (photons)"	
			aunit = xunit
			dolog = 1
			doalog = 1
			end
		"RE_DEV": begin
			if (strcmp(sim_arr0.model1, "deV")) then begin
				xrname = "RE1"
			endif else begin
			if ((not strcmp(sim_arr0.model1, "exp")) and (strcmp(sim_arr0.model2, "deV"))) then begin
				xrname = "RE2"
			endif else begin
					print, name, error_message
				error = 1
				endelse
			endelse
			arname = xrname
			arname = "MAG"
			brname = rname
			brname = "magnitude"
			xunit = " (pixel)"
			if (keyword_set(doarcsec)) then xunit = " (arcsec)"
			aunit = xunit
			dolog = 1
			doalog = 0
			end
		"PHI_DEV": begin
			if (strcmp(sim_arr0.model1, "deV")) then begin
				xrname = "PHI1"
			endif else begin
			if ((not strcmp(sim_arr0.model1, "exp")) and (strcmp(sim_arr0.model2, "deV"))) then begin
				xrname = "PHI2"
			endif else begin
					print, name, error_message
				error = 1
				endelse
			endelse
			arname = xrname
			arname = "MAG"
			brname = rname
			brname = "magnitude"
			xunit = " (degree)"
			aunit = xunit
			aunit = " ( )"
			dolog = 0
			doalog = 0
			end

		"MAG_EXP": begin
			if (strcmp(sim_arr0.model1, "exp")) then begin
				xrname = "MAG1"
			endif else begin
			if ((strcmp(sim_arr0.model1, "deV")) and (strcmp(sim_arr0.model2, "exp"))) then begin
				xrname = "MAG2"
			endif else begin
					print, name, error_message
				error = 1
				endelse
			endelse
			arname = xrname
			arname = "MAG"
			brname = rname
			brname = "magnitude"
			xunit = " ( )"
			aunit = xunit
			dolog = 0
			doalog = 0
			end
		"COUNTS_EXP": begin
			if (strcmp(sim_arr0.model1, "exp")) then begin
				xrname = "COUNTS1"
			endif else begin
			if ((strcmp(sim_arr0.model1, "deV")) and (strcmp(sim_arr0.model2, "exp"))) then begin
				xrname = "COUNTS2"
			endif else begin
					print, name, error_message
				error = 1
				endelse
			endelse
			arname = xrname
			brname = rname
			xunit = " (photons)"	
			aunit = xunit
			dolog = 1
			doalog = 1
			end
		"RE_EXP": begin
			if (strcmp(sim_arr0.model1, "exp")) then begin
				xrname = "RE1"
			endif else begin
			if ((strcmp(sim_arr0.model1, "deV")) and (strcmp(sim_arr0.model2, "exp"))) then begin
				xrname = "RE2"
			endif else begin
					print, name, error_message
				error = 1
				endelse
			endelse
			arname = xrname
			arname = "MAG"
			brname = rname
			brname = "magnitude"
			xunit = " (pixel)"
			if (keyword_set(doarcsec)) then xunit = " (arcsec)"
			aunit = xunit
			aunit = " ( )"
			dolog = 1
			doalog = 0
			end
		"PHI_EXP": begin
			if (strcmp(sim_arr0.model1, "exp")) then begin
				xrname = "PHI1"
			endif else begin
			if ((strcmp(sim_arr0.model1, "deV")) and (strcmp(sim_arr0.model2, "exp"))) then begin
				xrname = "PHI2"
			endif else begin
					print, name, error_message
				error = 1
				endelse
			endelse
			arname = xrname
			arname = "MAG"
			brname = rname
			brname = "magnitude"
			xunit = " (degree)"
			aunit = xunit
			aunit = " ( )"
			dolog = 0
			doalog = 0
			end
		"MAG_STAR": begin
			if (strcmp(sim_arr0.model1, "star")) then begin
				xrname = "MAG1"
			endif else begin
			if (strcmp(sim_arr0.model2, "star")) then begin
				xrname = "MAG2"
			endif else begin
					print, name, error_message
				error = 1
				endelse
			endelse
			arname = xrname
			brname = rname
			xunit = " ( )"
			aunit = xunit
			dolog = 0
			doalog = 0
			end
		"COUNTS_STAR": begin
			if (strcmp(sim_arr0.model1, "star")) then begin
				xrname = "COUNTS1"
			endif else begin
			if ((strcmp(sim_arr0.model2, "star"))) then begin
				xrname = "COUNTS2"
			endif else begin
					print, name, error_message
				error = 1
				endelse
			endelse
			arname = xrname
			brname = rname
			xunit = " (photons)"	
			aunit = xunit
			dolog = 1
			doalog = 1
			end
		"XC_STAR": begin
			xrname = "XC1"
			arname = "MAG1"
			brname = "magnitude"
			if (keyword_set(arcsec)) then xunit = " (arcsec)" else xunit = " (pixel)"	
			aunit = " ()"
			dolog = 0
			doalog = 0
			end
		"XC_DEV":  begin
			xrname = "XC1"
			arname = "MAG1"
			brname = "magnitude"
			if (keyword_set(arcsec)) then xunit = " (arcsec)" else xunit = " (pixel)"	
			aunit = " ( )"
			dolog = 0
			doalog = 0
			end
		"XC_EXP": begin
			xrname = "XC1"
			arname = "MAG1"
			brname = "magnitude"
			if (keyword_set(arcsec)) then xunit = " (arcsec)" else xunit = " (pixel)"	
			aunit = " ()"
			dolog = 0
			doalog = 0
			end
		"YC_STAR": begin
			xrname = "YC1"
			arname = "MAG1"
			brname = "magnitude"
			if (keyword_set(arcsec)) then xunit = " (arcsec)" else xunit = " (pixel)"	
			aunit = " ( )"
			dolog = 0
			doalog = 0
			end
		 "YC_DEV":  begin
			xrname = "YC1"
			arname = "MAG1"
			brname = "magnitude"
			if (keyword_set(arcsec)) then xunit = " (arcsec)" else xunit = " (pixel)"	
			aunit = " ( )"
			dolog = 0
			doalog = 0
			end
		"YC_EXP": begin
			xrname = "YC1"
			arname = "MAG1"
			brname = "magnitude"
			if (keyword_set(arcsec)) then xunit = " (arcsec)" else xunit = " (pixel)"	
			aunit = " ( )"
			dolog = 0
			doalog = 0
			end
		else: print, name, ": unsupported plot option ", rname
	endcase

	;; arname = "XC1"
	;; brname = "XC_STAR"
	;; aunit = " (pixel) "
	;; doalog = 0


	if (error eq 0) then begin

	xnames=tag_names(sim_arr0)
	xindex=where(strcmp(xnames, xrname) EQ 1)
	aindex = where(strcmp(xnames, arname) EQ 1)

	ynames1=tag_names(mle_arr[0])
	yindex1=where(strcmp(ynames1,rname) EQ 1)
	
	ynames2=tag_names(sdss_arr[0])
	yindex2=where(strcmp(ynames2,rname) EQ 1)

	;; if (keyword_set(verbose)) then begin
	print, name, ": xrname  = ", xrname, ", xindex  = ", xindex
	print, name, ": yrname1	= ", rname, ", yindex1 = ", yindex1
	print, name, ": yrname2 = ", rname, ", yindex2 = ", yindex2
	;; endif

	abscissa = sim_arr[*].(aindex)
	x = sim_arr[*].(xindex)
	y1 = mle_arr[*].(yindex1)
	y2 = sdss_arr[*].(yindex2)
 	;; bad - negative numbers - from mle and sdss

	;; if (keyword_set(verbose)) then begin
	print, name, " - x : ", min(x), max(x), median(x)
	print, name, " - y1: ", min(y1), max(y1), median(y1)
	print, name, " - y2: ", min(y2), max(y2), median(y2)
	;; endif


	counts_arr = sim_arr.counts
	gcount = 0
	if (dolog) then begin
		gindex = where(x gt 0.0, gcount) ;;  and y1 gt 0.0 and y2 gt 0.0, gcount) ;; and counts_arr eq 50000.0, gcount)
	endif else begin
		;; gindex = where(sim_arr.e1 gt 0.4, gcount)	
		gindex = lindgen(n_elements(x)) ;; where(x gt 0.0 and y1 gt 0.0 and y2 gt 0.0) ;; and counts_arr eq 50000.0, gcount)
		gcount = n_elements(gindex)
	endelse
	print, name, ": plotting ", rname, ", # of points = ", gcount
	goodplot = (gcount gt 0)
	endif else begin
		goodplot = 0
	endelse


	if (goodplot) then begin

	abscissa = abscissa[gindex]
	x = x[gindex]
	y1 = y1[gindex]
	y2 = y2[gindex]

	;; now getting the range
	;; because plot is log-log positive values are separated first
	arange = get_range(abscissa)
	xrange = get_range(x);
	yrange1 = get_range(y1)
	yrange2 = get_range(y2)
	yrange = get_range([yrange1, yrange2])
	;; making the x and y ranges the same
	;; xrange = get_range([xrange, yrange])
	;; yrange = get_range([xrange, yrange])
	;; information for drawing the hypothetical line
	aa = dindgen(100)
	aa = aa / max(aa)
	alen = arange[1] - arange[0]
	a0 = arange[0]
	aa = aa * alen + a0

	xx = dindgen(100)
	xx = xx / max(xx)
	xlen = xrange[1] - xrange[0]
	x0 = xrange[0]
	xx = xx * xlen + x0

	aticks = [-1.0]
	xticks = [-1.0]
	yticks = [-1.0]

	atfmt = get_tick_format(range1 = arange)
	xtfmt = get_tick_format(range1 = xrange)
	ytfmt = get_tick_format(range1 = yrange)

	if (dolog) then z1 = (y1 - x) / x else z1 = y1 - x
	if (dolog) then z2 = (y2 - x) / x else z2 = y2 - x
	z1range = get_range([z1, -z1])
	z2range = get_range([z2, -z2])

	z1range = get_range(z1)
	z2range = get_range(z2)

	if (dophoto and domle) then begin 
		zrange = get_range([z1, z2, -z1, -z2])
		zrange = get_range([z1, z2])
	endif else begin
		if (domle) then zrange = z1range
		if (dophoto) then zrange = z2range
	endelse

	;; if (rname eq "XC_STAR" or rname eq "YC_STAR") then zrange = [-0.4, 0.4]
	 if (rname eq "XC_DEV" or rname eq "YC_DEV" or rname eq "XC_EXP" or rname eq "YC_EXP") then zrange = [-1.51, 1.51]

	ztfmt = get_tick_format(range1 = zrange)
	dozlog = 0

	print, name, " - z1 : ", min(z1), max(z1), median(z1)
	print, name, " - z2 : ", min(z2), max(z2), median(z2)

	min_y1 = min(y1)
	min_y1_index = where(y1 eq min_y1)
	print, name, ": min_y1_index: ", min_y1_index
	print, name, ": x  = ", x[min_y1_index]
	print, name, ": y1 = ", y1[min_y1_index]	
	print, name, ": z1 = ", z1[min_y1_index]

	psym1 = 3
	psym2 = 3
	color1 = floor(!D.TABLE_SIZE / 3)
	color2 = floor(2 * !D.TABLE_SIZE / 3)
	catcolors = floor(!D.TABLE_SIZE * findgen(ncluster) / (ncluster * 1.0))
	symsize = 0

	error_string = " error"
	if (dolog) then error_string = " rel. error"
	atitle = brname+aunit
	ztitle = rname+error_string+xunit
	;; now plotting
	print, name, ": record = ", rname, ', data range = ', xrange
	;;setup_plot, rname = rname, xrange = xrange, yrange = yrange
	;;do_legends, names = names, legends = legends	
	title=rname+error_string+xunit
	if (keyword_set(domle)) then begin
		for icluster = 0L, ncluster - 1L, 1L do begin
			category = catindex[icluster]
			ncndex = 0
			cndex = where(catresult eq category, ncndex)
			colorx = catcolors[icluster]
			if (ncndex gt 0) then $
			plot, abscissa[cndex], z1[cndex], xrange = arange, yrange = zrange, psym = psym1, symsize = symsize, color = colorx, $
			xlog = doalog, ylog = dozlog, xstyle = 4, ystyle = 4, xtitle = atitle, ytitle = ztitle;;, legend = legends[0]
		endfor	
	endif
	if (keyword_set(separate)) then begin
		plot, aa, xx - xx, xrange = arange, yrange = zrange, linestyle = 0, $
		xlog = doalog, ylog = dozlog, title = title+"(MLE)", $
		font = 2, xtickformat = atfmt, ytickformat = ztfmt, xtick_get = xticks, ytick_get = yticks, $
		xtitle = atitle, ytitle = ztitle
		yy = xx / tolfactor
		if (dolog) then zz = (yy - xx) / xx else zz = yy - xx
		plot, aa, zz, xrange = arange, yrange = zrange, linestyle = 1, $
		xlog = doalog, ylog = dozlog, xstyle = 4, ystyle = 4
		yy = xx * tolfactor		
		if (dolog) then zz = (yy - xx) / xx else zz = yy - xx
		plot, aa, zz, xrange = arange, yrange = zrange, linestyle = 1, $
		xlog = doalog, ylog = dozlog, xstyle = 4, ystyle = 4
		multiplot
	endif
	if (keyword_set(dophoto)) then begin
		for icluster = 0L, ncluster - 1L, 1L do begin
			category = catindex[icluster]
			ncndex = 0
			cndex = where(catresult eq category, ncndex)
			colorx = catcolors[icluster]
			if (ncndex gt 0) then $
			plot, abscissa[cndex], z2[cndex], xrange = arange, yrange = zrange, psym = psym2, symsize = symsize, color = colorx, $
			xlog = doalog, ylog = dozlog, xstyle = 4, ystyle = 4, xtitle = atitle, ytitle = ztitle;;, legend = legends[1]
		endfor
	endif
	title=rname+error_string+xunit
	if (keyword_set(separate)) then title=title+"(PHOTO)"
	plot, aa, xx - xx, xrange = arange, yrange = zrange, linestyle = 0, $
	xlog = doalog, ylog = dozlog, title = title, $
	font = 2, xtickformat = atfmt, ytickformat = ztfmt, xtitle = atitle, ytitle = ztitle
	yy = xx / tolfactor
	if (dolog) then zz = (yy - xx) / xx else zz = yy - xx
	plot, aa, zz, xrange = arange, yrange = zrange, linestyle = 1, $
	xlog = doalog, ylog = dozlog, xstyle = 4, ystyle = 4
	yy = xx * tolfactor	
	if (dolog) then zz = (yy - xx) / xx else zz = yy - xx
	plot, aa, zz, xrange = arange, yrange = zrange, linestyle = 1, $
	xlog = doalog, ylog = dozlog, xstyle = 4, ystyle = 4
	multiplot

	endif
endfor
multiplot, /reset
if (keyword_set(verbose)) then print, name, ": closing device"
device, /close ; close the file
set_plot, olddevice ; go back to the graphics device
command="epstopdf "+file
spawn, command
print, name, ": successful end of plotting (finding vs. simulation)"


end


pro plot_poor_fits, sim_arr = sim_arr, sdss_arr = sdss_arr, mle_arr = mle_arr, $
	dopoormle = dopoormle, dopoorsdss = dopoorsdss, file = file, $
	rnames = rnames, source_model = source_model, target_model = target_model, tolfactor = tolfactor
name = "plot_poor_fits"
if (n_elements(sim_arr) eq 0 or n_elements(sdss_arr) eq 0 or n_elements(mle_arr) eq 0) then begin
	print, name, ": ERROR - null input"
	return
endif
if (n_elements(file) eq 0) then begin
	print, name, ": ERROR - null file - no output can be made"
	return
endif


nrec = n_elements(rnames)
allpoorindex_mle = [-1L]
allpoorindex_sdss = [-1L]

sim_arr0 = sim_arr[0];
;; making a list of poor fits
for irec=0, nrec - 1, 1 do begin
	rname = rnames[irec];
	;; number of the index in each of the structures 
	case rname of
		"MAG_DEV": begin
			if (sim_arr0.modelname1 eq "deV") then begin
				xrname = "MAG1"
			endif
			if (sim_arr0.modelname1 eq "exp" and sim_arr0.modelname2 eq "deV") then begin
				xrname = "MAG2"
			endif
			xunit = " ()"	
			end
		"COUNTS_DEV": begin
			if (sim_arr0.modelname1 eq "deV") then begin
				xrname = "COUNTS1"
			endif
			if (sim_arr0.modelname1 eq "exp" and sim_arr0.modelname2 eq "deV") then begin
				xrname = "COUNTS2"
			endif
			xunit = " (photons)"	
			end
		"RE_DEV": begin
			if (sim_arr0.modelname1 eq "deV") then begin
				xrname = "RE1"
			endif
			if (sim_arr0.modelname1 eq "exp" and sim_arr0.modelname2 eq "deV") then begin
				xrname = "RE2"
			endif	
			xunit = " (pixel)"
			if (keyword_set(doarcsec)) then xunit = " (arcsec)"
			end
		"E_DEV": begin
			if (sim_arr0.modelname1 eq "deV") then begin
				xrname = "E1"
			endif
			if (sim_arr0.modelname1 eq "exp" and sim_arr0.modelname2 eq "deV") then begin
				xrname = "E2"
			endif	
			xunit = " "
			if (keyword_set(doarcsec)) then xunit = " (arcsec)"
			end

		"MAG_EXP": begin
			if (sim_arr0.modelname1 eq "exp") then begin
				xrname = "MAG1"
			endif
			if (sim_arr0.modelname1 eq "deV" and sim_arr0.modelname2 eq "exp") then begin
				xrname = "MAG2"
			endif	
			xunit = " ()"
			end
		"COUNTS_EXP": begin
			if (sim_arr0.modelname1 eq "exp") then begin
				xrname = "COUNTS1"
			endif
			if (sim_arr0.modelname1 eq "deV" and sim_arr0.modelname2 eq "exp") then begin
				xrname = "COUNTS2"
			endif
			xunit = " (photons)"	
			end
		"RE_EXP": begin
			if (sim_arr0.modelname1 eq "exp") then begin
				xrname = "RE1"
			endif
			if (sim_arr0.modelname1 eq "deV" and sim_arr0.modelname2 eq "exp") then begin
				xrname = "RE2"
			endif
			xunit = " (pixel)"
			if (keyword_set(doarcsec)) then xunit = " (arcsec)"
			end
		"E_EXP": begin
			if (sim_arr0.modelname1 eq "exp") then begin
				xrname = "E1"
			endif
			if (sim_arr0.modelname1 eq "deV" and sim_arr0.modelname2 eq "deV") then begin
				xrname = "E2"
			endif	
			xunit = " "
			if (keyword_set(doarcsec)) then xunit = " (arcsec)"
			end
		"MAG_STAR": begin
			if (sim_arr0.modelname1 eq "star") then begin
				xrname = "MAG1"
			endif else begin
			if (sim_arr0.modelname2 eq "star") then begin
				xrname = "MAG2"
			endif	
			endelse
			xunit = " ()"
			end
		"COUNTS_STAR": begin
			if (sim_arr0.modelname1 eq "star") then begin
				xrname = "COUNTS1"
			endif else begin
			if (sim_arr0.modelname2 eq "star") then begin
				xrname = "COUNTS2"
			endif
			endelse
			xunit = " (photons)"	
			end

		else: print, name, ": unsupported argument option ", rname
	endcase
	xnames=tag_names(sim_arr0)
	xindex=where(strcmp(xnames,xrname) EQ 1)
		
	ynames1=tag_names(mle_arr[0])
	yindex1=where(strcmp(ynames1,rname) EQ 1)
	
	ynames2=tag_names(sdss_arr[0])
	yindex2=where(strcmp(ynames2,rname) EQ 1)
	
	x = sim_arr[*].(xindex)
	y1 = mle_arr[*].(yindex1)
	y2 = sdss_arr[*].(yindex2)
 	;; bad - negative numbers - from mle and sdss
	
	if (keyword_set(dopoormle)) then begin
		poorindex = where((y1 gt x * tolfactor) or (y1 lt x / tolfactor))
		if (poorindex[0] eq -1) then begin
			npoor = 0
		endif else begin
			npoor = n_elements(poorindex2)	
			allpoorindex_mle = [allpoorindex_mle, poorindex]
		endelse 
		print, name, ": MLE fit results include ", npoor, " poor fits"
	endif
	if (keyword_set(dopoorsdss)) then begin
		poorindex = where((y2 gt x * tolfactor) or (y2 lt x / tolfactor))
		if (poorindex[0] eq -1) then begin
			npoor = 0
		endif else begin
			npoor = n_elements(poorindex)	
			allpoorindex_sdss = [allpoorindex_sdss, poorindex]
		endelse 
		print, name, ": PHOTO fit results include ", npoor, " poor fits"
	endif
	
endfor

;; now analyzing poor indices
;; making unique arrays
npoor_mle = n_elements(allpoorindex_mle)
npoor_sdss = n_elements(allpoorindex_sdss)

if (npoor_mle le 1) then begin
	print, name, ": MLE - no poor mle results were found"
	npoor_mle = 0
endif else begin
	allpoorindex_mle = allpoorindex_mle[1:npoor_mle - 1L]
	uindex = uniq(allpoorindex_mle)
	allpoorindex_mle = allpoorindex_mle[uindex]
	counts_mle = 1.0D * sim_arr[allpoorindex_mle].counts1
	mag_mle = 1.0D * sim_arr[allpoorindex_mle].mag1
	re_mle = sim_arr[allpoorindex_mle].re1
	smallindex = where(counts_mle eq 0.0)
	nsmall = n_elements(smallindex)
	if (smallindex[0] eq -1) then nsmall = 0
	if (nsmall gt 0) then counts_mle[smallindex] = 0.1
	print, name, ": MLE simulation counts equal to 0.0 #", nsmall
	print, name, ": MLE simulations (poor) #", npoor_mle
endelse

if (npoor_sdss le 1) then begin
	print, name, ": PHOTO - no poor photo results were found"
	npoor_sdss = 0
endif else begin
	allpoorindex_sdss = allpoorindex_sdss[1:npoor_sdss - 1L]
	uindex = uniq(allpoorindex_sdss)
	allpoorindex_sdss = allpoorindex_sdss[uindex]
	counts_sdss = 1.0D * sim_arr[allpoorindex_sdss].counts1
	mag_sdss = 1.0D * sim_arr[allpoorindex_sdss].mag1
	re_sdss = sim_arr[allpoorindex_sdss].re1
	smallindex = where(counts_sdss eq 0.0)
	nsmall = n_elements(smallindex)
	if (smallindex[0] eq -1) then nsmall = 0
	if (nsmall gt 0) then counts_sdss[smallindex] = 0.1
	print, name, ": PHOTO simulation counts equal to 0.0 #", nsmall
	print, name, ": PHOTO simulations (poor) #", npoor_sdss
endelse

;; the ranges should be that of the whole simulations including the good fits so that 
;; the bad fit scatter plot shows how much of the phases space is not reachable
mag_sim = sim_arr.mag1
re_sim = sim_arr.re1
e_sim = sim_arr.e1
lgre_sim = alog10(re_sim)
counts_sim = sim_arr.counts1
lgcounts_sim = alog10(sim_arr.counts1)

;; debugging
mag_range = get_range(sim_arr.mag1)
counts_range = get_range(sim_arr.counts1)
lgcounts_range = get_range(lgcounts_sim)
re_range = get_range(sim_arr.re1)
lgre_range = get_range(lgre_sim)
e_range = get_range(e_sim)

chisq_sdss = sdss_arr.chisq
chisq_mle = mle_arr.chisq
nstep_mle = mle_arr.nstep

badchisq = where(chisq_sdss eq 0.0 or chisq_mle eq 0.0)
goodchisq = where(chisq_sdss ne 0.0 and chisq_mle ne 0.0)
print, name, ": # of chisq = 0 roots:  ", n_elements(badchisq)
print, name, ": # of good chisq point: ", n_elements(goodchisq)

xrange = mag_range
yrange = re_range

xsize= 20
ysize= 20
symsize=2.0 / 20.0 * xsize

if (keyword_set(verbose)) then print, name, ": setting up graphic device, xsize = ", xsize, ", ysize = ", ysize
print, name, ": writing to file ", file
olddevice = !d.name ; store the current device name in a variable
set_plot, 'PS' ; switch to the postscript device
Device, DECOMPOSED=decomposed, COLOR=1, BITS_PER_PIXEL=8
device, /encapsulated, file=file, font_size = !my_font_size; specify some details, give the file a name
loadct, ct, /silent
device, xsize=xsize, ysize=ysize

multiplot, [1, 1], /rowmajor, $
mTitSize = !my_font_size, mxTitSize = !my_font_size, myTitSize = !my_font_size, $
myTitOffset = !myTitOffset, mxTitOffset = !mxTitOffset

print, name, ": xrange = ", xrange[0], xrange[1], ", yrange = ", yrange[0], yrange[1]
if (npoor_mle ne 0 and keyword_set(dopoormle)) then plot, mag_mle[allpoorindex_mle], re_mle[allpoorindex_mle], psym = 5, symsize = symsize, xrange = xrange, yrange = yrange, /ylog, xstyle = 4, ystyle = 4
if (npoor_sdss ne 0 and keyword_set(dopoorsdss)) then plot, mag_sdss[allpoorindex_sdss], re_sdss[allpoorindex_sdss], psym = 1, symsize = symsize, xrange = xrange, yrange = yrange, /ylog, xstyle = 4, ystyle = 4
;; plotting the box
fpCCstr = "(fpC)"
if (keyword_set(fit2fpCC)) then fpCCstr = "(fpCC)"
title="POOR FITS for source = "+source_model+fpCCstr+", target = "+target_model+", tol factor = "+strtrim(STRING(tolfactor, FORMAT='(F5.2)'), 2)
plot, [1], [1], xrange = xrange, yrange = yrange, /ylog, title = title, font = 2, xtickformat = tfmt, ytickformat = tfmt, xtitle = "MAG (maggies)", ytitle = "RE", /nodata
multiplot, /reset

if (keyword_set(verbose)) then print, name, ": closing device"
device, /close ; close the file
set_plot, olddevice ; go back to the graphics device
command="epstopdf "+file
spawn, command

end


pro dither_xy, data, dolog = dolog, seed = seed
name = "dither_xy"
return

ndither=50.0
ndata = n_elements(data)
if (ndata lt 5) then begin
	print, name, ": ERROR - array should be of at least size 5 to dither"
	return
endif

fndex = finite(data)
gndex = where(fndex eq 1, gsize)
if (gsize ne ndata) then begin
	print, name, ": ERROR - array should all be defined, found NaN or Inf values"
	stop
	return
endif

unique_array = uniq(data[sort(data)])
nunique = n_elements(unique_array)
index = lindgen(nunique - 1L)
indexp1 = index + 1
if (not keyword_set(dolog)) then begin
	distance = min(abs(unique_array[indexp1] - unique_array[index]))
	if (distance eq 0.0) then begin
		print, name, ": WARNING - all elements are the same, don't know the radius for dithering"
		return
	endif
	dither = distance / ndither
	data = data + dither * randomn(seed, ndata)
endif else begin
	nbaddata = 0
	badindex = where(data le 0.0, nbaddata)
	if (nbaddata gt 0) then begin
		print, name, ": ERROR - log cannot be applied to array"
		return
	endif
	ldata = alog10(data)
	distance = min(abs(alog10(unique_array[indexp1]) - alog10(unique_array[index])))
	if (distance eq 0.0) then begin
		print, name, ": WARNING - all elements are the same, don't know the radius for dithering"
		return
	endif
	dither = distance / ndither
	ldata = ldata + dither * randomn(seed, ndata)
	data = 10.0 ^ ldata

endelse

fndex = finite(data)
gndex = where(fndex eq 1, gsize)
if (gsize ne ndata) then begin
	print, name, ": ERROR - array should all be defined, found NaN or Inf values after dithering"
	return
endif



return
end 

pro do_error_contour_plots, file = file, source_model = source_model, target_model = target_model, $
	sim_arr = sim_arr, sdss_arr = sdss_arr, mle_arr = mle_arr, $
	n_level = n_level, nbin = nbin, gndex = gndex, dogood = dogood, $
	domag = domag, doarcsec = doarcsec, doell = doell, $
	catell = catell, catre = catre, catlum = catlum, ncat = ncat, $
	fit2fpCC = fit2fpCC, comment_string = comment_string, $
	uname = uname, urange = urange, seed = seed, random = random, bias = bias, rnderror = rnderror, prob = prob, $
	doraw = doraw, dosmooth = dosmooth, dohisto = dohisto, doshisto = doshisto, $
	doabsolute = doabsolute, dorelative = dorelative, dopositive = dopositive, $
	dolog = dolog, dographicallog = dographicallog, docolor = docolor, docompare = docompare

name = "do_error_contour_plots"
 
min_number_points_for_plot = 10

if (n_elements(sim_arr) eq 0 or n_elements(sdss_arr) eq 0 or n_elements(mle_arr) eq 0) then begin
	print, name, ": ERROR - null input"
	return
endif
if (n_elements(sim_arr) ne n_elements(sdss_arr) or n_elements(sim_arr) ne n_elements(mle_arr)) then begin
	print, name, ": ERROR - SDSS, MLE and SIM arrays should be of the same size"
	return
endif
if (n_elements(file) eq 0) then begin
	print, name, ": ERROR - null filename - no export can be done"
	return
endif
if (n_elements(n_level) eq 0) then n_level = 10
if (n_elements(nbin) eq 0) then nbin = 50

if (not (keyword_set(doraw) or keyword_set(dosmooth) or keyword_set(dohisto) or keyword_set(doshisto))) then begin
	print, name, ": ERROR - at least one type of plot should be specified"
	return
endif
if (keyword_set(random) + keyword_set(rnderror) + keyword_set(bias) + keyword_set(prob) ne 1) then begin
	print, name, ": ERROR - one and only of the keywords random, rnderror or bia should be set"
	error = -1
	return
endif

double_model_check = strpos(target_model, '-')
if (double_model_check ge 0) then begin
	switch uname of
		'xc':
		'yc': 
		'drc':
		'dxc':
		'dyc':
		'mag':
		'counts': begin
			print, name, ": WARNING - limited unames supported for '", target_model, "'"
			if (keyword_set(verbose)) then print, name, ": COMMENT - uname = '", uname, "' supported for '", target_model, "'"
			break
			end
		else: 	begin
			print, name, ": ERROR - uname = '", uname, "' not supported for '", target_model, "'"
			return
			break
			end
	endswitch
endif else begin
	switch uname of 
		'drc':
		'dxc':
		'dyc': begin
			print, name, ": ERROR - uname = '", uname, "' not supported for '", target_model, "'"
			return
			break
			end
		else:	begin
			break
			end
	endswitch
endelse

;; counting the categorization keywords set
cat_keyword_num = 0
cat_keyword_num += keyword_set(catell)
cat_keyword_num += keyword_set(catre)
cat_keyword_num += keyword_set(catlum)
if (cat_keyword_num ne 1) then begin
	print, name, ": ERROR - expecting only 1 categorization keyword to be set while ", cat_keyword_num, " was set"
	stop
endif
;; deciding which plots to make
dolum = 1 - keyword_set(catlum)
dore = 1 - keyword_set(catre)
doell = 1 - keyword_set(catell)

;; combining structures all together
if (keyword_set(verbose)) then begin
print, name, ": combining input arrays into one structure"
print, name, ": # (sim_arr) = ", n_elements(sim_arr)
print, name, ": # (mle_arr) = ", n_elements(mle_arr)
print, name, ": # (sdss_arr)= ", n_elements(sdss_arr)
print, name, ": info on chisq (sdss_arr) : min = ", min(sdss_arr.chisq), ", max = ", max(sdss_arr.chisq)
endif

comb_array1 = combine_array(array1 = sim_arr, suffix1 = "_sim", array2 = mle_arr, suffix2 = "_mle")
if (keyword_set(verbose)) then print, name, ": # (sim_arr ^ mle_arr)    = ", n_elements(comb_array1)
rawdata = combine_array(array1 = comb_array1, suffix1 = "", array2 = sdss_arr, suffix2 = "_sdss")
if (keyword_set(verbose)) then print, name, ": # (comb_arr1 ^ sdss_arr) = ", n_elements(rawdata)
comb_array1 = -1

error = 0
if (keyword_set(catell)) then begin
	if (keyword_set(verbose)) then print, name, ": creating cat variable: 'cat_e1_sim'"
	rawdata.cat_e1_sim = create_cat_variable(rawdata.e1_sim, ncat = ncat, error = error)
	inrecords = ["counts_sim", "counts1_sim", "counts2_sim", $
			"mag_sim", "mag1_sim", "mag2_sim", $
			"cat_e1_sim", "re1_sim", "re2_sim"]
	outrecords = ["e1_sim", "e2_sim", "chisq_mle", "nstep_mle", "chisq_sdss", $
		"re_dev_mle", "re_exp_mle", "re_dev_sdss", "re_exp_sdss", $
		"mag_dev_mle", "mag_exp_mle", "mag_mle", "mag_dev_sdss", "mag_exp_sdss", "mag_sdss", $
		"counts_dev_mle", "counts_exp_mle", "counts_mle", "counts_dev_sdss", "counts_exp_sdss", "counts_sdss", $
		"e_dev_mle", "e_exp_mle", "e_dev_sdss", "e_exp_sdss", "fracpsf_sdss", $
		"phi1_sim", "phi2_sim", "phi_dev_mle", "phi_exp_mle", "phi_dev_sdss", "phi_exp_sdss", $
		"xc1_sim", "yc1_sim", "xc_dev_mle", "yc_dev_mle", "xc_exp_mle", "yc_exp_mle", $
		"xc_dev_sdss", "yc_dev_sdss", "xc_exp_sdss", "yc_exp_sdss", $ 
		"dxc_sim", "dyc_sim", "drc_sim", "dxc_mle", "dyc_mle", "drc_mle", "dxc_sdss", "dyc_sdss", "drc_sdss", $
		"counts_star_mle", "mag_star_mle", "counts_star_sdss", "mag_star_sdss"]
	catrecord = "e1_sim"
endif
if (keyword_set(catre)) then begin
	if (keyword_set(verbose)) then print, name, ": creating cat variable: 'cat_re_sim'"
	rawdata.cat_re1_sim = create_cat_variable(rawdata.re1_sim, ncat = ncat, error = error)
	inrecords = ["counts_sim", "counts1_sim", "counts2_sim", $
		"mag_sim", "mag1_sim", "mag2_sim", $
		"e1_sim", "e2_sim", "cat_re1_sim"]
	outrecords = ["re1_sim", "re2_sim", "chisq_mle", "nstep_mle", "chisq_sdss", $
		"re_dev_mle", "re_exp_mle", "re_dev_sdss", "re_exp_sdss", $
		"mag_dev_mle", "mag_exp_mle", "mag_mle", "mag_dev_sdss", "mag_exp_sdss", "mag_sdss", $
		"counts_dev_mle", "counts_exp_mle", "counts_mle", "counts_dev_sdss", "counts_exp_sdss", "counts_sdss", $
 		"e_dev_mle", "e_exp_mle", "e_dev_sdss", "e_exp_sdss", "fracpsf_sdss", $	
		"phi1_sim", "phi2_sim", "phi_dev_mle", "phi_exp_mle", "phi_dev_sdss", "phi_exp_sdss", $
		"xc1_sim", "yc1_sim", "xc_dev_mle", "yc_dev_mle", "xc_exp_mle", "yc_exp_mle", $
		"xc_dev_sdss", "yc_dev_sdss", "xc_exp_sdss", "yc_exp_sdss",  $
		"dxc_sim", "dyc_sim", "drc_sim", "dxc_mle", "dyc_mle", "drc_mle", "dxc_sdss", "dyc_sdss", "drc_sdss", $
		"counts_star_mle", "mag_star_mle", "counts_star_sdss", "mag_star_sdss"]
	catrecord = "re1_sim"
endif
if (keyword_set(catlum)) then begin	
	if (keyword_set(domag)) then begin
		if (keyword_set(verbose)) then print, name, ": creating cat variable: 'cat_mag_sim'"
		rawdata.cat_mag1_sim = create_cat_variable(rawdata.mag1_sim, ncat = ncat, error = error)
		inrecords = ["cat_mag1_sim", "e1_sim", "e2_sim", "re1_sim", "re2_sim"] 
		outrecords = ["counts_sim", "counts1_sim", "counts2_sim", $
			"mag_sim", "mag1_sim", "mag2_sim", "chisq_mle", "nstep_mle", "chisq_sdss", $
			"re_dev_mle", "re_exp_mle", "re_dev_sdss", "re_exp_sdss", $
			"mag_dev_mle", "mag_exp_mle", "mag_mle", "mag_dev_sdss", "mag_exp_sdss", "mag_sdss", $
			"counts_dev_mle", "counts_exp_mle", "counts_mle", "counts_dev_sdss", "counts_exp_sdss", "counts_sdss", $
			"e_dev_mle", "e_exp_mle", "e_dev_sdss", "e_exp_sdss", "fracpsf_sdss", $		
			"phi1_sim", "phi2_sim", "phi_dev_mle", "phi_exp_mle", "phi_dev_sdss", "phi_exp_sdss", $
			"xc1_sim", "yc1_sim", "xc_dev_mle", "yc_dev_mle", "xc_exp_mle", "yc_exp_mle", $
			"xc_dev_sdss", "yc_dev_sdss", "xc_exp_sdss", "yc_exp_sdss", $
			"dxc_sim", "dyc_sim", "drc_sim", "dxc_mle", "dyc_mle", "drc_mle", "dxc_sdss", "dyc_sdss", "drc_sdss", $
			"counts_star_mle", "mag_star_mle", "counts_star_sdss", "mag_star_sdss"]

	catrecord = "mag1_sim"	
	endif else  begin
		if (keyword_set(verbose)) then print, name, ": creating cat variable: 'cat_counts_sim'"
		rawdata.cat_counts1_sim = create_cat_variable(rawdata.counts1_sim, ncat = ncat, error = error)
		inrecords = ["cat_counts1_sim", "e1_sim", "e2_sim", "re1_sim", "re2_sim"]
		outrecords = ["counts_sim", "counts1_sim", "counts2_sim", $
			"mag_sim", "mag1_sim", "mag2_sim", "chisq_mle", "nstep_mle", "chisq_sdss", $
			"re_dev_mle", "re_exp_mle", "re_dev_sdss", "re_exp_sdss", $
			"mag_dev_mle", "mag_exp_mle", "mag_mle", "mag_dev_sdss", "mag_exp_sdss", "mag_sdss", $
			"counts_dev_mle", "counts_exp_mle", "counts_mle", "counts_dev_sdss", "counts_exp_sdss", "counts_sdss", $
			"e_dev_mle", "e_exp_mle", "e_dev_sdss", "e_exp_sdss", "fracpsf_sdss", $	
			"phi1_sim", "phi2_sim", "phi_dev_mle", "phi_exp_mle", "phi_dev_sdss", "phi_exp_sdss", $
			"xc1_sim", "yc1_sim", "xc_dev_mle", "yc_dev_mle", "xc_exp_mle", "yc_exp_mle", $
			"xc_dev_sdss", "yc_dev_sdss", "xc_exp_sdss", "yc_exp_sdss", $ 	
			"dxc_sim", "dyc_sim", "drc_sim", "dxc_mle", "dyc_mle", "drc_mle", "dxc_sdss", "dyc_sdss", "drc_sdss", $
			"counts_star_mle", "mag_star_mle", "counts_star_sdss", "mag_star_sdss"]
		catrecord = "counts1_sim"
	endelse
endif
if (error ne 0) then begin
	print, name, ": ERROR - problem categorizing '", catrecord, "'"
	return
endif

;; the following command collapses the array of structures on a certain number of its records
collapse_index = [-1]
data = collapse_array(data = rawdata, innames = inrecords, outnames = outrecords, $
	seed = seed, random = keyword_set(random), bias = keyword_set(bias) + keyword_set(rnderror) + keyword_set(prob), $
	collapse_index = collapse_index)
;; rawdata = -1

;; the ranges should be that of the whole simulations including the good fits so that 
;; this test seems to fail
if (1 eq 0) then begin
	print, name, ": COMMENT - testing collapsed array"
	catname = "cat_"+catrecord
	my_tagnames = tag_names(data[0])
	rcat = where(strlowcase(my_tagnames) eq strlowcase(catname))
	rrcat = where(strlowcase(my_tagnames) eq strlowcase(catrecord))
	for icat = 1, ncat - 1L, 1L do begin
		catindex = where(data.(rcat) eq float(icat))
		if (catindex[0] ne -1) then begin
			min_catrecord = min(data[catindex].(rrcat))
			max_catrecord = max(data[catindex].(rrcat))
			print, name, ": catvalue = ", icat, ", catrecord = '", catrecord, $
				"', range = [", min_catrecord, ", ", max_catrecord, "]"
		endif
	endfor
endif

mag_sim = data.mag1_sim
re_sim = data.re1_sim
lgre_sim = alog10(re_sim)
lgre_sim = re_sim ;; doing log by the plot command itself
counts_sim = data.counts1_sim
lgcounts_sim = alog10(counts_sim)
lgcounts_sim = counts_sim ;; doing log by the plot command itself
e_sim = data.e1_sim


;; debugging
mag_range = get_range(mag_sim)
counts_range = get_range(counts_sim)
lgcounts_range = get_range(lgcounts_sim)
re_range = get_range(re_sim)
lgre_range = get_range(lgre_sim)
e_range = get_range(e_sim)

;; defining u parameter
tags = strlowcase(tag_names(data[0]))
rawtags = strlowcase(tag_names(rawdata[0]))

do_sdss_error = 1
switch uname of
	'xc':
	'yc':   begin
		s_target_model = strsplit(target_model, "-", /extract)
		uname_sim  = uname + '1'
		uname_mle  = uname + '_' + s_target_model[0]
		uname_sdss = uname + '_' + s_target_model[0]
		break
		end
	're':   begin
		uname_sim  = uname + '1'
		uname_mle  = uname + '_' + target_model
		uname_sdss = uname + '_' + target_model 
		break
		end
	 'phi':
	 'e': 	begin
		uname_sim  = uname + '1'
		uname_mle  = uname + '_' + target_model	
		uname_sdss = uname + '_' + target_model 
		do_sdss_error = 1
		break
		end
	'counts':
	 'mag':	begin 
		uname_sim = uname 
		uname_mle = uname 
		uname_sdss = uname 
		break
		end
	'drc': begin
		uname_sim = uname
		uname_mle = uname
		uname_sdss = uname
		break
		end
	'dxc': begin
		uname_sim = uname
		uname_mle = uname
		uname_sdss = uname
		break
		end
	'dyc': begin
		uname_sim = uname
		uname_mle = uname
		uname_sdss = uname
		break
		end
	else:	begin
		print, name, "WARNING - uname = '", uname, "' should be explicitly present in simulation, MLE and SDSS arrays"
		uname_sim = uname
		uname_mle = uname
		uname_sdss = uname
		break
		end
endswitch		
tag_sim = strlowcase(uname_sim+"_sim")
tag_mle = strlowcase(uname_mle+"_mle")
tag_sdss = strlowcase(uname_sdss+"_sdss")

ctag_sim = 0
ctag_mle = 0
ctag_sdss = 0
i_sim = where(rawtags eq tag_sim, ctag_sim)
i_mle = where(rawtags eq tag_mle, ctag_mle)
i_sdss = where(rawtags eq tag_sdss, ctag_sdss)

if (ctag_sim eq 0) then begin
	print, name, ": ERROR - could not find tag '", tag_sim, "' in the raw data"
	return
endif
if (ctag_mle eq 0) then begin
	print, name, ": ERROR - could not find tag '", tag_mle, "' in the raw data"
	return
endif
if (ctag_sdss eq 0 and do_sdss_error eq 1) then begin
	print, name, ": ERROR - could not find tag '", tag_sdss, "' in the raw data"
	return
endif
var_sim = rawdata.(i_sim)
var_mle = rawdata.(i_mle)

if (do_sdss_error eq 1) then var_sdss = rawdata.(i_sdss) else var_sdss = var_mle
if (not keyword_set(dorelative) and not keyword_set(doabsolute)) then begin
	dorelative = 0
	doabsolute = 0
	if (keyword_set(verbose)) then print, name, ": WARNING - deciding whether to plot absolute or relative error for uname = '", uname, "'"
	switch strlowcase(uname) of
		'xc':
		'yc': 	begin
			doabsolute = 1
			break
			end
		're':   begin
			dorelative = 1
			break
			end
		'phi':
		'e':	begin
			doabsolute = 1
			break
			end
		'mag':	begin
			doabsolute = 1
			break
			end
		'counts': begin
			dorelative = 1
			break
			end
		'drc': begin
			doabsolute = 1
			break
			end
		'dxc': begin
			doabsolute = 1
			break
			end
		'dyc': begin
			doabsolute = 1
			break
			end
		else:	begin 
			print, name, ": ERROR - 'doabsolute' or 'dorelative' keywords should be set for uname = '", uname, "'"
			return
			break
			end
	endswitch
endif else begin
	if (keyword_set(dorelative)) then dorelative = 1 else dorelative = 0
	if (keyword_set(doabsolute)) then doabsolute = 1 else doabsolute = 0
endelse
if (dorelative or doabsolute) then begin
	if (dorelative and doabsolute) then begin
		print, name, ": ERROR - both 'doabsolute' and 'dorelative' keywords cannot be set at the same time"
		return
	endif
	if (dorelative) then begin
		u_mle = (var_mle - var_sim) / var_sim
		u_sdss = (var_sdss - var_sim) / var_sim
	endif
	if (doabsolute) then begin
		u_mle = var_mle - var_sim
		u_sdss = var_sdss - var_sim
		if (uname eq "phi") then begin
			u_mle = 180.0 /!pi * atan(tan(!pi * u_mle / 180.0))
			u_sdss = 180.0 / !pi * atan(tan(!pi * u_sdss / 180.0))
		endif
	endif
	if (keyword_set(dopositive)) then begin
		u_mle = abs(u_mle)
		u_sdss = abs(u_sdss)
	endif
endif

u_mle = collapse_array_with_index(data = u_mle, collapse_index = collapse_index, $
	seed = seed, random = keyword_set(random), bias = keyword_set(bias), rnderror = keyword_set(rnderror), prob = keyword_set(prob))
u_sdss = collapse_array_with_index(data = u_sdss, collapse_index = collapse_index, $
	seed = seed, random = keyword_set(random), bias = keyword_set(bias), rnderror = keyword_set(rnderror), prob = keyword_set(prob))

u_ratio = abs(u_mle) / (abs(u_sdss) + 1.0E-4)
do_compare = keyword_set(docompare)

;; calculating the range
u_mle_range = get_range(u_mle)
u_sdss_range = get_range(u_sdss)		

if (n_elements(urange) eq 0) then begin
	u_range = get_range([u_mle_range, u_sdss_range])
endif else begin
	u_range = urange
endelse

u_color_range = u_range

u_ratio_range = [-1.0, 1.0]
u_ratio_color_range = [-1.0, 1.0]
	
if (dorelative) then begin
	if (u_range[1] gt 10.0 or u_range[0] lt -10.0) then begin
		frange = [0.025, 0.975]
		print, name, ": WARNING - replacing u_range with ", frange, " fraction of the data"
		print, name, ": WARNING - (old) u_range = ", u_range
		u_color_range = get_range([u_mle, u_sdss], frange = frange)
		print, name, ": WARNING - (new) u_range = ", u_range
	endif
	if (u_color_range[1] gt 10.0) then begin
		u_color_range[1] = 10.0
		if (u_color_range[0] lt -10) then u_color_range[0] = -10.0
		if (u_color_range[0] ge u_color_range[1]) then u_color_range[0] = 0.0
		print, name, ": WARNING - u_color_range corrected"
	endif
	if (u_color_range[0] lt -10.0) then begin
		u_color_range[0] = -10.0
		if (u_color_range[0] ge u_color_range[1]) then u_color_range[1] = 0.0
		print, name, ": WARNING - u_color_range corrected"
	endif
endif
reverse_ct = 0
if (keyword_set(docolor) and not keyword_set(dopositive)) then begin
	;; if (u_range[1] * u_range[0] lt 0.0) then begin
		if (keyword_set(verbose)) then $
		print, name, ": setting up a color map"
		big = max(abs(u_color_range))
		u_color_range[0] = -big
		u_color_range[1] = big
		ct = 16
endif else begin 
	print, name, ": using greyscale"
	ct = 0
endelse
 
decomposed = 0 ;; color decomposition

my_fracs = findgen(n_level+1) / (n_level)

;; getting levels from function get_contour_levels
if (keyword_set(verbose)) then print, name, ": invoking 'get_contour_levels' function"
u_levels = get_contour_levels([u_mle, u_sdss], range = u_color_range, n_level = n_level) ;; /dohisto
if (keyword_set(verbose)) then print, name, ": COMMENT - u_levels = ", u_levels[0: min([5, n_elements(u_levels) - 1])]
u_ratio_levels = get_contour_levels(u_ratio, range = u_ratio_color_range, n_level = n_level) ;; /dohisto
if (keyword_set(verbose)) then print, name, ": COMMENT - u_ratio_levels = ", u_ratio_levels[0: min([5, n_elements(u_levels) - 1])]

;; smooth_chisq_range = chisq_range
smooth_u_range = u_range
smooth_u_ratio_range = u_ratio_range


n1 = 0
n2 = 1
if (keyword_set(doraw)) then n1++
if (keyword_set(dosmooth)) then n1++
if (keyword_set(dohisto)) then n1++
if (keyword_set(doshisto)) then n1++
if (keyword_set(allcats_in_one_file)) then n1 = ncat * n1
if (do_sdss_error eq 1) then n2++
if (do_compare) then n2++

aspect_ratio=(1.0 * n1) / n2
xsize= !my_xsize * n1 / 2.0 ;; 45
ysize= !my_ysize * n2 / 2.0 ;; 35
symsize=1.0 / 20.0 * xsize
gap = !my_gap

;; fixing max_nstep
if (n_elements(force_nstep_max) eq 1) then begin
	print, name, ": forcing nstep max = ", force_nstep_max
	nstep_max_index = where(nstep_mle ge force_nstep_max)
	if (nstep_max_index[0] ne -1) then nstep_mle[nstep_max_index] = force_nstep_max
endif

if (keyword_set(catell)) then begin
	catname = "cat_e1_sim"
	if (keyword_set(verbose)) then print, name, ": cat-name = ", catname
	;; setting the x-axis
	if (keyword_set(verbose)) then 	print, name, ": x-axis shows brightness"
	if (keyword_set(domag)) then begin
		x = mag_sim
		doxlog = 0
		xrange = get_range(mag_sim)
		xtitle = "mag"
	endif else begin
		x = lgcounts_sim
		doxlog = 1
		xrange = get_range(counts_sim)
		xtitle = "COUNTS (photons)"
	endelse
	;; setting the y-axis
	if (keyword_set(verbose)) then print, name, ": y-axis shows size"
	y = lgre_sim
	yrange = get_range(re_sim)
	if (keyword_set(doarcsec)) then begin
		ytitle = "RE (arcsec)"
	endif else begin
		ytitle = "RE (pixel)"
	endelse
	doylog = 1
endif
if (keyword_set(catre)) then begin
	catname = "cat_re1_sim"
	if (keyword_set(verbose)) then print, name, ": cat-name = ", catname
	;; setting the x-axis
	if (keyword_set(verbose)) then print, name, ": x-axis shows ellipticity"
	x = e_sim
	xrange = get_range(e_sim)
	xtitle = "ellipticity"
	doxlog = 0
	;; setting the y-axis	
	if (keyword_set(verbose)) then print, name, ": y-axis shows brightness"
	if (keyword_set(domag)) then begin
		y = mag_sim
		doylog = 0
		yrange = get_range(mag_sim)
		ytitle = "magnitude"
	endif else begin
		y = lgcounts_sim
		doylog = 1
		yrange = get_range(counts_sim)
		ytitle = "COUNTS (photons)"
	endelse
endif
if (keyword_set(catlum)) then begin
	if (keyword_set(domag)) then catname = "cat_mag1_sim" else catname = "cat_counts1_sim"
	if (keyword_set(verbose)) then print, name, ": cat-name = ", catname
	;; setting the x-axis
	if (keyword_set(verbose)) then print, name, ": x-axis shows ellipticity"
	x = e_sim
	xrange = get_range(e_sim)
	xtitle = "ellipticity"
	doxlog = 0
	;; setting the y-axis
	if (keyword_set(verbose)) then print, name, ": y-axis shows size"
	y = lgre_sim
	yrange = get_range(re_sim)
	if (keyword_set(doarcsec)) then begin
		ytitle = "RE (arcsec)"
	endif else begin
		ytitle = "RE (pixel)"
	endelse
	doylog = 1
endif


;; now loop over cats
tags = tag_names(data[0])
rcat = where(strlowcase(tags) eq strlowcase(catname))
if (rcat lt 0) then begin
	print, name, ": ERROR - could not find the categorical variable '", catname, "' in the data structure"
	return
endif
all_catvalues = data.(rcat)
catvalues = all_catvalues[uniq(all_catvalues, sort(all_catvalues))]
ncat_unique = n_elements(catvalues)
if (keyword_set(verbose)) then begin
	print, name, ": finding unique cat values for catname = ", catname, "'"
	print, name, ": unique catvalues = ", catvalues
endif
rrcat=where(strlowcase(tags) eq strlowcase(catrecord))
if (rrcat[0] lt 0) then begin
	print, name, ": ERROR - could not find category record '", catrecord, "' in the data structure"
	return
endif
catrecords = data.(rrcat)
;; selecting categories with enough data points
if (keyword_set(verbose)) then print, name, ": finding dense categories, catname = '", catname, "'"
mcat = intarr(ncat_unique)
for icat = 0, ncat_unique - 1L, 1 do begin
	catvalue = catvalues[icat]
	mcat[icat] = n_elements(where(all_catvalues eq catvalue))
endfor
dense_cat_index = where(mcat gt min_number_points_for_plot)
if (dense_cat_index[0] eq -1) then begin
	print, name, ": ERROR - no category with # of data points bigger than ", $
	 min_number_points_for_plot
	return
endif
dense_catvalues = catvalues[dense_cat_index]
ncat_dense = n_elements(dense_catvalues)
if (keyword_set(verbose)) then print, name, ": dense catvalues = ", dense_catvalues

if (keyword_set(verbose)) then begin
	print, name, ": COMMENT - n(mle_arr)  = ", n_elements(mle_arr)
	print, name, ": COMMENT - n(sdss_arr) = ", n_elements(sdss_arr)
	print, name, ": COMMENT - n(sim_arr)  = ", n_elements(sim_arr)
	print, name, ": COMMENT - n(data)     = ", n_elements(data)
endif

;; all_catvalues = -1
mcat = -1

for icat = 0, ncat_dense - 1L, 1 do begin

	;; extracting simulations belonging to this category
	catvalue = dense_catvalues[icat]
	if (keyword_set(verbose)) then print, name, ": catname = ", catname, ", icat = ", icat, ", catvalue = ", catvalue
	;; catindex = where_cat(data, catname = catname, catvalue = catvalue)
	ncatdata = 0
	catindex = where(all_catvalues eq catvalue and finite(x) and finite(y) and finite(u_mle) and finite(u_sdss), ncatdata)
	draw_x = x[catindex]
	draw_y = y[catindex]
	draw_u_mle = u_mle[catindex]
	draw_u_sdss = u_sdss[catindex]
	draw_u_ratio = u_ratio[catindex] ;; comparing the errors

	;; find number of unique positions on the graph for plottings
	;; collapse_ratio = 1.0
	;; collapse_on_x_y, x = draw_x, y = draw_y, array1=draw_u_mle, array2 = draw_u_sdss, array3 = draw_u_ratio, collapse_ratio = collapse_ratio
	dither_xy, draw_x, dolog = keyword_set(doxlog), seed = seed
	dither_xy, draw_y, dolog = keyword_set(doylog), seed = seed


	;; deleting erroneous entries
	im_all = finite(draw_u_mle) * finite(draw_u_sdss) * finite(draw_u_ratio) * finite(draw_x) * finite(draw_y)
	nchosen_index = 0
	choose_index = where(im_all eq 1, nchosen_index)
	print, name, ": COMMENT - finite array elements found: ", nchosen_index
	if (nchosen_index gt 0) then begin
		draw_x = draw_x[choose_index]
		draw_y = draw_y[choose_index]
		draw_u_mle = draw_u_mle[choose_index]
		draw_u_sdss = draw_u_sdss[choose_index]
		draw_u_ratio = draw_u_ratio[choose_index]
	endif
	choose_index = [-1]
	im_all = [-1]

	print, name, ": COMMENT - draw_x (", xtitle, "): ", min(draw_x), max(draw_x), median(draw_x)
	print, name, ": COMMENT - draw_y (", ytitle, "): ", min(draw_y), max(draw_y), median(draw_y)

	;finding overlapping points
	epsilon = 0.005
	GRID_INPUT, draw_x, draw_y, draw_u_mle, draw_x1, draw_y1, draw_u_mle1, DUPLICATES="Avg", epsilon = epsilon 
	GRID_INPUT, draw_x, draw_y, draw_u_sdss, draw_x1, draw_y1, draw_u_sdss1, DUPLICATES="Avg", epsilon = epsilon
	GRID_INPUT, draw_x, draw_y, draw_u_ratio, draw_x1, draw_y1, draw_u_ratio1, DUPLICATES="Avg", epsilon = epsilon

	dm = n_elements(draw_x)
	dm1 = n_elements(draw_x1)

	if (dm ne dm1) then begin ;;  or dm ne dm2 or dm ne dm3) then begin
		print, name, ": adjusting for overlapping points (", dm, ") changed into (", dm1, ") points"
		;; print, name, ": dm = ", dm, ", dm1 = ", dm1, ", dm2 = ", dm2, ", dm3 = ", dm3
		if (keyword_set(verbose)) then begin
			print, name, ": before --- "
			print, name, ": draw_u_mle   (", n_elements(draw_u_mle), ")"
			print, name, ": draw_u_sdss  (", n_elements(draw_u_sdss), ")"
			print, name, ": draw_u_ratio (", n_elements(draw_u_ratio), ")"
			print, name, ": after --- "
			print, name, ": draw_u_mle   (", n_elements(draw_u_mle1), ")"
			print, name, ": draw_u_sdss  (", n_elements(draw_u_sdss1), ")"
			print, name, ": draw_u_ratio (", n_elements(draw_u_ratio1), ")"
		endif
		draw_x = draw_x1
		draw_y = draw_y1
		draw_u_mle = draw_u_mle1
		draw_u_sdss = draw_u_sdss1
		draw_u_ratio = draw_u_ratio1	
	endif
	;; cleaning memory
	draw_x1 = [-1]
	draw_y1 = [-1]
	draw_u_mle1 = [-1]
	draw_u_sdss1 = [-1]
	draw_u_ratio1 = [-1]

	;; reflecting the U range
	if (keyword_set(verbose)) then begin
	print, name, ": U (1) MLE  : min = ", min(draw_u_mle), ", max = ", max(draw_u_mle)
	print, name, ": U (1) SDSS : min = ", min(draw_u_sdss), ", max = ", max(draw_u_sdss)
	print, name, ": COMMENT - fixing draw_data to fit to the u_range = ", u_range
	endif
	error = 0
	fix_range, draw_u_mle, range = u_range, error = error
	if (error ne 0) then begin
		print, name, ": could not fix the dynamic range of draw_u_mle, uname = '", uname, "'"
		return
	endif
	fix_range, draw_u_sdss, range = u_range, error = error
	if (error ne 0) then begin
		print, name, ": could not fix the dynamic range of draw_u_sdss, uname = '", uname, "'"
		return
	endif
	;; extracting catrecord information for this category
	error = 0
	cat_stat = extract_cat_stat(catname = catname, catvalue = catvalue, icat = icat, cat_records = catrecords[catindex], error = error)
	if (error ne 0) then begin
		print, name, ": ERROR - could not extract cat stat for '", catname, "' = ", catvalue
		return
	endif

	catfile = add_cat_to_filename(file, catname = catname, catvalue = catvalue)
	print, name, ": writing to file = ", catfile
	olddevice = !d.name ; store the current device name in a variable
	set_plot, 'PS' ; switch to the postscript device
	Device, DECOMPOSED=decomposed, COLOR=1, BITS_PER_PIXEL=8
	device, /encapsulated, file=catfile, font_size = !my_font_size ; specify some details, give the file a name
	loadct, ct, file = "./colors/colors1.tbl", /silent

	;; size of the region
	ymargin_size = (!Y.Margin[0] + !Y.Margin[1]) * !D.Y_CH_SIZE/ !D.Y_PX_CM 
	xmargin_size = (!X.Margin[0] + !X.Margin[1]) * !D.X_CH_SIZE / !D.X_PX_CM

	my_xgap = !my_xgap / (n1)
	my_ygap = !my_ygap / (n2)
	xsize = !my_xsize
	ysize = (!my_xsize * (1.0 - (n1) * my_xgap) - xmargin_size) * n2 / (n1 * !my_xyratio) + !my_lygap * (n2) + ymargin_size
	my_ygap = !my_lygap / ysize
	
	if (keyword_set(verbose)) then $
	print, name, ": setting up graphic device, xsize = ", xsize, ", ysize = ", ysize, ", [n1, n2] = ", n1, n2
	device, xsize=xsize, ysize=ysize
	multiplot, /reset
	multiplot, [n1, n2], xgap = my_xgap, ygap = my_ygap, $
		mTitSize = !my_font_size, $
		mxTitSize = !my_font_size, myTitSize = !my_font_size, $ 
		myTitOffset = !myTitOffset, mxTitOffset = !mxTitOffset
	
	nx = 200
	ny = 200

	if (keyword_set(verbose)) then print, name, ": determining the ranges and the bounds"
	xrange = get_range(draw_x)
	yrange = get_range(draw_y)
	fractions = [0.01, 0.99]
	smooth_xrange0 = get_range(draw_x)
	smooth_yrange0 = get_range(draw_y)
	smooth_xrange = quantile(fractions, draw_x, range = smooth_xrange0, /nosort)
	smooth_yrange = quantile(fractions, draw_y, range = smooth_yrange0, /nosort)
	smooth_xrange = xrange
	smooth_yrange = yrange
	if (keyword_set(verbose)) then begin
	print, name, ": fractions = ", fractions
	print, name, ": smooth x-range = ", smooth_xrange
	print, name, ": smooth y-range = ", smooth_yrange
	endif
	;; end of quantile calculation

	bounds = [min(x), min(y), max(x), max(y)]
	if (keyword_set(verbose)) then begin
	print, name, ": bounds = ", bounds
	print, name, ": creating smooth x- and y-grid"
	endif

	x_start = smooth_xrange[0]
	x_end = smooth_xrange[1]
	if (doxlog eq 1) then begin
		x_start = alog(x_start)
		x_end = alog(x_end)
	endif
	y_start = smooth_yrange[0]
	y_end = smooth_yrange[1]
	if (doylog eq 1) then begin
		y_start = alog(y_start)
		y_end = alog(y_end)
	endif
	x_delta = (x_end - x_start) / (nx - 1.0)
	y_delta = (y_end - y_start) / (ny - 1.0)
	dimension = [nx, ny]
	start = [x_start, y_start]
	delta = [x_delta, y_delta]
	;;; creating x-grid and y-grid
	smooth_x_grid = replicate(0.0, [nx, ny])
	smooth_y_grid = replicate(0.0, [nx, ny])
	for i = 0, nx - 1, 1 do begin
		smooth_x_grid[i, *] = i * x_delta + x_start
	endfor
	if (doxlog eq 1) then begin
		smooth_x_grid = exp(smooth_x_grid)
		sdraw_x = alog(draw_x)
	endif else begin
		sdraw_x = draw_x
	endelse
	for i = 0, ny - 1, 1 do begin
		smooth_y_grid[*, i] = i * y_delta + y_start
	endfor
	if (doylog eq 1) then begin
		smooth_y_grid = exp(smooth_y_grid)
		sdraw_y = alog(draw_y)
	endif else begin
		sdraw_y = draw_y
	endelse
	if (keyword_set(verbose)) then print, name, ": smoothing data"	
	smooth_u_mle = smooth_data_for_plotting(sdraw_x, sdraw_y, draw_u_mle, bounds = bounds, $
		start = start, delta = delta, nx = nx, ny = ny)
	smooth_u_sdss = smooth_data_for_plotting(sdraw_x, sdraw_y, draw_u_sdss, bounds = bounds, $
		start = start, delta = delta, nx = nx, ny = ny)
	;; smooth_u_ratio = smooth_data_for_plotting(sdraw_x, sdraw_y, draw_u_ratio, bounds = bounds, $
	;; 	start = start, delta = delta, nx = nx, ny = ny, /ts)
	smooth_u_ratio = abs(smooth_u_mle) - abs(smooth_u_sdss)

	
	npix = n_elements(smooth_u_mle)

	; using only one range for all categories
	if (keyword_set(verbose)) then print, name, ": using the same range for all categories"
	npix = n_elements(u_mle)
		
	;; bins for histogram
	bins_u =  findgen(nbin) * (u_range[1] - u_range[0]) / (nbin - 1) + u_range[0]
		
	;; printing the ranges
	if (keyword_set(verbose)) then begin
	print, name, ": U plotting range = ", u_range
	print, name, ": calculating color levels"
	print, name, ": plotting macro does not do chisq plots for MLE or SDSS but only for their difference"
	endif

	dolabels = replicate(1, n_level);
	cell_fill = 0
	fill = 1
	charsize = 0.25
	font = 2
	position = !my_cposition
	leg_position =  !my_lposition
	;; plotting the box
	fpCCstr = "(fpC)"
	if (keyword_set(fit2fpCC)) then fpCCstr = "(fpCC)"
	ctkfmt=!my_ctick_format

	xstyle = 1
	ystyle = 1
	pxrange = xrange
	pyrange = yrange

	xtfmt = get_tick_format(range1 = pxrange)
	ytfmt = get_tick_format(range1 = pyrange)
	ctkfmt=!my_ctick_format
	
	title= "'"+strupcase(uname)+"' error (MLE - SIM): src("+source_model+"), trg("+target_model+")"

	error = 0
	plot_contour_and_hist, draw_x = draw_x, draw_y = draw_y, draw_u = draw_u_mle, $
	smooth_x = smooth_x_grid, smooth_y = smooth_y_grid, smooth_u = smooth_u_mle, $
	pxrange = pxrange, pyrange = pyrange, xstyle = xstyle, ystyle = ystyle, $
	xtfmt = xtfmt, ytfmt = ytfmt, xlog = doxlog, ylog = doylog, $
	xtitle = xtitle, ytitle = ytitle, $
	u_color_range = u_color_range, u_levels = u_levels, u_range = u_range, $
	uname = uname, $
	ctkfmt = ctkfmt, font = font, title = title, nbin = nbin, $
	doraw = keyword_set(doraw), dosmooth = keyword_set(dosmooth), $
	dohisto = keyword_set(dohisto), doshisto = keyword_set(doshisto), $	
	error = error

	if (do_sdss_error eq 1) then begin

	ctkfmt=!my_ctick_format
	title= "'"+strupcase(uname)+"' error (SDSS - SIM): src("+source_model+"), trg("+target_model+")"	
	title= "'"+strupcase(uname)+"' (SDSS - SIM)"

	error = 0
	plot_contour_and_hist, draw_x = draw_x, draw_y = draw_y, draw_u = draw_u_sdss, $
	smooth_x = smooth_x_grid, smooth_y = smooth_y_grid, smooth_u = smooth_u_sdss, $
	pxrange = pxrange, pyrange = pyrange, xstyle = xstyle, ystyle = ystyle, $
	xtfmt = xtfmt, ytfmt = ytfmt, xlog = doxlog, ylog = doylog, $
	xtitle = xtitle, ytitle = ytitle, $
	u_color_range = u_color_range, u_levels = u_levels, u_range = u_range, $
	uname = uname, $
	ctkfmt = ctkfmt, font = font, title = title, nbin = nbin, $
	doraw = keyword_set(doraw), dosmooth = keyword_set(dosmooth), $
	dohisto = keyword_set(dohisto), doshisto = keyword_set(doshisto), $	
	error = error

	endif else begin
	
		print, name, ": WARNING - no ( SDSS - SIM ) plots to be made"
		
	endelse

	if (do_compare eq 1) then begin

	ctkfmt=!my_ctick_format
	title= "| err(MLE) | / | err(SDSS) |"

	error = 0
	plot_contour_and_hist, draw_x = draw_x, draw_y = draw_y, draw_u = draw_u_ratio, $
	smooth_x = smooth_x_grid, smooth_y = smooth_y_grid, smooth_u = smooth_u_ratio, $
	pxrange = pxrange, pyrange = pyrange, xstyle = xstyle, ystyle = ystyle, $
	xtfmt = xtfmt, ytfmt = ytfmt, xlog = doxlog, ylog = doylog, $
	xtitle = xtitle, ytitle = ytitle, $
	u_color_range = u_ratio_color_range, u_levels = u_ratio_levels, $
	u_range = u_ratio_range, uname = uname, $
	ctkfmt = ctkfmt, font = font, title = title, nbin = nbin, $
	doraw = keyword_set(doraw), dosmooth = keyword_set(dosmooth), $
	dohisto = keyword_set(dohisto), doshisto = keyword_set(doshisto), $	
	error = error
	
	endif

	if (keyword_set(verbose)) then print, name, ": closing device"
	device, /close ; close the file
	set_plot, olddevice ; go back to the graphics device
	command="epstopdf "+catfile
	spawn, command

	if (keyword_set(dolog)) then begin
		error = 0	
		name2 = uname + '(SDSS)'
		name1 = uname + '(MLE)'
		output_log, catfile, addcomment = comment, stat_catrecord = cat_stat, $
			draw_x = draw_x, draw_y = draw_y, smooth_x = smooth_x_grid, smooth_y = smooth_y_grid, $
			xrange = pxrange, yrange = pyrange, xtitle = xtitle, ytitle = ytitle, $
			xlog = doxlog, ylog = doylog, $
			name1 = name1, name2 = name2, row1 = draw_u_mle, row2 = draw_u_sdss, $
			dographicallog = keyword_set(dographicallog), error = error
		if (error ne 0) then begin
			print, name, ": ERROR - could not write to the logfile '", logfile, "'"
			return
		endif

	endif
endfor

end


function analyze_fpObjc_file, run = run, field = field, rerun = rerun, dir = dir, camcol = camcol, band = band, dostar = dostar, dogalaxy = dogalaxy, rowc = rowc, colc = colc, error = error

name = "read_fpObjc_file"
;;fpObjc = get_fpObjc_filename(dir = dir, run = run, field = field, camcol = camcol, band = band)
objc_list = mrdfits(fpObjc, 1, hdr, /silent)
row = objc_list.rowc[0]
col = objc_list.colc[0]
counts_model = objc_list.counts_model[0]
type = objc_list.objc_type

ngalaxy = 0
nstar = 0
nobjc = 0
galaxy_index = where(type eq 3, counts = ngalaxy)
star_index = where(type eq 6, count = nstar)
objc_index = where(type eq 3 or type eq 6, count = nobjc)

d = sqrt((row - rowc) ^ 2 + (col - colc) ^ 2)
closest_star = min(d[star_index])
closest_galaxy = min(d[galaxy_index])
closest_object = min(d[objc_index])

dimmest_star = min(counts_model[star_index], dimmest_star_i)
dimmest_galaxy = min(counts_model[galaxy_index], dimmest_galaxy_i)
dimmest_objc = min(counts_model[objc_index], dimmest_objc_i)

brightest_star = max(counts_model[star_index], brightest_star_i)
brightest_galaxy = max(counts_model[galaxy_index], brightest_galaxy_i)
brightest_objc = max(counts_model[galaxy_index], brightest_galaxy_i)

dimmest_star_d = d[star_index[dimmest_star_i]]
brightest_star_d = d[star_index[brightest_star_i]]

dimmest_galaxy_d = d[galaxy_index[dimmest_galaxy_i]]
brightes_galaxy_d = d[galaxy_index[brightest_galaxy_i]]

dimmest_objc_d = d[objc_index[dimmest_objc_i]]
brightest_galaxy_d = d[objc_index[brightest_objc_i]]

analysis = {run: 0L, camcol: 0L, field: 0L, rerun: "", band: "", camcol: 0L, $
	rowc: 0.0D0, colc: 0.0D0, $
	n_star: 0L, closest_star_counts: 0.0D0, closest_star_d: 0.0D0, $
	dimmest_star_counts: 0.0D0, dimmest_star_d: 0.0D0, $
	brightest_star_counts: 0.0D0, brightest_star_d: 0.0D0, $
	n_galaxy: 0L, closest_galaxy_counts: 0.0D0, closest_star_d: 0.0D0, $
	dimmest_galaxy_counts: 0.0D0, dimmest_galaxy_d: 0.0D0, $
	brightest_galaxy_counts: 0.0D0, brightest_galaxy_d: 0.0D0, $
	n_objc: 0L, closest_objc_counts: 0.0D0, closest_objc_d: 0.0D0, $
	dimmest_objc_counts: 0.0D0, dimmest_objc_d: 0.0D0, $
	brightest_objc_counts: 0.0D0, brightest_objc_d: 0.0D0 }
analysis.run = run
analysis.camcol = camcol
analysis.field = field
analysis.rerun = rerun
analysis.band = band
analysis.camcol = camcol

analysis.rowc = rowc
analysis.colc = colc

analysis.n_star = nstar
analysis.closest_star_counts = closest_star_counts
analysis.closest_star_d = closest_star_d
analysis.dimmest_star_counts = dimmest_star_counts
analysis.dimmest_star_d = dimmest_star_d
analysis.brightest_star_counts = brightest_star_counts
analysis.brightest_star_d = brightest_star_d

analysis.n_galaxy = ngalaxy
analysis.closest_galaxy_counts = closest_galaxy_counts
analysis.closest_galaxy_d = closest_galaxy_d
analysis.dimmest_galaxy_counts = dimmest_galaxy_counts
analysis.dimmest_galaxy_d = dimmest_galaxy_d
analysis.brightest_galaxy_counts = brightest_galaxy_counts
analysis.brightest_galaxy_d = brightest_galaxy_d

analysis.n_objc = nobjc
analysis.closest_objc_counts = closest_objc_counts
analysis.closest_objc_d = closest_objc_d
analysis.dimmest_objc_counts = dimmest_objc_counts
analysis.dimmest_objc_d = dimmest_objc_d
analysis.brightest_objc_counts = brightest_objc_counts
analysis.brightest_objc_d = brightest_objc_d

return, analysis
end

pro plot_contour_and_hist, draw_x = draw_x, draw_y = draw_y, draw_u = draw_u, $
	smooth_x = smooth_x, smooth_y = smooth_y, smooth_u = smooth_u, $
	pxrange = pxrange, pyrange = pyrange, xstyle = xstyle, ystyle = ystyle, $
	xtfmt = xtfmt, ytfmt = ytmft, xlog = xlog, ylog = ylog, $
	xtitle = xtitle, ytitle = ytitle, $
	u_color_range = u_color_range, u_levels = u_levels, u_range = u_range, $
	uname = uname, $
	ctkfmt = ctkfmt, font = font, title = title, nbin = nbin, $
	doraw = doraw, dosmooth = dosmooth, dohisto = dohisto, doshisto = doshisto, $	
	error = error

name = "plot_contour_and_hist"

doxlog = keyword_set(xlog)
doylog = keyword_set(ylog)

if (keyword_set(doraw)) then begin
	
	if (keyword_set(verbose)) then	print, name, ": plotting U (no smoothing): ", uname

	get_contour_position, position = position, leg_position = leg_position

	error = 0
	adjusted_u = adjust_array_to_range(draw_u, range = u_color_range, error = error)
	if (error ne 0) then begin
	print, name, ": ERROR - could not adjust the range for data (raw)"
	return
	endif

	xstyle = !my_xstyle + 4
	ystyle = !my_ystyle + 4
	contour, adjusted_u, draw_x, draw_y, levels = u_levels, /fill, /irregular, $
	xlog = doxlog, ylog = doylog, xstyle = xstyle, ystyle = ystyle, $
	xrange = pxrange, yrange = pyrange, position = position
	do_contour_legend, levels = u_levels, position = leg_position, $
	tickformat = ctkfmt, RGB_TABLE=ct

	xstyle = !my_xstyle
	ystyle = !my_ystyle
	plot, draw_x, draw_y, xrange = pxrange, yrange = pyrange, $
	xlog = doxlog, ylog = doylog, $
	xstyle = xstyle, ystyle = ystyle, xtickformat = xtfmt, ytickformat = ytfmt, $
	xtitle = xtitle, ytitle = ytitle, $	
	xtickname = replicate("", 30), ytickname = replicate("", 30), $
	font = font, title = title, position = position, /nodata

	multiplot
	title = ""
endif

if (keyword_set(dosmooth)) then begin
	if (keyword_set(verbose)) then print, name, ": plotting U (smooth): ", uname
	get_contour_position, position = position, leg_position = leg_position
	adjusted_u = adjust_array_to_range(smooth_u, range = u_color_range, error = error)
	if (error ne 0) then begin
		print, name, ": ERROR - could not adjust the range for U (smooth)"
		return
	endif

	xstyle = !my_xstyle + 4
	ystyle = !my_ystyle + 4

	contour, adjusted_u, smooth_x, smooth_y, $
	levels = u_levels, /fill, /irregular, $
	xlog = doxlog, ylog = doylog, xstyle = xstyle, ystyle = ystyle, $
	xrange = pxrange, yrange = pyrange, position = position
	do_contour_legend, levels = u_levels, position = leg_position, $
	tickformat = ctkfmt, RGB_TABLE=ct

	xstyle = !my_xstyle
	ystyle = !my_ystyle

	plot, smooth_x, smooth_y, $
	xrange = pxrange, yrange = pyrange, xlog = doxlog, ylog = doylog, $
	font = font, title = title, xtickformat = xtfmt, ytickformat = ytfmt, $
	xtitle = xtitle, ytitle = ytitle, position = position, $
	xtickname = replicate("", 30), ytickname = replicate("", 30), $
	xstyle = xstyle, ystyle = ystyle, /nodata
	multiplot
	title = ""
endif

h_xtitle=uname
htfmt = get_tick_format(range1 = u_range)
bins_u =  findgen(nbin) * (u_range[1] - u_range[0]) / (nbin - 1) + u_range[0]

psym = 10

if (keyword_set(dohisto)) then begin
	if (keyword_set(verbose)) then print, name, ": histogram for U (raw):", uname
	hist_u = histogram(draw_u, min = u_range[0], max = u_range[1], nbin = nbin)
	zcount = 0
	z_index = where(hist_u eq 0.0, zcount)
	if (zcount ne 0) then begin
		hist_u = 1.0 * hist_u
		hist_u[z_index] = 0.1
	endif else begin
		if (keyword_set(verbose)) then print, name, ": WARNING - no bins seems to have 0 counts in MLE data"
	endelse
	hytfmt = get_tick_format(range1 = hist_u)
	plot, bins_u, hist_u, xrange = u_range, $
	xstyle = 1, ystyle = 2, xtickformat = htfmt, ytickformat = hytfmt, $
	xtickname = replicate("", 30), ytickname = replicate("", 30), $
	/ylog, xtitle = h_xtitle, ytitle = "freq.", psym = psym, title = title
	multiplot
	title = ""
endif
	
if (keyword_set(doshisto)) then begin
	if (keyword_set(verbose)) then print, name, ": histogram for U (smooth):", uname
	smooth_hist_u = histogram(smooth_u, min = u_range[0], max = u_range[1], nbin = nbin)
	zcount = 0
	z_index = where(smooth_hist_u eq 0.0, zcount)
	if (zcount ne 0) then begin
		smooth_hist_u = 1.0 * smooth_hist_u
		smooth_hist_u[z_index] = 0.1
	endif else begin
		if (keyword_set(verbose)) then print, name, ": WARNING - no bins seems to have 0 counts in MLE data"
	endelse
	hytfmt = get_tick_format(range1 = smooth_hist_u)
	plot, bins_u, smooth_hist_u, xrange = u_range, $
	xstyle = 1, ystyle = 2, xtickformat = htfmt, ytickformat = hytfmt, /ylog, $
	xtickname = replicate("", 30), ytickname = replicate("", 30), $
	xtitle = h_xtitle, ytitle = "freq.", psym = psym, title = title
	multiplot
endif

error = 0
return
end

