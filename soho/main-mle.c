/* PHOTO libraries */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ftcl.h"
#include "dervish.h"
#include "strings.h"
#include "time.h"

#include "phUtils.h"

#include "phPhotoFitsIO.h"
#include "phSpanUtil.h"

#include "thMath.h" 
#include "thDebug.h" 
#include "thIo.h" 
#include "thAscii.h" 
#include "thProc.h" 
#include "thMask.h" 
#include "thMSky.h" 
#include "thObjcTypes.h"
#include "thMGalaxy.h"
#include "thAlgorithm.h"
#include "thMap.h"
#include "thMle.h"
#include "thResidual.h"
#include "thParse.h"
#include "thDump.h"

typedef enum tweakflag {
	ABSOLUTE, FRACTIONAL, N_TWEAKFLAG, UNKNOWN_TWEAKFLAG
	} TWEAKFLAG;

#define SIM_SKY 1
#define CLASS_BAND R_BAND
#define STAR_CLASSIFICATION 1 /* testing star classification */
#define GALAXY_CLASSIFICATION 1 /* testing galaxy classification */	
#define TABLEROW 5
#define N_RANDOM_FRAMES 2
#define N_OUTPUT_OBJC 2
#define NSIMOBJC 3000
#define INIT_NDEV 0.0
#define WORK_BAND R_BAND
/* image clustering and memory constants */
#define THMEMORY 1300
#define DO_ALGORITHM 1

typedef struct strategy {
	THPIX dev_adj_factor, exp_adj_factor;
	char *adj_method;
	int l_walktrap;
	int hierarchy_step;
	
	char *gfile_prefix, *clusterfile_prefix, *clusterlog_prefix;
	} STRATEGY;
 
int verbose = 0;

static int nrow = 1489;
static int ncol = 2048; 
static int CLASS_INDEX = UNKNOWN_BNDEX;

static void usage(void);
void chain_model_output(char *job, CHAIN *objclist, THOBJCTYPE otype);
static void print_phchain(CHAIN *chain);
void model_output(char *job, THOBJC *objc);
int compare_re(const void *x1, const void *x2);
int compare_count_galaxy(const void *x1, const void *x2);
int compare_count_star(const void *x1, const void *x2);

extern double ddot_(int *, double [], int *, double [], int *);
extern float sdot_(int *, float [], int *, float [], int *);

static int fl_compare(const void *a, const void *b);
static RET_CODE output_memory_stat(LSTRUCT *lstruct, char *fname1, char *fname2);
	
int
main(int ac, char *av[])
{

  char *name = "do-mle";
  /* Arguments */

#if THCLOCK 
clock_t clk1;
clk1 = clock();
#endif

printf("%s: initiating modules \n", name);

 printf("%s: initiating IO \n", name); 
 thInitIo();
 printf("%s: initiating Tank \n", name);
  thTankInit();
 printf("%s: initiating Ascii \n", name);
  thAsciiInit();  
 printf("%s: initiating SpanObjmask \n", name);
  initSpanObjmask();

/* passing av, ac to input parser */
MLE_PARSED_INPUT *mleinput = thMleParsedInputNew();
RET_CODE status;
status = mleparser(ac, av, mleinput);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not parse the input", name);
	return(SH_GENERIC_ERROR);
}

/* initializing model and object bank */
  thModelInit();
/* introducing sky to the object bank */
printf("%s: initiating all models and object types  ... \n", name);
printf("%s: initiating non-sky models \n", name);
status = MGModelsInit();
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not initiate object models", name);
	return(-1);
}

int i, nm_max = 2;
char *mname, **mnames = thCalloc(nm_max, sizeof(char *));
for (i = 0; i < nm_max; i++) {
	mnames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
} 
mname = mnames[0];

int dostar = 1;
dostar = mleinput->dostar;
if (dostar) {
	strcpy(mnames[0], "star");
	printf("%s: initiating object type 'STAR' \n", name);
	status = MStarObjcInit(mnames[0]);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object type", name);
		return(-1);
	}
}

#if CDCANDIDATE_CLASSIFICATION 
strcpy(mnames[0], "deV");
strcpy(mnames[1], "Exp2");
printf("%s: initiating object type 'CDCANDIDATE' \n", name);
printf("%s: adding two models '%s', '%s' \n", name, mnames[0], mnames[1]);
status = McDCandidateObjcInit(mnames, 2);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not initiate object type", name);
	return(-1);
}
#endif

GCLASS galaxyclass = mleinput->gclass;
if (galaxyclass == DEVEXP_GCLASS) {
	strcpy(mnames[0], "deV");
	strcpy(mnames[1], "Exp");
	printf("%s: initiating object type 'GALAXY' as 'deV-Exp' \n", name);
	printf("%s: adding two models '%s', '%s' \n", name, mnames[0], mnames[1]);
	status = MGalaxyObjcInit(mnames, 2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object type", name);
		return(-1);
	}
} else if (galaxyclass == DEV_GCLASS) {
	strcpy(mnames[0], "deV");
	printf("%s: initiating object type 'DEVGALAXY' \n", name);
	printf("%s: adding one model '%s' \n", name, mnames[0]);
	status = MdeVGalaxyObjcInit(mnames, 1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object type", name);
		return(-1);
	}
} else if (galaxyclass == EXP_GCLASS) {
	strcpy(mnames[0], "Exp");
	printf("%s: initiating object type 'EXPGALAXY' \n", name);
	printf("%s: adding one model '%s' \n", name, mnames[0]);
	status = MExpGalaxyObjcInit(mnames, 1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object type", name);
		return(-1);
	}
} else if (galaxyclass == DEVDEV_GCLASS) {	
	strcpy(mnames[0], "deV");
	strcpy(mnames[1], "deV2");
	printf("%s: initiating object type 'GALAXY' as 'deV-deV' \n", name);
	printf("%s: adding two models '%s', '%s' \n", name, mnames[0], mnames[1]);
	status = MGalaxyObjcInit(mnames, 2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object type", name);
		return(-1);
	}
} else if (galaxyclass == EXPEXP_GCLASS) {	
	strcpy(mnames[0], "Exp");
	strcpy(mnames[1], "Exp2");
	printf("%s: initiating object type 'GALAXY' as 'Exp-Exp' \n", name);
	printf("%s: adding two models '%s', '%s' \n", name, mnames[0], mnames[1]);
	status = MGalaxyObjcInit(mnames, 2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object type", name);
		return(-1);
	}
} else if (galaxyclass == EXPDEV_GCLASS) {
	strcpy(mnames[0], "Exp");
	strcpy(mnames[1], "deV2");
	printf("%s: initiating object type 'GALAXY' as 'Exp-deV' \n", name);
	printf("%s: adding two models '%s', '%s' \n", name, mnames[0], mnames[1]);
	status = MGalaxyObjcInit(mnames, 2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate object type", name);
		return(-1);
	}
} else if (galaxyclass == STAR_GCLASS) {
	strcpy(mnames[0], "star");
	if (dostar == 0) {
		printf("%s: initiating object type 'STAR' \n", name);
		status = MStarObjcInit(mname);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not initiate the object type", name);
			return(-1);
		}
	} else {
		printf("%s: star object already initiated \n", name);
	}
} else {
	char *cname = shEnumNameGetFromValue("GCLASS", galaxyclass);
	thError("%s: ERROR - galaxy class '%s' not supported", name, cname);
	return(-1);
}


CDESCRIPTION csystem = mleinput->csystem;
status = MGInitProfile(csystem);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not initiate profile package", name);
	return(-1);
}


  printf("%s: new process \n", name);
  PROCESS *proc;
  proc = thProcessNew();


printf("%s: reading framefile \n", name);

  /* get the related schema */
  ASCIISCHEMA *schema = NULL;
  char *strutype = "FRAMEFILES";	
  schema = thTankSchemaGetByType(strutype);

  if (schema == NULL) {
    printf("%s: No schema exists for structure %s", 
		   name, strutype);
  }

  /* get the structure constructor */
  void *(*strunew)();
  strunew = schema->constructor;
  if (strunew == NULL) {
    printf("%s: structure constructor for %s not available in tank", 
		   name, strutype);
  }

  /* reading the framefile */
  char *fname = mleinput->framefile;
  void *stru;
  stru = (*strunew)();

  FRAMEFILES *ff;
  ff = (FRAMEFILES *) stru;

  FILE *fil;
  fil = fopen(fname, "r");
  if (fil == NULL) {
	thError("%s: ERROR - could not open framefile '%s'", name, fname);
	return(-1);
	} 
  CHAIN *fchain;
  fchain = thAsciiChainRead(fil);

  if (shChainSize(fchain) == 0) thError("%s: no structures was properly read from the par file", name);
  printf("%s: (%d) pieces of frame info read \n", name, shChainSize(fchain));

  int nframe,  iframe = 0;
  nframe = shChainSize(fchain);

  printf("%s: total of (%d) frames read \n", name, nframe);

  for (iframe = 0; iframe < nframe; iframe++) {
	
	printf("%s: --------------------------- \n", name);
	printf("%s: working on frame (%d) \n", name, iframe);
	ff = (FRAMEFILES *) shChainElementGetByPos(fchain, iframe); 
 
	 if (ff == NULL) {
	    thError("%s: NULL returned from chain - check source code", name);
	  } else {  
	    SCHEMA *s = shSchemaGet("FRAMEFILES");
	    if (s == NULL) {
		thError("%s: WARNING - structure 'FRAMEFILES' not surpported by schema", name);
	    } else {
	    	int nse = s->nelem;
		for (i = 0; i < nse; i++) {
			SCHEMA_ELEM *se = s->elems + i;
			if (!strcmp(se->type, "char")) {
				char **value = shElemGet(ff, se, NULL);
				printf("%s: %s = '%s' \n", name, se->name, *value);
			}
	    	}
	    }
	}

  printf("%s: process and frame \n", name);
  int i;
  int camcol = 4, camrow = R_ROW; 

  FRAME *frame = NULL;
  REGION *im = NULL;


  frame = thFrameNew();
  if (frame->files != NULL) {
	thFramefilesDel(frame->files);
	}
      
   frame->files = ff; 
   frame->proc = proc;
     
/* setting up camrow and camcol by hand since the simulated fpC file does not include the proper header information */
   printf("%s: manually setting (camrow = %d) and (camcol = %d) \n", name, camrow, camcol);
   frame->id->camrow = camrow;
   frame->id->camcol = camcol;
 
   int fit2fpCC = mleinput->fit2fpCC;
   if (fit2fpCC) {
	printf("%s: switching fpC and fpCC files \n", name);
	char *temp;
	temp = ff->phflatframefile;
	ff->phflatframefile=ff->thflatframefile;
	ff->thflatframefile=temp;
	printf("%s: loading fpCC file \n", name);
   } else { 
	   printf("%s: loading fpC file\n", name);
   }
   status = thFrameLoadImage(frame); 
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not load fpC image", name);
		return(-1);
	}
   im = frame->data->image;

   printf("%s: loading amplifier information\n", name);
   status = thFrameLoadAmpl(frame); 
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not load amplifier information", name);
		return(-1);
	}
	/* no need to read the mask since we are gonna work with a fake sky only frame 
      printf("%s: loading PHOTO masks\n", name);
      thFrameLoadPhMask(frame);
	*/
/* testing with PHOTO calibration data */
	if (ff->phcalibfile != NULL && strlen(ff->phcalibfile) != 0) {
		printf("%s: loading PHCALIB \n", name);
		status = thFrameLoadCalib(frame);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not load calibration info", name);
			return(-1);
		}
	} else {
		printf("%s: no calibration file passed - cannot load calibration information \n", name);
	}
	/* testing with SDSS objects */
      printf("%s: loading OBJC's \n", name);
      status = thFrameLoadPhObjc(frame);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not load OBJ's", name);
		return(-1);
	}
	/* testing PSF at this stage */ 

      printf("%s: loading PHOTO psf \n", name);
      status = thFrameLoadPhPsf(frame);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not load PHOTO PSF structure", name);
		return(-1);
	}
      printf("%s: reshaping into SOHO psf \n", name);
      thFrameLoadThPsf(frame);

      printf("%s: loading weight matrix -- space saver \n", name); 
      status = thFrameLoadWeight(frame);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not load weights", name);
		return(-1);
	}
	/* mask is null at this point for this test */
      printf("%s: loading SOHO masks \n", name);
      thFrameLoadThMask(frame);

	RET_CODE status;
	int nsimobjc = 0;

	printf("%s: initiating sky model and object typec \n", name);
	status = thSkyObjcInit(nrow, ncol, frame->data->ampl, &g0sky);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate sky object type for this particular field", name);
		return(-1);
	}
	printf("%s: models and object types initiated \n", name);	

	/* define THOBJC with type SKY
	   construct a MAP
	*/

	THOBJCID id = 0;
	THOBJC *objc = NULL;
	CHAIN *objclist = NULL;
	objclist = shChainNew("THOBJC");

	OBJC_IO *phobjc;
	CHAIN *phobjcs = NULL;
	int *bndex = NULL;
	int iobjc, nobjc;
	status = thFrameGetPhObjcs(frame, &phobjcs, &bndex);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (phobjc chain) and (bndex array) from (frame)", name);
		return(-1);
	}
	CLASS_INDEX = bndex[CLASS_BAND];
	shAssert(phobjcs != NULL);
	nobjc = shChainSize(phobjcs);
	printf("%s: (%d) objects read from SDSS file \n", name, nobjc);
	MAPMACHINE *map;
#if 1 /* debug nov 5, 2012 */
	printf("%s: loading models into (mapmachine) \n", name);
	map = thMapmachineNew();
	/* inserting map into frame */
	frame->work->map = map;

	/* add all the models in the object to the map */
	char **rnames, **iornames;
	int nrname = 10;
	rnames = thCalloc(nrname, sizeof(char*));
	iornames = thCalloc(nrname, sizeof(char *));
	for (i = 0; i < nrname; i++) {
		rnames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
		iornames[i] = thCalloc(MX_STRING_LEN, sizeof(char));
	}
	LSTRUCT *l;
	LWORK *lwork;

	int pos = 0;
	/* introducing sky to the simulated set of objects  */
	#define DO_SKY 1
	#if DO_SKY
	printf("%s: creating sky object \n", name);
	objc = thSkyObjcNew();
	if (objc == NULL) {
		thError("%s: ERROR - could no create object sample", name);
		return(-1);
	}
	printf("%s: loading sky object onto (mapmachine) \n", name);
	thMAddObjcAllModels(map, objc, WORK_BAND, SEPARATE);
	objc->thid = id++;
	shChainElementAddByPos(objclist, objc, "THOBJC", TAIL, AFTER);

	#if SIM_SKY

	printf("%s: uploading sky simulation parameters \n", name);
	status = thMCompile(map);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not compile the map", name);
		return(-1);
	}
	status = thMapPrint(map);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not print the compiled map", name);
		return(-1);
	}	
	/* constructing LSTRUCT */
	printf("%s: constructing likelihood data structure (lstruct) \n", name);
 	status = thFrameLoadLstruct(frame, NULL, DEFAULT_CALIBTYPE, DEFAULT_LFITRECORD);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not load (lstruct)", name);
		return(-1);
	}
	l = frame->work->lstruct;
	l->lmodel->lmachine = map;
	lwork = l->lwork;
	/* simulating the sky */
	#if DO_BKGRND

	printf("%s: 1. memory estimates \n", name); 
	status = MakeImageAndMatrices(l, MEMORYESTIMATE);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get memory estimate for the model", name);
		return(-1);
	}
	printf("%s: 2. init images \n", name);
	status = MakeImageAndMatrices(l, INITIMAGE); /* MODELONLY */
   	if (status != SH_SUCCESS) {
		thError("%s: could not simulate the basis models for sky", name);
		return(-1);
	}
	status = thMakeM(l);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add up models to create sky", name);
		return(-1);
	}
	#endif

	#endif

	#endif
#endif

	printf("%s: proceeding to the rest of the objects ... \n", name);
	status = thFrameGetPhObjcs(frame, &phobjcs, &bndex);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (phobjc chain) and (bndex array) from (frame)", name);
		return(-1);
	}
	CLASS_INDEX = bndex[CLASS_BAND];
	nobjc = shChainSize(phobjcs);
	printf("%s: (%d) objects read from SDSS file \n", name, nobjc);

	/* introducing a galaxy */

	/* 
	printf("%s: simulating objects as '%s' \n", name, sim_name);
	*/
	printf("%s: sorting objects \n", name);
	
	void *sort_function = NULL;
	if (galaxyclass  == STAR_GCLASS) {
		sort_function = &compare_count_star;
	} else {
		sort_function = &compare_count_galaxy;
	}
	status = thChainQsort(phobjcs, sort_function); /* does thiswork correctly? */	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - unable to sort (phobjcs)", name);
		return(-1);
	}
	print_phchain(phobjcs);	
	#if EXAMIN_SORT
	printf("%s: testing the sorted chain of objects", name);
	test_sorted_chain(phobjcs, &compare_re);
	#endif

	printf("%s: initiating random numbers \n", name);
	RANDOM *rand;
	char *randstr;
	int nrandom;
	randstr = thCalloc(MX_STRING_LEN, sizeof(char));
	nrandom = (int) (nrow * ncol * (float) N_RANDOM_FRAMES);
	sprintf(randstr, "%d:2", nrandom);
	printf("%s: generating %d random numbers (random frames= %g) \n", name, nrandom, (float) N_RANDOM_FRAMES);
	rand = phRandomNew(randstr, 1);

	/* fitting procedure */
	int nmaxobjc = mleinput->nobjc;
	int nfitobjc = mleinput->nfit;

	int fitshape = 0, fitsize = 0, fitcenter = 0, lockcenter = 1;
	fitshape = mleinput->fitshape;
	fitsize = mleinput->fitsize;
	fitcenter = mleinput->fitcenter;
	lockcenter = mleinput->lockcenter;

	LINITMODE initmode = mleinput->init;
	THPIX initerror = (THPIX) mleinput->error;

	/* making record names for OBJC_IO */
	char **rnames_sdss = thCalloc(50, sizeof(char *));
	for (i = 0; i < 50; i++) {
		rnames_sdss[i] = thCalloc(MX_STRING_LEN, sizeof(char));
	}
	int j = 0;
	if (fitsize) {
		strcpy(rnames_sdss[j++], "r_deV");
		strcpy(rnames_sdss[j++], "r_exp");
	}
	if (fitshape) {
		strcpy(rnames_sdss[j++], "ab_deV");
		strcpy(rnames_sdss[j++], "ab_exp");
		strcpy(rnames_sdss[j++], "phi_deV");
		strcpy(rnames_sdss[j++], "phi_exp");
	}
	if (fitcenter) {
		strcpy(rnames_sdss[j++], "rowc");
		strcpy(rnames_sdss[j++], "colc");
	}
	int nrecord_sdss = j;

	for (iobjc = 0; iobjc < MIN(nobjc, nmaxobjc); iobjc++) {

		/* initiating the object */
		phobjc = shChainElementGetByPos(phobjcs, iobjc);
		if (iobjc < nfitobjc && initmode == SDSSpERROR_INIT) {
			printf("%s: adding noise (%g) to fit parameters in objc (%d) \n", name, (float) initerror, iobjc);
			status = thObjcIoDraw(phobjc, phobjc, rnames_sdss, nrecord_sdss, (THPIX) initerror);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not randomize fit parameter for photo object (iobjc = %d)", name, iobjc);
				return(-1);
			}
		}
		PHPROPS *phprop = phPropsNew();
		PHCALIB *calibData;
		calibData = frame->data->phcalib;
		status = thPhpropsPut(phprop, phobjc, bndex, NULL, NULL);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not put object information in 'PHPROP'", name);
			return(-1);
		} 
		objc = thObjcNewFromPhprop(phprop, WORK_BAND, CLASSIFY, NULL);
		if (objc == NULL) {
			thError("%s: ERROR - could not create object for object (%d) in the sdss list", name, iobjc);
			return(-1);
		}
		objc->thid = (THOBJCID) phobjc->id;
		THOBJCTYPE objctype;
		char *objcname;
		objctype = objc->thobjctype;
		status = thObjcNameGetFromType(objctype, &objcname);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get type name from object type (iobjc = %d)", name, iobjc);
			return(-1);
		}
		if (objctype != SKY_OBJC && objctype != PHOTO_OBJC) {
			/* one should create the ioprop of the object after it is properly classified */
 	
			status = thObjcInitMpars(objc);
			if (status != SH_SUCCESS) {
				char *tname = NULL;
				thObjcNameGetFromType(objc->thobjctype, &tname);
				thError("%s: ERROR - could not initiate model parameters for (objc: %d) of class '%s'", 
				name, objc->thid, tname);
				return(-1);
			}

			shChainElementAddByPos(objclist, objc, "THOBJC", TAIL, AFTER);		
			thMAddObjcAllModels(map, objc, WORK_BAND, SEPARATE);
			nsimobjc++;
			if (nsimobjc == NSIMOBJC) break;
		
		} else if (objctype == PHOTO_OBJC) {
			thObjcDel(objc);
		}	


	}

	nobjc = shChainSize(objclist);
	printf("%s: total of (%d) objects added to (mapmachine) \n", name, nobjc);
	if (nobjc <= 1) {
		thError("%s: ERROR - no objects loaded from SDSS", name);
		return(-1);
	}

	int icenter, jcenter, ncenter;
	int ishape, jshape, nshape;
	int ii;
	j = 0;
	ii = j;
	strcpy(rnames[j++], "I");
	if (fitshape || fitsize) { 
		ishape = j;
		if (csystem == ALGEBRAIC) {
			strcpy(rnames[j++], "a");
 			strcpy(rnames[j++], "b");
			strcpy(rnames[j++], "c");
		} else if (csystem == HYPERBOLIC) {
		if (fitsize) strcpy(rnames[j++], "re");
		if (fitshape) {
			strcpy(rnames[j++], "v");
			strcpy(rnames[j++], "w");
		}
		} else if (csystem == LOGHYPERBOLIC) {
		if (fitsize) strcpy(rnames[j++], "re");
		if (fitshape) {
			strcpy(rnames[j++], "V");
			strcpy(rnames[j++], "W");
		} 
		} else if (csystem == LOGHYPERBOLIC_LOGR) {
			if (fitsize) strcpy(rnames[j++], "ue");
			if (fitshape) {
				strcpy(rnames[j++], "V");
				strcpy(rnames[j++], "W");
			} 
		} else if (csystem == CANONICAL) {
			if (fitsize) strcpy(rnames[j++], "re");
			if (fitshape) {
				strcpy(rnames[j++], "e");
				strcpy(rnames[j++], "phi");
			}
		} else if (csystem == SUPERCANONICAL || csystem == UBERCANONICAL) {
			if (fitsize) strcpy(rnames[j++], "re"); 
			if (fitshape) {
				strcpy(rnames[j++], "E");
				strcpy(rnames[j++], "phi");
			}
		} else if (csystem == SUPERCANONICAL_RSUPERLOG) {
			if (fitsize) strcpy(rnames[j++], "ue"); 
			if (fitshape) {
				strcpy(rnames[j++], "E");
				strcpy(rnames[j++], "phi");
			}
		}
	jshape = j;
	nshape = j;
	}	
	if (fitcenter) {
		icenter = j;
		strcpy(rnames[j++], "xc");  
		strcpy(rnames[j++], "yc");
		jcenter = j;
		ncenter = (jcenter - icenter);
	}
	
	int k, kmax, kfit = 0;
	if (j > 1) { 
		printf("%s: adding variables to nonlinear fit (variables/model = %d)\n", name, j - 1);
		kmax = MIN(nfitobjc, shChainSize(objclist));
		printf("%s: number of objects for nonlinear fit = %d \n", name, kmax); 
	}
	for (k = 0; k < shChainSize(objclist); k++) {

		if (kfit == kmax) break;	
		THOBJCTYPE objctype;
		char *objcname;
		objc = shChainElementGetByPos(objclist, k);
		objctype = objc->thobjctype;
		thObjcNameGetFromType(objctype, &objcname);
		printf("%s: objc (k = %d, objcname = '%s') \n", name, k, objcname);
		if (!strcmp(objcname, "DEVGALAXY") || 
			!strcmp(objcname, "EXPGALAXY") || 
			!strcmp(objcname, "GAUSSIAN") || 
			!strcmp(objcname, "GALAXY") || 
			!strcmp(objcname, "CDCANDIDATE") || 
			!strcmp(objcname, "STAR")) {
	
			kfit++;	
			if (!strcmp(objcname, "DEVGALAXY")) strcpy(mnames[0], "deV");
			if (!strcmp(objcname, "EXPGALAXY")) strcpy(mnames[0], "Exp");
			if (!strcmp(objcname, "GAUSSIAN")) strcpy(mnames[0], "gaussian");
			if (!strcmp(objcname, "GALAXY")) {
				strcpy(mnames[0], "deV");
				strcpy(mnames[1], "Exp");
			}
			if (!strcmp(objcname, "CDCANDIDATE")) {
				strcpy(mnames[0], "deV");
				strcpy(mnames[1], "Exp2");
			}	
			if (!strcmp(objcname, "STAR")) {
				strcpy(mnames[0], "star");
			}
			if (j > 1) {
				mname = mnames[0];	
				status = thMClaimObjcVar(map, objc, mname, rnames + 1, j - 1);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not add model '%s' in (objc: %d) to fit", 
					name, mname, objc->thid);
					return(-1);
				}
				mname = mnames[1];
				if (!strcmp(objcname, "GALAXY") || !strcmp(objcname, "CDCANDIDATE")) {
					status = thMClaimObjcVar(map, objc, mname, rnames + 1, j - 1);	
					if (status != SH_SUCCESS) {
						thError("%s: ERROR - could not add model '%s' in (objc: %d) to fit", 
						name, mname, objc->thid);
						return(-1);
					}
				} 
				if (fitcenter && lockcenter) {
					if (!strcmp(objcname, "GALAXY") || !strcmp(objcname, "CDCANDIDATE")) {
						printf("%s: locking centers for models during fit \n", name);
						status = thMEquivObjcVars(map, objc, mnames[0], rnames + icenter,
							objc, mnames[1], rnames + icenter, ncenter);
						if (status != SH_SUCCESS) {
							thError("%s: ERROR - could not claim centers as equiv", name);
							return(-1);
						}
					}
				} else if (fitcenter)  {
					printf("%s: centers not to be locked based on demand from mle input \n",name);
				}
	
				printf("%s: (objc: %d) ", name, objc->thid);
				for (i = 0; i < j; i++) printf(" '%s' ", rnames[i]);
				printf("\n");
			}	
		} else {
			printf("%s: no nonlinear component to the fit", name);
		}
	}

	/* compiling map */	
	printf("%s: compiling (mapmachine) \n", name);
	status = thMCompile(map);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not compile map", name);
		return(-1);
	} 
	status = thMapPrint(map);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not print the compiled map", name);
		return(-1);
	}	

	printf("%s: outputting (mapmachine) info \n", name);
	/* 
	thMInfoPrint(map, EXTENSIVE);	
	*/
	printf("%s: n(model) = %d, n(par) = %d, n(objc) = %d \n", name, map->namp, map->npar, map->nobjc);

	/* constructing LSTRUCT */
	printf("%s: constructing likelihood data structure (lstruct) \n", name);
 	status = thFrameLoadLstruct(frame, NULL, DEFAULT_CALIBTYPE, DEFAULT_LFITRECORD);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not load (lstruct)", name);
		return(-1);
	}
	status = thFrameGetLstruct(frame, &l);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (lstruct) out of (frame)", name);
		return(-1);
	}
	status = thLstructPutLmachine(l, map);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (lmachine) out of (lstruct)", name);
		return(-1);
	}
	status = thLstructGetLwork(l, &lwork);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (lwork) out of (lstruct)", name);
		return(-1);
	}

	
	/* simulating the galaxy */ 
	pos++;	
	printf("%s: assigining parameters and amplitude arrays in (mapmachine) from SDSS model parameters\n", 
	name);

	  
	status = thLDumpAmp(l, MODELTOIO); 
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump (amp) from (model) to (io)", name);
		return(-1);
	}
	status = thLDumpPar(l, MODELTOIO);
 	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump (par) from (model) to (io)", name);
		return(-1);
	}
	status = thLDumpAmp(l, MODELTOX);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump (amp) from (model) to (X)", name);
		return(-1);
	}
	status = thLDumpPar(l, MODELTOX);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump (par) from (model) to (X)", name);
		return(-1);
	}
	status = thLDumpPar(l, MODELTOXN);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump (par) from (model) to (XN)", name);
		return(-1);
	}
	/* 
	status = thLDumpCountsIo(l);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump (counts) onto (IO)", name);
		return(-1);
	}
	*/
	status = thLCalib(l);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not calibrate (l) with initial count data", name);
		return(-1);
	}
	/* showing the statistics */
	/* 
	chain_model_output("input parameters from SDSS fpObjc file ", objclist, UNKNOWN_OBJC);
	*/

	/* calculation needs proper initiation of quadrature methods and elliptical description */
	
	printf("%s: * memory estimates \n", name);
	status = MakeImageAndMatrices(l, MEMORY_ESTIMATE);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not estimate the memory for the model elements", name);
		return(-1);
	}
	/*
	status = output_memory_stat(l, "./memory-stat.txt", NULL);
	*/
	/* re-creating the poisson weights */
	printf("%s: loading poisson weights \n", name);
      	thFrameLoadWeight(frame);
	printf("%s: loading SOHO masks \n", name);
      	thFrameLoadThMask(frame);
	
	printf("%s: number of objects = %d \n", name, shChainSize(objclist));

	/* dumper call - note that if their is a need to calibrate the init counts, it should be done before initializing the dumpers */
	printf("%s: initilizing dump \n", name);
	status = thFrameInitIo(frame);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not init dumpers for this frame", name);
		return(-1);
	}
	clock_t clk1, clk2;
	time_t time1, time2, dif;
	printf("%s: * fit algorithm \n", name);


	
	MEMDBLE memory = mleinput->memory;
	if ((memory <= MEMORY_MIN || memory >= MEMORY_MAX) && (memory != INDEFINITE_MEMORY)) {
		memory = MEMORY_DEFAULT;
	}
	printf("%s: algorithms will be generated for a memory of (%g MB) \n", name, (float) memory / (float) MEGABYTE);
	printf("%s: i. l compilation (automatic) \n", name);	
	clk1 = clock();
	status = thLCompile(l, memory, SIMFINALFIT);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not compile (l)", name);
		return(-1);
	}
	clk2 = clock();
	printf("%s: compile time: %g sec \n", name, ((float) (clk2 - clk1)) / (float) CLOCKS_PER_SEC);

	printf("%s: l-run (automatic) \n", name);
	time(&time1);
	status = thLRun(l);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not run (l)", name);
		return(-1);
	}
	time(&time2);
	dif = difftime(time2, time1);	
	printf("%s: run time: %g sec \n", name, (float) dif);

	/* dumping onto io structure and calculating counts */
	#if 0 /* this is now part of thLRun */
	status = thLDumpAmp(l, XTOIO); 
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not map amplitude array (X) onto IO structures", name);
		return(-1);
	}
	status = thLDumpPar(l, MODELTOIO);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not map parameters from (model) onto IO structures", name);
		return(-1);
	}
	status = thLDumpCountsIo(l);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (counts) in (ioprops) in (l)", name);
		return(status);
	}
	status = thLCalib(l);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not calibrate (l) with initial count data", name);
		return(-1);
	}
	#endif
	/* 
	chain_model_output("output parameters after fit", objclist, UNKNOWN_OBJC);
	*/
	printf("%s: finalizing frame dumpers \n", name);
	status = thFrameFiniIo(frame);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not finalize dumpers for frame", name);
		return(-1);
	}
	printf("%s: dumping log, sdss pars, init pars and fit findings\n", name);
	status = thFrameDumpIo(frame);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump frame information", name);
		return(-1);
	}

	
	MSkyObjcFini();
	shChainDestroy(objclist, &thObjcDel);
	thMapmachineDel(map);
	thFrameDel(frame);

	} /* end of frame loop */
	printf("%s: end of program \n", name);

	/* 
		outputting mle results 
	*/
	/* 
		Tweaking Initial Parameters a bit 
	*/

  thErrShowStack();
  /* for unknown reason, this returns an error - distribution of the variates were checked and the results produced satisfy the 
	randomness condition.
  phRandomDel(rand);
	*/

  /* successfull run */
  return(0);

#if 0
/* doing residual reports */

	REGION *diff;
	diff = shRegNew("residual image", simreg->nrow, simreg->ncol, simreg->type);
	thPutModelComponent(diff, simreg, 1.0); /* sim reg is the product of simulation */
	thAddModelComponent(diff, im, -1.0); /* im is the data  */	

	int rbin1 = 1, cbin1 = 1, rbin2 = 20, cbin2 = 20;
	printf("%s: iv. esidual reports \n", name);
	printf("%s: iv.a. creating report for (rbin, cbin) = (%d, %d) \n", name, rbin1, cbin1);
	RESIDUAL_REPORT *res = NULL;
	status = thCreateResidualReport(diff, NULL, rbin1, cbin1, &res);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not create residual report", name);
		return(-1);
	}
	printf("%s: iv.a. compiling report \n", name);
	status = thCompileResidualReport(res);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not compile residual report", name);
		return(-1);
	}
	
	printf("%s: iv.b. creating chain of reports (rbin = %d, %d), (cbin = %d, %d) \n", name, rbin1, rbin2, cbin1, cbin2);
	CHAIN *reschain;
	reschain = shChainNew("RESIDUAL_REPORT");
	status = thCreateResidualReportChain(diff, NULL, rbin1, rbin2, cbin1, cbin2, reschain);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not create residual report chain", name);
		return(-1);
	}
	printf("%s: iv.b. compiling residual report chain \n", name);
	status = thCompileResidualReportChain(reschain);	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not compile residual report chain", name);
		return(-1);
	}
	status = output_residual_report_chain(reschain);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not output (residual report) chain", name);
		return(-1);
	}

printf("%s: return requested by user \n", name);
return(1);
#endif
	/* 
		outputting simulation paramters 
	*/
	/* 
		Tweaking Initial Parameters a bit 
	*/

  thErrShowStack();
  /* for unknown reason, this returns an error - distribution of the variates were checked and the results produced satisfy the 
	randomness condition.
  phRandomDel(rand);
	*/

  /* successfull run */
  return(0);

}

/*****************************************************************************/

void chain_model_output(char *job, CHAIN *objclist, THOBJCTYPE otype) {

THOBJC *objc;
int iobjc, nobjc;
nobjc = shChainSize(objclist);
for (iobjc = 0; iobjc < MIN(nobjc, N_OUTPUT_OBJC); iobjc++) {
	objc = shChainElementGetByPos(objclist, iobjc);
	model_output(job, objc);
}
printf("--- %s: end of output --- \n", job);
return;
}


void model_output(char *job, THOBJC *objc) {

CHAIN *thprops;
int nmodel, imodel;
static char *line = NULL;
if (line == NULL) line = thCalloc(MX_STRING_LEN, sizeof(char));

thprops = objc->thprop;
nmodel = shChainSize(thprops);
for (imodel = 0; imodel < nmodel; imodel++) {
	SCHEMA *s;
	SCHEMA_ELEM *se;
	int n, i;
	char *rname;
	THPIX *value;
	THPROP *prop;
	prop = shChainElementGetByPos(thprops, imodel);	 		
	s = shSchemaGetFromType(shTypeGetFromName(prop->pname));
	n = s->nelem;
	printf("--- %s parameters for (objc: %d) (model: '%s') --- \n", 
		job, objc->thid, prop->mname);
	for  (i = 0; i < n; i++) {
		se = s->elems + i;
		rname = se->name;
		value = shElemGet(prop->value, se, NULL);
		if (strlen(line) != 0) {
			sprintf(line, "%s, %15s = %.5e", line, rname, *value);
		} else {
			sprintf(line,"%15s = %.5e", rname, *value);
		}
		if (((i + 1)%TABLEROW == 0) || i == n - 1) {
			printf("%s\n", line);
			strcpy(line, "");
		}
	}
}
return;
}

/* note that if x1 > x2 then compare should result in 1 if you want things to be assending */

int compare_re(const void *x1, const void *x2) {
	const OBJC_IO *y1, *y2;
	shAssert(x1 != NULL);
	shAssert(x2 != NULL);
	y1 = *(OBJC_IO **)x1;
	y2 = *(OBJC_IO **)x2;
	float z1 = -200.0, z2 = -200.0;
	float c1 = 1.0, c2 = 1.0;
	if (y1->objc_type == OBJ_GALAXY) {
		if (y1->deV_L[CLASS_INDEX] > y1->exp_L[CLASS_INDEX]) {
			z1 = (float) y1->r_deV[CLASS_INDEX]; 
			c1 = (float) y1->counts_deV[CLASS_INDEX];
		} else {
			z1 = (float) y1->r_exp[CLASS_INDEX];	
			c1 = (float) y1->counts_exp[CLASS_INDEX];
		}
	} else if (y1->objc_type == OBJ_STAR) {
		z1 = -100.0;
		c1 = 1000.00;
	}
	if (y2->objc_type == OBJ_GALAXY) {
		if (y2->deV_L[CLASS_INDEX] > y2->exp_L[CLASS_INDEX]) {
			z2 = (float) y2->r_deV[CLASS_INDEX];
			c2 = (float) y2->counts_deV[CLASS_INDEX];
		} else {
			z2 = (float) y2->r_exp[CLASS_INDEX];
			c2 = (float) y2->counts_exp[CLASS_INDEX];
		}
	} else if (y2->objc_type == OBJ_STAR) {
		z2 = -100.0;
		c2 = 1000.0;
	}
	int res = 0;
	double dbres;
	dbres = (double) c2 * (double) z2 - (double) c1 * (double) z1;
	if (dbres > 0.0) {
		res = 1.0;
	} else if (dbres < 0.0) {
		res = -1.0;
	}
	
	/* 
	if (y1->objc_type == OBJ_GALAXY && 
		y2->objc_type == OBJ_GALAXY) {
		if (z1 > z2) {
			res = -1;
		} else if (z1 < z2) {
			res = 1;
		} else {
			res = 0;
		}
	} else if (y1->objc_type == OBJ_GALAXY && y2->objc_type != OBJ_GALAXY) {
		res = -1;
	} else if (y1->objc_type != OBJ_GALAXY && y2->objc_type == OBJ_GALAXY) {
		res = 1;
	} else {
		res = 0;
	}
	*/
	return(res);
}

int compare_count_galaxy(const void *x1, const void *x2) {
	OBJC_IO *y1, *y2;
	shAssert(x1 != NULL);
	shAssert(x2 != NULL);
	y1 = *(OBJC_IO **) x1;
	y2 = *(OBJC_IO **) x2;
	int res;
	if (y1->type[CLASS_INDEX] == OBJ_GALAXY && y2->type[CLASS_INDEX] == OBJ_GALAXY) {
		float z1, z2;
		z1 = y1->counts_model[CLASS_INDEX];
		z2 = y2->counts_model[CLASS_INDEX];
		if (z1 > z2) {
			res = -1;
		} else if (z1 < z2) {
			res = 1;
		} else {
			res = 0;
		}
	} else if (y1->type[CLASS_INDEX] == OBJ_GALAXY) {
		res = -1;
	} else if (y2->type[CLASS_INDEX] == OBJ_GALAXY) {
		res = 1;
	} else {
		res = 0;
	}
	return(res);
}

int compare_count_star(const void *x1, const void *x2) {
	OBJC_IO *y1, *y2;
	shAssert(x1 != NULL);
	shAssert(x2 != NULL);
	y1 = *(OBJC_IO **) x1;
	y2 = *(OBJC_IO **) x2;
	int res;
	if (y1->type[CLASS_INDEX] == OBJ_STAR && y2->type[CLASS_INDEX] == OBJ_STAR) {
		float z1, z2;
		z1 = y1->psfCounts[CLASS_INDEX];
		z2 = y2->psfCounts[CLASS_INDEX];
		if (z1 > z2) {
			res = -1;
		} else if (z1 < z2) {
			res = 1;
		} else {
			res = 0;
		}
	} else if (y1->type[CLASS_INDEX] == OBJ_STAR) {
		res = -1;
	} else if (y2->type[CLASS_INDEX] == OBJ_STAR) {
		res = 1;
	} else {
		res = 0;
	}
	return(res);
}



int fl_compare(const void *a, const void *b) {
	float aa, bb;
	aa = *(float *) a;
	bb = *(float *) b;
	if (aa > bb) return(1);
	if (aa < bb) return(-1);
	if (aa == bb) return(0);
}

RET_CODE output_memory_stat(LSTRUCT *lstruct, char *fname1, char *fname2) {
char *name = "output_memory_stat";

static LIMPACK *impack = NULL;
if (impack == NULL) impack = thLimpackNew();

int i;
FILE *file;
THOBJCTYPE objctype;
char *objcname;
MEMFL memory;
THOBJCID objcid;
THPIX *xx = NULL, re = 0.0, e = 0.0, phi = 0.0;

LMACHINE *map = lstruct->lmodel->lmachine;
char **mnames = map->mnames;
char **pnames = map->pnames;
void **ps = map->ps;
THOBJC **objcs = (THOBJC **) map->objcs;
int nmodel = map->namp;
if (fname1 != NULL && strlen(fname1) != 0) {
	printf("%s: outputting memory statistics for models to '%s' \n", name, fname1);
	file = fopen(fname1, "w");
	fprintf(file, "%10s %10s %10s %10s %10s %10s %10s %10s \n", 
		"memory", "model", "objcname", "objctype", "objcid", "re", "e", "phi");
	for (i = 0; i < nmodel; i++) {
		char *mname = mnames[i];
		char *pname = pnames[i];
		void *p = ps[i];
		re = VALUE_IS_BAD;
		e = VALUE_IS_BAD;
		phi = VALUE_IS_BAD;
		TYPE t = shTypeGetFromName(pname);
		SCHEMA_ELEM *se = shSchemaElemGetFromType(t, "re");
		if (se != NULL) {
			xx = shElemGet(p, se, NULL);
			if (xx != NULL) re = *xx;
		} 
		se = shSchemaElemGetFromType(t, "e");
		if (se != NULL) {
			xx = shElemGet(p, se, NULL);
			if (xx != NULL) e = *xx;
		}
		se = shSchemaElemGetFromType(t, "phi");
		if (se != NULL) {
			xx = shElemGet(p, se, NULL);
			if (xx != NULL) phi = *xx;
		}
		THOBJC *objc = objcs[i];
		objctype = objc->thobjctype;
		RET_CODE status = thObjcNameGetFromType(objctype, &objcname);		
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (objcname) for (objctype)", name);
			return(status);
		}
		THOBJCID objcid = objc->thid;
		status = thConstructImagePack(lstruct, i, impack);	
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not construct impack for model (%d) '%s'", name, i, mname);
			return(status);
		}
		status = thLimpackGetMemory(impack, &memory);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (impack) memory for model (%d) '%s'", name, i, mname);
			return(status);
		}
		fprintf(file, "%10.4g %10s %10s %10d %10d %10.4g %10.4g %10.4g \n", 
			(float) memory, mname, objcname, (int) objctype, (int) objcid, re, e, phi);
	}
	fclose(file);
}

int nobjc;
if (fname2 != NULL && strlen(fname2) != 0) {
	printf("%s: outputying memory statistics for objects to '%s' \n", name, fname2);
	file = fopen(fname2, "w");
	for (i = 0; i < nobjc; i++) {
		fprintf(file, "%g, %s, %d, %d \n", (float) memory, objcname, (int) objctype, (int) objcid);
	}
	fclose(file);
}
return(SH_SUCCESS);
}


void print_phchain(CHAIN *chain) {
char *name = "print_phchain";
if (chain == NULL) {
	thError("%s: ERROR - null input", name);
	return;
}
int n = shChainSize(chain);
int i;

printf("%s: listing chain elements (r_deV) (chain size = %d) \n", name, n);
for (i = 0; i < n; i++) {
	OBJC_IO *x1;
	x1 = shChainElementGetByPos(chain, i);
	if (x1->objc_type == OBJ_STAR) {
		printf("%02d STAR  : (xc, yc) = (%g, %g), counts = %g \n", i, x1->rowc[CLASS_INDEX], x1->colc[CLASS_INDEX], x1->psfCounts[CLASS_INDEX]); 	
	} else if (x1->objc_type == OBJ_GALAXY) {
		printf("%02d GALAXY: (xc, yc) = (%g, %g), (counts_deV, re_deV, ab_deV, phi_deV) = (%g, %g, %g, %g), (counts_exp, re_exp, ab_exp, phi_exp) = (%g, %g, %g, %g) \n", 
	i, (float) x1->rowc[CLASS_INDEX], (float) x1->colc[CLASS_INDEX], 
	(float) x1->counts_deV[CLASS_INDEX], (float) x1->r_deV[CLASS_INDEX], (float) x1->ab_deV[CLASS_INDEX], (float) x1->phi_deV[CLASS_INDEX],
	(float) x1->counts_exp[CLASS_INDEX], (float) x1->r_exp[CLASS_INDEX], (float) x1->ab_exp[CLASS_INDEX], (float) x1->phi_exp[CLASS_INDEX]);	
	} else {
		printf("%02d %s \n", i, shEnumNameGetFromValue("OBJ_TYPE", x1->objc_type));
	}
}

return;
}

void test_sorted_chain(CHAIN *chain, int (*compare)(const void *, const void *)) {
char *name = "test_sorted_chain";
if (chain == NULL || compare == NULL) {
	thError("%s: ERROR - null input", name);
	return;
}
int n = shChainSize(chain);
int i, error = 0;

printf("%s: listing chain elements (chain size = %d) \n", name, n);
for (i = 0; i < n; i++) {
	OBJC_IO *x1;
	x1 = shChainElementGetByPos(chain, i);
	if (x1->objc_type == OBJ_STAR) {
		printf("%02d STAR  : (xc, yc) = (%g, %g), counts = %g \n", i, x1->rowc[CLASS_INDEX], x1->colc[CLASS_INDEX], x1->psfCounts[CLASS_INDEX]); 	} else {
		printf("%02d GALAXY: (xc, yc) = (%g, %g), (counts_deV, re_deV, ab_deV, phi_deV) = (%g, %g, %g, %g), (counts_exp, re_exp, ab_exp, phi_exp) = (%g, %g, %g, %g) \n", 
	i, (float) x1->rowc[CLASS_INDEX], (float) x1->colc[CLASS_INDEX], 
	(float) x1->counts_deV[CLASS_INDEX], (float) x1->r_deV[CLASS_INDEX], (float) x1->ab_deV[CLASS_INDEX], (float) x1->phi_deV[CLASS_INDEX],
	(float) x1->counts_exp[CLASS_INDEX], (float) x1->r_exp[CLASS_INDEX], (float) x1->ab_exp[CLASS_INDEX], (float) x1->phi_exp[CLASS_INDEX]);	
	}
}

for (i = 0; i < n - 1; i++) {
	const void *x1, *x2;
	x1 = shChainElementGetByPos(chain, i);
	x2 = shChainElementGetByPos(chain, i+1);
	int res = compare(&x1, &x2);
	if (res == -1) {
		thError("%s: ERROR - chain descends at (i = %d)", i);
		error++;
	}
}
if (error) {
	thError("%s: ERROR - found at least (%d) misplaced elements in chain", name, error);
}
return;
}


