#!/bin/bash

source "./write_parfile.sh"
source "./write_condorjob.sh"
source "./find_npsf.sh"
source "./counts_files_in_dir.sh"

name="sim-single-script"

SOHO_DIR="/u/khosrow/thesis/opt/soho"

debug=no
double=no
verbose=no
circular=no
sdss=no
testcenter=no

TODAY=$(date +"%m-%d-%Y")
NOW=$(date +"%T") 

logdir="$SOHO_DIR/log"

Options=$@
Optnum=$#


nrow=1489
ncol=2048

xc=`echo "$nrow/2 + 0.570" | bc`
yc=`echo "$ncol/2 + 0.570" | bc`

loutdir=no
lfield=no
lfield0=no
lrun=no
lrerun=no
lcamcols=no
lbands=no
lLOGFILE=no
lnfield=no
lmodel=no
lexecutable=no
linner_lumcut=no
louter_lumcut=no

outdir=empty
field=empty
field0=empty
run=empty
camcols=empty
bands=empty
nfield=empty
model=empty
executable=empty
inner_lumcut=empty
outer_lumcut=empty

doutdir="$SOHO_DIR/single-galaxy/images/sims"
dfield=101
drun=$DEFAULT_SDSS_RUN
drerun="137"
dcamcols="123456"
dbands="ugriz"
dLOGFILE="$SOHO_DIR/single-galaxy-process-list.txt"
dnfield=5000
dmodel="exp"
dexecutable="do-sim"
dinner_lumcut="0.0001"
douter_lumcut="0.0001"
tempfile="./temp-condor-submit.output"

while getopts ':-:' OPTION ; do
  case "$OPTION" in
    -  ) [ $OPTIND -ge 1 ] && optind=$(expr $OPTIND - 1 ) || optind=$OPTIND
         eval OPTION="\$$optind"
         OPTARG=$(echo $OPTION | cut -d'=' -f2)
         OPTION=$(echo $OPTION | cut -d'=' -f1)
         case $OPTION in
	     --debug     ) debug=yes			 ;;
     	     --verbose   ) verbose=yes			 ;; 
	     --docondor  ) docondor=yes			;;
	     --circular  ) circular=yes			;;
	     --sdss	 ) sdss=yes			;;	
	     --testcenter  ) testcenter=yes		;;
	     --outdir      ) loutdir=yes; outdir="$OPTARG"     ;;
	     --field	 ) lfield=yes; field="$OPTARG"   ;;
	     --field0    ) lfield0=yes; field0="$OPTARG" ;;
	     --run       ) lrun=yes; run="$OPTARG"	 ;;
	     --rerun     ) lrerun=yes; rerun="$OPTARG"    ;;
	     --camcols    ) lcamcols=yes; camcols="$OPTARG" ;;
	     --bands      ) lbands=yes; bands="$OPTARG"	 ;;
	     --model     ) lmodel=yes; model="$OPTARG"	 ;;
	     --inner_lumcut      ) linner_lumcut=yes; inner_lumcut="$OPTARG"     ;;	
	     --outer_lumcut      ) louter_lumcut=yes; outer_lumcut="$OPTARG"     ;; 
	     --logfile   ) lLOGFILE=yes; LOGFILE="$OPTARG" ;;	
             --commandfile ) lcommandfile=yes; commandfile="$OPTARG" ;;
	     --executable  ) lexecutable=yes; executable="$OPTARG"   ;;
	     --nfield      ) lnfield=yes; nfield="$OPTARG"	;;
	* )  echo "$name: invalide options (long)" ;;
         esac
       OPTIND=1
       shift
      ;;
    ? )  echo "$name: invalid options (short) "  ;;
  esac
done

if [ $loutdir == no ]; then
	outdir=$doutdir
	echo "$name: argument (outdir)   set to '$outdir'"
fi
if [ $lfield == no ]; then
	field=$dfield
	echo "$name: argument (field)  set to '$field'"
fi
if [ $lrun == no ]; then
	run=$drun
	echo "$name: argument (run)    set to '$run'"
fi
if [ $lrerun == no ]; then
	rerun=$drerun;
	echo "$name: argument (rerun)  set to '$rerun'"
fi
if [ $lcamcols == no ]; then
	camcols=$dcamcols
	echo "$name: argument (camcol) set to '$camcols'"
fi
if [ $lbands == no ]; then
	bands=$dbands
	echo "$name: argument (bands)   set to '$bands'"
fi
if [ $lLOGFILE == no ]; then
	LOGFILE=$dLOGFILE
	echo "$name: argument (logfile) set to '$LOGFILE'"
fi
if [ $lfield0 == no ]; then
	field0=$field
	echo "$name: argument (field0) set to '$field0'"
fi
if [ $lnfield == no ]; then
	nfield=$dnfield
	echo "$name: argument (nfield) set to '$nfield'"
fi
if [ $nfield -lt 0 ]; then
	echo "$name: argument (nfield) set to invalid value '$nfield'"
	exit
fi
if [ $lmodel == no ]; then
	model=$dmodel
	echo "$name: argument (model) set to '$model'"
fi
if [ $lexecutable == no ]; then 
	executable=$dexecutable
	echo "$name: argument (executable) set to '$executable'"
fi
if [ $linner_lumcut == no ]; then
	inner_lumcut=$dinner_lumcut
	echo "$name: argument (inner_lumcut) set to '$inner_lumcut'"
fi
if [ $louter_lumcut == no ]; then
	outer_lumcut=$douter_lumcut
	echo "$name: argument (outer_lumcut) set to '$outer_lumcut'"
fi

echo "$name: debug    = $debug"
echo "$name: verbose  = $verbose"
echo "$name: EXTRA_FLAGS = '$EXTRA_FLAGS'"

if [ ! -d $datadir ]; then
	echo "$name: making (datadir) '$datadir'"
	mkdir -p $datadir
fi
if [ ! -d $outdir ]; then
	echo "$name: making (outdir) '$outdir'"
	mkdir -p $outdir
fi

srcdir_objcs="$PHOTO_REDUX/$DEFAULT_SDSS_RERUN/$run/objcs"
if [ ! -d $srcdir_objcs ]; then 
	echo "$name: SDSS source directory '$srcdir_objcs' does not exist"
	exit
fi

nband=${#bands}
for (( iband=0; iband<$nband; iband++ ))
do
	band=${bands:$iband:1}
	if [[ $band != "u" && $band != "g" && $band != "r" && $band != "i" && $band != "z" ]]; then
		echo "$name: undefined band ($band) found in (bands = $bands)"
		exit
	fi
done

ncamcol=${#camcols}
if [ $ncamcol == 0 ]; then
	echo "$name: no camcol list was provided"
fi

for (( icamcol=0; icamcol<$ncamcol; icamcol++ ))
do
	camcol=${camcols:$icamcol:1}
	if [[ $camcol != "1" && $camcol != "2" && $camcol != "3" && $camcol != "4"  && $camcol != "5" && $camcol != "6" ]]; then
		echo "$name: undefined camcol ($camcol) found in (camcolss = $camcols)"
		exit
	fi
	condordir="$outdir/$camcol/condor"
	clogdir="$condordir/log"
	cerrordir="$condordir/error"
	coutputdir="$condordir/output"
	csubmitdir="$condordir/submit"
	objcdir="$outdir/$camcol/objc"
	parfiledir="$outdir/$camcol/parfiles"
	photodir="$outdir/$camcol/photo"

	if [ ! -d $condordir ]; then
		echo "$name: making (condordir) '$condordir'"
		mkdir -p $condordir
	fi
	if [ ! -d $clogdir ]; then
		echo "$name: making (clogdir) '$clogdir'"
		mkdir -p $clogdir
	fi
	if [ ! -d $cerrordir ]; then
		echo "$name: making (cerrordir) '$cerrordir'"
		mkdir -p $cerrordir
	fi
	if [ ! -d $coutputdir ]; then
		echo "$name: making (coutputdir) '$coutputdir'"
		mkdir -p $coutputdir
	fi
	if [ ! -d $csubmitdir ]; then
		echo "$name: making (csubmitdir) '$csubmitdir'"
		mkdir -p $csubmitdir
	fi
	if [ ! -d $objcdir ]; then
		echo "$name: making (objcdir) '$objcdir'"
		mkdir -p $objcdir
	fi
	if [ ! -d $parfiledir ]; then 
		echo "$name: making (parfiledir) '$parfiledir'"
		mkdir -p $parfiledir
	fi
	if [ ! -d $photodir ]; then
		echo "$name: making (photodir) '$photodir'"
		mkdir -p $photodir
	fi
done

echo "DATE: $TODAY, TIME: $NOW" >>$LOGFILE
echo "run, camcol, field, sky1, sky2, model1, counts1, re1, e1, phi1, model2, counts2, re2, e2, phi2, dir" >>$LOGFILE

declare -a usky_a=(35 26 30 27 27)
declare -a gsky_a=(85 91 85 84 87)
declare -a rsky_a=(60 56 58 57 55)
declare -a isky_a=(37 48 40 35 38)
declare -a zsky_a=(31 30 26 28 26)

model1=$model
runstr=`printf "%06d" $run` 

declare -a psf_a=(9999 999 99 90 80 70 60 50 40 30 20 10)
npsf=1

if [ $testcenter == yes ]; then

#declare -a dxyc_a=(0.00 0.10 0.15 0.20 0.25 0.30 0.35 0.40 0.45 0.50 0.55 0.60 0.65 0.70 0.75 0.80 0.85 0.90 0.95 1.00)

nxyc=100
mxyc=`echo "$nxyc - 1" | bc`
#for i in {0..$mxyc}
for (( i=0; i<$nxyc; i++ ))
do
 	dxyc_i=`echo "(1.0 * $i) / (1.0 * $nxyc)" | bc -l`	
	dxyc_a[$i]=$dxyc_i
done
nxyc=${#dxyc_a[@]}

else

#declare -a dxyc_a=(0.570) 
#nxyc=${#dxyc_a[@]}

nxyc=100
mxyc=`echo "$nxyc - 1" | bc`
#for i in {0..$mxyc}
for (( i=0; i<$nxyc; i++ ))
do
 	dxyc_i=`echo "(1.0 * $i) / (1.0 * $nxyc)" | bc -l`	
	dxyc_a[$i]=$dxyc_i
done
nxyc=${#dxyc_a[@]}

fi

if [ $circular == yes ]; then 

#defining simulation parameters for re-counts simulations
declare -a sky_a=(50 100 150 200)
if [ $testcenter == yes ]; then 
	declare -a counts1_a=(100000 125000 160000 200000)
	declare -a re_a=(2.0 3.0 4.0)
else
	declare -a counts1_a=(50 100 200 300 500 750 1000 1500 2000 3000 5000 7500 10000 15000 20000 50000 100000 150000 200000 500000) # 750000 1000000)
	declare -a re_a=(2.0 3.0 4.0 5.0 7.0 10.0 15.0 20.0 25.0 30.0 50.0 75.0 100.0 150.0 200.0 250.0 500.0)
fi
declare -a counts2_a=$counts1_a
declare -a counts21_ratio_a=("0.001" "0.005" "0.01" "0.05" "0.1" "0.2" "0.5" "0.75" "1.0" "1.5" "2.0" "3.0" "4.0" "10.0")
declare -a e_a=(0.0 0.05 0.1 0.2 0.5 0.7 0.01 0.02 0.05 0.075 0.1 0.2 0.3 0.5)
declare -a phi_a=(0.0 10.0 20.0 30.0 40.0 50.0 60.0 70.0 80.0 90.0)
declare -a n_a=(0.5 0.75 1.0 2.0 3.0 4.0 5.0 7.5 10.0 12.0)

nsky=2
ncounts1=${#counts1_a[@]}
ncounts2=$ncounts1
ncounts21_ratio=14
nre=${#re_a[@]}
ne=1
nphi=1
nn=10

else

if [ $sdss == yes ]; then 

#defining simulation parameters for sdss based simulations
declare -a sky_a=(0)
declare -a counts1_a=(0)
declare -a counts2_a=$counts1_a
declare -a counts21_ratio_a=(0.0)
declare -a re_a=(2.0)
declare -a e_a=(0.0)
declare -a phi_a=(0.0)
declare -a n_a=(0.5)

nsky=1 ##
ncounts1=1 ##
ncounts2=$ncounts1
ncounts21_ratio=1
nre=1 ##
ne=1 ##
nphi=1 ##
nn=1

else 

#defining simulation parameters for ellipticity simulations
if [ $testcenter == yes ]; then 

declare -a counts1_a=(30000 80000 200000)
declare -a re_a=(2.0 4.0 7.5)
declare -a e_a=(0.0 0.10 0.30 0.50 0.70 0.9 0.95)
declare -a phi_a=(0.0 30.0)



else

declare -a counts1_a=(5000 15000 30000 80000 200000)
declare -a re_a=(2.0 4.0 7.5 15.0 30.0 55.0 100.0 200.0)
declare -a e_a=(0.0 0.01 0.02 0.03 0.04 0.05 0.07 0.10 0.12 0.15 0.17 0.20 0.22 0.25 0.27 0.30 0.32 0.35 0.37 0.40 0.45 0.50 0.55 0.60 0.65 0.70 0.75 0.80 0.85 0.9 0.95)
declare -a phi_a=(0.0 30.0 90.0)

fi


declare -a sky_a=(50 100 150 200)
declare -a counts2_a=$counts1_a
declare -a counts21_ratio_a=(0.0)
declare -a n_a=(0.5 0.75 1.0 2.0 3.0 4.0 5.0 7.5 10.0 12.0)

nsky=2 ##
ncounts1=${#counts1_a[@]} ##
ncounts2=$ncounts1
ncounts21_ratio=1
nre=${#re_a[@]} ##
ne=${#e_a[@]}  ##
nphi=${#phi_a[@]}  ##
nn=10


fi
fi

COUNTS2_CUT="1.0" #cut for excluding underluminous objects
#end of definition 

maxseed=4294967295 # the biggest unsigned int that can be passed as a seed to simulator 

if [ $sdss == no ]; then 

	if [ $double == yes ]; then
	ntotal=$(perl -e "printf "%d", $nsky*$ncounts1*$ncounts21_ratio*$nre*$nre*$ne*$ne*$nphi*$nphi*$nxyc")
	else
	ntotal=$(perl -e "printf "%d", $nsky*$ncounts1*$nre*$ne*$nphi*$nxyc")
	fi

	echo "$name: n-total     = $ntotal"
	echo "$name: n-simulated = $nfield"

	fillfactor=$(perl -e "printf "%g", $nfield/$ntotal")
	echo "$name: parameter space filling factor = $fillfactor"

	fi

ifield=$field
ifield0=$field0
field0=`printf "%04d" $ifield0`

ipsf=0
f1=${psf_a[$ipsf]}
f2=$f1

field0=$(find_f0 --run=$run --camcol="1" --band=$band)
npsf=$(find_npsf --run=$run --camcol="1" --field0=$field0 --band=$band)
echo "$name: npsf = $npsf"
if [ $sdss == yes ]; then 
	nfield=$npsf
	ntotal=$nfield
	echo "$name: sdss-based simulation mode: n-field = $nfield"
	echo "$name: sdss-based simulation mode: n-total = $ntotal"
fi

nsim=$(( nfield * ncamcol ))
njob=0 # number of jobs submitted so far 


ifield=$(( 10#$field0 ))
nsim=$(( nfield * ncamcol ))

datalis_filename="sim-data-$runstr.lis"
datalis_file="$outdir/$datalis_filename"
if [ -e $datalis_file ]; then
	rm $datalis_file
fi

echo "$name: preparing ($nsim) simulation jobs in ($nfield) fields and ($ncamcol) camcols"
for (( isim=0; isim<$nsim; isim++ ))
do

	randport1=$(python -S -c "import random; print random.randrange(1,$ntotal)")
	randport2=$(python -S -c "import random; print random.randrange(1,$ntotal)")
	randport3=$(python -S -c "import random; print random.randrange(1,$ntotal)")
	randport4=$(python -S -c "import random; print random.randrange(1,$ntotal)")
	randport5=$(python -S -c "import random; print random.randrange(1,$ntotal)")
	randport6=$(python -S -c "import random; print random.randrange(1,$ntotal)")

	#echo "$name: random = $randport, mall = $mall, nfield = $nfield, ntotal = $ntotal"

	jfield=$(( isim % nfield ))
	ifield=$(( jfield + field0 ))
	icamcol=$(( isim / nfield ))

	isky=$(( randport1 % nsky ))
	icounts1=$(( randport2 % ncounts1 )) 
	ire1=$(( randport3 % nre ))
	ie1=$(( randport4 % ne ))
	iphi1=$(( randport5 % nphi ))
	ixyc=$(( randport6 % nxyc ))

	sky1=${sky_a[$isky]}
	sky2=${sky_a[$isky]}
	counts1=${counts1_a[$icounts1]}
	re1=${re_a[$ire1]}
	e1=${e_a[$ie1]}
	phi1=${phi_a[$iphi1]}
	dxyc=${dxyc_a[$ixyc]}

	xc0=`echo "$nrow/2" | bc`
	yc0=`echo "$ncol/2" | bc`
	xc=`echo "$xc0 + $dxyc" | bc -l`
	yc=`echo "$yc0 + $dxyc" | bc -l`

	camcol=${camcols:$icamcol:1}

	condordir="$outdir/$camcol/condor"
	clogdir="$condordir/log"
	cerrordir="$condordir/error"
	coutputdir="$condordir/output"
	csubmitdir="$condordir/submit"

	objcdir="$outdir/$camcol/objc"
	photodir="$outdir/$camcol/photo"

	srcdir_redux="$srcdir_objcs/$camcol"
	srcdir_data="$srcdir_objcs/$camcol"
	srcdir_calib="$srcdir_objcs/$camcol"

	if [ $sdss == no ]; then 
		#finding a random psf
		jpsffield=$(python -S -c "import random; print random.randrange(1,$npsf)")
		let "ipsffield = $field0 + $jpsffield - 1"
	else
		#psf should not be random in sdss-based simulation
		jpsffield=$jfield
		let "ipsffield = $field0 + $jpsffield"
	fi

	for (( iband=0; iband<$nband; iband++ )) 
	do

	band=${bands:$iband:1}	
	modelskystring="-sky '-s00 $sky1 -z00 $sky2'"
	if [ "$model1" = "star" ]; then 
		modelstring1="-$model1 '-counts $counts1 -xc $xc -yc $yc -inner_lumcut $inner_lumcut -outer_lumcut $outer_lumcut'"
	else 	
		modelstring1="-$model1 '-counts $counts1 -xc $xc -yc $yc -re $re1 -e $e1 -phi $phi1 -inner_lumcut $inner_lumcut -outer_lumcut $outer_lumcut'"
	fi
	seed=$(python -S -c "import random; print random.randrange(1,$maxseed)")
		
	#designing the file names #
	modelstr=$model1	
	field=`printf "%04d" $ifield`
	psffield=`printf "%04d" $ipsffield`

	dir="$outdir"
	parfiledir="$outdir/$camcol/parfiles"
	parfilename="ff-sim-$runstr-$band$camcol-$field.par"
	targetparfile="$parfiledir/$parfilename"

	if [ $sdss == no ]; then 
		condorpreprefix="condor-sim"
	else
		condorpreprefix="condor-sdss-sim"
	fi
	condorprefix="$condorpreprefix-$runstr-$band$camcol-$field"
	condor_submitfile="$csubmitdir/$condorprefix.submit"
	condor_errorfile="$cerrordir/$condorprefix.error"
	condor_logfile="$clogdir/$condorprefix.log"
	condor_outputfile="$coutputdir/$condorprefix.out"

	if [[ $sdss == no ]]; then 
		condor_argument="\"-band $band -camcol $camcol -run $run -rerun $rerun -field $field -framefile $targetparfile -noobjcfile -seed $seed $modelstring1 $modelskystring\""
		condor_comment="date: $TODAY, time: $NOW"
	else
		condor_argument="\"-band $band -camcol $camcol -run $run -rerun $rerun -field $field -framefile $targetparfile -readskyfile -seed $seed\""
		condor_comment="sdss-based simulation; date: $TODAY, time: $NOW"
	fi

	#echo "field = $field, camcol = $camcol"
	if [[ $sdss == no ]]; then 
	#psf should be copied
	write_parfile --copypsf --datadir="$photodir" --objcdir="$objcdir" --field=$field --psffield=$psffield --run=$run --camcol=$camcol --band=$band --outdir="$outdir" --comment="$comment" --output="$targetparfile"	
	else 
	#for sdss based simulations psf should not be copied
	write_parfile --simulator --datadir="$photodir" --objcdir="$objcdir" --field=$field --psffield=$psffield --run=$run --camcol=$camcol --band=$band --outdir="$outdir" --comment="$comment" --output="$targetparfile"	
	fi

	write_condorjob --restrict --submitfile=$condor_submitfile --errorfile=$condor_errorfile --logfile=$condor_logfile --outputfile=$condor_outputfile --executable="$executable" --arguments="$condor_argument" --comment="$condor_comment"
	
	if [ $verbose == yes ]; then

	echo "$name: $condor_submitfile"
	echo "condor_arguments: $condor_argument"
	echo "model1: $modelstring1"
	echo "sky:    $modelskystring"
	echo "condor: $condor_submitfile"

	else
		echo -n "."
	fi
		
	if [ $debug == yes ]; then
		echo -n 'e'
	else 
		if [ $docondor == yes ]; then
 		if [ $lcommandfile == yes ]; then
			echo "condor_submit $condor_submitfile >> $tempfile" >> $commandfile
		else
			eval "condor_submit $condor_submitfile >> $tempfile"
		fi
		else
			echo "$name: $executable $condor_argument >> $tempfile"
			eval "$executable $condor_argument >> $tempfile"
		fi
		echo -n "."
	fi
	((njob++))
done #for band loop	

if [ "$model1" = "star" ]; then 
	echo "$run, $camcol, $field, $sky1, $sky2, '$model1', $counts1, $xc, $yc, $dir" >> $LOGFILE
	echo "$run $camcol $field $psffield $sky1 $sky2 '$model1' $counts1 $xc $yc" >> $datalis_file

coldesc=$(cat <<'END_HEREDOC'
run J
camcol J
field  J
psffield J
sky1 D DN
sky2 D DN
model1 12A
counts1 D DN
xc D pixel
yc  D pixel
END_HEREDOC
);


else 
	echo "$run, $camcol, $field, $sky1, $sky2, '$model1', $counts1, $re1, $e1, $phi1, $xc, $yc, '$model2', $counts2, $re2, $e2, $phi2, $xc, $yc, $dir" >> $LOGFILE
	echo "$run $camcol $field $psffield $sky1 $sky2 '$model1' $counts1 $re1 $e1 $phi1 $xc $yc ' ' 0.0  0.0 0.0 0.0 0.0 0.0" >> $datalis_file

coldesc=$(cat <<'END_HEREDOC'
run J
camcol J
field  J
psffield J
sky1 D DN
sky2 D DN
model1 12A
counts1 D DN
re1 D DN
e1  D DN
phi1 D DN
xc1 D pixel
yc1 D pixel
model2 12A
counts2 D DN
re2 D DN
e2 D DN
phi2 D DN
xc2 D pixel
yc2 D pixel
END_HEREDOC
);



fi

    
done
echo


keywords=$(cat <<'END_HEREDOC'
CREATOR $$name / name of the script that created the file
CRT-DATE $$TODAY date of creation of the file
history This file was created with the ftools ftcreate task	
END_HEREDOC
);	

simreport_filename="smR-$runstr.fits"
coldesc_lisfilename="coldesc-$runstr.lis"
keyword_lisfilename="keyword-$runstr.lis"
simreport_file="$outdir/$simreport_filename"
coldesc_lisfile="$outdir/$coldesc_lisfilename"
keyword_lisfile="$outdir/$keyword_lisfilename"

echo "$coldesc" > $coldesc_lisfile
echo "$keywords" > $keyword_lisfile
if [ -e $simreport_file ]; then
	rm $simreport_file
fi

ftcreate_exec="/u/khosrow/lib/heasoft-6.16/x86_64-unknown-linux-gnu-libc2.12/bin/ftcreate"
ftcreate_command="$ftcreate_exec cdfile=$coldesc_lisfile datafile=$datalis_file outfile=$simreport_file headfile=$keywords_lisfile"
($ftcreate_command);

#freeing disk space by zipping
if [ $debug == no ]; then
	
	echo ""
	echo "$name: zipping fpC and fpCC files in $dir"
	pattern="fpC*fit"
	path="$dir/$pattern"
	zipcmd="gzip -f $path"
	while $( any_with_ext $dir $pattern ) ; do
		echo -n "-"
		($zipcmd);
		echo -n "."
	done
	echo " "

fi


echo "$name: ($njob) jobs submitted or written to command file"

