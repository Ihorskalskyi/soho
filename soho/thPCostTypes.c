#include "thPCostTypes.h"

PCOST_GENERIC *thPCostGenericNew() {
PCOST_GENERIC *pcost_generic = thCalloc(1, sizeof(PCOST_GENERIC));
return(pcost_generic);
}

void thPCostGenericDel(PCOST_GENERIC *pcost_generic) {
if (pcost_generic == NULL) return;
thFree(pcost_generic);
return;
}


