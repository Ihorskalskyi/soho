typedef struct funcmodel {

  /* name of the function */
  char *funcid;
  /* its real name and the index list */
  char *name;
  int *index;
  /* value for parameters and their names */
  THPIX *parval;
  char **parname;
  int npar;
  /* 
     the working region and the image 
     suggest to pass both from outside 
  */
  REGION *reg, *temp;
  
} FUNCMODEL;

typedef struct objcmodel {
  /* the object type this model is designed for */
  char *type;
  /* the list of functions */
  char **funcs;
  int nfunc;
  /* 
     parameters of the functions 
     --- 
     not sure if the cov, invcov would be useful at all
  */
  THPIX *par;
  THPIX **parcov, **parinvcov;
  int npar;
  /* map machines 
     this maps various parameters onto each other.
     for example various centroids for could be set as equivalent
     the par list above is as concise as possible.
     the map machine gets the list of parameters and generates
     the ordered list of parameters for the function
     
     map machine should naturally include default values 
     for the parameters not listed in the par list above.
  */
  MAPMACHINE *map;
  
  /* the model image - since one works with a big number of objects
     it is best to have one reg as work space and to pass it around 
     between objects when generating the frame image - this way we can 
     avoid multiple creation and deletion of the region
  */
  REGION *reg, *temp;

} OBJCMODEL;

typedef struct fitmodel {
  /* 
     name of the fit 
     an optional field to tell fitmodels from each others 
  */
  char *name;
  /* 
     list of the types for the objects in this fit 
     - can be repetative: naturally you can have several objects
     of the same type in the field
  */
  char **objctype;
  int nobjc;
  /* list of parameters and their covariance */
  THPIX *par;
  THPIX **parcov, **parinvcov;
  int npar;
  /* the map machine
     maps mix model parameters into object parameters
     this also includes default parameters */
  MAPMACHINE *map;
  /* the image of the model */
  REGION *reg, *temp;
  
} FITMODEL;
