#ifndef THJOBS_H
#define THJOBS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "strings.h"

/* astrotools */
#include "ftcl.h"
#include "dervish.h"

/* PHOTO libraries */
#include "phUtils.h"
#include "phPhotoFitsIO.h"
#include "phSpanUtil.h"

/* SOHO libraries */
#include "thMath.h"
#include "thDebug.h"
#include "thIo.h"
#include "thAscii.h"
#include "thProc.h"
#include "thMask.h"

void thProcDoSky (PROCESS *proc);

#endif
