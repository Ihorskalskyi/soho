function write_photocmd {
local name="write_photocmd"

local Options=$@
local Optnum=$#

local SOHO_DIR="/u/khosrow/thesis/opt/soho"

local debug=no
local double=no
local verbose=no

local TODAY=$(date +"%m-%d-%Y")
local NOW=$(date +"%T") 

local logdir="$SOHO_DIR/log"

local lfpCDir=no
local lphotofile=no
local lfield=no
local lrun=no
local lcamcol=no
local lband=no
local lpsf1=no
local lpsf2=no
local lLOGFILE=no
local setpsdir=no
local overwrite=no

local fpCDir=empty
local photofile=empty
local field=empty
local run=empty
local camcol=empty
local band=empty
local psf1=empty
local psf2=empty

local dfpCDir="$SOHO_DIR/single-galaxy/images/sims"
local dfield="102"
local drun=$DEFAULT_SDSS_RUN
local dcamcol=4
local dband="ugriz"
local dpsf1="9999"
local dpsf2="9999"
local dLOGFILE="$SOHO_DIR/single-galaxy-photo-list.txt"
local tempfile="./temp-photo-submit.output"

local maxfield="4290"

while getopts ':-:' OPTION ; do
  case "$OPTION" in
    -  ) [ $OPTIND -ge 1 ] && optind=$(expr $OPTIND - 1 ) || optind=$OPTIND
         eval OPTION="\$$optind"
         OPTARG=$(echo $OPTION | cut -d'=' -f2)
         OPTION=$(echo $OPTION | cut -d'=' -f1)
         case $OPTION in
	     --debug     ) debug=yes			 ;;
	     --double    ) double=yes                    ;;
     	     --verbose   ) verbose=yes			 ;;
	     --fpCDir    ) lfpCDir=yes; fpCDir="$OPTARG"     ;;
	     --photofile ) lphotofile=yes; photofile="$OPTARG"     ;;
	     --field	 ) lfield=yes; field="$OPTARG"   ;;
	     --run       ) lrun=yes; run="$OPTARG"	 ;;
	     --camcol    ) lcamcol=yes; camcol="$OPTARG" ;;
	     --band      ) lband=yes; band="$OPTARG"	 ;; 
	     --logfile   ) lLOGFILE=yes; LOGFILE="$OPTARG" ;;
		--setpsdir ) setpsdir=yes	;;
		--overwrite ) overwrite=yes	;;
	* )  echo "$name: invalide options (long)" ;;
         esac
       OPTIND=1
       shift
      ;;
    ? )  echo "$name: invalid options (short) "  ;;
  esac
done

if [ $lfpCDir == no ]; then
	fpCDir=$dfpCDir
	echo "$name: argument (fpCDir)   set to '$fpCDir'"
fi
if [ $lphotofile == no ]; then
	echo "$name: argument (photofile) should be provided"
	stop
fi
if [ $lfield == no ]; then
	field=$dfield
	echo "$name: argument (field)  set to '$field'"
fi
if [ $lrun == no ]; then
	run=$drun
	echo "$name: argument (run)    set to '$run'"
fi
if [ $lcamcol == no ]; then
	camcol=$dcamcol
	echo "$name: argument (camcol) set to '$camcol'"
fi
if [ $lband == no ]; then
	band=$dband
	echo "$name: argument (band)   set to '$band'"
fi
if [ $lLOGFILE == no ]; then
	LOGFILE=$dLOGFILE
	echo "$name: argument (logfile) set to '$LOGFILE'"
fi

local max_field=5000
if [ $field -gt $max_field ]; then
	echo "$name: ERROR - 'field' may not be greater than '$max_field', found '$field'"
fi

local runstr=`printf "%06d" $run` 
local executable="do-frames"
local photo="/u/khosrow/lib/Linux/photo/bin/photo"
local rerun=$DEFAULT_SDSS_RERUN
local fpCflag="-read_fpC" #read generated fpC file 
local fpMflag="" #"-fpM"
local filterflag="" #"-filterlist {r}"

local fpObjcDir=$fpCDir
local psDir=$fpCDir
local fieldstr=`printf "%04d" $field`
local source_psfDir=$fpCDir #"/u/dss/data/$run/$rerun/objcs/$camcol"
local source_psField="$source_psfDir/psField-$runstr-$camcol-$fieldstr1.fit"
# this is necessary due to FAHL crash
local planDir="/u/dss/sas/dr12/boss/photo/redux/$rerun/$run/photo"
local fpPlanfile="$planDir/fpPlan.par"
local psPlanfile="$planDir/psPlan.par"

local tempfile="./temp-condor-submit.output"


if [ -e $photofile ]; then
echo "$name: command file '$photofile' is to be over-written"
fi

local default_band="u" #this is to test if the image was generated at all in all bands
local source_psField="$source_psfDir/psField-$runstr-$camcol-$fieldstr.fit"
local target_psField="$fpCDir/psField-$runstr-$camcol-$fieldstr.fit"	
local fpObjcfile="$fpCDir/fpObjc-$runstr-$camcol-$fieldstr.fit"
local fpCfile="$fpCDir/fpC-$runstr-$default_band$camcol-$fieldstr.fit"
local fpCgzfile="$fpCfile.gz"
if [[ $source_psField != $target_psField && ! -e $target_psField ]]; then
	eval "cp $source_psField $target_psField"
	echo -n "."
fi

local atlasFlag="" #-noFpAtlas
local fpPlanFlag="-fpPlan $fpPlanfile"
local psPlanFlag="-psPlan $psPlanfile"
local psPlanFlag=""

#note that is noss format rerun comes before run
local baseDir="/u/dss/sas/dr12/boss/photo/redux"
local configDir="$baseDir/$rerun/$run/logs"
local koDir="$baseDir/$rerun/$run/astrom"
local logFileDir="$baseDir/$rerun/$run/logs"
local outputDir="$baseDir/$rerun/$run/astrom"
local parametersDir="$baseDir/$rerun/$run/astrom"
local transFileDir="$baseDir/$rerun/$run/astrom"

	# commandPlan="{baseDir $baseDir outputDir $fpObjcDir imageDir $fpCDir psDir $psDir configDir $configDir parametersDir $parametersDir transFileDir $transFileDir}"
	# the following was changed from above because long strings were creating memory segmentation faults in tcl / tk part of dervish / photo suit.
if [ $setpsdir == yes ]; then 
	local psDir="$PHOTO_REDUX/$DEFAULT_SDSS_RERUN/$run/objcs/$camcol"
else
	local psDir="."
fi
local commandPlan="{baseDir $baseDir outputDir . imageDir . psDir $psDir configDir $configDir parametersDir $parametersDir transFileDir $transFileDir}"
read -r -d '' command <<PHOTO_COMMAND
#=========================
#photo command file
#generated by $name
#DATE: $TODAY, TIME: $NOW
#run:    $run
#camcol: $camcol
#band:   $band
#field:  $field
#==========================
cd $fpCDir
$executable $run $camcol $field $field $filterflag -rerun $rerun -read_fpC $atlasFlag -v 2 $fpPlanFlag $psPlanFlag -plan $commandPlan
defragment_memory
PHOTO_COMMAND
#$executable $run $camcol $field1 $field2 -filterlist $band -rerun $rerun $fpCflag $fpMflag -v 2 -plan {fpCDir $fpCDir}

if [[ -e $fpObjcfile && $overwrite == no ]]; then
	echo "$name: found - $fpObjcfile" >> $LOGFILE
else
	echo -n "p"
	echo "$command" >$photofile
fi

}


