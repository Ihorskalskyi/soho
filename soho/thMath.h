#ifndef THMATH_H
#define THMATH_H

#include "thBasic.h"
#include "thMathTypes.h"

RET_CODE thMathInit();
RET_CODE thMatrixInvert(THPIX **arr, THPIX *b, int n);
RET_CODE LMatrixInvert(MLEFL **arr, MLEFL *b, MLEFL *c, int n, int *singular);  
 
/* function handling */

void assign_coord(VECT  *vec, THPIX x0, THPIX x1);
void assign_knots(VECT  *vec, THPIX x0, THPIX x1);

VECT *do_tcheb_vec(VECT *vec, int n);

REGION *gen_reg_vec_vec(VECT *row, VECT *col);

CHAIN *gen_tcheb_chain_vec(VECT *vec, int n);

CHAIN *gen_Bspline_vec(int n, int m, THPIX x0, THPIX x1);
CHAIN *gen_tcheb_vec(int n, int m, THPIX x0, THPIX x1);

CHAIN *gen_tcheb_chain_func(int nrow, int ncol,  
			    /* size of the region */
			    int n_tr, int n_tc,  
			    /* number of tchebythev polynomials 
			       in each direction
			    */
			    THPIX r0, THPIX r1,     
			    /* beginning and end coordinate in 
			       direction of rows
			       suggested values: 0.0, 1.0
			    */
			    THPIX c0, THPIX c1      
			    /* beginning and end coordinate in 
			       direction of columns
			       suggested values: 0.0, 1.0
			    */
			    );

CHAIN *gen_Bspline_tcheb_chain_func(int nrow, int ncol,  
				    /* size of the region */
				    int n_tr, int n_tc,  
				    /* number of tchebychev polynomials 
				       in each direction
				    */
				    THPIX r0, THPIX r1,     
				    /* beginning and end coordinate in 
				       direction of rows
				       suggested values: 0.0, 1.0
				    */
				    THPIX c0, THPIX c1      
				    /* beginning and end coordinate in 
				       direction of columns
				       suggested values: 0.0, 1.0
				    */
				    );
int thGetVectPosByIndex(CHAIN *vchain, int r);
int thGetFuncPosByPar(CHAIN *fchain, int *indices, THPIX *par);

/* b-splines */

void gen_Bspline_basis(THPIX *x, THPIX *b, 
		       THPIX *t, int j, 
		       int Nx, int N);
void gen_spline_Bjk(THPIX *x, THPIX *b, THPIX *t, THPIX *delta, 
		    int j, int k, int N, int Nx);


void gen_Bspline_basis_inplace(THPIX *x, int Nx, 
			       THPIX *t, int j, int N);
int calc_spline_Dj(THPIX x, THPIX *t, THPIX *delta, 
		   int j, int N);
void gen_spline_Bjk_inplace(THPIX *x, THPIX *t, THPIX *delta, 
			    int j, int k, int N, int Nx);

/* M-spline, I-spline  */
void gen_Mspline_Mik (THPIX *x, THPIX ** Mik, THPIX *t, 
		      int Nx, int Ni, int k);
THPIX *get_Mspline_knots(THPIX *x, int Nx, int k, int *dknots);
THPIX **get_Ispline_basis (THPIX *x, 
			   THPIX *in_t, 
			   int Nx, int N_int, int k, int *df);
THPIX **get_Mspline_basis (THPIX *x, 
			   THPIX *in_t, 
			   int Nx, int N_int, int k, int *df);
THPIX **get_Bspline_basis (THPIX *x, 
			   THPIX *in_t, 
			   int Nx, int N_int, int k, int *n_basis);

THPIX *get_Bspline_knots(THPIX *x, int Nx, int k);

double thGamma(double z);
double thLnGamma(double z);

THPIX circle_square_overlap(THPIX xc, THPIX yc, THPIX r, THPIX xs, THPIX ys, THPIX a, int n);

int thDMatrixInvert(MATHDBLE **arr, MATHDBLE *b, int n);

float  parameter_epsilon_random_draw(void);

MATHDBLE qgamma_inv_asym(MATHDBLE q, MATHDBLE a);
MATHDBLE erf_inv_asym(MATHDBLE x);
MATHDBLE erfc_inv_asym(MATHDBLE x);
MATHDBLE erf_inv(MATHDBLE x);
MATHDBLE erfc_inv(MATHDBLE x);


RET_CODE LMatrixSolveSVD(MLEFL **A, MLEFL *b, MLEFL *x, int nx, MAT *wA, MAT *wQ, VEC **wwVecs);
RET_CODE LMatrixInvertSVD(MLEFL **A, MAT *wA, MAT *wQ, VEC *wlambda, int nx);
RET_CODE thEigenBackSub(const MAT *Q, const VEC *lambda, const VEC *b, VEC *x, VEC *w);
RET_CODE thEigenBack(const MAT *Q, const VEC *lambda, const MAT *A);
RET_CODE LCopyMatOnto(MLEFL **A, MAT *wA);
RET_CODE LCopyOntoMat(MLEFL **A, MAT *wA);
RET_CODE LCopyOntoVec(MLEFL *x, VEC *wx);
RET_CODE LCopyFromVec(VEC *wx, MLEFL *x);

RET_CODE thMatSetIdentity(MAT *A);
RET_CODE LMatrixAbsoluteValueSVD(MLEFL **A, MLEFL **AA, int nx, MAT *wA, MAT *wQ, VEC **wVecs);

#endif
