#include "thMaskTypes.h"
#include "thBasic.h"
#include "thConsts.h"


static int thmask_size_cmp(THMASK *mask1, THMASK *mask2);
static int region_size_type_cmp(REGION *reg1, REGION *reg2);
static RET_CODE randomize_reg(REGION *reg);
static RET_CODE randomize_reg_int(REGION *reg, int nactive);
static int compare_index_value_pair(const void *v1, const void *v2);
static RET_CODE count_pixel_in_reg_int(REGION *reg, int index, int *pixcount);

#if 0
static RET_CODE draw_mini_batch_from_active_mask(WMASK *lwmask, int mini_index_active, int mini_mask_nactive);
#endif

WMASK *thWmaskNew() {
WMASK *lwmask;
lwmask = (WMASK *) thCalloc(1, sizeof(WMASK));
if (WMASK_NACTIVE > 1) {
	void **wshuffle = shCalloc(WMASK_NACTIVE, sizeof(void **));
	int i;
	for (i = 0; i < WMASK_NACTIVE; i++) wshuffle[i] = thIndexValuePairNew();
	lwmask->wshuffle = wshuffle;
}
	
return(lwmask);
}

void thWmaskDel(WMASK *lwmask) {
if (lwmask == NULL) return;
void **wshuffle = lwmask->wshuffle;
if (wshuffle != NULL) {
	int i;
	for (i = 0; i < WMASK_NACTIVE; i++) thIndexValuePairDel(wshuffle[i]);
	thFree(wshuffle);
}
thFree(lwmask);
return;
}

void thWmaskDeepDel(WMASK *wmask) {
if (wmask == NULL) return;
if (wmask->parent != NULL) {
	thWmaskDel(wmask);
	return;
}
if (wmask->W != NULL) {
	shRegDel(wmask->W);
	wmask->W = NULL;
}
if (wmask->mask != NULL) {
	thMaskDel(wmask->mask);
	wmask->mask = NULL;
}
thFree(wmask);
return;
}

RET_CODE thWmaskCopy(WMASK *s, WMASK *t) {
char *name = "thWmaskCopy";
if (s == NULL) {
	thError("%s: ERROR - null (source / input) WMASK", name);
	return(SH_GENERIC_ERROR);
}
if (t == NULL) {
	thError("%s: ERROR - null (target / output) WMASK", name);
	return(SH_GENERIC_ERROR);
}
if (s == t) {
	thError("%s: ERROR - found source = target", name);
	return(SH_GENERIC_ERROR);
}

t->W = s->W;
t->mask = s->mask;
#ifdef THMASKisMASK 
t->maskbit = s->maskbit;
#endif
#ifdef THMASKisSUPERMASK
t->maskbit = s->maskbit;
#endif

t->parent = s;

RET_CODE status;
#if 0
status = thLwmaskSample(t, s->mask_nactive, s->mini_mask_nactive);
#else
status = thLwmaskSample(t, s->mask_nactive);
#endif
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make (lwmask) with (nactive = %d)", name, s->mask_nactive);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE thMaskGetSize(THMASK *mask, int *nrow, int *ncol) {
char *name = "thMaskGetSize";
if (nrow == NULL && ncol == NULL) {
	thError("%s: WARNING - null output placeholders", name);
	return(SH_SUCCESS);
}
if (mask == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (nrow != NULL) *nrow = mask->nrow;
if (ncol != NULL) *ncol = mask->ncol;
return(SH_SUCCESS);
}

RET_CODE thLwmaskPrintInfo(WMASK *wmask, char *comment) {
char *name = "thLwmaskPrintInfo";
if (wmask == NULL) {
	thError("%s: ERROR -  null input", name);
	return(SH_GENERIC_ERROR);
}
THMASK *mask = wmask->mask;
if (mask == NULL) {
	thError("%s: WARNING - null (mask) found in (wmask)", comment);
	return(SH_SUCCESS);
}
int nrow = mask->nrow;
int ncol = mask->ncol;
#if (THMASKisMASK | THMASKisSUPERMASK)
MASKBIT maskbit = 0x0;
MASKBIT bit;

RET_CODE status;
status = thLwmaskGetMaskbit(wmask, &maskbit);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (maskbit) from (wmask)", name);
	return(status);
}
#endif
printf("%s: mask size (nrow = %d, ncol = %d) \n", comment, nrow, ncol);
printf("%s: maskbit = %x \n", comment, (int) maskbit);
int npix, b, i, j;
for (b = 0; b < TH_NMASK_TYPES; b++) {

	bit = 1 << b;
	npix = 0;
	for (i = 0; i < nrow; i++) {
		#if (THMASKisMASK | THMASKisSUPERMASK)
		MASKBIT *row_i = mask->rows_mask[i];
		#endif
		for (j = 0; j < ncol; j++) {
			if (bit & row_i[j]) npix++;
		}
	}
	printf("%s: bit[%d, '%s'] = %x, pixel count = %d \n", comment, b, shEnumNameGetFromValue("TH_MASKTYPE", b), (int) bit, npix);

}

npix = 0;
for (i = 0; i < nrow; i++) {
	#if (THMASKisMASK | THMASKisSUPERMASK)
	MASKBIT *row_i = mask->rows_mask[i];
	#endif
	for (j = 0; j < ncol; j++) {
		if (maskbit & row_i[j]) npix++;
	}
}

printf("%s: current maskbit = %x, pixel count = %d \n", comment, (int) maskbit, npix);

return(SH_SUCCESS);
}

void thMaskClear(THMASK *mask) {
#if THMASKisMASK
shMaskClear(mask);
return;
#endif
#if THMASKisSUPERMASK
shRegClear(mask);
return;
#endif
#if THMASKisSPANMASK
shAssert(1 == 0);
return;
#endif
shAssert(1 == 0);
return;
}

RET_CODE thMaskSetPix(THMASK *mask, MASKBIT maskbit) {
char *name = "thMaskSetPix";
if (mask == NULL) {
	thError("%s: ERROR - null input mask", name);
	return(SH_GENERIC_ERROR);
}
int nrow = mask->nrow;
int ncol = mask->ncol;
if (nrow <= 0 || ncol <= 0) {
	thError("%s: ERROR - unacceptable (nrow = %d, ncol = %d)", name);
	return(SH_GENERIC_ERROR);
}
MASKBIT **mrows = NULL;
#if (THMASKisMASK | THMASKisSUPERMASK)
mrows = mask->rows_mask;
#endif
#if THMASKisSPANMASK
shAssert(1 == 0);
#endif
int i, j;
for (i = 0; i < nrow; i++) {
	MASKBIT *mrows_i = mrows[i];
	for (j = 0; j < ncol; j++) {
		mrows_i[j] = maskbit;
	}
}
return(SH_SUCCESS);
}

RET_CODE thLwmaskPutW(WMASK *lwmask, REGION *W) {
char *name = "thLwmaskPutW";
if (lwmask == NULL) {
	thError("%s: ERROR - null (lwmask) provided", name);
	return(SH_GENERIC_ERROR);
}
if (W == NULL) {
	thError("%s: WARNING - null (W) passed - will proceed", name);
}
if (lwmask->W != NULL) {
	thError("%s: ERROR - (W) found in (lwmask) - will not proceed", name);
	return(SH_GENERIC_ERROR);
}
lwmask->W = W;
	
return(SH_SUCCESS);
}

RET_CODE thLwmaskPutMask(WMASK *lwmask, THMASK *mask) {
char *name = "thLwmaskPutMask";
if (lwmask == NULL) {
	thError("%s: ERROR - null (lwmask) provided", name);
	return(SH_GENERIC_ERROR);
}
if (mask == NULL) {
	thError("%s: WARNING - null (mask) passed - will proceed", name);
}
if (lwmask->mask != NULL) {
	thError("%s: ERROR - (mask) found in (lwmask) - will not proceed", name);
	return(SH_GENERIC_ERROR);
}
lwmask->mask = mask;
return(SH_SUCCESS);
}

RET_CODE thLwmaskPutMaskbit(WMASK *lwmask, MASKBIT maskbit) {
char *name = "thLwmaskPutMaskbit";
if (lwmask == NULL) {
	thError("%s: ERROR - null (lwmask) provided", name);
	return(SH_GENERIC_ERROR);
}
if (maskbit == 0x0) {
	thError("%s: WARNING - (maskbit = %d) passed - will proceed", name, maskbit);
}
if (lwmask->maskbit != 0x0) {
	thError("%s: ERROR - (maskbit) found in (lwmask) - will not proceed", name);
	return(SH_GENERIC_ERROR);
}
lwmask->maskbit = maskbit;
return(SH_SUCCESS);
}

RET_CODE thLwmaskGetW(WMASK *lwmask, REGION **W) {
char *name = "thLwmaskGetW";
if (lwmask == NULL) {
	thError("%s: ERROR - null (lwmask) provided", name);
	return(SH_GENERIC_ERROR);
}
if (W == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (lwmask->W == NULL) {
	thError("%s: WARNING - null (W) found in (lwmask) - will proceed", name);
}
*W = lwmask->W;
return(SH_SUCCESS);
}

RET_CODE thLwmaskGetMask(WMASK *lwmask, THMASK **mask) {
char *name = "thLwmaskGetMask";
if (lwmask == NULL) {
	thError("%s: ERROR - null (lwmask) provided", name);
	return(SH_GENERIC_ERROR);
}
if (mask == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (lwmask->mask == NULL) {
	thError("%s: WARNING - null (mask) found in (lwmask) - will proceed", name);
}
*mask = lwmask->mask;
return(SH_SUCCESS);
}

RET_CODE thLwmaskGetMaskbit(WMASK *lwmask, MASKBIT *maskbit) {
char *name = "thLwmaskGetMaskbit";
if (lwmask == NULL) {
	thError("%s: ERROR - null (lwmask) provided", name);
	return(SH_GENERIC_ERROR);
}
if (maskbit == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (lwmask->maskbit == 0x0) {
	thError("%s: WARNING - (maskbit = 0x0) found in (lwmask) - will proceed", name);
}
*maskbit = lwmask->maskbit;
return(SH_SUCCESS);
}

RET_CODE thLwmaskGetWActive(WMASK *lwmask, REGION **W) {
char *name = "thLwmaskGetWActive";
if (lwmask == NULL) {
	thError("%s: ERROR - null (lwmask) provided", name);
	return(SH_GENERIC_ERROR);
}
if (W == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
int mask_nactive = lwmask->mask_nactive;
if (mask_nactive == 0) {
	*W = lwmask->W;
	return(SH_SUCCESS);
}

int i = lwmask->index_active;
if (i == ROOT_INDEX) {	
	if (lwmask->W == NULL) {
		thError("%s: WARNING - null (W) found in (lwmask) - will proceed", name);
	}
	*W = lwmask->W;
} else if (i < 0 || i >= mask_nactive) {
	thError("%s: ERROR - unacceptable (index_active = %d) while (mask_nactive = %d)", name, i, mask_nactive);
	*W = NULL;
	return(SH_GENERIC_ERROR);
} else {
	*W = lwmask->W_active;
	if (*W == NULL) {
		thError("%s: WARNING - null (W_active) found in (lwmask) for (index_active = %d, mask_nactive = %d)", name, i, mask_nactive);
	}
}
return(SH_SUCCESS);

}

RET_CODE thLwmaskGetMaskActive(WMASK *lwmask, THMASK **mask) {
char *name = "thLwmaskGetMaskActive";
if (lwmask == NULL) {
	thError("%s: ERROR - null (lwmask) provided", name);
	return(SH_GENERIC_ERROR);
}
if (mask == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
int mask_nactive = lwmask->mask_nactive;
if (mask_nactive == 0) {
	*mask = lwmask->mask;
	return(SH_SUCCESS);
}

#if 0
int i = lwmask->index_active;
int mini_i = lwmask->mini_index_active;
if (i == ROOT_INDEX) {	
	if (lwmask->mask == NULL) {
		thError("%s: WARNING - null (mask) found in (lwmask) - will proceed", name);
	}
	*mask = lwmask->mask;
} else if (i < 0 || i >= mask_nactive) {
	thError("%s: ERROR - unacceptable (index_active = %d) while (mask_nactive = %d)", name, i, mask_nactive);
	*mask = NULL;
	return(SH_GENERIC_ERROR);
} else {
	if (mini_i == ROOT_INDEX) {
		*mask = lwmask->mask_active[i];
		if (*mask == NULL) {
			thError("%s: WARNING - null (mask_active) found in (lwmask) for (index_active = %d, mask_nactive = %d)", name, i, mask_nactive);
		}
	} else {
		*mask = lwmask->mini_mask_active;
	}
}
#else
int i = lwmask->index_active;
if (i == ROOT_INDEX) {	
	if (lwmask->mask == NULL) {
		thError("%s: WARNING - null (mask) found in (lwmask) - will proceed", name);
	}
	*mask = lwmask->mask;
} else if (i < 0 || i >= mask_nactive) {
	thError("%s: ERROR - unacceptable (index_active = %d) while (mask_nactive = %d)", name, i, mask_nactive);
	*mask = NULL;
	return(SH_GENERIC_ERROR);
} else {
	*mask = lwmask->mask_active;
	if (*mask == NULL) {
		thError("%s: WARNING - null (mask_active) found in (lwmask) for (index_active = %d, mask_nactive = %d)", name, i, mask_nactive);
	}
}
#endif

return(SH_SUCCESS);

}

RET_CODE thLwmaskGetIndexActive(WMASK *lwmask, int *index_active, int *mini_index_active) {
char *name = "thLwmaskGetIndexActive";
if (lwmask == NULL) {
	thError("%s: ERROR - null (lwmask) provided", name);
	return(SH_GENERIC_ERROR);
}
if (index_active == NULL || mini_index_active == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
*index_active = lwmask->index_active;
#if 0
*mini_index_active = lwmask->mini_index_active;
#endif
return(SH_SUCCESS);
}

int thmask_size_cmp(THMASK *mask1, THMASK *mask2) {
if (mask1 == mask2) {
	return(0);
} else if (mask1 != NULL && mask2 != NULL) {
	if (mask1->nrow == mask2->nrow && mask1->ncol == mask2->ncol) return(0);
	return(1);
} else {
	return(1);
}
shAssert(1 == 0);
return(10);
}

int region_size_type_cmp(REGION *reg1, REGION *reg2) {
if (reg1 == reg2) {
	return(0);
} else if (reg1 != NULL && reg2 != NULL) {
	if (reg1->nrow == reg2->nrow && reg1->ncol == reg2->ncol && reg1->type == reg2->type) return(0);
	return(1);
} else {
	return(1);
}
shAssert(1 == 0);
return(10);
}

RET_CODE randomize_reg(REGION *reg) {
char *name = "randomize_reg";
if (reg == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (reg->type != TYPE_THPIX) {
	thError("%s: ERROR - unsupported pixel type", name);
	return(SH_GENERIC_ERROR);
}
int nrow, ncol;
nrow = reg->nrow;
ncol = reg->ncol;
if (nrow <= 0 || ncol <= 0) {
	thError("%s: ERROR - unsupported region size (nrow, ncol) = (%d, %d)", name, nrow, ncol);
	return(SH_GENERIC_ERROR);
}
int i, j;
for (i = 0; i < nrow; i++) {
	THPIX *rows_i = reg->rows_thpix[i];
	for (j = 0; j < ncol; j++) {
		rows_i[j] = phRandomUniformdev();
	}
}
return(SH_SUCCESS);
}

RET_CODE randomize_reg_int(REGION *reg, int nactive) {
char *name = "randomize_reg_int";
if (reg == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (reg->type != TYPE_S32) {
	thError("%s: ERROR - unsupported pixel type", name);
	return(SH_GENERIC_ERROR);
}
int nrow, ncol;
nrow = reg->nrow;
ncol = reg->ncol;
if (nrow <= 0 || ncol <= 0) {
	thError("%s: ERROR - unsupported region size (nrow, ncol) = (%d, %d)", name, nrow, ncol);
	return(SH_GENERIC_ERROR);
}
if (nactive <= 0) {
	thError("%s: ERROR - unacceptable (nactive = %d)", name, nactive);
	return(SH_GENERIC_ERROR);
}
int i, j;
for (i = 0; i < nrow; i++) {
	S32 *rows_i = reg->rows_s32[i];
	for (j = 0; j < ncol; j++) {
		rows_i[j] = (S32) (((int) (phRandomUniformdev() * (float) nactive)) % nactive);
		/* 
		printf("%d, ", (int) rows_i[j]);
		*/
	}
}

RET_CODE status;
for (i = 0; i < nactive; i++) {
	int pixcount = -1;
	status = count_pixel_in_reg_int(reg, i, &pixcount);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not count (index = %d) in (reg: '%s')", name, i, reg->name);
		return(status);
	}
	printf("%s: DEBUG - index = %d, pix count = %d \n", name, i, pixcount);
}

return(SH_SUCCESS);
}

RET_CODE count_pixel_in_reg_int(REGION *reg, int index, int *pixcount) {
char *name = "count_pixel_in_reg_int";
if (reg == NULL) {
	thError("%s: ERROR - null input (reg)", name);
	return(SH_GENERIC_ERROR);
}
if (reg->nrow <= 0 || reg->ncol <= 0 || reg->type != TYPE_S32) {
	thError("%s: ERROR - unacceptable (reg) size or type", name);
	return(SH_GENERIC_ERROR);
}
if (pixcount == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
int count = 0;
int i, j;
int nrow = reg->nrow;
int ncol = reg->ncol;
for (i = 0; i < nrow; i++) {
	S32 *rows_i = reg->rows_s32[i];
	for (j = 0; j < ncol; j++) {
		if ((S32) rows_i[j] == (S32) index) count++;
		/* 
		printf("%d(%d) ", (int) row_i[j], (int) index);
		*/
	}
}
*pixcount = count;
return(SH_SUCCESS);
}

#if 0

RET_CODE thLwmaskSample(WMASK *lwmask, int nactive, int mini_mask_nactive) {
char *name = "thLwmaskSample";
if (lwmask == NULL) {
	thError("%s: ERROR - null input and output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (nactive <= 0) {
	thError("%s: ERROR - unacceptable value of (mask_nactive = %d) requested", name, nactive);
	return(SH_GENERIC_ERROR);
}
if (nactive > (int) WMASK_NACTIVE) {
	thError("%s: ERROR - unsupported value of (mask_nactive = %d > %d) requested", name, nactive, (int) WMASK_NACTIVE);
	return(SH_GENERIC_ERROR);
}

int i;
THMASK *mask = lwmask->mask;
REGION *W = lwmask->W;
int mask_nactive = lwmask->mask_nactive;
if (nactive < mask_nactive) {
	for (i = nactive; i < mask_nactive; i++) {
		THMASK *mask_i = lwmask->mask_active[i];
		REGION *W_i = lwmask->W_active[i];
		if (mask != mask_i) {
			thMaskDel(mask_i);
			mask_i = NULL;
		}
		lwmask->mask_active[i] = NULL;
		if (W != W_i) {
			shRegDel(W_i);
			W_i = NULL;
		}
		lwmask->W_active[i] = NULL;
	}
}
for (i = 0; i < nactive; i++) {
	THMASK *mask_i = lwmask->mask_active[i];
	REGION *W_i = lwmask->W_active[i];
	if (thmask_size_cmp(mask_i, mask)) {
		thError("%s: WARNING - found mask_i[i = %d] different in shape or type from (mask)", name, i);
		if (mask != mask_i) thMaskDel(mask_i);
		mask_i = NULL;
	}
	if (region_size_type_cmp(W_i, W)) {
		thError("%s: WARNING - found W_i[i = %d] different in shape or type from (W)", name, i);
		if (W != W_i) shRegDel(W_i);
		W_i = NULL;
	}
	if (mask_i == NULL) {
		if (nactive > 1) {
			char mask_name[200];
			if (mask != NULL) {
				sprintf(mask_name, "mask_active[%d]", i);
				mask_i = thMaskNew(mask_name, mask->nrow, mask->ncol);
			}
			lwmask->mask_active[i] = mask_i;
		} else if (nactive == 1) {
			mask_i = mask;
			lwmask->mask_active[i] = mask;
		} else {
			thError("%s: ERROR - binary error - unexpected value of (nactive = %d)", name, nactive);
			return(SH_GENERIC_ERROR);
		}
	}
	if (W_i == NULL) {
		if (nactive > 1) {
			char W_name[200];
			if (W != NULL) {
				sprintf(W_name, "W_active[%d]", i);
				W_i = shRegNew(W_name, W->nrow, W->ncol, W->type);
			}
			lwmask->W_active[i] = W_i;
		} else if (nactive == 1) {
			lwmask->W_active[i] = W;
		} else {
			thError("%s: ERROR - binary error - unexpected value of (nactive = %d)", name, nactive);
		}
	}
}

if (mask != NULL && mask->rows_mask == NULL) {
	thError("%s: ERROR - found null (rows) in main (mask)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
if (nactive > 1 && (W != NULL || mask != NULL)) {
	int irow, j, nrow, ncol;
	if (W != NULL) {
		nrow = W->nrow;
		ncol = W->ncol;
	} else if (mask != NULL) {
		nrow = mask->nrow;
		ncol = mask->ncol;
	} else {
		thError("%s: ERROR - cannot determine (nrow, ncol)", name);
		return(SH_GENERIC_ERROR);
	}
	REGION *random_reg = shRegNew("random reg", nrow, ncol, TYPE_THPIX);
	status = randomize_reg(random_reg);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not randomize region", name);
		return(status);
	}
	for (i = 0; i < nactive; i++) {
		THPIX lower_bound = (i * (THPIX) 1.0) / (nactive * (THPIX) 1.0);
		THPIX upper_bound = ((i + 1) * (THPIX) 1.0) / (nactive  * (THPIX) 1.0);
		if (i == (nactive - 1)) upper_bound += 0.5;
		if (mask != NULL) {
			THMASK *mask_i = lwmask->mask_active[i];
			if (mask_i->rows_mask == NULL) {
				thError("%s: ERROR - null (rows) is (mask[i_m = %d])", name, i);
				return(SH_GENERIC_ERROR);
			}
			for (irow = 0; irow < nrow; irow++) {
				MASKBIT *mask_i_i = mask_i->rows_mask[irow];
				MASKBIT *master_mask_i = mask->rows_mask[irow];
				THPIX *random_i = random_reg->rows_thpix[irow];
				for (j = 0; j < ncol; j++) {
					if (random_i[j] >= lower_bound && random_i[j] < upper_bound) {
						mask_i_i[j] = master_mask_i[j];
					} else {
						mask_i_i[j] = 0x0;
					}
				}
			}
		}
		if (W != NULL) {
			REGION *W_i = lwmask->W_active[i];
			for (irow = 0; irow < nrow; irow++) {
				THPIX *W_i_i = W_i->rows_thpix[irow];
				THPIX *master_W_i = W_i->rows_thpix[irow];
				THPIX *random_i = random_reg->rows_thpix[irow];
				for (j = 0; j < ncol; j++) {
					if (random_i[j] >= lower_bound && random_i[j] < upper_bound) {
						W_i_i[j] = master_W_i[j];
					} else {
						W_i_i[j] = (THPIX) 0.0;
					}
				}
			}
		}
	}	
	shRegDel(random_reg);
}

lwmask->mask_nactive = nactive;

int mini_index_active = 0;

status = draw_mini_batch_from_active_mask(lwmask, mini_index_active, mini_mask_nactive);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not draw (mini_mask) for (mini_index_active = %d, mini_mask_nactive = %d)", name, mini_index_active, mini_mask_nactive);
	return(status);
}

return(SH_SUCCESS);
}	

#else

RET_CODE thLwmaskSample(WMASK *lwmask, int nactive) {
char *name = "thLwmaskSample";
if (lwmask == NULL) {
	thError("%s: ERROR - null input and output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (nactive <= 0) {
	thError("%s: ERROR - unacceptable value of (mask_nactive = %d) requested", name, nactive);
	return(SH_GENERIC_ERROR);
}

int i;
THMASK *mask = lwmask->mask;
REGION *W = lwmask->W;
MASKBIT maskbit = lwmask->maskbit;
if (nactive == 1) {
	if (lwmask->random_active != NULL) shRegDel(lwmask->random_active);
	lwmask->random_active = NULL;
	if (lwmask->W_active != NULL && lwmask->W_active != lwmask->W) {
		shRegDel(lwmask->W_active);
		lwmask->W_active = NULL;
	}
	lwmask->W_active = lwmask->W;
	if (lwmask->mask_active != NULL && lwmask->mask_active != lwmask->mask) {
		thMaskDel(lwmask->mask_active);
		lwmask->mask_active = NULL;
	}
	lwmask->mask_active = lwmask->mask;
	lwmask->mask_nactive = nactive;

	lwmask->index_active = 0;
	if (lwmask->index_active_list != NULL) thFree(lwmask->index_active_list);
	lwmask->index_active_list = NULL;
	lwmask->index_active_index = 0;

	int i, j;
	int mask_size = 0;
	for (i = 0; i < mask->nrow; i++) {
		MASKBIT *mask_i = mask->rows_mask[i];
		for (j = 0; j < mask->ncol; j++) {
			if (mask_i[j] & maskbit) mask_size++;
		}
	}
	lwmask->mask_active_size = mask_size;

	return(SH_SUCCESS);
}

RET_CODE status;

THMASK *mask_active = lwmask->mask_active;
if (mask_active == mask) {
	mask_active = NULL;
	lwmask->mask_active = NULL;
} else if (thmask_size_cmp(mask_active, mask)) {
	thError("%s: WARNING - found (mask_active) different in shape or type from (mask)", name);
	if (mask != mask_active) thMaskDel(mask_active);
	mask_active = NULL;
}
REGION *W_active = lwmask->W_active;
if (W_active == W) {
	W_active = NULL;
	lwmask->W_active = NULL;
} else if (region_size_type_cmp(W_active, W)) {
	thError("%s: WARNING - found (W_active) different in shape or type from (W)", name);
	if (W != W_active) shRegDel(W_active);
	W_active = NULL;
}
if (mask_active == NULL) {
	if (nactive > 1) {
		char mask_name[200];
		if (mask != NULL) {
			sprintf(mask_name, "mask_active[nactive = %d]", nactive);
			mask_active = thMaskNew(mask_name, mask->nrow, mask->ncol);
		} 
		lwmask->mask_active = mask_active;
	} else if (nactive == 1) {
		mask_active = mask;
		lwmask->mask_active = mask;
	} else {
		thError("%s: ERROR - binary error - unexpected value of (nactive = %d)", name, nactive);
		return(SH_GENERIC_ERROR);
	}
}
if (W_active == NULL) {
	if (nactive > 1) {
		char W_name[200];
		if (W != NULL) {
			sprintf(W_name, "W_active[nactive = %d]", nactive);
			W_active = shRegNew(W_name, W->nrow, W->ncol, W->type);
		}
		lwmask->W_active = W_active;
	} else if (nactive == 1) {
		lwmask->W_active = W;
	} else {
		thError("%s: ERROR - binary error - unexpected value of (nactive = %d)", name, nactive);
	}
}


if (mask != NULL && mask->rows_mask == NULL) {
	thError("%s: ERROR - found null (rows) in main (mask)", name);
	return(SH_GENERIC_ERROR);
}

if (nactive > 1 && (W != NULL || mask != NULL)) {
	if (lwmask->index_active_list != NULL && nactive > lwmask->mask_nactive) {
		thFree(lwmask->index_active_list);
		lwmask->index_active_list = NULL;
	}	
	if (lwmask->index_active_list == NULL) lwmask->index_active_list = thCalloc(nactive, sizeof(int));
	int *index_active_list = lwmask->index_active_list;
	for (i = 0; i < nactive; i++) index_active_list[i] = i;
	int index_active_index = 0;
	int active_index = index_active_list[index_active_index];

	int irow, j, nrow, ncol;
	if (W != NULL) {
		nrow = W->nrow;
		ncol = W->ncol;
	} else if (mask != NULL) {
		nrow = mask->nrow;
		ncol = mask->ncol;
	} else {
		thError("%s: ERROR - cannot determine (nrow, ncol)", name);
		return(SH_GENERIC_ERROR);
	}
	REGION *random_reg = lwmask->random_active;
	if (random_reg!= NULL && (random_reg->nrow != nrow || random_reg->ncol != ncol || random_reg->type != TYPE_S32)) {
		shRegDel(random_reg);
		random_reg = NULL;
		lwmask->random_active = NULL;
	}
	if (random_reg == NULL) {
		char random_name[200];
		sprintf(random_name, "random_reg_int [nactive = %d]", nactive);
		random_reg = shRegNew(random_name, nrow, ncol, TYPE_S32);
		lwmask->random_active = random_reg;
	}
	status = randomize_reg_int(random_reg, nactive);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not randomize region", name);
		return(status);
	}
	int mask_active_size = 0;
	if (mask != NULL) {
		THMASK *mask_active = lwmask->mask_active;
		if (mask_active->rows_mask == NULL) {
			thError("%s: ERROR - null (rows) is (mask_active)", name);
			return(SH_GENERIC_ERROR);
		}
		for (irow = 0; irow < nrow; irow++) {
			MASKBIT *mask_active_i = mask_active->rows_mask[irow];
			MASKBIT *master_mask_i = mask->rows_mask[irow];
			S32 *random_i = random_reg->rows_s32[irow];
			for (j = 0; j < ncol; j++) {
				if ((random_i[j] % nactive) == active_index && (master_mask_i[j] & maskbit)) {
					mask_active_i[j] = master_mask_i[j];
					mask_active_size++;
				} else {
					mask_active_i[j] = 0x0;
				}
			}
		}
	}
	lwmask->mask_active_size = mask_active_size;
	printf("%s: DEBUG - found (mask_size = %d) while (maskbit = %d, active_index = %d, nactive = %d) \n", name, mask_active_size, maskbit, active_index, nactive);
	fflush(stdout);
	fflush(stderr);

	if (W != NULL) {
		REGION *W_active = lwmask->W_active;
		for (irow = 0; irow < nrow; irow++) {
			THPIX *W_active_i = W_active->rows_thpix[irow];
			THPIX *master_W_i = W->rows_thpix[irow];
			int *random_i = random_reg->rows_s32[irow];
			for (j = 0; j < ncol; j++) {
				if ((random_i[j] % nactive) == active_index) {
					W_active_i[j] = master_W_i[j];
				} else {
					W_active_i[j] = (THPIX) 0.0;
				}
			}
		}
	}
}

lwmask->mask_nactive = nactive;

return(SH_SUCCESS);
}	

#endif
	
RET_CODE thLwmaskFree(WMASK *lwmask) {
char *name = "thLwmaskFree";
if (lwmask == NULL) {
	thError("%s: WARNING - null input", name);
	return(SH_SUCCESS);
}

#if 0
REGION *W = lwmask->W;
THMASK *mask = lwmask->mask;
int i;
for (i = 0; i < WMASK_NACTIVE; i++) {
	THMASK *mask_i = lwmask->mask_active[i];
	REGION *W_i = lwmask->W_active[i];
	if (mask_i != NULL && mask_i != mask) thMaskDel(mask_i);
	if (W_i != NULL && W_i != W) shRegDel(W_i);
	lwmask->mask_active[i] = NULL;
	lwmask->W_active[i] = NULL;
}
lwmask->mask_nactive = 0;
lwmask->index_active = 0;
lwmask->W = NULL;
lwmask->mask = NULL;
lwmask->maskbit = 0x0;


#else

REGION *W = lwmask->W;
THMASK *mask = lwmask->mask;
THMASK *mask_active = lwmask->mask_active;
REGION *W_active = lwmask->W_active;
REGION *random_active = lwmask->random_active;
int *index_active_list = lwmask->index_active_list;

if (mask_active != NULL && mask_active != mask) thMaskDel(mask_active);
if (W_active != NULL && W_active != W) shRegDel(W_active);
if (random_active != NULL) shRegDel(random_active);
if (index_active_list != NULL) thFree(index_active_list);

lwmask->mask_nactive = 0;
lwmask->index_active = 0;
lwmask->W = NULL;
lwmask->mask = NULL;
lwmask->maskbit = 0x0;
lwmask->W_active = NULL;
lwmask->mask_active = NULL;
lwmask->random_active = NULL;
lwmask->index_active_list = NULL;




#endif
return(SH_SUCCESS);
}

#if 0
RET_CODE thLwmaskShuffle(WMASK *lwmask) {
char *name = "thLwmaskShuffle";
if (lwmask == NULL) {
	thError("%s: ERROR - null input (lwmask)", name);
	return(SH_GENERIC_ERROR);
}
int mask_nactive = lwmask->mask_nactive;
if (mask_nactive <= 1) {
	thError("%s: WARNING - no need to shuffle when (nactive = %d)", name, mask_nactive);
	return(SH_SUCCESS);
}

void **random_array = lwmask->wshuffle;
if (random_array == NULL) {
	thError("%s: ERROR - null shuffling array in (lwmask)", name);
	return(SH_GENERIC_ERROR);
}

int i;
for (i = 0; i < mask_nactive; i++) {
	INDEX_VALUE_PAIR *random_elem = random_array[i];
	random_elem->index = i;
	random_elem->value = (float) phRandomUniformdev();
	random_elem->objc1 = lwmask->mask_active[i];
	random_elem->objc2 = lwmask->W_active[i];
}
if (mask_nactive > 0) qsort(random_array, mask_nactive, sizeof(void *), &compare_index_value_pair);
for (i = 0; i < mask_nactive; i++) {
	INDEX_VALUE_PAIR *random_elem = random_array[i];
	lwmask->mask_active[i] = random_elem->objc1;
	lwmask->W_active[i] = random_elem->objc2;
}
for (i = 0; i < mask_nactive; i++) {
	INDEX_VALUE_PAIR *random_elem = random_array[i];
	memset(random_elem, '\0', sizeof(INDEX_VALUE_PAIR));
}

return(SH_SUCCESS);
}
#else

RET_CODE thLwmaskShuffle(WMASK *lwmask) {
char *name = "thLwmaskShuffle";
if (lwmask == NULL) {
	thError("%s: ERROR - null input (lwmask)", name);
	return(SH_GENERIC_ERROR);
}
int mask_nactive = lwmask->mask_nactive;
if (mask_nactive <= 1) {
	thError("%s: WARNING - no need to shuffle when (nactive = %d)", name, mask_nactive);
	return(SH_SUCCESS);
}

void **random_array = lwmask->wshuffle;
if (random_array == NULL) {
	thError("%s: ERROR - null shuffling array in (lwmask)", name);
	return(SH_GENERIC_ERROR);
}

int i;
for (i = 0; i < mask_nactive; i++) {
	INDEX_VALUE_PAIR *random_elem = random_array[i];
	random_elem->index = i;
	random_elem->value = (float) phRandomUniformdev();
	random_elem->objc1 = NULL;
	random_elem->objc2 = NULL;
}
if (mask_nactive > 0) qsort(random_array, mask_nactive, sizeof(void *), &compare_index_value_pair);
for (i = 0; i < mask_nactive; i++) {
	INDEX_VALUE_PAIR *random_elem = random_array[i];
	lwmask->index_active_list[i] = random_elem->index;
}
int index_active = lwmask->index_active;
int index_active_index = lwmask->index_active_index;
int index_active_index2 = -1;
for (i = 0; i < mask_nactive; i++) {
	if (lwmask->index_active_list[i] == index_active) index_active_index2 = i;
}
if (index_active_index2 < 0) {
	thError("%s: ERROR - could not find the matching (index_active_index)", name);
	return(SH_GENERIC_ERROR);
}
if (index_active_index2 != index_active_index) {
	int index2 = lwmask->index_active_list[index_active_index2];
	int index1 = lwmask->index_active_list[index_active_index];
	lwmask->index_active_list[index_active_index] = index2;
	lwmask->index_active_list[index_active_index2] = index1;
}
for (i = 0; i < mask_nactive; i++) {
	INDEX_VALUE_PAIR *random_elem = random_array[i];
	memset(random_elem, '\0', sizeof(INDEX_VALUE_PAIR));
}

return(SH_SUCCESS);
}

#endif


RET_CODE thLwmaskNextActive(WMASK *lwmask) {
char *name = "thLwmaskNextActive";
if (lwmask == NULL) {
	thError("%s: ERROR - null input (lwmask)", name);
	return(SH_GENERIC_ERROR);
}
#if 0
int mask_nactive = lwmask->mask_nactive;
int index_active = lwmask->index_active;
int mini_mask_nactive = lwmask->mini_mask_nactive;
int mini_index_active = lwmask->mini_index_active;

if (mask_nactive < 0) {
	thError("%s: ERROR - found (mask_nactive = %d) in (lwmask)", name, mask_nactive);
	return(SH_GENERIC_ERROR);
}
if (mini_mask_nactive < 0) {
	thError("%s: ERROR - found (mini_mask_nactive = %d) in (lwmask)", name, mini_mask_nactive);
	return(SH_GENERIC_ERROR);
}
if (mask_nactive <= 1 && mini_mask_nactive <= 1) {
	thError("%s: WARNING - found (mask_nactive = %d, mini_mask_nactive = %d) in (lwmask)", name, mask_nactive, mini_mask_nactive);
	return(SH_SUCCESS);
}
if (index_active < 0 || index_active >= mask_nactive) {
	if (index_active == ROOT_INDEX) {
		thError("%s: WARNING - found root index in (lwmask) - not drawing the next", name);
		return(SH_SUCCESS);
	}
	thError("%s: ERROR - found (index_active = %d, mask_nactive = %d) in (lwmask)", name, index_active, mask_nactive);
	return(SH_GENERIC_ERROR);
}

int mini_index_active_prev = mini_index_active;
int index_active_prev = index_active;

mini_index_active++;
if (mini_index_active % mini_mask_nactive == 0) {
	mini_index_active = 0;
	index_active++;
	index_active %= mask_nactive;
}

RET_CODE status;
status = draw_mini_batch_from_active_mask(lwmask, mini_index_active, mini_mask_nactive);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not draw mini_mask", name);
	return(status);
}

lwmask->index_active_prev = index_active_prev;
lwmask->mini_index_active_prev = mini_index_active_prev;
lwmask->index_active = index_active;
lwmask->mini_index_active = mini_index_active;
#else
int mask_nactive = lwmask->mask_nactive;
int index_active = lwmask->index_active;

if (mask_nactive < 0) {
	thError("%s: ERROR - found (mask_nactive = %d) in (lwmask)", name, mask_nactive);
	return(SH_GENERIC_ERROR);
}
if (mask_nactive <= 1) {
	thError("%s: WARNING - found (mask_nactive = %d in (lwmask)", name, mask_nactive);
	return(SH_SUCCESS);
}
if (index_active < 0 || index_active >= mask_nactive) {
	if (index_active == ROOT_INDEX) {
		thError("%s: WARNING - found root index in (lwmask) - not drawing the next", name);
		return(SH_SUCCESS);
	}
	thError("%s: ERROR - found (index_active = %d, mask_nactive = %d) in (lwmask)", name, index_active, mask_nactive);
	return(SH_GENERIC_ERROR);
}

int index_active_index = lwmask->index_active_index;
index_active_index++;
index_active_index %= mask_nactive;
index_active = lwmask->index_active_list[index_active_index];

THMASK *mask = lwmask->mask;
REGION *W = lwmask->W;
REGION *random_reg = lwmask->random_active;
MASKBIT maskbit = lwmask->maskbit;

if (random_reg == NULL) {
	thError("%s: ERROR - found (random_active = null) while (nactive = %d)", name, mask_nactive);
	return(SH_GENERIC_ERROR);
}

int irow, j, nrow, ncol;
int mask_active_size = 0;
if (mask != NULL) {
	if (random_reg->nrow != mask->nrow || random_reg->ncol != mask->ncol) {
		thError("%s: ERROR - found (random_active) with mismatching size to (mask)", name);
		return(SH_GENERIC_ERROR);
	}	
	THMASK *mask_active = lwmask->mask_active;
	if (mask_active->rows_mask == NULL) {
		thError("%s: ERROR - null (rows) is (mask_active)", name);
		return(SH_GENERIC_ERROR);
	}
	nrow = mask->nrow;
	ncol = mask->ncol;
	int non_zero_count = 0;
	if (mask_active == mask) {
		thError("%s: ERROR - (mask_active) not properly allocated", name);
		return(SH_GENERIC_ERROR);
	}
	for (irow = 0; irow < nrow; irow++) {
		MASKBIT *mask_active_i = mask_active->rows_mask[irow];
		MASKBIT *master_mask_i = mask->rows_mask[irow];
		S32 *random_i = random_reg->rows_s32[irow];
		for (j = 0; j < ncol; j++) {
			if (((random_i[j] % mask_nactive) == index_active) && (master_mask_i[j] & maskbit)) {
				mask_active_i[j] = master_mask_i[j];
				mask_active_size++;
			} else {
				mask_active_i[j] = 0x0;
			}
			if (master_mask_i[j] != (MASKBIT) 0) non_zero_count++;
		}
	}
	printf("%s: DEBUG - non-zero pixel count in mask = %d \n", name, non_zero_count);

	if (mask_active_size <= 0) {
		thError("%s: WARNING - found (mask_size = %d) for (index_active = %d, nactive = %d, maskbit = %d)", name, mask_active_size, index_active, mask_nactive, maskbit);
	} else {
		printf("%s: DEBUG - found (mask_size = %d) for (index_active = %d, nactive = %d, maskbit = %d) \n", name, mask_active_size, index_active, mask_nactive, maskbit);
		fflush(stdout);
		fflush(stdin);
	}
} else {
	thError("%s: WARNING - null master (mask) found in (lwmask)", name);
}

lwmask->mask_active_size = mask_active_size;

if (W != NULL) {
	if (random_reg->nrow != W->nrow || random_reg->ncol != W->ncol) {
		thError("%s: ERROR - found (random_active) with mismatching size to (W)", name);
		return(SH_GENERIC_ERROR);
	}
	REGION *W_active = lwmask->W_active;
	nrow = W->nrow;
	ncol = W->ncol;
	for (irow = 0; irow < nrow; irow++) {
		THPIX *W_active_i = W_active->rows_thpix[irow];
		THPIX *master_W_i = W->rows_thpix[irow];
		S32 *random_i = random_reg->rows_s32[irow];
		for (j = 0; j < ncol; j++) {
			if ((random_i[j] % mask_nactive) == index_active) {
				W_active_i[j] = master_W_i[j];
			} else {
				W_active_i[j] = (THPIX) 0.0;
			}
		}
	}
}

#endif


return(SH_SUCCESS);
}


int compare_index_value_pair(const void *v1, const void *v2) {
shAssert(v1 != NULL);
shAssert(v2 != NULL);
INDEX_VALUE_PAIR *r1 = *((void **) v1);
INDEX_VALUE_PAIR *r2 = *((void **) v2);
int w = (int) (1024.0 * 1024.0 * (r1->value - r2->value));
return(w);
}

INDEX_VALUE_PAIR *thIndexValuePairNew() {
INDEX_VALUE_PAIR *x = shCalloc(1, sizeof(INDEX_VALUE_PAIR));
return(x);
}

void thIndexValuePairDel(INDEX_VALUE_PAIR *x) {
if (x == NULL) return;
thFree(x);
return;
}

#if 0
RET_CODE draw_mini_batch_from_active_mask(WMASK *lwmask, int mini_index_active, int mini_mask_nactive) {
char *name = "draw_mini_batch_from_active_mask";

if (lwmask == NULL) {	
	thError("%s: ERROR - null input placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (mini_index_active == ROOT_INDEX) {	
	thError("%s: WARNING - no drawing when (mini_index = ROOT_INDEX)", name);
	return(SH_SUCCESS);
}
if (mini_mask_nactive <= 0) {
	thError("%s: ERROR - unexpected value (mini_nactive = %d <= 0)", name, mini_mask_nactive);
	return(SH_GENERIC_ERROR);
}
if (mini_index_active < 0 || mini_index_active >= mini_mask_nactive) {
	thError("%s: WARNING - (mini_mask_index_active = %d) to be converted in modulo", name);
	mini_index_active %= mini_mask_nactive;
}
int index_active = lwmask->index_active;
THMASK *mask_active = lwmask->mask_active[index_active];
int mask_nactive = lwmask->mask_nactive;

int nrow = mask_active->nrow;
int ncol = mask_active->ncol;

lwmask->mini_index_active_prev = lwmask->mini_index_active;
MASKBIT maskbit = lwmask->maskbit;

int i, j;
if (mini_mask_nactive > 1) {
	THMASK *mini_mask_active = lwmask->mini_mask_active;
	if (mini_mask_active != NULL && (mini_mask_active->nrow != nrow || mini_mask_active->ncol != ncol)) {
		thMaskDel(mini_mask_active);
		mini_mask_active = NULL;
	}
	if (mini_mask_active == NULL) {
		mini_mask_active = thMaskNew("mini_mask_active", nrow, ncol);
		lwmask->mini_mask_active = mini_mask_active;
	}
	for (i = 0; i < nrow; i++) memcpy(mini_mask_active->rows[i], mask_active->rows[i], ncol * sizeof(MASKBIT));
	int pix_count = 0, mini_batch_size = 0;
	for (i = 0; i < nrow; i++) {
		MASKBIT *rows = mini_mask_active->rows_mask[i];
		for (j = 0; j < ncol; j++) {
			if (rows[j] & maskbit) {
				if (pix_count % mini_mask_nactive == mini_index_active) {
					mini_batch_size++;
				} else {
					rows[j] = 0x0;
				}
				pix_count++;
			}	
		}
	}	
	lwmask->mini_batch_size_active = mini_batch_size;
	lwmask->mini_mask_nactive = mini_mask_nactive;
} else {
	THMASK *mini_mask_active = lwmask->mini_mask_active;
	int found = 0, i_found = -1;
	for (i = 0; i < mask_nactive; i++) {
		if (mini_mask_active == lwmask->mask_active[i]) {
			found = 1;
			i_found = i;
		}
	}
	if (!found && mini_mask_active != NULL) {
		thError("%s: ERROR - could not find (mask_active) matching (mini_mask_active) while (mini_mask_nactive = %d)", name, mini_mask_nactive);
		return(SH_GENERIC_ERROR);

		thMaskDel(mini_mask_active);
		lwmask->mini_mask_active = NULL;
		mini_mask_active = NULL;
	}
	mini_mask_active = mask_active;
	thError("%s: WARNING - (mini_mask_nactive = %d), no mini-batch creation \n", name, mini_mask_nactive);
	int mini_batch_size = 0;
	if (mini_mask_active->rows_mask == NULL) {
		thError("%s: ERROR - found null (rows) in (mini_mask_active)", name);
		return(SH_GENERIC_ERROR);
	}
	for (i = 0; i < nrow; i++) {
		MASKBIT *rows = mini_mask_active->rows_mask[i];
		for (j = 0; j < ncol; j++) {
			if (rows[j] & maskbit) {
					mini_batch_size++;
			}	
		}
	}	
	lwmask->mini_batch_size_active = mini_batch_size;
	lwmask->mini_mask_active = mini_mask_active;
	lwmask->mini_mask_nactive = mini_mask_nactive;
}

return(SH_SUCCESS);
}
#endif

RET_CODE thLwmaskGetMiniBatchSize(WMASK *lwmask, int *mini_batch_size) {
char *name = "thLwmaskGetMiniBatchSize";
if (lwmask == NULL) {
	thError("%s: ERROR - null input placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (mini_batch_size == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
#if 0
*mini_batch_size = lwmask->mini_batch_size_active;
#else
*mini_batch_size = lwmask->mask_active_size;
#endif
return(SH_SUCCESS);
}
