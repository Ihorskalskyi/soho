fields =  lindgen(300)
runs =  [1033]

reruns =  ['150x5M50', '150x6M50',  $
           '150x5M100',  '150x6M100',  $
           '150x5', '150x6',  $
           '150x5N64',  '150x6N64', $
           '150x5R64',  '150x6R64']

reruns =  ['150x1', '150x2',  '150x3',  '150x4',  '150x5', $
           '150x6',  '150x7',  '150x8',  '150x9',  '150x10']

reruns =  [ '150x2M30', '150x3M30', '150x4M30', '150x5M30',  '150x6M30', $
            '150x2R64', '150x3R64', '150x4R64', '150x5R64',  '150x6R64', $
            '150x2M20R64', '150x3M20R64', '150x4M20R64', '150x5M20R64',  '150x6M20R64', $
            '150x2M30R64', '150x3M30R64', '150x4M30R64', '150x5M30R64',  '150x6M30R64', $
            '150x2M20R128', '150x3M20R128', '150x4M20R128', '150x5M20R128',  '150x6M20R128', $
            '150x2M30R128', '150x3M30R128',  '150x4M30R128', '150x5M30R128',  '150x6M30R128',  $
            '150x2M30R192', '150x3M30R192',  '150x4M30R192', '150x5M30R192',  '150x6M30R192']
           

camcols = [2, 3, 4]
bands =  ['r', 'g', 'u', 'i', 'z']

directory =  '/u/khosrow/dss/data/'


print, "* reduced rerun[",  r, "]= ",  reruns[r]
print, "* complex rerun[",  c, "]= ",  reruns[c]

xr =  [0]
xc =  [0]
mr =  [0]
mc =  [0]

xr =  fetch_sky_fit(run = runs,  rerun =  reruns[r],  camcol =  camcols,  $
                    band =  bands,  field =  fields,  directory =  directory,  $
                    /coeffs,  /invcov,  /cov)


xc =  fetch_sky_fit(run = runs,  rerun =  reruns[c],  camcol =  camcols,  $
                    band =  bands,  field =  fields,  directory =  directory,  $
                    /coeffs,  /invcov,  /cov)

; filtering based on the availability of a given field
; camcol, band in (run, rerun) pair based on
; the value of run in the  SKYFIT structure

help,  xr

runlist = xr[*].run
index =  where(runlist ne -1)
runlist = [0]

xr =  xr[index]
xc =  xc[index]

help,  xr

runlist = xc[*].run
index =  where(runlist ne -1)
runlist = [0]

xr =  xr[index]
xc =  xc[index]

help,  xr

npixel =  xr[*].npixel
index =  where(npixel ne 0)
xr =  xr[index]
xc =  xc[index]

print,  "* filtered for xr.npixel == 0"
help,  xr

npixel =  xc[*].npixel
index =  where(npixel ne 0)
xr =  xr[index]
xc =  xc[index]

print,  "* filtered for xc.npixel == 0"
help,  xr

;;npixel =  [-1]
index = [-1]

;help,  xr,  /stru

ir =  [0];,  0,  1,  1,  2,  2,  2,  0,  1] ;, 1,  1,  1,  2,  2,  2,  2,  3,  3,  3,  3,  4,  4,  4,  4]
ic =  [0];,  1,  0,  1,  1,  0,  2,  2,  2] ;, 2,  3,  4,  1,  2,  3,  4,  1,  2,  3,  4,  1,  2,  3,  4]

mr =  get_sky_matrices(fit = xr, ir =  ir,  ic =  ic,  /ampl1)
mc =  get_sky_matrices(fit = xc, ir =  ir,  ic =  ic,  /ampl1)

;; freeing memory
free_skyfit,  fit = xr
free_skyfit,  fit = xc

;help,  mr,  /stru
;help,  mc,  /stru

npix = xr[*].npixel
print,  "*** good pixel statistics: "
print,  "median (NPIXEL) = ",  median(npix)
print,  "min    (NPIXEL) = ",  min(npix)
print,  "max    (NPIXEL) = ",  max(npix)
;; finding the most and the least crowded fields 
minnpix =  min(npix)
j = where(npix eq minnpix)
jj =  j[0]
maxnpix =  max(npix)
i =  where(npix eq maxnpix)
ii =  i[0]
;; freeing memory
npix =  [0]

chisq = compare_skyfit_chisq(mr, mc)

print,  "*** comparative chisq statistics: "
print,  "median(X2) = ",  median(chisq)
print,  "min   (X2) = ",  min(chisq)
print,  "max   (X2) = ",  max(chisq)

;; handling the chisq fit

condition = ((ir[*] ne 0) + (ic[*] ne 0))
index =  where(condition,  count)
index =  [0]

nu =  count + 1
p =  [0.05,  0.01,  0.001,  0.0001]
crt =  replicate(0.0,  n_elements(p))
for i =  0,  n_elements(p) - 1,  1 do begin
    crt[i] =  chisqr_cvf(p[i], nu)
endfor
count =  replicate(0,  n_elements(p))
for i =  0,  n_elements(p) - 1,  1 do begin
    thiscrt =  crt[i]
    index =  where(chisq ge thiscrt,  thiscount)
    count[i] =  thiscount
endfor
index = [0]

ratio =  replicate(0.0,  n_elements(p))
nsample =  n_elements(chisq) * 1.0
for i = 0,  n_elements(p) - 1,  1 do begin
    ratio[i] = count[i] / nsample
endfor

nustr =  strtrim(string(nu,  format =  "(I5)"),  2)
for i =  0,  n_elements(p) -1,  1 do begin
    pstr =  strtrim(string(p[i],  format =  "(F6.4)"),  2)
    rstr =  strtrim(string(ratio[i],  format =  "(F6.3)"),  2)
    print,  "* population ratio( X2 > X2(nu = ",  $
            nustr,  ", p = ",  pstr,  ") = ",  rstr
endfor


;; now plotting the histogram

index =  where(chisq ne 0.0000,  nfield)

if index[0] ne -1 then begin
    logchisq =  alog10(chisq[index])
    help,  logchisq
    plot,  histogram(logchisq,  nbins = nfield / 10)
endif else begin
    print,  "all chisq values are 0.000 - a perfect match between fits"
endelse

print,  "the following file contains the minimum pixel count: "
print,  fetch_sky_filename(run =  mr[jj].run,  rerun =  mr[jj].rerun,  $
                           band =  mr[jj].band,  field =  mr[jj].field, $
                           camcol =  mr[jj].camcol,  $
                           directory = "/u/khosrow/dss/data/")

print,  "the following file contains the maxmimum pixel count: "
print,  fetch_sky_filename(run =  mr[ii].run,  rerun =  mr[ii].rerun,  $
                           band =  mr[ii].band,  field =  mr[ii].field, $
                           camcol =  mr[ii].camcol,  $
                           directory = "/u/khosrow/dss/data/")

END
