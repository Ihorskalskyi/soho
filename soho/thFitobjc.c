#include "thFitobjc.h"

int thFitobjcGetNDeriv(FITOBJC *objc, RET_CODE *status) {
  
  char *name = "thFitobjcGetNDeriv";
  if (objc == NULL) {
    thError("%s: ERROR - cannot work with a null object", name);
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(-1);
  }

  int n;

  PROCESS *proc;
  FRAME *frame;
  OBJCMODEL *objcmodel;
  FUNCMODEL *funcmodel;

  switch (objc->type) {
  case PROCESS_TYPE:
    proc = (PROCESS *) objc->val;
    n = thProcGetNFrame(proc, status);
    return(n);
    break;
  case FRAME_TYPE:
    frame = (FRAME *) objc->val;
    n = thFrameGetNObjcmodel(frame, status);
    return(n);
    break;
  case OBJCMODEL_TYPE:
    objcmodel = (OBJCMODEL *) objc->val;
    n = thObjcmodelGetNFuncmodel(objcmodel, status);
    return(n);
    break;
  case FUNCMODEL_TYPE:
    funcmodel = (FUNCMODEL *) objc->val;
    n = thFuncmodelGetNComponent(funcmodel, status);
    return(n);
    break;
  default:
    thError("%s: ERROR - can't handle this type for object", name);
    return(-1);
    break;
  }

  /* the program should not reach here */
  thError("%s: ERROR: problem with the software, results not reliable", 
		 name);
  return(-1);
}
    
FITOBJC *thFitobjcDerive(FITOBJC *objc, int i, RET_CODE *status) {

  char *name = "thFitobjcDerive";
  if (objc == NULL) {
    thError("%s: ERROR - can't handle null object", name);
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
  }

  if (i < 0) {
    thError("%s: ERROR - negative index is not defined", name);
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
  }
  
  PROCESS *proc;
  FRAME *frame;
  OBJCMODEL *objcmodel;
  FUNCMODEL *funcmodel;
  long funcstat;

  void *derval;
  FITOBJC_TYPE dertype;
  FITOBJC *derobjc;
  void *dermap;

  switch (objc->type) {
  case PROCESS_TYPE:
    proc = (PROCESS *) objc->val;
    derval = shChainElementGetByPos(proc->frames, i);
    dermap = thFrameGetMapmachine((FRAME *) derval);
    dertype = FRAME_TYPE;
    break;
  case FRAME_TYPE:
    frame = (FRAME *) objc->val;
    derval = shChainElementGetByPos(frame->objcs, i);
    dermap = thObjcmodelGetMapmachine((OBJCMODEL *) derval);
    dertype = OBJCMODEL_TYPE;
    break;
  case OBJCMODEL_TYPE:
    objcmodel = (OBJCMODEL *) objc->val;
    derval = shChainElementGetByPos(objcmodel->funcs, i);
    dermap = thFuncmodelGetMapmachine((FUNCMODEL *) derval);
    dertype = FUNCMODEL_TYPE;
    break;
  case FUNCMODEL_TYPE:
    funcmodel = (FUNCMODEL *) objc->val;
    funcstat = funcmodel->stat;
    if (funcstat & COMPLEX_FUNCMODEL) {
      derval = shChainElementGetByPos(funcmodel->components, i);
      dermap = thFuncmodelGetMapmachine((FUNCMODEL *) derval);
      dertype = FUNCMODEL_TYPE;
    } else {
      if (i != 0) {
	thError("%s: ERROR - for simple FUNCMODELs, only 0's derivative is defined", name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
      }
      thError("%s: WARNING - returning the self pointer for a simple FUNCMODEL", name);
      if (status != NULL) *status = SH_SUCCESS;
      return(objc);
    }
    break;
  default:
    thError("%s: ERROR - can't handle this type for object", name);
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
    break;
  }

  RET_CODE newstatus;
  derobjc = (FITOBJC *) thFitobjcNew();
  newstatus = thFitobjcPut(derobjc, derval, dertype, objc, dermap);
  if (status != NULL) *status = newstatus;
  
  return(derobjc);
}


/* handling the pixelized representation of FITOBJCs */

RET_CODE thFitobjcModel(FITOBJC *objc, REGION *dirreg, REGION *fftwreg) {
  
  return(SH_GENERIC_ERROR);

}

RET_CODE thFitobjcModelAdd(FITOBJC *objc, 
			   REGION *dirreg, REGION *fftwreg,
			   REGION *dirtemp, REGION *fftwtemp,
			   long *FitobjcStat) {
  
  /* 
     this function returns (and adds) the direct or inverse (or both) representation of 
     a model 
  */

  char *name = "thFitobjcModelAdd";

  if (objc == NULL) {
    thError("%s: cannot work on a null object", name);
    return(SH_GENERIC_ERROR);
  }

  RET_CODE status;

  /* if dealing with a simple function model */
  if (objc->type == FUNCMODEL_TYPE && 
      !(((FUNCMODEL *)(objc->val))->stat & COMPLEX_FUNCMODEL)) {
    status = thSimpleFuncModelAdd((FUNCMODEL *) (objc->val), 
				  dirreg, fftwreg, 
				  dirtemp, fftwtemp,
				  FitobjcStat);
    if (status != SH_SUCCESS) {
      thError("%s: ERROR - representing simple function model", name);
    }
    return(status);
  }

  /* 
     now you are dealing with a complex FITOBJC 
     you should take derivatives 
  */

  int i, n;
  n = thFitobjcGetNDeriv(objc, &status);

  if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not find the N-derivative of the object", name);
	return(status);
  }	
  FITOBJC *derobjc;

  for (i = 0; i < n; i++) {
    derobjc = thFitobjcDerive(objc, i, &status);
    if (status != SH_SUCCESS) {
      thError("%s: ERROR - could not derive sub-object", name);
      return(status);
    }
    status = thFitobjcModelAdd(objc, dirreg, fftwreg, dirtemp, fftwtemp, FitobjcStat);
    if (status != SH_SUCCESS) {
      thError("%s: ERROR - could not calculate the model for the derivative object", name);
      return(status);
    }
  }
      
  return(status);

}

RET_CODE thSimpleFuncModelAdd(FUNCMODEL *objc, 
			      REGION *dirreg, REGION *fftwreg, 
			      REGION *dirtemp, REGION *fftwtemp,
			      long *FitobjcStat) {
  

  char *name = "thSimpleFuncModelAdd";

  if (objc == NULL) {
    thError("%s: ERROR - cannot work with null function models", name);
    return(SH_GENERIC_ERROR);
  }
  
  if (objc->stat & COMPLEX_FUNCMODEL) {
    thError("%s: ERROR - cannot work on complex functions", name);
    return(SH_GENERIC_ERROR);
  }

  RET_CODE status;
  long myFuncStat;
  
  status = thFuncmodelGetStat(objc, &myFuncStat);
  
  /* checking the status of function */
  if ((status != SH_SUCCESS) || (myFuncStat & UNKNOWN_FUNCMODEL)) {
    thError("%s: ERROR - could not locate object prototype in the database", name);
    return(SH_GENERIC_ERROR);
  }  

  /* checking the availability of space */
   if (dirreg == NULL && !(myFuncStat & HAS_FFTW_FUNCMODEL)) {
    thError("%s: ERROR - the function has direct representation ONLY but no space is speicified", name);
    return(SH_GENERIC_ERROR);
  }
  if (dirreg == NULL && (myFuncStat & HAS_DIR_FUNCMODEL)) {
    thError("%s: WARNING - the function has direct representation but no space is speicified", name);
  }
  if (fftwreg == NULL && !(myFuncStat & HAS_DIR_FUNCMODEL)) {
    thError("%s: ERROR - the function has inverse (fftw) representation ONLY but no space is speicified", name);
    return(SH_GENERIC_ERROR);
  }
  if (fftwreg == NULL && (myFuncStat & HAS_FFTW_FUNCMODEL)) {
    thError("%s: WARNING - the function has inverse (fftw) representation but no space is speicified", name);
  }

  if (dirreg == NULL) dirtemp = NULL;
  if (fftwreg == NULL) fftwtemp = NULL;

  if (myFuncStat & CALCULATE_FUNCMODEL) {
    if ((myFuncStat & HAS_DIR_FUNCMODEL) && dirtemp == NULL) {
      thError("%s: WARNING -  direct representations is needed, no space provided", name);
    }
    if ((myFuncStat & HAS_FFTW_FUNCMODEL) && fftwtemp == NULL) {
      thError("%s: WARNING -  fftw representations is needed, no space provided", name);
    }
    /* calculating */
    status = thSimpleFuncCalculate(objc, dirtemp, fftwtemp, &myFuncStat);
    if (status != SH_SUCCESS) {
      thError("%s: problem calculating the function", name);
      return(SH_GENERIC_ERROR);
    }
    /* adding up the result */
    if ((myFuncStat & HAS_DIR_FUNCMODEL) && (dirtemp != NULL)) {
      shRegAdd(dirreg, dirtemp, dirreg);
      thRegMakeZero(dirtemp);
    }
    if (myFuncStat & HAS_FFTW_FUNCMODEL && (fftwtemp != NULL)) {
      shRegAdd(fftwreg, fftwtemp, fftwreg);
      thRegMakeZero(fftwtemp);
    }

    /* now updating the fitobjectstatus */
    if (FitobjcStat != NULL) *FitobjcStat = (*FitobjcStat) | myFuncStat | CALCULATE_FUNCMODEL;
  }

  /* now describing which elements in tank should be used to represent the function */
  CHAIN *funclist;
  funclist = shChainNew ("FUNCMODEL");

  /* calculating the part which is saved */
  if (myFuncStat & SAVED_FUNCMODEL) {

    status = thSimpleFuncModelDecompose(objc, funclist); 
    if ((status != SH_SUCCESS) || (shChainSize(funclist) == 0)) {
      thError("%s: ERROR - problem decomposing the function", name);
      return(SH_GENERIC_ERROR);
    }

    int i, n;
    FUNCMODEL *thisFunc;

    n = shChainSize(funclist);

    for (i = 0; i < n; i++) {
      thisFunc = (FUNCMODEL *) shChainElementGetByPos(funclist, i);
      if ((dirtemp != NULL) && (thisFunc->dirreg != NULL)) {
	status = thRegInplaceAddRegCrossScalar(dirtemp, thisFunc->dirreg, thisFunc->coeff);
	if (status != SH_SUCCESS) {
	  thError("%s: ERROR - unable to load calculated pixel values", name);
	  return(SH_GENERIC_ERROR);
	}
      }
      if ((fftwtemp != NULL) && (thisFunc->fftwreg != NULL)) {
	status = thRegInplaceAddRegCrossScalar(fftwtemp, thisFunc->fftwreg, thisFunc->coeff);
	if (status != SH_SUCCESS) {
	  thError("%s: ERROR - unable to load calculated pixel values", name);
	  return(SH_GENERIC_ERROR);
	}
      }
    }
    
    /* now adding up the calculated pixelized value to the given regions */
    if (dirtemp != NULL) shRegAdd(dirreg, dirtemp, dirreg);
    if (fftwtemp != NULL) shRegAdd(fftwreg, fftwtemp, fftwreg);

    /* now zero-ing the temporary working space */
    if (thisFunc->dirreg != NULL) thRegMakeZero(dirtemp);
    if (thisFunc->fftwreg != NULL) thRegMakeZero(fftwtemp);

    /* now updating the status flag of the function model */
    if (FitobjcStat != NULL) *FitobjcStat = (*FitobjcStat) | thisFunc->stat | SAVED_FUNCMODEL;
  }
    
  return(SH_SUCCESS);
  
}
  
