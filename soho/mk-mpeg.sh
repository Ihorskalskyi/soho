#!/bin/bash
root="/u/khosrow/thesis/opt/soho/single-galaxy/images"
ffmpeg="/u/khosrow/lib/ffmpeg/ffmpeg -loglevel panic" 
mencoder="/usr/bin/mencoder"

videoflag="-vcodec huffyuv"
codecflag="-ovc lavc -lavcopts vcodec=ffvhuff"
subsizeflag="-subfont-text-scale 2"
msgflag="-msglevel all=-1"

killold=yes
killmovie=no

declare -a dir1_a=("deV" "exp" "deV"); n1=1
declare -a dir2_a=("deV-exp" "exp" "deV" "deV-exp"); n2=3
declare -a dir3_a=("fpCC" "fpC"); n3=2
declare -a dir4_a=("single" "multiple"); n4=2


for (( i1=0; i1<n1; i1++ )) do
dir1=${dir1_a[$i1]}

for (( i2=0; i2<n2; i2++ )) do
dir2=${dir2_a[$i2]}

for (( i3=0; i3<n3; i3++ )) do
dir3=${dir3_a[$i3]}

for (( i4=0; i4<n4; i4++ )) do
dir4=${dir4_a[$i4]}

dir="$root/$dir1/9999-9999/$dir2-$dir3-$dir4"

if [[ -d $dir ]]; then 

echo -n "$dir: "
for (( ifield=1; ifield<5000; ifield++ )) do
fieldstr=`printf "%04d" $ifield`
prefix="$dir/mvRes-1033-r4-$fieldstr"
filename="$prefix-X0020.pgm"
if [[ -e $filename ]]; then

	#generating the movie via ffmpeg
	framepattern="$prefix-X%04d.pgm"
	moviefile="$prefix.avi"
	command="$ffmpeg -i $framepattern $videoflag $moviefile"
	if [[ -e $moviefile && $killold == yes ]]; then
		rm -r -f $moviefile
		echo -n "d"
	fi
	if [[ ! -e $moviefile ]]; then
		echo -n "."
		eval "$command"
	fi
	#adding subtitles
	subtitlefile="$prefix.sub"
	outputfile="$prefix.subbed.avi"
	command="$mencoder $moviefile -sub $subtitlefile $subsizeflag -o $outputfile $codecflag $msgflag"
	if [[ -e $subtitlefile ]]; then
		if [[ -e $outputfile && $killold == yes ]]; then
			rm -r -f $outputfile
			echo -n "D"
		fi
		echo -n "~"
		eval "$command"
		if [[ $killmovie == yes ]]; then
			rm -r -f $moviefile
		fi
	fi	
fi

done
echo " "

fi

done
done
done
done


