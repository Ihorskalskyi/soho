#include "thCalibTypes.h"

CRUDE_CALIB *thCrudeCalibNew() {
CRUDE_CALIB *cc = thCalloc(1, sizeof(CRUDE_CALIB));
return(cc);
}

void thCrudeCalibDel(CRUDE_CALIB *x) {
if (x == NULL) return;
thFree(x);
return;
}

CRUDE_CALIB_IO *thCrudeCalibIoNew() {
CRUDE_CALIB_IO *cc = thCalloc(1, sizeof(CRUDE_CALIB_IO));
return(cc);
}

void thCrudeCalibIoDel(CRUDE_CALIB_IO *cc) {
if (cc == NULL) return;
thFree(cc);
return;
}

PHCALIB *thPhcalibNew() {
	PHCALIB *fc = thCalloc(1, sizeof(PHCALIB));
	fc->rerun = thCalloc(MX_STRING_LEN, sizeof(char));
	return(fc);
}

RET_CODE thPhcalibCopy(PHCALIB *t, PHCALIB *s) {
	char *name = "thPhcalibCopy";
	if (t != NULL && s == NULL) {
		thError("%s: ERROR - cannot copy null (source) to non-null (target)", name);
		return(SH_GENERIC_ERROR);
	}
	if (t == NULL && s== NULL) {
		thError("%s: WARNING - null (source) and (target)", name);
		return(SH_GENERIC_ERROR);
	}
	char *rerun = t->rerun;
	memcpy(t, s, sizeof(PHCALIB));
	if (rerun == NULL && s->rerun != NULL) {
		t->rerun = thCalloc(MX_STRING_LEN, sizeof(char));
	} else {
		t->rerun = rerun;
	}
	if (s->rerun != NULL) strcpy(t->rerun, s->rerun);
	return(SH_SUCCESS);
}
	
void thPhcalibDel(PHCALIB *fc) {
	if (fc == NULL) return;
	if (fc->rerun != NULL) thFree(fc->rerun);
	thFree(fc);
	return;
}

CALIB_WOBJC_IO *thCalib_wobjc_ioNew() {
CALIB_WOBJC_IO *cobjc = thCalloc(1, sizeof(CALIB_WOBJC_IO));
return(cobjc);
}

void thCalib_wobjc_ioDel(CALIB_WOBJC_IO *cobjc) {
if (cobjc == NULL) return;
thFree(cobjc);
return;
}

void thCalibWObjcIoCopy(CALIB_WOBJC_IO *t, CALIB_WOBJC_IO *s) {
if (s == NULL && t == NULL) return;
shAssert(s != NULL && t != NULL);
memcpy(t, s, sizeof(CALIB_WOBJC_IO));
return;
}


CALIB_PHOBJC_IO *thCalib_phobjc_ioNew(ncolor) {
CALIB_PHOBJC_IO *x = thCalloc(1, sizeof(CALIB_PHOBJC_IO));

#if CALIB_PHOBJC_IO_CHAR_ARRAY
#else
x->objid = thCalloc(MX_STRING_LEN, sizeof(char));
x->parentid = thCalloc(MX_STRING_LEN, sizeof(char));
x->fieldid = thCalloc(MX_STRING_LEN, sizeof(char));
x->rerun = thCalloc(MX_STRING_LEN, sizeof(char));
#endif

return(x);
}

void thCalib_phobjc_ioDel(CALIB_PHOBJC_IO *x) {
if (x == NULL) return;

#if CALIB_PHOBJC_IO_CHAR_ARRAY
#else
thFree(x->objid);
thFree(x->parentid);
thFree(x->fieldid);
thFree(x->rerun);
#endif

thFree(x);
return;
}

RET_CODE thCalibPhObjcIoCopy(CALIB_PHOBJC_IO *t, CALIB_PHOBJC_IO *s) {

if (t == s) return(SH_SUCCESS);
shAssert(s != NULL);
shAssert(t != NULL);
memcpy(t, s, sizeof(CALIB_PHOBJC_IO));
#if CALIB_PHOBJC_IO_CHAR_ARRAY
#else
strcpy(t->objid, s->objid, MX_STRING_LEN * sizeof(char));
strcpy(t->parentid, s->parentid, MX_STRING_LEN * sizeof(char));
strcpy(t->fieldid, s->fieldid, MX_STRING_LEN * sizeof(char));
strcpy(t->rerun, s->rerun, MX_STRING_LEN * sizeof(char));
#endif
return(SH_SUCCESS);
}

GALACTIC_CALIB *thGalactic_calibNew() {
GALACTIC_CALIB *x = thCalloc(1, sizeof(GALACTIC_CALIB));
return(x);
}

void thGalactic_calibDel(GALACTIC_CALIB *gc) {
if (gc == NULL) return;
thFree(gc);
return;
}

REGRESSION_INFO *thRegression_infoNew() {
REGRESSION_INFO *x = thCalloc(1, sizeof(REGRESSION_INFO));
return(x);
}

void thRegression_infoDel(REGRESSION_INFO *x) {
if (x == NULL) return;
thFree(x);
return;
}



