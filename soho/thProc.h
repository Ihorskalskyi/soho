#ifndef THPROC_H
#define THPROC_H

#include "thDefTypes.h"
#include "thProcTypes.h"
#include "thMath.h"
#include "thPsf.h"
#include "thSky.h"
#include "thMask.h"
#include "thPar.h"
#include "thIo.h"
#include "thMle.h"
#include "thStat.h"
#include "thLRG.h"
#include "thFitmask.h"

#include "thClass.h"

/* frame level loaders, machines, and dumpers */

RET_CODE thFrameLoadAmpl(FRAME *f);

RET_CODE thFrameLoadImage(FRAME *f); 
  
RET_CODE thFrameFakeImage(FRAME *f, int run, int field, char band, int camcol, char *rerun);

RET_CODE thFrameLoadPhMask(FRAME *f);

RET_CODE thFrameLoadPhAtlas(FRAME *f);

RET_CODE thFrameLoadSkybasisset(FRAME *f);
  
RET_CODE thFrameLoadObjcbasisset(FRAME *f);

RET_CODE thFrameLoadPhObjc(FRAME *f);

RET_CODE thFrameLoadPhCalibObjc(FRAME *f, GC_SOURCE gcsource);
RET_CODE thFrameLoadPhCalibObjcFromSweepFile(FRAME *f);
RET_CODE thFrameLoadPhCalibObjcFromGC(FRAME *f);

RET_CODE thFrameLoadPhPsf(FRAME *f);

RET_CODE thFrameLoadThPsf(FRAME *f);

RET_CODE thFrameLoadThPsfFromFile(FRAME *f);

RET_CODE thFrameLoadWeight(FRAME *f);

RET_CODE thFrameLoadThMask(FRAME *f);

RET_CODE thFrameUpdateThMask(FRAME *f, MAPMACHINE *map, MASK_UPDATE mupdate);

RET_CODE thFrameLoadCalib(FRAME *f);

RET_CODE thFrameLoadLstruct(FRAME *f, LMODEL *background, CALIBTYPE calibtype, LFITRECORDTYPE rtype);

RET_CODE thFrameFitSky(FRAME *f);

RET_CODE thFrameDumpThPsf (FRAME *f);

RET_CODE thFrameDumpSkymodel (FRAME *f);

/* process level loaders and tank handlers */

CHAIN *thDeffilesRead(char **files);
RET_CODE thProcAddObjcdef(PROCESS *proc,  OBJCDEF *elem);
RET_CODE thProcAddMaskdef(PROCESS *proc,  MASKDEF *elem);
RET_CODE thProcAddSentence(PROCESS *proc, SENTENCE *elem);
RET_CODE thProcSeparateDef(PROCESS *proc);
RET_CODE thProcCreateDefTree(PROCESS *proc);
RET_CODE thProcLoadDef(PROCESS *proc);

RET_CODE thProcLoadEcalib(PROCESS *proc, char *file);

RET_CODE thProcLoadCcdconfig(PROCESS *proc, char *file);

RET_CODE thProcLoadSbf(PROCESS *proc, char *file);

RET_CODE thProcLoadObf(PROCESS *proc, char *file);

int thProcTankGetPosByFilename(CHAIN *tchain, char *filename);

CHAIN *thProcTankGetChainByFilename (CHAIN *tchain, char *filename);

/* searching for a frame in a process */

FRAME *thProcGetFrameByPos(PROCESS *proc, int pos);

int thProcGetPosByFramefiles (PROCESS *proc, FRAMEFILES *ff);

FRAME *thProcGetFrameByFramefiles (PROCESS *proc, FRAMEFILES *ff);

/* handlers */

int thFramefilesIn(FRAMEFILES *trgff, FRAMEFILES *srcff);

RET_CODE thFramefilesCopy(FRAMEFILES *trg, FRAMEFILES *src);

/* ID-related handlers */

FRAMEID *thFrameidGenFromHdr(HDR *hdr);
RET_CODE thUpdateFrameidFromHdr(FRAMEID *id, HDR *hdr);
RET_CODE thUpdateHdrFromFrameid(HDR *hdr, FRAMEID *id);

int thFrameidMatchHdr(FRAMEID *id, HDR *hdr);
int thFilterGetFromName(char *fname);
char *thNameGetFromFilter(int filter);

RET_CODE thFrameUpdateHdr(FRAME *f);

RET_CODE thFrameDoCrudeCalib(FRAME *f);

RET_CODE thImageFakeHdr(HDR *hdr, int run, int field, char cband, int camcol, char *rerun);

RET_CODE thCreateFakeImage(REGION *reg);
RET_CODE thFrameLoadPhSky(FRAME *f);
RET_CODE thFrameLoadCObjc(FRAME *f);
RET_CODE thFrameLoadPhprops(FRAME *f);
RET_CODE thFrameLoadGalacticCalib(FRAME *f, GC_SOURCE source);
RET_CODE thFrameWriteGalacticCalib(FRAME *f);
RET_CODE thFrameLoadData(FRAME *frame, GC_SOURCE gcsource);
RET_CODE thFramefilesPutBand(FRAMEFILES *ff, char band, RUN_MODE mode);

RET_CODE uncalibrate_frame_to_fpC_image(REGION *image, REGION *calibration, REGION *sky_image);

RET_CODE thFrameTweakFitWobjcPar(FRAME *f, int fitflag);
RET_CODE thFrameSubtractSky(FRAME *f);

RET_CODE thFrameAssessTerrainComplexity(FRAME *f, int initid, int initn, int *initk);
RET_CODE thFrameCalcChisqDispersion(FRAME *f, THPIX *iqr, THPIX *stdev, THPIX *nstep_mean, THPIX *nstep_mean_all);

#endif



