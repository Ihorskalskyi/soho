#include "thDebug.h" /* defines the debug macros */
#include "thMleTypes.h"
#include "thMap.h"
#include "thMGalaxyTypes.h"
#include "thKahan.h"

static const SCHEMA *schema_thregion_type = NULL;
static const SCHEMA *schema_fabrication_flag = NULL;
static void init_static_vars (void);
static RET_CODE check_lfitrecordtype_for_conflict(LFITRECORDTYPE rtype);
static RET_CODE get_lm_lp_nmax_from_rtype(LFITRECORDTYPE rtype, int *lm_nmax, int *lp_nmax);
static double mag_mlefl_double(MLEFL *x, int n);

void init_static_vars(void) {
	TYPE t;
	t = shTypeGetFromName("THREGION_TYPE");
	shAssert(t != UNKNOWN_SCHEMA);
	schema_thregion_type = shSchemaGetFromType(t);
	t = shTypeGetFromName("FABRICATION_FLAG");
	schema_fabrication_flag = shSchemaGetFromType(t);
	return;
}

LDATA *thLdataNew() {
LDATA *ldata;
ldata = thCalloc(1, sizeof(LDATA));
ldata->imagemode = UNKNOWN_MLEIMAGE;
return(ldata);
}

RET_CODE thLdataDel(LDATA *ldata) {
if (ldata == NULL) return(SH_SUCCESS);
#if MLE_LDATA_WEIGHT_DEEP_COPY
if (ldata->weight != NULL) {
	shRegDel(ldata->weight);
	ldata->weight = NULL;
}
#else
#endif
thFree(ldata);
return(SH_SUCCESS);
}

LMODEL *thLmodelNew() {
LMODEL *lmodel;
lmodel = thCalloc(1, sizeof(LMODEL));
return(lmodel);
}

RET_CODE thLmodelDel(LMODEL *lmodel) {
char *name = "thLmodelDel";
if (lmodel == NULL) return(SH_SUCCESS);
RET_CODE status;
status = thLmodelDealloc(lmodel);
if (status != SH_SUCCESS) {
	thError("%s: could not deallocate (lmodel) - returning with minimal change", name);
	return(status);
}
thFree(lmodel);
return(SH_SUCCESS);
}

LWORK *thLworkNew() {
LWORK *lwork;
lwork = thCalloc(1, sizeof(LWORK));
return(lwork);
}

RET_CODE thLworkDel(LWORK *lwork) {
if (lwork == NULL) return(SH_SUCCESS);
char *name = "thLworkDel";

RET_CODE status;
status = thLworkDealloc(lwork);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not deallocate (lwork), returning (lwork) with minimal change", name);
	return(status);
}

thFree(lwork);
return(SH_SUCCESS);
}


LMODVAR *thLmodvarNew() {
LMODVAR *lmodvar;
lmodvar = thCalloc(1, sizeof(LMODVAR));
return(lmodvar);
}

RET_CODE thLmodvarDel(LMODVAR *lmodvar) {
char *name = "thLmodvarDel";
if (lmodvar == NULL) return(SH_SUCCESS);

RET_CODE status;
status = thLmodvarDealloc(lmodvar);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not deallocate (lmodvar), returning (lmodvar) with minimal change", name);
	return(status);
}

thFree(lmodvar);
return(SH_SUCCESS);
}

LMETHOD *thLmethodNew() {
LMETHOD *lmethod;
lmethod = thCalloc(1, sizeof(LMETHOD));
lmethod->chisq = CHISQ_IS_BAD;
lmethod->cost = CHISQ_IS_BAD;
lmethod->delta_qn = QN_DELTA_DEFAULT;
return(lmethod);
}

RET_CODE thLmethodDel(LMETHOD *lmethod) {
if (lmethod == NULL) return(SH_SUCCESS);
thFree(lmethod);
return(SH_SUCCESS);
}

LSTRUCT *thLstructNew() {
LSTRUCT *lstruct;
lstruct = thCalloc(1, sizeof(LSTRUCT));

lstruct->ldata = thLdataNew();
lstruct->lmodel = thLmodelNew();
lstruct->lwmask = thLwmaskNew();
lstruct->lwork = thLworkNew();
lstruct->lcov = thLmodvarNew();
lstruct->lmethod = thLmethodNew();
lstruct->lfbank = NULL; /* the current version only supports one object / model bank already loaded */
lstruct->lalg = thLalgNew();
lstruct->ltest = thLtestNew();
lstruct->lcalib = thLCalibNew();
lstruct->lfit = thLfitNew();

lstruct->lprivtype = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
return(lstruct);
}

RET_CODE thLstructDel(LSTRUCT *lstruct) {

char *name = "thLstructDel";
RET_CODE status;

if (lstruct == NULL) return(SH_SUCCESS);
if (lstruct->ldata != NULL) {
	status = thLdataDel(lstruct->ldata);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not delete (ldata) in (lstruct)", name);
		return(status);
	}
	lstruct->ldata = NULL;
}
if (lstruct->lmodel != NULL) {
	status = thLmodelDel(lstruct->lmodel);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not delete (lmodel) in (lstruct)", name);
		return(status);
	}
	lstruct->lmodel = NULL;
}
if (lstruct->lwork != NULL) {
	status = thLworkDel(lstruct->lwork);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not delete (lwork) in (lstruct)", name);
		return(status);
	}
	lstruct->lwork = NULL;
}
if (lstruct->lcov != NULL) {
	status = thLmodvarDel(lstruct->lcov);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not delete (lcov) in (lstruct)", name);
		return(status);
	}
	lstruct->lcov = NULL;
}
if (lstruct->lmethod != NULL) {
	status = thLmethodDel(lstruct->lmethod);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not delete (lmethod) in (lstruct)", name);
		return(status);
	}
	lstruct->lmethod = NULL;
}
if (lstruct->lwmask != NULL) {
	thLwmaskDel(lstruct->lwmask);
	lstruct->lwmask = NULL;
}
/* LFBANK belongs to an upper structure and should not be destroyed */
if (lstruct->lfbank != NULL) lstruct->lfbank = NULL;
if (lstruct->lalg != NULL) thLalgDel(lstruct->lalg);
if (lstruct->ltest != NULL) thLtestDel(lstruct->ltest);
if (lstruct->lcalib != NULL) thLCalibDel(lstruct->lcalib);
if (lstruct->lfit != NULL) thLfitDel(lstruct->lfit);

status = thLstructDelPrivate(lstruct);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not delete (lprivate) in (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
if (lstruct->lprivtype != NULL) {
	thFree(lstruct->lprivtype);
}
thFree(lstruct);
return(SH_SUCCESS);
}

/* allocation - deallocation */

RET_CODE thLmodelAlloc(LMODEL *lmodel, int namp, int npar) {
char *name = "thLmodelAlloc";
if (lmodel == NULL) {
	thError("%s: ERROR - cannot allocate null (lmodel)", name);
	return(SH_GENERIC_ERROR);
}

if (namp < 0 || npar < 0) {
	thError("%s: ERROR - negative sizes (namp = %d) (npar = %d) provided", name, namp, npar);
	return(SH_GENERIC_ERROR);
}

int nap;
nap = namp + npar;

if (lmodel->nap < nap && lmodel->a != NULL) {
	thFree(lmodel->a);
	lmodel->a = NULL;
}
if (nap == 0 && lmodel->a != NULL) {
	thFree(lmodel->a);
	lmodel->a = NULL;
}
if (lmodel->a == NULL && nap != 0) {
	lmodel->a = thCalloc(nap, sizeof(MLEFL));
	}
if (lmodel->mcount == NULL && namp != 0) {
	lmodel->mcount = thCalloc(namp, sizeof(MLEFL));
}
if (npar == 0) {
	lmodel->p = NULL;
} else {
	lmodel->p = lmodel->a + namp;
}

lmodel->namp = namp;
lmodel->npar = npar;
lmodel->nap = nap;

return(SH_SUCCESS);
}


RET_CODE thLmodelDealloc(LMODEL *lmodel) {
char *name = "thLmodelDealloc";
if (lmodel == NULL) {
	thError("%s: WARNING - cannot deallocate null (lmodel)", name);
	return(SH_SUCCESS);
}

if (lmodel->a != NULL && lmodel->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lmodel): (a) is allocated while (nap = %d)", name, lmodel->nap);
	return(SH_GENERIC_ERROR);
}
if (lmodel->a != NULL) {
	thFree(lmodel->a);
	lmodel->a = NULL;
}
if (lmodel->mcount != NULL && lmodel->namp <= 0) {
	thError("%s: ERROR - wrongly allocated (lmodel): (mcount) is allocated while (namp = %d)", name, lmodel->namp);
	return(SH_GENERIC_ERROR);
}
if (lmodel->mcount != NULL) {
	thFree(lmodel->mcount);
	lmodel->mcount = NULL;
}

if (lmodel->p != NULL && lmodel->npar <= 0) {
	thError("%s: ERROR - wrongly allocated (lmodel): (p) is allocated while (npar = %d)", name, lmodel->npar);
	return(SH_GENERIC_ERROR);
}	
lmodel->p = NULL;

lmodel->nap = 0;
lmodel->npar = 0;
lmodel->namp = 0;

lmodel->lmachine = NULL;
lmodel->modvar = NULL;

#if THMASKisMASK
lmodel->fitregbit = 0;
#endif
#if THMASKisSUPERMASK
lmodel->fitregbit = 0;
#endif

return(SH_SUCCESS);
}


RET_CODE thLworkAlloc(LWORK *lwork, LMODEL *lmodel, LMACHINE *map, int nm, int np, int npp, int nc, int nacc, int nj, int nrow, int ncol) {
char *name = "thLworkAlloc";
if (lwork == NULL) {
	thError("%s: ERROR - null (lwork) cannot be allocated", name);
	return(SH_GENERIC_ERROR);
}
if (lmodel == NULL) {
	thError("%s: ERROR - null (lmodel)", name);
	return(SH_GENERIC_ERROR);
}
if (nm < 0) {
	thError("%s: ERROR - cannot allocate (lwork) with (nm = %d)", name, nm);
	return(SH_GENERIC_ERROR);
}
if (np < 0) {
	thError("%s: ERROR - cannot allocate (lwork) with (np = %d)", name, np);
	return(SH_GENERIC_ERROR);
}
if (npp < 0) {
	thError("%s: ERROR - cannot allocate (lwork) with (npp = %d)", name, npp);
	return(SH_GENERIC_ERROR);
}
if (nc < 0) {
	thError("%s: ERROR - cannot allocate (lwork) with (nc = %d)", name, nc);
	return(SH_GENERIC_ERROR);
}
if (nj < 0) {
	thError("%s: ERROR - cannot allocate (lwork) with (nj = %d)", name, nj);
	return(SH_GENERIC_ERROR);
}

#if DEBUG_MEMORY
printf("%s: memory statistics before allocating (lwork)", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#endif

int nDDacc = npp;

RET_CODE status;
MCFLAG cflag;
if (map != NULL) {
	status = thMapmachineGetCflag(map, &cflag);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not extract (cflag) from (lmachine)", name);
		return(status);
	}	 
	if (cflag != COMPILED) {
		thError("%s: ERROR - (lmachine) should be compiled before allocating (lwork)", name);
		return(status);
	}
}

int nap;
nap = nm + np;

if (
	(lwork->nj != 0 && lwork->nj != nj) || 
	(lwork->nc != 0 && lwork->nc != nc) || 
	(lwork->np != 0 && lwork->np != np) || 
	(lwork->nm != 0 && lwork->nm != nm) ||
	(lwork->npp != 0 && lwork->npp != npp)
	) {
	thError("%s: ERROR - (lwork) seems to be already allocated (nm, np, nc, nj) = (%d, %d, %d, %d)", 
	name, lwork->nm, lwork->np, lwork->nc, lwork->nj);
	return(SH_GENERIC_ERROR);
}

if (lwork->m != NULL && (
	lwork->m->nrow != nrow || 
	lwork->m->ncol != ncol || 
	lwork->m->type != TYPE_THPIX)) {
	shRegDel(lwork->m);
	lwork->m = NULL;
}
if (lwork->m == NULL) lwork->m = shRegNew("accumulative model image - (lwork)", nrow, ncol, TYPE_THPIX);

if (lwork->dmm != NULL && (
	lwork->dmm->nrow != nrow || 
	lwork->dmm->ncol != ncol || 
	lwork->dmm->type != TYPE_THPIX)) {
	shRegDel(lwork->dmm);
	lwork->dmm = NULL;
}

if (lwork->dmm == NULL) lwork->dmm = shRegNew("model - data - (lwork)", nrow, ncol, TYPE_THPIX);

int i, j;

WPSF *wpsf;
if (lwork->wpsf == NULL) lwork->wpsf = thWpsfNew();
wpsf = lwork->wpsf;

if (lwork->mpsf != NULL && nm != lwork->nm) {
	for (i = 0; i < nm; i++) {
		thPsfConvInfoDel(lwork->mpsf[i]);
	}
	thFree(lwork->mpsf);
	lwork->mpsf = NULL;
}

PSF_CONVOLUTION_INFO **mpsf = NULL, *psf_info = NULL;
if (lwork->mpsf == NULL && nm != 0) lwork->mpsf = thCalloc(nm, sizeof(PSF_CONVOLUTION_INFO *));			
mpsf = lwork->mpsf;
for (i = 0; i < nm; i++) {
	if (mpsf[i] == NULL) {
		psf_info = thPsfConvInfoNew(NO_PSF_CONVOLUTION, 1);
		if (psf_info == NULL) {
			thError("%s: ERROR - could not construct a (psf_info) at (i = %d)", name, i);
			return(SH_GENERIC_ERROR);
		}
		mpsf[i] = psf_info;
	} else {
		psf_info = mpsf[i];
	}
	psf_info->wpsf = wpsf;
} 

static char *regname = NULL;
if (regname == NULL) regname = thCalloc(MX_STRING_LEN, sizeof(char));
THREGION **mregs = NULL, *reg = NULL;
char *mname = NULL;


/* parameter costs and their derivatives */

if (lwork->dpcost != NULL && lwork->np != np) {
	thFree(lwork->dpcost);
	lwork->dpcost = NULL;
}
if (lwork->dpcostN != NULL && lwork->np != np) {
	thFree(lwork->dpcostN);
	lwork->dpcostN = NULL;
}
if (lwork->ddpcost != NULL && lwork->np != np) {
	if (lwork->ddpcost[0] != NULL) thFree(lwork->ddpcost[0]);
	thFree(lwork->ddpcost);
	lwork->ddpcost = NULL;
}
if (lwork->ddpcostN != NULL && lwork->np != np) {
	if (lwork->ddpcostN[0] != NULL) thFree(lwork->ddpcostN[0]);
	thFree(lwork->ddpcostN);
	lwork->ddpcostN = NULL;
}
if (lwork->dpcost == NULL && np != 0) lwork->dpcost = thCalloc(np, sizeof(CHISQFL));
if (lwork->dpcostN == NULL && np != 0) lwork->dpcostN = thCalloc(np, sizeof(CHISQFL));
if (lwork->ddpcost == NULL && np != 0) {
	lwork->ddpcost = thCalloc(np, sizeof(CHISQFL *));
	lwork->ddpcost[0] = thCalloc(np * np, sizeof(CHISQFL));
	for (i = 0; i < np; i++) lwork->ddpcost[i] = lwork->ddpcost[0] + i * np;
}
if (lwork->ddpcostN == NULL && np != 0) {
	lwork->ddpcostN = thCalloc(np, sizeof(CHISQFL *));
	lwork->ddpcostN[0] = thCalloc(np * np, sizeof(CHISQFL));
	for (i = 0; i < np; i++) lwork->ddpcostN[i] = lwork->ddpcostN[0] + i * np;
}

if (lwork->da != NULL && lwork->nap != nap) {
	thFree(lwork->da);
	lwork->da = NULL;
}
lwork->dp = NULL;
if (lwork->daT != NULL && lwork->nap != nap) {
	thFree(lwork->daT);
	lwork->daT = NULL;
}
lwork->dpT = NULL;
if (lwork->db != NULL && lwork->nap != nap) {
	thFree(lwork->db);
	lwork->db = NULL;
}
lwork->db = NULL;
if (lwork->aN != NULL && lwork->nap != nap) {
	thFree(lwork->aN);
	lwork->aN = NULL;
}
lwork->pN = NULL;
if (lwork->mcountN != NULL && lwork->nm != nm) {
	thFree(lwork->mcountN);
	lwork->mcountN = NULL;
}

if (lwork->aold != NULL && lwork->nap != nap) {
	thFree(lwork->aold);
	lwork->aold = NULL;
}
lwork->pold = NULL;

if (lwork->aT != NULL && lwork->nap != nap) {
	thFree(lwork->aT);
	lwork->aT = NULL;
}
lwork->pT = NULL;

if (lwork->mcountN != NULL && lwork->nm != nm) {
	thFree(lwork->mcountN);
	lwork->mcountN = NULL;
}

if (lwork->mcountold != NULL && lwork->nm != nm) {
	thFree(lwork->mcountold);
	lwork->mcountold = NULL;
}

if (lwork->mcountT != NULL && lwork->nm != nm) {
	thFree(lwork->mcountT);
	lwork->mcountT = NULL;
}

if (lwork->covapT != NULL) {
	if (lwork->covapT[0] != NULL) {
		thFree(lwork->covapT[0]);
	}
	thFree(lwork->covapT);
	lwork->covapT = NULL;
}
if (lwork->covaT != NULL) {
	thFree(lwork->covaT);
	lwork->covaT = NULL;
}
if (lwork->covpT != NULL) {
	thFree(lwork->covpT);
	lwork->covpT = NULL;
}
/* allocating da, dp */
if (lwork->da == NULL && nap != 0) lwork->da = thCalloc(nap, sizeof(MLEFL));
if (np != 0) {
	lwork->dp = lwork->da + nm;
} else {
	lwork->dp = NULL;
}
if (lwork->daT == NULL && nap != 0) lwork->daT = thCalloc(nap, sizeof(MLEFL));
if (np != 0) {
	lwork->dpT = lwork->daT + nm;
} else {
	lwork->dpT = NULL;
}
if (lwork->db == NULL && nap != 0) lwork->db = thCalloc(nap, sizeof(MLEFL));
if (np != 0) {
	lwork->dq = lwork->db + nm;
} else {
	lwork->dq = NULL;
}


/* allocating aN, pN */
if (lwork->aN == NULL && nap != 0) lwork->aN = thCalloc(nap, sizeof(MLEFL));
if (np != 0) {
	lwork->pN = lwork->aN + nm;
} else {
	lwork->pN = NULL;
}
if (lwork->aold == NULL && nap != 0) lwork->aold = thCalloc(nap, sizeof(MLEFL));
if (np != 0) {
	lwork->pold = lwork->aold + nm;
} else {
	lwork->pold = NULL;
}

if (lwork->aT == NULL && nap != 0) lwork->aT = thCalloc(nap, sizeof(MLEFL));
if (np != 0) {
	lwork->pT = lwork->aT + nm;
} else {
	lwork->pT = NULL;
}

if (lwork->covapT == NULL && nap != 0) {
	lwork->covapT = shCalloc(nap, sizeof(MLEFL *));
	lwork->covapT[0] = shCalloc(nap * nap, sizeof(MLEFL));
	for (i = 0; i < nap; i++) {
		lwork->covapT[i] = lwork->covapT[0] + i * nap;
	}
	if (lwork->covpT != NULL) {
		thError("%s: ERROR - expected null (covpT) array in (lwork)", name);
		return(SH_GENERIC_ERROR);
	}
	lwork->covpT = shCalloc(np, sizeof(MLEFL *));
	for (i = 0; i < np; i++) {
		lwork->covpT[i] = lwork->covapT[i + nm] + nm;
	}
	if (lwork->covaT != NULL) {
		thError("%s: ERROR - expected null (covaT) array in (lwork)", name);
		return(SH_GENERIC_ERROR);
	}
	lwork->covaT = shCalloc(nm, sizeof(MLEFL *));
	for (i = 0; i < nm; i++) {
		lwork->covaT[i] = lwork->covapT[i];
	}
}
/* allocating mcountN */
if (lwork->mcountN == NULL && nm != 0) lwork->mcountN = thCalloc(nm, sizeof(MLEFL));
if (lwork->mcountold == NULL && nm != 0) lwork->mcountold = thCalloc(nm, sizeof(MLEFL));

if (lwork->mN != NULL && (
	lwork->mN->nrow != nrow || 
	lwork->mN->ncol != ncol || 
	lwork->mN->type != TYPE_THPIX)) {
	shRegDel(lwork->mN);
	lwork->mN = NULL;
}
if (lwork->mN == NULL) lwork->mN = shRegNew("accumulated model for new parameter - (lwork)", nrow, ncol, TYPE_THPIX);


if (lwork->dmmN != NULL && (
	lwork->dmmN->nrow != nrow || 
	lwork->dmmN->ncol != ncol || 
	lwork->dmmN->type != TYPE_THPIX)) {
	shRegDel(lwork->dmmN);
	lwork->dmmN = NULL;
}
if (lwork->dmmN == NULL) lwork->dmmN = shRegNew("model - data for new parameter -  (lwork)", nrow, ncol, TYPE_THPIX);

if (lwork->mregN == NULL && nc != 0) lwork->mregN = thCalloc(nc, sizeof(THREGION *));
mregs = lwork->mregN;

#if MLE_CONDUCT_HIGHER_ORDER
THREGION **DDmregs = NULL; 
if (lwork->DDmregN == NULL && npp != 0) lwork->DDmregN = thCalloc(npp, sizeof(THREGION *));
DDmregs = lwork->DDmregN;
#endif

mpsf = lwork->mpsf;
if (map == NULL) {
	strcpy(regname, "model or derivative image - pN");
	for (i = 0; i < nc; i++) {
		reg = mregs[i];
		if (reg == NULL) {
			/* 	
			reg = shRegNew(regname, 0, 0, TYPE_THPIX);
			*/
			reg = thRegNewFromModelName(mname, NULL);
			if (reg == NULL) {
				thError("%s: ERROR - could not generate (mregN) for (ic = %d)", name, i);
				return(SH_GENERIC_ERROR);
			}
			mregs[i] = reg;
		} else {
			status = thRegRenewFromModelName(reg, mname, NULL);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not renew (thregion) for model '%s'", name, mname);
				return(status);
			}
		}
	}
} else {
	int *nmpar;
	nmpar = map->nmpar;
	char *mname, *rname, **rnames, *objctype;
	THOBJC *objc;
	THOBJCID objcid;
	RET_CODE status;
	for (i = 0; i < nm; i++) {
		mname = map->mnames[i];
		rnames = map->enames[i];
		objc = (THOBJC *) (map->objcs[i]);
		objcid = objc->thid;
		status = thObjcNameGetFromType(objc->thobjctype, &objctype);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - unrecognized type for (objc: %d)", name, objcid);
			return(status);
		}
		sprintf(regname, "model image for '%s' (objc: %d) of class '%s' - pN", 
				mname, objcid, objctype);
		psf_info = lwork->mpsf[i];
		if (psf_info == NULL) {
			thError("%s: ERROR - expected a non-null (psf_info) in (mpsf) for model (%d) '%s'", name, i, mname);
			return(SH_GENERIC_ERROR);
		} 
		reg = *mregs;
		if (reg == NULL) {
			reg = thRegNewFromModelName(mname, psf_info);
			if (reg == NULL) {
				thError("%s: ERROR - could not construct (mregN) for model (%d) '%s' w.r.t. '%s'", name, i, mname);
				return(SH_GENERIC_ERROR);
			}
			*mregs = reg;
		} else {
			status = thRegRenewFromModelName(reg, mname, psf_info);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not renew (thregion) for model '%s'", name, mname);
				return(status);
			}
		}
		for (j = 0; j < nmpar[i]; j++) {
			rname = rnames[j];
			mregs++;
			sprintf(regname, 
			"derivate image for '%s' w.r.t. '%s' (objc: %d) of class '%s' - pN", 
			mname, rname, objcid, objctype);
			reg = *mregs;
			if (reg == NULL) {
				/* 
				reg = shRegNew(regname, 0, 0, TYPE_THPIX);
				*/
				reg = thRegNewFromModelName(mname, psf_info);
				if (reg == NULL) {
					thError("%s: ERROR - could not construct (mregN) for derivative of model (%d) '%s' w.r.t. '%s'", name, i, mname, rname);
					return(SH_GENERIC_ERROR);
				}
				*mregs = reg;
			} else {
				status = thRegRenewFromModelName(reg, mname, psf_info);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not renew (thregion) for derivative of model '%s' w.r.t. '%s'", name, mname, rname);
					return(status);
				}
			}	 
		}	
	mregs++;	
	}
}

#if MLE_CONDUCT_HIGHER_ORDER

if (map == NULL) {
	thError("%s: ERROR - does not supprot (map = null) when higher order MLE is on", name);
	return(SH_GENERIC_ERROR);
}

for (i = 0; i < nm; i++) {
	int k, *nmpar;
	nmpar = map->nmpar;
	char *mname = map->mnames[i];
	char **rnames = map->enames[i];
	THOBJC *objc = (THOBJC *) (map->objcs[i]);
	THOBJCID objcid = objc->thid;

	char *objctype = NULL;
	RET_CODE status = thObjcNameGetFromType(objc->thobjctype, &objctype);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - unrecognized type for (objc: %d)", name, objcid);
		return(status);
	}
	psf_info = lwork->mpsf[i];
	if (psf_info == NULL) {
		thError("%s: ERROR - expected a non-null (psf_info) in (mpsf) for model (%d) '%s'", name, i, mname);
		return(SH_GENERIC_ERROR);
	} 
	for (j = 0; j < nmpar[i]; j++) {
	for (k = 0; k <= j; k++) {
		if (DDmregs == NULL) {
			thError("%s: ERROR - null (DDmreg) array found in (map)", name);
			return(SH_GENERIC_ERROR);
		}
		char *rname1 = rnames[j];
		char *rname2 = rnames[k];
		sprintf(regname, "deriv img of '%s' wrt. '%s', '%s' (objc: %d) of class '%s'", 
			mname, rname1, rname2, objcid, objctype);
		reg = *DDmregs;
		if (reg == NULL) {
			reg = thRegNewFromModelName(mname, psf_info);
			if (reg == NULL) {
				thError("%s: ERROR - could not construct (DDmreg) for deriv of model (%d) '%s' wrt. '%s', '%s'", name, i, mname, rname1, rname2);
					return(SH_GENERIC_ERROR);
			}
			*DDmregs = reg;
		} else {
			status = thRegRenewFromModelName(reg, mname, psf_info);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not renew (thregion) for deriv of model '%s' wrt. '%s', '%s'", name, mname, rname1, rname2);
				return(status);
			}
		}	 
	DDmregs++;
	}
	}	
}

#endif

/* care should be paid here since this matrix is a sub-matrix of one which also includes amplitudes */

if (nap != lwork->nap && lwork->jtjap != NULL) {
	if (lwork->nap != 0 && lwork->jtjap != NULL && lwork->jtjap[0] != NULL) thFree(lwork->jtjap[0]);
	if (lwork->nap != 0) thFree(lwork->jtjap);
	lwork->jtjap = NULL;
}

if (lwork->jtjap == NULL && nap != 0) {
	lwork->jtjap = thCalloc(nap, sizeof(MLEFL *));
	lwork->jtjap[0] = thCalloc(nap * nap, sizeof(MLEFL));
}
for (i = 0; i < nap; i++) if (lwork->jtjap[i] == NULL) lwork->jtjap[i] = lwork->jtjap[0] + i * nap;

if (np != lwork->np && lwork->jtj != NULL) {
	if (lwork->np != 0) thFree(lwork->jtj);
	lwork->jtj = NULL;
}

if (lwork->jtj == NULL && np != 0) lwork->jtj = thCalloc(np, sizeof(MLEFL *));
for (i = 0; i < np; i++) lwork->jtj[i] = lwork->jtjap[i + nm] + nm;

if (nap != lwork->nap && lwork->wjtjap != NULL) {
	if (lwork->nap != 0 && lwork->wjtjap != NULL && lwork->wjtjap[0] != NULL) thFree(lwork->wjtjap[0]);
	if (lwork->nap != 0) thFree(lwork->wjtjap);
	lwork->wjtjap = NULL;
}

if (lwork->wjtjap == NULL && nap != 0) {
	lwork->wjtjap = thCalloc(nap, sizeof(MLEFL *));
	lwork->wjtjap[0] = thCalloc(nap * nap, sizeof(MLEFL));
}
for (i = 0; i < nap; i++) if (lwork->wjtjap[i] == NULL) lwork->wjtjap[i] = lwork->wjtjap[0] + i * nap;

if (np != lwork->np && lwork->wjtj != NULL) {
	if (lwork->np != 0) thFree(lwork->wjtj);
	lwork->wjtj = NULL;
}

if (lwork->wjtj == NULL && np != 0) lwork->wjtj = thCalloc(np, sizeof(MLEFL *));
for (i = 0; i < np; i++) lwork->wjtj[i] = lwork->wjtjap[i + nm] + nm;


#if QUASI_NEWTON

if (lwork->Bqnap == NULL && nap != 0) {
	lwork->Bqnap = thCalloc(nap, sizeof(MLEFL *));
	lwork->Bqnap[0] = thCalloc(nap * nap, sizeof(MLEFL));
}
for (i = 0; i < nap; i++) if (lwork->Bqnap[i] == NULL) lwork->Bqnap[i] = lwork->Bqnap[0] + i * nap;

if (lwork->BBqnap == NULL && nap != 0) {
	lwork->BBqnap = thCalloc(nap, sizeof(MLEFL *));
	lwork->BBqnap[0] = thCalloc(nap * nap, sizeof(MLEFL));
}
for (i = 0; i < nap; i++) if (lwork->BBqnap[i] == NULL) lwork->BBqnap[i] = lwork->BBqnap[0] + i * nap;

/* initiating value to identity matrix */

for (i = 0; i < nap; i++) memset(lwork->Bqnap[i], '\0', nap * sizeof(MLEFL));
for (i = 0; i < nap; i++) {
		lwork->Bqnap[i][i] = (MLEFL) 1.0;
}

for (i = 0; i < nap; i++) memset(lwork->BBqnap[i], '\0', nap * sizeof(MLEFL));
for (i = 0; i < nap; i++) {
		lwork->BBqnap[i][i] = (MLEFL) 1.0;
}


if (np != lwork->np && lwork->Bqn != NULL) {
	if (lwork->np != 0) thFree(lwork->Bqn);
	lwork->Bqn = NULL;
}

if (np != lwork->np && lwork->BBqn != NULL) {
	if (lwork->np != 0) thFree(lwork->BBqn);
	lwork->BBqn = NULL;
}


/* Quasi-Newton's B matrix (inverse of Hessian) */
if (lwork->Bqn == NULL && np != 0) lwork->Bqn = thCalloc(np, sizeof(MLEFL *));
for (i = 0; i < np; i++) lwork->Bqn[i] = lwork->Bqnap[i + nm] + nm;

if (lwork->BBqn == NULL && np != 0) lwork->BBqn = thCalloc(np, sizeof(MLEFL *));
for (i = 0; i < np; i++) lwork->BBqn[i] = lwork->BBqnap[i + nm] + nm;

if (lwork->Bqnap_prev == NULL && nap != 0) {
	lwork->Bqnap_prev = thCalloc(nap, sizeof(MLEFL *));
	lwork->Bqnap_prev[0] = thCalloc(nap * nap, sizeof(MLEFL));
}
for (i = 0; i < nap; i++) if (lwork->Bqnap_prev[i] == NULL) lwork->Bqnap_prev[i] = lwork->Bqnap_prev[0] + i * nap;

/* initiating value to identity matrix */
for (i = 0; i < nap; i++) memset(lwork->Bqnap_prev[i], '\0', nap * sizeof(MLEFL));
for (i = 0; i < nap; i++) {
		lwork->Bqnap_prev[i][i] = (MLEFL) 1.0;
}

if (np != lwork->np && lwork->Bqn_prev != NULL) {
	if (lwork->np != 0) thFree(lwork->Bqn_prev);
	lwork->Bqn_prev = NULL;
}

if (lwork->Bqn_prev == NULL && np != 0) lwork->Bqn_prev = thCalloc(np, sizeof(MLEFL *));
for (i = 0; i < np; i++) lwork->Bqn_prev[i] = lwork->Bqnap_prev[i + nm] + nm;

if (lwork->BBqnap_prev == NULL && nap != 0) {
	lwork->BBqnap_prev = thCalloc(nap, sizeof(MLEFL *));
	lwork->BBqnap_prev[0] = thCalloc(nap * nap, sizeof(MLEFL));
}
for (i = 0; i < nap; i++) if (lwork->BBqnap_prev[i] == NULL) lwork->BBqnap_prev[i] = lwork->BBqnap_prev[0] + i * nap;

/* initiating value to identity matrix */
for (i = 0; i < nap; i++) memset(lwork->BBqnap_prev[i], '\0', nap * sizeof(MLEFL));
for (i = 0; i < nap; i++) {
		lwork->BBqnap_prev[i][i] = (MLEFL) 1.0;
}

if (np != lwork->np && lwork->BBqn_prev != NULL) {
	if (lwork->np != 0) thFree(lwork->BBqn_prev);
	lwork->BBqn_prev = NULL;
}

if (lwork->BBqn_prev == NULL && np != 0) lwork->BBqn_prev = thCalloc(np, sizeof(MLEFL *));
for (i = 0; i < np; i++) lwork->BBqn_prev[i] = lwork->BBqnap_prev[i + nm] + nm;

if (lwork->Hqnap == NULL && nap != 0) {
	lwork->Hqnap = thCalloc(nap, sizeof(MLEFL *));
	lwork->Hqnap[0] = thCalloc(nap * nap, sizeof(MLEFL));
}
for (i = 0; i < nap; i++) if (lwork->Hqnap[i] == NULL) lwork->Hqnap[i] = lwork->Hqnap[0] + i * nap;

/* initiating value to identity matrix */
for (i = 0; i < nap; i++) memset(lwork->Hqnap[i], '\0', nap * sizeof(MLEFL));
for (i = 0; i < nap; i++) {
		lwork->Hqnap[i][i] = (MLEFL) 1.0;
}

if (np != lwork->np && lwork->Hqn != NULL) {
	if (lwork->np != 0) thFree(lwork->Hqn);
	lwork->Hqn = NULL;
}

/* Quasi-Newton's H matrix (Hessian) */
if (lwork->Hqn == NULL && np != 0) lwork->Hqn = thCalloc(np, sizeof(MLEFL *));
for (i = 0; i < np; i++) lwork->Hqn[i] = lwork->Hqnap[i + nm] + nm;

if (lwork->Hqnap_prev == NULL && nap != 0) {
	lwork->Hqnap_prev = thCalloc(nap, sizeof(MLEFL *));
	lwork->Hqnap_prev[0] = thCalloc(nap * nap, sizeof(MLEFL));
}
for (i = 0; i < nap; i++) if (lwork->Hqnap_prev[i] == NULL) lwork->Hqnap_prev[i] = lwork->Hqnap_prev[0] + i * nap;

/* initiating value to identity matrix */
for (i = 0; i < nap; i++) memset(lwork->Hqnap_prev[i], '\0', nap * sizeof(MLEFL));
for (i = 0; i < nap; i++) {
		lwork->Hqnap_prev[i][i] = (MLEFL) 1.0;
}

if (np != lwork->np && lwork->Hqn_prev != NULL) {
	if (lwork->np != 0) thFree(lwork->Hqn_prev);
	lwork->Hqn_prev = NULL;
}

if (lwork->Hqn_prev == NULL && np != 0) lwork->Hqn_prev = thCalloc(np, sizeof(MLEFL *));
for (i = 0; i < np; i++) lwork->Hqn_prev[i] = lwork->Hqnap_prev[i + nm] + nm;

if (lwork->HHqnap == NULL && nap != 0) {
	lwork->HHqnap = thCalloc(nap, sizeof(MLEFL *));
	lwork->HHqnap[0] = thCalloc(nap * nap, sizeof(MLEFL));
}
for (i = 0; i < nap; i++) if (lwork->HHqnap[i] == NULL) lwork->HHqnap[i] = lwork->HHqnap[0] + i * nap;

/* initiating value to identity matrix */
for (i = 0; i < nap; i++) memset(lwork->HHqnap[i], '\0', nap * sizeof(MLEFL));
for (i = 0; i < nap; i++) {
		lwork->HHqnap[i][i] = (MLEFL) 1.0;
}

if (np != lwork->np && lwork->HHqn != NULL) {
	if (lwork->np != 0) thFree(lwork->HHqn);
	lwork->HHqn = NULL;
}

/* Quasi-Newton's H matrix (Hessian) */
if (lwork->HHqn == NULL && np != 0) lwork->HHqn = thCalloc(np, sizeof(MLEFL *));
for (i = 0; i < np; i++) lwork->HHqn[i] = lwork->HHqnap[i + nm] + nm;

if (lwork->HHqnap_prev == NULL && nap != 0) {
	lwork->HHqnap_prev = thCalloc(nap, sizeof(MLEFL *));
	lwork->HHqnap_prev[0] = thCalloc(nap * nap, sizeof(MLEFL));
}
for (i = 0; i < nap; i++) if (lwork->HHqnap_prev[i] == NULL) lwork->HHqnap_prev[i] = lwork->HHqnap_prev[0] + i * nap;

/* initiating value to identity matrix */
for (i = 0; i < nap; i++) memset(lwork->HHqnap_prev[i], '\0', nap * sizeof(MLEFL));
for (i = 0; i < nap; i++) {
		lwork->HHqnap_prev[i][i] = (MLEFL) 1.0;
}

if (np != lwork->np && lwork->HHqn_prev != NULL) {
	if (lwork->np != 0) thFree(lwork->HHqn_prev);
	lwork->HHqn_prev = NULL;
}

if (lwork->HHqn_prev == NULL && np != 0) lwork->HHqn_prev = thCalloc(np, sizeof(MLEFL *));
for (i = 0; i < np; i++) lwork->HHqn_prev[i] = lwork->HHqnap_prev[i + nm] + nm;

#endif

#if (ADADELTA | 1)

if (nap != lwork->nap && lwork->E_ggap != NULL) {
	if (lwork->nap != 0 && lwork->E_ggap != NULL) thFree(lwork->E_ggap);
	lwork->E_ggap = NULL;
}

if (lwork->E_ggap == NULL && nap != 0) {
	lwork->E_ggap = thCalloc(nap, sizeof(MLEFL));
}

if (np != 0) {
	lwork->E_gg = lwork->E_ggap + nm * sizeof(MLEFL);
} else {
	lwork->E_gg = NULL;
}

/* 
if (np != lwork->np && lwork->E_gg != NULL) {
	if (lwork->np != 0) thFree(lwork->E_gg);
	lwork->E_gg = NULL;
}

if (lwork->E_gg == NULL && np != 0) lwork->E_gg = thCalloc(np, sizeof(MLEFL));
*/

if (nap != lwork->nap && lwork->E_ggap_prev != NULL) {
	if (lwork->nap != 0 && lwork->E_ggap_prev != NULL) thFree(lwork->E_ggap_prev);
	lwork->E_ggap_prev = NULL;
}

if (lwork->E_ggap_prev == NULL && nap != 0) {
	lwork->E_ggap_prev = thCalloc(nap, sizeof(MLEFL));
}

if (np != 0) {
	lwork->E_gg_prev = lwork->E_ggap_prev + nm * sizeof(MLEFL);
} else {
	lwork->E_gg_prev = NULL;
}

/* 
if (np != lwork->np && lwork->E_gg_prev != NULL) {
	if (lwork->np != 0) thFree(lwork->E_gg_prev);
	lwork->E_gg_prev = NULL;
}
*/

if (nap != lwork->nap && lwork->rms_gap_prev != NULL) {
	if (lwork->nap != 0 && lwork->rms_gap_prev != NULL) thFree(lwork->rms_gap_prev);
	lwork->rms_gap_prev = NULL;
}

if (lwork->rms_gap_prev == NULL && nap != 0) {
	lwork->rms_gap_prev = thCalloc(nap, sizeof(MLEFL));
}

if (np != 0) {
	lwork->rms_g_prev = lwork->rms_gap_prev + nm * sizeof(MLEFL);
} else {
	lwork->rms_g_prev = NULL;
}

if (nap != lwork->nap && lwork->rms_gap != NULL) {
	if (lwork->nap != 0 && lwork->rms_gap != NULL) thFree(lwork->rms_gap);
	lwork->rms_gap = NULL;
}

if (lwork->rms_gap == NULL && nap != 0) {
	lwork->rms_gap = thCalloc(nap, sizeof(MLEFL));
}

if (np != 0) {
	lwork->rms_g = lwork->rms_gap + nm * sizeof(MLEFL);
} else {
	lwork->rms_g = NULL;
}

if (nap != lwork->nap && lwork->rms_ginvap_prev != NULL) {
	if (lwork->nap != 0 && lwork->rms_ginvap_prev != NULL) thFree(lwork->rms_ginvap_prev);
	lwork->rms_ginvap_prev = NULL;
}

if (lwork->rms_ginvap_prev == NULL && nap != 0) {
	lwork->rms_ginvap_prev = thCalloc(nap, sizeof(MLEFL));
}

if (np != 0) {
	lwork->rms_ginv_prev = lwork->rms_ginvap_prev + nm * sizeof(MLEFL);
} else {
	lwork->rms_ginv_prev = NULL;
}

if (nap != lwork->nap && lwork->rms_ginvap != NULL) {
	if (lwork->nap != 0 && lwork->rms_ginvap != NULL) thFree(lwork->rms_ginvap);
	lwork->rms_ginvap = NULL;
}

if (lwork->rms_ginvap == NULL && nap != 0) {
	lwork->rms_ginvap = thCalloc(nap, sizeof(MLEFL));
}

if (np != 0) {
	lwork->rms_ginv = lwork->rms_ginvap + nm * sizeof(MLEFL);
} else {
	lwork->rms_ginv = NULL;
}

if (nap != lwork->nap && lwork->E_dxdxap != NULL) {
	if (lwork->nap != 0 && lwork->E_dxdxap != NULL) thFree(lwork->E_dxdxap);
	lwork->E_dxdxap = NULL;
}

if (lwork->E_dxdxap == NULL && nap != 0) {
	lwork->E_dxdxap = thCalloc(nap, sizeof(MLEFL));
}

if (np != 0) {
	lwork->E_dxdx = lwork->E_dxdxap + nm * sizeof(MLEFL);
} else {
	lwork->E_dxdx = NULL;
}
/* 
if (np != lwork->np && lwork->E_dxdx != NULL) {
	if (lwork->np != 0) thFree(lwork->E_dxdx);
	lwork->E_dxdx = NULL;
}

if (lwork->E_dxdx == NULL && np != 0) lwork->E_dxdx = thCalloc(np, sizeof(MLEFL));
*/

if (nap != lwork->nap && lwork->E_dxdxap_prev != NULL) {
	if (lwork->nap != 0 && lwork->E_dxdxap_prev != NULL) thFree(lwork->E_dxdxap_prev);
	lwork->E_dxdxap_prev = NULL;
}

if (lwork->E_dxdxap_prev == NULL && nap != 0) {
	lwork->E_dxdxap_prev = thCalloc(nap, sizeof(MLEFL));
}

/* 
if (np != lwork->np && lwork->E_dxdx_prev != NULL) {
	if (lwork->np != 0) thFree(lwork->E_dxdx_prev);
	lwork->E_dxdx_prev = NULL;
}

if (lwork->E_dxdx_prev == NULL && np != 0) {
	lwork->E_dxdx_prev = thCalloc(np, sizeof(MLEFL));
}
*/

if (np != 0) {
	lwork->E_dxdx_prev = lwork->E_dxdxap_prev + nm * sizeof(MLEFL);
} else {
	lwork->E_dxdx_prev = NULL;
}

if (nap != lwork->nap && lwork->rms_dxap_prev != NULL) {
	if (lwork->nap != 0 && lwork->rms_dxap_prev != NULL) thFree(lwork->rms_dxap_prev);
	lwork->rms_dxap_prev = NULL;
}

if (lwork->rms_dxap_prev == NULL && nap != 0) {
	lwork->rms_dxap_prev = thCalloc(nap, sizeof(MLEFL));
}

/* 
if (np != lwork->np && lwork->rms_dx_prev != NULL) {
	if (lwork->np != 0) thFree(lwork->rms_dx_prev);
	lwork->rms_dx_prev = NULL;
}

if (lwork->rms_dx_prev == NULL && np != 0) lwork->rms_dx_prev = thCalloc(np, sizeof(MLEFL));
*/

if (np != 0) {
	lwork->rms_dx_prev = lwork->rms_dxap + nm * sizeof(MLEFL);
} else {
	lwork->rms_dx_prev = NULL;
}

if (nap != lwork->nap && lwork->rms_dxap != NULL) {
	if (lwork->nap != 0 && lwork->rms_dxap != NULL) thFree(lwork->rms_dxap);
	lwork->rms_dxap = NULL;
}

if (lwork->rms_dxap == NULL && nap != 0) {
	lwork->rms_dxap = thCalloc(nap, sizeof(MLEFL));
}

/* 
if (np != lwork->np && lwork->rms_dx != NULL) {
	if (lwork->np != 0) thFree(lwork->rms_dx);
	lwork->rms_dx = NULL;
}

if (lwork->rms_dx == NULL && np != 0) lwork->rms_dx = thCalloc(np, sizeof(MLEFL));
*/

if (np != 0) {
	lwork->rms_dx = lwork->rms_dxap + nm * sizeof(MLEFL);
} else {
	lwork->rms_dx = NULL;
}
#endif

/* allocating Jt dmm */

if (nap != lwork->nap && lwork->jtdmmap != NULL) {
	if (lwork->nap != 0) thFree(lwork->jtdmmap);
	lwork->jtdmmap = NULL;
}
if (lwork->jtdmmap == NULL) lwork->jtdmmap = thCalloc(nap, sizeof(MLEFL));
lwork->jtdmm = lwork->jtdmmap + nm;

#if QUASI_NEWTON
if (nap != lwork->nap && lwork->jtdmmap_prev != NULL) {
	if (lwork->nap != 0) thFree(lwork->jtdmmap_prev);
	lwork->jtdmmap_prev = NULL;
}
if (lwork->jtdmmap_prev == NULL) lwork->jtdmmap_prev = thCalloc(nap, sizeof(MLEFL));
lwork->jtdmm_prev = lwork->jtdmmap_prev + nm;

#if 0
if (nap != lwork->nap && lwork->dxap != NULL) {
	if (lwork->nap != 0) thFree(lwork->dxap);
	lwork->dxap = NULL;
}
if (lwork->dxap == NULL) lwork->dxap = thCalloc(nap, sizeof(MLEFL));
lwork->dx = lwork->dxap + nm;
#endif

#endif

/* allocating Jt ddm */

if (nap != lwork->nap && lwork->jtddmap != NULL) {
	if (lwork->nap != 0) thFree(lwork->jtddmap);
	lwork->jtddmap = NULL;
}
if (lwork->jtddmap == NULL) lwork->jtddmap = thCalloc(nap, sizeof(MLEFL));
lwork->jtddm = lwork->jtddmap + nm;


/* allocating jtj + lambda diag (jtj) */

if (nap != lwork->nap && lwork->jtjpdiagap != NULL) {
	if (lwork->nap != 0 && lwork->jtjpdiagap[0] != NULL) thFree(lwork->jtjpdiagap[0]);	
	if (lwork->nap != 0) thFree(lwork->jtjpdiagap);
	lwork->jtjpdiagap = NULL;
}

/* note that jtjdiag matrices have different data type than MLEFL. 
	they are work spaces used to pass to thMatrixInvert */
#if 0
if (lwork->jtjpdiagap == NULL && nap != 0) lwork->jtjpdiagap = thCalloc(nap, sizeof(MLEFL *));
for (i = 0; i < nap; i++) {
	if (lwork->jtjpdiagap[i] == NULL) lwork->jtjpdiagap[i] = thCalloc(nap, sizeof(MLEFL));
	}

#else

if (lwork->jtjpdiagap == NULL && nap != 0) {
	lwork->jtjpdiagap = thCalloc(nap, sizeof(MLEFL *));
	lwork->jtjpdiagap[0] = thCalloc(nap * nap, sizeof(MLEFL));
	for (i = 0; i < nap; i++) {
		if (lwork->jtjpdiagap[i] == NULL) lwork->jtjpdiagap[i] = lwork->jtjpdiagap[0] + i * nap;
	}
}

#endif
if (np != lwork->np && lwork->jtjpdiag != NULL) {
	if (lwork->np != 0) thFree(lwork->jtjpdiag);
	lwork->jtjpdiag = NULL;
}

if (lwork->jtjpdiag == NULL && np != 0) lwork->jtjpdiag = thCalloc(np, sizeof(MLEFL *));
for (i = 0; i < np; i++) {
	lwork->jtjpdiag[i] = lwork->jtjpdiagap[i + nm] + nm;
}

/* allocating Djtj */

if (nap != lwork->nap && lwork->Djtjap != NULL) {
	if (lwork->nap != 0) thFree(lwork->Djtjap);
	lwork->Djtjap = NULL;
}
if (lwork->Djtjap == NULL) {
	lwork->Djtjap = thCalloc(nap, sizeof(MLEFL));
}

lwork->Djtj = lwork->Djtjap + nm;

/* allocating m-brackets */

if (lwork->mid != NULL && nm != lwork->nm) {
	if (lwork->nm != 0) thFree(lwork->mid);
	lwork->mid = NULL;
}
if (lwork->mid == NULL && nm != 0) lwork->mid = thCalloc(nm, sizeof(MLEFL));

if (lwork->midmm != NULL && nm != lwork->nm) {
	if (lwork->nm != 0) thFree(lwork->midmm);
	lwork->midmm = NULL;
}
if (lwork->midmm == NULL && nm != 0) lwork->midmm = thCalloc(nm, sizeof(MLEFL));

if (lwork->mimj != NULL && nm != lwork->nm) {
	if (lwork->mimj[0] != NULL) thFree(lwork->mimj[0]);
	thFree(lwork->mimj);
	lwork->mimj = NULL;
}
if (lwork->mimj == NULL && nm != 0) lwork->mimj = thCalloc(nm, sizeof(MLEFL *));
if (lwork->mimj[0] == NULL) lwork->mimj[0] = thCalloc(nm * nm, sizeof(MLEFL));
for (i = 0; i < nm; i++) {
	lwork->mimj[i] = lwork->mimj[0] + i * nm;
}

#if OLD_MAP

int k;
if (lwork->mimjk != NULL && nm != lwork->nm) {
	for (i = 0; i < lwork->nm; i++) {
		if (lwork->mimjk[i] != NULL) {
			for (j = 0; j < lwork->nm; j++) {
				if (lwork->mimjk[i][j] != NULL && lwork->np != 0) thFree(lwork->mimjk[i][j]);
			}
			thFree(lwork->mimjk[i]);
		}
	}
	if (lwork->nm != 0) thFree(lwork->mimjk);
	lwork->mimjk = NULL;
}
if (lwork->mimjk == NULL && nm != 0) lwork->mimjk = thCalloc(nm, sizeof(MLEFL **));
for (i = 0; i < nm; i++) {
	if (lwork->nm != nm && lwork->mimjk[i] != NULL) {
		for (j = 0; j < lwork->nm; j++) {
			if (lwork->mimjk[i][j] != NULL) thFree(lwork->mimjk[i][j]);
		}
		thFree(lwork->mimjk[i]);
		lwork->mimjk[i] = NULL;
	}
	if (lwork->mimjk[i] == NULL) {
		lwork->mimjk[i] = thCalloc(nm, sizeof(MLEFL *));
		for (j = 0; j < nm; j++) {
			lwork->mimjk[i][j] = thCalloc(np, sizeof(MLEFL));
		}
	} else if (np != lwork->np) {
		for (j = 0; j < nm; j++) {
			if (lwork->mimjk[i][j] != NULL) thFree(lwork->mimjk[i][j]);
			lwork->mimjk[i][j] = thCalloc(np, sizeof(MLEFL));
		}
	}
}
if (lwork->mijmkl != NULL && (nm != lwork->nm || np != lwork->np)) {
	for (i = 0; i < lwork->nm; i++) {
		if (lwork->mijmkl[i] != NULL) {
			for (j = 0; j < lwork->np; j++) {
				if (lwork->mijmkl[i][j] != NULL) {
					for (k = 0; k < lwork->nm; k++) {
						if (lwork->mijmkl[i][j][k] != NULL && lwork->np != 0) thFree(lwork->mijmkl[i][j][k]);
					}
					if (lwork->nm != 0) thFree(lwork->mijmkl[i][j]);
				}
			}
			if (lwork->np != 0) thFree(lwork->mijmkl[i]);
		}
	}
	if (lwork->nm != 0) thFree(lwork->mijmkl);
	lwork->mijmkl = NULL;
}
if (lwork->mijmkl == NULL && nm != 0 && np != 0) {
	lwork->mijmkl = thCalloc(nm, sizeof(MLEFL ***));
	for (i = 0; i < nm; i++) {
		lwork->mijmkl[i] = thCalloc(np, sizeof(MLEFL**));
		for (j = 0; j < np; j++) {
			lwork->mijmkl[i][j] = thCalloc(nm, sizeof(MLEFL *));
			for (k = 0; k < nm; k++) {
				lwork->mijmkl[i][j][k] = thCalloc(np, sizeof(MLEFL));
			}
		}
	}
}

if (lwork->mijdmm != NULL && (nm != lwork->nm || np != lwork->np)) {
	for (i = 0; i < lwork->nm; i++) {
		if (lwork->mijdmm[i] != NULL && lwork->np != 0) thFree(lwork->mijdmm[i]);
	}
	if (lwork->nm != 0) thFree(lwork->mijdmm);
	lwork->mijdmm = NULL;
}
if (lwork->mijdmm == NULL && nm != 0 && np != 0) {
	lwork->mijdmm = thCalloc(nm, sizeof(MLEFL *));
	for  (i = 0; i < nm; i++) {
		lwork->mijdmm[i] = thCalloc(np, sizeof(MLEFL));
	}
}

#else 

/* new format to save space due to non-existent parameters in models */
if (lwork->MM != NULL && nacc != lwork->nacc) {
	for (i = 0; i < lwork->nacc; i++) {
		if (lwork->MM[i] != NULL) thFree(lwork->MM[i]);
		}
	if (lwork->nacc != 0) thFree(lwork->MM);
	lwork->MM = NULL;
}
if (lwork->MM == NULL && nacc != 0) lwork->MM = thCalloc(nacc, sizeof(MLEFL *));
if (lwork->MM[0] == NULL && nacc != 0) lwork->MM[0] = thCalloc(nacc * nacc, sizeof(MLEFL));
for (i = 0; i < nacc; i++) {
	lwork->MM[i] = lwork->MM[0] + i * nacc;
}
if (lwork->MDmm != NULL && nacc != lwork->nacc) {
	if (lwork->nacc != 0) thFree(lwork->MDmm);
	lwork->MDmm = NULL;
}
if (lwork->MDmm == NULL && nacc != 0) {
	lwork->MDmm = thCalloc(nacc, sizeof(MLEFL));
}
if (lwork->MD != NULL && nacc != lwork->nacc) {
	if (lwork->nacc != 0) thFree(lwork->MD);
	lwork->MD = NULL;
}
if (lwork->MD == NULL && nacc != 0) {
	lwork->MD = thCalloc(nacc, sizeof(MLEFL));
}

if (lwork->MddM != NULL && (nDDacc != lwork->nDDacc || nacc != lwork->nacc)) {
	if (lwork->MddM[0] != NULL) thFree(lwork->MddM[0]);
	thFree(lwork->MddM);
	lwork->MddM = NULL;
}
if (lwork->MddM == NULL && nDDacc != 0 && nacc != 0) {
	lwork->MddM = thCalloc(nacc, sizeof(MLEFL *));
	lwork->MddM[0] = thCalloc(nacc * nDDacc, sizeof(MLEFL));
	for (i = 0; i < nacc; i++) {
		lwork->MddM[i] = lwork->MddM[0] + i * nDDacc;
	}
}

#endif

if (lwork->DaDp != NULL && np != lwork->np && nm != lwork->nm) {
	thFree(lwork->DaDp[0]);
	thFree(lwork->DaDp);
	lwork->DaDp = NULL;
}
if (lwork->DaDp == NULL && nm != 0 && np != 0) {
	lwork->DaDp = thCalloc(nm, sizeof(MLEFL *));
	lwork->DaDp[0] = thCalloc(nm * np, sizeof(MLEFL));
	for (i = 0; i < nm; i++) {
		lwork->DaDp[i] = lwork->DaDp[0] + i * np;
	}
}

if (lwork->DaDpT != NULL && np != lwork->np && nm != lwork->nm) {
	thFree(lwork->DaDpT[0]);
	thFree(lwork->DaDpT);
	lwork->DaDpT = NULL;
}
if (lwork->DaDpT == NULL && nm != 0 && np != 0) {
	lwork->DaDpT = thCalloc(np, sizeof(MLEFL *));
	lwork->DaDpT[0] = thCalloc(nm * np, sizeof(MLEFL));
	for (i = 0; i < np; i++) {
		lwork->DaDpT[i] = lwork->DaDpT[0] + i * nm;
	}
}
if (lwork->wN != NULL && nm != lwork->nm) {
	thFree(lwork->wN[0]);
	thFree(lwork->wN);
	lwork->wN = NULL;
}
if (lwork->wN == NULL && nm != 0) {
	lwork->wN = thCalloc(nm, sizeof(MLEFL *));
	lwork->wN[0] = thCalloc(nm * nm, sizeof(MLEFL));
	for (i = 0; i < nm; i++) {
		lwork->wN[i] = lwork->wN[0] + i * nm;
	}
}
if (lwork->wwN != NULL && nm != lwork->nm) {
	thFree(lwork->wwN[0]);
	thFree(lwork->wwN);
	lwork->wwN = NULL;
}
if (lwork->wwN == NULL && nm != 0) {
	lwork->wwN = thCalloc(nm, sizeof(MLEFL *));
	lwork->wwN[0] = thCalloc(nm * nm, sizeof(MLEFL));
	for (i = 0; i < nm; i++) {
		lwork->wwN[i] = lwork->wwN[0] + i * nm;
	}
}
if (lwork->wA != NULL && nm != lwork->nm) {
	thFree(lwork->wA[0]);
	thFree(lwork->wA);
	lwork->wA = NULL;
}
if (lwork->wA == NULL && nm != 0) {
	lwork->wA = thCalloc(nm, sizeof(MLEFL *));
	lwork->wA[0] = thCalloc(nm * nm, sizeof(MLEFL));
	for (i = 0; i < nm; i++) {
		lwork->wA[i] = lwork->wA[0] + i * nm;
	}
}
if (lwork->wC != NULL && nm != lwork->nm) {
	thFree(lwork->wC);
	lwork->wC = NULL;
}
if (lwork->wC == NULL && nm != 0) {
	lwork->wC = thCalloc(nm, sizeof(MLEFL));
}

MLEFL **w;
w = lwork->wap;
if (w != NULL && (nap != lwork->nap)) {
	if (w[0] != NULL) thFree(w[0]);
	thFree(w);
	w = NULL;
}
if (w == NULL && nap != 0) {
	w = thCalloc(nap, sizeof(MLEFL*));
	w[0] = thCalloc(nap * nap, sizeof(MLEFL));
	for (i = 0; i < nap; i++) {
		w[i] = w[0] + i * nap;
	}
}
lwork->wap = w;

w = lwork->wwap;
if (w != NULL && (nap != lwork->nap)) {
	if (w[0] != NULL) thFree(w[0]);
	thFree(w);
	w = NULL;
}
if (w == NULL && nap != 0) {
	w = thCalloc(nap, sizeof(MLEFL*));
	w[0] = thCalloc(nap * nap, sizeof(MLEFL));
	for (i = 0; i < nap; i++) {
		w[i] = w[0] + i * nap;
	}
}
lwork->wwap = w;

MLEFL **wap;
wap = lwork->wap;
w = lwork->wp;
if (w != NULL && np != lwork->np) {
	if (lwork->np != 0) thFree(w);
	w = NULL;
}
if (w == NULL) w = thCalloc(np, sizeof(MLEFL *));
for (i = 0; i < np; i++) {
	w[i] = wap[i + nm] + nm;
}
lwork->wp = w;

wap = lwork->wwap;
w = lwork->wwp;
if (w != NULL && np != lwork->np) {
	if (lwork->np != 0) thFree(w);
	w = NULL;
}
if (w == NULL) w = thCalloc(np, sizeof(MLEFL *));
for (i = 0; i < np; i++) {
	w[i] = wap[i + nm] + nm;
}
lwork->wwp = w;


/* work space for quasi-newtonian calculations  of type MLEFL */

#if QUASI_NEWTON
MLEFL *wqny, *wqnz;
wqny = lwork->wqny;
wqnz = lwork->wqnz;
if (nap != lwork->nap) {
	if (wqny != NULL) {
		thFree(wqny);
		wqny = NULL;
	}
	if (wqnz != NULL) {
		thFree(wqnz);
		wqnz = NULL;
	}
}
if (wqny == NULL) wqny = thCalloc(nap, sizeof(MLEFL));
if (wqnz == NULL) wqnz = thCalloc(nap, sizeof(MLEFL));

lwork->wqny = wqny;
lwork->wqnz = wqnz;
#endif

/* work space for parameters of type THPIX */
MLEFL *wx, *wwx;
wx = lwork->wx;
wwx = lwork->wwx;
if (nap != lwork->nap) {
	if (wx != NULL) {
		thFree(wx);
		wx = NULL;
	}
	if (wwx != NULL) {
		thFree(wwx);
		wwx = NULL;
	}
}
if (wx == NULL) wx = thCalloc(nap, sizeof(MLEFL));
if (wwx == NULL) wwx = thCalloc(nap, sizeof(MLEFL));

lwork->wx = wx;
lwork->wwx = wwx;

if (lwork->wrnames == NULL) lwork->wrnames = thCalloc(MX_PAR_PER_OBJECT, sizeof(char *));
if (lwork->wpndexmap == NULL) lwork->wpndexmap = thCalloc(MX_PAR_PER_OBJECT, sizeof(int));

if (lwork->wpsfwing == NULL) {
	lwork->wpsfwing = thPsfwingNew();
	if (lwork->wpsfwing->reg == NULL) {
		lwork->wpsfwing->reg = shRegNew("model reg for LPROFILE in LFIT", 
		(int) (2 * PROFILENRAD + 1), (int) (2 * PROFILENRAD + 1), TYPE_THPIX);
		}
	}

if (np != lwork->np && lwork->wMatp != NULL) {
	phMatDel(lwork->wMatp);
	lwork->wMatp = NULL;
}
if (lwork->wMatp == NULL && np > 0) {
	lwork->wMatp = phMatNew(np, np);
	phMatClear(lwork->wMatp);
}
if (nap != lwork->nap && lwork->wMatap != NULL) {
	phMatDel(lwork->wMatap);
	lwork->wMatap = NULL;
}
if (lwork->wMatap == NULL && nap > 0) {
	lwork->wMatap = phMatNew(nap, nap);
	phMatClear(lwork->wMatap);
}
if (nm != lwork->nm && lwork->wMata != NULL) {
	phMatDel(lwork->wMata);
	lwork->wMata = NULL;
}
if (lwork->wMata == NULL && nm > 0) {
	lwork->wMata = phMatNew(nm, nm);
	phMatClear(lwork->wMata);
}
if (np != lwork->np && lwork->wwMatp != NULL) {
	phMatDel(lwork->wwMatp);
	lwork->wwMatp = NULL;
}
if (lwork->wwMatp == NULL && np > 0) {
	lwork->wwMatp = phMatNew(np, np);
}
if (nap != lwork->nap && lwork->wwMatap != NULL) {
	phMatDel(lwork->wwMatap);
	lwork->wwMatap = NULL;
}
if (lwork->wwMatap == NULL && nap > 0) {
	lwork->wwMatap = phMatNew(nap, nap);
}
if (nm != lwork->nm && lwork->wwMata != NULL) {
	phMatDel(lwork->wwMata);
	lwork->wwMata = NULL;
}
if (lwork->wwMata == NULL && nm > 0) {
	lwork->wwMata = phMatNew(nm, nm);
}

if (lwork->wwMatp != NULL) phMatClear(lwork->wwMatp);
if (lwork->wwMatap != NULL) phMatClear(lwork->wwMatap);
if (lwork->wwMata != NULL) phMatClear(lwork->wwMata);

if (np != lwork->np && lwork->wVecp != NULL) {
	for (i = 0; i < LWORK_N_WVEC; i++) {
		phVecDel(lwork->wVecp[i]);
		lwork->wVecp[i] = NULL;
	}
	shFree(lwork->wVecp);
	lwork->wVecp = NULL;
}
if (lwork->wVecp == NULL && np > 0) {
	lwork->wVecp = shCalloc(LWORK_N_WVEC, sizeof(VEC *));
	for (i = 0; i < LWORK_N_WVEC; i++) {
		lwork->wVecp[i] = phVecNew(np);
	}
}
if (nap != lwork->nap && lwork->wVecap != NULL) {
	for (i = 0; i < LWORK_N_WVEC; i++) {
		phVecDel(lwork->wVecap[i]);
		lwork->wVecap[i] = NULL;
	}
	shFree(lwork->wVecap);
	lwork->wVecap = NULL;
}
if (lwork->wVecap == NULL && nap > 0) {
	lwork->wVecap = shCalloc(LWORK_N_WVEC, sizeof(VEC *));
	for (i = 0; i < LWORK_N_WVEC; i++) {
		lwork->wVecap[i] = phVecNew(nap);
	}
}
if (nm != lwork->nm && lwork->wVeca != NULL) {
	for (i = 0; i < LWORK_N_WVEC; i++) {
		phVecDel(lwork->wVeca[i]);
		lwork->wVeca[i] = NULL;
	}
	shFree(lwork->wVeca);
	lwork->wVeca = NULL;
}
if (lwork->wVeca == NULL && nm > 0) {
	lwork->wVeca = shCalloc(LWORK_N_WVEC, sizeof(VEC *));
	for (i = 0; i < LWORK_N_WVEC; i++) {
		lwork->wVeca[i] = phVecNew(nm);
	}
}

if (lwork->wVecp != NULL) {
	for (i = 0; i < LWORK_N_WVEC; i++) {
		phVecClear(lwork->wVecp[i]);
	}
}
if (lwork->wVecap != NULL) {
	for (i = 0; i < LWORK_N_WVEC; i++) {
		phVecClear(lwork->wVecap[i]);
	}
}
if (lwork->wVeca != NULL) {
	for (i = 0; i < LWORK_N_WVEC; i++) {
		 phVecClear(lwork->wVeca[i]);
	}
}

lwork->nj = nj;
lwork->np = np;
lwork->npp = npp;
lwork->nm = nm;
lwork->nc = nc;
lwork->nap = nap;
lwork->nacc = nacc;
lwork->nDDacc = nDDacc;

if (lwork->impacks == NULL) lwork->impacks = thCalloc(NLIMPACKS, sizeof(LIMPACK *));
for (i = 0; i < NLIMPACKS; i++) {
	if (lwork->impacks[i] == NULL) lwork->impacks[i] = thLimpackNew();
}

/* linkage between established amplitude and parameter values in (lmodel) and (lwork) */
if (lmodel->p == NULL && np > 0) {
	thError("%s: ERROR - discovered null (p-array) in (lmodel) while (npar = %d)", name, np);
	return(SH_GENERIC_ERROR);
}
if (lmodel->a == NULL && nm > 0) {
	thError("%s: ERROR - discovered null (a-array) in (lmodel) while (namp = %d)", name, nm);
	return(SH_GENERIC_ERROR);
}
if (lmodel->mcount == NULL && nm > 0) {
	thError("%s: ERROR - discovered null (mcount-array) in (lmodel) while (nmodel = %d)", name, np);
	return(SH_GENERIC_ERROR);
}

lwork->p = lmodel->p;
lwork->a = lmodel->a;
lwork->mcount = lmodel->mcount;

#if DEBUG_MEMORY
printf("%s: memory statistics after allocating (lwork)", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#endif

return(SH_SUCCESS);
}

RET_CODE thLworkDealloc(LWORK *lwork) {
char *name = "thLworkDealloc";
if (lwork == NULL) {
	thError("%s: WARNING - null (lwork) cannot be deallocated", name);
	return(SH_SUCCESS);
}

if (lwork->m != NULL) {
	shRegDel(lwork->m);
	lwork->m = NULL;
}


if (lwork->dmm != NULL) {
	shRegDel(lwork->dmm);
	lwork->dmm = NULL;
}

int i;
/* old - oct 16, 2012 
if (lwork->mreg != NULL && lwork->nc <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (mreg) is allocated while (nc = %d)", name, lwork->nc);
	return(SH_GENERIC_ERROR);
}
if (lwork->mreg != NULL) {
	for (i = 0; i < lwork->nc; i++) {
		if (lwork->mreg[i] != NULL) { 
				thRegDel(lwork->mreg[i]);
		}
	}
	thFree(lwork->mreg);
	lwork->mreg = NULL;
}
*/
if (lwork->mpsf != NULL) {
	for (i = 0; i < lwork->nm; i++) {
		thPsfConvInfoDel(lwork->mpsf[i]);
	}
	thFree(lwork->mpsf);
	lwork->mpsf = NULL;
}

if (lwork->wpsf != NULL) {
	thWpsfDel(lwork->wpsf);
	lwork->wpsf = NULL;
}

if (lwork->da != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (da) is allocated while (nap = %d)", name, lwork->nap);
	return(SH_GENERIC_ERROR);
}
if (lwork->da != NULL) {
	thFree(lwork->da);
	lwork->da = NULL;
}
if (lwork->dp != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (dp) is allocated while (np = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}
if (lwork->dp != NULL) {
	lwork->dp = NULL;
}
if (lwork->daT != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (daT) is allocated while (nap = %d)", name, lwork->nap);
	return(SH_GENERIC_ERROR);
}
if (lwork->daT != NULL) {
	thFree(lwork->daT);
	lwork->daT = NULL;
}
if (lwork->dpT != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (dpT) is allocated while (np = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}
if (lwork->dpT != NULL) {
	lwork->dpT = NULL;
}

if (lwork->db != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (db) is allocated while (nap = %d)", name, lwork->nap);
	return(SH_GENERIC_ERROR);
}
if (lwork->db != NULL) {
	thFree(lwork->db);
	lwork->db = NULL;
}
if (lwork->dq != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (dq) is allocated while (np = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}
if (lwork->dq != NULL) {
	lwork->dq = NULL;
}

/* parameter costs and their derivatives */

if (lwork->dpcost != NULL) {
	thFree(lwork->dpcost);
	lwork->dpcost = NULL;
}
if (lwork->dpcostN != NULL) {
	thFree(lwork->dpcostN);
	lwork->dpcostN = NULL;
}
if (lwork->ddpcost != NULL) {
	if (lwork->ddpcost[0] != NULL) thFree(lwork->ddpcost[0]);
	thFree(lwork->ddpcost);
	lwork->ddpcost = NULL;
}
if (lwork->ddpcostN != NULL) {
	if (lwork->ddpcostN[0] != NULL) thFree(lwork->ddpcostN[0]);
	thFree(lwork->ddpcostN);
	lwork->ddpcostN = NULL;
}

if (lwork->aN != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (aN) is allocated while (nap = %d)", name, lwork->nap);
	return(SH_GENERIC_ERROR);
}
if (lwork->aN != NULL) {
	thFree(lwork->aN);
	lwork->aN = NULL;
}
if (lwork->pN != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (pN) is allocated while (np = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}
if (lwork->pN != NULL) {
	lwork->pN = NULL;
}

if (lwork->aT != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (aT) is allocated while (nap = %d)", name, lwork->nap);
	return(SH_GENERIC_ERROR);
}
if (lwork->aT != NULL) {
	thFree(lwork->aT);
	lwork->aT = NULL;
}
if (lwork->pT != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (pT) is allocated while (np = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}
if (lwork->pT != NULL) {
	lwork->pT = NULL;
}

if (lwork->aold != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (aold) is allocated while (nap = %d)", name, lwork->nap);
	return(SH_GENERIC_ERROR);
}
if (lwork->aold != NULL) {
	thFree(lwork->aold);
	lwork->aold = NULL;
}
if (lwork->pold != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (pold) is allocated while (np = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}
if (lwork->pold != NULL) {
	lwork->pold = NULL;
}

if (lwork->covapT != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (covapT) is allocated while (nap = %d)", name, lwork->nap);
	return(SH_GENERIC_ERROR);
}
if (lwork->covapT != NULL) {
	if (lwork->covapT[0] != NULL) thFree(lwork->covapT[0]);
	thFree(lwork->covapT);
	lwork->covapT = NULL;
}
if (lwork->covaT != NULL && lwork->nm <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (covaT) is allocated while (nm = %d)", name, lwork->nm);
	return(SH_GENERIC_ERROR);
}
if (lwork->covaT != NULL) {
	thFree(lwork->covaT);
	lwork->covaT = NULL;
}
if (lwork->covpT != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (covpT) is allocated while (np = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}
if (lwork->covpT != NULL) {
	thFree(lwork->covpT); 
	lwork->covpT = NULL;
}

if (lwork->mcountN != NULL && lwork->nm <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (mcountN) is allocated while (nm = %d)", name, lwork->nm);
	return(SH_GENERIC_ERROR);
}
if (lwork->mcountN != NULL) {
	thFree(lwork->mcountN);
	lwork->mcountN = NULL;
}

if (lwork->mcountold != NULL && lwork->nm <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (mcountold) is allocated while (nm = %d)", name, lwork->nm);
	return(SH_GENERIC_ERROR);
}
if (lwork->mcountold != NULL) {
	thFree(lwork->mcountold);
	lwork->mcountold = NULL;
}

if (lwork->mN != NULL) {
	shRegDel(lwork->mN);
	lwork->mN = NULL;
}

if (lwork->dmmN != NULL) {
	shRegDel(lwork->dmmN);
	lwork->dmmN = NULL;
}

if (lwork->mregN != NULL && lwork->nc <= 0 ) {
	thError("%s: ERROR - wrongly allocated (lwork): (mregN) is allocated while (nc = %d)", name, lwork->nc);
	return(SH_GENERIC_ERROR);
}
if (lwork->mregN != NULL) {
	for (i = 0; i < lwork->nc; i++) {
		if (lwork->mregN[i] != NULL) { 
			thRegDel(lwork->mregN[i]);
		}
	}
	thFree(lwork->mregN);
	lwork->mregN = NULL;
}

if (lwork->DDmregN != NULL && lwork->npp <= 0 ) {
	thError("%s: ERROR - wrongly allocated (lwork): (DDmregN) is allocated while (npp = %d)", name, lwork->npp);
	return(SH_GENERIC_ERROR);
}
if (lwork->DDmregN != NULL) {
	for (i = 0; i < lwork->npp; i++) {
		if (lwork->DDmregN[i] != NULL) { 
			thRegDel(lwork->DDmregN[i]);
		}
	}
	thFree(lwork->DDmregN);
	lwork->DDmregN = NULL;
}

/* care should be paid here since this matrix is a sub-matrix of one which also includes amplitudes */
if (lwork->jtjap != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (jtjap) allocated while (nap = %d)", name, lwork->nap);
	return(SH_GENERIC_ERROR);
}

if (lwork->jtj != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (jtj) allocated while (nj = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}

if (lwork->jtjap != NULL) {
	 if (lwork->jtjap[0] != NULL) thFree(lwork->jtjap[0]);
	thFree(lwork->jtjap);
	lwork->jtjap = NULL;
}

if (lwork->jtj != NULL) {
	thFree(lwork->jtj);
	lwork->jtj = NULL;
}

if (lwork->wjtjap != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (wjtjap) allocated while (nap = %d)", name, lwork->nap);
	return(SH_GENERIC_ERROR);
}

if (lwork->wjtj != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (wjtj) allocated while (nj = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}

if (lwork->wjtjap != NULL) {
	 if (lwork->wjtjap[0] != NULL) thFree(lwork->wjtjap[0]);
	thFree(lwork->wjtjap);
	lwork->wjtjap = NULL;
}

if (lwork->wjtj != NULL) {
	thFree(lwork->wjtj);
	lwork->wjtj = NULL;
}

/* deallocating AdaDelta related matrices */

if (lwork->E_gg != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (E_gg) allocated while (nj = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}

if (lwork->E_ggap != NULL) {
	thFree(lwork->E_ggap);
	lwork->E_ggap = NULL;
}

if (lwork->E_gg != NULL) {
	/* 
	thFree(lwork->E_gg);
	*/
	lwork->E_gg = NULL;
}

if (lwork->E_gg_prev != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (E_gg_prev) allocated while (nj = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}

if (lwork->E_ggap_prev != NULL) {
	thFree(lwork->E_ggap_prev);
	lwork->E_ggap_prev = NULL;
}

if (lwork->E_gg_prev != NULL) {
	/* 
	thFree(lwork->E_gg_prev);
	*/
	lwork->E_gg_prev = NULL;
}

if (lwork->rms_g != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (rms_g) allocated while (nj = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}

if (lwork->rms_gap != NULL) {
	thFree(lwork->rms_gap);
	lwork->rms_gap = NULL;
}

if (lwork->rms_g != NULL) {
	/* 
	thFree(lwork->rms_g);
	*/
	lwork->rms_g = NULL;
}

if (lwork->rms_g_prev != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (rms_g_prev) allocated while (nj = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}

if (lwork->rms_gap_prev != NULL) {
	thFree(lwork->rms_gap_prev);
	lwork->rms_gap_prev = NULL;
}

if (lwork->rms_g_prev != NULL) {
	/* 
	thFree(lwork->rms_g_prev);
	*/
	lwork->rms_g_prev = NULL;
}

if (lwork->rms_ginv != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (rms_ginv) allocated while (nj = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}

if (lwork->rms_ginvap != NULL) {
	thFree(lwork->rms_ginvap);
	lwork->rms_ginvap = NULL;
}

if (lwork->rms_ginv != NULL) {
	/* 
	thFree(lwork->rms_ginv);
	*/
	lwork->rms_ginv = NULL;
}

if (lwork->rms_ginv_prev != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (rms_ginv_prev) allocated while (nj = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}

if (lwork->rms_ginvap_prev != NULL) {
	thFree(lwork->rms_ginvap_prev);
	lwork->rms_ginvap_prev = NULL;
}

if (lwork->rms_ginv_prev != NULL) {
	/* 
	thFree(lwork->rms_ginv_prev);
	*/
	lwork->rms_ginv_prev = NULL;
}




/* deallocating quasi-newton B-matrix (inverse of Hessian) */
if (lwork->Bqnap != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (Bqnap) allocated while (nap = %d)", name, lwork->nap);
	return(SH_GENERIC_ERROR);
}

if (lwork->Bqn != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (Bqn) allocated while (np = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}

if (lwork->Bqnap != NULL) {
	 if (lwork->Bqnap[0] != NULL) thFree(lwork->Bqnap[0]);
	thFree(lwork->Bqnap);
	lwork->Bqnap = NULL;
}

if (lwork->Bqn != NULL) {
	thFree(lwork->Bqn);
	lwork->Bqn = NULL;
}

if (lwork->Bqnap_prev != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (Bqnap_prev) allocated while (nap = %d)", name, lwork->nap);
	return(SH_GENERIC_ERROR);
}

if (lwork->Bqn_prev != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (Bqn_prev) allocated while (np = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}

if (lwork->Bqnap_prev != NULL) {
	 if (lwork->Bqnap_prev[0] != NULL) thFree(lwork->Bqnap_prev[0]);
	thFree(lwork->Bqnap_prev);
	lwork->Bqnap_prev = NULL;
}

if (lwork->Bqn_prev != NULL) {
	thFree(lwork->Bqn_prev);
	lwork->Bqn_prev = NULL;
}

if (lwork->BBqnap != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (BBqnap) allocated while (nap = %d)", name, lwork->nap);
	return(SH_GENERIC_ERROR);
}

if (lwork->BBqn != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (BBqn) allocated while (np = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}

if (lwork->BBqnap != NULL) {
	if (lwork->BBqnap[0] != NULL) thFree(lwork->BBqnap[0]);
	thFree(lwork->BBqnap);
	lwork->BBqnap = NULL;
}

if (lwork->BBqn != NULL) {
	thFree(lwork->BBqn);
	lwork->BBqn = NULL;
}

if (lwork->BBqnap_prev != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (BBqnap_prev) allocated while (nap = %d)", name, lwork->nap);
	return(SH_GENERIC_ERROR);
}

if (lwork->BBqn_prev != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (BBqn_prev) allocated while (np = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}

if (lwork->BBqnap_prev != NULL) {
	 if (lwork->BBqnap_prev[0] != NULL) thFree(lwork->BBqnap_prev[0]);
	thFree(lwork->BBqnap_prev);
	lwork->BBqnap_prev = NULL;
}

if (lwork->BBqn_prev != NULL) {
	thFree(lwork->BBqn_prev);
	lwork->BBqn_prev = NULL;
}


/* deallocating quasi-newton H-matrix (Hessian) */
if (lwork->Hqnap != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (Hqnap) allocated while (nap = %d)", name, lwork->nap);
	return(SH_GENERIC_ERROR);
}

if (lwork->Hqn != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (Hqn) allocated while (np = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}

if (lwork->Hqnap != NULL) {
	 if (lwork->Hqnap[0] != NULL) thFree(lwork->Hqnap[0]);
	thFree(lwork->Hqnap);
	lwork->Hqnap = NULL;
}

if (lwork->Hqn != NULL) {
	thFree(lwork->Hqn);
	lwork->Hqn = NULL;
}

if (lwork->Hqnap_prev != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (Hqnap_prev) allocated while (nap = %d)", name, lwork->nap);
	return(SH_GENERIC_ERROR);
}

if (lwork->Hqn_prev != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (Hqn_prev) allocated while (np = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}

if (lwork->Hqnap_prev != NULL) {
	 if (lwork->Hqnap_prev[0] != NULL) thFree(lwork->Hqnap_prev[0]);
	thFree(lwork->Hqnap_prev);
	lwork->Hqnap_prev = NULL;
}

if (lwork->Hqn_prev != NULL) {
	thFree(lwork->Hqn_prev);
	lwork->Hqn_prev = NULL;
}

if (lwork->HHqnap != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (HHqnap) allocated while (nap = %d)", name, lwork->nap);
	return(SH_GENERIC_ERROR);
}

if (lwork->HHqn != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (HHqn) allocated while (np = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}

if (lwork->HHqnap != NULL) {
	 if (lwork->HHqnap[0] != NULL) thFree(lwork->HHqnap[0]);
	thFree(lwork->HHqnap);
	lwork->HHqnap = NULL;
}

if (lwork->HHqn != NULL) {
	thFree(lwork->HHqn);
	lwork->HHqn = NULL;
}

if (lwork->HHqnap_prev != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (HHqnap_prev) allocated while (nap = %d)", name, lwork->nap);
	return(SH_GENERIC_ERROR);
}

if (lwork->HHqn_prev != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (HHqn_prev) allocated while (np = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}

if (lwork->HHqnap_prev != NULL) {
	 if (lwork->HHqnap_prev[0] != NULL) thFree(lwork->HHqnap_prev[0]);
	thFree(lwork->HHqnap_prev);
	lwork->HHqnap_prev = NULL;
}

if (lwork->HHqn_prev != NULL) {
	thFree(lwork->HHqn_prev);
	lwork->HHqn_prev = NULL;
}


/* deallocating jtdmm */

if (lwork->jtdmmap != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (jtdmm) allocated while (nap = %d)", name, lwork->nap);
	return(SH_GENERIC_ERROR);
}
if (lwork->jtdmmap != NULL) {
	thFree(lwork->jtdmmap);
	lwork->jtdmmap = NULL;
	}

lwork->jtdmm = NULL;

if (lwork->jtdmmap_prev != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (jtdmm) allocated while (nap = %d)", name, lwork->nap);
	return(SH_GENERIC_ERROR);
}
if (lwork->jtdmmap_prev != NULL) {
	thFree(lwork->jtdmmap_prev);
	lwork->jtdmmap_prev = NULL;
	}

lwork->jtdmm_prev = NULL;

#if 0
if (lwork->dxap != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (dx) allocated while (nap = %d)", name, lwork->nap);
	return(SH_GENERIC_ERROR);
}
if (lwork->dxap != NULL) {
	thFree(lwork->dxap);
	lwork->dxap = NULL;
	}

lwork->dx = NULL;
#endif

/* deallocating jtddm */
if (lwork->jtddmap != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (jtddm) allocated while (nap = %d)", name, lwork->nap);
	return(SH_GENERIC_ERROR);
}
if (lwork->jtddmap != NULL) {
	thFree(lwork->jtddmap);
	lwork->jtddmap = NULL;
	}

lwork->jtddm = NULL;


/* deallocating jtjpdiag */
if (lwork->jtjpdiagap != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (jtjpdiagap) allocated while (nap = %d)", name, lwork->nap);
	return(SH_GENERIC_ERROR);
}

if (lwork->jtjpdiagap != NULL) {
	if (lwork->jtjpdiagap[0] != NULL) thFree(lwork->jtjpdiagap[0]);
	thFree(lwork->jtjpdiagap);
	lwork->jtjpdiagap = NULL;
}

if (lwork->jtjpdiag != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (jtjpdiag) allocated while (np = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}

if (lwork->jtjpdiag != NULL) {
	thFree(lwork->jtjpdiag);
	lwork->jtjpdiag = NULL;
}

/* deallocating Djtj */

if (lwork->Djtjap != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (Djtjap) allocated while (nap = %d)", name, lwork->nap);
	return(SH_GENERIC_ERROR);
	}

if (lwork->Djtjap != NULL) {
	thFree(lwork->Djtjap);
	lwork->Djtjap = NULL;
}
lwork->Djtj = NULL;

/* de-allocating m-matrices */

if (lwork->mid != NULL && lwork->nm <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (mid) allocated while (nm = %d)", name, lwork->nm);
	return(SH_GENERIC_ERROR);
}

if (lwork->mid != NULL) {
	thFree(lwork->mid);
	lwork->mid = NULL;
}

if (lwork->midmm != NULL && lwork->nm <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (midmm) allocated while (nm = %d)", name, lwork->nm);
	return(SH_GENERIC_ERROR);
}
if (lwork->midmm != NULL) {
	thFree(lwork->midmm);
	lwork->midmm = NULL;
}

if (lwork->mimj != NULL && lwork->nm <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (mimj) allocated while (nm = %d)", name, lwork->nm);
	return(SH_GENERIC_ERROR);
}

if (lwork->mimj != NULL) {
	if (lwork->mimj[0] != NULL) thFree(lwork->mimj[0]);
	thFree(lwork->mimj);
	lwork->mimj = NULL;
}

#if OLD_MAP

int j, k;

if (lwork->mimjk != NULL && lwork->nm <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (mimjk) allocated while (nm = %d, np = %d)", name, 
	lwork->nm, lwork->np);
	return(SH_GENERIC_ERROR);
}

if (lwork->mimjk != NULL) {
	for (i = 0; i < lwork->nm; i++) {
		if (lwork->mimjk[i] != NULL) {
			for (j = 0; j < lwork->nm; j++) {
				if (lwork->mimjk[i][j] != NULL && lwork->np > 0) thFree(lwork->mimjk[i][j]);
			}
			thFree(lwork->mimjk[i]);
		}
	}
	thFree(lwork->mimjk);
	lwork->mimjk = NULL;
}

if (lwork->mijmkl != NULL && (lwork->nm <= 0)) {
	thError("%s: ERROR - wrongly allocated (lwork): (mijmkl) allocated while (nm = %d, np = %d)", name,
	lwork->nm, lwork->np);
	return(SH_GENERIC_ERROR);
}

if (lwork->mijmkl != NULL) {
	for (i = 0; i < lwork->nm; i++) {
		if (lwork->mijmkl[i] != NULL) {
			for (j = 0; j < lwork->np; j++) {
				if (lwork->mijmkl[i][j] != NULL) {
					for (k = 0; k < lwork->nm; k++) {
						if (lwork->mijmkl[i][j][k] != NULL) thFree(lwork->mijmkl[i][j][k]);
					}
					thFree(lwork->mijmkl[i][j]);
				}
			}
			if (lwork->np > 0) thFree(lwork->mijmkl[i]);
		}
	}
	thFree(lwork->mijmkl);
	lwork->mijmkl = NULL;
}

if (lwork->mijdmm != NULL && (lwork->nm <= 0 || lwork->np <= 0)) {
	thError("%s: ERROR - wrongly allocated (lwork): (mijdmm) allocated while (nm = %d, np = %d)", name, lwork->nm, lwork->np);
	return(SH_GENERIC_ERROR);
}

if (lwork->mijdmm != NULL) {
	for (i = 0; i < lwork->nm; i++) {
		if (lwork->mijdmm[i] != NULL) thFree(lwork->mijdmm[i]);
	}
	thFree(lwork->mijdmm);
	lwork->mijdmm = NULL;
}

#else

/* the new format is used to save space which was taken due to unavailable parameters in models */
if (lwork->MM != NULL && lwork->nacc <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (MM) allocated while (nacc = %d)", name, 
	lwork->nacc);
	return(SH_GENERIC_ERROR);
}

if (lwork->MM != NULL) {
	if (lwork->MM[0] != NULL) thFree(lwork->MM[0]);
	thFree(lwork->MM);
	lwork->MM = NULL;
}

if (lwork->MDmm != NULL && lwork->nacc <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (MDmm) allocated while (nacc = %d)", name, lwork->nacc);
	return(SH_GENERIC_ERROR);
}

if (lwork->MDmm != NULL) {
	thFree(lwork->MDmm);
	lwork->MDmm = NULL;
}

if (lwork->MD != NULL && lwork->nacc <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (MDmm) allocated while (nacc = %d)", name, lwork->nacc);
	return(SH_GENERIC_ERROR);
}

if (lwork->MD != NULL) {
	thFree(lwork->MD);
	lwork->MD = NULL;
}

if ((lwork->MddM != NULL) && (lwork->nDDacc <= 0) && (lwork->nacc <= 0)) {
	thError("%s: wrongly allocated (lwork): (MddM) allocated while (nacc = %d, nDDacc = %d)", name, lwork->nacc, lwork->nDDacc);
	return(SH_GENERIC_ERROR);
}
if (lwork->MddM != NULL) {
	if (lwork->MddM[0] != NULL) thFree(lwork->MddM[0]);
	thFree(lwork->MddM);
	lwork->MddM = NULL;
}

#endif
/* temporary space */

MLEFL **w;
w = lwork->wap;
if (w != NULL) {
	if (w[0] != NULL) thFree(w[0]);
	thFree(w);
	w = NULL;
}
lwork->wap = NULL;
w = lwork->wwap;
if (w != NULL) {
	if (w[0] != NULL) thFree(w[0]);
	thFree(w);
	w = NULL;
}
lwork->wwap = NULL;
if (lwork->wp != NULL) thFree(lwork->wp);
if (lwork->wwp != NULL) thFree(lwork->wwp);
lwork->wp = NULL;
lwork->wwp = NULL;

/* emptying work space for quasi newtonian work space in MLEFL */
if (lwork->wqny != NULL) thFree(lwork->wqny);
if (lwork->wqnz != NULL) thFree(lwork->wqnz);
lwork->wqny = NULL;
lwork->wqnz = NULL;

if (lwork->DaDp != NULL) {
	if (lwork->DaDp[0] != NULL) thFree(lwork->DaDp[0]);
	thFree(lwork->DaDp);
	lwork->DaDp = NULL;
}
if (lwork->DaDpT != NULL) {
	if (lwork->DaDpT[0] != NULL) thFree(lwork->DaDpT[0]);
	thFree(lwork->DaDpT);
	lwork->DaDpT = NULL;
}
if (lwork->wN != NULL) {
	if (lwork->wN[0] != NULL) thFree(lwork->wN[0]);
	thFree(lwork->wN);
	lwork->wN = NULL;
}
if (lwork->wwN != NULL) {
	if (lwork->wwN[0] != NULL) thFree(lwork->wwN[0]);
	thFree(lwork->wwN);
	lwork->wwN = NULL;
}
if (lwork->wA != NULL) {
	if (lwork->wA[0] != NULL) thFree(lwork->wA[0]);
	thFree(lwork->wA);
	lwork->wA = NULL;
}
if (lwork->wC != NULL) {
	thFree(lwork->wC);
	lwork->wC = NULL;
}


/* emptying work space for THPIX params */
MLEFL *wx;
wx = lwork->wx;
if (wx != NULL) thFree(wx);
lwork->wx = NULL;
wx = lwork->wwx;
if (wx != NULL) thFree(wx);
lwork->wwx = NULL;

char **wrnames = lwork->wrnames;
if (wrnames != NULL) thFree(wrnames);
lwork->wrnames = NULL;

int *wpndexmap = lwork->wpndexmap;
if (wpndexmap != NULL) thFree(wpndexmap);
lwork->wpndexmap = NULL;

if (lwork->wpsfwing != NULL) thPsfwingDel(lwork->wpsfwing);
lwork->wpsfwing = NULL;

if (lwork->wMata != NULL) phMatDel(lwork->wMata);
lwork->wMata = NULL;
if (lwork->wMatp != NULL) phMatDel(lwork->wMatp);
lwork->wMatp = NULL;
if (lwork->wMatap != NULL) phMatDel(lwork->wMatap);
lwork->wMatap = NULL;
if (lwork->wwMata != NULL) phMatDel(lwork->wwMata);
lwork->wwMata = NULL;
if (lwork->wwMatp != NULL) phMatDel(lwork->wwMatp);
lwork->wwMatp = NULL;
if (lwork->wwMatap != NULL) phMatDel(lwork->wwMatap);
lwork->wwMatap = NULL;
if (lwork->wVeca != NULL) {
	for (i = 0; i < LWORK_N_WVEC; i++) {
		phVecDel(lwork->wVeca[i]);
		lwork->wVeca[i] = NULL;
	}
	shFree(lwork->wVeca);
}
lwork->wVeca = NULL;
if (lwork->wVecp != NULL) {
	for (i = 0; i < LWORK_N_WVEC; i++) {
		phVecDel(lwork->wVecp[i]);
		lwork->wVecp[i] = NULL;
	}
	shFree(lwork->wVecp);
}
lwork->wVecp = NULL;
if (lwork->wVecap != NULL) {
	for (i = 0; i < LWORK_N_WVEC; i++) {
		phVecDel(lwork->wVecap[i]);
		lwork->wVecap[i] = NULL;
	}
	shFree(lwork->wVecap);
}
lwork->wVecap = NULL;

lwork->nj = 0;
lwork->np = 0;
lwork->nm = 0;
lwork->nap = 0;
lwork->nc = 0;

if (lwork->impacks != NULL) {
	for (i = 0; i < NLIMPACKS; i++) {
		if (lwork->impacks[i] != NULL) thLimpackDel(lwork->impacks[i]);
	}
	thFree(lwork->impacks);
	lwork->impacks = NULL;
}

return(SH_SUCCESS);
}

RET_CODE thLmodvarAlloc(LMODVAR *lmodvar, int namp, int npar) {
char *name = "thLmodvarAlloc";
if (lmodvar == NULL) {
	shError("%s: ERROR - null (lmodvar) cannot be allocated", name);
	return(SH_GENERIC_ERROR);
}
if (npar < 0) {
	thError("%s: ERROR - cannot allocate (lmodvar) with (npar = %d)", name, npar);
	return(SH_GENERIC_ERROR);
}
if (namp < 0) {
	thError("%s: ERROR - cannot allocate (lmodvar) with (namp = %d)", name, namp);
	return(SH_GENERIC_ERROR);
}
if (lmodvar->npar != 0 && lmodvar->npar != npar) {
	thError("%s: ERROR - (lmodvar) seems to be already allocated with (npar = %d)", name, lmodvar->npar);
	return(SH_GENERIC_ERROR);
}
int nap;
nap = npar + namp;
if (lmodvar->nap != 0 && lmodvar->nap != nap) {
	thError("%s: ERROR - (lmodvar) seems to be already allocated with (namp = %d, npar = %d)", 
	name, lmodvar->namp, lmodvar->npar);
	return(SH_GENERIC_ERROR);
}

int i;
if (lmodvar->covap != NULL && nap != lmodvar->nap) {
	for (i = 0; i < lmodvar->nap; i++) {
		if (lmodvar->covap[i] != NULL) thFree(lmodvar->covap[i]);
	}
	thFree(lmodvar->covap);
	lmodvar->covap = NULL;
}
if (lmodvar->covap == NULL && nap != 0) lmodvar->covap = thCalloc(nap, sizeof(MLEFL *));
for (i = 0; i < nap; i++) if (lmodvar->covap[i] == NULL) lmodvar->covap[i] = thCalloc(nap, sizeof(MLEFL));

if (lmodvar->invcovap != NULL && nap != lmodvar->nap) {
	for (i = 0; i < lmodvar->nap; i++) {
		if (lmodvar->invcovap[i] != NULL) thFree(lmodvar->invcovap[i]);
	}
	thFree(lmodvar->invcovap);
	lmodvar->invcovap = NULL;
}
if (lmodvar->invcovap == NULL && nap != 0) lmodvar->invcovap = thCalloc(nap, sizeof(MLEFL *));
for (i = 0; i < nap; i++) if (lmodvar->invcovap[i] == NULL) lmodvar->invcovap[i] = thCalloc(nap, sizeof(MLEFL));



if (lmodvar->cov != NULL && npar != lmodvar->npar) {
	thFree(lmodvar->cov);
	lmodvar->cov  = NULL;
}
if (lmodvar->cov == NULL && npar != 0) lmodvar->cov = thCalloc(npar, sizeof(MLEFL*));
for (i = 0; i < npar; i++) lmodvar->cov[i] = lmodvar->covap[i + namp] + namp;

if (lmodvar->invcov != NULL && npar != lmodvar->npar) {
	thFree(lmodvar->invcov);
	lmodvar->invcov  = NULL;
}
if (lmodvar->invcov == NULL && npar != 0) lmodvar->invcov = thCalloc(npar, sizeof(MLEFL*));
for (i = 0; i < npar; i++) lmodvar->invcov[i] = lmodvar->invcovap[i + namp] + namp;

lmodvar->L0 = (MLEFL) 0.0;
lmodvar->npar = npar;
lmodvar->namp = namp;
lmodvar->nap = nap;


return(SH_SUCCESS);
}

RET_CODE thLmodvarDealloc(LMODVAR *lmodvar) {
char *name = "thLmodvarDealloc";
if (lmodvar == NULL) {
	shError("%s: WARNING - null (lmodvar) cannot be allocated", name);
	return(SH_SUCCESS);
}

int npar, nap;
MLEFL **cov, **invcov, **covap, **invcovap;

cov = lmodvar->cov;
invcov = lmodvar->invcov;
covap = lmodvar->covap;
invcovap = lmodvar->invcovap;

npar = lmodvar->npar;
nap = lmodvar->nap;

if (cov != NULL && npar <= 0) {
	thError("%s: ERROR - wrongly allocated (lmodvar): (cov) allocated while (npar = %d)", name, lmodvar->npar);
	return(SH_GENERIC_ERROR);
}
if (invcov != NULL && npar <= 0) {
	thError("%s: ERROR - wrongly allocated (lmodvar): (invcov) allocated while (npar = %d)", name, lmodvar->npar);
	return(SH_GENERIC_ERROR);
}
if (invcovap != NULL && nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lmodvar): (invcovap) allocated while (nap = %d)", name, nap);
	return(SH_GENERIC_ERROR);
}
if (covap != NULL && nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lmodvar): (covap) allocated while (nap = %d)", name, nap);
	return(SH_GENERIC_ERROR);
}
int i;
if (covap != NULL) {
	for (i = 0; i < nap; i++) {
		if (covap[i] != NULL) thFree(covap[i]);
	}
	thFree(covap);
	lmodvar->covap = NULL;
}
if (invcovap != NULL) {
	for (i = 0; i < nap; i++) {
		if (invcovap[i] != NULL) thFree(invcovap[i]);
	}
	thFree(invcovap);
	lmodvar->invcovap = NULL;
}
if (cov != NULL) {
	thFree(cov);
	lmodvar->cov = NULL;
}
if (invcov != NULL) {
	thFree(invcov);
	lmodvar->invcov = NULL;
}

lmodvar->nap = 0;
lmodvar->npar = 0;
lmodvar->namp = 0;
lmodvar->L0 = (MLEFL) 0.0;

return(SH_SUCCESS);
}

RET_CODE thLdataClean(LDATA *ldata) {
char *name = "thLdataClean";
if (ldata == NULL) {
	thError("%s: WARNING - null (ldata) cannot be cleaned", name);
	return(SH_SUCCESS);
}

ldata->image = NULL;
ldata->weight = NULL;
ldata->background = NULL;
ldata->mask = NULL;
ldata->psf = NULL;

return(SH_SUCCESS);
}

RET_CODE thLmethodDefault(LMETHOD *lmethod) {
char *name = "thLmethodDefault";
if (lmethod == NULL) {
	thError("%s: ERROR - null (lmethod) cannot be set to default value", name);
	return(SH_GENERIC_ERROR);
}

#ifdef LMETHOD_DEFAULT
lmethod->type = LMETHOD_DEFAULT;
#else
lmethod->type = UNKNOWN_LMETHOD;
#endif

lmethod->cflag = CONTINUE;

#ifdef LM_LAMBDA_DEFAULT
lmethod->lambda = LM_LAMBDA_DEFAULT;
#else 
lmethod->lambda = THNAN;
#endif

#ifdef LM_NU_DEFAULT
lmethod->nu = LM_NU_DEFAULT;
#else
lmethod->nu = THNAN;
#endif

#ifdef LM_GAMMA_DEFAULT
lmethod->gamma = LM_GAMMA_DEFAULT; 
#else 
lmethod->gamma = THNAN;
#endif

#ifdef LM_BETA_DEFAULT
lmethod->beta = LM_BETA_DEFAULT;
#else
lmethod->beta = THNAN;
#endif

#ifdef LM_RHOBAR_DEFAULT
lmethod->rhobar = LM_RHOBAR_DEFAULT;
#else
lmethod->rhobar = THNAN;
#endif

#ifdef LM_Q_DEFAULT
lmethod->q = LM_Q_DEFAULT;
#else
lmethod->q = 0;
#endif


#ifdef LM_TAU_DEFAULT
lmethod->tau = LM_TAU_DEFAULT;
#else
lmethod->tau = THNAN;
#endif

/* convergence for LM */

#ifdef LM_E1_DEFAULT
lmethod->e1 = LM_E1_DEFAULT;
#else
lmethod->e1 = THNAN;
#endif

#ifdef LM_E2_DEFAULT
lmethod->e2 = LM_E2_DEFAULT;
#else
lmethod->e2 = THNAN;
#endif

lmethod->k = 0;
lmethod->count_PGD_noise = 0;
lmethod->k_PGD_noise = 0;
lmethod->perturbation_PGD_condition = 0;

#ifdef LM_KMAX_DEFAULT
lmethod->kmax = LM_KMAX_DEFAULT;
#else 
lmethod->kmax = 0;
#endif

/* Markovian */
#ifdef LM_T_DEFAULT
lmethod->T = LM_T_DEFAULT;
#else 
lmethod->T = THNAN;
#endif

#ifdef LM_SIGMAL_DEFAULT
lmethod->sigmaL = LM_SIGMAL_DEFAULT;
#else
lmethod->sigmaL = THNAN;
#endif

#ifdef LM_ETA_MAX_DEFAULT
lmethod->eta_max = LM_ETA_MAX_DEFAULT;
#else
lmethod->eta_max = THNAN;
#endif

#ifdef LM_MOMENTUM_DEFAULT
lmethod->momentum = LM_MOMENTUM_DEFAULT;
#else
lmethod->momentum = THNAN;
#endif

#ifdef LM_LEARNING_RATE_DEFAULT
lmethod->lr_gd = LM_LEARNING_RATE_DEFAULT;
#else
lmethod->lr_gd = THNAN;
#endif

lmethod->k_fprime_incidence = 0;

#ifdef LM_RHO_ADADELTA_DEFAULT
lmethod->rho_adadelta = LM_RHO_ADADELTA_DEFAULT;
#else
lmethod->rho_adadelta = THNAN;
#endif

#ifdef LM_EPSILON_ADADELTA_DX_DEFAULT
lmethod->epsilon_adadelta_dx = LM_EPSILON_ADADELTA_DX_DEFAULT;
#else
lmethod->epsilon_adadelta_dx = THNAN;
#endif

#ifdef LM_EPSILON_ADADELTA_G_DEFAULT
lmethod->epsilon_adadelta_g = (FL64) LM_EPSILON_ADADELTA_G_DEFAULT;
#else
lmethod->epsilon_adadelta_g = (FL64) THNAN;
#endif

#ifdef LM_COUNT_PGD_THRESH_DEFAULT
lmethod->count_PGD_thresh = LM_COUNT_PGD_THRESH_DEFAULT;
#else
lmethod->count_PGD_thresh = (int) 1 / (int) 0;
#endif

#ifdef LM_K_PGD_THRESH_DEFAULT
lmethod->k_PGD_thresh = LM_K_PGD_THRESH_DEFAULT;
#else
lmethod->k_PGD_thresh = (int) 1 / (int) 0;
#endif

#ifdef LM_PERTURBATION_PGD_SIZE_DEFAULT
lmethod->perturbation_PGD_size = (FL64) LM_PERTURBATION_PGD_SIZE_DEFAULT;
#else
lmethod->perturbation_PGD_size = (FL64) THNAN; 
#endif

#ifdef LM_COST_PGD_THRESH_DEFAULT
lmethod->cost_PGD_thresh = (FL64) LM_COST_PGD_THRESH_DEFAULT;
#else
lmethod->cost_PGD_thresh = (FL64) THNAN;
#endif 

#ifdef LM_MAG_FPRIME_PGD_THRESH
lmethod->mag_Fprime_PGD_thresh = (FL64) LM_MAG_FPRIME_PGD_THRESH;
#else
lmethod->mag_Fprime_PGD_thresh = (FL64) THNAN
#endif

return(SH_SUCCESS);
}

RET_CODE thLstructDealloc(LSTRUCT *lstruct) {
char *name = "thLstructDealloc";
if (lstruct == NULL) {
	thError("%s: WARNING - null (lstruct) cannot be deallocated", name);
	return(SH_SUCCESS);
}

RET_CODE status;
status = thLworkDealloc(lstruct->lwork);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not deallocate (lwork) in (lstruct)", name);
	return(status);
}
status = thLmodelDealloc(lstruct->lmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not deallocate (lmodel) in (lstruct)", name);
	return(status);
}
status = thLmodvarDealloc(lstruct->lcov);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not deallocate (lcov) in (lstruct)", name);
	return(status);
}
status = thLdataClean(lstruct->ldata);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not clean (ldata) in (lstruct)", name);
	return(status);
}
status = thLmethodDefault(lstruct->lmethod);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not set (lmethod) to default value", name);
	return(status);
}

return(SH_SUCCESS);
}




RET_CODE thLdataPut(LDATA *ldata, REGION *image, REGION *weight, LMODEL *background, THMASK *mask, PSFMODEL *psf) {
char *name = "thLdataPut";
if (ldata == NULL) {
	thError("%s: ERROR - cannot put fit info in null (ldata)", name);
	return(SH_GENERIC_ERROR);
}

if (image == NULL && weight == NULL && background == NULL && mask == NULL && psf == NULL) {
	thError("%s: WARNING - nullifying contents of (ldata)", name);
	ldata->image = image;
	ldata->weight = weight;
	ldata->background = background; 
	ldata->mask = mask;   
	ldata->psf = psf;
	return(SH_SUCCESS);
}

if (image == NULL) thError("%s: WARNING - nullifying (image) in (ldata)", name);
ldata->image = image;

#if MLE_LDATA_WEIGHT_DEEP_COPY
if (weight == NULL) {
	thError("%s: WARNING - nullifying (weight) in (ldata)", name);
}
if (ldata->weight != NULL) {
	shRegDel(ldata->weight);
	ldata->weight = NULL;
}
if (weight != NULL) {
	if (weight->type != TYPE_THPIX) {
		thError("%s: ERROR - only THPIX type supported for weight", name);
		return(SH_GENERIC_ERROR);
	}
	int nrow, ncol;
	nrow = weight->nrow;
	ncol = weight->ncol;
	if (nrow > 0 && ncol > 0) {
		REGION *target = shRegNew("ldata - weight - deep copy", nrow, ncol, TYPE_THPIX);
		shRegPixCopy(weight, target);	
		ldata->weight = target;
	} else {
		thError("%s: WARNING - discovered (nrow, ncol) = (%d, %d) while deep copying, leaving null", name, nrow, ncol);
	}
}
#else
if (weight == NULL) thError("%s: WARNING - nullifying (weight) in (ldata)", name);
ldata->weight = weight;
#endif

if (background == NULL) thError("%s: WARNING - nullifying (bkgrnd) in (ldata)", name);
ldata->background = background;

if (mask == NULL) thError("%s: WARNING - nullifying (mask) in (ldata)", name);
ldata->mask = mask;

if (psf == NULL) thError("%s WARNING - nullifying (psf) in (ldata)", name);
ldata->psf = psf;

return(SH_SUCCESS);
}

RET_CODE thLdataGet(LDATA *ldata, REGION **image, REGION **weight, LMODEL **background, THMASK **mask, PSFMODEL **psf) {
char *name = "thLdataGet";
if (ldata == NULL) {
	thError("%s: ERROR - cannot get fit info from null (ldata)", name);
	return(SH_GENERIC_ERROR);
}

if (image != NULL) *image = ldata->image;
if (weight != NULL) *weight = ldata->weight;
if (background != NULL) *background = ldata->background;
if (mask != NULL) *mask = ldata->mask;
if (psf != NULL) *psf = ldata->psf;

return(SH_SUCCESS);
}

RET_CODE thLworkPut(LWORK *lwork, PSFMODEL *psf_model) {
char *name = "thLworkPut";
if (lwork == NULL || psf_model == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

WPSF *wpsf;
if ((wpsf = lwork->wpsf) == NULL) {
	thError("%s: ERROR - null (wpsf): improperly allocated (lwork)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = thWpsfUpdate(wpsf, psf_model);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update (wpsf) for the (psf_model)", name);
	return(status);
}

return(SH_SUCCESS);
}


#if OLD_MAP
#else

RET_CODE thLworkGetMddM(LWORK *lwork, MLEFL ***mDDm, int *nDm, int *nDDm) {
char *name = "thLworkGetMddM";
if (lwork == NULL) {
	thError("%s: ERROR - null (lwork)", name);
	return(SH_GENERIC_ERROR);
}
if (mDDm == NULL) {
	thError("%s: WARNING - null placeholder for (MddM)", name);
} else {
	*mDDm = lwork->MddM;
}
if (nDm != NULL) *nDm = lwork->nacc;
if (nDDm != NULL) *nDDm = lwork->nDDacc;

return(SH_SUCCESS);
}

RET_CODE thLworkGetMM(LWORK *lwork, MLEFL ***mm, int *nmm) {
char *name = "thLworkGetMM";
if (lwork == NULL) {
	thError("%s: ERROR - null (lwork)", name);
	return(SH_GENERIC_ERROR);
}
if (mm == NULL) {
	thError("%s: WARNING - null placeholder for (MM)", name);
} else {
	*mm = lwork->MM;
}
if (nmm != NULL) *nmm = lwork->nacc;
return(SH_SUCCESS);
}

RET_CODE thLworkGetMDmm(LWORK *lwork, MLEFL **mdmm) {
char *name = "thLworkGetMDmm";
if (lwork == NULL) {
	thError("%s: ERROR - null (lwork)", name);
	return(SH_GENERIC_ERROR);
}
if (mdmm == NULL) {
	thError("%s: WARNING - null placeholder for (MDmm)", name);
} else {
	*mdmm = lwork->MDmm;
}
return(SH_SUCCESS);
}

RET_CODE thLworkGetMD(LWORK *lwork, MLEFL **md) {
char *name = "thLworkGetMD";
if (lwork == NULL) {
	thError("%s: ERROR - null (lwork)", name);
	return(SH_GENERIC_ERROR);
}
if (md == NULL) {
	thError("%s: WARNING - null placeholder for (MD)", name);
} else {
	*md = lwork->MD;
}
return(SH_SUCCESS);
}


#endif



/* basic manipulations */
RET_CODE IncrementX(LSTRUCT *lstruct, LSECTOR sector) {
char *name = "IncrementX";
if (lstruct == NULL) {
	thError("%s: ERROR - null (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
if (sector == ASECTOR) {
	status = IncrementA(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not incremenet (amplitude) in (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(status);
	}
} else if (sector == PSECTOR) {
	status = IncrementP(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not increment (parameter) in (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(status);
	}
} else if (sector == APSECTOR) {
	status = IncrementA(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not incremenet (amplitude) in (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(status);
	}
	status = IncrementP(lstruct);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not increment (parameter) in (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(status);
	}
} else {
	thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE IncrementA(LSTRUCT *lstruct) {
char *name = "IncrementA";

LWORK *lwork;
LMODEL *lmodel;
MLEFL *a= NULL, *aN = NULL, *da = NULL;
int nm = 0;
if (lstruct == NULL || (lwork = lstruct->lwork) == NULL || 
	(((da = lwork->da) == NULL || (aN = lwork->aN) == NULL ||
	(lmodel = lstruct->lmodel) == NULL || (a = lmodel->a) == NULL) 
	&& lwork->nm != 0)) {
	thError("%s: ERROR - null input or insufficiently allocated memory", name);
	return(SH_GENERIC_ERROR);
} 
/* this line is needed since inserting it in the if clause might not always execute */
nm = lwork->nm;
int i;
/* incrementing */
for (i = 0; i < nm; i++) {
	aN[i] = a[i] + da[i];
}	
#if DEEP_DEBUG_MLETYPES
printf("%s: ", name);
if (nm == 0) {
	printf("nm = 0 \n");
} else {
	printf("da = ");
}
for (i = 0; i < nm; i++) {
	if (i < nm - 1) {
		printf("%.5e, ", (float) da[i]);
	} else {
		printf("%.5e \n", (float) da[i]);
	}
}
#endif
/* no flag is set in the current version */
return(SH_SUCCESS);
}

RET_CODE IncrementP(LSTRUCT *lstruct) {
char *name = "IncrementP";

LWORK *lwork;
LMODEL *lmodel;
MLEFL *p = NULL, *pN = NULL, *dp = NULL;
int np = 0;
if (lstruct == NULL || (lwork = lstruct->lwork) == NULL || 
	(((dp = lwork->dp) == NULL || (pN = lwork->pN) == NULL ||
	(lmodel = lstruct->lmodel) == NULL || (p = lmodel->p) == NULL) 
	&& lwork->np != 0)) {
	thError("%s: ERROR - null input or insufficiently allocated memory", name);
	return(SH_GENERIC_ERROR);
} 
/* this line is needed since inserting it in the if clause might not always execute */
np = lwork->np;
int i;
/* incrementing */
for (i = 0; i < np; i++) {
	pN[i] = p[i] + dp[i];
}	
#if DEEP_DEBUG_MLETYPES
printf("%s: ", name);
if (np == 0) {
	printf("np = 0 \n");
} else {
	printf("dp = ");
}
for (i = 0; i < np; i++) {
	if (i < np - 1) {
		printf("%.5e, ", (float) dp[i]);
	} else {
		printf("%.5e \n", (float) dp[i]);
	}
}
#endif
/* no flag is set in the current version */
return(SH_SUCCESS);
}

RET_CODE UpdateLMDamping(LSTRUCT *lstruct, MLEFL lambda, MLEFL nu) {
char *name = "UpdateLMDamping";
LMETHOD *lmethod;
if (lstruct == NULL || (lmethod = lstruct->lmethod) == NULL) {
	thError("%s: ERROR - null or improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
	} 
if (lambda <= 0.0 || nu <= 0.0) {
	thError("%s: ERROR - negative or zero damping values (lambda, nu) = (%g, %g) at step (k= %d)", name, lambda, nu, lmethod->k);
	return(SH_GENERIC_ERROR);
	}
lmethod->lambda = lambda;
lmethod->nu = nu;

return(SH_SUCCESS);
}

RET_CODE UpdateXFromXN(LSTRUCT *lstruct, LSECTOR sector) {
char *name = "UpdateXFromXN";

if (lstruct == NULL) {
	thError("%s: ERROR - null (lstruct)", name);
	return(SH_GENERIC_ERROR);
	}

RET_CODE status;
MLEFL *x, *xN, *xold, *xT, *dx;
int n;
MLEFL **mcount, **mcountN;
status = ExtractXxNdX(lstruct, sector, &xold, &xT, &x, &xN, &dx, &n);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract, (x, xN, or dx) from (lstruct)", name);
	return(status);
}
status = ExtractMcount(lstruct, sector, &mcount, &mcountN);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract, (mcount, mcountN, or nmcount) from (lstruct)", name);
	return(status);
}

LWORK *lwork;
lwork = lstruct->lwork;

MLEFL *xtmp = NULL;
if (lwork->wx != NULL) xtmp = lwork->wx;


/* switching the place of the two arrays */
MLEFL dxdx = 0.0;
int i;
for (i = 0; i < n; i++) {
	dx[i] = xN[i] - x[i];
	dxdx += pow(dx[i], 2.0);
}
dxdx = pow(dxdx, 0.5);
printf("%s: sector = '%s' , || dx || = %G \n", name, shEnumNameGetFromValue("LSECTOR", sector), (double) dxdx);

if (xtmp != NULL) {
	for (i = 0; i < n; i++) {
		xtmp[i] = x[i];
		xold[i] = x[i];
		x[i] = xN[i];
		/* 
		xN[i] = xtmp[i];
		*/
	}
} else {
	for (i = 0; i < n; i++) {
		#if 0
		x[i] -= xN[i];
		xN[i] += x[i];
		x[i] = xN[i] - x[i];
		#else
		xold[i] = x[i];
		x[i] = xN[i];
		#endif
	}
}

if (mcount != NULL && mcountN != NULL) {	
MLEFL *mtmp;
mtmp = *mcount;
*mcount = *mcountN;
*mcountN = mtmp;
}

if (sector == APSECTOR) {
	lwork->pupdate = UPDATED;
	lwork->aupdate = UPDATED;
	/* 
	for (i = 0; i < n; i++) {
		dx[i] = x[i] - xN[i];
	}
	*/
} else if (sector == PSECTOR) {
	lwork->pupdate = UPDATED;
} else if (sector == ASECTOR) {
	lwork->aupdate = UPDATED;
	/* 
	for (i = 0; i < n; i++) {
		dx[i] = x[i] - xN[i];
	}
	*/
} else {
	thError("%s: ERROR - unsupported (sector = '%s'), cannot change (X-update) flag", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);	
}

return(SH_SUCCESS);
}	

RET_CODE ExtractLMConvergence(LSTRUCT *lstruct, int *k, MLEFL *e1, MLEFL *e2, int *kmax) {
char *name = "ExtractLMConvergence";
if (lstruct == NULL || lstruct->lmethod == NULL) {
	thError("%s: ERROR - null or insufficiently allocated object memory", name);
	return(SH_GENERIC_ERROR);
}
LMETHOD *lmethod = lstruct->lmethod;
if (k != NULL) *k = lmethod->k;
if (e1 != NULL) *e1 = lmethod->e1;
if (e2 != NULL) *e2 = lmethod->e2;
if (kmax != NULL) *kmax = lmethod->kmax;

LMETHODTYPE type = lmethod->type;
if (type != LM && type != EXTLM && type != HYBRIDLM) {
	thError("%s: WARNING - MLE method ('%s') is not Levenberg-Marquardt-based", name, 
	shEnumNameGetFromValue("LMETHODTYPE", type));
	}
return(SH_SUCCESS);
}

RET_CODE ExtractLMStepNo(LSTRUCT *lstruct, int *k, int *kmax) {
char *name = "ExtractLMStepNo";
if (lstruct == NULL || lstruct->lmethod == NULL) {
	thError("%s: ERROR - null or insufficiently allocated object memory", name);
	return(SH_GENERIC_ERROR);
}
if (k == NULL && kmax == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
LMETHOD *lmethod = lstruct->lmethod;
if (k != NULL) *k = lmethod->k;
if (kmax != NULL) *kmax = lmethod->kmax;

return(SH_SUCCESS);
}


RET_CODE UpdateLMStepNo(LSTRUCT *lstruct, int k) {
char *name = "UpdateLMStepNo";
if (lstruct == NULL || lstruct->lmethod == NULL) {
	thError("%s: ERROR - null or improperly allocated object", name);
	return(SH_GENERIC_ERROR);
}
lstruct->lmethod->k = k;
return(SH_SUCCESS);
}

RET_CODE UpdateLMConvergence(LSTRUCT *lstruct, CONVERGENCE flag) {
char *name = "UpdateLMConvergence";
if (lstruct == NULL || lstruct->lmethod == NULL) {
	thError("%s: ERROR - null or improperly allocated object", name);
	return(SH_GENERIC_ERROR);
	}
if (flag == N_CONVERGENCE || flag == UNKNOWN_CONVERGENCE) {
	thError("%s: WARNING - meaningless convergence flag supplied", name);
}
lstruct->lmethod->cflag = flag;
return(SH_SUCCESS);
}

RET_CODE UpdateLMSmallStepCount(LSTRUCT *lstruct, CHISQFL Dchisq) {
char *name = "UpdateLMSmallStepCount";
LMETHOD *lm = NULL;
if (lstruct == NULL || (lm = lstruct->lmethod) == NULL) {
	thError("%s: ERROR - null or improperly allocated object", name);
	return(SH_GENERIC_ERROR);
	}
if (fabs((float) Dchisq) > (float) LM_SMALL_STEP_DCHISQ_VALUE) {
	lm->small_step_count = 0.0;
} else if (Dchisq >= (CHISQFL) 0) {
	lm->small_step_count += 1.0;
} else if (Dchisq < (CHISQFL) 0) {
	lm->small_step_count += LM_SMALL_STEP_INCREMENT_NEGATIVE_DCHISQ;
} else {
	thError("%s: ERROR - problematic (Dchisq = %g) passed", name, (float) Dchisq);
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}
RET_CODE ExtractLMCurrentP(LSTRUCT *lstruct, MLEFL **p) {
char *name = "ExtractLMCurrentP";
if (lstruct == NULL || lstruct->lmodel == NULL || lstruct->lmodel->p == NULL || p == NULL) {
	thError("%s: ERROR - null or improperly allocated object", name);
	return(SH_GENERIC_ERROR);
	}
*p = lstruct->lmodel->p;
return(SH_SUCCESS);
}

RET_CODE ExtractLMDamping(LSTRUCT *lstruct, 
	MLEFL *lambda, MLEFL *beta, 
	MLEFL *gamma, MLEFL *rhobar, 
	MLEFL *nu, int *q, int *step_count, int *step_mode) {
char *name = "ExtractLMDamping";
if (lstruct == NULL || lstruct->lmethod == NULL) {
	thError("%s: ERROR - null or improperly allocated object", name);
	return(SH_GENERIC_ERROR);
}
LMETHOD *lmethod;
lmethod = lstruct->lmethod;
if (lambda != NULL) *lambda = lmethod->lambda;
if (beta != NULL) *beta = lmethod->beta;
if (gamma != NULL) *gamma = lmethod->gamma;
if (rhobar != NULL) *rhobar = lmethod->rhobar;
if (nu != NULL) *nu = lmethod->nu;
if (q != NULL) {
	*q = lmethod->q;
	if (*q % 2 != 1) {
		thError("%s: WARNING - exponent-q values are expected to be odd integers", name);
		}
}
if (step_count != NULL) *step_count = lmethod->step_count;
if (step_mode != NULL) *step_mode = lmethod->step_mode;
return(SH_SUCCESS);
}

RET_CODE thLstructGetNModel(LSTRUCT *lstruct, int *nmodel) {

char *name = "thLstructGetNModel";
if (lstruct == NULL || lstruct->lmodel == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}

if (nmodel == NULL) {
	thError("%s: WARNING - null (nmodel) passed, cannot serve as a placeholder for a real int variable", name);
	return(SH_SUCCESS);
}

*nmodel = lstruct->lmodel->namp;
return(SH_SUCCESS);
} 

RET_CODE thLstructGetNX(LSTRUCT *lstruct, LSECTOR sector, int *nx) {
char *name = "thLstructGetNX";
RET_CODE status;
if (sector == APSECTOR) {
	int np = -1, nm = -1;
	status = thLstructGetNPar(lstruct, &np);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (npar) while (sector = '%s')",name, shEnumNameGetFromValue("LSECTOR", sector));
		return(status);
	}
	status = thLstructGetNModel(lstruct, &nm);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (nmodel) while (sector = '%s')",name, shEnumNameGetFromValue("LSECTOR", sector));
		return(status);
	}
	if (np < 0 || nm < 0) {
		thError("%s: ERROR - received (nmodel = %d, np = %d)", name, nm, np);
		return(SH_GENERIC_ERROR);
	} 
	if (nx != NULL) *nx = np + nm;
} else if (sector == ASECTOR) {
	status = thLstructGetNModel(lstruct, nx);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (nmodel) from (lstruct) while (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(status);
	}
} else if (sector == PSECTOR) {
	status = thLstructGetNPar(lstruct, nx);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (npar) from (lstruct) while (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(status);
	}
} else {
	thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE thLstructGetNPar(LSTRUCT *lstruct, int *npar) {

char *name = "thLstructGetNPar";
if (lstruct == NULL || lstruct->lmodel == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}

if (npar == NULL) {
	thError("%s: WARNING - null (npar) passed, cannot serve as a placeholder for a real int variable", name);
	return(SH_SUCCESS);
}

*npar = lstruct->lmodel->npar;
return(SH_SUCCESS);
} 

RET_CODE thLstructGetLmachine(LSTRUCT *lstruct, LMACHINE **lmachine) {
char *name = "thLstructGetLmachine";
if (lstruct == NULL || lstruct->lmodel == NULL) {
	thError("%s: ERROR - improperly allocated (lmachine)", name);
	return(SH_GENERIC_ERROR);
	}
if (lmachine == NULL) {
	thError("%s: WARNING - null place holder for (lmachine) cannot contain the real value", name);
	return(SH_SUCCESS);
}
if (lstruct->lmodel->lmachine == NULL) {
	thError("%s: WARNING - null (lmachine) detected in (lstruct) - improper allocation", name);
	} else {
	*lmachine = lstruct->lmodel->lmachine;
	}
return(SH_SUCCESS);
}

RET_CODE thLstructGetPN(LSTRUCT *lstruct, MLEFL **pN) {
char *name = "thLstructGetpN";
LWORK *lwork;
if (lstruct == NULL || (lwork = lstruct->lwork) == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
if (pN == NULL) {
	thError("%s: WARNING - null place holder for (pN)", name);
	return(SH_SUCCESS);
}

int np;
if ((*pN = lwork->pN) == NULL && (np = lwork->np) != 0) {
	thError("%s: ERROR - null (pN) detected in (lstruct) while (np = %d)", name, np);
	return(SH_GENERIC_ERROR);
}
	
return(SH_SUCCESS);
}

RET_CODE thLwmaskPut(LWMASK *lwmask, LDATA *ldata, LMODEL *lmodel) {
char *name = "thLwmaskPut";
if (lwmask == NULL && (ldata != NULL || lmodel != NULL)) {
	thError("%s: ERROR - null (lwmask) cannot be used to put any contents", name);
	return(SH_GENERIC_ERROR);
}
if (ldata == NULL && lmodel == NULL) {
	thError("%s: WARNING - null (ldata) and (lmodel), no content change in (lwmask)",
	name);
	return(SH_SUCCESS);
}

RET_CODE status;
status = thLwmaskFree(lwmask);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not free (lwmask)", name);
	return(status);
}

if (ldata != NULL) {
		if (ldata->weight == NULL) {
			thError("%s: WARNING - null weight found in (ldata)", name);
		}
		if (ldata->mask == NULL) {
			thError("%s: WARNING - null mask found in (ldata)", name);
		}
		status = thLwmaskPutW(lwmask, ldata->weight);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not put (W) into (lwmask)", name);
			return(status);
		}
		status = thLwmaskPutMask(lwmask, ldata->mask);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not put (mask) into (lwmask)", name);
			return(status);
		}
	}
if (lmodel != NULL) {
	/* there is no bit mask for spankmask */
	#if (THMASKisMASK | THMASKisSUPERMASK)
	status = thLwmaskPutMaskbit(lwmask, lmodel->fitregbit);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put (maskbit) into (lwmask)", name);
		return(status);
	}
	#endif
	MASKBIT maskbit = 0x0;
	status = thLwmaskGetMaskbit(lwmask, &maskbit);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (maskbit) from (lwmask)", name);
		return(status);
	}
	if (maskbit == 0) {
		thError("%s: WARNING - (fitregbit=0x0) found in (lmodel), replacing it with (%x)", name, (int) DEFAULT_FITREGBIT);
		status = thLwmaskPutMaskbit(lwmask, (MASKBIT) DEFAULT_FITREGBIT);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not put default (maskbit) into (lwmask)", name);
			return(status);
		}
	}
} else {	
	thError("%s: WARNING - (lmodel) not found , setting (fitregbit = %x)", name, (int) DEFAULT_FITREGBIT);
	status = thLwmaskPutMaskbit(lwmask, (MASKBIT) DEFAULT_FITREGBIT);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put default (maskbit) into (lwmask)", name);
		return(status);
	}
}

int nactive = 1, mini_nactive = 1;
#if 0
status = thLwmaskSample(lwmask, nactive, mini_nactive);
#else
status = thLwmaskSample(lwmask, nactive);
#endif
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make (lwmask) with (nactive = %d, mini_nactive = %d)", name, nactive, mini_nactive);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE thLwmaskCountPix(LWMASK *lwmask, int *npix) {
char *name = "thLwmaskCountPix";

if (lwmask == NULL && npix == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
} else if (lwmask == NULL && npix != NULL) {
	thError("%s: ERROR - null input, cannot count pixels", name);
	return(SH_GENERIC_ERROR);
} else if (npix == NULL) {
	thError("%s: WARNING - null placeholder, wouldn't count pixels", name);
}

RET_CODE status;
THMASK *mask = NULL;
status = thLwmaskGetMask(lwmask, &mask);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (mask) from (lwmask)", name);
	return(status);
}

if (mask == NULL) {
	thError("%s: WARNING - null (mask), counting all pixels in the frame", name);
	REGION *W = NULL;
	status = thLwmaskGetW(lwmask, &W);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (W) from (lwmask)", name);
		return(status);
	}
	if (W != NULL) {
		if (npix != NULL) *npix = W->nrow * W->ncol;
	} else {
		thError("%s: ERROR - null weight matrix (W) in (lwmask) - no way to guess size of the frame", name);
		return(SH_GENERIC_ERROR);
	}
} else if (mask->rows_mask == NULL) {
	thError("%s: ERROR - null (rows) found in (mask)", name);
	return(SH_GENERIC_ERROR);
} else if (npix != NULL)  {
	int npx = 0;
	#if THMASKisSPANMASK
	if (npix != NULL) *npix = mask->npix;
	#endif
	#if (THMASKisSUPERMASK | THMASKisMASK)
	int i, j;
	MASKBIT bit, *row;
	status = thLwmaskGetMaskbit(lwmask, &bit);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (maskbit) from (lwmask)", name);
		return(status);
	}
	for (i = 0; i < mask->nrow; i++) {
		row = mask->rows_mask[i];
		for (j = 0; j < mask->ncol; j++) {
			npx += (row[j] == bit);
		}
	}
	*npix = npx;
	#endif
}

return(SH_SUCCESS);
}

RET_CODE thLstructGetAmp(LSTRUCT *lstruct, MLEFL **amp) {
char *name = "thLstructGetAmp";
if (lstruct == NULL || lstruct->lmodel == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
if (amp == NULL) {
	thError("%s: WARNING - null place holder for (amp) - cannot pass value", name);
	return(SH_SUCCESS);
}
if (lstruct->lmodel->a == NULL) {
	thError("%s: WARNING - null (amp) detected in (lstruct), suggesting improper allocation", name);
}
*amp = lstruct->lmodel->a;
return(SH_SUCCESS);
}

RET_CODE thLstructGetAmpN(LSTRUCT *lstruct, MLEFL **amp) {
char *name = "thLstructGetAmpN";
if (lstruct == NULL || lstruct->lwork == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
if (amp == NULL) {
	thError("%s: WARNING - null place holder for (amp) - cannot pass value", name);
	return(SH_SUCCESS);
}
if (lstruct->lwork->aN == NULL) {
	thError("%s: WARNING - null (ampN) detected in (lstruct), suggesting improper allocation", name);
}
*amp = lstruct->lwork->aN;
return(SH_SUCCESS);
}

RET_CODE thLstructAssignDa(LSTRUCT *lstruct) {
char *name = "thLstructAssignDa";
LWORK *lwork = NULL;
if (lstruct == NULL || (lwork = lstruct->lwork) == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
MLEFL *a = lwork->a;
MLEFL *aN = lwork->aN;
MLEFL *da = lwork->da;
int nmodel = lwork->nm;

if (nmodel <= 0) {
	thError("%s: ERROR - (nmodel = %d) found in (lwork)", name);
	return(SH_GENERIC_ERROR);
} else if (a == NULL || aN == NULL || da == NULL) {
	thError("%s: ERROR - null (a, aN, or da) array found in (lwork)", name);
	return(SH_GENERIC_ERROR);
}
int i;
for (i = 0; i < nmodel; i++) da[i] = aN[i] - a[i];

return(SH_SUCCESS);
}


RET_CODE thLstructGetChisq(LSTRUCT *lstruct,  CHISQFL *chisq, CHISQFL *cost) {
char *name = "thLstructGetChisq";
if (lstruct == NULL || lstruct->lwork == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
if (chisq == NULL && cost == NULL) {
	thError("%s: WARNING - null placeholder - cannot pass value", name);
	return(SH_SUCCESS);
}
if (chisq != NULL) *chisq = lstruct->lwork->chisq;
if (cost != NULL) *cost = lstruct->lwork->cost;
return(SH_SUCCESS);
}

RET_CODE thLstructGetChisqN(LSTRUCT *lstruct, CHISQFL *chisqN, CHISQFL *costN) {
char *name = "thLstructGetChisqN";
if (lstruct == NULL || lstruct->lwork == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
if (chisqN == NULL && costN == NULL) {
	thError("%s: WARNING - null placeholder - cannot pass value", name);
	return(SH_SUCCESS);
}
if (chisqN != NULL) *chisqN = lstruct->lwork->chisqN;
if (costN != NULL) *costN = lstruct->lwork->costN;
if (lstruct->lwork->chisqN > lstruct->lwork->costN) {
	thError("%s: WARNING - found (chisqN = %g) > (costN > %g)", name, (float) lstruct->lwork->chisqN, (float) lstruct->lwork->costN);
}

return(SH_SUCCESS);
}

RET_CODE UpdateChisq(LSTRUCT *lstruct) {
char *name = "UpdateChisq";
LWORK *lwork;
if (lstruct == NULL || (lwork = lstruct->lwork) == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
	}
CHISQFL x;
x = lwork->chisqN;
lwork->chisqN = lwork->chisq;
lwork->chisq = x;

x = lwork->costN;
lwork->costN = lwork->cost;
lwork->cost = x;

return(SH_SUCCESS);
}

RET_CODE CopyChisqNChisq(LSTRUCT *lstruct) {
char *name = "CopyChisqNChisq";
LWORK *lwork;
if (lstruct == NULL || (lwork = lstruct->lwork) == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
	}
lwork->chisq = lwork->chisqN;
lwork->cost = lwork->costN;

return(SH_SUCCESS);
}


LIMPACK *thLimpackNew() {
LIMPACK *impack;
impack = (LIMPACK *)thCalloc(1, sizeof(LIMPACK));
impack->mreg = (THREGION **) thCalloc(MX_PAR_PER_MODEL, sizeof(THREGION *));
#if MLE_CONDUCT_HIGHER_ORDER 
impack->DDmreg = (THREGION **) thCalloc(MX_PAR_PER_MODEL, sizeof(THREGION **));
impack->DDmreg[0] = (THREGION *) thCalloc(MX_PAR_PER_MODEL * MX_PAR_PER_MODEL, sizeof(THREGION *));
int i;
for (i = 0; i < MX_PAR_PER_MODEL; i++) {
	impack->DDmreg[i] = impack->DDmreg[0] + i * MX_PAR_PER_MODEL;
}
#endif

impack->nmpar_max = MX_PAR_PER_MODEL;

return(impack);
}
void thLimpackDel(LIMPACK *impack) {
if (impack != NULL) {
	if (impack->mreg != NULL) {
		thFree(impack->mreg);
	}
	if (impack->DDmreg != NULL) {
		if (impack->DDmreg[0] != NULL) thFree(impack->DDmreg[0]);
		thFree(impack->DDmreg);
	}
	thFree(impack);
}
return;
}

RET_CODE thLimpackGetMemory(LIMPACK *impack, MEMFL *memory) {
char *name = "thLimpackGetMemory";
if (impack == NULL && memory == NULL) {
	thError("%s: WARNING - null input and output", name);
	return(SH_SUCCESS);
}
if (impack == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (memory == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
THREGION *reg;
int nmpar = impack->nmpar;
int i;
MEMFL memory_i, memory_t = 0;
for (i = 0; i <= nmpar; i++) {
	reg = impack->mreg[i];
	if (reg != NULL) {
		RET_CODE status = thRegGetMemory(reg, &memory_i);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get memory for region (%d) in (impack)", name, i);
			return(status);
		}
		memory_t += memory_i;
	}
}

*memory = memory_t;
return(SH_SUCCESS);
}



RET_CODE thConstructImagePack(LSTRUCT *lstruct, int i, LIMPACK *impack) {
char *name = "thConstructImagePack";

if (lstruct == NULL || lstruct->lmodel == NULL || lstruct->lmodel->lmachine == NULL 
	|| lstruct->lwork == NULL 
	|| impack == NULL) {
	thError("%s: ERROR - wrongly allocated (lstruct) or (limpack)", name);
	return(SH_GENERIC_ERROR);
	}

RET_CODE status;
int nmodel;
status = thLstructGetNModel(lstruct, &nmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract the number of models", name);
	return(status);
	}
if (i < 0 || i >= nmodel) {
	thError("%s: ERROR - model number (%d) out of bound (0, %d)", name, i, nmodel -1);
	return(SH_GENERIC_ERROR);
	}
/* get name of the object model
get list of parameters
get location of the parameter in the list
*/
LMACHINE *lmachine;
lmachine = lstruct->lmodel->lmachine;

int npar;
status = thLstructGetNPar(lstruct, &npar);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract number of parameters for the fit", name);
	return(SH_GENERIC_ERROR);
	}

int *ploc;
int nmpar, nmpar_max;
nmpar_max = impack->nmpar_max;
status = thLmachineGetModelPars(lmachine, i, &nmpar, &ploc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract the number of paramaters and their location for model (%d)", name, i);
	return(SH_GENERIC_ERROR);
}
if (nmpar >= nmpar_max) {
	thError("%s: ERROR - model has (nmpar = %d) parameters while (impack) supports (%d) regions", name, nmpar, nmpar_max);
	return(SH_GENERIC_ERROR);
}

THREGION **mreg;
mreg = lstruct->lwork->mregN;

if (mreg == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (mregN = null) found", name);
	return(SH_GENERIC_ERROR);
	}

int j, k;
impack->nmpar = nmpar;
status = thMGetInModelMLocOfP(lmachine, i, -1, &k);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not find location of model (%d)", name, i);
	return(status);
}
impack->mreg[0] = mreg[k];
if (impack->mreg[0] == NULL) {
	thError("%s: WARNING - null region detected for the main model (%d)", name, i);
}
for (j = 0; j < nmpar; j++) {
	status = thMGetInModelMLocOfP(lmachine, i, ploc[j], &k);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not find location of derivative of model (%d) w.r.t. argument (%d)", name, i, j);
		return(SH_GENERIC_ERROR);
	}
	if (k < 0) {
		impack->mreg[j] = NULL;
	} else {
		impack->mreg[j + 1] = mreg[k];
		if (mreg[k] == NULL) {
		thError("%s: WARNING - null region detected in parameter (%d) of model(%d)", name, j, i);
		}
	}
}

/* mcount */
impack->mcount = lstruct->lwork->mcountN + i;

#if MLE_CONDUCT_HIGHER_ORDER

THREGION **DDmreg;
DDmreg = lstruct->lwork->DDmregN;

if (DDmreg == NULL && nmpar > 0) {
	thError("%s: ERROR - wrongly allocated (lwork) - found (DDmreg = null) while nmpar = %d", name, nmpar);
	return(SH_GENERIC_ERROR);
	}

int j1, j2;
impack->nmpar = nmpar;

for (j1 = 0; j1 < nmpar; j1++) {
for (j2 = 0; j2 < nmpar; j2++) {
	/* 
	status = thMGetInModelMLocOfPP(lmachine, i, ploc[j1], ploc[j2], &k);
	*/
	status = thMGetInModelMLocOfPP(lmachine, i, j1, j2, &k);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not find location of derivative of model (%d) w.r.t. argument (%d, %d)", name, i, j1, j2);
		return(SH_GENERIC_ERROR);
	}
	if (k < 0) {
		impack->DDmreg[j1][j2] = NULL;
	} else {
		impack->DDmreg[j1][j2] = DDmreg[k];
		#if DEBUG_MLETYPES_HIGHER_ORDER
		if (DDmreg[k] == NULL) {
		thError("%s: WARNING - null region detected in parameter (%d, %d) of model(%d)", name, j1, j2, i);
		}
		#endif
	
	}
}
}

#endif


return(SH_SUCCESS);
}

RET_CODE thLstructGetFpack(LSTRUCT *lstruct, int i, LFPACK **fpack) {
char *name = "thLstructGetFpack";

/* 

Parts of this function can be used to generate a list of function packs at the beginning of the process

this function creates an object FPACK that has the name of all function that can be used to create the main model image and its derivatives */

if (lstruct == NULL) {
	thError("%s: ERROR - null (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
LMODEL *lmodel;
if ((lmodel = lstruct->lmodel) == NULL) {
	thError("%s: ERROR - null (lmodel) in (lstruct)", name);
	return(SH_GENERIC_ERROR);
} 
LMACHINE *lmachine;
if ((lmachine = lmodel->lmachine) == NULL) {
	thError("%s: ERROR - null (lmachine) in (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
LFBANK *lfbank;
if ((lfbank = lstruct->lfbank) == NULL) {
	thError("%s: ERROR - null (lfbank) in (lstruct)", name);
	return(SH_GENERIC_ERROR);
} 
if (fpack == NULL) {
	thError("%s: WARNING - null placeholder for (fpack)", name);
	return(SH_SUCCESS);
	}

int namp;
if (i < 0 || i > (namp = lmodel->namp)) {
	thError("%s: ERROR - model number (%d) out of bound (0, %d)", name, i, namp - 1);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
char *fname;
status = thLmachineGetModelName(lmachine, i, &fname);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - no model name available for model (%d)", name, i);
	return(SH_GENERIC_ERROR);
	}

lfbank = lfbank;

int j, nf;
nf = shChainSize(lfbank);

LFPACK *fpack1;
*fpack = NULL;
for (j = 0; j < nf; j++) {
	fpack1 = (LFPACK *) shChainElementGetByPos(lfbank, j);
	if (!strcmp(fpack1->fname, fname)) {
		*fpack = fpack1;
	}
}
if (*fpack == NULL) {
	thError("%s: ERROR - could not find the appropriate function pack for model (%d) (%s)", name, i, fname);
	return(SH_GENERIC_ERROR);
}
return(SH_SUCCESS);
}

RET_CODE thGetFParam(LMACHINE *lmachine, int i, void **fparam) {
char *name = "thGetFParam";

/* this function generates a particular type given by lmachine, then casts values in p onto the parameters in fparam */

if (lmachine == NULL || lmachine->cflag != COMPILED) {
	thError("%s: ERROR - null or uncompiled (lmachine)", name);
	if (fparam != NULL) *fparam = NULL;
	return(SH_GENERIC_ERROR);
	}
if (lmachine->ps == NULL) {
	thError("%s: ERROR - null (ps) in (lmachine)", name);
	if (fparam != NULL) *fparam = NULL;
	return(SH_GENERIC_ERROR);
}

int namp;
if (i >= (namp = lmachine->namp)) {
	thError("%s: ERROR - (namp = %d) models supported by (lmachine) while searching for model (%d)", 
	name, namp, i);
	if (fparam != NULL) *fparam = NULL;
	return(SH_GENERIC_ERROR);
}
if (fparam == NULL) {
	thError("%s: WARNING - null placeholder for (fparam)", name);
	return(SH_SUCCESS);
}

*fparam = lmachine->ps[i];
return(SH_SUCCESS);

}

RET_CODE thDelFparam(LMACHINE *lmachine, int i, void *fparam) {
char *name = "thDelFparam";

if (lmachine == NULL) {
	thError("%s: ERROR - unallocated (lmachine) while processing model (%d)", name, i);
	return(SH_GENERIC_ERROR);
	}

RET_CODE status;
char *pname;
status = thLmachineGetParname(lmachine, i, &pname);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the input par type name for model %d", name, i);
	return(status);
	}

if (pname == NULL || strlen(pname) == 0) {
	if (fparam != NULL) {
	thError("%s: ERROR - detected null parameter type name but allocated memory (%d)", name, i);
	return(SH_GENERIC_ERROR);
	} else {
	return(SH_SUCCESS);
	}
}

thqDel(pname, fparam);

return(SH_SUCCESS);
}

RET_CODE thLExtractMiMj(LSTRUCT *lstruct, MLEFL ***mimj) {
char *name = "thLExtractMiMj";

if (lstruct == NULL) {
	thError("%s: ERROR - nule (lstruct)", name);
	return(SH_GENERIC_ERROR);
	}

LWORK *lwork;
if ((lwork = lstruct->lwork) == NULL) {
	thError("%s: ERROR - null (lwork) in (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
if (mimj != NULL) *mimj = lwork->mimj;
return(SH_SUCCESS);
}

RET_CODE thExtractBrackets(LSTRUCT *lstruct, 
	MLEFL **mid, MLEFL **midmm, 
	MLEFL ***mimj, MLEFL ***mijdmm, 
	MLEFL ****mimjk, MLEFL *****mijmkl) {
char *name = "thExtractBrackets";

if (lstruct == NULL) {
	thError("%s: ERROR - nule (lstruct)", name);
	return(SH_GENERIC_ERROR);
	}

LWORK *lwork;
if ((lwork = lstruct->lwork) == NULL) {
	thError("%s: ERROR - null (lwork) in (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
if (mid != NULL) *mid = lwork->mid;
if (midmm != NULL) *midmm = lwork->midmm;
if (mimj != NULL) *mimj = lwork->mimj;
#if OLD_MAP
if (mimjk != NULL) *mimjk = lwork->mimjk;
if (mijmkl != NULL) *mijmkl = lwork->mijmkl;
if (mijdmm != NULL) *mijdmm = lwork->mijdmm;
#else 
if (mimjk != NULL) *mimjk = NULL;
if (mijmkl != NULL) *mijmkl = NULL;
if (mijdmm != NULL) *mijdmm = NULL;
#endif



return(SH_SUCCESS);
}

RET_CODE thExtractMi(LSTRUCT *lstruct, int i, THREGION **Mi) {
char *name = "thExtractMi";
if (lstruct == NULL || lstruct->lwork == NULL || lstruct->lmodel == NULL || lstruct->lwork->mregN == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
	}

LMACHINE *lmachine;
int npar, nmodel, k;
RET_CODE status;
status = thLstructGetNModel(lstruct, &nmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract (nmodel) from (lstruct)", 
	name);
	return(status);
}
status = thLstructGetNPar(lstruct, &npar);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract (npar) from (lstruct)",
	name);
	return(status);
}
status = thLstructGetLmachine(lstruct, &lmachine);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract (lmachine) from (lstruct)", 
	name);
	return(status);
}
if (i < 0 || i >= nmodel) {
	thError("%s: ERROR - model number (%d) out of bound (0, %d)", name, i, nmodel - 1);
	return(SH_GENERIC_ERROR);
	}

LWORK *lwork;
lwork = lstruct->lwork;

if (Mi != NULL) {
	status = thMGetInModelMLocOfP(lmachine, i, -1, &k);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not locate image for model (%d)", 
		name, i);
		*Mi = NULL;
		return(status);
	}
	*Mi = lwork->mregN[k];
}
return(SH_SUCCESS);
}

RET_CODE thExtractMij(LSTRUCT *lstruct, int i, int j, THREGION **Mij) {
char *name = "thExtractMij";

LWORK *lwork;
LMODEL *lmodel;
THREGION **mregs;
if (lstruct == NULL || (lwork = lstruct->lwork) == NULL || 
	(lmodel = lstruct->lmodel) == NULL || (mregs = lstruct->lwork->mregN) == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
	}

RET_CODE status;
int nmodel, npar, k;
status = thLstructGetNModel(lstruct, &nmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract (nmodel) from (lstruct)", name);
	return(status);
	}
status = thLstructGetNPar(lstruct, &npar);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract (npar) from (lstruct)", name);
	return(status);
	}
if (i < 0 || i >= nmodel) {
	thError("%s: ERROR - model number (imodel = %d) out of bound (0, %d)", name, i, nmodel - 1);
	return(SH_GENERIC_ERROR);
	}
if (j < 0 || j >= npar) {
	thError("%s: ERROR - par number (j = %d) out of bound (0, %d)", name, j, npar - 1);
	return(SH_GENERIC_ERROR);
	}

LMACHINE *map;
status = thLstructGetLmachine(lstruct, &map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract (lmachine) from (lstruct)", name);
	return(SH_SUCCESS);
}
status = thMGetInModelMLocOfP(map, i, j, &k);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not locate parameter (%d) in model (%d)", name, j, i);
	return(status);
}

if (Mij != NULL) {
	if (k < 0) {
		*Mij = NULL;
	} else {
		*Mij = mregs[k];
	}
}

return(SH_SUCCESS);
}

RET_CODE thExtractMijk(LSTRUCT *lstruct, int i, int j, int k, THREGION **Mijk) {
char *name = "thExtractMijk";

LWORK *lwork;
LMODEL *lmodel;
THREGION **mregs;
if (lstruct == NULL || (lwork = lstruct->lwork) == NULL || 
	(lmodel = lstruct->lmodel) == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
	}

RET_CODE status;
int nmodel, npar, l;
status = thLstructGetNModel(lstruct, &nmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract (nmodel) from (lstruct)", name);
	return(status);
	}
status = thLstructGetNPar(lstruct, &npar);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract (npar) from (lstruct)", name);
	return(status);
	}
if ((mregs = lstruct->lwork->DDmregN) == NULL && npar != 0) {
	thError("%s: ERROR - null (DDmregN) found while (npar = %d)", name, npar);
	return(SH_GENERIC_ERROR);
}
if (i < 0 || i >= nmodel) {
	thError("%s: ERROR - model number (imodel = %d) out of bound (0, %d)", name, i, nmodel - 1);
	return(SH_GENERIC_ERROR);
	}
if (j < 0 || j >= npar) {
	thError("%s: ERROR - par number (j = %d) out of bound (0, %d)", name, j, npar - 1);
	return(SH_GENERIC_ERROR);
	}
if (k < 0 || k >= npar) {
	thError("%s: ERROR - par number (k = %d) out of bound (0, %d)", name, k, npar - 1);
	return(SH_GENERIC_ERROR);
	}
LMACHINE *map;
status = thLstructGetLmachine(lstruct, &map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract (lmachine) from (lstruct)", name);
	return(SH_SUCCESS);
}
int j2, k2;
int **invploc = map->invploc;
j2 = invploc[j][i];
k2 = invploc[k][i];
l = BAD_INDEX;
if (j2 != BAD_INDEX && k2 != BAD_INDEX) {
	status = thMGetInModelMLocOfPP(map, i, j2, k2, &l);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not locate parameter (%d, %d) in model (%d)", name, j,k,  i);
		return(status);
	}
}
if (Mijk != NULL) {
	if (l < 0) {
		*Mijk = NULL;
	} else {
		*Mijk = mregs[l];
	}
}

return(SH_SUCCESS);
}

RET_CODE thExtractD(LSTRUCT *lstruct, REGION **D) {
char *name = "thExtractD";
if (lstruct == NULL || lstruct->ldata == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
	}
if (D != NULL) *D = lstruct->ldata->image;
return(SH_SUCCESS);
}

RET_CODE thExtractM(LSTRUCT *lstruct, REGION **M) {
char *name = "thExtractM";
if (lstruct == NULL || lstruct->lmodel == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
	}
if (M != NULL) *M = lstruct->lwork->mN;
return(SH_SUCCESS);
}

RET_CODE thExtractDmm(LSTRUCT *lstruct, REGION **dmm) {
char *name = "thExtractDmm";
if (lstruct == NULL || lstruct->lwork == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
	}
if (dmm != NULL) *dmm = lstruct->lwork->dmmN;
return(SH_SUCCESS);
}

RET_CODE thExtractJtj(LSTRUCT *lstruct, LSECTOR sector, MLEFL ***jtj) { 
char *name = "thExtractJtj";
LWORK *lwork;
if (lstruct == NULL || (lwork = lstruct->lwork) == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
	}
if (jtj == NULL) {
	thError("%s: WARNING - null place holder for (jtj) - cannot pass value", name);
	return(SH_SUCCESS);
	}

if (sector == PSECTOR) {

if (lwork->jtj == NULL && lwork->np != 0) {
	thError("%s: WARNING - null (jtj) detected in (lstruct), suggesting improper allocation", name);
	if (lstruct->lmodel == NULL) {
	thError("%s: WARNING - null (lmodel) detected in (lstruct), suggesting improper allocation", name);
	} else {
	thError("%s: WARNING (continued) - npar = %d", name, lstruct->lmodel->npar);
	}
}
*jtj = lwork->jtj;

} else if (sector == ASECTOR || sector == APSECTOR) {

if (lwork->jtjap == NULL && lwork->nap != 0) {
	thError("%s: WARNING - null (jtj) detected in (lstruct), suggesting improper allocation", name);
	if (lstruct->lmodel == NULL) {
	thError("%s: WARNING - null (lmodel) detected in (lstruct), suggesting improper allocation", name);
	} else {
		if (sector == ASECTOR) {
			thError("%s: WARNING (continued) - (namp = %d)", name, lstruct->lmodel->namp);
		} else {
			thError("%s: WARNING (continued) - (nap = %d)", name, lstruct->lmodel->nap);
		}
	}	
}
*jtj = lwork->jtjap;

} else {
	thError("%s: ERROR - unsupported X-sector", name);
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE thLExtractJtDDm(LSTRUCT *lstruct, LSECTOR sector, MLEFL **jtddm) {
char *name = "thLExtractJtDDm";
LWORK *lwork;
if (lstruct == NULL || (lwork = lstruct->lwork) == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
if (jtddm == NULL) {
	thError("%s: WARNING - null place holder for (jtddm) - cannot pass value", name);
	return(SH_SUCCESS);
}

if (sector == PSECTOR) {

	if (lwork->jtddm == NULL) {
		thError("%s: WARNING - null (jtddm) detected in (lstruct), suggesting improper allocation", name);
		if (lstruct->lmodel == NULL) {
			thError("%s: WARNING - (continued) null (lmodel) detected in (lstruct), suggesting improper allocation", name);
		} else {
		thError("%s: WARNING - (continued) npar = %d", name, lstruct->lmodel->npar);
		}
	}
	*jtddm = lwork->jtddm;

} else if (sector == ASECTOR || sector == APSECTOR) {
	
	if (lwork->jtddmap == NULL) {
		thError("%s: WARNING - null (jtddmap) detected in (lstruct), suggesting improper allocation", name);
		if (lstruct->lmodel == NULL) {
			thError("%s: WARNING - (continued) null (lmodel) detected in (lstruct), suggesting improper allocation", name);
		} else {
		if (sector == APSECTOR) {
			thError("%s: WARNING - (continued) (nap = %d)", name, lstruct->lmodel->nap);
		} else {
			thError("%s: WARNING - (continued) (namp = %d)", name, lstruct->lmodel->namp);
		}
		}
	}
	*jtddm = lwork->jtddmap;
} else {

	thError("%s: ERROR - unsupported X-sector", name);
	return(SH_GENERIC_ERROR);
}


return(SH_SUCCESS);
}


RET_CODE thExtractJtdmm(LSTRUCT *lstruct, LSECTOR sector, MLEFL **jtdmm) {
char *name = "thExtractJtdmm";
LWORK *lwork;
if (lstruct == NULL || (lwork = lstruct->lwork) == NULL) {
	thError("%s: ERROR - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
if (jtdmm == NULL) {
	thError("%s: WARNING - null place holder for (jtdmm) - cannot pass value", name);
	return(SH_SUCCESS);
}

if (sector == PSECTOR) {

	if (lwork->jtdmm == NULL) {
		thError("%s: WARNING - null (jtdmm) detected in (lstruct), suggesting improper allocation", name);
		if (lstruct->lmodel == NULL) {
			thError("%s: WARNING - (continued) null (lmodel) detected in (lstruct), suggesting improper allocation", name);
		} else {
		thError("%s: WARNING - (continued) npar = %d", name, lstruct->lmodel->npar);
		}
	}
	*jtdmm = lwork->jtdmm;

} else if (sector == ASECTOR || sector == APSECTOR) {
	
	if (lwork->jtdmmap == NULL) {
		thError("%s: WARNING - null (jtdmmap) detected in (lstruct), suggesting improper allocation", name);
		if (lstruct->lmodel == NULL) {
			thError("%s: WARNING - (continued) null (lmodel) detected in (lstruct), suggesting improper allocation", name);
		} else {
		if (sector == APSECTOR) {
			thError("%s: WARNING - (continued) (nap = %d)", name, lstruct->lmodel->nap);
		} else {
			thError("%s: WARNING - (continued) (namp = %d)", name, lstruct->lmodel->namp);
		}
		}
	}
	*jtdmm = lwork->jtdmmap;
} else {

	thError("%s: ERROR - unsupported X-sector", name);
	return(SH_GENERIC_ERROR);
}


return(SH_SUCCESS);
}

#if OLD_MAP
#else

RET_CODE thLMijMklPut(LSTRUCT *lstruct, int i, int j, int k, int l, MLEFL x) {
char *name = "thLMijMklPut";

LWORK *lwork;
LMODEL *lmodel;
LMACHINE *map;
if (lstruct == NULL || (lwork = lstruct->lwork) == NULL || (lmodel = lstruct->lmodel) == NULL || (map = lmodel->lmachine) == NULL) {
	thError("%s: ERROR - null or improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}


int a, b;
MLEFL **mm;

RET_CODE status;
status = thMAccnmparGet(map, i, j + 1, &a);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not evaluate the combined index for (i = %d, j = %d)", name, i, j);
	return(status);
}
if (a == BAD_INDEX) {
	if (x == (MLEFL) 0) {
	#if DEEP_DEBUG_MLETYPES
	thError("%s: WARNING - combined index for (i = %d, j = %d) is non-existent, while (x = %g)", name, i, j, x);
	#endif
		return(SH_SUCCESS);
	} else {
	thError("%s: ERROR - combined index for (i = %d, j = %d) is non-existent, while (x = %g)", name, i, j, x);
		return(SH_GENERIC_ERROR);
	}
}
status = thMAccnmparGet(map, k, l + 1, &b);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not evaluate the combined index for (k = %d, l = %d)", name, k, l);
	return(status);
}
if (b == BAD_INDEX) {
	if (x == (MLEFL) 0) {
		#if DEEP_DEBUG_MLETYPES
		thError("%s: WARNING - combined index for (k = %d, l = %d) is non-existent, while (x = %g)", name, k, l, x);
		#endif
		return(SH_SUCCESS);
	} else {
		thError("%s: ERROR - combined index for (k = %d, l = %d) is non-existent, while (x = %g)", name, k, l, x);
		return(SH_GENERIC_ERROR);
	}
}
int nmm;
status = thLworkGetMM(lwork, &mm, &nmm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not obtain the bracket matrix < M_a | M_b >", name);
	return(status);
}
if (a >= nmm || a < 0) {
	thError("%s: ERROR - (i, j) = (%d, %d) is out of range for MijMk. index (a = %d) unacceptable when (nMM = %d)", name, i, j, a, nmm);
	return(SH_GENERIC_ERROR);
	}
if (b >= nmm || b < 0) {
	thError("%s: ERROR - (k, l)  = (%d, %d) is out of range for MijMk. index (b = %d) unacceptable when (nMM = %d)", name, k, l, b, nmm);
	return(SH_GENERIC_ERROR);
}


mm[a][b] = x;
return(SH_SUCCESS);
}

RET_CODE thLMijMklmPut(LSTRUCT *lstruct, int i, int j, int k, int l, int m, MLEFL x) {
char *name = "thLMijMklmPut";

LWORK *lwork;
LMODEL *lmodel;
LMACHINE *map;
if (lstruct == NULL || (lwork = lstruct->lwork) == NULL || (lmodel = lstruct->lmodel) == NULL || (map = lmodel->lmachine) == NULL) {
	thError("%s: ERROR - null or improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}


int a, b;
MLEFL **mDDm;

RET_CODE status;
status = thMAccnmparGet(map, i, j + 1, &a);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not evaluate the combined index for (i = %d, j = %d)", name, i, j);
	return(status);
}
if (a == BAD_INDEX) {
	if (x == (MLEFL) 0) {
	#if DEEP_DEBUG_MLETYPES
	thError("%s: WARNING - combined index for (i = %d, j = %d) is non-existent, while (x = %g)", name, i, j, x);
	#endif
		return(SH_SUCCESS);
	} else {
	thError("%s: ERROR - combined index for (i = %d, j = %d) is non-existent, while (x = %g)", name, i, j, x);
		return(SH_GENERIC_ERROR);
	}
}

int **invploc = map->invploc;
shAssert(invploc != NULL);
int l2 = invploc[l][k];
int m2 = invploc[m][k];
b = BAD_INDEX;

if (l2 != BAD_INDEX && m2 != BAD_INDEX) {
	status = thDDMAccnmparGet(map, k, l2, m2, &b);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not evaluate the combined index for (k = %d, l = %d, m = %d)", name, k, l, m);
		return(status);
	}
}

if (b == BAD_INDEX) {
	if (x == (MLEFL) 0) {
		#if DEEP_DEBUG_MLETYPES
		thError("%s: WARNING - combined index for (k = %d, l = %d) is non-existent, while (x = %g)", name, k, l, x);
		#endif
		return(SH_SUCCESS);
	} else {
		thError("%s: ERROR - combined index for (k,l,m)=(%d,%d,%d) is non-existent, while (x = %g)", name, k, l, m, x);
		return(SH_GENERIC_ERROR);
	}
}
int nDDm, nDm;
status = thLworkGetMddM(lwork, &mDDm, &nDm, &nDDm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not obtain the bracket matrix < M_a | ddM_b >", name);
	return(status);
}
if (a >= nDm || a < 0) {
	thError("%s: ERROR - (i, j) = (%d, %d) is out of range for MijMklm. index (a = %d) unacceptable when (nMM = %d)", name, i, j, a, nDm);
	return(SH_GENERIC_ERROR);
	}
if (b >= nDDm || b < 0) {
	thError("%s: ERROR - (k, l)  = (%d, %d) is out of range for MijMklm. index (b = %d) unacceptable when (nMM = %d)", name, k, l, b, nDDm);
	return(SH_GENERIC_ERROR);
}


mDDm[a][b] = x;
return(SH_SUCCESS);
}

RET_CODE thLMijMkPut(LSTRUCT *lstruct, int i, int j, int k, MLEFL x) {
char *name = "thLMijMkPut";

LWORK *lwork;
LMODEL *lmodel;
LMACHINE *map;
if (lstruct == NULL || (lwork = lstruct->lwork) == NULL || (lmodel = lstruct->lmodel) == NULL || (map = lmodel->lmachine) == NULL) {
	thError("%s: ERROR - null or improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}


int a, b, nmm;
MLEFL **mm;

RET_CODE status;
status = thMAccnmparGet(map, i, j + 1, &a);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not evaluate the combined index for (i = %d, j = %d)", name, i, j);
	return(status);
}
if (a == BAD_INDEX) {
	if (x == (MLEFL) 0) {
	#if DEEP_DEBUG_MLETYPES
	thError("%s: WARNING - combinded index for (i = %d, j = %d) is non-existent, while (x = %g)", name, i, j, x);
	#endif

		return(SH_SUCCESS);
	} else {
	thError("%s: ERROR - combinded index for (i = %d, j = %d) is non-existent, while (x = %g)", name, i, j, x);

		return(SH_GENERIC_ERROR);
	}
}
status = thMAccnmparGet(map, k, 0, &b);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not evaluate the combined index for (k = %d)", name, k);
	return(status);
}
if (b == BAD_INDEX) {
	if (x == (MLEFL) 0) {
#if DEEP_DEBUG_MLETYPES
	thError("%s: WARNING - combinded index for (k = %d) is non-existent, while  (x = %g)", name, k, x);
	#endif

	return(SH_SUCCESS);
	} else {
	thError("%s: ERROR - combinded index for (k = %d) is non-existent, while  (x = %g)", name, k, x);

	return(SH_GENERIC_ERROR);
	}
}

status = thLworkGetMM(lwork, &mm, &nmm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not obtain the bracket matrix < M_a | M_b >", name);
	return(status);
}
if (a >= nmm || a < 0) {
	thError("%s: ERROR - (i, j) = (%d, %d) is out of range for MijMk. index (a = %d) unacceptable when (nMM = %d)", name, i, j, a, nmm);
	return(SH_GENERIC_ERROR);
	}
if (b >= nmm || b < 0) {
	thError("%s: ERROR - (k = %d) is out of range for MijMk. index (b = %d) unacceptable when (nMM = %d)", name, k, b, nmm);
	return(SH_GENERIC_ERROR);
}

mm[a][b] = x;
return(SH_SUCCESS);
}


RET_CODE thLMijDmmPut(LSTRUCT *lstruct, int i, int j, MLEFL x) {
char *name = "thLMijDmmPut";

LWORK *lwork;
LMODEL *lmodel;
LMACHINE *map;
if (lstruct == NULL || (lwork = lstruct->lwork) == NULL || (lmodel = lstruct->lmodel) == NULL || (map = lmodel->lmachine) == NULL) {
	thError("%s: ERROR - null or improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}


int a;

RET_CODE status;
status = thMAccnmparGet(map, i, j + 1, &a);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not evaluate the combined index for (i = %d, j = %d)", name, i, j);
	return(status);
}
if (a == BAD_INDEX) {
		if (x == (MLEFL) 0) {
#if DEEP_DEBUG_MLETYPES
	thError("%s: WARNING - combined index for (i = %d, j = %d) is non-existent, while (x = %g)", name, i, j, x);
	#endif

	return(SH_SUCCESS);
	} else {
	thError("%s: ERROR - combined index for (i = %d, j = %d) is non-existent, while (x = %g)", name, i, j, x);

	return(SH_GENERIC_ERROR);
	}
}

MLEFL *mdmm;
status = thLworkGetMDmm(lwork, &mdmm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not obtain the bracket matrix < M_a | M_b >", name);
	return(status);
}
mdmm[a] = x;
return(SH_SUCCESS);
}

RET_CODE thLMijDPut(LSTRUCT *lstruct, int i, int j, MLEFL x) {
char *name = "thLMijDPut";

LWORK *lwork;
LMODEL *lmodel;
LMACHINE *map;
if (lstruct == NULL || (lwork = lstruct->lwork) == NULL || (lmodel = lstruct->lmodel) == NULL || (map = lmodel->lmachine) == NULL) {
	thError("%s: ERROR - null or improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}


int a;

RET_CODE status;
status = thMAccnmparGet(map, i, j + 1, &a);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not evaluate the combined index for (i = %d, j = %d)", name, i, j);
	return(status);
}
if (a == BAD_INDEX) {
	if (x == (MLEFL) 0) {
#if DEEP_DEBUG_MLETYPES
	thError("%s: WARNING - combined index for (i = %d, j = %d) is non-existent, while (x = %g)", name, i, j, x);
	#endif

		return(SH_SUCCESS);
	} else {
	thError("%s: ERROR - combined index for (i = %d, j = %d) is non-existent, while (x = %g)", name, i, j, x);

		return(SH_GENERIC_ERROR);
	}
}

MLEFL *md;
status = thLworkGetMD(lwork, &md);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not obtain the bracket matrix < M_a | M_b >", name);
	return(status);
}
md[a] = x;
return(SH_SUCCESS);
}

RET_CODE thLDDPut(LSTRUCT *lstruct, MLEFL x) {
char *name = "thLDDPut";

LWORK *lwork;
if (lstruct == NULL || (lwork = lstruct->lwork) == NULL) { 
	thError("%s: ERROR - null or improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
lwork->dd = x;
return(SH_SUCCESS);
}

RET_CODE thLMijMklmGet(LSTRUCT *lstruct, int i, int j, int k, int l, int m, MLEFL *x) {
char *name = "thLMijMklmGet";

LWORK *lwork;
LMODEL *lmodel;
LMACHINE *map;
if (lstruct == NULL || (lwork = lstruct->lwork) == NULL || (lmodel = lstruct->lmodel) == NULL || (map = lmodel->lmachine) == NULL) {
	if (x != NULL) *x = MLEFLNAN;
	thError("%s: ERROR - null or improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}


int a = BAD_INDEX, b = BAD_INDEX;
MLEFL **mDDm;

RET_CODE status;
status = thMAccnmparGet(map, i, j + 1, &a);
if (status != SH_SUCCESS) {
	if (x != NULL) *x = MLEFLNAN;
	thError("%s: ERROR - could not evaluate the combined index for (i = %d, j = %d)", name, i, j);
	return(status);
}
if (a == BAD_INDEX) {
	if (x != NULL) *x = (MLEFL) 0.0;
	return(SH_SUCCESS);
}
int **invploc = map->invploc;
int l2 = invploc[l][k];
int m2 = invploc[m][k];
if (l2 != BAD_INDEX && m2 != BAD_INDEX) {
	status = thDDMAccnmparGet(map, k, l2, m2, &b);
	if (status != SH_SUCCESS) {
		if (x != NULL) *x = MLEFLNAN;
		thError("%s: ERROR - could not evaluate the combined index for (k = %d, l = %d, m = %d)", name, k, l, m);
		return(status);
	}
}	
if (b == BAD_INDEX) {
	if (x != NULL) *x = (MLEFL) 0.0;
	return(SH_SUCCESS);
}
int nDDm, nDm;
status = thLworkGetMddM(lwork, &mDDm, &nDm, &nDDm);
if (status != SH_SUCCESS) {
	if (x != NULL) *x = MLEFLNAN;
	thError("%s: ERROR - could not obtain the bracket matrix < M_a | ddM_b >", name);
	return(status);
}
if (a >= nDm || a < 0) {
	thError("%s: ERROR - (i, j) = (%d, %d) is out of range for MijMk. index (a = %d) unacceptable when (nMM = %d)", name, i, j, a, nDm);
	if (x != NULL) *x = MLEFLNAN;
	return(SH_GENERIC_ERROR);
	}
if (b >= nDDm || b < 0) {
	if (x != NULL) *x = MLEFLNAN;
	thError("%s: ERROR - (k, l)  = (%d, %d) is out of range for MijMk. index (b = %d) unacceptable when (nMM = %d)", name, k, l, b, nDDm);
	return(SH_GENERIC_ERROR);
}

if (x != NULL) *x = (MLEFL) (mDDm[a][b]);

return(SH_SUCCESS);
}

RET_CODE thLMijMklGet(LSTRUCT *lstruct, int i, int j, int k, int l, MLEFL *x) {
char *name = "thLMijMklGet";

LWORK *lwork;
LMODEL *lmodel;
LMACHINE *map;
if (lstruct == NULL || (lwork = lstruct->lwork) == NULL || (lmodel = lstruct->lmodel) == NULL || (map = lmodel->lmachine) == NULL) {
	thError("%s: ERROR - null or improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}


int a, b;
MLEFL **mm;

RET_CODE status;
status = thMAccnmparGet(map, i, j + 1, &a);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not evaluate the combined index for (i = %d, j = %d)", name, i, j);
	return(status);
}
if (a == BAD_INDEX) {
	*x = 0.0;
	return(SH_SUCCESS);
}

status = thMAccnmparGet(map, k, l + 1, &b);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not evaluate the combined index for (k = %d, l = %d)", name, k, l);
	return(status);
}
if (b == BAD_INDEX) {
	*x = 0.0;
	return(SH_SUCCESS);
}

int nmm;
status = thLworkGetMM(lwork, &mm, &nmm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not obtain the bracket matrix < M_a | M_b >", name);
	return(status);
}
if (a >= nmm || a < 0) {
	thError("%s: ERROR - (i, j) = (%d, %d) is out of range for MijMk. index (a = %d) unacceptable when (nMM = %d)", name, i, j, a, nmm);
	return(SH_GENERIC_ERROR);
	}
if (b >= nmm || b < 0) {
	thError("%s: ERROR - (k, l) = (%d, %d) is out of range for MijMk. index (b = %d) unacceptable when (nMM = %d)", name, k, l, b, nmm);
	return(SH_GENERIC_ERROR);
}
*x = mm[a][b];
return(SH_SUCCESS);
}

RET_CODE thLMijMkGet(LSTRUCT *lstruct, int i, int j, int k, MLEFL *x) {
char *name = "thLMijMkGet";

LWORK *lwork;
LMODEL *lmodel;
LMACHINE *map;
if (lstruct == NULL || (lwork = lstruct->lwork) == NULL || (lmodel = lstruct->lmodel) == NULL || (map = lmodel->lmachine) == NULL) {
	thError("%s: ERROR - null or improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}


int a, b;
MLEFL **mm;

RET_CODE status;
status = thMAccnmparGet(map, i, j + 1, &a);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not evaluate the combined index for (i = %d, j = %d)", name, i, j);
	return(status);
}
if (a == BAD_INDEX) {
	#if VERY_DEEP_DEBUG_MLETYPES
	thError("%s: WARNING - entry (i = %d, j = %d, k = %d) does not exist in MM matrix", name, i, j, k);
	#endif
	*x = (MLEFL) 0.0;
	return(SH_SUCCESS);
} else {
	#if VERY_DEEP_DEBUG_MLETYPES
	printf("%s: entry (i = %2d, j = %2d, k = %2d), (a = %2d) \n", name, i, j, k, a);
	#endif
}

status = thMAccnmparGet(map, k, 0, &b);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not evaluate the combined index for (k = %d)", name, k);
	return(status);
}
if (b == BAD_INDEX) {
	*x = (MLEFL) 0.0;
	return(SH_SUCCESS);
}

int nmm;
status = thLworkGetMM(lwork, &mm, &nmm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not obtain the bracket matrix < M_a | M_b >", name);
	return(status);
}
if (a >= nmm || a < 0) {
	thError("%s: ERROR - (i, j) = (%d, %d) is out of range for MijMk. index (a = %d) unacceptable when (nMM = %d)", name, i, j, a, nmm);
	return(SH_GENERIC_ERROR);
	}
if (b >= nmm || b < 0) {
	thError("%s: ERROR - (k = %d) is out of range for MijMk. index (b = %d) unacceptable when (nMM = %d)", name, k, b, nmm);
	return(SH_GENERIC_ERROR);
}

*x = mm[a][b];
return(SH_SUCCESS);
}

RET_CODE thLMijDmmGet(LSTRUCT *lstruct, int i, int j, MLEFL *x) {
char *name = "thLMijDmmGet";

LWORK *lwork;
LMODEL *lmodel;
LMACHINE *map;
if (lstruct == NULL || (lwork = lstruct->lwork) == NULL || (lmodel = lstruct->lmodel) == NULL || (map = lmodel->lmachine) == NULL) {
	thError("%s: ERROR - null or improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}


int a;

RET_CODE status;
status = thMAccnmparGet(map, i, j + 1, &a);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not evaluate the combined index for (i = %d, j = %d)", name, i, j);
	return(status);
}
if (a == BAD_INDEX) {
	*x = (MLEFL) 0.0;
} else {
	MLEFL *mdmm;
	status = thLworkGetMDmm(lwork, &mdmm);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not obtain the bracket matrix < M_a | M_b >", name);
		return(status);
	}
	*x = mdmm[a];
}
return(SH_SUCCESS);
}

RET_CODE thLMijDGet(LSTRUCT *lstruct, int i, int j, MLEFL *x) {
char *name = "thLMijDGet";

LWORK *lwork;
LMODEL *lmodel;
LMACHINE *map;
if (lstruct == NULL || (lwork = lstruct->lwork) == NULL || (lmodel = lstruct->lmodel) == NULL || (map = lmodel->lmachine) == NULL) {
	thError("%s: ERROR - null or improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}


int a;

RET_CODE status;
status = thMAccnmparGet(map, i, j + 1, &a);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not evaluate the combined index for (i = %d, j = %d)", name, i, j);
	return(status);
}
if (a == BAD_INDEX) {
	*x = (MLEFL) 0.0;
} else {
	MLEFL *md;
	status = thLworkGetMD(lwork, &md);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not obtain the bracket matrix < M_a | M_b >", name);
		return(status);
	}
	*x = md[a];
}

return(SH_SUCCESS);
}



#endif

RET_CODE thLDDGet(LSTRUCT *lstruct, MLEFL *x) {
char *name = "thLDDGet";
if (x == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (lstruct == NULL || lstruct->lwork == NULL) {
	thError("%s: ERROR - null or improperly allocated (lstruct)", name);
	*x = (MLEFL) 1.0 / (MLEFL) 0.0;
	return(SH_GENERIC_ERROR);
}
*x = lstruct->lwork->dd;
return(SH_SUCCESS);
}

/* a new piece needs to be written to include mask_a, mask_b */
MLEFL thBracket(REGION *a, LWMASK *lwmask, REGION *b, RET_CODE *status) {
char *name = "thBracket";

#if THCLOCK
static int init_clk = 0;
static clock_t clk1, clk2;
if (!init_clk) clk1 = clock();
init_clk++;
#endif

RET_CODE status2 = SH_SUCCESS;
if (a == NULL || b == NULL) {
	char *err;
	if (a == NULL && b == NULL) {
		err = "a, b";
	} else if (a == NULL) {
		err = "a";
	} else {
		err = "b";
	}
	thError("%s: ERROR - null input (%s), no calculation to be done", name, err);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(MLEFLNAN);
}

if (a->type != TYPE_THPIX || b->type != TYPE_THPIX) {
	thError("%s: ERROR - only (TYPE_THPIX) pixel type is supported", name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
        return(MLEFLNAN);
}       

REGION *W = NULL;
register THPIX *wrow = NULL;
int wnrow, wncol, wrow0, wcol0, awrow0, awcol0;
THMASK *mask = NULL;
if (lwmask != NULL) {
	status2 = thLwmaskGetWActive(lwmask, &W);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not get (W_active) from (lwmask)", name);
		if (status != NULL) *status = status2;
		return(MLEFLNAN);
	}

	status2 = thLwmaskGetMaskActive(lwmask, &mask);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not get (mask_active) from (lwmask)", name);
		if (status != NULL) *status = status2;
		return(MLEFLNAN);
	}
}
int mnrow, mncol, mrow0, mcol0, amrow0, amcol0, npix = 0;

#if DEBUG_BRACKET
printf("%s: lwmask (= %p), weight (= %p), mask (= %p) passed \n", name, (void *) lwmask, (void *) W, (void *) mask);
#endif

int anrow, ancol, bnrow, bncol;
anrow = a->nrow;
ancol = a->ncol;
bnrow = b->nrow;
bncol = b->ncol;

int arow0, acol0, brow0, bcol0, abrow0, abcol0;
arow0 = a->row0;
acol0 = a->col0;
brow0 = b->row0;
bcol0 = b->col0;
abrow0 = arow0 - brow0;
abcol0 = acol0 - bcol0;

int i1, i2, j1, j2;
i1 = MAX(0, - abrow0);
i2 = MIN(anrow, bnrow - abrow0);
j1 = MAX(0, - abcol0);
j2 = MIN(ancol, bncol - abcol0);

if (i2 <= i1 || j2 <= j1) {
	#if DEEP_DEBUG_MLETYPES
	thError("%s: WARNING - empty overlap", name);
	#endif
	if (status != NULL) *status = SH_SUCCESS;
	return((MLEFL) 0.0);
}
int i, j;

register THPIX *arow = NULL, *brow = NULL;
MLEFL x = (MLEFL) 0.0;


#if DO_KAHAN_SUM
	#if USE_KAHAN_PACKAGE
	KAHANFL yy_item = 0.0;
	thKahanZero();
	#else
	MLEFL x_error = (MLEFL) 0.0;
	#endif
#endif
if (lwmask == NULL || (mask == NULL && W == NULL)) {
	#if DO_KAHAN_SUM
		#if USE_KAHAN_PACKAGE
		KAHANFL y = 0.0;
		#else
		register BRACKETFL y = 0.0;
		#endif
	#else
		register BRACKETFL y = 0.0;
	#endif

	#if DO_KAHAN_SUM
		#if USE_KAHAN_PACKAGE
		#else
		register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
		#endif
	for (i = i1; i < i2; i++) {
		arow = a->rows_thpix[i];
		brow = b->rows_thpix[i + abrow0] + abcol0;
		for (j = j1; j < j2; j++) {
			#if USE_KAHAN_PACKAGE
				#if USE_KAHAN_PACKAGE2
				thKahanAddDot2((KAHANFL) arow[j], (KAHANFL) brow[j]);
				#else
				yy_item =  (KAHANFL) arow[j] * (KAHANFL) brow[j];
				thKahanAdd(yy_item);
				#endif
			#else
			y_item = (BRACKETFL) arow[j] * (BRACKETFL) brow[j] - y_error;
			y2 = y + y_item;
			y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
			y = y2;	
			#endif
		}
	}
		#if USE_KAHAN_PACKAGE
		thKahanGet(NULL, NULL, &y);
		#else
		x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
		#endif
	#else
	for (i = i1; i < i2; i++) {
		arow = a->rows_thpix[i];
		brow = b->rows_thpix[i + abrow0] + abcol0;
		for (j = j1; j < j2; j = j+4) {
			y += (BRACKETFL) arow[j] * (BRACKETFL) brow[j];
			y += (BRACKETFL) arow[j+2] * (BRACKETFL) brow[j+2];	
			y += (BRACKETFL) arow[j+1] * (BRACKETFL) brow[j+1];
			y += (BRACKETFL) arow[j+3] * (BRACKETFL) brow[j+3];
		}
		for (j = j1 + ((j2 - j1)/4) * 4; j < j2; j++) {
			y += (BRACKETFL) arow[j] * (BRACKETFL) brow[j];
		}
	}
	#endif
	if (isnan(y) || isinf(y)) {
		thError("%s: ERROR - (NAN) detected in bracket sum #b#007", name);
		if (status != NULL) *status = SH_GENERIC_ERROR;
	} else {
		if (status != NULL) *status = SH_SUCCESS;
	}
	x = (MLEFL) y;
	if (isnan(x) || isinf(x)) {	
		thError("%s: ERROR - (NAN) detected after type conversion (before = %Lg, after = %Lg)", name, (long double) y, (long double) x);
		if (status != NULL) *status = SH_GENERIC_ERROR;
	}
	#if (DO_KAHAN_SUM & USE_KAHAN_PACKAGE)
	thKahanZero();
	#endif
	return(x);
}

if (W == NULL) {
	thError("%s: WARNING - empty weight region (W) - assuming 1", name);
}

if (W != NULL) {

	if (W->type != TYPE_THPIX) {
		thError("%s: ERROR - weight matrix (W) should be of type (TYPE_THPIX)", name);
 	        return(MLEFLNAN);
        	}
	wnrow = W->nrow;
	wncol = W->ncol;

	wrow0 = W->row0;
	wcol0 = W->col0;
	awrow0 = arow0 - wrow0;
	awcol0 = acol0 - wcol0;

	i1 = MAX(i1, -awrow0);
	i2 = MIN(i2, wnrow - awrow0);
	j1 = MAX(j1, -awcol0);
	j2 = MIN(j2, wncol - awcol0);

	if (i2 <= i1 || j2 <= j1) {
		#if DEEP_DEBUG_MLETYPES
		thError("%s: WARNING - empty overlap", name);
		#endif
		if (status != NULL) *status = SH_SUCCESS;
		return((MLEFL) 0.0);
	}

}

if (mask != NULL) {

	#if THMASKisMASK

	register MASKBIT *mrow = NULL;
	register MASKBIT maskbit = 0x0;
	MASKBIT maskbit_var = 0x0;
	status2 = thLwmaskGetMaskbit(lwmask, &maskbit_var);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not get (maskbit) from (lwmask)", name);
		if (status != NULL) *status = status2;
		return(MLEFLNAN);
	}
	maskbit = maskbit_var;
	mnrow = mask->nrow;
	mncol = mask->ncol;

	mrow0 = mask->row0;
	mcol0 = mask->col0;
	amrow0 = arow0 - mrow0;
	amcol0 = acol0 - mcol0;

	i1 = MAX(i1, -amrow0);
	i2 = MIN(i2, mnrow - amrow0);
	j1 = MAX(j1, -amcol0);
	j2 = MIN(j2, mncol - amcol0);

	if (i2 <= i1 || j2 <= j1) {
		#if DEEP_DEBUG_MLETYPES
		thError("%s: WARNING - empty overlap", name);
		#endif
		if (status != NULL) *status = SH_SUCCESS;
		return((MLEFL) 0.0);
	}

	if (W == NULL) {
		#if DO_KAHAN_SUM
			#if USE_KAHAN_PACKAGE
			KAHANFL y = 0.0;
			#else
			register BRACKETFL y = 0.0;
			#endif
		#else
			register BRACKETFL y = 0.0;
		#endif
		#if DO_KAHAN_SUM
			#if USE_KAHAN_PACKAGE
			#else
			register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
			#endif
		for (i = i1; i < i2; i++) {
			arow = a->rows_thpix[i];
			brow = b->rows_thpix[i + abrow0] + abcol0;
			mrow = mask->rows[i + amrow0] + amcol0;
			for (j = j1; j < j2; j++) {
				if (mrow[j] & maskbit) {
					#if USE_KAHAN_PACKAGE
						#if USE_KAHAN_PACKAGE2
							thKahanAddDot2((KAHANFL) arow[j], (KAHANFL) brow[j]);
						#else
							yy_item = (KAHANFL) arow[j] * (KAHANFL) brow[j];
							thKahanAdd(yy_item);
						#endif
					#else
						y_item = (BRACKETFL) arow[j] * (BRACKETFL) brow[j] - y_error;
						y2 = y + y_item;
						y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
						y = y2;
					#endif	
					npix++;
				}		
			}
		}
			#if USE_KAHAN_PACKAGE
			thKahanGet(NULL, NULL, &y);
			#else
			x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
			#endif
		#else
		for (i = i1; i < i2; i++) {
                	arow = a->rows_thpix[i];
                	brow = b->rows_thpix[i + abrow0] + abcol0;
			mrow = mask->rows[i + amrow0] + amcol0;
                	for (j = j1; j + 3 < j2; j=j+4) {
				if (mrow[j] & maskbit) {
                        		y += (BRACKETFL) arow[j] * (BRACKETFL) brow[j];
					npix++;
				}
				if (mrow[j+2] & maskbit) {
                        		y += (BRACKETFL) arow[j+2] * (BRACKETFL) brow[j+2];
					npix++;
				}
				if (mrow[j+1] & maskbit) {
                        		y += (BRACKETFL) arow[j+1] * (BRACKETFL) brow[j+1];
					npix++;
				}
				if (mrow[j+3] & maskbit) {
                        		y += (BRACKETFL) arow[j+3] * (BRACKETFL) brow[j+3];
					npix++;
				}
                	}
			for (j = j1 + ((j2 - j1) / 4) * 4; j < j2; j++) {
				if (mrow[j] & maskbit) {
                        		y += (BRACKETFL) arow[j] * (BRACKETFL) brow[j];
					npix++;
				}
			}
		}
		#endif
		if (isnan(y) || isinf(y)) {
			thError("%s: ERROR - (NAN) detected in bracket sum #b#008", name);
		}
		x = (MLEFL) y;
	} else {
		
		#if DO_KAHAN_SUM
			#if USE_KAHAN_PACKAGE
			KAHANFL y = 0.0;
			#else
			register BRACKETFL y = 0.0;
			#endif
		#else
			register BRACKETFL y = 0.0;
		#endif
		#if DO_KAHAN_SUM	
			#if USE_KAHAN_PACKAGE
			#else
			register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
			#endif
		for (i = i1; i < i2; i++) {
			arow = a->rows_thpix[i];
			brow = b->rows_thpix[i + abrow0] + abcol0;
			wrow = W->rows_thpix[i + awrow0] + awcol0;
			mrow = mask->rows[i + amrow0] + amcol0;
			for (j = j1; j < j2; j++) {
				if (mrow[j] & maskbit) {
					#if USE_KAHAN_PACKAGE
						#if USE_KAHAN_PACKAGE2
							thKahanAddDot3(arow[j], (KAHANFL) brow[j], (KAHANFL) wrow[j]);
						#else
							yy_item = (KAHANFL) arow[j] * (KAHANFL) brow[j] * (KAHANFL) wrow[j];
							thKahanAdd(yy_item);
						#endif
					#else
						y_item = (BRACKETFL) arow[j] * (BRACKETFL) brow[j] * (BRACKETFL) wrow[j] - y_error;
						y2 = y + y_item;
						y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
						y = y2;
					#endif	
					npix++;
				}		
			}
		}
			#if USE_KAHAN_PACKAGE
			thKahanGet(NULL, NULL, &y);
			#else
			x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
			#endif
		#else
		for (i = i1; i < i2; i++) {
                	arow = a->rows_thpix[i];
                	brow = b->rows_thpix[i + abrow0] + abcol0;
			wrow = W->rows_thpix[i + awrow0] + awcol0;
			mrow = mask->rows[i + amrow0] + amcol0;
                	for (j = j1; j + 3 < j2; j=j+4) {
				if (mrow[j] & maskbit) {
                        		y += (BRACKETFL) arow[j] * (BRACKETFL) brow[j] * (BRACKETFL) wrow[j];
					npix++;
				}
				if (mrow[j+2] & maskbit) {
                        		y += (BRACKETFL) arow[j+2] * (BRACKETFL) brow[j+2] * (BRACKETFL) wrow[j+2];
					npix++;
				}
				if (mrow[j+1] & maskbit) {
                        		y += (BRACKETFL) arow[j+1] * (BRACKETFL) brow[j+1] * (BRACKETFL) wrow[j+1];
					npix++;
				}
				if (mrow[j+3] & maskbit) {
                        		y += (BRACKETFL) arow[j+3] * (BRACKETFL) brow[j+3] * (BRACKETFL) wrow[j+3];
					npix++;
				}
                	}
			for (j = j1 + ((j2 - j1) / 4) * 4; j < j2; j++) {
				if (mrow[j] & maskbit) {
                        		y += (BRACKETFL) arow[j] * (BRACKETFL) brow[j] * (BRACKETFL) wrow[j];
					npix++;
				}
			}	
	        }
		#endif
		if (isnan(y) || isinf(y)) {
			thError("%s: ERROR - (NAN) detected in bracket #b#001", name);
			if (status != NULL) *status = SH_GENERIC_ERROR;
		}
		 x = (MLEFL) y;
	}

	#if DEEP_DEBUG_MLETYPES
	if (npix == 0) {
		thError("%s: WARNING - empty overlap", name);
	}
	#endif
	if (isnan(x) || isinf(x)) {
		thError("%s: ERROR - (NAN) detected in bracket #b#002", name);
		if (status != NULL) *status = SH_GENERIC_ERROR;
	} else if (status != NULL) {
		*status = SH_SUCCESS;
	}
	#if (DO_KAHAN_SUM & USE_KAHAN_PACKAGE)
	thKahanZero();
	#endif
	return(x); 
     	
	#endif

	#if THMASKisSUPERMASK

	register U16 *mrow = NULL;
	register MASKBIT maskbit = 0x0;
	status = thLwmaskGetMaskbit(lwmask, &maskbit);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (maskbit) from (lwmask)", name);
		return(status);
	}

	mnrow = mask->nrow;
	mncol = mask->ncol;

	mrow0 = mask->row0;
	mcol0 = mask->col0;
	amrow0 = arow0 - mrow0;
	amcol0 = acol0 - mcol0;

	i1 = MAX(i1, -amrow0);
	i2 = MIN(i2, mnrow - amrow0);
	j1 = MAX(j1, -amcol0);
	j2 = MIN(j2, mncol - amcol0);

	if (i2 <= i1 || j2 <= j1) {
		#if DEEP_DEBUG_MLETYPES
		thError("%s: WARNING - empty overlap", name);
		#endif
		if (status != NULL) *status = SH_SUCCESS;
		return((MLEFL) 0.0);
	}

	if (W == NULL) {
		#if DO_KAHAN_SUM
			#if USE_KAHAN_PACKAGE
			KAHANFL y = 0.0;
			#else
			register BRACKETFL y = 0.0;
			#endif
		#else
			register BRACKETFL y = 0.0;
		#endif
		#if DO_KAHAN_SUM
		register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
		for (i = i1; i < i2; i++) {
			arow = a->rows_thpix[i];
			brow = b->rows_thpix[i + abrow0] + abcol0;
			mrow = mask->rows_u16[i + amrow0] + amcol0;
			for (j = j1; j < j2; j++) {
				if (mrow[j] & maskbit) {
					#if USE_KAHAN_PACKAGE
						#if USE_KAHAN_PACKAGE2
						thKahanAddDot2((KAHANFL) arow[j], (KAHANFL) brow[j]);
						#else
						yy_item = (KAHANFL) arow[j] * (KAHANFL) brow[j];
						thKahanAdd(yy_item);
						#endif
					#else
					y_item = (BRACKETFL) arow[j] * (BRACKETFL) brow[j] - y_error;
					y2 = y + y_item;
					y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
					y = y2;	
					#endif
					npix++;
				}		
			}
		}
		#if USE_KAHAN_PACKAGE
		thKahanGet(NULL, NULL, &y);
		#endif
		x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
		#else
		for (i = i1; i < i2; i++) {
                	arow = a->rows_thpix[i];
                	brow = b->rows_thpix[i + abrow0] + abcol0;
			mrow = mask->rows_u16[i + amrow0] + amcol0;
                	for (j = j1; j + 3 < j2; j=j+4) {
				if (mrow[j] & maskbit) {
                        		y += (BRACKETFL) arow[j] * (BRACKETFL) brow[j];
					npix++;
				}
				if (mrow[j+2] & maskbit) {
                        		y += (BRACKETFL) arow[j+2] * (BRACKETFL) brow[j+2];
					npix++;
				}
				if (mrow[j+1] & maskbit) {
                        		y += (BRACKETFL) arow[j+1] * (BRACKETFL) brow[j+1];
					npix++;
				}
				if (mrow[j+3] & maskbit) {
                        		y += (BRACKETFL) arow[j+3] * (BRACKETFL) brow[j+3];
					npix++;
				}
                	}
			for (j = j1 + ((j2 - j1) / 4) * 4; j < j2; j++) {
				if (mrow[j] & maskbit) {
                        		y += (BRACKETFL) arow[j] * (BRACKETFL) brow[j];
					npix++;
				}
			}
			
	        }
		#endif
		if (isnan(y) || isinf(y)) {
			thError("%s: ERROR - (NAN) detected in bracket sum #b#009", name);
		}
		x = (MLEFL) y;
	} else {
		#if DO_KAHAN_SUM
			#if USE_KAHAN_PACKAGE
			KAHANFL y = 0.0;
			#else
			register BRACKETFL y = 0.0;
			#endif
		#else
			register BRACKETFL y = 0.0;
		#endif
		#if DO_KAHAN_SUM
		register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
		for (i = i1; i < i2; i++) {
			arow = a->rows_thpix[i];
			brow = b->rows_thpix[i + abrow0] + abcol0;
			wrow = W->rows_thpix[i + awrow0] + awcol0;
			mrow = mask->rows_u16[i + amrow0] + amcol0;
			for (j = j1; j < j2; j++) {
				if (mrow[j] & maskbit) {
					#if USE_KAHAN_PACKAGE
						#if USE_KAHAN_PACKAGE2
						thKahanAddDot2((KAHANFL) arow[j], (KAHANFL) brow[j]);
						#else
						yy_item = (KAHANFL) arow[j] * (KAHANFL) brow[j];
						thKahanAdd(yy_item);
						#endif
					#else
					y_item = (BRACKETFL) arow[j] * (BRACKETFL) brow[j] - y_error;
					y2 = y + y_item;
					y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
					y = y2;	
					#endif
					npix++;
				}		
			}
		}
		#if USE_KAHAN_PACKAGE
		thKahanGet(NULL, NULL, &y);
		#endif
		x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
		#else
		for (i = i1; i < i2; i++) {
                	arow = a->rows_thpix[i];
                	brow = b->rows_thpix[i + abrow0] + abcol0;
			wrow = W->rows_thpix[i + awrow0] + awcol0;
			mrow = mask->rows_u16[i + amrow0] + amcol0;
                	for (j = j1; j + 3  < j2; j=j+4) {
				if (mrow[j] & maskbit) {
                        		y += (BRACKETFL) arow[j] * (BRACKETFL) brow[j] * (BRACKETFL) wrow[j];
					npix++;
				}
				if (mrow[j+2] & maskbit) {
                        		y += (BRACKETFL) arow[j+2] * (BRACKETFL) brow[j+2] * (BRACKETFL) wrow[j+2];
					npix++;
				}
				if (mrow[j+1] & maskbit) {
                        		y += (BRACKETFL) arow[j+1] * (BRACKETFL) brow[j+1] * (BRACKETFL) wrow[j+1];
					npix++;
				}
				if (mrow[j+3] & maskbit) {
                        		y += (BRACKETFL) arow[j+3] * (BRACKETFL) brow[j+3] * (BRACKETFL) wrow[j+3];
					npix++;
				}
                	}
			for (j = j1 + ((j2 - j1) / 4) * 4; j < j2; j++) {
				if (mrow[j] & maskbit) {
                        		y += (BRACKETFL) arow[j] * (BRACKETFL) brow[j] * (BRACKETFL) wrow[j];
					npix++;
				}
			}

	        }
		#endif
		if (isnan(y) || isinf(y)) {
			thError("%s: ERROR - (NAN) detected in bracket sum #b#010", name);
		}
		x = (MLEFL) y;
	}

	#if DEEP_DEBUG_MLETYPES
	if (npix == 0) {
		thError("%s: WARNING - empty overlap", name);
	}
	#endif

	if (isinf(x) || isnan(x)) {
		thError("%s: ERROR - (NAN) detected in bracket value #b#003", name);
		if (status != NULL) *status = SH_GENERIC_ERROR;
	} else if (status != NULL) {
		*status = SH_SUCCESS;
	}
	thKhanZero();
	return(x); 
 
	#endif

	#if THMASKisOBJMASK
	
	mrow0 = mask->row0;
	mcol0 = mask->col0;
	amrow0 = arow0 - mrow0;
	amcol0 = acol0 - mcol0;

	int nspan;
	SPAN *s;
	s = mask->s;
	nspan = mask->nspan;
	if (mask->npix == 0) {
		thError("%s: WARNING - empty (objmask)", name);
		return((MLEFL) 0.0);
	}

	int x1, x2;
	if (W == NULL) {
		#if DO_KAHAN_SUM
			#if USE_KAHAN_PACKAGE
			KAHANFL y = 0.0;
			#else
			register BRACKETFL y = 0.0;
			#endif
		#else
			register BRACKETFL y = 0.0;
		#endif
		#if DO_KAHAN_SUM
		register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
		for (ispan = 0; ispan < nspan; ispan++) {
			i = s->y - amrow0;
			x1 = MAX(j1, s->x1 - amcol0);
			x2 = MIN(j2, 1 + s->x2 - amcol0);
			if (i >= i1 && i < i2) {
				arow = a->rows_thpix[i];
				brow = b->rows_thpix[i + abrow0] + abcol0;
				for (j = x1; j < x2; j++) {
					#if USE_KAHAN_PACKAGE
						#if USE_KAHAN_PACKAGE2
						thKahanAddDot2((KAHANFL) arow[j], (KAHANFL) brow[j]);
						#else
						yy_item = (KAHANFL) arow[j] * (KAHANFL) brow[j];
						thKahanAdd(yy_item);
						#endif
					#else
					y_item = (BRACKETFL) arow[j] * (BRACKETFL) brow[j] - y_error;
					y2 = y + y_item;
					y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
					y = y2;
					#endif	
					npix++;
				}		
				npix += MAX(x2 - x1, 0);
			}
		}
		#if USE_KAHAN_PACKAGE
		thKahanGet(NULL, NULL, &y);
		#endif
		x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
		#else
		for (ispan = 0; ispan < nspan; ispan++) {
			i = s->y - amrow0;
			x1 = MAX(j1, s->x1 - amcol0);
			x2 = MIN(j2, 1 + s->x2 - amcol0);
			if (i >= i1 && i < i2) {
				arow = a->rows_thpix[i];
				brow = b->rows_thpix[i + abrow0] + abcol0;
				for (j = x1; j + 3 < x2; j=j+4) {
				y += (BRACKETFL) arow[j] * (BRACKETFL) brow[j];
				y += (BRACKETFL) arow[j+2] * (BRACKETFL) brow[j+2];	
				y += (BRACKETFL) arow[j+1] * (BRACKETFL) brow[j+1];
				y += (BRACKETFL) arow[j+3] * (BRACKETFL) brow[j+3];
				}
				for (j = x1 + ((x2 - x1) / 4) * 4; j < x2; j++) {
				y += (BRACKETFL) arow[j] * (BRACKETFL) brow[j];
				}
				npix += MAX(x2 - x1, 0);
			}	
		}
		#endif
		if (isnan(y) || isinf(y)) {
			thError("%s: ERROR - (NAN) detected in bracket sum #b#011", name);
		}
		x = (MLEFL) y;
		#if DEEP_DEBUG_MLETYPES
		if (npix == 0) {
			thError("%s: WARNING - empty overlap", name);
		}
		#endif
		if (isnan(x) || isinf(x)) {
			thError("%s: ERROR - (NAN) detected in bracket value #b#004", name);
			if (status != NULL) *status = SH_GENERIC_ERROR;
		} else if (status != NULL) {
			*status = SH_SUCCESS;
		}
		thKahanZero();
		return(x);
	} else {
		#if DO_KAHAN_SUM
			#if USE_KAHAN_PACKAGE
			KAHANFL y = 0.0;
			#else
			register BRACKETFL y = 0.0;
			#endif
		#else
			register BRACKETFL y = 0.0;
		#endif
		#if DO_KAHAN_SUM
		register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
		for (ispan = 0; ispan < nspan; ispan++) {
			i = s->y - amrow0;
			x1 = MAX(j1, s->x1 - amcol0);
			x2 = MIN(j2, 1 + s->x2 - amcol0);
			if (i >= i1 && i < i2) {
				arow = a->rows_thpix[i];
				brow = b->rows_thpix[i + abrow0] + abcol0;
				wrow = W->rows_thpix[i + awrow0] + awcol0;
				for (j = x1; j < x2; j++) {
					y_item = (BRACKETFL) arow[j] * (BRACKETFL) brow[j] * (BRACKETFL) wrow[j] - y_error;
					y2 = y + y_item;
					y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
					y = y2;	
					npix++;
				}		
				npix += MAX(x2 - x1, 0);
			}
		}
		#if USE_KAHAN_PACKAGE
		thKahanGet(NULL, NULL, &y);
		#endif
		x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
		#else

		for (ispan = 0; ispan < nspan; ispan++) {
			i = s->y - amrow0;
			x1 = MAX(j1, s->x1 - amcol0);
			x2 = MIN(j2, 1 + s->x2 - amcol0);
			if (i >= i1 && i < i2) {
				arow = a->rows_thpix[i];
				brow = b->rows_thpix[i + abrow0] + abcol0;
				wrow = W->rows_thpix[i + awrow0] + awcol0;
				for (j = x1; j  + 3 < x2; j=j+4) {
				y += (BRACKETFL) arow[j] * (BRACKETFL) brow[j] * (BRACKETFL) wrow[j];
				y += (BRACKETFL) arow[j+2] * (BRACKETFL) brow[j+2] * (BRACKETFL) wrow[j+2];	
				y += (BRACKETFL) arow[j+1] * (BRACKETFL) brow[j+1] * (BRACKETFL) wrow[j+1];
				y += (BRACKETFL) arow[j+3] * (BRACKETFL) brow[j+3] * (BRACKETFL) wrow[j+3];
				}
				for (j = x1 + ((x2 - x1) / 4) * 4; j < x2; j++) {
				y += (BRACKETFL) arow[j] * (BRACKETFL) brow[j] * (BRACKETFL) wrow[j];
				}
				npix += MAX(x2 - x1, 0);
			}	
		}
		#endif
		if (isnan(y) || isinf(y)) {
			thError("%s: ERROR - (NAN) detected in bracket sum #b#012", name);
		}
		x = (MLEFL) y;
		#if DEEP_DEBUG_MLETYPES
		if (npix == 0) {
			thError("%s: WARNING - empty overlap", name);
		}
		#endif
		if (isnan(x) || isinf(x)) {
			thError("%s: ERROR - (NAN) detected in bracket value #b#005", name);
			if (status != NULL) *status = SH_GENERIC_ERROR;
		} else if (status != NULL) {
			*status = SH_SUCCESS;
		}
		thKahanZero();
		return(x);
	}
 	#endif

}

if (mask == NULL) {
	if (W == NULL) {
		thError("%s: ERROR - problematic source code", name);
		if (status != NULL) *status = SH_GENERIC_ERROR;
        	return(MLEFLNAN);
	}
	#if DO_KAHAN_SUM
		#if USE_KAHAN_PACKAGE
		KAHANFL y = 0.0;
		#else
		BRACKETFL y = 0.0;
		#endif
	#else
		BRACKETFL y = 0.0;
	#endif
	#if VERY_DEEP_DEBUG_MLETYPES
	printf("%s: a0= (%d, %d), b0= (%d, %d), w0= (%d, %d) \n", 
		name, arow0, acol0, brow0, bcol0, wrow0, wcol0);
	printf("%s: init= (%d, %d), end= (%d, %d), ab0= (%d, %d), aw0= (%d, %d) \n", 
		name, i1, j1, i2, j2, abrow0, abcol0, awrow0, awcol0);
	#endif
	#if DO_KAHAN_SUM
		#if USE_KAHAN_PACKAGE
		#else
		register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
		#endif
	for (i = i1; i < i2; i++) {
		arow = a->rows_thpix[i];
		brow = b->rows_thpix[i + abrow0] + abcol0;
		wrow = W->rows_thpix[i + awrow0] + awcol0;
		for (j = j1; j < j2; j++) {
			#if USE_KAHAN_PACKAGE
				#if USE_KAHAN_PACKAGE2
				thKahanAddDot3((KAHANFL) arow[j], (KAHANFL) brow[j], (KAHANFL) wrow[j]);
				#else
				yy_item = (KAHANFL) arow[j] * (KAHANFL) brow[j] * (KAHANFL) wrow[j];
				thKahanAdd(yy_item);
				#endif
			#else
			y_item = (BRACKETFL) arow[j] * (BRACKETFL) brow[j] * (BRACKETFL) wrow[j] - y_error;
			y2 = y + y_item;
			y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
			y = y2;
			#endif	
		}
	}
		#if USE_KAHAN_PACKAGE
		thKahanGet(NULL, NULL, &y);
		#else
		x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
		#endif
	#else

	for (i = i1; i < i2; i++) {
		arow = a->rows_thpix[i];
		brow = b->rows_thpix[i + abrow0] + abcol0;
		wrow = W->rows_thpix[i + awrow0] + awcol0;
		for (j = j1; j + 3 < j2; j = j+4) {

		#if OPTIMIZE_BRACKET
		register THPIX awb, bw0, bw1, bw2, bw3;

		bw0 = brow[j] * wrow[j];
		bw1 = brow[j + 1] * wrow[j + 1];
		bw2 = brow[j + 2] * wrow[j + 2];
		bw3 = brow[j + 3] * wrow[j + 3];

		awb = arow[j] * bw0;
		awb += arow[j+1] * bw1;
		awb += arow[j+2] * bw2;
		awb += arow[j+3] * bw3;

		y += (BRACKETFL) awb;

		#else

		y +=(BRACKETFL)arow[j] * (BRACKETFL)brow[j] * (BRACKETFL)wrow[j];
		y +=(BRACKETFL) arow[j+2] * (BRACKETFL) brow[j+2] * (BRACKETFL) wrow[j+2];
		y +=(BRACKETFL) arow[j+1] * (BRACKETFL) brow[j+1] * (BRACKETFL) wrow[j+1];
		y +=(BRACKETFL) arow[j+3] * (BRACKETFL) brow[j+3] * (BRACKETFL) wrow[j+3];
		#endif

		}
		for (j = ((j2 - j1) / 4) * 4 + j1; j < j2; j++) {
			y +=(BRACKETFL)arow[j] * (BRACKETFL)brow[j] * (BRACKETFL)wrow[j];
		}

	}
	#endif
	if (isnan(y) || isinf(y)) {
		thError("%s: ERROR - (NAN) detected in bracket sum #b#013", name);
	}
	x = (MLEFL) y;
	npix = MAX(i2 - i1, 0) * MAX(j2 - j1, 0);
	if (npix == 0) {
		thError("%s ERROR - problematic source code (no overlap while expecting)", name);
		if (status != NULL) *status = SH_GENERIC_ERROR;
		return(MLEFLNAN);	
	}
	if (isinf(x) || isnan(x)) {
		thError("%s: ERROR - (NAN) detected in bracket value #b#006", name);
		if (status != NULL) *status = SH_GENERIC_ERROR;
	} else if (status != NULL) {
		*status = SH_SUCCESS;
	}
	#if (DO_KAHAN_SUM & USE_KAHAN_PACKAGE)
	thKahanZero();		
	#endif
	return(x);
}

thError("%s: ERROR - problematic source code", name);
if (status != NULL) *status = SH_GENERIC_ERROR;
return(MLEFLNAN);

}

/* a new piece needs to be written to include mask_a, mask_b */
MLEFL thBracketOne(REGION *a, LWMASK *lwmask, RET_CODE *status) {
char *name = "thBracketOne";

#if THCLOCK
static int init_clk = 0;
static clock_t clk1, clk2;
if (!init_clk) clk1 = clock();
init_clk++;
#endif

if (a == NULL) {
	char *err;
	if (a == NULL) {
		err = "a";
	}
	thError("%s: ERROR - null input (%s), no calculation to be done", name, err);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	#ifdef NAN
	return(NAN);
	#endif
	#ifdef INFINITY
	return(INFINITY);
	#endif
	return(0.0);
}

if (a->type != TYPE_THPIX) {
	thError("%s: ERROR - only (TYPE_THPIX) pixel type is supported", name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
         #ifdef NAN
        return(NAN);
        #endif
        #ifdef INFINITY
        return(INFINITY);
        #endif
        return(0.0);
}       

REGION *W = NULL;
THMASK *mask = NULL;
register THPIX *wrow = NULL;
int wnrow, wncol, wrow0, wcol0, awrow0, awcol0;
RET_CODE status2;
if (lwmask != NULL) {
	status2 = thLwmaskGetWActive(lwmask, &W);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not get (W_active) from (lwmask)", name);
		if (status != NULL) *status = status2;
		return(MLEFLNAN);
	}

	status2 = thLwmaskGetMaskActive(lwmask, &mask);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not get (mask_active) from (lwmask)", name);
		if (status != NULL) *status = status2;
		return(MLEFLNAN);
	}
}
int mnrow, mncol, mrow0, mcol0, amrow0, amcol0, npix = 0;

#if DEBUG_BRACKET
printf("%s: lwmask (= %p), weight (= %p), mask (= %p) passed \n", name, (void *) lwmask, (void *) W, (void *) mask);
#endif


int anrow, ancol;
anrow = a->nrow;
ancol = a->ncol;

int arow0, acol0;
arow0 = a->row0;
acol0 = a->col0;

int i1, i2, j1, j2;
i1 = 0;
i2 = anrow;
j1 = 0;
j2 = ancol;

if (i2 <= i1 || j2 <= j1) {
	#if DEEP_DEBUG_MLETYPES
	thError("%s: WARNING - empty overlap", name);
	#endif
	if (status != NULL) *status = SH_SUCCESS;
	return((MLEFL) 0.0);
}
int i, j;

register THPIX *arow = NULL;
MLEFL x = (MLEFL) 0.0;
#if DO_KAHAN_SUM
	#if USE_KAHAN_PACKAGE
	thKahanZero();
	KAHANFL yy_item = 0.0;
	#endif
BRACKETFL x_error = (MLEFL) 0.0;
#endif
if (lwmask == NULL || (mask == NULL && W == NULL)) {
	#if DO_KAHAN_SUM
		#if USE_KAHAN_PACKAGE
		KAHANFL y = 0.0;
		#else
		register BRACKETFL y = 0.0;
		#endif
	#else
	register BRACKETFL y = 0.0;
	#endif
	#if DO_KAHAN_SUM
		#if USE_KAHAN_PACKAGE
		#else
		register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
		#endif
	for (i = i1; i < i2; i++) {
		arow = a->rows_thpix[i];
		for (j = j1; j < j2; j++) {
			#if USE_KAHAN_PACKAGE
			yy_item = (KAHANFL) arow[j];
			thKahanAdd(yy_item);
			#else
			y_item = (BRACKETFL) arow[j] - y_error;
			y2 = y + y_item;
			y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
			y = y2;	
			#endif
		}
	}
		#if USE_KAHAN_PACKAGE
		thKahanGet(NULL, NULL, &y);
		#else
		x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
		#endif
	#else
	for (i = i1; i < i2; i++) {
		arow = a->rows_thpix[i];
		for (j = j1; j < j2; j++) {
			y += (BRACKETFL) arow[j];
			y += (BRACKETFL) arow[j+2];	
			y += (BRACKETFL) arow[j+1];
			y += (BRACKETFL) arow[j+3];
		}
		for (j = j1 + ((j2 - j1)/4) * 4; j < j2; j++) {
			y += (BRACKETFL) arow[j];
		}
	}
	#endif
	x = (MLEFL) y;	
	if (status != NULL) *status = SH_SUCCESS;
	#if (DO_KAHAN_SUM & USE_KAHAN_PACKAGE)
	thKahanZero();
	#endif
	return(x);
}


if (W != NULL) {

	if (W->type != TYPE_THPIX) {
		thError("%s: ERROR - weight matrix (W) should be of type (TYPE_THPIX)", name);
		#ifdef NAN
 	        return(NAN);
        	#endif
                #ifdef INFINITY
                return(INFINITY);
                #endif
                return(0.0);
        	}
	wnrow = W->nrow;
	wncol = W->ncol;

	wrow0 = W->row0;
	wcol0 = W->col0;
	awrow0 = arow0 - wrow0;
	awcol0 = acol0 - wcol0;

	i1 = MAX(i1, -awrow0);
	i2 = MIN(i2, wnrow - awrow0);
	j1 = MAX(j1, -awcol0);
	j2 = MIN(j2, wncol - awcol0);

	if (i2 <= i1 || j2 <= j1) {
		#if DEEP_DEBUG_MLETYPES
		thError("%s: WARNING - empty overlap", name);
		#endif
		if (status != NULL) *status = SH_SUCCESS;
		return((MLEFL) 0.0);
	}

}

if (mask != NULL) {

	#if THMASKisMASK

	register U8 *mrow = NULL;
	register MASKBIT maskbit = 0x0;
	MASKBIT maskbit_var = 0x0;
	status2 = thLwmaskGetMaskbit(lwmask, &maskbit_var);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not get (maskbit) from (lwmask)", name);
		if (status != NULL) *status = status2;
		return(MLEFLNAN);
	}
	maskbit = maskbit_var;

	mnrow = mask->nrow;
	mncol = mask->ncol;

	mrow0 = mask->row0;
	mcol0 = mask->col0;
	amrow0 = arow0 - mrow0;
	amcol0 = acol0 - mcol0;

	i1 = MAX(i1, -amrow0);
	i2 = MIN(i2, mnrow - amrow0);
	j1 = MAX(j1, -amcol0);
	j2 = MIN(j2, mncol - amcol0);

	if (i2 <= i1 || j2 <= j1) {
		#if DEEP_DEBUG_MLETYPES
		thError("%s: WARNING - empty overlap", name);
		#endif
		if (status != NULL) *status = SH_SUCCESS;
		return((MLEFL) 0.0);
	}

	if (W == NULL) {
		#if DO_KAHAN_SUM
			#if USE_KAHAN_PACKAGE
			KAHANFL y = 0.0;
			#else
			register BRACKETFL y = 0.0;
			#endif
		#else
		register BRACKETFL y = 0.0;
		#endif
		#if DO_KAHAN_SUM
		register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
		for (i = i1; i < i2; i++) {
			arow = a->rows_thpix[i];
			mrow = mask->rows[i + amrow0] + amcol0;
			for (j = j1; j < j2; j++) {
				if (mrow[j] & maskbit) {
					y_item = (BRACKETFL) arow[j] - y_error;
					y2 = y + y_item;
					y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
					y = y2;
					npix++;
				}	
			}
		}
		#if USE_KAHAN_PACKAGE
		thKahanGet(NULL, NULL, &y);
		#endif
		x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
		#else
		for (i = i1; i < i2; i++) {
                	arow = a->rows_thpix[i];
			mrow = mask->rows[i + amrow0] + amcol0;
                	for (j = j1; j + 3 < j2; j=j+4) {
				if (mrow[j] & maskbit) {
                        		y += (BRACKETFL) arow[j];
					npix++;
				}
				if (mrow[j+2] & maskbit) {
                        		y += (BRACKETFL) arow[j+2];
					npix++;
				}
				if (mrow[j+1] & maskbit) {
                        		y += (BRACKETFL) arow[j+1];
					npix++;
				}
				if (mrow[j+3] & maskbit) {
                        		y += (BRACKETFL) arow[j+3];
					npix++;
				}
                	}
			for (j = j1 + ((j2 - j1) / 4) * 4; j < j2; j++) {
				if (mrow[j] & maskbit) {
                        		y += (BRACKETFL) arow[j];
					npix++;
				}
			}
		}
		#endif
		x = (MLEFL) y;
	} else {
		#if DO_KAHAN_SUM
			#if USE_KAHAN_PACKAGE
			KAHANFL y = 0.0;
			#else
			register BRACKETFL y = 0.0;
			#endif
		#else
		register BRACKETFL y = 0.0;
		#endif
		#if DO_KAHAN_SUM
			#if USE_KAHAN_PACKAGE
			#else
			register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
			#endif
		for (i = i1; i < i2; i++) {
			arow = a->rows_thpix[i];
			wrow = W->rows_thpix[i + awrow0] + awcol0;
			mrow = mask->rows[i + amrow0] + amcol0;
			for (j = j1; j < j2; j++) {
				if (mrow[j] & maskbit) {
					#if USE_KAHAN_PACKAGE
						#if USE_KAHAN_PACKAGE2
						thKahanAddDot2((KAHANFL) arow[j], (KAHANFL) wrow[j]);
						#else
						yy_item = (KAHANFL) arow[j] * (KAHANFL) wrow[j];
						thKahanAdd(yy_item);
						#endif
					#else
					y_item = (BRACKETFL) arow[j] * (BRACKETFL) wrow[j] - y_error;
					y2 = y + y_item;
					y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
					y = y2;
					#endif
					npix++;
				}	
			}
		}
			#if USE_KAHAN_PACKAGE
			thKahanGet(NULL, NULL, &y);
			#else
			x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
			#endif
		#else
		for (i = i1; i < i2; i++) {
                	arow = a->rows_thpix[i];
			wrow = W->rows_thpix[i + awrow0] + awcol0;
			mrow = mask->rows[i + amrow0] + amcol0;
                	for (j = j1; j + 3 < j2; j=j+4) {
				if (mrow[j] & maskbit) {
                        		y += (BRACKETFL) arow[j] * (BRACKETFL) wrow[j];
					npix++;
				}
				if (mrow[j+2] & maskbit) {
                        		y += (BRACKETFL) arow[j+2] * (BRACKETFL) wrow[j+2];
					npix++;
				}
				if (mrow[j+1] & maskbit) {
                        		y += (BRACKETFL) arow[j+1] * (BRACKETFL) wrow[j+1];
					npix++;
				}
				if (mrow[j+3] & maskbit) {
                        		y += (BRACKETFL) arow[j+3] * (BRACKETFL) wrow[j+3];
					npix++;
				}
                	}
			for (j = j1 + ((j2 - j1) / 4) * 4; j < j2; j++) {
				if (mrow[j] & maskbit) {
                        		y += (BRACKETFL) arow[j] * (BRACKETFL) wrow[j];
					npix++;
				}
			}	
	        }
		#endif
		 x = (MLEFL) y;
	}

	#if DEEP_DEBUG_MLETYPES
	if (npix == 0) {
		thError("%s: WARNING - empty overlap", name);
	}
	#endif
	if (status != NULL) *status = SH_SUCCESS;
	#if (DO_KAHAN_SUM & USE_KAHAN_PACKAGE)
	thKahanZero();
	#endif
	return(x); 
     	
	#endif

	#if THMASKisSUPERMASK

	register U16 *mrow = NULL;
	register MASKBIT maskbit = 0x0;
	status = thLwmaskGetMaskbit(lwmask, &maskbit);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (maskbit) from (lwmask)", name);
		return(status);
	}

	mnrow = mask->nrow;
	mncol = mask->ncol;

	mrow0 = mask->row0;
	mcol0 = mask->col0;
	amrow0 = arow0 - mrow0;
	amcol0 = acol0 - mcol0;

	i1 = MAX(i1, -amrow0);
	i2 = MIN(i2, mnrow - amrow0);
	j1 = MAX(j1, -amcol0);
	j2 = MIN(j2, mncol - amcol0);

	if (i2 <= i1 || j2 <= j1) {
		#if DEEP_DEBUG_MLETYPES
		thError("%s: WARNING - empty overlap", name);
		#endif
		if (status != NULL) *status = SH_SUCCESS;
		return((MLEFL) 0.0);
	}

	if (W == NULL) {
		BRACKETFL y = 0.0;
		#if DO_KAHAN_SUM
		register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
		for (i = i1; i < i2; i++) {
			arow = a->rows_thpix[i];
			mrow = mask->rows_u16[i + amrow0] + amcol0;
			for (j = j1; j < j2; j++) {
				if (mrow[j] & maskbit) {
					#if USE_KAHAN_PACKAGE
					yy_item = (KAHANFL) arow[j];
					thKahanAdd(yy_item);
					#else
					y_item = (BRACKETFL) arow[j] - y_error;
					y2 = y + y_item;
					y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
					y = y2;
					#endif
					npix++;
				}	
			}
		}
		#if USE_KAHAN_PACKAGE
		thKahanGet(NULL, NULL, &y);
		#endif
		x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
		#else
		for (i = i1; i < i2; i++) {
                	arow = a->rows_thpix[i];
			mrow = mask->rows_u16[i + amrow0] + amcol0;
                	for (j = j1; j + 3 < j2; j=j+4) {
				if (mrow[j] & maskbit) {
                        		y += (BRACKETFL) arow[j];
					npix++;
				}
				if (mrow[j+2] & maskbit) {
                        		y += (BRACKETFL) arow[j+2];
					npix++;
				}
				if (mrow[j+1] & maskbit) {
                        		y += (BRACKETFL) arow[j+1];
					npix++;
				}
				if (mrow[j+3] & maskbit) {
                        		y += (BRACKETFL) arow[j+3];
					npix++;
				}
                	}
			for (j = j1 + ((j2 - j1) / 4) * 4; j < j2; j++) {
				if (mrow[j] & maskbit) {
                        		y += (BRACKETFL) arow[j];
					npix++;
				}
			}
			
	        }
		#endif
		x = (MLEFL) y;
	} else {
		#if DO_KAHAN_SUM
			#if USE_KAHAN_PACKAGE
			KAHANFL y = 0.0;
			#else
			register BRACKETFL y = 0.0;
			#endif	
		#else
		register BRACKETFL y = 0.0;
		#endif
		#if DO_KAHAN_SUM
		register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
		for (i = i1; i < i2; i++) {
			arow = a->rows_thpix[i];
			wrow = W->rows_thpix[i + awrow0] + awcol0;
			mrow = mask->rows_u16[i + amrow0] + amcol0;
			for (j = j1; j < j2; j++) {
				if (mrow[j] & maskbit) {
					#if USE_KAHAN_PACKAGE
						#if USE_KAHAN_PACKAGE2
						thKahanAddDot2((KAHANFL) arow[j], (KAHANFL) wrow[j]);
						#else
						yy_item = (KAHANFL) arow[j] * (KAHANFL) wrow[j];
						thKahanAdd(yy_item);
						#endif
					#else
					y_item = (BRACKETFL) arow[j] * (BRACKETFL) wrow[j] - y_error;
					y2 = y + y_item;
					y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
					y = y2;
					#endif
					npix++;
				}	
			}
		}
		#if USE_KAHAN_PACKAGE
		thKahanGet(NULL, NULL, &y);
		#endif
		x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
		#else
		for (i = i1; i < i2; i++) {
                	arow = a->rows_thpix[i];
			wrow = W->rows_thpix[i + awrow0] + awcol0;
			mrow = mask->rows_u16[i + amrow0] + amcol0;
                	for (j = j1; j + 3  < j2; j=j+4) {
				if (mrow[j] & maskbit) {
                        		y += (BRACKETFL) arow[j] * (BRACKETFL) wrow[j];
					npix++;
				}
				if (mrow[j+2] & maskbit) {
                        		y += (BRACKETFL) arow[j+2] * (BRACKETFL) wrow[j+2];
					npix++;
				}
				if (mrow[j+1] & maskbit) {
                        		y += (BRACKETFL) arow[j+1] * (BRACKETFL) wrow[j+1];
					npix++;
				}
				if (mrow[j+3] & maskbit) {
                        		y += (BRACKETFL) arow[j+3] * (BRACKETFL) wrow[j+3];
					npix++;
				}
                	}
			for (j = j1 + ((j2 - j1) / 4) * 4; j < j2; j++) {
				if (mrow[j] & maskbit) {
                        		y += (BRACKETFL) arow[j] * (BRACKETFL) wrow[j];
					npix++;
				}
			}

	        }
		#endif
		x = (MLEFL) y;
	}

	#if DEEP_DEBUG_MLETYPES
	if (npix == 0) {
		thError("%s: WARNING - empty overlap", name);
	}
	#endif

	if (status != NULL) *status = SH_SUCCESS;
	thKahanZero();
	return(x); 
 
	#endif

	#if THMASKisOBJMASK
	
	mrow0 = mask->row0;
	mcol0 = mask->col0;
	amrow0 = arow0 - mrow0;
	amcol0 = acol0 - mcol0;

	int nspan;
	SPAN *s;
	s = mask->s;
	nspan = mask->nspan;
	if (mask->npix == 0) {
		thError("%s: WARNING - empty (objmask)", name);
		return((MLEFL) 0.0);
	}

	int x1, x2;
	if (W == NULL) {
		register BRACKETFL y = 0.0;
		for (ispan = 0; ispan < nspan; ispan++) {
			i = s->y - amrow0;
			x1 = MAX(j1, s->x1 - amcol0);
			x2 = MIN(j2, 1 + s->x2 - amcol0);
			if (i >= i1 && i < i2) {
				arow = a->rows_thpix[i];
				brow = b->rows_thpix[i + abrow0] + abcol0;
				for (j = x1; j + 3 < x2; j=j+4) {
				y += (BRACKETFL) arow[j];
				y += (BRACKETFL) arow[j+2];	
				y += (BRACKETFL) arow[j+1];
				y += (BRACKETFL) arow[j+3];
				}
				for (j = x1 + ((x2 - x1) / 4) * 4; j < x2; j++) {
				y += (BRACKETFL) arow[j];
				}
				npix += MAX(x2 - x1, 0);
			}	
		}
		x = (MLEFL) y;
		#if DEEP_DEBUG_MLETYPES
		if (npix == 0) {
			thError("%s: WARNING - empty overlap", name);
		}
		#endif
		if (status != NULL) *status = SH_SUCCESS;
		thKahanZero();
		return(x);
	} else {
		#if DO_KAHAN_SUM
			#if USE_KAHAN_PACKAGE
			KAHANFL y = 0.0;
			#else
			register BRACKETFL y = 0.0;
			#endif
		#else
		register BRACKETFL y = 0.0;
		#endif
		#if DO_KAHAN_SUM
		register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
		for (ispan = 0; ispan < nspan; ispan++) {
			i = s->y - amrow0;
			x1 = MAX(j1, s->x1 - amcol0);
			x2 = MIN(j2, 1 + s->x2 - amcol0);
			if (i >= i1 && i < i2) {
				arow = a->rows_thpix[i];
				wrow = W->rows_thpix[i + awrow0] + awcol0;
				for (j = x1; j < x2; j++) {
					#if USE_KAHAN_PACKAGE
						#if USE_KAHAN_PACKAGE2
						thKahanAddDot2((KAHANFL) arow[j], (KAHANFL) wrow[j]);
						#else
						yy_item = (KAHANFL) arow[j] * (KAHANFL) wrow[j];
						thKahanAdd(yy_item);	
						#endif
					#else
					y_item = (BRACKETFL) arow[j] * (BRACKETFL) wrow[j] - y_error;
					y2 = y + y_item;
					y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
					y = y2;
					#endif
				}	
				npix += MAX(x2 - x1, 0);
			}
		}
		#if USE_KAHAN_PACKAGE
		thKahanGet(NULL, NULL, &y);
		#endif
		x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
		#else
		for (ispan = 0; ispan < nspan; ispan++) {
			i = s->y - amrow0;
			x1 = MAX(j1, s->x1 - amcol0);
			x2 = MIN(j2, 1 + s->x2 - amcol0);
			if (i >= i1 && i < i2) {
				arow = a->rows_thpix[i];
				wrow = W->rows_thpix[i + awrow0] + awcol0;
				for (j = x1; j  + 3 < x2; j=j+4) {
				y += (BRACKETFL) arow[j] * (BRACKETFL) wrow[j];
				y += (BRACKETFL) arow[j+2] * (BRACKETFL) wrow[j+2];	
				y += (BRACKETFL) arow[j+1] * (BRACKETFL) wrow[j+1];
				y += (BRACKETFL) arow[j+3] * (BRACKETFL) wrow[j+3];
				}
				for (j = x1 + ((x2 - x1) / 4) * 4; j < x2; j++) {
				y += (BRACKETFL) arow[j] * (BRACKETFL) wrow[j];
				}
				npix += MAX(x2 - x1, 0);
			}	
		}
		#endif
		x = (MLEFL) y;
		#if DEEP_DEBUG_MLETYPES
		if (npix == 0) {
			thError("%s: WARNING - empty overlap", name);
		}
		#endif
		if (status != NULL) *status = SH_SUCCESS;
		thKahanZero();
		return(x);
	}
 	#endif

}

if (mask == NULL) {
	if (W == NULL) {
		thError("%s: ERROR - problematic source code", name);
		if (status != NULL) *status = SH_GENERIC_ERROR;
        	return(MLEFLNAN);
	}
	#if DO_KAHAN_SUM
		#if USE_KAHAN_PACKAGE
		KAHANFL y = 0.0;
		#else
		BRACKETFL y = 0.0;
		#endif
	#else
	BRACKETFL y = 0.0;
	#endif
	#if VERY_DEEP_DEBUG_MLETYPES
	printf("%s: a0= (%d, %d), b0= (%d, %d), w0= (%d, %d) \n", 
		name, arow0, acol0, brow0, bcol0, wrow0, wcol0);
	printf("%s: init= (%d, %d), end= (%d, %d), ab0= (%d, %d), aw0= (%d, %d) \n", 
		name, i1, j1, i2, j2, abrow0, abcol0, awrow0, awcol0);
	#endif
	#if DO_KAHAN_SUM
		#if USE_KAHAN_PACKAGE
		#else
		register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
		#endif
	for (i = i1; i < i2; i++) {
		arow = a->rows_thpix[i];
		wrow = W->rows_thpix[i + awrow0] + awcol0;
		for (j = j1; j < j2; j++) {
			#if USE_KAHAN_PACKAGE
				#if USE_KAHAN_PACKAGE2
				thKahanAddDot2((KAHANFL) arow[j], (KAHANFL) wrow[j]);
				#else
				yy_item = (KAHANFL) arow[j] * (KAHANFL) wrow[j];
				thKahanAdd(yy_item);
				#endif
			#else
			y_item = (BRACKETFL) arow[j] * (BRACKETFL) wrow[j] - y_error;
			y2 = y + y_item;
			y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
			y = y2;
			#endif
		}	
	}
		#if USE_KAHAN_PACKAGE
		thKahanGet(NULL, NULL, &y);
		#else
		x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
		#endif
	#else
	for (i = i1; i < i2; i++) {
		arow = a->rows_thpix[i];
		wrow = W->rows_thpix[i + awrow0] + awcol0;
		for (j = j1; j + 3 < j2; j = j+4) {

		#if OPTIMIZE_BRACKET
		register THPIX awb;

		awb = arow[j] * wrow[j];
		awb += arow[j + 1] * wrow[j + 1];
		awb += arow[j + 2] * wrow[j + 2];
		awb += arow[j + 3] * wrow[j + 3];

		y += (BRACKETFL) awb;

		#else

		y +=(BRACKETFL)arow[j] * (BRACKETFL)wrow[j];
		y +=(BRACKETFL) arow[j+2] * (BRACKETFL) wrow[j+2];
		y +=(BRACKETFL) arow[j+1] * (BRACKETFL) wrow[j+1];
		y +=(BRACKETFL) arow[j+3] * (BRACKETFL) wrow[j+3];
		#endif

		}
		for (j = ((j2 - j1) / 4) * 4 + j1; j < j2; j++) {
			y +=(BRACKETFL)arow[j] * (BRACKETFL)wrow[j];
		}

	}
	#endif
	x = (MLEFL) y;
	npix = MAX(i2 - i1, 0) * MAX(j2 - j1, 0);
	if (npix == 0) {
		thError("%s ERROR - problematic source code (no overlap while expecting)", name);
		if (status != NULL) *status = SH_GENERIC_ERROR;
		return(MLEFLNAN);	
	}	
	if (status != NULL) *status = SH_SUCCESS;
	#if (DO_KAHAN_SUM & USE_KAHAN_PACKAGE)
	thKahanZero();
	#endif
	return(x);
}

thError("%s: ERROR - problematic source code", name);
if (status != NULL) *status = SH_GENERIC_ERROR;
return(MLEFLNAN);

}

/* a new piece needs to be written to include mask_a, mask_b */
MLEFL thOneBracketOne(LWMASK *lwmask, RET_CODE *status) {
char *name = "thOneBracketOne";

#if THCLOCK
static int init_clk = 0;
static clock_t clk1, clk2;
if (!init_clk) clk1 = clock();
init_clk++;
#endif

REGION *W = NULL;
THMASK *mask = NULL;
register THPIX *wrow = NULL;
int wnrow = 0, wncol = 0, wrow0 = 0, wcol0 = 0;
RET_CODE status2;
if (lwmask != NULL) {
	status2 = thLwmaskGetWActive(lwmask, &W);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not get (W_active) from (lwmask)", name);
		if (status != NULL) *status = status2;
		return(MLEFLNAN);
	}
	status2 = thLwmaskGetMaskActive(lwmask, &mask);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not get (mask_active) from (lwmask)", name);
		if (status != NULL) *status = status2;
		return(MLEFLNAN);
	}
}
int mnrow, mncol, mrow0, mcol0, npix = 0;
int wmrow0, wmcol0;

#if DEBUG_BRACKET
printf("%s: lwmask (= %p), weight (= %p), mask (= %p) passed \n", name, (void *) lwmask, (void *) W, (void *) mask);
#endif

if (lwmask == NULL || (mask == NULL && W == NULL)) {
	char *err = "w";
	thError("%s: ERROR - null input (%s), no calculation to be done", name, err);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	#ifdef NAN
	return(NAN);
	#endif
	#ifdef INFINITY
	return(INFINITY);
	#endif
	return(0.0);
}
int i1, i2, j1, j2;
int i, j;

MLEFL x = (MLEFL) 0.0;
#if DO_KAHAN_SUM
	#if USE_KAHAN_PACKAGE
	thKahanZero();
	KAHANFL yy_item;
	#else
	MLEFL x_error = (MLEFL) 0.0;
	#endif
#endif
if (W != NULL) {

	if (W->type != TYPE_THPIX) {
		thError("%s: ERROR - weight matrix (W) should be of type (TYPE_THPIX)", name);
		#ifdef NAN
 	        return(NAN);
        	#endif
                #ifdef INFINITY
                return(INFINITY);
                #endif
                return(0.0);
        	}
	wnrow = W->nrow;
	wncol = W->ncol;

	wrow0 = W->row0;
	wcol0 = W->col0;

	i1 = 0; 
	i2 = wnrow;
	j1 = 0;
	j2 = wncol;

	if (i2 <= i1 || j2 <= j1) {
		#if DEEP_DEBUG_MLETYPES
		thError("%s: WARNING - empty overlap", name);
		#endif
		if (status != NULL) *status = SH_SUCCESS;
		return((MLEFL) 0.0);
	}
}

if (mask != NULL) {

	#if THMASKisMASK

	register MASKBIT *mrow = NULL;
	register MASKBIT maskbit = 0x0;
	MASKBIT maskbit_var = 0x0;
	status2 = thLwmaskGetMaskbit(lwmask, &maskbit_var);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not get (maskbit) from (lwmask)", name);
		if (status != NULL) *status = status2;
		return(MLEFLNAN);
	}
	maskbit = maskbit_var;
	mnrow = mask->nrow;
	mncol = mask->ncol;

	mrow0 = mask->row0;
	mcol0 = mask->col0;
	wmrow0 = wrow0 - mrow0;
	wmcol0 = wcol0 - mcol0;

	if (W != NULL) {
		i1 = MAX(i1, -wmrow0);
		i2 = MIN(i2, mnrow - wmrow0);
		j1 = MAX(j1, -wmcol0);
		j2 = MIN(j2, mncol - wmcol0);
	} else {
		i1 = 0;
		i2 = mnrow;
		j1 = 0;
		j2 = mncol;
	}

	if (i2 <= i1 || j2 <= j1) {
		#if DEEP_DEBUG_MLETYPES
		thError("%s: WARNING - empty overlap", name);
		#endif
		if (status != NULL) *status = SH_SUCCESS;
		return((MLEFL) 0.0);
	}

	if (W == NULL) {
		npix = 0;
/* Kahan sum would return the same as you are simply counting number of pixels in the mask */
		for (i = i1; i < i2; i++) {
			mrow = mask->rows[i];
                	for (j = j1; j + 3 < j2; j=j+4) {
				if (mrow[j] & maskbit) npix++;
				if (mrow[j+2] & maskbit) npix++;
				if (mrow[j+1] & maskbit) npix++;
				if (mrow[j+3] & maskbit) npix++;
                	}
			for (j = j1 + ((j2 - j1) / 4) * 4; j < j2; j++) {
				if (mrow[j] & maskbit) npix++;
			}
		}
		x = (MLEFL) npix;
	} else {
		#if DO_KAHAN_SUM
			#if USE_KAHAN_PACKAGE
			KAHANFL y = 0.0;
			#else
			register BRACKETFL y = 0.0;
			#endif
		#else
		register BRACKETFL y = 0.0;
		#endif
		#if DO_KAHAN_SUM
			#if USE_KAHAN_PACKAGE
			#else
			register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
			#endif
		for (i = i1; i < i2; i++) {
			wrow = W->rows_thpix[i];
			mrow = mask->rows[i + wmrow0] + wmcol0;
			for (j = j1; j < j2; j++) {
				if (mrow[j] & maskbit) {
					#if USE_KAHAN_PACKAGE
					yy_item = (KAHANFL) wrow[j];
					thKahanAdd(yy_item);
					#else
					y_item = (BRACKETFL) wrow[j] - y_error;
					y2 = y + y_item;
					y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
					y = y2;
					#endif
				}
			}		
		}
			#if USE_KAHAN_PACKAGE
			thKahanGet(NULL, NULL, &y);
			#else
			x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
			#endif
		#else
		for (i = i1; i < i2; i++) {
			wrow = W->rows_thpix[i];
			mrow = mask->rows[i + wmrow0] + wmcol0;
                	for (j = j1; j + 3 < j2; j=j+4) {
				if (mrow[j] & maskbit) {
                        		y += (BRACKETFL) wrow[j];
					npix++;
				}
				if (mrow[j+2] & maskbit) {
                        		y += (BRACKETFL) wrow[j+2];
					npix++;
				}
				if (mrow[j+1] & maskbit) {
                        		y += (BRACKETFL) wrow[j+1];
					npix++;
				}
				if (mrow[j+3] & maskbit) {
                        		y += (BRACKETFL) wrow[j+3];
					npix++;
				}
                	}
			for (j = j1 + ((j2 - j1) / 4) * 4; j < j2; j++) {
				if (mrow[j] & maskbit) {
                        		y += (BRACKETFL) wrow[j];
					npix++;
				}
			}	
	        }
		#endif
		 x = (MLEFL) y;
	}

	#if DEEP_DEBUG_MLETYPES
	if (npix == 0) {
		thError("%s: WARNING - empty overlap", name);
	}
	#endif
	if (status != NULL) *status = SH_SUCCESS;
	#if (DO_KAHAN_SUM & USE_KAHAN_PACKAGE)
	thKahanZero();
	#endif
	return(x); 
     	
	#endif

	#if THMASKisSUPERMASK


	register MASKBIT maskbit = 0x0;
	register MASKBIT *mrow = NULL;
	status = thLwmaskGetMaskbit(lwmask, &maskbit);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (maskbit) from (lwmask)", name);
		return(status);
	}
	mnrow = mask->nrow;
	mncol = mask->ncol;

	mrow0 = mask->row0;
	mcol0 = mask->col0;

	i1 = 0;
	i2 = mnrow;
	j1 = 0;
	j2 = mncol;

	if (i2 <= i1 || j2 <= j1) {
		#if DEEP_DEBUG_MLETYPES
		thError("%s: WARNING - empty overlap", name);
		#endif
		if (status != NULL) *status = SH_SUCCESS;
		return((MLEFL) 0.0);
	}

	if (W == NULL) {
		for (i = i1; i < i2; i++) {
			mrow = mask->rows_u16[i];
                	for (j = j1; j + 3 < j2; j=j+4) {
				if (mrow[j] & maskbit) npix++;
				if (mrow[j+2] & maskbit) npix++;
				if (mrow[j+1] & maskbit) npix++;
				if (mrow[j+3] & maskbit) npix++;
                	}
			for (j = j1 + ((j2 - j1) / 4) * 4; j < j2; j++) {
				if (mrow[j] & maskbit) npix++;
			}
			
	        }
		x = (MLEFL) npix;
	} else {
		wmrow0 = wrow0 - mrow0;
		wmcol0 = wcol0 - mcol0;
		#if DO_KAHAN_SUM
			#if USE_KAHAN_PACKAGE
			KAHANFL y = 0.0;
			#else
			register BRACKETFL y = 0.0;
			#endif
		#else
		register BRACKETFL y = 0.0;
		#endif
		#if DO_KAHAN_SUM
		register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
		for (i = i1; i < i2; i++) {
			wrow = W->rows_thpix[i];
			mrow = mask->rows_u16[i + wmrow0] + wmcol0;
			for (j = j1; j < j2; j++) {
				if (mrow[j] & maskbit) {
					#if USE_KAHAN_PACKAGE
					yy_item = (BRACKETFL) wrow[j];
					thKahanAdd(yy_item);
					#else
					y_item = (BRACKETFL) wrow[j] - y_error;
					y2 = y + y_item;
					y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
					y = y2;
					#endif
				}
			}		
		}
		#if USE_KAHAN_PACKAGE
		thKahanGet(NULL, NULL, &y);
		#endif
		x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
		#else
		for (i = i1; i < i2; i++) {
			wrow = W->rows_thpix[i];
			mrow = mask->rows_u16[i + wmrow0] + wmcol0;
                	for (j = j1; j + 3  < j2; j=j+4) {
				if (mrow[j] & maskbit) {
                        		y += (BRACKETFL) wrow[j];
					npix++;
				}
				if (mrow[j+2] & maskbit) {
                        		y += (BRACKETFL) wrow[j+2];
					npix++;
				}
				if (mrow[j+1] & maskbit) {
                        		y += (BRACKETFL) wrow[j+1];
					npix++;
				}
				if (mrow[j+3] & maskbit) {
                        		y += (BRACKETFL) wrow[j+3];
					npix++;
				}
                	}
			for (j = j1 + ((j2 - j1) / 4) * 4; j < j2; j++) {
				if (mrow[j] & maskbit) {
                        		y += (BRACKETFL) wrow[j];
					npix++;
				}
			}

	        }
		#endif
		x = (MLEFL) y;
	}

	#if DEEP_DEBUG_MLETYPES
	if (npix == 0) {
		thError("%s: WARNING - empty overlap", name);
	}
	#endif

	if (status != NULL) *status = SH_SUCCESS;
	thKahanZero();
	return(x); 
 
	#endif

	#if THMASKisOBJMASK
	
	mrow0 = mask->row0;
	mcol0 = mask->col0;

	int nspan;
	SPAN *s;
	s = mask->s;
	nspan = mask->nspan;
	if (mask->npix == 0) {
		thError("%s: WARNING - empty (objmask)", name);
		return((MLEFL) 0.0);
	}

	int x1, x2;
	if (W == NULL) {
		register BRACKETFL y = 0.0;
		npix = mask->npix;
		x = (MLEFL) npix;
		#if DEEP_DEBUG_MLETYPES
		if (npix == 0) {
			thError("%s: WARNING - empty overlap", name);
		}
		#endif
		if (status != NULL) *status = SH_SUCCESS;
		thKahanZero();
		return(x);
	} else {
		#if DO_KAHAN_SUM
			#if USE_KAHAN_PACKAGE
			KAHANFL y = 0.0;
			#else
			register BRACKETFL y = 0.0;
			#endif
		#else
			register BRACKETFL y = 0.0;
		#endif
		#if DO_KAHAN_SUM
		register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
		for (ispan = 0; ispan < nspan; ispan++) {
			i = s->y - wmrow0;
			x1 = MAX(j1, s->x1 - wmcol0);
			x2 = MIN(j2, 1 + s->x2 - wmcol0);
			if (i >= i1 && i < i2) {
				wrow = W->rows_thpix[i];
				for (j = x1; j < x2; j++) {
					y_item = (BRACKETFL) wrow[j] - y_error;
					y2 = y + y_item;
					y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
					y = y2;
				}
				npix += MAX(x2 - x1, 0);
			}
		}
		#if USE_KAHAN_PACKAGE
		thKahanGet(NULL, NULL, &y);
		#endif
		x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
		#else
		for (ispan = 0; ispan < nspan; ispan++) {
			i = s->y - wmrow0;
			x1 = MAX(j1, s->x1 - wmcol0);
			x2 = MIN(j2, 1 + s->x2 - wmcol0);
			if (i >= i1 && i < i2) {
				wrow = W->rows_thpix[i];
				for (j = x1; j  + 3 < x2; j=j+4) {
				y += (BRACKETFL) wrow[j];
				y += (BRACKETFL) wrow[j+2];	
				y += (BRACKETFL) wrow[j+1];
				y += (BRACKETFL) wrow[j+3];
				}
				for (j = x1 + ((x2 - x1) / 4) * 4; j < x2; j++) {
				y += (BRACKETFL) wrow[j];
				}
				npix += MAX(x2 - x1, 0);
			}	
		}
		#endif
		x = (MLEFL) y;
		#if DEEP_DEBUG_MLETYPES
		if (npix == 0) {
			thError("%s: WARNING - empty overlap", name);
		}
		#endif
		if (status != NULL) *status = SH_SUCCESS;
		thKahanZero();
		return(x);
	}
 	#endif

}

if (mask == NULL) {
	if (W == NULL) {
		thError("%s: ERROR - problematic source code", name);
		if (status != NULL) *status = SH_GENERIC_ERROR;
        	return(MLEFLNAN);
	}
	#if USE_KAHAN_PACKAGE
	KAHANFL y = 0.0;
	#else
	BRACKETFL y = 0.0;
	#endif
	#if VERY_DEEP_DEBUG_MLETYPES
	printf("%s: w0= (%d, %d) \n", name, wrow0, wcol0);
	printf("%s: init= (%d, %d), end= (%d, %d) \n", 
		name, i1, j1, i2, j2);
	#endif
	#if DO_KAHAN_SUM
		#if USE_KAHAN_PACKAGE
		#else	
		register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
		#endif
	for (i = i1; i < i2; i++) {
		wrow = W->rows_thpix[i];
		for (j = j1; j < j2; j++) {
			#if USE_KAHAN_PACKAGE
			yy_item = (KAHANFL) wrow[j];
			thKahanAdd(yy_item);
			#else
			y_item = (BRACKETFL) wrow[j] - y_error;
			y2 = y + y_item;
			y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
			y = y2;
			#endif
		}
	}
		#if USE_KAHAN_PACKAGE
		thKahanGet(NULL, NULL, &y);
		#else	
		x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
		#endif
	#else
	for (i = i1; i < i2; i++) {
		wrow = W->rows_thpix[i];
		for (j = j1; j + 3 < j2; j = j+4) {

		#if OPTIMIZE_BRACKET
		register THPIX awb;

		awb = wrow[j];
		awb += wrow[j + 1];
		awb += wrow[j + 2];
		awb += wrow[j + 3];

		y += (BRACKETFL) awb;

		#else

		y += (BRACKETFL) wrow[j];
		y += (BRACKETFL) wrow[j+2];
		y += (BRACKETFL) wrow[j+1];
		y += (BRACKETFL) wrow[j+3];
		
		#endif

		}
		for (j = ((j2 - j1) / 4) * 4 + j1; j < j2; j++) {
			y += (BRACKETFL)wrow[j];
		}

	}
	#endif
	x = (MLEFL) y;
	npix = MAX(i2 - i1, 0) * MAX(j2 - j1, 0);
	if (npix == 0) {
		thError("%s ERROR - problematic source code (no overlap while expecting)", name);
		if (status != NULL) *status = SH_GENERIC_ERROR;
		return(MLEFLNAN);	
	}	
	if (status != NULL) *status = SH_SUCCESS;
	#if (DO_KAHAN_SUM & USE_KAHAN_PACKAGE)
	thKahanZero();
	#endif
	return(x);
}

thError("%s: ERROR - problematic source code", name);
if (status != NULL) *status = SH_GENERIC_ERROR;
return(MLEFLNAN);

}


MLEFL thReg3BracketReg(THREGION3 *a, LWMASK *lwmask, REGION *b, RET_CODE *status) {
char *name = "thReg3BracketReg";

#if THCLOCK
static int init_clk = 0;
static clock_t clk1, clk2;
if (!init_clk) clk1 = clock();
init_clk++;
#endif

if (a == NULL || b == NULL) {
	thError("%s: ERROR - null input, no calculation to be done", name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(MLEFLNAN);
}

if (b->type != TYPE_THPIX) {
	thError("%s: ERROR - only (TYPE_THPIX) pixel type is supported", name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
        return(MLEFLNAN);
}       

REGION *W = NULL;
THMASK *mask = NULL;
THPIX *wrow;
int wnrow, wncol, wrow0, wcol0, awrow0, awcol0;
RET_CODE status2;
if (status != NULL) {
	status2 = thLwmaskGetWActive(lwmask, &W);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not get (W_active) from (lwmask)", name);
		if (status != NULL) *status = status2;
		return(MLEFLNAN);
	}
	status2 = thLwmaskGetMaskActive(lwmask, &mask);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not get (mask_active) from (lwmask)", name);
		if (status != NULL) *status = status2;
		return(MLEFLNAN);
	}
}
int mnrow, mncol, mrow0, mcol0, amrow0, amcol0, npix = 0;

#if DEBUG_BRACKET
printf("%s: lwmask (= %p), weight (= %p), mask (= %p) passed \n", name, (void *) lwmask, (void *) W, (void *) mask);
#endif

int anrow, ancol, bnrow, bncol;
anrow = a->nrow;
ancol = a->ncol;
bnrow = b->nrow;
bncol = b->ncol;

int arow0, acol0, brow0, bcol0, abrow0, abcol0;
arow0 = a->row0;
acol0 = a->col0;
brow0 = b->row0;
bcol0 = b->col0;
abrow0 = arow0 - brow0;
abcol0 = acol0 - bcol0;

int i1, i2, j1, j2;
i1 = MAX(0, - abrow0);
i2 = MIN(anrow, bnrow - abrow0);
j1 = MAX(0, - abcol0);
j2 = MIN(ancol, bncol - abcol0);

if (i2 <= i1 || j2 <= j1) {
	#if DEEP_DEBUG_MLETYPES
	thError("%s: WARNING - empty overlap", name);
	#endif
	if (status != NULL) *status = SH_SUCCESS;
	return((MLEFL) 0.0);
}
int i, j;
THPIX *afrow, *afcol, *brow;
status2 = thRegion3Get(a, NULL, NULL, (void **) &afrow, (void **) &afcol, BAD_PSF_INDEX);
if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not get f(x) and g(y) from (a)", name);
	if (status != NULL) *status = status2;
	return(MLEFLNAN);
}
THPIX avfrow = 1.0, avfcol = 1.0; /* these initial values are set such that a null f(x) or g(y) would be equated to 1.0 */
MLEFL x = (MLEFL) 0.0;
#if DO_KAHAN_SUM
MLEFL x_error = (MLEFL) 0.0;
#endif
if (lwmask == NULL || (mask == NULL && W == NULL)) {
	BRACKETFL y = 0.0;
	#if DO_KAHAN_SUM
	register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
	for (i = i1; i < i2; i++) {
		if (afrow != NULL) avfrow = afrow[i];
		brow = b->rows_thpix[i + abrow0] + abcol0;
		for (j = j1; j < j2; j++) {
			if (afcol != NULL) avfcol = afcol[j];
			y_item = (BRACKETFL) avfcol * (BRACKETFL) avfrow * (BRACKETFL) brow[j] - y_error;
			y2 = y + y_item;
			y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
			y = y2;
		}
	}		
	x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
	#else
	for (i = i1; i < i2; i++) {
		if (afrow != NULL) avfrow = afrow[i];
		brow = b->rows_thpix[i + abrow0] + abcol0;
		BRACKETFL z = 0.0;
		for (j = j1; j < j2; j++) {
			if (afcol != NULL) avfcol = afcol[j];
			z += (BRACKETFL) avfcol * (BRACKETFL) brow[j];
		}
		y += z * (BRACKETFL) avfrow;
	}
	#endif
	x = (MLEFL) y;
	if (status != NULL) *status = SH_SUCCESS;
	return(x);
}

if (W != NULL) {

	if (W->type != TYPE_THPIX) {
		thError("%s: ERROR - weight matrix (W) should be of type (TYPE_THPIX)", name);
                return(MLEFLNAN);
        	}
	wnrow = W->nrow;
	wncol = W->ncol;

	wrow0 = W->row0;
	wcol0 = W->col0;
	awrow0 = arow0 - wrow0;
	awcol0 = acol0 - wcol0;

	i1 = MAX(i1, -awrow0);
	i2 = MIN(i2, wnrow - awrow0);
	j1 = MAX(j1, -awcol0);
	j2 = MIN(j2, wncol - awcol0);

	if (i2 <= i1 || j2 <= j1) {
		#if DEEP_DEBUG_MLETYPES
		thError("%s: WARNING - empty overlap", name);
		#endif
		if (status != NULL) *status = SH_SUCCESS;
		return((MLEFL) 0.0);
	}

}

if (mask != NULL) {

	#if THMASKisMASK

	MASKBIT maskbit;
	MASKBIT *mrow;
	status2 = thLwmaskGetMaskbit(lwmask, &maskbit);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not get (maskbit) from (lwmask)", name);
		if (status != NULL) *status = status2;
		return(MLEFLNAN);
	}

	mnrow = mask->nrow;
	mncol = mask->ncol;

	mrow0 = mask->row0;
	mcol0 = mask->col0;
	amrow0 = arow0 - mrow0;
	amcol0 = acol0 - mcol0;

	i1 = MAX(i1, -amrow0);
	i2 = MIN(i2, mnrow - amrow0);
	j1 = MAX(j1, -amcol0);
	j2 = MIN(j2, mncol - amcol0);

	if (i2 <= i1 || j2 <= j1) {
		#if DEEP_DEBUG_MLETYPES
		thError("%s: WARNING - empty overlap", name);
		#endif
		if (status != NULL) *status = SH_SUCCESS;
		return((MLEFL) 0.0);
	}

	if (W == NULL) {
		BRACKETFL y = 0.0;
		#if DO_KAHAN_SUM
		register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
		for (i = i1; i < i2; i++) {
			if (afrow != NULL) avfrow = afrow[i];
			brow = b->rows_thpix[i + abrow0] + abcol0;
			mrow = mask->rows[i + amrow0] + amcol0;
			for (j = j1; j < j2; j++) {
				if (mrow[j] & maskbit) {
					if (afcol != NULL) avfcol = afcol[j];
					y_item = (BRACKETFL) avfcol * (BRACKETFL) avfrow * (BRACKETFL) brow[j] - y_error;
					y2 = y + y_item;
					y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
					y = y2;
					npix++;
				}
			}
		}		
		x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
		#else
		for (i = i1; i < i2; i++) {
                	if (afrow != NULL) avfrow = afrow[i];
			brow = b->rows_thpix[i + abrow0] + abcol0;
			mrow = mask->rows[i + amrow0] + amcol0;
			BRACKETFL z = 0.0;
                	for (j = j1; j < j2; j++) {
				if (mrow[j] & maskbit) {
					if (afcol != NULL) avfcol = afcol[j];
                        		z += (BRACKETFL) avfcol * (BRACKETFL) brow[j];
					npix++;
				}
                	}
			y += z * (BRACKETFL) avfrow;
		}
		#endif
		x = (MLEFL) y;
	} else {
		BRACKETFL y = 0.0;
		#if DO_KAHAN_SUM
		register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
		for (i = i1; i < i2; i++) {
			if (afrow != NULL) avfrow = afrow[i];
			brow = b->rows_thpix[i + abrow0] + abcol0;
			wrow = W->rows_thpix[i + awrow0] + awcol0;
			mrow = mask->rows[i + amrow0] + amcol0;
			for (j = j1; j < j2; j++) {
				if (mrow[j] & maskbit) {
					if (afcol != NULL) avfcol = afcol[j];
					y_item = (BRACKETFL) avfcol * (BRACKETFL) avfrow * (BRACKETFL) brow[j] * (BRACKETFL) wrow[j] - y_error;
					y2 = y + y_item;
					y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
					y = y2;
					npix++;
				}
			}
		}		
		x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
		#else
		for (i = i1; i < i2; i++) {
                	if (afrow != NULL) avfrow = afrow[i];
			brow = b->rows_thpix[i + abrow0] + abcol0;
			wrow = W->rows_thpix[i + awrow0] + awcol0;
			mrow = mask->rows[i + amrow0] + amcol0;
			BRACKETFL z = 0.0;
                	for (j = j1; j < j2; j++) {
				if (mrow[j] & maskbit) {
					if (afcol != NULL) avfcol = afcol[j];
                        		z += (BRACKETFL) avfcol * (BRACKETFL) brow[j] * (BRACKETFL) wrow[j];
					npix++;
				}
                	}
			y += z * (BRACKETFL) avfrow;
	        }
		#endif
		 x = (MLEFL) y;
	}

	#if DEEP_DEBUG_MLETYPES
	if (npix == 0) {
		thError("%s: WARNING - empty overlap", name);
	}
	#endif
	if (status != NULL) *status = SH_SUCCESS;
	return(x); 
     	
	#endif

	#if THMASKisSUPERMASK


	MASKBIT maskbit;
	MASKBIT *mrow;
	status = thLwmaskGetMaskbit(lwmask, &maskbit);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (maskbit) from (lwmask)", name);
		return(status);
	}

	mnrow = mask->nrow;
	mncol = mask->ncol;

	mrow0 = mask->row0;
	mcol0 = mask->col0;
	amrow0 = arow0 - mrow0;
	amcol0 = acol0 - mcol0;

	i1 = MAX(i1, -amrow0);
	i2 = MIN(i2, mnrow - amrow0);
	j1 = MAX(j1, -amcol0);
	j2 = MIN(j2, mncol - amcol0);

	if (i2 <= i1 || j2 <= j1) {
		#if DEEP_DEBUG_MLETYPES
		thError("%s: WARNING - empty overlap", name);
		#endif
		if (status != NULL) *status = SH_SUCCESS;
		return((MLEFL) 0.0);
	}

	if (W == NULL) {
		BRACKETFL y = 0.0;
		#if DO_KAHAN_SUM
		register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
		for (i = i1; i < i2; i++) {
			if (afrow != NULL) avfrow = afrow[i];
			brow = b->rows_thpix[i + abrow0] + abcol0;
			mrow = mask->rows_u16[i + amrow0] + amcol0;
			for (j = j1; j < j2; j++) {
				if (mrow[j] & maskbit) {
					if (afcol != NULL) avfcol = afcol[j];
					y_item = (BRACKETFL) avfcol * (BRACKETFL) avfrow * (BRACKETFL) brow[j] - y_error;
					y2 = y + y_item;
					y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
					y = y2;
					npix++;
				}
			}
		}		
		x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
		#else
		for (i = i1; i < i2; i++) {
                	if (afrow != NULL) avfrow = afrow[i];
			brow = b->rows_thpix[i + abrow0] + abcol0;
			mrow = mask->rows_u16[i + amrow0] + amcol0;
			BRACKETFL z = 0.0;
                	for (j = j1; j < j2; j++) {
				if (mrow[j] & maskbit) {
					if (afcol != NULL) avfcol = afcol[j];
                        		z += (BRACKETFL) avfcol * (BRACKETFL) brow[j];
					npix++;
				}
                	}
			y += z * (BRACKETFL) avfrow;
	        }
		#endif
		x = (MLEFL) y;
	} else {
		BRACKETFL y = 0.0;
		#if DO_KAHAN_SUM
		register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
		for (i = i1; i < i2; i++) {
			if (afrow != NULL) avfrow = afrow[i];
			brow = b->rows_thpix[i + abrow0] + abcol0;
			wrow = W->rows_thpix[i + awrow0] + awcol0;
			mrow = mask->rows_u16[i + amrow0] + amcol0;
			for (j = j1; j < j2; j++) {
				if (mrow[j] & maskbit) {
					if (afcol != NULL) avfcol = afcol[j];
					y_item = (BRACKETFL) avfcol * (BRACKETFL) avfrow * (BRACKETFL) brow[j] * (BRACKETFL) wrow[j] - y_error;
					y2 = y + y_item;
					y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
					y = y2;
					npix++;
				}
			}
		}		
		x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
		#else
		for (i = i1; i < i2; i++) {
                	if (afrow != NULL) avfrow = afrow[i];
			brow = b->rows_thpix[i + abrow0] + abcol0;
			wrow = W->rows_thpix[i + awrow0] + awcol0;
			mrow = mask->rows_u16[i + amrow0] + amcol0;
			BRACKETFL z = 0.0;
                	for (j = j1; j < j2; j++) {
				if (mrow[j] & maskbit) {
					if (afcol != NULL) avfcol = afcol[j];
                        		z += (BRACKETFL) avfcol * (BRACKETFL) brow[j] * (BRACKETFL) wrow[j];
					npix++;
				}
                	}
			y += z * (BRACKETFL) avfrow;
	        }
		#endif
		x = (MLEFL) y;
	}

	#if DEEP_DEBUG_MLETYPES
	if (npix == 0) {
		thError("%s: WARNING - empty overlap", name);
	}
	#endif

	if (status != NULL) *status = SH_SUCCESS;
	return(x); 
 
	#endif

	#if THMASKisOBJMASK
	
	mrow0 = mask->row0;
	mcol0 = mask->col0;
	amrow0 = arow0 - mrow0;
	amcol0 = acol0 - mcol0;

	int nspan;
	SPAN *s;
	s = mask->s;
	nspan = mask->nspan;
	if (mask->npix == 0) {
		thError("%s: WARNING - empty (objmask)", name);
		return((MLEFL) 0.0);
	}

	int x1, x2;
	if (W == NULL) {	
		BRACKETFL y = 0.0;
		#if DO_KAHAN_SUM
		register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
		for (ispan = 0; ispan < nspan; ispan++) {
			i = s->y - amrow0;
			x1 = MAX(j1, s->x1 - amcol0);
			x2 = MIN(j2, 1 + s->x2 - amcol0);
			if (i >= i1 && i < i2) {
				if (afrow != NULL) avfrow = afrow[i];
				brow = b->rows_thpix[i + abrow0] + abcol0;
				for (j = x1; j < x2; j++) {
					if (afcol != NULL) avfcol = afcol[j];
					y_item = (BRACKETFL) avfcol * (BRACKETFL) avfrow * (BRACKETFL) brow[j] - y_error;
					y2 = y + y_item;
					y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
					y = y2;
				}	
				npix += MAX(x2 - x1, 0);
			}
		}		
		x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
		#else
		for (ispan = 0; ispan < nspan; ispan++) {
			i = s->y - amrow0;
			x1 = MAX(j1, s->x1 - amcol0);
			x2 = MIN(j2, 1 + s->x2 - amcol0);
			if (i >= i1 && i < i2) {
				if (afrow != NULL) avfrow = afrow[i];
				brow = b->rows_thpix[i + abrow0] + abcol0;
				BRACKETFL z = 0.0;
				for (j = x1; j < x2; j++) {
					if (afcol != NULL) avfcol = afcol[j];
					z += (BRACKETFL) avfcol * (BRACKETFL) brow[j];
				}
				npix += MAX(x2 - x1, 0);
				y += z * (BRACKETFL) avfrow;
			}	
		}
		#endif
		x = (MLEFL) y;
		#if DEEP_DEBUG_MLETYPES
		if (npix == 0) {
			thError("%s: WARNING - empty overlap", name);
		}
		#endif
		if (status != NULL) *status = SH_SUCCESS;
		return(x);
	} else {
		BRACKETFL y = 0.0;
		#if DO_KAHAN_SUM
		register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
		for (ispan = 0; ispan < nspan; ispan++) {
			i = s->y - amrow0;
			x1 = MAX(j1, s->x1 - amcol0);
			x2 = MIN(j2, 1 + s->x2 - amcol0);
			if (i >= i1 && i < i2) {
				if (afrow != NULL) avfrow = afrow[i];
				brow = b->rows_thpix[i + abrow0] + abcol0;
				wrow = W->rows_thpix[i + awrow0] + awcol0;
				for (j = x1; j < x2; j++) {
					if (afcol != NULL) avfcol = afcol[j];
					y_item = (BRACKETFL) avfcol * (BRACKETFL) avfrow * (BRACKETFL) brow[j] * (BRACKETFL) wrow[j] - y_error;
					y2 = y + y_item;
					y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
					y = y2;
				}	
				npix += MAX(x2 - x1, 0);
			}
		}		
		x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
		#else

		for (ispan = 0; ispan < nspan; ispan++) {
			i = s->y - amrow0;
			x1 = MAX(j1, s->x1 - amcol0);
			x2 = MIN(j2, 1 + s->x2 - amcol0);
			if (i >= i1 && i < i2) {
				if (afrow != NULL) avfrow = afrow[i];
				brow = b->rows_thpix[i + abrow0] + abcol0;
				wrow = W->rows_thpix[i + awrow0] + awcol0;
				BRACKETFL z = 0.0;
				for (j = x1; j < x2; j++) {
					if (afcol != NULL) avfcol = afcol[j];
					z += (BRACKETFL) avfcol * (BRACKETFL) brow[j] * (BRACKETFL) wrow[j];
				}
				npix += MAX(x2 - x1, 0);
				y += z * (BRACKETFL) avfrow;
			}	
		}
		#endif
		x = (MLEFL) y;
		#if DEEP_DEBUG_MLETYPES
		if (npix == 0) {
			thError("%s: WARNING - empty overlap", name);
		}
		#endif
		if (status != NULL) *status = SH_SUCCESS;
		return(x);
	}
 	#endif

}

if (mask == NULL) {
	if (W == NULL) {
		thError("%s: ERROR - problematic source code", name);
		if (status != NULL) *status = SH_GENERIC_ERROR;
        	return(MLEFLNAN);
	}
	#if VERY_DEEP_DEBUG_MLETYPES
	printf("%s: a0= (%d, %d), b0= (%d, %d), w0= (%d, %d) \n", 
		name, arow0, acol0, brow0, bcol0, wrow0, wcol0);
	printf("%s: init= (%d, %d), end= (%d, %d), ab0= (%d, %d), aw0= (%d, %d) \n", 
		name, i1, j1, i2, j2, abrow0, abcol0, awrow0, awcol0);
	#endif	
	BRACKETFL y = 0.0;
	#if DO_KAHAN_SUM
	register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
	for (i = i1; i < i2; i++) {
		if (afrow != NULL) avfrow = afrow[i];
		brow = b->rows_thpix[i + abrow0] + abcol0;
		wrow = W->rows_thpix[i + awrow0] + awcol0;
		for (j = j1; j < j2; j++) {
			if (afcol != NULL) avfcol = afcol[j];
			y_item = (BRACKETFL) avfcol * (BRACKETFL) avfrow * (BRACKETFL) brow[j] * (BRACKETFL) wrow[j] - y_error;
			y2 = y + y_item;
			y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
			y = y2;
		}	
	}
	x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
	#else
	for (i = i1; i < i2; i++) {
		if (afrow != NULL) avfrow = afrow[i];
		brow = b->rows_thpix[i + abrow0] + abcol0;
		wrow = W->rows_thpix[i + awrow0] + awcol0;
		BRACKETFL z = 0.0;
		for (j = j1; j < j2; j++) {
			if (afcol != NULL) avfcol = afcol[j];
			z +=  (BRACKETFL) avfcol * (BRACKETFL) brow[j] * (BRACKETFL) wrow[j];
		}
		y += z * (BRACKETFL) avfrow;
	}
	#endif
	x = (MLEFL) y;
	npix = MAX(i2 - i1, 0) * MAX(j2 - j1, 0);
	if (npix == 0) {
		thError("%s ERROR - problematic source code (no overlap while expecting)", name);
		if (status != NULL) *status = SH_GENERIC_ERROR;
		return(MLEFLNAN);	
	}	
	if (status != NULL) *status = SH_SUCCESS;
	return(x);
}

thError("%s: ERROR - problematic source code", name);
if (status != NULL) *status = SH_GENERIC_ERROR;
return(MLEFLNAN);

}

MLEFL thReg3BracketReg3(THREGION3 *a, LWMASK *lwmask, THREGION3 *b, RET_CODE *status) {
char *name = "thReg3BracketReg3";

#if THCLOCK
static int init_clk = 0;
static clock_t clk1, clk2;
if (!init_clk) clk1 = clock();
init_clk++;
#endif

if (a == NULL || b == NULL) {
	thError("%s: ERROR - null input, no calculation to be done", name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(MLEFLNAN);
}

REGION *W = NULL;
THPIX *wrow;
THMASK *mask = NULL;
int wnrow, wncol, wrow0, wcol0, awrow0, awcol0;
RET_CODE status2;
if (lwmask != NULL)  {
	status2 = thLwmaskGetWActive(lwmask, &W);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not get (W_active) from (lwmask)", name);
		if (status != NULL) *status = status2;
		return(MLEFLNAN);
	}
	status2 = thLwmaskGetMaskActive(lwmask, &mask);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not get (mask_active) from (lwmask)", name);
		if (status != NULL) *status = status2;
		return(MLEFLNAN);
	}
}
int mnrow, mncol, mrow0, mcol0, amrow0, amcol0, npix = 0;

#if DEBUG_BRACKET
printf("%s: lwmask (= %p), weight (= %p), mask (= %p) passed \n", name, (void *) lwmask, (void *) W, (void *) mask);
#endif

int anrow, ancol, bnrow, bncol;
anrow = a->nrow;
ancol = a->ncol;
bnrow = b->nrow;
bncol = b->ncol;

int arow0, acol0, brow0, bcol0, abrow0, abcol0;
arow0 = a->row0;
acol0 = a->col0;
brow0 = b->row0;
bcol0 = b->col0;
abrow0 = arow0 - brow0;
abcol0 = acol0 - bcol0;

int i1, i2, j1, j2;
i1 = MAX(0, - abrow0);
i2 = MIN(anrow, bnrow - abrow0);
j1 = MAX(0, - abcol0);
j2 = MIN(ancol, bncol - abcol0);

if (i2 <= i1 || j2 <= j1) {
	#if DEEP_DEBUG_MLETYPES
	thError("%s: WARNING - empty overlap", name);
	#endif
	if (status != NULL) *status = SH_SUCCESS;
	return((MLEFL) 0.0);
}
int i, j;
THPIX *afrow, *afcol, *bfrow, *bfcol;
status2 = thRegion3Get(a, NULL, NULL, (void **) &afrow, (void **) &afcol, BAD_PSF_INDEX);
if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not get f(x) and g(y) from (a)", name);
	if (status != NULL) *status = status2;
	return(MLEFLNAN);
}
status2 = thRegion3Get(b, NULL, NULL, (void **) &bfrow, (void **) &bfcol, BAD_PSF_INDEX);
if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not get f(x) and g(y) from (a)", name);
	if (status != NULL) *status = status2;
	return(MLEFLNAN);
}
THPIX avfrow = 1.0, avfcol = 1.0; /* these initial values are set such that a null f(x) or g(y) would be equated to 1.0 */
THPIX bvfrow = 1.0, bvfcol = 1.0;
MLEFL x = (MLEFL) 0.0;
#if DO_KAHAN_SUM
MLEFL x_error = (MLEFL) 0.0;
#endif
if (lwmask == NULL || (mask == NULL && W == NULL)) {
	BRACKETFL y = 0.0;
	#if DO_KAHAN_SUM
	register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
	for (i = i1; i < i2; i++) {
		if (afrow != NULL) avfrow = afrow[i];	
		if (bfrow != NULL) bvfrow = bfrow[i + abrow0];
		for (j = j1; j < j2; j++) {
			if (afcol != NULL) avfcol = afcol[j];	
			if (bfcol != NULL) bvfcol = bfcol[j + abcol0];
			y_item = (BRACKETFL) avfcol * (BRACKETFL) avfrow * (BRACKETFL) bvfrow * (BRACKETFL) bvfcol - y_error;
			y2 = y + y_item;
			y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
			y = y2;
		}	
	}
	x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
	#else
	for (i = i1; i < i2; i++) {
		if (afrow != NULL) avfrow = afrow[i];
		if (bfrow != NULL) bvfrow = bfrow[i + abrow0];
		BRACKETFL z = 0.0;
		for (j = j1; j < j2; j++) {
			if (afcol != NULL) avfcol = afcol[j];
			if (bfcol != NULL) bvfcol = bfcol[j + abcol0];
			z += (BRACKETFL) avfcol * (BRACKETFL) bvfcol;
		}
		y += z * (BRACKETFL) avfrow * (BRACKETFL) bvfrow;
	}
	#endif
	x = (MLEFL) y;
	if (status != NULL) *status = SH_SUCCESS;
	return(x);
}

if (W != NULL) {

	if (W->type != TYPE_THPIX) {
		thError("%s: ERROR - weight matrix (W) should be of type (TYPE_THPIX)", name);
                return(MLEFLNAN);
        	}
	wnrow = W->nrow;
	wncol = W->ncol;

	wrow0 = W->row0;
	wcol0 = W->col0;
	awrow0 = arow0 - wrow0;
	awcol0 = acol0 - wcol0;

	i1 = MAX(i1, -awrow0);
	i2 = MIN(i2, wnrow - awrow0);
	j1 = MAX(j1, -awcol0);
	j2 = MIN(j2, wncol - awcol0);

	if (i2 <= i1 || j2 <= j1) {
		#if DEEP_DEBUG_MLETYPES
		thError("%s: WARNING - empty overlap", name);
		#endif
		if (status != NULL) *status = SH_SUCCESS;
		return((MLEFL) 0.0);
	}

}


if (mask != NULL) {

	#if THMASKisMASK

	MASKBIT maskbit = 0x0;
	MASKBIT *mrow;
	status2 = thLwmaskGetMaskbit(lwmask, &maskbit);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not get (maskbit) from (lwmask)", name);
		if (status != NULL) *status = status2;
		return(MLEFLNAN);
	}

	mnrow = mask->nrow;
	mncol = mask->ncol;

	mrow0 = mask->row0;
	mcol0 = mask->col0;
	amrow0 = arow0 - mrow0;
	amcol0 = acol0 - mcol0;

	i1 = MAX(i1, -amrow0);
	i2 = MIN(i2, mnrow - amrow0);
	j1 = MAX(j1, -amcol0);
	j2 = MIN(j2, mncol - amcol0);

	if (i2 <= i1 || j2 <= j1) {
		#if DEEP_DEBUG_MLETYPES
		thError("%s: WARNING - empty overlap", name);
		#endif
		if (status != NULL) *status = SH_SUCCESS;
		return((MLEFL) 0.0);
	}

	if (W == NULL) {
		BRACKETFL y = 0.0;
		for (i = i1; i < i2; i++) {
                	if (afrow != NULL) avfrow = afrow[i];
			if (bfrow != NULL) bvfrow = bfrow[i + abrow0];
			mrow = mask->rows[i + amrow0] + amcol0;
			BRACKETFL z = 0.0;
                	for (j = j1; j < j2; j++) {
				if (mrow[j] & maskbit) {
					if (afcol != NULL) avfcol = afcol[j];
					if (bfcol != NULL) bvfcol = bfcol[j + abcol0];
                        		z += (BRACKETFL) avfcol * (BRACKETFL) bvfcol;
					npix++;
				}
                	}
			y += z * (BRACKETFL) avfrow * (BRACKETFL) bvfrow;
		}
		x = (MLEFL) y;
	} else {
		BRACKETFL y = 0.0;
		for (i = i1; i < i2; i++) {
                	if (afrow != NULL) avfrow = afrow[i];
			if (bfrow != NULL) bvfrow = bfrow[i + abrow0];
			wrow = W->rows_thpix[i + awrow0] + awcol0;
			mrow = mask->rows[i + amrow0] + amcol0;
			BRACKETFL z = 0.0;
                	for (j = j1; j < j2; j++) {
				if (mrow[j] & maskbit) {
					if (afcol != NULL) avfcol = afcol[j];
                        		if (bfcol != NULL) bvfcol = bfcol[j + abcol0];
					z += (BRACKETFL) avfcol * (BRACKETFL) bvfcol * (BRACKETFL) wrow[j];
					npix++;
				}
                	}
			y += z * (BRACKETFL) avfrow * (BRACKETFL) bvfrow;
	        }
		 x = (MLEFL) y;
	}

	#if DEEP_DEBUG_MLETYPES
	if (npix == 0) {
		thError("%s: WARNING - empty overlap", name);
	}
	#endif
	if (status != NULL) *status = SH_SUCCESS;
	return(x); 
     	
	#endif

	#if THMASKisSUPERMASK


	MASKBIT maskbit = 0x0;
	MASKBIT *mrow;
	status = thLwmaskGetMaskbit(lwmask, &maskbit);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (maskbit) from (lwmask)", name);
		return(status);
	}

	mnrow = mask->nrow;
	mncol = mask->ncol;

	mrow0 = mask->row0;
	mcol0 = mask->col0;
	amrow0 = arow0 - mrow0;
	amcol0 = acol0 - mcol0;

	i1 = MAX(i1, -amrow0);
	i2 = MIN(i2, mnrow - amrow0);
	j1 = MAX(j1, -amcol0);
	j2 = MIN(j2, mncol - amcol0);

	if (i2 <= i1 || j2 <= j1) {
		#if DEEP_DEBUG_MLETYPES
		thError("%s: WARNING - empty overlap", name);
		#endif
		if (status != NULL) *status = SH_SUCCESS;
		return((MLEFL) 0.0);
	}

	if (W == NULL) {
		BRACKETFL y = 0.0;
		#if DO_KAHAN_SUM
		register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
		for (i = i1; i < i2; i++) {
			if (afrow != NULL) avfrow = afrow[i];	
			if (bfrow != NULL) bvfrow = bfrow[i + abrow0];
			mrow = mask->rows_u16[i + amrow0] + amcol0;
			for (j = j1; j < j2; j++) {
				if (afcol != NULL) avfcol = afcol[j];	
				if (bfcol != NULL) bvfcol = bfcol[j + abcol0];
				if (mrow[j] & maskbit) {
					y_item = (BRACKETFL) avfcol * (BRACKETFL) avfrow * (BRACKETFL) bvfrow * (BRACKETFL) bvfcol - y_error;
					y2 = y + y_item;
					y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
					y = y2;
					npix++;
				}
			}	
		}
		x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
		#else
		for (i = i1; i < i2; i++) {
                	if (afrow != NULL) avfrow = afrow[i];
			if (bfrow != NULL) bvfrow = bfrow[i + abrow0];
			mrow = mask->rows_u16[i + amrow0] + amcol0;
			BRACKETFL z = 0.0;
                	for (j = j1; j < j2; j++) {
				if (mrow[j] & maskbit) {
					if (afcol != NULL) avfcol = afcol[j];
					if (bfcol != NULL) bvfcol = bfcol[j + abcol0];
                        		z += (BRACKETFL) avfcol * (BRACKETFL) bvfcol;
					npix++;
				}
                	}
			y += z * (BRACKETFL) avfrow * (BRACKETFL) bvfrow;
	        }
		#endif
		x = (MLEFL) y;
	} else {
		BRACKETFL y = 0.0;
		#if DO_KAHAN_SUM
		register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
		for (i = i1; i < i2; i++) {
			if (afrow != NULL) avfrow = afrow[i];	
			if (bfrow != NULL) bvfrow = bfrow[i + abrow0];
			mrow = mask->rows_u16[i + amrow0] + amcol0;
			wrow = W->rows_thpix[i + awrow0] + awcol0;
			for (j = j1; j < j2; j++) {
				if (afcol != NULL) avfcol = afcol[j];	
				if (bfcol != NULL) bvfcol = bfcol[j + abcol0];
				if (mrow[j] & maskbit) {
					y_item = (BRACKETFL) avfcol * (BRACKETFL) avfrow * (BRACKETFL) bvfrow * (BRACKETFL) bvfcol * (BRACKETFL) wrow[j] - y_error;
					y2 = y + y_item;
					y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
					y = y2;
					npix++;
				}
			}	
		}
		x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
		#else
for (i = i1; i < i2; i++) {
                	if (afrow != NULL) avfrow = afrow[i];
			if (bfrow != NULL) bvfrow = bfrow[i + abrow0];
			wrow = W->rows_thpix[i + awrow0] + awcol0;
			mrow = mask->rows_u16[i + amrow0] + amcol0;
			BRACKETFL z = 0.0;
                	for (j = j1; j < j2; j++) {
				if (mrow[j] & maskbit) {
					if (afcol != NULL) avfcol = afcol[j];
					if (bfcol != NULL) bvfcol = bfcol[j + abcol0];
                        		z += (BRACKETFL) avfcol * (BRACKETFL) bvfcol * (BRACKETFL) wrow[j];
					npix++;
				}
                	}
			y += z * (BRACKETFL) avfrow * (BRACKETFL) bvfrow;
	        }
		#endif
		x = (MLEFL) y;
	}

	#if DEEP_DEBUG_MLETYPES
	if (npix == 0) {
		thError("%s: WARNING - empty overlap", name);
	}
	#endif

	if (status != NULL) *status = SH_SUCCESS;
	return(x); 
 
	#endif

	#if THMASKisOBJMASK
	
	mrow0 = mask->row0;
	mcol0 = mask->col0;
	amrow0 = arow0 - mrow0;
	amcol0 = acol0 - mcol0;

	int nspan;
	SPAN *s;
	s = mask->s;
	nspan = mask->nspan;
	if (mask->npix == 0) {
		thError("%s: WARNING - empty (objmask)", name);
		return((MLEFL) 0.0);
	}

	int x1, x2;
	if (W == NULL) {
		BRACKETFL y = 0.0;
		#if DO_KAHAN_SUM
		register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
		for (ispan = 0; ispan < nspan; ispan++) {
			i = s->y - amrow0;
			x1 = MAX(j1, s->x1 - amcol0);
			x2 = MIN(j2, 1 + s->x2 - amcol0);
			if (i >= i1 && i < i2) {
				if (afrow != NULL) avfrow = afrow[i];
				if (bfrow != NULL) bvfrow = bfrow[i + abrow0];
				BRACKETFL z = 0.0;
				for (j = x1; j < x2; j++) {
					if (afcol != NULL) avfcol = afcol[j];	
					if (bfcol != NULL) bvfcol = bfcol[j + abcol0];
					y_item = (BRACKETFL) avfcol * (BRACKETFL) avfrow * (BRACKETFL) bvfrow * (BRACKETFL) bvfcol - y_error;
					y2 = y + y_item;
					y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
					y = y2;
				}	
				npix += MAX(x2 - x1, 0);
			}	
		}
		x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
		#else
		for (ispan = 0; ispan < nspan; ispan++) {
			i = s->y - amrow0;
			x1 = MAX(j1, s->x1 - amcol0);
			x2 = MIN(j2, 1 + s->x2 - amcol0);
			if (i >= i1 && i < i2) {
				if (afrow != NULL) avfrow = afrow[i];
				if (bfrow != NULL) bvfrow = bfrow[i + abrow0];
				BRACKETFL z = 0.0;
				for (j = x1; j < x2; j++) {
					if (afcol != NULL) avfcol = afcol[j];
					if (bfcol != NULL) bvfcol = bfcol[j + abcol0];
					z += (BRACKETFL) avfcol * (BRACKETFL) bvfcol;
				}
				npix += MAX(x2 - x1, 0);
				y += z * (BRACKETFL) avfrow * (BRACKETFL) bvfrow;
			}	
		}
		#endif
		x = (MLEFL) y;
		#if DEEP_DEBUG_MLETYPES
		if (npix == 0) {
			thError("%s: WARNING - empty overlap", name);
		}
		#endif
		if (status != NULL) *status = SH_SUCCESS;
		return(x);
	} else {
		BRACKETFL y = 0.0;
		#if DO_KAHAN_SUM
		register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
		for (ispan = 0; ispan < nspan; ispan++) {
			i = s->y - amrow0;
			x1 = MAX(j1, s->x1 - amcol0);
			x2 = MIN(j2, 1 + s->x2 - amcol0);
			if (i >= i1 && i < i2) {
				if (afrow != NULL) avfrow = afrow[i];
				if (bfrow != NULL) bvfrow = bfrow[i + abrow0];
				wrow = W->rows_thpix[i + awrow0] + awcol0;
				BRACKETFL z = 0.0;
				for (j = x1; j < x2; j++) {
					if (afcol != NULL) avfcol = afcol[j];	
					if (bfcol != NULL) bvfcol = bfcol[j + abcol0];
					y_item = (BRACKETFL) avfcol * (BRACKETFL) avfrow * (BRACKETFL) bvfrow * (BRACKETFL) bvfcol * (BRACKETFL) wrow[j] - y_error;
					y2 = y + y_item;
					y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
					y = y2;
				}	
				npix += MAX(x2 - x1, 0);
			}	
		}
		x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
		#else
		for (ispan = 0; ispan < nspan; ispan++) {
			i = s->y - amrow0;
			x1 = MAX(j1, s->x1 - amcol0);
			x2 = MIN(j2, 1 + s->x2 - amcol0);
			if (i >= i1 && i < i2) {
				if (afrow != NULL) avfrow = afrow[i];
				if (bfrow != NULL) bvfrow = bfrow[i + abrow0];
				wrow = W->rows_thpix[i + awrow0] + awcol0;
				BRACKETFL z = 0.0;
				for (j = x1; j < x2; j++) {
					if (afcol != NULL) avfcol = afcol[j];
					if (bfcol != NULL) bvfcol = bfcol[j + abcol0];
					z += (BRACKETFL) avfcol * (BRACKETFL) bvfcol * (BRACKETFL) wrow[j];
				}
				npix += MAX(x2 - x1, 0);
				y += z * (BRACKETFL) avfrow * (BRACKETFL) bvfrow;
			}	
		}
		#endif
		x = (MLEFL) y;
		#if DEEP_DEBUG_MLETYPES
		if (npix == 0) {
			thError("%s: WARNING - empty overlap", name);
		}
		#endif
		if (status != NULL) *status = SH_SUCCESS;
		return(x);
	}
 	#endif

}

if (mask == NULL) {
	if (W == NULL) {
		thError("%s: ERROR - problematic source code", name);
		if (status != NULL) *status = SH_GENERIC_ERROR;
        	return(MLEFLNAN);
	}
	#if VERY_DEEP_DEBUG_MLETYPES
	printf("%s: a0= (%d, %d), b0= (%d, %d), w0= (%d, %d) \n", 
		name, arow0, acol0, brow0, bcol0, wrow0, wcol0);
	printf("%s: init= (%d, %d), end= (%d, %d), ab0= (%d, %d), aw0= (%d, %d) \n", 
		name, i1, j1, i2, j2, abrow0, abcol0, awrow0, awcol0);
	#endif	
	BRACKETFL y = 0.0;
	#if DO_KAHAN_SUM
	register BRACKETFL y2 = 0.0, y_item = 0.0, y_error = 0.0;
	for (i = i1; i < i2; i++) {
		if (afrow != NULL) avfrow = afrow[i];
		if (bfrow != NULL) bvfrow = bfrow[i + abrow0];
		wrow = W->rows_thpix[i + awrow0] + awcol0;
		for (j = j1; j < j2; j++) {
			if (afcol != NULL) avfcol = afcol[j];
			if (bfcol != NULL) bvfcol = bfcol[j + abcol0];
			y_item = (BRACKETFL) avfcol * (BRACKETFL) avfrow * (BRACKETFL) bvfrow * (BRACKETFL) bvfcol * (BRACKETFL) wrow[j] - y_error;
			y2 = y + y_item;
			y_error = (BRACKETFL) (y2 - y) - (BRACKETFL) y_item;
			y = y2;
		}	
	}	
	x_error = (MLEFL) (y_error + ((MLEFL) y - (BRACKETFL) y));
	#else

	for (i = i1; i < i2; i++) {
		if (afrow != NULL) avfrow = afrow[i];
		if (bfrow != NULL) bvfrow = bfrow[i + abrow0];
		wrow = W->rows_thpix[i + awrow0] + awcol0;
		BRACKETFL z = 0.0;
		for (j = j1; j < j2; j++) {
			if (afcol != NULL) avfcol = afcol[j];
			if (bfcol != NULL) bvfcol = bfcol[j + abcol0];
			z +=  (BRACKETFL) avfcol * (BRACKETFL) bvfcol* (BRACKETFL) wrow[j];
		}
		y += z * (BRACKETFL) avfrow * (BRACKETFL) bvfrow;
	}
	#endif
	x = (MLEFL) y;
	npix = MAX(i2 - i1, 0) * MAX(j2 - j1, 0);
	if (npix == 0) {
		thError("%s ERROR - problematic source code (no overlap while expecting)", name);
		if (status != NULL) *status = SH_GENERIC_ERROR;
		return(MLEFLNAN);	
	}	
	if (status != NULL) *status = SH_SUCCESS;
	return(x);
}

thError("%s: ERROR - problematic source code", name);
if (status != NULL) *status = SH_GENERIC_ERROR;
return(MLEFLNAN);

}




MLEFL thRegHalfBracket(REGION *reg1, LWMASK *lwmask, THREGION *threg2, RET_CODE *status) {
char *name = "thRegHalfBracket";
FABRICATION_FLAG fab2;
RET_CODE status2;
status2 = thRegGetFabFlag(threg2, &fab2);
if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not check fabrication status for (threg2)", name);
	if (status != NULL) *status = status2;
	return(MLEFLNAN);
}
if (fab2 != FAB && fab2 != PSFCONVOLVED) {
	thError("%s: ERROR - (threg2) should be FAB'ed or even PSFCONVOLVED", name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(MLEFLNAN);
}
if (fab2 == FAB) {
	thError("%s: WARNING - generally expect a PSFCONVOLVED region, came across a FAB'ed (threg2)", name);
}
int n2;
status2 = thRegGetNComponent(threg2, &n2);
if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not get (NComponent) for (threg2)", name);
	if (status != NULL) *status = status2;
	return(MLEFLNAN);
}

THREGION_TYPE type;
status2 = thRegGetType(threg2, &type);
if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not get type of (thregion)", name);
	if (status != NULL) *status = status2;
	return(MLEFLNAN);
}

MLEFL x = 0.0;
if (type == THREGION1_TYPE || type == THREGION2_TYPE) {

	int i2;
	THPIX a2;
	x = (MLEFL) 0.0;
	for (i2 = 0; i2 < n2; i2++) {
		REGION *reg2;
		NAIVE_MASK *mask2;
		status2 = thRegGetRegMaskA(threg2, &reg2, &mask2, &a2, NULL, i2);
		if (status2 != SH_SUCCESS) {
			thError("%s: ERROR - could not retrieve (reg, mask, a) for component (%d) in (threg2)", name, i2);
			if (status != NULL) *status = status2;
			return(MLEFLNAN);
		}
		/* at the moment mask information is not passed */
		x +=  ((MLEFL) a2) * thBracket(reg1, lwmask, reg2, &status2);
		if (status2 != SH_SUCCESS) {
			thError("%s: ERROR - could not perform the internal product between (reg1) and (reg2: %d)", name, i2);
			if (status != NULL) *status = status2;
			return(MLEFLNAN);
		}
	}
	
} else if (type == THREGION3_TYPE) {
	x = thReg3BracketReg(threg2->threg, lwmask, reg1, &status2);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not perform the dot product with (thregion3)", name);
		if (status != NULL) *status = status2;
		return(MLEFLNAN);
	}
} else {
	thError("%s: ERROR - unsupported (thregion_type)", name);
	if (status != NULL) *status = status2;
	return(MLEFLNAN);
}

if (status != NULL) *status = SH_SUCCESS;
return(x);
}



MLEFL thRegBracket(THREGION *threg1, LWMASK *lwmask, THREGION *threg2, RET_CODE *status) {
char *name = "thRegBracket";
FABRICATION_FLAG fab1, fab2;
RET_CODE status2;
status2 = thRegGetFabFlag(threg1, &fab1);
if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not check fabrication status for (threg1)", name);
	if (status != NULL) *status = status2;
	return(MLEFLNAN);
}
status2 = thRegGetFabFlag(threg2, &fab2);
if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not check fabrication status for (threg2)", name);
	if (status != NULL) *status = status2;
	return(MLEFLNAN);
}
if (fab1 != FAB && fab1 != PSFCONVOLVED) {
	if (schema_fabrication_flag == NULL) init_static_vars();
	thError("%s: ERROR - (threg1) should be FAB'ed or even PSFCONVOLVED - found '%s'", name, schema_fabrication_flag->elems[fab1].name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(MLEFLNAN);
}
if (fab2 != FAB && fab2 != PSFCONVOLVED) {
	if (schema_fabrication_flag == NULL) init_static_vars();
	thError("%s: ERROR - (threg2) should be FAB'ed or even PSFCONVOLVED - found '%s'", name, schema_fabrication_flag->elems[fab2].name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(MLEFLNAN);
}
if (fab1 == FAB) {
	thError("%s: WARNING - generally expect a PSFCOVOLVED region, came across a FAB'ed (threg1)", name);
}
if (fab2 == FAB) {
	thError("%s: WARNING - generally expect a PSFCONVOLVED region, came across a FAB'ed (threg2)", name);
}
int n1, n2;
THREGION_TYPE type1, type2;
status2 = thRegGetType(threg1, &type1);
if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not get type for (thregion1)", name);
	if (status != NULL) *status = status2;
	return(MLEFLNAN);
}
status2 = thRegGetType(threg2, &type2);
if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not get the type for (thregion2)", name);
	if (status != NULL) *status = status2;
	return(MLEFLNAN);
}

/* if thregion is constructed of one or more dervish REGIONs */
if ((type1 == THREGION1_TYPE || type1 == THREGION2_TYPE) && 
	(type2 == THREGION1_TYPE || type2 == THREGION2_TYPE)) {

	status2 = thRegGetNComponent(threg1, &n1);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not get (NComponent) from (threg1)", name);
		if (status != NULL) *status = status2;
		return(MLEFLNAN);
	}
	status2 = thRegGetNComponent(threg2, &n2);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not get (NComponent) from (threg2)", name);
		if (status != NULL) *status = status2;
		return(MLEFLNAN);
	}	
	int i1, i2;
	THPIX a1, a2;
	MLEFL x = 0.0;
	for (i1 = 0; i1 < n1; i1++) {
		REGION *reg1;
		NAIVE_MASK *mask1;
		status2 = thRegGetRegMaskA(threg1, &reg1, &mask1, &a1, NULL, i1);
		if (status2 != SH_SUCCESS) {
			thError("%s: ERROR - could not retrieve (reg, mask, a) for component (%d) in (threg1)", name, i1);
			if (status != NULL) *status = status2;
			return(MLEFLNAN);
		}
		if (reg1 == NULL) {
			thError("%s: WARNING - component region for (i1 = %d) found to be null", name, i1);
		}
		for (i2 = 0; i2 < n2; i2++) {
			REGION *reg2;
			NAIVE_MASK *mask2;
			status2 = thRegGetRegMaskA(threg2, &reg2, &mask2, &a2, NULL, i2);
			if (status2 != SH_SUCCESS) {
				thError("%s: ERROR - could not retrieve (reg, mask, a) for component (%d) in (threg2)", name, i2);
				if (status != NULL) *status = status2;
				return(MLEFLNAN);
			}
			if (reg2 == NULL) {
				thError("%s: WARNING - component region for (i2 = %d) found to be null", name, i2);
			}
			/* at the moment mask information is not passed */
			MLEFL xxx = thBracket(reg1, lwmask, reg2, &status2);
			if (status2 != SH_SUCCESS) {
				thError("%s: ERROR - could not perform the internal product between (reg1: %d) and (reg2: %d)", name, i1, i2);
				if (status != NULL) *status = status2;
				return(MLEFLNAN);
			}
			if (isnan(xxx) || isinf(xxx)) {
				thError("%s: ERROR - (NAN) detected at bracket value at (i1 = %d, i2 = %d)", name, i1, i2);
			}
			x += ((MLEFL) a1) * ((MLEFL) a2) * xxx;
			}
	}

	if (status != NULL) *status = SH_SUCCESS;
	if (isnan(x) || isinf(x)) {
		thError("%s: WARNING - bracket value sum doesn't pass (NAN) test", name);
	}
	return(x);

} else if (type1 != type2 && (type1 == THREGION3_TYPE || type2 == THREGION3_TYPE)) {
	THREGION *r1, *r2;
	if (type1 == THREGION3_TYPE) {
		r1 = threg1;
		r2 = threg2;
	} else {
		r1 = threg2;
		r2 = threg1;
	}

	status2 = thRegGetNComponent(r1, &n1);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not get (NComponent) from (r1)", name);
		if (status != NULL) *status = status2;
		return(MLEFLNAN);
	}
	status2 = thRegGetNComponent(r2, &n2);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not get (NComponent) from (r2)", name);
		if (status != NULL) *status = status2;
		return(MLEFLNAN);
	}	
	int i2;
	THPIX a2;
	MLEFL x = 0.0;
	for (i2 = 0; i2 < n2; i2++) {
		REGION *reg2;
		NAIVE_MASK *mask2;
		status2 = thRegGetRegMaskA(r2, &reg2, &mask2, &a2, NULL, i2);
		if (status2 != SH_SUCCESS) {
			thError("%s: ERROR - could not retrieve (reg, mask, a) for component (%d) in (r2)", name, i2);
			if (status != NULL) *status = status2;
			return(MLEFLNAN);
		}
		/* at the moment mask information is not passed */
		MLEFL xxx = thReg3BracketReg(r1->threg, lwmask, reg2, &status2);
		if (isnan(xxx) || isinf(xxx)) {
			thError("%s: ERROR - (NAN) detected at (i2 = %d)", name, i2);
		}
		x += ((MLEFL) a2) * xxx;
		if (status2 != SH_SUCCESS) {
			thError("%s: ERROR - could not perform the internal product between (r1) and (reg2: %d)", name, i2);
			if (status != NULL) *status = status2;
			return(MLEFLNAN);
		}
	}

	if (status != NULL) *status = SH_SUCCESS;
	if (isnan(x) ||  isinf(x)) {
		thError("%s: WARNING - bracket value doesn't pass (NAN) test", name);
	}
	return(x);
	
} else if (type1 == THREGION3_TYPE && type2 == THREGION3_TYPE) {
	
	MLEFL x = thReg3BracketReg3(threg1->threg, lwmask, threg2->threg,  &status2);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not perform the internal product between (threg1) and (threg2)", name);
		if (status != NULL) *status = status2;
		return(MLEFLNAN);
	}
	if (status != NULL) *status = SH_SUCCESS;
	if (isnan(x) ||  isinf(x)) {
		thError("%s: WARNING - bracket value doesn't pass (NAN) test", name);
	}
	return(x);
} else {
	if (schema_thregion_type == NULL) init_static_vars ();
	thError("%s: ERROR - unacceptable combination of type for (thregion1, thregion2) '%s', '%s'", name, (schema_thregion_type->elems[type1]).name, (schema_thregion_type->elems[type2]).name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(MLEFLNAN);
}

}

RET_CODE thPutModelXYComponent(REGION *m, int row0, int col0, int nrow, int ncol, THPIX *frow, THPIX *fcol, THPIX amp) {
char *name = "thPutModelXYComponent";

if (m == NULL) {
	thError("%s: ERROR - improperly allocated (region)", name);
	return(SH_GENERIC_ERROR);
}

REGION *b;
b = m;

int anrow, ancol, bnrow, bncol;

anrow = nrow;
ancol = ncol;
bnrow = b->nrow;
bncol = b->ncol;

if (b->type != TYPE_THPIX) {
	thError("%s: ERROR - only (TYPE_THPIX) pixel type is supported", name);
	return(SH_GENERIC_ERROR);
}       
if (anrow <= 0 || ancol <= 0) {
	thError("%s: WARNING - effectively null (f/f) region (nrow, ncol) = (%d, %d)", name, anrow, ancol);
	return(SH_SUCCESS);
}
if (bnrow == 0 || bncol == 0) {
	thError("%s: WARNING - effetively null (m) region (nrow, ncol) = (%d, %d)", 
	name, bnrow, bncol);
	return(SH_SUCCESS);
}

int arow0, acol0, brow0, bcol0, abrow0, abcol0;
arow0 = row0;
acol0 = col0;
brow0 = b->row0;
bcol0 = b->col0;
abrow0 = arow0 - brow0;
abcol0 = acol0 - bcol0;

int i1, i2, j1, j2;
i1 = MAX(0, - abrow0);
i2 = MIN(anrow, bnrow - abrow0);
j1 = MAX(0, - abcol0);
j2 = MIN(ancol, bncol - abcol0);

if (i2 >= i1 && j2 >= j1) {
THPIX *brow;
THPIX avfrow = 1.0, avfcol = 1.0;
int i, j;
for (i = i1; i < i2; i++) {
	if (frow != NULL) avfrow = frow[i];
	brow = b->rows_thpix[i + abrow0] + abcol0;	
	for (j = j1; j < j2; j++) {
		if (fcol != NULL) avfcol = fcol[j];
		brow[j] = amp * avfrow * avfcol;
	}
}

}

return(SH_SUCCESS);
}

RET_CODE thAddModelXYComponent(REGION *m, int row0, int col0, int nrow, int ncol, THPIX *frow, THPIX *fcol, THPIX amp) {
char *name = "thAddModelXYComponent";

if (m == NULL) {
	thError("%s: ERROR - improperly allocated (region)", name);
	return(SH_GENERIC_ERROR);
}

REGION *b;
b = m;

int anrow, ancol, bnrow, bncol;

anrow = nrow;
ancol = ncol;
bnrow = b->nrow;
bncol = b->ncol;

if (b->type != TYPE_THPIX) {
	thError("%s: ERROR - only (TYPE_THPIX) pixel type is supported", name);
	return(SH_GENERIC_ERROR);
}       
if (anrow <= 0 || ancol <= 0) {
	thError("%s: WARNING - effectively null (f/f) region (nrow, ncol) = (%d, %d)", 
	name, anrow, ancol);
	return(SH_SUCCESS);
}
if (bnrow == 0 || bncol == 0) {
	thError("%s: WARNING - effetively null (m) region (nrow, ncol) = (%d, %d)", 
	name, bnrow, bncol);
	return(SH_SUCCESS);
}

int arow0, acol0, brow0, bcol0, abrow0, abcol0;
arow0 = row0;
acol0 = col0;
brow0 = b->row0;
bcol0 = b->col0;
abrow0 = arow0 - brow0;
abcol0 = acol0 - bcol0;

int i1, i2, j1, j2;
i1 = MAX(0, - abrow0);
i2 = MIN(anrow, bnrow - abrow0);
j1 = MAX(0, - abcol0);
j2 = MIN(ancol, bncol - abcol0);

if (i2 >= i1 && j2 >= j1) {
THPIX *brow;
THPIX avfrow = 1.0, avfcol = 1.0;
int i, j;
for (i = i1; i < i2; i++) {
	if (frow != NULL) avfrow = frow[i];
	brow = b->rows_thpix[i + abrow0] + abcol0;	
	for (j = j1; j < j2; j++) {
		if (fcol != NULL) avfcol = fcol[j];
		brow[j] += amp * avfrow * avfcol;
	}
}

}

return(SH_SUCCESS);
}


RET_CODE thPutModelComponent(REGION *m, REGION *mi, THPIX amp) {
char *name = "thPutModelComponent";

if (m == NULL || mi == NULL) {
	thError("%s: ERROR - improperly allocated (region)", name);
	return(SH_GENERIC_ERROR);
}

REGION *a;
REGION *b;
a = mi;
b = m;

int anrow, ancol, bnrow, bncol;

anrow = a->nrow;
ancol = a->ncol;
bnrow = b->nrow;
bncol = b->ncol;

if (a->type != TYPE_THPIX || b->type != TYPE_THPIX) {
	thError("%s: ERROR - only (TYPE_THPIX) pixel type is supported", name);
	return(SH_GENERIC_ERROR);
}       
if (anrow == 0 || ancol == 0) {
	thError("%s: WARNING - effectively null (mi = '%s') region (nrow, ncol) = (%d, %d)", 
	name, mi->name, anrow, ancol);
	return(SH_SUCCESS);
}
if (bnrow == 0 || bncol == 0) {
	thError("%s: WARNING - effetively null (m) region (nrow, ncol) = (%d, %d)", 
	name, bnrow, bncol);
	return(SH_SUCCESS);
}

int arow0, acol0, brow0, bcol0, abrow0, abcol0;
arow0 = a->row0;
acol0 = a->col0;
brow0 = b->row0;
bcol0 = b->col0;
abrow0 = arow0 - brow0;
abcol0 = acol0 - bcol0;

int i1, i2, j1, j2;
i1 = MAX(0, - abrow0);
i2 = MIN(anrow, bnrow - abrow0);
j1 = MAX(0, - abcol0);
j2 = MIN(ancol, bncol - abcol0);

if (i2 >= i1 && j2 >= j1) {
THPIX *arow, *brow;
int i, j;
for (i = i1; i < i2; i++) {
	arow = a->rows_thpix[i];
	brow = b->rows_thpix[i + abrow0] + abcol0;	
	for (j = j1; j < j2; j++) {
		brow[j] = amp * arow[j];
	}
}

}

return(SH_SUCCESS);
}

RET_CODE thAddModelComponent(REGION *m, REGION *mi, THPIX amp) {
char *name = "thAddModelComponent";

if (m == NULL || mi == NULL) {
	thError("%s: ERROR - improperly allocated (region)", name);
	return(SH_GENERIC_ERROR);
}

REGION *a;
REGION *b;
a = mi;
b = m;

int anrow, ancol, bnrow, bncol;

anrow = a->nrow;
ancol = a->ncol;
bnrow = b->nrow;
bncol = b->ncol;

if (a->type != TYPE_THPIX || b->type != TYPE_THPIX) {
	thError("%s: ERROR - only (TYPE_THPIX) pixel type is supported", name);
	return(SH_GENERIC_ERROR);
}       
if (anrow == 0 || ancol == 0) {
	thError("%s: WARNING - effectively null (mi = '%s') region (nrow, ncol) = (%d, %d)", 
	name, mi->name, anrow, ancol);
	return(SH_SUCCESS);
}
if (bnrow == 0 || bncol == 0) {
	thError("%s: WARNING - effetively null (m) region (nrow, ncol) = (%d, %d)", 
	name, bnrow, bncol);
	return(SH_SUCCESS);
}

int arow0, acol0, brow0, bcol0, abrow0, abcol0;
arow0 = a->row0;
acol0 = a->col0;
brow0 = b->row0;
bcol0 = b->col0;
abrow0 = arow0 - brow0;
abcol0 = acol0 - bcol0;

int i1, i2, j1, j2;
i1 = MAX(0, - abrow0);
i2 = MIN(anrow, bnrow - abrow0);
j1 = MAX(0, - abcol0);
j2 = MIN(ancol, bncol - abcol0);

if (i2 >= i1 && j2 >= j1) {
THPIX *arow, *brow;
int i, j;
for (i = i1; i < i2; i++) {
	arow = a->rows_thpix[i];
	brow = b->rows_thpix[i + abrow0] + abcol0;	
	for (j = j1; j < j2; j++) {
		brow[j] += amp * arow[j];
	}
}

}

return(SH_SUCCESS);
}

RET_CODE thRegPutModelComponent(REGION *m, THREGION *mi, THPIX amp) {
char *name = "thRegPutModelComponent";

if (m == NULL || mi == NULL) {
	thError("%s: ERROR - improperly allocated (region)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
REGION *reg;
THPIX *frow, *fcol;
NAIVE_MASK *mask;
THPIX a, b;
int index, ncomponent = 0;
THREGION_TYPE regtype;
status = thRegGetNComponent(mi, &ncomponent);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract number of separate regions from (thregion)", name);
	return(status);
}
status = thRegGetType(mi, &regtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the extended region type", name);
	return(status);
}

for (index = 0; index < ncomponent; index++) {
	if (regtype != THREGION3_TYPE) {
		status = thRegGetRegMaskA(mi, &reg, &mask, &a, NULL, index);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not extract (region) from (thregion) for component (%d)", name, index);
			return(status);
		}
		b = a * amp;
		if (index == 0) {
			status = thPutModelComponent(m, reg, b);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not put component (%d) of (thregion) in model (region)", name, index);
				return(status);
			}
		} else {
			status = thAddModelComponent(m, reg, b);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not add component (%d) of (thregion) in model (region)", name, index);
				return(status);
			}
		}
	} else {
		/* 
		status = thRegGetRegMaskA(mi, NULL, NULL, &a, NULL, index);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not extract (region) from (thregion) for component (%d)", name, index);
			return(status);
		}
		*/
		int row0, col0, nrow, ncol;
		THREGION3 *reg3 = mi->threg;
		if (reg3 == NULL) {
			thError("%s: ERROR - null content in (mi)", name);
			return(SH_GENERIC_ERROR);
		}
		row0 = reg3->row0;
		col0 = reg3->col0;
		nrow = reg3->nrow;
		ncol = reg3->ncol; 
		frow = reg3->frow;
		fcol = reg3->fcol;
		b = amp;
		if (index == 0) {
			status = thPutModelXYComponent(m, row0, col0, nrow, ncol, frow, fcol, b);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not put component (%d) of (thregion) in model (region)", name, index);
				return(status);
			}
		} else {
			status = thAddModelXYComponent(m, row0, col0, nrow, ncol, frow, fcol, b);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not add component (%d) of (thregion) in model (region)", name, index);
				return(status);
			}
		}
	}
}

return(SH_SUCCESS);
}

RET_CODE thRegAddModelComponent(REGION *m, THREGION *mi, THPIX amp) {
char *name = "thRegAddModelComponent";

if (m == NULL || mi == NULL) {
	thError("%s: ERROR - improperly allocated (region)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
REGION *reg;
THPIX *frow, *fcol;
NAIVE_MASK *mask;
THPIX a, b;
int index, ncomponent = 0;
THREGION_TYPE regtype;
status = thRegGetNComponent(mi, &ncomponent);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract number of separate regions from (thregion)", name);
	return(status);
}
status = thRegGetType(mi, &regtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the extended region type", name);
	return(status);
}

for (index = 0; index < ncomponent; index++) {
	if (regtype != THREGION3_TYPE) {
		status = thRegGetRegMaskA(mi, &reg, &mask, &a, NULL, index);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not extract (region) from (thregion) for component (%d)", name, index);
			return(status);
		}
		b = a * amp;
		status = thAddModelComponent(m, reg, b);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not add component (%d) of (thregion) in model (region)", name, index);
			return(status);
		}
	} else {
		/* 
		status = thRegGetRegMaskA(mi, &reg, &mask, &a, NULL, index);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not extract (region) from (thregion) for component (%d)", name, index);
			return(status);
		}
		*/
		int row0, col0, nrow, ncol;
		THREGION3 *reg3 = mi->threg;
		if (reg3 == NULL) {
			thError("%s: ERROR - null content in (mi)", name);
			return(SH_GENERIC_ERROR);
		}
		row0 = reg3->row0;
		col0 = reg3->col0;
		nrow = reg3->nrow;
		ncol = reg3->ncol; 
		frow = reg3->frow;
		fcol = reg3->fcol;
		b = amp;
		status = thAddModelXYComponent(m, row0, col0, nrow, ncol, frow, fcol, b);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not add component (%d) of (thregion) in model (region)", name, index);
			return(status);
		}
	}
}

return(SH_SUCCESS);
}



RET_CODE thLstructGetLwmask (LSTRUCT *lstruct, LWMASK **lwmask) {
char *name = "thLstructGetLwmask";

if (lstruct == NULL) {
	thError("%s: ERROR - null (lstruct)", name);
	return(SH_GENERIC_ERROR);
}

if (lwmask == NULL && lstruct->lwmask != NULL) {
	thError("%s: WARNING - null placeholder for (lwmask), cannot pass contents", name);
	return(SH_SUCCESS);
}

RET_CODE status;
if (lstruct->lwmask == NULL) {
	thError("%s: WARNING - creating (lwmask) for (lstruct)", name);
	if (lstruct->ldata == NULL || lstruct->lmodel == NULL) {
		thError("%s: ERROR - improperly allocated (lstruct), cannot create (lwmask)", name);
		return(SH_GENERIC_ERROR);
	}
	LWMASK *lwmask2;
	lwmask2 = thLwmaskNew();
	status = thLwmaskPutW(lwmask2, lstruct->ldata->weight);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put (W) from (ldata) into (lwmask2)", name);
		return(status);
	}
	status = thLwmaskPutMask(lwmask2, lstruct->ldata->mask);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put (mask) from (ldata) onto (lwmask2)", name);
		return(status);
	}
	/* there is no bit mask for spankmask */
	#if (THMASKisMASK | THMASKisSUPERMASK)
	status = thLwmaskPutMaskbit(lwmask2, lstruct->lmodel->fitregbit);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put (maskbit) from (lmodel) into (lwmask2)", name);
		return(status);
	}
	#endif
	status = thLstructPutLwmask(lstruct, lwmask2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put (lwmask) into (lstruct)", name);
		return(status);
	}
}

if (lwmask != NULL) {
	*lwmask = lstruct->lwmask;
}

return(SH_SUCCESS);
}


RET_CODE thLDumpCov(LSTRUCT *lstruct) {
char *name = "thLDumpCov";

if (lstruct == NULL) {
	thError("%s: ERROR - null (lstruct) - no amplitude can be dumped");
	return(SH_GENERIC_ERROR);
}
if (lstruct->lmodel == NULL) {
	thError("%s: ERROR - null (lmodel) in (lstruct) - no (lmachine) was found");
	return(SH_GENERIC_ERROR);
}

LMACHINE *map;
map = lstruct->lmodel->lmachine;
LMODVAR *modvar;
modvar = lstruct->lmodel->modvar;
if (map == NULL) {
	thError("%s: ERROR - null (lmachine) in (lmodel) - cannot search for fit objects", name);
	return(SH_GENERIC_ERROR);
}
if (modvar == NULL) {
	thError("%s: ERROR - null (lmodvar) in (lmodel) - cannot locate the covariance matrix", name);
	return(SH_GENERIC_ERROR);
}
/* 

-- amplitude errors  --

get the list of fit models 

for each model get the object it belongs to

in that object find the specific record for that specific  model

find the record IErr_<modelname> in the io parameter and set its value to the fit value for the model amplitude 

*/

if (map->cflag != COMPILED) {
	thError("%s: ERROR - cannot process uncompiled map", name);
	return(SH_GENERIC_ERROR);
}

int i, namp, na;
namp = map->namp;
na = modvar->namp;
if (na != namp) {
	thError("%s: ERROR - (modvar, namp = %d) does not match (map, namp = %d)", name, na, namp);
	return(SH_GENERIC_ERROR);
}

if (namp < 0) {
	thError("%s: ERROR - non-standard map with (namp = %d)", name, namp);
	return(SH_GENERIC_ERROR);
} else if (namp == 0) {
	thError("%s: WARNING - no amplitudes were fit (namp = 0)", name);
	return(SH_SUCCESS);
} 
MLEFL **covap = modvar->covap;
if (covap == NULL && namp > 0) {
	thError("%s: ERROR - found (cov_ap = null) while (namp = %d)", name, namp);
	return(SH_GENERIC_ERROR);
}
MLEFL **invcovap = modvar->invcovap;
if (invcovap == NULL && namp > 0) {
	thError("%s: ERROR - found (invcov_ap = null) while (namp = %d)", name, namp);
	return(SH_GENERIC_ERROR);
}

CHAIN *alist;
alist = map->alist;
if (alist == NULL) {
	thError("%s: ERROR - null (alist) chain in (lmachine)", name);
	return(SH_GENERIC_ERROR);
}

if (shChainSize(alist) != namp) {
	thError("%s: ERROR - mismatch discovered between the size of (alist) and (namp)", name);
	return(SH_GENERIC_ERROR);
}

/* 
static char *ename = NULL;
if (ename == NULL) {
	ename = thCalloc(MX_STRING_LEN, sizeof(char));
}
*/
char ename[MX_STRING_LEN];
memset(ename, '\0', MX_STRING_LEN * sizeof(char));

MREC *mrec;
THOBJC *objc;
char *mname, *ioname;
TYPE iotype;
SCHEMA *s;

void *source, *target;
void *svalue, *tvalue;  
static char *stype = NULL, *ttype = NULL;
/* 
static char *esource = NULL, *etarget = NULL;
*/
char *estype, *ettype, *ettype_I, *ettype_I2;

char esource[MX_STRING_LEN];
memset(esource, '\0', MX_STRING_LEN * sizeof(char));
char etarget[MX_STRING_LEN];
memset(etarget, '\0', MX_STRING_LEN * sizeof(char));
char etarget_I[MX_STRING_LEN];
memset(etarget_I, '\0', MX_STRING_LEN * sizeof(char));
char etarget_I2[MX_STRING_LEN];
memset(etarget_I2, '\0', MX_STRING_LEN * sizeof(char));

/* 
if (esource == NULL) esource = thCalloc(MX_STRING_LEN, sizeof(char));
if (etarget == NULL) etarget = thCalloc(MX_STRING_LEN, sizeof(char));
*/

for (i = 0; i < namp; i++) {

	mrec = shChainElementGetByPos(alist, i);
	mname = mrec->mname;
	objc = mrec->objc;

	if (objc == NULL && mname != NULL) {
		thError("%s: ERROR - null (objc) for model (%d) of (map) (mname = %s)", name, i, mname);	
		return(SH_GENERIC_ERROR);
	} else if (objc == NULL && mname == NULL) {
		thError("%s: ERROR - null (objc) for model (%d) of (map) (NO MODEL NAME AVAILABLE)", name, i);
		return(SH_GENERIC_ERROR);
	}

	if (objc->iotype == UNKNOWN_SCHEMA) {
		if (objc->ioname == NULL || strlen(objc->ioname) == 0) {
		thError("%s: ERROR - unspecified type of parameter for (objc) in model (%d) of (objc: %d)", 
			name, i, objc->thid);
		return(SH_GENERIC_ERROR);
		}
		objc->iotype = shTypeGetFromName(objc->ioname);
	}

	iotype = objc->iotype;
	if (iotype == UNKNOWN_SCHEMA) {
		thError("%s: ERROR - unknown schema type for model (%d) of (objc: %d)", name, i, objc->thid);
		return(SH_GENERIC_ERROR);
	}
	if (objc->ioname == NULL) {
		objc->ioname = thCalloc(MX_STRING_LEN, sizeof(char));
		}
	ioname = objc->ioname;
	if (strlen(ioname) == 0) {
		strcpy(ioname, shNameGetFromType(iotype));
	}
	s = shSchemaGetFromType(iotype);
	if (objc->ioprop == NULL) {
		objc->ioprop = s->construct();
	}

	/* setting up the target */
	target = objc->ioprop;
	ttype = objc->ioname;
	if (mname != NULL && strlen(mname) != 0) {
		sprintf(etarget, "IErr_%s", mname);
		sprintf(etarget_I, "I_%s", mname);
	} else {
		sprintf(etarget, "IErr");
		sprintf(etarget_I, "I");
	}

	/* setting up the source */
	source = covap[i] + i;
	stype = "XARR";
	strcpy(esource, "ELEMENT");
	if (sizeof(MLEFL) == sizeof(FL32)) {
		estype = "FL32";
	} else if (sizeof(MLEFL) == sizeof(FL64)) {
		estype = "FL64";
	} else if (sizeof(MLEFL) == sizeof(FL96)) {
		estype = "FL96";
	} else {
		thError("%s: ERROR - (MLEFL) conversion not supported", name);
		return(SH_GENERIC_ERROR);
	}
	MLEFL variance = covap[i][i];
	if (variance < (MLEFL) 0.0) {
		thError("%s: WARNING - negative variance for (imodel = %d, model = '%s')", name, i, mname);
	}
	MLEFL std_err = sqrt(variance);
	svalue = (void *) &std_err;

	/* setting up the target */
	TYPE t;
	SCHEMA_ELEM *se, *se_I;
	t = shTypeGetFromName(ttype);
	se = shSchemaElemGetFromType(t, etarget);
	if (se == NULL) {
		thError("%s: ERROR - could not locate target - '%s' in '%s'", name, etarget, ttype);
		return(SH_GENERIC_ERROR);
	}
	tvalue = shElemGet(target, se, NULL);
	if (tvalue == NULL) {
		thError("%s: ERROR - could not locate target in memory - '%s' in '%s'", name, etarget, ttype);
		return(SH_GENERIC_ERROR);
	}
	ettype = se->type;

	RET_CODE status;
	#if DEBUG_QQ_TRANS
	printf("%s: tranforming '%s' of '%s' onto '%s' of '%s' \n", name, esource, stype, etarget, ttype);
	#endif

	status = thqqTrans(svalue, estype, tvalue, ettype);
	if (status != SH_SUCCESS) {
		thError("%s: failed at tranforming '%s' of '%s' onto '%s' of '%s'", 
		name, esource, stype, etarget, ttype);
		return(status);
	}

	se_I = shSchemaElemGetFromType(t, etarget_I);
	if (se_I == NULL) {
		thError("%s: ERROR - could not locate target - '%s' in '%s'", name, etarget_I, ttype);
		return(SH_GENERIC_ERROR);
	}
	void *tvalue_I = shElemGet(target, se_I, NULL);
	if (tvalue_I == NULL) {
		thError("%s: ERROR - could not locate target in memory - '%s' in '%s'", name, etarget_I, ttype);
		return(SH_GENERIC_ERROR);
	}
	ettype_I = se_I->type;

	MLEFL I_i = 0.0;
	status = thqqTrans(tvalue_I, ettype_I, &I_i, estype);
	if (status != SH_SUCCESS) {
		thError("%s: failed at tranforming '%s' of '%s' onto '%s' of '%s'", 
		name, etarget_I, ttype, "I_i", stype);
		return(status);
	}

	#if DEBUG_MLE_TYPES_DUMP_COVARIANCE
	printf("%s: mapping variance(imodel = %d, model = '%s') onto (ioprop = '%s'), varinace = %20.10G \n", name, i, mname, ioname, (double) variance);
	if (!strcmp(ioname, "OBJCPARS")) {
		OBJCPARS *objcpars = target;
		printf("%s: I_deV = %20.10G, IErr_deV = %20.10G \n", name, (double) objcpars->I_deV, (double) objcpars->IErr_deV);
	}	
	#endif
	
	int namp_objc = 0.0;
	MLEFL chisq_objc = 0.0;
	int j;
	for (j = 0; j < namp; j++) {
		
		MREC *mrec2 = shChainElementGetByPos(alist, j);
		char *mname2 = mrec2->mname;
		THOBJC *objc2 = mrec2->objc;
		if (objc2 == objc) {

			if (mname2 != NULL && strlen(mname2) != 0) {
				sprintf(etarget_I2, "I_%s", mname2);
			} else {
				sprintf(etarget_I2, "I");
			}

			SCHEMA_ELEM *se_I2 = shSchemaElemGetFromType(t, etarget_I2);
			if (se_I2 == NULL) {
				thError("%s: ERROR - could not locate target - '%s' in '%s'", name, etarget_I2, ttype);
				return(SH_GENERIC_ERROR);
			}
			void *tvalue_I2 = shElemGet(target, se_I2, NULL);
			if (tvalue_I2 == NULL) {
				thError("%s: ERROR - could not locate target in memory - '%s' in '%s'", name, etarget_I2, ttype);
				return(SH_GENERIC_ERROR);
			}
			ettype_I2 = se_I2->type;

			MLEFL I_j = 0.0;
			status = thqqTrans(tvalue_I2, ettype_I2, &I_j, estype);
			if (status != SH_SUCCESS) {
				thError("%s: failed at tranforming '%s' of '%s' onto '%s' of '%s'", 
				name, etarget_I2, ttype, "I_j", stype);
				return(status);
			}

			namp_objc++;
			chisq_objc += I_i * I_j * invcovap[i][j];
		}	
	}

	char *etarget_chisq = "chisq_model";
	SCHEMA_ELEM *se_chisq = shSchemaElemGetFromType(t, etarget_chisq);
	if (se_chisq == NULL) {
		thError("%s: WARNING - could not locate target - '%s' in '%s'", name, etarget_chisq, ttype);
	} else {
		void *tvalue_chisq = shElemGet(target, se_chisq, NULL);
		if (tvalue_chisq == NULL) {
			thError("%s: ERROR - could not locate target in memory - '%s' in '%s'", name, etarget_chisq, ttype);
			return(SH_GENERIC_ERROR);
		}
		char *ettype_chisq = se_chisq->type;

		status = thqqTrans(&chisq_objc, estype, tvalue_chisq, ettype_chisq);
		if (status != SH_SUCCESS) {
			thError("%s: failed at tranforming '%s' of '%s' onto '%s' of '%s'", 
			name, "chisq_objc", stype, etarget_chisq, ttype);
			return(status);
		}
	}
	char *etarget_namp = "namp_model";
	SCHEMA_ELEM *se_namp = shSchemaElemGetFromType(t, etarget_namp);
	if (se_namp == NULL) {
		thError("%s: WARNING - could not locate target - '%s' in '%s'", name, etarget_namp, ttype);
	} else {
		void *tvalue_namp = shElemGet(target, se_namp, NULL);
		if (tvalue_namp == NULL) {
			thError("%s: ERROR - could not locate target in memory - '%s' in '%s'", name, etarget_namp, ttype);
			return(SH_GENERIC_ERROR);
		}

		char *ettype_namp = se_namp->type;
		status = thqqTrans(&namp_objc, ettype_namp, tvalue_namp, ettype_namp);
		if (status != SH_SUCCESS) {
			thError("%s: failed at tranforming '%s' of '%s' onto '%s' of '%s'", 
			name, "namp_objc", stype, etarget_namp, ttype);
			return(status);
		}
	}

}

return(SH_SUCCESS);
}




RET_CODE thLDumpAmp(LSTRUCT *lstruct, ASSIGNFLAG aflag) {
char *name = "thLDumpAmp";

if (lstruct == NULL) {
	thError("%s: ERROR - null (lstruct) - no amplitude can be dumped");
	return(SH_GENERIC_ERROR);
}
if (lstruct->lmodel == NULL) {
	thError("%s: ERROR - null (lmodel) in (lstruct) - no (lmachine) was found");
	return(SH_GENERIC_ERROR);
}

LMACHINE *map;
map = lstruct->lmodel->lmachine;

if (map == NULL) {
	thError("%s: ERROR - null (lmachine) in (lmodel) - cannot search for fit objects");
	return(SH_GENERIC_ERROR);
}

/* 

-- amplitudes --

get the list of fit models 

for each model get the object it belongs to

in that object find the specific record for that specific  model

find the record I_<modelname> in the io parameter and set its value to the fit value for the model amplitude 

*/

if (map->cflag != COMPILED) {
	thError("%s: ERROR - cannot process uncompiled map", name);
	return(SH_GENERIC_ERROR);
}

int i, namp;
namp = map->namp;

if (namp < 0) {
	thError("%s: ERROR - non-standard map with (namp = %d)", name, namp);
	return(SH_GENERIC_ERROR);
} else if (namp == 0) {
	thError("%s: WARNING - no amplitudes were fit (namp = 0)", name);
	return(SH_SUCCESS);
} 

MLEFL *as = NULL;
as = lstruct->lmodel->a;
if (as == NULL) {
	thError("%s: ERROR - expected non-null amplitude array (a) in (lmodel)", name);
	return(SH_GENERIC_ERROR);
} 
MLEFL *aN = NULL;
aN = lstruct->lwork->aN;
if (aN == NULL) {
	thError("%s: ERROR - expected non-null amplitude array (aN) in (lwork)", name);
	return(SH_GENERIC_ERROR);
}

CHAIN *alist;
alist = map->alist;
if (alist == NULL) {
	thError("%s: ERROR - null (alist) chain in (lmachine)", name);
	return(SH_GENERIC_ERROR);
}

if (shChainSize(alist) != namp) {
	thError("%s: ERROR - mismatch discovered between the size of (alist) and (namp)", name);
	return(SH_GENERIC_ERROR);
}

static char *ename = NULL;
if (ename == NULL) {
	ename = thCalloc(MX_STRING_LEN, sizeof(char));
}

#if DEBUG_MLE_TYPES
int bad_amplitude = 0;
for (i = 0; i < namp; i++) {
	if (fabs(as[i]) < 1.0E-5) bad_amplitude++;
}
printf("%s: number of bad amplitudes (a) found = %d out of %d \n", name, bad_amplitude, namp);
bad_amplitude = 0;
for (i = 0; i < namp; i++) {
	if (fabs(aN[i]) < 1.0E-5) bad_amplitude++;
}
printf("%s: number of bad amplitudes (aN) found = %d out of %d\n", name, bad_amplitude, namp);
#endif

MREC *mrec;
THOBJC *objc;
char *mname, *ioname;
TYPE iotype;
SCHEMA *s;

void *source, *target;
void *svalue, *tvalue; 
static char *stype = NULL, *ttype = NULL;
static char *esource = NULL, *etarget = NULL;
char *estype, *ettype;

if (esource == NULL) esource = thCalloc(MX_STRING_LEN, sizeof(char));
if (etarget == NULL) etarget = thCalloc(MX_STRING_LEN, sizeof(char));

char *afname =  shEnumNameGetFromValue("ASSIGNFLAG", aflag);
for (i = 0; i < namp; i++) {

	mrec = shChainElementGetByPos(alist, i);
	mname = mrec->mname;
	objc = mrec->objc;
	
	if (objc == NULL && mname != NULL) {
		thError("%s: ERROR - null (objc) for model (%d) of (map) (mname = %s)", name, i, mname);	
		return(SH_GENERIC_ERROR);
	} else if (objc == NULL && mname == NULL) {
		thError("%s: ERROR - null (objc) for model (%d) of (map) (NO MODEL NAME AVAILABLE)", name, i);
		return(SH_GENERIC_ERROR);
	}

	if (objc->iotype == UNKNOWN_SCHEMA) {
		if (objc->ioname == NULL || strlen(objc->ioname) == 0) {
		thError("%s: ERROR - unspecified type of parameter for (objc) in model (%d) of (objc: %d)", 
			name, i, objc->thid);
		return(SH_GENERIC_ERROR);
		}
		objc->iotype = shTypeGetFromName(objc->ioname);
	}

	iotype = objc->iotype;
	if (iotype == UNKNOWN_SCHEMA) {
		thError("%s: ERROR - unknown schema type for model (%d) of (objc: %d)", name, i, objc->thid);
		return(SH_GENERIC_ERROR);
	}
	if (objc->ioname == NULL) {
		objc->ioname = thCalloc(MX_STRING_LEN, sizeof(char));
		}
	ioname = objc->ioname;
	if (strlen(ioname) == 0) {
		strcpy(ioname, shNameGetFromType(iotype));
	}
	s = shSchemaGetFromType(iotype);
	if (objc->ioprop == NULL) {
		objc->ioprop = s->construct();
	}

/* setting up the target */
if (aflag == XTOIO || aflag == MODELTOIO || aflag == XNTOIO) {
	target = objc->ioprop;
	ttype = objc->ioname;
	if (mname != NULL && strlen(mname) != 0) {
		sprintf(etarget, "I_%s", mname);
	} else {
		sprintf(etarget, "I");
	}
} else if (aflag == XTOMODEL || aflag == IOTOMODEL || aflag == XNTOMODEL) {
	target = mrec->ps; /* model-specific parameter */
	ttype = mrec->pname;
	strcpy(etarget, "I");
} else if (aflag == MODELTOX || aflag == IOTOX) {
	target = as + i;
	ttype = "XARR";
	strcpy(etarget, "ELEMENT");
	if (sizeof(MLEFL) == sizeof(FL32)) {
		ettype = "FL32";
	} else if (sizeof(MLEFL) == sizeof(FL64)) {
		ettype = "FL64";
	} else if (sizeof(MLEFL) == sizeof(FL96)) {
		ettype = "FL96";
	} else {
		thError("%s: ERROR - (MLEFL) conversion not supported", name);
		return(SH_GENERIC_ERROR);
	}
} else if (aflag == MODELTOXN || aflag == IOTOXN) {
	target = aN + i;
	ttype = "XARR";
	strcpy(etarget, "ELEMENT");
	if (sizeof(MLEFL) == sizeof(FL32)) {
		ettype = "FL32";
	} else if (sizeof(MLEFL) == sizeof(FL64)) {
		ettype = "FL64";
	} else if (sizeof(MLEFL) == sizeof(FL96)) {
		ettype = "FL96";
	} else {
		thError("%s: ERROR - (MLEFL) conversion not supported", name);
		return(SH_GENERIC_ERROR);
	}
} else {
	thError("%s: ERROR - assign flag (aflag = '%s', %d) not supported", name, afname, aflag);
	return(SH_GENERIC_ERROR);
}

/* setting up the source */
if (aflag == MODELTOIO || aflag == MODELTOX || aflag == MODELTOXN) {
	source = mrec->ps; /* model specific parameter */
	stype = mrec->pname;
	strcpy(esource, "I"); /* I of model */
} else if (aflag == XTOIO || aflag == XTOMODEL) {
	source = as + i;
	stype = "XARR";
	strcpy(esource, "ELEMENT");
	if (sizeof(MLEFL) == sizeof(FL32)) {
		estype = "FL32";
	} else if (sizeof(MLEFL) == sizeof(FL64)) {
		estype = "FL64";
	} else if (sizeof(MLEFL) == sizeof(FL96)) {
		estype = "FL96";
	}else {
		thError("%s: ERROR - (MLEFL) conversion not supported", name);
		return(SH_GENERIC_ERROR);
	}
} else if (aflag == IOTOMODEL || aflag == IOTOX || aflag == IOTOXN) {
	source = objc->ioprop;
	stype = objc->ioname;
	if (mname != NULL && strlen(mname) != 0) {
		sprintf(esource, "I_%s", mname);
	} else {
		sprintf(esource, "I");
	}
} else if (aflag == XNTOIO || aflag == XNTOMODEL) {
	source = aN + i;
	stype = "XARR";
	strcpy(esource, "ELEMENT");
	if (sizeof(MLEFL) == sizeof(FL32)) {
		estype = "FL32";
	} else if (sizeof(MLEFL) == sizeof(FL64)) {
		estype = "FL64";
	} else if (sizeof(MLEFL) == sizeof(FL96)) {
		estype = "FL96";
	} else {
		thError("%s: ERROR - (MLEFL) conversion not supported", name);
		return(SH_GENERIC_ERROR);
	}
} else {	
	thError("%s: ERROR - assign flag (aflag = '%s', %d) not supported", name, afname, aflag);
	return(SH_GENERIC_ERROR);
}

/* setting up source */
if (aflag != XTOIO && aflag != XTOMODEL && aflag != XNTOIO && aflag != XNTOMODEL) {

	TYPE t;
	SCHEMA_ELEM *se;
	t = shTypeGetFromName(stype);
	se = shSchemaElemGetFromType(t, esource);
	if (se == NULL) {
		thError("%s: ERROR - could not locate source - '%s' in '%s'", name, esource, stype);
		return(SH_GENERIC_ERROR);
	}
	svalue = shElemGet(source, se, NULL);
	if (svalue == NULL) {
		thError("%s: ERROR - could not locate source in memory - '%s' in '%s'", name, esource, stype);
		return(SH_GENERIC_ERROR);
	}
	estype = se->type;
} else if (aflag == XTOIO || aflag == XTOMODEL) {
	svalue = as + i;
} else if (aflag == XNTOIO || aflag == XNTOMODEL) {
	svalue = aN + i;
} else {
	thError("%s: ERROR - assign flag (aflag = '%s', %d) not supported", name, afname, aflag);
	return(SH_GENERIC_ERROR);
}

/* setting up the target */
if (aflag != IOTOX && aflag != MODELTOX && aflag != IOTOXN && aflag != MODELTOXN) {

	TYPE t;
	SCHEMA_ELEM *se;
	t = shTypeGetFromName(ttype);
	se = shSchemaElemGetFromType(t, etarget);
	if (se == NULL) {
		thError("%s: ERROR - could not locate target - '%s' in '%s'", name, etarget, ttype);
		return(SH_GENERIC_ERROR);
	}
	tvalue = shElemGet(target, se, NULL);
	if (tvalue == NULL) {
		thError("%s: ERROR - could not locate target in memory - '%s' in '%s'", name, etarget, ttype);
		return(SH_GENERIC_ERROR);
	}
	ettype = se->type;

} else if (aflag == IOTOX || aflag == MODELTOX) {
	tvalue = as + i;
} else if (aflag == IOTOXN || aflag == MODELTOXN) {
	tvalue = aN + i;
} else {
	thError("%s: ERROR - assign flag (aflag = '%s', %d) not supported", name, afname, aflag);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
#if DEBUG_QQ_TRANS
printf("%s: tranforming '%s' of '%s' onto '%s' of '%s' \n", name, esource, stype, etarget, ttype);
#endif
status = thqqTrans(svalue, estype, tvalue, ettype);
if (status != SH_SUCCESS) {
	thError("%s: failed at tranforming '%s' of '%s' onto '%s' of '%s'", 
	name, esource, stype, etarget, ttype);
	return(status);
}

}

return(SH_SUCCESS);
}

RET_CODE thObjcTransIo(THOBJC *objc, ASSIGNFLAG aflag) {
char *name = "thObjcTansIo";

/*

This function seems to be called as many times as the number of models in an object
It was however designed to be called once per object. This has to be figured out
Feb 7, 2012

This probem was resolved by inveting a unique object list and placing it in LMACHINE

-- nonlinear parameters -- 

obtain the list of objects for the fit

for each object do the following:

fetch the schema of the io-parameter

search within the list of model parameters 

for a parameter name "p" and model name "model" searh for a variable like "p_model" in the io-list if it doesn't exist go for a name like "p"; this is useful when a shared parameter exists between models, such as xc, yc, or when one separates the parameters 

*/

if (aflag != MODELTOIO && aflag != IOTOMODEL) {
	thError("%s: ERROR - unsupported 'ASSIGNFLAG' (aflag)", name);
	return(SH_GENERIC_ERROR);
}

static char *ename, *suffix;
if (ename == NULL) ename = thCalloc(MX_STRING_LEN, sizeof(char));
if (suffix == NULL) suffix = thCalloc(MX_STRING_LEN, sizeof(char));

int j, k;
char *mname, *pname, *xtype, *xiotype, *ioname;
void *p, *pio, *x, *xio;

SCHEMA *s;
SCHEMA_ELEM *se, *seio;
TYPE t, iotype;

THOBJCID objcid;
CHAIN *mprops;
THPROP *mprop;

if (objc == NULL) {
	thError("%s: ERROR - null (objc)", name);
	return(SH_GENERIC_ERROR);
}

objcid = objc->thid;
/* dealing with io parameter, its type and name */
if (objc->iotype == UNKNOWN_SCHEMA) {
	if (objc->ioname == NULL || strlen(objc->ioname) == 0) {
		thError("%s: ERROR - unspecified type of parameter for (objc = %p)  (objcid: %d)", name, (void *) objc, objcid);
		return(SH_GENERIC_ERROR);
	}
	objc->iotype = shTypeGetFromName(objc->ioname);
}		

iotype = objc->iotype;
if (iotype == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - unknown schema type for  (objc = %p) of (objcid: %d)", name, (void *) objc, objc->thid);
	return(SH_GENERIC_ERROR);
}

if (objc->ioname == NULL) objc->ioname = thCalloc(MX_STRING_LEN, sizeof(char));
ioname = objc->ioname;
if (strlen(ioname) == 0) strcpy(ioname, shNameGetFromType(iotype));

#if 0
/* debugging purposed only */
printf("%s: (objcid: %d), (ioname: %s) \n", name, objcid, ioname);
#endif

s = shSchemaGetFromType(iotype);
if (objc->ioprop == NULL) {
	objc->ioprop = s->construct();
}
pio = objc->ioprop;
		
/* now going model by model */
int nm = 0;
mprops = objc->thprop;
if (mprops == NULL) {
	thError("%s: ERROR - null (thprops) in (thobjc) for (objc: %d)", name, objcid);
	return(SH_GENERIC_ERROR);
} 

nm = shChainSize(mprops);
if (nm == 0) {
	thError("%s: ERROR - empty (thprops) in (thobjc) for (objc: %d)", name, objcid);
	return(SH_GENERIC_ERROR);
}
for (j = 0; j < nm; j++) {
	mprop = shChainElementGetByPos(mprops, j);
	if (mprop == NULL) {
		thError("%s: WARNING - empty (thprop) in (thprops) at position (%d) for (objc: %d)", name, j, objcid);
	} else {
		pname = mprop->pname;
		mname = mprop->mname;
		p = mprop->value;
		if (mname == NULL || strlen(mname) == 0) {
			thError("%s: WARNING - non-existent name for model (%d) of (objc: %d)", name, j, objcid);
			strcpy(suffix, "");
		} else {
			sprintf(suffix, "_%s", mname);
		}
		if ((pname == NULL || strlen(pname) == 0) && mname != NULL) {
			thError("%s: WARNING - unknown type, null (pname) for model parameter (%s) at position (%d) of (objc: %d) - no io translation to be done", name, mname, j, objcid);
			t = UNKNOWN_SCHEMA;
		} else if ((pname == NULL || strlen(pname) == 0) && mname == NULL) {
			thError("%s: WARNING - unknown type, null (pname) for model parameter at position (%d) of (objc: %d) - no io translation to be done", name, j, objcid);
			t = UNKNOWN_SCHEMA;
		} else {
			t = shTypeGetFromName(pname);
			if (t == UNKNOWN_SCHEMA) {
			thError("%s: WARNING - no schema available for parameter of type (%s) at position (%d) of (objc: %d) - no io translation to be done", name, pname, objcid);
		} else {
			s = shSchemaGetFromType(t);
			for (k = 0; k < s->nelem; k++) {
				se = s->elems + k;
				sprintf(ename, "%s%s", se->name, suffix);	
				seio = shSchemaElemGetFromType(iotype, ename);
				if (seio == NULL && j == 0) { /* only for the main model one might drop such suffixes as _model */
					strcpy(ename, se->name);
					seio = shSchemaElemGetFromType(iotype, ename);
				}
				if (seio != NULL) {
					x = shElemGet(p, se, NULL);
					xio = shElemGet(pio, seio, NULL);
					if (x != NULL) {
						if (xio == NULL) {
						thError("%s: WARNING - unallocated argument (%s) in (%s) for (objc: %d) - no io translation can be done", name, ename, ioname, objcid);
						} else {
							xtype = se->type;
							xiotype = seio->type;
							RET_CODE status;
						if (aflag == IOTOMODEL) {	
							#if DEBUG_QQ_TRANS
							printf("%s: transform IO record '%s' ('%s in '%s') onto model record '%s' ('%s' in '%s') \n", name, seio->name, xiotype, ioname, se->name, xtype, pname);
							#endif
							status = thqqTrans(xio, xiotype, x, xtype);
							if (status != SH_SUCCESS) {
								thError("%s: ERROR - could not transform IO record '%s' ('%s in '%s') onto model record '%s' ('%s' in '%s')", name, seio->name, xiotype, ioname, se->name, xtype, pname);
							return(status);
							}	
						} else if (aflag == MODELTOIO) {
							#if DEBUG_QQ_TRANS
							printf("%s: transform model record '%s' ('%s in '%s') onto IO record '%s' ('%s' in '%s') \n", name, se->name, xtype, pname, seio->name, xiotype, ioname);
							#endif
							status = thqqTrans(x, xtype, xio, xiotype);
							if (status != SH_SUCCESS) {
								thError("%s: ERROR - could not transform model record '%s' ('%s in '%s') onto IO record '%s' ('%s' in '%s')", name, se->name, xtype, pname, seio->name, xiotype, ioname);
							return(status);
							}
					
						}	
						}	
					}
				}	
			}
						#if 0	
							/* conversion of counts in model for start to counts_star in io is done correctly July 9, 2016 */
							if (!strcmp(mname, "star")) {
								OBJCPARS *io = pio;
								STARPARS *star = p;
								printf("%s: STAR: model (counts, I) = (%g, %g) --  io(counts, I)  = (%g, %g) \n", name, star->counts, star->I, io->counts_star, io->I_star);
							}
						#endif



		}
		}
	}
}		

return(SH_SUCCESS);
}

RET_CODE thLXDumpIo(LSTRUCT *lstruct, ASSIGNFLAG aflag) {
char *name = "thLXDumpIo";
if (lstruct == NULL) {
	thError("%s: ERROR - null (mapmachine)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
LMACHINE *map = NULL;
status = thLstructGetLmachine(lstruct, &map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not derive the (lmachine) from (lstruct)", name);
	return(status);
}

if (aflag != XTOIO && aflag != IOTOX && aflag != XNTOIO && aflag != IOTOXN) {
	thError("%s: ERROR - unsupported 'ASSIGNFLAG' (aflag)", name);
	return(SH_GENERIC_ERROR);
}

MLEFL *p, *pN;
int np = 0;
status = ExtractXxNdX(lstruct, PSECTOR, NULL, NULL, &p, &pN, NULL, &np);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract (p, pN) from (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
static char *mlefl_type = NULL;
if (mlefl_type == NULL) {
	mlefl_type = thCalloc(MX_STRING_LEN, sizeof(char));
	if (sizeof(MLEFL) == sizeof(FL32)) {
		strcpy(mlefl_type, "FL32");
	} else if (sizeof(MLEFL) == sizeof(FL64)) {
		strcpy(mlefl_type, "FL64");
	} else if (sizeof(MLEFL) == sizeof(FL96)) {
		strcpy(mlefl_type, "FL96");
	} else {
		thError("%s: ERROR - unsupported (MLEFL)", name);
		thFree(mlefl_type);
		mlefl_type = NULL;
		return(SH_GENERIC_ERROR);
	}
}

char *ename, ***enames;
char *mname, **mnames;
enames = map->enames;
mnames = map->mnames;

void *x, *xN;
int **ploc, *nmpar;
ploc = map->ploc;
nmpar = map->nmpar;

void *iox, *iop;
char *ioname, *iortype;
static char *iorname = NULL;
if (iorname == NULL) iorname = thCalloc(MX_STRING_LEN, sizeof(char));
TYPE iotype, ioxtype;

int i, j, nm;
THOBJC *objc, **objcs;
status = thMapmachineGetObjcs(map, (void ***) &objcs, &nm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract list of objects from (lmachine)", name);
	return(status);
}

for (i = 0; i < nm; i++) {
	int nm_of_objc = 0;
	objc = objcs[i];
	nm_of_objc = shChainSize(objc->thprop);
	iop = objc->ioprop;
	ioname = objc->ioname;
	if (ioname == NULL) {
		objc->ioname = thCalloc(MX_STRING_LEN, sizeof(char));
		ioname = objc->ioname;
	}
	if (strlen(ioname) == 0 || 
		(iotype = shTypeGetFromName(ioname)) == UNKNOWN_SCHEMA) {
		iotype = objc->iotype;
	}
	if (iotype == UNKNOWN_SCHEMA) {
		thError("%s: ERROR - unloaded parameter type for io-par '%s' in (objc: %d)", 
		name, ioname, objc->thid);
		return(SH_GENERIC_ERROR);
	}
	if (strlen(ioname) == 0) strcpy(ioname, shNameGetFromType(iotype));	


	mname = mnames[i];
	for (j = 0; j < nmpar[i]; j++) {

	SCHEMA_ELEM *iose;
	/* getting the io-record */	
	ename = enames[i][j];
	sprintf(iorname, "%s_%s", ename, mname);
	iose = shSchemaElemGetFromType(iotype, iorname);
	if (iose == NULL) {
		if (!strcmp(ename, "xc") || !strcmp(ename, "yc") || nm_of_objc == 1) {
		strcpy(iorname, ename);
		iose = shSchemaElemGetFromType(iotype, iorname);
		}
	}
	if (iose == NULL) {
		thError("%s: ERROR - undeclared record '%s' in '%s'", name, iorname, ioname);
		return(SH_GENERIC_ERROR);
	}
	iox = shElemGet(iop, iose, &ioxtype);
	iortype = shNameGetFromType(ioxtype);
	/* getting the X-array element */
	x = p + ploc[i][j];
	xN = pN + ploc[i][j];
	if (aflag == IOTOX) {
		#if DEBUG_QQ_TRANS
		printf("%s: transform from '%s' ('%s' in '%s') onto x-array \n", name, iorname, iortype, ioname);
		#endif
		status = thqqTrans(iox, iortype, x, mlefl_type);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not transform from '%s' ('%s' in '%s') onto x-array", name, iorname, iortype, ioname);
			return(status);
		}
	} else if (aflag == IOTOXN) {
		#if DEBUG_QQ_TRANS
		printf("%s: transform from '%s' ('%s' in '%s') onto xN-array \n", name, iorname, iortype, ioname);
		#endif
		status = thqqTrans(iox, iortype, xN, mlefl_type);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not transform from '%s' ('%s' in '%s') onto xN-array", name, iorname, iortype, ioname);
			return(status);
		}
	} else if (aflag == XTOIO) {
		#if DEBUG_QQ_TRANS
		printf("%s: transfrom from xN-array onto '%s' ('%s' in '%s') \n", name, iorname, iortype, ioname);
		#endif
		status = thqqTrans(x, mlefl_type, iox, iortype);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not transfrom from xN-array onto '%s' ('%s' in '%s')", name, iorname, iortype, ioname);
			return(status);
		}
	} else  if (aflag == XNTOIO) {
		#if DEBUG_QQ_TRANS
		printf("%s: transfrom from xN-array onto '%s' ('%s' in '%s') \n", name, iorname, iortype, ioname);
		#endif
		status = thqqTrans(xN, mlefl_type, iox, iortype);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not transfrom from xN-array onto '%s' ('%s' in '%s')", name, iorname, iortype, ioname);
			return(status);
		}

	}

	}
}

return(SH_SUCCESS);
}

RET_CODE thLXDumpModel(LSTRUCT *lstruct, ASSIGNFLAG aflag) {
char *name = "thLXDumpModel";
if (lstruct == NULL) {
	thError("%s: ERROR - null (mapmachine)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
LMACHINE *map = NULL;
status = thLstructGetLmachine(lstruct, &map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not derive the (lmachine) from (lstruct)", name);
	return(status);
}

if (aflag != XTOMODEL && aflag != MODELTOX && aflag != XNTOMODEL && aflag != MODELTOXN) {
	thError("%s: ERROR - unsupported 'ASSIGNFLAG' (aflag)", name);
	return(SH_GENERIC_ERROR);
}

MLEFL *p = NULL, *pN = NULL;
int np = 0;
status = ExtractXxNdX(lstruct, PSECTOR, NULL, NULL, &p, &pN, NULL, &np);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract (p, pN) from (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
static char *mlefl_type = NULL;
if (mlefl_type == NULL) {
	mlefl_type = thCalloc(MX_STRING_LEN, sizeof(char));
	if (sizeof(MLEFL) == sizeof(FL32)) {
		strcpy(mlefl_type, "FL32");
	} else if (sizeof(MLEFL) == sizeof(FL64)) {
		strcpy(mlefl_type, "FL64");
	} else if (sizeof(MLEFL) == sizeof(FL96)) {
		strcpy(mlefl_type, "FL96");
	} else {
		thError("%s: ERROR - unsupported (MLEFL)", name);
		thFree(mlefl_type);
		mlefl_type = NULL;
		return(SH_GENERIC_ERROR);
	}
}

void *x;
char *xtype;
int **ploc, *nmpar, pindex;
ploc = map->ploc;
nmpar = map->nmpar;

#if MAP_HAS_SCHEMA_INFO
void ***rs;
char ***etypes;
rs = map->rs;
etypes = map->etypes;
#else
void **ps;
char **pnames;
TYPE ptype;
char ***enames;
SCHEMA_ELEM *se;
ps = map->ps;
pnames = map->pnames;
enames = map->enames;
#endif

int i, j, nm;
status = thLstructGetNModel(lstruct, &nm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract (nmodel) from (lstruct)", name);
	return(status);
}
for (i = 0; i < nm; i++) {
	#if MAP_HAS_SCHEMA_INFO
	#else
	pname = pnames[i];
	ptype = shTypeGetFromName(pname);
	#endif
	for (j = 0; j < nmpar[i]; j++) {
		pindex = ploc[i][j];
		#if MAP_HAS_SCHEMA_INFO
		x = rs[i][j];
		xtype = etypes[i][j];
		#else
		rname = enames[i][j];
		se = shSchemaElemGetFromType(ptype, rname);
		xtype = se->type;
		x = shElemGet(ps, se, NULL);
		#endif

		if (aflag == XTOMODEL) {
			#if DEBUG_QQ_TRANS
			printf("%s: transform p-array element[%d] ('%s') onto model[%d] record ('%s') \n", name, pindex, mlefl_type, i, xtype); 
			#endif
			status = thqqTrans(p + pindex, mlefl_type, x, xtype);
		} else if (aflag == XNTOMODEL) {
			#if DEBUG_QQ_TRANS
			printf("%s: transform pN-array element[%d] ('%s') onto model[%d] record ('%s') \n", name, pindex, mlefl_type, i, xtype); 
			#endif
			status = thqqTrans(pN + pindex, mlefl_type, x, xtype);
		} else if (aflag == MODELTOX) {
			#if DEBUG_QQ_TRANS
			printf("%s: transform model[%d] record ('%s') onto p-array element[%d] ('%s') \n", name, i, xtype, pindex, mlefl_type); 
			#endif
			status = thqqTrans(x, xtype, p + pindex, mlefl_type);
		} else if (aflag == MODELTOXN) { 
			#if DEBUG_QQ_TRANS
			printf("%s: transform model[%d] record ('%s') onto pN-array element[%d] ('%s') \n", name, i, xtype, pindex, mlefl_type); 
			#endif
			status = thqqTrans(x, xtype, pN + pindex, mlefl_type);
		} else {
			thError("%s: ERROR - unsupported (assignflag)", name);
			return(SH_GENERIC_ERROR);
		}
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not transform between x-array element and model record", 
			name);
			return(status);
		}
	}
}

return(SH_SUCCESS);
}

RET_CODE thLDumpX(LSTRUCT *lstruct, ASSIGNFLAG aflag, LSECTOR sector) {
char *name = "thLDumpX";
if (lstruct == NULL) {
	thError("%s: ERROR - null (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
if (sector == ASECTOR) {
	status = thLDumpAmp(lstruct, aflag);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump (amplitude) with (aflag = '%s') at (sector = '%s')",name, shEnumNameGetFromValue("ASSIGNFLAG", aflag), shEnumNameGetFromValue("LSECTOR", sector));
		return(status);
	}
} else if (sector == PSECTOR) {
	status = thLDumpPar(lstruct, aflag);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump (parameter) with (aflag = '%s') at (sector = '%s')",name, shEnumNameGetFromValue("ASSIGNFLAG", aflag), shEnumNameGetFromValue("LSECTOR", sector));
		return(status);
	}
} else if (sector == APSECTOR) {
	status = thLDumpAmp(lstruct, aflag);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump (amplitude) with (aflag = '%s') at (sector = '%s')",name, shEnumNameGetFromValue("ASSIGNFLAG", aflag), shEnumNameGetFromValue("LSECTOR", sector));
		return(status);
	}
	status = thLDumpPar(lstruct, aflag);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump (parameter) with (aflag = '%s') at (sector = '%s')",name, shEnumNameGetFromValue("ASSIGNFLAG", aflag), shEnumNameGetFromValue("LSECTOR", sector));
		return(status);
	}
} else {
	thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}


RET_CODE thLDumpPar(LSTRUCT *lstruct, ASSIGNFLAG aflag) {
char *name = "thLDumpPar";

if (lstruct == NULL) {
	thError("%s: ERROR - (null) lstruct", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
LMACHINE *map;
status = thLstructGetLmachine(lstruct, &map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract (lmachine) in (lstruct)", name);
	return(status);
}

if (map->cflag != COMPILED) {
	thError("%s: ERROR - cannot process uncompiled map", name);
	return(SH_GENERIC_ERROR);
}

int i, namp;
namp = map->namp;

if (namp < 0) {
	thError("%s: ERROR - non-standard map with (namp = %d)", name, namp);
	return(SH_GENERIC_ERROR);
} else if (namp == 0) {
	thError("%s: WARNING - no amplitudes were fit (namp = 0)", name);
	return(SH_SUCCESS);
} 
int nobjc = 0;
THOBJC **objcs = NULL;
/* the following gets the unique list of objects - 
unique list is important so that redundant calls are not made*/
status = thMapmachineGetUObjcs(map, (void ***) &objcs, &nobjc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not exract (objc) array from (lmachine)", name);
	return(status);
}

if (aflag == MODELTOIO || aflag == IOTOMODEL) {
	for (i = 0; i < nobjc; i++) {
		status = thObjcTransIo(objcs[i], aflag);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not transfer between io-pars and its models in (objc: %d)", 
			name, objcs[i]->thid);
			return(status);
		}
	}

} else if (aflag == MODELTOX || aflag == XTOMODEL || 
	aflag == MODELTOXN || aflag == XNTOMODEL) {
	
	status = thLXDumpModel(lstruct, aflag);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not transfer between pars in (model)  to parameter array (x, xN)", name);
		return(status);
	}

} else if (aflag == XTOIO || aflag == IOTOX || 
	aflag == XNTOIO || aflag == IOTOXN) {
	status = thLXDumpIo(lstruct, aflag);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not transfer between pars in IO and the parameter array (x, xN)", name);
		return(status);
	}
} else {
	thError("%s: ERROR - unsupported (assignflag)", name);
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}


RET_CODE ExtractDX2(LSTRUCT *lstruct, LSECTOR sector, MLEFL **dx, int *n) {
char *name = "ExtractDX2";

if (lstruct == NULL) {
	thError("%s: ERROR - null (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
if (dx == NULL && n == NULL) {
	thError("%s: WARNING - null placeholder for (dx, n) - returning null", name);
	return(SH_SUCCESS);
}

LMODEL *lmodel;
LWORK *lwork;
lmodel = lstruct->lmodel;
lwork = lstruct->lwork;

if (sector == APSECTOR || sector == ASECTOR) {
	if (dx != NULL) {
		if (lwork != NULL) {
			*dx = lwork->db;
		} else {
			thError("%s: ERROR - null (lwork) in (lstruct), cannot look for (dbq)", name);
			return(SH_GENERIC_ERROR);
		}
	}
	if (n != NULL) {
		if (sector == APSECTOR) {
			if (lmodel != NULL) {
				*n = lmodel->nap;
			} else if (lwork != NULL) {
				*n = lwork->nap;
			} else {
				thError("%s: ERROR - null (lmodel) and (lwork) in (lstruct) - cannot extract (nap)", name);
			}
		} else {
			if (lmodel != NULL) {
				*n = lmodel->namp;
			} else if (lwork != NULL) {
				*n = lwork->nm;
			} else {
				thError("%s: ERROR - null (lmodel) and (lwork) in (lstruct) - cannot extract (namp)", name);
			}
		}
	}
} else if (sector == PSECTOR) {
	if (dx != NULL) {
		if (lwork != NULL) {
			*dx = lwork->dq;
		} else {
			thError("%s: ERROR - null (lwork) in (lstruct), cannot look for (dq)", name);
			return(SH_GENERIC_ERROR);
		}
	}
	if (n != NULL) {
		if (lwork != NULL) {
			*n = lwork->np;
		} else if (lmodel != NULL) {
			*n = lmodel->npar;
		} else {
			thError("%s: ERROR - null (lmodel) and (lwork) in (lstruct) - cannot extract (npar)", name);
			return(SH_GENERIC_ERROR);
		}
	}
} else {
	thError("%s: ERROR - unsupported X-sector", name);
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE ExtractXxNdX(LSTRUCT *lstruct, LSECTOR sector, MLEFL **xold, MLEFL **xT, MLEFL **x, MLEFL **xN, MLEFL **dx, int *n) {
char *name = "ExtractXxNdX";

if (lstruct == NULL) {
	thError("%s: ERROR - null (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
if (xold == NULL && xT == NULL && x == NULL && xN == NULL && dx == NULL && n == NULL) {
	thError("%s: WARNING - null placeholder for (xold, x, xN, dx, n) - returning null", name);
	return(SH_SUCCESS);
}

LMODEL *lmodel;
LWORK *lwork;
lmodel = lstruct->lmodel;
lwork = lstruct->lwork;

if (sector == APSECTOR || sector == ASECTOR) {
	if (x != NULL) {
		if (lmodel != NULL) {
			*x = lmodel->a;
		} else {
			thError("%s: ERROR - null (lmodel) in (lstruct), cannot look for (ap)", name);
			return(SH_GENERIC_ERROR);
		}
	}
	if (dx != NULL) {
		if (lwork != NULL) {
			*dx = lwork->da;
		} else {
			thError("%s: ERROR - null (lwork) in (lstruct), cannot look for (dap)", name);
			return(SH_GENERIC_ERROR);
		}
	}
	if (xN != NULL) {
		if (lwork != NULL) {
			*xN = lwork->aN;
		} else {
			thError("%s: ERROR - null (lwork) in (lstruct), cannot look for (apN)", name);
			return(SH_GENERIC_ERROR);
		}
	}
	if (xold != NULL) {
		if (lwork != NULL) {
			*xold = lwork->aold;
		} else {
			thError("%s: ERROR - null (lwork) in (lstruct)", name);
			return(SH_GENERIC_ERROR);
		}
	}
	if (xT != NULL) {
		if (lwork != NULL) {
			*xT = lwork->aT;
		} else {
			thError("%s: ERROR - null (lwork) in (lstruct)", name);
			return(SH_GENERIC_ERROR);
		}
	}
	if (n != NULL) {
		if (sector == APSECTOR) {
			if (lmodel != NULL) {
				*n = lmodel->nap;
			} else if (lwork != NULL) {
				*n = lwork->nap;
			} else {
				thError("%s: ERROR - null (lmodel) and (lwork) in (lstruct) - cannot extract (nap)", name);
			}
		} else {
			if (lmodel != NULL) {
				*n = lmodel->namp;
			} else if (lwork != NULL) {
				*n = lwork->nm;
			} else {
				thError("%s: ERROR - null (lmodel) and (lwork) in (lstruct) - cannot extract (namp)", name);
			}
		}
	}
} else if (sector == PSECTOR) {
	if (x != NULL) {
		if (lmodel != NULL) {
			*x = lmodel->p;
		} else {
			thError("%s: ERROR - null (lmodel) in (lstruct), cannot look for (p)", name);
			return(SH_GENERIC_ERROR);
		}
	}
	if (dx != NULL) {
		if (lwork != NULL) {
			*dx = lwork->dp;
		} else {
			thError("%s: ERROR - null (lwork) in (lstruct), cannot look for (dp)", name);
			return(SH_GENERIC_ERROR);
		}
	}
	if (xN != NULL) {
		if (lwork != NULL) {
			*xN = lwork->pN;
		} else {
			thError("%s: ERROR - null (lwork) in (lstruct), cannot look for (pN)", name);
			return(SH_GENERIC_ERROR);
		}
	}

	if (xold != NULL) {
		if (lwork != NULL) {
			*xold = lwork->pold;
		} else {
			thError("%s: ERROR - null (lwork) in (lstruct), cannot look for (pold)", name);
			return(SH_GENERIC_ERROR);
		}
	}
	if (xT != NULL) {
		if (lwork != NULL) {
			*xT = lwork->pT;
		} else {
			thError("%s: ERROR - null (lwork) in (lstruct), cannot look for (pT)", name);
			return(SH_GENERIC_ERROR);
		}
	}
	if (n != NULL) {
		if (lwork != NULL) {
			*n = lwork->np;
		} else if (lmodel != NULL) {
			*n = lmodel->npar;
		} else {
			thError("%s: ERROR - null (lmodel) and (lwork) in (lstruct) - cannot extract (npar)", name);
			return(SH_GENERIC_ERROR);
		}
	}
} else {
	thError("%s: ERROR - unsupported X-sector", name);
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE ExtractMcount(LSTRUCT *lstruct, LSECTOR sector, 
	MLEFL ***mcount, MLEFL ***mcountN) {
char *name = "ExtractMcount";
if (lstruct == NULL) {
	thError("%s: ERROR - null (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
LMODEL *lmodel;
if ((lmodel = lstruct->lmodel) == NULL) {
	thError("%s: ERROR - null (lmodel) in (lstruct)", name);
	return(SH_GENERIC_ERROR);
	}
LWORK *lwork;
if ((lwork = lstruct->lwork) == NULL) {
	thError("%s: ERROR - null (lwork) in (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
if (sector == ASECTOR) {
	if (mcount != NULL) *mcount = NULL;
	if (mcountN != NULL) *mcountN = NULL;
	return(SH_SUCCESS);
} else if (sector == APSECTOR || sector == PSECTOR) {
	if (mcount != NULL) *mcount = &(lmodel->mcount);
	if (mcountN != NULL) *mcountN = &(lwork->mcountN);
} else {
	thError("%s: ERROR - unsupported sector", name);
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE thExtractNx(LSTRUCT *lstruct, LSECTOR sector, int *nx) { 
char *name = "thExtractNx";

LMODEL *lmodel;
if (lstruct == NULL || (lmodel = lstruct->lmodel) == NULL) {
	thError("%s: ERROR - null (lstruct) or (lmodel)", name);
	return(SH_GENERIC_ERROR);
}
if (nx == NULL) {
	thError("%s: WARNING - null placeholder for (nx)", name);
	return(SH_SUCCESS);
}
if (sector == ASECTOR) {
	*nx = lmodel->namp;
} else if (sector == PSECTOR) {
	*nx = lmodel->npar;
} else if (sector == APSECTOR) {
	*nx = lmodel->nap;
} else {
	thError("%s: ERROR - unsupported X-sector", name);
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE thLstructSetXupdate(LSTRUCT *lstruct, XUPDATE update, LSECTOR sector) {
char *name = "thLstructSetXupdate";

LWORK *lwork;
if (lstruct == NULL || (lwork = lstruct->lwork) == NULL) {
	thError("%s: ERROR - null or improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}

if (sector == APSECTOR) {
	lwork->pupdate = update;
	lwork->aupdate = update;
} else if (sector == PSECTOR) {
	lwork->pupdate = update;
} else if (sector == ASECTOR) {
	lwork->aupdate = update;
} else {
	thError("%s: ERROR - unsupported X-sector, cannot change (X-update) flag", name);
	return(SH_GENERIC_ERROR);	
}

return(SH_SUCCESS);
}	

RET_CODE thLstructGetXupdate(LSTRUCT *lstruct, XUPDATE *update, LSECTOR sector) {
char *name = "thLstructGetXupdate";

LWORK *lwork;
if (lstruct == NULL || (lwork =  lstruct->lwork) == NULL) {
	thError("%s: ERROR - null or improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
if (update == NULL) {
	thError("%s: WARNING - null placeholder for (xupdate)", name);
	return(SH_SUCCESS);
}

if (sector == APSECTOR) {
	if ((lwork->aupdate == UPDATED || lwork->pupdate == UPDATED)) {
		 *update = UPDATED;
	} else {
		*update = OUTDATED;
	}
} else if (sector == PSECTOR) {
	*update = lwork->pupdate;
} else if (sector == ASECTOR) {
	*update = lwork->aupdate;
} else {
	thError("%s: ERROR - unsupported (xsector)", name);
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE thFreeImpackMemory(LIMPACK *impack) {
char *name = "thFreeImpackMemory";
if (impack == NULL) {
	thError("%s: WARNING - null input", name);
	return(SH_SUCCESS);
}
THREGION **mreg;
if ((mreg = impack->mreg) == NULL) {
	thError("%s: ERROR - improperly allocated (impack)", name);
	return(SH_GENERIC_ERROR);
}
#if MLE_CONDUCT_HIGHER_ORDER
THREGION ***DDreg;
if ((DDreg = impack->DDmreg) == NULL) {
	thError("%s: ERROR - improperly allocated (impack)", name);
	return(SH_GENERIC_ERROR);
}
#endif
int nmpar;
if ((nmpar = impack->nmpar) < 0) {
	thError("%s: ERROR - problematic (nmpar = %d) found in (impack)", name, nmpar);
	return(SH_GENERIC_ERROR);
}
int i;
for (i = 0; i < nmpar + 1; i++) {
	THREGION *mregi = mreg[i];
	FABRICATION_FLAG fab;
	RET_CODE status;
	status = thRegGetFabFlag(mregi, &fab);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (fabrication flag) from mreg[%d]", name, i);
		return(status);	
	}
	if (fab == FAB || fab == PSFCONVOLVED) {
		status = thRegFreeRows(mregi);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not free (thregion) of (impack) at location (%d)", name, i);
			return(status);
		}
	} else {
		#if DEBUG_MLE_MEMORY
		thError("%s: WARNING - found (fab = '%s') at mreg[%d]", name, shEnumNameGetFromValue("FABRICATION_FLAG", fab), i);
		#endif
	}
}

#if MLE_CONDUCT_HIGHER_ORDER
for (i = 0; i < nmpar; i++) {		
	THREGION **DDreg_i = DDreg[i];
	if (DDreg_i != NULL) {
		int j;
		for (j = 0; j <= i; j++) {
			THREGION *DDreg_ij = DDreg_i[j];
			if (DDreg_ij != NULL) {
				FABRICATION_FLAG fab = UNKNOWN_FABRICATION_FLAG;
				RET_CODE status;
				status = thRegGetFabFlag(DDreg_ij, &fab);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not get (fabrication flag) from (DDreg[%d][%d] = %p)", name, i, j, (void *) DDreg_ij);
					return(status);	
				}
				if (fab == FAB || fab == PSFCONVOLVED) {
					status = thRegFreeRows(DDreg_ij);
					if (status != SH_SUCCESS) {
						thError("%s: ERROR - could not free (DDreg[%d][%d]) of (impack)", name, i, j);
						return(status);
					}
				} else {
					#if DEBUG_MLE_MEMORY
					thError("%s: WARNING - found (fab = '%s') at DDreg[%d][%d]", name, shEnumNameGetFromValue("FABRICATION_FLAG", fab), i, j);
					#endif
				}
			}
		}
	}
}
#endif

return(SH_SUCCESS);
}	

LTEST *thLtestNew() {
LTEST *ltest = thCalloc(1, sizeof(LTEST));
ltest->diff = shRegNew("diff = model - data", 0, 0, TYPE_THPIX);
return(ltest);
}

void thLtestDel(LTEST *ltest) {
if (ltest == NULL) return;
if (ltest->diff != NULL) shRegDel(ltest->diff);
thFree(ltest);
return;
}

LALG *thLalgNew() {
LALG *lalg = thCalloc(1, sizeof(LALG));
lalg->runtype = UNKNOWN_RUNTYPE;
lalg->algs = thCalloc((int) N_LSTAGE, sizeof(ALGORITHM *));
return(lalg);
}

void thLalgDel(LALG *lalg) {
if (lalg == NULL) return;
if (lalg->algs != NULL) {
	int i;
	for (i = 0; i < (int) N_LSTAGE; i++) {
		if (lalg->algs[i] != NULL) thAlgorithmDel(lalg->algs[i]);
	}
}
thFree(lalg);
return;
}

RET_CODE thLstructPutLmachine(LSTRUCT *lstruct, LMACHINE *lmachine) {
char *name = "thLstructPutLmachine";
if (lstruct == NULL && lmachine == NULL) {
	thError("%s: WARNING - null input", name);
	return(SH_SUCCESS);
}
if (lstruct == NULL || lstruct->lmodel == NULL) {
	thError("%s: ERROR - null or improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
if (lstruct->lmodel->lmachine != NULL && lstruct->lmodel->lmachine != lmachine) {
	thError("%s: WARNING - replacing an already existing (lmachine) with a different one without freeing memory", name);
}
lstruct->lmodel->lmachine = lmachine;
return(SH_SUCCESS);
}

RET_CODE thLstructGetLwork(LSTRUCT *lstruct, LWORK **lwork) {
char *name = "thLstructGetLwork";
if (lwork == NULL) {
	thError("%s: WARNING - null input and output placeholder", name);
	return(SH_SUCCESS);
}
if (lstruct == NULL) {
	thError("%s: ERROR - null input", name);
	*lwork = NULL;
	return(SH_GENERIC_ERROR);
}
*lwork = lstruct->lwork;
if (*lwork == NULL ){
	thError("%s: WARNING - (lstruct) seems to be improperly allocated", name);
}
return(SH_SUCCESS);
}

RET_CODE thLstructGetImpacks(LSTRUCT *lstruct, LIMPACK ***impacks) {
char *name = "thLstructGetImpacks";
if (impacks == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (lstruct == NULL) {
	thError("%s: ERROR - null input", name);
	*impacks = NULL;
	return(SH_GENERIC_ERROR);
}
LWORK *lwork = NULL;
RET_CODE status;
status = thLstructGetLwork(lstruct, &lwork);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwork) from (lstruct)", name);
	*impacks = NULL;
	return(status);
}
LIMPACK **impacks1 = lwork->impacks;
if (impacks1 == NULL) {
	thError("%s: ERROR - null (impacks array) - improperly allocated (lstruct)", name);
	*impacks = NULL;
	return(SH_GENERIC_ERROR);
}
int i;
for (i = 0; i < NLIMPACKS; i++) {
	if (impacks1[i] == NULL || impacks1[i]->mreg == NULL) {
		thError("%s: ERROR - null or improperly allocated (impack) found at (pos = %d)", name, i);
		*impacks = NULL;
		return(SH_GENERIC_ERROR);
	}
}
*impacks = impacks1;
return(SH_SUCCESS);
}

RET_CODE thLstructGetAlgByStage(LSTRUCT *lstruct, LSTAGE stage, ALGORITHM **alg) {
char *name = "thLstructGetAlgByStage";

if (alg == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
LALG *lalg;
if (lstruct == NULL || (lalg = lstruct->lalg) == NULL) {
	thError("%s: ERROR - null or improperly allocated (lstruct)", name);
	*alg = NULL;
	return(SH_GENERIC_ERROR);
}
RUNTYPE runtype = lalg->runtype;
if (runtype == SIMPLERUN) {
	thError("%s: ERROR - (lalg) points to a simple run", name);
	*alg = NULL;
	return(SH_GENERIC_ERROR);
} else if (runtype == ALGRUN) {
	ALGORITHM **algs;
	if ((algs = lalg->algs) == NULL) {
		thError("%s: ERROR - improperly allocated (lalg)", name);
		*alg = NULL;
		return(SH_GENERIC_ERROR);
	}
	if (stage >= N_LSTAGE) {
		thError("%s: ERROR - (stage = %d) should be smaller than (N_LSTAGE = %d)", name, (int) stage, (int) N_LSTAGE);
		*alg = NULL;
		return(SH_GENERIC_ERROR);
	}
	*alg = algs[stage];
	return(SH_SUCCESS);
}

thError("%s: ERROR - unussported (runtype)", name);
*alg = NULL;
return(SH_GENERIC_ERROR);
}

 
RET_CODE thLstructGetConvergence(LSTRUCT *l, int *n, CONVERGENCE *c, LM_STEP_STATUS *step_status) {
char *name = "thLstructGetConvergence";
if (c == NULL && n == NULL && step_status == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (l == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

LMETHOD *lm = NULL;
if ((lm = l->lmethod) == NULL) {
	thError("%s: ERROR - null (lmethod) - improperly allocated (lstruct)", name);
	if (n != NULL) *n = -1;
	if (c != NULL) *c = UNKNOWN_CONVERGENCE;
	if (step_status != NULL) *step_status = UNKNOWN_LM_STEP_STATUS;
	return(SH_GENERIC_ERROR);
}

if (c != NULL) *c = lm->cflag;
if (n != NULL) *n = lm->k;
if (step_status != NULL) *step_status = lm->status;

return(SH_SUCCESS);
}

RET_CODE thLstructGetInitmode(LSTRUCT *l, LINITMODE *imode) {
char *name = "thLstructGetInitmode";
if (imode == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (l == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
LMODEL *lmodel = NULL;
status = thLstructGetLmodel(l, &lmodel);
if (status != SH_SUCCESS) {
	*imode = UNKNOWN_LINITMODE;
	thError("%s: ERROR - could not get (lmodel) from (lstruct)", name);
	return(status);
}
if (lmodel == NULL) {
	thError("%s: ERROR - null (lmodel) - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
*imode = lmodel->initmode;
return(SH_SUCCESS);
}

RET_CODE thLstructPutInitmode(LSTRUCT *l, LINITMODE imode) {
char *name = "thLstructPutInitmode";
if (l == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
LMODEL *lmodel = NULL;
status = thLstructGetLmodel(l, &lmodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmodel) from (lstruct)", name);
	return(status);
}
if (lmodel == NULL) {
	thError("%s: ERROR - null (lmodel) - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
lmodel->initmode=imode;
return(SH_SUCCESS);
}

RET_CODE thLstructGetImagemode(LSTRUCT *l, LIMAGEMODE *imode) {
char *name = "thLstructGetImagemode";
if (imode == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (l == NULL) {
	thError("%s: ERROR - null input", name);
	*imode = UNKNOWN_MLEIMAGE;
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
LDATA *ldata = NULL;
status = thLstructGetLdata(l, &ldata);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (ldata) from (lstruct)", name);
	return(status);
}
if (ldata == NULL) {
	thError("%s: ERROR - null (ldata) - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
*imode = ldata->imagemode;
return(SH_SUCCESS);
}

RET_CODE thLstructPutImagemode(LSTRUCT *l, LIMAGEMODE imode) {
char *name = "thLstructPutImagemode";
if (l == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
LDATA *ldata = NULL;
status = thLstructGetLdata(l, &ldata);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (ldata) from (lstruct)", name);
	return(status);
}
if (ldata == NULL) {
	thError("%s: ERROR - null (ldata) - improperly allocated (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
ldata->imagemode=imode;
return(SH_SUCCESS);
}

RET_CODE thLstructGetLdata(LSTRUCT *l, LDATA **ldata) {
char *name = "thLstructGetLdata";
if (ldata == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (l == NULL) {
	thError("%s: ERROR - null input", name);
	*ldata = NULL;
	return(SH_GENERIC_ERROR);
}
*ldata = l->ldata;
if (*ldata == NULL) {
	thError("%s: WARNING - null (ldata) - improperly allocated (lstruct)", name);
}
return(SH_SUCCESS);
}

RET_CODE thLstructGetLmodel(LSTRUCT *l, LMODEL **lmodel) {
char *name = "thLstructGetLmodel";
if (lmodel == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (l == NULL) {
	thError("%s: ERROR - null input", name);
	*lmodel = NULL;
	return(SH_GENERIC_ERROR);
}
*lmodel = l->lmodel;
if (*lmodel == NULL) {
	thError("%s: WARNING - null (lmodel) - improperly allocated (lstruct)", name);
}
return(SH_SUCCESS);
}


RET_CODE thLstructCopyLastparchain(LSTRUCT *l, CHAIN **pChain, unsigned int cmode) {
char *name = "thLstructCopyLastparchain";
if (pChain == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (l == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
status = thLDumpPar(l, XNTOIO);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not map parameter array (XN) onto IO structures", name);
	return(status);
}
status = thLDumpAmp(l, XTOIO);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not map amplitude array (X) onto IO structures", name);
	return(status);
}
status = thLDumpCountsIo(l);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (counts) in (ioprops) in (l)", name);
	return(status);
}
status = thLDumpCov(l);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not map (variance) matrix onto IO structures", name);
	return(status);
}
LMACHINE *map = NULL;
status = thLstructGetLmachine(l, &map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract (lmachine) in (lstruct)", name);
	return(status);
}

if (map->cflag != COMPILED) {
	thError("%s: ERROR - cannot process uncompiled map", name);
	return(SH_GENERIC_ERROR);
}

int i, namp;
namp = map->namp;
if (namp < 0) {
	thError("%s: ERROR - non-standard map with (namp = %d)", name, namp);
	return(SH_GENERIC_ERROR);
} else if (namp == 0) {
	thError("%s: WARNING - no amplitudes were fit (namp = 0)", name);
	return(SH_SUCCESS);
} 

THOBJC **objcs = NULL;
int nobjc = 0;
status = thMapmachineGetUPObjcs(map, (void ***) &objcs, &nobjc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get unique list of objects from (map)", name);
	return(status);
}
CHAIN *chain = shChainNew("THOBJC");
for (i = 0; i < nobjc; i++) {
	THOBJC *tobjc, *sobjc;
	sobjc = objcs[i];
	status = thObjcCopy(sobjc, &tobjc, cmode);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy source (objc) at location (i = %d)", name, i);
		shChainDestroy(chain, &thObjcDel);
		*pChain = NULL;
		return(status);
	}
	shChainElementAddByPos(chain, tobjc, "THOBJC", TAIL, AFTER);
}
*pChain = chain;
return(SH_SUCCESS);
}

RET_CODE thLstructGetPrivate(LSTRUCT *l, void **priv, char **privtype) {
char *name = "thLstructGetPrivate";
if (priv == NULL && privtype == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (l == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (priv != NULL) *priv = l->lpriv;
if (privtype != NULL) *privtype = l->lprivtype;
return(SH_SUCCESS);
}

RET_CODE thLstructPutPrivate(LSTRUCT *l, void *priv, char *privtype) {
char *name = "thLstructPutPrivate";
if (l == NULL) {
	thError("%s: ERROR - null (lstruct) placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (priv == NULL && (privtype == NULL || strlen(privtype) == 0)) {
	l->lpriv = NULL;
	strcpy(l->lprivtype, "");
	return(SH_SUCCESS);
}
if (privtype == NULL || strlen(privtype) == 0) {
	thError("%s: ERROR - cannot put an unknown structured in (lprivate)", name);
	return(SH_GENERIC_ERROR);
}
TYPE type = shTypeGetFromName(privtype);
if (type == UNKNOWN_SCHEMA) {
	thError("%s: WARNING - structure of type '%s' is not supported by schema - user responsible for memory management", name, privtype);
}
l->lpriv = priv;
strcpy(l->lprivtype, privtype);
return(SH_SUCCESS);
}

RET_CODE thLstructDelPrivate(LSTRUCT *l) {
char *name = "thLstructDelPrivate";
if (l == NULL) {
	thError("%s: WARNING - null input", name);
	return(SH_SUCCESS);
}
char *privtype = l->lprivtype;
void *priv = l->lpriv;
if (priv == NULL) {
	strcpy(privtype, "");
	return(SH_SUCCESS);
}
SCHEMA *s = NULL;
s = shSchemaGet(privtype);
if (s == NULL) {
	thError("%s: ERROR - unuspported type '%s' for (lprivate)", name, privtype);
	return(SH_GENERIC_ERROR);
} else if (s->destruct == NULL) {
	thError("%s: ERROR - null destructor found for type '%s' - problem with (schema)", name, privtype);
	return(SH_GENERIC_ERROR);
} 
s->destruct(priv);
l->lpriv = NULL;
strcpy(l->lprivtype, "");
return(SH_SUCCESS);
}

RET_CODE thLstructGetSource(LSTRUCT *l, void **source) {
char *name = "thLstructGetSource";
if (l == NULL) {
	thError("%s: WARNING - null input", name);
	return(SH_SUCCESS);
}
if (source == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
*source = l->source;
if (*source == NULL) {
	thError("%s: WARNING - (source) found in (lstruct) is (null)", name);
}
return(SH_SUCCESS);
}

RET_CODE thLstructPutSource(LSTRUCT *l, void *source) {
char *name = "thLstructPutSource";
if (l == NULL) {
	thError("%s: ERROR - null (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
if (l->source != NULL && l->source != source) {
	thError("%s: WARNING - wishing to change an already registered (source) for (lstruct)", name);
}
l->source = source;
return(SH_SUCCESS);
}

RET_CODE thLstructPutLwmask(LSTRUCT *l, LWMASK *lwmask) {
char *name = "thLstructPutLwmask";
if (l == NULL) {
	thError("%s: ERROR - null (lstruct)", name);
	return(SH_GENERIC_ERROR);
}
if (l->lwmask != NULL) {
	thError("%s: WARNING - wishing to change (lwmask) already in (lstruct)", name);
}

l->lwmask = lwmask;
return(SH_SUCCESS);
}

RET_CODE thLstructGetMN(LSTRUCT *l, REGION **mN) {
char *name = "thLstructGetMN";
if (mN == NULL) {
	thError("%s: WARNING - null output placeholdr", name);
	return(SH_SUCCESS);
}
if (l == NULL) {
	thError("%s: ERROR - null input", name);
	*mN = NULL;
	return(SH_GENERIC_ERROR);
}
LWORK *lwork = l->lwork;
if (lwork == NULL) {
	thError("%s: ERROR - null (lwork) - improperly allocated (lstruct)", name);
	*mN = NULL;
	return(SH_GENERIC_ERROR);
}
*mN = lwork->mN;
if (*mN == NULL) {
	thError("%s: WARNING - null (mN) - improperly allocated (lstruct)", name);
}
return(SH_SUCCESS);
}

RET_CODE thLmodelGetLmachine(LMODEL *lmodel, LMACHINE **lmachine) {
char *name = "thLmodelGetLmachine";
if (lmachine == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (lmodel == NULL) {
	thError("%s: ERROR - null input", name);
	*lmachine = NULL;
}
*lmachine = lmodel->lmachine;
return(SH_SUCCESS);
}

RET_CODE thLdataGetLpars(LDATA *ldata, int *nrow, int *ncol) {
char *name = "thLdataGetLpars";
if (nrow == NULL && ncol == NULL) {
	thError("%s: ERROR - null output placeholders", name);
	return(SH_SUCCESS);
}
if (ldata == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

REGION *reg = NULL;	
THMASK *mask = NULL;

if (ldata->image != NULL) {
	reg = ldata->image; 
} else if (ldata->weight != NULL) {
	reg = ldata->weight;
	thError("%s: WARNING - extracting (nrow, ncol) from (weight) while (image) is null", name);
} else if (ldata->mask != NULL) {
	mask = ldata->mask;
	thError("%s: WARNING - extracting (nrow, ncol) from (mask) while (image) is null", name);
} else {
	thError("%s: ERROR - not enough information in (ldata) to extract (nrow, ncol)", name);
	if (nrow != NULL) *nrow = UNKNOWN_NROW;
	if (ncol != NULL) *ncol = UNKNOWN_NCOL;
	return(SH_GENERIC_ERROR);
}

if (reg != NULL) {
	if (nrow != NULL) *nrow = reg->nrow;
	if (ncol != NULL) *ncol = reg->ncol;
} else if (mask != NULL) {
	RET_CODE status = thMaskGetSize(mask, nrow, ncol);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (nrow, ncol) from (mask)", name);
		if (nrow != NULL) *nrow = UNKNOWN_NROW;
		if (ncol != NULL) *ncol = UNKNOWN_NCOL;
		return(status);
	}
} 

return(SH_SUCCESS);
}




RET_CODE thLstructCopy(LSTRUCT *s, LSTRUCT *t, int cmode) {
char *name = "thLstructCopy";
if (t == NULL || s == NULL) {
	thError("%s: ERRROR - null input or output", name);
	return(SH_GENERIC_ERROR);
} else if (t == s) {
	thError("%s: ERROR - cannot copy an (lstruct) onto itself", name);
	return(SH_GENERIC_ERROR);
} else if (!(cmode & LCOPY_ALL)) {
	thError("%s: ERROR - not supported (copy mode = %d)", name, cmode);
	return(SH_GENERIC_ERROR);
} else if (t->parent != NULL) {
	thError("%s: ERROR - (target) already has a (parent)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
if (cmode & LCOPY_SOURCE) {
	status = thLdataCopy(s->ldata, t->ldata);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy (ldata)", name);
		return(status);
	}
	status = thLwmaskCopy(s->lwmask, t->lwmask);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy (lwmask)", name);
		return(status);
	}
}
if (cmode & LCOPY_FIT) {
	status = thLmodelCopy(s->lmodel, t->lmodel);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy (lmodel) in (source) to (target)", name);
		return(status);
	}
	LMACHINE *tmap = NULL;
	status = thLmodelGetLmachine(t->lmodel, &tmap);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (map) from (lmodel) in (target)", name);
		return(status);
	}
	MCFLAG cflag = UNKNOWN_MCFLAG;
	status = thMapmachineGetCflag(tmap, &cflag);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (cflag) from (target map)", name);
		return(status);
	}
	if (cflag == COMPILED) {
		int nm = -1, np = -1, npp = -1, nc = -1, nacc = -1, nj = -1;
		status = thMapGetLpars(tmap, &nm, &np, &npp, &nc, &nacc, &nj);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (lpars) from (map)", name);
			return(status);
		}
		int nrow = -1, ncol = -1;
		status = thLdataGetLpars(s->ldata, &nrow, &ncol);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get (nrow, ncol) from (ldata) in (source)", name);
			return(status);
		}
		status = thLworkAlloc(t->lwork, t->lmodel, tmap, nm, np, npp, nc, nacc, nj, nrow, ncol);	
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not allocate (lwork) from (target map)", name);
			return(status);
		}
	}
	/* cannot really copy, rather create first hand
	status = thLworkCopy(s->lwork, t->lwork);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy (lwork)", name);
		return(status);
	}
	*/
	status = thLmodvarCopy(s->lcov, t->lcov);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy (lcov)", name);
		return(status);
	}
}
if (cmode & LCOPY_METHOD) {
	status = thLmethodCopy(s->lmethod,t->lmethod, 0);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy (lmethod)", name);
		return(status);
	}
}
/* the current version of MLE packages does not support a separate LFBANK for each MLE structure - 
the model and object banks should be initiated and loaded in their respective packages first
status = thLfbankCopy(lsource->lfbank, ltarget->lfbank, cmode);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not copy (lfbank)", name);
	return(status);
}
*/
if (cmode & LCOPY_ALGORITHM) {
	status = thLalgCopy(s->lalg, t->lalg);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy (lalg)", name);
		return(status);
	}
}
if (cmode & LCOPY_TEST) {
	status = thLtestCopy(s->ltest, t->ltest);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copt (ltest)", name);
		return(status);
	}
}
if (cmode & LCOPY_PRIV) {
	t->lpriv = s->lpriv;
	if (s->lprivtype != NULL || strlen(s->lprivtype) != 0) {
			if (t->lprivtype == NULL) t->lprivtype = thCalloc(MX_STRING_LEN, sizeof(char));
			strcpy(t->lprivtype, s->lprivtype);
	}
}

t->source = s->source;
t->parent = s;
t->cmode = cmode;

return(SH_SUCCESS);
}

RET_CODE thLdataCopy(LDATA *s, LDATA *t) {
char *name = "thLdataCopy";
if (s == NULL || t == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (s == t) {
	thError("%s: ERROR - cannot copy an (ldata) onto itself", name);
	return(SH_GENERIC_ERROR);
}

if (t->image != NULL) {
	thError("%s: WARNING - target (ldata) contains non-null (image)", name);
}
t->image = s->image;
if (t->weight != NULL) {
	thError("%s: WARNING - target (ldata) contains non-null (weight)", name);
}
t->weight = s->weight;
if (t->background != NULL) {
	thError("%s: WARNING - target (ldata) contains non-null (background)", name);
}
t->background = s->background;
if (t->mask != NULL) {
	thError("%s: WARNING - target (ldata) contains non-null (mask)", name);
}
t->mask = s->mask;
if (t->psf != NULL) {
	thError("%s: WARNING - target (ldata) contains non-null (psf)", name);
}
t->psf = s->psf;
if (t->imagemode != UNKNOWN_MLEIMAGE) {
	thError("%s: WARNING - target (ldata) contains non-null (imagemode)", name);
}
t->imagemode = s->imagemode;
if (t->parent != NULL) {
	thError("%s: WARNING - target (ldata) contains non-null (parent)", name);
}
t->parent = (void *) s;
return(SH_SUCCESS);
}

RET_CODE thLworkCopy(LWORK *s, LWORK *t) {
char *name = "thLworkCopy";
if (s == NULL || t == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (t == s) {
	thError("%s: ERROR - cannot copy an (lwmask) onto itself", name);
	return(SH_GENERIC_ERROR);
}


return(SH_SUCCESS);
}

RET_CODE thLmethodCopy(LMETHOD *s, LMETHOD *t, int full) {
char *name = "thLmethodCopy";
if (s == NULL || t == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (t == s) {
	thError("%s: ERROR - cannot copy an (lmethod) onto itself", name);
	return(SH_GENERIC_ERROR);
}
memcpy(t, s, sizeof(LMETHOD));
if (!full) {
	t->cflag = UNKNOWN_CONVERGENCE;
	t->k = 0;
	t->count_PGD_noise = 0;
	t->k_PGD_noise = 0;
	t->perturbation_PGD_condition = 0;
	t->k_fprime_incidence = 0;
}

return(SH_SUCCESS);
}

RET_CODE thLprofileCopy(LPROFILE *s, LPROFILE *t, int full) {
char *name = "thLprofileCopy";
if (s == NULL || t == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (t == s) {
	thError("%s: ERROR - cannot copy an (lprofile) onto itself", name);
	return(SH_GENERIC_ERROR);
}
char *modelnames[NCOMPONENT];
char *objcname = t->objcname;
int i, n_null = 0;
for (i = 0; i < NCOMPONENT; i++) {
	modelnames[i] = t->modelnames[i];
}
memcpy(t, s, sizeof(LPROFILE));
for (i = 0; i < NCOMPONENT; i++) {
	t->modelnames[i] = modelnames[i];
	if (s->modelnames[i] != NULL) {
		strcpy(t->modelnames[i], s->modelnames[i]);
	} else {
		if (t->modelnames[i] != NULL) strcpy(t->modelnames[i], "");	
		n_null++;
	}
}
t->objcname = objcname;
if (s->objcname != NULL) {
	strcpy(t->objcname, s->objcname);
} else {
	if (t->objcname != NULL) strcpy(t->objcname, "");
	n_null++;
}

if (!full) {
	t->lm_cflag = UNKNOWN_CONVERGENCE;
	t->lm_k = 0;
}
#if DEBUG_MLETYPES_LPROFILE
if (n_null != 0) {
	thError("%s: WARNING - found (%d) occurance of null in string record of the source (lprofile)", name, n_null);
}
#endif

return(SH_SUCCESS);
}

RET_CODE thLtestCopy(LTEST *s, LTEST *t) {
char *name = "thLtestCopy";
if (s == NULL || t == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (t == s) {
	thError("%s: ERROR - cannot copy an (lwmask) onto itself", name);
	return(SH_GENERIC_ERROR);
}
REGION *diff = s->diff;
if (diff != NULL) {
	t->diff = shRegNew(diff->name, diff->nrow, diff->ncol, diff->type);
}
t->parent = (void *) s;
return(SH_SUCCESS);
}

RET_CODE thLalgCopy(LALG *s, LALG *t) {
char *name = "thLalgCopy";
if (s == NULL || t == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (t == s) {
	thError("%s: ERROR - cannot copy an (lwmask) onto itself", name);
	return(SH_GENERIC_ERROR);
}
int i;
ALGORITHM **salgs, **talgs;
salgs = s->algs;
talgs = t->algs;
if (salgs == NULL || talgs == NULL) {
	thError("%s: ERROR - improperly allocated (source) or (target)", name);
	return(SH_GENERIC_ERROR);
}
for (i = 0; i < N_LSTAGE; i++) {
	RET_CODE status;
	ALGORITHM *talg = NULL, *salg = NULL;
	salg = salgs[i];
	talg = talgs[i];
	if (salg != NULL) {
		if (talg == NULL) {
			talg = thAlgorithmNew();
			talgs[i] = talg;
		}
		status = thAlgorithmCopy(salg, talg);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not copy (algorithm) at location (i = %d) of (lalg)", name, i);
			return(status);
		}
	} else if (talg != NULL) {
		thAlgorithmDel(talg);
		talgs[i] = NULL;
	}
}

t->runtype = s->runtype;
return(SH_SUCCESS);
}

RET_CODE thLmodelCopy(LMODEL *s, LMODEL *t) {
char *name = "thLmodelCopy";
if (s == NULL || t == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (t == s) {
	thError("%s: ERROR - cannot copy an (lwmask) onto itself", name);
	return(SH_GENERIC_ERROR);
}
LMACHINE *smap, *tmap;
smap = s->lmachine;
tmap = t->lmachine;
if (smap != NULL && tmap == NULL) {
	tmap = thLmachineNew();
	t->lmachine = tmap;
} else if (smap == NULL && tmap != NULL) {
	thLmachineDel(tmap);
	tmap = NULL;
	t->lmachine = NULL;
}
LMODVAR *scov, *tcov;
scov = s->modvar;
tcov = t->modvar;
if (scov != NULL && tcov == NULL) {
	tcov = thLmodvarNew();
	t->modvar = tcov;
} else if (scov == NULL && tcov != NULL) {
	thLmodvarDel(tcov);
	t->modvar = NULL;
	tcov = NULL;
}
RET_CODE status;
if (smap != NULL) {
	status = thLmachineCopy(smap, tmap);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy (lmachine) from (source) to (target)", name);
		return(status);
	}
	MCFLAG sflag, tflag;
	status = thMapmachineGetCflag(smap, &sflag);	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (cflag) from (source map)", name);
		return(status);
	}
	status = thMapmachineGetCflag(tmap, &tflag);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (cflag) from (target map)", name);
		return(status);
	}
	if (sflag == COMPILED && tflag != COMPILED) {
		status = thLmachineCompile(tmap);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not compile (target map)", name);
			return(status);
		}
	}
} 
t->namp = s->namp;
t->npar = s->npar;
t->nap = s->nap;

if (scov != NULL) {
	status = thLmodvarCopy(scov, tcov);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy (modvar) from (source) to (target)", name);
		return(status);
	}
}
#if THMASKisMASK
t->fitregbit = s->fitregbit; 
#endif
#if THMASKisSUPERMASK
t->fitregbit = s->fitregbit;
#endif
t->initmode=s->initmode;

return(SH_SUCCESS);
}

RET_CODE thLmodvarCopy(LMODVAR *s, LMODVAR *t) {
char *name = "thLmodvarCopy";
if (s == NULL || t == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (t == s) {
	thError("%s: ERROR - cannot copy an (lwmask) onto itself", name);
	return(SH_GENERIC_ERROR);
}
MLEFL **scovap = s->covap;
MLEFL **sinvap = s->invcovap;
MLEFL **tcovap = t->covap;
MLEFL **tinvap = t->invcovap;

if (tcovap != NULL || tinvap != NULL) {
	thError("%s: ERROR - (target) already allocated", name);
	return(SH_GENERIC_ERROR);
}
int nap = s->nap;
if (nap != 0 && (scovap == NULL || sinvap == NULL)) {
	thError("%s: ERROR - improperly allocated (source)", name);
	return(SH_GENERIC_ERROR);
}
if (nap != 0) {
	int i;
	tcovap = thCalloc(nap, sizeof(MLEFL *));
	tcovap[0] = thCalloc(nap * nap, sizeof(MLEFL));
	for (i = 0; i < nap; i++) {
		tcovap[i] = tcovap[0] + i * nap;
	}
	tinvap = thCalloc(nap, sizeof(MLEFL *));
	tinvap = thCalloc(nap * nap, sizeof(MLEFL));
	for (i = 0; i < nap; i++) {
		tinvap[i] = tcovap[0] + i * nap;
	}
	t->covap = tcovap;
	t->invcovap = tinvap;
	for (i = 0; i < nap; i++) {
		memcpy(tcovap[i], scovap[i], nap * sizeof(MLEFL));
		memcpy(tinvap[i], tinvap[i], nap * sizeof(MLEFL));
	}
	int npar = s->npar;
	int namp = s->namp;
	MLEFL **tcov = thCalloc(npar, sizeof(MLEFL *));
	MLEFL **tinv = thCalloc(npar, sizeof(MLEFL *));
	for (i = 0; i < npar; i++) {
		tcov[i] = tcovap[i + namp] + namp;
		tinv[i] = tinvap[i + namp] + namp;
	}
	t->cov = tcov;
	t->invcov = tinv;

	t->nap = nap;
	t->namp = namp;
	t->npar = npar;
} else {
	t->nap = 0;
	t->namp = 0;
	t->npar = 0;
}

return(SH_SUCCESS);
}

RET_CODE thObjcDumpCounts(THOBJC *objc) {
char *name = "thObjcDumpCounts";
if (objc == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}


/* dealing with io parameter, its type and name */
THOBJCID objcid = objc->thid;
if (objc->iotype == UNKNOWN_SCHEMA) {
	if (objc->ioname == NULL || strlen(objc->ioname) == 0) {
		thError("%s: ERROR - unspecified type of (ioprop) for (objc: %d)", name, objc->thid);
		return(SH_GENERIC_ERROR);
	}
	objc->iotype = shTypeGetFromName(objc->ioname);
}		
TYPE iotype = objc->iotype;
if (iotype == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - unknown schema type (ioprop) for (objc: %d)", name, objc->thid);
	return(SH_GENERIC_ERROR);
}
if (objc->ioname == NULL) objc->ioname = thCalloc(MX_STRING_LEN, sizeof(char));
char *ioname = objc->ioname;
if (strlen(ioname) == 0) strcpy(ioname, shNameGetFromType(iotype));

void *pio = objc->ioprop;
if (pio == NULL) {
	thError("%s: ERROR - null (ioprop) in (objc: %x)", name, objc->thid);
	return(SH_SUCCESS);
}

/* now going model by model */
char suffix[MX_STRING_LEN];
char eout[MX_STRING_LEN];
char e1[MX_STRING_LEN];
char e2[MX_STRING_LEN];

memset(suffix, '\0', MX_STRING_LEN * sizeof(char));
memset(eout, '\0', MX_STRING_LEN * sizeof(char));
memset(e1, '\0', MX_STRING_LEN * sizeof(char));
memset(e2, '\0', MX_STRING_LEN * sizeof(char));

int nm = 0;
CHAIN *mprops = objc->thprop;
if (mprops == NULL) {
	thError("%s: ERROR - null (thprops) in (thobjc) for (objc: %d)", name, objcid);
	return(SH_GENERIC_ERROR);
} 

nm = shChainSize(mprops);
if (nm == 0) {
	thError("%s: ERROR - empty (thprops) in (thobjc) for (objc: %d)", name, objcid);
	return(SH_GENERIC_ERROR);
}
int j;
THPIX counts_model = 0.0;
for (j = 0; j < nm; j++) {
	THPROP *mprop = shChainElementGetByPos(mprops, j);
	if (mprop == NULL) {
		thError("%s: WARNING - empty (thprop) in (thprops) at position (%d) for (objc: %d)", name, j, objcid);
	} else {
		char *mname = mprop->mname;
		if (mname == NULL || strlen(mname) == 0) {
			thError("%s: WARNING - non-existent name for model (%d) of (objc: %d)", name, j, objcid);
			strcpy(suffix, "");
		} else {
			sprintf(suffix, "_%s", mname);
		}

		sprintf(eout, "counts%s", suffix);
		sprintf(e1, "I%s", suffix);
		sprintf(e2, "mcount%s", suffix);

		SCHEMA_ELEM *se1, *se2, *seout;
		seout = shSchemaElemGetFromType(iotype, eout);
		se1 = shSchemaElemGetFromType(iotype, e1);
		se2 = shSchemaElemGetFromType(iotype, e2);	

		if (seout != NULL && se1 != NULL && se2 != NULL) {	
			THPIX *x1, *x2, *xout;
			x1 = shElemGet(pio, se1, NULL);
			x2 = shElemGet(pio, se2, NULL);
			xout = shElemGet(pio, seout, NULL);
			*xout = (*x1) * (*x2);
			if (strcmp(suffix, "_star")) {
				counts_model += *xout;
			}
		} 
		#if VERBOSE_MLETYPES
		if (seout == NULL) {
			thError("%s: WARNING - no 'counts' keyword for model '%s' in '%s'", name, mname, ioname);
		} else if (se1 == NULL) {
			thError("%s: WARNING - no 'I' keyword for model '%s' in '%s'", name, mname, ioname);
		} else if (se2 == NULL) {
			thError("%s: WARNING - no 'mcount' keyword for model '%s' in '%s'", name, mname, ioname);
		}
		#endif
	}
}

/* inserting total galaxy light in counts_model */
char *mname = "model";
sprintf(suffix, "_%s", mname);

sprintf(eout, "counts%s", suffix);
SCHEMA_ELEM *seout;
seout = shSchemaElemGetFromType(iotype, eout);

if (seout != NULL) {	
	THPIX *xout;
	xout = shElemGet(pio, seout, NULL);
	*xout = counts_model;
	}


return(SH_SUCCESS);
}

RET_CODE thLDumpCountsIo(LSTRUCT *lstruct) {
char *name = "thLDumpCountsIo";
if (lstruct == NULL) {
	thError("%s: ERROR - null input (lstruct)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
LMACHINE *map;
status = thLstructGetLmachine(lstruct, &map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract (lmachine) in (lstruct)", name);
	return(status);
}

if (map->cflag != COMPILED) {
	thError("%s: ERROR - cannot process uncompiled map", name);
	return(SH_GENERIC_ERROR);
}

int i, namp;
namp = map->namp;

if (namp < 0) {
	thError("%s: ERROR - non-standard map with (namp = %d)", name, namp);
	return(SH_GENERIC_ERROR);
} else if (namp == 0) {
	thError("%s: WARNING - no amplitudes were fit (namp = 0)", name);
	return(SH_SUCCESS);
} 
int nobjc = 0;
THOBJC **objcs = NULL;
/* the following gets the unique list of objects - 
unique list is important so that redundant calls are not made*/
status = thMapmachineGetUObjcs(map, (void ***) &objcs, &nobjc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not exract (objc) array from (lmachine)", name);
	return(status);
}

for (i = 0; i < nobjc; i++) {
	status = thObjcDumpCounts(objcs[i]);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not dump (counts) in (objc: %d)", name, objcs[i]->thid);
		return(status);
	}
}

return(SH_SUCCESS);
}

RET_CODE thLdataGetPsfModel(LDATA *ld, PSFMODEL **psf) {
char *name = "thLdataGetPsfModel";
if (psf == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (ld == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
*psf = ld->psf;
return(SH_SUCCESS);
}

RET_CODE thLGetCrudeCalib(LSTRUCT *l, CRUDE_CALIB **cc) {
char *name = "thLGetCrudeCalib";
if (cc == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (l == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
LDATA *ld = NULL;
status = thLstructGetLdata(l, &ld);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (ldata) from (lstruct)", name);
	return(status);
}
PSFMODEL *psf = NULL;
status = thLdataGetPsfModel(ld, &psf);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (psfmodel) from (ldata)", name);
	return(status);
}
status = thPsfModelGetCrudeCalib(psf, cc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (crude_calib) from (psfmodel)", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE thLstructGetLCalib(LSTRUCT *l, LCALIB **lc) {
char *name = "thLstructGetLCalib";
if (lc == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (l == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
*lc = l->lcalib;
return(SH_SUCCESS);
}

RET_CODE thLCalibGetType(LCALIB *lc, CALIBTYPE *type) {
char *name = "thLCalibGetType";
if (type == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (lc == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
*type = lc->type;
return(SH_SUCCESS);
}

RET_CODE thLCalibPutType(LCALIB *lc, CALIBTYPE type) {
char *name = "thLCalibPutType";
if (lc == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
lc->type = type;
return(SH_SUCCESS);
}

RET_CODE thLCalibGetCrudeCalib(LCALIB *lc, CRUDE_CALIB **cc) {
char *name = "thLCalibGetCrudeCalib";
if (cc == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (lc == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
*cc = lc->cc;
return(SH_SUCCESS);
}

RET_CODE thLCalibPutCrudeCalib(LCALIB *lc, CRUDE_CALIB *cc) {
char *name = "thLCalibPutCrudeCalib";
if (lc == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
lc->cc = cc;
return(SH_SUCCESS);
}

LCALIB *thLCalibNew() {
LCALIB *lc = thCalloc(1, sizeof(LCALIB));
return(lc);
}

void thLCalibDel(LCALIB *lc) {
if (lc == NULL) return;
thFree(lc);
return;
}


LFIT *thLfitNew() {
LFIT *lf = thCalloc(1, sizeof(LFIT));

lf->lms = NULL;
lf->lm_nmax = 0;
lf->lm_filename = thCalloc(MX_STRING_LEN, sizeof(char));
lf->lm_init = thLmethodNew();
lf->lm_fin = thLmethodNew();

lf->lps = NULL;
lf->lp_nmax = 0;
lf->lp_filename = thCalloc(MX_STRING_LEN, sizeof(char));

lf->rtype = NULL_LFITRECORD;
return(lf);
}

void thLfitDel(LFIT *lf) {
if (lf == NULL) return;
if (lf->lm_filename != NULL) thFree(lf->lm_filename);
if (lf->lms != NULL) thFree(lf->lms);
if (lf->lm_init != NULL) thLmethodDel(lf->lm_init);
if (lf->lm_fin != NULL) thLmethodDel(lf->lm_fin);
if (lf->lp_filename != NULL) thFree(lf->lp_filename);
if (lf->lps != NULL) thFree(lf->lps);

thFree(lf);
return;
}

RET_CODE check_lfitrecordtype_for_conflict(LFITRECORDTYPE rtype) {
char *name = "check_lfitrecordtype_for_conflict";
LFITRECORDTYPE lm_all = (LM_RECORD_FIRSTANDLAST | LM_RECORD_ALLGOOD | LM_RECORD_ALL);
LFITRECORDTYPE lp_all = (LP_RECORD_FIRSTANDLAST | LP_RECORD_ALLGOOD | LP_RECORD_ALL);

int rtype_int = (int) rtype;
int lm_sector = rtype & lm_all;
int lp_sector = rtype & lp_all;

int lm_sector_int = (int) lm_sector;
int lp_sector_int = (int) lp_sector;
int lm_sector_okay = !(lm_sector_int & (lm_sector_int-1));
int lp_sector_okay = !(lp_sector_int & (lp_sector_int-1));
if (lm_sector_int == 0 && lp_sector_int == 0) {
	thError("%s: WARNING - no flag in LM or LP sector set in (rflag = %x)", name, rtype_int);
}
if (lm_sector_okay && lp_sector_okay) return(SH_SUCCESS);
if (!lm_sector_okay) {
	thError("%s: ERROR - unsupported (rtype = %x) - problematic lm_sector", name, lm_sector_int);
}
if (!lp_sector_okay) {
	thError("%s: ERROR - unsupported (rtype = %x) - problematic lp_sector", name, lp_sector_int);
}
return(SH_GENERIC_ERROR);
}

RET_CODE get_lm_lp_nmax_from_rtype(LFITRECORDTYPE rtype, int *lm_nmax, int *lp_nmax) {
char *name = "get_lm_lp_nmax_from_rtype";
if (lm_nmax == NULL && lp_nmax == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}

int this_lm_nmax = 0, this_lp_nmax = 0;

if (rtype & (LM_RECORD_ALL | LM_RECORD_ALLGOOD)) {
	this_lm_nmax = LFIT_NSTEP_DEFAULT;
} else if (rtype & (LM_RECORD_FIRSTANDLAST)) {
	this_lm_nmax = 2;
}
if (rtype & (LP_RECORD_ALL | LP_RECORD_ALLGOOD)) {
	this_lp_nmax = LFIT_NPROFILE_DEFAULT;
} else if (rtype & (LP_RECORD_FIRSTANDLAST)) {
	this_lp_nmax = LFIT_NPROFILE_FIRSTANDLAST;
}
if (rtype & LP_RECORD_LASTOFBEST) {
	this_lp_nmax += LFIT_NPROFILE_LASTOFBEST; /* not necessarily 1 because each field might contain more than one LRG */
}
if (lm_nmax != NULL) *lm_nmax = this_lm_nmax;
if (lp_nmax != NULL) *lp_nmax = this_lp_nmax;

return(SH_SUCCESS);
}


RET_CODE thLfitAlloc(LFIT *lf, LFITRECORDTYPE rtype) {
char *name = "thLfitAlloc";
if (lf == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = check_lfitrecordtype_for_conflict(rtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - check failed for 'LFITRECORDTYPE' rtype = %x", name, (int) rtype);
	return(status);
}

int lp_nmax = -1, lm_nmax = -1;
status = get_lm_lp_nmax_from_rtype(rtype, &lm_nmax, &lp_nmax);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lm_, lp_nmax) for (rtype = %d)", name, (int) rtype);
	return(status);
}

if (lf->nlm != 0)  {
	thError("%s: ERROR - (lfit) contains (LMETHOD) data, deallocate before reallocation", name);
	return(SH_GENERIC_ERROR);
} 
if (lf->nlp != 0) {
	thError("%s: ERROR - (lfit) contains (LPROFILE) data - deallocate before reallocation", name);
	return(SH_GENERIC_ERROR);
}

if (lf->lms != NULL && lf->lm_nmax != lm_nmax) {
	thFree(lf->lms);
	lf->lms = NULL;
} else if (lf->lms != NULL) {
	memset(lf->lms, '\0', lm_nmax * sizeof(LMETHOD));
}

if (lf->lps != NULL && lf->lp_nmax != lp_nmax) {
	thFree(lf->lps);
	lf->lps = NULL;
} else if (lf->lps != NULL) {
	memset(lf->lps, '\0', lp_nmax * sizeof(LPROFILE));
}



if (lm_nmax != 0 && lf->lms == NULL) {
	lf->lms = thCalloc(lm_nmax, sizeof(LMETHOD));
	int i;
	for (i = 0; i < lm_nmax; i++) {
		LMETHOD *lm = lf->lms + i;
		lm->chisq = CHISQ_IS_BAD;
		lm->cost = CHISQ_IS_BAD;
	}
	lf->lm_nmax = lm_nmax;
}
if (lp_nmax != 0 && lf->lps == NULL) {
	lf->lps = thCalloc(lp_nmax, sizeof(LPROFILE));
	lf->lp_nmax = lp_nmax;
}

lf->lm_nmax = lm_nmax;
lf->lp_nmax = lp_nmax;

lf->rtype = rtype;
return(SH_SUCCESS);
}

RET_CODE thLfitDealloc(LFIT *lf) {
char *name = "thLfitDealloc";
if (lf == NULL) {
	thError("%s: WARNING - null input", name);
	return(SH_SUCCESS);
}
if (lf->hdr != NULL) lf->hdr = NULL;
int nmax = LFIT_NSTEP_DEFAULT;
if (lf->lms != NULL && lf->lm_nmax < nmax) {
	thFree(lf->lms);
	lf->lms = thCalloc(nmax, sizeof(LMETHOD));
	int i;
	for (i = 0; i < nmax; i++) {
		LMETHOD *lm = lf->lms + i;
		lm->chisq = CHISQ_IS_BAD;
		lm->cost = CHISQ_IS_BAD;
	}
	lf->lm_nmax = nmax;
}
if (lf->lm_filename != NULL) {
	if (strlen(lf->lm_filename) != 0) lf->lm_filename[0] = '\0';
} else {
	lf->lm_filename = thCalloc(MX_STRING_LEN, sizeof(char));
}
lf->nlm = 0;

nmax = LFIT_NPROFILE_DEFAULT;
if (lf->lps != NULL && lf->lp_nmax < nmax) {
	thFree(lf->lps);
	lf->lps = thCalloc(nmax, sizeof(LPROFILE));
	lf->lp_nmax = nmax;
}
if (lf->lp_filename != NULL) {
	if (strlen(lf->lp_filename) != 0) lf->lp_filename[0] = '\0';
} else {
	lf->lp_filename = thCalloc(MX_STRING_LEN, sizeof(char));
}
lf->nlp = 0;



return(SH_SUCCESS);
}

RET_CODE thLfitRefresh(LFIT *lf) {
char *name = "thLfitRefresh";
if (lf == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
LFITRECORDTYPE rtype = lf->rtype;
status = check_lfitrecordtype_for_conflict(rtype);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - check failed for 'LFITRECORDTYPE' rtype = %x", name, (int) rtype);
	return(status);
}

int lp_nmax = -1, lm_nmax = -1;
status = get_lm_lp_nmax_from_rtype(rtype, &lm_nmax, &lp_nmax);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lm_, lp_nmax) for (rtype = %d)", name, (int) rtype);
	return(status);
}

lf->nlm = 0;
lf->nlp = 0;

if (lf->lms != NULL && lf->lm_nmax != lm_nmax) {
	thFree(lf->lms);
	lf->lms = NULL;
} else if (lf->lms != NULL) {
	memset(lf->lms, '\0', lm_nmax * sizeof(LMETHOD));
}

if (lf->lps != NULL && lf->lp_nmax != lp_nmax) {
	thFree(lf->lps);
	lf->lps = NULL;
} else if (lf->lps != NULL) {
	memset(lf->lps, '\0', lp_nmax * sizeof(LPROFILE));
}



if (lm_nmax != 0 && lf->lms == NULL) {
	lf->lms = thCalloc(lm_nmax, sizeof(LMETHOD));
	int i;
	for (i = 0; i < lm_nmax; i++) {
		LMETHOD *lm = lf->lms + i;
		lm->chisq = CHISQ_IS_BAD;
		lm->cost = CHISQ_IS_BAD;
	}	
	lf->lm_nmax = lm_nmax;
}
if (lp_nmax != 0 && lf->lps == NULL) {
	lf->lps = thCalloc(lp_nmax, sizeof(LPROFILE));
	lf->lp_nmax = lp_nmax;
}

lf->lm_nmax = lm_nmax;
lf->lp_nmax = lp_nmax;

if (lf->lm_init != NULL) {
	memset(lf->lm_init, '\0', sizeof(LMETHOD));
} else {
	lf->lm_init = thLmethodNew();
	memset(lf->lm_init, '\0', sizeof(LMETHOD));
}
if (lf->lm_fin != NULL) {
	memset(lf->lm_fin, '\0', sizeof(LMETHOD));
} else {
	lf->lm_fin = thLmethodNew();
	memset(lf->lm_fin, '\0', sizeof(LMETHOD));
}

return(SH_SUCCESS);
}



RET_CODE thLfitAddLmethod(LFIT *lf, LMETHOD *lm, XUPDATE update) {
char *name = "thLfitAddLmethod";
if (lf == NULL && lm == NULL) {
	thError("%s: WARNING - null input and output placeholder", name);
	return(SH_SUCCESS);
}
if (lf == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
} else if (lm == NULL) {
	thError("%s: ERROR - null (lmethod) cannot be added to (lfit)", name);
	return(SH_GENERIC_ERROR);
}
LFITRECORDTYPE rtype = lf->rtype;
if (!(rtype & LM_RECORD_ANY)) {
	thError("%s: WARNING - no (LM) related flag set in (rtype = %x)", name, rtype);
	return(SH_SUCCESS);
}

int k = lm->k;
CONVERGENCE cflag = lm->cflag;
RET_CODE status;

if ((rtype & (LM_RECORD_ALLGOOD | LM_RECORD_FIRSTANDLAST) && update == UPDATED && k > LM_FIRST_RECORD_K) || ((rtype & LM_RECORD_FIRSTANDLAST) && k == LM_FIRST_RECORD_K) || (rtype & LM_RECORD_ALL)) {
	printf("%s: rtype = %d, update = '%s', nlm = %d: recording \n", name, rtype, shEnumNameGetFromValue("XUPDATE", update), lf->nlm);

	LMETHOD *lm2 = lf->lms + lf->nlm;
	status = thLmethodCopy(lm, lm2, 1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy (lmethod)", name);
		return(status);
	}
	if (!(rtype & LM_RECORD_FIRSTANDLAST)) {
		lf->nlm++;
	} else if (lf->nlm == 0) {
		lf->nlm++;
	}
} else {

	printf("%s: rtype = %d, update = '%s', nlm = %d: skipping \n", name, rtype, shEnumNameGetFromValue("XUPDATE", update), lf->nlm);
}

if (k == LM_FIRST_RECORD_K) {
	LMETHOD *lm2 = lf->lm_init;
	status = thLmethodCopy(lm, lm2, 1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not make a copy of initial (lm)", name);
		return(status);
	}
} 

if (cflag & GENERAL_CONVERGENCE) {
	LMETHOD *lm2 = lf->lm_fin;
	status = thLmethodCopy(lm, lm2, 1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not make a copy of final (lm)", name);
		return(status);
	}
	/* update the chisq value if the copies made in LM chain are from all not only accepted steps */
	if (rtype & LM_RECORD_ANY) {
		CHISQFL chisq_min, cost_min;
		int record_count = -1;
		status = thLFitGetChisqMin(lf, &chisq_min, &cost_min, &record_count);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get min (chisq) value from (lmchain)", name);
			return(status);
		}	
		status = thLmethodPutChisq(lm2, chisq_min, cost_min);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not insert (chisq_min) in (fin - lm)", name);
			return(status);
		}
		if (record_count > 1) {
			printf("%s: (final chisq = %20.10G) value inserted to be the minimum in chain [size = %d] \n", name, (double) chisq_min, record_count);
		} else if (record_count <= 0) {
			thError("%s: ERROR - found (record_count = %d) in (lfit)", name, record_count);
			return(SH_GENERIC_ERROR);
		} 
	}
	
}

return(SH_SUCCESS);
}

RET_CODE thLstructGetLfit(LSTRUCT *l, LFIT **lf) {
char *name = "thLstructGetLfit";
if (lf == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (l == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
*lf = l->lfit;
return(SH_SUCCESS);
}

RET_CODE thLstructGetLmethod(LSTRUCT *l,LMETHOD **lm) {
char *name = "thLstructGetLmethod";
if (lm == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (l == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
*lm = l->lmethod;
return(SH_SUCCESS);
}

RET_CODE thLfitGetInitFiniLM(LFIT *lf, LMETHOD **init, LMETHOD **fini) {
char *name = "thLfitGetInitFiniLM";
if (init == NULL && fini == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (lf == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (init != NULL) *init = lf->lm_init;
if (fini != NULL) *fini = lf->lm_fin;
return(SH_SUCCESS);
}

RET_CODE thLmethodPutCflag(LMETHOD *lm, CONVERGENCE cflag) {
char *name = "thLmethodCflag";
if (lm == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
lm->cflag = cflag;
return(SH_SUCCESS);
}

RET_CODE thLmethodPutChisq(LMETHOD *lm, CHISQFL chisq, CHISQFL cost) {
char *name = "thLmethodPutChisq";
if (lm == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
lm->chisq = (FL64) chisq;
lm->cost =  (FL64) cost;
lm->pcost = (FL64) (cost - chisq);
if (chisq > cost) {
	thError("%s: WARNING - passed (chisq = %g) > (cost = %g)", name, (float) chisq, (float) cost);
	}
return(SH_SUCCESS);
}

RET_CODE thLmethodPutStatus(LMETHOD *lm, LM_STEP_STATUS step_status, int step_count, int step_mode) {
char *name = "thLmethodPutStatus";
if (lm == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
lm->status = step_status;
lm->step_count = step_count;
lm->step_mode = step_mode;
return(SH_SUCCESS);
}

RET_CODE thLmethodPutStepCount(LMETHOD *lm, int step_count, int step_mode) {
char *name = "thLmethodPutStepCount";
if (lm == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
lm->step_count = step_count;
lm->step_mode = step_mode;
return(SH_SUCCESS);
}


RET_CODE thLmethodGetDeltaQn(LMETHOD *lm, MLEFL *delta_qn) {
char *name = "thLmethodGetDeltaQn";
if (delta_qn == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (lm == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
*delta_qn = (MLEFL) lm->delta_qn;
return(SH_SUCCESS);
}

RET_CODE thLmethodPutDeltaQn(LMETHOD *lm, MLEFL delta_qn) {
char *name = "thLmethodPutDeltaQn";
if (lm == NULL) {
	thError("%s: ERROR - null (lmethod)", name);
	return(SH_GENERIC_ERROR);
}
if (delta_qn <= 0.0) {
	thError("%s: WARNING - (delta_qn = %g) is out of bounds", name, (float) delta_qn);
}
lm->delta_qn = delta_qn;
return(SH_SUCCESS);
}

RET_CODE thLmethodGetType(LMETHOD *lm, LMETHODTYPE *type, LMETHODSUBTYPE *wtype) {
char *name = "thLmethodGetType";
if (type == NULL && wtype == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (lm == NULL) {
	thError("%s: ERROR - null input", name);
	if (type != NULL) *type = UNKNOWN_LMETHODTYPE;
	if (wtype != NULL) *wtype = UNKNOWN_LMETHODSUBTYPE;
	return(SH_GENERIC_ERROR);
}
if (type != NULL) *type = lm->type;
if (wtype != NULL) *wtype = lm->wtype;
return(SH_SUCCESS);
}

RET_CODE thLmethodPutType(LMETHOD *lm, LMETHODTYPE type, LMETHODSUBTYPE wtype) {
char *name = "thLmethodPutType";
if (lm == NULL) {
	thError("%s: ERROR - null (lmethod) passed", name);
	return(SH_GENERIC_ERROR);
}
lm->type = type;
lm->wtype = wtype;
return(SH_SUCCESS);
}


RET_CODE thLmethodGetStepCount(LMETHOD *lm, int *step_count, int *step_mode) {
char *name = "thLmethodGetStepCount";
if (step_count == NULL && step_mode == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (lm == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (step_count != NULL) *step_count = lm->step_count;
if (step_mode != NULL) *step_mode = lm->step_mode;

return(SH_SUCCESS);
}


RET_CODE thLmethodGetChisq(LMETHOD *lm, CHISQFL *chisq, CHISQFL *cost) {
char *name = "thLmethodGetChisq";
if (lm == NULL && chisq == NULL && cost == NULL) {
	thError("%s: WARNING - null input and output placeholder", name);
	return(SH_SUCCESS);
}
if (lm == NULL) {
	thError("%s: ERROR - null input", name);
	if (chisq != NULL) *chisq = CHISQFLNAN;
	if (cost != NULL) *cost = CHISQFLNAN;
	return(SH_GENERIC_ERROR);
}
if (chisq == NULL && cost == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}

if (chisq != NULL) *chisq = (CHISQFL) lm->chisq;
if (cost != NULL) *cost = (CHISQFL) lm->cost;
return(SH_SUCCESS);
}


RET_CODE thLFitGetChisqMin(LFIT *lf, CHISQFL *chisq_min, CHISQFL *cost_min, int *record_count) {
char *name = "thLFitGetChisqMin";
if (lf == NULL && chisq_min == NULL) {
	thError("%s: WARNING - null input and output placeholder", name);
	*chisq_min = CHISQFLNAN;
	if (record_count != NULL) *record_count = -1;
	return(SH_SUCCESS);
}
if (lf == NULL) {
	thError("%s: ERROR - null input", name);
	if (record_count != NULL) *record_count = -1;
	return(SH_GENERIC_ERROR);
}
if (chisq_min == NULL && cost_min == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	if (record_count != NULL) *record_count = -1;
	return(SH_SUCCESS);
}
LMETHOD *lms = lf->lms;
int n = lf->nlm;
int nmax = lf->lm_nmax;
if (n > nmax) {
	thError("%s: ERROR - improperly allocated (lfit)", name);
	*chisq_min = CHISQFLNAN;
	if (record_count != NULL) *record_count = -1;
	return(SH_GENERIC_ERROR);
}
if (n > 0 && lms == NULL) {
	thError("%s: ERROR - null (lms) - improperly allocated (lfit)", name);
	*chisq_min = CHISQFLNAN;
	if (record_count != NULL) *record_count = -1;
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
int i;
CHISQFL mychisq = CHISQ_IS_BAD, mycost = CHISQ_IS_BAD;

int nvalid = 0;
for (i = 0; i < n; i++) {
	LMETHOD *lm = lms + i;
	CHISQFL chisq, cost;
	status = thLmethodGetChisq(lm, &chisq, &cost);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (chisq) value from (lmethod)", name);
		return(status);
	}
	if (mychisq == CHISQ_IS_BAD) {
		mychisq = chisq;
	} else if (chisq != CHISQ_IS_BAD) {
		mychisq = MIN(chisq, mychisq);
	}
	if (mycost == CHISQ_IS_BAD) {	
		mycost = cost;
	} else if (cost != CHISQ_IS_BAD) {
		mycost = MIN(cost, mycost);
	}
	if (chisq != CHISQ_IS_BAD || cost != CHISQ_IS_BAD) nvalid++;
}
if (mychisq == CHISQ_IS_BAD) thError("%s: WARNING - no valid chisq was found in the array of (lmethod, n = %d)", name, n); 
if (mycost == CHISQ_IS_BAD) thError("%s: WARNING - no valid cost was found in the array of (lmethod, n = %d)", name, n);

if (chisq_min != NULL) *chisq_min = mychisq;
if (cost_min != NULL) *cost_min = mycost;
if (record_count != NULL) *record_count = nvalid;
return(SH_SUCCESS);
}

RET_CODE thLfitGetRtype(LFIT *lf, LFITRECORDTYPE *rtype) {
char *name = "thLfitGetRtype";
if (lf == NULL && rtype == NULL) {
	thError("%s: WARNING - null input and output placeholder", name);
	return(SH_SUCCESS);
}
if (lf == NULL) {
	thError("%s: ERROR - null input", name);
	*rtype = NULL_LFITRECORD;
	return(SH_GENERIC_ERROR);
}
if (rtype == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
*rtype = lf->rtype;
return(SH_SUCCESS);
}

RET_CODE thLfitPutRtype(LFIT *lf, LFITRECORDTYPE rtype) {
char *name = "thLfitPutRtype";
if (lf == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
lf->rtype = rtype;
return(SH_SUCCESS);
}



RET_CODE thLfitGetNLprofile(LFIT *lf, int *nlp, int *nlpmax) {
char *name = "thLfitGetNLprofile";
if (lf == NULL && nlp == NULL && nlpmax == NULL) {
	thError("%s: WARNING - null input and output placeholder", name);
	return(SH_SUCCESS);
}
if (lf == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (nlp != NULL) *nlp = lf->nlp;
if (nlpmax != NULL) *nlpmax = lf->lp_nmax;
if (lf->lp_nmax < lf->nlp) {
	thError("%s: WARNING - found (nlp = %d > nlp_max = %d) in LFIT", name, lf->nlp,lf->lp_nmax);
}
return(SH_SUCCESS);
}

RET_CODE thLfitPutNLprofile(LFIT *lf, int nlp) {
char *name = "thLfitPutNLprofile";
if (lf == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
int nlpmax = lf->lp_nmax;
if (nlp < 0 || nlp > nlpmax) {
	thError("%s: ERROR - unacceptable value for (nlp = %d) while (nlpmax = %d)", name, nlp, nlpmax);
	return(SH_GENERIC_ERROR);
}
lf->nlp = nlp;
return(SH_SUCCESS);
}

RET_CODE thLfitGetLprofileByIndex(LFIT *lf, int index, LPROFILE **lprofile) {
char *name = "thLfitGetLprofileByIndex";
if (lf == NULL || lprofile == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (index < 0 || index >= lf->lp_nmax) {
	thError("%s: ERROR - unaccaptable (index = %d) while (nlpmax = %d)", name, index, lf->lp_nmax);
	*lprofile = NULL;
	return(SH_GENERIC_ERROR);
}
if (lf->lps == NULL) {
	thError("%s: ERROR - null (LPROFILE) array discovered in LFIT", name);
	return(SH_GENERIC_ERROR);
}
*lprofile = lf->lps + index;
return(SH_SUCCESS);
}
 


RET_CODE thLCGet(LSTRUCT *lstruct, CHISQFL *pcost) {
char *name = "thLCGet";
if (pcost == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (lstruct == NULL || lstruct->lwork == NULL) {
	thError("%s: ERROR - empty or problematic lstruct", name);
	return(SH_GENERIC_ERROR);
}
*pcost = lstruct->lwork->pcostN;
return(SH_SUCCESS);
}

RET_CODE thLPiCGet(LSTRUCT *lstruct, int i, CHISQFL *dpcost) {
char *name = "thLPiCGet";
if (dpcost == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (lstruct == NULL || lstruct ->lwork == NULL) {
	thError("%s: ERROR - empty or problematic lstruct", name);
	*dpcost = CHISQFLNAN;
	return(SH_GENERIC_ERROR);
}
LWORK *lwork = NULL;
lwork = lstruct->lwork;
int np = lwork->np;
if (i >= np || i < 0) {
	thError("%s: ERROR - asking for param (i = %d) while (npar = %d)", name, i, np);
	*dpcost = CHISQFLNAN;
	return(SH_GENERIC_ERROR);
}
if (lwork->dpcostN == NULL) {
	thError("%s: ERROR - improperly allocated (lwork)", name);
	*dpcost = CHISQFLNAN;
	return(SH_GENERIC_ERROR);
}

*dpcost = lwork->dpcostN[i];
return(SH_SUCCESS);
}

RET_CODE thLPiPjCGet(LSTRUCT *lstruct, int i, int j, CHISQFL *ddpcost) {
char *name = "thLPiPjCGet";
if (ddpcost == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (lstruct == NULL || lstruct ->lwork == NULL) {
	thError("%s: ERROR - empty or problematic lstruct", name);
	*ddpcost = CHISQFLNAN;
	return(SH_GENERIC_ERROR);
}
LWORK *lwork = NULL;
lwork = lstruct->lwork;
int np = lwork->np;
if (i >= np || i < 0 || j >= np || j < 0) {
	thError("%s: ERROR - asking for params (i = %d, j = %d) while (npar = %d)", name, i, j, np);
	*ddpcost = CHISQFLNAN;
	return(SH_GENERIC_ERROR);
}
if (lwork->ddpcostN == NULL || lwork->ddpcostN[i] == NULL) {
	thError("%s: ERROR - improperly allocated (lwork)", name);
	*ddpcost = CHISQFLNAN;
	return(SH_GENERIC_ERROR);
}

*ddpcost = lwork->ddpcostN[i][j];
return(SH_SUCCESS);
}

RET_CODE thLGetObjcRnamesPndex(LSTRUCT *lstruct, void *objc, char ***rnames, int *nrnames,int **pndex, int *npndex) {
char *name = "thLGetObjcRnamesPndex";
LMACHINE *map = NULL;
RET_CODE status;
status = thLstructGetLmachine(lstruct, &map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmachine) from (lstruct)", name);
	return(status);
}
status = thMapGetObjcRnamesPndex(map, objc, rnames, nrnames, pndex, npndex);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (rnames and pndex) for (objc = %p) from (lmachine)", name, objc);
	return(status);
}
return(SH_SUCCESS);
}

LPROFILE *thLprofileNew() {
LPROFILE *x = thCalloc(1, sizeof(LPROFILE));
x->modelnames[0] = thCalloc(NCOMPONENT * MX_STRING_LEN, sizeof(char));
int i;
for (i = 0; i < NCOMPONENT; i++) {
	x->modelnames[i] = x->modelnames[0] + i * MX_STRING_LEN;
}
x->objcname = thCalloc(MX_STRING_LEN, sizeof(char));
return(x);
}

void thLprofileDel(LPROFILE *x){
if (x == NULL) return;
if (x->modelnames[0] != NULL) thFree(x->modelnames[0]);
if (x->objcname != NULL) thFree(x->objcname);
thFree(x);
return;
}

RET_CODE thLstructModelIndexGetByThprop(LSTRUCT *l, THPROP *prop, int *imodel) {
char *name = "thLstructModelIndexGetByThprop";

if (l == NULL && prop == NULL && imodel == NULL) {
	thError("%s: WARNING - null input and output placeholder", name);
	return(SH_SUCCESS);
} else if (l == NULL) {
	thError("%s: ERROR - null input placeholder (lstruct)", name);
	return(SH_GENERIC_ERROR);
} else if (prop == NULL) {
	thError("%s: ERROR - null input placeholder (prop)", name);
	return(SH_GENERIC_ERROR);
} else if (imodel == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}

RET_CODE status;
LMACHINE *map = NULL;
status = thLstructGetLmachine(l, &map);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lmachine) from (lstruct)", name);
	return(status);
}
if (map == NULL) {
	thError("%s: ERROR - null (lmachine) found in (lstruct) - allocate properly", name);
	return(SH_GENERIC_ERROR);
}
status = thMapmachineModelIndexGetFromThprop(map, prop, imodel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not seek model prop in (lmachine)", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE thLworkGetWpsfwing(LWORK *lwork, PSFWING **wpsfwing) {
char *name = "thLworkGetWpsfwing";
if (lwork == NULL && wpsfwing == NULL) {
	thError("%s: WARNING - null input and output placeholder", name);
	return(SH_SUCCESS);
}
if (lwork == NULL) {
	thError("%s: ERROR - null input", name);
	*wpsfwing = NULL;
} else if (wpsfwing == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}

*wpsfwing = lwork->wpsfwing;
return(SH_SUCCESS);
}

RET_CODE thLworkGetWpsf(LWORK *lwork, WPSF **wpsf) {
char *name = "thLworkGetWpsf";
if (lwork == NULL && wpsf == NULL) {
	thError("%s: WARNING - null input and output placeholder", name);
	return(SH_SUCCESS);
}
if (lwork == NULL) {
	thError("%s: ERROR - null input", name);
	*wpsf = NULL;
} else if (wpsf == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}

*wpsf = lwork->wpsf;
return(SH_SUCCESS);
}



RET_CODE thTransModelPars(THOBJC *objc, char*ioname, void *pio, ASSIGNFLAG aflag) {
char *name = "thTransModelPars";

shAssert(objc != NULL);
shAssert(ioname != NULL);
shAssert(pio != NULL);
shAssert(aflag == MODELTOIO || aflag == IOTOMODEL);

/* now going model by model */
int nm = 0;
CHAIN *mprops = objc->thprop;
if (mprops == NULL) {
	thError("%s: ERROR - null (thprops) in (thobjc = %p)", name, (void *) objc);
	return(SH_GENERIC_ERROR);
} 

nm = shChainSize(mprops);
if (nm == 0) {
	thError("%s: ERROR - empty (thprops) in (thobjc = %p)", name, (void *) objc);
	return(SH_GENERIC_ERROR);
}

TYPE iotype = shTypeGetFromName(ioname);
void *x = NULL, *xio = NULL;
char *xtype, *xiotype; 
char suffix[MX_STRING_LEN];
char ename[MX_STRING_LEN];
memset(suffix, '\0', MX_STRING_LEN * sizeof(char));
memset(ename, '\0', MX_STRING_LEN * sizeof(char));

int j;
for (j = 0; j < nm; j++) {

TYPE t = UNKNOWN_SCHEMA;
THPROP *mprop = shChainElementGetByPos(mprops, j);
if (mprop == NULL) {
	thError("%s: WARNING - empty (thprop) in (thprops[%d]) for (objc: %p)", name, j, (void *) objc);
} else {

	char *pname = mprop->pname;
	char * mname = mprop->mname;
	void *p = mprop->value;
	if (mname == NULL || strlen(mname) == 0) {
	thError("%s: WARNING - non-existent name for model (%d) of (objc: %p)", name, j, objc);
	strcpy(suffix, "");
	} else {
	sprintf(suffix, "_%s", mname);
	}
	if ((pname == NULL || strlen(pname) == 0) && mname != NULL) {
	thError("%s: WARNING - unknown type, null (pname) for model parameter (%s) at position (%d) of (objc: %p) - no io translation to be done", name, mname, j, objc);
	t = UNKNOWN_SCHEMA;
	} else if ((pname == NULL || strlen(pname) == 0) && mname == NULL) {
	thError("%s: WARNING - unknown type, null (pname) for model parameter at position (%d) of (objc: %p) - no io translation to be done", name, j, objc);
	t = UNKNOWN_SCHEMA;
	} else {

	TYPE t = shTypeGetFromName(pname);
	if (t == UNKNOWN_SCHEMA) {
		thError("%s: ERROR - no schema available for parameter of type (%s) at position (%d) of (objc: %p) - no io translation to be done", name, pname, objc);
		return(SH_GENERIC_ERROR);
	}	 
	SCHEMA *s = shSchemaGetFromType(t);
	int k;
	for (k = 0; k < s->nelem; k++) {
		SCHEMA_ELEM *se = s->elems + k;
		sprintf(ename, "%s%s", se->name, suffix);	
		SCHEMA_ELEM *seio = shSchemaElemGetFromType(iotype, ename);
		if (seio == NULL && j == 0) { 
		/* only for the main model one might drop such suffixes as _model */
			strcpy(ename, se->name);
			seio = shSchemaElemGetFromType(iotype, ename);
		}
		if (seio != NULL) {
			x = shElemGet(p, se, NULL);
			xio = shElemGet(pio, seio, NULL);
			if (x != NULL) {
				if (xio == NULL) {
					thError("%s: WARNING - unallocated argument (%s) in (%s) for (objc: %p)", name, ename, ioname, objc);
				} else {
					xtype = se->type;
					xiotype = seio->type;
					RET_CODE status;
					if (aflag == IOTOMODEL) {
						#if DEBUG_QQ_TRANS
						printf("%s: transform IO record '%s' ('%s in '%s') onto model record '%s' ('%s' in '%s') \n", name, seio->name, xiotype, ioname, se->name, xtype, pname);
						#endif
						status = thqqTrans(xio, xiotype, x, xtype);
						if (status != SH_SUCCESS) {
							thError("%s: ERROR - could not transform IO record '%s' ('%s in '%s') onto model record '%s' ('%s' in '%s')", name, seio->name, xiotype, ioname, se->name, xtype, pname);
							return(status);
						}	
					} else if (aflag == MODELTOIO) {
						#if DEBUG_QQ_TRANS
						printf("%s: transform model record '%s' ('%s in '%s') onto IO record '%s' ('%s' in '%s') \n", name, se->name, xtype, pname, seio->name, xiotype, ioname);
						#endif
						status = thqqTrans(x, xtype, xio, xiotype);
						if (status != SH_SUCCESS) {
							thError("%s: ERROR - could not transform model record '%s' ('%s in '%s') onto IO record '%s' ('%s' in '%s')", name, se->name, xtype, pname, seio->name, xiotype, ioname);
							return(status);
						}
					}	
				}	
			}
		}
	}
	}
}
}

return(SH_SUCCESS);
}


RET_CODE thLprofileAddLmethodInfo(LPROFILE *lprofile, LMETHOD *lmethod) {
char *name = "thLprofileAddLmethodInfo";
if (lprofile == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (lmethod == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}


lprofile->lm_type = lmethod->type;
lprofile->lm_cflag = lmethod->cflag;
lprofile->lm_k = lmethod->k;
lprofile->lm_kmax = lmethod->kmax;
lprofile->lm_status = lmethod->status; /* accepted or rejected */
/* chisq values */
lprofile->lm_cost = lmethod->cost;
lprofile->lm_chisq = lmethod->chisq;
lprofile->lm_pcost = lmethod->pcost; /* this is particulalry needed for tracking the chisq values */

lprofile->step = lmethod->k;
lprofile->chisq = lmethod->chisq;

return(SH_SUCCESS);
}

RET_CODE thLworkCopyJBqnPrev(LWORK *lwork) {
char *name = " thLworkCopyJBqnPrev";
if (lwork == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
int nap = 0;
if ((nap = lwork->nap) <= 0) {
	thError("%s: WARNING - found (nap = %d) - nothing to perform", name, lwork->nap);
	return(SH_SUCCESS);
}
printf("%s: moving  QN(k + 1) work space into QN(k) \n", name);

if (lwork->jtdmmap == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (jtdmm) found to be null", name);
	return(SH_GENERIC_ERROR);
} else if (lwork->jtdmmap_prev == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (jtdmmap_prev) found to be null", name);
	return(SH_GENERIC_ERROR);
}
memcpy(lwork->jtdmmap_prev, lwork->jtdmmap, nap * sizeof(MLEFL));

if (lwork->Bqnap == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (Bqnap) found to be null (nap = %d)", name, nap);
	return(SH_GENERIC_ERROR);
} else if (lwork->Bqnap[0] == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (Bqnap[0]) found to be null (nap = %d)", name, nap);
	return(SH_GENERIC_ERROR);
} else if (lwork->Bqnap_prev == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (Bqnap_prev) found to be null (nap = %d)", name, nap);
	return(SH_GENERIC_ERROR);
} else if (lwork->Bqnap_prev[0] == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (Bqnap_prev[0]) found to be null (nap = %d)", name, nap);
	return(SH_GENERIC_ERROR);
}
int i;
for (i = 0; i < nap; i++) memcpy(lwork->Bqnap_prev[i], lwork->Bqnap[i], nap * sizeof(MLEFL));


if (lwork->BBqnap == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (BBqnap) found to be null (nap = %d)", name, nap);
	return(SH_GENERIC_ERROR);
} else if (lwork->BBqnap[0] == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (BBqnap[0]) found to be null (nap = %d)", name, nap);
	return(SH_GENERIC_ERROR);
} else if (lwork->BBqnap_prev == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (BBqnap_prev) found to be null (nap = %d)", name, nap);
	return(SH_GENERIC_ERROR);
} else if (lwork->BBqnap_prev[0] == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (BBqnap_prev[0]) found to be null (nap = %d)", name, nap);
	return(SH_GENERIC_ERROR);
}
for (i = 0; i < nap; i++) {
	memcpy(lwork->BBqnap_prev[i], lwork->BBqnap[i], nap * sizeof(MLEFL));
}
if (lwork->Hqnap == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (Hqnap) found to be null", name);
	return(SH_GENERIC_ERROR);
} else if (lwork->Hqnap[0] == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (Hqnap[0]) found to be null", name);
	return(SH_GENERIC_ERROR);
} else if (lwork->Hqnap_prev == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (Hqnap_prev) found to be null", name);
	return(SH_GENERIC_ERROR);
} else if (lwork->Hqnap_prev[0] == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (Hqnap_prev[0]) found to be null", name);
	return(SH_GENERIC_ERROR);
}
for (i = 0; i < nap; i++) {
	memcpy(lwork->Hqnap_prev[i], lwork->Hqnap[i], nap * sizeof(MLEFL));
}
if (lwork->HHqnap == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (HHqnap) found to be null", name);
	return(SH_GENERIC_ERROR);
} else if (lwork->HHqnap[0] == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (HHqnap[0]) found to be null", name);
	return(SH_GENERIC_ERROR);
} else if (lwork->HHqnap_prev == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (HHqnap_prev) found to be null", name);
	return(SH_GENERIC_ERROR);
} else if (lwork->HHqnap_prev[0] == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (HHqnap_prev[0]) found to be null", name);
	return(SH_GENERIC_ERROR);
}
for (i = 0; i < nap; i++) {
	memcpy(lwork->HHqnap_prev[i], lwork->HHqnap[i], nap * sizeof(MLEFL));
}
return(SH_SUCCESS);
}

RET_CODE thLworkGetQN(LWORK *lwork, 
		MLEFL ***Bk, MLEFL ***Bkplus1, MLEFL ***BBk, MLEFL ***BBkplus1, 
		MLEFL **dfk, MLEFL **dfkplus1, 
		MLEFL ***Hk, MLEFL ***Hkplus1, 	MLEFL ***HHk, MLEFL ***HHkplus1,
		MLEFL **dxk, int *n, LSECTOR sector) {
char *name = "thLworkGetQN";
if (lwork == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (sector != APSECTOR && sector != ASECTOR && sector != PSECTOR) {
	thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
} else if (sector == ASECTOR) {
	thError("%s: WARNING - strange  (sector = '%s') - is this a linear fit", name, shEnumNameGetFromValue("LSECTOR", sector));
}

if (Bk == NULL && Bkplus1 == NULL && BBk == NULL && BBkplus1 == NULL && 
	dfk == NULL && dfkplus1 == NULL && dxk == NULL && n == NULL && 
	Hk == NULL && Hkplus1 == NULL && HHk == NULL && HHkplus1 == NULL) {
	thError("%s: WARNING - null output placeholder for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_SUCCESS);
}
if (n != NULL) {
	if (sector == ASECTOR) {
		*n = lwork->nm;
	} else if (sector == APSECTOR) {
		*n = lwork->nap;
	} else if (sector == PSECTOR) {
		*n = lwork->np;
	} else {
		thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(SH_GENERIC_ERROR);
	}
}
if (sector == ASECTOR || sector == APSECTOR) {
	if (Bk != NULL) *Bk = lwork->Bqnap_prev;
	if (Bkplus1 != NULL) *Bkplus1 = lwork->Bqnap;
	if (BBk != NULL) *BBk = lwork->BBqnap_prev;
	if (BBkplus1 != NULL) *BBkplus1 = lwork->BBqnap;
	if (dfk != NULL) *dfk = lwork->jtdmmap_prev;
	if (dfkplus1 != NULL) *dfkplus1 = lwork->jtdmmap;
	if (dxk != NULL) *dxk = lwork->da; /* dxap; */
	if (Hk != NULL) *Hk = lwork->Hqnap_prev;
	if (Hkplus1 != NULL) *Hkplus1 = lwork->Hqnap;
	if (HHk != NULL) *HHk = lwork->HHqnap_prev;
	if (HHkplus1 != NULL) *HHkplus1 = lwork->HHqnap;

} else if (sector == PSECTOR) {
	if (Bk != NULL) *Bk = lwork->Bqn_prev;
	if (Bkplus1 != NULL) *Bkplus1 = lwork->Bqn;
	if (BBk != NULL) *BBk = lwork->BBqn_prev;
	if (BBkplus1 != NULL) *BBkplus1 = lwork->BBqn;
	if (dfk != NULL) *dfk = lwork->jtdmm_prev;
	if (dfkplus1 != NULL) *dfkplus1 = lwork->jtdmm;
	if (dxk != NULL) *dxk = lwork->dp; /* dx */
	if (Hk != NULL) *Hk = lwork->Hqn_prev;
	if (Hkplus1 != NULL) *Hkplus1 = lwork->Hqn;
	if (HHk != NULL) *HHk = lwork->HHqn_prev;
	if (HHkplus1 != NULL) *HHkplus1 = lwork->HHqn;
} else {
	thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE thLworkGetQNWorkSpace(LWORK *lwork, MLEFL **wqny, MLEFL **wqnz, int *n, LSECTOR sector) {
char *name = "thLworkGetQNWorkSpace";
if (lwork == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (sector != APSECTOR && sector != ASECTOR && sector != PSECTOR) {
	thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
} else if (sector == ASECTOR) {
	thError("%s: WARNING - strange  (sector = '%s') - is this a linear fit", name, shEnumNameGetFromValue("LSECTOR", sector));
}

if (n != NULL) {
	if (sector == ASECTOR) {
		*n = lwork->nm;
	} else if (sector == APSECTOR) {
		*n = lwork->nap;
	} else if (sector == PSECTOR) {
		*n = lwork->np;
	} else {
		thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(SH_GENERIC_ERROR);
	}
}

if (wqny != NULL) *wqny = lwork->wqny;
if (wqnz != NULL) *wqnz = lwork->wqnz;

return(SH_SUCCESS);
}

RET_CODE thLworkGetX(LWORK *lwork, MLEFL **x, int *n, LSECTOR sector) {
char *name = "thLworkGetX";
if (lwork == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (x == NULL || n == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (sector == ASECTOR) {
	*x = lwork->a;
	*n = lwork->nm;
	if (lwork->nm == 0) *x = NULL;
} else if (sector == PSECTOR) {
	*x = lwork->p;
	*n = lwork->np;
	if (lwork->np == 0) *x = NULL;
} else if (sector == APSECTOR) {
	*x = lwork->a;
	*n = lwork->nap;
	if (lwork->nap == 0) *x = NULL;
} else {
	thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE thLworkGetXN(LWORK *lwork, MLEFL **x, int *n, LSECTOR sector) {
char *name = "thLworkGetXN";
if (lwork == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (x == NULL || n == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (sector == ASECTOR) {
	*x = lwork->aN;
	*n = lwork->nm;
	if (lwork->nm == 0) *x = NULL;
} else if (sector == PSECTOR) {
	*x = lwork->pN;
	*n = lwork->np;
	if (lwork->np == 0) *x = NULL;
} else if (sector == APSECTOR) {
	*x = lwork->aN;
	*n = lwork->nap;
	if (lwork->nap == 0) *x = NULL;
} else {
	thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE thLworkGetWMatVec(LWORK *lwork, LSECTOR sector, MAT **wMat, MAT **wwMat, VEC ***wVec) {
char *name = "thLworkGetWMatVec";
if (lwork == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (wMat == NULL && wwMat == NULL && wVec == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (sector == ASECTOR) {
	if (wMat != NULL) *wMat = lwork->wMata;
	if (wwMat != NULL) *wwMat = lwork->wwMata;
	if (wVec != NULL) *wVec = lwork->wVeca;
} else if (sector == PSECTOR) {
	if (wMat != NULL) *wMat = lwork->wMatp;
	if (wwMat != NULL) *wwMat = lwork->wwMatp;
	if (wVec != NULL) *wVec = lwork->wVecp;
} else if (sector == APSECTOR) {
	if (wMat != NULL) *wMat = lwork->wMatap;
	if (wwMat != NULL) *wwMat = lwork->wwMatap;
	if (wVec != NULL) *wVec = lwork->wVecap;
} else {
	thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}
return(SH_SUCCESS);
}


RET_CODE UpdateQnPrevFromQn(LSTRUCT *lstruct, LSECTOR sector) {
char *name = "UpdateQnPrevFromQn";
if (lstruct == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
LWORK *lwork = NULL;
status = thLstructGetLwork(lstruct, &lwork);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (lwork) from (lstruct)", name);
	return(status);
}
MLEFL **Bk = NULL, **Bkplus1 = NULL, **BBk = NULL, **BBkplus1 = NULL; 
MLEFL *dfk = NULL, *dfkplus1 = NULL;
MLEFL **Hk = NULL, **Hkplus1 = NULL, **HHk = NULL, **HHkplus1 = NULL, *dxk = NULL;

int n = -1;
status = thLworkGetQN(lwork, 
	&Bk, &Bkplus1, &BBk, &BBkplus1, 
	&dfk, &dfkplus1, 
	&Hk, &Hkplus1, &HHk, &HHkplus1, 
	&dxk, &n, sector);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (quasi-newtonian) array from (lwork)", name);
	return(status);
}
if (n < 0) {
	thError("%s: ERROR - received (n = %d) for (quasi-newtonian) array for (sector = '%s')", name, n, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
} else if (n == 0) {
	thError("%s: WARNING - received (n = %d) for (quasi-newtonian) array for (sector = '%s')", name, n, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_SUCCESS);
}
int i;
if (Bk != NULL && Bkplus1 != NULL) {
	if (Bk[0] != NULL && Bkplus1[0] != NULL) {
	for (i = 0; i < n; i++) memcpy(Bk[i], Bkplus1[i], n * sizeof(MLEFL));
	} else {
		thError("%s: ERROR - improperly allocated (Bk, Bkplus1) matrices were found in (lwork) for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(SH_GENERIC_ERROR);
	}
} else if (Bk == NULL && Bkplus1 == NULL) {
	thError("%s: WARNING - (Bk, Bk+1) not allocated while (n = %d) for (sector = '%s')", name, n, shEnumNameGetFromValue("LSECTOR", sector));
} else {
	thError("%s: ERROR - improperly allocated (Bk, Bkplus1) matrices were found in (lwork) for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}
if (BBk != NULL && BBkplus1 != NULL) {
	if (BBk[0] != NULL && BBkplus1[0] != NULL) {
		for (i = 0; i < n; i++) memcpy(BBk[i], BBkplus1[i], n * sizeof(MLEFL));
	} else {
		thError("%s: ERROR - improperly allocated (BBk, BBkplus1) matrices were found in (lwork) for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(SH_GENERIC_ERROR);
	}
} else if (BBk == NULL && BBkplus1 == NULL) {
	thError("%s: WARNING - (BBk, BBk+1) not allocated while (n = %d) for (sector = '%s')", name, n, shEnumNameGetFromValue("LSECTOR", sector));
} else {
	thError("%s: ERROR - improperly allocated (BBk, BBkplus1) matrices were found in (lwork) for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}


if (dfk != NULL && dfkplus1 != NULL) {
	memcpy(dfk, dfkplus1, n * sizeof(MLEFL));
} else if (dfk == NULL && dfkplus1 == NULL) {	
	thError("%s: WARNING - (dfk, dfk+1) not allocated while (n = %d) for (sector = '%s')", name, n, shEnumNameGetFromValue("LSECTOR", sector));
} else  {
	thError("%s: ERROR - improperly allocated (dfk, dfkplus1) matrices were found in (lwork) for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}

if (Hk != NULL && Hkplus1 != NULL) {
	if (Hk[0] != NULL && Hkplus1[0] != NULL) {
		for (i = 0; i < n; i++) memcpy(Hk[i], Hkplus1[i], n * sizeof(MLEFL));
	} else {
		thError("%s: ERROR - improperly allocated (Hk, Hkplus1) matrices were found in (lwork) for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(SH_GENERIC_ERROR);
	}
} else if (Hk == NULL && Hkplus1 == NULL) {
	thError("%s: WARNING - (Hk, Hk+1) not allocated while (n = %d) for (sector = '%s')", name, n, shEnumNameGetFromValue("LSECTOR", sector));
} else {
	thError("%s: ERROR - improperly allocated (Hk, Hkplus1) matrices were found in (lwork) for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}

if (HHk != NULL && HHkplus1 != NULL) {
	if (HHk[0] != NULL && HHkplus1[0] != NULL) {
		for (i = 0; i < n; i++) memcpy(HHk[i], HHkplus1[i], n * sizeof(MLEFL));
	} else {
		thError("%s: ERROR - improperly allocated (HHk, HHkplus1) matrices were found in (lwork) for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(SH_GENERIC_ERROR);
	}
} else if (HHk == NULL && HHkplus1 == NULL) {
	thError("%s: WARNING - (HHk, HHk+1) not allocated while (n = %d) for (sector = '%s')", name, n, shEnumNameGetFromValue("LSECTOR", sector));
} else {
	thError("%s: ERROR - improperly allocated (HHk, HHkplus1) matrices were found in (lwork) for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}


return(SH_SUCCESS);
}

RET_CODE thLworkGetMemSize(LWORK *lw, MEMDBLE *memory) { 
char *name = "thLworkGetMemSize";
if (lw == NULL && memory == NULL) {
	thError("%s: WARNING - null input and output placeholder", name);
	return(SH_SUCCESS);
}
if (lw == NULL) {
	thError("%s: WARNING - null input", name);
	*memory = 0;
	return(SH_SUCCESS);
}
if (memory == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
	}
MEMDBLE mem = sizeof(LWORK);
MEMDBLE mem_item  = 0;
RET_CODE status;

int na = lw->nm;
int np = lw->np;
int nap = lw->nap;
int nacc = lw->nacc;
int nc = lw->nc;
int nj = lw->nj;
int npp = lw->npp;
int nDDacc = lw->nDDacc;
if (na < 0 || np < 0 || nap < 0 || nacc < 0 || nc < 0 || nj < 0) {
	thError("%s: ERROR - unacceptable negative combination (na= %d, np= %d, nap= %d, nacc= %d, nc= %d, nj= %d)", name, na, np, nap, nacc, nc, nj);
	return(SH_GENERIC_ERROR);
}
if (npp < 0 || nDDacc < 0 ) {
	thError("%s: ERROR - unacceptable negative combination (npp = %d, nDDacc = %d)", name, npp, nDDacc);
	return(SH_GENERIC_ERROR);
}

if (lw->m != NULL) {
	mem_item = 0;
	status = shRegGetMemSize(lw->m, &mem_item);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get the size of (m) in (lwork)", name);
		return(status);
	}
	mem += mem_item;
}
if (lw->dmm != NULL) {
	mem_item = 0;
	status = shRegGetMemSize(lw->dmm, &mem_item);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get the size of (dmm) in (lwork)", name);
		return(status);
	}
	mem += mem_item;
}
if (lw->wpsf != NULL) {
	mem_item = 0;
	status = thWpsfGetMemSize(lw->wpsf, &mem_item);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get the size of (wpsf) in (lwork)", name);
		return(status);
	}
}
if (lw->mpsf != NULL) mem += na * sizeof(PSF_CONVOLUTION_INFO);
if (lw->dpcost != NULL) mem += np * sizeof(CHISQFL);
if (lw->ddpcost != NULL) mem += np * np * sizeof(CHISQFL);
if (lw->da != NULL) mem += nap * sizeof(MLEFL);
if (lw->daT != NULL) mem += nap * sizeof(MLEFL);
if (lw->db != NULL) mem += nap * sizeof(MLEFL);
if (lw->a != NULL) mem += nap * sizeof(MLEFL);
if (lw->mcount != NULL) mem += na * sizeof(MLEFL);
if (lw->aN != NULL) mem += nap * sizeof(MLEFL);
if (lw->mcountN != NULL) mem += na * sizeof(MLEFL);
if (lw->a0 != NULL) mem += nap * sizeof(MLEFL);
if (lw->mcount0 != NULL) mem += na * sizeof(MLEFL);
if (lw->aold != NULL) mem += nap * sizeof(MLEFL);
if (lw->mcountold != NULL) mem += na * sizeof(MLEFL);
if (lw->aT != NULL) mem += nap * sizeof(MLEFL);
if (lw->mcountT != NULL) mem += na * sizeof(MLEFL);
if (lw->mN != NULL) {
	mem_item = 0;
	status = shRegGetMemSize(lw->mN, &mem_item);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get size of (region)", name);
		return(status);
	}
	mem += mem_item;
}
if (lw->dmmN != NULL) {
	mem_item = 0;
	status = shRegGetMemSize(lw->dmmN, &mem_item);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get size of (lw->dmmN)", name);
		return(status);
	}
	mem += mem_item;
}
if (lw->mregN != NULL) mem += nc * sizeof(THREGION *);
if (lw->DDmregN != NULL) mem += npp * sizeof(THREGION *);

if (lw->dpcostN != NULL) mem += np * sizeof(CHISQFL);
if (lw->ddpcostN != NULL) mem += np * np * sizeof(CHISQFL);
if (lw->m0 != NULL) {
	mem_item = 0;
	status = shRegGetMemSize(lw->m0, &mem_item);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get size of (lw->m0)", name);
		return(status);
	}
	mem += mem_item;
}
if (lw->d_reduced != NULL) {
	mem_item = 0;
	status = shRegGetMemSize(lw->d_reduced, &mem_item);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get size of (lw->d_reduced)", name);
		return(status);
	}
}
if (lw->jtjap != NULL) mem += nap * nap * sizeof(MLEFL);
if (lw->jtdmmap != NULL) mem += nap * sizeof(MLEFL);
if (lw->jtdmmap_prev != NULL) mem += nap * sizeof(MLEFL);
if (lw->jtddmap != NULL) mem += nap * sizeof(MLEFL);
if (lw->jtjpdiagap != NULL) mem += nap * nap * sizeof(MLEFL);
if (lw->Djtj != NULL) mem += nap * sizeof(MLEFL);
if (lw->Bqnap != NULL) mem += nap * nap * sizeof(MLEFL);
if (lw->Hqnap != NULL) mem += nap * nap * sizeof(MLEFL);
if (lw->Bqnap_prev != NULL) mem += nap * nap * sizeof(MLEFL);
if (lw->Hqnap_prev != NULL) mem += nap * nap * sizeof(MLEFL);
if (lw->BBqnap != NULL) mem += nap * nap * sizeof(MLEFL);
if (lw->HHqnap != NULL) mem += nap * nap * sizeof(MLEFL);
if (lw->BBqnap_prev != NULL) mem += nap * nap * sizeof(MLEFL);
if (lw->HHqnap_prev != NULL) mem += nap * nap * sizeof(MLEFL);
if (lw->mid != NULL) mem += na * sizeof(MLEFL);
if (lw->midmm != NULL) mem += na * sizeof(MLEFL);
if (lw->mimj != NULL) mem += na * na * sizeof(MLEFL);
#if OLD_MAP
if (lw->mimjk != NULL) mem += na * na * np * sizeof(MLEFL);
if (lw->mijmkl != NULL) mem += na * np * na * np * sizeof(MLEFL);
if (lw->mijdmm != NULL) mem += na * np * sizeof(MLEFL);
if (lw->mijd != NULL) mem += na * np * sizeof(MLEFL);
#else
if (lw->MM != NULL) mem += nacc * nacc * sizeof(MLEFL);
if (lw->MddM != NULL) mem += nacc * nDDacc * sizeof(MLEFL);
if (lw->MDmm != NULL) mem += nacc * sizeof(MLEFL);
if (lw->MD != NULL) mem += nacc * sizeof(MLEFL);
#endif
if (lw->wqny != NULL) mem += nap * sizeof(MLEFL);
if (lw->wqnz != NULL) mem += nap * sizeof(MLEFL);
if (lw->wap != NULL) mem += nap * nap * sizeof(MLEFL);
if (lw->wwap != NULL) mem += nap * nap * sizeof(MLEFL);
if (lw->wx != NULL) mem += nap * sizeof(MLEFL);
if (lw->wwx != NULL) mem += nap * sizeof(MLEFL);
if (lw->wrnames != NULL) mem += np * MX_STRING_LEN * sizeof(char);
if (lw->wrnames != NULL) mem += MX_STRING_LEN * MX_PAR_PER_OBJECT * sizeof(char);
if (lw->wpndexmap != NULL) mem += MX_PAR_PER_OBJECT * sizeof(int);
if (lw->wreg != NULL) {
	mem_item = 0;
	status = shRegGetMemSize(lw->wreg, &mem_item);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get size of (lw->wreg)", name);
		return(status);
	}
	mem += mem_item;
}
if (lw->wreg2 != NULL) {
	mem_item = 0;
	status = shRegGetMemSize(lw->wreg2, &mem_item);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get size of (lw->wreg2)", name);
		return(status);
	}
	mem += mem_item;
}
if (lw->wreg_reduced1 != NULL) {
	mem_item = 0;
	status = shRegGetMemSize(lw->wreg_reduced1, &mem_item);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get size of (lw->wreg_reduced)", name);
		return(status);
	}
	mem += mem_item;
}
if (lw->wreg_reduced2 != NULL) {
	mem_item = 0;
	status = shRegGetMemSize(lw->wreg_reduced2, &mem_item);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get size of (lw->wreg_reduced2)", name);
		return(status);
	}
	mem += mem_item;
}
if (lw->wpsfwing != NULL) {
	mem_item = 0;
	status = thPsfWingGetMemSize(lw->wpsfwing, &mem_item);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get size of (lw->wpsfwing)", name);
		return(status);
	}
	mem += mem_item;
}
if (lw->impacks != NULL) {
	mem_item = 0;
	if (lw->impacks[0] != NULL) {
		status = thLimpackGetMemSize(lw->impacks[0], &mem_item);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get memory useful for (impack)", name);
			return(status);
		}
	} else {
		mem_item = sizeof(LIMPACK);
	}
	mem += NLIMPACKS * (mem_item + sizeof(LIMPACK *));
}

*memory = mem;
return(SH_SUCCESS);
}



RET_CODE thLimpackGetMemSize(LIMPACK *impack, MEMDBLE *memory) {
char *name = "thLimpackGetMemSize";
if (impack == NULL && memory == NULL) {
	thError("%s: ERROR - null input and output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (impack == NULL) {
	thError("%s: ERROR - null input", name);
	*memory = 0;
	return(SH_GENERIC_ERROR);
}
if (memory == NULL) {
	thError("%s: ERROR - null output", name);
	return(SH_GENERIC_ERROR);
}

MEMDBLE mem = sizeof(LIMPACK);
mem += MX_PAR_PER_MODEL * sizeof(THREGION *);
mem += MX_PAR_PER_MODEL * MX_PAR_PER_MODEL * sizeof(THREGION *);
*memory = mem;
return(SH_SUCCESS);
}

RET_CODE shRegGetMemSize(REGION *reg, MEMDBLE *memory) {
char *name = "shRegGetMemSize";
if (reg == NULL || memory == NULL) {
	thError("%s: WARNING - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
int nrow, ncol;
TYPE type;
nrow = reg->nrow;
ncol = reg->ncol;
type = reg->type;
MEMDBLE pixel_size;
if (type == TYPE_FL32) {
	pixel_size = sizeof(FL32);
} else if (type == TYPE_FL64) {
	pixel_size = sizeof(FL64);
} else if (type == TYPE_S32) {
	pixel_size = sizeof(S32);
} else if (type == TYPE_U32) {
	pixel_size = sizeof(U32);
} else if (type == TYPE_S16) {
	pixel_size = sizeof(S16);
} else if (type == TYPE_U16) {
	pixel_size = sizeof(U16);
} else {
	thError("%s: ERROR - unsupported pixel type ('%s')", name, shEnumNameGetFromValue("TYPE", type));
	*memory = 0;
	return(SH_GENERIC_ERROR);
}

MEMDBLE mem = sizeof(REGION);
mem += nrow * ncol * pixel_size;
*memory = mem;
return(SH_SUCCESS);
}

RET_CODE thWpsfGetMemSize(WPSF *wpsf, MEMDBLE *memory) {
char *name = "thWpsfGetMemSize";
if (wpsf == NULL || memory == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
MEMDBLE mem = sizeof(WPSF);
MEMDBLE mem_item;
RET_CODE status;
if (wpsf->psf_model != NULL) {
	status = thPsfModelGetMemSize(wpsf->psf_model, &mem_item);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get memory size for (psf_model)", name);
		return(status);
	}
	mem += mem_item;
}
if (wpsf->psf_reg != NULL) {
	status = thPsfRegGetMemSize(wpsf->psf_reg, &mem_item);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get memory for (psf_reg)", name);
		return(status);
	}
	mem += mem_item;
}
if (wpsf->fftw_kernel[0] != NULL) {
	status = thFftwKernelGetMemsize(wpsf->fftw_kernel[0], &mem_item);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get memory for (fftw_kernel)", name);
		return(status);
	}
	mem += mem_item * N_PSF_COMPONENT;
}

*memory = mem;
return(SH_SUCCESS);
}

RET_CODE thPsfModelGetMemSize(PSFMODEL *psf_model, MEMDBLE *memory) {
char *name = "thPsfModelGetMemSize";
if (psf_model == NULL || memory == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
*memory = sizeof(PSFMODEL);
return(SH_SUCCESS);
}

RET_CODE thPsfRegGetMemSize(PSF_REG *psfreg, MEMDBLE *memory) {
char *name = "thPsfRegGetMemSize";
if (psfreg == NULL || memory == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
MEMDBLE mem_item = 0;
MEMDBLE mem = sizeof(PSF_REG);
if (psfreg->reg != NULL) {
	status = shRegGetMemSize(psfreg->reg, &mem_item);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get memory size of (psfreg->reg)", name);
		*memory = 0;
		return(status);
	}
mem += mem_item;
}
*memory = mem;
return(SH_SUCCESS);
}

RET_CODE thPsfWingGetMemSize(PSFWING *psfwing, MEMDBLE *memory) {
char *name = "thPsfWingGetMemSize";
if (psfwing == NULL || memory == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
MEMDBLE mem_item = 0;
MEMDBLE mem = sizeof(PSFWING);
if (psfwing->reg != NULL) {
	status = shRegGetMemSize(psfwing->reg, &mem_item);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get memory size of (psfwing->reg)", name);
		*memory = 0;
		return(status);
	}
	mem += mem_item;
}
*memory = mem;
return(SH_SUCCESS);
}

RET_CODE thFftwKernelGetMemsize(FFTW_KERNEL *fftw_kernel, MEMDBLE *memory) {
char *name = "thFftwKernelGetMemsize";
if (fftw_kernel == NULL || memory == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
MEMDBLE mem = sizeof(FFTW_KERNEL);
#if 0
int prow = fftw_kernel->prow;
int pcol = fftw_kernel->pcol;
if (fftw_kernel->fftw_kern != NULL) {
	mem += prow * pcol * sizeof(FFTW_COMPLEX);
}
if (fftw_kernel->fftw_kern_pix != NULL) {
	mem += prow * pcol * sizeof(FFTW_COMPLEX);
}
#else
mem += MAXNROW * MAXNCOL * sizeof(FFTW_COMPLEX);
#endif

*memory = mem;
return(SH_SUCCESS);
}


RET_CODE thLworkCopyJAdaDeltaPrev(LWORK *lwork, LMETHOD *lm) {
char *name = "thLworkCopyJAdaDeltaPrev";
if (lwork == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
int nap = 0;
if ((nap = lwork->nap) <= 0) {
	thError("%s: WARNING - found (nap = %d) - nothing to perform", name, lwork->nap);
	return(SH_SUCCESS);
}
int np = 0;
if ((np = lwork->np) <= 0) {
	thError("%s: WARNING - found (np = %d) - nothing to perform", name, lwork->np);
	return(SH_SUCCESS);
}

printf("%s: moving  (S)GD(k+1) work space into (S)GD(k) \n", name);

if (lwork->jtdmmap == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (jtdmm) found to be null", name);
	return(SH_GENERIC_ERROR);
} else if (lwork->jtdmmap_prev == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (jtdmmap_prev) found to be null", name);
	return(SH_GENERIC_ERROR);
}
#if DEEP_DEBUG_MLETYPES
double mag_fprime_prev = mag_mlefl_double(lwork->jtdmmap_prev, nap);
double mag_fprime =  mag_mlefl_double(lwork->jtdmmap, nap);
printf("%s: DEBUG -  before copying |F'| = %G, |F'N| = %G, dim = %d \n", name, (double) mag_fprime_prev, (double) mag_fprime, nap);
#endif

memcpy(lwork->jtdmmap_prev, lwork->jtdmmap, nap * sizeof(MLEFL));

#if DEEP_DEBUG_MLETYPES
mag_fprime_prev = mag_mlefl_double(lwork->jtdmmap_prev, nap);
mag_fprime =  mag_mlefl_double(lwork->jtdmmap, nap);
printf("%s: DEBUG -  after copying |F'| = %G, |F'N| = %G, dim = %d \n", name, (double) mag_fprime_prev, (double) mag_fprime, nap);
#endif

if (lwork->E_ggap == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (E_ggap) found to be null (nap = %d)", name, nap);
	return(SH_GENERIC_ERROR);
} else if (lwork->E_ggap_prev == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (E_ggap_prev) found to be null (nap = %d)", name, nap);
	return(SH_GENERIC_ERROR);
}
 
memcpy(lwork->E_ggap_prev, lwork->E_ggap, nap * sizeof(MLEFL));

if (lwork->E_gg == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (E_ggp) found to be null (nap = %d)", name, np);
	return(SH_GENERIC_ERROR);
} else if (lwork->E_gg_prev == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (E_ggp_prev) found to be null (np = %d)", name, np);
	return(SH_GENERIC_ERROR);
}
 
memcpy(lwork->E_gg_prev, lwork->E_gg, np * sizeof(MLEFL));



if (lwork->rms_gap == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (rms_gap) found to be null (nap = %d)", name, nap);
	return(SH_GENERIC_ERROR);
} else if (lwork->rms_gap_prev == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (rms_gap_prev) found to be null (nap = %d)", name, nap);
	return(SH_GENERIC_ERROR);
} 

memcpy(lwork->rms_gap_prev, lwork->rms_gap, nap * sizeof(MLEFL));

if (lwork->rms_g == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (rms_gp) found to be null (np = %d)", name, np);
	return(SH_GENERIC_ERROR);
} else if (lwork->rms_g_prev == NULL) {
	thError("%s: ERROR - wrongly allocated (lwork) - (rms_gap_prev) found to be null (np = %d)", name, np);
	return(SH_GENERIC_ERROR);
} 

memcpy(lwork->rms_g_prev, lwork->rms_g, np * sizeof(MLEFL));

#if 0
MLEFL dxdxap = 0.0, dxdx = 0.0;
MLEFL *dp = lwork->dp;
MLEFL *dap = lwork->da;
int i, np;
np = lwork->np;

RET_CODE status;
MLEFL rho_adadelta = 0.0;
status = thLmethodGetRhoAdaDelta(lm, &rho_adadelta, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (rho_adadelta) from (lmethod)", name);
	return(status);
}

for (i = 0; i < nap; i++) dxdxap += pow(dap[i], 2.0);
for (i = 0; i < np; i++) dxdx += pow(dp[i], 2.0);

lwork->rms_dxap_prev = lwork->rms_dxap;
lwork->E_dxdxap = rho_adadelta * (lwork->E_dxdxap_prev) * (1.0 - rho_adadelta) * dxdxap;
lwork->E_dxdxap_prev = lwork->E_dxdxap;

lwork->rms_dx_prev = lwork->rms_dx;
lwork->E_dxdx = rho_adadelta * (lwork->E_dxdx_prev) * (1.0 - rho_adadelta) * dxdx;
lwork->E_dxdx_prev = lwork->E_dxdx;

printf("%s: E_dxdxap_prev = %g, E_dxdxap = %g, dxdxap = %g \n", name, (float) lwork->E_dxdxap_prev, (float) lwork->E_dxdxap, (float) dxdxap);
printf("%s: E_dxdx_prev = %g, E_dxdx = %g, dxdxp = %g \n", name, (float) lwork->E_dxdx_prev, (float) lwork->E_dxdx, (float) dxdx);
printf("%s: rms_dx_prev = %g, rms_dx = %g \n", name, (float) lwork->rms_dx_prev, (float) lwork->rms_dx);
#endif

return(SH_SUCCESS);
}

RET_CODE UpdateEAdaDelta(LWORK *lwork, LMETHOD *lm) {
char *name = "UpdateEAdaDelta";

MLEFL *dp = lwork->dp;
MLEFL *dap = lwork->da;
int i, np;
np = lwork->np;


RET_CODE status;
MLEFL rho_adadelta = 0.0;
status = thLmethodGetRhoAdaDelta(lm, &rho_adadelta, NULL, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (rho_adadelta) from (lmethod)", name);
	return(status);
}

for (i = 0; i < lwork->nap; i++) {
	lwork->rms_dxap_prev[i] = lwork->rms_dxap[i];
	lwork->E_dxdxap[i] = rho_adadelta * (lwork->E_dxdxap_prev[i]) + (1.0 - rho_adadelta) * pow(dap[i], 2.0);
	lwork->E_dxdxap_prev[i] = lwork->E_dxdxap[i];
}
for (i = 0; i < lwork->np; i++) {

	lwork->rms_dx_prev[i] = lwork->rms_dx[i];
	lwork->E_dxdx[i] = rho_adadelta * (lwork->E_dxdx_prev[i]) + (1.0 - rho_adadelta) * pow(dp[i], 2.0);
	lwork->E_dxdx_prev[i] = lwork->E_dxdx[i];
}

#if 0
printf("%s: E_dxdxap_prev = %G, E_dxdxap = %G, dxdxap = %G \n", name, (double) lwork->E_dxdxap_prev, (double) lwork->E_dxdxap, (double) dxdxap);
printf("%s: E_dxdx_prev = %G, E_dxdx = %G, dxdxp = %G \n", name, (double) lwork->E_dxdx_prev, (double) lwork->E_dxdx, (double) dxdx);
printf("%s: rms_dx_prev = %g, rms_dx = %g \n", name, (float) lwork->rms_dx_prev, (float) lwork->rms_dx);
#endif

return(SH_SUCCESS);
}


RET_CODE thLworkGetEGgAdaDelta(LWORK *lwork, LSECTOR sector, MLEFL **E_gg, MLEFL **E_gg_prev, int *nx) {
char *name = "thLworkGetEGgAdaDelta";
if (lwork == NULL) {
	thError("%s: ERROR - null input placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (E_gg == NULL && E_gg_prev == NULL && nx == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
MLEFL *e_gg = NULL, *e_gg_prev = NULL;
int ny;
if (sector == PSECTOR) {
	ny = lwork->np;
	e_gg = lwork->E_gg;
	e_gg_prev = lwork->E_gg_prev;
} else if (sector == APSECTOR || sector == ASECTOR) {
	if (sector == ASECTOR) {
		ny = lwork->nm;
	} else {
		ny = lwork->nap;
	}
	e_gg = lwork->E_ggap;
	e_gg_prev = lwork->E_ggap_prev;
} else {
	thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}

if (E_gg != NULL) *E_gg = e_gg;
if (E_gg_prev != NULL) *E_gg_prev = e_gg_prev;		
if (nx != NULL) *nx = ny;

return(SH_SUCCESS);
}


RET_CODE thLworkGetRmsGAdaDelta(LWORK *lwork, LSECTOR sector, MLEFL **rms_g, MLEFL **rms_g_prev, int *nx){
char *name = "thLworkGetRmsGAdaDelta";
if (lwork == NULL) {
	thError("%s: ERROR - null input placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (rms_g == NULL && rms_g_prev == NULL && nx == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}

MLEFL *rms_k = NULL;
MLEFL *rms_k_prev = NULL;
MLEFL ny = -1;
if (sector == PSECTOR) {
	rms_k = lwork->rms_g;
	rms_k_prev = lwork->rms_g_prev;
	ny = lwork->np;
} else if (sector == APSECTOR || sector == ASECTOR) {
	rms_k = lwork->rms_gap;
	rms_k_prev = lwork->rms_gap_prev;
	if (sector == APSECTOR) {
		ny = lwork->nap;
	} else {
		ny = lwork->nm;
	}
} else {
	thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}

if (rms_g != NULL) *rms_g = rms_k;
if (rms_g_prev != NULL) *rms_g_prev = rms_k_prev;
if (nx != NULL) *nx = ny;
return(SH_SUCCESS);
}

RET_CODE thLworkGetRmsDxAdaDelta(LWORK *lwork, LSECTOR sector, MLEFL **rms_dx) {
char *name = "thLworkGetRmsDxAdaDelta";
if (lwork == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (rms_dx == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (sector == APSECTOR) {
	*rms_dx = lwork->rms_dxap;
} else if (sector == PSECTOR) {
	*rms_dx = lwork->rms_dx;
} else if (sector == ASECTOR) {
	*rms_dx = lwork->rms_dxap;
} else {
	thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE thLworkPutRmsDxAdaDelta(LWORK *lwork, LSECTOR sector, MLEFL *rms_dx) {
char *name = "thLworkPutRmsDxAdaDelta";
if (lwork == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (rms_dx == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

if (sector == APSECTOR) {
	memcpy(lwork->rms_dxap, rms_dx, lwork->nap * sizeof(MLEFL));
} else if (sector == PSECTOR) {
	memcpy(lwork->rms_dx, rms_dx, lwork->np * sizeof(MLEFL));
} else if (sector == ASECTOR) {
	memcpy(lwork->rms_dxap, rms_dx, lwork->nm * sizeof(MLEFL));
} else {
	thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE thLworkGetEDxdx(LWORK *lwork, LSECTOR sector, MLEFL **E_dxdx, MLEFL **E_dxdx_prev, int *nx) {
char *name = "thLworkGetEDxdx";
if (lwork == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (E_dxdx == NULL || E_dxdx_prev == NULL || nx == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (sector == APSECTOR) {
	*E_dxdx = lwork->E_dxdxap;
	*E_dxdx_prev = lwork->E_dxdxap_prev;
	*nx = lwork->nap;
} else if (sector == PSECTOR) {
	*E_dxdx = lwork->E_dxdx;
	*E_dxdx_prev = lwork->E_dxdx_prev;
	*nx = lwork->np;
} else if (sector == ASECTOR) {
	*E_dxdx = lwork->E_dxdxap;
	*E_dxdx_prev = lwork->E_dxdxap_prev;
	*nx = lwork->nm;
} else {
	thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE thLworkPutEDxdx(LWORK *lwork, LSECTOR sector, MLEFL *E_dxdx) {
char *name = "thLworkPutEDxdx";
if (lwork == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (E_dxdx == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_SUCCESS);
}
if (sector == APSECTOR) {
	memcpy(lwork->E_dxdxap, E_dxdx, lwork->nap * sizeof(MLEFL));
} else if (sector == PSECTOR) {
	memcpy(lwork->E_dxdx, E_dxdx, lwork->np * sizeof(MLEFL));
} else if (sector == ASECTOR) {
	memcpy(lwork->E_dxdxap, E_dxdx, lwork->nm * sizeof(MLEFL)); 
} else {
	thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}



RET_CODE thLworkGetRhoAdaDelta(LWORK *lwork, MLEFL *rho_adadelta, MLEFL *epsilon_adadelta_dx, MLEFL *epsilon_adadelta_g) {
char *name = "thLworkGetRhoAdaDelta";
if (lwork == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (rho_adadelta == NULL && epsilon_adadelta_dx == NULL && epsilon_adadelta_g == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (rho_adadelta != NULL) *rho_adadelta = lwork->rho_adadelta;
if (epsilon_adadelta_dx != NULL) *epsilon_adadelta_dx = lwork->epsilon_adadelta_dx;
if (epsilon_adadelta_g != NULL) *epsilon_adadelta_g = lwork->epsilon_adadelta_g;
return(SH_SUCCESS);
}

RET_CODE thLworkGetJtdmm(LWORK *lwork, LSECTOR sector, MLEFL **jtdmm, int *nx) {
char *name = "thLworkGetJtdmm";
if (lwork == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (jtdmm == NULL && nx == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
MLEFL *ktdmm = NULL;
int ny = -1;
if (sector == PSECTOR) {
	ktdmm = lwork->jtdmm;
	ny = lwork->np;
} else if (sector == APSECTOR || sector == ASECTOR) {
	ktdmm = lwork->jtdmmap;
	if (sector == APSECTOR) {
		ny = lwork->nap;
	} else {
		ny = lwork->nm;
	}
}
if (jtdmm != NULL) *jtdmm = ktdmm;
if (nx != NULL) *nx = ny;
return(SH_SUCCESS);
}

RET_CODE thLworkGetJtj(LWORK *lwork, LSECTOR sector, MLEFL ***jtj, int *nx) {
char *name = "thLworkGetJtJ";
if (lwork == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (jtj == NULL && nx == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
MLEFL *ktj = NULL;
int ny = -1;
if (sector == PSECTOR) {
	ktj = lwork->jtj;
	ny = lwork->np;
} else if (sector == APSECTOR || sector == ASECTOR) {
	ktj = lwork->jtjap;
	if (sector == APSECTOR) {
		ny = lwork->nap;
	} else {
		ny = lwork->nm;
	}
}
if (jtj != NULL) *jtj = ktj;
if (nx != NULL) *nx = ny;
return(SH_SUCCESS);
}

RET_CODE thLworkGetWJtj(LWORK *lwork, LSECTOR sector, MLEFL ***wjtj, int *nx) {
char *name = "thLworkGetWJtJ";
if (lwork == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (wjtj == NULL && nx == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
MLEFL *ktj = NULL;
int ny = -1;
if (sector == PSECTOR) {
	ktj = lwork->wjtj;
	ny = lwork->np;
} else if (sector == APSECTOR || sector == ASECTOR) {
	ktj = lwork->wjtjap;
	if (sector == APSECTOR) {
		ny = lwork->nap;
	} else {
		ny = lwork->nm;
	}
}
if (wjtj != NULL) *wjtj = ktj;
if (nx != NULL) *nx = ny;
return(SH_SUCCESS);
}

RET_CODE thLworkGetRSV(LWORK *lwork, MLEFL **dfk, MLEFL **dfkplus1, MLEFL **dxk, int *n, LSECTOR sector) {

char *name = "thLworkGetRSV";
if (lwork == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (sector != APSECTOR && sector != ASECTOR && sector != PSECTOR) {
	thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
} else if (sector == ASECTOR) {
	thError("%s: WARNING - strange  (sector = '%s') - is this a linear fit", name, shEnumNameGetFromValue("LSECTOR", sector));
}

if (dfk == NULL && dfkplus1 == NULL && dxk == NULL && n == NULL) {
	thError("%s: WARNING - null output placeholder for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_SUCCESS);
}
if (n != NULL) {
	if (sector == ASECTOR) {
		*n = lwork->nm;
	} else if (sector == APSECTOR) {
		*n = lwork->nap;
	} else if (sector == PSECTOR) {
		*n = lwork->np;
	} else {
		thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(SH_GENERIC_ERROR);
	}
}
if (sector == ASECTOR || sector == APSECTOR) {

	if (dfk != NULL) *dfk = lwork->jtdmmap_prev;
	if (dfkplus1 != NULL) *dfkplus1 = lwork->jtdmmap;
	if (dxk != NULL) *dxk = lwork->da; /* dxap; */

} else if (sector == PSECTOR) {

	if (dfk != NULL) *dfk = lwork->jtdmm_prev;
	if (dfkplus1 != NULL) *dfkplus1 = lwork->jtdmm;
	if (dxk != NULL) *dxk = lwork->dp; /* dx */

} else {
	thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}





RET_CODE thLworkGetPGD(LWORK *lwork, MLEFL **dfk, MLEFL **dfkplus1, MLEFL **dxk, int *n, LSECTOR sector) {

char *name = "thLworkGetPGD";
if (lwork == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (sector != APSECTOR && sector != ASECTOR && sector != PSECTOR) {
	thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
} else if (sector == ASECTOR) {
	thError("%s: WARNING - strange  (sector = '%s') - is this a linear fit", name, shEnumNameGetFromValue("LSECTOR", sector));
}

if (dfk == NULL && dfkplus1 == NULL && dxk == NULL && n == NULL) {
	thError("%s: WARNING - null output placeholder for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_SUCCESS);
}
if (n != NULL) {
	if (sector == ASECTOR) {
		*n = lwork->nm;
	} else if (sector == APSECTOR) {
		*n = lwork->nap;
	} else if (sector == PSECTOR) {
		*n = lwork->np;
	} else {
		thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(SH_GENERIC_ERROR);
	}
}
if (sector == ASECTOR || sector == APSECTOR) {

	if (dfk != NULL) *dfk = lwork->jtdmmap_prev;
	if (dfkplus1 != NULL) *dfkplus1 = lwork->jtdmmap;
	if (dxk != NULL) *dxk = lwork->da; /* dxap; */

} else if (sector == PSECTOR) {

	if (dfk != NULL) *dfk = lwork->jtdmm_prev;
	if (dfkplus1 != NULL) *dfkplus1 = lwork->jtdmm;
	if (dxk != NULL) *dxk = lwork->dp; /* dx */

} else {
	thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}


RET_CODE thLworkGetGD(LWORK *lwork, MLEFL **dfk, MLEFL **dfkplus1, MLEFL **rms_g, MLEFL **rms_ginv, MLEFL **rms_dx, MLEFL **dxk, int *n, LSECTOR sector) {

char *name = "thLworkGetGD";
if (lwork == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (sector != APSECTOR && sector != ASECTOR && sector != PSECTOR) {
	thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
} else if (sector == ASECTOR) {
	thError("%s: WARNING - strange  (sector = '%s') - is this a linear fit", name, shEnumNameGetFromValue("LSECTOR", sector));
}

if (dfk == NULL && dfkplus1 == NULL && rms_g == NULL && rms_ginv == NULL && rms_dx == NULL && dxk == NULL && n == NULL) {
	thError("%s: WARNING - null output placeholder for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_SUCCESS);
}
if (n != NULL) {
	if (sector == ASECTOR) {
		*n = lwork->nm;
	} else if (sector == APSECTOR) {
		*n = lwork->nap;
	} else if (sector == PSECTOR) {
		*n = lwork->np;
	} else {
		thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(SH_GENERIC_ERROR);
	}
}
if (sector == ASECTOR || sector == APSECTOR) {

	if (dfk != NULL) *dfk = lwork->jtdmmap_prev;
	if (dfkplus1 != NULL) *dfkplus1 = lwork->jtdmmap;
	if (dxk != NULL) *dxk = lwork->da; /* dxap; */
	if (rms_g != NULL) *rms_g = lwork->rms_gap;
	if (rms_ginv != NULL) *rms_ginv = lwork->rms_ginvap;
	if (rms_dx != NULL) *rms_dx = lwork->rms_dxap;

} else if (sector == PSECTOR) {

	if (dfk != NULL) *dfk = lwork->jtdmm_prev;
	if (dfkplus1 != NULL) *dfkplus1 = lwork->jtdmm;
	if (dxk != NULL) *dxk = lwork->dp; /* dx */
	if (rms_g != NULL) *rms_g = lwork->rms_g;
	if (rms_ginv != NULL) *rms_ginv = lwork->rms_ginv;
	if (rms_dx != NULL) *rms_dx = lwork->rms_dx;

} else {
	thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE thLworkGetMGD(LWORK *lwork, MLEFL **dfk, MLEFL **dfkplus1, MLEFL **dxk, int *n, LSECTOR sector) {

char *name = "thLworkGetMGD";
if (lwork == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (sector != APSECTOR && sector != ASECTOR && sector != PSECTOR) {
	thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
} else if (sector == ASECTOR) {
	thError("%s: WARNING - strange  (sector = '%s') - is this a linear fit", name, shEnumNameGetFromValue("LSECTOR", sector));
}

if (dfk == NULL && dfkplus1 == NULL && dxk == NULL && n == NULL) {
	thError("%s: WARNING - null output placeholder for (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_SUCCESS);
}
if (n != NULL) {
	if (sector == ASECTOR) {
		*n = lwork->nm;
	} else if (sector == APSECTOR) {
		*n = lwork->nap;
	} else if (sector == PSECTOR) {
		*n = lwork->np;
	} else {
		thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
		return(SH_GENERIC_ERROR);
	}
}
if (sector == ASECTOR || sector == APSECTOR) {

	if (dfk != NULL) *dfk = lwork->jtdmmap_prev;
	if (dfkplus1 != NULL) *dfkplus1 = lwork->jtdmmap;
	if (dxk != NULL) *dxk = lwork->da; /* dxap; */

} else if (sector == PSECTOR) {

	if (dfk != NULL) *dfk = lwork->jtdmm_prev;
	if (dfkplus1 != NULL) *dfkplus1 = lwork->jtdmm;
	if (dxk != NULL) *dxk = lwork->dp; /* dx */
} else {
	thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}



RET_CODE thLmethodGetEta(LMETHOD *lmethod, MLEFL *eta) {
char *name = "thLmethodGetEta";
if (lmethod == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (eta == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
*eta = lmethod->eta_max;
return(SH_SUCCESS);
}

RET_CODE thLmethodPutEta(LMETHOD *lmethod, MLEFL eta) {
char *name = "thLmethodPutEta";
if (lmethod == NULL) {
	thError("%s: ERROR - null (lmethod)", name);
	return(SH_GENERIC_ERROR);
}
if (eta <= (MLEFL) 0.0) {
	thError("%s: ERROR - unsupported value for (eta = %G)", name, (double) eta);
	return(SH_GENERIC_ERROR);
}
lmethod->eta_max = eta;
return(SH_SUCCESS);
}

RET_CODE thLmethodGetRhoAdaDelta(LMETHOD *lm, MLEFL *rho_adadelta, MLEFL *epsilon_adadelta_dx, MLEFL *epsilon_adadelta_g) {
char *name = "thLmethodGetRhoAdaDelta";
if (lm == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (rho_adadelta == NULL && epsilon_adadelta_dx == NULL && epsilon_adadelta_g == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (rho_adadelta != NULL) *rho_adadelta = lm->rho_adadelta;
if (epsilon_adadelta_dx != NULL) *epsilon_adadelta_dx = lm->epsilon_adadelta_dx;
if (epsilon_adadelta_g != NULL) *epsilon_adadelta_g = lm->epsilon_adadelta_g;
return(SH_SUCCESS);
}


double mag_mlefl_double(MLEFL *x, int n) {
shAssert(x != NULL);
shAssert(n > 0);
double mag = 0.0;
int i;
for (i = 0; i < n; i++) {
	mag += pow(x[i], 2.0);
}
mag = pow(mag, 0.5);
return(mag);
}


RET_CODE thLworkGetDaDp(LWORK *lwork, MLEFL ***DaDp, MLEFL ***DaDpT, MLEFL ***wN, MLEFL ***wA, MLEFL ***wwN, MLEFL **wC, MAT **wQ1, MAT **wQ2, VEC **wX, int *nmodel, int *nparam) {
char *name = "thLworkGetDaDp";
if (lwork == NULL) {
	thError("%s: ERROR - null input placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (DaDp == NULL && DaDpT == NULL && wN == NULL && wwN == NULL && wA == NULL && wC == NULL && nmodel == NULL && nparam == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}

if (DaDp != NULL) *DaDp = lwork->DaDp;
if (DaDpT != NULL) *DaDpT = lwork->DaDpT;
if (wN != NULL) *wN = lwork->wN;
if (wwN != NULL) *wwN = lwork->wwN;
if (wA != NULL) *wA = lwork->wA;
if (wC != NULL) *wC = lwork->wC;
if (wQ1 != NULL) *wQ1 = lwork->wMata;
if (wQ2 != NULL) *wQ2 = lwork->wwMata;
if (wX != NULL) *wX = lwork->wVeca[0];
if (nmodel != NULL) *nmodel = lwork->nm;
if (nparam != NULL) *nparam = lwork->np;

return(SH_SUCCESS);
}

RET_CODE thLworkGetDx(LWORK *lwork, LSECTOR sector, MLEFL **dx, int *nx) {
char  *name = "thLworkGetDx";
if (lwork == NULL) {
	thError("%s: ERROR - null input (lwork)", name);
	return(SH_GENERIC_ERROR);
}
if (dx == NULL || nx == NULL) {
	thError("%s: ERROR - null input placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (sector == APSECTOR) {
	*dx = lwork->da;
	*nx = lwork->nap;
} else if (sector == ASECTOR) {
	*dx = lwork->da;
	*nx = lwork->nm;
} else if (sector == PSECTOR) {
	*dx = lwork->dp;
	*nx = lwork->np;
} else {
	thError("%s: ERROR - unsupported (sector = '%s')", name, shEnumNameGetFromValue("LSECTOR", sector));
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE thLmethodGetMGD(LMETHOD *lmethod, 
	MLEFL *momentum, MLEFL *lr_gd, 
	LMETHODSUBTYPE *wmethod_type) {

char *name = "thLmethodGetMGD";

if (lmethod == NULL) {
	thError("%s: ERROR - null input placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (momentum != NULL) *momentum = (MLEFL) lmethod->momentum;
if (lr_gd != NULL) *lr_gd = (MLEFL) lmethod->lr_gd;
if (wmethod_type != NULL) *wmethod_type = lmethod->wtype;

return(SH_SUCCESS);
}

RET_CODE thLmethodRSVPut(LMETHOD *lmethod, LMETHODSUBTYPE wmethod_type) {
char *name = "thLmethodRSVPut";
if (lmethod == NULL) {
	thError("%s: ERROR - null input placeholder", name);
	return(SH_GENERIC_ERROR);
}
lmethod->wtype = wmethod_type;

return(SH_SUCCESS);
}




RET_CODE thLmethodGetPGD(LMETHOD *lmethod, 
	int *count_PGD_noise, int *count_PGD_thresh, int *k_PGD_noise, int *k_PGD_thresh, 
	int *perturbation_PGD_condition, MLEFL *perturbation_PGD_size, 
	MLEFL *mag_Fprime_PGD_thresh, MLEFL *eta_max, 
	MLEFL *cost_PGD_noise, MLEFL *chisq_PGD_noise, MLEFL *cost_PGD_thresh, 
	LMETHODSUBTYPE *wmethod_type) {

char *name = "thLmethodGetPGD";

if (lmethod == NULL) {
	thError("%s: ERROR - null input placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (count_PGD_noise != NULL) *count_PGD_noise = lmethod->count_PGD_noise;
if (count_PGD_thresh != NULL) *count_PGD_thresh = lmethod->count_PGD_thresh;
if (k_PGD_noise != NULL) *k_PGD_noise = lmethod->k_PGD_noise;
if (k_PGD_thresh != NULL) *k_PGD_thresh = lmethod->k_PGD_thresh;
if (perturbation_PGD_condition != NULL) *perturbation_PGD_condition = lmethod->perturbation_PGD_condition;
if (mag_Fprime_PGD_thresh != NULL) *mag_Fprime_PGD_thresh = (MLEFL) lmethod->mag_Fprime_PGD_thresh;
if (eta_max != NULL) *eta_max = (MLEFL) lmethod->eta_max;
if (perturbation_PGD_size != NULL) *perturbation_PGD_size = (MLEFL) lmethod->perturbation_PGD_size;
if (cost_PGD_noise != NULL) *cost_PGD_noise = (MLEFL) lmethod->cost_PGD_noise;
if (chisq_PGD_noise != NULL) *chisq_PGD_noise = (MLEFL) lmethod->chisq_PGD_noise;
if (cost_PGD_thresh != NULL) *cost_PGD_thresh = (MLEFL) lmethod->cost_PGD_thresh;
if (wmethod_type != NULL) *wmethod_type = lmethod->wtype;

return(SH_SUCCESS);
}

RET_CODE thLmethodPGDPut(LMETHOD *lmethod, int k_PGD_noise, int perturbation_PGD_condition, MLEFL eta_gd_max, MLEFL cost_PGD_noise, MLEFL chisq_PGD_noise, LMETHODSUBTYPE wmethod_type) {
char *name = "thLmethodPGDPut";
if (lmethod == NULL) {
	thError("%s: ERROR - null input placeholder", name);
	return(SH_GENERIC_ERROR);
}
lmethod->k_PGD_noise = k_PGD_noise;
lmethod->perturbation_PGD_condition = perturbation_PGD_condition;
if (perturbation_PGD_condition & (PERTURB_DUE_TO_CONVERGE | PERTURB_DUE_TO_LATE)) {
	lmethod->count_PGD_noise++;
}
lmethod->eta_max = (FL64) eta_gd_max;
lmethod->cost_PGD_noise = (FL64) cost_PGD_noise;
lmethod->chisq_PGD_noise = (FL64) chisq_PGD_noise;
lmethod->wtype = wmethod_type;
if (wmethod_type == PGD_SECOND_PHASE) {
	printf("%s: adjusting (eta_max, e1, e2) in the second phase of PGD = '%s' \n", 
		name, shEnumNameGetFromValue("LMETHODSUBTYPE", wmethod_type));
	printf("%s: old PGD values (eta, e1, e2) = (%g, %g, %g) will be updated to (%g, %g, %g) \n",
			name, (float) lmethod->eta_max, (float) lmethod->e1, (float) lmethod->e2, 
			(float) LM_ETA_MAX_PGD_SECOND_PHASE, 
			(float) LM_E1_PGD_SECOND_PHASE, 
			(float) LM_E2_PGD_SECOND_PHASE);

	lmethod->eta_max = (FL64) LM_ETA_MAX_PGD_SECOND_PHASE;
	lmethod->e1 = (FL64) LM_E1_PGD_SECOND_PHASE; 
	lmethod->e2 = (FL64) LM_E2_PGD_SECOND_PHASE;
}

return(SH_SUCCESS);
}

RET_CODE thLmethodGetFprimeIncidence(LMETHOD *lmethod, int *k_fprime_incidence) {
char *name = "thLmethodGetFprimeIncidence";
if (k_fprime_incidence == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
if (lmethod == NULL) {
	thError("%s: ERROR - null input placeholder", name);
	return(SH_GENERIC_ERROR);
}
*k_fprime_incidence = lmethod->k_fprime_incidence;
return(SH_SUCCESS);
}

RET_CODE thLmethodPutFprimeIncidence(LMETHOD *lmethod, int k_fprime_incidence) {
char *name = "thLmethodPutFprimeIncidence";
if (lmethod == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (k_fprime_incidence < 0) {
	thError("%s: WARNING - passed a negative value of (k_fprime_incidence = %d)", name, k_fprime_incidence);
}
lmethod->k_fprime_incidence = k_fprime_incidence;
return(SH_SUCCESS);
}

