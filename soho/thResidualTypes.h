#ifndef THRESIDUALTYPES_H
#define THRESIDUALTYPES_H

#include "phSpanUtil.h"
#include "thSkyTypes.h"
#include "thMleTypes.h"
#include "thMapTypes.h"

#define thResidualReportNew thResidual_reportNew
#define thResidualReportDel thResidual_reportDel

typedef struct residual_report {
	OBJMASK *mask; /* input mask */
	OBJMASK *binmask; /* binned mask */
	int rbin, cbin;
	AMPL *ampl; /* gain matrix */
	REGION *diff; /* difference between data and fit results */
	REGION *bindiff; /* binned image */ 
	THPIX mean, variance, stddev; /* variance of the binned image */
	int npix, binpix; /* number of pixels in mask and binmask */
	int npix_eff, binpix_eff; /* number of pixels in mask and binmask normalized with gain */
	/* source information */
	LSTRUCT *lstruct;
	FITID fitid;
	/* compilation status */
	MCFLAG cflag;
} RESIDUAL_REPORT; /* pragma IGNORE */

RESIDUAL_REPORT *thResidualReportNew();
void thResidualReportDel(RESIDUAL_REPORT *x);

RET_CODE thCreateResidualReport(REGION *diff, OBJMASK *mask, int rbin, int cbin, RESIDUAL_REPORT **res);
RET_CODE thCreateResidualReportChain(REGION *diff, OBJMASK *mask, int rbin1, int rbin2, int cbin1, int cbin2, CHAIN *reschain);

#endif


