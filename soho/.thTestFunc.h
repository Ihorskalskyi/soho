#ifndef THTESTFUNC_H
#define THTESTFUNC_H

#include "thBasic.h"
#include "thMath.h"
#include "thFuncLoad.h"
#include "thFuncmodelTypes.h"
#include "thProfile.h"

typedef struct testpar {
	/* test parameters */
	FL32 a;
	FL32 b;

	/* galaxy profiles */
	FL32 r_e;
	FL32 n_sersic;
	FL32 epsilon;
	FL32 phi;
	FL32 rowc, colc;
		
	/* the following is private */
	FL32 b_n;
	FL32 a_xx, a_xy, a_yy;	

	/* sky basis */	
	int irow, icol;
	int mrow, mcol;
	int nrow, ncol;
} TESTPAR;

TESTPAR *thTestparNew();
void thTestparDel(TESTPAR *q);

RET_CODE sinxcosy(TESTPAR *q, FUNCMODEL *f);
RET_CODE sinxsiny(TESTPAR *q, FUNCMODEL *f);
#if 0
RET_CODE deV(TESTPAR *q, FUNCMODEL *f);
#endif
RET_CODE SersicProfile(TESTPAR *q, FUNCMODEL *f);
RET_CODE deVprofile(TESTPAR *q, FUNCMODEL *f);

FL32 deVpixel(PIXPOS *pixpos, void *q);
FL32 SersicPixel(PIXPOS *pixpos, TESTPAR *q);

RET_CODE BsplineXTcheb(TESTPAR *q, FUNCMODEL *f);


#endif
