#ifndef THIOTYPES_H
#define THIOTYPES_H                                                                                                                  
#include "dervish.h"                                            
#include "phPhotoFitsIO.h"                                          
#include "phSpanUtil.h"        
#include "phObjc.h"
                                  
#include "thConsts.h"
#include "thFnalTypes.h"
#include "thReg.h"
#include "thMathTypes.h"      
#include "thSkyTypes.h"
#include "thFuncmodelTypes.h"
#include "thFuncmodelIoTypes.h" 
#include "thProcTypes.h"



typedef struct skymodelio {
  
  /* frameid unpacked */
  int run, rerun, field, camcol, camrow, filter, mjd;

  /* sky coeffs unpacked: */
  int bsize;
  FL32 c[SKYBSIZE];
  FL32 cov[SKYBSIZE][SKYBSIZE], invcov[SKYBSIZE][SKYBSIZE];

  /* the rest of sky model */
  REGION *reg;
  FL64 bzero, bscale;

  /* log likelihood of the model */
  FL32 loglikelihood;
  int npixel;

  /* framefiles unpacked - mask and object selection files */
  char *thmaskselfile;
  char *thobjcselfile;

} SKYMODELIO;

CHAIN *thChainObjcIoNewBlock(int ncolor, int nchain);

SKYMODELIO *thSkymodelioNew(int nrow, int ncol);
void thSkymodelioDel(SKYMODELIO *sm);

SKYMODELIO *thSkymodelioPackFrame(FRAME *f);
CHAIN *thSkymodelIoPackProc(PROCESS *proc);

#endif
