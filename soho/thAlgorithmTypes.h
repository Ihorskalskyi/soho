#ifndef THALGORITHMTYPES_H
#define THALGORITHMTYPES_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ftcl.h"
#include "dervish.h"
#include "strings.h"

#include "thConsts.h"
#include "thMath.h"
#include "thDebug.h"

#define thAdjMatrixNew thAdj_matrixNew
#define thAdjMatrixDel thAdj_matrixDel
#define thAlgorithmStepNew thAlgorithm_stepNew
#define thAlgorithmStepDel thAlgorithm_stepDel

typedef enum fit_stat {
	GENERAL_LINEAR_MODEL, FITMODEL, NEIGHBOR_TO_FITMODEL,
	N_FITSTAT, UNKNOWN_FITSTAT
} FITSTAT;

typedef enum mem_stat {
	UNLOADED, LOADED_IN_MEMORY,
	N_MEMSTAT, UNKNOWN_MEMSTAT
} MEMSTAT;

#define ALGORITHM_DESIGN int
#define INNER_PRODUCTS ((ALGORITHM_DESIGN) 1)<<0
#define CREATE_MODEL ((ALGORITHM_DESIGN) 1)<<1
#define VALID_ALGORITHM_DESIGNS (CREATE_MODEL | INNER_PRODUCTS)

typedef enum run_stage {
	INITRUN, MIDDLERUN, ENDRUN, UNKNOWN_RUNSTAGE, N_RUNSTAGE
} RUNSTAGE;

typedef enum naive_run {
	IMPOSSIBLE_NAIVERUN, POSSIBLE_NAIVERUN, UNKNOWN_NAIVERUN,
	N_NAIVERUN
} NAIVERUN;

typedef struct nodememory {
	int node;
	MEMFL mem;
} NODEMEMORY; /* pragma IGNORE */
 
typedef enum algorithm_action {
	NODE_LOAD, NODE_UNLOAD, NODE_ADDTOMODEL, 
	EDGE_COMPUTE, COST_COMPUTE, INIT_MODEL, 
	PRINT_PROGRESS, 
	UNKNOWN_ALGORITHM_ACTION, N_ALGORITHM_ACTION
	} ALGORITHM_ACTION;

/* defining ALG_ACTION as short saves memory space when allocating algorithm */
/* 
typedef short ALGORITHM_ACTION;
#define NODE_LOAD ((ALGORITHM_ACTION) 0)
#define NODE_UNLOAD ((ALGORITHM_ACTION) 1)
#define EDGE_COMPUTE ((ALGORITHM_ACTION) 2)
#define PRINT_PROGRESS ((ALGORITHM_ACTION) 3)
#define NODE_ADDTOMODEL ((ALGORITHM_ACTION) 4)
#define INIT_MODEL ((ALGORITHM_ACTION) 5)
#define UNKNOWN_ALGORITHM_ACTION ((ALGORITHM_ACTION) 6)
#define N_ALGORITHM_ACTION ((ALGORITHM_ACTION) 7)
*/

/* defining ALG_NODE as short instead of int saves memory space when allocating algorithm */
typedef short ALGORITHM_NODE; /* pragma SCHEMA */

typedef struct adj_mat {
	MEMFL **matrix;
	MEMFL **wmatrix;
	MEMFL *memspace;
	MEMFL *degree;
	MEMSTAT *memstat;
	FITSTAT *fitstat;
	int *list;
	NODEMEMORY *node_mem_list;

	int n, nmax;
} ADJ_MATRIX; /* pragma IGNORE */


typedef struct algorithm_step {
	ALGORITHM_ACTION action;
	ALGORITHM_NODE node1;
	ALGORITHM_NODE node2;
	int runtime;
	int runtime_source;
	} ALGORITHM_STEP; /* pragma IGNORE */

typedef struct algorithm {
	MEMDBLE memory_total;
	ALGORITHM_STEP *steps;
	int nsteps;
	int nmax;

	/* private info */
	void *parent;
} ALGORITHM; /* pragma IGNORE */

NODEMEMORY *thNodememoryNew();
void thNodememoryDel(NODEMEMORY *x);

ADJ_MATRIX *thAdjMatrixNew();
void thAdjMatrixDel(ADJ_MATRIX *adj);
RET_CODE thAdjMatrixRenew(ADJ_MATRIX *adj, int n);

ALGORITHM_STEP *thAlgorithmStepNew();
void thAlgorithmStepDel(ALGORITHM_STEP *x);

ALGORITHM *thAlgorithmNew();
void thAlgorithmDel(ALGORITHM *x);
RET_CODE thAlgorithmGetNsteps(ALGORITHM *alg, int *n);
ALGORITHM_STEP *thAlgorithmStepGetByPos(ALGORITHM *alg, int pos, RET_CODE *status);

RET_CODE get_memory_total(ALGORITHM *algorithm, MEMDBLE *memory_total);
RET_CODE add_node_to_algorithm(ALGORITHM *algorithm, int node, int runtime);
RET_CODE delete_node_from_algorithm(ALGORITHM *algorithm, int node, int runtime);
RET_CODE add_edge_to_algorithm(ALGORITHM *algorithm, int node1, int node2);
RET_CODE add_pcost_to_algorithm(ALGORITHM *algorithm);

RET_CODE init_model_algorithm(ALGORITHM *algorithm);
RET_CODE add_node_to_model_algorithm(ALGORITHM *algorithm, int node);

RET_CODE thAlgorithmCopy(ALGORITHM *s, ALGORITHM *t);
RET_CODE thAlgorithmStepCopy(ALGORITHM_STEP *s, ALGORITHM_STEP *t);

RET_CODE thAlgorithmRefresh(ALGORITHM *s);
RET_CODE thAlgorithmStepRefresh(ALGORITHM_STEP *s);

#endif
