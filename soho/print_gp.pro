pro print_gp, file = file



x = mrdfits(file, 2, hdr)
y = mrdfits(file, 1, hdr)

print, " *** initial ***"

print, "counts_deV = ", y.counts_deV
print, "re_deV     = ", y.re_deV
print, "             "
print, "counts_exp = ", y.counts_exp
print, "re_exp     = ", y.re_exp


print, " *** fit value ***"

print, "counts_deV = ", x.counts_deV
print, "re_deV     = ", x.re_deV
print, "             "
print, "counts_exp = ", x.counts_exp
print, "re_exp     = ", x.re_exp

return
end
