pro create_lrg_match_plots

name = "create_lrg_match_plots"

rejection_distance = 3.0
prefix0 = "nearby-"
boxplots = 0
camcol_list = [2, 4, 5, 6]
camcol_list = [2]
run_list = [4207]

suffix = "all-"
mag_thresh = 100
sas1 = 0
sim1 = 1
mle2 = 1 ;; set to 0 when analyzing photo results 
sas3 = 1
locmap = 1

plot_on_demand = 1

dolrg_list = [1, 0, 0]
dogalaxy_list = [0, 1, 0]
dostar_list = 1 - dogalaxy_list - dolrg_list

dolrg_list = [0]
dogalaxy_list = [1]
dostar_list = 1 - dogalaxy_list - dolrg_list

;; /u/khosrow/thesis/opt/soho/multi-object/images/mle/sdss-g0-s0-cd1-extended/deV-exp-fpC-multiple-corrected/301/4797/2/objc

dir_list  = ["sdss-gA-s0-cd1","sdss-gA-sA-cd1"]
dir_list  = ["sdss-g0-s0-cd1", "sdss-gA-s0-cd1","sdss-gA-sA-cd1", $
	"sdss-gA-sA-lrg1", $
	"sdss-gA-s0-lrg1", "sdss-g0-s0-lrgA", $
	"sdss-g0-s0-lrg1", "sdss-gA-sA-lrgA", "sdss-gA-s0-lrgA"]
dir_list = ["sdss-g0-sA-cd1", "sdss-g0-s0-cd1", "sdss-gA-s0-cd1", "sdss-gA-sA-cd1"]
dir_list = ["sdss-g0-s0-cd1-extended", "sdss-g0-s0-cd1"] ;; , "sdss-g0-s0-cd1"]
dir_list = ["sdss-g0-s0-cd1-aligned-2"]
dir_list = ["sdss-g0-s0-lrg1-aligned-6-poisson-noise", "sdss-g0-s0-lrg1-aligned-6-variable-gaussian-noise", "sdss-g0-s0-lrg1-aligned-6-const-noise"]
dir_list = ["sdss-g0-s0-lrg1-aligned-6-const-noise"]

dir_list = ["demand-1", "demand-2", "demand-3", "demand-4", "demand-11", "demand-12", "demand-13", "demand-14"]
dir_list = ["demand-6", "demand-7", "demand-8", "demand-9", "demand-16", "demand-17", "demand-18", "demand-19", "demand-14"]

dir_prefix="demand-"
sim_dir_prefix="sim-demand-"
dir_suffix_numerical_list = [19, 16, 18, 14, 13, 11] ;; , 12, 13, 14, 17, 18, 19, 1, 2, 3, 4, 6, 7, 8, 9] 
dir_suffix_numerical_list = 80 - lindgen(40)
dir_suffix_numerical_list = [52] ;; [57] ;; [52, 57, 72, 77] ;; [72, 77] ;; [52, 57] ;;, 57, 77]
;;dir_suffix_list = ["1", "2", "3", "4", "6", "7", "8", "9", "11", "12", "13", "14", "15", "16", "17", "18", "19"]
dir_suffix_list = strtrim(string(dir_suffix_numerical_list), 2)

mle_type_list = ["deV-exp-fpC-multiple-init-6-pcost-8-DD1-Rlog-stage-1", ""] ;; , "deV-exp-fpC-single-init-6-pcost-8-DD1-Rlog-stage-2"] ;; , "deV-exp-fpC-multiple-init-5-pcost-6"]
mle_type_numerical_list=[0, 100]
mle_type_numerical_list=[60] ;; [0, 100]; , 200, 300, 400, 500, 600]
mle_type_prefix =["mle-demand-"]

mle_type_list2 = ["flag-amplitude", "flag-fitsize", "flag-fitcenter", "flag-fitshape", "flag-fitsize-fitcenter", "flag-fitsize-fitshape", "flag-fitcenter-fitshape", "flag-fitsize-fitcenter-fitshape"]
mle_prefix_list2 = ["flag-amplitude-", "flag-fitsize-", "flag-fitcenter-", "flag-fitshape-", "flag-fitsize-fitcenter-", "flag-fitsize-fitshape-", "flag-fitcenter-fitshape-", "flag-fitsize-fitcenter-fitshape-"]
mle_type_list2 = ["flag-amplitude-5", "flag-fitsize-6",  "flag-fitsize-9", "flag-fitsize-8", "flag-fitsize-7", "flag-fitcenter-5", "flag-fitshape-5", "flag-fitsize-fitcenter-5", "flag-fitsize-fitshape-5", "flag-fitcenter-fitshape-5", "flag-fitsize-fitcenter-fitshape-5"]
mle_prefix_list2 = ["flag-amplitude-5-","flag-fitsize-6-", "flag-fitsize-9-", "flag-fitsize-8-",  "flag-fitsize-7-", "flag-fitcenter-5-", "flag-fitshape-5-", "flag-fitsize-fitcenter-5-", "flag-fitsize-fitshape-5-", "flag-fitcenter-fitshape-5-", "flag-fitsize-fitcenter-fitshape-5-"]

mle_type_list2 = ["flag-fitsize-unconstrained-init-1-nstep-9000-nearby-10-full-sky-apsector-hqn", "flag-fitsize-unconstrained-init-1-nstep-9000-nearby-10-sky-subtracted-apsector-hqn", "flag-fitsize-unconstrained-init-1-nstep-9000-nearby-10-full-sky-apsector", "flag-fitsize-unconstrained-init-1-nstep-9000-nearby-10-sky-subtracted-apsector", "flag-amplitude-unconstrained-init-1-nstep-9000-nearby-10-full-sky", "flag-fitsize-unconstrained-init-1-nstep-9000-nearby-10-full-sky",  "flag-fitsize-init-1-nstep-0-nearby-10", "flag-amplitude-unconstrained-init-1-nstep-9000-nearby-10-full-sky", "flag-amplitude-unconstrained-init-1-nstep-9000-nearby-10-sky-subtracted", "flag-fitsize-unconstrained-init-1-nstep-9000-nearby-10-sky-subtracted", "flag-fitsize-unconstrained-init-1-nstep-0-nearby-10-full-sky", "flag-fitsize-unconstrained-init-1-nstep-9000-nearby-10-full-sky", "flag-fitsize-accelerated-unconstrained-init-1-nstep-9000-nearby-10", "flag-fitsize-init-1-nstep-0-nearby-10", "flag-fitsize-init-1-nstep-0-nearby-25", "flag-fitsize-init-1-nstep-1-nearby-25", "flag-fitsize-init-1-nstep-9000-nearby-25", "flag-fitsize-init-1-nstep-0-nearby", "flag-fitsize-init-1-nstep-1-nearby", "flag-fitsize-init-1-nstep-9000-nearby", "flag-fitsize-init-1-nstep-0", "flag-fitsize-init-1-nstep-1", "flag-fitsize-init-1-nstep-9000", "flag-fitsize-explore-1a", "flag-fitsize-6", "flag-fitsize-7", "flag-fitsize-8", "flag-fitsize-9",  "flag-fitsize-10", "flag-fitsize-11", "flag-fitsize-12", "flag-fitsize-13"]
mle_prefix_list2 = ["flag-fitsize-unconstrained-init-1-nstep-9000-nearby-10-full-sky-apsector-hqn-", "flag-fitsize-unconstrained-init-1-nstep-9000-nearby-10-sky-subtracted-apsector-hqn-", "flag-fitsize-unconstrained-init-1-nstep-9000-nearby-10-full-sky-apsector-", "flag-fitsize-unconstrained-init-1-nstep-9000-nearby-10-sky-subtracted-apsector-", "flag-amplitude-unconstrained-init-1-nstep-9000-nearby-10-full-sky-", "flag-fitsize-unconstrained-init-1-nstep-9000-nearby-10-full-sky-", "flag-fitsize-init-1-nstep-0-nearby-10-", "flag-amplitude-unconstrained-init-1-nstep-9000-nearby-10-full-sky-", "flag-amplitude-unconstrained-init-1-nstep-9000-nearby-10-sky-subtracted-", "flag-fitsize-unconstrained-init-1-nstep-9000-nearby-10-sky-subtracted-", "flag-fitsize-unconstrained-init-1-nstep-0-nearby-10-full-sky-", "flag-fitsize-unconstrained-init-1-nstep-9000-nearby-10-full-sky-", "flag-fitsize-accelerated-unconstrained-init-1-nstep-9000-nearby-10-", "flag-fitsize-init-1-nstep-0-nearby-10-", "flag-fitsize-init-1-nstep-0-nearby-25-", "flag-fitsize-init-1-nstep-1-nearby-25-", "flag-fitsize-init-1-nstep-9000-nearby-25-", "flag-fitsize-init-1-nstep-0-nearby-", "flag-fitsize-init-1-nstep-1-nearby-", "flag-fitsize-init-1-nstep-9000-nearby-", "flag-fitsize-init-1-nstep-0-", "flag-fitsize-init-1-nstep-1-", "flag-fitsize-init-1-nstep-9000-", "flag-fitsize-explore-1a-", "flag-fitsize-6-", "flag-fitsize-7-", "flag-fitsize-8-", "flag-fitsize-9-", "flag-fitsize-10-", "flag-fitsize-11-",  "flag-fitsize-12-", "flag-fitsize-13-"]

;; mle_type_list2 = ["flag-amplitude-unconstrained-init-1-nstep-9000-nearby-10-full-sky"]
;; mle_prefix_list2 = ["flag-amplitude-unconstrained-init-1-nstep-9000-nearby-10-full-sky-"]

select_index = [2, 0] ;, 1] ;; , 1, 2, 4, 5, 7, 8]
mle_type_list2 = mle_type_list2[select_index]
mle_prefix_list2 = mle_prefix_list2[select_index]

flagLRG_list = [-1, 1, 2] 
flagLRG_list = [-1]

distance_match_flag_list = [1, 0]
dm_prefix_list = ["pos-match-", "pos-shape-match-"]
distance_match_flag_list = [distance_match_flag_list[0:0]]
dm_prefix_list = [dm_prefix_list[0:0]]

chisq_list = [5.0D10, 15.0D6]
chisq_prefix_list = ["chisq-5D10-", "chisq-15D6-"]
chisq_list = [chisq_list[0:0]]
chisq_prefix_list = [chisq_prefix_list[0:0]]

do_fake_cd2 = 0

root3="/u/dss/sas/dr12/boss/photo/redux"

if (plot_on_demand) then begin
	print, name, ": WARNING - plotting mle and sim on demand"
	print, name, ": demand_suffix_numerical_list = ", dir_suffix_numerical_list
	print, name, ": demand_suffix_list = ", dir_suffix_list

	dir_list = lindgen(n_elements(dir_suffix_list))
	mle_type_list = lindgen(n_elements(mle_type_numerical_list))
endif

mixed_noise_dir_list = [""] ;; , "mixed-noise"]
mixed_noise_prefix_list = [""] ;; , "mixed-noise-"]

;; mixed_noise_dir = ""
;; mixed_noise_prefix = ""

report_all_created = 0
;; chisq_all_filename = "/u/khosrow/thesis/opt/soho/plots/plot-contour-sim-demand-57a-mle-demand-57-full-sky-CHISQ-all.eps"
;; cost_all_filename = "/u/khosrow/thesis/opt/soho/plots/plot-contour-sim-demand-57a-mle-demand-57-full-sky-COST-all.eps"

chisq_all_filename = "/u/khosrow/thesis/opt/soho/plots/plot-contour-sim-demand-52a-mle-demand-112-full-sky-CHISQ-all.eps"
cost_all_filename = "/u/khosrow/thesis/opt/soho/plots/plot-contour-sim-demand-52a-mle-demand-112-full-sky-COST-all.eps"

for i_objc_type = 0L, n_elements(dolrg_list) - 1L do begin
for i_flagLRG = 0L, n_elements(flagLRG_list) - 1L, 1L do begin
for i_dm_list = 0L, n_elements(distance_match_flag_list) - 1L, 1L do begin
for i_chisq = 0L, n_elements(chisq_list) - 1L, 1L do begin
for i_dir = 0L, n_elements(dir_list) - 1L, 1L do begin
for i_mixed_noise = 0L, n_elements(mixed_noise_dir_list) - 1L do begin
for i_mle_type = 0L, n_elements(mle_type_list) - 1L do begin
for i_mle_type2 = 0L, n_elements(mle_type_list2) - 1L do begin

dolrg = dolrg_list[i_objc_type]
dogalaxy = dogalaxy_list[i_objc_type]
dostar = dostar_list[i_objc_type]

dir = dir_list[i_dir]
sim_dir = dir
mle_type = mle_type_list[i_mle_type]
mle_type2 = mle_type_list2[i_mle_type2]

mixed_noise_dir = mixed_noise_dir_list[i_mixed_noise]
mixed_noise_prefix = mixed_noise_prefix_list[i_mixed_noise]

if (plot_on_demand) then begin
	dir_suffix = dir_suffix_list[i_dir] + "a"
	sim_dir = sim_dir_prefix+dir_suffix
	dir = dir_prefix+dir_suffix
	mle_dir = mle_type_prefix + strtrim(string(mle_type_numerical_list[i_mle_type] + dir_suffix_numerical_list[i_dir]), 2)
	mle_type = mle_dir 
endif

root1="/u/khosrow/thesis/opt/soho/multi-object/images/sims/float/"+mixed_noise_dir+"/"+dir
if (mle2 eq 0) then begin
	root2 = root1 ;; used when analyzing photo results
endif else begin
	root2="/u/khosrow/thesis/opt/soho/multi-object/images/mle/float/" + mixed_noise_dir + "/" + sim_dir+"/"+mle_type+"/"+mle_type2
endelse
root3 = root1
sas3 = 0

flagLRG = flagLRG_list[i_flagLRG]
distance_match = distance_match_flag_list[i_dm_list]
dm_prefix = dm_prefix_list[i_dm_list]
rejection_chisq = chisq_list[i_chisq]
chisq_prefix = chisq_prefix_list[i_chisq]
mle_prefix2 = mle_prefix_list2[i_mle_type2]

prefix = dir+"-"+prefix0 + dm_prefix + chisq_prefix + mle_prefix2
if (plot_on_demand) then begin
	prefix=mixed_noise_prefix+sim_dir+"-"+mle_dir+"-"+prefix0 + dm_prefix + chisq_prefix + mle_prefix2
endif

print, "root1 = ", root1
print, "root2 = ", root2
print, "flagLRG = ", flagLRG
print, "match style = ", dm_prefix
print, "rejection chisq = ", rejection_chisq
print, "mle style = ", mle_type
print, "mle prefix = ", mle_prefix2
print, "object type (lrg, star, galaxy) = ", dolrg, dostar, dogalaxy 
report_item = draw_matched_pairs_for_run_list(root1 = root1, root2 = root2, root3 = root3, camcol_list = camcol_list, $
	sas1 = sas1, sim1 = sim1, sas3 = sas3, mle2 = mle2, fake_cd2 = do_fake_cd2, boxplots = boxplots, $
	galaxy = dogalaxy, star = dostar, lrg = dolrg, flagLRG = flagLRG, distance_match = distance_match, $
	locmap = locmap, mag_thresh = mag_thresh, prefix = prefix, suffix = suffix, $
	rejection_distance = rejection_distance, rejection_chisq = rejection_chisq, run_list = run_list)

if (report_all_created) then begin
	report_all = [report_all, report_item]
endif else begin
	report_all = report_item;
	report_all_created = 1
endelse

endfor
endfor
endfor
endfor
endfor
endfor
endfor
endfor

tps = 0
print, name, ": creating contour plots "
print, name, ": chi-squared value output to: ", chisq_all_filename
do_chisq_contour, output = chisq_all_filename, data = report_all, tps = tps
print, name, ": cost value output to: ", cost_all_filename
do_chisq_contour, output = cost_all_filename, data = report_all, /docost

return
end
