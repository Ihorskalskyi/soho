#!/bin/bash

run="001033"
root="./simfit"
fpCprefix="fpC"
fpCCprefix="fpCC"

iband=$1
camcol=4

declare -a band_a=("u" "g" "r" "i" "z")
band=${band_a[$iband]}
declare -a field_a=("0101" "0102" "0103" "0104" "0105")

#declare -a psf_a=(9999 999 99 90 80 70 60 50 40 30 20 10)
declare -a psf_a=(9999 50)
nband=5
npsf=2
nfield=5

for (( ipsf=0; ipsf<$npsf; ipsf++ ))
do		
	f1=${psf_a[$ipsf]}
	f2=$f1

	dir="$f1-$f2"
		
	executable="do-mle"
	parfile="$root/$dir/mle-$run-$band$camcol.par"
	logfile="nohup.do-mle.$f1-$f2-$run-$band$camcol.out";

	command="nohup $executable $parfile 42 > $logfile"
	echo "band=$band camcol=$camcol run=$run field=$field psf=$f1"
	#echo $command
	eval $command

done

echo "end of all simulations for band=$band"
