#include "thFitobjcTypes.h"

FITOBJC *thFitobjcNew() {
  FITOBJC *objc;
  objc = (FITOBJC*) thCalloc(1, sizeof(FITOBJC));
  objc->type = UNKNOWN_FITOBJC;
  
  return(objc);
}

RET_CODE thFitobjcDel(FITOBJC *objc) {
 
  if (objc == NULL) return(SH_SUCCESS);
  
  objc->val = NULL;
  objc->upper = NULL;
  objc->map = NULL;

  thFree(objc);
  return(SH_SUCCESS);
}
  
RET_CODE thFitobjcPut(FITOBJC *objc, void *val, FITOBJC_TYPE type, 
		      void *upper, void *map) {

  char *name = "thFitobjcPut";

  if (objc ==  NULL) {
    thError("%s: Error: a null FITOBJC cannot be changed", name);
    return(SH_GENERIC_ERROR);
  }

  if (val == NULL && objc->val != NULL) {
    thError("%s: Warning - nullifying VAL record", name);
  }
  objc->val = val;

  if (type == UNKNOWN_FITOBJC && objc->type != UNKNOWN_FITOBJC) {
    thError("%s: Warning - converting TYPE to UNKNOWN", name);
  }
  objc->type = type;

  if (upper == NULL && objc->upper != NULL) {
    thError("%s: Warning - nullifying UPPER record", name);
  }
  objc->upper = upper;

  if (map == NULL && objc->map != NULL) {
    thError("%s: Warning - nullifying MAP record", name);
  }
  objc->map = map;

  return(SH_SUCCESS);
}
