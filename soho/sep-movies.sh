#!/bin/bash
sourceroot="/u/khosrow/thesis/opt/soho/single-galaxy/images"
targetroot="/u/khosrow/thesis/opt/soho/single-galaxy/movies/oct-16-movies"
extension="subbed.avi"

declare -a dir1_a=("exp" "deV"); n1=2
declare -a dir2_a=("deV-exp" "exp" "deV" "deV-exp"); n2=3
declare -a dir3_a=("fpC" "fpCC"); n3=2
declare -a dir4_a=("single" "multiple"); n4=2


for (( i1=0; i1<n1; i1++ )) do
dir1=${dir1_a[$i1]}

for (( i2=0; i2<n2; i2++ )) do
dir2=${dir2_a[$i2]}

for (( i3=0; i3<n1; i3++ )) do
dir3=${dir3_a[$i3]}

for (( i4=0; i4<n4; i4++ )) do
dir4=${dir4_a[$i4]}

dir="$dir1/9999-9999/$dir2-$dir3-$dir4"
sourcedir="$sourceroot/$dir"
targetdir="$targetroot/$dir"

if [[ ! -d $targetdir ]]; then
	mkdir -p $targetdir
fi
echo $dir
command="rsync -r $sourcedir/*.$extension $targetdir"
eval $command

done
done
done
done
