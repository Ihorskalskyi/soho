function get_location_for_object, objc
loc = {xc:!value_is_bad, yc:!value_is_bad}
if (n_elements(objc) ne 1) then return, loc
if (size(objc, /type) ne 8) then return, loc

if (tag_exist(objc, "I_STAR")) then begin
	if (objc.counts_star ne 0.0) then begin
		loc.xc = objc.xc_star
		loc.yc = objc.yc_star
	endif
	if (objc.counts_dev ne 0.0) then begin
		loc.xc = objc.xc_dev
		loc.yc =  objc.yc_dev
	endif
	if (objc.counts_dev1 ne 0.0) then begin
		loc.xc = objc.xc_dev1
		loc.yc = objc.yc_dev1
	endif
	if (objc.counts_dev2 ne 0.0) then begin
		loc.xc = objc.xc_dev2
		loc.yc =  objc.yc_dev2
	endif
	if (objc.counts_exp ne 0.0) then begin
		loc.xc = objc.xc_exp
		loc.yc = objc.yc_exp
	endif
	if (objc.counts_exp1 ne 0.0) then begin
		loc.xc = objc.xc_exp1
		loc.yc = objc.yc_exp1
	endif
	if (objc.counts_exp2 ne 0.0) then begin
		loc.xc = objc.xc_exp2
		loc.yc = objc.yc_exp2
	endif
endif else begin
	loc.xc = objc.objc_colc
	loc.yc = objc.objc_rowc
endelse
return, loc
end

function get_location_for_list, objclist

loc = {xc:!value_is_bad, yc:!value_is_bad}

if (n_elements(objclist) eq 0) then return, loc
if (size(objclist[0], /type) ne 8) then return, loc

n = n_elements(objclist)
loclist = replicate(loc, n)

if (tag_exist(objclist[0], "I_STAR")) then begin
	ntype = 0
	tndex = where(objclist.counts_star ne 0.0, ntype)
	if (ntype ne 0) then begin
		loclist[tndex].xc = objclist[tndex].xc_star
		loclist[tndex].yc = objclist[tndex].yc_star
	endif
	ntype = 0
	tndex = where(objclist.counts_dev ne 0.0, ntype)
	if (ntype ne 0) then begin
		loclist[tndex].xc = objclist[tndex].xc_dev
		loclist[tndex].yc = objclist[tndex].yc_dev
	endif
	ntype = 0
	tndex = where(objclist.counts_dev1 ne 0.0, ntype)
	if (ntype ne 0) then begin
		loclist[tndex].xc = objclist[tndex].xc_dev1
		loclist[tndex].yc = objclist[tndex].yc_dev1
	endif
	ntype = 0
	tndex = where(objclist.counts_dev2 ne 0.0, ntype)
	if (ntype ne 0) then begin
		loclist[tndex].xc = objclist[tndex].xc_dev2
		loclist[tndex].yc = objclist[tndex].yc_dev2
	endif
	ntype = 0
	tndex = where(objclist.counts_exp ne 0.0, ntype)
	if (ntype ne 0) then begin
		loclist[tndex].xc = objclist[tndex].xc_exp
		loclist[tndex].yc = objclist[tndex].yc_exp
	endif
	ntype = 0
	tndex = where(objclist.counts_exp1 ne 0.0, ntype)
	if (ntype ne 0) then begin
		loclist[tndex].xc = objclist[tndex].xc_exp1
		loclist[tndex].yc = objclist[tndex].yc_exp1
	endif
	ntype = 0
	tndex = where(objclist.counts_exp2 ne 0.0, ntype)
	if (ntype ne 0) then begin
		loclist[tndex].xc = objclist[tndex].xc_exp2
		loclist[tndex].yc = objclist[tndex].yc_exp2
	endif
endif else begin
	tndex = lindgen(n)
	loclist[tndex].xc = objclist[tndex].objc_colc
	loclist[tndex].yc = objclist[tndex].objc_rowc
endelse
return, loclist
end

function calculate_distance, list, objc
if (n_elements(list) eq 0) then return, !value_is_bad
if (n_elements(objc) ne 1) then return, !value_is_bad
n = n_elements(list)
delta = replicate(!value_is_bad, n);
loc1 = get_location_for_object(objc)
loclist = get_location_for_list(list)
index = lindgen(n)
xc1 = loc1.xc
yc1 = loc1.yc
delta[index] = (loclist[index].xc - xc1) ^ 2 + (loclist[index].yc - yc1) ^ 2	
delta = !pixelsize * sqrt(delta)
return, delta
end

function find_fraction_prof_in_prof, r1 = r1, r2 = r2, n1 = n1, k1 = k1, status = status

fraction = !value_is_bad
if (n_elements(r1) ne n_elements(r2) or n_elements(n1) ne 1 or n_elements(k1) ne 1) then begin
	status = -1
	return, fraction
endif
if (n1 le 0.0 or k1 le 0.0) then begin
	status = -1
	return, fraction
endif


x = k1 * (r2 / r1) ^ (1.0 / n1)
a = 2.0 * n1
nr1 = n_elements(r1)
fraction = replicate(!value_is_bad, nr1)
for i = 0, nr1 - 1L, 1L do begin
	if (x[i] ge 0.0 and finite(x[i])) then fraction[i] = igamma(a, x[i])
endfor

status = 0
return, fraction
end



function turn_counts_into_mags, counts, calib_info = calib_info

badcount = 0
bad_index = where(counts eq !VALUE_IS_BAD, badcount)

bfilter = [1.4E-10, 0.9E-10, 1.2E-10, 1.8E-10, 7.4E-10]
A = 1.0E8 * calib_info.flux20;
ff0 = counts / A
B = 2.0 * bfilter
x = ff0 / B
C = replicate(2.5 / alog(10.0), n_elements(A));
D = alog(bfilter);
mag = -C*(asinh(x)+ D);

if (badcount gt 0) then mag[bad_index] = 1.0 * !VALUE_IS_BAD

return, mag
end

function get_galactic_extinction, rowc = rowc, colc = colc, galactic_calib = galactic_calib

extinction = galactic_calib[*].BETA[0] + $
	galactic_calib[*].BETA[1] * rowc + galactic_calib[*].BETA[2] * colc + $
	galactic_calib[*].BETA[3] * rowc ^ 2 + galactic_calib[*].BETA[4] * rowc * colc+$
	galactic_calib[*].BETA[5] * colc ^ 2 + galactic_calib[*].BETA[6] * rowc ^ 3 + $
	galactic_calib[*].BETA[7] * rowc ^ 2 * colc + galactic_calib[*].BETA[8] * rowc * colc ^ 2 + $
	galactic_calib[*].BETA[9] * colc ^ 3 + galactic_calib[*].BETA[10] * rowc ^ 4 + $
	galactic_calib[*].BETA[11] * rowc ^ 3 * colc + galactic_calib[*].BETA[12] * rowc ^ 2 * colc ^ 2 + $
	galactic_calib[*].BETA[13] * rowc * colc ^ 3 + galactic_calib[*].BETA[14] * colc ^ 4 

return, extinction
end

function calib_objc, objc, calib_info = calib_info, galactic_calib = galactic_calib, status = status

name = "calib_objc"
NCOLOR = 5
if (n_elements(galactic_calib) ne NCOLOR or n_elements(calib_info) ne NCOLOR) then begin
	if (n_elements(galactic_calib) ne NCOLOR) then print, name, ": ERROR - problematic galactic calib ~", n_elements(galactic_calib)
	if (n_elements(calib_info) ne NCOLOR) then print, name, ": ERROR - problematic calib_info ~", n_elements(calib_info)
	status = -1
	return, [0]
endif
BAD_ARRAY = replicate(1.0 * !VALUE_IS_BAD, NCOLOR)
cphobjc = {id: !VALUE_IS_BAD, rowc: 1.0 * !VALUE_IS_BAD, colc: 1.0 * !VALUE_IS_BAD, $
	PetroMag: BAD_ARRAY, PetroTh50: BAD_ARRAY, $
	psfMag: BAD_ARRAY, ModelMag: BAD_ARRAY, cModelMag: BAD_ARRAY, $
	extinction: BAD_ARRAY}


cphobjc.id = objc.id
cphobjc.rowc = objc.objc_rowc
cphobjc.colc = objc.objc_colc

cphobjc.psfMag = turn_counts_into_mags(objc.PsfCounts, calib_info = calib_info)
cphobjc.ModelMag = turn_counts_into_mags(objc.counts_model, calib_info = calib_info)
cphobjc.PetroMag = turn_counts_into_mags(objc.PetroCounts, calib_info = calib_info)

counts_cmodel = objc.fracPSF * objc.counts_dev + (1.0 - objc.fracPSF) * objc.counts_exp
cphobjc.cModelMag = turn_counts_into_mags(counts_cmodel, calib_info = calib_info)
cphobjc.PetroTh50 = objc.PetroR50 * !PIXELSIZE
cphobjc.extinction = get_galactic_extinction(rowc = objc.objc_rowc, colc = objc.objc_colc, $
	galactic_calib = galactic_calib)

return, cphobjc
end

function get_all_mags, objc, status = status

status = 0

G_BAND = 1
R_BAND = 2
I_BAND = 3

g = objc.petroMag[G_BAND] - objc.extinction[G_BAND]
r = objc.petroMag[R_BAND] - objc.extinction[R_BAND]
i = objc.petroMag[I_BAND] - objc.extinction[I_BAND]

mags = {g: !VALUE_IS_BAD, r: !VALUE_IS_BAD, i: !VALUE_IS_BAD}
mags.g = g
mags.r = r
mags.i = i

return, mags
end

function get_mu_from_mag, mag, theta, status = status
status = 0
if (mag eq !VALUE_IS_BAD or theta eq !VALUE_IS_BAD) then return, 1.0 * !VALUE_IS_BAD	
mu  = mag + 2.5 * alog10(2.0 * !PI * theta ^ 2);

return, mu
end
	

function  get_necessary_mags, objc, band = band, status = status

status = 0
PetroMag = objc.petroMag[band] - objc.extinction[band]
PetroTheta = objc.PetroTh50[band]
PetroMu = get_mu_from_mag(PetroMag, PetroTheta, status = status)
PsfMag = objc.psfMag[band]
ModelMag = objc.modelMag[band]

mags = {PetroMag: !VALUE_IS_BAD, PetroTheta: !VALUE_IS_BAD, PetroMu: !VALUE_IS_BAD, PsfMag: !VALUE_IS_BAD, ModelMag: !VALUE_IS_BAD}
mags.PetroMag = PetroMag
mags.PetroTheta = PetroTheta
mags.PetroMu = PetroMu
mags.PsfMag = PsfMag 
mags.ModelMag = ModelMag

return, mags
end

function get_c1_c2_for_objc, objc, status = status
status = 0
Mag = get_all_mags(objc)
c1 = (Mag.r - Mag.i) - (Mag.g - Mag.r) / 4.0 - 0.18;
c2 = (!DEFAULT_C_LRG) * (Mag.g - Mag.r) + (1.0 - !DEFAULT_C_LRG) * 4.0 * ((Mag.r - Mag.i) - 0.18);
if (Mag.r eq !VALUE_IS_BAD or Mag.i eq !VALUE_IS_BAD or Mag.g eq !VALUE_IS_BAD) then begin
	c1 =  !VALUE_IS_BAD;
	c2 =  !VALUE_IS_BAD;
endif

return, [c1, c2]
end



function test_LRG_cut1, objc, status = status

status = 0
cvalues = get_c1_c2_for_objc(objc, status = status);
if (status ne 0) then return, 0L
c1 = cvalues[0]
c2 = cvalues[1]

r_band = 2
r = get_necessary_Mags(objc, band = r_band, status = status)
if (status ne 0) then return, 0L

if (r.PetroMag eq !VALUE_IS_BAD or r.PetroMu eq  !VALUE_IS_BAD or r.PsfMag eq !VALUE_IS_BAD or r.ModelMag eq !VALUE_IS_BAD) then return, 0

res = 0L
d1 = (r.PetroMag lt 13.1 + c2 / 0.3);
d2 = (r.PetroMag lt 19.2);
d3 = (abs(c1) lt 0.2);
d4 = (r.PetroMu lt 24.2);
d5 = ((r.PsfMag - r.ModelMag) gt 0.3);

if (d1 and d2 and d3 and d4 and d5) then res = 1L

return, res
end

function test_LRG_cut2, objc, status = status

status = 0
cvalues = get_c1_c2_for_objc(objc, status = status);
if (status ne 0) then return, 0L
c1 = cvalues[0]
c2 = cvalues[1]

r_band = !R_BAND
r = get_necessary_Mags(objc, band = r_band, status = status)
if (status ne 0) then return, 0L

mag = get_all_mags(objc, status = status)

if (r.PetroMag eq !VALUE_IS_BAD or r.PetroMu eq  !VALUE_IS_BAD or r.PsfMag eq !VALUE_IS_BAD or r.ModelMag eq !VALUE_IS_BAD) then return, 0
if (mag.r eq !VALUE_IS_BAD or mag.g eq !VALUE_IS_BAD or mag.i eq !VALUE_IS_BAD) then return, 0

res = 0L
d1 = (r.PetroMag lt 19.5);
d2 = (c1 gt 0.45 - (Mag.g - Mag.r) / 6.0);
d3 = ((Mag.g - Mag.r) gt 1.30 + (0.25) * (Mag.r - Mag.i));
d4 = (r.PetroMu lt 24.2);
d5 = ((r.PsfMag - r.ModelMag) gt 0.5);

if (d1 and d2 and d3 and d4 and d5) then res = 2L
return, res
end

function test_LRG, objc, status = status 
name = "test_LRG"
status = 0
c1 = test_LRG_cut1(objc, status = status);
if (status ne 0) then begin
	print, name, ": ERROR - could not perform cut1 on object"
	return, 0L
endif
c2 = test_LRG_cut2(objc, status = status);
if (status ne 0) then begin
	print, name, ": ERROR - could not perform cut2 on object"
	return, 0L
endif
c = c1 + c2
return, c
end

function create_flagLRG, list, calib_info = calib_info, galactic_calib = galactic_calib, status = status
name = "create_flagLRG";
status = 0
if (n_elements(list) eq 0) then begin
	print, name, ": ERROR - list should be provided"
	status = -1
	return, [0]
endif

n = n_elements(list)
flags = replicate(0L, [n])
for i = 0L, n-1L, 1L do begin
	objc = list[i]
	cphobjc = calib_objc(objc, $
	calib_info = calib_info, galactic_calib = galactic_calib, status = status)
	if (status ne 0) then begin
		print, name, ": ERROR - could not calibrate object"
		return, [0]
	endif
	flags[i] = test_LRG(cphobjc, status = status)
	if (status ne 0) then begin
		print, name, ": ERROR - could not test LRG cuts for object ", i
		return, [0]
	endif
endfor

status = 0
return, flags
end


pro match_two_lists, list1 = list1, list2 = list2, match_index = match_index, max_delta = max_delta, distance_match = distance_match, galaxy = galaxy, lrg = lrg, star = star, flagLRG1 = flagLRG1, flagLRG2 = flagLRG2
name = "match_two_lists"

if (n_elements(list1) eq 0) then begin
	print, name, ": ERROR - list1 should be provided"
	match_index = [-1]
	return
endif
if (n_elements(list2) eq 0) then begin
	print, name, ": ERROR - list2 should be provoded"
	match_index = [-1]
	return
endif

distance_match = 1
;; if (n_elements(x2) gt n_elements(x1)) then begin
;; 	print, name, ": WARNING - outlist is bigger than inlist"
;; endif

n1 = n_elements(list1)
n2 = n_elements(list2)
if (n1 eq 0 or n2 eq 0) then begin
	print, name, ": ERROR - empty inlist or outlist"
	match_index = [-1]
endif

band = 2 ;; r band
do_cd1 = tag_exist(list1[0], "I_DEV2") or tag_exist(list1[0], "I_SERSIC") or tag_exist(list1[0], "I_SERSIC1") or tag_exist(list1[0], "I_SERSIC2") or tag_exist(list1[0], "I_EXP2")   
do_cd2 = tag_exist(list2[0], "I_DEV2") or tag_exist(list2[0], "I_SERSIC") or tag_exist(list2[0], "I_SERSIC1") or tag_exist(list2[0], "I_SERSIC2") or tag_exist(list2[0], "I_EXP2")
do_star1 = tag_exist(list1[0], "COUNTS_STAR")
do_star2 = tag_exist(list2[0], "COUNTS_STAR")

if (do_cd2) then begin
	if (keyword_set(galaxy) or keyword_set(lrg)) then begin
		y2 = list2.xc_dev	
		yvar = 1.0
		x2 = list2.yc_dev
		xvar = 1.0

		if (tag_exist(list2[0], "I_SERSIC")) then begin
			aux_count = 0
			aux_index = where(list2.I_sersic ne 0.0, aux_count)
			if (aux_count gt 0) then y2[aux_index] = list2[aux_index].xc_sersic
			if (aux_count gt 0) then x2[aux_index] = list2[aux_index].yc_sersic
		endif
		if (tag_exist(list2[0], "I_SERSIC1")) then begin
			aux_count = 0
			aux_index = where(list2.I_sersic1 ne 0.0, aux_count)
			if (aux_count gt 0) then y2[aux_index] = list2[aux_index].xc_sersic1
			if (aux_count gt 0) then x2[aux_index] = list2[aux_index].yc_sersic1
		endif
		if (tag_exist(list2[0], "I_SERSIC2")) then begin
			aux_count = 0
			aux_index = where(list2.I_sersic2 ne 0.0, aux_count)
			if (aux_count gt 0) then y2[aux_index] = list2[aux_index].xc_sersic2
			if (aux_count gt 0) then x2[aux_index] = list2[aux_index].yc_sersic2
		endif
		if (tag_exist(list2[0], "I_DEV1")) then begin
			aux_count = 0
			aux_index = where(list2.I_deV1 ne 0.0, aux_count)
			if (aux_count gt 0) then y2[aux_index] = list2[aux_index].xc_deV1
			if (aux_count gt 0) then x2[aux_index] = list2[aux_index].yc_deV1
		endif
		if (tag_exist(list2[0], "I_DEV2")) then begin
			aux_count = 0
			aux_index = where(list2.I_deV2 ne 0.0, aux_count)
			if (aux_count gt 0) then y2[aux_index] = list2[aux_index].xc_deV2
			if (aux_count gt 0) then x2[aux_index] = list2[aux_index].yc_deV2
		endif
		if (tag_exist(list2[0], "I_EXP1")) then begin
			aux_count = 0
			aux_index = where(list2.I_exp1 ne 0.0, aux_count)
			if (aux_count gt 0) then y2[aux_index] = list2[aux_index].xc_exp1
			if (aux_count gt 0) then x2[aux_index] = list2[aux_index].yc_exp1
		endif
		if (tag_exist(list2[0], "I_EXP2")) then begin
			aux_count = 0
			aux_index = where(list2.I_exp2 ne 0.0, aux_count)
			if (aux_count gt 0) then y2[aux_index] = list2[aux_index].xc_exp2
			if (aux_count gt 0) then x2[aux_index] = list2[aux_index].yc_exp2
		endif
		if (tag_exist(list2[0], "I_PL")) then begin
			aux_count = 0
			aux_index = where(list2.I_pl ne 0.0, aux_count)
			if (aux_count gt 0) then y2[aux_index] = list2[aux_index].xc_pl
			if (aux_count gt 0) then x2[aux_index] = list2[aux_index].yc_pl
		endif
		if (tag_exist(list2[0], "I_PL1")) then begin
			aux_count = 0
			aux_index = where(list2.I_pl1 ne 0.0, aux_count)
			if (aux_count gt 0) then y2[aux_index] = list2[aux_index].xc_pl1
			if (aux_count gt 0) then x2[aux_index] = list2[aux_index].yc_pl1
		endif
		if (tag_exist(list2[0], "I_PL2")) then begin
			aux_count = 0
			aux_index = where(list2.I_pl2 ne 0.0, aux_count)
			if (aux_count gt 0) then y2[aux_index] = list2[aux_index].xc_pl2
			if (aux_count gt 0) then x2[aux_index] = list2[aux_index].yc_pl2
		endif
		if (tag_exist(list2[0], "I_CORESERSIC")) then begin
			aux_count = 0
			aux_index = where(list2.I_coresersic ne 0.0, aux_count)
			if (aux_count gt 0) then y2[aux_index] = list2[aux_index].xc_coresersic
			if (aux_count gt 0) then x2[aux_index] = list2[aux_index].yc_coresersic
		endif

	endif else begin
		y2 = list2.xc_star
		yvar = 1.0
		x2 = list2.yc_star
		xvar = 1.0
	endelse
endif else begin
	x2 = list2.objc_colc
	xvar = (list2.objc_colcErr) ^ 2
	y2 = list2.objc_rowc
	yvar = (list2.objc_rowcErr) ^ 2
endelse

if (do_cd1) then begin
	if (keyword_set(galaxy) or keyword_set(lrg)) then begin
		y1 = list1.xc_dev	
		x1 = list1.yc_dev
	endif else begin
		y1 = list1.xc_star
		x1 = list1.yc_star
	endelse
endif else begin
	x1 = list1.objc_colc
	y1 = list1.objc_rowc
endelse

if (n1 eq 1 and n2 eq 1) then begin
match_index = [0]
input_index = [-1]
endif

if (n1 gt !shaky_n) then begin

print, name, ": WARNING - shaky algorithm"
triangulate, x1, y1, c ; Compute Delaunay triangulation
input_index = lindgen(n1)
match_index = GRIDDATA(x1,y1, input_index, XOUT=x2, YOUT=y2, /NEAREST_N, TRIANGLES =c)
input_index = [-1]

endif else begin

chisq_threshold = !rej_chisq

index2 = lindgen(n2)
match_index = replicate(0L, n2)
distance = replicate(0.0D0, [n1, n2])

type1 = replicate(!unknown_type, n1)
type2 = replicate(!unknown_type, n2)

if (do_cd1) then begin
	frac_dev1 = replicate(0.0, n_elements(list1))
	counts_dev1 = list1[*].counts_dev
	counts_exp1 = list1[*].counts_dev2
	r_dev1 = list1[*].re_dev
	r_exp1 = list1[*].re_dev2
	I_dev1 = list1[*].I_deV
	I_exp1 = list1[*].I_deV2
	IErr_dev1 = list1[*].IErr_deV
	IErr_exp1 = list1[*].IErr_deV2
	counts_sersic1 = replicate(0.0, n_elements(list1))
	r_sersic1 = replicate(0.0, n_elements(list1))
	n_sersic1 = replicate(!value_is_bad, n_elements(list1))
	I_sersic1 = replicate(0.0, n_elements(list1))
	IErr_sersic1 = replicate(0.0, n_elements(list1))
	if (tag_exist(list1[0], "I_PL")) then begin
		;; print, 'PL1', format='(A, $)'
		pl_count = 0
		pl_index = where(list1.I_pl ne 0.0, pl_count) 
		if (pl_count gt 0) then begin
			counts_exp1[pl_index] = list1[pl_index].counts_pl	
			r_exp1[pl_index] = list1[pl_index].re_pl
			I_exp1[pl_index] = list1[pl_index].I_pl
			IErr_exp1[pl_index] = list1[pl_index].IErr_pl			
		endif
	endif
	if (tag_exists(list1[0], "I_SERSIC")) then begin
		;; print, 'SERSIC1', format='(A, $)'
		aux_count = 0
		aux_index = where(list1.I_sersic ne 0.0, aux_count) 
		if (aux_count gt 0) then begin
			counts_sersic1[pl_index] = list1[aux_index].counts_sersic
			r_sersic1[aux_index] = list1[aux_index].re_sersic
			I_sersic1[aux_index] = list1[aux_index].I_sersic
			IErr_sersic1[aux_index] = list1[aux_index].IErr_sersic		
		endif
	endif



	;; determining object type
	counts_star1 = 0
	starndex = where(lis1[*].counts_star ne 0.0, counts_star1)
	counts_galaxy1 = 0
	galaxyndex = where(list1[*].counts_star eq 0.0 and list1[*].counts_dev2 eq 0.0, counts_galaxy1)
	counts_lrg1 = 0
	lrgndex = where(list1[*].counts_dev2 ne 0.0, counts_lrg1)
	if (counts_star1 ne 0) then type1[starndex] = !star_type
	if (counts_galaxy1 ne 0) then type1[galaxyndex] = !galaxy_type
	if (counts_lrg1 ne 0) then type1[lrgndex] = !lrg_type

endif else begin

	frac_dev1 = list1[*].fracPSF[band]
	counts_dev1 = list1[*].counts_dev[band]
	counts_exp1 = list1[*].counts_exp[band]
	r_dev1 = list1[*].r_dev[band]
	r_exp1 = list1[*].r_exp[band]
	type1 = list1[*].objc_type
	I_dev1 = replicate(!value_is_bad, n_elements(list1))
	I_exp1 = replicate(!value_is_bad, n_elements(list1))
	IErr_dev1 = replicate(!value_is_bad, n_elements(list1))
	IErr_exp1 = replicate(!value_is_bad, n_elements(list1))
	counts_sersic1 = replicate(0.0, n_elements(list1))
	r_sersic1 = replicate(0.0, n_elements(list1))
	n_sersic1 = replicate(!value_is_bad, n_elements(list1))
	I_sersic1 = replicate(!value_is_bad, n_elements(list1))
	IErr_sersic1 = replicate(!value_is_bad, n_elements(list1))

	if (tag_exist(list1[0], "COUNTS_PL")) then begin
		;; print, 'PL1', format='(A, $)'
		pl_count = 0
		pl_index = where(list1.COUNTS_pl ne 0.0, pl_count) 
		if (pl_count gt 0) then begin
			counts_exp1 = list1[*].counts_pl[band]
			r_exp1 = list1[*].r_pl[band]
		endif
	endif
	if (tag_exist(list1[0], "COUNTS_SERSIC")) then begin
		;; print, 'SERSIC1', format='(A, $)'
		aux_count = 0
		aux_index = where(list1.COUNTS_sersic ne 0.0, pl_count) 
		if (aux_count gt 0) then begin
			counts_sersic1 = list1[*].counts_sersic[band]
			r_sersic1 = list1[*].r_sersic[band]
			n_sersic1 = list1[*].n_sersic[band]
		endif
	endif


	;; determining object type
	counts_lrg1 = 0
	counts_galaxy1 = 0
	counts_unknown1 = 0
	unknowndex = where(type1 ne !galaxy_type and type1 ne !star_type, counts_unknown1)
	if (n_elements(flagLRG1) ne 0) then begin
		lrgndex = where(flagLRG1 ne 0, counts_lrg1)
		if (counts_lrg1 gt 0) then type1[lrgndex] = !lrg_type
	endif else begin	
		print, 'G1', format='(A, $)'
		galaxyndex = where(type1 eq !galaxy_type, counts_galaxy1)		
		if (counts_galaxy1 ne 0) then type1[galaxyndex] = !lrg_galaxy_type
	endelse
	if (counts_unknown1 ne 0) then type1[unknowndex] = !unknown_type
endelse

if (do_cd2) then begin
	counts_dev2 = list2[*].counts_dev
	counts_devvar = 1.0D10
	counts_exp2 = list2[*].counts_deV2
	counts_expvar = 1.0D10
	r_dev2 = list2[*].re_dev
	r_devvar = 1.0D10
	r_exp2 = list2[*].re_deV2
	r_expvar = 1.0D10
	I_dev2 = list2[*].I_deV
	I_exp2 = list2[*].I_deV2
	IErr_dev2 = list2[*].IErr_deV
	IErr_exp2 = list2[*].IErr_deV2
	counts_sersic2 = replicate(0.0, n_elements(list2))
	r_sersic2 = replicate(0.0, n_elements(list2))
	n_sersic2 = replicate(!value_is_bad, n_elements(list2))
	I_sersic2 = replicate(!value_is_bad, n_elements(list2))
	IErr_sersic2 = replicate(!value_is_bad, n_elements(list2))
	if (tag_exist(list2[0], "I_PL")) then begin
		;; print, 'PL2', format='(A, $)'
		pl_count = 0
		pl_index = where(list2.I_pl ne 0.0, pl_count) 
		if (pl_count gt 0) then begin
			counts_exp2[pl_index] = list2[pl_index].counts_pl	
			r_exp2[pl_index] = list2[pl_index].re_pl
			I_exp2[pl_index] = list2[pl_index].I_pl
			IErr_exp2[pl_index] = list2[pl_index].IErr_pl			
		endif
	endif
	if (tag_exist(list2[0], "I_SERSIC")) then begin
		;; print, 'SERSIC2', format='(A, $)'
		aux_count = 0
		aux_index = where(list2.I_sersic ne 0.0, aux_count) 
		if (aux_count gt 0) then begin
			;;print, "sersic count = ", aux_count
			counts_sersic2[aux_index] = list2[aux_index].counts_sersic
			r_sersic2[aux_index] = list2[aux_index].re_sersic
			I_sersic2[aux_index] = list2[aux_index].I_sersic
			IErr_sersic2[aux_index] = list2[aux_index].IErr_sersic		
		endif
	endif

	if (tag_exist(list2[0], "I_CORESERSIC")) then begin
		aux_count = 0
		aux_index = where(list2.I_coresersic ne 0.0, aux_count) 
		if (aux_count gt 0) then begin
			counts_dev2 = list2[*].counts_coresersic
			counts_devvar = 1.0D10
			counts_exp2 = list2[*].counts_coresersic
			counts_expvar = 1.0D10
			r_dev2 = list2[*].rb_coresersic
			r_devvar = 1.0D10
			r_exp2 = list2[*].re_coresersic
			r_expvar = 1.0D10
			I_dev2 = list2[*].I_coresersic
			I_exp2 = list2[*].I_coresersic
			IErr_dev2 = list2[*].IErr_coresersic
			IErr_exp2 = list2[*].IErr_coresersic
		endif
	endif
	;; determining object type
	counts_star2 = 0
	starndex = where(list2[*].counts_star ne 0.0, counts_star2)
	counts_galaxy2 = 0
	if (tag_exist(list2[0], "I_sersic")) then begin
		if (tag_exist(list2[0], "I_coresersic")) then begin
		galaxyndex = where(list2[*].counts_star eq 0.0 and list2[*].counts_dev2 eq 0.0 and list2[*].counts_exp2 eq 0.0 and $
		list2[*].counts_coresersic eq 0.0 and list2[*].counts_sersic eq 0.0, counts_galaxy2)
		endif else begin
		galaxyndex = where(list2[*].counts_star eq 0.0 and list2[*].counts_dev2 eq 0.0 and list2[*].counts_exp2 eq 0.0 and $
		list2[*].counts_pl eq 0.0 and list2[*].counts_sersic eq 0.0, counts_galaxy2)
		endelse

		if (tag_exist(list2[0], "I_sersic1") and counts_galaxy2 eq 0) then begin
			galaxyndex = where(list2[*].counts_star eq 0.0 and list2[*].counts_dev2 eq 0.0 and list2[*].counts_exp2 eq 0.0 and $
			list2[*].counts_sersic eq 0.0 and  list2[*].counts_sersic1 eq 0.0, counts_galaxy2)
			if (tag_exist(list2[0], "I_sersic2") and counts_galaxy2 eq 0) then begin
				galaxyndex = where(list2[*].counts_star eq 0.0 and list2[*].counts_dev2 eq 0.0 and list2[*].counts_exp2 eq 0.0 and $
				list2[*].counts_sersic eq 0.0 and  list2[*].counts_sersic1 eq 0.0 and list2[*].counts_sersic2 eq 0.0, counts_galaxy2)
			endif
		endif
	endif
	counts_lrg2 = 0
	if (tag_exist(list2[0], "I_SERSIC")) then begin
		if (tag_exist(list2[0], "I_CORESERSIC")) then begin
		lrgndex = where(list2[*].counts_dev2 ne 0.0 or list2[*].counts_exp2 ne 0.0 or list2[*].counts_sersic ne 0.0 or list2[*].counts_coresersic ne 0.0, counts_lrg2)
		endif else begin
		lrgndex = where(list2[*].counts_dev2 ne 0.0 or list2[*].counts_exp2 ne 0.0 or list2[*].counts_pl ne 0.0 or list2[*].counts_sersic ne 0.0, counts_lrg2)
		endelse
		if (tag_exist(list2[0], "I_SERSIC1")) then begin
			lrgndex = where(list2[*].counts_dev2 ne 0.0 or list2[*].counts_sersic ne 0.0 or list2[*].counts_sersic1 ne 0.0, counts_lrg2)
			if (tag_exist(list2[0], "I_SERSIC2")) then begin
				lrgndex = where(list2[*].counts_dev2 ne 0.0 or list2[*].counts_sersic ne 0.0 or list2[*].counts_sersic1 ne 0.0 or list2[*].counts_sersic2 ne 0.0, counts_lrg2)
			endif
		endif
	endif
	if (counts_star2 ne 0) then type2[starndex] = !star_type
	if (counts_galaxy2 ne 0) then type2[galaxyndex] = !galaxy_type
	if (counts_lrg2 ne 0) then type2[lrgndex] = !lrg_type

endif else begin
	counts_dev2 = list2[*].counts_dev[band]
	counts_devvar = (list2[*].counts_devErr[band]) ^ 2
	counts_exp2 = list2[*].counts_exp[band]
	counts_expvar = (list2[*].counts_expErr[band]) ^ 2
	r_dev2 = list2[*].r_dev[band]
	r_devvar = (list2[*].r_deVErr[band]) ^ 2
	r_exp2 = list2[*].r_exp[band]
	r_expvar = (list2[*].r_expErr[band]) ^ 2

	;; determining object type
	counts_lrg2 = 0
	counts_galaxy2 = 0
	counts_unknown2 = 0
	unknowndex = where(type2 ne !galaxy_type and type2 ne !star_type, counts_unknown2)
	galaxyndex = where(type2 eq !galaxy_type, counts_galaxy2)
	if (n_elements(flagLRG2) ne 0) then begin
		lrgndex = where(flagLRG2 ne 0, counts_lrg2)
		if (counts_lrg2 gt 0) then type2[lrgndex] = !lrg_type
	endif else begin
		print, 'G2', format='(A, $)'
		if (counts_galaxy2 ne 0) then type2[galaxyndex] = !lrg_galaxy_type
	endelse
	if (counts_unknown2 ne 0) then type2[unknowndex] = !unknown_type



endelse

delta = dblarr(n1, n2)
delta[*, *] = 0.0D0 
if (keyword_set(distance_match)) then begin

pushtype = 0
for i1 = 0L, n1 - 1L, 1L do begin
	
	;; distance[*] = (x1[i1] - x2[*]) ^ 2 + (y1[i1] - y2[*]) ^ 2
	distance[i1, *] = (x1[i1] - x2[*]) ^ 2 / xvar + (y1[i1] - y2[*]) ^ 2 / yvar 
	delta[i1, *] = SQRT((x1[i1] - x2[*]) ^ 2 + (y1[i1] - y2[*]) ^ 2)
	;; faraway = where(delta gt dthreshold * r_exp1[i1] and delta gt dthreshold * r_dev1[i1] and delta gt dthreshold * r_dev2 and delta gt dthreshold * r_exp2 and delta gt 5.0, fcount)
	fcount = 0
	faraway = where(delta[i1, *] gt !rej_d, fcount)
	if (fcount gt 0) then distance[i1, faraway] = chisq_threshold * 10.0
	if (keyword_set(pushtype)) then begin
		if (type1[i1] eq !galaxy_type) then begin
			fcount = 0
			faraway = where(type2 ne !galaxy_type and type2 ne !lrg_galaxy_type, fcount)
			if (fcount gt 0) then distance[i1, faraway] = chisq_threshold * 10.0
		endif
		if (type1[i1] eq !lrg_type) then begin
			fcount = 0
			faraway = where(type2 ne !lrg_type and type2 ne !lrg_galaxy_type, fcount)
			if (fcount gt 0) then distance[i1, faraway] = chisq_threshold * 10.0
		endif
		if (type1[i1] eq !lrg_galaxy_type) then begin
			count = 0
                        faraway = where(type2 ne !galaxy_type and type2 ne !lrg_type and type2 ne !lrg_galaxy_type, fcount)
                        if (fcount gt 0) then distance[i1, faraway] = chisq_threshold * 10.0
		endif
		if (type1[i1] eq !star_type) then begin
			fcount = 0
			faraway = where(type2 ne !star_type, fcount)
			if (fcount eq 0) then distance[i1, faraway] = chisq_threshold * 10.0
		endif	
	endif
	
endfor

endif else begin

for i1 = 0L, n1 - 1L, 1L do begin
	
	;; distance[*] = (x1[i1] - x2[*]) ^ 2 + (y1[i1] - y2[*]) ^ 2
	distance[i1, *] = (x1[i1] - x2[*]) ^ 2 / xvar + (y1[i1] - y2[*]) ^ 2 / yvar + $
	frac_dev1[i1] * (counts_dev1[i1] - counts_dev2[*]) ^ 2 / counts_devvar + (1.0 - frac_dev1[i1]) * (counts_exp1[i1] - counts_exp2[*]) ^ 2 / counts_expvar + $
	frac_dev1[i1] * (r_dev1[i1] - r_dev2[*]) ^ 2 / r_devvar + (1.0 - frac_dev1[i1]) * (r_exp1[i1] - r_exp2[*]) ^ 2 / r_expvar

	delta[i1, *] = SQRT((x1[i1] - x2[*]) ^ 2 + (y1[i1] - y2[*]) ^ 2)
	;; faraway = where(delta gt dthreshold * r_exp1[i1] and delta gt dthreshold * r_dev1[i1] and delta gt dthreshold * r_dev2 and delta gt dthreshold * r_exp2 and delta gt 5.0, fcount)
	fcount = 0
	faraway = where(delta[i1, *] gt !rej_d, fcount)
	if (fcount gt 0) then distance[i1, faraway] = chisq_threshold * 10.0

endfor

endelse

match_index_2D = replicate(0L, [n1, n2])
used_match_index = replicate(0L, n2)
used_index1 = replicate(0L, n1)

for i1 = 0L, n1-1L, 1L do begin
	used_index1[i1] = total(match_index_2D[i1, *])
endfor
available_index2 = where(used_match_index ne 1, c2)
available_index1 = where(used_index1 eq 0, c1)


loop = 0
something_happened = 1
while (c1 gt 0 and c2 gt 0 and something_happened gt 0) do begin

something_happened = 0
loop += 1
;;print, loop, c1, c2

if (c1 gt 0 and c2 gt 0) then begin
for j1 = 0L, c1 - 1L, 1L do begin
	i1 = available_index1[j1]
	dmin = min(distance[i1, available_index2])
	if (dmin lt chisq_threshold) then begin
		j2 = where(distance[i1, available_index2] eq dmin)
		i2 = available_index2[j2]
		match_index_2D[i1, i2] = 1
		used_match_index[i2] += 1
		something_happened += 1
	endif
endfor
endif

rcount = 0
rndex = where(used_match_index gt 1, rcount)
if (rcount gt 0) then begin
for j2 = 0L, rcount - 1L, 1L do begin
	i2 = rndex[j2]
	index1 = where(match_index_2D[*, i2] eq 1)
	dmin = min(distance[index1, i2])
	if (dmin lt chisq_threshold) then begin
		j1 = where(distance[index1, i2] eq dmin)	
		i1 = index1[j1]
		match_index_2D[index1, i2] = 0L
		match_index_2D[i1, i2] = 1L
		used_match_index[i2] = 1
		something_happened += 1
	endif else begin
		;; j1 = where(distance[index1, i2] eq dmin)	
		;; i1 = index1[j1]
		match_index_2D[index1, i2] = 0L
		;; match_index_2D[i1, i2] = 1L
		used_match_index[i2] = 0
		;; something_happened += 1
	endelse
endfor
endif

for i1 = 0L, n1-1L, 1L do begin
	used_index1[i1] = total(match_index_2D[i1, *])
endfor
available_index2 = where(used_match_index ne 1, c2)
available_index1 = where(used_index1 eq 0, c1)

endwhile

max_delta = -1.0
match_index = replicate(-1L, n2)
for i2 = 0L, n2-1L, 1 do begin
	i1 = where(match_index_2D[*, i2] eq 1L, mcount)
	if (mcount eq 1) then begin
		if (delta[i1, i2] lt !rej_d) then match_index[i2] = i1
		max_delta = max([max_delta, delta[i1, i2]])
	endif else begin
		if (mcount gt 1) then print, name, ": ERROR - repetative match found"
	endelse
endfor
;; print, n1, n2
;; print, match_index

;;print, loop, match_index
endelse


return
end

function createboxplotdata, data

Y = PERCENTILES(DATA, value = [0.0, 0.25, 0.50, 0.75, 1.0])
return, y
end 

function get_random_index, n, p, seed = seed

m = min([round(p * n) + 1, n])
randomNumbers = RANDOMU(seed, n)            
sortedRandomNumbers = SORT(randomNumbers)     
rndex = sortedRandomNumbers[0:m-1] 

return, rndex
end

function photo_calibrate, counts, A, B, C, D
ff0 = counts / A
x = ff0/ B
mag = -C * (asinh(x) + D)
return, mag
end 


function construct_fpObjc_dir, root = root, run = run, rerun = rerun, camcol = camcol, sas = sas, sim = sim, mle = mle, status = status
name = "construct_fpObjc_dir"
status = -1
if (n_elements(root) eq 0) then return, -1
if (n_elements(run) eq 0) then return, -1
if (n_elements(rerun) eq 0) then return, -1
if (n_elements(camcol) eq 0) then return, -1

rundir=strtrim(string(run), 2)
runstr=string(run, format = "(I6.6)")
camcolstr=strtrim(string(camcol), 2)

if (keyword_set(sas)) then begin
dir=root+"/"+rerun+"/"+rundir+"/objcs/"+camcolstr
endif else begin
	if (keyword_set(sim)) then begin	
		dir=root+"/"+rerun+"/"+rundir+"/"+camcolstr+"/siminput"
		bandstr = "r"
		prefix = "gpObjc-"
	endif else if (keyword_set(mle)) then begin
		dir=root+"/"+rerun+"/"+rundir+"/"+camcolstr+"/objc"
		bandstr = "r"
		prefix = "gpObjc-"
	endif else begin
		dir=root+"/"+rerun+"/"+rundir+"/"+camcolstr+"/photo"
	endelse
endelse

status = 0
return, dir
end



function construct_objc_filename, root = root, run = run, rerun = rerun, field = field, camcol = camcol, band = band, status = status, sas = sas, sim = sim, mle = mle
name = "construct_objc_filename"
status = -1
if (n_elements(root) eq 0) then begin
	print, name, ": ERROR - expected a non-null root"
	return, -1
endif
if (n_elements(run) eq 0) then begin
	print, name, ": ERROR - expected a non-null run"
	return, -1
endif
if (n_elements(rerun) eq 0) then begin
	print, name, ": ERROR - expected a non-null rerun"
	return, -1
endif
if (n_elements(field) eq 0) then begin
	print, name, ": ERROR - expected a non-null field"
	return, -1
endif
if (n_elements(camcol) eq 0) then begin 
	print, name, ": ERROR - expected a non-null camcol"
	return, -1
endif
if (n_elements(band) eq 0) then band = "r"

rundir=strtrim(string(run), 2)
runstr=string(run, format = "(I6.6)")
camcolstr=strtrim(string(camcol), 2)
fieldstr=string(field, format = "(I4.4)")

prefix = "fpObjc-"
bandstr = ""
if (keyword_set(sas)) then begin
dir=root+"/"+rerun+"/"+rundir+"/objcs/"+camcolstr
endif else begin 
	if (keyword_set(sim)) then begin	
		dir=root+"/"+rerun+"/"+rundir+"/"+camcolstr+"/siminput"
		bandstr = band
		prefix = "gpObjc-"
	endif else if (keyword_set(mle)) then begin
		dir=root+"/"+rerun+"/"+rundir+"/"+camcolstr+"/objc"
		bandstr = band
		prefix = "gpObjc-"
	endif else begin
		dir=root+"/"+rerun+"/"+rundir+"/"+camcolstr+"/photo"
	endelse
endelse

filename=prefix+runstr+"-"+bandstr+camcolstr+"-"+fieldstr+".fit"
file = dir+"/"+filename

status = 0
return, file
end

function construct_objc_files_array, root = root, run = run, rerun = rerun, field = field, camcol = camcol, band = band, status = status, sas = sas, sim = sim, mle = mle
name = "construct_objc_files_array"

if (n_elements(band) eq 0) then band = ["r"]
for iroot = 0, n_elements(root) - 1L, 1 do begin
for iband = 0, n_elements(band) - 1L, 1 do begin
	status = 0
	filename = construct_objc_filename(root = root[iroot], run = run, rerun = rerun, field = field, camcol = camcol, band = band[iband], status = status, $
	sas = keyword_set(sas), sim = keyword_set(sim), mle = keyword_set(mle))
	if (status ne 0) then begin
		print, name, ": ERROR - could not make one of the filenames (iroot = ", iroot, ", iband = ", iband, "), root = ", root[iroot], ", band = ", band[iband]
		return, []
	endif
	if (n_elements(file_array) gt 0) then begin
		file_array = [file_array, filename]
	endif else begin
		file_array = [filename]
	endelse
endfor
endfor

status = 0
return, file_array

end
function construct_sky_filename, root = root, run = run, rerun = rerun, field = field, camcol = camcol, band = band, status = status, sas = sas, sim = sim, mle = mle
name = "construct_sky_filename"
status = -1
if (n_elements(root) eq 0) then return, -1
if (n_elements(run) eq 0) then return, -1
if (n_elements(rerun) eq 0) then return, -1
if (n_elements(field) eq 0) then return, -1
if (n_elements(camcol) eq 0) then return, -1
if (n_elements(band) eq 0) then band = "r"

rundir=strtrim(string(run), 2)
runstr=string(run, format = "(I6.6)")
camcolstr=strtrim(string(camcol), 2)
fieldstr=string(field, format = "(I4.4)")

prefix = "fpBIN-"
bandstr = "r"
if (keyword_set(sas)) then begin
dir=root+"/"+rerun+"/"+rundir+"/objcs/"+camcolstr
endif else begin 
	if (keyword_set(sim)) then begin	
		dir=root+"/"+rerun+"/"+rundir+"/"+camcolstr+"/siminput"
		bandstr = band
		prefix = "gpBIN-"
	endif else if (keyword_set(mle)) then begin
		dir=root+"/"+rerun+"/"+rundir+"/"+camcolstr+"/objc"
		bandstr = band
		prefix = "gpBIN-"
	endif else begin
		dir=root+"/"+rerun+"/"+rundir+"/"+camcolstr+"/photo"
	endelse
endelse

filename=prefix+runstr+"-"+bandstr+camcolstr+"-"+fieldstr+".fit"
file = dir+"/"+filename

status = 0
return, file
end

function construct_fpC_filename, root = root, run = run, rerun = rerun, field = field, camcol = camcol, band = band, status = status, sas = sas, sim = sim, mle = mle
name = "construct_fpC_filename"
status = -1
if (n_elements(root) eq 0) then return, -1
if (n_elements(run) eq 0) then return, -1
if (n_elements(rerun) eq 0) then return, -1
if (n_elements(field) eq 0) then return, -1
if (n_elements(camcol) eq 0) then return, -1
if (n_elements(band) eq 0) then band = "r"

rundir=strtrim(string(run), 2)
runstr=string(run, format = "(I6.6)")
camcolstr=strtrim(string(camcol), 2)
fieldstr=string(field, format = "(I4.4)")

prefix = "fpC-"
bandstr = "r"
if (keyword_set(sas)) then begin
dir=root+"/"+rerun+"/"+rundir+"/objcs/"+camcolstr
endif else begin 
	if (keyword_set(sim)) then begin	
		dir=root+"/"+rerun+"/"+rundir+"/"+camcolstr+"/siminput"
		bandstr = band
		prefix = "gpBIN-"
	endif else if (keyword_set(mle)) then begin
		dir=root+"/"+rerun+"/"+rundir+"/"+camcolstr+"/objc"
		bandstr = band
		prefix = "gpBIN-"
	endif else begin
		dir=root+"/"+rerun+"/"+rundir+"/"+camcolstr+"/photo"
	endelse
endelse

filename=prefix+runstr+"-"+bandstr+camcolstr+"-"+fieldstr+".fit"
file = dir+"/"+filename

status = 0
return, file
end





function construct_LM_filename, root = root, run = run, rerun = rerun, field = field, camcol = camcol, band = band, status = status, sas = sas, sim = sim, mle = mle, lp = lp
name = "construct_LM_filename"
status = -1
if (n_elements(root) eq 0) then return, -1
if (n_elements(run) eq 0) then return, -1
if (n_elements(rerun) eq 0) then return, -1
if (n_elements(field) eq 0) then return, -1
if (n_elements(camcol) eq 0) then return, -1
if (n_elements(band) eq 0) then band = "r"

rundir=strtrim(string(run), 2)
runstr=string(run, format = "(I6.6)")
camcolstr=strtrim(string(camcol), 2)
fieldstr=string(field, format = "(I4.4)")

prefix = "lM-"
if (keyword_set(lp)) then prefix="lP-"

bandstr = ""
if (keyword_set(sas)) then begin
dir=root+"/"+rerun+"/"+rundir+"/objcs/"+camcolstr
endif else begin 
	if (keyword_set(sim)) then begin	
		dir=root+"/"+rerun+"/"+rundir+"/"+camcolstr+"/siminput"
		bandstr = band
	endif else if (keyword_set(mle)) then begin
		dir=root+"/"+rerun+"/"+rundir+"/"+camcolstr+"/objc"
		bandstr = band
	endif else begin
		dir=root+"/"+rerun+"/"+rundir+"/"+camcolstr+"/photo"
	endelse
endelse

filename=prefix+runstr+"-"+bandstr+camcolstr+"-"+fieldstr+".fit"
file = dir+"/"+filename

status = 0
return, file
end





function construct_ps_filename, root = root, run = run, rerun = rerun, field = field, camcol = camcol, status = status, sas = sas
name = "construct_ps_filename"
status = -1
if (n_elements(root) eq 0) then return, -1
if (n_elements(run) eq 0) then return, -1
if (n_elements(rerun) eq 0) then return, -1
if (n_elements(field) eq 0) then return, -1
if (n_elements(camcol) eq 0) then return, -1

rundir=strtrim(string(run), 2)
runstr=string(run, format = "(I6.6)")
camcolstr=strtrim(string(camcol), 2)
fieldstr=string(field, format = "(I4.4)")

if (keyword_set(sas)) then begin
dir=root+"/"+rerun+"/"+rundir+"/objcs/"+camcolstr
endif else begin
dir=root+"/"+rerun+"/"+rundir+"/"+camcolstr+"/photo"
endelse

filename="psField-"+runstr+"-"+camcolstr+"-"+fieldstr+".fit"
file = dir+"/"+filename

status = 0
return, file
end

function construct_gc_filename, root = root, run = run, rerun = rerun, field = field, camcol = camcol, status = status, sas = sas, data = data
name = "construct_ps_filename"
status = -1
if (n_elements(root) eq 0) then return, -1
if (n_elements(run) eq 0) then return, -1
if (n_elements(rerun) eq 0) then return, -1
if (n_elements(field) eq 0) then return, -1
if (n_elements(camcol) eq 0) then return, -1

rundir=strtrim(string(run), 2)
runstr=string(run, format = "(I6.6)")
camcolstr=strtrim(string(camcol), 2)
fieldstr=string(field, format = "(I4.4)")

if (keyword_set(sas)) then begin
dir=root+"/"+rerun+"/"+rundir+"/objcs/"+camcolstr
endif else begin
	if (keyword_set(data)) then begin
		dir=root+"/"+rerun+"/"+rundir+"/"+camcolstr+"/objc"
	endif else begin
		dir=root+"/"+rerun+"/"+rundir+"/"+camcolstr+"/photo"
	endelse
endelse
filename="gC-"+runstr+"-"+camcolstr+"-"+fieldstr+".fit"
file = dir+"/"+filename

status = 0
return, file
end



function create_destination_lP_filename, root = root, run = run, rerun = rerun, field = field, camcol = camcol, $
	galaxy = galaxy, LRG = LRG, flagLRG = flagLRG, star = star, mle = mle, suffix = suffix, status = status, $
	tracing = tracing
name = "create_destination_filename"
status = -1
if (n_elements(root) eq 0) then return, -1
;;if (n_elements(run) eq 0) then return, -1
if (n_elements(rerun) eq 0) then return, -1
;; if (n_elements(field) eq 0) then return, -1
;;if (n_elements(camcol) eq 0) then return, -1
if (n_elements(suffix) eq 0) then suffix = ""

if (n_elements(run) ne 0) then begin
rundir=strtrim(string(run), 2)
runstr=string(run, format = "(I6.6)")
endif else begin
rundir=""
runstr=""
endelse

if (n_elements(camcol) ne 0) then begin
camcoldir=strtrim(string(camcol), 2)
camcolstr="-"+strtrim(string(camcol), 2)
endif else begin
camcoldir=""
camcolstr=""
endelse


if (n_elements(field) ne 0) then fieldstr="-"+string(field, format = "(I4.4)") else fieldstr = ""
typestr = ""
if (keyword_set(galaxy)) then typestr = "gal-"
if (keyword_set(LRG)) then begin
	typestr = "lrg-"
	if (n_elements(flagLRG) eq 1) then begin
		if (flagLRG eq 1) then typestr = typestr+"cutI-"
		if (flagLRG eq 2) then typestr = typestr+"cutII-"
	endif
endif
if (keyword_set(star)) then typestr = "star-"
source1 = "sim-"
source2 = "photo-"
if (keyword_set(mle)) then source2 = "mle-"
prefix="lP-"
if (keyword_set(tracing)) then prefix = "lT-";

dir=root+"/"+rerun+"/"+rundir+"/"+camcoldir+"/plots"
filename=prefix + source1 + source2 +typestr+suffix+runstr+camcolstr+fieldstr+".eps"
file = dir+"/"+filename

if (file_test(dir, /directory) eq 0) then file_mkdir, dir
status = 0
return, file
end


function create_destination_filename, root = root, run = run, rerun = rerun, field = field, camcol = camcol, $
	galaxy = galaxy, LRG = LRG, flagLRG = flagLRG, star = star, mle = mle, suffix = suffix, status = status
name = "create_destination_filename"
status = -1
if (n_elements(root) eq 0) then return, -1
;;if (n_elements(run) eq 0) then return, -1
if (n_elements(rerun) eq 0) then return, -1
;; if (n_elements(field) eq 0) then return, -1
;;if (n_elements(camcol) eq 0) then return, -1
if (n_elements(suffix) eq 0) then suffix = ""

if (n_elements(run) ne 0) then begin
rundir=strtrim(string(run), 2)
runstr=string(run, format = "(I6.6)")
endif else begin
rundir=""
runstr=""
endelse

if (n_elements(camcol) ne 0) then begin
camcoldir=strtrim(string(camcol), 2)
camcolstr="-"+strtrim(string(camcol), 2)
endif else begin
camcoldir=""
camcolstr=""
endelse


if (n_elements(field) ne 0) then fieldstr="-"+string(field, format = "(I4.4)") else fieldstr = ""
typestr = ""
if (keyword_set(galaxy)) then typestr = "gal-"
if (keyword_set(LRG)) then begin
	typestr = "lrg-"
	if (n_elements(flagLRG) eq 1) then begin
		if (flagLRG eq 1) then typestr = typestr+"cutI-"
		if (flagLRG eq 2) then typestr = typestr+"cutII-"
	endif
endif
if (keyword_set(star)) then typestr = "star-"
source1 = "sim-"
source2 = "photo-"
if (keyword_set(mle)) then source2 = "mle-"

dir=root+"/"+rerun+"/"+rundir+"/"+camcoldir+"/plots"
filename="match-" + source1 + source2 +typestr+suffix+runstr+camcolstr+".eps"
file = dir+"/"+filename

if (file_test(dir, /directory) eq 0) then file_mkdir, dir
status = 0
return, file
end

function create_matchlog_filename, root = root, run = run, rerun = rerun, field = field, camcol = camcol, $
	galaxy = galaxy, LRG = LRG, flagLRG = flagLRG, star = star, mle = mle, suffix = suffix, status = status
name = "create_destination_filename"
status = -1
if (n_elements(root) eq 0) then return, -1
if (n_elements(run) eq 0) then return, -2
if (n_elements(rerun) eq 0) then return, -3
if (n_elements(field) eq 0) then return, -4
if (n_elements(camcol) eq 0) then return, -5
if (n_elements(suffix) eq 0) then suffix = ""

rundir=strtrim(string(run), 2)
runstr=string(run, format = "(I6.6)")
camcolstr=strtrim(string(camcol), 2)
fieldstr=string(field, format = "(I4.4)")
typestr = ""
if (keyword_set(galaxy)) then typestr = "gal-"
if (keyword_set(LRG)) then begin
	typestr = "lrg-"
	if (n_elements(flagLRG) eq 1) then begin
		if (flagLRG eq 1) then typestr = typestr+"cutI-"
		if (flagLRG eq 2) then typestr = typestr+"cutII-"
	endif
endif

if (keyword_set(star)) then typestr = "star-"
source1 = "sim-"
source2 = "photo-"
if (keyword_set(mle)) then source2 = "mle-"

dir=root+"/"+rerun+"/"+rundir+"/"+camcolstr+"/plots"
filename="match-" + source1 + source2 + typestr+suffix+runstr+"-"+camcolstr+"-"+fieldstr+".csv"
file = dir+"/"+filename

if (file_test(dir, /directory) eq 0) then file_mkdir, dir
status = 0
return, file
end

function create_goodness_report_for_objc, objc_info, sdss = sdss, sample_report = sample_report, title = title, status = status

name = "create_goodness_report_for_objc"
if (n_elements(sample_report) eq 0) then begin
	print, name, ": ERROR - need a sample report to compile a report"
	status = -1
	bad_report = {status: "bad input"}
	return, bad_report
endif

if (n_elements(title) eq 0) then title = "generic model"

if (not tag_exist(objc_info, "chisq_mask")) then begin
	report = sample_report
	report.n_circle = 0
	report.n_ring = 0
	status = 0
	return, report
endif

report = sample_report

newline = string(10B)
format = "(F10.1)"
mag_mle = objc_info.mag_model
comment = [title, "mag_tot = " + strtrim(string(mag_mle, format = format), 2)]
active_model_count = 0

threshold_rad = 100.0 * sdss.petrotheta[2] * !PIXELSIZE
local_min = 0

if (objc_info.I_sersic1 ne 0.0) then begin
	mag_sersic1 = objc_info.mag_sersic1 
	r_sersic1 = objc_info.re_sersic1 * !PIXELSIZE
	n_sersic1 = objc_info.n_sersic1
	comment = [comment, $
	"mag1 = " + strtrim(string(mag_sersic1, format = format), 2) + $
	", r1 = " + strtrim(string(r_sersic1, format = format), 2) + ", n1 = " + strtrim(string(n_sersic1, format =  format), 2)]
	active_model_count++
	if (r_sersic1 gt threshold_rad) then local_min++
endif
if (objc_info.I_sersic2 ne 0.0) then begin
	mag_sersic2 = objc_info.mag_sersic2
	r_sersic2 = objc_info.re_sersic2 * !PIXELSIZE
	n_sersic2 = objc_info.n_sersic2
	comment = [comment, $ 
	"mag2 = " + strtrim(string(mag_sersic2, format =  format), 2) + $
	", r2 = " + strtrim(string(r_sersic2, format =  format), 2) + ", n2 = " + strtrim(string(n_sersic2, format =  format), 2)]
	active_model_count++
	if (r_sersic2 gt threshold_rad) then local_min++
endif
if (objc_info.I_sersic ne 0.0) then begin
	mag_sersic = objc_info.mag_sersic
	r_sersic = objc_info.re_sersic * !PIXELSIZE
	n_sersic = objc_info.n_sersic
	comment = [comment, $
	"mag = " + strtrim(string(mag_sersic, format =  format), 2) + $
	", r = " + strtrim(string(r_sersic, format =  format), 2) + ", n = " + strtrim(string(n_sersic, format =  format), 2)]
	active_model_count++
	if (r_sersic gt threshold_rad) then local_min++
endif
if (objc_info.I_deV ne 0.0) then begin
	mag_deV = objc_info.mag_deV
	r_deV = objc_info.re_deV * !PIXELSIZE
	comment = [comment, $
	"mag = " + strtrim(string(mag_deV, format =  format), 2) + $
	", r = " + strtrim(string(r_deV, format =  format), 2)]
	active_model_count++
	if (r_deV gt threshold_rad) then local_min++
endif
if (objc_info.I_deV1 ne 0.0) then begin
	mag_deV1 = objc_info.mag_deV1
	r_deV1 = objc_info.re_deV1 * !PIXELSIZE
	comment = [comment, $
	"mag1 = " + strtrim(string(mag_deV1, format =  format), 2) + $
	", r1 = " + strtrim(string(r_deV1, format =  format), 2)]
	active_model_count++
	if (r_deV1 gt threshold_rad) then local_min++
endif
if (objc_info.I_deV2 ne 0.0) then begin
	mag_deV2 = objc_info.mag_deV2
	r_deV2 = objc_info.re_deV2 * !PIXELSIZE
	comment = [comment, $
	"mag2 = " + strtrim(string(mag_deV2, format =  format), 2) +  $
	", r2 = " + strtrim(string(r_deV2, format =  format), 2)]
	active_model_count++
	if (r_deV2 gt threshold_rad) then local_min++
endif
if (objc_info.I_exp ne 0.0) then begin
	mag_exp = objc_info.mag_exp
	r_exp = objc_info.re_exp * !PIXELSIZE
	comment = [comment, $
	"mag = " + strtrim(string(mag_exp, format =  format), 2) + $
	", r = " + strtrim(string(r_exp, format =  format), 2)]
	active_model_count++
	if (r_exp gt threshold_rad) then local_min++
endif
if (objc_info.I_exp1 ne 0.0) then begin
	mag_exp1 = objc_info.mag_exp1
	r_exp1 = objc_info.re_exp1 * !PIXELSIZE
	comment = [comment, $
	"mag1 = " + strtrim(string(mag_exp1, format =  format), 2) + $
	", r1 = " + strtrim(string(r_exp1, format =  format), 2)]
	active_model_count++
	if (r_exp1 gt threshold_rad) then local_min++
endif
if (objc_info.I_exp2 ne 0.0) then begin
	mag_exp2 = objc_info.mag_exp2
	r_exp2 = objc_info.re_exp2 * !PIXELSIZE
	comment = [comment, $
	"mag2 = " + strtrim(string(mag_exp2, format =  format), 2) + $
	", r2 = " + strtrim(string(r_exp2, format =  format), 2)]
	active_model_count++
	if (r_exp2 gt threshold_rad) then local_min++
endif

if (active_model_count eq 1) then begin
	n_comments = n_elements(comment)
	comment = [title, comment[2L:n_comments - 1L]]
endif
comment = [comment, ""]
n_comment = n_elements(comment)
c_index = lindgen(n_comment)
report.rowc = sdss.objc_rowc
report.colc = sdss.objc_colc
report.sdss_petrorad = sdss.petrotheta[2] * !PIXELSIZE
report.sdss_mag = sdss.cmodelmag[2]
report.mle_mag = mag_mle
report.comment[c_index]=comment[c_index]
report.n_comment = n_comment
goodness_array = objc_info.chisq_mask
radius_array = !PIXELSIZE * objc_info.radius_mask
npix_array = objc_info.npix_mask

n_goodmask = 0
good_masks = where(npix_array gt 0, n_goodmask)

report.n_circle = 0
report.n_ring = 0
if ((n_goodmask gt 0) and (local_min eq 0))  then begin
	goodness_array = goodness_array[good_masks]
	radius_array = radius_array[good_masks]
	npix_array = npix_array[good_masks]

	n_unique_radius = n_elements(UNIQ(radius_array, SORT(radius_array)))
	if (n_unique_radius ne n_goodmask) then begin
		print, name, ": ERROR - repetative radius found - rad = ", radius_array
	endif
	
	n_goodmask = 0
	good_masks = where(finite(goodness_array) and (goodness_array ne 0.0), n_goodmask)
	report.n_circle = n_goodmask
	report.n_ring = n_goodmask

	if (n_goodmask gt 0) then begin
		goodness_array = goodness_array[good_masks]
		radius_array = radius_array[good_masks]
		npix_array = npix_array[good_masks]

		report.goodness_circle[lindgen(n_goodmask)] = goodness_array[lindgen(n_goodmask)]	
		report.goodness_circle_reduced[lindgen(n_goodmask)] = goodness_array[lindgen(n_goodmask)] / npix_array[lindgen(n_goodmask)]	
		
		report.radius_circle[lindgen(n_goodmask)] = radius_array[lindgen(n_goodmask)]	
		report.npix_circle[lindgen(n_goodmask)] = npix_array[lindgen(n_goodmask)]		
	
		goodness_ring = goodness_array
		npix_ring = npix_array
		radius_ring = radius_array


		goodness_ring[0] = goodness_array[0]
		npix_ring[0] = npix_array[0]
		radius_ring[0] = 0.5 * radius_array[0]
		n_goodring = n_goodmask
		if (n_goodmask gt 1) then begin
			upper_index = lindgen(n_goodmask - 1) + 1L
			lower_index = lindgen(n_goodmask - 1)
			goodness_ring[upper_index] = goodness_array[upper_index] -goodness_array[lower_index]
			npix_ring[upper_index] = npix_array[upper_index] - npix_array[lower_index]
			radius_ring[upper_index] = sqrt(radius_array[upper_index] * radius_array[lower_index])
		
			n_goodring = 0
			good_rings = where((npix_ring gt 0) and (goodness_ring gt 0.0), n_goodring)
		
			if (n_goodring gt 0) then begin
				goodness_ring = goodness_ring[good_rings]
				npix_ring = npix_ring[good_rings]
				radius_ring = radius_ring[good_rings]
			endif		
		endif

		if (n_goodring gt 0) then begin
			report.goodness_ring[lindgen(n_goodring)] = goodness_ring[lindgen(n_goodring)]	
			report.goodness_ring_reduced[lindgen(n_goodring)] = goodness_ring[lindgen(n_goodring)] / npix_ring[lindgen(n_goodring)]	
			report.radius_ring[lindgen(n_goodring)] = radius_ring[lindgen(n_goodring)]	
			report.npix_ring[lindgen(n_goodring)] = npix_ring[lindgen(n_goodring)]		
		endif

		report.n_ring = n_goodring
	endif

	report.goodness_type = "chi-squared-reduced"
endif

return, report
end	


function determine_radial_range_from_report, report, status = status
name = "determine_radial_range_from_report"
n_circle = report.n_circle
n_ring = report.n_ring

if (n_circle gt 0 and n_ring gt 0) then begin
	max_rad = max([report.radius_circle[lindgen(n_circle)], report.radius_ring[lindgen(n_ring)]])
endif else if (n_circle gt 0) then begin	
	max_rad = max([report.radius_circle[lindgen(n_circle)]])
endif else if (n_ring gt 0) then begin
	max_rad =  max([report.radius_ring[lindgen(n_ring)]]) 
endif else begin
	status = 0
	;; print, name, ": ERROR - found n_circle = 0, n_ring = 0 in report"
	max_rad = 0.0
endelse

if (max_rad le 0.0) then begin
	;; print, name, ": ERROR - found unacceptable maximum radius in report, max_rad = ", max_rad
	status = 0
	if (n_circle gt 0 and n_ring gt 0) then	status = -1
	rad_range = [0.0, 0.0]
endif else begin
	status = 0
	rad_range = [0.0, max_rad]
endelse

return, rad_range
end

function determine_rrange_report_array, reports, status = status
name = "determine_rrange_report_array"
rad_range = [0.0, 0.0]
if (n_elements(reports) eq 0) then begin
	print, name, ": ERROR - no report passed"
	status = -1
	return, rad_range
endif

status = 0
for ireport = 0, n_elements(reports) - 1L, 1L do begin
	xrange = determine_radial_range_from_report(reports[ireport], status = status)
	if (status ne 0) then begin
		print, name, ": ERROR - could not work with report at location ireport = ", ireport
		return, rad_range
	endif
	min_rad = min([rad_range, xrange])
	max_rad = max([rad_range, xrange])
	rad_range = [min_rad, max_rad]
endfor

return, rad_range
end

function determine_goodness_range_from_report, report, status = status, rflag = rflag, gflag = gflag, dolog = dolog, adjflag = adjflag
name = "determine_goodness_range_from_report"
n_circle = report.n_circle
n_ring = report.n_ring

if (gflag eq "reduced") then begin
	dolog = 0
endif else if (gflag eq "unreduced") then begin
	dolog = 1
endif else begin
	print, name, ": ERROR - unsupported gflag = ", gflag
endelse

if (n_circle gt 0 and n_ring gt 0) then begin
	if (gflag eq "reduced") then begin
		max_goodness = max([report.goodness_circle_reduced[lindgen(n_circle)], report.goodness_ring_reduced[lindgen(n_ring)]])
		min_goodness = min([report.goodness_circle_reduced[lindgen(n_circle)], report.goodness_ring_reduced[lindgen(n_ring)]])
	endif else if (rflag eq "circle" and gflag eq "unreduced") then begin
		max_goodness = max(report.goodness_circle[lindgen(n_circle)])
		min_goodness = min(report.goodness_circle[lindgen(n_circle)])
	endif else if (rflag eq "ring" and gflag eq "unreduced") then begin
		max_goodness = max(report.goodness_ring[lindgen(n_ring)])
		min_goodness = min(report.goodness_ring[lindgen(n_ring)])
	endif else begin
		print, name, ": ERROR - rflag, gflag values not supported", rflag, gflag
		status = -1
		max_goodness = 0.0
		min_goodness = 0.0
	endelse
endif else if (n_circle gt 0) then begin	
	if (gflag eq "reduced") then begin
		max_goodness = max(report.goodness_ring_reduced[lindgen(n_ring)])
		min_goodness = min(report.goodness_ring_reduced[lindgen(n_ring)])
	endif else if (rflag eq "circle" and gflag eq "unreduced") then begin
		print, name, ": ERROR - circle goodness sought while n_circle = 0"
		status = -1
		max_goodness = 0.0
		min_goodness = 0.0
	endif else if (rflag eq "ring" and gflag eq "unreduced") then begin
		max_goodness = max(report.goodness_ring[lindgen(n_ring)])
		min_goodness = min(report.goodness_ring[lindgen(n_ring)])
	endif else begin
		print, ": ERROR - rflag, gflag values not supported", rflag, gflag
		status = -1
		max_goodness = 0.0
		min_goodness = 0.0
	endelse
endif else if (n_ring gt 0) then begin
	if (gflag eq "reduced") then begin
		max_goodness = max(report.goodness_circle_reduced[lindgen(n_circle)])
		min_goodness = min(report.goodness_circle_reduced[lindgen(n_circle)])
	endif else if (rflag eq "circle" and gflag eq "unreduced") then begin
		max_goodness = max(report.goodness_circle[lindgen(n_circle)])
		min_goodness = min(report.goodness_circle[lindgen(n_circle)])
	endif else if (rflag eq "ring" and gflag eq "unreduced") then begin
                print, name, ": ERROR - ring goodness sought while n_ring = 0"
		status = -1
		max_goodness = 0.0
		min_goodness = 0.0
	endif else begin
		print, ": ERROR - rflag, gflag values not supported", rflag, gflag
		status = -1
		max_goodness = 0.0
		min_goodness = 0.0
	endelse
endif else begin
	status = 0
	;; print, name, ": ERROR - found n_circle = 0, n_ring = 0 in report"
	max_goodness = 0.0
	min_goodness = 0.0
endelse

if (keyword_set(adjflag)) then begin
	max_goodness = max_goodness - min_goodness
	min_goodness = -max_goodness
endif

range_data = max_goodness - min_goodness
max_goodness += 0.1 * range_data
min_goodness -= 0.1 * range_data

max_goodness_ref = max_goodness
min_goodness_ref = min_goodness

if (keyword_set(adjflag)) then begin
	if (gflag eq "reduced") then min_goodness_ref = -3.0
	if (gflag eq "reduced") then max_goodness_ref = 3.0
	if (gflag eq "unreduced") then min_goodness_ref = -1.9E2
	if (gflag eq "unreduced") then max_goodness_ref = 1.9E2
	dolog = 0
endif else begin
	min_goodness_ref = 0.0
	if (dolog) then min_goodness_ref = 1.0
	if (gflag eq "reduced") then max_goodness_ref = 3.0
	if (gflag eq "unreduced") then max_goodness_ref = 1.0E4
endelse


max_goodness = min([max_goodness, max_goodness_ref])
min_goodness = max([min_goodness, min_goodness_ref])

if ((min_goodness gt max_goodness_ref) or (max_goodness lt min_goodness_ref)) then begin
	max_goodness = 0.0
	min_goodness = 0.0
endif
if (keyword_set(adjflag)) then begin
	max_goodness = max([abs(max_goodness), abs(min_goodness)])
	min_goodness = -max_goodness
endif
if (max_goodness le 0.0) then begin
	;; print, name, ": ERROR - found unacceptable maximum goodness in report, max_goodness = ", max_goodness
	;; if (n_circle gt 0 or n_ring gt 0) then status = -1
	status = 0
	goodness_range = [0.0, 0.0]
endif else begin
	status = 0
	goodness_range = [min_goodness, max_goodness]
endelse

return, goodness_range
end

function determine_grange_report_array, reports, status = status, rflag = rflag, gflag = gflag, dolog = dolog, adjflag = adjflag
name = "determine_grange_report_array"
g_range = [0.0, 0.0]
if (n_elements(reports) eq 0 or n_elements(rflag) eq 0 || n_elements(gflag) eq 0) then begin
	print, name, ": ERROR - no report or proper flag passed"
	status = -1
	return, g_range
endif
status = 0
min_g = 0.0
max_g = 0.0
first_report = 1
for ireport = 0, n_elements(reports) - 1L, 1L do begin
	xrange = determine_goodness_range_from_report(reports[ireport], rflag = rflag, gflag = gflag, dolog = dolog, adjflag = keyword_set(adjflag), status = status)
	if (status ne 0) then begin
		print, name, ": ERROR - could not work with report at location ireport = ", ireport
		return,g_range
	endif
	if (min(xrange) lt max(xrange)) then begin
		if (first_report) then begin
			g_range = xrange
			first_report = 0
		endif else begin
			min_g = min([g_range, xrange])
			max_g = max([g_range, xrange])
			g_range = [min_g, max_g]
		endelse
	endif
endfor

return, g_range
end

function get_legend_report_array, reports, status = status
name = "get_legend_report_array"
nreport = n_elements(reports)
legend = {name: "unknown report", color: 1}
if ((nreport) eq 0) then begin	
	print, name, ": ERROR - no report passed"
	status = -1
	return, [legend]
endif
legend_array = replicate(legend, [nreport])
for ireport = 0, nreport - 1L, 1L do begin
	legend_array[ireport].color = ireport
endfor

status = 0
return, legend_array
end

function get_goodness_graph_file, outdir = outdir, iobjc = iobjc, gpfilename = gpfilename, rflag = rflag, gflag = gflag, adjflag = adjflag
name = "get_goodness_graph_file"
if (n_elements(gpfilename) eq 0) then begin
	tempfilename = "./temporary-goodness-for.fits"
	print, name, ": WARNING - no (gpfilename) has been passed - will use a temporary locator: ", tempfilename
endif else begin
	tempfilename = gpfilename[0]
endelse
if (n_elements(iobjc) eq 0) then begin
	jobjc = 0
	print, name, ": WARNING - object index should be passed, using default = ", jobjc
endif else begin
	jobjc = iobjc[0]
endelse
if (n_elements(outdir) eq 0) then begin
	print, name, ": WARNING - outdir should be passed - using default value"
	outdir = "."
endif

adjsuffix = ""
if (keyword_set(adjflag)) then adjsuffix = "-adjusted"
prefix = "chi-squared-"+gflag+adjsuffix+"-"+rflag

tempfilename = repstr(tempfilename, "gpObjc", prefix)
extpos1 = STRPOS(tempfilename, '/', /REVERSE_SEARCH)
extpos2 = STRPOS(tempfilename, '.', /REVERSE_SEARCH)
filename = outdir + "/" + STRMID(tempfilename, extpos1, extpos2 - extpos1) + "-iobjc-" + strtrim(string(jobjc), 2) + ".eps"
return, filename
end

function get_radii_from_report, report, rflag = rflag, gflag = gflag, title = title
name = "get_radii_from_report"

if (rflag eq "ring") then begin
	radii = report.radius_ring
	nring = report.n_ring
	if (nring le 0) then begin
		radii = []
	endif else begin
		radii = radii[lindgen(nring)]
	endelse
	title = "ring radius (geom. mean, arcsec)"
endif else if (rflag eq "circle") then begin
	radii = report.radius_circle
	ncircle = report.n_circle
	if (ncircle le 0) then begin
		radii = []
	endif else begin
		radii = radii[lindgen(ncircle)]
	endelse
	title = "circular mask radius (arcsec)"
endif else begin
	print, name, ": ERROR - unsupproted rflag = ", rflag
	radii = []
	title = "unknown"
endelse


return, radii
end

function get_goodness_from_report, report, rflag = rflag, gflag = gflag, adjflag = adjflag, title = title
name = "get_goodness_from_report"

if ((rflag eq "ring" and gflag eq "reduced")) then begin
	goodness = report.goodness_ring_reduced
	nx = report.n_ring
	title = "reduced chi-squared for ring"
endif else if ((rflag eq "ring" and gflag eq "unreduced")) then begin
	goodness = report.goodness_ring
        nx = report.n_ring
	title = "chi-squared for ring"
endif else if ((rflag eq "circle" and gflag eq "reduced")) then begin
	goodness = report.goodness_circle_reduced
        nx = report.n_circle
	title = "reduced chi-squared for circular mask"
endif else if ((rflag eq "circle" and gflag eq "unreduced")) then begin
	goodness = report.goodness_circle
        nx = report.n_circle
	title = "chi-squared for circular mask"
endif else begin
	print, name, ": ERROR - unsupported rflag, gflag = ", rflag, ", ", gflag
	goodness = []
	nx = 0
	title = "unknown"
endelse

if (keyword_set(adjflag)) then title += " - adjusted"

if (nx le 0) then begin
	goodness = []
endif else begin
	goodness = goodness[lindgen(nx)]
endelse

return, goodness
end


pro field_draw_matched_tuples_from_mle, $
	roots = roots, band = band, root_titles = root_titles, $
	outdir = outdir, gflag = gflag, rflag = rflag, $
	run = run, rerun = rerun, field = field, camcol = camcol, $
	star = star, galaxy = galaxy, LRG = LRG, flagLRG = flagLRG, $
	locmap = locmap, mag_thresh = mag_thresh, suffix = suffix, status = status, $
	sample_report = sample_report, $
	distance_match = distance_match, data = data


name = "field_draw_matched_tuples_from_mle"

if (n_elements(band) eq 0) then band = ["r"]
n_roots = n_elements(roots)
n_band = n_elements(band)
n_graph = n_roots * n_band
report = replicate(sample_report, [!max_nobjc_in_field, n_roots, n_band])
title = replicate("unknown title", [!max_nobjc_in_field, n_roots, n_band])
if (n_elements(gflag) ne n_elements(rflag)) then begin
	print, name, ": ERROR - gflag-array and rflag-array should be of the same size"
	status = -1
	return
endif
if (n_elements(gflag) eq 0) then begin
	gflag = ["reduced", "reduced", "unreduced", "unreduced"]
	rflag = ["ring", "circle", "ring", "circle"]
endif
n_grflag = n_elements(gflag)

linestyle_list = [0, 1, 2, 3, 4, 5]
nlinestyle = n_elements(linestyle_list)
color_list = ['Blue', 'Green', 'Red', 'Cyan', 'Magenta', 'Yellow', 'Black']
color_list = [0, 40, 80, 120, 160]
ncolor = n_elements(color_list)
thick_list = [3]
nthick = n_elements(thick_list)

if (n_elements(adjflag) eq 0) then begin
	adjflag = [1, 0]
endif
n_adjflag = n_elements(adjflag)

ifile = 0
status = 0
gpfile_array = construct_objc_files_array(root = roots, run = run, rerun = rerun, field = field, camcol = camcol, band = band, status = status, /mle)
if (status ne 0) then begin
	print, name, ": ERROR - could not get array of gpobjc files"
	return
endif

nobjc_all = 0
for iroot = 0, n_elements(roots) - 1L, 1 do begin
for iband = 0, n_elements(band) - 1L, 1 do begin
	gpfile = gpfile_array[iroot, iband]	
	filelist = file_search(gpfile, count = filecount)	
	if (filecount gt 0) then begin
		gpx_sdss = mrdfits(gpfile, 2, hdr, /silent)
		gpx = mrdfits(gpfile, 4, hdr, /silent)
		print, ".", format='(A, $)'
		nobjc = n_elements(gpx)
		if (nobjc gt !max_nobjc_in_field) then nobjc = !max_nobjc_in_field
		for iobjc = 0, nobjc - 1L, 1 do begin
			ireport = create_goodness_report_for_objc(gpx[iobjc], sdss = gpx_sdss[iobjc], sample_report = sample_report, title = root_titles[iroot], status = status)
			report[iobjc, iroot, iband] = ireport
		endfor
		if (nobjc_all lt nobjc) then nobjc_all = nobjc
	endif
endfor
endfor

print, "f", format='(A, $)'
write_count = 0

for iobjc = 0, nobjc_all - 1L, 1 do begin

	for i_grflag = 0, n_grflag -1L, 1 do begin
	for i_adjflag = 0, n_adjflag - 1L, 1 do begin

	doylog = 0
	gpfilename = gpfile_array[0]	
	rad_range = determine_rrange_report_array(report[iobjc, *, *], status = status)
	goodness_range = determine_grange_report_array(report[iobjc, *, *], status = status, gflag = gflag[i_grflag], rflag = rflag[i_grflag], dolog = doylog, adjflag = adjflag[i_adjflag])
	legend_array = get_legend_report_array(report[iobjc, *, *], status = status)
	graph_file = get_goodness_graph_file(outdir = outdir, iobjc = iobjc, gpfilename = gpfilename, gflag = gflag[i_grflag], rflag = rflag[i_grflag], adjflag = adjflag[i_adjflag])

	doxlog = 1
	rad_range = [1.0, 10.0 * max(rad_range)]
	jreport = 0
	iplot = 0
	count_plot_within_range = 0
	x_comment = 0.67 ;; in normal coordinates
	y_comment = 0.90
	xd_comment = 0.0
	yd_comment = -0.025

	for iroot = 0, n_roots - 1L, 1 do begin
	for iband = 0, n_band - 1L, 1 do begin

		xtitle = "radius (arcsec)"
		ytitle = "goodness"	
		ireport = report[iobjc, iroot, iband]	
		rad = get_radii_from_report(ireport, gflag = gflag[i_grflag], rflag = rflag[i_grflag], title = xtitle)
		goodness = get_goodness_from_report(ireport, gflag = gflag[i_grflag], rflag = rflag[i_grflag], adjflag = adjflag[i_adjflag], title = ytitle)
		goodness_type = ireport.goodness_type
		legend = legend_array[jreport]
		comment = ireport.comment
		n_comment = ireport.n_comment
		if (n_elements(rad) gt 3 and n_elements(goodness) gt 3) then begin


			if (n_elements(rad) ge 3 and n_elements(goodness) ge 3) then begin
				if (doxlog) then begin
					xplot = min(rad_range) * (max(rad_range) / min(rad_range)) ^ (dindgen(1000) / 999.0)
				endif else begin
					xplot = min(rad_range) + (max(rad_range) - min(rad_range)) * dindgen(1000) / 999
				endelse
				yplot = replicate(0.0, 1000)
				spline_p, rad, alog10(goodness + 1.0E-2), xplot, yplot
				yplot = 10.0 ^ yplot
			endif else begin
				xplot = rad
				yplot = goodness
			endelse
		
			this_mag_is_in_range = 0
			if (((ireport.mle_mag - ireport.sdss_mag) lt 2.0) and $
			(ireport.mle_mag lt 21.0) and (ireport.mle_mag gt 0.0)) then begin
				this_mag_is_in_range = 1
			endif
			linestyle = linestyle_list[iroot mod nlinestyle]
			color = color_list[iroot mod ncolor]
			thick = thick_list[iroot mod nthick]
			plot_title = $
			"SDSS c-mag = "+strtrim(string(ireport.sdss_mag, format = "(F10.1)"), 2) + $
			", R_petro = "+strtrim(string(ireport.sdss_petrorad, format = "(F10.1)"), 2) + " arcsec" + $
			", rowc = " + strtrim(string(ireport.rowc, format = "(F10.1)"), 2) + ", colc = " + strtrim(string(ireport.colc, format = "(F10.1)"), 2)

			;; print, "nrad = ", n_elements(rad)
			if (this_mag_is_in_range) then begin

			if (iplot eq 0) then begin

				olddevice = !d.name
				set_plot, 'PS' ; switch to the postscript device
				Device, DECOMPOSED=decomposed, COLOR=1, BITS_PER_PIXEL=8
				device, /encapsulated, file=graph_file, font_size = !my_font_size ; specify some details, give the file a name
				device, xsize= 8.0, ysize=6.0, /inch
				loadct, 5, /silent

				legend_items = [root_titles[iroot]]
				legend_linestyles = [linestyle]
				legend_colors = [color]
				legend_thicks = [thick]
				iroot_first = iroot
				iband_first = iband
				y_first = goodness
				yplot_first = yplot
				this_doylog = doylog	
				if (adjflag[i_adjflag]) then begin
					yadj = goodness - y_first
					yadj_plot = replicate(0.0, 1000)
					nx = min([n_elements(rad), n_elements(yadj)])
					index_nx = lindgen(nx)
					;; spline_p, rad[index_nx], yadj[index_nx], xplot, yadj_plot
					xplot = rad[index_nx]
					yadj_plot = yadj[index_nx]
					yplot = 20.0 * asinh((yadj_plot)/20.0) / asinh(1.0)
					this_doylog = 0
				endif
				plot, xplot, yplot, xrange = rad_range, yrange = goodness_range, linestyle = linestyle, color = 0, $
					xtitle = xtitle, ytitle = ytitle, xlog = doxlog, ylog = this_doylog, title = plot_title, /nodata	
				oplot, xplot, yplot, linestyle = linestyle, color = color, thick = thick
				for i_comment = 0, n_comment - 1L, 1 do begin
					xyouts, x_comment, y_comment, comment[i_comment], /normal, color = color
					x_comment += xd_comment
					y_comment += yd_comment
				endfor
				if (min(yplot) lt max(goodness_range)) then begin $
					this_plot_is_in_range = 1
					count_plot_within_range++
				endif

				iplot++
			endif else begin
				if (adjflag[i_adjflag]) then begin
					yadj = goodness - y_first
					yadj_plot = replicate(0.0, 1000)
					nx = min([n_elements(rad), n_elements(yadj)])
					index_nx = lindgen(nx)
					;; spline_p, rad[index_nx], yadj[index_nx], xplot, yadj_plot
					xplot = rad[index_nx]
					yadj_plot = yadj[index_nx]
					yplot = 20.0 * asinh((yadj_plot)/20.0) / asinh(1.0)
				endif
				oplot, xplot, yplot, linestyle = linestyle, color = color, thick = thick
				for i_comment = 0, n_comment - 1L, 1 do begin
					xyouts, x_comment, y_comment, comment[i_comment], /normal, color = color
					x_comment += xd_comment
					y_comment += yd_comment
				endfor
				legend_items = [legend_items, root_titles[iroot]]
				legend_linestyles = [legend_linestyles, linestyle]
				legend_colors = [legend_colors, color]
				legend_thicks = [legend_thicks, thick]
				if (min(yplot) lt max(goodness_range)) then begin $
					this_plot_is_in_range = 1
					count_plot_within_range++
				endif
				iplot++
			endelse

			endif else begin
				;; print, "r", format='(A, $)'
			endelse
				
		endif
		jreport++
	endfor
	endfor

	if (iplot gt 0) then begin
	
		al_legend, legend_items, linestyle=legend_linestyles, colors = legend_colors, thick = legend_thicks, /bottom, /right
		;; closing graphic device
		device, /close ; close the file
		set_plot, olddevice ; go back to the graphics device
		if (count_plot_within_range ne 2) then begin
			command = "rm -r -f "+graph_file
			spawn, command
			;; print, "d", format='(A, $)'
		endif else begin
			if (write_count eq 0) then print, "w", format = '(A, $)'
			write_count++
		endelse
			extpos = STRPOS(graph_file, '.', /REVERSE_SEARCH)
			jpegfile = STRMID(graph_file, 0, extpos)+".jpg"
			if (file_test(jpegfile)) then begin
				command = "rm -r -f "+jpegfile
				spawn, command
			endif	
			command = "convert " + graph_file + " " + jpegfile
			;; if (file_test(graph_file)) then spawn, command

			pdffile = STRMID(graph_file, 0, extpos)+".pdf"
			if (file_test(pdffile)) then begin
				command = "rm -r -f "+pdffile
				spawn, command
			endif
			;; command = "convert " + graph_file + " " + pdffile
			;; if (file_test(graph_file)) then spawn, command

			command = "ps2pdf -dPDFSETTINGS=/prepress -dEPSCrop " + graph_file + " " + pdffile
			if (file_test(graph_file)) then spawn, command

			pngfile = STRMID(graph_file, 0, extpos)+".png"
			if (file_test(pngfile)) then begin
				command = "rm -r -f "+pngfile
				spawn, command
			endif
			command = "convert " + graph_file + " " + pngfile
			;; if (file_test(graph_file)) then spawn, command

		
	endif

	endfor
	endfor

endfor

if (write_count gt 0) then print, strtrim(string(write_count), 2), format = '(A, $)'
end


function field_draw_matched_pairs_for_field, root1 = root1, root2 = root2, root3 = root3, $
	run = run, rerun = rerun, field = field, camcol = camcol, $
	sas1 = sas1, sas2 = sas2, sas3 = sas3, sim1 = sim1, sim2 = sim2, sim3 = sim3, mle1 = mle1, mle2 = mle2, mle3 = mle3, $
	fake_cd2 = fake_cd2, $
	boxplots = boxplots, star = star, galaxy = galaxy, LRG = LRG, flagLRG = flagLRG, $
	locmap = locmap, mag_thresh = mag_thresh, suffix = suffix, status = status, sample_report = sample_report, $
	distance_match = distance_match, data = data
name = "draw_matched_pairs_for_field"

verbose = !verbose_deep
verbose = 1
bad_report = sample_report
if (keyword_set(galaxy) + keyword_set(LRG) + keyword_set(star) ne 1) then begin
	print, name, ": ERROR - one of the object type keywords should be set"
	status = -1
	return, bad_report
endif

;; r-band
bandindex = 2
if (n_elements(suffix) eq 0) then suffix = ""

status = 0
file1 = construct_objc_filename(root = root1, run = run, rerun = rerun, field = field, camcol = camcol, status = status, sas = keyword_set(sas1), sim = keyword_set(sim1), mle = keyword_set(mle1))
if (status ne 0) then begin
	if (verbose) then print, name, ": ERROR- could not construct objc filename for root = ", root1
	return, bad_report
endif
file2 = construct_objc_filename(root = root2, run = run, rerun = rerun, field = field, camcol = camcol, status = status, sas = keyword_set(sas2), sim = keyword_set(sim2), mle = keyword_set(mle2))
if (status ne 0) then begin
	if (verbose) then print, name, ": ERROR - could not construct objc filename for root = ", root2
	return, bad_report
endif
if (keyword_set(mle2)) then begin
	lMfile2 = construct_LM_filename(root = root2, run = run, rerun = rerun, field = field, camcol = camcol, status = status, sas = keyword_set(sas2), sim = keyword_set(sim2), mle = keyword_set(mle2))
	if (status ne 0) then begin
       	 	if (verbose) then print, name, ": ERROR - could not construct filename for root = ", root2
        	return, bad_report
	endif
endif
skyfile1 = construct_sky_filename(root = root1, run = run, rerun = rerun, field = field, camcol = camcol, status = status) ;;construct_sky_filenamesas = keyword_set(sas1), sim = keyword_set(sim1), mle = keyword_set(mle1))
if (status ne 0) then begin
	if (verbose) then print, name, ": ERROR - could not construct sky filename for root = ", root1
	return, bad_report
endif
fpcfile1 = construct_fpC_filename(root = root1, run = run, rerun = rerun, field = field, camcol = camcol, status = status);
if (status ne 0) then begin
	if (verbose) then print, name, ": ERROR - could not construct fpC filename for root = ", root1
endif
skyfile2 = construct_sky_filename(root = root2, run = run, rerun = rerun, field = field, camcol = camcol, status = status) ;; , sas = keyword_set(sas2), sim = keyword_set(sim2), mle = keyword_set(mle2))
if (status ne 0) then begin
	if (verbose) then print, name, ": ERROR - could not construct sky filename for root = ", root2
	return, bad_report
endif
skyfile3 = construct_sky_filename(root = root2, run = run, rerun = rerun, field = field, camcol = camcol, status = status) ;; , sas = keyword_set(sas2), sim = keyword_set(sim2), mle = keyword_set(mle2))
if (status ne 0) then begin
	if (verbose) then print, name, ": ERROR - could not construct sky filename for root = ", root2
	return, bad_report
endif
lPfile1 = construct_LM_filename(/lP, root = root1, run = run, rerun = rerun, field = field, camcol = camcol, status = status, sas = keyword_set(sas1), sim = keyword_set(sim1), mle = keyword_set(mle1))
lPfile2 =  construct_LM_filename(/lP, root = root2, run = run, rerun = rerun, field = field, camcol = camcol, status = status, sas = keyword_set(sas2), sim = keyword_set(sim2), mle = keyword_set(mle2))

destination = create_destination_filename(root = root2, run = run, rerun = rerun, $
	field = field, camcol = camcol, suffix = suffix, $
	star = keyword_set(star), galaxy = keyword_set(galaxy), LRG = keyword_set(LRG), mle = keyword_set(mle2), flagLRG = flagLRG, $
	status = status)
if (status ne 0) then begin
	if (verbose) then print, name, ": ERROR - could not create destination epsfile name, (run, rerun, field, camcol) = ", run, rerun, field, camcol
	return, bad_report
endif
suffix1 = suffix + "sim-"
matchlog1 = create_matchlog_filename(root = root2, run = run, rerun = rerun, $
	field = field, camcol = camcol, suffix = suffix1, $
	star = keyword_set(star), galaxy = keyword_set(galaxy), LRG = keyword_set(LRG), mle = keyword_set(mle2), flagLRG = flagLRG, $
	status = status)
if (status ne 0) then begin
	if (verbose) then print, name, ": ERROR - could not create destination matchlog(1) filename, (run, rerun, field, camcol, status) = ", run, rerun, field, camcol, status
	return, bad_report
endif
suffix2 = suffix + "ph-"
matchlog2 = create_matchlog_filename(root = root2, run = run, rerun = rerun, $
	field = field, camcol = camcol, suffix = suffix2, $
	star = keyword_set(star), galaxy = keyword_set(galaxy), LRG = keyword_set(LRG), mle = keyword_set(mle2), flagLRG = flagLRG, $
	status = status)
if (status ne 0) then begin
	if (verbose) then print, name, ": ERROR - could not create destination matchlog(2) filename, (run, rerun, field, camcol) = ", run, rerun, field, camcol
	return, bad_report
endif

psfile1 = construct_ps_filename(root = root3, run = run, rerun = rerun, field = field, camcol = camcol, status = status, sas = keyword_set(sas3))
if (status ne 0) then begin
	if (verbose) then print, name, ": ERROR - could not create psfile name, (run, rerun, field, camcol) = ", run, rerun, field, camcol
	return, bad_report
endif
if (keyword_set(data)) then begin 
	gcdir = root2
endif else begin
	gcdir = root1
endelse
gcfile2 = construct_gc_filename(root = gcdir, run = run, rerun = rerun, field = field, camcol = camcol, status = status, sas = keyword_set(sas2), data = keyword_set(data))
if (status ne 0) then begin
	if (verbose) then print, name, ": ERROR - could not create gcfile name, (run rerun, field, camcol) = ", run, rerun, field, camcol
	return, bad_report
endif

status = -1
if (file_test(file1) ne 1) then begin
	if (verbose) then print, " : ERROR - file does not exist: file1 = ", file1
	return, bad_report
endif
if (file_test(file2) ne 1) then begin
	; if (verbose) then print, " : ERROR - file does not exist: file2 = ", file2
	return, bad_report
endif
if (file_test(psfile1) ne 1) then begin
	if (verbose) then print, " : ERROR - file does not exist: psfile1 = ", psfile1
	return, bad_report
endif
;; if (file_test(file1) ne 1 or file_test(file2) ne 1 or file_test(psfile1) ne 1) then return, bad_report

status = 0
list1 = mrdfits(file1, 1, hdr1, status = status, /silent)
if (status ne 0) then begin
	if (verbose) then print, name, ": ERROR - problem reading file1 = ", file1
	return, bad_report
endif
ext2 = 1
if (keyword_set(mle2)) then ext2 = 4 ;; ext2

list3_flag = 0
list2_flag = 0
list2 = [0.0, 0.0]
liststar2 = [0.0, 0.0]

if (file_test(file2) eq 0) then begin
	print, "f2", format='(A, $)'
endif
if (file_test(file2) eq 1) then begin


	list2 = mrdfits(file2, ext2, hdr2, status = status, /silent)
	if (status ne 0) then begin
		if (verbose) then print, name, ": ERROR - problem reading file2 = ", file2
		return, bad_report
	endif
	list2_flag = 1
	liststar2 = mrdfits(file2, 1, hdr5, status = status, /silent)
	if (keyword_set(mle2)) then begin
		ext3 = 8
		n_ext = -1
		fits_info, file2, /SILENT, TEXTOUT=1, N_ext=n_ext
		if (n_ext ge ext3) then begin
			list3 = mrdfits(file2, ext3, hdr2, status = status, /silent)
			if (status ne 0) then begin
				list3_flag = 0
			endif else begin
				list3_flag = 1
			endelse
		endif else begin
			list3_flag = 0
		endelse
	endif else begin
		list3_flag = 0
	endelse

endif

sky1 = !value_is_bad
chisq1 = !value_is_bad
chisq_float1 = !value_is_bad
chisq1_nu = !value_is_bad
chisqfl1_nu = !value_is_bad
chisq2_nu = !value_is_bad
gauss_dn1 = !default_gauss_dn
gauss_dn2 = !default_gauss_dn
gauss_dn3 = !default_gauss_dn

if (file_test(fpCfile1) eq 1) then begin
	countsky = 0
	countchisq = 0
	countchisq_float = 0
	countgauss_dn = 0
	skyheader1 = HEADFITS(fpcfile1, EXTEN = 0, /silent)
	sky1 = 1.0E0 * SXPAR(skyheader1, "SKYFPCC", count = countsky)
	chisq1 = 1.0E0 * SXPAR(skyheader1, "CHISQ", count = countchisq)
	chisq_float1 = 1.0E0 * SXPAR(skyheader1, "CHISQFL", count = countchisq_float);
	gauss_dn1 = 1.0E0 * SXPAR(skyheader1, "GAUSSDN", count = countgauss_dn)
	chisq1_nu = chisq1 / !degree_of_freedom
	chisqfl1_nu = chisq_float1 / !degree_of_freedom
	if (countsky eq 0) then sky1 = !value_is_bad
	if (countchisq eq 0) then chisq1 = !value_is_bad
	if (countchisq_float eq 0) then chisq_float1 = !value_is_bad
	if (countchisq eq 0) then chisq1_nu = !value_is_bad
	if (countchisq_float eq 0) then chisqfl1_nu = !value_is_bad
	if (countgauss_dn eq 0) then gauss_dn1 = !default_gauss_dn
endif else begin
	if (file_test(skyfile1) eq 1) then begin
	;; sky_image1 = mrdfits(skyfile1, 0, hdr);
	countsky = 0
	countchisq = 0
	countchisq_float = 0
	countgauss_dn = 0
	skyheader1 = HEADFITS(skyfile1, EXTEN = 0, /silent)
	sky1 = 1.0E0 * SXPAR(skyheader1, "SKYFPCC", count = countsky)
	chisq1 = 1.0E0 * SXPAR(skyheader1, "CHISQ", count = countchisq)
	chisq_float1 = 1.0E0 * SXPAR(skyheader1, "CHISQFL", count = countchisq_float);
	gauss_dn1 = 1.0E0 * SXPAR(skyheader1, "GAUSSDN", count = countgauss_dn)
	chisq1_nu = chisq1 / !degree_of_freedom
	chisqfl1_nu = chisq_float1 / !degree_of_freedom
	if (countsky eq 0) then sky1 = !value_is_bad
	if (countchisq eq 0) then chisq1 = !value_is_bad
	if (countchisq_float eq 0) then chisq_float1 = !value_is_bad
	if (countchisq eq 0) then chisq1_nu = !value_is_bad
	if (countchisq_float eq 0) then chisqfl1_nu = !value_is_bad
	if (countgauss_dn eq 0) then gauss_dn1 = !default_gauss_dn
	endif 
endelse

sky2 = !value_is_bad
if (file_test(skyfile2) eq 1) then begin
	;; sky_image2 = mrdfits(skyfile2, 0, hdr);
	skyheader2 = HEADFITS(skyfile2, EXTEN = 0, /silent)
	countsky = 0
	sky2 = 1.0E0 * SXPAR(skyheader2, "SKYFPCC", count = countsky)
	if (countsky eq 0) then sky2 = !value_is_bad
endif

sky3 = !value_is_bad
if (file_test(skyfile3) eq 1) then begin
	;; sky_image1 = mrdfits(skyfile3, 0, hdr);
	skyheader3 = HEADFITS(skyfile3, EXTEN = 0, /silent)
	countsky = 0
	sky3 = 1.0E0 * SXPAR(skyheader3, "SKYFPCC", count = countsky)
	if (countsky eq 0) then sky3 = !value_is_bad
endif



lMlist2 = [0.0, 0.0]
list2_lMflag = 0
list3_lMflag = 0
if (file_test(lMfile2) eq 1) then begin

	if (keyword_set(mle2)) then begin
		lMlist2 = mrdfits(lMfile2, 1, hdr4, status = status, /silent)
		if (status ne 0) then begin
			if (verbose) then print, name, ": ERROR - problem reading lMfile2 = ", lMfile2
			return, bad_report
		endif
		list2_lMflag = 1
		ext3 = 2
		n_ext = -1
		fits_info, lMfile2, /SILENT, TEXTOUT=1, N_ext=n_ext
		if (n_ext ge ext3) then begin
			lMlist3 = mrdfits(lMfile2, ext3, hdr2, status = status, /silent)
			if ((status ne 0) or (size(lMlist3[0], /type) ne 8)) then begin
				list3_lMflag = 0
			endif else begin
				list3_lMflag = 1
			endelse
		endif else begin
			list3_lMflag = 0
		endelse
	endif

endif
calib_info = mrdfits(psfile1, 7, hdr3, status = status, /silent)
if (status ne 0) then begin
	if (verbose) then print, name, ": ERROR - problem reading psfile1 = ", psfile1
	return, bad_report
endif

galactic_calib = mrdfits(gcfile2, 1, hdr4, status = status, /silent)
if (status ne 0) then begin
	if (verbose) then print, name, ": ERROR - gcfile2 could not be read = ", gcfile2
 	return, bad_report
endif

status = -1
if (size(list1[0], /type) ne 8) then begin
	if (verbose) then print, name, ": ERROR - list1 does not contain structures"
	return, bad_report
endif
if (list2_flag eq 1 and size(list2[0], /type) ne 8) then begin
	if (verbose) then print, name, ": ERROR - list2 does not contain structures"
	return, bad_report
endif
if (size(calib_info[0], /type) ne 8) then begin
	if (verbose) then print, name, ": ERROR - calib_info file does not contain any structures"
	return, bad_report
endif
chisq2 = !value_is_bad
chisq2_nu = !value_is_bad
dchisq2 = !value_is_bad
nstep2 = !value_is_bad
cost2 = !value_is_bad
chisq3 = !value_is_bad
chisq3_nu = !value_is_bad
dchisq3 = !value_is_bad
nstep3 = !value_is_bad
cost3 = !value_is_bad
if (keyword_set(mle2)) then begin
	if (size(lMlist2[0], /type) ne 8) then begin
		if (verbose and 1 eq 0) then print, name, ": ERROR - lM file does not contain any structures"
		dchisq2 = !value_is_bad
		chisq2 = !value_is_bad
		nstep2 = !value_is_bad
		if (list3_lMflag eq 1) then begin
			dchisq3 = !value_is_bad 
			chisq3 = !value_is_bad
			nstep3 = !value_is_bad
		endif
	endif else begin
		dchisq2 = lMlist2[0].chisq - min(lMlist2[*].chisq)
		chisq2 = min(lMlist2[*].chisq)
		chisq2_nu = chisq2 / !degree_of_freedom
		nstep2 = n_elements(lMlist2)
		nstep2 = max(lMlist2[*].k)
		cost2 = min(lMlist2[*].cost)
		if (list3_lMflag eq 1) then begin
			dchisq3 = lMlist3[0].chisq - min(lMlist3[*].chisq)
			chisq3 = min(lMlist3[*].chisq)
			chisq3_nu = chisq3 / !degree_of_freedom
			nstep3 = n_elements(lMlist3)
			nstep3 = max(lMlist3[*].k)
			cost3 = min(lMlist3[*].cost)
		endif
	endelse
endif
status = 0

;; setting up the graphic device
epsfile = destination

if (n_elements(seed) eq 0) then seed = 0;

if (!graphical_output eq 1) then begin
	if (keyword_set(galaxy) or keyword_set(LRG)) then begin
		n1 = 2
		n2 = 3
	endif
	if (keyword_set(star)) then begin
		n1 = 2
		n2 = 1
	endif
	if (keyword_set(locmap)) then n1 += 1

	xsize= !my_xsize ;;  * n1 / 2.0 ;; 45
	ysize= (!my_xsize * n2) / (n1 * !my_xyratio) ;; 45

	olddevice = !d.name ; store the current device name in a variable
	if (!do_plots) then begin
		set_plot, 'PS' ; switch to the postscript device
		Device, DECOMPOSED=decomposed, COLOR=1, BITS_PER_PIXEL=8
		device, /encapsulated, file=epsfile, font_size = !my_font_size ; specify some details, give the file a name
		loadct, 40, /silent
		device, xsize=xsize, ysize=ysize
	endif
	xformat = "(I10)"
	yformat = "(E10.1)"

	!Y.Margin = [4, 4]
	!Y.OMargin = !Y.Margin
	if (!do_plots) then multiplot, /reset
	if (!do_plots) then multiplot, [n1, n2], $
		xgap = !my_xgap, ygap = !my_ygap, $
		xtickformat = xformat, ytickformat = yformat, $
		mtitle = mtitle, mTitOffset = 2.0, $
		/doxaxis, /doyaxis

	;; device is set up now
	;; 

endif

gal_type = !galaxy_type
star_type = !star_type

color1 = 1
color2 = 20
color3 = 60

forward_function basic_draw_matched_pairs
report = basic_draw_matched_pairs(list1 = list1, list2 = list2, liststar2 = liststar2, list3 = list3, $
	sky1 = sky1, sky2 = sky2, sky3 = sky3, $
        color1 = color1, color2 = color2, color3 = color3, boxplots = keyword_set(boxplots), $
	galaxy = keyword_set(galaxy), star = keyword_set(star), LRG = keyword_set(LRG), flagLRG = flagLRG, locmap = keyword_set(locmap), $
	fake_cd2 = keyword_set(fake_cd2), $
	mag_thresh = mag_thresh, matchlog1 = matchlog1, matchlog2 = matchlog2, $
	calib_info = calib_info, galactic_calib = galactic_calib, sample_report = sample_report, distance_match = distance_match)

report.field = field
report.run = run
report.camcol = camcol
report.sky1 = sky1
report.sky2 = sky2
report.sky3 = sky3
report.chisq1 = chisq1
report.chisq_float1 = chisq_float1
;; report.dchisq2 = dchisq2
;; report.chisq2 = chisq2
report.nstep2 = nstep2
;; report.cost2 = cost2
;; report.dchisq3 = dchisq3
;; report.chisq3 = chisq3
report.nstep3 = nstep3
;; report.cost3 = cost3

;;report.chisq1_nu = chisq1_nu
;; report.chisqfl1_nu = chisqfl1_nu
;; report.chisq2_nu = chisq2_nu
;; eport.chisq3_nu = chisq3_nu

report.gaussdn1 = gauss_dn1
report.gaussdn2 = gauss_dn2
report.gaussdn3 = gauss_dn3

;; if (not !do_plots and file_test(lPfile1) and file_test(lPfile2)) then begin
if (not !do_plots and (file_test(lPfile1) or file_test(lPfile2))) then begin

	do_lp1 = file_test(lpfile1)
	lp1 = [-1L]
	if (file_test(lpfile1)) then lp1 = mrdfits(lpfile1, 1, hdr_lp, /silent);
	if (size(lp1[0], /type) eq 8) then begin

		nlp_1 = n_elements(lp1)
		lp1 = lp1[nlp_1 - 1L]
	
		nmodel_profile1 = lp1.nmodel
		if (nmodel_profile1 gt 1) then begin
			core_r1 = lp1.re_deV1 * !pixelsize
			halo_r1 = lp1.re_deV2 * !pixelsize
		endif else begin
			core_r1 = lp1.re_deV * !pixelsize
			halo_r1 = !value_is_bad
		endelse
		cprofile1 = lp1.profile
		core_profile1 = lp1.modelprofiles[0:999]
		halo_profile1 = lp1.modelprofiles[1000:1999]
		
		cprofile1[*] = cprofile1[*];;  + sky1
		;; if (min(cprofile1) eq max(cprofile1)) then print, lpfile1, format='(A, $)'
		rad = lp1.rad

		do_lp1 = 1
	endif else begin
		do_lp1 = 0
	endelse
	

	do_lp2 = file_test(lpfile2)
	lp2 = [-1L]
	lp2_list = [-1L]
	n_accepted_steps = 0
	n_rejected_steps = 0
	if (file_test(lpfile2)) then lp2_list = mrdfits(lpfile2, 1, hdr_lp, /silent);
	if (size(lp2_list[0], /type) eq 8) then begin
		nlp_2 = n_elements(lp2_list)
		n_accepted_steps = 0
		objid_list = lp2_list.objid
		objid_list = objid_list[UNIQ(objid_list, SORT(objid_list))]
		objid0 = ""
		count_objid0 = 0
		for i_objid = 0, n_elements(objid_list) - 1L, 1L do begin
			objid = objid_list[i_objid]
			objid_index = where(lp2_list.objid eq objid, count_objid)
			if (count_objid gt count_objid0) then begin
				objid0 = objid
				count_objid0 = count_objid
			endif
		endfor
		print, objid0, count_objid0, n_elements(lp2_list)	
		accepted_steps_index = where(lp2_list.lm_status eq 1 and lp2_list.objid eq objid0, n_accepted_steps)
		n_rejected_steps = 0
		rejected_steps_index = where(lp2_list.lm_status eq 0 and lp2_list.objid eq objid0, n_rejected_steps)
		if (n_accepted_steps gt 0) then begin
			lp2_good_list = lp2_list[accepted_steps_index]
			lp2_good_list = lp2_list
			lp2 = lp2_list[max(accepted_steps_index)]
		endif else begin
			lp2_good_list = lp2_list
			lp2 = lp2_list[nlp_2 - 1L]
		endelse
		rad_deV = lp2_good_list.re_deV
		rad_deV1 = lp2_good_list.re_deV1
		rad_deV2 = lp2_good_list.re_deV2
		if (tag_exist(lp2_good_list[0], "RE_PL")) then begin
			rad_pl = lp2_good_list.re_pl
		endif else begin
			rad_pl = replicate(0.0, n_elements(lp2_good_list))
		endelse

		temp_index = where(rad_deV ne 0, count_rad_deV)
		temp_index = where(rad_deV1 ne 0, count_rad_deV1)
		temp_index = where(rad_deV2 ne 0, count_rad_deV2)
		temp_index = where(rad_pl ne 0, count_rad_pl)
		temp_index = [-1]
		if (count_rad_deV gt 0) then rad_core = rad_deV
		if (count_rad_deV1 gt 0) then rad_core = rad_deV1
		if (count_rad_deV2 gt 0) then rad_halo = rad_deV2
		if (count_rad_pl gt 0) then rad_halo = rad_pl	

		do_lp2 = 1
		if (n_elements(rad_core) + n_elements(rad_halo) eq 0) then do_lp2 = 0

			if (do_lp2) then begin
			if (n_elements(rad_core) eq 0) then begin	
				rad_code = findgen(n_elements(rad_halo)) + 1.0
			endif else begin	
				rad_core = rad_core * !PIXELSIZE	
			endelse		
			if (n_elements(rad_halo) eq 0) then begin
				rad_halo = findgen(n_elements(rad_core)) + 1.0
			endif else begin
				rad_halo = rad_halo * !PIXELSIZE
			endelse
			nmodel_profile2 = lp2.nmodel
			cprofile2 = lp2.profile
			core_profile2 = lp2.modelprofiles[0:999]
			halo_profile2 = lp2.modelprofiles[1000:1999]
			cprofile2[*] = cprofile2[*];;  + sky1
			;; if (min(cprofile2) eq max(cprofile2)) then print, lpfile2, format='(A, $)'
	
			if (not do_lp1) then rad = lp2.rad
			do_lp2 = 1
		endif
	endif else begin
		do_lp2 = 0
	endelse

	if (do_lp1 or do_lp2) then begin
	if (size(lp1[0], /type) ne 8) then print, "lP1", format='(A, $)'
	if (size(lp2[0], /type) ne 8) then print, "lP2", format='(A, $)'	

	if (do_lp1) then begin
		profile_title = "I. noise = "+strtrim(string(gauss_dn1, FORMAT='(F5.2)'), 2)+$
		" (DN/pixel), r_core = "+strtrim(string(core_r1, FORMAT='(F5.2)'), 2) + $
		"(arcsec), r_halo = "+strtrim(string(halo_r1, FORMAT='(F5.2)'), 2)+"(arcsec)"
	endif else begin
		profile_title = "II. noise = "+strtrim(string(gauss_dn2, FORMAT='(F5.2)'), 2)+$
		" (DN/pixel), r_core = "+strtrim(string(!value_is_bad, FORMAT='(F5.2)'), 2) + $
		"(arcsec), r_halo = "+strtrim(string(!value_is_bad, FORMAT='(F5.2)'), 2)+"(arcsec)"
	endelse

	count_valid_rad = 0
	valid_rad_index = where(rad gt 0.0, count_valid_rad)
	if (count_valid_rad gt 0 and valid_rad_index[0] ne 0) then valid_rad_index = [0, valid_rad_index]
	float_soft_bias = 2.0E-4
	if (do_lp1) then begin
		cprofile1 = cprofile1[valid_rad_index] + float_soft_bias
		core_profile1 = core_profile1[valid_rad_index] + float_soft_bias
		halo_profile1 = halo_profile1[valid_rad_index] + float_soft_bias
	endif
	if (do_lp2) then begin
		cprofile2 = cprofile2[valid_rad_index] + float_soft_bias
		core_profile2 = core_profile2[valid_rad_index] + float_soft_bias
		halo_profile2 = halo_profile2[valid_rad_index] + float_soft_bias
	endif
	rad = rad[valid_rad_index] * !PIXELSIZE + 0.1

	do_lp_ylog = 1
	min_yrange = 1.0E-5
	max_yrange = 1.0E5
	if (do_lp1 and do_lp2) then begin
		min_yrange = min([cprofile1, cprofile2])
		max_yrange = max([cprofile1, cprofile2])
	endif else begin
		if (do_lp1) then begin
			min_yrange = min(cprofile1)
			max_yrange = max(cprofile1)
		endif 
		if (do_lp2) then begin
			min_yrange = min(cprofile2)
			max_yrange = max(cprofile2)
		endif
	endelse

	if (do_lp_ylog and min_yrange lt 0.0) then min_yrange = 1.0E-5
	min_yrange = 1.0E-4	
	min_xrange = min(rad)
	max_xrange = max(rad)
	lp_xrange = [min_xrange, max_xrange]
	lp_yrange = [min_yrange, max_yrange]

	lpepsfile = create_destination_lP_filename(root = root2, run = run, rerun = rerun, $
		field = field, camcol = camcol, suffix = suffix, $
		star = keyword_set(star), galaxy = keyword_set(galaxy), LRG = keyword_set(LRG), mle = keyword_set(mle2), flagLRG = flagLRG, $
		status = status)

	set_plot, 'PS' ; switch to the postscript device
	Device, DECOMPOSED=decomposed, COLOR=1, BITS_PER_PIXEL=8
	device, /encapsulated, file= lpepsfile, font_size = !my_font_size ; specify some details, give the file a name
	loadct, 40, /silent
	device, xsize=8.0, ysize=6.0, /inch

	if (1 eq 0) then begin

		if (nmodel_profile1 gt 1 or nmodel_profile2 gt 1) then begin
			multiplot, /reset	
			multiplot, [3, 1], /doxaxis, /doyaxis
		endif

		plot, rad, cprofile1, /xlog, ylog = do_lp_ylog, xrange = lp_xrange, yrange = lp_yrange, thick = 15, linestyle = 5, xtitle = "r (arcsec)", ytitle = "intensity (DN / pixel)"
		oplot, rad, cprofile2, thick = 8, linestyle = 0, color = 220 ;; /xlog, /ylog, xrange = lp_xrange, yrange = lp_yrange, thick = 2, linestyle = 0, xtitle = "r", ytitle = "intensity (DN / pixel)"
		if (nmodel_profile1 gt 1 or nmodel_profile2 gt 1) then begin
			multiplot
			plot, rad, core_profile1, /xlog, ylog = do_lp_ylog, xrange = lp_xrange, yrange = lp_yrange, thick = 10, linestyle = 5, xtitle = "r (arcsec)", ytitle = "intensity (DN / pixel)"
			if (nmodel_profile2 gt 1) then begin
				oplot, rad, core_profile2, thick = 4, linestyle = 0, color = 220
			endif
			multiplot

			if (nmodel_profile1 gt 1) then begin	
				plot, rad, halo_profile1, /xlog, ylog = do_lp_ylog, xrange = lp_xrange, yrange = lp_yrange, thick = 10, linestyle = 5, xtitle = "r (arcsec)", ytitle = "intensity (DN / pixel)"
				if (nmodel_profile2 gt 1) then begin
					oplot, rad, halo_profile2, thick = 4, linestyle = 0, color = 220
				endif
			endif else begin 
			plot, rad, halo_profile2, /xlog, ylog = do_lp_ylog, xrange = lp_xrange, yrange = lp_yrange, thick = 14, linestyle = 0, color = 220, xtitle = "r (arcsec)", ytitle = "intensity (DN / pixel)"
			endelse
		endif

	endif else begin
		
		if (do_lp1 and n_elements(rad) gt 1) then begin

			plot, rad, cprofile1, /xlog, ylog = do_lp_ylog, xrange = lp_xrange, yrange = lp_yrange, thick = 15, linestyle = 5, $
			xtitle = "r (arcsec)", ytitle = "intensity (DN / pixel)", title = profile_title
			if (nmodel_profile1 gt 1) then begin
				oplot, rad, core_profile1, thick = 4, linestyle = 5
			oplot, rad, halo_profile1, thick = 4, linestyle = 5
			endif
		
			if (do_lp2) then begin
				oplot, rad, cprofile2, thick = 8, linestyle = 0, color = 220 ;; /xlog, /ylog, xrange = lp_xrange, yrange = lp_yrange, thick = 2, linestyle = 0, xtitle = "r", ytitle = "intensity (DN / pixel)"
				if (nmodel_profile2 gt 1) then begin
				oplot, rad, core_profile2, thick = 2, linestyle = 0, color = 120
				oplot, rad, halo_profile2, thick = 2, linestyle = 0, color = 120
				endif
			endif

		endif else begin

			if (do_lp2 and n_elements(rad) gt 1) then begin
				plot, rad, cprofile2, /xlog, ylog = do_lp_ylog, xrange = lp_xrange, yrange = lp_yrange, thick = 15, linestyle = 5, $
				xtitle = "r (arcsec)", ytitle = "intensity (DN / pixel)", title = profile_title
				if (nmodel_profile2 gt 1) then begin
				oplot, rad, core_profile2, thick = 2, linestyle = 0, color = 120
				oplot, rad, halo_profile2, thick = 2, linestyle = 0, color = 120
				endif
			endif

		endelse
	
	endelse

	lp1 = [-1]
	lp2 = [-1]
	cprofile1 = [-1]
	cprofile2 = [-1]
	rad = [-1]	

	;; closing graphic device
	device, /close ; close the file
	set_plot, olddevice ; go back to the graphics device
	;; command = "epstopdf "+lpepsfile
	;; spawn, command
	extpos = STRPOS(lpepsfile, '.', /REVERSE_SEARCH)
	jpegfile = STRMID(lpepsfile, 0, extpos)+".jpg"
	command = "convert " + lpepsfile + " " + jpegfile
	if (file_test(jpegfile)) then begin
		command = "rm -r -f "+jpegfile
		spawn, command
	endif
	pdffile = STRMID(lpepsfile, 0, extpos)+".pdf"
	command = "convert " + lpepsfile + " " + pdffile
	if (file_test(pdffile)) then begin
		command = "rm -r -f "+pdffile
		spawn, command
	endif
	pngfile = STRMID(lpepsfile, 0, extpos)+".png"
	command = "convert " + lpepsfile + " " + pngfile
	spawn, command

	lpepsfile = create_destination_lP_filename(root = root2, run = run, rerun = rerun, $
		field = field, camcol = camcol, suffix = suffix, $
		star = keyword_set(star), galaxy = keyword_set(galaxy), LRG = keyword_set(LRG), $
		mle = keyword_set(mle2), flagLRG = flagLRG, $
		status = status, /tracing)
	
	set_plot, 'PS' ; switch to the postscript device
	Device, DECOMPOSED=decomposed, COLOR=1, BITS_PER_PIXEL=8
	device, /encapsulated, file= lpepsfile, font_size = !my_font_size 
	; specify some details, give the file a name
	loadct, 40, /silent
	;; device, xsize=8.0, ysize=6.0, /inch
	device, xsize=10.0, ysize=10.0, /inch

	if (do_lp2 eq 1 and n_elements(rad_core) gt 1 and n_accepted_steps gt 0) then begin
		print, "lT2", format = '(A, $)'
		x_target = [12.0] * !PIXELSIZE
		y_target = [36.0] * !PIXELSIZE / 2.26444
		y_target = x_target

		x_line = [0.0, 10.0]
		y_line = [8.0 / 3.0, 143.0 / 192.0 * 10.0 + 8.0 / 3.0]
		xrange = [0.0, 15.0]
		yrange = [0.0, 15.0]
		dolog_path = 0

		x_line = xrange
		y_line = yrange
		x_line2 = [x_target, x_target]
		y_line2 = [0.01, 200.0]
		x_line3 = y_line2
		y_line3 = x_line2
		
		Atemp = FIndGen(16) * (!PI*2/16.) 
		UserSym, cos(Atemp), sin(Atemp), /fill 
	
		plot, rad_core[accepted_steps_index] + 1.0E-3, rad_halo[accepted_steps_index] + 1.0E-3, xrange = xrange, yrange = yrange, psym = -8, /xstyle, /ystyle, $
		xtitle = "r_core (arcsec)", ytitle = "r_halo (arcsec)", xcharsize = 1.5, ycharsize= 1.5, symsize=2.0, xlog = dolog_path, ylog = dolog_path, $
		/isotropic, position = [0.1, 0.1, 0.9, 0.9]
		if (n_rejected_steps gt 0) then oplot, rad_core[rejected_steps_index] + 1.0E-3, rad_halo[rejected_steps_index] + 1.0E-3, psym = 8, color = 250, symsize=0.5
		oplot, x_line, y_line, color = 250
		oplot, x_line2, y_line2, color = 200, linestyle = 2
		oplot, x_line3, y_line3, color = 200, linestyle = 2
		oplot, x_target, y_target, psym = 4, color = 20, symsize= 3
	endif else begin	
		print, "lt2e", format = '(A, $)'
	endelse
	;; closing graphic device
	device, /close ; close the file
	set_plot, olddevice ; go back to the graphics device
	;; command = "epstopdf "+lpepsfile
	;; spawn, command
	extpos = STRPOS(lpepsfile, '.', /REVERSE_SEARCH)
	jpegfile = STRMID(lpepsfile, 0, extpos)+".jpg"
	command = "convert " + lpepsfile + " " + jpegfile
	if (file_test(jpegfile)) then begin
		command = "rm -r -f "+jpegfile
		spawn, command
	endif
	pdffile = STRMID(lpepsfile, 0, extpos)+".pdf"
	command = "convert " + lpepsfile + " " + pdffile
	if (file_test(pdffile)) then begin
		command = "rm -r -f "+pdffile
		spawn, command
	endif
	pngfile = STRMID(lpepsfile, 0, extpos)+".png"
	command = "convert " + lpepsfile + " " + pngfile
	spawn, command





	endif else begin
	;; 	if (size(lp1[0], /type) ne 8) then print, "lP1", format='(A, $)'
	;; 	if (size(lp2[0], /type) ne 8) then print, "lP2", format='(A, $)'	
	endelse
endif else begin
	;; if (file_test(lpfile1) eq 0) then print, "lp1", format='(A, $)'
	;; if (file_test(lpfile2) eq 0) then print, "lp2", format='(A, $)'
endelse

	

if (!do_plots and !graphical_output eq 1) then begin

	;; closing graphic device
	device, /close ; close the file
	set_plot, olddevice ; go back to the graphics device
	command = "epstopdf "+epsfile
	spawn, command
	extpos = STRPOS(epsfile, '.', /REVERSE_SEARCH)
	jpegfile = STRMID(epsfile, 0, extpos)+".jpg"
	command = "convert " + epsfile + " " + jpegfile
	spawn, command

endif
;; print, name, ": report - ", n_elements(report)
return, report
end


pro analyze_report_for_coincidence, report = report
name = "analyze_report_for_coincidence"
x0 = report[0].x1
y0 = report[0].y1
n = n_elements(report)
x = report[1:n-1].x1
y = report[1:n-1].y1
d = sqrt((x - x0) ^ 2 + (y - y0) ^ 2)
dmin = min(d)
count = 0
index = where(d eq dmin, count)
print, name, ": found near coincidence", count + 1
print, name, ": dmin = ", dmin
index[*] = index[*] + 1
index = [0, index]
print, name, ": index - ", index
print, name, ": fields - ", report[index].field
print, name, ": bands  - ", report[index].band
return
end



pro draw_report, filename = filename, report = report, star = star, galaxy = galaxy, LRG = LRG, flagLRG = flagLRG, locmap = locmap, boxplots = boxplots,  status = status
name = "draw_report"
verbose = !verbose

epsfile = filename
if (n_elements(report) eq 0) then begin
	print, name, ": ERROR - no report passed"
	status = -1
	return
endif


print, "DDD", format='(A, $)'
count = 0
index = where(report.match ge 0L and report.bname eq 'r', count)
if (count eq 0) then begin
	if (verbose) then print, name, ": ERROR - report does not contain any objects"
	print, "X", format='(A, $)'
	;; print, name, ": report match (min, max) = ", min(report.match), max(report.match)
	;; print, name, ": report.bname = ", report[0:10].bname
	status = -1
	return
endif 
greport = report[index]

print, "matched report created"

;; analyze_report_for_coincidence, report = greport

status = 0

if (!graphical_output eq 1) then begin

	print, name, ": graphical output"

	if (keyword_set(galaxy) or keyword_set(LRG)) then begin
		n1 = 3
		n2 = 3
	endif
	if (keyword_set(star)) then begin
		n1 = 3
		n2 = 1
	endif
	if (keyword_set(locmap)) then n1 += 1

	xsize= !my_xsize ;;  * n1 / 2.0 ;; 45
	ysize= (!my_xsize * n2) / (n1 * !my_xyratio) ;; 45

	olddevice = !d.name ; store the current device name in a variable
	if (!do_plots) then begin
		set_plot, 'PS' ; switch to the postscript device
		Device, DECOMPOSED=decomposed, COLOR=1, BITS_PER_PIXEL=8
		device, /encapsulated, file=epsfile, font_size = !my_font_size ; specify some details, give the file a name
		loadct, 40, /silent
		device, xsize=xsize, ysize=ysize
	endif
	xformat = "(I10)"
	yformat = "(E10.1)"

	!Y.Margin = [4, 4]
	!Y.OMargin = !Y.Margin
	if (!do_plots) then multiplot, /reset
	if (!do_plots) then multiplot, [n1, n2], $
	xgap = !my_xgap, ygap = !my_ygap, $
	xtickformat = xformat, ytickformat = yformat, $
	mtitle = mtitle, mTitOffset = 2.0, $
	/doxaxis, /doyaxis

	match_count = 0
	match_index = where(greport.match eq 1, match_count)
	if (match_count gt 0) then begin
		matched_report = greport[match_index] 
	endif else begin
		if (verbose) then print, name, ": WARNING - no object was reported as matched"
	endelse

	unmatch_count = 0
	unmatch_index = where(greport.match eq 0, unmatch_count)
	if (unmatch_count gt 0) then begin 
		unmatched_report = greport[unmatch_index] 
	endif else begin
		if (verbose) then print, name, ": WARNING - all objects were reported as matched"
	endelse

	if (match_count gt 0) then match_distance = alog10((matched_report.delta + 0.01) * !PIXELSIZE)
	if (match_count gt 1) then begin
	if (match_count ge 100) then $
		nbin = 20 + fix((alog10(match_count) - 2.0) * 20.0) $
	else $
		nbin = 1 + fix(match_count / 5)
	distance_pdf = histogram(match_distance, locations = distance_bin, nbin = nbin)
	endif 

	mag_range = [12.0, 27.0]
	r_range = [-2.0, 2.0]

	xrange = [0.0, 2048.0]
	yrange = [0.0, 1489.0]
	xstyle = 7
	ystyle = 7
	psym1 = 1
	psym2 = 4
	color_match = 1 
	color_unmatch = 80 
	color_line = 120

	title = "FOV - sim input - M, U = "+strtrim(string(match_count), 2)+","+strtrim(string(unmatch_count), 2)
	if (!do_plots and match_count gt 0) then plot, [matched_report.x1], [matched_report.y1], $
	psym=psym1,color = color_match,  xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle
	if (!do_plots and unmatch_count gt 0) then plot, [unmatched_report.x1], [unmatched_report.y1], $
	psym=psym1,color = color_unmatch,  xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle
	if (!do_plots) then plot, [0], [0], /nodata, xrange = xrange, yrange = yrange, xstyle =  5, ystyle = 5, color = 1, title = title
	if (!do_plots) then multiplot, /doxaxis, /doyaxis

	title = "FOV - PHOTO finding"
	if (match_count gt 0) then begin
	if (!do_plots) then plot, [matched_report.x2], [matched_report.y2], $
	psym=psym2,color = color_match,  xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle
	if (!do_plots) then plot, [0], [0], /nodata, xrange = xrange, yrange = yrange, xstyle =  5, ystyle = 5, color = 1, title = title
	if (!do_plots) then multiplot, /doxaxis, /doyaxis

	title = "input <--> output"
	if (!do_plots) then plot, [matched_report.x1], [matched_report.y1], $
	psym=psym1,color = color_match,  xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle
	if (!do_plots) then plot, [matched_report.x2], [matched_report.y2], $
	psym=psym2,color = color_match,  xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle
	for i = 0, match_count-1L, 1L do begin
		x = [matched_report[i].x1, matched_report[i].x2]
		y = [matched_report[i].y1, matched_report[i].y2]
		if (!do_plots) then plot, x, y, xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle, color = color_line

	endfor	
	endif


	if (!do_plots) then plot, [0], [0], /nodata, xrange = xrange, yrange = yrange, xstyle =  5, ystyle = 5, color = 1, title = title
	if (!do_plots) then multiplot, /doxaxis, /doyaxis

	xrange = mag_range
	yrange = r_range

	title = "match distance (log arcsec) histogram"
	if (!do_plots and match_count gt 1) then begin
	plot, distance_bin, distance_pdf, title = title, xtickformat = "(F10.1)"
	endif
	if (!do_plots) then multiplot

	title1 = "mag - r: simulation input - all"
	if (!do_plots and match_count gt 0) then plot, [matched_report.mag1], [matched_report.r1], $
		psym=psym1,color = color_match,  xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle
	if (!do_plots and unmatch_count gt 0) then plot, [unmatched_report.mag1], [unmatched_report.r1], $
	psym=psym1,color = color_unmatch,  xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle
	if (!do_plots) then plot, [0], [0], /nodata, xrange = xrange, yrange = yrange, xstyle =  1, ystyle = 1, color = 1, title = title1
	if (!do_plots) then multiplot, /doxaxis, /doyaxis

	title1 = "simulation - unmatched"
	if (!do_plots and unmatch_count gt 0) then plot, [unmatched_report.mag1], [unmatched_report.r1], $
	psym=psym1,color = color_unmatch,  xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle
	if (!do_plots) then plot, [0], [0], /nodata, xrange = xrange, yrange = yrange, xstyle =  1, ystyle = 1, color = 1, title = title1
	if (!do_plots) then multiplot, /doxaxis, /doyaxis

	title1 = "simulation - matched"
	if (!do_plots and match_count gt 0) then plot, [matched_report.mag1], [matched_report.r1], $
	psym=psym1,color = color_match,  xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle
	if (!do_plots) then plot, [0], [0], /nodata, xrange = xrange, yrange = yrange, xstyle =  1, ystyle = 1, color = 1, title = title1
	if (!do_plots) then multiplot, /doxaxis, /doyaxis

	title1 = "mag - r: PHOTO"
	if (!do_plots and match_count gt 0) then plot, [matched_report.mag2], [matched_report.r2], $
	psym=psym2,color = color_match,  xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle
	if (!do_plots) then plot, [0], [0], /nodata, xrange = xrange, yrange = yrange, xstyle =  1, ystyle = 1, color = 1, title = title1
	if (!do_plots) then multiplot, /doxaxis, /doyaxis

	title1 = "input <--> output"
	if (!do_plots and match_count gt 0) then begin
		if (!do_plots) then plot, [matched_report.mag1], [matched_report.r1], $
		psym=psym1,color = color_match,  xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle
		if (!do_plots) then plot, [matched_report.mag2], [matched_report.r2], $
		psym=psym2,color = color_match,  xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle
		for i = 0, match_count-1L, 1L do begin
		x = [matched_report[i].mag1, matched_report[i].mag2]
		y = [matched_report[i].r1, matched_report[i].r2]
		if (!do_plots) then plot, x, y, xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle, color = color_line
	endfor
	endif
	if (!do_plots) then plot, [0], [0], /nodata, xrange = xrange, yrange = yrange, xstyle =  1, ystyle = 1, color = 1, title = title1
	if (!do_plots) then multiplot, /doxaxis, /doyaxis

	psym3 = 5
	color_error = 200

	if (match_count gt 0) then begin

	xrange = mag_range
	yrange = [-5.0, 5.0]
	title1 = "mag error vs. mag"
	if (!do_plots) then plot, [matched_report.mag1], [matched_report.dmag], $
	psym=psym3, color = color_error,  xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle
	if (!do_plots) then plot, [0], [0], /nodata, xrange = xrange, yrange = yrange, xstyle =  1, ystyle = 1, color = 1, title = title1
	if (!do_plots) then multiplot, /doxaxis, /doyaxis

	xrange = r_range
	yrange = [-5.0, 5.0]
	title1 = "mag error vs. r"
	if (!do_plots) then plot, [matched_report.r1], [matched_report.dmag], $
	psym=psym3, color = color_error,  xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle
	if (!do_plots) then plot, [0], [0], /nodata, xrange = xrange, yrange = yrange, xstyle =  1, ystyle = 1, color = 1, title = title1
	if (!do_plots) then multiplot, /doxaxis, /doyaxis

	xrange = [-5.0, 5.0]
	yrange = [-3.0, 3.0]
	title1 = "mag error - r error distribution"
	if (!do_plots) then plot, [matched_report.dmag], [matched_report.dr], $
	psym=psym3, color = color_error,  xrange = xrange, yrange = yrange, xstyle = xstyle, ystyle = ystyle
	if (!do_plots) then plot, [0], [0], /nodata, xrange = xrange, yrange = yrange, xstyle =  1, ystyle = 1, color = 1, title = title1
	if (!do_plots) then multiplot, /doxaxis, /doyaxis

	endif

	;; closing graphic device
	if (!do_plots) then device, /close ; close the file
	if (!do_plots) then set_plot, olddevice ; go back to the graphics device
	if (!do_plots) then begin
		command = "epstopdf "+epsfile
		spawn, command
		extpos = STRPOS(epsfile, '.', /REVERSE_SEARCH)
		jpegfile = STRMID(epsfile, 0, extpos)+".jpg"
		command = "convert " + epsfile + " " + jpegfile
		spawn, command
	endif
endif

;; now outputting reports to fits file
extpos = STRPOS(epsfile, '.', /REVERSE_SEARCH)
fitsfile=STRMID(epsfile, 0, extpos)+".fits"
FILE_DELETE, fitsfile, /ALLOW_NONEXISTENT
print, name, "writing matched report"
if (match_count gt 0) then MWRFITS, matched_report, fitsfile, /silent
if (unmatch_count gt 0) then MWRFITS, unmatched_report, fitsfile, /silent
print, "w", format='(A, $)'
MWRFITS, greport, fitsfile, status = status, /silent
if (status ne 0) then print, "e", format='(A, $)'

;; debug
if (1 eq 0 and n_elements(greport) gt 5) then begin
print
print, name, ": SN1 - (min, max, median, mean, std-dev) = ", min(greport.SN1), max(greport.SN1), median(greport.SN1), mean(greport.SN1), sqrt(variance(greport.SN1))
print, name, ": SN-HALO1 - (min, max, median, mean, std-dev) = ", min(greport.SN_halo1), max(greport.SN_halo1), median(greport.SN_halo1), mean(greport.SN_halo1), sqrt(variance(greport.SN_halo1))
endif

return
end


pro run_draw_matched_tuples_from_mle, $
	roots = roots, band = band, root_titles = root_titles, $
	run = run, rerun = rerun, camcol = camcol, outdir = outdir, $
	star = star, galaxy = galaxy, LRG = LRG, flagLRG = flagLRG, $
	locmap = locmap, mag_thresh = mag_thresh, suffix = suffix, status = status, $
	sample_report = sample_report, $
	distance_match = distance_match, data = data

name = "run_draw_matched_tuples_from_mle"

if (n_elements(band) eq 0) then band = ["r"]
status = 0
dir = construct_fpObjc_dir(root = roots[0], run = run, rerun = rerun, camcol = camcol, /mle, status = status)
if (status ne 0) then begin
	print, name, ": ERROR - could not get fpObjc dir for (root, run, rerun, camcol) = ", root1, run, rerun, camcol
	return
endif

pattern_spec = "gpObjc*"+band[0]+"*fit"
count = 0
file_list = FILE_SEARCH(dir, pattern_spec, count = count)

rundir = strtrim(string(run), 2)
camcolstr = strtrim(string(camcol), 2)
outdir_for_fields = outdir+"/"+rerun+"/"+rundir+"/"+camcolstr
if (file_test(outdir_for_fields, /directory) eq 0) then file_mkdir, outdir_for_fields

;; print, name, ": file count = ", count
nfield = count
mfield = min([nfield, 100000L])
for ifield = 0L, mfield - 1L, 1L do begin

	file = file_list[ifield]
	;; "fpObjc-004797-1-0075.fit"
	pos = strpos(file, "fpObj")
	fieldstr = strmid(file, pos + 16, 4)	
	if (pos eq -1) then begin
		pos = strpos(file, "gpObj")
		fieldstr = strmid(file, pos + 17, 4)	
	endif
	field = -1L
	reads, fieldstr, field,	 format = "(I4)"
	status = 0
	field_draw_matched_tuples_from_mle, roots = roots, band = band, root_titles = root_titles, $
	run = run, rerun = rerun, field = field, camcol = camcol, outdir = outdir_for_fields, $
	star = keyword_set(star), galaxy = keyword_set(galaxy), LRG = keyword_set(LRG), flagLRG = flagLRG, $
	locmap = locmap, mag_thresh = mag_thresh, suffix = suffix, status = status, sample_report = sample_report, $
	distance_match = distance_match, data = data

	if (status ne 0) then begin
		print, name, ": ERROR - could not create goodness plots for field = ", field
		return
	endif

	if (status eq 0) then begin
		print, ".", format='(A, $)' 
	endif else  begin
	      	print, "e", format='(A, $)' 
	endelse
	
endfor	

return
end

function run_draw_matched_pairs_for_run, root1 = root1, root2 = root2, root3 = root3, $
	run = run, rerun = rerun, camcol = camcol, band = band, $
	sas1 = sas1, sas2 = sas2, sas3 = sas3, sim1 = sim1, sim2 = sim2, sim3 = sim3, $
	mle1 = mle1, mle2 = mle2, mle3 = mle3, fake_cd2 = fake_cd2, data = data, $
	boxplots = boxplots, star = star, galaxy = galaxy, LRG = LRG, flagLRG = flagLRG, $
	locmap = locmap, mag_thresh = mag_thresh, distance_match = distance_match, $
	 prefix = prefix, suffix = suffix, status = status, sample_report = sample_report
name = "draw_matched_pairs_for_run"

status = -1
if (n_elements(root1) eq 0) then return, bad_report
if (n_elements(root2) eq 0) then return, bad_report
if (n_elements(run) eq 0) then return, bad_report
if (n_elements(camcol) eq 0) then return, bad_report
if (n_elements(prefix) eq 0) then prefix = ""
if (n_elements(suffix) eq 0) then suffix = ""
if (n_elements(band) eq 0) then band = "r"
status = 0
dir = construct_fpObjc_dir(root = root1, run = run, rerun = rerun, camcol = camcol, sas = keyword_set(sas1), sim = keyword_set(sim1), mle = keyword_set(mle1), status = status)
if (status ne 0) then begin
	print, name, ": ERROR - could not get fpObjc dir for (root, run, rerun, camcol) = ", root1, run, rerun, camcol
	return, bad_report
endif

pattern_spec = "fpObjc*fit"
count = 0
file_list = FILE_SEARCH(dir, pattern_spec, count = count)

if (count eq 0) then begin
pattern_spec = "gpObjc*"+band+"*fit"
count = 0
file_list = FILE_SEARCH(dir, pattern_spec, count = count)
endif

;; print, name, ": count = ", count
nfield = count
report_all = sample_report
report_exists = 0L
mfield = min([nfield, 100000L])
for ifield = 0L, mfield - 1L, 1L do begin

	file = file_list[ifield]
	;; "fpObjc-004797-1-0075.fit"
	pos = strpos(file, "fpObj")
	fieldstr = strmid(file, pos + 16, 4)	
	if (pos eq -1) then begin
		pos = strpos(file, "gpObj")
		fieldstr = strmid(file, pos + 17, 4)	
	endif
	field = -1L
	reads, fieldstr, field,	 format = "(I4)"
	status = 0
	report = field_draw_matched_pairs_for_field(root1 = root1, root2 = root2, root3 = root3, $
		run = run, rerun = rerun, field = field, camcol = camcol, $
		sas1 = keyword_set(sas1), sas2 = keyword_set(sas2), sas3 = keyword_set(sas3), $
		sim1 = keyword_set(sim1), sim2 = keyword_set(sim2), sim3 = keyword_set(sim3), $
		mle1 = keyword_set(mle1), mle2 = keyword_set(mle2), mle3 = keyword_set(mle3), $
		fake_cd2 = keyword_set(fake_cd2), $
		boxplots = keyword_set(boxplots), $
		star = keyword_set(star), galaxy = keyword_set(galaxy), LRG = keyword_set(LRG), flagLRG = flagLRG, locmap = keyword_set(locmap), $
		mag_thresh = mag_thresh, distance_match = distance_match, $
		suffix = suffix, status = status, sample_report = sample_report, data = keyword_set(data))
	if (status eq 0) then begin
		print, ".", format='(A, $)' 
	endif ;; else  begin
	      ;;	print, "e", format='(A, $)' 
	;; endelse
	
	if (report_exists eq 0) then begin
		report_all = [report]
		report_exists = 1
	endif else begin
		report_all = [report, report_all]
	endelse
	;; print, name, ": report - ", n_elements(report)
	;; print, name, ": report_all -", n_elements(report_all)
	;; print, name, ": ifield, mfield = ", ifield, mfield
endfor	

;; print, name, ": report_all", n_elements(report_all)
status = 0
file_suffix = prefix+"acc-fields-"+suffix
report_dest = create_destination_filename(root = root2, run = run, rerun = rerun, camcol = camcol, suffix = file_suffix, $
	star = keyword_set(star), galaxy = keyword_set(galaxy), LRG = keyword_set(LRG), mle = keyword_set(mle2), flagLRG = flagLRG, $
	status = status)
if (status ne 0) then begin
	print, name, ": ERROR - could not make the report destination filename - no report to be generated"
	return, report_all
endif
if (n_elements(report_all) gt 0) then begin
	draw_report, filename = report_dest, report = report_all, star = keyword_set(star), galaxy = keyword_set(galaxy), lrg = keyword_set(lrg), flagLRG = flagLRG, locmap = keyword_set(locmap), boxplots = keyword_set(boxplots), status = status
	verbose = 0
	if (status ne 0 and verbose gt 0) then begin
		;; print, name, ": ERROR - could not output report in the file ", report_dest
	endif 
endif else begin
		print, "O", format='(A, $)'
endelse

;; print, name, ": report_all - ", n_elements(report_all)
return, report_all
end

function define_report

sample = CREATE_STRUCT("field", -1L, "run", -1L, "camcol", -1L, "band", -1L, "bname", "unknown", $
	"id", -1L, "sky1", !value_is_bad, "sky2", !value_is_bad, "sky3", !value_is_bad, $
	"I1", !value_is_bad, "I_halo1", !value_is_bad, "I_sersic1", !value_is_bad, "I_sersic_inner1", !value_is_bad, "I_sersic_outer1", !value_is_bad,  $
	"IErr1", !value_is_bad, "IErr_halo1", !value_is_bad, "IErr_sersic1", !value_is_bad, "IErr_sersic_inner1", !value_is_bad, "IErr_sersic_outer1", !value_is_bad,$
	"z1", !value_is_bad, "z_halo1", !value_is_bad, "z_sersic1", !value_is_bad, $
	"counts1", !value_is_bad, "counts_halo1", !value_is_bad, "counts_sersic1", !value_is_bad, $
	"counts_sersic_inner1", !value_is_bad, "counts_sersic_outer1", !value_is_bad, $
	 "counts_csersic_inner1", !value_is_bad, "counts_csersic_outer1", !value_is_bad, $ 
	"counts_total1", !value_is_bad, $
	"mag1", !VALUE_IS_BAD, "r1",  !VALUE_IS_BAD, "a1", !value_is_bad, "b1", !value_is_bad, "ab1", !value_is_bad, "e1", !value_is_bad, "phi1", !value_is_bad, $
	"mu1", !value_is_bad, "SN1", !value_is_bad, "fracPSF1", !value_is_bad, $
	"mag_halo1", !VALUE_IS_BAD, "r_halo1",  !VALUE_IS_BAD, "a_halo1", !value_is_bad, "b_halo1", !value_is_bad, "ab_halo1", !value_is_bad, "e_halo1", !value_is_bad, "phi_halo1", !value_is_bad, $
	"mu_halo1", !value_is_bad, "SN_halo1", !value_is_bad, "fracPSF_halo1", !value_is_bad, $
	"mag_sersic1", !VALUE_IS_BAD, "r_sersic1",  !VALUE_IS_BAD, "a_sersic1", !value_is_bad, "b_sersic1", !value_is_bad, "ab_sersic1", !value_is_bad, $
	"e_sersic1", !value_is_bad, "phi_sersic1", !value_is_bad, "n_sersic1", !value_is_bad,$
	"mu_sersic1", !value_is_bad, "SN_sersic1", !value_is_bad, $
	"mag_sersic_inner1", !VALUE_IS_BAD, "r_sersic_inner1",  !VALUE_IS_BAD, "a_sersic_inner1", !value_is_bad, "b_sersic_inner1", !value_is_bad, $
	"ab_sersic_inner1", !value_is_bad, "e_sersic_inner1", !value_is_bad, "phi_sersic_inner1", !value_is_bad, "n_sersic_inner1", !value_is_bad,$
	"mu_sersic_inner1", !value_is_bad, "SN_sersic_inner1", !value_is_bad, $
	"mag_sersic_outer1", !VALUE_IS_BAD, "r_sersic_outer1",  !VALUE_IS_BAD, "a_sersic_outer1", !value_is_bad, "b_sersic_outer1", !value_is_bad, $
	"ab_sersic_outer1", !value_is_bad, "e_sersic_outer1", !value_is_bad, "phi_sersic_outer1", !value_is_bad, "n_sersic_outer1", !value_is_bad,$
	"mu_sersic_outer1", !value_is_bad, "SN_sersic_outer1", !value_is_bad, $
	"mag_csersic_inner1", !VALUE_IS_BAD, "r_csersic_inner1",  !VALUE_IS_BAD, "a_csersic_inner1", !value_is_bad, "b_csersic_inner1", !value_is_bad, $
	"ab_csersic_inner1", !value_is_bad, "e_csersic_inner1", !value_is_bad, "phi_csersic_inner1", !value_is_bad, "n_csersic_inner1", !value_is_bad,$
	"mu_csersic_inner1", !value_is_bad, "SN_csersic_inner1", !value_is_bad, $
	"mag_csersic_outer1", !VALUE_IS_BAD, "r_csersic_outer1",  !VALUE_IS_BAD, "a_csersic_outer1", !value_is_bad, "b_csersic_outer1", !value_is_bad, $
	"ab_csersic_outer1", !value_is_bad, "e_csersic_outer1", !value_is_bad, "phi_csersic_outer1", !value_is_bad, "n_csersic_outer1", !value_is_bad,$
	"mu_csersic_outer1", !value_is_bad, "SN_csersic_outer1", !value_is_bad, $
	"alpha_csersic1", !value_is_bad, $
	"I2", !value_is_bad, "I_halo2", !value_is_bad, "I_sersic2", !value_is_bad, "I_sersic_inner2", !value_is_bad, "I_sersic_outer2", !value_is_bad, $
	 "I_csersic_inner2", !value_is_bad, "I_csersic_outer2", !value_is_bad, $
	"IErr2", !value_is_bad, "IErr_halo2", !value_is_bad, "IErr_sersic2", !value_is_bad, "IErr_sersic_inner2", !value_is_bad, "IErr_sersic_outer2", !value_is_bad,$
	"IErr_csersic_inner2", !value_is_bad, "IErr_csersic_outer2", !value_is_bad,$
	 "z2", !value_is_bad, "z_halo2", !value_is_bad, "z_sersic2", !value_is_bad, $
	"z_sersic_inner2", !value_is_bad, "z_sersic_outer2", !value_is_bad, $
	"z_csersic_inner2", !value_is_bad, "z_csersic_outer2", !value_is_bad, $
	"mag2", !VALUE_IS_BAD, "r2", !VALUE_IS_BAD, "a2", !value_is_bad, "b2", !value_is_bad, "ab2", !value_is_bad, "e2", !value_is_bad, "phi2", !value_is_bad, $
	"mu2", !value_is_bad, "SN2", !value_is_bad, "fracPSF2", !value_is_bad, $
	"mag_halo2", !VALUE_IS_BAD, "r_halo2", !VALUE_IS_BAD, "a_halo2", !value_is_bad, "b_halo2", !value_is_bad, "ab_halo2", !value_is_bad, "e_halo2", !value_is_bad, "phi_halo2", !value_is_bad, $
	"mu_halo2", !value_is_bad, "SN_halo2", !value_is_bad, "fracPSF_halo2", !value_is_bad, $
	"mag_sersic2", !VALUE_IS_BAD, "r_sersic2", !VALUE_IS_BAD, "a_sersic2", !value_is_bad, "b_sersic2", !value_is_bad, $
	"ab_sersic2", !value_is_bad, "e_sersic2", !value_is_bad, "phi_sersic2", !value_is_bad, "n_sersic2", !value_is_bad, $
	"mu_sersic2", !value_is_bad, "SN_sersic2", !value_is_bad, "fracPSF_sersic2", !value_is_bad, $
	"mag_sersic_inner2", !VALUE_IS_BAD, "r_sersic_inner2", !VALUE_IS_BAD, "a_sersic_inner2", !value_is_bad, "b_sersic_inner2", !value_is_bad, $
	"ab_sersic_inner2", !value_is_bad, "e_sersic_inner2", !value_is_bad, "phi_sersic_inner2", !value_is_bad, "n_sersic_inner2", !value_is_bad, $
	"mu_sersic_inner2", !value_is_bad, "SN_sersic_inner2", !value_is_bad, "fracPSF_sersic_inner2", !value_is_bad, $
	"mag_sersic_outer2", !VALUE_IS_BAD, "r_sersic_outer2", !VALUE_IS_BAD, "a_sersic_outer2", !value_is_bad, "b_sersic_outer2", !value_is_bad, $
	"ab_sersic_outer2", !value_is_bad, "e_sersic_outer2", !value_is_bad, "phi_sersic_outer2", !value_is_bad, "n_sersic_outer2", !value_is_bad, $
	"mu_sersic_outer2", !value_is_bad, "SN_sersic_outer2", !value_is_bad, "fracPSF_sersic_outer2", !value_is_bad, $
	"mag_csersic_inner2", !VALUE_IS_BAD, "r_csersic_inner2", !VALUE_IS_BAD, "a_csersic_inner2", !value_is_bad, "b_csersic_inner2", !value_is_bad, $
	"ab_csersic_inner2", !value_is_bad, "e_csersic_inner2", !value_is_bad, "phi_csersic_inner2", !value_is_bad, "n_csersic_inner2", !value_is_bad, $
	"mu_csersic_inner2", !value_is_bad, "SN_csersic_inner2", !value_is_bad, "fracPSF_csersic_inner2", !value_is_bad, $
	"mag_csersic_outer2", !VALUE_IS_BAD, "r_csersic_outer2", !VALUE_IS_BAD, "a_csersic_outer2", !value_is_bad, "b_csersic_outer2", !value_is_bad, $
	"ab_csersic_outer2", !value_is_bad, "e_csersic_outer2", !value_is_bad, "phi_csersic_outer2", !value_is_bad, "n_csersic_outer2", !value_is_bad, $
	"mu_csersic_outer2", !value_is_bad, "SN_csersic_outer2", !value_is_bad, "fracPSF_csersic_outer2", !value_is_bad, $	
	"alpha_csersic2", !value_is_bad, $
	"counts2", !value_is_bad, "counts_halo2", !value_is_bad, "counts_sersic2", !value_is_bad, $
	"counts_sersic_inner2", !value_is_bad, "counts_sersic_outer2", !value_is_bad, "counts_csersic_inner2", !value_is_bad, "counts_csersic_outer2", !value_is_bad, $
	"counts_total2", !value_is_bad, $
	"mag_total1", !VALUE_IS_BAD, "mag_total2", !VALUE_IS_BAD, $
	"dr", !VALUE_IS_BAD, "da", !value_is_bad, "db", !value_is_bad, "dab", !value_is_bad, "de", !value_is_bad, "dmag", !VALUE_IS_BAD, "dmu", !value_is_bad, $
	"dr_halo", !VALUE_IS_BAD, "da_halo", !value_is_bad, "db_halo", !value_is_bad, "dab_halo", !value_is_bad, "de_halo", !value_is_bad, "dmag_halo", !VALUE_IS_BAD, "dmu_halo", !value_is_bad, $
	"dmag_total", !VALUE_IS_BAD, "dphi", !value_is_bad, "dphi_halo", !value_is_bad, $ 
	"x1", !VALUE_IS_BAD, "y1", !VALUE_IS_BAD, "x2", !VALUE_IS_BAD, "y2", !VALUE_IS_BAD, $
	"delta", !VALUE_IS_BAD, "distance", !VALUE_IS_BAD, $
	"ncd", !VALUE_IS_BAD, $
	"nstari", !VALUE_IS_BAD, "mag_star1i", !VALUE_IS_BAD, "mag_star2i", !VALUE_IS_BAD, "mag_star3i", !VALUE_IS_BAD, $
	"delta_star1i", !VALUE_IS_BAD, "delta_star2i", !VALUE_IS_BAD, "delta_star3i", !VALUE_IS_BAD, $
	"nstarf", !VALUE_IS_BAD, "mag_star1f", !VALUE_IS_BAD, "mag_star2f", !VALUE_IS_BAD, "mag_star3f", !VALUE_IS_BAD, $
	"delta_star1f", !VALUE_IS_BAD, "delta_star2f", !VALUE_IS_BAD, "delta_star3f", !VALUE_IS_BAD, $
	"ngalaxyi", !VALUE_IS_BAD, "mag_galaxy1i", !VALUE_IS_BAD, "mag_galaxy2i", !VALUE_IS_BAD, "mag_galaxy3i", !VALUE_IS_BAD, $
	"delta_galaxy1i", !VALUE_IS_BAD, "delta_galaxy2i", !VALUE_IS_BAD, "delta_galaxy3i", !VALUE_IS_BAD, $
	"ngalaxyf", !VALUE_IS_BAD, "mag_galaxy1f", !VALUE_IS_BAD, "mag_galaxy2f", !VALUE_IS_BAD, "mag_galaxy3f", !VALUE_IS_BAD, $	
	"delta_galaxy1f", !VALUE_IS_BAD, "delta_galaxy2f", !VALUE_IS_BAD, "delta_galaxy3f", !VALUE_IS_BAD, $
	"I3", !value_is_bad, "I_halo3", !value_is_bad, "IErr3", !value_is_bad, "IErr_halo3", !value_is_bad, "z3", !value_is_bad, "z_halo3", !value_is_bad, $
	"mag3", !VALUE_IS_BAD, "r3", !VALUE_IS_BAD, "a3", !value_is_bad, "b3", !value_is_bad, "ab3", !value_is_bad, "e3", !value_is_bad, "phi3", !value_is_bad, $
	"mu3", !value_is_bad, "SN3", !value_is_bad, "fracPSF3", !value_is_bad, $
	"mag_halo3", !VALUE_IS_BAD, "r_halo3", !VALUE_IS_BAD, "a_halo3", !value_is_bad, "b_halo3", !value_is_bad, "ab_halo3", !value_is_bad, "e_halo3", !value_is_bad, "phi_halo3", !value_is_bad, $
	"mu_halo3", !value_is_bad, "SN_halo3", !value_is_bad, "fracPSF_halo3", !value_is_bad, $
	"mag_total3", !VALUE_IS_BAD, $
	"counts3", !value_is_bad, "counts_halo3", !value_is_bad, "counts_total3", !value_is_bad, $
	"dr3", !VALUE_IS_BAD, "da3", !value_is_bad, "db3", !value_is_bad, "dab3", !value_is_bad, "de3", !value_is_bad, "dmag3", !VALUE_IS_BAD, "dmu3", !value_is_bad, $
	"dr_halo3", !VALUE_IS_BAD, "da_halo3", !value_is_bad, "db_halo3", !value_is_bad, "dab_halo3", !value_is_bad, "de_halo3", !value_is_bad, "dmag_halo3", !VALUE_IS_BAD, "dmu_halo3", !value_is_bad, $
	"dmag_total3", !VALUE_IS_BAD, "dphi3", !value_is_bad, "dphi_halo3", !value_is_bad, $ 
	"x3", !VALUE_IS_BAD, "y3", !VALUE_IS_BAD, $
	"delta3", !VALUE_IS_BAD, "distance3", !VALUE_IS_BAD, $
	"chisq1", !VALUE_IS_BAD, "chisq_float1", !VALUE_IS_BAD, "dchisq2", !VALUE_IS_BAD, "chisq2", !VALUE_IS_BAD, "npix2", !VALUE_IS_BAD, "nstep2", !VALUE_IS_BAD, "cost2", !VALUE_IS_BAD, $
	"dchisq3", !VALUE_IS_BAD, "chisq3", !VALUE_IS_BAD, "nstep3", !VALUE_IS_BAD, "cost3", !value_is_bad, $
	"chisq1_nu", !value_is_bad, "chisqfl1_nu", !value_is_bad, "chisq2_nu", !value_is_bad, "chisq3_nu", !value_is_bad, $
	"gaussdn1", !value_is_bad, "gaussdn2", !value_is_bad, "gaussdn3", !value_is_bad, $
	"match", -1L, "cdmatch", 0L)

return, sample
end

function define_goodness_report

sample = CREATE_STRUCT( $
	"n_circle", 0L, "radius_circle", dblarr(!max_n_circle), $
	"goodness_circle", dblarr(!max_n_circle), $
	"goodness_circle_reduced",  dblarr(!max_n_circle), "npix_circle",  dblarr(!max_n_circle), $
	"n_ring",  0L, "radius_ring",  dblarr(!max_n_circle),$
	"goodness_ring", dblarr(!max_n_circle), $
	"goodness_ring_reduced",  dblarr(!max_n_circle), "npix_ring",  dblarr(!max_n_circle), $
	"goodness_type",  "chi-squared-reduced", "sdss_mag", 0.0, "sdss_petrorad", 0.0, $
	"rowc", 0.0, "colc", 0.0, $
	"mle_mag", 0.0, $
	"comment", strarr(!max_n_circle), "n_comment", 0L)

return, sample
end

pro define_globals, rejection_distance = rejection_distance, rejection_chisq = rejection_chisq

DEFSYSV, '!MY_XSIZE', 42.0
DEFSYSV, '!MY_YSIZE', 20.3
DEFSYSV, '!MY_XYRATIO', 1.23

DEFSYSV, '!MY_XGAP', 0.06
DEFSYSV, '!MY_YGAP', 0.06
DEFSYSV, '!MY_FONT_SIZE', 11
DEFSYSV, '!VALUE_IS_BAD', -999.0D0
DEFSYSV, '!DEV_RE_SMALL', 4.0E-20
DEFSYSV, '!EXP_RE_SMALL', 4.0E-20
DEFSYSV, '!DEV_RE_SMALL2', 4.0E-20
DEFSYSV, '!EXP_RE_SMALL2', 4.0E-20
DEFSYSV, '!PIXELSIZE', 0.396, 1
DEFSYSV, '!DEFAULT_C_LRG', 0.7, 1
DEFSYSV, '!U_BAND', 0
DEFSYSV, '!G_BAND', 1
DEFSYSV, '!R_BAND', 2
DEFSYSV, '!I_BAND', 3
DEFSYSV, '!Z_BAND', 4
DEFSYSV, '!SHAKY_N', 1000000L
DEFSYSV, '!verbose_deep', 0
DEFSYSV, '!verbose', 0
DEFSYSV, '!K_DEV', 7.67E0
DEFSYSV, '!degree_of_freedom', 2048L * 1489L
DEFSYSV, '!default_gauss_dn', 15.0
;; SDSS object types
defsysv, '!unknown_type', 0
defsysv, '!star_type', 6
defsysv, '!galaxy_type', 3
defsysv, '!lrg_type', 9
defsysv, '!lrg_galaxy_type', 12


if (n_elements(rejection_distance) eq 0) then rejection_distance = 3.0
if (rejection_distance le 0.0) then rejection_distance = 3.0
DEFSYSV, '!REJ_D', rejection_distance

if (n_elements(rejection_chisq) eq 0) then rejection_chisq = 5.0D10
if (rejection_chisq le 0.0) then rejection_chisq = 5.0D10
DEFSYSV, '!REJ_CHISQ', rejection_chisq

defsysv, '!graphical_output', 1
defsysv, '!do_plots', 0

defsysv, '!max_nobjc_in_field', 10
defsysv, '!max_n_circle', 10

return
end


pro runlist_draw_matched_tuples_from_mle, $
	roots = roots, band = band, root_titles = root_titles, $
	run_list = run_list, rerun = rerun, camcol_list = camcol_list, outdir = outdir, $
	star = star, galaxy = galaxy, LRG = LRG, flagLRG = flagLRG, $
	locmap = locmap, mag_thresh = mag_thresh, suffix = suffix, status = status, sample_report = sample_report, $
	distance_match = distance_match, data = data

name = "runlist_draw_matched_tuples_from_mle"

status = 0
if (n_elements(camcol_list) eq 0) then camcol_list = [1, 2, 3, 4, 5, 6]
if (n_elements(run_list) eq 0) then begin
run_list=[4797, 2700, 2703, 2708, 2709, 2960, 3565, 3434, 3437, 2968, 4207, 1742, 4247, 4252, 4253, 4263, 4288, 1863, 1887, 2570, 2578, 2579, 109, 125, 211, 240, 241, 250, 251, 256, 259, 273, 287, 297, 307, 94, 5036, 5042, 5052, 2873, 21, 24, 1006, 1009, 1013, 1033, 1040, 1055, 1056, 1057, 4153, 4157, 4158, 4184, 4187, 4188, 4191, 4192, 2336, 2886, 2955, 3325, 3354, 3355, 3360, 3362, 3368, 2385, 2506, 2728, 4849, 4858, 4868, 4874, 4894, 4895, 4899, 4905, 4927, 4930, 4933, 4948, 2854, 2855, 2856, 2861, 2662, 3256, 3313, 3322, 4073, 4198, 4203, 3384, 3388, 3427, 3430, 4128, 4136, 4145, 2738, 2768, 2820, 1752, 1755, 1894, 2583, 2585, 2589, 2591, 2649, 2650, 2659, 2677, 3438, 3460, 3458, 3461, 3465, 7674, 7183]
run_list = [4797]
run_list=[4797, 2700, 2703, 2708, 2709, 2960, 3565, 3434, 3437, 2968, 4207, 1742]
endif

if (n_elements(rerun) eq 0) then rerun = '301'

nrun = n_elements(run_list)
ncamcol = n_elements(camcol_list)

print, ""

status = 0
for irun = 0, nrun - 1, 1L do begin
for icamcol = 0, ncamcol - 1, 1L do begin

	print, ">", format='(A, $)'
	run =  run_list[irun]
	camcol = camcol_list[icamcol]
	run_draw_matched_tuples_from_mle, $
	roots = roots, band = band, root_titles = root_titles, $
	run = run, rerun = rerun, camcol = camcol, outdir = outdir, $
	star = keyword_set(star), galaxy = keyword_set(galaxy), LRG = keyword_set(LRG), flagLRG = flagLRG, $
	locmap = locmap, mag_thresh = mag_thresh, suffix = suffix, status = status, sample_report = sample_report, $
	distance_match = distance_match, data = data

	if (status ne 0) then begin
		print, name, ": ERROR - could not draw goodness of fit for run = ", run, ", camcol = ", camcol
	endif
endfor
endfor

return
end


function draw_matched_pairs_for_run_list, root1 = root1, root2 = root2, root3 = root3, $
	run_list = run_list, rerun = rerun, camcol_list = camcol_list, $
	sas1 = sas1, sas2 = sas2, sas3 = sas3, sim1 = sim1, sim2 = sim2, sim3 = sim3, $
	mle1 = mle1, mle2 = mle2, mle3 = mle3, $
	fake_cd2 = fake_cd2, data = data, $
	boxplots = boxplots, star = star, galaxy = galaxy, LRG = LRG, flagLRG = flagLRG, locmap = locmap, $
	mag_thresh = mag_thresh, distance_match = distance_match, $
	prefix = prefix, suffix = suffix, rejection_distance = rejection_distance, rejection_chisq = rejection_chisq, $
	create_chisq_contour = create_chisq_contour, status = status
name = "draw_matched_pairs_for_run_list"

status = -1
if (n_elements(root1) eq 0) then return, [-1L]
if (n_elements(root2) eq 0) then return, [-1L]
if (n_elements(suffix) eq 0) then suffix = ""
if (n_elements(prefix) eq 0) then prefix = ""

status = 0
if (n_elements(camcol_list) eq 0) then camcol_list = [1, 2, 3, 4, 5, 6]
if (n_elements(run_list) eq 0) then begin
run_list=[4797, 2700, 2703, 2708, 2709, 2960, 3565, 3434, 3437, 2968, 4207, 1742, 4247, 4252, 4253, 4263, 4288, 1863, 1887, 2570, 2578, 2579, 109, 125, 211, 240, 241, 250, 251, 256, 259, 273, 287, 297, 307, 94, 5036, 5042, 5052, 2873, 21, 24, 1006, 1009, 1013, 1033, 1040, 1055, 1056, 1057, 4153, 4157, 4158, 4184, 4187, 4188, 4191, 4192, 2336, 2886, 2955, 3325, 3354, 3355, 3360, 3362, 3368, 2385, 2506, 2728, 4849, 4858, 4868, 4874, 4894, 4895, 4899, 4905, 4927, 4930, 4933, 4948, 2854, 2855, 2856, 2861, 2662, 3256, 3313, 3322, 4073, 4198, 4203, 3384, 3388, 3427, 3430, 4128, 4136, 4145, 2738, 2768, 2820, 1752, 1755, 1894, 2583, 2585, 2589, 2591, 2649, 2650, 2659, 2677, 3438, 3460, 3458, 3461, 3465, 7674, 7183]
run_list = [4797]
run_list=[4797, 2700, 2703, 2708, 2709, 2960, 3565, 3434, 3437, 2968, 4207, 1742]
endif

if (n_elements(rerun) eq 0) then rerun = '301'

nrun = n_elements(run_list)
ncamcol = n_elements(camcol_list)

define_globals, rejection_distance = rejection_distance, rejection_chisq = rejection_chisq
bad_report = define_report()
sample_report = bad_report
report_all = bad_report
report_all_exists = 0L
for irun = 0, nrun - 1L, 1L do begin
	report_camcol_exists = 0L
	for icamcol = 0, ncamcol - 1L, 1L do begin
		run = run_list[irun]
		camcol = camcol_list[icamcol]
		print, ">", format='(A, $)'
		status = 0
		report = run_draw_matched_pairs_for_run(root1 = root1, root2 = root2, root3 = root3, $
		run = run, rerun = rerun, camcol = camcol, $
		sas1 = keyword_set(sas1), sas2 = keyword_set(sas2), sas3 = keyword_set(sas3), $
		sim1 = keyword_set(sim1), sim2 = keyword_set(sim2), sim3 = keyword_set(sim3), $
		mle1 = keyword_set(mle1), mle2 = keyword_set(mle2), mle3 = keyword_set(mle3), $
		fake_cd2 = keyword_set(fake_cd2), data = keyword_set(data), $
		boxplots = keyword_set(boxplots), $
		star = keyword_set(star), galaxy = keyword_set(galaxy), LRG = keyword_set(LRG), flagLRG = flagLRG, locmap = keyword_set(locmap), $
		mag_thresh = mag_thresh, distance_match = distance_match, $
		prefix = prefix, suffix = suffix, status = status, sample_report = sample_report)
		if (status ne 0) then print, "E", format='(A, $)'
		if (report_camcol_exists eq 0) then begin
			report_camcol = [report]
			report_camcol_exists = 1L
		endif else begin
			report_camcol = [report, report_camcol]
		endelse
	endfor

	status = 0
	file_suffix = prefix+"acc-camcols-acc-fields-"+suffix
	report_dest = create_destination_filename(root = root2, run = run, rerun = rerun, suffix = file_suffix, $
	star = keyword_set(star), galaxy = keyword_set(galaxy), LRG = keyword_set(LRG), mle = keyword_set(mle2), flagLRG = flagLRG, $
	status = status)
	if (status ne 0) then begin
	print, name, ": ERROR - could not make the report destination filename - no report to be generated"
	return, report_all
	endif
	if (n_elements(report_camcol) gt 0) then begin
		draw_report, filename = report_dest, report = report_camcol, star = keyword_set(star), galaxy = keyword_set(galaxy), lrg = keyword_set(lrg), flagLRG = flagLRG, locmap = keyword_set(locmap), boxplots = keyword_set(boxplots), status = status
		verbose = 0
		if (status ne 0 and verbose gt 0) then begin
		print, name, ": ERROR - could not output report in the file ", report_dest
		endif
	endif else begin
		print, "O", format='(A, $)'
	endelse

	if (report_all_exists eq 0L) then begin
		report_all = [report_camcol]
		report_all_exists = 1L
	endif else begin
		report_all = [report_camcol, report_all]
	endelse

endfor

status = 0
file_suffix = prefix + "acc-runs-acc-camcols-acc-fields-" + suffix
report_dest = create_destination_filename(root = root2, rerun = rerun, suffix = file_suffix, $
	star = keyword_set(star), galaxy = keyword_set(galaxy), LRG = keyword_set(LRG), mle = keyword_set(mle2), flagLRG = flagLRG, $
	status = status)
if (status ne 0) then begin
	print, name, ": ERROR - could not make the report destination filename - no report to be generated"
	return, report_all
endif
file_suffix = prefix + "CHISQ-acc-runs-acc-camcols-acc-fields-" + suffix
chisq_contour_file = create_destination_filename(root = root2, rerun = rerun, suffix = file_suffix, $
	star = keyword_set(star), galaxy = keyword_set(galaxy), LRG = keyword_set(LRG), mle = keyword_set(mle2), flagLRG = flagLRG, $
	status = status)
if (status ne 0) then begin
	print, name, ": ERROR - could not make the chisq contour filename - no report to be generated"
	return, report_all
endif
file_suffix = prefix + "COST-acc-runs-acc-camcols-acc-fields-" + suffix
cost_contour_file = create_destination_filename(root = root2, rerun = rerun, suffix = file_suffix, $
	star = keyword_set(star), galaxy = keyword_set(galaxy), LRG = keyword_set(LRG), mle = keyword_set(mle2), flagLRG = flagLRG, $
	status = status)
if (status ne 0) then begin
	print, name, ": ERROR - could not make the chisq contour filename - no report to be generated"
	return, report_all
endif


if (n_elements(report_all) gt 0) then begin
	draw_report, filename = report_dest, report = report_all,  star = keyword_set(star), galaxy = keyword_set(galaxy), lrg = keyword_set(lrg), flagLRG = flagLRG, locmap = keyword_set(locmap), boxplots = keyword_set(boxplots), status = status
	verbose = 0
	if (status ne 0 and verbose gt 0) then begin
		print, name, ": ERROR - could not output report in the file ", report_dest
	endif
	if (keyword_set(create_chisq_contour)) then begin
		do_chisq_contour, output = chisq_contour_file, data = report_all
		do_chisq_contour, output = cost_contour_file, data = report_all, /docost
	endif
endif else begin
	print, "O", format='(A, $)'
endelse

return, report_all
end

pro do_chisq_contour, output = output, input = input, data = data, docost = docost, tps = tps
name = "do_chisq_contour"
if (n_elements(data) eq 0 and n_elements(input) eq 0) then begin
	print, name, ": ERROR - null data or null input"
	return
endif
if (n_elements(data) eq 0) then data = mrdfits(input, 1, hdr, /silent)
gcount = 0
gndex = where(data.r1 ne !value_is_bad and data.r_halo1 ne !value_is_bad and data.r_halo2 ne !value_is_bad and data.r2 ne !value_is_bad, gcount)
if (gcount le 0) then begin
	print, name, ": ERROR - no good data points were found"
	return
endif

r1 = data[gndex].r1
r_halo1 = data[gndex].r_halo1
r2 = data[gndex].r2
r_halo2 = data[gndex].r_halo2
chisq1 = data[gndex].chisqfl1_nu
chisq2 = data[gndex].chisq2_nu
dchisq = (chisq2 - chisq1) * !degree_of_freedom
z = asinh(dchisq)
z = dchisq

if (keyword_set(docost)) then begin
	dchisq = data[gndex].cost2
endif
x = 10 ^ (r2);;  / !pixelsize
y = 10 ^ (r_halo2);;  / !pixelsize

my_ratio = 10
nx = 200 * my_ratio
ny = 200 * my_ratio
nlevels = 100 * my_ratio

x_target = 12.0 * !PIXELSIZE
y_target = 36.0 * !PIXELSIZE / 2.26444
y_target = x_target

x_line = [0.0, 15.0]
y_line = [0.0, 15.0]
xrange = [0.0, 15.0]
yrange = [0.0, 15.0]

print, name, "checking for degeneracy"
degeneracy_check_ratio = ((8.5 - x_target) / (8.0 - x_target) - 1.0) / 2.0
print, name, "degeracy check ratio used = ", degeneracy_check_ratio
n_non_deg = 0
degeneracy_check_index = where(abs((y - x) / (x - x_target)) gt degeneracy_check_ratio, n_non_deg)
if (n_non_deg gt 0) then begin
	print, name, ": number of possible degeneracy candidates discovered = ", n_non_deg
	x = x[degeneracy_check_index]
	y = y[degeneracy_check_index]
	z = z[degeneracy_check_index]
	dchisq = dchisq[degeneracy_check_index]
endif else begin
	print, name, ": no degeneracy candidate was discovered"
endelse

TRIANGULATE, x, y, tr, b
if (keyword_set(tps)) then begin
print, name, ": using grid_tps for data interporlation"
z_plot = GRID_TPS(x, y, z, ngrid = [nx, ny])
x_plot = GRID_TPS(x, y, x, ngrid = [nx, ny])
y_plot = GRID_TPS(x, y, y, ngrid = [nx, ny])
endif else begin
ngrid = [nx, ny]
x_start = min(x)
y_start = min(y)
x_end = max(x)
y_end = max(y)

delta_x = (x_end - x_start) / (nx - 1.0D)
delta_y = (y_end - y_start) / (ny - 1.0D)
x_plot = x_start + dindgen(nx) * delta_x
y_plot = y_start + dindgen(ny) * delta_y

start_pos = [x_start, y_start]
delta = [delta_x, delta_y]

print, name, ": linear interporlation"
;;z = griddata(x, y, (dchisq), /quintic, triangles = tr, dimension = ngrid, start = start_pos, delta = delta)
z_plot = griddata(x, y, z, /linear, triangles = tr, dimension = ngrid, start = start_pos, delta = delta)
endelse

extpos = STRPOS(output, '.', /REVERSE_SEARCH)
epsfile = STRMID(output, 0, extpos)+".eps"

if (n_elements(z) gt 1) then begin

olddevice = !d.name
set_plot, 'PS' ; switch to the postscript device
Device, DECOMPOSED=decomposed, COLOR=1, BITS_PER_PIXEL=8
device, /encapsulated, file=epsfile, font_size = !my_font_size ; specify some details, give the file a name
loadct, 40, /silent
device, xsize= 10.0, ysize=10.0, /inch

;; contour, dchisq, x, y, /irregular
print, name, ": min(dchisq) = ", min(dchisq)
print, name, ": max(dchisq) = ", max(dchisq)

Atemp = FIndGen(16) * (!PI*2/16.) 
UserSym, cos(Atemp), sin(Atemp), /fill
levels = min(z)+ findgen(nlevels) * (max(z) - min(z)) / (nlevels - 1.0)

levels = (max(z) - min(z) + 1.0) ^ (findgen(1201) / 1200.0) + min(z) - 1.0
levels = [-30.0, -25.0, -22.50, -20.0, -15.0, -10.0, -5.0, 0.0, 50.0, 100.0, 150.0, 200.0, 500.0, 1000.0, 2000.0, 5000.0]
c_levels = [-25.0, -22.50, -20.0, -15.0, -10.0, -5.0, 0.0, 50.0, 100.0, 1000.0, 1500.0]
match_levels = 0
match, alog(levels + 50.0), alog(c_levels + 50.0), index_levels, index_c_levels, count = match_levels, /sort, epsilon = 0.1
c_labels = replicate(0, n_elements(levels))
c_labels[index_levels] = 1


plot, [1.0], [1.0], xrange = xrange, yrange = yrange, /xstyle, /ystyle, $
xtitle = "r_core (arcsec)", ytitle = "r_halo (arcsec)", xlog = dolog_path, ylog = dolog_path, $
/isotropic, /nodata, position = [0.1, 0.1, 0.9, 0.9], xcharsize = 1.5, ycharsize= 1.5, symsize=2.0 ;;  

contour, z_plot, x_plot, y_plot, $
 	levels = levels, c_labels = c_labels, /overplot

;; contour, z_plot, x_plot, y_plot, xrange = xrange, yrange = yrange, /xstyle, /ystyle, $
;;  	xtitle = "r_core (arcsec)", ytitle = "r_halo (arcsec)", xcharsize = 1.5, ycharsize= 1.5, $
;;  	levels = levels, c_labels = c_labels, /overplot, /isotropic ;; , xmargin = [10.0, 10.0], ymargin = [10.0, 10.0]



;; contour, z, x, y, xrange = xrange, yrange = yrange, /irregular, /isotropic, /xstyle, /ystyle, $
;; 	xtitle = "r_core (arcsec)", ytitle = "r_halo (arcsec)", xcharsize = 1.5, ycharsize= 1.5, $
;; 	levels = levels, c_labels = c_labels	

oplot, x_line, y_line, color = 250
oplot, [x_target], [y_target], psym = 8, color = 150, symsize= 3

device, /close ; close the file
set_plot, olddevice

pngfile = STRMID(output, 0, extpos)+".png"
command = "convert " + epsfile + " " + pngfile
spawn, command

endif else begin
print, name, ": ERROR - cholesky factorization failed"

endelse

return
end
