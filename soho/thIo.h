#ifndef THIO_H
#define THIO_H

#include "thPhotoFitsIO.h"
#include "thConsts.h"
#include "thIoTypes.h"
#include "thFnalTypes.h"
#include "thObjcTypes.h"

RET_CODE thInitIo();

/* schematrans handlers */

SCHEMATRANS *thSchemaGetObjcpars();

SCHEMATRANS *thSchemaGetFunc();

SCHEMATRANS *thSchemaGetReg();

SCHEMATRANS *thSchemaGetObjmask();

SCHEMATRANS *thSchemaGetObject1();

SCHEMATRANS *thSchemaGetObjc();

SCHEMATRANS *thSchemaGetGalacticCalib();

SCHEMATRANS *thSchemaGetCalibPhObjcIo(IO_MODE mode);

SCHEMATRANS *thSchemaGetFnalObjcIo(IO_MODE mode);

SCHEMATRANS *thSchemaGetObjcIo(IO_MODE mode);

SCHEMATRANS *thSchemaGetAtlasImage();

SCHEMATRANS *thSchemaGetPhCalib();

SCHEMATRANS *thSchemaGetPsfKLComp();

SCHEMATRANS *thSchemaGetCalibIo();

SCHEMATRANS *thSchemaGetCrudeCalib();

SCHEMATRANS *thSchemeGetPsfwing();

SCHEMATRANS *thSchemaGetSkycoeffs();

SCHEMATRANS *thSchemaGetSkymodel();

SCHEMATRANS *thSchemaGetSkymodelIo ();

SCHEMATRANS *thSchemaGetExtregion (); 

SCHEMATRANS *thSchemaGetFmhdu ();

SCHEMATRANS *thSchemaGetParelemio ();
 
 
SCHEMATRANS *thSchemaGetLmethod ();

SCHEMATRANS *thSchemaGetLprofile ();

SCHEMATRANS *thSchemaGetPsfReport();

SCHEMATRANS *thSchemaGetBasicStat();

SCHEMATRANS *thSchemaGetBriefPsfReport();

SCHEMATRANS *thSchemaGetBinregion();

/* fits file readers */

CHAIN *thFitsFuncRead(PHFITSFILE *fits, RET_CODE *status);

SPANMASK *thFitsSpanmaskRead(PHFITSFILE *fits, 
			     int nrow, int ncol, 
			     RET_CODE *status);


CHAIN *thFitsAtlasRead(PHFITSFILE *fits, RET_CODE *status);
CHAIN *thFitsObjcRead(PHFITSFILE *fits, RET_CODE *status);
CHAIN *thFitsObjcIoRead(PHFITSFILE *fits, RET_CODE *status);
CHAIN *thFitsWObjcIoRead(PHFITSFILE *fits, RET_CODE *status);

THBINREGION *thFitsBinregionRead(PHFITSFILE *fits, RET_CODE *status);

#if CALIB_PHOBJC_IO_USE_ALTERED_FITS_PACKAGE
CHAIN *thFitsCalibPhObjcIoRead(THFITSFILE *fits, RET_CODE *status);
#else
CHAIN *thFitsCalibPhObjcIoRead(PHFITSFILE *fits, RET_CODE *status);
#endif
PHCALIB *thFitsPhCalibRead(PHFITSFILE *fits, int field, RET_CODE *status); 
CHAIN *thFitsSkymodelRead(PHFITSFILE *fits, RET_CODE *status);
PSF_BASIS *thFitsPhPsfBasisRead(PHFITSFILE *fits, RET_CODE *status);
PHPSF_WING *thFitsPhPsfWingRead(PHFITSFILE *fits, RET_CODE *status);
PSFWING *thFitsPsfWingRead(PHFITSFILE *fits, RET_CODE *status);

RET_CODE thFitsDumpChain(PHFITSFILE *fits, CHAIN *chain, char *ioname);
RET_CODE thAsciiDumpStru(FILE *file, void *stru, char *type, char **rnames, char **comments, int nrecord, char *hdr);

#endif

