/* Standard C Libs */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dervish.h"
#include "strings.h"
/* SDSS construction */
#include "dervish.h"
#include "phObjc.h"
#include "phPhotoFitsIO.h"
#include "phConsts.h"
/* SOHO Libs */
#include "sohoEnv.h"
#include "thDebug.h"

RET_CODE thCpFpObjc(char *infile, char *outfile);
CHAIN *thReadFpObjc(char *file, RET_CODE *status);


/**/

RET_CODE thCpFpObjc(char *infile, char *outfile)
{

  char *name = "thCpFpObjc";

  RET_CODE status;

  CHAIN *ObjcList = NULL;
  
  ObjcList = thReadFpObjc(infile, &status);
  if (status != SH_SUCCESS) {
    return(status);
  }
  
  PHFITSFILE *fd = NULL;
  if ((fd = phFitsBinTblOpen(outfile, 1, NULL)) == NULL) {
    return(SH_GENERIC_ERROR);
  }
  if (fd == NULL){
    return(SH_GENERIC_ERROR);
  }
  
  int phStatus = 0;
  phStatus = phFitsBinTblChainWrite(fd, ObjcList);

  if (phStatus < 0) {
    thError("%s: Unsuccessful WRITE attempt to %s", name, outfile);
    status = SH_GENERIC_ERROR;
  }
  shChainDel(ObjcList);
  return(status);
}

CHAIN *
thReadFpObjc(char *file, RET_CODE *status)
{

  char *name = "thReadFpObjc";
  OBJC *objc = NULL;
  PHFITSFILE *fd = NULL;
  int ncolor, j;
  
  int phStatus = 0;
  
  /*allocate file type */
  if ((fd = phFitsBinTblOpen(file,		/* file to open */
			     0,		/* open for read (flag == 0) */
			     NULL))		/* additional header info */
      == NULL) {
    *status = SH_GENERIC_ERROR;
    return(NULL);
  }
  if (fd == NULL){
    *status =  SH_GENERIC_ERROR;
    return(NULL);
  }

  int nfitsrow = 0, quiet = 0;
  /* reading the extension header and evaluating the number of rows */
  phStatus = phFitsBinTblHdrRead(fd, "OBJC", NULL, &nfitsrow, quiet);


  CHAIN *ObjcChain;
  int i;
  if (nfitsrow > 0 && phStatus >= 0) {

    ObjcChain = shChainNew ("OBJC");

    /* reading ncolor */ 
    objc = phObjcNew(NCOLOR);
    phFitsBinTblRowRead(fd, objc);
    shErrStackClear();
    phFitsBinTblRowUnread(fd);
    ncolor = objc->ncolor;
    phObjcDel(objc, 1);
  
    for (i = 0; i < nfitsrow; i++){
      /* read the whole obj list */
      objc = phObjcNew(ncolor);
      for (j = 0; j < ncolor; j++){
	objc->color[j] = phObject1New();
	objc->color[j]->region = shRegNew("", 0, 0, TYPE_PIX);
      }
      phStatus = phFitsBinTblRowRead(fd, objc);
      
      if (phStatus < 0) {
	thError("%s: problem reading fits file %s [1] at row %d", name, file, i);
	*status = SH_GENERIC_ERROR;
	return(NULL);
      } else {
	shChainElementAddByPos(ObjcChain, objc, "OBJC", TAIL, AFTER);
      }
    }}
  
  /* deallocate file type */
  phStatus = phFitsBinTblClose(fd);
  if (phStatus < 0) {
    thError("%s: problem closing the file %s", name, file);
    *status =  SH_GENERIC_ERROR;
  } else {
    *status = SH_SUCCESS;
  }
  
  /* return the object list read */
  return(ObjcChain);
}
  
