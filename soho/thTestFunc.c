#include "thTestFunc.h"

TESTPAR *thTestparNew() {
TESTPAR *q;
q = thCalloc(1, sizeof(TESTPAR));
return(q);
}
void thTestparDel(TESTPAR *q) {
if (q == NULL) return;
thFree(q);
return;
}


RET_CODE sinxcosy(TESTPAR *q, FUNCMODEL *f) {
char *name = "sinxcosy";

const THPIX pi = 3.14;

if (q == NULL || f == NULL) {
	thError("%s: ERROR - null input or output", name);
	return(SH_GENERIC_ERROR);
}

/* set the name */
if (f->funcname == NULL) f->funcname = thCalloc(SIZE, sizeof(char));
strcpy(f->funcname, name);
/* set the type */
f->stat = HAS_DIR_FUNCMODEL;
f->ncomp = 1;
if (f->compindex == NULL) {
	f->compindex = thCalloc(N_FUNCMODEL_COMP_INDEX, sizeof(int));
}
	f->compindex[0]=0;
if (f->parlist != NULL) {
	thParlistDel(f->parlist);
	}
/* now adjust the REGIONs */
REGION *reg; 
if (f->regs == NULL) {
	f->regs = (REGION **) thCalloc(N_FUNCMODEL_COMP_INDEX, sizeof(REGION*));
	}
reg = (f->regs)[DIRECT_FUNCMODELINDEX];
int nrow, ncol;
nrow = f->nrow;
ncol = f->ncol;
if (nrow <= 0 || ncol <= 0) {
	thError("%s: ERROR - unrealistic nrow (%d) or ncol (%d)", name, nrow, ncol);
	return(SH_GENERIC_ERROR);
	}

if (reg == NULL || reg->nrow != nrow || reg->ncol != ncol) {
	if (reg != NULL) shRegDel(reg);	
	if (f->wregs == NULL) f->wregs = thCalloc(N_FUNCMODEL_COMP_INDEX, sizeof(REGION *));
	if (f->wregs[DIRECT_FUNCMODELINDEX] == NULL) {
		f->wregs[DIRECT_FUNCMODELINDEX] = shRegNew("work space", nrow, ncol, TYPE_THPIX);
	}
	(f->regs)[DIRECT_FUNCMODELINDEX] = shSubRegNew(name, (f->wregs)[DIRECT_FUNCMODELINDEX], nrow, ncol, 0, 0, NO_FLAGS);
}
reg = (f->regs)[DIRECT_FUNCMODELINDEX];


/* now compute the region */
int i, j;
THPIX *row, x, y, a, b;
a = q->a;
b = q->b;
for (i = 0; i < nrow; i++) {
	row = reg->rows_thpix[i];
	x = sin(a * i * 2.0 * pi / nrow);
	for (j = 0; j < ncol; j++) {
		y = j * 2.0 * pi / ncol;
		row[j] = x * cos(b * y);
	}
	}

return(SH_SUCCESS);
}

RET_CODE sinxsiny(TESTPAR *q, FUNCMODEL *f) {
char *name = "sinxsiny";
const THPIX pi = 3.14159;
if (q == NULL || f == NULL) {
	thError("%s: ERROR - null input or output", name);
	return(SH_GENERIC_ERROR);
}

/* set the name */
if (f->funcname == NULL) f->funcname = thCalloc(SIZE, sizeof(char));
strcpy(f->funcname, name);
/* set the type */
f->stat = HAS_DIR_FUNCMODEL;
if (f->parlist != NULL) {
	thParlistDel(f->parlist);
	}
f->ncomp = 1;
if (f->compindex == NULL) {
	f->compindex = thCalloc(N_FUNCMODEL_COMP_INDEX, sizeof(int));
}
f->compindex[0]=0;
/* now adjust the REGIONs */
REGION *reg; 
if (f->regs == NULL) {
	f->regs = (REGION **) thCalloc(N_FUNCMODEL_COMP_INDEX, sizeof(REGION*));
	}
reg = (f->regs)[DIRECT_FUNCMODELINDEX];
int nrow, ncol;
nrow = f->nrow;
ncol = f->ncol;
if (nrow <= 0 || ncol <= 0) {
	thError("%s: ERROR - unrealistic nrow (%d) or ncol (%d)", name, nrow, ncol);
	return(SH_GENERIC_ERROR);
	}

if (reg == NULL || reg->nrow != nrow || reg->ncol != ncol) {
	if (reg != NULL) shRegDel(reg);	
	if (f->wregs == NULL) f->wregs = thCalloc(N_FUNCMODEL_COMP_INDEX, sizeof(REGION *));
	if (f->wregs[DIRECT_FUNCMODELINDEX] == NULL) {
		f->wregs[DIRECT_FUNCMODELINDEX] = shRegNew("work space", nrow, ncol, TYPE_THPIX);
	}
	(f->regs)[DIRECT_FUNCMODELINDEX] = shSubRegNew(name, (f->wregs)[DIRECT_FUNCMODELINDEX], nrow, ncol, 0, 0, NO_FLAGS);
}
reg = (f->regs)[DIRECT_FUNCMODELINDEX];


/* now compute the region */
int i, j;
THPIX *row, x, y, a, b;
a = q->a;
b = q->b;
for (i = 0; i < reg->nrow; i++) {
	row = reg->rows_thpix[i];
	x = sin(a * i * 2.0 * pi / nrow);
	for (j = 0; j < reg->ncol; j++) {
		y = j * 2.0 * pi / ncol;
		row[j] = x * sin(b * y);
	}
	}

return(SH_SUCCESS);
}

#if 0
 
RET_CODE deV(TESTPAR *q, FUNCMODEL *f) {
char *name = "deV";
static const THPIX nu = 7.66925;
/*
if (q == NULL && f == NULL) {
	thFuncLoadAddEntry(name, &deV);
	return(SH_SUCCESS);
}
*/
if (q == NULL || f == NULL) {
	thError("%s: ERROR - null input or output", name);
	return(SH_GENERIC_ERROR);
}

/* set the name */
if (f->funcname == NULL) f->funcname = thCalloc(SIZE, sizeof(char));
strcpy(f->funcname, name);
/* set the type */
f->stat = HAS_DIR_FUNCMODEL;
if (f->parlist != NULL) {
	thParlistDel(f->parlist);
	}
f->ncomp = 1;
if (f->compindex == NULL) {
	f->compindex = thCalloc(N_FUNCMODEL_COMP_INDEX, sizeof(int));
}
f->compindex[0]=0;
/* now adjust the REGIONs */
REGION *reg; 
if (f->regs == NULL) {
	f->regs = (REGION **) thCalloc(N_FUNCMODEL_COMP_INDEX, sizeof(REGION*));
	}
reg = (f->regs)[DIRECT_FUNCMODELINDEX];
int nrow, ncol;
nrow = f->nrow;
ncol = f->ncol;
if (nrow <= 0 || ncol <= 0) {
	thError("%s: ERROR - unrealistic nrow (%d) or ncol (%d)", name, nrow, ncol);
	return(SH_GENERIC_ERROR);
	}

int devnrow, devncol;
devnrow = MIN((int) (6.0 + 6 * q->r_e), nrow);
devncol = MIN((int) (6.0 + 6 * q->r_e), ncol);

if (reg == NULL || reg->nrow != devnrow || reg->ncol != devncol) {
	if (reg != NULL) shRegDel(reg);
	if (f->wregs == NULL) f->wregs = thCalloc(N_FUNCMODEL_COMP_INDEX, sizeof(REGION *));
	if (f->wregs[DIRECT_FUNCMODELINDEX] == NULL) {
		f->wregs[DIRECT_FUNCMODELINDEX] = shRegNew("work space", nrow, ncol, TYPE_THPIX);
	}
	(f->regs)[DIRECT_FUNCMODELINDEX] = shSubRegNew("de Vaucouleur Profile", (f->wregs)[DIRECT_FUNCMODELINDEX], devnrow, devncol, 0, 0, NO_FLAGS);
}

reg = (f->regs)[DIRECT_FUNCMODELINDEX];


/* now compute the region */
int i, j, idr, idc;
THPIX *row, x, y, rsqr, asqr, rowc, colc;
asqr = pow(q->r_e, 2);

idr = devnrow / 2;
idc = devncol / 2;
rowc = q->rowc + idr;
colc = q->colc + idc;

for (i = 0; i < reg->nrow; i++) {
	row = reg->rows_thpix[i];
	x = (i  - rowc + (THPIX) 0.5);
	for (j = 0; j < reg->ncol; j++) {
		y = (j - colc + (THPIX) 0.5);
		rsqr = (pow(x, 2) + pow(y, 2)) / asqr;
		row[j] = exp(nu * (1.0-pow(rsqr, 0.125)));
	}
	}

reg->row0 = -idr;
reg->col0 = -idc;

return(SH_SUCCESS);
}

#endif

RET_CODE SersicProfile(TESTPAR *q, FUNCMODEL *f) {
char *name = "SersicProfile";

if (q == NULL && f == NULL) {
	thFuncLoadAddEntry(name, &SersicProfile);
	return(SH_SUCCESS);
}

if (q == NULL || f == NULL) {
	thError("%s: ERROR - null input or output", name);
	return(SH_GENERIC_ERROR);
}

/* setting the constant later to be used */
static int init_const = 0;
static double pi = 0.0;
if (!init_const) {
pi = 4.0 * atan((double) 1.0);
init_const = 1;
}

/* 
the following aymptotic expansion is for the inverse of gamma distribution at point p = q = 1/2

ref.

MATHEMATICS OF COMPUTATION
VOLUME 58, NUMBER 198
APRIL 1992, PAGES 755-764

ASYMPTOTIC INVERSION OF INCOMPLETE GAMMA FUNCTIONS

by

N.M. TEMME
*/

double a;
a = 2.0 * (double) (q->n_sersic);
q->b_n = (THPIX) (a * (1.0 - 1.0 / (3.0 * a) + 8.0 / (405.0 * pow(a, 2)) + 184.0 / (25515.0 * pow(a, 3)) + 2248.0 / (3444525.0 * pow(a, 4))));
double gamma;
gamma = thGamma(a + (double) 1.0);
THPIX I0;
I0 = (THPIX) (pow((double) q->b_n, a) / (pi * gamma * pow((double) q->r_e, 2)));

/* elliptical sersic profile */
q->a_xx = (1.0 + q->epsilon * pow(sin(pi * q->phi / 180.0), 2));
q->a_xy = -2.0 * q->epsilon * sin(pi * q->phi / 180.0) * cos(pi * q->phi / 180.0);
q->a_yy = (1.0 + q->epsilon * pow(cos(pi * q->phi / 180.0), 2));


/* set the name */
if (f->funcname == NULL) f->funcname = thCalloc(SIZE, sizeof(char));
strcpy(f->funcname, name);
/* set the type */
f->stat = HAS_DIR_FUNCMODEL;
if (f->parlist != NULL) {
	thParlistDel(f->parlist);
	}
f->ncomp = 1;
if (f->compindex == NULL) {
	f->compindex = thCalloc(N_FUNCMODEL_COMP_INDEX, sizeof(int));
}
f->compindex[0]=0;
/* now adjust the REGIONs */
REGION *reg; 
if (f->regs == NULL) {
	f->regs = (REGION **) thCalloc(N_FUNCMODEL_COMP_INDEX, sizeof(REGION*));
	}
reg = (f->regs)[DIRECT_FUNCMODELINDEX];
int nrow, ncol;
nrow = f->nrow;
ncol = f->ncol;
if (nrow <= 0 || ncol <= 0) {
	thError("%s: ERROR - unrealistic nrow (%d) or ncol (%d)", name, nrow, ncol);
	return(SH_GENERIC_ERROR);
	}
/* ideal rectangle that includes the least significant ellipse */
int xmax, ymax, rhomax;
rhomax = q->r_e * pow((2.0 * q->n_sersic + PROFILE_TAIL * sqrt(2.0 * q->n_sersic)) / q->b_n, q->n_sersic);
xmax = rhomax / sqrt(q->a_xx + 0.75 * pow(q->a_xy, 2) / q->a_yy);
ymax = rhomax / sqrt(q->a_yy + 0.75 * pow(q->a_xy, 2) / q->a_yy);
int devnrow, devncol;
devnrow = MIN((int) (2.0 * xmax + 3.0), nrow);
devncol = MIN((int) (2.0 * ymax + 3.0), ncol);

if (reg == NULL || reg->nrow != devnrow || reg->ncol != devncol) {
	if (reg != NULL) shRegDel(reg);
	if (f->wregs == NULL) f->wregs = thCalloc(N_FUNCMODEL_COMP_INDEX, sizeof(REGION *));
	if (f->wregs[DIRECT_FUNCMODELINDEX] == NULL) {
		f->wregs[DIRECT_FUNCMODELINDEX] = shRegNew("work space", MAXNROW, MAXNCOL, TYPE_THPIX);
	}
	(f->regs)[DIRECT_FUNCMODELINDEX] = shSubRegNew("Sersic Profile", (f->wregs)[DIRECT_FUNCMODELINDEX], devnrow, devncol, 0, 0, NO_FLAGS);
}
reg = (f->regs)[DIRECT_FUNCMODELINDEX];


/* now compute the region */
int idr, idc;
idr = devnrow / 2;
idc = devncol / 2;

RET_CODE status;

status = CreateProfile(&SersicPixel, q, "TESTPAR",  reg, idr, idc, NULL, 0);

if (status != SH_SUCCESS) {
	thError("%s: ERROR - unable to generate profile", name);
	return(status);
}

int i, j;
THPIX *row;
for (i = 0; i < devnrow; i++) {
	row = reg->rows_thpix[i];
	for (j = 0; j < devncol; j++) {
	row[j] *= I0;
	}
}

return(SH_SUCCESS);
}


RET_CODE deVprofile(TESTPAR *q, FUNCMODEL *f) {
char *name = "deVprofile";

if (q == NULL && f == NULL) {
	thFuncLoadAddEntry(name, &deVprofile);
	return(SH_SUCCESS);
}

if (q == NULL || f == NULL) {
	thError("%s: ERROR - null input or output", name);
	return(SH_GENERIC_ERROR);
}

/* setting the constant later to be used */
static const THPIX nu = 7.66925;
static int init_const = 0;
static double pi = 0.0;
static double gamma_9 = 1.0;
static double theta;
if (!init_const) {
int i;
for (i = 1; i < 9; i++) gamma_9 *= ((double) i);
pi = 4.0 * atan((double) 1.0);
theta = pow(nu, 8) / (pi * gamma_9);
init_const = 1;
}

/* elliptical isophotes */
q->a_xx = (1.0 + q->epsilon * pow(sin(pi * q->phi / 180.0), 2));
q->a_xy = -2.0 * q->epsilon * sin(pi * q->phi / 180.0) * cos(pi * q->phi / 180.0);
q->a_yy = (1.0 + q->epsilon * pow(cos(pi * q->phi / 180.0), 2));

/* set the name */
if (f->funcname == NULL) f->funcname = thCalloc(SIZE, sizeof(char));
strcpy(f->funcname, name);
/* set the type */
f->stat = HAS_DIR_FUNCMODEL;
if (f->parlist != NULL) {
	thParlistDel(f->parlist);
	}
f->ncomp = 1;
if (f->compindex == NULL) {
	f->compindex = thCalloc(N_FUNCMODEL_COMP_INDEX, sizeof(int));
}
f->compindex[0]=DIRECT_FUNCMODELINDEX;
/* now adjust the REGIONs */
REGION *reg; 
if (f->regs == NULL) {
	f->regs = (REGION **) thCalloc(N_FUNCMODEL_COMP_INDEX, sizeof(REGION*));
	}
reg = (f->regs)[DIRECT_FUNCMODELINDEX];
int nrow, ncol;
nrow = f->nrow;
ncol = f->ncol;
if (nrow <= 0 || ncol <= 0) {
	thError("%s: ERROR - unrealistic nrow (%d) or ncol (%d)", name, nrow, ncol);
	return(SH_GENERIC_ERROR);
	}

int xmax, ymax, rhomax;
rhomax = q->r_e * pow((8.0 + PROFILE_TAIL * sqrt(8.0)) / nu, 4.0);
xmax = rhomax / sqrt(q->a_xx + 0.75 * pow(q->a_xy, 2) / q->a_yy);
ymax = rhomax / sqrt(q->a_yy + 0.75 * pow(q->a_xy, 2) / q->a_yy);
int devnrow, devncol;
devnrow = MIN((int) (2.0 * xmax + 3.0), nrow);
devncol = MIN((int) (2.0 * ymax + 3.0), ncol);

if (reg == NULL || reg->nrow != devnrow || reg->ncol != devncol) {
	if (reg != NULL) shRegDel(reg);
	if (f->wregs == NULL) f->wregs = thCalloc(N_FUNCMODEL_COMP_INDEX, sizeof(REGION *));
	if (f->wregs[DIRECT_FUNCMODELINDEX] == NULL) {
		f->wregs[DIRECT_FUNCMODELINDEX] = shRegNew("work space", MAXNROW, MAXNCOL, TYPE_THPIX);
	}
	(f->regs)[DIRECT_FUNCMODELINDEX] = shSubRegNew("deVaucouleur Profile", (f->wregs)[DIRECT_FUNCMODELINDEX], devnrow, devncol, 0, 0, NO_FLAGS);
	}
reg = (f->regs)[DIRECT_FUNCMODELINDEX];


/* now compute the region */
int idr, idc;
idr = devnrow / 2;
idc = devncol / 2;

RET_CODE status; 
status = CreateProfile(&deVpixel, q, "TESTPAR",  reg, idr, idc, NULL, 0);

if (status != SH_SUCCESS) {
	thError("%s: ERROR - unable to generate profile", name);
	return(status);
}

int i, j;
THPIX *row;
THPIX I0 = 1.0;
I0 =(THPIX) (theta / (pow((double) (q->r_e), 2)));
for (i = 0; i < devnrow; i++) {
	row = reg->rows_thpix[i];
	for (j = 0; j < devncol; j++) {
	row[j] *= I0;
	}
	}

return(SH_SUCCESS);
}

THPIX deVpixel(PIXPOS *pixpos, void *q2) {
	static const THPIX nu = 7.66925;
	TESTPAR *q;
	q = (TESTPAR *) q2;
	return((THPIX) exp(-nu * pow(
	(
	q->a_xx * pow(pixpos->row - q->rowc, 2) + 
	q->a_xy * (pixpos->row - q->rowc) * (pixpos->col - q->colc) + 
	q->a_yy * pow(pixpos->col - q->colc, 2)
	) / pow(q->r_e, 2)
	, 0.125)));
}	

THPIX SersicPixel(PIXPOS *pixpos, TESTPAR *q) {
	return((THPIX) exp(-q->b_n * 
	pow(
	(q->a_xx * pow(pixpos->row - q->rowc, 2) + 
	q->a_xy * (pixpos->row - q->rowc) * (pixpos->col - q->colc) + 
	q->a_yy * pow(pixpos->col - q->colc, 2)
	) 
	/ pow(q->r_e, 2), (0.5 / q->n_sersic))));

}	


RET_CODE BsplineXTcheb(TESTPAR *q, FUNCMODEL *f) {
char *name = "BsplineXTcheb";

static int init = 0;
static CHAIN *stored_data = NULL;
static int init_mrow = -1, init_mcol = -1, init_nrow = -1, init_ncol = -1;

if (q == NULL && f == NULL) {
	thFuncLoadAddEntry(name, &BsplineXTcheb);
	if (stored_data != NULL) shChainDestroy(stored_data, &thFuncDel);
	stored_data = NULL;
	init_mrow = -1; init_mcol = -1;
	init_nrow = -1; init_ncol = -1;
	init = 0;
	return(SH_SUCCESS);
}

if (q == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

int mrow, mcol, nrow, ncol;
mrow = q->mrow;
mcol = q->mcol;
nrow = q->nrow;
ncol = q->ncol;

if (mrow != init_mrow || mcol != init_mcol || init_nrow != nrow || init_ncol != ncol) {
	if (stored_data != NULL) shChainDestroy(stored_data, &thFuncDel);
	stored_data = gen_Bspline_tcheb_chain_func(nrow, ncol, mrow, mcol, -1.0, 1.0, -1.0, 1.0);
	init_mrow = mrow;
	init_mcol = mcol;
	init_nrow = nrow;
	init_ncol = ncol;
	init = 1;
}

if (f == NULL) {
	thError("%s: WARNING - null output", name);
	return(SH_SUCCESS);
}
/* set the name */
if (f->funcname == NULL) f->funcname = thCalloc(SIZE, sizeof(char));
strcpy(f->funcname, name);
/* set the type */
f->stat = HAS_DIR_FUNCMODEL;
if (f->parlist != NULL) {
	thParlistDel(f->parlist);
	}
f->ncomp = 1;
if (f->compindex == NULL) {
	f->compindex = thCalloc(N_FUNCMODEL_COMP_INDEX, sizeof(int));
}
f->compindex[0]=0;

/* now adjust the REGIONs */
REGION *reg; 
if (f->regs == NULL) {
	f->regs = (REGION **) thCalloc(N_FUNCMODEL_COMP_INDEX, sizeof(REGION*));
	}
reg = (f->regs)[DIRECT_FUNCMODELINDEX];
if (nrow <= 0 || ncol <= 0) {
	thError("%s: ERROR - unrealistic nrow (%d) or ncol (%d)", name, nrow, ncol);
	return(SH_GENERIC_ERROR);
	}

if (reg == NULL || reg->nrow != nrow || reg->ncol != ncol) {
	if (reg != NULL) shRegDel(reg);
	if (f->wregs == NULL) f->wregs = thCalloc(N_FUNCMODEL_COMP_INDEX, sizeof(REGION *));
	if (f->wregs[DIRECT_FUNCMODELINDEX] == NULL) {
		f->wregs[DIRECT_FUNCMODELINDEX] = shRegNew("work space", nrow, ncol, TYPE_THPIX);
	}
	(f->regs)[DIRECT_FUNCMODELINDEX] = shSubRegNew("B-spline(row) X Tcheb (col)", (f->wregs)[DIRECT_FUNCMODELINDEX], nrow, ncol, 0, 0, NO_FLAGS);
}
reg = (f->regs)[DIRECT_FUNCMODELINDEX];

int irow, icol;
irow = q->irow;
icol = q->icol;

int indices[2];
indices[0] = irow;
indices[1] = icol;

int pos;
FUNC *old_func;
pos =  thGetFuncPosByPar(stored_data, indices, NULL);
if (pos < 0) {	
	thError("%s: ERROR - could not find function for (irow = %d, icol = %d; mrow = %d, mcol = %d)", 	name, irow, icol, mrow, mcol);	
	shRegDel(reg);
	(f->regs)[DIRECT_FUNCMODELINDEX] = shSubRegNew("B-spline(row) X Tcheb (col)", (f->wregs)[DIRECT_FUNCMODELINDEX], 0, 0, 0, 0, NO_FLAGS);
	return(SH_GENERIC_ERROR);
	}
	
old_func = shChainElementGetByPos(stored_data, pos);

shRegPixCopy(old_func->reg, reg);

return(SH_SUCCESS);
}

RET_CODE TchebXBspline(TESTPAR *q, FUNCMODEL *f) {
char *name = "TchebXBspline";

static int init = 0;
static CHAIN *stored_data = NULL;
static int init_mrow = -1, init_mcol = -1, init_nrow = -1, init_ncol = -1;

if (q == NULL && f == NULL) {
	thFuncLoadAddEntry(name, &BsplineXTcheb);
	if (stored_data != NULL) shChainDestroy(stored_data, &thFuncDel);
	stored_data = NULL;
	init_mrow = -1; init_mcol = -1;
	init_nrow = -1; init_ncol = -1;
	init = 0;
	return(SH_SUCCESS);
}

if (q == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

int mrow, mcol, nrow, ncol;
mrow = q->mrow;
mcol = q->mcol;
nrow = q->nrow;
ncol = q->ncol;

if (mrow != init_mrow || mcol != init_mcol || init_nrow != nrow || init_ncol != ncol) {
	if (stored_data != NULL) shChainDestroy(stored_data, &thFuncDel);
	/*
	stored_data = gen_tcheb_Bspline_chain_func(nrow, ncol, mrow, mcol, -1.0, 1.0, -1.0, 1.0);
	*/
	init_mrow = mrow;
	init_mcol = mcol;
	init_nrow = nrow;
	init_ncol = ncol;
	init = 1;
}

if (f == NULL) {
	thError("%s: WARNING - null output", name);
	return(SH_SUCCESS);
}
/* set the name */
if (f->funcname == NULL) f->funcname = thCalloc(SIZE, sizeof(char));
strcpy(f->funcname, name);
/* set the type */
f->stat = HAS_DIR_FUNCMODEL;
if (f->parlist != NULL) {
	thParlistDel(f->parlist);
	}
f->ncomp = 1;
if (f->compindex == NULL) {
	f->compindex = thCalloc(N_FUNCMODEL_COMP_INDEX, sizeof(int));
}
f->compindex[0]=0;

/* now adjust the REGIONs */
REGION *reg; 
if (f->regs == NULL) {
	f->regs = (REGION **) thCalloc(N_FUNCMODEL_COMP_INDEX, sizeof(REGION*));
	}
reg = (f->regs)[DIRECT_FUNCMODELINDEX];
if (nrow <= 0 || ncol <= 0) {
	thError("%s: ERROR - unrealistic nrow (%d) or ncol (%d)", name, nrow, ncol);
	return(SH_GENERIC_ERROR);
	}

if (reg == NULL || reg->nrow != nrow || reg->ncol != ncol) {
	if (reg != NULL) shRegDel(reg);
	if (f->wregs == NULL) f->wregs = thCalloc(N_FUNCMODEL_COMP_INDEX, sizeof(REGION *));
	if (f->wregs[DIRECT_FUNCMODELINDEX] == NULL) {
		f->wregs[DIRECT_FUNCMODELINDEX] = shRegNew("work space", nrow, ncol, TYPE_THPIX);
	}
	(f->regs)[DIRECT_FUNCMODELINDEX] = shSubRegNew("B-spline(row) X Tcheb (col)", (f->wregs)[DIRECT_FUNCMODELINDEX], nrow, ncol, 0, 0, NO_FLAGS);
}
reg = (f->regs)[DIRECT_FUNCMODELINDEX];

int irow, icol;
irow = q->irow;
icol = q->icol;

int indices[2];
indices[0] = irow;
indices[1] = icol;

int pos;
FUNC *old_func;
pos =  thGetFuncPosByPar(stored_data, indices, NULL);
if (pos < 0) {	
	thError("%s: ERROR - could not find function for (irow = %d, icol = %d; mrow = %d, mcol = %d)", 	name, irow, icol, mrow, mcol);	
	shRegDel(reg);
	(f->regs)[DIRECT_FUNCMODELINDEX] = shSubRegNew("Tcheb(row) X Bspline (col)", (f->wregs)[DIRECT_FUNCMODELINDEX], 0, 0, 0, 0, NO_FLAGS);
	return(SH_GENERIC_ERROR);
	}
	
old_func = shChainElementGetByPos(stored_data, pos);

shRegPixCopy(old_func->reg, reg);

return(SH_SUCCESS);
}

