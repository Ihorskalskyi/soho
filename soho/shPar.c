#include "shPar.h"

static PAR_STATUS check_ln_delimiter(FILE *fil);
static char *get_format_heap(char *format, const int n);
static int par_check_struct_name(FILE *fil, char *stru);


 char **read_line(FILE *fil, const int n){

  if (n <= 0) {return(NULL);}

  char ** hdr;
  hdr = (char **) thMalloc(n * sizeof(char *));

  int i, j, notendofline = 1;
  char c;

  for (i = 0; i < n; i++){    
    hdr[i] = (char *) thCalloc((MX_HDR_LINE_LEN + 1), sizeof(char));
    j = 0;
    notendofline = 1;
    while (notendofline) {
      c = fgetc(fil);
      if (c != '\n') {
	hdr[i][j] = c;
	j++;
      } else {
	notendofline = 0;
      }
    }

  }
  return(hdr);

}

void skip_line(FILE *fil, const int n){

  /* reads n lines from the header but returns nothing 
     can be used to set the position of the cursor to the 
     desired beginning of a structure, if the length of the
     header is known
  */

  int i = 0;
  char c;

  while (i < n) {
    c = 'x';
    while (c != '\n') {
      c = fgetc(fil);
    }
    i++;
  }

  return;

}
  

static PAR_STATUS check_ln_delimiter(FILE *fil) {

  /* 
     this function returns 1 if one has reached the end of the structure
     and places the stream at the beginning of the new structure 
     it takes the stream to the next line otherwise 
     
     the convention used throughout this function is that structures are 
     separated by "\n" characters, while one uses "\\" (a backslash) 
     in separation of lines within one structure to gain readability
  */
  char c, oldc;
  char cbackslash = '\\', cspace = ' ', ctab = '\t', cnewline = '\n';
  int cont = 1;
  PAR_STATUS status  = PAR_MOSTRUCT;
  
  while (cont == 1){

    if (feof(fil) != 0) {
      cont = 0;
      status = PAR_EOF;
    } else {
      c = fgetc(fil);
      if (c == cbackslash) {
	oldc = c;
	cont = 1;
      } else if (c == cnewline) {
	if (oldc != cbackslash) {
	  status = PAR_EOSTRUCT;
	}
	oldc = c;
	cont = 1;
      } else if (c == cspace || c == ctab) {
	status = PAR_MOSTRUCT;
	cont = 1;
	
      } else {
	cont = 0;
      }
    }
  }

  if (status != PAR_EOF) {
    //ungetc(1, fil);
    fseek(fil, -1, SEEK_CUR);
  }

  return(status);
}

static  char *get_format_heap(char *format, const int n){

  int i;
  char *f_heap;
  
  f_heap = (char *) thMalloc((strlen(format) + n + 3) * sizeof(char));
  strcpy(f_heap, "{");

  for (i = 0; i < n; i++){
    strcat(f_heap, " ");
    strcat(f_heap, format);
  }
  strcat(f_heap, "}");
  
  return(f_heap);

}


  void read_i(FILE *fil, int *var) {
  
  check_ln_delimiter(fil);

  char *format = "%d";
  fscanf(fil, format, var); 
  return;
}

 void read_l(FILE *fil,  long *var) {
  
  check_ln_delimiter(fil);

  char *format = "%ld";
  fscanf(fil, format, var); 
  return;
}

 void read_ll(FILE *fil, long long *var) {
  
  check_ln_delimiter(fil);

  char *format = "%Ld";
  fscanf(fil, format, var); 
  return;
}


  void read_f(FILE *fil, float *var) {

  check_ln_delimiter(fil);
  char *format = "%f";
  fscanf(fil, format, var);
  return;
}
   
  void read_d(FILE *fil, double *var) {
    
    check_ln_delimiter(fil);
    char *format = "%lf";
    fscanf(fil, format, var);
    return;
  }

  void read_ld(FILE *fil, long double *var) {
    
    check_ln_delimiter(fil);
    char *format = "%Lf";
    fscanf(fil, format, var);
    return;
  }

  void read_s(FILE *fil, char *var) {
  
  check_ln_delimiter(fil);
  char *format = "%s";
  fscanf(fil, format, var);
  return;
}

void read_i_heap(FILE *fil, int *var, const int n) {
  
  char *name = "read_i_heap";
  
  check_ln_delimiter(fil);
  
  int loc;
  loc = ftell(fil);
  
  char c;
  c = fgetc(fil);
  
  if (c != '{') {
    shErrStackPush("%s: next element is not an array -- no elements were read", name);
    fseek(fil, loc, SEEK_SET);
    return;
  }
  
  int i;
  for (i = 0; i < n; i++) {
    read_i(fil, var + i);
  }
  
  check_ln_delimiter(fil);
  
  c = fgetc(fil);
  
  if (c != '}') {
    shErrStackPush("%s: next element is not an array - wrong elements read", name);
    fseek(fil, loc, SEEK_SET);
    return;
  }
  
    return;
}

void read_l_heap(FILE *fil, long *var, const int n) {
  
  char *name = "read_l_heap";
  
  check_ln_delimiter(fil);
  
  int loc;
  loc = ftell(fil);
  
  char c;
  c = fgetc(fil);
  
  if (c != '{') {
    shErrStackPush("%s: next element is not an array -- no elements were read", name);
    fseek(fil, loc, SEEK_SET);
    return;
  }
  
  int i;
  for (i = 0; i < n; i++) {
    read_l(fil, var + i);
  }
  
  check_ln_delimiter(fil);
  
  c = fgetc(fil);
  
  if (c != '}') {
    shErrStackPush("%s: next element is not an array - wrong elements read", name);
    fseek(fil, loc, SEEK_SET);
    return;
  }
  
    return;
}

void read_ll_heap(FILE *fil, long long *var, const int n) {
  
  char *name = "read_ll_heap";
  
  check_ln_delimiter(fil);
  
  int loc;
  loc = ftell(fil);
  
  char c;
  c = fgetc(fil);
  
  if (c != '{') {
    shErrStackPush("%s: next element is not an array -- no elements were read", name);
    fseek(fil, loc, SEEK_SET);
    return;
  }
  
  int i;
  for (i = 0; i < n; i++) {
    read_ll(fil, var + i);
  }
  
  check_ln_delimiter(fil);
  
  c = fgetc(fil);
  
  if (c != '}') {
    shErrStackPush("%s: next element is not an array - wrong elements read", name);
    fseek(fil, loc, SEEK_SET);
    return;
  }
  
    return;
}

void read_f_heap(FILE *fil, float *var, const int n) {
  
  char *name = "read_f_heap";
  
  check_ln_delimiter(fil);
  
  int loc;
  loc = ftell(fil);
  
  char c;
  c = fgetc(fil);
  
  if (c != '{') {
    shErrStackPush("%s: next element is not an array -- no elements were read", name);
    fseek(fil, loc, SEEK_SET);
    return;
  }
  
  int i;
  for (i = 0; i < n; i++) {
    read_f(fil, var + i);
  }
  
  check_ln_delimiter(fil);
  
  c = fgetc(fil);
  
  if (c != '}') {
    shErrStackPush("%s: next element is not an array - wrong elements read", name);
    fseek(fil, loc, SEEK_SET);
    return;
  }
  
    return;
}

void read_d_heap(FILE *fil, double *var, const int n) {
  
  char *name = "read_d_heap";
  
  check_ln_delimiter(fil);
  
  int loc;
  loc = ftell(fil);
  
  char c;
  c = fgetc(fil);
  
  if (c != '{') {
    shErrStackPush("%s: next element is not an array -- no elements were read", name);
    fseek(fil, loc, SEEK_SET);
    return;
  }
  
  int i;
  for (i = 0; i < n; i++) {
    read_d(fil, var + i);
  }
  
  check_ln_delimiter(fil);
  
  c = fgetc(fil);
  
  if (c != '}') {
    shErrStackPush("%s: next element is not an array - wrong elements read", name);
    fseek(fil, loc, SEEK_SET);
    return;
  }
  
    return;
}

void read_ld_heap(FILE *fil, long double *var, const int n) {
  
  char *name = "read_ld_heap";
  
  check_ln_delimiter(fil);
  
  int loc;
  loc = ftell(fil);
  
  char c;
  c = fgetc(fil);
  
  if (c != '{') {
    shErrStackPush("%s: next element is not an array -- no elements were read", name);
    fseek(fil, loc, SEEK_SET);
    return;
  }
  
  int i;
  for (i = 0; i < n; i++) {
    read_ld(fil, var + i);
  }
  
  check_ln_delimiter(fil);
  
  c = fgetc(fil);
  
  if (c != '}') {
    shErrStackPush("%s: next element is not an array - wrong elements read", name);
    fseek(fil, loc, SEEK_SET);
    return;
  }
  
    return;
}

void read_s_heap(FILE *fil, char **var, const int n) {
  
  char *name = "read_s_heap";
  
  check_ln_delimiter(fil);
  
  int loc;
  loc = ftell(fil);
  
  char c;
  c = fgetc(fil);
  
  if (c != '{') {
    shErrStackPush("%s: next element is not an array -- no elements were read", name);
    fseek(fil, loc, SEEK_SET);
    return;
  }
  
  int i;
  for (i = 0; i < n; i++) {
    read_s(fil, var + i);
  }
  
  check_ln_delimiter(fil);
  
  c = fgetc(fil);
  
  if (c != '}') {
    shErrStackPush("%s: next element is not an array - wrong elements read", name);
    fseek(fil, loc, SEEK_SET);
    return;
  }
  
    return;
}

void read_enum(FILE *fil, int *var, char **enum_str_list, const int n) {


  char *name = "read_enum";

  int loc;
  loc = ftell(fil);

  char *svar;
  svar = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  read_s(fil, svar);

  int i, j, l, l_svar, l_senum, diff;
  j = n;

  l_svar = strlen(svar);
  for (i = 0; i < n; i++) {
    l_senum = strlen(enum_str_list[i]);
    l = MAX(l_senum, l_svar);
    diff = memcmp(enum_str_list[i], svar, l);
    if (diff == 0) {
      j = i;
    }
  }

  if (j == n) {
    shErrStackPush("%s: the variable is not of the provided enum type", name);
    fseek(fil, loc, SEEK_SET);
    return;
  }
  
  *var = j;
  return;

}

  

void skip_var(FILE *fil) {
  
  check_ln_delimiter(fil);

  char c;
  c = fgetc(fil);

  if (c == '{') {
    /* we have confronted an array
       and we will read until it ends
       i.e. when we reach '}'
       Note that this way we cannot read
       arrays of arrays
    */
    while (c != '}') {
      c = fgetc(fil);
    }

    fseek(fil, -1, SEEK_CUR);

  } else {

    char *var = thMalloc(MX_STRING_LEN * sizeof(char));
    read_s(fil, var);
    
    thFree(var);
  }

  check_ln_delimiter(fil);
}

void skip_vars(FILE *fil, const int n) {

  int i;

  for (i = 0; i < n; i++) {
    skip_var(fil);
  }

  return;

}

static int par_check_struct_name(FILE *fil, char *stru) {
  
  char *name = "par_check_struct_name";

  char *strname;
  strname = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  read_s(fil, strname);

  int l, l1, l2;
  l1 = strlen(strname);
  l2 = strlen(stru);

  l = MIN(l1, l2);

  int diff;
  diff = memcmp(strname, stru, l);

  thFree(strname);

  if (diff == 0) {
    return(0);
  } else {
    return(-1);
  }

}
  
  
/* 
   the following functions read specific structures in par files
   the list of structures and their definitions are available 
   in shParTypes.h
*/

void read_FRAMEINFO(FILE *fil, FRAMEINFO *var /* output */) 
{ 

  char *nameofstruct = "FRAMEINFO";
  char name[100] = "read_";
  strcat(name, nameofstruct);

  //irun = IMAGINGRUN *thCalloc(1, sizeof(IMAGINGRUN));

  int status;
  
  status = feof(fil);

  if (status !=  0) {
    shErrStackPush("%s: reached EOF", name);
    return;
  }

  status = par_check_struct_name(fil, nameofstruct);

  if (status != 0) {
    shErrStackPush("%s: the next stuctures is not a %s", name, nameofstruct);
    return;
  }
   
  /* now reading the real structure */

    read_i(fil, &(var->frame));          
    read_d(fil, &(var->mjd));            
    read_d(fil, &(var->ra));             
    read_d(fil, &(var->dec));            
    read_f(fil, &(var->spa));            
    read_f(fil, &(var->ipa));            
    read_f(fil, &(var->ipaRate));        
    read_d(fil, &(var->az));             
    read_d(fil, &(var->alt));            
    /*} FRAMEINFO; */

    return;
}

void read_RUNMARK(FILE *fil, RUNMARK *var /* output */) 
{ 

  char *nameofstruct = "RUNMARK";
  char name[100] = "read_";
  strcat(name, nameofstruct);

  //irun = IMAGINGRUN *thCalloc(1, sizeof(IMAGINGRUN));

  int status;
  
  status = feof(fil);

  if (status !=  0) {
    shErrStackPush("%s: reached EOF", name);
    return;
  }

  status = par_check_struct_name(fil, nameofstruct);

  if (status != 0) {
    shErrStackPush("%s: the next stuctures is not a %s", name, nameofstruct);
    return;
  }
   
  /* now reading the real enum */

  int i, n_enum;
  char **enum_str;
  enum_str = (char **) thCalloc(n_enum, sizeof(char *));
  for (i = 0; i < n_enum; i++) {
    enum_str[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }

  enum_str[0] = "START";
  enum_str[1] = "END";
    /*} RUNMARK; */
   
  int ivar;
  read_enum(fil, &ivar, enum_str, n_enum);
  *var = ivar;
  
  return;

}

void read_IMAGINGRUN(FILE *fil, IMAGINGRUN *var /* output */) 
{ 

  char *nameofstruct = "IMAGINGRUN";
  char name[100] = "read_";
  strcat(name, nameofstruct);

  //irun = IMAGINGRUN *thCalloc(1, sizeof(IMAGINGRUN));

  int status;
  
  status = feof(fil);

  if (status !=  0) {
    shErrStackPush("%s: reached EOF", name);
    return;
  }

  status = par_check_struct_name(fil, nameofstruct);

  if (status != 0) {
    shErrStackPush("%s: the next stuctures is not a %s", name, nameofstruct);
    return;
  }
   
  /* now reading the real structure */

  read_s(fil, var->program);
  read_i(fil, &(var->stripe));
  read_s(fil, var->strip);
  read_i(fil, &(var->run));
  read_s(fil, var->flavor);
  read_s(fil, var->sys_scn);
  read_d(fil, &(var->eqnx_scn));
  read_d(fil, &(var->node));
  read_d(fil, &(var->incl));
  read_d(fil, &(var->muStart));
  read_d(fil, &(var->muEnd));
  read_i(fil, &(var->lastFrame));
  read_d(fil, &(var->xBore));
  read_d(fil, &(var->yBore));
  read_s(fil, var->system);
  read_d(fil, &(var->equinox));
  read_d(fil, &(var->c_obs));
  read_d(fil, &(var->tracking));
  read_s(fil, var->quality);
  read_d(fil, &(var->mjd));
  read_RUNMARK(fil, &(var->mark));

  /* IMAGINGRUN */

  return;

}


void read_DFTYPE(FILE *fil, DFTYPE*var /* output */) 
{ 

  char *nameofstruct = "DFTYPE";
  char name[100] = "read_";
  strcat(name, nameofstruct);

  //irun = IMAGINGRUN *thCalloc(1, sizeof(IMAGINGRUN));

  int status;
  
  status = feof(fil);

  if (status !=  0) {
    shErrStackPush("%s: reached EOF", name);
    return;
  }

  status = par_check_struct_name(fil, nameofstruct);

  if (status != 0) {
    shErrStackPush("%s: the next stuctures is not a %s", name, nameofstruct);
    return;
  }
   
  /* now reading the real enum */

  int i, n_enum;
  n_enum = 9;
  
  char **enum_str;
  enum_str = (char **) thCalloc(n_enum, sizeof(char *));
  for (i = 0; i < n_enum; i++) {
    enum_str[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }

  enum_str[0] = "DRKCUR"; 		 // low level dark current 
  enum_str[1] = "HOTCOL"; 		 // Hot column - bad dark current 
  enum_str[2] = "DEPCOL"; 		 // Depressed column
  enum_str[3] = "CTECOL"; 		 // Bad charge transfer efficiency 
  enum_str[4] = "BLKCOL"; 		 // blocked column - fix with flatfield 
  enum_str[5] = "BADBLK"; 		 // badly blocked col - just fill in
  enum_str[6] = "HOTSPOT"; 		 // a single hot pixel 
  enum_str[7] = "NOISY"; 		 // a noisy column 
  enum_str[8] = "TRAP";  		 // location of trap head 

  /*} DFTYPE;*/

  int ivar;
  read_enum(fil, &ivar, enum_str, n_enum);
  *var = ivar;
  
  return;

}


void read_CCDBC(FILE *fil, CCDBC*var /* output */) 
{ 

  char *nameofstruct = "CCDBC";
  char name[100] = "read_";
  strcat(name, nameofstruct);

  //irun = IMAGINGRUN *thCalloc(1, sizeof(IMAGINGRUN));

  int status;
  
  status = feof(fil);

  if (status !=  0) {
    shErrStackPush("%s: reached EOF", name);
    return;
  }

  status = par_check_struct_name(fil, nameofstruct);

  if (status != 0) {
    shErrStackPush("%s: the next stuctures is not a %s", name, nameofstruct);
    return;
  }
   
  /* now reading the real structure */

  read_s(fil, var->program); 	 // program name 'stare_0123'
  read_i(fil, &(var-> camRow));      	 // camRow mt=0 spec=0 dsc=9
  read_i(fil, &(var-> camCol));        	 // camCol mt=0 spec=1234 dsc=9
  read_i(fil, &(var-> dfcol0));        	 // starting column of this defect (trimmed image
  
  read_i(fil, &(var-> dfncol));        	 // number of columns in this defect
  read_i(fil, &(var-> dfrow0));        	 // starting row of this defect (trimmed image)
  read_i(fil, &(var-> dfnrow));        	 // number of rows in this defect
  read_DFTYPE(fil, &(var->dftype));     	 // defect type
  read_i(fil, &(var-> astroline));     	 // 1 if astroline to pay attention, 0 else 
  
  /*} CCDBC; */

  return;

}

void read_CCDGEOMETRY(FILE *fil, CCDGEOMETRY*var /* output */) 
{ 

  char *nameofstruct = "CCDGEOMETRY";
  char name[100] = "read_";
  strcat(name, nameofstruct);

  //irun = IMAGINGRUN *thCalloc(1, sizeof(IMAGINGRUN));

  int status;
  
  status = feof(fil);

  if (status !=  0) {
    shErrStackPush("%s: reached EOF", name);
    return;
  }

  status = par_check_struct_name(fil, nameofstruct);

  if (status != 0) {
    shErrStackPush("%s: the next stuctures is not a %s", name, nameofstruct);
    return;
  }
   
  /* now reading the real structure */

  read_i(fil, &(var->dewarID));
  read_i(fil, &(var->camRow));
  read_i(fil, &(var->camCol));
  read_d(fil, &(var->rowRef));
  read_d(fil, &(var->colRef));
  read_d(fil, &(var->xc));
  read_d(fil, &(var->yc));
  read_d(fil, &(var->theta));
  read_d(fil, &(var->sfactc));
  read_d(fil, &(var->pscale));
  read_d(fil, &(var->xmag));
  read_i(fil, &(var->rowOffset));
  read_i(fil, &(var->frameOffset));
  read_d(fil, &(var->dRow0));
  read_d(fil, &(var->dRow1));
  read_d(fil, &(var->dRow2));
  read_d(fil, &(var->dRow3));
  read_d(fil, &(var->dCol0));
  read_d(fil, &(var->dCol1));
  read_d(fil, &(var->dCol2));
  read_d(fil, &(var->dCol3));
  /*} CCDGEOMETRY; */

  return;

}

void read_DEWARGEOMETRY(FILE *fil, DEWARGEOMETRY*var /* output */) 
{ 

  char *nameofstruct = "DEWARGEOMETRY";
  char name[100] = "read_";
  strcat(name, nameofstruct);

  //irun = IMAGINGRUN *thCalloc(1, sizeof(IMAGINGRUN));

  int status;
  
  status = feof(fil);

  if (status !=  0) {
    shErrStackPush("%s: reached EOF", name);
    return;
  }

  status = par_check_struct_name(fil, nameofstruct);

  if (status != 0) {
    shErrStackPush("%s: the next stuctures is not a %s", name, nameofstruct);
    return;
  }
   
  /* now reading the real structure */

  read_i(fil, &(var-> dewarID));
  read_d(fil, &(var-> xb));
  read_d(fil, &(var-> yb));
  read_d(fil, &(var-> thetai));
  read_d(fil, &(var-> sfacti));
  /*} DEWARGEOMETRY; */

  return;

}


void read_CCDCONFIG(FILE *fil,CCDCONFIG*var /* output */) 
{ 

  char *nameofstruct = "CCDCONFIG";
  char name[100] = "read_";
  strcat(name, nameofstruct);

  //irun = IMAGINGRUN *thCalloc(1, sizeof(IMAGINGRUN));

  int status;
  
  status = feof(fil);

  if (status !=  0) {
    shErrStackPush("%s: reached EOF", name);
    return;
  }

  status = par_check_struct_name(fil, nameofstruct);

  if (status != 0) {
    shErrStackPush("%s: the next stuctures is not a %s", name, nameofstruct);
    return;
  }
   
  /* now reading the real structure */

  read_s(fil, var->program);
  read_i(fil, &(var->camRow));
  read_i(fil, &(var->camCol));
  read_i(fil, &(var->rowBinning));
  read_i(fil, &(var->colBinning));
  read_i(fil, &(var->amp0));
  read_i(fil, &(var->amp1));
  read_i(fil, &(var->amp2));
  read_i(fil, &(var->amp3));
  read_i(fil, &(var->nrows));
  read_i(fil, &(var->ncols));
  read_i(fil, &(var->sPreBias0 ));
  read_i(fil, &(var->nPreBias0 ));
  read_i(fil, &(var->sPostBias0 ));
  read_i(fil, &(var->nPostBias0 ));
  read_i(fil, &(var->sOverScan0 ));
  read_i(fil, &(var->nOverScan0 ));
  read_i(fil, &(var->sMapOverScan0 ));
  read_i(fil, &(var->nMapOverScan0 ));
  read_i(fil, &(var->sOverScanRows0 ));
  read_i(fil, &(var->nOverScanRows0 ));
  read_i(fil, &(var->sDataSec0 ));
  read_i(fil, &(var->nDataSec0 ));
  read_i(fil, &(var->sDataRow0 ));
  read_i(fil, &(var->nDataRow0 ));
  read_i(fil, &(var->sCCDRowSec0 ));
  read_i(fil, &(var->sCCDColSec0 ));
  read_i(fil, &(var->sPreBias1 ));
  read_i(fil, &(var->nPreBias1 ));
  read_i(fil, &(var->sPostBias1 ));
  read_i(fil, &(var->nPostBias1 ));
  read_i(fil, &(var->sOverScan1 ));
  read_i(fil, &(var->nOverScan1 ));
  read_i(fil, &(var->sMapOverScan1 ));
  read_i(fil, &(var->nMapOverScan1 ));
  read_i(fil, &(var->sOverScanRows1 ));
  read_i(fil, &(var->nOverScanRows1 ));
  read_i(fil, &(var->sDataSec1 ));
  read_i(fil, &(var->nDataSec1 ));
  read_i(fil, &(var->sDataRow1 ));
  read_i(fil, &(var->nDataRow1 ));
  read_i(fil, &(var->sCCDRowSec1 ));
  read_i(fil, &(var->sCCDColSec1 ));
  read_i(fil, &(var->sPreBias2 ));
  read_i(fil, &(var->nPreBias2 ));
  read_i(fil, &(var->sPostBias2 ));
  read_i(fil, &(var->nPostBias2 ));
  read_i(fil, &(var->sOverScan2 ));
  read_i(fil, &(var->nOverScan2 ));
  read_i(fil, &(var->sMapOverScan2 ));
  read_i(fil, &(var->nMapOverScan2 ));
  read_i(fil, &(var->sOverScanRows2 ));
  read_i(fil, &(var->nOverScanRows2 ));
  read_i(fil, &(var->sDataSec2 ));
  read_i(fil, &(var->nDataSec2 ));
  read_i(fil, &(var->sDataRow2 ));
  read_i(fil, &(var->nDataRow2 ));
  read_i(fil, &(var->sCCDRowSec2 ));
  read_i(fil, &(var->sCCDColSec2 ));
  read_i(fil, &(var->sPreBias3 ));
  read_i(fil, &(var->nPreBias3 ));
  read_i(fil, &(var->sPostBias3 ));
  read_i(fil, &(var->nPostBias3 ));
  read_i(fil, &(var->sOverScan3 ));
  read_i(fil, &(var->nOverScan3 ));
  read_i(fil, &(var->sMapOverScan3 ));
  read_i(fil, &(var->nMapOverScan3 ));
  read_i(fil, &(var->sOverScanRows3 ));
  read_i(fil, &(var->nOverScanRows3 ));
  read_i(fil, &(var->sDataSec3 ));
  read_i(fil, &(var->nDataSec3 ));
  read_i(fil, &(var->sDataRow3 ));
  read_i(fil, &(var->nDataRow3 ));
  read_i(fil, &(var->sCCDRowSec3 ));
  read_i(fil, &(var->sCCDColSec3 ));
  read_i(fil, &(var->sPreBias0good ));
  read_i(fil, &(var->nPreBias0good ));
  read_i(fil, &(var->sPostBias0good ));
  read_i(fil, &(var->nPostBias0good ));
  read_i(fil, &(var->sOverScan0good ));
  read_i(fil, &(var->nOverScan0good ));
  read_i(fil, &(var->sMapOverScan0good ));
  read_i(fil, &(var->nMapOverScan0good ));
  read_i(fil, &(var->sOverScanRows0good ));
  read_i(fil, &(var->nOverScanRows0good ));
  read_i(fil, &(var->sDataSec0good ));
  read_i(fil, &(var->nDataSec0good ));
  read_i(fil, &(var->sDataRow0good ));
  read_i(fil, &(var->nDataRow0good ));
  read_i(fil, &(var->sCCDRowSec0good ));
  read_i(fil, &(var->sCCDColSec0good ));
  read_i(fil, &(var->sPreBias1good ));
  read_i(fil, &(var->nPreBias1good ));
  read_i(fil, &(var->sPostBias1good ));
  read_i(fil, &(var->nPostBias1good ));
  read_i(fil, &(var->sOverScan1good ));
  read_i(fil, &(var->nOverScan1good ));
  read_i(fil, &(var->sMapOverScan1good ));
  read_i(fil, &(var->nMapOverScan1good ));
  read_i(fil, &(var->sOverScanRows1good ));
  read_i(fil, &(var->nOverScanRows1good ));
  read_i(fil, &(var->sDataSec1good ));
  read_i(fil, &(var->nDataSec1good ));
  read_i(fil, &(var->sDataRow1good ));
  read_i(fil, &(var->nDataRow1good ));
  read_i(fil, &(var->sCCDRowSec1good ));
  read_i(fil, &(var->sCCDColSec1good ));
  read_i(fil, &(var->sPreBias2good ));
  read_i(fil, &(var->nPreBias2good ));
  read_i(fil, &(var->sPostBias2good ));
  read_i(fil, &(var->nPostBias2good ));
  read_i(fil, &(var->sOverScan2good ));
  read_i(fil, &(var->nOverScan2good ));
  read_i(fil, &(var->sMapOverScan2good ));
  read_i(fil, &(var->nMapOverScan2good ));
  read_i(fil, &(var->sOverScanRows2good ));
  read_i(fil, &(var->nOverScanRows2good ));
  read_i(fil, &(var->sDataSec2good ));
  read_i(fil, &(var->nDataSec2good ));
  read_i(fil, &(var->sDataRow2good ));
  read_i(fil, &(var->nDataRow2good ));
  read_i(fil, &(var->sCCDRowSec2good ));
  read_i(fil, &(var->sCCDColSec2good ));
  read_i(fil, &(var->sPreBias3good ));
  read_i(fil, &(var->nPreBias3good ));
  read_i(fil, &(var->sPostBias3good ));
  read_i(fil, &(var->nPostBias3good ));
  read_i(fil, &(var->sOverScan3good ));
  read_i(fil, &(var->nOverScan3good ));
  read_i(fil, &(var->sMapOverScan3good ));
  read_i(fil, &(var->nMapOverScan3good ));
  read_i(fil, &(var->sOverScanRows3good ));
  read_i(fil, &(var->nOverScanRows3good ));
  read_i(fil, &(var->sDataSec3good ));
  read_i(fil, &(var->nDataSec3good ));
  read_i(fil, &(var->sDataRow3good ));
  read_i(fil, &(var->nDataRow3good ));
  read_i(fil, &(var->sCCDRowSec3good ));
  read_i(fil, &(var->sCCDColSec3good ));
  /*} CCDCONFIG */

  return;

}


void read_LINEARITY_TYPE(FILE *fil, LINEARITY_TYPE*var /* output */) 
{ 
  
  char *nameofstruct = "LINEARITY_TYPE";
  char name[100] = "read_";
  strcat(name, nameofstruct);
  
  //irun = IMAGINGRUN *thCalloc(1, sizeof(IMAGINGRUN));
  
  int status;
  
  status = feof(fil);
  
  if (status !=  0) {
    shErrStackPush("%s: reached EOF", name);
    return;
  }
  
  status = par_check_struct_name(fil, nameofstruct);
  
  if (status != 0) {
    shErrStackPush("%s: the next stuctures is not a %s", name, nameofstruct);
    return;
  }
   
  /* now reading the real enum */
  
  int i, n_enum;
  n_enum = 6;
  char **enum_str;
  enum_str = (char **) thCalloc(n_enum, sizeof(char *));
  for (i = 0; i < n_enum; i++) {
    enum_str[i] = (char *) thCalloc(MX_STRING_LEN, sizeof(char));
  }
    
  enum_str[0] = "LINEAR_ILLEGAL"; // = 666,  // illegal type; this amp isn't used
  enum_str[1] = "LINEAR_NONE"; //= 1,      // C/R = 1
  enum_str[2] = "LINEAR_QUADRATIC";     // C/R = 1 + c1 R
  enum_str[3] = "LINEAR_LOG";           // C/R = 1 + c1 lg(R)
  enum_str[4] = "LINEAR_THRESHLOG";     // C/R = 1 + (R<c2 ? 0 : c1 lg(R/c2))
  enum_str[5] = "LINEAR_NTYPE";          // number of functional forms

  /*} LINEARITY_TYPE; */
  
  int ivar;
  read_enum(fil, &ivar, enum_str, n_enum);
  *var = ivar;

  return;
  
}


void read_ECALIB(FILE *fil, ECALIB*var /* output */) 
{ 

  char *nameofstruct = "ECALIB";
  char name[100] = "read_";
  strcat(name, nameofstruct);

  //irun = IMAGINGRUN *thCalloc(1, sizeof(IMAGINGRUN));

  int status;
  
  status = feof(fil);

  if (status !=  0) {
    shErrStackPush("%s: reached EOF", name);
    return;
  }

  status = par_check_struct_name(fil, nameofstruct);

  if (status != 0) {
    shErrStackPush("%s: the next stuctures is not a %s", name, nameofstruct);
    return;
  }
   
  /* now reading the real structure */

  read_s(fil, var->program);
  read_i(fil, &(var->camRow));
  read_i(fil, &(var->camCol));
  read_f(fil, &(var->readNoiseDN0));
  read_f(fil, &(var->fullWellDN0));
  read_f(fil, &(var->gain0));
  read_f(fil, &(var->biasLevel0));
  read_f_heap(fil, var->DN0, 13);       /* array */
  read_f_heap(fil, var->linearity0, 13);/* array */
  read_f(fil, &(var->readNoiseDN1));
  read_f(fil, &(var->fullWellDN1));
  read_f(fil, &(var->gain1));
  read_f(fil, &(var->biasLevel1));
  read_f_heap(fil, var->DN1, 13);       /* array */
  read_f_heap(fil, var->linearity1, 13);/* array */
  read_f(fil, &(var->readNoiseDN2));
  read_f(fil, &(var->fullWellDN2));
  read_f(fil, &(var->gain2));
  read_f(fil, &(var->biasLevel2));
  read_f_heap(fil, var->DN2, 13);       /* array */
  read_f_heap(fil, var->linearity2, 13);/* array */
  read_f(fil, &(var->readNoiseDN3));
  read_f(fil, &(var->fullWellDN3));
  read_f(fil, &(var->gain3));
  read_f(fil, &(var->biasLevel3));
  read_f_heap(fil, var->DN3, 13);        /* array */
  read_f_heap(fil, var->linearity3, 13); /* array */

  /* } ECALIB; */

  return;

}


void read_RUNLIST(FILE *fil, RUNLIST *var /* output */) 
{ 

  char *nameofstruct = "RUNLIST";
  char name[100] = "read_";
  strcat(name, nameofstruct);

  //irun = IMAGINGRUN *thCalloc(1, sizeof(IMAGINGRUN));

  int status;
  
  status = feof(fil);

  if (status !=  0) {
    shErrStackPush("%s: reached EOF", name);
    return;
  }

  status = par_check_struct_name(fil, nameofstruct);

  if (status != 0) {
    shErrStackPush("%s: the next stuctures is not a %s", name, nameofstruct);
    return;
  }
   
  /* now reading the real structure */

  read_i(fil, &(var->run));
  read_i(fil, &(var->mjd));
  read_s(fil, var->datestring);  /* array */
  read_i(fil, &(var->stripe));
                    // Stripe number, 0 if not on a survey stripe
  read_s(fil, var->strip);  /* array */
                    // N=north, S=south, O=overlap
  read_d(fil, &(var->xbore));
                    // Boresight offset perpendicular to great circle in degrees
  read_i(fil, &(var->field_ref));
                    // Field reference for mu_ref,mjd_ref
  read_i(fil, &(var->lastfield));
                    // Last field number
  read_s(fil, var->flavor);  /* array */
                    // science, engineering, bias
  read_i(fil, &(var->xbin));
                    // CCD binning in X (perpendicular to scan direction)
  read_i(fil, &(var->ybin));
                    // CCD binning in Y (scan direction)
  read_d(fil, &(var->mjd_ref));
                    // Fractional MJD at row 0 of reference frame
  read_d(fil, &(var->mu_ref));
                    // Mu position at reference field number
  read_i(fil, &(var->linestart));
                    // Linestart rate in microsec between each (binned) row
  read_d(fil, &(var->tracking));
                    // Tracking rate in arcsec/sec
  read_d(fil, &(var->node));
                    // Node of great circle (RA on the J2000 equator)
  read_d(fil, &(var->incl));
                    // Inclination of great circle (relative to J2000 equator)
  read_s(fil, var->comments);  /* array */
  read_f(fil, &(var->qterm));
                    // Quadratic term for astrometric solution in arcsec/hr^2
  read_f(fil, &(var->maxmuresid));
                    // Max arcsec residual from great circle in scan direction
  read_f(fil, &(var->maxnuresid));
                    // Max arcsec residual from great circle in x-scan direction
  /* } RUNLIST; */

  return;

}
