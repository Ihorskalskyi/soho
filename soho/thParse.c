#include "stdio.h"
#include "time.h"
#include "thParse.h"
#include "thCalib.h"

static unsigned int get_myseed();
static unsigned int generate_seed(unsigned int seed0);

RET_CODE simparser(int argc, char **argv, SIM_PARSED_INPUT *input) {

char *name = "simparser";
shAssert(input != NULL);

int prev_optind = optind;
optind = 1;

int mycamcol = DEFAULT_CAMCOL, myband = DEFAULT_BAND, mysmearobjc = 0;
char mybandname = '\0';
int mynrow = DEFAULT_NROW, myncol = DEFAULT_NCOL;
int myrun = DEFAULT_RUN, myrerun = DEFAULT_RERUN;
int mystripe = DEFAULT_STRIPE, myfield = DEFAULT_FIELD;
float myexp_inner_lumcut = DEFAULT_EXP_INNER_LUMCUT, myexp_outer_lumcut = DEFAULT_EXP_OUTER_LUMCUT;
float mydev_inner_lumcut = DEFAULT_DEV_INNER_LUMCUT, mydev_outer_lumcut = DEFAULT_DEV_OUTER_LUMCUT;
float mysmearf = 0.0;
int mynstar = (int) DEFAULT_SIM_NSTAR;
int myngalaxy = (int) DEFAULT_SIM_NGALAXY;
int mynlrg = -1;
float mystar_threshold = (float) DEFAULT_SIM_STAR_THRESHOLD;
float mygalaxy_threshold = (float) DEFAULT_SIM_GALAXY_THRESHOLD;
float mydeV_purity_threshold = (float) DEFAULT_SIM_DEV_PURITY_THRESHOLD;
float myexp_purity_threshold = (float) DEFAULT_SIM_EXP_PURITY_THRESHOLD;
int mycD_classify = 0, myforce_cD_classify = 0;
GCLASS mycdclass = DEFAULT_CDCLASS;
struct option longopts[] = {
	{"setup",     required_argument, NULL, 'a'},
	{"camcol",    required_argument, NULL, 'b'},
	{"band",      required_argument, NULL, 'c'},
	{"noobjcfile", no_argument, NULL, 'd'},
	{"framefile", required_argument, NULL, 'e'}, 
	{"ff",	      required_argument, NULL, 'f'},
	{"smearobjc", no_argument, NULL, 'g'},
	{"sky", required_argument, NULL, 'h'},
	{"exp",  required_argument, NULL, 'i'},
	{"deV",  required_argument, NULL, 'j'},
	{"star", required_argument, NULL, 'k'}, 
 	{"nrow",    required_argument, NULL, 'l'},
	{"ncol",    required_argument, NULL, 'm'},	
	{"run", 	required_argument, NULL, 'n'},
	{"rerun", required_argument, NULL, 'o'},
	{"field", required_argument, NULL, 'p'},
	{"stripe", required_argument, NULL, 'q'},
	{"seed", required_argument, NULL, 'r'},
	/* added on April 27, 2015 */
	{"exp_inner_lumcut", required_argument, NULL, 's'},
	{"exp_outer_lumcut", required_argument, NULL, 't'},
	{"dev_inner_lumcut", required_argument, NULL, 'u'},
	{"dev_outer_lumcut", required_argument, NULL, 'v'},
	{"readskyfile", no_argument, NULL, 'w'},
	{"smearf", required_argument, NULL, 'x'},
	{"nstar", required_argument, NULL, 'y'},
	{"ngalaxy", required_argument, NULL, 'z'},
	{"nlrg", required_argument, NULL, '4'},
	{"star_threshold", required_argument, NULL, '0'},
	{"galaxy_threshold", required_argument, NULL, '1'},
	{"deV_purity_threshold", required_argument, NULL, '2'},
	{"exp_purity_threshold", required_argument, NULL, '3'},
	{"gcsource", required_argument, NULL, '5'},
	{"cD_classify", no_argument, NULL, '6'}, 
 	{"cD_force", no_argument, NULL, '7'},
	{"cdclass", required_argument, NULL, '8'},
	{"demand", required_argument, NULL, '9'}, 
	{"fpC_do_float", no_argument, NULL, '{'}, 
	{"gaussian_noise", required_argument, NULL, '}'}, 
	{ 0, 0, 0, 0 }
};

char *mysetupfile = NULL, *myframefile = NULL, *myargv = NULL;
#if USE_FNAL_OBJC_IO
char *wobjc_name = "FNAL_OBJC_IO";
#else
char *wobjc_name = "OBJC_IO";
#endif
CHAIN *myobjclist = shChainNew(wobjc_name);
WOBJC_IO *myobjc = NULL;
SKYPARS *myskyobjc = NULL;
FRAMEFILES *myff = NULL;
myargv = thCalloc(MX_STRING_LEN, sizeof(char));
int readobjcfile = 1, readskyfile = 0;
unsigned int myseed = get_myseed();
GC_SOURCE mygcsource =  UNKNOWN_GC_SOURCE;
char *mystring = thCalloc(MX_STRING_LEN, sizeof(char));
int mydemandcode = NO_DEMAND_CODE;
SKYPARS **myskylist = NULL;
PSF_CONVOLUTION_TYPE mygalaxy_psf = DEFAULT_GALAXY_PSF_CONVOLUTION;
int myfpC_do_float = DEFAULT_SIM_FPC_FLOAT;
float mygaussian_noise_dn = (float) DEFAULT_GAUSSIAN_NOISE_DN;
RET_CODE status;
TYPE ttype = UNKNOWN_SCHEMA;
char c, *stype = NULL;
while ((c = getopt_long_only(argc, argv, "", longopts, NULL)) != -1) {
    switch (c) {
	case 'a':
		mysetupfile = optarg;
		break;	
	case 'b':
		sscanf(optarg, "%d", &mycamcol);
		break;
	case 'c': 
		sscanf(optarg, "%c", &mybandname);
		break;
	case 'g':
		mysmearobjc = 1;
		break;
	case 'd':
		readobjcfile = 0;
		break;
	case 'e':
		myframefile = optarg;
		break;
	case 'f':
		strcpy(myargv, optarg);
		ffparser(myargv, &myff);
		break;
	case 'i':
        	strcpy(myargv, optarg);
 		expparser(myargv, &myobjc);
		shChainElementAddByPos(myobjclist, myobjc, wobjc_name, TAIL, AFTER);
        	break;
    	case 'j':
		strcpy(myargv, optarg);
 		deVparser(myargv, &myobjc);
		shChainElementAddByPos(myobjclist, myobjc, wobjc_name, TAIL, AFTER);
       		 break;
	case 'k':
		strcpy(myargv, optarg);
 		starparser(myargv, &myobjc);
		shChainElementAddByPos(myobjclist, myobjc, wobjc_name, TAIL, AFTER);
       		 break;
	case 'h':
		strcpy(myargv, optarg);
		skyparser(myargv, &myskyobjc);
		break;
	case 'l': 
		sscanf(optarg, "%d", &mynrow);
		break;
	case 'm':
		sscanf(optarg, "%d", &myncol);
		break;
	case 'n':
		sscanf(optarg, "%d", &myrun);
		break;
	case 'o':
		sscanf(optarg, "%d", &myrerun);
		break;
	case 'p':
		sscanf(optarg, "%d", &myfield);
		break;
	case 'q':
		sscanf(optarg, "%d", &mystripe);
		break; 
	case 'r':
		sscanf(optarg, "%u", &myseed);
		break;
	case 's':
		sscanf(optarg, "%g", &myexp_inner_lumcut);
		break;
	case 't':
		sscanf(optarg, "%g", &myexp_outer_lumcut);
		break;
	case 'u':
		sscanf(optarg, "%g", &mydev_inner_lumcut);
		break;
	case 'v':
		sscanf(optarg, "%g", &mydev_outer_lumcut);
		break;
	case 'w':
		readskyfile = 1;
		break;
	case 'x':
		sscanf(optarg, "%g", &mysmearf);
		break;
	case 'y':
		sscanf(optarg, "%d", &mynstar);
		break;
	case 'z':
		sscanf(optarg, "%d", &myngalaxy);
		break;
	case '0':
		sscanf(optarg, "%g", &mystar_threshold);
		break;
	case '1':
		sscanf(optarg, "%g", &mygalaxy_threshold);
		break;
	case '2':
		sscanf(optarg, "%g", &mydeV_purity_threshold);
		break;
	case '3':
		sscanf(optarg, "%g", &myexp_purity_threshold);
		break;
	case '4':
		sscanf(optarg, "%d", &mynlrg);
		break;
	case '5':
		strcpy(mystring, optarg);
		stype = "GC_SOURCE";
		ttype = shTypeGetFromName(stype);
		if (ttype == UNKNOWN_SCHEMA) {
			thError("%s: ERROR - schema for '%s' not defined", name, stype);
			return(SH_GENERIC_ERROR);
		} else if (shValueGetFromEnumName(stype, mystring, (int *) &mygcsource) == NULL) {
			thError("%s: WARNING - '%s' could not be interpreted", name, mystring);
		}
		break;
	case '6': 
		mycD_classify = 1;
		break;
	case '7':
		myforce_cD_classify = 1;
		break;
	case '8':
		strcpy(mystring, optarg);
		stype = "GCLASS";
		ttype = shTypeGetFromName(stype);
		if (ttype == UNKNOWN_SCHEMA) {
			thError("%s: ERROR - schema for '%s' not defined", name, stype);
			return(SH_GENERIC_ERROR);
		} else if (shValueGetFromEnumName(stype, mystring, (int *) &mycdclass) == NULL) {
			thError("%s: WARNING - '%s' could not be interpreted", name, mystring);
		}
		break;
	case '9':
		sscanf(optarg, "%d", &mydemandcode);	
       		 break;
	case '{':
		myfpC_do_float = 1;
		break;
	case '}': 
		sscanf(optarg, "%g", &mygaussian_noise_dn);
		break;
	case 0:     /* getopt_long() set a variable, just keep going */
       		 break;
#if 0
    case 1:
        /*
         * Use this case if getopt_long() should go through all
         * arguments. If so, add a leading '-' character to optstring.
         * Actual code, if any, goes here.
         */
        break;
#endif
    	case ':':   /* missing option argument */
        	fprintf(stderr, "%s: ERROR - %s: option `-%c' requires an argument\n",
             	  	 name, argv[0], optopt);
       		 break;
   	 case '?':
  	 default:    /* invalid option */
        	fprintf(stderr, "%s: ERROR - %s: option `-%c' is invalid: ignored\n",
                	name, argv[0], optopt);
        	break;
    }
}

if (mydemandcode != NO_DEMAND_CODE) {
	if (myobjclist == NULL) myobjclist = shChainNew(wobjc_name);
 	status = demandparser(mydemandcode, myobjclist, &myskylist, &mygalaxy_psf, &mycdclass);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not generate object and sky list on demand (code = %d)", name, mydemandcode);
		return(status);
	}
} 

status = thBandGetFromName(mybandname, &myband);
if (status != SH_SUCCESS) {
	thError("%s: WARNING - could not retrieve band from band name (%c)", name, mybandname);
}
input->setupfile = mysetupfile; 
input->camcol = mycamcol; 
input->readobjcfile = readobjcfile;
input->framefile = myframefile;
input->ff = myff; 
input->smearobjc = mysmearobjc;
input->demand = mydemandcode; 
input->objclist = myobjclist; 
input->skylist = myskylist;
input->sky = myskyobjc;
input->band = myband; 
input->nrow = mynrow;
input->ncol = myncol;
input->field = myfield;
input->run = myrun;
input->rerun = myrerun;
input->stripe = mystripe;
input->seed = generate_seed(myseed);
/* added on April 27, 2015 */
input->exp_inner_lumcut = (THPIX) myexp_inner_lumcut;
input->exp_outer_lumcut = (THPIX) myexp_outer_lumcut;
input->dev_inner_lumcut = (THPIX) mydev_inner_lumcut;
input->dev_outer_lumcut = (THPIX) mydev_outer_lumcut;
/* added on August 11, 2015 */
input->readskyfile = readskyfile;
/* added on June 11, 2016 */
input->nstar = mynstar;
input->ngalaxy = myngalaxy;
input->nlrg = mynlrg;
input->star_threshold = mystar_threshold;
input->galaxy_threshold = mygalaxy_threshold;
input->deV_purity_threshold = mydeV_purity_threshold;
input->exp_purity_threshold = myexp_purity_threshold;
input->gcsource = mygcsource; /* galactic caliberation source */
input->do_cD_classify = mycD_classify; /* should i classify objects as cD's or not */
input->cdclass = mycdclass;
input->galaxypsf = mygalaxy_psf;
input->fpC_do_float = myfpC_do_float;
input->gaussian_noise_dn = mygaussian_noise_dn;
thFree(myargv);
optind = prev_optind;
return(SH_SUCCESS);
}

RET_CODE expparser(char *arg, WOBJC_IO **objc) {

int prev_optind = optind;
optind = 1;
int argc; char **argv;
argv = str_all_tokens(arg, " ", &argc);
float myre = 0.0, mycounts = 0.0, myxc = 0.0, myyc = 0.0, mye = 0.0, myphi = 0.0, myinner_lumcut = 0.0, myouter_lumcut = 0.0;
struct option longopts[] = {
	{"re", required_argument, NULL, 'r'},
	{"counts", required_argument, NULL, 'c'},
	{"xc",	required_argument, NULL, 'x'},
	{"yc", 	required_argument, NULL, 'y'},
	{"e", 	required_argument, NULL, 'e'},
	{"phi", required_argument, NULL, 'p'},
	{"inner_lumcut", required_argument, NULL, 'i'},
	{"outer_lumcut", required_argument, NULL, 'o'},
	{0, 0, 0, 0}
};

char c;
while ((c = getopt_long_only(argc, argv, "", longopts, NULL)) != -1) {
    switch (c) {
	case 'r':
		sscanf(optarg, "%g", &myre);
		break;
	case 'c':
		sscanf(optarg, "%g", &mycounts);
		break;
	case 'x':
		sscanf(optarg, "%g", &myxc);
		break;
	case 'y':
		sscanf(optarg, "%g", &myyc);
		break;
	case 'e':
		sscanf(optarg, "%g", &mye);
		break;
	case 'p':
		sscanf(optarg, "%g", &myphi);
		break;
	case 'i':
		sscanf(optarg, "%g", &myinner_lumcut);
		break;
	case 'o':
		sscanf(optarg, "%g", &myouter_lumcut);
		break;
    	case 0:     /* getopt_long() set a variable, just keep going */
       		break;
#if 0
    case 1:
        /*
         * Use this case if getopt_long() should go through all
         * arguments. If so, add a leading '-' character to optstring.
         * Actual code, if any, goes here.
         */
        break;
#endif
  	case ':':   /* missing option argument */
        	fprintf(stderr, "%s: option `-%c' requires an argument\n",
               		 argv[0], optopt);
       		break;
   	case '?':
   	default:    /* invalid option */
       		fprintf(stderr, "%s: option `-%c' is invalid: ignored\n",
               		 argv[0], optopt);
        	break;
    }	
}

int i;
WOBJC_IO *myobjc = thWObjcIoNew(NCOLOR);
myobjc->objc_type = OBJ_GALAXY;
for (i = 0; i < NCOLOR; i++) {
	myobjc->r_exp[i] = myre;
	myobjc->counts_exp[i] = mycounts;
	myobjc->rowc[i] = myxc;
	myobjc->colc[i] = myyc;
	myobjc->ab_exp[i] = pow(1.0-mye*mye, -0.5);
	myobjc->phi_exp[i] = myphi;
	myobjc->type[i] = OBJ_GALAXY;
	/* added on April 27, 2015 
	myobjc->inner_lumcut_exp[i] = myinner_lumcut;
	myobjc->outer_lumcut_exp[i] = myouter_lumcut; 	
	*/
	/* the other component */
	myobjc->r_deV[i] = 1.0;
	myobjc->counts_deV[i] = 0.0;
	myobjc->ab_deV[i] = 1.0;
	myobjc->phi_deV[i] = 0.0;
	myobjc->fracPSF[i] = 0.0; 

}

delete_all_tokens(argv, argc);
optind = prev_optind;
*objc = myobjc;
return(SH_SUCCESS);
}

RET_CODE deVparser(char *arg, WOBJC_IO **objc) {

int prev_optind = optind;
optind = 1;

int argc; char **argv;
argv = str_all_tokens(arg, " ", &argc);
float myre = 0.0, mycounts = 0.0, myxc = 0.0, myyc = 0.0, mye = 0.0, myphi = 0.0;
float myinner_lumcut = 0.0, myouter_lumcut = 0.0;
struct option longopts[] = {
	{"re", 	required_argument, NULL, 'r'},
	{"counts", required_argument, NULL, 'c'},
	{"xc", 	required_argument, NULL, 'x'},
	{"yc", 	required_argument, NULL, 'y'},
	{"e",	required_argument, NULL, 'e'},
	{"phi", required_argument, NULL, 'p'},
	{"inner_lumcut", required_argument, NULL, 'i'},
	{"outer_lumcut", required_argument, NULL, 'o'},
	{0, 0, 0, 0}
};

char c;
while ((c = getopt_long_only(argc, argv, "", longopts, NULL)) != -1) {
    switch (c) {
	case 'r':
		sscanf(optarg, "%g", &myre);
		break;
	case 'c':
		sscanf(optarg, "%g", &mycounts);
		break;
	case 'x':
		sscanf(optarg, "%g", &myxc);
		break;
	case 'y':
		sscanf(optarg, "%g", &myyc);
		break;
	case 'e':
		sscanf(optarg, "%g", &mye);
		break;
	case 'p':
		sscanf(optarg, "%g", &myphi);
		break;
	case 'i':
		sscanf(optarg, "%g", &myinner_lumcut);
		break;
	case 'o':
		sscanf(optarg, "%g", &myouter_lumcut);
		break;
    	case 0:     /* getopt_long() set a variable, just keep going */
       		break;
#if 0
    case 1:
        /*
         * Use this case if getopt_long() should go through all
         * arguments. If so, add a leading '-' character to optstring.
         * Actual code, if any, goes here.
         */
        break;
#endif
    	case ':':   /* missing option argument */
       		 fprintf(stderr, "%s: option `-%c' requires an argument\n",
               		 argv[0], optopt);
       		 break;
    	case '?':
    	default:    /* invalid option */
        	fprintf(stderr, "%s: option `-%c' is invalid: ignored\n",
               	argv[0], optopt);
        	break;
    }
}

int i;
WOBJC_IO *myobjc = thWObjcIoNew(NCOLOR);
myobjc->objc_type = OBJ_GALAXY;
for (i = 0; i < NCOLOR; i++) {
	myobjc->r_deV[i] = myre;
	myobjc->counts_deV[i] = mycounts;
	myobjc->rowc[i] = myxc;
	myobjc->colc[i] = myyc;
	myobjc->ab_deV[i] = pow(1.0-mye*mye, -0.5);
	myobjc->phi_deV[i] = myphi;
	myobjc->type[i] = OBJ_GALAXY;
	/* added on April 27, 2015
	myobjc->inner_lumcut_deV[i] = myinner_lumcut;
	myobjc->outer_lumcut_deV[i] = myouter_lumcut; 	
	*/
	/* the other component */
	myobjc->r_exp[i] = 100.0;
	myobjc->counts_exp[i] = 0.0;
	myobjc->ab_exp[i] = 1.0;
	myobjc->phi_exp[i] = 0.0;
	myobjc->fracPSF[i] = 1.0; 
}

delete_all_tokens(argv, argc);
optind = prev_optind;
*objc = myobjc;
return(SH_SUCCESS);
}

RET_CODE starparser(char *arg, WOBJC_IO **objc) {

int prev_optind = optind;
optind = 1;
int argc; char **argv;
argv = str_all_tokens(arg, " ", &argc);
float mycounts, myxc, myyc;
float myinner_lumcut = 0.0, myouter_lumcut = 0.0;
struct option longopts[] = {
	{"counts", required_argument, NULL, 'c'},
	{"xc",	required_argument, NULL, 'x'},
	{"yc", 	required_argument, NULL, 'y'},
	{"inner_lumcut", required_argument, NULL, 'i'},
	{"outer_lumcut", required_argument, NULL, 'o'},
	{0, 0, 0, 0}
};

char c;
while ((c = getopt_long_only(argc, argv, "", longopts, NULL)) != -1) {
    switch (c) {
	case 'c':
		sscanf(optarg, "%g", &mycounts);
		break;
	case 'x':
		sscanf(optarg, "%g", &myxc);
		break;
	case 'y':
		sscanf(optarg, "%g", &myyc);
		break;
	case 'i':
		sscanf(optarg, "%g", &myinner_lumcut);
		break;
	case 'o':
		sscanf(optarg, "%g", &myouter_lumcut);
		break;
    	case 0:     /* getopt_long() set a variable, just keep going */
       		break;
#if 0
    case 1:
        /*
         * Use this case if getopt_long() should go through all
         * arguments. If so, add a leading '-' character to optstring.
         * Actual code, if any, goes here.
         */
        break;
#endif
  	case ':':   /* missing option argument */
        	fprintf(stderr, "%s: option `-%c' requires an argument\n",
               		 argv[0], optopt);
       		break;
   	case '?':
   	default:    /* invalid option */
       		fprintf(stderr, "%s: option `-%c' is invalid: ignored\n",
               		 argv[0], optopt);
        	break;
    }	
}

int i;
WOBJC_IO *myobjc = thWObjcIoNew(NCOLOR);
myobjc->objc_type = OBJ_STAR;
for (i = 0; i < NCOLOR; i++) {
	myobjc->psfCounts[i] = mycounts;
	myobjc->rowc[i] = myxc;
	myobjc->colc[i] = myyc;
	myobjc->type[i] = OBJ_STAR;
	/* added on April 27, 2015
	myobjc->inner_lumcut_psf[i] = myinner_lumcut;
	myobjc->outer_lumcut_psf[i] = myouter_lumcut; 		
	*/
	/* added on Oct 1, 2015 to comenpsate for the bias detected in PHOTO analysis of my simulated stars
	myobjc->rowc[i] -= 0.5;
	Deleted on Oct 1, 2015 */

}

delete_all_tokens(argv, argc);
optind = prev_optind;
*objc = myobjc;
return(SH_SUCCESS);
}



RET_CODE skyparser(char *arg, SKYPARS **sky) {
	shAssert(arg != NULL);
	shAssert(sky != NULL);

int prev_optind = optind;
optind = 1;
int argc; char **argv;
argv = str_all_tokens(arg, " ", &argc);
struct option longopts[] = {
	{"s00", required_argument, NULL, 's'},
	{"z00", required_argument, NULL, 'z'},
	{0, 0, 0, 0}
};

SKYPARS *mysky = thSkyparsNew();
char c;
while ((c = getopt_long_only(argc, argv, "", longopts, NULL)) != -1) {
    switch (c) {
	case 's':
		sscanf(optarg, "%g", &(mysky->I_s00));
		break;
	case 'z':
		sscanf(optarg, "%g", &(mysky->I_z00));
		break;
    	case 0:     /* getopt_long() set a variable, just keep going */
	        break;
#if 0
    case 1:
        /*
         * Use this case if getopt_long() should go through all
         * arguments. If so, add a leading '-' character to optstring.
         * Actual code, if any, goes here.
         */
        break;
#endif
	case ':':   /* missing option argument */
        	fprintf(stderr, "%s: option `-%c' requires an argument\n",
               	 argv[0], optopt);
        	break;
    	case '?':
    	default:    /* invalid option */
       		 fprintf(stderr, "%s: option `-%c' is invalid: ignored\n",
               		 argv[0], optopt);
        	break;
    }
}

delete_all_tokens(argv, argc);
*sky = mysky;
optind = prev_optind;

return(SH_SUCCESS);
}

RET_CODE ffparser(char *arg, FRAMEFILES **ff) {

int prev_optind = optind;
optind = 1;
int argc; char **argv;
argv = str_all_tokens(arg, " ", &argc);
struct option longopts[] = {
	{"phconfig",	required_argument, NULL, 'a'},
	{"phcalib", 	required_argument, NULL, 'b'},
	{"phflat",	required_argument, NULL, 'c'},
	{"phmask",	required_argument, NULL, 'd'},
	{"phsm",	required_argument, NULL, 'e'},
	{"phpsf",	required_argument, NULL, 'f'},
	{"phobjc",	required_argument, NULL, 'g'},
	{"thsb",	required_argument, NULL, 'h'},
	{"thobj",	required_argument, NULL, 'i'},
	{"thmaksel",	required_argument, NULL, 'j'},
	{"thobjcsel",	required_argument, NULL, 'k'},
	{"thsm",	required_argument, NULL, 'l'},
	{"thpsf",	required_argument, NULL, 'm'},
	{"thobjc",	required_argument, NULL, 'n'},
	{"thflat", 	required_argument, NULL, 'o'},
	{0, 0, 0, 0}
};

FRAMEFILES *myff = thFramefilesNew();
char c;
while ((c = getopt_long_only(argc, argv, "", longopts, NULL)) != -1) {
    switch (c) {
	case 'a':
		strcpy(myff->phconfigfile, optarg);
		break;
	case 'b':
		strcpy(myff->phecalibfile, optarg);
		break;
	case 'c':
		strcpy(myff->phflatframefile, optarg);
		break;
	case 'd':
		strcpy(myff->phmaskfile, optarg);
		break;
	case 'e':
		strcpy(myff->phsmfile, optarg);
		break;
	case 'f':
		strcpy(myff->phpsfile, optarg);
		break;
	case 'g':
		strcpy(myff->phobjcfile, optarg);
		break;
	case 'h':
		strcpy(myff->thsbfile, optarg);
		break;
	case 'i':
		strcpy(myff->thobfile, optarg);
		break;
	case 'j':
		strcpy(myff->thmaskselfile, optarg);
		break;
	case 'k':
		strcpy(myff->thobjcselfile, optarg);
		break;
	case 'l':
		strcpy(myff->thsmfile, optarg);
		break;
	case 'm':
		strcpy(myff->thpsfile, optarg);
		break;
	case 'n':
		strcpy(myff->thobjcfile, optarg);
		break;
	case 'o':
		strcpy(myff->thflatframefile, optarg);
		break;
    	case 0:     /* getopt_long() set a variable, just keep going */
	        break;
#if 0
    case 1:
        /*
         * Use this case if getopt_long() should go through all
         * arguments. If so, add a leading '-' character to optstring.
         * Actual code, if any, goes here.
         */
        break;
#endif
	case ':':   /* missing option argument */
        	fprintf(stderr, "%s: option `-%c' requires an argument\n",
               	 argv[0], optopt);
        	break;
    	case '?':
    	default:    /* invalid option */
       		 fprintf(stderr, "%s: option `-%c' is invalid: ignored\n",
               		 argv[0], optopt);
        	break;
    }
}

delete_all_tokens(argv, argc);
*ff = myff;
optind = prev_optind;

return(SH_SUCCESS);
}


RET_CODE mleparser(int argc, char **argv, MLE_PARSED_INPUT *input) {

char *name = "mleparser";
shAssert(input != NULL);

int prev_optind = optind;
optind = 1;

int mydostar = 1;
GCLASS mygclass = DEVEXP_GCLASS;
CDESCRIPTION mycsystem = LOGHYPERBOLIC_LOGR;
int myfitsize = MFIT_NULL, myfitindex = MFIT_NULL, myfitshape = MFIT_NULL, myfitcenter = MFIT_NULL;
int mylockcenter = 1;
int mylockre = 0;
int mynfit = 1, mynobjc = DEFAULT_MAX_NOBJC, myfit2fpCC = 0;
LINITMODE myinit = SDSS_INIT;
int mynrow = DEFAULT_NROW, myncol = DEFAULT_NCOL;
int myinitn = DEFAULT_INIT_COUNT;

struct option longopts[] = {
	{"nostar", 	no_argument, NULL, 'a'},
	{"fitsize", 	no_argument, NULL, 'b'},
	{"fitindex",    no_argument, NULL, 'y'}, 
	{"fitshape", 	no_argument, NULL, 'c'},
	{"fitcenter", 	no_argument, NULL, 'd'},
	{"nolockcenter", no_argument, NULL, 'e'},
	{"gclass", 	required_argument, NULL, 'f'},
	{"csystem",	required_argument, NULL, 'g'},
	{"nobjc",     	required_argument, NULL, 'h'},
	{"nfit",     	required_argument, NULL, 'i'},
	{"framefile", 	required_argument, NULL, 'j'},
	{"memory", 	required_argument, NULL, 'k'},
	{"fit2fpCC",	no_argument, NULL, 'l'},
	{"init", 	required_argument, NULL, 'm'},
	{"error", 	required_argument, NULL, 'n'},
	{"seed", 	required_argument, NULL, 'o'},
	{"cD_force", no_argument, NULL, 'p'},
	{"cD_classify", no_argument, NULL, 'q'},
	{"multistage", no_argument, NULL, 'r'}, 
	{"gcsource", required_argument, NULL, 's'},
	{"cdclass", required_argument, NULL, 't'}, 
	{"demand", required_argument, NULL, 'u'},
	{"initn", required_argument, NULL, 'v'},
	{"fixsky", no_argument, NULL, 'w'},
	{"nolockre", no_argument, NULL, 'x'},
	{"mgalaxy", required_argument, NULL, 'z'},
	{"ctransform", required_argument, NULL, '1'}, 
 	{"fitsize1", 	no_argument, NULL, '2'},
	{"fitindex1",    no_argument, NULL, '3'}, 
	{"fitshape1", 	no_argument, NULL, '4'},
	{"fitcenter1", 	no_argument, NULL, '5'},
	{"fitsize2", 	no_argument, NULL, '6'},
	{"fitindex2",    no_argument, NULL, '7'}, 
	{"fitshape2", 	no_argument, NULL, '8'},
	{"fitcenter2", 	no_argument, NULL, '9'},

	{ 0, 0, 0, 0 }
};

RET_CODE status;

float myerror = DEFAULT_PARAMETER_TWEAK_COEFF;
char *myframefile = NULL;
char *mystring = thCalloc(MX_STRING_LEN, sizeof(char));
char *mystring2 = thCalloc(MX_STRING_LEN, sizeof(char));
int mymemory = 0;
unsigned int myseed = get_myseed();
int mydo_cD_classify = 0, myforce_cD_classify = 0, mymultistage = 0;
int mygcsource=DEFAULT_GC_SOURCE;
GCLASS mycdclass = DEFAULT_CDCLASS;
int mydemandcode = NO_DEMAND_CODE;
SKYPARS **myskylist = NULL;
PSF_CONVOLUTION_TYPE mygalaxy_psf = UNKNOWN_PSF_CONVOLUTION;
int mydo_fit_sky = 1;
MGALAXY_PARSED_INPUT *my_mgalaxy_input = NULL;
CTRANSFORM_PARSED_INPUT *my_ctransform_input = NULL;

char c; int i; char *stype = NULL; TYPE ttype = UNKNOWN_SCHEMA;
while ((c = getopt_long_only(argc, argv, "", longopts, NULL)) != -1) {
    switch (c) {
	case 'a':
		mydostar=0;
		break;	
	case 'b':
		myfitsize |= MFIT_SIZE ;
		break;
	case 'y':
		myfitindex |= MFIT_INDEX;
		break;
	case 'c':
		myfitshape |= MFIT_SHAPE; 
		break;
	case 'd':
		myfitcenter |= MFIT_CENTER;
		break;
	case 'e':
		mylockcenter = 0;
		break;
	case 'f':
		if (shTypeGetFromName("GCLASS") == UNKNOWN_SCHEMA) {
		thError("%s: ERROR - 'GCLASS' not supported by schema", name);
		return(SH_GENERIC_ERROR);
	}
		strcpy(mystring, optarg);
		mygclass = UNKNOWN_GCLASS;
		int found=0;
		for (i = 0; i < N_GCLASS; i++) {
			char *gclassenum = shEnumNameGetFromValue("GCLASS", i);
			if (!strcmp(mystring, gclassenum)) {
				mygclass = i;
				found++;
				break;
			}
		}
		if (!found) {
			thError("%s: ERROR - could not find '%s' among 'GCLASS' enum", name, mystring);
			return(SH_GENERIC_ERROR);
		}
		break;
	case 'g':	
		if (shTypeGetFromName("CDESCRIPTION") == UNKNOWN_SCHEMA) {
		thError("%s: ERROR - 'CDESCRIPTION' not supported by schema", name);
		return(SH_GENERIC_ERROR);
	}
		strcpy(mystring, optarg);
		mycsystem = UNKNOWN_CDESCRIPTION;
		for (i = 0; i < N_CDESCRIPTION; i++) {
			if (!strcmp(mystring, shEnumNameGetFromValue("CDESCRIPTION", i))) {
				mycsystem = i;
				break;
			}
		}
		break;
	case 'h':
		#if ALLOW_MAX_NOBJC 
		sscanf(optarg, "%d", &mynobjc);	
        	break;
		#else
		thError("%s: ERROR - 'nobjc' keyword flag not supported in the dispatched binary", name);
		return(SH_GENERIC_ERROR);
		#endif
    	case 'i':
		sscanf(optarg, "%d", &mynfit);
       		break;
	case 'j':
		myframefile=optarg;
		break;
	case 'k':
		sscanf(optarg, "%d", &mymemory);
		break;
	case 'l':
		myfit2fpCC = 1;
		break;
	case 'm':
		if (shTypeGetFromName("LINITMODE") == UNKNOWN_SCHEMA) {
		thError("%s: ERROR - 'LINITMODE' not supported by schema", name);
		return(SH_GENERIC_ERROR);
	}
		strcpy(mystring, optarg);
		myinit = UNKNOWN_LINITMODE;
		for (i = 0; i < N_LINITMODE; i++) {
			if (!strcmp(mystring, shEnumNameGetFromValue("LINITMODE", i))) {
				myinit = i;
				break;
			}
		}
		break;
	case 'n':
		sscanf(optarg, "%g", &myerror);
		break;
	case 'o':
		sscanf(optarg, "%u", &myseed);
		break;
	case 'p':
		myforce_cD_classify = 1;
		break;
	case 'q': 
		mydo_cD_classify = 1;
		break;
	case 'r': 
		mymultistage = 1;
		break;
	case 's':
		strcpy(mystring, optarg);
		stype = "GC_SOURCE";
		ttype = shTypeGetFromName(stype);
		if (ttype == UNKNOWN_SCHEMA) {
			thError("%s: ERROR - schema for '%s' not defined", name, stype);
			return(SH_GENERIC_ERROR);
		} else if (shValueGetFromEnumName(stype, mystring, (int *) &mygcsource) == NULL) {
			thError("%s: WARNING - '%s' could not be interpreted", name, mystring);
		}
		break;
	case 't':
		strcpy(mystring, optarg);
		stype = "GCLASS";
		ttype = shTypeGetFromName(stype);
		if (ttype == UNKNOWN_SCHEMA) {
			thError("%s: ERROR - schema for '%s' not defined", name, stype);
			return(SH_GENERIC_ERROR);
		} else if (shValueGetFromEnumName(stype, mystring, (int *) &mycdclass) == NULL) {
			thError("%s: WARNING - '%s' could not be interpreted", name, mystring);
		}
		break;
	case 'u':
		sscanf(optarg, "%d", &mydemandcode);
		break;
	case 'v':
		sscanf(optarg, "%d", &myinitn);
		break;
	case 'w': 
		mydo_fit_sky = 0;
		break;
	case 'x':
		mylockre = 0;
		break;
	case 'z':
		strcpy(mystring, optarg);
		my_mgalaxy_input = thMGalaxyParsedInputNew();
		status = mgalaxyparser(mystring, my_mgalaxy_input);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not parse (mgalaxy_pars)", name);
			return(status);
		}
		break;
	case '1':
		strcpy(mystring2, optarg);
		my_ctransform_input = thCTransformParsedInputNew();
		status = ctransformparser(mystring2, my_ctransform_input);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not parse (ctransform_pars)", name);
			return(status);
		}
		break;
	case '2':
		myfitsize |= MFIT_SIZE1;
		break;
	case '3':
		myfitindex |= MFIT_INDEX1;
		break;
	case '4':
		myfitshape |= MFIT_SHAPE1; 
		break;
	case '5':
		myfitcenter |= MFIT_CENTER1;
		break;
	case '6':
		myfitsize |= MFIT_SIZE2;
		break;
	case '7':
		myfitindex |= MFIT_INDEX2;
		break;
	case '8':
		myfitshape |= MFIT_SHAPE2; 
		break;
	case '9':
		myfitcenter |= MFIT_CENTER2;
		break;
	case 0:     /* getopt_long() set a variable, just keep going */
       		 break;
#if 0
    case 1:
        /*
         * Use this case if getopt_long() should go through all
         * arguments. If so, add a leading '-' character to optstring.
         * Actual code, if any, goes here.
         */
        break;
#endif
    	case ':':   /* missing option argument */
        	fprintf(stderr, "%s: ERROR - %s: option `-%c' requires an argument\n",
             	  	 name, argv[0], optopt);
       		 break;
   	 case '?':
  	 default:    /* invalid option */
        	fprintf(stderr, "%s: ERROR - %s: option `-%c' is invalid: ignored\n",
                	name, argv[0], optopt);
        	break;
    }
}

#if USE_FNAL_OBJC_IO
char *wobjc_name = "FNAL_OBJC_IO";
#else
char *wobjc_name = "OBJC_IO";
#endif
CHAIN *myobjclist = NULL;
if (mydemandcode != NO_DEMAND_CODE) {
	if (myobjclist == NULL) myobjclist = shChainNew(wobjc_name);
 	RET_CODE status;
	status = initdemandparser(mydemandcode, myobjclist, &myskylist, &mygalaxy_psf, &mycdclass);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not generate object and sky list on demand (code = %d)", name, mydemandcode);
		return(status);
	}
} 


input->framefile = myframefile;
input->dostar = mydostar;
input->gclass = mygclass;
input->csystem = mycsystem; 
input->fitshape = myfitshape;
input->fitsize = myfitsize;
input->fitindex = myfitindex;
input->fitcenter = myfitcenter;
input->lockcenter = mylockcenter;
input->lockre = mylockre;
input->nobjc = mynobjc;
input->nfit = mynfit;
input->memory=(MEMDBLE) mymemory * (MEMDBLE) MEGABYTE;
input->fit2fpCC = myfit2fpCC;
input->init = myinit;
input->error=myerror;
input->seed=generate_seed(myseed);
input->gcsource=mygcsource;
input->do_cD_classify = mydo_cD_classify;
input->demand = mydemandcode;
input->initn = myinitn;
input->do_fit_sky = mydo_fit_sky;
input->objclist = myobjclist;
input->skylist = myskylist;
input->galaxypsf = mygalaxy_psf;
#if ALLOW_FORCE_CD_CLASSIFICATION
if (myforce_cD_classify == 1) {
	thError("%s: WARNING - force_cD_classification flag set - only recommeneded in the debug mode", name);
}
input->force_cD_classify = myforce_cD_classify;
#else
if (myforce_cD_classify == 1) {
	thError("%s: ERROR - force_cD_classification flag is not allowed during this run", name);
	return(SH_GENERIC_ERROR);
}
#endif
input->multistage = mymultistage;
input->nrow = mynrow;
input->ncol = myncol;
input->cdclass = mycdclass;
if (input->mgalaxy_input != NULL) {
	memcpy(input->mgalaxy_input, my_mgalaxy_input, sizeof(MGALAXY_PARSED_INPUT));
	thMGalaxyParsedInputDel(my_mgalaxy_input);
} else {
	input->mgalaxy_input = my_mgalaxy_input;
}
if (input->ctransform_input != NULL) {
	memcpy(input->ctransform_input, my_ctransform_input, sizeof(CTRANSFORM_PARSED_INPUT));
	thCTransformParsedInputDel(my_ctransform_input);
} else {
	input->ctransform_input = my_ctransform_input;
}
optind = prev_optind;
return(SH_SUCCESS);
}

RET_CODE psfparser(int argc, char **argv, PSF_PARSED_INPUT *input) {
char *name = "psfparser";
shAssert(input != NULL);

int prev_optind = optind;
optind = 1;

int mynpsfstar = DEFAULT_N_PSF_STAR;
int myxcmin = DEFAULT_PSF_XCMIN, myxcmax = DEFAULT_PSF_XCMAX;
int myycmin = DEFAULT_PSF_YCMIN, myycmax = DEFAULT_PSF_YCMAX;

struct option longopts[] = {
	{"setup",     required_argument, NULL, 'a'},
	{"camcol",    required_argument, NULL, 'b'},
	{"band",      required_argument, NULL, 'c'},
	{"run", 	required_argument, NULL, 'd'},
	{"rerun", required_argument, NULL, 'e'},
	{"field", required_argument, NULL, 'f'},
	{"stripe", required_argument, NULL, 'g'},

	{"framefile", required_argument, NULL, 'i'}, 
	{"ff",	      required_argument, NULL, 'j'},
	
	{"seed", 	required_argument, NULL, 'k'},
	{"psfreportfile", 	required_argument, NULL, 'l'},
	{"psfstatfile", 	required_argument, NULL, 'm'},
	{"xcmin", 	required_argument, NULL, 'n'},
	{"xcmax", 	required_argument, NULL, 'o'},
	{"ycmin", 	required_argument, NULL, 'p'},
	{"ycmax", 	required_argument, NULL, 'q'},
	{"npsfstar", 	required_argument, NULL, 'r'},
	{ 0, 0, 0, 0 }
};

char *mysetupfile = NULL, *myframefile = NULL;
char *mypsfreportfile = NULL, *mypsfstatfile;
char *myargv = NULL;

FRAMEFILES *myff = NULL;
myargv = thCalloc(MX_STRING_LEN, sizeof(char));

int mycamcol = DEFAULT_CAMCOL, myband = DEFAULT_BAND;
char mybandname = '\0';
int myrun = DEFAULT_RUN, myrerun = DEFAULT_RERUN;
int mystripe = DEFAULT_STRIPE, myfield = DEFAULT_FIELD;

unsigned int myseed = get_myseed();

char c;
while ((c = getopt_long_only(argc, argv, "", longopts, NULL)) != -1) {
    switch (c) {
	case 'a':
		mysetupfile = optarg;
		break;	
	case 'b':
		sscanf(optarg, "%d", &mycamcol);
		break;
	case 'c':
		sscanf(optarg, "%c", &mybandname);
		break;
	case 'd':
		sscanf(optarg, "%d", &myrun);
		break;
	case 'e':
		sscanf(optarg, "%d", &myrerun);
		break;
	case 'f':
		sscanf(optarg, "%d", &myfield);
		break;
	case 'g':
		sscanf(optarg, "%d", &mystripe);
		break; 
	case 'i':
		myframefile = optarg;
		break;
	case 'j':
		strcpy(myargv, optarg);
		ffparser(myargv, &myff);
		break;
	case 'k':
		sscanf(optarg, "%u", &myseed);
		break;
	case 'l':
		mypsfreportfile = optarg;
		break;	
	case 'm':
		mypsfstatfile = optarg;
		break;
	case 'n':
		sscanf(optarg, "%d", &myxcmin);
		break; 
	case 'o':
		sscanf(optarg, "%d", &myxcmax);
		break; 
	case 'p':
		sscanf(optarg, "%d", &myycmin);
		break; 
	case 'q':
		sscanf(optarg, "%d", &myycmax);
		break; 
	case 'r':
		sscanf(optarg, "%d", &mynpsfstar);
		break; 
	case 0:     /* getopt_long() set a variable, just keep going */
       		 break;
#if 0
    case 1:
        /*
         * Use this case if getopt_long() should go through all
         * arguments. If so, add a leading '-' character to optstring.
         * Actual code, if any, goes here.
         */
        break;
#endif
    	case ':':   /* missing option argument */
        	fprintf(stderr, "%s: ERROR - %s: option `-%c' requires an argument\n",
             	  	 name, argv[0], optopt);
       		 break;
   	 case '?':
  	 default:    /* invalid option */
        	fprintf(stderr, "%s: ERROR - %s: option `-%c' is invalid: ignored\n",
                	name, argv[0], optopt);
        	break;
    }
}

RET_CODE status;
status = thBandGetFromName(mybandname, &myband);
if (status != SH_SUCCESS) {
	thError("%s: WARNING - could not retrieve band from band name (%c)", name, mybandname);
	myband = DEFAULT_BAND;
	thError("%s: WARNING - band set to '%d'", name, myband);
}

#ifdef DEFAULT_MAX_PSF_STAR 
if ((int) DEFAULT_MAX_PSF_STAR > 0 && mynpsfstar > (int) DEFAULT_MAX_PSF_STAR) {
	thError("%s: Warning - adjusting (npsfstar)", name);
	mynpsfstar = (int) DEFAULT_MAX_PSF_STAR;
}
#endif

input->setupfile = mysetupfile; 
input->camcol = mycamcol; 
input->band = myband; 
input->bandname = mybandname;
input->field = myfield;
input->run = myrun;
input->rerun = myrerun;
input->stripe = mystripe;

input->framefile = myframefile;
input->ff = myff; 

input->seed = generate_seed(myseed);
input->psfreportfile = mypsfreportfile;
input->psfstatfile = mypsfstatfile;
input->xcmin = myxcmin;
input->xcmax = myxcmax;
input->ycmin = myycmin;
input->ycmax = myycmax;
input->npsfstar = mynpsfstar;

thFree(myargv);

optind = prev_optind;
return(SH_SUCCESS);
}



unsigned int get_myseed() {

struct timespec tm;
clock_gettime(CLOCK_REALTIME, &tm);
long int ns = tm.tv_nsec;
unsigned int seed;
memcpy(&seed, &ns, sizeof(unsigned int));
return(seed);
}


RET_CODE demandparser(int demand, CHAIN *objclist_out, SKYPARS ***skylist_out, PSF_CONVOLUTION_TYPE *galaxy_psf_out, GCLASS *cdclass_out) {
char *name = "demandparser";
shAssert(objclist_out != NULL && skylist_out != NULL);
shAssert(galaxy_psf_out != NULL && cdclass_out != NULL);
if (shChainSize(objclist_out) != 0) {
	thError("%s: ERROR - object chain supplied should be empty, found (%d)", name, shChainSize(objclist_out));
	return(SH_GENERIC_ERROR);
}

SKYPARS **skylist = thCalloc(NCOLOR, sizeof(SKYPARS *));
*skylist_out = skylist;

/* 

r-band:
LRG Magnitude 					
	Q10	Q1	Median	Q3	Q90
Mag	14.53313255	15.6736536	17.239330291717.3072	18.52834702	19.00540543
Counts	2.54404E+05	8.46E+04	2.04E+04	6.10E+03	4.14E+03

LRG R_eff 					
	Q10	Q1	Median	Q3	Q90
log(arcsec)	-0.04265	0.14749	0.37160	0.68939	1.00160
arcsec	0.906	1.404	2.353	4.891	10.037
pixel	2.297	3.559	5.963	12.394	25.435
a/b	1.41	1.19	1.28	1.49	1.84


*/

WOBJC_IO *objc = NULL;
char *objc_type_name = "OBJC_IO";
#if USE_FNAL_OBJC_IO
objc_type_name = "FNAL_OBJC_IO";
#endif

PSF_CONVOLUTION_TYPE galaxy_psf_convolution = DEFAULT_GALAXY_PSF_CONVOLUTION;
GCLASS cdclass = DEFAULT_CDCLASS;

int i;
if (demand == 0) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	for (i = 0; i < NCOLOR; i++) {
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;	
	}
} else if (demand == 1 || demand == 11) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {2.5E5, 2.5E5, 2.5E5, 2.5E5, 2.5E5}; /* 10th percentile */
	float r_deV[NCOLOR] = {12.0, 12.0, 12.0, 12.0, 12.0}; /* a little bit less than the third quartile */
	float r_deVErr[NCOLOR] = {3.0, 3.0, 3.0, 3.0, 3.0}; 
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */	
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];		
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;
		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	cdclass = DEV_GCLASS;
	if (demand == 11) galaxy_psf_convolution = NO_PSF_CONVOLUTION;
} else if (demand == 2 || demand == 12) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {8.5E4, 8.5E4, 8.5E4, 8.5E4, 8.5E4}; /* 10th percentile */
	float r_deV[NCOLOR] = {12.0, 12.0, 12.0, 12.0, 12.0}; /* a little bit less than the third quartile */	
	float r_deVErr[NCOLOR] = {3.0, 3.0, 3.0, 3.0, 3.0}; 
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;
		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	cdclass = DEV_GCLASS;
	if (demand == 12) galaxy_psf_convolution = NO_PSF_CONVOLUTION;
} else if (demand == 3 || demand == 13) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {2.5E5, 2.5E5, 2.5E5, 2.5E5, 2.5E5}; /* 10th percentile */
	float r_deV[NCOLOR] = {6.0, 6.0, 6.0, 6.0, 6.0}; /* a little bit less than the third quartile */	
	float r_deVErr[NCOLOR] = {3.0, 3.0, 3.0, 3.0, 3.0}; 
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;
		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	cdclass = DEV_GCLASS;
	if (demand == 13) galaxy_psf_convolution = NO_PSF_CONVOLUTION;
} else if (demand == 4 || demand == 14) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {8.5E4, 8.5E4, 8.5E4, 8.5E4, 8.5E4}; /* 10th percentile */
	float r_deV[NCOLOR] = {6.0, 6.0, 6.0, 6.0, 6.0}; /* a little bit less than the third quartile */
	float r_deVErr[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; 
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */	
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;
		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	cdclass = DEV_GCLASS;
	if (demand == 14) galaxy_psf_convolution = NO_PSF_CONVOLUTION;
} else if (demand == 6 || demand == 16) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {2.5E5, 2.5E5, 2.5E5, 2.5E5, 2.5E5}; /* 10th percentile */
	float r_deV[NCOLOR] = {12.0, 12.0, 12.0, 12.0, 12.0}; /* a little bit less than the third quartile */
	float r_deVErr[NCOLOR] = {3.0, 3.0, 3.0, 3.0, 3.0}; /* a little bit less than the third quartile */
	float counts_deV2[NCOLOR] = {2.5E5, 2.5E5, 2.5E5, 2.5E5, 2.5E5}; /* 10th percentile */
	float r_deV2[NCOLOR] = {36.0, 36.0, 36.0, 36.0, 36.0}; /* a little bit less than the third quartile */	
	float r_deV2Err[NCOLOR] = {9.0, 9.0, 9.0, 9.0, 9.0}; /* a little bit less than the third quartile */	
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */	
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;

		#if USE_FNAL_OBJC_IO	
		objc->counts_deV2[i] = (float) counts_deV2[i];
		objc->r_deV2[i] = (float) r_deV2[i];	
		objc->r_deV2Err[i] = (float) r_deV2Err[i];	
		objc->ab_deV2[i] = (float) 1.0;
		objc->counts_exp2[i] = (float) 0.0;
		objc->r_exp2[i] = (float) VALUE_IS_BAD;
		objc->ab_exp2[i] = (float) 1.0;
		#else
		objc->counts_exp[i] = (float) counts_deV2[i];
		objc->r_exp[i] = (float) r_deV2[i];
		objc->r_expErr[i] = (float) r_deV2Err[i];
		objc->ab_exp[i] = (float) 1.0;
		#endif


		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	cdclass = DEVDEV_GCLASS;
	if (demand == 16) galaxy_psf_convolution = NO_PSF_CONVOLUTION;
} else if (demand == 7 || demand == 17) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {8.5E4, 8.5E4, 8.5E4, 8.5E4, 8.5E4}; /* 10th percentile */
	float r_deV[NCOLOR] = {12.0, 12.0, 12.0, 12.0, 12.0}; /* a little bit less than the third quartile */
	float r_deVErr[NCOLOR] = {3.0, 3.0, 3.0, 3.0, 3.0}; 
	float counts_deV2[NCOLOR] = {8.5E4, 8.5E4, 8.5E4, 8.5E4, 8.5E4}; /* 10th percentile */
	#if 1
	float r_deV2[NCOLOR] = {12.0, 12.0, 12.0, 12.0, 12.0}; /* a little bit less than the third quartile */
	float r_deV2Err[NCOLOR] = {3.0, 3.0, 3.0, 3.0, 3.0}; /* a little bit less than the third quartile */
	#else
	float r_deV2[NCOLOR] = {36.0, 36.0, 36.0, 36.0, 36.0}; /* a little bit less than the third quartile */
	float r_deV2Err[NCOLOR] = {9.0, 9.0, 9.0, 9.0, 9.0}; /* a little bit less than the third quartile */	
	#endif
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif
	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;

		#if USE_FNAL_OBJC_IO
		objc->counts_deV2[i] = (float) counts_deV2[i];
		objc->r_deV2[i] = (float) r_deV2[i];	
		objc->r_deV2Err[i] = (float) r_deV2Err[i];	
		objc->ab_deV2[i] = (float) 1.0;
		objc->counts_exp2[i] = (float) 0.0;
		objc->r_exp2[i] = (float) VALUE_IS_BAD;
		objc->ab_exp2[i] = (float) 1.0;
		#else
		objc->counts_exp[i] = (float) counts_deV2[i];
		objc->r_exp[i] = (float) r_deV2[i];
		objc->r_expErr[i] = (float) r_deV2Err[i];
		objc->ab_exp[i] = (float) 1.0;
		#endif


		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	cdclass = DEVDEV_GCLASS;
	if (demand == 17) galaxy_psf_convolution = NO_PSF_CONVOLUTION;
} else if (demand == 8 || demand == 18) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {2.5E5, 2.5E5, 2.5E5, 2.5E5, 2.5E5}; /* 10th percentile */
	float r_deV[NCOLOR] = {6.0, 6.0, 6.0, 6.0, 6.0}; /* a little bit less than the third quartile */
	float r_deVErr[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; /* a little bit less than the third quartile */
	float counts_deV2[NCOLOR] = {2.5E5, 2.5E5, 2.5E5, 2.5E5, 2.5E5}; /* 10th percentile */
	float r_deV2[NCOLOR] = {18.0, 18.0, 18.0, 18.0, 18.0}; /* a little bit less than the third quartile */	
	float r_deV2Err[NCOLOR] = {6.0, 6.0, 6.0, 6.0, 6.0}; /* a little bit less than the third quartile */	
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;

		#if USE_FNAL_OBJC_IO
		objc->counts_deV2[i] = (float) counts_deV2[i];
		objc->r_deV2[i] = (float) r_deV2[i];	
		objc->r_deV2Err[i] = (float) r_deV2Err[i];	
		objc->ab_deV2[i] = (float) 1.0;
		objc->counts_exp2[i] = (float) 0.0;
		objc->r_exp2[i] = (float) VALUE_IS_BAD;
		objc->ab_exp2[i] = (float) 1.0;
		#else
		objc->counts_exp[i] = (float) counts_deV2[i];
		objc->r_exp[i] = (float) r_deV2[i];
		objc->r_expErr[i] = (float) r_deV2Err[i];
		objc->ab_exp[i] = (float) 1.0;
		#endif

		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	cdclass = DEVDEV_GCLASS;
	if (demand == 18) galaxy_psf_convolution = NO_PSF_CONVOLUTION;
} else if (demand == 9 || demand == 19) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {8.5E4, 8.5E4, 8.5E4, 8.5E4, 8.5E4}; /* 10th percentile */
	float r_deV[NCOLOR] = {6.0, 6.0, 6.0, 6.0, 6.0}; /* a little bit less than the third quartile */
	float r_deVErr[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; /* a little bit less than the third quartile */
	float counts_deV2[NCOLOR] = {8.5E4, 8.5E4, 8.5E4, 8.5E4, 8.5E4}; /* 10th percentile */
	float r_deV2[NCOLOR] = {18.0, 18.0, 18.0, 18.0, 18.0}; /* a little bit less than the third quartile */
	float r_deV2Err[NCOLOR] = {6.0, 6.0, 6.0, 6.0, 6.0}; /* a little bit less than the third quartile */
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */	
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;

		#if USE_FNAL_OBJC_IO
		objc->counts_deV2[i] = (float) counts_deV2[i];
		objc->r_deV2[i] = (float) r_deV2[i];	
		objc->r_deV2Err[i] = (float) r_deV2Err[i];	
		objc->ab_deV2[i] = (float) 1.0;
		objc->counts_exp2[i] = (float) 0.0;
		objc->r_exp2[i] = (float) VALUE_IS_BAD;
		objc->ab_exp2[i] = (float) 1.0;
		#else
		objc->counts_exp[i] = (float) counts_deV2[i];
		objc->r_exp[i] = (float) r_deV2[i];
		objc->r_expErr[i] = (float) r_deV2Err[i];
		objc->ab_exp[i] = (float) 1.0;
		#endif


		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;

		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	cdclass = DEVDEV_GCLASS;
	if (demand == 19) galaxy_psf_convolution = NO_PSF_CONVOLUTION;
}  else if (demand == 21 || demand == 31 || demand == 26 || demand == 36) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {2.5E5, 2.5E5, 2.5E5, 2.5E5, 2.5E5}; /* 10th percentile */
	float r_deV[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; /* a little bit less than the third quartile */
	float r_deVErr[NCOLOR] = {0.7, 0.7, 0.7, 0.7, 0.7}; /* a little bit less than the third quartile */
	float counts_deV2[NCOLOR] = {2.5E5, 2.5E5, 2.5E5, 2.5E5, 2.5E5}; /* 10th percentile */
	float r_deV2[NCOLOR] = {6.0, 6.0, 6.0, 6.0, 6.0}; /* a little bit less than the third quartile */	
	float r_deV2Err[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; /* a little bit less than the third quartile */	
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */	
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;

		if ((demand % 10) >  5) {
			#if USE_FNAL_OBJC_IO
			objc->counts_deV2[i] = (float) counts_deV2[i];
			objc->r_deV2[i] = (float) r_deV2[i];	
			objc->r_deV2Err[i] = (float) r_deV2Err[i];	
			objc->ab_deV2[i] = (float) 1.0;
			objc->counts_exp2[i] = (float) 0.0;
			objc->r_exp2[i] = (float) VALUE_IS_BAD;
				objc->ab_exp2[i] = (float) 1.0;
			#else
			objc->counts_exp[i] = (float) counts_deV2[i];
			objc->r_exp[i] = (float) r_deV2[i];
			objc->r_expErr[i] = (float) r_deV2Err[i];
			objc->ab_exp[i] = (float) 1.0;
			#endif
		} else {
			#if USE_FNAL_OBJC_IO
			objc->counts_deV2[i] = (float) 0.0;
			objc->r_deV2[i] = (float) VALUE_IS_BAD;	
			objc->r_deV2Err[i] = (float) 0.0;	
			objc->ab_deV2[i] = (float) 1.0;
			objc->counts_exp2[i] = (float) 0.0;
			objc->r_exp2[i] = (float) VALUE_IS_BAD;
			objc->ab_exp2[i] = (float) 1.0;
			#else
			objc->counts_exp[i] = (float) 0.0;
			objc->r_exp[i] = (float) VALUE_IS_BAD;
			objc->r_expErr[i] = (float) 0.0;
			objc->ab_exp[i] = (float) 1.0;
			#endif
		}

		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	if (demand % 10 > 5) {
		cdclass = DEVDEV_GCLASS;
	} else {
		cdclass = DEV_GCLASS;
	}
	if (((demand / 10)%10)%2 == 1) galaxy_psf_convolution = NO_PSF_CONVOLUTION;

} else if (demand == 22 || demand == 32 || demand == 27 || demand == 37) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {2.5E5, 2.5E5, 2.5E5, 2.5E5, 2.5E5}; /* 10th percentile */
	float r_deV[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; /* a little bit less than the third quartile */
	float r_deVErr[NCOLOR] = {0.7, 0.7, 0.7, 0.7, 0.7}; /* a little bit less than the third quartile */
	float counts_deV2[NCOLOR] = {4.5E5, 4.5E5, 4.5E5, 4.5E5, 4.5E5}; /* 10th percentile */
	float r_deV2[NCOLOR] = {8.0, 8.0, 8.0, 8.0, 8.0}; /* a little bit less than the third quartile */	
	float r_deV2Err[NCOLOR] = {2.7, 2.7, 2.7, 2.7, 2.7}; /* a little bit less than the third quartile */	
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;

		if ((demand % 10) > 5) {
			#if USE_FNAL_OBJC_IO
			objc->counts_deV2[i] = (float) counts_deV2[i];
			objc->r_deV2[i] = (float) r_deV2[i];	
			objc->r_deV2Err[i] = (float) r_deV2Err[i];	
			objc->ab_deV2[i] = (float) 1.0;
			objc->counts_exp2[i] = (float) 0.0;
			objc->r_exp2[i] = (float) VALUE_IS_BAD;
				objc->ab_exp2[i] = (float) 1.0;
			#else
			objc->counts_exp[i] = (float) counts_deV2[i];
			objc->r_exp[i] = (float) r_deV2[i];
			objc->r_expErr[i] = (float) r_deV2Err[i];
			objc->ab_exp[i] = (float) 1.0;
			#endif
		} else {
			#if USE_FNAL_OBJC_IO
			objc->counts_deV2[i] = (float) 0.0;
			objc->r_deV2[i] = (float) VALUE_IS_BAD;	
			objc->r_deV2Err[i] = (float) 0.0;	
			objc->ab_deV2[i] = (float) 1.0;
			objc->counts_exp2[i] = (float) 0.0;
			objc->r_exp2[i] = (float) VALUE_IS_BAD;
			objc->ab_exp2[i] = (float) 1.0;
			#else
			objc->counts_exp[i] = (float) 0.0;
			objc->r_exp[i] = (float) VALUE_IS_BAD;
			objc->r_expErr[i] = (float) 0.0;
			objc->ab_exp[i] = (float) 1.0;
			#endif
		}

		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	if (demand % 10 > 5) {
		cdclass = DEVDEV_GCLASS;
	} else {
		cdclass = DEV_GCLASS;
	}
	if (((demand / 10)%10)%2 == 1) galaxy_psf_convolution = NO_PSF_CONVOLUTION;

} else if (demand == 23 || demand == 33 || demand == 28 || demand == 38) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {8.5E4, 8.5E4, 8.5E4, 8.5E4, 8.5E4}; /* 10th percentile */
	float r_deV[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; /* a little bit less than the third quartile */
	float r_deVErr[NCOLOR] = {0.7, 0.7, 0.7, 0.7, 0.7}; /* a little bit less than the third quartile */
	float counts_deV2[NCOLOR] = {8.5E4, 8.5E4, 8.5E4, 8.5E4, 8.5E4}; /* 10th percentile */
	float r_deV2[NCOLOR] = {6.0, 6.0, 6.0, 6.0, 6.0}; /* a little bit less than the third quartile */	
	float r_deV2Err[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; /* a little bit less than the third quartile */	
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;

		if ((demand % 10) >  5) {
			#if USE_FNAL_OBJC_IO
			objc->counts_deV2[i] = (float) counts_deV2[i];
			objc->r_deV2[i] = (float) r_deV2[i];	
			objc->r_deV2Err[i] = (float) r_deV2Err[i];	
			objc->ab_deV2[i] = (float) 1.0;
			objc->counts_exp2[i] = (float) 0.0;
			objc->r_exp2[i] = (float) VALUE_IS_BAD;
				objc->ab_exp2[i] = (float) 1.0;
			#else
			objc->counts_exp[i] = (float) counts_deV2[i];
			objc->r_exp[i] = (float) r_deV2[i];
			objc->r_expErr[i] = (float) r_deV2Err[i];
			objc->ab_exp[i] = (float) 1.0;
			#endif
		} else {
			#if USE_FNAL_OBJC_IO
			objc->counts_deV2[i] = (float) 0.0;
			objc->r_deV2[i] = (float) VALUE_IS_BAD;	
			objc->r_deV2Err[i] = (float) 0.0;	
			objc->ab_deV2[i] = (float) 1.0;
			objc->counts_exp2[i] = (float) 0.0;
			objc->r_exp2[i] = (float) VALUE_IS_BAD;
			objc->ab_exp2[i] = (float) 1.0;
			#else
			objc->counts_exp[i] = (float) 0.0;
			objc->r_exp[i] = (float) VALUE_IS_BAD;
			objc->r_expErr[i] = (float) 0.0;
			objc->ab_exp[i] = (float) 1.0;
			#endif
		}

		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	if (demand % 10 > 5) {
		cdclass = DEVDEV_GCLASS;
	} else {
		cdclass = DEV_GCLASS;
	}
	if (((demand / 10)%10)%2 == 1) galaxy_psf_convolution = NO_PSF_CONVOLUTION;

} else if (demand == 24 || demand == 34 || demand == 29 || demand == 39) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {8.5E4, 8.5E4, 8.5E4, 8.5E4, 8.5E4}; /* 10th percentile */
	float r_deV[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; /* a little bit less than the third quartile */
	float r_deVErr[NCOLOR] = {0.7, 0.7, 0.7, 0.7, 0.7}; /* a little bit less than the third quartile */
	float counts_deV2[NCOLOR] = {1.5E5, 1.5E5, 1.5E5, 1.5E5, 1.5E5}; /* 10th percentile */
	float r_deV2[NCOLOR] = {8.0, 8.0, 8.0, 8.0, 8.0}; /* a little bit less than the third quartile */	
	float r_deV2Err[NCOLOR] = {2.7, 2.7, 2.7, 2.7, 2.7}; /* a little bit less than the third quartile */	
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;

		if ((demand % 10) > 5) {
			#if USE_FNAL_OBJC_IO
			objc->counts_deV2[i] = (float) counts_deV2[i];
			objc->r_deV2[i] = (float) r_deV2[i];	
			objc->r_deV2Err[i] = (float) r_deV2Err[i];	
			objc->ab_deV2[i] = (float) 1.0;
			objc->counts_exp2[i] = (float) 0.0;
			objc->r_exp2[i] = (float) VALUE_IS_BAD;
				objc->ab_exp2[i] = (float) 1.0;
			#else
			objc->counts_exp[i] = (float) counts_deV2[i];
			objc->r_exp[i] = (float) r_deV2[i];
			objc->r_expErr[i] = (float) r_deV2Err[i];
			objc->ab_exp[i] = (float) 1.0;
			#endif
		} else {
			#if USE_FNAL_OBJC_IO
			objc->counts_deV2[i] = (float) 0.0;
			objc->r_deV2[i] = (float) VALUE_IS_BAD;	
			objc->r_deV2Err[i] = (float) 0.0;	
			objc->ab_deV2[i] = (float) 1.0;
			objc->counts_exp2[i] = (float) 0.0;
			objc->r_exp2[i] = (float) VALUE_IS_BAD;
			objc->ab_exp2[i] = (float) 1.0;
			#else
			objc->counts_exp[i] = (float) 0.0;
			objc->r_exp[i] = (float) VALUE_IS_BAD;
			objc->r_expErr[i] = (float) 0.0;
			objc->ab_exp[i] = (float) 1.0;
			#endif
		}

		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	if (demand % 10 > 5) {
		cdclass = DEVDEV_GCLASS;
	} else {
		cdclass = DEV_GCLASS;
	}
	if (((demand / 10)%10)%2 == 1) galaxy_psf_convolution = NO_PSF_CONVOLUTION;

} else if (demand == 41 || demand == 51) {
	
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {2.5E5, 2.5E5, 2.5E5, 2.5E5, 2.5E5}; /* 10th percentile */
	float r_deV[NCOLOR] = {12.0, 12.0, 12.0, 12.0, 12.0}; /* a little bit less than the third quartile */
	float r_deVErr[NCOLOR] = {3.0, 3.0, 3.0, 3.0, 3.0}; 
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */	
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];		
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;
		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	cdclass = DEV_GCLASS;
	if (demand == 51) galaxy_psf_convolution = NO_PSF_CONVOLUTION;
} else if (demand == 42 || demand == 52) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {8.5E4, 8.5E4, 8.5E4, 8.5E4, 8.5E4}; /* 10th percentile */
	float r_deV[NCOLOR] = {12.0, 12.0, 12.0, 12.0, 12.0}; /* a little bit less than the third quartile */	
	float r_deVErr[NCOLOR] = {3.0, 3.0, 3.0, 3.0, 3.0}; 
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;
		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	cdclass = DEV_GCLASS;
	if (demand == 52) galaxy_psf_convolution = NO_PSF_CONVOLUTION;
} else if (demand == 43 || demand == 53) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {2.5E5, 2.5E5, 2.5E5, 2.5E5, 2.5E5}; /* 10th percentile */
	float r_deV[NCOLOR] = {6.0, 6.0, 6.0, 6.0, 6.0}; /* a little bit less than the third quartile */	
	float r_deVErr[NCOLOR] = {3.0, 3.0, 3.0, 3.0, 3.0}; 
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */	
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;
		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	cdclass = DEV_GCLASS;
	if (demand == 53) galaxy_psf_convolution = NO_PSF_CONVOLUTION;
} else if (demand == 44 || demand == 54) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {8.5E4, 8.5E4, 8.5E4, 8.5E4, 8.5E4}; /* 10th percentile */
	float r_deV[NCOLOR] = {6.0, 6.0, 6.0, 6.0, 6.0}; /* a little bit less than the third quartile */
	float r_deVErr[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; 
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */	
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;
		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	cdclass = DEV_GCLASS;
	if (demand == 54) galaxy_psf_convolution = NO_PSF_CONVOLUTION;
} else if (demand == 46 || demand == 56) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {2.5E5, 2.5E5, 2.5E5, 2.5E5, 2.5E5}; /* 10th percentile */
	float r_deV[NCOLOR] = {12.0, 12.0, 12.0, 12.0, 12.0}; /* a little bit less than the third quartile */
	float r_deVErr[NCOLOR] = {3.0, 3.0, 3.0, 3.0, 3.0}; /* a little bit less than the third quartile */
	float counts_pl[NCOLOR] = {2.5E5, 2.5E5, 2.5E5, 2.5E5, 2.5E5}; /* 10th percentile */
	float r_pl[NCOLOR] = {36.0, 36.0, 36.0, 36.0, 36.0}; /* a little bit less than the third quartile */	
	float r_plErr[NCOLOR] = {9.0, 9.0, 9.0, 9.0, 9.0}; /* a little bit less than the third quartile */	
	
	float n_pl[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; 
	float n_plErr[NCOLOR] = {0.2, 0.2, 0.2, 0.2, 0.2};
 
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;

		#if USE_FNAL_OBJC_IO	
		objc->counts_pl[i] = (float) counts_pl[i];
		objc->r_pl[i] = (float) r_pl[i] / PL_HALFLIGHT_SCALE;	
		objc->r_plErr[i] = (float) r_plErr[i] / PL_HALFLIGHT_SCALE;	
		objc->ab_pl[i] = (float) 1.0;
		objc->n_pl[i] = n_pl[i];
		objc->n_plErr[i] = n_plErr[i];		
		objc->counts_exp2[i] = (float) 0.0;
		objc->r_exp2[i] = (float) VALUE_IS_BAD;
		objc->ab_exp2[i] = (float) 1.0;
		#else
		objc->counts_exp[i] = (float) counts_pl[i];
		objc->r_exp[i] = (float) r_pl[i] / PL_HALFLIGHT_SCALE;
		objc->r_expErr[i] = (float) r_plErr[i] / PL_HALFLIGHT_SCALE;
		objc->ab_exp[i] = (float) 1.0;
		#endif


		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	cdclass = DEVPL_GCLASS;
	if (demand == 56) galaxy_psf_convolution = NO_PSF_CONVOLUTION;
} else if (demand == 47 || demand == 57) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {8.5E4, 8.5E4, 8.5E4, 8.5E4, 8.5E4}; /* 10th percentile */
	float r_deV[NCOLOR] = {12.0, 12.0, 12.0, 12.0, 12.0}; /* a little bit less than the third quartile */
	float r_deVErr[NCOLOR] = {3.0, 3.0, 3.0, 3.0, 3.0}; 
	float counts_pl[NCOLOR] = {8.5E4, 8.5E4, 8.5E4, 8.5E4, 8.5E4}; /* 10th percentile */
	float r_pl[NCOLOR] = {36.0, 36.0, 36.0, 36.0, 36.0}; /* a little bit less than the third quartile */
	float r_plErr[NCOLOR] = {9.0, 9.0, 9.0, 9.0, 9.0}; /* a little bit less than the third quartile */
	float n_pl[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; 
	float n_plErr[NCOLOR] = {0.2, 0.2, 0.2, 0.2, 0.2};
 

	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;

		#if USE_FNAL_OBJC_IO
		objc->counts_pl[i] = (float) counts_pl[i];
		objc->r_pl[i] = (float) r_pl[i] / PL_HALFLIGHT_SCALE;	
		objc->r_plErr[i] = (float) r_plErr[i] / PL_HALFLIGHT_SCALE;	
		objc->ab_pl[i] = (float) 1.0;
		objc->n_pl[i] = n_pl[i];
		objc->n_plErr[i] = n_plErr[i];		
		objc->counts_exp2[i] = (float) 0.0;
		objc->r_exp2[i] = (float) VALUE_IS_BAD;
		objc->ab_exp2[i] = (float) 1.0;
		#else
		objc->counts_exp[i] = (float) counts_pl[i];
		objc->r_exp[i] = (float) r_pl[i] / PL_HALFLIGHT_SCALE;
		objc->r_expErr[i] = (float) r_plErr[i] / PL_HALFLIGHT_SCALE;
		objc->ab_exp[i] = (float) 1.0;
		#endif


		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	cdclass = DEVPL_GCLASS;
	if (demand == 57) galaxy_psf_convolution = NO_PSF_CONVOLUTION;
} else if (demand == 48 || demand == 58) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {2.5E5, 2.5E5, 2.5E5, 2.5E5, 2.5E5}; /* 10th percentile */
	float r_deV[NCOLOR] = {6.0, 6.0, 6.0, 6.0, 6.0}; /* a little bit less than the third quartile */
	float r_deVErr[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; /* a little bit less than the third quartile */
	float counts_pl[NCOLOR] = {2.5E5, 2.5E5, 2.5E5, 2.5E5, 2.5E5}; /* 10th percentile */
	float r_pl[NCOLOR] = {18.0, 18.0, 18.0, 18.0, 18.0}; /* a little bit less than the third quartile */	
	float r_plErr[NCOLOR] = {6.0, 6.0, 6.0, 6.0, 6.0}; /* a little bit less than the third quartile */	
	float n_pl[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; 
	float n_plErr[NCOLOR] = {0.2, 0.2, 0.2, 0.2, 0.2};
 
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;

		#if USE_FNAL_OBJC_IO
		objc->counts_pl[i] = (float) counts_pl[i];
		objc->r_pl[i] = (float) r_pl[i] / PL_HALFLIGHT_SCALE;	
		objc->r_plErr[i] = (float) r_plErr[i] / PL_HALFLIGHT_SCALE;	
		objc->ab_pl[i] = (float) 1.0;
		objc->n_pl[i] = n_pl[i];
		objc->n_plErr[i] = n_plErr[i];		
		objc->counts_exp2[i] = (float) 0.0;
		objc->r_exp2[i] = (float) VALUE_IS_BAD;
		objc->ab_exp2[i] = (float) 1.0;
		#else
		objc->counts_exp[i] = (float) counts_pl[i];
		objc->r_exp[i] = (float) r_pl[i] / PL_HALFLIGHT_SCALE;
		objc->r_expErr[i] = (float) r_plErr[i] / PL_HALFLIGHT_SCALE;
		objc->ab_exp[i] = (float) 1.0;
		#endif

		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	cdclass = DEVPL_GCLASS;
	if (demand == 58) galaxy_psf_convolution = NO_PSF_CONVOLUTION;
} else if (demand == 49 || demand == 59) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {8.5E4, 8.5E4, 8.5E4, 8.5E4, 8.5E4}; /* 10th percentile */
	float r_deV[NCOLOR] = {6.0, 6.0, 6.0, 6.0, 6.0}; /* a little bit less than the third quartile */
	float r_deVErr[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; /* a little bit less than the third quartile */
	float counts_pl[NCOLOR] = {8.5E4, 8.5E4, 8.5E4, 8.5E4, 8.5E4}; /* 10th percentile */
	float r_pl[NCOLOR] = {18.0, 18.0, 18.0, 18.0, 18.0}; /* a little bit less than the third quartile */
	float r_plErr[NCOLOR] = {6.0, 6.0, 6.0, 6.0, 6.0}; /* a little bit less than the third quartile */
	float n_pl[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; 
	float n_plErr[NCOLOR] = {0.2, 0.2, 0.2, 0.2, 0.2};
 
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;

		#if USE_FNAL_OBJC_IO
		objc->counts_pl[i] = (float) counts_pl[i];
		objc->r_pl[i] = (float) r_pl[i] / PL_HALFLIGHT_SCALE;	
		objc->r_plErr[i] = (float) r_plErr[i] / PL_HALFLIGHT_SCALE;	
		objc->ab_pl[i] = (float) 1.0;
		objc->n_pl[i] = n_pl[i];
		objc->n_plErr[i] = n_plErr[i];		
		objc->counts_exp2[i] = (float) 0.0;
		objc->r_exp2[i] = (float) VALUE_IS_BAD;
		objc->ab_exp2[i] = (float) 1.0;
		#else
		objc->counts_exp[i] = (float) counts_pl[i];
		objc->r_exp[i] = (float) r_pl[i] / PL_HALFLIGHT_SCALE;
		objc->r_expErr[i] = (float) r_plErr[i] / PL_HALFLIGHT_SCALE;
		objc->ab_exp[i] = (float) 1.0;
		#endif


		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;

		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	cdclass = DEVPL_GCLASS;
	if (demand == 59) galaxy_psf_convolution = NO_PSF_CONVOLUTION;
}  else if (demand == 61 || demand == 71 || demand == 66 || demand == 76) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {2.5E5, 2.5E5, 2.5E5, 2.5E5, 2.5E5}; /* 10th percentile */
	float r_deV[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; /* a little bit less than the third quartile */
	float r_deVErr[NCOLOR] = {0.7, 0.7, 0.7, 0.7, 0.7}; /* a little bit less than the third quartile */
	float counts_pl[NCOLOR] = {2.5E5, 2.5E5, 2.5E5, 2.5E5, 2.5E5}; /* 10th percentile */
	float r_pl[NCOLOR] = {6.0, 6.0, 6.0, 6.0, 6.0}; /* a little bit less than the third quartile */	
	float r_plErr[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; /* a little bit less than the third quartile */	
	float n_pl[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; 
	float n_plErr[NCOLOR] = {0.2, 0.2, 0.2, 0.2, 0.2};
 
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;

		if ((demand % 10) >  5) {
			#if USE_FNAL_OBJC_IO
			objc->counts_pl[i] = (float) counts_pl[i];
			objc->r_pl[i] = (float) r_pl[i] / PL_HALFLIGHT_SCALE;	
			objc->r_plErr[i] = (float) r_plErr[i] / PL_HALFLIGHT_SCALE;	
			objc->ab_pl[i] = (float) 1.0;
			objc->n_pl[i] = n_pl[i];
			objc->n_plErr[i] = n_plErr[i];
			objc->counts_exp2[i] = (float) 0.0;
			objc->r_exp2[i] = (float) VALUE_IS_BAD;
			objc->ab_exp2[i] = (float) 1.0;
			#else
			objc->counts_exp[i] = (float) counts_pl[i];
			objc->r_exp[i] = (float) r_pl[i] / PL_HALFLIGHT_SCALE;
			objc->r_expErr[i] = (float) r_plErr[i] / PL_HALFLIGHT_SCALE;
			objc->ab_exp[i] = (float) 1.0;
			#endif
		} else {
			#if USE_FNAL_OBJC_IO
			objc->counts_pl[i] = (float) 0.0;
			objc->r_pl[i] = (float) VALUE_IS_BAD;	
			objc->r_plErr[i] = (float) 0.0;	
			objc->ab_pl[i] = (float) 1.0;
			objc->n_pl[i] = n_pl[i];
			objc->n_plErr[i] = n_plErr[i];	
			objc->counts_exp2[i] = (float) 0.0;
			objc->r_exp2[i] = (float) VALUE_IS_BAD;
			objc->ab_exp2[i] = (float) 1.0;
			#else
			objc->counts_exp[i] = (float) 0.0;
			objc->r_exp[i] = (float) VALUE_IS_BAD;
			objc->r_expErr[i] = (float) 0.0;
			objc->ab_exp[i] = (float) 1.0;
			#endif
		}

		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	if (demand % 10 > 5) {
		cdclass = DEVPL_GCLASS;
	} else {
		cdclass = DEV_GCLASS;
	}
	if (((demand / 10)%10)%2 == 1) galaxy_psf_convolution = NO_PSF_CONVOLUTION;

} else if (demand == 62 || demand == 72 || demand == 67 || demand == 77) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {2.5E5, 2.5E5, 2.5E5, 2.5E5, 2.5E5}; /* 10th percentile */
	float r_deV[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; /* a little bit less than the third quartile */
	float r_deVErr[NCOLOR] = {0.7, 0.7, 0.7, 0.7, 0.7}; /* a little bit less than the third quartile */
	float counts_pl[NCOLOR] = {4.5E5, 4.5E5, 4.5E5, 4.5E5, 4.5E5}; /* 10th percentile */
	float r_pl[NCOLOR] = {8.0, 8.0, 8.0, 8.0, 8.0}; /* a little bit less than the third quartile */	
	float r_plErr[NCOLOR] = {2.7, 2.7, 2.7, 2.7, 2.7}; /* a little bit less than the third quartile */	
	float n_pl[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; 
	float n_plErr[NCOLOR] = {0.2, 0.2, 0.2, 0.2, 0.2};
 
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;

		if ((demand % 10) > 5) {
			#if USE_FNAL_OBJC_IO
			objc->counts_pl[i] = (float) counts_pl[i];
			objc->r_pl[i] = (float) r_pl[i] / PL_HALFLIGHT_SCALE;	
			objc->r_plErr[i] = (float) r_plErr[i] / PL_HALFLIGHT_SCALE;	
			objc->ab_pl[i] = (float) 1.0;
			objc->n_pl[i] = n_pl[i];
			objc->n_plErr[i] = n_plErr[i];	
			objc->counts_exp2[i] = (float) 0.0;
			objc->r_exp2[i] = (float) VALUE_IS_BAD;
			objc->ab_exp2[i] = (float) 1.0;
			#else
			objc->counts_exp[i] = (float) counts_pl[i];
			objc->r_exp[i] = (float) r_pl[i] / PL_HALFLIGHT_SCALE;
			objc->r_expErr[i] = (float) r_plErr[i] / PL_HALFLIGHT_SCALE;
			objc->ab_exp[i] = (float) 1.0;
			#endif
		} else {
			#if USE_FNAL_OBJC_IO
			objc->counts_pl[i] = (float) 0.0;
			objc->r_pl[i] = (float) VALUE_IS_BAD;	
			objc->r_plErr[i] = (float) 0.0;	
			objc->ab_pl[i] = (float) 1.0;
			objc->n_pl[i] = n_pl[i];
			objc->n_plErr[i] = n_plErr[i];	
			objc->counts_exp2[i] = (float) 0.0;
			objc->r_exp2[i] = (float) VALUE_IS_BAD;
			objc->ab_exp2[i] = (float) 1.0;
			#else
			objc->counts_exp[i] = (float) 0.0;
			objc->r_exp[i] = (float) VALUE_IS_BAD;
			objc->r_expErr[i] = (float) 0.0;
			objc->ab_exp[i] = (float) 1.0;
			#endif
		}

		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	if (demand % 10 > 5) {
		cdclass = DEVPL_GCLASS;
	} else {
		cdclass = DEV_GCLASS;
	}
	if (((demand / 10)%10)%2 == 1) galaxy_psf_convolution = NO_PSF_CONVOLUTION;

} else if (demand == 63 || demand == 73 || demand == 68 || demand == 78) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {8.5E4, 8.5E4, 8.5E4, 8.5E4, 8.5E4}; /* 10th percentile */
	float r_deV[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; /* a little bit less than the third quartile */
	float r_deVErr[NCOLOR] = {0.7, 0.7, 0.7, 0.7, 0.7}; /* a little bit less than the third quartile */
	float counts_pl[NCOLOR] = {8.5E4, 8.5E4, 8.5E4, 8.5E4, 8.5E4}; /* 10th percentile */
	float r_pl[NCOLOR] = {6.0, 6.0, 6.0, 6.0, 6.0}; /* a little bit less than the third quartile */	
	float r_plErr[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; /* a little bit less than the third quartile */	
	float n_pl[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; 
	float n_plErr[NCOLOR] = {0.2, 0.2, 0.2, 0.2, 0.2};
 
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;

		if ((demand % 10) >  5) {
			#if USE_FNAL_OBJC_IO
			objc->counts_pl[i] = (float) counts_pl[i];
			objc->r_pl[i] = (float) r_pl[i] / PL_HALFLIGHT_SCALE;	
			objc->r_plErr[i] = (float) r_plErr[i] / PL_HALFLIGHT_SCALE;	
			objc->ab_pl[i] = (float) 1.0;
			objc->n_pl[i] = n_pl[i];
			objc->n_plErr[i] = n_plErr[i];		
			objc->counts_exp2[i] = (float) 0.0;
			objc->r_exp2[i] = (float) VALUE_IS_BAD;
				objc->ab_exp2[i] = (float) 1.0;
			#else
			objc->counts_exp[i] = (float) counts_pl[i];
			objc->r_exp[i] = (float) r_pl[i] / PL_HALFLIGHT_SCALE;
			objc->r_expErr[i] = (float) r_plErr[i] / PL_HALFLIGHT_SCALE;
			objc->ab_exp[i] = (float) 1.0;
			#endif
		} else {
			#if USE_FNAL_OBJC_IO
			objc->counts_pl[i] = (float) 0.0;
			objc->r_pl[i] = (float) VALUE_IS_BAD;	
			objc->r_plErr[i] = (float) 0.0;	
			objc->ab_pl[i] = (float) 1.0;
			objc->n_pl[i] = n_pl[i];
			objc->n_plErr[i] = n_plErr[i];	
			objc->counts_exp2[i] = (float) 0.0;
			objc->r_exp2[i] = (float) VALUE_IS_BAD;
			objc->ab_exp2[i] = (float) 1.0;
			#else
			objc->counts_exp[i] = (float) 0.0;
			objc->r_exp[i] = (float) VALUE_IS_BAD;
			objc->r_expErr[i] = (float) 0.0;
			objc->ab_exp[i] = (float) 1.0;
			#endif
		}

		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	if (demand % 10 > 5) {
		cdclass = DEVPL_GCLASS;
	} else {
		cdclass = DEV_GCLASS;
	}
	if (((demand / 10)%10)%2 == 1) galaxy_psf_convolution = NO_PSF_CONVOLUTION;

} else if (demand == 64 || demand == 74 || demand == 69 || demand == 79) {
	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {8.5E4, 8.5E4, 8.5E4, 8.5E4, 8.5E4}; /* 10th percentile */
	float r_deV[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; /* a little bit less than the third quartile */
	float r_deVErr[NCOLOR] = {0.7, 0.7, 0.7, 0.7, 0.7}; /* a little bit less than the third quartile */
	float counts_pl[NCOLOR] = {1.5E5, 1.5E5, 1.5E5, 1.5E5, 1.5E5}; /* 10th percentile */
	float r_pl[NCOLOR] = {8.0, 8.0, 8.0, 8.0, 8.0}; /* a little bit less than the third quartile */	
	float r_plErr[NCOLOR] = {2.7, 2.7, 2.7, 2.7, 2.7}; /* a little bit less than the third quartile */	
	float n_pl[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; 
	float n_plErr[NCOLOR] = {0.2, 0.2, 0.2, 0.2, 0.2};
 
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i];	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;

		if ((demand % 10) > 5) {
			#if USE_FNAL_OBJC_IO
			objc->counts_pl[i] = (float) counts_pl[i];
			objc->r_pl[i] = (float) r_pl[i] / PL_HALFLIGHT_SCALE;	
			objc->r_plErr[i] = (float) r_plErr[i] / PL_HALFLIGHT_SCALE;	
			objc->ab_pl[i] = (float) 1.0;
			objc->n_pl[i] = n_pl[i];
			objc->n_plErr[i] = n_plErr[i];
			objc->counts_exp2[i] = (float) 0.0;
			objc->r_exp2[i] = (float) VALUE_IS_BAD;
				objc->ab_exp2[i] = (float) 1.0;
			#else
			objc->counts_exp[i] = (float) counts_pl[i];
			objc->r_exp[i] = (float) r_pl[i] / PL_HALFLIGHT_SCALE;
			objc->r_expErr[i] = (float) r_plErr[i] / PL_HALFLIGHT_SCALE;
			objc->ab_exp[i] = (float) 1.0;
			#endif
		} else {
			#if USE_FNAL_OBJC_IO
			objc->counts_pl[i] = (float) 0.0;
			objc->r_pl[i] = (float) VALUE_IS_BAD;	
			objc->r_plErr[i] = (float) 0.0;	
			objc->ab_pl[i] = (float) 1.0;
			objc->n_pl[i] = n_pl[i];
			objc->n_plErr[i] = n_plErr[i];	
			objc->counts_exp2[i] = (float) 0.0;
			objc->r_exp2[i] = (float) VALUE_IS_BAD;
			objc->ab_exp2[i] = (float) 1.0;
			#else
			objc->counts_exp[i] = (float) 0.0;
			objc->r_exp[i] = (float) VALUE_IS_BAD;
			objc->r_expErr[i] = (float) 0.0;
			objc->ab_exp[i] = (float) 1.0;
			#endif
		}

		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	if (demand % 10 > 5) {
		cdclass = DEVPL_GCLASS;
	} else {
		cdclass = DEV_GCLASS;
	}
	if (((demand / 10)%10)%2 == 1) galaxy_psf_convolution = NO_PSF_CONVOLUTION;

} else if (demand == 81 || demand == 91 || demand == 86 || demand == 96) {
	float counts = 0.0;
	if (demand % 10 > 5) {
		counts = 5.0E5;
	} else {
		counts = 2.5E5;
	}

	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_csersic[NCOLOR] = {counts, counts, counts, counts, counts}; 
	float rb_csersic[NCOLOR] = {6.0, 6.0, 6.0, 6.0, 6.0};
	float rb_csersicErr[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0};
	float re_csersic[NCOLOR] = {15.0, 15.0, 15.0, 15.0, 15.0};
	float re_csersicErr[NCOLOR] = {5.0, 5.0, 5.0, 5.0, 5.0};	
	float d_csersic[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; 
	float d_csersicErr[NCOLOR] = {0.2, 0.2, 0.2, 0.2, 0.2};
 	float n_csersic[NCOLOR] = {4.0, 4.0, 4.0, 4.0, 4.0};
	float n_csersicErr[NCOLOR] = {1.0, 1.0, 1.0, 1.0, 1.0};
	float g_csersic[NCOLOR] = {0.1, 0.1, 0.1, 0.1, 0.1};
	float g_csersicErr[NCOLOR] = {0.05, 0.05, 0.05, 0.05, 0.05};

	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		#if USE_FNAL_OBJC_IO
	
		objc->counts_csersic[i] = (float) counts_csersic[i];
		objc->r_csersic[i] = (float) re_csersic[i];	
		objc->r_csersicErr[i] = (float) re_csersicErr[i];	
		objc->n_csersic[i] = (float) n_csersic[i];	
		objc->n_csersicErr[i] = (float) n_csersicErr[i];	
		objc->g_csersic[i] = (float) g_csersic[i];	
		objc->g_csersicErr[i] = (float) g_csersicErr[i];	
		objc->d_csersic[i] = (float) d_csersic[i];	
		objc->d_csersicErr[i] = (float) d_csersicErr[i];	
		objc->rb_csersic[i] = (float) rb_csersic[i];	
		objc->rb_csersicErr[i] = (float) rb_csersicErr[i];	
		objc->ab_csersic[i] = (float) 1.0;
		objc->ab_csersicErr[i] = (float) 0.0;
		objc->phi_csersic[i] = (float) 0.0;	
		objc->phi_csersicErr[i] = (float) 0.0;	

		objc->counts_deV[i] = (float) 0.0;
		objc->r_deV[i] = (float) VALUE_IS_BAD;	
		objc->r_deVErr[i] = (float) 0.0;	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0;
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->r_expErr[i] = (float) 0.0;
		objc->ab_exp[i] = (float) 1.0;

		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;	
		objc->r_expErr[i] = (float) 0.0;	
		objc->ab_exp[i] = (float) 1.0;
		
		objc->counts_pl[i] = (float) 0.0;
		objc->r_pl[i] = (float) VALUE_IS_BAD;
		objc->ab_pl[i] = (float) 1.0;
		objc->n_pl[i] = (float) VALUE_IS_BAD;
		objc->n_plErr[i] = (float) VALUE_IS_BAD;

		objc->counts_exp2[i] = (float) 0.0;
		objc->r_exp2[i] = (float) VALUE_IS_BAD;
		objc->r_exp2Err[i] = (float) VALUE_IS_BAD;
		objc->ab_exp2[i] = (float) 1.0;

		objc->counts_deV2[i] = (float) 0.0;
		objc->r_deV2[i] = (float) VALUE_IS_BAD;
		objc->r_deV2Err[i] = (float) VALUE_IS_BAD;
		objc->ab_deV2[i] = (float) 1.0;

		#else
		
		thError("%s: WOBJC_IO not supported for (demand = %d)", name, demand);
		return(SH_GENERIC_ERROR);
	
		#endif

		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	cdclass = CORESERSIC_GCLASS;
	if (((demand / 10)%10)%2 == 1) galaxy_psf_convolution = NO_PSF_CONVOLUTION;

} else if (demand == 82 || demand == 92 || demand == 87 || demand == 97) {
	
	float counts = 0.0;
	if (demand % 10 > 5) {
		counts = 1.5E5;
	} else {
		counts = 8.5E4;
	}

	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_csersic[NCOLOR] = {counts, counts, counts, counts, counts};
	float rb_csersic[NCOLOR] = {6.0, 6.0, 6.0, 6.0, 6.0};
	float rb_csersicErr[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0};
	float re_csersic[NCOLOR] = {15.0, 15.0, 15.0, 15.0, 15.0};
	float re_csersicErr[NCOLOR] = {5.0, 5.0, 5.0, 5.0, 5.0};
	float d_csersic[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; 
	float d_csersicErr[NCOLOR] = {0.2, 0.2, 0.2, 0.2, 0.2};
 	float n_csersic[NCOLOR] = {4.0, 4.0, 4.0, 4.0, 4.0};
	float n_csersicErr[NCOLOR] = {1.0, 1.0, 1.0, 1.0, 1.0};
	float g_csersic[NCOLOR] = {0.1, 0.1, 0.1, 0.1, 0.1};
	float g_csersicErr[NCOLOR] = {0.05, 0.05, 0.05, 0.05, 0.05};


	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
		
		#if USE_FNAL_OBJC_IO
	
		objc->counts_csersic[i] = (float) counts_csersic[i];
		objc->r_csersic[i] = (float) re_csersic[i];	
		objc->r_csersicErr[i] = (float) re_csersicErr[i];	
		objc->n_csersic[i] = (float) n_csersic[i];	
		objc->n_csersicErr[i] = (float) n_csersicErr[i];	
		objc->g_csersic[i] = (float) g_csersic[i];	
		objc->g_csersicErr[i] = (float) g_csersicErr[i];	
		objc->d_csersic[i] = (float) d_csersic[i];	
		objc->d_csersicErr[i] = (float) d_csersicErr[i];	
		objc->rb_csersic[i] = (float) rb_csersic[i];	
		objc->rb_csersicErr[i] = (float) rb_csersicErr[i];	
		objc->ab_csersic[i] = (float) 1.0;
		objc->ab_csersicErr[i] = (float) 0.0;
		objc->phi_csersic[i] = (float) 0.0;	
		objc->phi_csersicErr[i] = (float) 0.0;	

		objc->counts_deV[i] = (float) 0.0;
		objc->r_deV[i] = (float) VALUE_IS_BAD;	
		objc->r_deVErr[i] = (float) 0.0;	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0;
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->r_expErr[i] = (float) 0.0;
		objc->ab_exp[i] = (float) 1.0;

		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;	
		objc->r_expErr[i] = (float) 0.0;	
		objc->ab_exp[i] = (float) 1.0;
		
		objc->counts_pl[i] = (float) 0.0;
		objc->r_pl[i] = (float) VALUE_IS_BAD;
		objc->ab_pl[i] = (float) 1.0;
		objc->n_pl[i] = (float) VALUE_IS_BAD;
		objc->n_plErr[i] = (float) VALUE_IS_BAD;

		objc->counts_exp2[i] = (float) 0.0;
		objc->r_exp2[i] = (float) VALUE_IS_BAD;
		objc->r_exp2Err[i] = (float) VALUE_IS_BAD;
		objc->ab_exp2[i] = (float) 1.0;

		objc->counts_deV2[i] = (float) 0.0;
		objc->r_deV2[i] = (float) VALUE_IS_BAD;
		objc->r_deV2Err[i] = (float) VALUE_IS_BAD;
		objc->ab_deV2[i] = (float) 1.0;

		#else
		
		thError("%s: WOBJC_IO not supported for (demand = %d)", name, demand);
		return(SH_GENERIC_ERROR);
	
		#endif


		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	cdclass = CORESERSIC_GCLASS;
	if (((demand / 10)%10)%2 == 1) galaxy_psf_convolution = NO_PSF_CONVOLUTION;

} else if (demand == 83 || demand == 93 || demand == 88 || demand == 98) {

	float counts = 0.0;
	if (demand % 10 > 5) {
		counts = 5.0E5;
	} else {
		counts = 2.5E5;
	}

	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_csersic[NCOLOR] = {counts, counts, counts, counts, counts}; 
	float rb_csersic[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0};
	float rb_csersicErr[NCOLOR] = {0.7, 0.7, 0.7, 0.7, 0.7};
	float re_csersic[NCOLOR] = {8.0, 8.0, 8.0, 8.0, 8.0};
	float re_csersicErr[NCOLOR] = {2.7, 2.7, 2.7, 2.7, 2.7};
	float d_csersic[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; 
	float d_csersicErr[NCOLOR] = {0.2, 0.2, 0.2, 0.2, 0.2};
 	float n_csersic[NCOLOR] = {4.0, 4.0, 4.0, 4.0, 4.0};
	float n_csersicErr[NCOLOR] = {1.0, 1.0, 1.0, 1.0, 1.0};
	float g_csersic[NCOLOR] = {0.1, 0.1, 0.1, 0.1, 0.1};
	float g_csersicErr[NCOLOR] = {0.05, 0.05, 0.05, 0.05, 0.05};


	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		#if USE_FNAL_OBJC_IO
	
		objc->counts_csersic[i] = (float) counts_csersic[i];
		objc->r_csersic[i] = (float) re_csersic[i];	
		objc->r_csersicErr[i] = (float) re_csersicErr[i];	
		objc->n_csersic[i] = (float) n_csersic[i];	
		objc->n_csersicErr[i] = (float) n_csersicErr[i];	
		objc->g_csersic[i] = (float) g_csersic[i];	
		objc->g_csersicErr[i] = (float) g_csersicErr[i];	
		objc->d_csersic[i] = (float) d_csersic[i];	
		objc->d_csersicErr[i] = (float) d_csersicErr[i];	
		objc->rb_csersic[i] = (float) rb_csersic[i];	
		objc->rb_csersicErr[i] = (float) rb_csersicErr[i];	
		objc->ab_csersic[i] = (float) 1.0;
		objc->ab_csersicErr[i] = (float) 0.0;
		objc->phi_csersic[i] = (float) 0.0;	
		objc->phi_csersicErr[i] = (float) 0.0;	

		objc->counts_deV[i] = (float) 0.0;
		objc->r_deV[i] = (float) VALUE_IS_BAD;	
		objc->r_deVErr[i] = (float) 0.0;	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0;
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->r_expErr[i] = (float) 0.0;
		objc->ab_exp[i] = (float) 1.0;

		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;	
		objc->r_expErr[i] = (float) 0.0;	
		objc->ab_exp[i] = (float) 1.0;
		
		objc->counts_pl[i] = (float) 0.0;
		objc->r_pl[i] = (float) VALUE_IS_BAD;
		objc->ab_pl[i] = (float) 1.0;
		objc->n_pl[i] = (float) VALUE_IS_BAD;
		objc->n_plErr[i] = (float) VALUE_IS_BAD;

		objc->counts_exp2[i] = (float) 0.0;
		objc->r_exp2[i] = (float) VALUE_IS_BAD;
		objc->r_exp2Err[i] = (float) VALUE_IS_BAD;
		objc->ab_exp2[i] = (float) 1.0;

		objc->counts_deV2[i] = (float) 0.0;
		objc->r_deV2[i] = (float) VALUE_IS_BAD;
		objc->r_deV2Err[i] = (float) VALUE_IS_BAD;
		objc->ab_deV2[i] = (float) 1.0;

		#else
		
		thError("%s: WOBJC_IO not supported for (demand = %d)", name, demand);
		return(SH_GENERIC_ERROR);
	
		#endif
	
		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	cdclass = CORESERSIC_GCLASS;
	if (((demand / 10)%10)%2 == 1) galaxy_psf_convolution = NO_PSF_CONVOLUTION;

} else if (demand == 84 || demand == 94 || demand == 89 || demand == 99) {
	float counts = 0.0;
	if (demand % 10 > 5) {
		counts = 1.5E5;
	} else {
		counts = 8.5E4;
	}

	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_csersic[NCOLOR] = {counts, counts, counts, counts, counts};
	float rb_csersic[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0};
	float rb_csersicErr[NCOLOR] = {0.7, 0.7, 0.7, 0.7, 0.7};
	float re_csersic[NCOLOR] = {8.0, 8.0, 8.0, 8.0, 8.0};
	float re_csersicErr[NCOLOR] = {2.7, 2.7, 2.7, 2.7, 2.7};
	float d_csersic[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; 
	float d_csersicErr[NCOLOR] = {0.2, 0.2, 0.2, 0.2, 0.2};
 	float n_csersic[NCOLOR] = {4.0, 4.0, 4.0, 4.0, 4.0};
	float n_csersicErr[NCOLOR] = {1.0, 1.0, 1.0, 1.0, 1.0};
	float g_csersic[NCOLOR] = {0.1, 0.1, 0.1, 0.1, 0.1};
	float g_csersicErr[NCOLOR] = {0.05, 0.05, 0.05, 0.05, 0.05};

	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		#if USE_FNAL_OBJC_IO
	
		objc->counts_csersic[i] = (float) counts_csersic[i];
		objc->r_csersic[i] = (float) re_csersic[i];	
		objc->r_csersicErr[i] = (float) re_csersicErr[i];	
		objc->n_csersic[i] = (float) n_csersic[i];	
		objc->n_csersicErr[i] = (float) n_csersicErr[i];	
		objc->g_csersic[i] = (float) g_csersic[i];	
		objc->g_csersicErr[i] = (float) g_csersicErr[i];	
		objc->d_csersic[i] = (float) d_csersic[i];	
		objc->d_csersicErr[i] = (float) d_csersicErr[i];	
		objc->rb_csersic[i] = (float) rb_csersic[i];	
		objc->rb_csersicErr[i] = (float) rb_csersicErr[i];	
		objc->ab_csersic[i] = (float) 1.0;
		objc->ab_csersicErr[i] = (float) 0.0;
		objc->phi_csersic[i] = (float) 0.0;	
		objc->phi_csersicErr[i] = (float) 0.0;	

		objc->counts_deV[i] = (float) 0.0;
		objc->r_deV[i] = (float) VALUE_IS_BAD;	
		objc->r_deVErr[i] = (float) 0.0;	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0;
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->r_expErr[i] = (float) 0.0;
		objc->ab_exp[i] = (float) 1.0;

		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;	
		objc->r_expErr[i] = (float) 0.0;	
		objc->ab_exp[i] = (float) 1.0;
		
		objc->counts_pl[i] = (float) 0.0;
		objc->r_pl[i] = (float) VALUE_IS_BAD;
		objc->ab_pl[i] = (float) 1.0;
		objc->n_pl[i] = (float) VALUE_IS_BAD;
		objc->n_plErr[i] = (float) VALUE_IS_BAD;

		objc->counts_exp2[i] = (float) 0.0;
		objc->r_exp2[i] = (float) VALUE_IS_BAD;
		objc->r_exp2Err[i] = (float) VALUE_IS_BAD;
		objc->ab_exp2[i] = (float) 1.0;

		objc->counts_deV2[i] = (float) 0.0;
		objc->r_deV2[i] = (float) VALUE_IS_BAD;
		objc->r_deV2Err[i] = (float) VALUE_IS_BAD;
		objc->ab_deV2[i] = (float) 1.0;

		#else
		
		thError("%s: WOBJC_IO not supported for (demand = %d)", name, demand);
		return(SH_GENERIC_ERROR);
	
		#endif
	
		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	cdclass = CORESERSIC_GCLASS;
	if (((demand / 10)%10)%2 == 1) galaxy_psf_convolution = NO_PSF_CONVOLUTION;

} else if (demand % 100 == 57) {

	float sky[NCOLOR] = {150.0, 150.0, 150.0, 150.0, 150.0};
	float counts_deV[NCOLOR] = {8.5E4, 8.5E4, 8.5E4, 8.5E4, 8.5E4}; /* 10th percentile */
	float r_deVErr[NCOLOR] = {3.0, 3.0, 3.0, 3.0, 3.0}; 
	float counts_pl[NCOLOR] = {8.5E4, 8.5E4, 8.5E4, 8.5E4, 8.5E4}; /* 10th percentile */
	float r_plErr[NCOLOR] = {9.0, 9.0, 9.0, 9.0, 9.0}; /* a little bit less than the third quartile */
	float n_pl[NCOLOR] = {2.0, 2.0, 2.0, 2.0, 2.0}; 
	float n_plErr[NCOLOR] = {0.2, 0.2, 0.2, 0.2, 0.2};
	float rowc = ((float) DEFAULT_NROW) / ((float) 2.0); /* at the center of FOV */
	float colc = ((float) DEFAULT_NCOL) / ((float) 2.0); /* at the center of FOV */
	#if USE_FNAL_OBJC_IO
	int flagLRG = 1;
	#endif

	printf("%s: local minima for demand code = %d \n", name, demand);
	float r_deV_item = VALUE_IS_BAD, r_pl_item = VALUE_IS_BAD;
	if (demand == 257) {	
		r_deV_item = 10.78193485;
		r_pl_item = 33.12847529;
	} else if (demand == 357) {
		r_deV_item = 10.06387753;
		r_pl_item = 31.69941528;
	} else if (demand == 457) {
		r_deV_item = 9.608561364;
		r_pl_item = 30.80728476;
	} else if (demand == 557) {
		r_deV_item = 9.278217424;
		r_pl_item = 30.13676635;
	} else if (demand == 657) {
		r_deV_item = 6.802534596;
		r_pl_item = 25.08793433;
	} else {
		thError("%s: ERROR - (demand code = %d) not supported", name, demand);
		return(SH_GENERIC_ERROR);
	}
	float r_deV[NCOLOR] = {r_deV_item, r_deV_item, r_deV_item, r_deV_item, r_deV_item};
	float r_pl[NCOLOR] = {r_pl_item, r_pl_item, r_pl_item, r_pl_item, r_pl_item};

	#if 0
	for (i = 0; i < NCOLOR; i++) {
		printf("%s: r_deV[%d] = %g, r_pl[%d] = %g \n", name, i, r_deV[i], i, r_pl[i]);
	}
	#endif
	objc = thWObjcIoNew(NCOLOR);
	memset(objc, '\0', sizeof(objc));

	for (i = 0; i < NCOLOR; i++) {
		
		skylist[i] = thSkyparsNew();
		skylist[i]->I_s00 = (THPIX) sky[i];;	
		skylist[i]->I_z00 = skylist[i]->I_s00;
	
		objc->counts_deV[i] = (float) counts_deV[i];
		objc->r_deV[i] = (float) r_deV[i];	
		objc->r_deVErr[i] = (float) r_deVErr[i] / DEMAND_PAR_ERR_ADJUSTMENT;	
		objc->ab_deV[i] = (float) 1.0;
		objc->fracPSF[i] = (float) 1.0; 
		objc->counts_exp[i] = (float) 0.0;
		objc->r_exp[i] = (float) VALUE_IS_BAD;
		objc->ab_exp[i] = (float) 1.0;

		#if USE_FNAL_OBJC_IO
		objc->counts_pl[i] = (float) counts_pl[i];
		objc->r_pl[i] = (float) r_pl[i] / PL_HALFLIGHT_SCALE;	
		objc->r_plErr[i] = (float) r_plErr[i] / PL_HALFLIGHT_SCALE / DEMAND_PAR_ERR_ADJUSTMENT;	
		objc->ab_pl[i] = (float) 1.0;
		objc->n_pl[i] = n_pl[i];
		objc->n_plErr[i] = n_plErr[i];		
		objc->counts_exp2[i] = (float) 0.0;
		objc->r_exp2[i] = (float) VALUE_IS_BAD;
		objc->ab_exp2[i] = (float) 1.0;
		#else
		objc->counts_exp[i] = (float) counts_pl[i];
		objc->r_exp[i] = (float) r_pl[i] / PL_HALFLIGHT_SCALE;
		objc->r_expErr[i] = (float) r_plErr[i] / PL_HALFLIGHT_SCALE;
		objc->ab_exp[i] = (float) 1.0;
		#endif


		objc->objc_rowc = (float) rowc;
		objc->objc_colc = (float) colc;
		objc->rowc[i] = (float) rowc;
		objc->colc[i] = (float) colc;
		objc->type[i] = OBJ_GALAXY;
	}
	
	objc->objc_type = OBJ_GALAXY;
	#if USE_FNAL_OBJC_IO
	objc->flagLRG = flagLRG;
	#endif
	shChainElementAddByPos(objclist_out, objc, objc_type_name, TAIL, AFTER);
	cdclass = DEVPL_GCLASS;
	galaxy_psf_convolution = NO_PSF_CONVOLUTION;


} else {

	thError("%s: ERROR - (demand code = %d) not supported", name, demand);
	thFree(skylist);
	*skylist_out = NULL;
	return(SH_GENERIC_ERROR);
}

*cdclass_out = cdclass;

printf("%s: setting galaxy psf convolution type to '%s' \n", name, shEnumNameGetFromValue("PSF_CONVOLUTION_TYPE", galaxy_psf_convolution));
*galaxy_psf_out = galaxy_psf_convolution;

return(SH_SUCCESS);
}

RET_CODE initdemandparser(int demand, CHAIN *objclist_out, SKYPARS ***skylist_out, PSF_CONVOLUTION_TYPE *galaxy_psf_out, GCLASS *cdclass_out) {
char *name = "initdemandparser";

shAssert(cdclass_out != NULL);
RET_CODE status;

int demand_psf_sim = demand % 100;
int demand_sim = demand % 10;
int demand_fit = demand / 100;
if (demand_fit >= 2) {
	demand_psf_sim = demand;
}
if (demand_sim < 5 && demand_fit == 1) {
	demand_psf_sim += 5;
} else if (demand_sim  > 5 && demand_fit == 1) {
	demand_psf_sim -= 5;
}

GCLASS cd_class_fit;
status = demandparser(demand_psf_sim, objclist_out, skylist_out, galaxy_psf_out, &cd_class_fit);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not parse sim demand (code= %d) from demand (code = %d)", name, demand_sim, demand);
	return(status);
}
printf("%s: target cD class set to '%s' because demand (code = %d) was detected \n", name, shEnumNameGetFromValue("GCLASS", cd_class_fit), demand);
*cdclass_out = cd_class_fit;

printf("%s: setting fit target class for cD-galaxies to '%s' \n", name, shEnumNameGetFromValue("GCLASS", cd_class_fit));
*cdclass_out = cd_class_fit;
return(SH_SUCCESS);
}

unsigned int generate_seed(unsigned int seed0) {
#if MANIPULATE_SEED_WITH_TIME
time_t seconds = time(NULL);
unsigned int u_seconds = (unsigned int) seconds;
unsigned int seed = seed0 ^ u_seconds;
return(seed);
#else
return(seed0);
#endif
}

RET_CODE mgalaxyparser(char *arg, MGALAXY_PARSED_INPUT *input) {
char *name = "mgalaxyparser";

shAssert(input != NULL);
int prev_optind = optind;
optind = 1;
int argc; char **argv;
argv = str_all_tokens(arg, " ", &argc);

struct option longopts[] = {
	{"N_SERSIC_1",     required_argument, NULL, 'a'}, 
	{"N_SERSIC_2",     required_argument, NULL, 'b'}, 
	{"N_SERSIC1_1",     required_argument, NULL, 'c'}, 
	{"N_SERSIC1_2",     required_argument, NULL, 'd'}, 
	{"N_SERSIC2_1",     required_argument, NULL, 'e'}, 
	{"N_SERSIC2_2",     required_argument, NULL, 'f'}, 
	{"N_SERSIC_DRAW_1",     required_argument, NULL, 'g'}, 
	{"N_SERSIC_DRAW_2",     required_argument, NULL, 'h'}, 
	{"N_SERSIC_DRAW1_1",     required_argument, NULL, 'i'}, 
	{"N_SERSIC_DRAW1_2",     required_argument, NULL, 'j'}, 
	{"N_SERSIC_DRAW2_1",     required_argument, NULL, 'k'}, 
	{"N_SERSIC_DRAW2_2",     required_argument, NULL, 'l'}, 
	{"N_CORESERSIC_1",     required_argument, NULL, 'm'}, 
	{"N_CORESERSIC_2",     required_argument, NULL, 'n'}, 
	{"DELTA_CORESERSIC_1",     required_argument, NULL, 'o'}, 
	{"DELTA_CORESERSIC_2",     required_argument, NULL, 'p'}, 
	{"DELTA_CORESERSIC",     required_argument, NULL, 'q'}, 
	{"GAMMA_CORESERSIC_1",     required_argument, NULL, 'r'}, 
	{"GAMMA_CORESERSIC_2",     required_argument, NULL, 's'}, 
	{"GAMMA_CORESERSIC",     required_argument, NULL, 't'}, 
 	{"N_CORESERSIC_DRAW_1",     required_argument, NULL, 'u'}, 
	{"N_CORESERSIC_DRAW_2",     required_argument, NULL, 'v'}, 
	{"DELTA_CORESERSIC_DRAW_1",     required_argument, NULL, 'w'}, 
	{"DELTA_CORESERSIC_DRAW_2",     required_argument, NULL, 'x'}, 
	{"GAMMA_CORESERSIC_DRAW_1",     required_argument, NULL, 'y'}, 
	{"GAMMA_CORESERSIC_DRAW_2",     required_argument, NULL, 'z'}, 
	{ 0, 0, 0, 0 }
};

memset(input, '\0', sizeof(MGALAXY_PARSED_INPUT));

char c;
while ((c = getopt_long_only(argc, argv, "", longopts, NULL)) != -1) {
    switch (c) {
	case 'a':
		sscanf(optarg, "%g", &(input->N_SERSIC_1));
		input->lN_SERSIC_1 = 1;
		break;	
	case 'b':
		sscanf(optarg, "%g", &(input->N_SERSIC_2));
		input->lN_SERSIC_2 = 1;
		break;	
	case 'c':
		sscanf(optarg, "%g", &(input->N_SERSIC1_1));
		input->lN_SERSIC1_1 = 1;
		break;	
	case 'd':
		sscanf(optarg, "%g", &(input->N_SERSIC1_2));
		input->lN_SERSIC1_2 = 1;
		break;	
	case 'e':
		sscanf(optarg, "%g", &(input->N_SERSIC2_1));
		input->lN_SERSIC2_1 = 1;
		break;	
	case 'f':
		sscanf(optarg, "%g", &(input->N_SERSIC2_2));
		input->lN_SERSIC2_2 = 1;
		break;	
	case 'g':
		sscanf(optarg, "%g", &(input->N_SERSIC_DRAW_1));
		input->lN_SERSIC_DRAW_1 = 1;
		break;
 	case 'h':
		sscanf(optarg, "%g", &(input->N_SERSIC_DRAW_2));
		input->lN_SERSIC_DRAW_2 = 1;
		break; 
	case 'i':
		sscanf(optarg, "%g", &(input->N_SERSIC_DRAW1_1));
		input->lN_SERSIC_DRAW1_1 = 1;
		break; 
	case 'j':
		sscanf(optarg, "%g", &(input->N_SERSIC_DRAW1_2));
		input->lN_SERSIC_DRAW1_2 = 1;
		break; 
	case 'k':
		sscanf(optarg, "%g", &(input->N_SERSIC_DRAW2_1));
		input->lN_SERSIC_DRAW2_1 = 1;
		break; 
	case 'l':
		sscanf(optarg, "%g", &(input->N_SERSIC_DRAW2_2));
		input->lN_SERSIC_DRAW2_2 = 1;
		break; 
	case 'm':
		sscanf(optarg, "%g", &(input->N_CORESERSIC_1));
		input->lN_CORESERSIC_1 = 1;
		break; 
	case 'n':
		sscanf(optarg, "%g", &(input->N_CORESERSIC_2));
		input->lN_CORESERSIC_2 = 1;
		break; 
	case 'o':
		sscanf(optarg, "%g", &(input->DELTA_CORESERSIC_1));
		input->lDELTA_CORESERSIC_1 = 1;
		break; 
	case 'p':
		sscanf(optarg, "%g", &(input->DELTA_CORESERSIC_2));
		input->lDELTA_CORESERSIC_2 = 1;
		break; 
	case 'q':
		sscanf(optarg, "%g", &(input->DELTA_CORESERSIC));
		input->lDELTA_CORESERSIC = 1;
		break; 
	case 'r':
		sscanf(optarg, "%g", &(input->GAMMA_CORESERSIC_1));
		input->lGAMMA_CORESERSIC_1 = 1;
		break; 
	case 's':
		sscanf(optarg, "%g", &(input->GAMMA_CORESERSIC_2));
		input->lGAMMA_CORESERSIC_2 = 1;
		break; 
	case 't':
		sscanf(optarg, "%g", &(input->GAMMA_CORESERSIC));
		input->lGAMMA_CORESERSIC = 1;
		break;
	case 'u':
		sscanf(optarg, "%g", &(input->N_CORESERSIC_DRAW_1));
		input->lN_CORESERSIC_DRAW_1 = 1;
		break; 
	case 'v':
		sscanf(optarg, "%g", &(input->N_CORESERSIC_DRAW_2));
		input->lN_CORESERSIC_DRAW_2 = 1;
		break; 
	case 'w':
		sscanf(optarg, "%g", &(input->DELTA_CORESERSIC_DRAW_1));
		input->lDELTA_CORESERSIC_DRAW_1 = 1;
		break; 
	case 'x':
		sscanf(optarg, "%g", &(input->DELTA_CORESERSIC_DRAW_2));
		input->lDELTA_CORESERSIC_DRAW_2 = 1;
		break; 
	case 'y':
		sscanf(optarg, "%g", &(input->GAMMA_CORESERSIC_DRAW_1));
		input->lGAMMA_CORESERSIC_DRAW_1 = 1;
		break; 
	case 'z':
		sscanf(optarg, "%g", &(input->GAMMA_CORESERSIC_DRAW_2));
		input->lGAMMA_CORESERSIC_DRAW_2 = 1;
		break; 
	case 0:     /* getopt_long() set a variable, just keep going */
       		 break;
    	case ':':   /* missing option argument */
        	fprintf(stderr, "%s: ERROR - %s: option `-%c' requires an argument\n",
             	  	 name, argv[0], optopt);
		return(SH_GENERIC_ERROR);
       		 break;
   	 case '?':
  	 default:    /* invalid option */
        	fprintf(stderr, "%s: ERROR - %s: option `-%c' is invalid: ignored\n",
                	name, argv[0], optopt);
		return(SH_GENERIC_ERROR);
        	break;
    }
}

optind = prev_optind; 
return(SH_SUCCESS);
}

RET_CODE ctransformparser(char *arg, CTRANSFORM_PARSED_INPUT *input) {
char *name = "ctransformparser";

shAssert(input != NULL);
int prev_optind = optind;
optind = 1;
int argc; char **argv;
argv = str_all_tokens(arg, " ", &argc);

struct option longopts[] = {
	{"SERSIC_INDEX_SMALL", required_argument, NULL, 'a'},
	{"CORESERSIC_INDEX_SMALL", required_argument, NULL, 'b'},
	{"SERSIC1_INDEX_SMALL", required_argument, NULL, 'c'},
	{"SERSIC2_INDEX_SMALL", required_argument, NULL, 'd'},
	{"SERSIC_INDEX_LARGE", required_argument, NULL, 'e'},
	{"CORESERSIC_INDEX_LARGE", required_argument, NULL, 'f'},
	{"SERSIC1_INDEX_LARGE", required_argument, NULL, 'g'},
	{"SERSIC2_INDEX_LARGE", required_argument, NULL, 'h'},
	{ 0, 0, 0, 0 }
};

memset(input, '\0', sizeof(CTRANSFORM_PARSED_INPUT));

char c;
while ((c = getopt_long_only(argc, argv, "", longopts, NULL)) != -1) {
    switch (c) {
	case 'a':
		sscanf(optarg, "%g", &(input->SERSIC_INDEX_SMALL));
		input->lSERSIC_INDEX_SMALL = 1;
		break;	
	case 'b':
		sscanf(optarg, "%g", &(input->CORESERSIC_INDEX_SMALL));
		input->lCORESERSIC_INDEX_SMALL = 1;
		break;	
	case 'c':
		sscanf(optarg, "%g", &(input->SERSIC1_INDEX_SMALL));
		input->lSERSIC1_INDEX_SMALL = 1;
		break;	
	case 'd':
		sscanf(optarg, "%g", &(input->SERSIC2_INDEX_SMALL));
		input->lSERSIC2_INDEX_SMALL = 1;
		break;	
	case 'e':
		sscanf(optarg, "%g", &(input->SERSIC_INDEX_LARGE));
		input->lSERSIC_INDEX_LARGE = 1;
		break;	
	case 'f':
		sscanf(optarg, "%g", &(input->CORESERSIC_INDEX_LARGE));
		input->lCORESERSIC_INDEX_LARGE = 1;
		break;	
	case 'g':
		sscanf(optarg, "%g", &(input->SERSIC1_INDEX_LARGE));
		input->lSERSIC1_INDEX_LARGE = 1;
		break;
 	case 'h':
		sscanf(optarg, "%g", &(input->SERSIC2_INDEX_LARGE));
		input->lSERSIC2_INDEX_LARGE = 1;
		break;
	case 0:     /* getopt_long() set a variable, just keep going */
       		 break;
    	case ':':   /* missing option argument */
        	fprintf(stderr, "%s: ERROR - %s: option `-%c' requires an argument\n",
             	  	 name, argv[0], optopt);
		return(SH_GENERIC_ERROR);
       		 break;
   	 case '?':
  	 default:    /* invalid option */
        	fprintf(stderr, "%s: ERROR - %s: option `-%c' is invalid: ignored\n",
                	name, argv[0], optopt);
		return(SH_GENERIC_ERROR);
        	break;
    }
}

optind = prev_optind; 
return(SH_SUCCESS);
}
