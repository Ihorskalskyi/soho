#if !defined(THPHOTOFITSIO_H)
#define THPHOTOFITSIO_H

#include <sys/types.h>
#include <unistd.h>
#include "dervish.h"
#include "phPhotoFitsIO.h"

typedef struct {
   TYPE type;				/* type of data being written */
   int mode;				/* mode file was opened in */
   int fd;				/* file's unix file descriptor */
   int heapfd;				/* unix file descriptor for heap */
   long old_fd_pos, old_heapfd_pos;	/* old seek pointers; really off_t */
   long hdr_start;			/* offset of header block (really an
					   off_t, but that confuses dervish) */
   long hdr_end;			/* offset of end of header (another
					   off_t) */
   long heap_start;			/* offset of start of heap */
   long heap_end;			/* offset of end of heap */
   char *row;				/* The buffer used to read and write
					   the main table */
   int rowlen;				/* number of bytes in a row */
   int nrow;				/* number of rows in table */
   struct thfitsb_machine **machine;	/* binary table I/O FSM;
					   see photoFitsIO.c */
   struct thfitsb_machine **omachine;	/* optimised binary table I/O FSM;
					   see photoFitsIO.c */
   int start_ind;			/* which machine to start with */
   void **stack;			/* stack for machine */
   int stacksize;			/* size of stack */
   int *istack;				/* integer stack for machine */
   int istacksize;			/* size of istack */
} THFITSFILE;				/* pragma IGNORE */


int thFitsBinDeclareSchemaTrans(SCHEMATRANS *trans, char *type);
void thFitsBinForgetSchemaTrans(char *type);

int thFitsBinTblMachineTrace(char *type, int exec,  THFITSFILE *fil);

THFITSFILE *thFitsBinTblOpen(char *file, int flag, HDR *hdr);
int thFitsBinTblHdrRead(THFITSFILE *fd, char *type, HDR *hdr,
			int *nrow, int quiet);
int thFitsBinTblHdrWrite(THFITSFILE *fd, char *type, HDR *hdr);

int thFitsBinTblRowSeek(THFITSFILE *fil, int n, int whence);
int thFitsBinTblRowRead(THFITSFILE *fil, void *data);
int thFitsBinTblRowUnread(THFITSFILE *fil);
int thFitsBinTblRowWrite(THFITSFILE *fil, void *data);
int thFitsBinTblChainWrite(THFITSFILE *fil, CHAIN *chain);

int thFitsBinTblEnd(THFITSFILE *fd);
int thFitsBinTblClose(THFITSFILE *fil);

int p_thFitsBinMachineSet(struct thfitsb_machine *mac);

#endif
