#ifndef THFMDG_H
#define THFMDG_H

#include "thBasic.h"
#include "thQq.h"

RET_CODE FMDBGenInit(void *ptr_DBFuncLoaded, void *ptr_DBUploadValue, void *ptr_DBUploadHeader, void *ptr_DBSealHeader, void *ptr_QDraw, void *QGetSizeTable);
RET_CODE thDBGen(void *fmfits, RET_CODE (*f)(void *q, void *fm), void *wq, void *wfm, char *funcname, void *Qtable, char *type);

#endif
