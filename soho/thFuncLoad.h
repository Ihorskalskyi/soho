#ifndef THFUNCLOAD_H
#define THFUNCLOAD_H

#include "dervish.h"
#include "thMath.h"

typedef struct funcschema {
	char *fname;
	const void *fptr;
} FUNCSCHEMA;

FUNCSCHEMA *thFuncschemaNew();
void thFuncschemaDel(FUNCSCHEMA *s);

void thFuncLoadInit();
RET_CODE thFuncLoadAddEntry(char *fname, const void *fptr);
RET_CODE thFuncLoadDelEntry(char *fname);
RET_CODE thFuncLoadReplaceEntry(char *fname, const void *fptr);

FUNCSCHEMA *thFuncLoadFindEntryByName(char *name, int *pos);
FUNCSCHEMA *thFuncLoadFindEntryByPtr(const void *fptr, int *pos);

const void *thFuncPtrGetFromName(char *fname);
const char *thFuncNameGetFromPtr(const void *fptr);

#endif
