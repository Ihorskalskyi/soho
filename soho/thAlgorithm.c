#include "thAlgorithm.h"

/* work space */
static int init_n = 0;

/* --- */

static MEMFL *comparison_degree = NULL;
static int compare_degree(const void *x1, const void *x2);
static int compare_nm(const void *x1, const void *x2);

void free_algrorithm_work_space() {
	
	init_n = 0;	
	return;
}

	
RET_CODE design_algorithm(ADJ_MATRIX *adj_matrix, void *algorithm, ALGORITHM_DESIGN design, RUNSTAGE runstage) {

char *name = "design_algorithm";

if (adj_matrix == NULL) {
	thError("%s: ERROR - null adjacency matrix", name);
	return(SH_GENERIC_ERROR);
}
if (algorithm == NULL) {
	thError("%s: ERROR - null placeholder for output (algorithm)", name);
	return(SH_GENERIC_ERROR);
}
if (!(design & VALID_ALGORITHM_DESIGNS)) {
	thError("%s: ERROR - algorithm design (%d) does not match any known pattern", name, (int) design);
	return(SH_GENERIC_ERROR);
}
if ((design & CREATE_MODEL) && runstage != INITRUN) {
	thError("%s: ERROR - 'CREATE_MODEL' design only supported at 'INITRUN' stage", name);
	return(SH_GENERIC_ERROR);
}

if (runstage == ENDRUN && design != INNER_PRODUCTS) {
	thError("%s: ERROR - (runstage = 'ENDRUN') only supports (design = INNER_PRODUCTS)", name);
	return(SH_GENERIC_ERROR);
}

int n;
MEMFL **adj= adj_matrix->matrix;
MEMFL **wadj = adj_matrix->wmatrix;
MEMFL *degree = adj_matrix->degree;
MEMSTAT *memstat = adj_matrix->memstat;
FITSTAT *fitstat = adj_matrix->fitstat;
MEMFL *memspace = adj_matrix->memspace;
int *list = adj_matrix->list;

n = adj_matrix->n;
if (n <= 0) {
	thError("%s: graph size (n = %d) not acceptable", name, n);
	return(SH_GENERIC_ERROR);
}

int i;
for (i = 0; i < n; i++) {
	if (adj[i] == NULL) {
		thError("%s: ERROR - adjacency matrix improperly allocated - null row discovered (row = %d)", name, i);
		return(SH_GENERIC_ERROR);
	}
	if (wadj[i] == NULL) {
		thError("%s: ERROR - work space improperly allocated - null row discovered at (row = %d)", name, i);
		return(SH_GENERIC_ERROR);
	}
}
if (degree == NULL) {
	thError("%s: ERROR - improperly allocated (adj_matrix), null (degree) found", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
if (runstage != ENDRUN) {
	status = init_wadj(adj_matrix, design, runstage);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not initiate (wadj) matrix", name);
		return(status);
	}
}

/* memspace is the real memory taken by the model
digonal elements of adj shows the number of pixel overlap which is not the same as the
amount of memory especially for models or sub-models saved and not produced in runtime

memspace is being created by the Mle package

status = get_memspace_from_adj(adj, memspace, n);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not derive (memspace) from (adj_matrix)", name);
	return(status);
}
*/

if (design & CREATE_MODEL) {
	status = init_model_algorithm(algorithm);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not insert model initiation command in algorithm", name);
		return(status);
	}
}
 
status = get_degree_from_adj(wadj, degree, n, design);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not create degree array", name);
	return(status);
	}
if (memstat == NULL) {
	thError("%s: ERROR - improperly allocated (adj_matrix), null (memstat)", name);
	return(SH_GENERIC_ERROR);
}
for (i = 0; i < n; i++) {
	memstat[i] = UNLOADED;
}

if (memspace == NULL) {
	thError("%s: ERROR - improperly allocated (adj_matrix), null (memspace)", name);
	return(SH_GENERIC_ERROR);
}

if (runstage == MIDDLERUN) {
	NAIVERUN naiverun = POSSIBLE_NAIVERUN;
	status = try_naive_run_algorithm(adj_matrix, algorithm, &naiverun);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - failed at implementing the naive algorithm to the middle run", name);
		return(status);
	}
	#if MLE_APPLY_CONSTRAINTS
	status = add_pcost_to_algorithm(algorithm);
	if (status != SH_SUCCESS) {
		thError("%d: ERROR - could not add parameter cost step to algorithm", name);
		return(status);
	}
	#endif
	if (naiverun == POSSIBLE_NAIVERUN) {
		return(SH_SUCCESS);
	} else if (naiverun != IMPOSSIBLE_NAIVERUN) {
		thError("%s: ERROR - unknown (naiverun) flag", name);
		return(SH_GENERIC_ERROR);
	}
} else if (runstage == ENDRUN) {
	NAIVERUN naiverun = POSSIBLE_NAIVERUN;
	status = try_naive_del_algorithm(adj_matrix, algorithm, &naiverun);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - failed at implementing the naive algorithm to the middle run", name);
		return(status);
	}
	if (naiverun == POSSIBLE_NAIVERUN) {
		return(SH_SUCCESS);
	} else if (naiverun != IMPOSSIBLE_NAIVERUN) {
		thError("%s: ERROR - unknown (naiverun) flag returned", name);
		return(SH_GENERIC_ERROR);
	}
	return(SH_SUCCESS);
}

if (runstage == INITRUN && (design & INNER_PRODUCTS)) {
	status = add_edge_to_algorithm(algorithm, DATA_NODE, DATA_NODE);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add data-data edge to algorithm", name);
		return(status);
	}
}

if (list == NULL) {
	thError("%s: ERROR - improperly allocated (adj_matrix), null (list)", name);
	return(SH_GENERIC_ERROR);
}

int work_node = NON_EXISTENT_NODE, neighbor = NON_EXISTENT_NODE, prev_work_node = NON_EXISTENT_NODE;

/* sometimes some models have interaction only with the data, this is seen during simulations or during runs when sky is subtracted from the data and then models are fit for */
list[0] = NON_EXISTENT_NODE;
if (runstage == INITRUN && (design & (INNER_PRODUCTS | CREATE_MODEL))) {
	prev_work_node = NON_EXISTENT_NODE;
	for (work_node = 0; work_node < n; work_node++) {
		if (degree[work_node] == 0) {
		/* do we really need list here */
		status = load_node(degree, memstat, memspace, list, n, work_node, prev_work_node, algorithm); 
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not load (work_node = %d) when (degree = 0)", name, work_node);
			return(status);
		}
		status = do_edges_to_node(memstat, wadj, degree, n, work_node, algorithm, design);		
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not load edged to (work_node = %d)", name, work_node);
			return(status);
		}
		status = unload_null_nodes(memstat, degree, fitstat, n, algorithm);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not unload (work_node = %d)", name, work_node);
			return(status);
		}
		prev_work_node = work_node;
		}
	}
}


list[0] = NON_EXISTENT_NODE;
work_node = NON_EXISTENT_NODE;
neighbor = NON_EXISTENT_NODE;
prev_work_node = NON_EXISTENT_NODE;

if (runstage == MIDDLERUN && (design & (INNER_PRODUCTS | CREATE_MODEL))) {
	prev_work_node = NON_EXISTENT_NODE;
	for (work_node = 0; work_node < n; work_node++) {
		if ((degree[work_node] == 0) && 
		((design & CREATE_MODEL) || ((design & INNER_PRODUCTS) && fitstat[work_node] == FITMODEL))) {
		/* do we really need list here */
		status = load_node(degree, memstat, memspace, list, n, work_node, prev_work_node, algorithm); 
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not load (work_node = %d) when (degree = 0)", name, work_node);
			return(status);
		}
		status = do_edges_to_node(memstat, wadj, degree, n, work_node, algorithm, design);		
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not load edged to (work_node = %d)", name, work_node);
			return(status);
		}
		status = unload_null_nodes(memstat, degree, fitstat, n, algorithm);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not unload (work_node = %d)", name, work_node);
			return(status);
		}
		prev_work_node = work_node;
		}
	}
}

list[0] = NON_EXISTENT_NODE;
work_node = NON_EXISTENT_NODE;
neighbor = NON_EXISTENT_NODE;
prev_work_node = NON_EXISTENT_NODE;

status = find_the_next_node(memstat, degree, list, n, &work_node);		
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not find the next node from the current list", name);
	return(status);
}
while (work_node != NON_EXISTENT_NODE) {
	prev_work_node = NON_EXISTENT_NODE;
	status = load_node(degree, memstat, memspace, list, n, work_node, prev_work_node, algorithm);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not load the designaed working node (%d)", name, work_node);
		return(status);
	}
	status = do_edges_to_node(memstat, wadj, degree, n, work_node, algorithm, design);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not clean the edges to the designated working node (%d)", name, work_node);
		return(status);
	}
	/* get_degree_from_adj takes a long time running */ 
	status = get_degree_from_adj(wadj, degree, n, design);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update the adjacency matrix after cleaning the edges to the working node (%d)", name, work_node);
		return(status);
	}
	status = unload_null_nodes(memstat, degree, fitstat, n, algorithm);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not unload null nodes", name);
		return(status);
	}	
	int i_neighbor = 0;
	list[i_neighbor] = NON_EXISTENT_NODE;
	status = find_best_neighbor(wadj, degree, n, work_node, &neighbor);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get the best neighbor to working node (%d)", name, work_node);
		return(status);
	}
	while (neighbor != NON_EXISTENT_NODE) {
		if (i_neighbor < n && neighbor != NON_EXISTENT_NODE) {
			status = add_node_to_list(neighbor, list, i_neighbor, n);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not add neighbor (%d) to the list for the working node (%d)", 
					name, neighbor, work_node);
				return(status);
			}
		}
		#if DEEP_DEBUG_ALGORITHM
		printf("%s: i, neighbor (deg) , work (deg) , n: %d, %d (%g) , %d (%g), %d \n", 
			name, i_neighbor, neighbor, (float) degree[neighbor], work_node, (float) degree[work_node], n);
		#endif
		status = load_node(degree, memstat, memspace, list, n, neighbor, work_node, algorithm);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not load node (%d), neighbor to working node (%d)", name, neighbor, work_node);
			return(status);
		}
		status = do_edges_to_node(memstat, wadj, degree, n, neighbor, algorithm, design);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not clean the edges to node (%d), neighbor to work node (%d)", name, neighbor, work_node);
			return(status);
		}
		/* this takes too long */
		#if UPDATE_DEGREE_FROM_ADJ  
		status = get_degree_from_adj(wadj, degree, n, design);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not update the adjacency matrix after loading node (%d), neighbor to work node (%d)", name, neighbor, work_node);
			return(status);
		}
		#endif
		status = unload_null_nodes(memstat, degree, fitstat, n, algorithm);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not unload null nodes", name);
			return(status);
		}	
		status = find_best_neighbor(wadj, degree, n, work_node, &neighbor);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not find the next best neighbor to work node (%d)", name, work_node);
			return(status);
		}
		i_neighbor++;
		if (i_neighbor == n && neighbor != NON_EXISTENT_NODE) {
			thError("%s: ERROR - reached end of the search while receiving an existing node as the best neighbor (%d)", name, neighbor);
			return(SH_GENERIC_ERROR);
		}
		}
	prev_work_node = work_node;
	status = find_the_next_node(memstat, degree, list, n, &work_node);	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not designate a node as the next working node", name);
		return(status);
	}
}

int loaded = 0;
int runtime = INDEFINITE_TIMES;
for (work_node = 0; work_node < n; work_node++) {
	if (memstat[work_node]  == LOADED_IN_MEMORY) {
		status = delete_node_from_algorithm(algorithm, work_node, runtime);
		memstat[work_node] = UNLOADED;
	loaded++;
	}
}
if (loaded != 0) {
	thError("%s: WARNING - (%d out of %d) nodes remained 'LOADED', unloaded them manually", name, loaded, n);
}	

#if MLE_APPLY_CONSTRAINTS 
status = add_pcost_to_algorithm(algorithm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not add parameter cost step to algorithm", name);
	return(status);
}
#endif

return(SH_SUCCESS);
}


RET_CODE init_wadj(ADJ_MATRIX *adj_matrix, 
	ALGORITHM_DESIGN design, RUNSTAGE runstage) {
char *name = "init_wadj";

if (adj_matrix == NULL) {
	thError("%s: ERROR - null adjacency matrix", name);
	return(SH_GENERIC_ERROR);
}
if (!(design & VALID_ALGORITHM_DESIGNS)) {
	thError("%s: ERROR - algorithm design (%d) does not match any known pattern", name, (int) design);
	return(SH_GENERIC_ERROR);
}

int i, j, n;
MEMFL **adj= adj_matrix->matrix;
MEMFL **wadj = adj_matrix->wmatrix;
FITSTAT *fitstat = adj_matrix->fitstat;
n = adj_matrix->n;

FITSTAT fitstat1;
MEMFL *wadj_i, *adj_i;
if (runstage == INITRUN) {

	if (design & INNER_PRODUCTS) {
		for (i = 0; i < n; i++) {
			wadj_i = wadj[i];
			adj_i = adj[i];
			memcpy(wadj_i, adj_i, n * sizeof(MEMFL));
		}
	} else if (design & CREATE_MODEL) {
		for (i = 0; i < n; i++) {
			wadj_i = wadj[i];
			adj_i = adj[i];
			memset(wadj_i, '\0', n * sizeof(MEMFL));
			wadj_i[i] = adj_i[i];
		}
	} else {
		thError("%s: ERROR - unsupported design (%d)", 
			name, (int) design);
		return(SH_GENERIC_ERROR);
	}
	return(SH_SUCCESS);

} else if (runstage == MIDDLERUN) {

	if (design & INNER_PRODUCTS) {
		for (i = 0; i < n; i++) {
			wadj_i = wadj[i];
			memset(wadj_i, '\0', n * sizeof(MEMFL));
		}
		for (i = 0; i < n; i++) {
			fitstat1 = fitstat[i];
			wadj_i = wadj[i];
			adj_i = adj[i];
			if (fitstat1 == FITMODEL) {	
				memcpy(wadj_i, adj_i, n * sizeof(MEMFL));
				for (j = 0; j < n; j++) {
					wadj[j][i] = adj_i[j];
				}
			}
		}
		#if DEEP_DEBUG_ALGORITHM
		int asymmetry = 0;
		for (i = 0; i < n; i++) {
			for (j = 0; j < n; j++) {
				if (wadj[i][j] != wadj[j][i]) asymmetry++;
			}
		}
		printf("%s: asymmetries discovered in wadj: %d \n", name, asymmetry);
		#endif
	} else if (design & CREATE_MODEL) {
		for (i = 0; i < n; i++) {
			adj_i = adj[i];
			wadj_i = wadj[i];
			memset(wadj_i, '\0', n * sizeof(MEMFL));
			fitstat1 = fitstat[i];
			if (fitstat1 == FITMODEL || fitstat1 == NEIGHBOR_TO_FITMODEL) {
				wadj_i[i] = adj_i[i];
			}
		}
	} else {
		thError("%s: ERROR - unsupported design (%d)", name, (int) design);
		return(SH_GENERIC_ERROR);
	}
	return(SH_SUCCESS);

} else {
	thError("%s: ERROR - unsupported (runstage) flag", name);
	return(SH_GENERIC_ERROR);
}

thError("%s: ERROR - source code problem", name);
return(SH_GENERIC_ERROR);
}


RET_CODE try_naive_run_algorithm(ADJ_MATRIX *adj_matrix, ALGORITHM *algorithm, NAIVERUN *naiverun) {
char *name = "try_naive_run_algorithm";
if (adj_matrix == NULL || algorithm == NULL || naiverun == NULL) {
	thError("%s: ERROR - null input or output", name);
	return(SH_GENERIC_ERROR);
}
	
int n;
MEMFL **wadj = adj_matrix->wmatrix;
MEMSTAT *memstat = adj_matrix->memstat;
FITSTAT *fitstat = adj_matrix->fitstat;
MEMFL *memspace = adj_matrix->memspace;
int *list = adj_matrix->list;
NODEMEMORY *nmlist = adj_matrix->node_mem_list;

n = adj_matrix->n;
if (n <= 0) {
	thError("%s: graph size (n = %d) not acceptable", name, n);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
int i, pos = 0, fitpos = 0;
for (i = 0; i < n; i++) {
	if (fitstat[i] == NEIGHBOR_TO_FITMODEL) {
		nmlist[pos].node = i;
		nmlist[pos].mem = memspace[i];
		pos++;
	} else if (fitstat[i] == FITMODEL) {
		list[fitpos] = i;
		fitpos++;
	}
}

if (fitpos == 0) {
	#if VERBOSE_ALGORITHM
	thError("%s: WARNING - only (%d) nonlinear fit models were found -- no need for an algorithm", name, fitpos);
	#endif
	*naiverun = POSSIBLE_NAIVERUN;
	return(SH_SUCCESS);
} else if (fitpos < 0) {
	thError("%s: ERROR - source code problem", name);
	*naiverun = IMPOSSIBLE_NAIVERUN;
	return(SH_GENERIC_ERROR);
}


qsort(nmlist, pos, sizeof(NODEMEMORY), &compare_nm);
MEMDBLE m_x = 0, m0;
m0 = algorithm->memory_total;

for (i = 0; i < fitpos; i++) {
	m0 -= memspace[list[i]];
}

i = pos;
MEMFL x = memspace[nmlist[i - 1].node];
int neighborcut = i;
while ((m_x < (m0 - x)) && i > 0) {
	i--;
	if (i - 1 >= 0) {
		x = memspace[nmlist[i-1].node];
	} else {
		x = (MEMFL) 0;
	}
	neighborcut = i;
	m_x += memspace[nmlist[i].node];
	if ((m_x + x) > m0) break;
}

neighborcut++;
if (neighborcut >= pos) {
	*naiverun = IMPOSSIBLE_NAIVERUN;
	return(SH_SUCCESS);
}

*naiverun = POSSIBLE_NAIVERUN;

MEMDBLE memory_total, memory_free, memory_needed;
memory_total = algorithm->memory_total;
memory_free = memory_total;

int work_node = NON_EXISTENT_NODE;
int runtime = INDEFINITE_TIMES; /* calculation of fit models should be done each time */
for (i = 0; i < fitpos; i++) {
	work_node = list[i];
	memory_needed = memspace[work_node];
	if (memory_free < memory_needed) {
		thError("%s: ERROR - reached memory limit before adding fitmodel node (%d)", name, work_node);
		return(SH_GENERIC_ERROR);
	}
	status = add_node_to_algorithm(algorithm, work_node, runtime);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add fitmodel node (%d) to algorithm", name, work_node);
		return(status);
	}
	memstat[work_node] = LOADED_IN_MEMORY;
	memory_free -= memory_needed;
	status = do_edges_to_node(memstat, wadj, NULL, n, work_node, algorithm, INNER_PRODUCTS);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not take dot-action for node (%d)", name, work_node);
		return(status);
	}
}

runtime = 1;
for (i = pos - 1; i >= neighborcut; i--) {
	work_node = nmlist[i].node;
	memory_needed = memspace[work_node];
	if (memory_free < memory_needed) {
		thError("%s: ERROR - reached memory limit before adding large neighbor node (%d) memory (free = %4g MB, needed = %4g)", 
		name, work_node, 
		(float) (memory_free / MEGABYTE), (float) (memory_needed / MEGABYTE));
		return(SH_GENERIC_ERROR);
	}
	status = add_node_to_algorithm(algorithm, work_node, runtime);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add neighbor node (%d) to algorithm", name, work_node);
		return(status);
	}
	memstat[work_node] = LOADED_IN_MEMORY;
	memory_free -= memory_needed;
	status = do_edges_to_node(memstat, wadj, NULL, n, work_node, algorithm, INNER_PRODUCTS);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not take dot-action for node (%d)", name, work_node);
		return(status);
	}
}

runtime = INDEFINITE_TIMES;
for (i = neighborcut - 1; i >= 0; i--) {

	work_node = nmlist[i].node;
	memory_needed = memspace[work_node];
	if (memory_free < memory_needed) {
		thError("%s: ERROR - reached memory limit before adding small neighbor node (%d), memory (free = %4g MB, needed = %4g MB)", name, work_node, 
	(float) (memory_free / MEGABYTE), (float) (memory_needed / MEGABYTE));
		return(SH_GENERIC_ERROR);
	}
	status = add_node_to_algorithm(algorithm, work_node, runtime);
	memstat[work_node] = LOADED_IN_MEMORY;
	status = do_edges_to_node(memstat, wadj, NULL, n, work_node, algorithm, INNER_PRODUCTS);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add edges to node (%d) to the algorithm", 
			name, work_node);
		return(status);
	}
	status = delete_node_from_algorithm(algorithm, work_node, runtime);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not delete node (%d) from algorithm", 
			name, work_node);
		return(status);
	}
	memstat[work_node] = UNLOADED;
}

/* because fit models need to be recalculated each time we need to free them */
runtime = INDEFINITE_TIMES;
for (i = 0; i < fitpos; i++) {
	work_node = list[i];
	memory_needed = memspace[work_node];
	status = delete_node_from_algorithm(algorithm, work_node, runtime);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not delete fitmodel node (%d) from algorithm", name, work_node);
		return(status);
	}
	memstat[work_node] = UNLOADED;
	memory_free += memory_needed;
}

return(SH_SUCCESS);
}	


RET_CODE try_naive_del_algorithm(ADJ_MATRIX *adj_matrix, ALGORITHM *algorithm, NAIVERUN *naiverun) {
char *name = "try_naive_del_algorithm";
if (adj_matrix == NULL || algorithm == NULL || naiverun == NULL) {
	thError("%s: ERROR - null input or output", name);
	return(SH_GENERIC_ERROR);
}
	
int n;
MEMFL **wadj = adj_matrix->wmatrix;
MEMSTAT *memstat = adj_matrix->memstat;
FITSTAT *fitstat = adj_matrix->fitstat;
MEMFL *memspace = adj_matrix->memspace;
int *list = adj_matrix->list;
NODEMEMORY *nmlist = adj_matrix->node_mem_list;

n = adj_matrix->n;
if (n <= 0) {
	thError("%s: graph size (n = %d) not acceptable", name, n);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
int i, pos = 0, fitpos = 0;
for (i = 0; i < n; i++) {
	if (fitstat[i] == NEIGHBOR_TO_FITMODEL) {
		nmlist[pos].node = i;
		nmlist[pos].mem = memspace[i];
		pos++;
	} else if (fitstat[i] == FITMODEL) {
		list[fitpos] = i;
		fitpos++;
	}
}

if (fitpos == 0) {
	#if VERBOSE_ALGORITHM
	thError("%s: WARNING - only (%d) nonlinear fit models were found -- no need for an algorithm", name, fitpos);
	#endif
	*naiverun = POSSIBLE_NAIVERUN;
	return(SH_SUCCESS);
} else if (fitpos < 0) {
	thError("%s: ERROR - source code problem", name);
	*naiverun = IMPOSSIBLE_NAIVERUN;
	return(SH_GENERIC_ERROR);
}



qsort(nmlist, pos, sizeof(NODEMEMORY), &compare_nm);
MEMDBLE m_x = 0, m0;
m0 = algorithm->memory_total;

for (i = 0; i < fitpos; i++) {
	m0 -= memspace[list[i]];
}

i = pos;
MEMFL x = memspace[nmlist[i - 1].node];
int neighborcut = i;
while ((m_x < (m0 - x)) && i > 0) {
	i--;
	if (i - 1 >= 0) {
		x = memspace[nmlist[i-1].node];
	} else {
		x = (MEMFL) 0;
	}
	neighborcut = i;
	m_x += memspace[nmlist[i].node];
	if ((m_x + x) > m0) break;
}

neighborcut++;
if (neighborcut >= pos) {
	*naiverun = IMPOSSIBLE_NAIVERUN;
	return(SH_SUCCESS);
}

*naiverun = POSSIBLE_NAIVERUN;

MEMDBLE memory_total, memory_free, memory_needed;
memory_total = algorithm->memory_total;
memory_free = memory_total;

int work_node = NON_EXISTENT_NODE;

/* deleting only those nodes which are created and kept */
int runtime = 1;
for (i = pos - 1; i >= neighborcut; i--) {
	work_node = nmlist[i].node;
	status = delete_node_from_algorithm(algorithm, work_node, runtime);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not delete neighbor node (%d) to algorithm", name, work_node);
		return(status);
	}
}

return(SH_SUCCESS);
}

		
	
RET_CODE get_degree_from_adj(MEMFL **adj, MEMFL *degree, int n, ALGORITHM_DESIGN design) {
char *name = "get_degree_from_adj";
if (n <= 0) {
	thError("%s: ERROR - unacceptable size for graph (n = %d)", name, n);
	return(SH_GENERIC_ERROR);
}
if (adj == NULL) {
	thError("%s: ERROR - null adjacency matrix", name);
	return(SH_GENERIC_ERROR);
}
int i, j;
MEMFL *adj_i;
for (i = 0; i < n; i++) {
	if (adj[i] == NULL) {
		thError("%s: ERROR - improperly allocated adjacency matrix - null row discovered at (row = %d)", name, i);	
		return(SH_GENERIC_ERROR);
	}
}
if (degree == NULL) {
	thError("%s: ERROR - null degree array", name);
	return(SH_GENERIC_ERROR);
}
if (design == CREATE_MODEL) {
	for (i = 0; i < n; i++) {
	adj_i = adj[i];
	MEMFL deg = 0;
	for (j = 0; j < n; j++) {
		deg += adj_i[j];
	}
	degree[i] = deg;
	}
} else if (design & INNER_PRODUCTS) {
	for (i = 0; i < n; i++) {
	adj_i = adj[i];
	MEMFL deg = 0;
	for (j = 0; j < i; j++) {
		deg += adj_i[j];
	}
	for (j = i+1; j < n; j++) {
		deg += adj_i[j];
	}
	degree[i] = deg;
}
} else {
	thError("%s: ERROR - unsupported design", name);
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}

RET_CODE get_memspace_from_adj(MEMFL **adj, MEMFL *memspace, int n) {
char *name = "get_memspace_from_adj";
if (n <= 0) {
	thError("%s: ERROR - unaccpetable graph size (n = %d)", name, n);
	return(SH_GENERIC_ERROR);
}
if (adj == NULL) {
	thError("%s: ERROR - null adjacency matrix", name);
	return(SH_GENERIC_ERROR);
}
int i;
for (i = 0; i < n; i++) {
	if (adj[i] == NULL) {
		thError("%s: ERROR - improperly allocated adjacency matrix, empty row discovered at (row = %d)", name, i);
		return(SH_GENERIC_ERROR);
	}
}
if (memspace == NULL) {
	thError("%s: ERROR - null (memspace) matrix", name);
	return(SH_GENERIC_ERROR);
}
for (i = 0; i < n; i++) {
	memspace[i] = adj[i][i];
	if (memspace[i] < 0) {
		thError("%s: ERROR - memory space for node (%d) is negative (%g)", name, i, memspace[i]);
		return(SH_GENERIC_ERROR);
	
	}
}

return(SH_SUCCESS);
}

RET_CODE find_the_next_node(MEMSTAT *memstat, MEMFL *degree, int *list, int n, int *node) {
char *name = "find_the_next_node";
if (n <= 0) {
	thError("%s: ERROR - unacceptable graph size (n = %d)", name, n);
	return(SH_GENERIC_ERROR);
}
if (memstat == NULL) {
	thError("%s: ERROR - null (memstat) array)", name);
	return(SH_GENERIC_ERROR);
	}
if (degree == NULL) {
	thError("%s: ERROR - null (degree) array", name);
	return(SH_GENERIC_ERROR);
}
if (node == NULL) {
	thError("%s: WARNING - null output, no analysis performed", name);
	return(SH_SUCCESS);
}

int list_node = NON_EXISTENT_NODE;
if (list != NULL) {
	int i;
	for (i = 0; i < n; i++) {
		list_node = list[i];
		if (list_node == NON_EXISTENT_NODE) {
			break;
		}
		if (degree[list_node] > 0) {
			*node = list_node;
			return(SH_SUCCESS);
		}
	}
}

int min_node = NON_EXISTENT_NODE;
if (list_node == NON_EXISTENT_NODE) {
	int j;
	MEMFL this_deg, min_deg;
	for (j = 0; j < n; j++) {
		this_deg = degree[j];
		if (min_node == NON_EXISTENT_NODE && this_deg > 0) {
			min_deg = this_deg;
			min_node = j;
		} else if (this_deg < min_deg && this_deg > 0) {
			min_deg = this_deg;
			min_node = j;
		}
	}
	*node = min_node;
}

#if 0
/* the following if clause was added to accomodate models whose degree is 0 if positive degrees were not found 
 * it was found to create indefinite loops 
 */
if (min_node == NON_EXISTENT_NODE) {
	int j;
	MEMFL this_deg, min_deg;
	for (j = 0; j < n; j++) {
		this_deg = degree[j];
		if (min_node == NON_EXISTENT_NODE && this_deg >= 0) {
			min_deg = this_deg;
			min_node = j;
		} else if (this_deg < min_deg && this_deg >= 0) {
			min_deg = this_deg;
			min_node = j;
		}
	}
	*node = min_node;
}
#endif

return(SH_SUCCESS);
}

RET_CODE load_node(MEMFL *degree, MEMSTAT *memstat, MEMFL *memspace, int *list, int n, int node, int work_node, void *algorithm) {
char *name = "load_node";
if (n <= 0) {
	thError("%s: ERROR - unacceptable graph size (n = %d)", name, n);
	return(SH_GENERIC_ERROR);
}
if (node < 0 || node > n) {
	thError("%s: ERROR - unacceptable node number (node = %d)", name, node);
	return(SH_GENERIC_ERROR);
}
if (memstat == NULL) {
	thError("%s: ERROR - null (memstat) array", name);
	return(SH_GENERIC_ERROR);
}
if (memspace == NULL) {
	thError("%s: ERROR - null (memspace) array", name);
	return(SH_GENERIC_ERROR);
}
if (algorithm == NULL) {
	thError("%s: ERROR - null (algorithm)", name);
	return(SH_GENERIC_ERROR);
}
if (degree == NULL) {
	thError("%s: ERROR - null (degree) array", name);
	return(SH_GENERIC_ERROR);
}
if (memstat[node] == LOADED_IN_MEMORY) {
	return(SH_SUCCESS);
}
if (list == NULL) {
	thError("%s: ERROR - null (list)", name);
	return(SH_GENERIC_ERROR);
}

MEMDBLE memory_taken = 0;
int i, n_loaded = 0;
for (i = 0; i < n; i++) {
	if (memstat[i] == LOADED_IN_MEMORY) {
		memory_taken += (MEMDBLE) memspace[i];
		n_loaded++;
	}
}
MEMDBLE memory_needed = (MEMDBLE) memspace[node];
MEMDBLE memory_total = 0;
RET_CODE status;
status = get_memory_total(algorithm, &memory_total);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get memory total from algorithm", name);
	return(status);
}
if (memory_taken > memory_total) {
	thError("%s: ERROR - memory already taken (%g) is larger than total allowed (%g) -- (%d) objects loaded", 
		name, (float) memory_taken / (float) MEGABYTE, (float) memory_total / (float) MEGABYTE, n_loaded);
	return(SH_GENERIC_ERROR);
}
MEMDBLE memory_free = memory_total - memory_taken;

#if DEEP_DEBUG_ALGORITHM
printf("%s: memory - taken (%g), needed (%g), free (%g) \n", name, 
	(float) memory_taken / (float) MEGABYTE,
	(float) memory_needed / (float) MEGABYTE, 
	(float) memory_free / (float) MEGABYTE);
#endif

status = order_loaded_nodes_by_degree(memstat, degree, list, n);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not order loaded objects by degree", name);
	return(status);
}
i = 0;
int deleted_node = list[i];
if (deleted_node == work_node) {
	i++;
	deleted_node = list[i];
}
MEMDBLE memory_deleted = 0;
while (memory_free < memory_needed && deleted_node != NON_EXISTENT_NODE) {
	memory_deleted = (MEMDBLE) memspace[deleted_node];
	memstat[deleted_node] = UNLOADED;
	status = delete_node_from_algorithm(algorithm, deleted_node, INDEFINITE_TIMES);
	if (status != SH_SUCCESS) {	
		thError("%s: ERROR - could not delete node (%d) from (algorithm)", name, deleted_node);
		return(status);
	}
	memory_free += memory_deleted;
	#if DEEP_DEBUG_ALGORITHM
	if (i == 0) {
		printf("%s: deleting nodes: ", name);
	}
	printf("%d (%d, %g), ", deleted_node, degree[deleted_node], (float) memory_deleted / (float) MEGABYTE);
	#endif
	i++;
	if (i == n) {
		deleted_node = NON_EXISTENT_NODE;
	} else {
		deleted_node = list[i];
	}
	if (deleted_node == work_node) {
		i++;
		if (i == n) {
			deleted_node = NON_EXISTENT_NODE;
		} else {
			deleted_node = list[i];
		}
	}

}
#if DEEP_DEBUG_ALGORITHM
if (i > 0) printf("\n");
#endif

if (memory_free >= memory_needed) {
	status = add_node_to_algorithm(algorithm, node, INDEFINITE_TIMES);
	memstat[node] = LOADED_IN_MEMORY;
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not add node to (algorithm)", name);
		return(status);
	}
	return(SH_SUCCESS);
} else {
	thError("%s: ERROR - free memory (%g M) could not fit node (%d, memspace = %g M) in", 
			name, (float) memory_free / (float) MEGABYTE, node, (float) memory_needed / (float) MEGABYTE);
	return(SH_GENERIC_ERROR);
}

}	

RET_CODE unload_null_nodes(MEMSTAT *memstat, MEMFL *degree, FITSTAT *fitstat, int n, void *alg) {
char *name = "unload_null_nodes";
if (memstat == NULL || degree == NULL || fitstat == NULL || alg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
int i;
for (i = 0; i < n; i++) {
	if (memstat[i] == LOADED_IN_MEMORY && degree[i] == (MEMFL) 0 && 
		#if KEEP_FITMODEL_IN_MEMORY
		fitstat[i] == GENERAL_LINEAR_MODEL
		#else
		1
		#endif
	) {
		RET_CODE status = delete_node_from_algorithm(alg, i, INDEFINITE_TIMES);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not unload node (%d) which seems to be a null", name, i);
			return(status);
		}
	memstat[i] = UNLOADED;
	}
}

return(SH_SUCCESS);
}

	


RET_CODE do_edges_to_node(MEMSTAT *memstat, MEMFL **adj, MEMFL *degree, int n, int node, void *algorithm, ALGORITHM_DESIGN design) {
char *name = "do_edges_to_node";  
/* note that in spite of (node)'s being passed, the edges from all the loaded nodes are calculates */
if (n <= 0) {
	thError("%s: ERROR - unacceptable graph size", name, n);
	return(SH_GENERIC_ERROR);
}
if (node < 0 || node >= n) {
	thError("%s: ERROR - unacceptable node number (node = %d)", name, node);
	return(SH_GENERIC_ERROR);
}
if (memstat == NULL) {
	thError("%s: ERROR - null (memstat) array", name);
	return(SH_GENERIC_ERROR);
}
if (adj == NULL) {
	thError("%s: ERROR - null adjacency matrix", name);
	return(SH_GENERIC_ERROR);
}

int i;
for (i = 0; i < n; i++) {
	if (adj[i] == NULL) {
		thError("%s: ERROR - inappropriately allocated adjacency matrix - null row discovered at (row = %d)", name, i);
		return(SH_GENERIC_ERROR);
	}
}
if (algorithm == NULL) {
	thError("%s: ERROR - null (algorithm)", name);
	return(SH_GENERIC_ERROR);
}
if (!(design & VALID_ALGORITHM_DESIGNS)) {
	thError("%s: ERROR - unsupported algorithm design (%d)", name, (int) design);
	return(SH_GENERIC_ERROR);
}
for (i = 0; i < n; i++) {
	if (memstat[i] == LOADED_IN_MEMORY) {
		int j;
		MEMFL *adj_i = adj[i];
		if (design & (CREATE_MODEL | INNER_PRODUCTS)) {
			if (adj_i[i] > (MEMFL) 0) {
				RET_CODE status;
				if (design & INNER_PRODUCTS) {
					#if DEEP_DEBUG_ALGORITHM
					if (i < 10) {
						printf("%s: adding self-dot algorithm edge for (imodel = %d) \n", name, i);
					}
					#endif
					status = add_edge_to_algorithm(algorithm, i, i);
					if (status != SH_SUCCESS) {
						thError("%s: ERROR - could not add self-dot for node (%d) to algorithm", name, i);
						return(status);
					}
					status = add_edge_to_algorithm(algorithm, i, DATA_NODE);
					if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not add data-model (%d) edge to algorithm", name, i);
						return(status);
					}
				}
				if (design & CREATE_MODEL) {
					status = add_node_to_model_algorithm(algorithm, i);
					if (status != SH_SUCCESS) {
						thError("%s: ERROR - could not add image for node (%d) to algorithm", name, i);
						return(status);
					}
				}
				if (degree != NULL) {
					degree[i] -= adj_i[i];
					if (fabs((float) degree[i]) < 0.5) degree[i] = (MEMFL) 0;
				}
				adj_i[i] = (MEMFL) 0;
			} 
		}
		if (design & INNER_PRODUCTS) {	
			for (j = 0; j < i; j++) { /* j <= i compared to j < i has the capacity of calculating self-inner-products */
				MEMFL *adj_j = adj[j];
				if (memstat[j] == LOADED_IN_MEMORY && adj_i[j] > (MEMFL) 0) {
					adj_j = adj[j];
					RET_CODE status = add_edge_to_algorithm(algorithm, i, j);
					if (status != SH_SUCCESS) {
						thError("%s: ERROR - could not add edge calculation between nodes (%d, %d) to algorithm", 
						name, i, j);
					}
					#if UPDATE_DEGREE_FROM_ADJ
					#else
					if (degree != NULL) {
						degree[i] -= adj_i[j];
						if (fabs((float) degree[i]) < 0.5) degree[i] = (MEMFL) 0;
						degree[j] -= adj_j[i];
						if (fabs((float) degree[j]) < 0.5) degree[j] = (MEMFL) 0;
					}
					#endif
					adj_i[j] = (MEMFL) 0;
					adj_j[i] = (MEMFL) 0;
				}
			}
		}
	}
}

return(SH_SUCCESS);
}
				
		
RET_CODE find_best_neighbor(MEMFL **adj, MEMFL *degree, int n, int node, int *neighbor) {
char *name = "find_best_neighbor";
if (n <= 0) {
	thError("%s: ERROR - unacceptable graph size (n = %d)", name, n);
	return(SH_GENERIC_ERROR);
}
if (node < 0 || node >= n) {
	thError("%s: ERROR - unacceptable node (%d)", name, node);
	return(SH_GENERIC_ERROR);
}
if (degree == NULL) {
	thError("%s: ERROR - null (degree) array", name);
	return(SH_GENERIC_ERROR);
}
if (adj == NULL) {
	thError("%s: ERROR - null adjacency matrix", name);
	return(SH_GENERIC_ERROR);
}
int i;
for (i = 0; i < n; i++) {
	if (adj[i] == NULL) {
		thError("%s: ERROR - improperly allocated adjacency matrix - null row discovered at (row = %d)", name, i);
		return(SH_GENERIC_ERROR);
	}
}
if (neighbor == NULL) {
	thError("%s: WARNING - null output place holder, no calculation to be done", name);
	return(SH_SUCCESS);
}

int best_neighbor = NON_EXISTENT_NODE;
MEMFL min_degree = 0;
MEMFL *adj_i = adj[node];
for (i = 0; i < node; i++) {
	if (adj_i[i] > (MEMFL) 0) {
		if (best_neighbor == NON_EXISTENT_NODE) {
			best_neighbor = i;
			min_degree = degree[i];
		} else if (min_degree > degree[i]) {
			min_degree = degree[i];
			best_neighbor = i;
		}
	}
}

for (i = node + 1; i < n; i++) {
	if (adj_i[i] > (MEMFL) 0) {
		if (best_neighbor == NON_EXISTENT_NODE) {
			best_neighbor = i;
			min_degree = degree[i];
		} else if (min_degree > degree[i]) {
			min_degree = degree[i];
			best_neighbor = i;
		}
	}
}
*neighbor = best_neighbor;
return(SH_SUCCESS);
}
				
RET_CODE add_node_to_list(int neighbor, int *list, int i_neighbor, int n) {
char *name = "add_node_to_list";
if (n <= 0) {
	thError("%s: ERROR - unacceptable graph size (n = %d)", name, n);
	return(SH_GENERIC_ERROR);
}

if (neighbor < 0 || neighbor >= n) {
	thError("%s: ERROR - unacceptable neighbor (%d)", name, neighbor);
	return(SH_SUCCESS);
}
if (i_neighbor < 0 || i_neighbor >= n) {
	thError("%s: ERROR - unacceptable index in list (i_neighbor = %d, n = %d)", name, i_neighbor, n);
	return(SH_GENERIC_ERROR);
}
if (list == NULL) {
	thError("%s: ERROR - null list", name);
	return(SH_GENERIC_ERROR);
}
list[i_neighbor] = neighbor;
i_neighbor++;
if (i_neighbor < n) list[i_neighbor] = NON_EXISTENT_NODE;

return(SH_SUCCESS);
}

RET_CODE order_loaded_nodes_by_degree(MEMSTAT *memstat, MEMFL *degree, 
				int *list, int n) {
char *name = "order_loaded_nodes_by_degree";
if (n <= 0) {
	thError("%s: ERROR - unacceptable graph size (n = %d)", name);
	return(SH_SUCCESS);
}
if (memstat == NULL) {
	thError("%s: ERROR - null (memstat) array", name);
	return(SH_GENERIC_ERROR);
}
if (degree == NULL) {
	thError("%s: ERROR - null (degree) array", name);
	return(SH_GENERIC_ERROR);
}
if (list == NULL) {
	thError("%s: WARNING - null output placeholder (list)", name);
	return(SH_SUCCESS);
}

int i, m = 0;
for (i = 0; i < n; i++) {
	if (memstat[i] == LOADED_IN_MEMORY) {
		list[m] = i;
		m++;
	}
}
if (m < n)  list[m] = NON_EXISTENT_NODE;

comparison_degree = degree;
qsort(list, m, sizeof(int), &compare_degree);
comparison_degree = NULL;

#if DEEP_DEBUG_ALGORITHM
if (m > 0) {
	printf("%s: ordered list of loaded nodes: ", name);
	int i;
	for (i = 0; i < m; i++) {
		printf("%d, ", list[i]);
	}
	printf("\n");
} else {
	printf("%s: no node was loaded", name);
}
#endif

return(SH_SUCCESS);
}

int compare_degree(const void *x1, const void *x2) {
	/* ascending order if used with qsort */
	int i1, i2;
	i1 = * (int *) x1;
	i2 = * (int *) x2;
	return(comparison_degree[i1] - comparison_degree[i2]);
}


RET_CODE output_algorithm(ALGORITHM *alg, ADJ_MATRIX *adj_matrix) {
char *name = "output_algorithm";
if (alg == NULL) {
	thError("%s: ERROR - null algorithm", name);
	return(SH_GENERIC_ERROR);
}
if (adj_matrix == NULL) {
	thError("%s: ERROR - null (adj_matrix)", name);
	return(SH_GENERIC_ERROR);
}
MEMFL *memspace = adj_matrix->memspace;
if (memspace == NULL) {
	thError("%s: ERROR - improperly allocated (adj_matrix)", name);
	return(SH_GENERIC_ERROR);
}

MEMDBLE memory_total = alg->memory_total;
printf("%s: total memory allowed: %g (MB) \n", name, (float) memory_total / (float) MEGABYTE);
RET_CODE status;
int nstep = 0;
status = thAlgorithmGetNsteps(alg, &nstep);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the number steps in the algorithm", name);
	return(status);
}
int nunload = 0, nedge = 0, ncost = 0, nload = 0, naddmodel = 0;
ALGORITHM_STEP *step;
int i, node;
MEMDBLE memload = 0;
for (i = 0; i < nstep; i++) {
	step = thAlgorithmStepGetByPos(alg, i, &status);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get the (step) at (pos = %d)", name, i);
		return(status);
	}
	ALGORITHM_ACTION action = step->action;
	if (action == NODE_LOAD) {
		node = step->node1;
		memload += (MEMDBLE) memspace[node];	
		nload++;
	} else if (action == NODE_UNLOAD) {
		nunload++;
	} else if (action == EDGE_COMPUTE) {
		nedge++;
	} else if (action == COST_COMPUTE) {
		ncost++;
	} else if (action == NODE_ADDTOMODEL) {
		naddmodel++;
	}
	#if DEBUG_MLE_ALGORITHM
	printf("%s: [step%d]: (node1 = %d, node2 = %d), (action = '%s', runtime = %d) \n", name, i, step->node1, step->node2, shEnumNameGetFromValue("ALGORITHM_ACTION", action), step->runtime);	
	#endif
}
#if 0
printf("%s: steps: %d \n", name, nstep);
printf("%s: load steps: %d \n", name, nload);
printf("%s: unload steps: %d \n", name, nunload);
printf("%s: node interaction: %d \n", name, nedge);
printf("%s: node adds: %d \n", name, naddmodel);
printf("%s: load cost: %g (GB) \n", name, (float) ((double) memload / (double) GIGABYTE));
#else
printf("%s: steps (T/L/U/*/+/$): (%d/%d/%d/%d/%d/%d),  Cost: %3g GB \n", name, nstep, nload, nunload, nedge, naddmodel, ncost, (float) ((double) memload / (double) GIGABYTE));
#endif

return(SH_SUCCESS);
}

RET_CODE output_adj_matrix(ADJ_MATRIX *adj) {
char *name = "output_adj_matrix";
if (adj == NULL) {
	thError("%s: ERROR - null adjacencu matrix", name);
	return(SH_GENERIC_ERROR);
}
int i, nfitmodel = 0,  nneighbor = 0, nlinear = 0, nunknown = 0;
MEMDBLE mfitmodel = 0, mneighbor = 0, mlinear = 0, munknown = 0;
int n = adj->n;
FITSTAT *fitstat = adj->fitstat;
MEMFL *memspace = adj->memspace;
for (i = 0; i < n; i++) {
	FITSTAT stat = fitstat[i];
	MEMDBLE memory = (MEMDBLE) memspace[i];
	 if (stat == FITMODEL) {
		nfitmodel++;
		mfitmodel += memory;
	} else if (stat == NEIGHBOR_TO_FITMODEL) {
		nneighbor++;
		mneighbor += memory;
	} else if (stat == GENERAL_LINEAR_MODEL) {
		nlinear++;
		mlinear += memory;
	} else {
		nunknown++;
		munknown += memory;
	}
}

printf("%s: nonlinear fit objects (n = %6d, mem = %4g MB) \n", name, nfitmodel, (float) ((double) mfitmodel / (double) MEGABYTE)); 	

printf("%s: neighbor objects      (n = %6d, mem = %4g MB) \n", name, nneighbor, (float) ((double) mneighbor / (double) MEGABYTE)); 

printf("%s: linear fit objects    (n = %6d, mem = %4g MB) \n", name, nlinear, (float) ((double) mlinear / (double) MEGABYTE)); 

printf("%s: unknown objects       (n = %6d, mem = %4g MB) \n", name, nunknown, (float) ((double) munknown / (double) MEGABYTE)); 

return(SH_SUCCESS);
}


int compare_nm(const void *x1, const void *x2) {
const NODEMEMORY *nm1, *nm2;
nm1 = x1;
nm2 = x2;
return((int) (nm1->mem - nm2->mem));
}
