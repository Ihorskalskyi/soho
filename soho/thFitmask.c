#include "thFitmask.h"
#include "thMGProfile.h"
#include "thPsf.h"

static THPIX fit_mask_radial_ratio[30] = {0.25, 0.5, 0.75, 1.0, 2.0, 3.0, 4.0, 5.0, 7.5, 10.0, 15.0, 20.0, 25.0, 30.0, 35.0, 40.0, 45.0, 50.0, 60.0, 73.0, 86.0, 100.0, 130.0, 165.0, 200.0, 235.0, 260.0,300.0, 350.0, 400.0};
/* makes the fit mask for each object */


static RET_CODE calculate_objc_objmask_size(PHPROPS *phprops, THPIX *radius, int *nfitmask);
static OBJMASK *make_circle_spanmask(PHPROPS *phprops, THPIX radius_i);


static OBJMASK *make_high_SN_objmask_for_star(WOBJC_IO *wobjc, WPSF *wpsf, OBJMASK *mask_ai, THPIX factor, int aggressive);
static OBJMASK *make_high_SN_objmask_for_galaxy(WOBJC_IO *wobjc, WPSF *wpsf, OBJMASK *mask_ai, THPIX factor, int aggressive);

RET_CODE thObjcMakeFitmask(THOBJC *objc, OBJMASK *goodmask, THOBJC **objc_list, int nobjc) {
char *name = "thObjcMakeFitmask";
RET_CODE status;
if (objc == NULL || goodmask == NULL) {
	thError("%s: ERROR - null input placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (nobjc < 0) {
	thError("%s: ERROR - (nobjc = %d) is not acceptable", name, nobjc);
	return(SH_GENERIC_ERROR);
}

PHPROPS *phprops = NULL;
status = thObjcGetPhprops(objc, &phprops);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (PHOBJC) from (THBJC)", name);
	return(status);
}	
THPIX radius = -1.0;
int nfitmask = -1;
status = calculate_objc_objmask_size(phprops, &radius, &nfitmask);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get basic size of spanmask from (PHOBJC)", name);
	return(status);
}
FITMASK *fitmask = NULL;
status = thObjcGetFitmask(objc, &fitmask);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (fitmask) from (thobjc)", name);
	return(status);
}
if (fitmask == NULL) {
	thError("%s: ERROR - null (fitmask) - improperly allocated (objc)", name);
	return(SH_GENERIC_ERROR);
}
status = thFitmaskPutNmask(fitmask, nfitmask);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not put (nmask = %d) in (fitmask)", name, nfitmask);
	return(status);
}
int imask, iobjc;
for (imask = 0; imask < nfitmask; imask++) {

	int n_null_phprops = 0;
	THPIX radius_i = -1.0;
	radius_i = radius * fit_mask_radial_ratio[imask];
	OBJMASK *sp0 = make_circle_spanmask(phprops, radius_i);
	phObjmaskAndObjmask(sp0, goodmask);
	OBJMASK *sp0_good = sp0;
	if (sp0_good != NULL) {
		status = thCanonizeObjmask(sp0_good, 0);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not canonize (sp0_good) mask", name);
			return(status);
		}
	}
	for (iobjc = 0; iobjc < nobjc; iobjc++) {
		THOBJC *other_objc = objc_list[iobjc];
		if (other_objc == NULL) {
			thError("%s: WARNING - null (objc) found in (objc_list)[%d]", name, iobjc);
		}
		int comparison = 0;
		status = thObjcCompare(objc, other_objc, &comparison);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not compare (objc) with (other_objc) at (iobjc = %d)", name, iobjc);
			return(status);
		}
		if (comparison && other_objc != NULL) {
			OBJMASK *sp_objc = NULL;
			PHPROPS *other_phprops = NULL;
			status = thObjcGetPhprops(other_objc, &other_phprops);
			if (status != SH_SUCCESS) {
				thError("%s: ERROR - could not get (phprops) for (objc) at (iobjc = %d)", name, iobjc);
				return(status);
			}
			if (other_phprops != NULL) { /* phprops is null for sky object */
				status = thPhpropsGetObjmask(other_phprops, &sp_objc);
				if (status != SH_SUCCESS) {
					thError("%s: ERROR - could not get (objmask) for (objc) at (iobjc = %d)", name, iobjc);
					return(status);
				}
			} else {
				n_null_phprops++;
			}
			if (sp_objc != NULL && sp_objc->npix > 0) {
				phObjmaskAndNotObjmask(sp0_good, sp_objc);
				if (sp0_good != NULL) {
					status = thCanonizeObjmask(sp0_good, 0);
					if (status != SH_SUCCESS) {
						thError("%s: ERROR - could not canonize (sp0_good) mask", name);
						return(status);
					}
				}
			}
		}
		if (sp0_good == NULL || sp0_good->nspan == 0) break;
	}
	if (sp0_good == NULL || sp0_good->nspan == 0) {
		thError("%s: WARNING - empty (mask) after deleting other objects", name);
	}
	status = thFitmaskPutRadiusByPos(fitmask, imask, radius_i);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not place radius of mask[%d] in (fitmask)", name, imask);
		return(status);
	}
	status = thFitmaskPutSpanmaskByPos(fitmask, imask, sp0_good);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not place spanmask (%d) inside the (fitmask)", name, imask);
		return(status);
	}	
	#if DEBUG_FITMASK
	if (n_null_phprops > 0) {
		thError("%s: WARNING - found (%d) null (phprops) in the (objc) array", name);
	}
	#endif
}

#if DEBUG_MEMORY
printf("%s: memory statistics before defragmenting memory", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#endif

int ret, free_to_os = FREE_TO_OS;
ret = shMemDefragment(free_to_os);
if (ret != 0) {
	thError("%s: ERROR - problem defragmenting memory", name);
	return(SH_GENERIC_ERROR);
}

#if DEBUG_MEMORY
printf("%s: memory statistics after defragmenting memory", name);
shMemStatsPrint();
fflush(stderr);
fflush(stdout);
#endif

return(SH_SUCCESS);
}
	
/* sums the squares of each pixel in the spanmask */
RET_CODE thSpanmaskSumSq(REGION *reg, OBJMASK *sp, MLEFL *ss, int *npix) {
char *name = "thSpanmaskSumSq";
if (reg == NULL || sp == NULL || ss == NULL || npix == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (reg->type != TYPE_THPIX) {
	thError("%s: ERROR - unsupported pixel type in (reg)", name);
	return(SH_GENERIC_ERROR);
}
MLEFL ss_tmp = 0.0;
int nspan = sp->nspan;
if (nspan == 0) {
	thError("%s: WARNING - empty (spanmask)", name);
}
int nrow, ncol;
nrow = reg->nrow;
ncol = reg->ncol;
int ispan;
int count_pix = 0;
for (ispan = 0; ispan < nspan; ispan++) {
	int i, j, j1, j2;
	SPAN *s = sp->s + ispan;
	i = (int) (s->y);
	j1 = MAX((int) 0, (int) (s->x1));
	j2 = MIN((int) (ncol - 1), (int) (s->x2));
	if (i >= 0 && i < nrow) {
		THPIX *rows_i = reg->rows_thpix[i];
		for (j = j1; j <= j2; j++) {
			ss_tmp += (MLEFL) (pow(rows_i[j], 2.0));
			count_pix++;
		}
	}
}
#if DEBUG_FITMASK
printf("%s: calculated sum of squared for (objmask = %p, nspan = %d, npix = %d) of the region (name = '%s', nrow = %d, ncol = %d) \n", name, sp, sp->nspan, sp->npix, reg->name, reg->nrow, reg->ncol);
#endif

if (count_pix == 0) thError("%s: WARNING - no overlap between (spanmask) and (reg)", name);
if (count_pix != sp->npix) thError("%s: WARNING - number of overlapping pixels (%d) does not match number of spanmask pixels (%d)", name, count_pix, sp->npix);
*npix = count_pix;

*ss = ss_tmp;
return(SH_SUCCESS);
}
/* create the necessary sum of squares in the fitmask structure */
RET_CODE thFitmaskSumSq(FITMASK *mask, THPIX rPetro, REGION *r_image, REGION *z_image) {
char *name = "thFitmaskSumSq";
if (mask == NULL || r_image == NULL || z_image == NULL) {
	thError("%s: null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
int nmask = 0;
status = thFitmaskGetNmask(mask, &nmask);
int imask;
if (nmask <= 0) {
	thError("%s: ERROR - (fitnmask) not properly allocated (nmask = %d)", name, nmask);
	return(SH_GENERIC_ERROR);
}
#if DEBUG_FITMASK || 1
printf("%s: computing SSE's for a total of (%d) masks \n", name, nmask);
clock_t clk1, clk2;
clk1 = clock();
#endif
THPIX rmask_objc = VALUE_IS_BAD, rPetroCoeff_objc = (THPIX) DEFAULT_RPETRO_MASK_COEFF;
THPIX rTarget = rPetro * (THPIX) DEFAULT_RPETRO_MASK_COEFF;
int npix_objc = (int) VALUE_IS_BAD, indexPetro_objc = -1;
THPIX chisq_objc = VALUE_IS_BAD, chisq_nu_objc = VALUE_IS_BAD, delta_rms_objc = VALUE_IS_BAD;
THPIX rDiff = 0.0, radius_mask;

for (imask = 0; imask < nmask; imask++) {
	#if DEBUG_FITMASK || 1
	printf("%s: computing SSE's for (imask = %d, nmask = %d) \n", name, imask, nmask);
	#endif
	MLEFL ss_delta_rms = 0.0, ss_chisq = 0.0;
	int npix1 = 0, npix2 = 0;
	OBJMASK *sp = NULL;
	status = thFitmaskGetSpanmaskByPos(mask, imask, &sp);	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (spanmask) for (imask = %d)", name, imask);
		return(status);
	}
	status = thSpanmaskSumSq(r_image, sp, &ss_delta_rms, &npix1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not compute (Sum of Squared) for (r_image)", name);
		return(status);
	}
	status = thFitmaskPutDeltaRmsByPos(mask, imask, ss_delta_rms);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put (delta_rms) in (fitmask) for (imask = %d)", name, imask);
		return(status);
	}
	status = thSpanmaskSumSq(z_image, sp, &ss_chisq, &npix2);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (chisq) limited to mask at (imask = %d)", name, imask);
		return(status);
	}
	status = thFitmaskPutChisqByPos(mask, imask, ss_chisq);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put (chisq) limited to mask at (imask = %d) in (fitmask)", name, imask);
		return(status);
	}
	if (npix1 != npix2) {
		thError("%s: ERROR - expected the same number of pixels for both (z- and r-) steps", name);
		return(SH_GENERIC_ERROR);
	}
	status = thFitmaskPutNpixByPos(mask, imask, npix1);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put (npix) in (fitmask) for (imask = %d)", name, imask);
		return(status);
	}
	status = thFitmaskGetRadiusByPos(mask, imask, &radius_mask);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (radius) from (fitmask, imask = %d)", name, imask);
		return(SH_SUCCESS);
	}
	if (imask == 0 || fabs(radius_mask - rTarget) < fabs(rDiff)) {
		rDiff = radius_mask - rTarget;
		rmask_objc = radius_mask;
		indexPetro_objc = imask;
		npix_objc = npix1;
		chisq_objc = ss_chisq;
		delta_rms_objc = ss_delta_rms;
		chisq_nu_objc = chisq_objc / (THPIX) npix1;
	}
}

if (rPetro < 0.0) {
	thError("%s: WARNING - passed (rPetro = %g) - possibly due to null (wobj) or bad SDSS fit", name, (float) rPetro);
}

status = thFitmaskPutObjcPetro(mask, rPetro, rmask_objc, rPetroCoeff_objc, indexPetro_objc, npix_objc, chisq_objc, chisq_nu_objc, delta_rms_objc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not put chosen (Petro) variables in (fitmask)", name);
	return(status);
}

#if DEBUG_FITMASK || 1
clk2 = clock();
printf("%s: time spent on computing SSE's = %g sec \n", name, ((float) (clk2 - clk1)) / CLOCKS_PER_SEC);
#endif
return(SH_SUCCESS);
}


RET_CODE thObjcUpdateFitMask(THOBJC *objc, REGION *r_image, REGION *z_image) {
char *name = "thObjcUpdateFitMask";
if (objc == NULL || r_image == NULL || z_image == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
FITMASK *fitmask = NULL;
#if DEBUG_FITMASK
printf("%s: getting (fitmask) from (objc = %p) \n", name, objc);
#endif
status = thObjcGetFitmask(objc, &fitmask);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (fitmask) from (objc)", name);
	return(status);
}
PHPROPS *phprops = NULL;
status = thObjcGetPhprops(objc, &phprops);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (phprops) from (thobjc)", name);
	return(status);
}
THPIX rPetro = (THPIX) VALUE_IS_BAD;
if (phprops != NULL) {
	WOBJC_IO *wobjc = NULL;
	status = thPhpropsGet(phprops, &wobjc, NULL, NULL, NULL, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (wobjc) from (phprops)", name);
		return(status);
	}
	if (wobjc != NULL) {
		rPetro = (THPIX) wobjc->petroRad[R_BAND];    
	} else {
		thError("%s: WARNING - cannot find (rPetro) because (wobjc = NULL)", name);
	}
} else {
	thError("%s: WARNING - cannot find (rPetro) because (phprops = NULL)", name);
}

#if DEBUG_FITMASK
printf("%s: calculating (SSE)'s for (objc = %p) \n", name, objc);
#endif
status = thFitmaskSumSq(fitmask, rPetro, r_image, z_image);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not calculate (SSE's) for (fitmask)", name);
	return(status);
}
return(SH_SUCCESS);
}


RET_CODE calculate_objc_objmask_size(PHPROPS *phprops, THPIX *radius, int *nfitmask) {
char *name = "calculate_objc_objmask_size";
if (phprops == NULL || radius == NULL || nfitmask == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
WOBJC_IO *phObjc = NULL;
int *bndex;
status = thPhpropsGet(phprops, &phObjc, &bndex, NULL, NULL, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (wobjc_io) from (phprops)", name);
	return(status);
}
THPIX r = 0.0;
OBJ_TYPE type = phObjc->objc_type;
if (type == OBJ_GALAXY) {
	r = MIN((THPIX) (phObjc->r_deV[R_BAND]), (THPIX) (phObjc->r_exp[R_BAND]));
	r = (THPIX) phObjc->petroRad[R_BAND];
} else if (type == OBJ_STAR) {
	r = (THPIX) DEFAULT_STAR_FITRADIUS;
} else {
	thError("%s: ERROR - unsupported (obj_type = '%s')", name, shEnumNameGetFromValue("OBJ_TYPE", type));
	*radius = THNAN;
	return(SH_GENERIC_ERROR);
}

*radius = r;
*nfitmask = (int) DEFAULT_NFITMASK;
return(SH_SUCCESS);
}

OBJMASK *make_circle_spanmask(PHPROPS *phprops, THPIX radius) {
char *name = "make_circle_spanmask";
if (phprops == NULL) {
	thError("%s: ERROR - null input (phprops)", name);
	return(NULL);
}
if (isnan(radius) || isinf(radius) || radius <= (THPIX) 0.0) {
	thError("%s: ERROR - unacceptable value detected in radius (= %g)", name, (float) radius);
	return(NULL);
}

RET_CODE status;
WOBJC_IO *phObjc = NULL;
int *bndex = NULL;
status = thPhpropsGet(phprops, &phObjc, &bndex, NULL, NULL, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (wobjc_io) from (phprops)", name);
	return(NULL);
}

int xc, yc, r;
xc = (int) phObjc->objc_colc;
yc = (int) phObjc->objc_rowc;
r = MAX((int) radius, 1);
OBJMASK *mask = phObjmaskFromCircle(yc, xc, r);
if (mask == NULL) {
	thError("%s: ERROR - null (objmask) returned from (PHOTO) function", name);
	return(NULL);
}
return(mask);
}

RET_CODE thFitmaskTrans(FITMASK *mask, void *p, char *pname) {
char *name = "thFitmaskTrans";
if (mask == NULL || p == NULL || pname == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (strlen(pname) == 0) {
	thError("%s: ERROR - empty (pname) string", name);
	return(SH_GENERIC_ERROR);
}
char *fname = "FITMASK";
TYPE ptype = shTypeGetFromName(pname);
if (ptype == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - (pname = '%s') is not supported by a schema", name, pname);
	return(SH_GENERIC_ERROR);
}
TYPE ftype = shTypeGetFromName(fname);
if (ftype == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - ('%s') is not supported by a schema", name, fname);
	return(SH_GENERIC_ERROR);
}
char **rnames = NULL, *rname  = NULL;
int *is_arr = NULL;
rnames = shCalloc(6, sizeof(char *));

int nrecord = 0, i = 0;
rnames[i] = "nmask"; i++;
rnames[i] = "npar"; i++;
rnames[i] = "npix_mask"; i++;
rnames[i] = "radius_mask"; i++;
rnames[i] = "delta_rms_mask"; i++;
rnames[i] = "chisq_mask"; i++;

nrecord = i;
int type_err = 0, ferr = 0, perr = 0, fverr = 0, pverr = 0, arr_err = 0;
for (i = 0; i < nrecord; i++) {
	rname = rnames[i];
	SCHEMA_ELEM *fe = shSchemaElemGetFromType(ftype, rname);	
	SCHEMA_ELEM *pe = shSchemaElemGetFromType(ptype, rname);
	if (fe != NULL && pe != NULL) {
		if (strcmp(fe->type, pe->type)) {
			type_err++;
		} else {
			void *fv = NULL, *pv = NULL;
			fv = shElemGet(mask, fe, NULL);	
			pv = shElemGet(p, pe, NULL);
			size_t tsize = (size_t) fe->size;
			int rsize = MIN((int) (fe->i_nelem), (int) (pe->i_nelem));
			if (fe->i_nelem != pe->i_nelem) arr_err++;
			if (fv != NULL && pv != NULL) memcpy(pv, fv, rsize * tsize);
			if (fv == NULL) fverr++;
			if (pv == NULL) pverr++;
		}
	} 
	if (fe == NULL) ferr++;
	if (pe == NULL) perr++;
}
if (type_err > 0) {
	thError("%s: WARNING - found (%d) instances of type mistamtch between 'FITMASK' and '%s'", name, type_err, pname);
}
if (arr_err > 0) {
	thError("%s: WARNING - found (%d) instances of array size mistatch between 'FITMASK' and '%s'", name, arr_err, pname);
}
if (ferr > 0) {
	thError("%s: WARNING - found (%d) non existent records in 'FTIMASK'", name, ferr);
}
if (perr > 0) {
	thError("%s: WARNING - found (%d) non-existent records in '%s'", name, perr, pname);
}
if (fverr > 0) {
	thError("%s: WARNING - found (%d) null record addresses in 'FITMASK'", name, fverr);
}
if (pverr > 0) {
	thError("%s: WARNING - found (%d) null record addresses in '%s'", name, pverr, pname);
}

shFree(rnames);
shFree(is_arr);
return(SH_SUCCESS);
}

OBJMASK *thObjcMakeObjmaskHighSN(THOBJC *objc, WPSF *wpsf, THPIX sfactor, int aggressive, RET_CODE *status) {
char *name = "thObjcMakeObjmaskHighSN";
if (objc == NULL) {
	thError("%s: ERROR - null input (objc)", name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
}
if (sfactor <= 0.0) {
	thError("%s: ERROR - (size factor = %g) not supported", name, (float) sfactor);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
}
RET_CODE status0;

THOBJCTYPE class_type = UNKNOWN_OBJC, SKY_OBJC = UNKNOWN_OBJC; 
MSkyGetType(&SKY_OBJC);

status0 = thObjcGetType(objc, &class_type);
if (status0 != SH_SUCCESS) {
	thError("%s: ERROR - could not get (class_type) from (thobjc)", name);
	if (status != NULL) *status = status0;
	return(NULL);
}

if (class_type == SKY_OBJC) {
	if (status != NULL) *status = SH_SUCCESS;
	return(NULL);
}

PHPROPS *phprops = NULL;
status0 = thObjcGetPhprops(objc, &phprops);
if (status0 != SH_SUCCESS) {
	thError("%s: ERROR - could not get (phprops) from (thobjc)", name);
	if (status != NULL) *status = status0;
	return(NULL);
}
if (phprops == NULL) {
	thError("%s: ERROR - null (phprops) found in (thobjc)", name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
}
WOBJC_IO *wobjc = NULL;
status0 = thPhpropsGet(phprops, &wobjc, NULL, NULL, NULL, NULL);
if (status0 != SH_SUCCESS) {
	thError("%s: ERROR - could not get (wobjc) from (phprops)", name);
	if (status != NULL) *status = status0;
	return(NULL);
}
if (wobjc == NULL) {
	thError("%s: ERROR - null (wobjc_io) found in (phprops)", name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
}

int sdss_npix = 0;
OBJMASK *mask_ai = NULL;
status0 = thPhpropsGetObjmask(phprops, &mask_ai);
if (status0 != SH_SUCCESS) {
	thError("%s: ERROR - could not get (mask_ai) from (phprops)", name);
	if (status != NULL) *status = status0;
	return(NULL);
}
if (mask_ai != NULL) {
	sdss_npix = mask_ai->npix;
}

OBJ_TYPE type = wobjc->objc_type;
OBJMASK *objmask = NULL;

if (type == OBJ_STAR) {
	#if MASK_BAD_STAR_SDSS_PREFERRED
	if (sdss_npix > 0) objmask = phObjmaskCopy(mask_ai, 0.0, 0.0);
	#endif
	if (objmask == NULL) objmask = make_high_SN_objmask_for_star(wobjc, wpsf, mask_ai, sfactor, aggressive);
} else if (type == OBJ_GALAXY) {
	#if MASK_BAD_GALAXY_SDSS_PREFERRED
	if (sdss_npix > 0) objmask = phObjmaskCopy(mask_ai, 0.0, 0.0);
	#endif
	if (objmask == NULL) objmask = make_high_SN_objmask_for_galaxy(wobjc, wpsf, mask_ai, sfactor, aggressive);
} else {
	thError("%s: ERROR - unsupported (objc_type = '%s')", name, shEnumNameGetFromValue("OBJ_TYPE", type));
	return(NULL);
}

return(objmask);
}

OBJMASK *make_high_SN_objmask_for_star(WOBJC_IO *wobjc, WPSF *wpsf, OBJMASK *mask_ai, THPIX factor, int aggressive) {
char *name = "make_high_SN_objmask_for_star";
shAssert(wobjc != NULL);
shAssert(factor > (THPIX) 0.0);
shAssert(aggressive == MASK_BAD_FLAG || aggressive == MASK_BAD_AGGRESSIVE_FLAG || aggressive == MASK_BAD_MILD_FLAG);
int r_mask = 0;
int sdss_npix = 0;
if (mask_ai != NULL) sdss_npix = mask_ai->npix;
if (sdss_npix > 0) {
	sdss_npix = mask_ai->npix;
	int r_dim = mask_ai->rmax - mask_ai->rmin + 1;
	int c_dim = mask_ai->cmax - mask_ai->cmin + 1;
	int rc_dim = MAX(r_dim, c_dim);
	/* 
	int oblique_area = 2 * rc_dim * rc_dim;
	printf("%s: r_dim = %d, c_dim = %d, rc_dim = %d, r x c = %d, rc_area = %d, sdss_npix = %d \n", name, r_dim, c_dim, rc_dim, r_dim * c_dim, oblique_area, sdss_npix); 
	sdss_npix = MAX(sdss_npix, oblique_area);
	*/

	THPIX sdss_npix_correction_factor = 0.0;
	if (aggressive & MASK_BAD_FLAG) {
		sdss_npix_correction_factor = MASK_BAD_STAR_SDSS_NPIX_RATIO;
	} else if (aggressive & MASK_BAD_AGGRESSIVE_FLAG) {
		sdss_npix_correction_factor = MASK_BAD_STAR_AGGRESSIVE_SDSS_NPIX_RATIO;
	} else if (aggressive &  MASK_BAD_MILD_FLAG) {
		sdss_npix_correction_factor = MASK_BAD_STAR_MILD_SDSS_NPIX_RATIO; 
	} else {
		thError("%s: ERROR - binary error (aggressive = %x) not supported", name, aggressive);
		return(NULL);
	}

	THPIX npix_max = (THPIX) sdss_npix * (THPIX) sdss_npix_correction_factor;
	r_mask = (int) (pow(npix_max / PI, 0.5) + 0.5);
} else {

	int r_mask_min = 0;
	if (aggressive & MASK_BAD_FLAG) {
		factor *= (THPIX) MASK_BAD_STAR_FACTOR;
		r_mask_min = (int) (MASK_BAD_STAR_RMIN);
	} else if (aggressive & MASK_BAD_AGGRESSIVE_FLAG) {
		factor *= (THPIX) MASK_BAD_STAR_AGGRESSIVE_FACTOR;
		r_mask_min = (int) (MASK_BAD_STAR_AGGRESSIVE_RMIN);
	} else if (aggressive &  MASK_BAD_MILD_FLAG) {
		factor *= (THPIX) MASK_BAD_STAR_MILD_FACTOR;
		r_mask_min = (int) (MASK_BAD_STAR_MILD_RMIN);
	} else {
		thError("%s: ERROR - binary error (aggressive = %x) not supported", name, aggressive);
		return(NULL);
	}

	THPIX cutoff_psf = 0.0;
	int margin_min = 0, margin_max = 0, margin_default = 0;

	if (aggressive & MASK_BAD_FLAG) {
		cutoff_psf = (THPIX) MASK_BAD_STAR_CUTOFF_PSF;
		margin_min = (int) MASK_BAD_STAR_MARGIN_MIN;
		margin_max = (int) MASK_BAD_STAR_MARGIN_MAX;
		margin_default = (int) MASK_BAD_STAR_MATGIN_DEFAULT;
	} else if (aggressive & MASK_BAD_AGGRESSIVE_FLAG) {
		cutoff_psf = (THPIX) MASK_BAD_STAR_AGGRESSIVE_CUTOFF_PSF;
		margin_min = (int) MASK_BAD_STAR_AGGRESSIVE_MARGIN_MIN;
		margin_max = (int) MASK_BAD_STAR_AGGRESSIVE_MARGIN_MAX;
		margin_default = (int) MASK_BAD_STAR_AGGRESSIVE_MATGIN_DEFAULT;
	} else if (aggressive &  MASK_BAD_MILD_FLAG) {
		cutoff_psf = (THPIX) MASK_BAD_STAR_MILD_CUTOFF_PSF;
		margin_min = (int) MASK_BAD_STAR_MILD_MARGIN_MIN;
		margin_max = (int) MASK_BAD_STAR_MILD_MARGIN_MAX;
		margin_default = (int) MASK_BAD_STAR_MILD_MATGIN_DEFAULT;
	} else {
		thError("%s: ERROR - binary error (aggressive = %x) not supported", name, aggressive);
		return(NULL);
	}


		
	RET_CODE status;

	int margin = -1;
	status = thWpsfGetMarginFromWObjcIo(wobjc, NULL, wpsf, cutoff_psf, margin_min, margin_max, margin_default, &margin);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (margin) for (wobjc)", name);
		return(NULL);
	}

	r_mask = (int) (((float) margin) * factor + 0.5);
	r_mask = MAX(r_mask, r_mask_min);
}

int rowc, colc;
rowc = (int) wobjc->objc_rowc;
colc = (int) wobjc->objc_colc;
if (r_mask < 1.0) return(NULL);
OBJMASK *mask = phObjmaskFromCircle(rowc, colc, r_mask);
if (mask == NULL) {
	thError("%s: WARNING - null mask generated while (r_mask = %g)", name, (float) r_mask);
}

return(mask);
}
OBJMASK *make_high_SN_objmask_for_galaxy(WOBJC_IO *wobjc, WPSF *wpsf, OBJMASK *mask_ai, THPIX factor, int aggressive) {
char *name = "make_high_SN_objmask_for_galaxy";
shAssert(wobjc != NULL);
shAssert(factor > (THPIX) 0.0);
shAssert(aggressive == MASK_BAD_FLAG || aggressive == MASK_BAD_AGGRESSIVE_FLAG || aggressive == MASK_BAD_MILD_FLAG);
int sdss_npix = 0;
if (mask_ai != NULL) sdss_npix = mask_ai->npix;

THPIX sdss_npix_correction_factor = 0.0;
if (aggressive & MASK_BAD_FLAG) {
	sdss_npix_correction_factor = MASK_BAD_GALAXY_SDSS_NPIX_RATIO;
} else if (aggressive & MASK_BAD_AGGRESSIVE_FLAG) {
	sdss_npix_correction_factor = MASK_BAD_GALAXY_AGGRESSIVE_SDSS_NPIX_RATIO;
} else if (aggressive & MASK_BAD_MILD_FLAG) {
	sdss_npix_correction_factor = MASK_BAD_GALAXY_MILD_SDSS_NPIX_RATIO;
} else {
	thError("%s: ERROR - binary error (aggressive = %x) not supported", name, aggressive);
	return(NULL);
}

THPIX npix_max = (THPIX) sdss_npix * (THPIX) sdss_npix_correction_factor;
int rmask_sdss = (int) (pow(npix_max / PI, 0.5) + 0.5);
/* 
int rmask_max = rmask_sdss;
*/
int r_mask_min = 0;
if (aggressive & MASK_BAD_FLAG) {
	factor *= (THPIX) MASK_BAD_GALAXY_FACTOR;
	r_mask_min = (int) (MASK_BAD_GALAXY_RMIN);
} else if (aggressive & MASK_BAD_AGGRESSIVE_FLAG) {
	factor *= (THPIX) MASK_BAD_GALAXY_AGGRESSIVE_FACTOR;
	r_mask_min = (int) (MASK_BAD_GALAXY_AGGRESSIVE_RMIN);
} else if (aggressive & MASK_BAD_MILD_FLAG) {
	factor *= (THPIX) MASK_BAD_GALAXY_MILD_FACTOR;
	r_mask_min = (int) (MASK_BAD_GALAXY_MILD_RMIN);
} else {
	thError("%s: ERROR - binary error (aggressive = %x) not supported", name, aggressive);
	return(NULL);
}


int rowc, colc;
rowc = (int) wobjc->objc_rowc;
colc = (int) wobjc->objc_colc;
int id = wobjc->id;
int objc_flag = wobjc->objc_flags;
float r_petro = wobjc->petroRad[R_BAND];
float frac_deV = wobjc->fracPSF[R_BAND];
float r_deV = wobjc->r_deV[R_BAND];
float r_deV_80 = 3.17 * r_deV;
float r_exp = wobjc->r_exp[R_BAND];
float r_exp_80 = 1.78 * r_exp;
float r_80 = frac_deV * r_deV_80 + (1.0 - frac_deV) * r_exp_80;

float count_petro = wobjc->petroCounts[R_BAND];
float mag_petro = (float) VALUE_IS_BAD;
float sky = wobjc->sky[R_BAND];
float light_out = 2.0 * (float) PI * (float) r_mask_min * sqrt(sky / (float) MASK_BAD_GAIN_ESTIMATE) / factor;
float frac_out = light_out / fabs(count_petro);
int r_mask = -1;
if (frac_out >= 0.95) {
	r_mask = MAX(r_mask_min, rmask_sdss);
} else {
	THPIX g1, g2;
	int r_mask_deV = 0, r_mask_exp = 0, r_mask_temp, r_mask_temp2;
	for (r_mask_temp = r_mask_min; r_mask_temp < (r_mask_min + 1000); r_mask_temp++) {
		light_out = 2.0 * (float) PI * (float) r_mask_temp * sqrt(sky / (float) MASK_BAD_GAIN_ESTIMATE) / factor;
		frac_out = light_out / fabs(count_petro);
		if (frac_out > 0.95) {
			r_mask_deV = r_mask_temp;
			break;
		} else {
			g1 = g_deV(NULL, (THPIX) frac_out);
			r_mask_temp2 = (int) (g1 * r_deV + 0.5);
			if (r_mask_temp2 <= r_mask_temp) {
				r_mask_deV = r_mask_temp;
				break;
			}
		}
	}
	for (r_mask_temp = r_mask_min; r_mask_temp < (r_mask_min + 1000); r_mask_temp++) {
		light_out = 2.0 * (float) PI * (float) r_mask_temp * sqrt(sky / (float) MASK_BAD_GAIN_ESTIMATE) / factor;
		frac_out = light_out / fabs(count_petro);
		if (frac_out > 0.95) {
			r_mask_exp = r_mask_temp;
			break;
		} else {
			g2 = g_exp(NULL, (THPIX) frac_out);
			r_mask_temp2 = (int) (g2 * r_exp + 0.5);
			if (r_mask_temp2 <= r_mask_temp) {
				r_mask_exp = r_mask_temp;
				break;
			}
		}
	}		
	r_mask = MAX(r_mask_deV, r_mask_exp);
	r_mask = MAX(r_mask, r_mask_min);
	r_mask = MAX(r_mask, rmask_sdss);
	/* 
	#if 0
	int r_mask = (int) (r_petro * factor + 0.5);
	#else
	int r_mask = (int) (r_80 * factor + 0.5);
	#endif
	*/
}
if (r_mask < 1) return(NULL);
OBJMASK *mask = phObjmaskFromCircle(rowc, colc, r_mask);
if (mask == NULL) {
	thError("%s: WARNING - null mask generated while (r_petro = %g)", name, (float) r_petro);
}

printf("%s: created a mask (id = %x, aggressive = %x, r_min = %g, factor = %g, flag = %08x), centered at (rowc = %g, colc = %g), mask radius (= %g), r (deV = %g, exp = %g 80percent = %g), petroR (= %g), petroMag (= %g), petroCount (= %g), sky (= %g), light_out (= %g), frac_out (= %g) \n", name, id, aggressive, (float) r_mask_min, (float) factor, objc_flag, (float) rowc, (float) colc, (float) r_mask, (float) r_deV, (float) r_exp, (float) r_80, (float) r_petro, (float) mag_petro, (float) count_petro, (float) sky, (float) light_out, (float) frac_out);

 
return(mask);
}

