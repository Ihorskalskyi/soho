#ifndef THCLASS_H
#define THCLASS_H

#include "thBasic.h"
#include "thDefTypes.h"
#include "thObjcTypes.h"

/* MASKDEF handling */
RET_CODE thAddMaskDefTree(CHAIN *maskdefs, CHAIN *sens, CHAIN *tree);
RET_CODE thMaskDefInsertInTree(MASKDEF *def, CHAIN *tree, int addflag);
RET_CODE thSenInsertInMaskDef(MASKDEF *def, CHAIN *sens);

/* OBJCDEF handling */
RET_CODE thAddObjcDefTree(CHAIN *objcdefs, CHAIN *sens, CHAIN *tree);
CHAIN *thObjcClassGetFromTree(OBJCDEF *def, CHAIN *tree, int addflag);
int thObjcClassGetPosFromTree(OBJCDEF *def, CHAIN *tree, int addflag);
int thObjcDefGetPosInClass(OBJCDEF *def, CHAIN *class);

/* SENTENCE handling */
RET_CODE thSenInsertInObjcDef(OBJCDEF *def, CHAIN *sens);
int thChainSenGetPosById(CHAIN *sens, char *id);

#endif
