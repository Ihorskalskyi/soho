if (nap != lwork->nap && lwork->wjtjap != NULL) {
	if (lwork->nap != 0 && lwork->wjtjap != NULL && lwork->wjtjap[0] != NULL) thFree(lwork->wjtjap[0]);
	if (lwork->nap != 0) thFree(lwork->wjtjap);
	lwork->wjtjap = NULL;
}

if (lwork->wjtjap == NULL && nap != 0) {
	lwork->wjtjap = thCalloc(nap, sizeof(MLEFL *));
	lwork->wjtjap[0] = thCalloc(nap * nap, sizeof(MLEFL));
}
for (i = 0; i < nap; i++) if (lwork->wjtjap[i] == NULL) lwork->wjtjap[i] = lwork->wjtjap[0] + i * nap;

if (np != lwork->np && lwork->wjtj != NULL) {
	if (lwork->np != 0) thFree(lwork->wjtj);
	lwork->wjtj = NULL;
}

if (lwork->wjtj == NULL && np != 0) lwork->wjtj = thCalloc(np, sizeof(MLEFL *));
for (i = 0; i < np; i++) lwork->wjtj[i] = lwork->wjtjap[i + nm] + nm;





if (lwork->jtjap != NULL && lwork->nap <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (jtjap) allocated while (nap = %d)", name, lwork->nap);
	return(SH_GENERIC_ERROR);
}

if (lwork->jtj != NULL && lwork->np <= 0) {
	thError("%s: ERROR - wrongly allocated (lwork): (jtj) allocated while (nj = %d)", name, lwork->np);
	return(SH_GENERIC_ERROR);
}

if (lwork->jtjap != NULL) {
	 if (lwork->jtjap[0] != NULL) thFree(lwork->jtjap[0]);
	thFree(lwork->jtjap);
	lwork->jtjap = NULL;
}

if (lwork->jtj != NULL) {
	thFree(lwork->jtj);
	lwork->jtj = NULL;
}


