#include "thPsf.h"
#include "phMathUtils.h"
#include "thStat.h"
static int stat_inbound = BORDER_PSF_ONE, stat_outbound = BORDER_PSF_TWO;
/* ====================================== */
/* the following in copied from extract.c */

static const int anndex[NANN + 1] =
  {0, 1, 2, 3, 5, 8, 12, 19, 29, 46, 71, 111, 173, 270, 421, 653};
static const int myanndex[MYNANN + 1] =  
 {0, 1, 2, 3, 5, 8, 12, 15, 18, 21, 29, 46, 71, 111, 173, 270, 421, 653};
static const int myanndex2[MYNANN2 + 1] = 
 {0, 1, 2, 3, 5, 8, 12, 15, 18, 21, 29, 46, 71, 111, 173, 270, 421, 524, 653};
static const int myanndex3[MYNANN3 + 1] = 
 {0, 1, 2, 3, 5, 8, 12, 15, 18, 21, 29, 46, 55, 66, 78, 93, 111, 133, 159, 189, 226, 270, 322, 384, 458, 547, 653};
static const int myanndex4[MYNANN4 + 1] = 
 {0, 1, 2, 3, 5, 8, 12, 15, 18, 21, 29, 46, 71, 89, 111, 139, 173, 216, 270, 337, 421, 524, 653, 810, 1000, 1241, 1532};

static int psf_warning_count = 0;

#if 0 
static const int *wanndex = anndex;
static const int WNANN = NANN;
#else
static int wanndex_version = ANNULUS_UPDATED_PSF_VER;
static int *wanndex = myanndex;
static int WNANN = MYNANN;
#endif
/* 0  1  2  3  4  5   6   7   8   9  10   11   12   13   14   15 */

/* NB!!! these are beginning radial INDICES, not radii, for the annuli;
 * The radial indices and their relation to radii are discussed in the
 * section on `RING STUFF' below. The rounded radii for all but the
 * smallest annuli are between anndex[i] anndex[i+1]-1 inclusive
 * which means that scaled or float radii lie between anndex[i]-0.5 and
 * anndex[i+1]-0.5. OK? The small annuli (ann<=5) lie in the sync-shifted
 * region and have unique assignments to pixels. See `RING STUFF' below.
 * Note that anndex[NSYNCANN]-0.5 is the radius of the OUTER
 * BOUNDARY of the inner sync-shifted region, and FRLIM = andex[NANN] - 0.5
 * that of the outer boundary of the largest allowable object
 * (657.5 pixels=4.4 arcmin radius, 8.8 arcmin diameter). Objects
 * larger than this must use a rebinned frame to define their
 * outer parts.
 * NB!!!!!  This limit is set by the largest cell containing 65K pixels,
 * so if one wants to extend THESE algorithms very much, one must go to
 * int histograms.
 */
/* ====================================== */

/* static functions */

static const SCHEMA *schema_conv_type = NULL;
static const SCHEMA *schema_conv_method = NULL;
static const SCHEMA *schema_psf_source = NULL;

int find_best_fftw_size(int a);
static void init_static_vars();

static void tell_me_about_fftw_arr(char *comment, FFTW_COMPLEX *arr, int n);
static void tell_me_about_reg(char *comment, REGION *reg);

static RET_CODE extract_counts_from_param(void *q, char *qname, THPIX *counts);
static RET_CODE  get_margin_from_fraction(THPIX fraction, THPIX *IntegLP, THPIX *rIntegLP, int nr, int margin_min, int margin_max, int  margin_default, int *margin);

void init_psf_warning_count() {
psf_warning_count = 0;
return;
}

void init_static_vars(void) {
	TYPE t;
	if (schema_conv_type == NULL) {
		t = shTypeGetFromName("PSF_CONVOLUTION_TYPE");
		shAssert(t != UNKNOWN_SCHEMA);
		schema_conv_type = shSchemaGetFromType(t);
		shAssert(schema_conv_type != NULL);
	}	
	if (schema_conv_method == NULL) {
		t = shTypeGetFromName("CONV_METHOD");
		shAssert(t != UNKNOWN_SCHEMA);
		schema_conv_method = shSchemaGetFromType(t);
		shAssert(schema_conv_type != NULL);
	}
	if (schema_psf_source == NULL) {
		t = shTypeGetFromName("PSF_SOURCE");
		shAssert(t != UNKNOWN_SCHEMA);
		schema_psf_source = shSchemaGetFromType(t);
		shAssert(schema_psf_source != NULL);
	}
return;
}

RET_CODE thPsfGetAnndexVer(int *version) {
shAssert(version != NULL);
*version = wanndex_version;
return(SH_SUCCESS);
}

RET_CODE thPsfSetAnndexVer(int version) {
char *name = "thPsfSetAnndex";

if (version == ANNULUS_PHOTO_VER) {
	wanndex = anndex;
	WNANN = NANN;
} else if (version == ANNULUS_UPDATED_PSF_VER) {
	wanndex = myanndex;
	WNANN = MYNANN;
} else if (version == ANNULUS_UPDATED_GALAXY_VER) {
	wanndex = myanndex2;
	WNANN = MYNANN2;
} else {
	thError("%s: ERROR - unsupported annulus version (= %d)", name, version);
	return(SH_GENERIC_ERROR);
}

wanndex_version = version;
return(SH_SUCCESS);
}

RET_CODE thPsfInit(int inbound1, int outbound1) {
char *name = "thPsfInit";
static int init_glue = 0;
if (inbound1 < 0) {
	thError("%s: ERROR - inbound cannot be less than (0) received (%d)", name, inbound1);
	return(SH_GENERIC_ERROR);
}
if (outbound1 <= inbound1) {
	thError("%s: ERROR - outbound (%d) should be greater than inbound (%d)", name, outbound1, inbound1);
	return(SH_GENERIC_ERROR);
}
init_glue++;
#if VERBOSE_PSF
printf("%s: PSF glue parameters initiated (%d) times \n", name, init_glue);
#endif
stat_inbound = inbound1;
stat_outbound = outbound1;
return(SH_SUCCESS);
}

RET_CODE thPsfGetGluePars(int *inbound1, int *outbound1) {
char *name = "thPsfGetGluePars";
if (inbound1 != NULL) *inbound1 = stat_inbound;
if (outbound1 != NULL) *outbound1 = stat_outbound;
if (inbound1 == NULL && outbound1 == NULL) {
	thError("%s: WARNING - null output placeholder", name);
}
return(SH_SUCCESS);
}


/* ====================================== */

PSFMODEL *thConvertPhPsfToThPsf (PHPSFMODEL *phpsf, RET_CODE *status) {

  char *name = "thConvertPhPsfToThPsf";
  
  if (phpsf == NULL) {
    thError("%s: ERROR - null (phpsf)", name);
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
  }

  if (phpsf->basis == NULL) {
    thError("%s: WARNING -- PHPSF_BASIS null", name);
  }

  if (phpsf->wing == NULL) {
    thError("%s: Warning -- PHPSF_WING null", name);
  }

  if (phpsf->filter < 0 || phpsf->filter >= NCOLOR) {
    thError("%s: ERROR - filter (%d) does not lie within the range [%d, %d]",
		   name, phpsf->filter, 0, NCOLOR - 1);
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
  }
  
  RET_CODE status2;
  PSFMODEL *psf;
  psf = thPsfmodelNew();

  psf->basis = phpsf->basis;
  psf->wing =  thPsfWingGetFromAnnulus(phpsf->wing, phpsf->filter, &status2);
  if (status2 != SH_SUCCESS) {
    thError("%s: ERROR - could not generate psf wings from annular integrals",
		   name);
    if (status != NULL) *status = status2;
#if 0 /* searching for a memory bug - jan 23, 2018 */
    thPsfmodelDel(psf);
    psf = NULL;
#endif
    return(NULL);
  }
  
  CRUDE_CALIB *cc = phpsf->cc;
  if (cc == NULL) {
	thError("%s: WARNING - null (crude_calib) found in (phpsf)", name);
  }
  psf->cc = cc;
  psf->parent = phpsf;

	/*  border creation has moved to fftw_kern
  status2 = create_kl_psf_border(psf->basis, psf->border_mask);
  if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not create KL-psf's border mask", name);
	if (status != NULL) *status = status2;
	thPsfmodelDel(psf);
        psf = NULL;
 	return(NULL);
  }
	*/

  if (status != NULL) *status = SH_SUCCESS;
  return(psf);
}

/* 
RET_CODE create_kl_psf_border(PSF_BASIS *basis, WMASK *wmask) {
char *name = "create_kl_psf_boder";
shAssert(basis != NULL);
shAssert(wmask != NULL);

return(SH_GENERIC_ERROR);

}
*/

PSFWING *thPsfWingGetFromAnnulus(PHPSF_WING *phwing, int filter, 
				  RET_CODE *status) {

  char *name = "thPsfWingGetFromAnnulus";

  if (phwing == NULL) {
    thError("%s: null wing", name);
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
  }

  if (filter < 0 || filter >= NCOLOR) {
    thError("%s: filter (%d) does not lie within the range [%d, %d]",
		   name, filter, 0, NCOLOR - 1);
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
  }

  /* generate list of radii {R} */
  THPIX *t, *area;
  int i, nrad;
  nrad = phwing->prof_nprof[filter];

#if DEBUG_PSF
printf("%s: read number of annula (nrad = %d) \n", name, nrad);
#endif
 
 t = (THPIX *) thCalloc((nrad+1), sizeof(THPIX));
  area = (THPIX *) thCalloc(nrad+1, sizeof(THPIX));
  t[0] = (THPIX) 0.0;
  area[0] = (THPIX) 0.0;
  for (i = 1; i < nrad+1; i++) {
    t[i] = anndex[i] - 0.5;
    area[i] = (pow(t[i], 2) - pow(t[i-1], 2)) * THPI; 
  }
 
  int maxr;
  maxr = anndex[nrad];
 
  /* generate the integrated radial profile I(r < R) */
  THPIX *fI;
  fI = (THPIX *) thCalloc(nrad + 1, sizeof(THPIX));
  fI[0] = (THPIX) 0.0;
  for (i = 1; i < nrad + 1; i++) {
    fI[i] = fI[i-1] + area[i] * phwing->prof_mean[filter][i-1];
  }

 THPIX totlum = fI[nrad];
  int dfI = 0; 
  THPIX *gI = NULL;
  gI = thISplineExpandAnnulus(t, fI, nrad + 1, &dfI); 
  if (gI == NULL) {
	thError("%s: ERROR - could not conduct I-spline on annular information", name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
   }
  REGION *wreg = NULL;
  RET_CODE status2;
  wreg = thRegFromWingInfo(maxr, gI, t, dfI, nrad+1, &status2);
  if (status2 != SH_SUCCESS) {
    thError("%s: problem generating the wing image", name);
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
  }
  if (wreg == NULL) {
	thError("%s: problem generating the wing image. (null) region produced", name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
  }

  int nr = maxr;
  THPIX *IntegLP = NULL;
  IntegLP = thIntegLPFromWingInfo(maxr, gI, t, dfI, nrad + 1, &status2);
  if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not make integrated light profile", name);
	if (status != NULL) *status = status2;
	return(NULL);
	} 
  int err_count = 0, err_count2 = 0;
  for (i = 0; i < nr; i++) {
	IntegLP[i] = (THPIX) 1.0 - IntegLP[i]  / totlum;
	if (IntegLP[i] < (THPIX) 0.0) {
		err_count++;
	} else if (IntegLP[i] > (THPIX) 1.0) {
		err_count2++;
	}
	}
  if (err_count > 0) {
	thError("%s: ERROR - found (%d) negative values in integrated PSF profile", name, err_count);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
	}
  if (err_count2 > 0) {	
	thError("%s: ERROR - found (%d) values above 1.0 in integrate PSF profile", name, err_count2);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
	}

  /* now map onto output */
  PSFWING *wing;
  wing = thPsfwingNew();
  for (i = 0; i < nrad; i++) {
    wing->a[i] = fI[i];
    wing->rad[i] = t[i];
  }
  wing->nrad = nrad;
  wing->reg = wreg;
  wing->totlum = totlum;

#if DEBUG_PSF
  printf("%s: Integrated PSF = ", name);
  for (i = 0; i < nr; i++) printf("%20.10F, ", (double) IntegLP[i]);
  printf("\n");
#endif

  memset(wing->IntegLP, '\0', MAXNROW * sizeof(THPIX));
  memcpy(wing->IntegLP, IntegLP, nr * sizeof(THPIX));
  THPIX *rIntegLP = wing->rIntegLP;
  for (i = 0; i < MAXNROW; i++) rIntegLP[i] = (THPIX) i;
  wing->nr = nr;
  thFree(IntegLP);



  /* freeing work space */
  thFree(t);
  thFree(fI);

  if (status != NULL) *status = SH_SUCCESS;
  return(wing);
}

RET_CODE thPsfRegGetFromAnnulus(PSFWING *psfwing) {

  char *name = "thPsfRegGetFromAnnulus";

  if (psfwing == NULL) {
    thError("%s: null wing", name);
    return(SH_GENERIC_ERROR);
  }

  /* generate list of radii {R} */
  THPIX *t, *area, *r;
  int i, nrad;
  nrad = psfwing->nrad;
  if ((r = psfwing->rad) == NULL) {
	thError("%s: ERROR - null radii list", name);
	return(SH_GENERIC_ERROR);
	}
  THPIX *a;
  if ((a = psfwing->a) == NULL) {
	thError("%s: ERROR - null annular sum array", name);
	return(SH_GENERIC_ERROR);
	}
  if (nrad <= 0) {
	thError("%s: ERROR - (nrad = %d) not supported", name, nrad);
	return(SH_GENERIC_ERROR);
	}
#if DEBUG_PSF_2
printf("%s: read number of annula (nrad = %d) \n", name, nrad);
#endif
 
 t = (THPIX *) thCalloc((nrad+1), sizeof(THPIX));
  area = (THPIX *) thCalloc(nrad+1, sizeof(THPIX));
  t[0] = 0.0;
  area[0] = 0.0;
  for (i = 1; i < nrad+1; i++) {
    t[i] = r[i - 1]; /* changed */
    area[i] = (pow(t[i], 2) - pow(t[i-1], 2)) * THPI; 
  }
 
  int maxr;
  maxr = t[nrad];
 
  /* generate the integrated radial profile I(r < R) */
  THPIX *fI = (THPIX *) thCalloc(nrad + 1, sizeof(THPIX));
  fI[0] = (THPIX) 0.0;
  for (i = 1; i < nrad + 1; i++) {
    fI[i] = fI[i-1] + a[i-1]; 
  }

	/* debug */
  #if DEBUG_PSF_2
  printf("%s: (i, r[i], f[i], a[i]) = ", name);
  for (i = 0; i < nrad + 1; i++) {
	printf("(%d, %g, %g, %g), ", i, (float) t[i], (float) fI[i], (float) a[i]);
	}
	printf("\n");
  #endif
  THPIX totlum = fI[nrad];
  int dfI = 0;
  THPIX *gI = thISplineExpandAnnulus(t, fI, nrad + 1, &dfI);
  if (gI == NULL) {
	thError("%s: ERROR - could not expand in I-spline basis", name);
	return(SH_GENERIC_ERROR);
  }
/* 
  for (i = 0; i < nrad + 1; i++) {
	fI[i] = fI[i+1];
  }
  for (i = nrad + 1; i < dfI; i++) {
	fI[i] = (THPIX) 0.0;
  }
*/

  RET_CODE status2;

  int nr = maxr;
  THPIX *IntegLP = NULL;
  IntegLP = thIntegLPFromWingInfo(maxr, gI, t, dfI, nrad + 1, &status2);
  if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not make integrated light profile", name);
	return(status2);
	} 
  int err_count = 0, err_count2 = 0;
  for (i = 0; i < nr; i++) {
	IntegLP[i] = (THPIX) 1.0 - IntegLP[i]  / totlum;
	if (IntegLP[i] < (THPIX) 0.0) {
		err_count++;
	} else if (IntegLP[i] > (THPIX) 1.0) {
		err_count2++;
	}
	}
  if (err_count > 0) {
	thError("%s: ERROR - found (%d) negative values in integrated PSF profile", name, err_count);
	return(SH_GENERIC_ERROR);
	}
  if (err_count2 > 0) {	
	thError("%s: ERROR - found (%d) values above 1.0 in integrate PSF profile", name, err_count2);
	return(SH_GENERIC_ERROR);
	}

  REGION *wreg;
  wreg = thRegFromWingInfo(maxr, gI, t, dfI, nrad + 1, &status2);
  if (status2 != SH_SUCCESS) {
    thError("%s: problem generating the wing image", name);
    return(SH_GENERIC_ERROR);
  } 

  /* now map onto output */
  if (psfwing->reg == NULL) {
	psfwing->reg = shRegNew(name, wreg->nrow, wreg->ncol, TYPE_THPIX);
  }
  shRegReshape(psfwing->reg, wreg->nrow, wreg->ncol);
  shRegClear(psfwing->reg);
  shRegPixCopy(wreg, psfwing->reg);
  shRegDel(wreg);
  psfwing->totlum = totlum;

#if DEBUG_PSF
  printf("%s: Integrated PSF = ", name);
  for (i = 0; i < nr; i++) printf("%20.10F, ", (double) IntegLP[i]);
  printf("\n");
#endif

  memset(psfwing->IntegLP, '\0', MAXNROW * sizeof(THPIX));
  memcpy(psfwing->IntegLP, IntegLP, nr * sizeof(THPIX));
  THPIX *rIntegLP = psfwing->rIntegLP;
  for (i = 0; i < MAXNROW; i++) rIntegLP[i] = (THPIX) i;
  psfwing->nr = nr;
  thFree(IntegLP);

  /* freeing work space */
  thFree(t);
  thFree(fI);

  return(SH_SUCCESS);
}



THPIX *thISplineExpandAnnulus(THPIX *t, THPIX *fI, int nrad, int *dgI) {
  char *name = "thISplineExpandAnnulus";
  shAssert(fI != NULL);
  shAssert(t != NULL);
  shAssert(nrad > 1);

  /* generate I(i | k, {R}) - don't save it */
  THPIX **bI;
  int dfI = 0;
  bI = get_Ispline_basis(t, t, nrad, nrad, WING_SPLINE_ORDER, &dfI);
  int i;
  #if DEBUG_PSF_2
	printf("%s: bI[degree.of.freedom][t.index] \n", name);
	for (i = 0; i < dfI; i++) {
		int j;
		for (j = 0; j < nrad; j++) {
			printf("[%2d][%2d]= %6.2g, ", i, j, (float) bI[i][j]);
		}
		printf("\n");
	}
  #endif
 
  /* generate derivatives at L, U */
  RET_CODE status2;

#if DEBUG_PSF_2
	printf("%s: calculating derivatives at U and L \n", name);
#endif

  THPIX U = t[nrad - 1], L = t[0];
  int ndiv = WING_SPLINE_ORDER + 10;
  int ddegree = MIN(WING_SPLINE_ORDER, ndiv);

  THPIX Udiv = (t[nrad - 1] - t[nrad - 2]) / (THPIX) ndiv;
  THPIX Ldiv = (t[1] - t[0]) / (THPIX) ndiv;
#if DEBUG_PSF_2
  printf("%s: U-div = %6.2g, L-div = %6.2g \n", name, (float) Udiv, (float) Ldiv);
#endif

  THPIX *dtU = thCalloc(ndiv, sizeof(THPIX));
  THPIX *dtL = thCalloc(ndiv, sizeof(THPIX));
  THPIX *tU = thCalloc(ndiv, sizeof(THPIX));
  THPIX *tL = thCalloc(ndiv, sizeof(THPIX));
  for (i = 0; i < ndiv; i++) {
	 tU[i]  = U - i * Udiv;
   	 dtU[i] = tU[i] - U;
	 tL[i]  = L + i * Ldiv;
	 dtL[i] = tL[i] - L;
  }
#if DEBUG_PSF_2
printf("%s: tU[i] = ", name);
for (i = 0; i < ndiv; i++) {
	printf("[%2d]= %7.2g, ", i, tU[i]);
}
printf("\n");
#endif
  /* it is better to use M as derivative of I than to take the derivative of I directly. I-spline gets very close to 1.0 when x --> U, and taking differences becomes problematic due to precision considerations */
  int dfUM = 0, dfLM = 0;
  THPIX **uM = get_Mspline_basis(tU, t, ndiv, nrad, WING_SPLINE_ORDER, &dfUM); 
  THPIX **lM = get_Mspline_basis(tL, t, ndiv, nrad, WING_SPLINE_ORDER, &dfLM);
  if (dfLM != dfUM) {
	thError("%s: ERROR - source code problem", name);
	return(NULL);
  }
  int dfM = dfUM;
  int j, k;
  THPIX **bUM = thCalloc(ndiv, sizeof(THPIX *));
  THPIX **bLM = thCalloc(ndiv, sizeof(THPIX *));
  bUM[0] = thCalloc(ndiv * ndiv, sizeof(THPIX)); 
  bLM[0] = thCalloc(ndiv * ndiv, sizeof(THPIX));
 for (i = 1; i < ndiv; i++)  {
	bUM[i] = bUM[0] + i * ndiv;
	bLM[i] = bLM[0] + i * ndiv;
  }
  for (i = 0; i < ndiv; i++) { /* note that the first row and first column are 0's and 1's */
 	THPIX dU = dtU[i];
	THPIX dL = dtL[i];
	THPIX *bUM_i =  bUM[i];
	THPIX *bLM_i =  bLM[i];
	bUM_i[0] = 1.0;
	bLM_i[0] = 1.0;
	if (i == 0) {
		for (j = 1; j < ndiv; j++) {
			bUM_i[j] = 0.0;
			bLM_i[j] = 0.0;
		}
	} else {	 
		for (j = 1; j < ndiv; j++) {
			/* factorial taken care of here */
			bUM_i[j] = bUM_i[j-1] * dU / (THPIX) j;
			bLM_i[j] = bLM_i[j-1] * dL / (THPIX) j;
		}
	}
  }

  THPIX **cUM, **cLM;
  cUM = thCalloc(ndiv, sizeof(THPIX *));
  cLM = thCalloc(ndiv, sizeof(THPIX *));
  
  cUM[0] = thCalloc(ndiv * ndiv, sizeof(THPIX));
  cLM[0] = thCalloc(ndiv * ndiv, sizeof(THPIX));
  for (i = 1; i < ndiv; i++) {
	cUM[i] = cUM[0] + i * ndiv;
	cLM[i] = cLM[0] + i * ndiv;
  }

for (k = 0; k < dfM; k++) { 
 
	THPIX *DUM = uM[k];
	THPIX *DLM = lM[k];
	for (i = 0; i < ndiv; i++) {
		memcpy(cUM[i], bUM[i], ndiv * sizeof(THPIX));
		memcpy(cLM[i], bLM[i], ndiv * sizeof(THPIX));
	}
	#if DEBUG_PSF_2
	printf("%s: differences: DUM[degree.of.freedom][diff.order = %d]: ", name, WING_SPLINE_ORDER);
	for (i = 0; i < ndiv; i++) {
		printf("[%2d][%2d]= %7.2g, ", k, i, (float) DUM[i]);
	}
	printf("\n");	
	printf("%s: differences: DLM[degree.of.freedom][diff.order = %d]: ", name, WING_SPLINE_ORDER);
	for (i = 0; i < ndiv; i++) {
		printf("[%2d][%2d]= %7.2g, ", k, i, (float) DLM[i]);
	}
	printf("\n");		

	#endif
	  status2 = thMatrixInvert(cUM, DUM, ndiv);
	  if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not get the derivatives of %d'th I-spline basis at point U-", name, k);
		if (dgI != NULL) *dgI = -1;
		return(NULL);
	  }
	  status2 = thMatrixInvert(cLM, DLM, ndiv);
	  if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not get the derivatives of %d'th I-spline basis at point L+", name, k);
		return(NULL);
	  }

/* 
	  THPIX factorial = 1.0;
	  for (i = 1; i < ndiv; i++) {
		factorial *= (THPIX) (i);
		DUM[i] *= factorial;
		DLM[i] *= factorial;
	  }
*/

	#if DEBUG_PSF_2
	printf("%s: derivatives: DUM[degree.of.freedom][diff.order = %d]: ", name, WING_SPLINE_ORDER);
	for (i = 0; i < ndiv; i++) {
		printf("[%2d][%2d]= %7.2g, ", k, i, (float) DUM[i]);
	}
	printf("\n");	
	printf("%s: derivatives: DLM[degree.of.freedom][diff.order = %d]: ", name, WING_SPLINE_ORDER);
	for (i = 0; i < ndiv; i++) {
		printf("[%2d][%2d]= %7.2g, ", k, i, (float) DLM[i]);
	}
	printf("\n");		

	#endif
  }
#if DEBUG_PSF_2
printf("%s: derivatives of M-spline bases at U [degree.of.freedom][order.of.deriv.]\n", name);
for (k = 0; k < dfM; k++) {
	THPIX *DUM = uM[k];
	for (i = 0; i < ndiv; i++) {
		printf("[%2d][%2d]: %7.2g, ", k, i, (float) DUM[i]);
	}
	printf("\n");
}

printf("%s: derivatives of M-spline bases at L [degree.of.freedom][order.of.deriv.]\n", name);
for (k = 0; k < dfM; k++) {
	THPIX *DLM = lM[k];
	for (i = 0; i < ndiv; i++) {
		printf("[%2d][%2d]: %7.2g, ", k, i, (float) DLM[i]);
	}
	printf("\n");
}
#endif

  /* multiplying the derivatives such that it gets sizable values */
  for (i = 0; i < ndiv; i++) {
	THPIX maxdiff = thabs(uM[0][i]);
	for (k = 1; k < dfM; k++) {
		maxdiff = MAX(maxdiff, thabs(uM[k][i]));
	}
	for (k = 0; k < dfM; k++) {
		uM[k][i] /= maxdiff;
	}
  }
  for (i = 0; i < ndiv; i++) {
	THPIX maxdiff = thabs(lM[0][i]);
	for (k = 1; k < dfM; k++) {
		maxdiff = MAX(maxdiff, thabs(lM[k][i]));
	}
	for (k = 0; k < dfM; k++) {
		lM[k][i] /= maxdiff;
	}
  }

  THPIX **dmatrix = uM, **lmatrix = lM;

  /* 
     get the total luminosity -- 
     compare it with the total luminosity 
     mentioned in PHPSF_WING structure
  */
  THPIX totlum;
  totlum = fI[nrad - 1];
 /* 
     find the I-spline coefficients a_i such that
     a_i * I(i, r_j | k, {R}) = f_j
     Notice that this is a row equation and not a column one so a transposition of the matrix is necessary
  */

#if DEBUG_PSF_2
  printf("%s: dfI = %d, nrad = %d \n", name, dfI, nrad);
#endif
  THPIX **bbI = thCalloc(dfI, sizeof(THPIX *)); 
  bbI[0] = thCalloc(dfI * dfI, sizeof(THPIX));
  for (k = 1; k < dfI; k++) {
	bbI[k] = bbI[0] + k * dfI;
  }
  for (i = 0; i < dfI; i++) {
	for (j = 0; j < nrad; j++) {
		bbI[j][i] = bI[i][j];
	}
	if (i != 0) {

	if (dfI - nrad >= 1) {
		j = 0;
		bbI[j + nrad][i] = lmatrix[i-1][j];
	}
	for (j = 1; j < dfI - nrad; j++) {
  		bbI[j + nrad][i] = dmatrix[i-1][ddegree - j]; /* nrad - j */
	} 
	
	} else {
		for (j = 0; j < dfI - nrad; j++) {
  		bbI[j + nrad][i] = 0.0;
	} 
	}
  }
  #if DEBUG_PSF_2
 printf("%s: bI[t.index][degree.of.freedom] = \n", name);
  for (i = 0; i < dfI; i++) {
	for (j = 0; j < nrad; j++) {
		printf("[%2d][%2d]= %6.2g, ", i, j, (float) bI[i][j]);
	}
	printf("\n");
  }
 
 printf("%s: bbI[t.index][degree.of.freedom] = \n", name);
  for (i = 0; i < dfI; i++) {
	for (j = 0; j < dfI; j++) {
		printf("[%2d][%2d]= %6.2g, ", i, j, (float) bbI[i][j]);
	}
	printf("\n");
  }
  #endif

  THPIX *gI = thCalloc(dfI, sizeof(THPIX));
  for (k = 0; k < nrad; k++) {
	gI[k] = fI[k];
  }
  status2 = thMatrixInvert(bbI, gI, dfI);
  if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not find I-spline coefficients", name);
	if (dgI != NULL) *dgI = -1;
	return(NULL);
  }

  /* debug */
#if DEBUG_PSF_2

  printf("%s: inverteed bbI matrix \n", name);
  for (i = 0; i < dfI; i++) {
	for (k = 0; k < dfI; k++) {
		printf("[%2d][%2d]= %7.1g,", i, k, (float) bbI[i][k]);
	}
	printf("\n");
  }
#endif

#if DEBUG_PSF_2

  printf("%s: I-spline annular input (k: fI[k]) = ", name);
  for (k = 0; k < nrad; k++) {
	printf("[%2d]=  %8.3g, ", k, (float) fI[k]);
  }
  printf("\n");


  printf("%s: I-spline coefficients  (k: gI[k]) = ", name);
  for (k = 0; k < dfI; k++) {
	printf("[%2d]=  %8.3g, ", k, (float) gI[k]);
  }
  printf("\n");

#endif

  /* freeing the memory */
  thFree(bbI[0]);
  thFree(bbI);

  for (k = 0; k < dfM; k++) {
	thFree(uM[k]);
	thFree(lM[k]);
  }
  thFree(uM);
  thFree(lM);
  thFree(bUM[0]);
  thFree(bUM);
  thFree(bLM[0]);
  thFree(bLM);
  thFree(cUM[0]);
  thFree(cUM);
  thFree(cLM[0]);
  thFree(cLM);
  thFree(dtU);
  thFree(dtL);
  thFree(tU);
  thFree(tL);

  /* free i-space */

  for (i = 0; i < dfI; i++) {
    thFree(bI[i]);
  }
  thFree(bI);

  if (dgI != NULL) *dgI = dfI;
  return(gI);
}

THPIX *thIntegLPFromWingInfo(int maxr, THPIX *fI, 
			  THPIX *t, int dfI, int nrad, 
			  RET_CODE *status) {

  char *name = "thIntegLPFromWingInfo";

  if (fI == NULL ||  t == NULL) {
    thError("%s: ERROR - null input", name);
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
  }

  if (maxr > (int) (t[nrad-1] + 0.5) || maxr <= 0) {
    thError("%s: ERROR - size of the region (%d) does not match profile description (rm = %g)", name, maxr, (float) (t[nrad-1] + 0.5));
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
  }

  

  /* To get the value of profile I(x, y) on the lattice 
     generate the list of radii {r} for the quarter (1/8) lattice (x, y)
     generate M(r, i | k, {R}) for {r}
     add multiply values of the M-spline basis with a_i and divide by r
     
     when saving, take note of the pixel coordinates of the center of
     the profile
  */
  int i, nr;
  nr = (maxr);
  
  THPIX *r = shCalloc(nr, sizeof(THPIX));
  for (i = 0; i < nr; i++) {
       r[i] = (THPIX) i;
  }
  r[0] = (THPIX) SPLINE_R_DITHER;

  THPIX **bI = NULL, *bIval = NULL;
  int dfI2 = 0;
  bI = get_Ispline_basis (r, t, nr, nrad, WING_SPLINE_ORDER, &dfI2);
 
  if (dfI2 != dfI) {
	thError("%s: ERROR - I degrees of freedom mismatch (dfI = %d, dfI2 = %d)", name, dfI, dfI2);
    if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
  } 
  THPIX *IntegLP = shCalloc(nr, sizeof(THPIX)); 

  /* there might be a shorter way of doing the following calculation
     if one could specify for a given x, the range of i such that 

     M(i, x | k, t) .ne. 0

     Having this you will have an algorithm aboutr 3-5 times faster
     for our chosen values of k and {t}
  */

  /* finding the linear combination of bM's */

  #if DEBUG_PSF
  int czero = 0;
  #endif
  int l;
  THPIX a;
  for (l = 0; l < dfI; l++) {
    bIval = bI[l];
    a = fI[l]; /* note that the first fI belongs to a constant function with derivative 0, that is why we are using fI[l + 1] */
    for (i = 0; i < nr; i++) {
	IntegLP[i] += bIval[i] * a;
	#if DEBUG_PSF
	if (bIval[i] == 0) czero++;
	#endif
    }
  }

  #if DEBUG_PSF
  printf("%s: IntegLP[%d] = %20.10F \n", name, nr - 1, (double) IntegLP[nr - 1]); 
  printf("%s: (%d) out of (%d) elements were zero in bIval (%g) \n", name, czero, dfI * nr, (float) czero / (float) (dfI * nr));
  #endif
  /* dividing by radius */

  /* free work space */ 

  for (i = 0; i < dfI; i++) {
    thFree(bI[i]);
  }
  thFree(bI);
  thFree(r);

  if (status != NULL) *status = SH_SUCCESS;
  return(IntegLP);
}

REGION *thRegFromWingInfo(int maxr, THPIX *fI, 
			  THPIX *t, int dfI, int nrad, 
			  RET_CODE *status) {

  char *name = "thRegFromWingInfo";

  if (fI == NULL ||  t == NULL) {
    thError("%s: ERROR - null input", name);
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
  }

  if (maxr > (int) (t[nrad-1] + 0.5) || maxr <= 0) {
    thError("%s: ERROR - size of the region (%d) does not match profile description (rm = %g)", name, maxr, (float) (t[nrad-1] + 0.5));
    if (status != NULL) *status = SH_GENERIC_ERROR;
    return(NULL);
  }

  

  /* To get the value of profile I(x, y) on the lattice 
     generate the list of radii {r} for the quarter (1/8) lattice (x, y)
     generate M(r, i | k, {R}) for {r}
     add multiply values of the M-spline basis with a_i and divide by r
     
     when saving, take note of the pixel coordinates of the center of
     the profile
  */
  int i, j, nr;
  nr = (maxr + 1) * (maxr + 1);
  
  THPIX *r, *this_r;
  THPIX **rrow = thCalloc(maxr + 1, sizeof(THPIX *));
  rrow[0] = thCalloc(nr, sizeof(THPIX));
  for (i = 1; i < maxr + 1; i++) {
	rrow[i] = rrow[0] + i * (maxr + 1);
  }
  r = rrow[0];
  this_r = r;
  for (i = 0; i < maxr + 1; i++) {
    THPIX *rrow_i = rrow[i];
    for (j = 0; j < maxr + 1; j++) {
       rrow_i[j] = pow(pow((THPIX) i, 2.0) + pow((THPIX) j, 2.0), 0.5);
    }
  }
  rrow[0][0] = (THPIX) SPLINE_R_DITHER;

  THPIX **bM, *bMval, *rrows;
  REGION *reg, *wreg;
  int dfM = 0;
  bM = get_Mspline_basis (r, t, nr, nrad, WING_SPLINE_ORDER, &dfM);
 
  if (dfM + 1 != dfI) {
	thError("%s: ERROR - I-M degrees of freedom mismatch (dfI = %d, dfM = %d)", name, dfI, dfM);
    if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
  }  
  reg = shRegNew("full psf wing image", 2 * maxr + 1, 2 * maxr + 1, TYPE_THPIX); 
  wreg = shSubRegNew("quarter wing-profile", reg, maxr + 1, maxr + 1, maxr, maxr, TYPE_THPIX); 
  

  /* there might be a shorter way of doing the following calculation
     if one could specify for a given x, the range of i such that 

     M(i, x | k, t) .ne. 0

     Having this you will have an algorithm aboutr 3-5 times faster
     for our chosen values of k and {t}
  */

  shRegClear(wreg);

  /* finding the linear combination of bM's */

  #if DEBUG_PSF
  int czero = 0;
  #endif
  int l;
  THPIX a;
  for (l = 0; l < dfM; l++) {
    bMval = bM[l];
    a = fI[l + 1]; /* note that the first fI belongs to a constant function with derivative 0, that is why we are using fI[l + 1] */
    for (i = 0; i < maxr + 1; i++) {
      rrows = wreg->rows_thpix[i];
      for (j = 0; j < maxr + 1; j++) {
	rrows[j] += (*bMval) * a;
	#if DEBUG_PSF
	if (*bMval == 0) czero++;
	#endif
	bMval++;
      }
    }
  }

  #if DEBUG_PSF
  printf("%s: (%d) out of (%d) elements were zero in bMval (%g) \n", name, czero, nrad * nr, (float) czero / (float) (nrad * nr));
  #endif
  /* dividing by radius */

  THPIX *rval;
  rval = r;

  for (i = 0; i < maxr + 1; i++) {
      THPIX *wrows_i = wreg->rows_thpix[i];
      THPIX *rows_i = rrow[i];
      for (j = 0; j < maxr + 1; j++) {
	wrows_i[j] /= rows_i[j] * THTWOPI;
      }
    }
 
  /* converting quarter to full image */
  for (i = 0; i < maxr; i++) {
	THPIX *wrow_i = wreg->rows_thpix[maxr - i];
	THPIX *row_i = reg->rows_thpix[i];
	for (j = 0; j < maxr; j++) {
		row_i[j] = wrow_i[maxr - j]; /* 3rd Quadrant */
	}
	for (j = maxr; j < 2 * maxr + 1; j++) {
		row_i[j] = wrow_i[j - maxr]; /* 4th Quadrant */
	}
  }
  for (i = maxr; i < 2 * maxr + 1; i++) {
	THPIX *wrow_i = wreg->rows_thpix[i - maxr];
	THPIX *row_i = reg->rows_thpix[i];
	for (j = 0; j < maxr; j++) {
		row_i[j] = wrow_i[maxr - j]; /* 2nd Quadrant */
	}
  }

  /* free work space */ 

  for (i = 0; i < dfM; i++) {
    thFree(bM[i]);
  }
  thFree(bM);
  thFree(rrow[0]);
  thFree(rrow);
  shRegDel(wreg);

  if (status != NULL) *status = SH_SUCCESS;
  return(reg);
}

/* ========================================================================*/

RET_CODE shRegConvolve(REGION *reg, REGION *outreg, PSF_CONVOLUTION_INFO *psf_info) {
char *name = "shRegConvolve";
if (reg == NULL || outreg == NULL || psf_info == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = thPsfInfoUpdateWpsf(psf_info);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update (wpsf) in (psf_info)", name);
	return(status);
}

WPSF *wpsf; PSF_CONVOLUTION_TYPE conv_type; int psf_n;
wpsf = psf_info->wpsf;
conv_type = psf_info->conv_type;
status = thPsfConvInfoGetNComponent(psf_info, &psf_n);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get number of components for (psf_info)", name);
	return(status);
}

REGION *scr = NULL, *inscr = NULL;
int nrow, ncol, row0, col0;
row0 = reg->row0; col0 = reg->col0;
nrow = reg->nrow; ncol = reg->ncol;

REGION *screg;
if (reg != outreg) {
	screg = outreg;
} else {
	screg = shRegNew("scratch output region", nrow, ncol, TYPE_THPIX);
	screg->row0 = row0;
	screg->col0 = col0;
}
shRegClear(screg);
if (psf_n <= 1) {
	scr = screg;
} else {
	scr = shRegNew("scratch region for convolution", nrow, ncol, TYPE_THPIX);
	scr->row0 = row0;
	scr->col0 = col0;
	shRegClear(scr);
}
	
if (psf_n <= 1) {
	inscr = reg;
} else {
	inscr = shRegNew("scratch input region for convolution", nrow, ncol, TYPE_THPIX);
	inscr->row0 = row0;
	inscr->col0 = col0;
}
int i;
for (i = 0; i < psf_n; i++) {
	CONV_METHOD conv_method;
	FFTW_KERNEL *fftw_kern;
	status = thPsfInfoGetFftwKernel(psf_info, i, &fftw_kern);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get componet (%d) of (wpsf) in (psf_info)", name, i);
		return(status);
	}
	NAIVE_MASK *inmask, *outmask;
	int margin = -1;
	status = thPsfInfoGetMask(psf_info, i, &inmask, &outmask, &margin);
	if (status != SH_SUCCESS) {
		init_static_vars();
		thError("%s: ERROR - could not get mask for component (%d) of (psf_info) of type '%s'", name, i, schema_conv_type->elems[conv_type].name);
		return(status);
	}
	REGION *scrout = NULL, *scrin = NULL;
	scrout = shSubRegNewFromRegNaiveMask("scratch outreg. for psf conv.", scr, outmask);
	if (scrout == NULL) {
		thError("%s: ERROR - could not create subreg (%d) '%s' of the out-scratch region", name, i, shEnumNameGetFromValue("PSF_COMPONENT", i));
		return(SH_GENERIC_ERROR);
	}
	if (psf_n > 1)	shRegClear(scrout); /* otherwise it is clean */
	scrin = shSubRegNewFromRegNaiveMask("scratch inreg. for psf conv.", inscr, outmask);
	if (scrin == NULL) {
		thError("%s: ERROR - could not create subregion for component (%d) of the in-scracth region", name, i);
		return(SH_GENERIC_ERROR);
	}
	if (psf_n > 1) {
		shRegClear(scrin);
		status = shRegPixCopyMask(reg, scrin, inmask);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not copy in-pixels inside mask", name);
			return(status);
		}
	}
	#if DEEP_DEBUG_PSF
	tell_me_about_reg("main in-region", reg);
	tell_me_about_reg("scratch in-region", scrin);
	#endif

	conv_method = fftw_kern->conv_method;
	if (conv_method == DIRECT_CONV) {
		status = thDirectConvolve(scrin, scrout, fftw_kern, margin);
	} else if (conv_method == FFTW_CONV) {
		status = thFFTWConvolve(scrin, scrout, fftw_kern, margin);
	} else {
		init_static_vars();
		thError("%s: ERROR - unsupported (conv_method) '%s' in component (%d) of (psf_info) of type '%s'", name, schema_conv_method->elems[conv_method].name, i, schema_conv_type->elems[conv_type].name);
		return(SH_GENERIC_ERROR);
	}
	if (status != SH_SUCCESS) {
		init_static_vars();
		thError("%s: ERROR - could not perform '%s' for component (%d) of (psf_info) of (type) '%s'", name, schema_conv_method->elems[conv_method].name, i, schema_conv_type->elems[conv_type].name);
		return(status);
	}
	THPIX psf_a;
	psf_a = fftw_kern->psf_a;
	#if DEEP_DEBUG_PSF
	printf("%s: psf_a (i = %d) = %10.2e \n", name, i, (float) psf_a);
	#endif
	if (psf_n > 1) {
		status = thAddModelComponent(screg, scrout, psf_a);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not add result of psf convolution for component (%d) of type '%s'", name, i, schema_conv_type->elems[conv_type].name);
			return(status);
		}
	} else {
		shRegMultWithDbl ((double) psf_a, scrout, scrout);	
	}
	#if DEEP_DEBUG_PSF
	tell_me_about_reg("scrin for component", scrin);
	tell_me_about_reg("scrout for component", scrout);
	#endif

	shRegDel(scrout);
	shRegDel(scrin);
	scrout = NULL;
	scrin = NULL;	
}
#if DEEP_DEBUG_PSF
	tell_me_about_reg("*** main out-scr", screg);
#endif
if (reg == outreg) {
	shRegClear(outreg);
	shRegPixCopy(screg, outreg);
	shRegDel(screg);
}
if (psf_n > 1) {
	shRegDel(inscr);
	shRegDel(scr);
}
#if DEEP_DEBUG_PSF
	tell_me_about_reg("*** main out-region", outreg);
#endif

return(SH_SUCCESS);
}




RET_CODE shRegConvCentralPsf(REGION *reg, REGION *outreg, PSF_CONVOLUTION_INFO *psf_info) {
char *name = "shRegConvCentralPsf";
if (reg == NULL || psf_info == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (outreg == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}

thError("%s: ERROR - function outdated", name);
return(SH_GENERIC_ERROR);

#if 0
RET_CODE status;
status = thPsfInfoUpdateWpsf(psf_info);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update (wpsf) in (psf_info)", name);
	return(status);
}

status = thDirectConvolve(reg, outreg, NULL);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run the direct convolution on image ('%s') and kernel ('%s')", name, reg->name, kernel->name);
	return(status);
}

return(SH_SUCCESS);
#endif
}

RET_CODE thPsfInfoUpdateCentral(PSF_CONVOLUTION_INFO *psf_info, FFTW_KERNEL **fftw_kern_out) {
char *name = "thPsfInfoUpdateCentral";
if (psf_info == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_SUCCESS);
}
if (fftw_kern_out == NULL) {
	thError("%s: WARNING - null output placeholder", name);	
	return(SH_SUCCESS);
}
RET_CODE status;
FFTW_KERNEL *fftw_kernel = NULL;
status = thPsfInfoGetFftwKernel(psf_info, INNER_PSF, &fftw_kernel);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (psf_reg) from (psf_info)", name);
	return(status);
}
if (fftw_kernel == NULL) {
	thError("%s: ERROR - improperly allocated (psf_info)", name);
	return(SH_GENERIC_ERROR);
}
THPIX rowc, colc;
status = thPsfInfoGetCenter(psf_info, &rowc, &colc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the center location from (psf_info)", name);
	return(status);
}
if (rowc == fftw_kernel->rowc && colc == fftw_kernel->colc) {
	*fftw_kern_out = fftw_kernel;
	return(SH_SUCCESS);
}

/*
 * Read PSF basis which should already be constructed 
 */
PSF_BASIS *basis = NULL;
status = thPsfInfoGetPsfBasis(psf_info, &basis);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the (psf_basis) from (psf_info)", name);
	return(status);
}
/* reconstruct PSF at the specified point */
status = thPsfKLReconstruct(basis, fftw_kernel, rowc, colc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not reconstruct (psf) at (rowc, colc) = (%g, %g)", name, (float) rowc, (float) colc);
	return(status);
}

*fftw_kern_out = fftw_kernel;
return(SH_SUCCESS);
}

RET_CODE shRegConvWingPsf(REGION *reg, REGION *outreg, PSF_CONVOLUTION_INFO *psf_info) {
char *name = "shRegConvWingPsf";
if (reg == NULL || psf_info == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (outreg == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
FFTW_KERNEL *wing_kern;
status = thPsfInfoUpdateWingKern(psf_info, &wing_kern);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (psf_reg) from (psf_info)", name);
	return(status);
}
int margin = -1; /* this will cause a crash */
status = thFFTWConvolve(reg, outreg, wing_kern, margin);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not run the direct convolution on image ('%s') and kernel ('%s')", 
		name, reg->name, wing_kern->reg->name);
	return(status);
}

return(SH_SUCCESS);
}

/* adoptated from PHOTO function with the same name to save in memory fragmentation */

/*****************************************************************************/
/*
 * Given the position of an object (<rowc>, <colc>) and a KL PSF_BASIS
 * with its coefficients set, return a PSF_REG consisting of the PSF
 * reconstructed at that point
 */
RET_CODE  
thPsfKLReconstruct(const PSF_BASIS *basis, /* basis to use */
		   FFTW_KERNEL *fftw_kernel,
		   THPIX rowc,		/* location of */
		   THPIX colc)		/*    desired PSF */
{
char *name = "thPsfKLReconstruct";
float acoeff;			/* the "a" coefficient for component */
float *coeffs;			/* coefficients for spatial variation*/
const REGION *comp;			/* component of KL decomposition */
double counts;			/* psf_out->counts (from basis) */
const PSF_KERNEL *kern;		/* == basis->kern */
int is;				/* counter in sigma (== comp) dirn. */
int i, j, k;
int ncomp;				/* number of KL components in use */
int nrow, ncol;			/* == reg->n{rol,col} */
int nrow_b, ncol_b;			/* == kern->n{row,col}_b */
REGION *out;				/* == psf_out->reg */
FL32 **rows_comp, *row_comp;		/* == comp->rows, comp->rows[] */
FL32 **rows_out, *row_out;		/* == out->rows, out->rows[] */

kern = basis->kern;
shAssert(kern != NULL);   
shAssert(kern->nrow_a == 1 && kern->ncol_a == 1);
ncomp = kern->nsigma + 1;
nrow_b = kern->nrow_b;
ncol_b = kern->ncol_b;

shAssert(basis != NULL && basis->regs != NULL && basis->type == KL_BASIS);
comp = basis->regs[-1][0][0]->reg;
shAssert(comp != NULL && comp->type == TYPE_FL32);
nrow = comp->nrow; 
ncol = comp->ncol;
shAssert(fftw_kernel != NULL);
out = fftw_kernel->reg;

int tnrow, tncol;
tnrow = MIN(nrow, OUTER_PSF_SIZE);
tncol = MIN(ncol, OUTER_PSF_SIZE);

if (out == NULL) {
	out = shRegNew("reconstructed psf", tnrow, tncol, TYPE_THPIX);
	fftw_kernel->reg = out;
} else if (out->nrow != tnrow || out->ncol != tncol) {
	p_shRegRowsFree(out);
	p_shRegVectorFree(out);
	if (p_shRegVectorGet(out, tnrow, TYPE_THPIX) != 1) {
		thError("%s: ERROR - could not allocate (row) addresses in (psf_reg)", 
			name);
		return(SH_GENERIC_ERROR);
	}	
	if (p_shRegRowsGet(out, tnrow, tncol, TYPE_THPIX) != 1) {
		thError("%s: ERROR - could not allocated (rows) in (psf_reg)", name);
		return(SH_GENERIC_ERROR);
	}
}		
#if DEEP_DEBUG_PSF
printf("%s: psf_reg - (nrow, ncol) = (%d, %d) name = '%s' \n", name, out->nrow, out->ncol, out->name);
printf("%s: comp - (nrow, ncol) = (%d, %d) name = '%s' \n", name, nrow, ncol, comp->name);
#endif

rows_out = out->rows_thpix;
/*
 * Set a convenience array coeffs with the powers of rowc and colc to be used
 * for each of the nrow_b*ncol_b spatial terms
 */
coeffs = alloca(nrow_b*ncol_b*sizeof(float));

for(k = 0; k < nrow_b*ncol_b; k++) {
	coeffs[k] = pow(rowc*RC_SCALE, k%nrow_b)*pow(colc*RC_SCALE, k/nrow_b);
}

int trow0, tcol0;
trow0 = (nrow - tnrow) / 2;
tcol0 = (ncol - tncol) / 2;
shRegClear(out);
counts = 0;
for(is = -1; is < ncomp - 1; is++) {
	acoeff = 0;
	for(k = 0; k < nrow_b*ncol_b; k++) {
		acoeff += kern->a[is][0][0].c[k%nrow_b][k/nrow_b]*coeffs[k];
	}
	counts += acoeff*basis->regs[is][0][0]->counts;
	comp = basis->regs[is][0][0]->reg;
	rows_comp = comp->rows_fl32 + trow0;	
	for(i = 0; i < tnrow; i++) {
		row_out = rows_out[i];
		row_comp = rows_comp[i];
		for(j = 0; j < tncol; j++) {
	   		 row_out[j] += acoeff*row_comp[j + tcol0];
	 	}
      	}
}

fftw_kernel->counts = (THPIX) counts;
fftw_kernel->rowc = (THPIX) rowc;
fftw_kernel->colc = (THPIX) colc;

/* assigning the border */
WMASK *border;
if ((border = fftw_kernel->border) == NULL) {
	border = thLwmaskNew();
	fftw_kernel->border = border;
}
if (border->mask != NULL) {
	thMaskDel(border->mask);
	border->mask = NULL;
}
REGION *W;
if ((W = border->W) == NULL) {
	W = shRegNew("border for KL-PSF", tnrow, tncol, TYPE_THPIX);
	border->W = W;
}
if (W->nrow < tnrow || W->ncol < tncol) {
	shRegDel(W);
	W = shRegNew("border for KL-PSF", TYPE_THPIX, tnrow, tncol);
	border->W = W;
}

shRegClear(W);
THPIX *wrowi, **wrows;
wrows = W->rows_thpix;
THPIX icenter, jcenter;
icenter = W->nrow / 2.0;
jcenter = W->ncol / 2.0;
for (i = 0; i < W->nrow; i++) {
	wrowi = wrows[i];
	for (j = 0; j < W->ncol; j++) {
		THPIX r = pow(pow((THPIX) i - icenter, (THPIX) 2.0) + pow((THPIX) j - jcenter, (THPIX) 2.0), 0.5);
		if ((r - stat_inbound) * (r - stat_outbound) <= 0.0) wrowi[j] = (THPIX) 1.0;
	}
}

	

return(SH_SUCCESS);
}

RET_CODE thConvolve(REGION *in, REGION *out, FFTW_KERNEL *fftw_kern) {
char *name = "thConvolve";
if (in == NULL || out == NULL || fftw_kern == NULL) {
	thError("%s: ERROR - null input or output", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
CONV_METHOD conv_method;
conv_method = fftw_kern->conv_method;
int margin = -1; /* this will cause a crash */
if (conv_method == DIRECT_CONV) {
	status = thDirectConvolve(in, out, fftw_kern, margin);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not conduct direct convolution on (in-region) '%s'", name, in->name);
		return(status);
	}
	return(SH_SUCCESS);
} else if (conv_method == FFTW_CONV) {
	status = thFFTWConvolve(in, out, fftw_kern, margin);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not conduct FFT-convolution on (in-region) '%s'", name, in->name);
		return(status);
	}
	return(SH_SUCCESS);
} else {
	thError("%s: ERROR - unsupported (conv_type)", name);
	return(SH_GENERIC_ERROR);
}

}

RET_CODE thDirectConvolve(REGION *in, REGION *out, FFTW_KERNEL *fftw_kern, int margin) {
char *name = "thDirectConvolve";
if (in == NULL || out == NULL || fftw_kern == NULL) {
	thError("%s: ERROR - null input or output", name);
	return(SH_GENERIC_ERROR);
}
if (in->nrow != out->nrow || in->ncol != out->ncol) {
	thError("%s: ERROR - (in) and (out) (region) should have the same dimension", name);
	return(SH_GENERIC_ERROR);
}
if (margin != 0) {
	thError("%s: current function does not support variable psf size (margin = %d) while (0) was expected", name, margin);
	return(SH_GENERIC_ERROR);
}
int nrow, ncol;
nrow = in->nrow;
ncol = in->ncol;
if (nrow == 0 || ncol == 0) {
	thError("%s: ERROR - unaccepatble zero dimensions for (in) and (out) regions", name);
	return(SH_GENERIC_ERROR);
}
REGION *kern = NULL;
if (fftw_kern == NULL || (kern = fftw_kern->reg) == NULL) {
	thError("%s: ERROR - null or improperly allocated (fftw_kernel)", name);
	return(SH_GENERIC_ERROR);
}

int nrow_k, ncol_k;
nrow_k = kern->nrow;
ncol_k = kern->ncol;
if (nrow_k == 0 || ncol_k == 0) {
	thError("%s: ERROR - unacceptable zero dimentions for (kernel) region", name);
	return(SH_GENERIC_ERROR);
}
REGION *b = NULL;
if (in == out) {
	b = shRegNew("scratch space for direct convolution", nrow, ncol, TYPE_THPIX);
} else {
	b = out;
}
shRegClear(b);
int i_a, j_a, i_k, j_k;
THPIX *arow_i, *brow_i, *krow_i;
THPIX inval;
#if 0
int i_kmin, i_kmax, j_kmin, j_kmax, m_k;
for (i_a = 0; i_a < nrow; i_a++) {
	arow_i = in->rows_thpix[i_a];
	i_kmin = MAX(0, nrow_k / 2 - i_a);
	i_kmax = MIN(nrow_k, nrow + nrow_k / 2 - i_a);
	for (i_k = i_kmin; i_k < i_kmax; i_k++) {
		i_b = i_k + i_a - nrow_k / 2;
		krow_i = kern->rows_thpix[i_k];	
		for (j_a = 0; j_a < ncol; j_a++) {
			inval = arow_i[j_a];
			/* 
			brow_i = b->rows_thpix[i_b] + j_a - ncol_k / 2;
			*/
			j_kmin = MAX(0, ncol_k / 2 - j_a);
			j_kmax = MIN(ncol_k, ncol + ncol_k / 2 - j_a);
			m_k = j_kmax - j_kmin;
			krow_i = kern->rows_thpix[i_k] + j_kmin;
			brow_i = b->rows_thpix[i_b] + j_a - ncol_k / 2 + j_kmin; 
			for (j_k = 0; j_k + 3 < m_k; j_k = j_k + 4) {
				brow_i[j_k] += inval * krow_i[j_k];
				brow_i[j_k + 1] += inval * krow_i[j_k + 1];
				brow_i[j_k + 2] += inval * krow_i[j_k + 2];
				brow_i[j_k + 3] += inval * krow_i[j_k + 3];
			}
			for (j_k = (m_k / 4) * 4; j_k < m_k; j_k++) {
				brow_i[j_k] += inval * krow_i[j_k];
			}
		}
	}
}

#else 

int half_nrow_k, half_ncol_k;
half_nrow_k = (nrow_k + 1) / 2;
half_ncol_k = (ncol_k + 1) / 2;

for (i_a = half_nrow_k; i_a < nrow - half_nrow_k; i_a++) {
	arow_i = in->rows_thpix[i_a];
	for (j_a = half_ncol_k; j_a < ncol - half_ncol_k; j_a++) {
		inval = arow_i[j_a];
		for (i_k = 0; i_k < nrow_k; i_k++) {
			krow_i = kern->rows_thpix[i_k];
			brow_i = out->rows_thpix[i_a + i_k - nrow_k / 2] 
					+ j_a - ncol_k / 2;
			for (j_k = 0; j_k + 3 < ncol_k; j_k = j_k + 4) {
				brow_i[j_k] += inval * krow_i[j_k];
				brow_i[j_k + 1] += inval * krow_i[j_k + 1];
				brow_i[j_k + 2] += inval * krow_i[j_k + 2];
				brow_i[j_k + 3] += inval * krow_i[j_k + 3];
			}
			for (j_k = (ncol_k / 4) * 4; j_k < ncol_k; j_k++) {
				brow_i[j_k] += inval * krow_i[j_k];
			}
		}
	}
}

#endif
	
if (in == out) {
	shRegPixCopy(b, out);
	shRegDel(b);
}
return(SH_SUCCESS);
}

RET_CODE thPsfBasisFromKlcChain(CHAIN *klc_chain, PSF_BASIS **basis_out) {
char *name = "thPsfBasisFromKlcChain";
if (basis_out == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
int ncomp;
if (klc_chain == NULL || (ncomp = shChainSize(klc_chain)) == 0) {
	thError("%s: ERROR - null or empty (klc) chain", name);
	return(SH_GENERIC_ERROR);
}

PSF_BASIS *basis = NULL;
int icomp;
for(icomp = 0; icomp < ncomp; icomp++) {
	PSF_KL_COMP *klc = NULL;
	klc = shChainElementGetByPos(klc_chain, icomp);
	if (klc == NULL) {
		thError("%s: ERROR - null (klc) found for component (%d)", name, icomp);
		return(SH_GENERIC_ERROR);
	}
/*
 * Create the PSF_BASIS; we need the first PSF_KL_BASIS to do this
 */
	if(basis == NULL) {
		PSF_KERNEL *kern = phPsfKernelNew(ncomp - 1, 1, 1, klc->nrow_b, klc->ncol_b);
		basis = phPsfBasisNew(kern, KL_BASIS, -1, -1, klc->reg->type);
		phPsfKernelDel(kern);		/* decrement reference counter */
		kern = NULL;
	}
	phPsfBasisSetFromKLComp(basis, icomp, klc, 0); /* klc-reg is not copied */
}

*basis_out = basis;
return(SH_SUCCESS);
}

RET_CODE thFFTWConvolve(REGION *in, REGION *out, FFTW_KERNEL *kern, int margin) {
char *name = "thFFTWConvolve";
if (in == NULL || out == NULL || kern == NULL) {
	thError("%s: ERROR - null input or output", name);
	return(SH_GENERIC_ERROR);
}
int nrow, ncol;
nrow = in->nrow;
ncol = in->ncol;
if (nrow != out->nrow || ncol != out->ncol) {
	thError("%s: ERROR - incompatible (in: %d, %d) and (out: %d, %d) sizes", name, nrow, ncol, out->nrow, out->ncol);
	return(SH_GENERIC_ERROR);
}
FFTW_COMPLEX **fftw_image = NULL, **fftw_kern = NULL;
int prow, pcol;
RET_CODE status;

status = thFftwGetPadSize(in, kern, margin, &prow, &pcol);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the size of the padded image", name);
	return(status);
}

status = thFftwKernelUpdatePlan(kern, margin, prow, pcol);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update the (fftw plans)", name);
	return(status);
}

#if DEEP_DEBUG_PSF
tell_me_about_reg("in-image - region ", in);
#endif
fftw_image = thFftwComplexNewFromReg(in, kern, margin, prow, pcol, &status);
if (status != SH_SUCCESS || fftw_image == NULL) {
	thError("%s: ERROR - problem generating 2d-array for (in) region", name);
	if (status == SH_SUCCESS) status = SH_GENERIC_ERROR;
	return(status);
}

/* this function should care about alteration flag */
fftw_kern = thFftwKernelGetComplex(kern, margin, prow, pcol, &status);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get the (fft) of the (kernel)", name);
	return(status);
}

#if DEEP_DEBUG_PSF
tell_me_about_fftw_arr("fftw_image - direct  space", &fftw_image[0][0], prow * pcol);
tell_me_about_fftw_arr("fftw_kern  - forward space", &fftw_kern[0][0], prow * pcol);
#endif

#if NO_FFTW_DURING_TEST
#else
fftwnd(kern->plan_fwd, 1, &fftw_image[0][0], 1, 0, NULL, 1, 0);
#endif
#if DEEP_DEBUG_PSF
tell_me_about_fftw_arr("fftw_image - forward space", &fftw_image[0][0], prow * pcol);
#endif

status = thFftwComplex2dMultiply(fftw_image, fftw_kern, fftw_image, prow, pcol);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not multiply (fftw-in) with (fftw-kernel) in place", name);
	return(status);
}
#if DEEP_DEBUG_PSF
tell_me_about_fftw_arr("fftw_image - multiplied", &fftw_image[0][0], prow * pcol);
#endif
#if NO_FFTW_DURING_TEST
#else
fftwnd(kern->plan_bck, 1, &fftw_image[0][0], 1, 0, NULL, 1, 0);
#endif
#if DEEP_DEBUG_PSF
tell_me_about_fftw_arr("ffw-image - convolved", &fftw_image[0][0], prow * pcol);
#endif

FFTWFL scale;
scale = pow((FFTWFL) prow * (FFTWFL) pcol, -1);
status = shRegFromFftwComplex(fftw_image, prow, pcol, scale, out);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not transform an image of type 'FFTW_COMPLEX' into dervish 'REGION'", name);
	return(status);
}

thFftwComplex2dDel(fftw_image);
return(SH_SUCCESS);
}

RET_CODE thPsfInfoUpdateWingKern(PSF_CONVOLUTION_INFO *psf_info, FFTW_KERNEL **wing_kern) {
char *name = "thPsfInfoUpdateWingKern";
PSFMODEL *psf_model;
if (psf_info == NULL || (psf_model = psf_info->wpsf->psf_model) == NULL) {
	thError("%s: ERROR - null or improperly allocated (psf_info)", name);
	return(SH_GENERIC_ERROR);
}
if (wing_kern == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
FFTW_KERNEL *wing_kernel;
if ((wing_kernel = psf_info->wpsf->fftw_kernel[1]) == NULL) {
	thError("%s: ERROR - improperly allocated (psf_info)", name);
	return(SH_SUCCESS);
}
PSFWING *psfwing;
if ((psfwing = psf_model->wing) == NULL) {
	thError("%s: ERROR - improperly allocated (psf_model) in (psf_info)", name);
	return(SH_GENERIC_ERROR);
}

wing_kernel->reg = psfwing->reg;
if (psfwing->reg == NULL) {
	thError("%s: ERROR - improperly allocated (psf_info->psf_model->wing)", name);
	return(SH_GENERIC_ERROR);
}

wing_kernel->nrow = psfwing->reg->nrow;
wing_kernel->nrow = psfwing->reg->ncol;
wing_kernel->totlum = psfwing->totlum;

*wing_kern = wing_kernel;
return(SH_SUCCESS);
}


FFTW_COMPLEX **thFftwComplexNewFromReg(REGION *in, FFTW_KERNEL *kern, int margin, int prow, int pcol, RET_CODE *status) {
char *name = "thFftwComplexNewFromReg";
if (in == NULL || kern == NULL) {
	thError("%s: ERROR - null input", name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
}
if (margin < 0) {
	thError("%s: ERROR - unsupported (margin = %d < 0)", name, margin);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
}
int psf_size = 2 * margin + 1;
if (prow < psf_size || pcol < psf_size) {
	thError("%s: ERROR - padded region (prow = %d, pcol = %d) too small for psf (size = %d, margin = %d)", name, prow, pcol, psf_size, margin);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
}
RET_CODE status2;

int nrow, ncol;
nrow = in->nrow;
ncol = in->ncol;
if (nrow > prow || ncol > pcol) {
	thError("%s: ERROR - unacceptable padded size (%d, %d) while the image size is (%d, %d)", name, 
		prow, pcol, nrow, ncol);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
}
if (nrow < psf_size || ncol < psf_size) {
	thError("%s: ERROR - image size (%d, %d) too small for psf (size = %d, margin = %d)", name, nrow, ncol, psf_size, margin);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
}
status2 = thFftwKernelUpdatePlan(kern, margin, prow, pcol);
if (status2 != SH_SUCCESS) {
	thError("%s: ERROR - could not update (fftw plans)", name);
	if (status != NULL) *status = status2;
	return(NULL);
}

FFTW_COMPLEX **fftw_image;
fftw_image = thFftwComplex2dNew(prow, pcol);
int i, j;
for (i = 0; i < nrow; i++) {
	THPIX *row_i = in->rows_thpix[i];
	FFTW_COMPLEX *fftw_row = fftw_image[i];
	for (j = 0; j < ncol; j++) {
		fftw_row[j].re = row_i[j];
	}
}
/* 
#if NO_FFTW_DURING_TEST
#else
fftwnd(kern->plan_fwd, 1, &fftw_image[0][0], 1, 0, NULL, 1, 0);
#endif
*/
if (status != NULL) *status = SH_SUCCESS;

return(fftw_image);
}

FFTW_COMPLEX **thFftwKernelGetComplex(FFTW_KERNEL *kern, int margin, int prow, int pcol, RET_CODE *status) {
char *name = "thFftwKernelGetComplex";

if (kern == NULL) {
	thError("%s: ERROR - null input", name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
}
int psf_size = 2 * margin + 1;
if (prow < psf_size || pcol < psf_size) {
	thError("%s: ERROR - padded size (%d, %d) too small for psf (size = %d, margin = %d)", name, prow, pcol, psf_size, margin);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
}
REGION *reg;
int nrow, ncol;
reg = kern->reg;
if (reg == NULL || (nrow = reg->nrow) == 0 || (ncol = reg->ncol) == 0) {
	thError("%s: ERROR - improperly allocated (fftw_kernel)", name);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
}
int nrow_eff, ncol_eff;
nrow_eff = MIN(nrow, psf_size);
ncol_eff = MIN(ncol, psf_size);
if (prow <= nrow_eff || pcol <= ncol_eff) {
	thError("%s: ERROR - unacceptable pad size (%d, %d) while kernel size is (%d, %d)", name, prow, pcol, nrow_eff, ncol_eff);
	if (status != NULL) *status = SH_GENERIC_ERROR;
	return(NULL);
}

FFTW_COMPLEX **fftw_kern;
fftw_kern = kern->fftw_kern;
MODIFICATION_FLAG aflag;
aflag = kern->aflag;
if (aflag == MODIFIED || prow != kern->prow || pcol != kern->pcol || kern->fftw_kern == NULL) {
	if ((aflag == MODIFIED || prow != kern->prow || pcol != kern->pcol) && 
		fftw_kern != NULL) {
		thFree(fftw_kern[0]);
		thFree(fftw_kern);	
		fftw_kern = NULL;

	}
	if (fftw_kern == NULL) {
		fftw_kern = thCalloc(prow, sizeof(FFTW_COMPLEX *));
		fftw_kern[0] = thCalloc(prow * pcol, sizeof(FFTW_COMPLEX));
		int i;
		for (i = 1; i < prow; i++) {
			fftw_kern[i] = fftw_kern[0] + i * pcol;
		}
	}
	kern->prow = prow;
	kern->pcol = pcol;	

	#if 1
	RET_CODE status2;	
	status2 = thFftwKernelUpdatePlan(kern, margin, prow, pcol);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not update plan to (prow = %d, pcol = %d)", name, prow, pcol);
		if (status != NULL) *status = status2;
	}
	#endif

	int i, j;

	#if 1 /* used to accomodate convolution with PSF wings smaller than the maximum size - jan 23, 2018 */
	int krow, kcol;
	krow = nrow_eff;
	kcol = ncol_eff;
	for (i = - (krow / 2); i < (krow + 1) / 2; i++) {
		int i_k = (i + prow)%prow;
		int i_r = i + nrow / 2;
		FFTW_COMPLEX *kern_row_i = fftw_kern[i_k];
		THPIX *reg_row_i = reg->rows_thpix[i_r];
		for (j = - (kcol / 2); j < (kcol + 1) / 2; j++) {
			int j_k = (j + pcol)%pcol;
			int j_r = j + ncol / 2;
			kern_row_i[j_k].re = (FFTWFL) reg_row_i[j_r];
		}
	}
	#if DEEP_DEBUG_PSF
	printf("%s: prow = %d, nrow = %d, nrow_eff = %d, krow = %d \n", name, prow, nrow, nrow_eff, krow);
	printf("%s: pcol = %d, ncol = %d, ncol_eff = %d, kcol = %d \n", name, pcol, ncol, ncol_eff, kcol);
	printf("%s: margin = %d, psf_size = %d \n", name, margin, psf_size);
	#endif

	#else
	for (i = 0; i < nrow; i++) {
		int i_k = (i - nrow / 2 + prow)%prow;
		FFTW_COMPLEX *kern_row_i = fftw_kern[i_k];
		THPIX *reg_row_i = reg->rows_thpix[i];
		for (j = 0; j < ncol; j++) {
			kern_row_i[(j - ncol / 2 + pcol)%pcol].re = reg_row_i[j];
		}
	}
	#endif
	/* creating the hole */
	int hrow, hcol;
	hrow = kern->hrow;
	hcol = kern->hcol;
	for (i = - (hrow / 2); i < (hrow + 1) / 2; i++) {
		int i_k = (i + prow)%prow;
		FFTW_COMPLEX *kern_row_i = fftw_kern[i_k];
		for (j = - (hcol / 2); j < (hcol + 1) / 2; j++) {
			kern_row_i[(j + pcol)%pcol].re = (FFTWFL) 0.0;
		}
	}
	int nbadpixel = 0;
	status2 = thFftwArrayCountBadPixels(fftw_kern, prow, pcol, &nbadpixel);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not count bad pixels in (fftw_complex) array of size (%d, %d)", name, prow, pcol);
		if (status != NULL) *status = status2;
	}
	if (nbadpixel != 0) {
		thError("%s: ERROR - (%d) bad pixels found in (fftw_complex) array made of (psf) image (%d, %d)", name, nbadpixel, prow, pcol);
		if (status != NULL) *status = status2;
		return(NULL);
	}
	/* note: in different version of fftw, planning is dependent on the array which necessitates the planning to be done prior to setting up the image when FFTW_MEASURE is used as a flag */
	#if 0
	RET_CODE status2;	
	status2 = thFftwKernelUpdatePlan(kern, margin, prow, pcol);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not update plan to (prow = %d, pcol = %d)", name, prow, pcol);
		if (status != NULL) *status = status2;
	}
	#endif
	#if 0
	if (kern->planrow != prow || kern->plancol != pcol) {
		#if NO_FFTW_DURING_TEST
		#else
		if (kern->planrow != 0 && kern->plancol != 0) {
			fftwnd_destroy_plan(kern->plan_fwd);
			fftwnd_destroy_plan(kern->plan_bck);
		}		
		kern->plan_fwd = fftw2d_create_plan(prow, pcol, FFTW_FORWARD, FFTW_IN_PLACE | FFTW_USE_WISDOM | FFTW_ESTIMATE);
		kern->plan_bck = fftw2d_create_plan(prow, pcol, FFTW_BACKWARD, FFTW_IN_PLACE | FFTW_USE_WISDOM | FFTW_ESTIMATE);
		#endif
		kern->planrow = prow;
		kern->plancol = pcol;	
	}
	#endif

	#if NO_FFTW_DURING_TEST
	#else
	fftwnd(kern->plan_fwd, 1, &fftw_kern[0][0], 1, 0, NULL, 1, 0);
	#endif
	nbadpixel = 0;
	status2 = thFftwArrayCountBadPixels(fftw_kern, prow, pcol, &nbadpixel);
	if (status2 != SH_SUCCESS) {
		thError("%s: ERROR - could not count bad pixels in (fftw_complex) array of size (%d, %d)", name, prow, pcol);
		if (status != NULL) *status = status2;
		return(NULL);
	}
	if (nbadpixel != 0) {
		thError("%s: ERROR - (%d) bad pixels found in (fftw_complex) array after (fftw, size = %d, %d)", name, nbadpixel, prow, pcol);
		if (status != NULL) *status = SH_GENERIC_ERROR;
	} 	
	kern->fftw_kern = fftw_kern;
	kern->prow = prow;
	kern->pcol = pcol;
	kern->aflag = BUILT;
}

if (status != NULL) *status = SH_SUCCESS;
return(fftw_kern);
}


FFTW_COMPLEX fftw_multiply(FFTW_COMPLEX a, FFTW_COMPLEX b) {
FFTW_COMPLEX c;
c.re = a.re * b.re - a.im * b.im;
c.im = a.re * b.im + a.im * b.re;
return(c);
}

RET_CODE thFftwKernelUpdatePlan(FFTW_KERNEL *kern, int margin, int prow, int pcol) {
char *name = "thFftwKernelUpdatePlan";
if (kern == NULL) {
	thError("%s: ERROR - null (fftw_kernel)", name);
	return(SH_SUCCESS);
}
if (prow <= 0 || pcol <= 0) {
	thError("%s: ERROR - unacceptable pad size (prow, pcol) = (%d, %d)", name, prow, pcol);
	return(SH_GENERIC_ERROR);
}
if (margin < 0) {
	thError("%s: ERROR - value of (margin = %d < 0) not supported", name, margin);
	return(SH_GENERIC_ERROR);
}
int psf_size = 2 * margin + 1;
if (prow < psf_size || pcol < psf_size) {
	thError("%s: ERROR - padded size (prow = %d, pcol = %d) too small for psf (margin = %d, size = %d)", name, prow, pcol, margin, psf_size);
	return(SH_GENERIC_ERROR);
}

if (prow == kern->planrow && pcol == kern->plancol) {
	return(SH_SUCCESS);
}
#if NO_FFTW_DURING_TEST
#else
if (kern->planrow != 0 && kern->plancol != 0) {
	fftwnd_destroy_plan(kern->plan_fwd);
	fftwnd_destroy_plan(kern->plan_bck);
}	
kern->plan_fwd = fftw2d_create_plan(prow, pcol, FFTW_FORWARD, FFTW_IN_PLACE | FFTW_USE_WISDOM | FFTW_MEASURE);
kern->plan_bck = fftw2d_create_plan(prow, pcol, FFTW_BACKWARD, FFTW_IN_PLACE | FFTW_USE_WISDOM | FFTW_MEASURE);
if (kern->plan_fwd == NULL || kern->plan_bck == NULL) {
	thError("%s: ERROR - could not create forward / backward (fftw_plan) for (prow, pcol) = (%d, %d)", name, prow, pcol);	
	return(SH_GENERIC_ERROR);
}
#endif
kern->planrow = prow;
kern->plancol = pcol;
return(SH_SUCCESS);
}

RET_CODE thFftwComplex2dMultiply(FFTW_COMPLEX **in1, FFTW_COMPLEX **in2, FFTW_COMPLEX **out, int prow, int pcol) {
char *name = "thFftwComplex2dMultiply";
if (in1 == NULL || in2 == NULL || out == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
int i;
FFTW_COMPLEX *outrow, *inrow1, *inrow2;
outrow = out[0];
inrow1 = in1[0];
inrow2 = in2[0];
for (i = 0; i < prow * pcol; i++) {
	outrow[i] = fftw_multiply(inrow1[i], inrow2[i]);
}
return(SH_SUCCESS);
}

FFTW_COMPLEX **thFftwComplex2dNew(int nrow, int ncol) {
char *name = "thFftwComplex2dNew";
if (nrow <= 0 || ncol <= 0) {
	thError("%s: ERROR - cannot create a 2d array of size (%d, %d)", name, nrow, ncol);
	return(NULL);
}
FFTW_COMPLEX **arr;
arr = thCalloc(nrow, sizeof(FFTW_COMPLEX *));
arr[0] = thCalloc(nrow * ncol, sizeof(FFTW_COMPLEX));
int i;
for (i = 1; i < nrow; i++) {
	arr[i] = arr[0] + i * ncol;
}
return(arr);
}

void thFftwComplex2dDel(FFTW_COMPLEX **arr) {
if (arr == NULL) {
	return;
}
thFree(arr[0]);
thFree(arr);
return;
}

RET_CODE shRegFromFftwComplex(FFTW_COMPLEX **carr, int prow, int pcol, FFTWFL scale, REGION *out) {
char *name = "shRegFromFftwComplex";
if (carr == NULL || out == NULL) {
	thError("%s: ERROR - null input or output array", name);
	return(SH_GENERIC_ERROR);
}
if (prow <= 0 || pcol <= 0) {
	thError("%s: ERROR - unsupported values for (prow, pcol) = (%d, %d)", name, prow, pcol);
	return(SH_GENERIC_ERROR);
}
int nrow, ncol;
nrow = out->nrow;
ncol = out->ncol;
if (nrow == 0 || ncol == 0) {
	thError("%s: ERROR - (out) region has no pixels, (nrow, ncol) = (%d, %d)", name, nrow, ncol);
	return(SH_GENERIC_ERROR);
}
if (nrow > prow || ncol > pcol) {
	thError("%s: WARNING - expected (nrow, ncol) <= (prow, pcol) receieved (%d, %d; %d, %d)", name, nrow, ncol, prow, pcol);
}
int i, j;
for (i = 0; i < MIN(prow, nrow); i++) {
	FFTW_COMPLEX *crow_i;
	THPIX *row_i;
	crow_i = carr[i];
	row_i = out->rows_thpix[i];
	int mcol = MIN(pcol, ncol);
	for (j = 0; j + 3 < mcol; j=j+4) {
		row_i[j] = (THPIX) (scale * crow_i[j].re);
		row_i[j + 1] = (THPIX) (scale * crow_i[j + 1].re);
		row_i[j + 2] = (THPIX) (scale * crow_i[j + 2].re);
		row_i[j + 3] = (THPIX) (scale * crow_i[j + 3].re);
	}
	for (j = (mcol / 4) * 4; j < mcol; j++) {
		row_i[j] = (THPIX) (scale * crow_i[j].re);
	}
}

return(SH_SUCCESS);
}

RET_CODE thFftwGetPadSize(REGION *reg, FFTW_KERNEL *kern, int margin, int *prow, int *pcol) {
char *name = "thFftwGetPadSize";
if (reg == NULL || kern == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (kern->reg == NULL) {
	thError("%s: ERROR - null (kern->reg): improperly allocated (fftw_kernel) input", name);
	return(SH_GENERIC_ERROR);
}
if (prow == NULL && pcol == NULL) {
	thError("%s: WARNING - null output placeholder", name);
	return(SH_SUCCESS);
}
int nrow, ncol, krow, kcol;
krow = kern->reg->nrow;
kcol = kern->reg->ncol;
nrow = reg->nrow;
ncol = reg->ncol;
if (nrow == 0 || ncol == 0) {
	thError("%s: ERROR - empty region (nrow, ncol) = (%d, %d)", name, nrow, ncol);
	return(SH_GENERIC_ERROR);
}
if (krow == 0 || kcol == 0) {
	thError("%s: ERROR - empty kernel (krow, kcol) = (%d, %d)", name, krow, kcol);
	return(SH_GENERIC_ERROR);
}
if (margin < 0) {
	thError("%s: ERROR - (margin = %d < 0) is not supported", name, margin);
	return(SH_GENERIC_ERROR);
}

int prow_t;
int pcol_t;

prow_t = find_best_fftw_size(nrow); /* note that the image is already assumed to be padded prior to a call to this function */
pcol_t = find_best_fftw_size(ncol);

if (prow != NULL) *prow = prow_t;
if (pcol != NULL) *pcol = pcol_t;

int psf_size = 2 * margin + 1;
if (prow_t < psf_size || pcol_t < psf_size) {
	thError("%s: ERROR - pad size (prow = %d, pcol = %d) while (psf_size = %d, margin = %d)", name, prow_t, pcol_t, psf_size, margin);
	return(SH_GENERIC_ERROR);
}
#if DEBUG_PSF_VARIABLE_SIZE
printf("%s: input (%d, %d), padded (%d, %d) \n", name, nrow, ncol, prow_t, pcol_t);
#endif
return(SH_SUCCESS);
}

int find_best_fftw_size(int a) {

shAssert(a > 0);
int d1[4] = {2, 3, 5, 7};
int d2[2] = {11, 13};

int n1 =  4; /* size of array d1 */
int n2 = 0; /* size of array d2 */
int m2 = 1; /* sum of exponents of {d2} cannot exceed this amount -- there is no limit for exponents of {d1}*/

int isgood = 0;
int b = a;
while (!isgood) {
	int c = b;
	int i;
	for (i = 0; i < n1; i++) {
		if (c == 1) break;
		int d = d1[i];
		while (c%d == 0) {
			c = c / d;
		}
	}
	int p2 = 0;
	for (i = 0; i < n2; i++) {	
		if (p2 >= m2 || c == 1); break;
		int d = d2[i];
		while (c%d == 0) {
			c = c / d;
			p2++;
			if (p2 >= m2); break;
		}
	}
	if (c == 1) {
		isgood = 1;
	} else {
		b++;
	}
}

shAssert(b > 0);
return(b);
}

RET_CODE thPsfInfoUpdateWpsf(PSF_CONVOLUTION_INFO *psf_info) {
char *name = "thPsfInfoUpdateWpsf";
if (psf_info == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = thPsfInfoUpdateCounts(psf_info);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update counts in (psf_info)", name);
	return(status);
}
status = thPsfInfoUpdatePsfA(psf_info);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not update (psf_a) values in (psf_info)", name);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE thPsfInfoUpdateCounts(PSF_CONVOLUTION_INFO *psf_info) {
char *name = "thPsfInfoUpdateCounts";
if (psf_info == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

PSF_CONVOLUTION_TYPE conv_type;
conv_type = psf_info->conv_type;

RET_CODE status;

WPSF *wpsf;
wpsf = psf_info->wpsf;

int i, psf_n;
status = thPsfConvInfoGetNComponent(psf_info, &psf_n);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not obtain (n_component) for (psf_info)", name);
	return(status);
}

for (i = 0; i < psf_n; i++) {
	PSF_SOURCE source; CONV_METHOD conv_method; int hrow, hcol;
	status = thConvTypeGetComponent(conv_type, wpsf, i, &source, &conv_method, &hrow, &hcol);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get characteristics of component (%d) for (psf_info)", name, i);
		return(status);
	}
	status = thPsfInfoUpdateFftwKernel(psf_info, i, source, conv_method, hrow, hcol);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update componet (%d) of (wpsf) in (psf_info)", name);
		return(status);
	}
}

return(SH_SUCCESS);
}

RET_CODE thPsfInfoUpdatePsfA(PSF_CONVOLUTION_INFO *psf_info) {
char *name = "thPsfInfoUpdatePsfA";
if (psf_info == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
int psf_n;
status = thPsfConvInfoGetNComponent(psf_info, &psf_n);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get number of components for (psf_info)", name);
	return(status);
}
PSF_CONVOLUTION_TYPE conv_type;
status = thPsfConvInfoGetPsfConvType(psf_info, &conv_type);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (conv_type) for (psf_info)", name);
	return(status);
}

if (conv_type == NO_PSF_CONVOLUTION) {
	if (psf_n > 0) {
		thError("%s: WARNING - (psf_n = %d) discovered for (conv_type) of 'NO_PSF_CONVOLUTION'", name, psf_n);
	}
	return(SH_SUCCESS);
} else if (psf_n < 0 && psf_n != DEFAULT_NCOMPONENT) {
	init_static_vars ();
	thError("%s: ERROR - (psf_n = %d) unacceptable for (conv_type) '%s'", name, psf_n, schema_conv_type->elems[conv_type].name);
	return(SH_GENERIC_ERROR);
} else if (psf_n == 0) {
	init_static_vars ();
	thError("%s: WARNING - (psf_n = %d) found for (conv_type) '%s': no (psf_a) seems available", name, psf_n, schema_conv_type->elems[conv_type].name);
	return(SH_SUCCESS);
} else if (conv_type == CENTRAL_PSF || conv_type == RADIAL_PSF) {
	THPIX counts;
	FFTW_KERNEL *fftw_kern;
	status = thPsfInfoGetFftwKernel(psf_info, 0, &fftw_kern);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get ffw_kernel for component '0'", name);
		return(status);
	}
	status = thFftwKernelGetCounts(fftw_kern, &counts, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get counts from (fftw_kent)", name);
		return(status);
	}
	if (counts <= (THPIX) 0.0) {
	
	#if PSF_NEGATIVE_COUNTS_ISSUE_ERROR
	thError("%s: ERROR - unacceptable (counts = %g) discoevered in (fftw_kern) for (conv_type = '%s')", name, (float) counts, shEnumNameGetFromValue("PSF_CONVOLUTION_TYPE", conv_type));
		return(SH_GENERIC_ERROR);
	#else
	thError("%s: WARNING - unacceptable (counts = %g) discoevered in (fftw_kern) for (conv_type = '%s')", name, (float) counts, shEnumNameGetFromValue("PSF_CONVOLUTION_TYPE", conv_type));
	#endif

	}

	THPIX psf_a;
	psf_a = 1.0 / counts;
	status = thFftwKernelPutPsfA(fftw_kern, psf_a);
	return(SH_SUCCESS);
} else if (conv_type == CENTRAL_AND_WINGS_PSF || conv_type == STAR_CENTRAL_AND_WINGS) {
	if (psf_n != 2) {
		init_static_vars ();
		thError("%s: ERROR - (psf_n = %d) unexpected for (conv_type = '%s')", name, psf_n, schema_conv_type->elems[conv_type].name);
		return(SH_GENERIC_ERROR);
	}
	/* condition of continuity between PSF components on the edge */
	FFTW_KERNEL *fftw_kernel_inner, *fftw_kernel_outer;
	status = thPsfInfoGetFftwKernel(psf_info, INNER_PSF, &fftw_kernel_inner);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get ffw_kernel for 'INNER_PSF'", name);
		return(status);
	}
	status = thPsfInfoGetFftwKernel(psf_info, OUTER_PSF, &fftw_kernel_outer);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get ffw_kernel for 'OUTER_PSF'", name);
		return(status);
	}
	
	REGION *reg_inner = NULL, *reg_outer = NULL;
	WMASK *w = NULL;
	reg_inner = fftw_kernel_inner->hreg; /* hole region in the wing */
	reg_outer = fftw_kernel_outer->reg;
	status = thPsfInfoGetBorderMask(psf_info, &w);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get the border mask", name);
		return(status);
	}
	#if DEBUG_PSF
	tell_me_about_reg("matrix one", w->W);
	tell_me_about_reg("inner PSF", reg_inner);
	tell_me_about_reg("outer PSF", reg_outer);
	#endif
	
	if (reg_inner == NULL) {
		thError("%s: ERROR - null (reg_inner) found in (psf_info)", name);
		return(SH_GENERIC_ERROR);
	} else if (reg_outer == NULL) {
		thError("%s: ERROR - null (reg_outer) found in (psf_info)", name);
		return(SH_GENERIC_ERROR);
	}

	/* move all regions to the center */
	int row0_inner, row0_outer, row0_w, col0_inner, col0_outer, col0_w;
	status = centralize_region(reg_inner, &row0_inner, &col0_inner);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not centralize inner psf region", name);
		return(status);
	}
	status = centralize_region(reg_outer, &row0_outer, &col0_outer);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not centralize outer psf region", name);
		return(status);
	}
	status = centralize_region(w->W, &row0_w, &col0_w);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not centralize psf border mask", name);
		return(status);
	}

	/* doing the inner products */
	MLEFL in_in, out_out, in_out, in_one, out_one, one_one;
	PSFDOTPRODUCT *prod = NULL;
	
	prod  = thPsfDotProductNew();	
	status = thPsfGetDotProduct(reg_inner, reg_outer, w, prod);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not calculate PSF dot products", name);
		return(status);
	}
	status = thPsfDotProductGet(prod, &in_in, &out_out, &in_out, &in_one, &out_one, &one_one);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get dot products from (psf_dot_products)", name);
		return(status);
	}
	/* 
	KL-PSF: g
	Wing PSF: f

	a1 = < g | Wb | g > (inner product done over Border region)
        a2 = < 1 | Wb | g > 
	a3 = < g | 1 >      (inner product done over KL region) 
	a  = < f | Wb | g>

	b1 = < 1 | Wb | g >
        b2 = < 1 | Wb | 1 > 
	beta =    < 1 | 1 > 
	b  = < f | Wb | g >   
	
        c1 = < g | 1 >  (inner product done over KL region)
	c2 = < 1 | 1 >  (inner product done over KL region)
	c =  < f | 1 >  (inner product done over KL region) fraction of light in KL

	d =  < f | Wing | 1 > (inner product done over wing region, anything outside of KL region )

 	I)  a1 x1 + a2 x2 = a
        II) b1 x1 + b2 x2 = b

	gamma = 1 / (a1 b2 - a2 b1)
	x1    = gamma * (a * b2 - a2 * b)
        x2    = gamma * (a1 * b - a * b1)

	Normalization:
	
	norm = x1 a3 + x2 b3 + d
	y1 = x1 / norm
	y2 = x2 / norm
	y3 = 1  / norm
    
	now multiply and add to get the normalized PSF 

	KL   -----> y1 KL + y2
	WING -----> y3 WING

	the following was found to be wrong on Sep 20, 2014

	I)   a1 x1 + a2 x2 + a3 x3       = a
	II)  b1 x1 + b2 x2 + b3 x3       = b
	III) c1 x1 + c2 x2               = c

	x1 = amplitude of KL component
	x2 = background correct to KL component
	x3 = lagrange multiplier

	d1 = (b3 a1 - b1 a3)
	d2 = (b3 a2 - b2 a3)
	d  = (b3 a  - a3 b )
	
	c1 x1 + c2 x2 = c
	d1 x1 + d2 x2 = d
	
	gamma = 1 / (d1 c2 - d2 c1)
	x1 = gamma * (d c2 - d2 c)
	x2 = gamma * (d1 c - d c2)

	 

	gamma = 1 / (a1 c2 - a2 c 1)
	A = beta * gamma * c1 / a3
	B = gamma * (-beta * a1 * c / a3 + c1 * (b2 + beta * a / a3) - b1 * c2)
	C = b - a1 * c (b2 + beta * a / a3) gamma + a2 * c * b1 * gamma

	Solve:
	A y3 ^ 2 + B y3 + C = 0

	D = B ^ 2 - 4 A C
	y3 = (- B +/- D ^ 0.5) / 2 / A

	x1 = gamma  (c2 y3 - a2 c)
	x2 = -gamma (c1 y3 - a1 c)

	*/

	THPIX counts_inner, counts_outer, hcount_inner;	
	status = thFftwKernelGetCounts(fftw_kernel_inner, &counts_inner, &hcount_inner); 
	/* for this solution counts_inner should be the total count of WING psf 
  	rather including the central region */
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get the counts for 'INNER_PSF'", name);
		return(status);
	}
	status = thFftwKernelGetCounts(fftw_kernel_outer, &counts_outer, NULL);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get the counts for 'OUTER_PSF'", name);
		return(status);
	}

	#if DEBUG_PSF	
	printf("%s: inner counts = %g, hole count = %g \n", name, (float) counts_inner, hcount_inner);
	printf("%s: outer counts = %g \n", name, (float) counts_outer);
	#endif

	int nrow_outer, ncol_outer;
	nrow_outer = reg_outer->nrow;
	ncol_outer = reg_outer->ncol;

	MLEFL a1, a2, a3, a, b1, b2, b3, b, e1, e2;
	a1 = (MLEFL) out_out;
	a2 = (MLEFL) out_one;
	a3 = (MLEFL) counts_outer;
	a  = (MLEFL) in_out;
	
	b1 = (MLEFL) a2;
	b2 = (MLEFL) one_one;
	b3 = (MLEFL) (nrow_outer * ncol_outer);
	b  = (MLEFL) in_one;

	e1 = (MLEFL) counts_inner - (MLEFL) hcount_inner; /* double check for precision */
	e2 = (MLEFL) counts_inner;

	MLEFL gamma, norm, x1, x2, y1, y2, y3, z1, z2;

#if PSF_GLUE_METHOD_1

/* suggested after Sep 20, 2014 */

	gamma = ((MLEFL) 1.0) / (a1 * b2 - a2 * b1);
	x1 = gamma * (a * b2 - a2 * b);
        x2 = gamma * (a1 * b - a * b1);

	/* normalization */
	norm = x1 * a3 + x2 * b3 + e1;
	y1 = x1 / norm;
	y2 = x2 / x1;
	y3 = ((MLEFL) 1.0) / norm;
	
	z1 = e1 / norm; /* lost light */    
	z2 = e1 / e2;  /* halo light fraction in the wing info in psField */
	
	if (psf_warning_count < MAX_PSF_WARNING_COUNT) {
		if (z1 < (MLEFL) 0.0 || z2 < (MLEFL) 0.0) {
			if (z1 < (MLEFL) 0.0) thError("%s: WARNING - in PSF_GLUE_METHOD_1: (lost light = %g) found to be negative", name, (float) z1);
			if (z2 < (MLEFL) 0.0) thError("%s: WARNING - in PSF_GLUE_METHOD_1 (halo light = %g) found to be negative", name, (float) z2);
			thError("%s: WARNING - in PSF_GLUE-METHOD_1: sign error - norm = %g, e1 = %g, counts_inner = %g, hcount_inner = %g", name, (float) norm, (float) e1, (float) counts_inner, (float) hcount_inner);
		}
	psf_warning_count++;
	}
#else

/* this part was found to be problematic on Sep 20, 2014  */

	MLEFL c1, c2, c, d1, d2, d;
	c1 = (MLEFL) counts_outer;
	c2 = b3;
	c  = (MLEFL) hcount_inner;

	d1 = b3 * a1 - b1 * a3;
	d2 = b3 * a2 - b2 * a3;
	d  = b3 * a  - a3 * b;

	gamma = ((MLEFL) 1.0) / (d1 * c2 - d2 * c1);
	x1 = gamma * (d * c2 - d2 * c);
	x2 = gamma * (d1 * c - d * c1);


	y1 = x1 / counts_inner; 
	y2 = (THPIX) (x2 / x1);
	y3 = ((THPIX) 1.0) / counts_inner;

	norm = x1 * a3 + x2 * b3 + e1;
	z1 = e1 / norm; /* lost light */
	z2 = e1 / e2; /* halo light fraction in the wing info in psField */

	if (psf_warning_count < MAX_PSF_WARNING_COUNT) {
		if (z1 < (MLEFL) 0.0 || z2 < (MLEFL) 0.0) {
			if (z1 < (MLEFL) 0.0) thError("%s: WARNING - in PSF_GLUE_METHOD_0: (lost light = %g) found to be negative", name, (float) z1);
			if (z2 < (MLEFL) 0.0) thError("%s: WARNING - in PSF_GLUE_METHOD_0 (halo light = %g) found to be negative", name, (float) z2);
			thError("%s: WARNING - in PSF_GLUE-METHOD_0: sign error - norm = %g, e1 = %g, counts_inner = %g, hcount_inner = %g", name, (float) norm, (float) e1, (float) counts_inner, (float) hcount_inner);
		}
	psf_warning_count++;
	}
#endif

	#if DEBUG_PSF
	printf("%s: x1  = %g, x2  = %g \n", name, (float) x1, (float) x2);
	#endif

	THPIX bkg_outer, alpha_inner, alpha_outer;
	alpha_outer = (THPIX) y1;
	bkg_outer = (THPIX) y2;
	alpha_inner = (THPIX) y3;

	THPIX lost_light, halo_frac;
	lost_light = (THPIX) z1;
	halo_frac  = (THPIX) z2; 

	/* see if background correction is too negative */	
	THPIX psf_correction = alpha_outer * bkg_outer * nrow_outer * ncol_outer;
	if (psf_warning_count < MAX_PSF_WARNING_COUNT) {
		if (psf_correction < - (THPIX) PSF_CORRECTION_TOLERANCE ) {
			thError("%s: WARNING - background = %7.2g, PSF-correction ~ %9.4g \n", name, (float) bkg_outer, (float) psf_correction);
		}
		if (fabs(psf_correction) > (THPIX) PSF_RELATIVE_CORRECTION_TOLERANCE * fabs(lost_light) || fabs(psf_correction) > (THPIX) PSF_RELATIVE_CORRECTION_TOLERANCE * fabs(halo_frac)) {
			thError("%s: WARNING - background = %7.2g, PSF-correction ~ %9.4g, lost-light ~ %9.4g, halo-frac ~ %9.4g \n", name, 
			(float) bkg_outer, (float) psf_correction, (float) lost_light, (float) halo_frac);
		}
		if (alpha_inner < (THPIX) 0.0) {
			thError("%s: WARNING - alpha_inner = %7.2g \n", name, (float) alpha_inner);
		}
		if (alpha_outer < (THPIX) 0.0) {
			thError("%s: WARNING - alpha_outer = %7.2g \n", name, (float) alpha_outer);
		}
		psf_warning_count++;
	}
	/* now updating the shape of each component of the PSF */
	status = thFftwKernelAddConst(fftw_kernel_outer, bkg_outer); /* note that the background is added before amplitude correction */	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not shift inner component of the psf by background amount", name);
		return(status);
	}
	status = thFftwKernelPutPsfA(fftw_kernel_inner, alpha_inner);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put amplitude for 'INNER_PSF'", name);
		return(status);
	}
	status = thFftwKernelPutPsfA(fftw_kernel_outer, alpha_outer);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put amplitude for 'OUTER_PSF'", name);
		return(status);
	}
	/* we might need to redo the counts at this point */
	#if DEBUG_PSF
	printf("%s: bkg_outer   = %g \n", name, (float) bkg_outer);
	printf("%s: alpha_inner = %g \n", name, (float) alpha_inner);
	printf("%s: alpha_outer = %g \n", name, (float) alpha_outer);
	#endif

#if PSF_REPLACE_PIX
	
	status = thPsfReplaceKLPixels(reg_outer, reg_inner, (THPIX) PSF_REPLACE_RAD, alpha_outer / alpha_inner, bkg_outer);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not replace boundary KL-pixels (rad > %d pix) with wing pixels", name,  (int) PSF_REPLACE_RAD);
		return(status);
	}	
	/* normalize once more after replacing pixels */	
	/* updated the corrected PSF's */
	status = thPsfInfoUpdateCounts(psf_info);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update the counts in (psf_info)", name);
		return(status);
	}
	/* wing portion does not need to be updated 
	status = thFftwKernelGetCounts(fftw_kernel_inner, &counts_inner, &hcount_inner); 
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get counts from (fftw_kernel) of (PSF_INNER)", name);
		return(status);
	} 
	*/
	status = thFftwKernelGetCounts(fftw_kernel_outer, &counts_outer, NULL);	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get counts from (fftw_kernel) of (PSF_OUTER)", name);
		return(status);
	}
	norm = alpha_outer * counts_outer + alpha_inner * (counts_inner - hcount_inner);
	/* update lost light amount */
	lost_light = (counts_inner - hcount_inner) * alpha_inner / norm;
	/* update alpha_inner and alpha_outer */
	alpha_inner = alpha_inner / norm;
	alpha_outer = alpha_outer / norm;
	status = thFftwKernelPutPsfA(fftw_kernel_inner, alpha_inner);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put amplitude for 'INNER_PSF' after pixel replacement", name);
		return(status);
	}
	status = thFftwKernelPutPsfA(fftw_kernel_outer, alpha_outer);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not put amplitude for 'OUTER_PSF' after pixel replacement", name);
		return(status);
	}

#endif

	thPsfDotProductDel(prod);
	/* this part is meant to be used by psf report and not in any calculations - added august 2014 */
	PSF_CONVOLUTION_REPORT *report = NULL;
	status = thPsfInfoGetPsfConvReport(psf_info, &report);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not get (psf_conv_report) from (psf_conv_info)", name);
		return(status);
	}
	status = thPsfConvReportUpdate(report, &alpha_inner, &alpha_outer, &bkg_outer, &lost_light, &halo_frac);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not update (report iterms ) in (psf_conv_report)", name);
		return(status);
	}
	/* returning regions to the previous shape */
	decentralize_region(reg_inner, row0_inner, col0_inner);
	decentralize_region(reg_outer, row0_outer, col0_outer);
	decentralize_region(w->W, row0_w, col0_w);

	/* the right background and normalizer chosen */
	return(SH_SUCCESS);
} else {
	init_static_vars ();
	thError("%s: ERROR - (conv_type = '%s') not supported", name, 
		schema_conv_type->elems[conv_type].name);
	return(SH_GENERIC_ERROR);
}

} 
	

RET_CODE thPsfInfoUpdateFftwKernel(PSF_CONVOLUTION_INFO *psf_info, PSF_COMPONENT psf_c, PSF_SOURCE source, CONV_METHOD conv_method, int hrow, int hcol) {
char *name = "thPsfInfoUpdateFftwKernel";
if (psf_info == NULL) {
	thError("%s: ERROR - null (wpsf) as input", name);
	return(SH_GENERIC_ERROR);
}
if (psf_c >= N_PSF_COMPONENT) {
	thError("%s: ERROR - (psf_c = %d) index out of range", name, psf_c);
	return(SH_GENERIC_ERROR);
}
if (hrow < 0 || hcol < 0) {
	thError("%s: WARNING - negative central hole width (%d, %d) converted to defult width of (0)", name, hrow, hcol);
	hrow = 0;
	hcol = 0;
}

RET_CODE status;

THPIX rowc, colc;
status = thPsfInfoGetCenter(psf_info, &rowc, &colc);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get center location for (psf_info)", name);
	return(status);
}
FFTW_KERNEL *fftw_kern = NULL;
status = thPsfInfoGetFftwKernel(psf_info, psf_c, &fftw_kern);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (fftw_kernel) for component (%d) of (wpsf)", name, psf_c);
	return(status);
} else if (fftw_kern == NULL) {
	thError("%s: ERROR - imporperly allocated (wpsf) - null (fftw_kernel) for component (%d)", name, psf_c);
	return(SH_GENERIC_ERROR);
}
PSFMODEL *psf_model;
status = thPsfInfoGetPsfModel(psf_info, &psf_model);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (psf_model) in (psf_info)", name);
	return(status);
} else if (psf_model == NULL) {
	thError("%s: ERROR - null (psf_model): improperly allocated (psf_info)", name);
	return(SH_GENERIC_ERROR);
}

if (source == KL) {
	if (fftw_kern->source != KL) {
		if (fftw_kern->reg != NULL) {
			if (fftw_kern->source != WING) {
				thError("%s: WARNING - deleting (dirreg) in (fftw_ken) with (source) of '%s'", name, shEnumNameGetFromValue("PSF_SOURCE", fftw_kern->source));
			}
			shRegDel(fftw_kern->reg);
			fftw_kern->reg = NULL;
		}
		fftw_kern->aflag = MODIFIED;
	}
	if (rowc != fftw_kern->rowc || colc != fftw_kern->colc || 
		fftw_kern->source != KL) {
		status =  thPsfKLReconstruct(psf_model->basis, fftw_kern, rowc, colc);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not reconstruct KL-psf at (rowc, colc) = (%g, %g)", name, (float) rowc, (float) colc);
			return(status);
		}
		fftw_kern->source = source;
		fftw_kern->rowc = rowc;
		fftw_kern->colc = colc;
		fftw_kern->aflag = MODIFIED;
	}
	shAssert(hrow == 0 && hcol == 0);
	if (fftw_kern->hrow != hrow || fftw_kern->hcol != hcol || 
			fftw_kern->conv_method != conv_method) {
		fftw_kern->aflag = MODIFIED;
	}
	if (fftw_kern->aflag == MODIFIED) {
		int nrow, ncol;	
		REGION *reg = fftw_kern->reg;
		nrow = reg->nrow;
		ncol = reg->ncol;
		REGION *hreg;
		hreg = shSubRegNew("hole in KL-psf", reg, 
			hrow, hcol, (nrow + 1) / 2 - (hrow + 1) / 2, (ncol + 1) / 2 - (hcol + 1) / 2, NO_FLAGS);
		fftw_kern->counts = thRegSum(reg, 0, nrow, 0, ncol, NULL, NULL, '\0');
		fftw_kern->hcounts = thRegSum(hreg, 0, hrow, 0, hcol, NULL, NULL, '\0');
		shRegDel(hreg);
	}

	fftw_kern->hrow = hrow;	
	fftw_kern->hcol = hcol;
	fftw_kern->conv_method = conv_method;
	return(SH_SUCCESS);
} else if (source == WING) {
	
	REGION *sreg = NULL;
	PSFMODEL *psf_model;
	if (psf_info->wpsf == NULL) {
		thError("%s: ERROR - null (wpsf): improperly allocated (psf_info)", name);
		return(SH_GENERIC_ERROR);
	}
	psf_model = psf_info->wpsf->psf_model;
	sreg = psf_model->wing->reg;
	/* rowc and colc do not matter for WING section */
	if (fftw_kern->source != WING) {
		if (fftw_kern->reg != NULL) {
			if (fftw_kern->source != KL) {
				thError("%s: WARNING - deleting (dirreg) in (fftw_ken) with (source) of '%s'", name, shEnumNameGetFromValue("PSF_SOURCE", fftw_kern->source));
			}
			#if VERBOSE_PSF
			printf("%s: deleting (reg) defined of type '%s' \n", name, shEnumNameGetFromValue("PSF_SOURCE", fftw_kern->source));
			#endif
			shRegDel(fftw_kern->reg);
			fftw_kern->reg = NULL;
		}	
		fftw_kern->reg = shRegNew("copied version of wing psf", sreg->nrow, sreg->ncol, TYPE_THPIX);
		shRegPixCopy(sreg, fftw_kern->reg);
		fftw_kern->aflag = MODIFIED;
	}

	if (fftw_kern->hrow != hrow || fftw_kern->hcol != hcol || 
			fftw_kern->conv_method != conv_method) {
		fftw_kern->aflag = MODIFIED;
	}

	if (fftw_kern->aflag == MODIFIED) {
		int nrow, ncol;
		REGION *reg = fftw_kern->reg;
		nrow = reg->nrow;
		ncol = reg->ncol;
		REGION *hreg;
		fftw_kern->counts = thRegSum(sreg, 0, nrow, 0, ncol, NULL, NULL, '\0');
		if (fftw_kern->hreg != NULL) {
			shRegDel(fftw_kern->hreg);
			fftw_kern->hreg = NULL;
		}
		if (hrow != 0 && hcol != 0) {
			hreg = shSubRegNew("hole in source wing-psf", sreg, hrow, hcol, (nrow + 1) / 2 - (hrow + 1) / 2, (ncol + 1) / 2 - (hcol + 1) / 2, NO_FLAGS);	
			fftw_kern->hreg = hreg; /* note that the hole inside copy is cleared */
			fftw_kern->hcounts = thRegSum(hreg, 0, hrow, 0, hcol, NULL, NULL, '\0');
		} else {
			fftw_kern->hcounts = (THPIX) 0.0;
		}
	
		if (hrow != 0 && hcol != 0) {	
			hreg = shSubRegNew("hole in copy wing-psf", reg, hrow, hcol, (nrow + 1) / 2 - (hrow + 1) / 2, (ncol + 1) / 2 - (hcol + 1) / 2, NO_FLAGS);
				shRegClear(hreg);
				shRegDel(hreg);
		}
	}
	if (hrow == 0 || hcol == 0) {
		hrow = 0;
		hcol = 0;
	}
	fftw_kern->hrow = hrow;	
	fftw_kern->hcol = hcol;
	fftw_kern->source = source;
	fftw_kern->conv_method = conv_method;
	return(SH_SUCCESS);
} else {
	init_static_vars();
	thError("%s: ERROR - source type '%s' not supported for component (%d)", name, schema_psf_source->elems[source].name, psf_c);
	return(SH_GENERIC_ERROR);
}

}		

RET_CODE thFftwKernelCleanup(FFTW_KERNEL *fftw_kern) {
char *name = "thFftwKernelCleanup";
if (fftw_kern == NULL) {
	thError("%s: WARNING - null input", name);
	return(SH_SUCCESS);
}
if (fftw_kern->planrow != 0 && fftw_kern->plancol != 0) {
	fftwnd_destroy_plan(fftw_kern->plan_fwd);
	fftwnd_destroy_plan(fftw_kern->plan_bck);
}
fftw_kern->planrow = 0;
fftw_kern->plancol = 0;
return(SH_SUCCESS);
}


RET_CODE thPsfInfoCleanup(PSF_CONVOLUTION_INFO *psf_info) {
char *name = "thPsfInfoCleanup";
if (psf_info == NULL) {
	thError("%s: WARNING - null input", name);
	return(SH_SUCCESS);
}
RET_CODE status;
int psf_n;
status = thPsfConvInfoGetNComponent(psf_info, &psf_n);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (psf_n) for (psf_info)", name);
	return(status);
}

if (psf_n < DEFAULT_NCOMPONENT) {
	thError("%s: WARNING - returned default value for (psf_n), will clean up all existing components", name);
	psf_n = N_PSF_COMPONENT;
}

FFTW_KERNEL *fftw_kern;
int i;
for (i = 0; i < psf_n; i++) {
	status = thPsfInfoGetFftwKernel(psf_info, i, &fftw_kern);
	if (status != SH_SUCCESS) {	
		thError("%s: ERROR - could not get (fftw_kern) for component (%d)", name, i);
		return(status);
	} else if (fftw_kern == NULL) {
		thError("%s: ERROR - improperly allocated (psf_info): null (fftw_kern) found in component (%d)", name, i);
		return(SH_GENERIC_ERROR);
	}
	status = thFftwKernelCleanup(fftw_kern);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not clean up (fftw_kern) for component (%d)", name, i);
		return(status);
	}
}

/* fftw_forget_wisdom();
*/
return(SH_SUCCESS);
}

RET_CODE thConvTypeGetComponent(PSF_CONVOLUTION_TYPE conv_type, WPSF *wpsf,  
		PSF_COMPONENT psf_c, PSF_SOURCE *source, CONV_METHOD *conv_method, 
		int *hrow, int *hcol) {
char *name = "thConvTypeGetComponent";
RET_CODE status;
if (conv_type == NO_PSF_CONVOLUTION) {
	thError("%s: ERROR - convolution type does not have any components", name);
	return(SH_GENERIC_ERROR);
} else if (conv_type == CENTRAL_PSF) {
	if (psf_c == 0) {
		*source = KL;
		*conv_method = FFTW_CONV; /* FFTW_CONV; */
		*hrow = 0;
		*hcol = 0;
		return(SH_SUCCESS);
	} else {
		init_static_vars();
		thError("%s: ERROR - (psf_c = %d) index not supported for (conv_type = '%s')", 
			name, psf_c, schema_conv_type->elems[conv_type].name);
		return(SH_GENERIC_ERROR);
	}  
} else if (conv_type == RADIAL_PSF) {
	if (psf_c == 0) {
		*source = WING;
		*conv_method = FFTW_CONV; /* FFTW_CONV; */
		*hrow = 0;
		*hcol = 0;
		return(SH_SUCCESS);
	} else {
		init_static_vars();
		thError("%s: ERROR - (psf_c = %d) index not supported for (conv_type = '%s')", 
			name, psf_c, schema_conv_type->elems[conv_type].name);
		return(SH_GENERIC_ERROR);
	}  
} else if (conv_type == CENTRAL_AND_WINGS_PSF) {
	if (psf_c == OUTER_PSF) {
		*source = KL;
		*conv_method = FFTW_CONV;
		*hrow = 0;
		*hcol = 0;
		return(SH_SUCCESS);
	} else if (psf_c == INNER_PSF) {
		*source = WING;
		*conv_method = FFTW_CONV;
		status = thWpsfGetSizeOfCentral(wpsf, hrow, hcol);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get the size of central hole for wing component", name);
			return(status);
		}
		return(SH_SUCCESS);
	} else {
		init_static_vars();
		thError("%s: ERROR - (psf_c = %d) index not supported for (conv_type = '%s')", 
			name, psf_c, schema_conv_type->elems[conv_type].name);
		return(SH_GENERIC_ERROR);
	}
} else if (conv_type == STAR_CENTRAL_AND_WINGS) {
	if (psf_c == OUTER_PSF) {
		*source = KL;
		*conv_method = DIRECT_CONV;
		*hrow = 0;
		*hcol = 0;
		return(SH_SUCCESS);
	} else if (psf_c == INNER_PSF) {
		*source = WING;
		*conv_method = DIRECT_CONV;
		status = thWpsfGetSizeOfCentral(wpsf, hrow, hcol);
		if (status != SH_SUCCESS) {
			thError("%s: ERROR - could not get the size of central hole for wing component", name);
			return(status);
		}
		return(SH_SUCCESS);
	} else {
		init_static_vars();
		thError("%s: ERROR - (psf_c = %d) index not supported for (conv_type = '%s')", 
			name, psf_c, schema_conv_type->elems[conv_type].name);
		return(SH_GENERIC_ERROR);
	}
} else {
	init_static_vars();
	thError("%s: ERROR - (conv_type = '%s') not supported at this point", name, schema_conv_type->elems[conv_type].name);
	return(SH_GENERIC_ERROR);
}

}

RET_CODE thWpsfGetSizeOfCentral(WPSF *wpsf, int *hrow, int *hcol) {
char *name = "thWpsfGetSizeOfCentral";
if (wpsf == NULL) {
	thError("%s: ERROR - null input (wpsf)", name);
	return(SH_GENERIC_ERROR);
}
if (hrow == NULL && hcol == NULL) {	
	thError("%s: WARNING - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
PSFMODEL *psf_model;
PSF_BASIS *basis;
if ((psf_model = wpsf->psf_model) == NULL || (basis = psf_model->basis) == NULL) {
	thError("%s: ERROR - improperly allocated (wpsf)", name);
	return(SH_GENERIC_ERROR);
}

REGION *comp;
if (basis->regs == NULL || basis->type != KL_BASIS || 
	(comp = basis->regs[-1][0][0]->reg) == NULL) {
	thError("%s: ERROR - improperly allocated (basis) in (wpsf)", name);
	return(SH_GENERIC_ERROR);
}


if (hrow != NULL) *hrow = MIN(comp->nrow, OUTER_PSF_SIZE); 
if (hcol != NULL) *hcol = MIN(comp->ncol, OUTER_PSF_SIZE);

return(SH_SUCCESS);
}

void tell_me_about_fftw_arr(char *comment, FFTW_COMPLEX *arr, int n) {
char *name = "tell_me_about_fftw_arr";
printf("%s: %s \n", name, comment);
if (arr == NULL || n <= 0) {
	thError("%s: ERROR - null array or unacceptable size", name);
	return;
} 
int j;
double r, i, m, r_max, i_max, m_max, r_min, i_min, m_min;
r_max = arr[0].re;
i_max = arr[0].im;
m_max = pow(r_max, 2) + pow(i_max, 2);
r_min = r_max;
i_min = i_max;
m_min = m_max;

for (j = 1; j < n; j++) {
	r = arr[j].re;
	i = arr[j].im;
	m = pow(r, 2) + pow(i, 2);
	r_max = MAX(r_max, r);
	i_max = MAX(i_max, i);
	m_max = MAX(m_max, m);
	r_min = MIN(r_min, r);
	i_min = MIN(i_min, i);
	m_min = MIN(m_min, m);
}

m_min = pow(m_min, 0.5);
m_max = pow(m_max, 0.5);

printf("%s: n = %d \n", name, n);
printf("%s: MIN: Re (%12.2e), Im(%12.2e), |z| (%12.2e) \n", name, r_min, i_min, m_min);
printf("%s: MAX: Re (%12.2e), Im(%12.2e), |z| (%12.2e) \n", name, r_max, i_max, m_max);

return;
}

void tell_me_about_reg(char *comment, REGION *reg) {
char *name = "tell_me_about_reg";
printf("%s: %s \n", name, comment);
if (reg == NULL || reg->nrow <= 0 || reg->ncol <= 0) {
	thError("%s: ERROR - null array or unacceptable size", name);
	return;
} 
int j, k;
THPIX i, i_max, i_min;
i_max = reg->rows_thpix[0][0];
i_min = i_max;

double total = 0.0;
for (j = 0; j < reg->nrow; j++) {
	THPIX *rows_j;
	rows_j = reg->rows_thpix[j];
	for (k = 0; k < reg->ncol; k++) {
		i = rows_j[k];	
		total += (double) i;	
		i_max = MAX(i_max, i);
		i_min = MIN(i_min, i);
	}
}

#if DEBUG_PSF
printf("%s: '%s' nrow = %d, ncol = %d \n", name, reg->name, reg->nrow, reg->ncol);
printf("%s: MIN: I (%12.2e) \n", name, i_min);
printf("%s: MAX: I (%12.2e) \n", name, i_max);
printf("%s: TOT: I (%12.2e) \n", name, (float) total);
#endif

return;
}

RET_CODE thFftwKernelAddConst(FFTW_KERNEL *fftw_kern, THPIX bkg) {
char *name = "thFftwKernelAddConst";
shAssert(fftw_kern != NULL);
RET_CODE status;
REGION *reg = NULL;
status = thFftwKernelGetDirReg(fftw_kern, &reg);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (reg) in (fftw_kern)", name);
	return(status);
}
shRegAddWithDbl ((double) bkg, reg, reg);
fftw_kern->aflag = MODIFIED;
return(SH_SUCCESS);
}


RET_CODE thAnnulusGetFromReg(PSFWING *psfwing) {
char *name = "thAnnulusGetFromReg";

if (psfwing == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}

REGION *reg = psfwing->reg;
THPIX  *acc = psfwing->a;
THPIX *rad = psfwing->rad;

if (reg == NULL || acc == NULL || rad == NULL) {
	thError("%s: ERROR - improperly allocated (psfwing)", name);
	return(SH_GENERIC_ERROR);
}



int nrow, ncol, row0, col0;
row0 = reg->row0;
col0 = reg->col0;
nrow = reg->nrow;
ncol = reg->ncol;

THPIX xc, yc;
xc = ((THPIX) (2 * row0 + nrow)) / 2.0;
yc = ((THPIX) (2 * col0 + ncol)) / 2.0;

THPIX annulus[WNANN + 1];
memset(annulus, '\0', sizeof(THPIX) * (WNANN + 1));
memset(acc, '\0', sizeof(THPIX) * (WNANN + 1));

int k, nrad;
THPIX halfd = 0.5 * (THPIX) MIN(nrow, ncol);
/* pow((THPIX) (nrow * nrow + ncol * ncol), 0.5); */
for (k = 0; k < WNANN; k++) {
	rad[k] = (THPIX) wanndex[k + 1] - 0.5;
}
for (k = WNANN; k > 0; k--) {
	if (rad[k-1] <= halfd) {
		nrad = k;
		break;
	}
}
psfwing->nrad = nrad;

#if DEBUG_PSF_3
printf("%s: annular representation of a square region: nrow = %d, ncol = %d, nrad = %d, rad = %9.4g rad(+) = %9.4g\n", name, nrow, ncol, nrad, (float) rad[nrad - 1], (float) rad[nrad]);
#endif
/* 
if ((THPIX) nrow < maxr || (THPIX) ncol < maxr) {
 	shRegReshape(reg, (int) (maxr + 0.5), (int) (maxr + 0.5));
	nrow = reg->nrow;
	ncol = reg->ncol;
	row0 = reg->row0;
	col0 = reg->col0;
}
*/

#if DEBUG_PSF_3
int nbadvalue = 0;
#endif
int i, j; 
for (i = 0; i < nrow; i++) {
	THPIX *row_i = reg->rows_thpix[i];
	THPIX xs = (THPIX) (i + row0);
	for (j = 0; j < ncol; j++) {
		THPIX ys = (THPIX) (j + col0);
		THPIX value = row_i[j];
		#if DEBUG_PSF_3
		if (value != value) nbadvalue++;
		#endif
		for (k = 0; k < nrad; k++) {
			THPIX area = circle_square_overlap(xc, yc, rad[k], xs, ys, 
						(THPIX) 1.0, SQUARE_AREA_ITER);
			acc[k] += (float) (value * area);
			annulus[k] += area;
		}
	}
}

#if DEBUG_PSF_3
printf("%s: number of bad values in the PSF region = %d \n", name, nbadvalue);
printf("%s: accumulated circles inside square region \n", name);
printf("%s: (k, r[k], acc[k], area[k]): ", name);
for (k = 0; k < nrad; k++) {
	printf("(%2d: %9.4g, %9.4g, %9.4g), ", k, (float) rad[k], (float) acc[k], (float) annulus[k]);
}
printf("\n");
#endif

/* finding the wring contribution */
for (k = nrad - 1; k > 0; k--) {
	if (k != 0) {
		acc[k] -= acc[k-1];
		annulus[k] -= annulus[k-1];
	}
}

#if DEBUG_PSF_3
printf("%s: rings inside the square region \n", name);
printf("%s: (k, r[k], acc[k], area[k]): ", name);
for (k = 0; k < nrad; k++) {
	printf("(%2d: %9.4g, %9.4g, %9.4g), ", k, (float) rad[k], (float) acc[k], (float) annulus[k]);
}
printf("\n");
#endif

for (k = 0; k < nrad; k++) {
	if (annulus[k] <= (THPIX) 0.0) {
		thError("%s: ERROR - ring at (k = %d) discovered with unsupported area (%g)", name, k, (float) annulus[k]);
	}
}

THPIX area0 = 0.0;
for (k = 0; k < nrad; k++) {
	THPIX area1 = THPI * pow(rad[k], 2.0);
	acc[k] *= (area1 - area0) / annulus[k]; /* normalizing for the ring area */
	if (acc[k] != acc[k]) {
		thError("%s: ERROR - discovered ring with zero area at (k = %d, r_out = %g)", name, k, (float) rad[k]);
	}
	area0 = area1;
}

#if DEBUG_PSF_3
printf("%s: rings normalized for area \n", name);
printf("%s: (k, r[k], acc[k], area[k]): ", name);
for (k = 0; k < nrad; k++) {
	printf("(%2d: %9.4g, %9.4g, %9.4g), ", k, (float) rad[k], (float) acc[k], (float) annulus[k]);
}
printf("\n");
#endif

/* reaccumulating rings to get fI */
/*  
for (k = 1; k < nrad; k++) {
	acc[k] += acc[k - 1];
}

#if DEBUG_PSF_2
printf("%s: circles normalized for area \n", name);
printf("%s: (k, r[k], acc[k], area[k]): ", name);
for (k = 0; k < nrad; k++) {
	printf("(%2d: %9.4g, %9.4g, %9.4g), ", k, (float) rad[k], (float) acc[k], (float) annulus[k]);
}
printf("\n");
#endif
*/

#if DEBUG_PSF_3
printf("%s: psf count [k] -  acc[k]: ", name);
for (k = 0; k < nrad; k++) {
	printf("[%2d]=%9.4g, ", k, (float) acc[k]);
}
printf("\n");
printf("%s: nrad was set to %d \n", name, psfwing->nrad);
#endif


return(SH_SUCCESS);
}

RET_CODE thPsfReportInit(PSFREPORT *report, THPIX xc, THPIX yc, int inboundary, int outboundary) {
char *name = "thPsfReportInit";
if (report == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
report->xc = xc;
report->yc = yc;
report->inboundary = inboundary;
report->outboundary = outboundary;

return(SH_SUCCESS);
}

RET_CODE thPsfReportFinalize(PSFREPORT *report, PSF_CONVOLUTION_REPORT *creport) {
char *name = "thPsfReportFinalize";
if (report == NULL || creport == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
report->background = creport->bkg_outer;
report->amplitude = creport->alpha_outer;
report->lostlight = creport->lost_light;
report->halofrac = creport->halo_frac;
return(SH_SUCCESS);

}


RET_CODE thPsfStatFromPsfReport(CHAIN *report_chain, BRIEFPSFREPORT *breport) {
char *name = "thPsfStatFromPsfReport";
shAssert(report_chain != NULL);
shAssert(breport != NULL);

int i, ipsf, n, nwork;
n = shChainSize(report_chain);
nwork = (6 + N_PSF_REPORT_TYPE * 5) * n;
THPIX *work_array = thCalloc(nwork, sizeof(THPIX));
THPIX *inboundary_array, *outboundary_array, *background_array, *amplitude_array, *lostlight_array, *halofrac_array;
THPIX *sigma_array[N_PSF_REPORT_TYPE], *sigma1_array[N_PSF_REPORT_TYPE], *sigma2_array[N_PSF_REPORT_TYPE], *FWHM_array[N_PSF_REPORT_TYPE], *sum_array[N_PSF_REPORT_TYPE];

inboundary_array = work_array;
outboundary_array = work_array + n;
background_array = work_array + 2 * n;
amplitude_array = work_array + 3 * n;
lostlight_array = work_array + 4 * n;
halofrac_array = work_array + 5 * n;
sigma_array[0] = halofrac_array + n;
for (ipsf = 1; ipsf < N_PSF_REPORT_TYPE; ipsf++) {
	sigma_array[ipsf] = sigma_array[ipsf-1] + n;
}
sigma1_array[0] = sigma_array[N_PSF_REPORT_TYPE-1] + n;
for (ipsf = 1; ipsf < N_PSF_REPORT_TYPE; ipsf++) {
	sigma1_array[ipsf] = sigma1_array[ipsf-1] + n;
}
sigma2_array[0] = sigma1_array[N_PSF_REPORT_TYPE-1] + n;
for (ipsf = 1; ipsf < N_PSF_REPORT_TYPE; ipsf++) {
	sigma2_array[ipsf] = sigma2_array[ipsf-1] + n;
}
FWHM_array[0] = sigma2_array[N_PSF_REPORT_TYPE-1] + n;
for (ipsf = 1; ipsf < N_PSF_REPORT_TYPE; ipsf++) {
	FWHM_array[ipsf] = FWHM_array[ipsf-1] + n;
}
sum_array[0] = FWHM_array[N_PSF_REPORT_TYPE-1] + n;
for (ipsf = 1; ipsf < N_PSF_REPORT_TYPE; ipsf++) {
	sum_array[ipsf] = sum_array[ipsf-1] + n;
}


for (i = 0; i < n; i++) {
	PSFREPORT *report = shChainElementGetByPos(report_chain, i);
	inboundary_array[i] = (THPIX) report->inboundary;
	outboundary_array[i] = (THPIX) report->outboundary;
	background_array[i] = report->background;
	amplitude_array[i] = report->amplitude;
	lostlight_array[i] = report->lostlight;
	halofrac_array[i] = report->halofrac;
	for (ipsf = 0; ipsf < N_PSF_REPORT_TYPE; ipsf++) {
		THPIX *sigma_array_i = sigma_array[ipsf];
		THPIX *sigma1_array_i = sigma1_array[ipsf];
		THPIX *sigma2_array_i = sigma2_array[ipsf];
		THPIX *FWHM_array_i = FWHM_array[ipsf];
		THPIX *sum_array_i = sum_array[ipsf];
		sigma_array_i[i] = report->sigma[ipsf];
		sigma1_array_i[i] = report->sigma1[ipsf];
		sigma2_array_i[i] = report->sigma2[ipsf];
		FWHM_array_i[i] = report->FWHM[ipsf];
		sum_array_i[i] = report->sum[ipsf];
	}
}

RET_CODE status;
BASIC_STAT *basic = thBasicStatNew();

printf("%s: creating inboundary_array statistics \n", name);
fflush(stderr);
fflush(stdout);
status = thBasicStatFromArray(inboundary_array, NULL, basic, n);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not create (basic stat) from  (inboundary array)", name);
	return(status);
}
breport->inboundary_c1 = basic->mean;
breport->inboundary_c2 = basic->median;
breport->inboundary_w1 = basic->s;
breport->inboundary_w2 = basic->iqr;
printf("%s: creating outbounadry_array statistics \n", name);
fflush(stderr);
fflush(stdout);
status = thBasicStatFromArray(outboundary_array, NULL, basic, n);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not create (basic stat) from (outboundary array)", name);
	return(status);
}
breport->outboundary_c1 = basic->mean;
breport->outboundary_c2 = basic->median;
breport->outboundary_w1 = basic->s;
breport->outboundary_w2 = basic->iqr;

breport->inboundary = (int) breport->inboundary_c1;
breport->outboundary = (int) breport->outboundary_c1;

printf("%s: creating background_array statistics \n", name);
fflush(stderr);
fflush(stdout);
status = thBasicStatFromArray(background_array, NULL, basic, n);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not create (basic stat) from (background array)", name);
	return(status);
}
breport->background_c1 = basic->mean;
breport->background_c2 = basic->median;
breport->background_w1 = basic->s;
breport->background_w2 = basic->iqr;

printf("%s: creating amplitude_array statistics \n", name);
fflush(stderr);
fflush(stdout);
status = thBasicStatFromArray(amplitude_array, NULL, basic, n);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not create (basic stat) from (amplitude array)", name);
	return(status);
}
breport->amplitude_c1 = basic->mean;
breport->amplitude_c2 = basic->median;
breport->amplitude_w1 = basic->s;
breport->amplitude_w2 = basic->iqr;

printf("%s: creating lostlight_array statistics \n", name);
fflush(stderr);
fflush(stdout);
status = thBasicStatFromArray(lostlight_array, NULL, basic, n);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not create (basic stat) from (lostlight array)", name);
	return(status);
}
breport->lostlight_c1 = basic->mean;
breport->lostlight_c2 = basic->median;
breport->lostlight_w1 = basic->s;
breport->lostlight_w2 = basic->iqr;
if (basic->mean < (THPIX) 0.0) thError("%s: WARNING - mean (lostlight = %g) found to be negative", name, (float) basic->mean);
if (basic->median < (THPIX) 0.0) thError("%s: WARNING - median (lostlight = %g) found to be negative", name, (float) basic->median);
if (basic->s < (THPIX) 0.0) thError("%s: WARNING - standard derviation for (lostlight = %g) found to be negative", name, (float) basic->s);
if (basic->iqr < (THPIX) 0.0) thError("%s: WARNING - IQR (lostlight = %g) found to be negative", name, (float) basic->iqr);

printf("%s: creating halofrac_array statistics \n", name);
fflush(stderr);
fflush(stdout);

status = thBasicStatFromArray(halofrac_array, NULL, basic, n);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not create (basic stat) from (halofrac array)", name);
	return(status);
}
breport->halofrac_c1 = basic->mean;
breport->halofrac_c2 = basic->median;
breport->halofrac_w1 = basic->s;
breport->halofrac_w2 = basic->iqr;

if (basic->mean < (THPIX) 0.0) thError("%s: WARNING - mean (halofrac = %g) found to be negative", name, (float) basic->mean);
if (basic->median < (THPIX) 0.0) thError("%s: WARNING - median (halofrac = %g) found to be negative", name, (float) basic->median);
if (basic->s < (THPIX) 0.0) thError("%s: WARNING - standard derviation for (halofrac = %g) found to be negative", name, (float) basic->s);
if (basic->iqr < (THPIX) 0.0) thError("%s: WARNING - IQR (halofrac = %g) found to be negative", name, (float) basic->iqr);


for (ipsf = 0; ipsf < N_PSF_REPORT_TYPE; ipsf++) {
	printf("%s: creating sigma_array[%d] statistics \n", name, (int) ipsf);
	fflush(stderr);
	fflush(stdout);
	status = thBasicStatFromArray(sigma_array[ipsf], NULL, basic, n);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not create (stat report) for 'sigma' for (ipsf = %d)", name, ipsf);
		return(status);
	}
	breport->sigma_c1[ipsf] = basic->mean;
	breport->sigma_c2[ipsf] = basic->median;
	breport->sigma_w1[ipsf] = basic->s;
	breport->sigma_w2[ipsf] = basic->iqr;
	
	printf("%s: creating sigma1_array[%d] statistics \n", name, (int) ipsf);
	fflush(stderr);
	fflush(stdout);
	status = thBasicStatFromArray(sigma1_array[ipsf], NULL, basic, n);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not create (stat report) for 'sigma1' for (ipsf = %d)", name, ipsf);
		return(status);
	}
	breport->sigma1_c1[ipsf] = basic->mean;
	breport->sigma1_c2[ipsf] = basic->median;
	breport->sigma1_w1[ipsf] = basic->s;
	breport->sigma1_w2[ipsf] = basic->iqr;
	
	printf("%s: creating sigma2_array[%d] statistics \n", name, (int) ipsf);
	fflush(stderr);
	fflush(stdout);
	status = thBasicStatFromArray(sigma2_array[ipsf], NULL, basic, n);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not create (stat report) for 'sigma2' for (ipsf = %d)", name, ipsf);
		return(status);
	}
	breport->sigma2_c1[ipsf] = basic->mean;
	breport->sigma2_c2[ipsf] = basic->median;
	breport->sigma2_w1[ipsf] = basic->s;
	breport->sigma2_w2[ipsf] = basic->iqr;
	
	printf("%s: creating FWHM_array[%d] statistics \n", name, (int) ipsf);
	fflush(stderr);
	fflush(stdout);
	status = thBasicStatFromArray(FWHM_array[ipsf], NULL, basic, n);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not create (stat report) for 'FWHM' for (ipsf = %d)", name, ipsf);
		return(status);
	}
	breport->FWHM_c1[ipsf] = basic->mean;
	breport->FWHM_c2[ipsf] = basic->median;
	breport->FWHM_w1[ipsf] = basic->s;
	breport->FWHM_w2[ipsf] = basic->iqr;
	
	printf("%s: creating sum_array[%d] statistics \n", name, (int) ipsf);
	fflush(stderr);
	fflush(stdout);
	status = thBasicStatFromArray(sum_array[ipsf], NULL, basic, n);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not create (stat report) for 'sum' for (ipsf = %d)", name, ipsf);
		return(status);
	}
	breport->sum_c1[ipsf] = basic->mean;
	breport->sum_c2[ipsf] = basic->median;
	breport->sum_w1[ipsf] = basic->s;
	breport->sum_w2[ipsf] = basic->iqr;
	
}
breport->n = n;
strcpy(breport->name_c1, "mean");
strcpy(breport->name_c2, "median");
strcpy(breport->name_w1, "s");
strcpy(breport->name_w2, "iqr");

thFree(work_array);
thBasicStatDel(basic);
return(SH_SUCCESS);
}
	
RET_CODE thPsfReportCompileFromPsfWing(PSFWING *psfwing, PSFREPORT *psfreport, int component) {
char *name = "thPsfReportCompileFromPsfWing";
shAssert(psfwing != NULL);
shAssert(psfreport != NULL);
if ((component > N_PSF_REPORT_TYPE) || (component < 0)) {
	thError("%s: ERROR - received (component = %d) while N_PSF_REPORT_TYPE = %d", name, component, N_PSF_REPORT_TYPE);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
REGION *reg = psfwing->reg;
int nrow = reg->nrow;
int ncol = reg->ncol;		
THPIX sum = thRegSum(reg, 0, nrow, 0, ncol, NULL, NULL, 0);


#if PSF_REPORT_REGION
/* now copy psfreg onto psfreport */

REGION *image = psfreport->image[component];
if (image != NULL && image->type != THIOPIXTYPE) { 
	shRegDel(image);
	image = NULL;
}
if (image == NULL) {
	#if VERBOSE_PSF
	thError("%s: WARNING - allocating the image region in 'PSFREPROT'", name);
	#endif
	image = shRegNew(name, nrow, ncol, THIOPIXTYPE);
	psfreport->image[component] = image;
}

shRegReshape(image, nrow, ncol);
shRegClear(image);
THPIX bzero = 0.0, bscale = 1.0;
if (image->type == reg->type) {
	shRegPixCopy(reg, image);

} else {
	/* adding and multiplying with large numbers so that the output is positive */
	shRegMultWithDbl((double) PSF_BSCALE, reg, reg);
	shRegAddWithDbl((double) PSF_BZERO, reg, reg);
	status = shRegIntCopy(image, reg);	
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not copy (reg) into (int reg) for (component = '%d')", name, component);
		return(status);
	}
	shRegAddWithDbl(-(double) (PSF_BZERO), reg, reg);
	shRegMultWithDbl((double) 1.0 / (double) PSF_BSCALE, reg, reg);
	bzero = PSF_BZERO;
	bscale = PSF_BSCALE;
}
psfreport->bzero[component] = bzero;
psfreport->bscale[component] = bscale;
#endif

/* generating annular sum of psf */
status = thAnnulusGetFromReg(psfwing);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not create annular integrals of the psf", name);
	return(status);
}
status = thPsfRegGetFromAnnulus(psfwing);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not perform annulus - region transformation", name);
	return(status);
}
/* note that the size of psfwing->reg might have changed */
reg = psfwing->reg;
nrow = reg->nrow;
ncol = reg->ncol;		
if (nrow != ncol) {
	thError("%s: ERROR - expected a square region created from the radial PSF profile", name);
	return(status);
}
if (nrow % 2 == 0) {
	thError("%s: WARNING: expected odd number of cells per row and column, received (%d)", name, nrow);
}

int i_center, n_center;
i_center = nrow / 2;
n_center = i_center + (nrow % 2);
THPIX *rad_profile = reg->rows_thpix[i_center] + i_center; /* this radial profile will be used to find FWHM */

/* now copying average radial profile */
#if PSF_REPORT_RADIAL_PROFILE
int nrad = MIN(PSFNRAD, n_center);
THPIX *rad = psfreport->rad;
int i;
for (i = 0; i < nrad; i++) {
	rad[i] = i;
}
memcpy(psfreport->average[component], rad_profile, nrad * sizeof(THPIX));
psfreport->nrad[component] = nrad;
#endif

THPIX FWHM = VALUE_IS_BAD;
status = thFWHMGetFromRadial(NULL, rad_profile, n_center, &FWHM);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not derive FWHM from annulus", name);
	return(status);
}
THPIX sigma = VALUE_IS_BAD, sigma1 = VALUE_IS_BAD, sigma2 = VALUE_IS_BAD;
psfreport->FWHM[component] = FWHM;
psfreport->sigma[component] = sigma;
psfreport->sigma1[component] = sigma1;
psfreport->sigma2[component] = sigma2;
psfreport->sum[component] = sum;

/* freeing the memory */
memset(psfwing->a, '\0', psfwing->nrad * sizeof(THPIX));
psfwing->nrad = 0;
shRegReshape(psfwing->reg, 0, 0);	

return(SH_SUCCESS);

}

RET_CODE thFWHMGetFromRadial(THPIX *rad, THPIX *a, int nrad, THPIX *FWHM) {
char *name = "thFWHMGetFromRadial";
if (FWHM == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
shAssert(a != NULL);
if (nrad < 2) {
	thError("%s: ERROR - radial profile 'a' should at least have two elements, received (nrad = %d)", name, nrad);
	*FWHM = VALUE_IS_BAD;
	return(SH_GENERIC_ERROR);
}
int i, imax = 0, imin = 0, n_negative = 0;
THPIX max = a[0], min = a[0];
for (i = 0; i < nrad; i++) {
	if (a[i] < 0.0) {
		n_negative++;
	}
	if (a[i] >= max) {
		max = a[i];
		imax = i;
	}
	if (a[i] <= min) {
		min = a[i];
		imin = i;
	}
}

#if VERBOSE_PSF
if (n_negative != 0) {
	thError("%s: WARNING - (%d) negative elements found in the radial profile", name, n_negative);
}
#endif
THPIX hmax = 0.5 * (max);
int ifound = -1;
THPIX a1 = VALUE_IS_BAD, a2 = VALUE_IS_BAD, rfound = VALUE_IS_BAD;
a1 = a[imax];
i = imax;

while (i < nrad) {
	a2 = a[i]; 
	/* debug 
	printf("(i = %d, a1 = %g, a2 = %g)", i, (float) a1, (float) a2); 
	*/
 	if (a2 == hmax) {
		ifound = i;
		if (rad == NULL) {
			rfound = (THPIX) i;
		} else {	
			rfound = rad[i];
		}
	} else if ((a2 - hmax) * (a1 - hmax)  < (THPIX) 0.0) {
		THPIX r1, r2;
		if (rad == NULL) {
			r1 = (THPIX) (i-1);
			r2 = (THPIX) i;
		} else {
			r1 = rad[i-1];
			r2 = rad[i];
		}
		rfound = (r1 * (hmax - a2) + r2 * (a1 - hmax)) / (a1 - a2);
		ifound = i;
	}
	a1 = a2;
	i++;
}
if (ifound == -1) {
	thError("%s: WARNING - could not find FWHM", name);
} else {
	*FWHM = 2.0 * rfound;
}
return(SH_SUCCESS);
}
	
RET_CODE thPsfReplaceKLPixels(REGION *klreg, REGION *wingreg, THPIX rin, THPIX alpha, THPIX gamma) {
char *name = "thPsfReplaceKLPixels";
/* this function assumes centralized regions */

if (klreg == NULL || wingreg == NULL) {
	thError("%s: ERROR - null input", name);
	return(SH_GENERIC_ERROR);
}
if (rin < 0.0) {
	thError("%s: ERROR - inner radius of the boundary should be positive, received (%g)", name, rin);
	return(SH_GENERIC_ERROR);
}
int klnrow, klncol, wnrow, wncol;
klnrow = klreg->nrow;
klncol = klreg->ncol;
wnrow = wingreg->nrow;
wncol = wingreg->ncol;

if (klnrow > wnrow || klncol > wncol) {
	thError("%s: ERROR - (kl-reg: %d, %d) should be smaller than (wing-reg: %d, %d)", klnrow, klncol, wnrow, wncol);
	return(SH_GENERIC_ERROR);
}
if (klnrow != klncol) {
	thError("%s: WARNING - square (kl-reg: %d, %d) is preferred", name, klnrow, klncol);
}
if (wnrow != wncol) {
	thError("%s: WARNING - square (wing-reg: %d, %d) is preferred", name, wnrow, wncol);
}
if (klnrow %2 != 1 || klncol %2 != 1) {
	thError("%s: ERROR - expects odd number of rows and columns for (kl-reg: %d, %d)", name, klnrow, klncol);
	return(SH_GENERIC_ERROR);
}
if (wnrow %2 != 1 || wncol %2 != 1) {
	thError("%s: ERROR - expectes odd number of rows an columns for (wing-reg: %d, %d)", name, wnrow, wncol);
	return(SH_GENERIC_ERROR);
}

THPIX rinsq = pow(rin, 2);
int wrow0, wcol0, klrow0, klcol0;
wrow0 = wingreg->row0;
wcol0 = wingreg->col0;
klrow0 = klreg->row0;
klcol0 = klreg->col0;
int i, j, pix = 0;
for (i = -(klnrow / 2); i <= klnrow / 2; i++) {
	THPIX *klrow_i = klreg->rows_thpix[i-klrow0];
	THPIX *wrow_i = wingreg->rows_thpix[i-wrow0];
	for (j = -(klncol / 2); j <= klncol / 2; j++) {
		THPIX dist2 = pow((THPIX) i, 2) + pow((THPIX) j, 2);
		if (dist2 >= rinsq) {
			pix++;
			/* note: at this point background correction is already made to the KL-PSF and will not be conducted again */
			klrow_i[j-klcol0] = (wrow_i[j-wcol0])  / alpha; 
		}
	}
}

if (pix == 0) {
	thError("%s: WARNING - no pixels were pleaced in (kl-reg: %d, %d) from (wing-reg: %d, %d) for (r_in = %g)", name, klnrow, klncol, wnrow, wncol, rin);
}
return(SH_SUCCESS);
}

RET_CODE thPsfGetDotProduct(REGION *reg_inner, REGION *reg_outer, WMASK *w, PSFDOTPRODUCT *product) {
char *name = "thPsfGetDotProduct";
if (reg_inner == NULL || reg_outer == NULL) {
	thError("%s: ERROR - both inner and outer regions should be provided", name);
	return(SH_GENERIC_ERROR);
}
if (product == NULL) {
	thError("%s: ERROR - output placeholder should be passed", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
MLEFL in_in, in_out, out_out, in_one, out_one, one_one;
in_in = thBracket(reg_inner, w, reg_inner, &status);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not find the inner product between 'INNER_PSF' and 'INNER_PSF'", name);
	return(status);
}
out_out = thBracket(reg_outer, w, reg_outer, &status);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not find the inner product between 'OUTER_PSF' and 'OUTER_PSF'", name);
	return(status);
} 
in_out = thBracket(reg_inner, w, reg_outer, &status);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not find the inner product between 'INNER_PSF' and 'OUTER_PSF'", name);
	return(status);
}
in_one = thBracketOne(reg_inner, w, &status);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not find the inner product between 'INNER_PSF' and '1'", name);
	return(status);
}
out_one = thBracketOne(reg_outer, w, &status);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not find the inner product between 'OUTER_PSF' and '1'", name);
	return(status);
}
one_one = thOneBracketOne(w, &status);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not find the inner product between '1' and '1'", name);
	return(status);
}
#if VERBOSE_PSF
if (in_in <= (MLEFL) 0.0) {
	thError("%s: WARNING - < PSF_in | W | PSF_in > = %g", name, (float) in_in);
} 
if (out_out <= (MLEFL) 0.0) {
	thError("%s: WARNING - < PSF_out | W | PSF_out > = %g", name, (float) out_out);
} 
if (in_out <= (MLEFL) 0.0) {
	thError("%s: WARNING - < PSF_in | W | PSF_out > = %g", name, (float) in_out);
} 
if (in_one <= (MLEFL) 0.0) {
	thError("%s: WARNING - < PSF_in | W | 1 > = %g", name, (float) in_one);
}  
if (out_one <= (MLEFL) 0.0) {
	thError("%s: WARNING - < PSF_out | W | 1 > = %g", name, (float) out_one);
}  
if (one_one <= (MLEFL) 0.0) {
	thError("%s: WARNING - < 1 | W | 1 > = %g", name, (float) one_one);
} 
#endif

if (in_in != in_in || in_out != in_out || in_one != in_one || out_one != out_one) {
		thError("%s: ERROR - failed NAN test - problem doing inner products on PSF components", name);
		return(SH_GENERIC_ERROR);
}
#if DEBUG_PSF
printf("%s: in_in   = %g \n", name, (float) in_in);
printf("%s: in_out  = %g \n", name, (float) in_out);
printf("%s: in_one  = %g \n", name, (float) in_one);
printf("%s: out_one = %g \n", name, (float) out_one);
printf("%s: out_out = %g \n", name, (float) out_out);
printf("%s: one_one = %g \n", name, (float) one_one); 
#endif

status = thPsfDotProductPut(product, in_in, out_out, in_out, in_one, out_one, one_one);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not put dot products in place", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE  get_margin_from_fraction(THPIX fraction, THPIX *IntegLP, THPIX *rIntegLP, int nr, int margin_min, int margin_max, int  margin_default, int *margin) {
char *name = "get_margin_from_fraction";

shAssert(margin_min >= 0);
shAssert(margin_max >= 0);
shAssert(margin_default >= 0);
if (margin_min > MAXNROW || margin_max > MAXNROW || margin_default > MAXNROW) {
	thError("%s: ERROR - unacceptable value for (margin_min = %d, margin_max = %d, margin_default = %d)", name, margin_min, margin_max, margin_default);
	return(SH_GENERIC_ERROR);
}
if (margin_min > margin_max) {
	int margin_temp = margin_max;
	margin_max = margin_min;
	margin_min = margin_temp;
	thError("%s: WARNING - margin (min, max) values switched", name);
}

if (margin == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (isnan(fraction) || isinf(fraction)) {
	thError("%s: ERROR - (fraction) passed in (NAN)", name);
	*margin = THNAN;
	return(SH_GENERIC_ERROR);
} else if (fraction < (THPIX) 0.0) {
	thError("%s: ERROR - (fraction = %g) found to be negative", name, (float) fraction);
	*margin = THNAN;
	return(SH_GENERIC_ERROR);
}

if (fraction > (THPIX) 1.0) {
	*margin = MIN(margin_min, margin_max);
	return(SH_SUCCESS);
} else if (fraction == (THPIX) 0.0) {
	thError("%s: WARNING - fraction = (%20.10g) of light to be neglected", name, (float) fraction);
}

if (IntegLP == NULL || rIntegLP == NULL) {
	thError("%s: ERROR - null (IntegLP or rIntegLP) found in (psf_info)", name);
	*margin = THNAN;
	return(SH_GENERIC_ERROR);
} else if (nr <= 0) {
	thError("%s: ERROR - unsupported size of IntegLP array (nr = %d)", name, nr);
	*margin = THNAN;
	return(SH_GENERIC_ERROR);
}
int i;
int matching_index = nr-1;
THPIX matching_radius = (THPIX) margin_default;
for (i = nr - 1; i >= 0; i--) {
	if (fraction > IntegLP[i]) matching_index = i;
}
if (matching_index >= 0) {
	matching_index++;
	if (matching_index >= nr) matching_index = nr-1;
	if (matching_index < 0) matching_index = 0;
	matching_radius = rIntegLP[matching_index];
	if (matching_radius < margin_min) matching_radius = margin_min;
	if (matching_radius > margin_max) matching_radius = margin_max;
} else {
	thError("%s: WARNING - did not discover a matching index", name);
	matching_radius = (THPIX) margin_default;
}

#if DEBUG_PSF_VARIABLE_SIZE
printf("%s: fraction = %g, match_index = %d, match-radius = %g, margin_min = %d, margin_max = %d, margin_def = %d \n", name, (float) fraction, matching_index, (float) matching_radius, margin_min, margin_max, margin_default);
#endif
*margin = (int) matching_radius;
return(SH_SUCCESS);
}


RET_CODE  thPsfConvInfoGetMarginFromFraction(THPIX fraction, void *p_ptr, PSF_CONVOLUTION_INFO *psf_info, int margin_min, int margin_max, int  margin_default, int *margin) {
char *name = "thPsfConvInfoGetMarginFromFraction";

RET_CODE status;

THPIX *IntegLP = NULL;
THPIX *rIntegLP = NULL;
int nr = -1;
status = thPsfConvInfoGetIntegLP(psf_info, &IntegLP, &rIntegLP, &nr);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (IntegLP) from (psf_info)", name);
	*margin = THNAN;
	return(status);
}

status = get_margin_from_fraction(fraction, IntegLP, rIntegLP, nr, margin_min, margin_max, margin_default, margin);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get appropriate (margin) for (fraction = %g)", name, (float) fraction);
	return(status);
}
return(SH_SUCCESS);
}


RET_CODE  thWpsfGetMarginFromFraction(THPIX fraction, void *p_ptr, WPSF *wpsf, int margin_min, int margin_max, int  margin_default, int *margin) {
char *name = "thWpsfGetMarginFromFraction";

RET_CODE status;
THPIX *IntegLP = NULL;
THPIX *rIntegLP = NULL;
int nr = -1;
status = thWpsfGetIntegLP(wpsf, &IntegLP, &rIntegLP, &nr);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get (IntegLP) from (psf_info)", name);
	*margin = THNAN;
	return(status);
}

status = get_margin_from_fraction(fraction, IntegLP, rIntegLP, nr, margin_min, margin_max, margin_default, margin);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not get appropriate (margin) for (fraction = %g)", name, (float) fraction);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE  thWpsfGetMarginFromWObjcIo(WOBJC_IO *q, void *p_ptr, WPSF *wpsf, THPIX cutoff_psf, int margin_min, int margin_max, int  margin_default, int *margin) {
char *name = "thWpsfGetMarginFromWObjcIo";

if (q == NULL) {
	thError("%s: ERROR - null input (wobjc_io)", name);
	if (margin != NULL) *margin = THNAN;
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
OBJ_TYPE type = q->objc_type;
THPIX counts;
if (type == OBJ_GALAXY) {
	counts = q->petroCounts[R_BAND];
} else if (type == OBJ_STAR) {
	counts = q->psfCounts[R_BAND];
} else {
	thError("%s: WARNING - object type is neither 'STAR' nor 'GALAXY'", name);
	counts = q->counts_model[R_BAND];
}

if (isnan(counts) || isinf(counts) || counts == (THPIX) 0.0) {
	thError("%s: WARNING - unsupported value (counts = %g) found in (wobjc_io) - returning default margin = %d", name, (float) counts, margin_default);
	*margin = margin_default;
	return(SH_SUCCESS);
}
counts = thabs(counts);
cutoff_psf = thabs(cutoff_psf);

THPIX fraction = cutoff_psf / counts;

status = thWpsfGetMarginFromFraction(fraction, p_ptr, wpsf, margin_min, margin_max, margin_default, margin);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not find (psf_margin) for (cutoff = %g, counts = %g)", name, (float) cutoff_psf, (float) counts);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE  thPsfConvInfoGetMargin(void *q, char *qname, void *p_ptr, PSF_CONVOLUTION_INFO *psf_info, THPIX cutoff_psf, int margin_min, int margin_max, int  margin_default, int *margin) {
char *name = "thPsfConvInfoGetMargin";

RET_CODE status;
THPIX counts = THNAN;
status = extract_counts_from_param(q, qname, &counts);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not extract (counts) from parameter with type '%s'", name, qname);
	return(status);
}
if (isnan(counts) || isinf(counts) || counts == (THPIX) 0.0) {
	thError("%s: WARNING - unsupported value (counts = %g) found for param ('%s') - returning default margin = %d", name, (float) counts, qname, margin_default);
	*margin = margin_default;
	return(SH_SUCCESS);
}
counts = thabs(counts);
cutoff_psf = thabs(cutoff_psf);

THPIX fraction = cutoff_psf / counts;

status = thPsfConvInfoGetMarginFromFraction(fraction, p_ptr, psf_info, margin_min, margin_max, margin_default, margin);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not find (psf_margin) for (cutoff = %g, counts = %g)", name, (float) cutoff_psf, (float) counts);
	return(status);
}

return(SH_SUCCESS);
}

RET_CODE extract_counts_from_param(void *q, char *qname, THPIX *counts) {
char *name = "extract_counts_from_param";
if (counts == NULL) {
	thError("%s: ERROR - null output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (q == NULL || qname == NULL || strlen(qname) == 0) {
	thError("%s: ERROR - null input", name);
	*counts = THNAN;
	return(SH_GENERIC_ERROR);
}
THPIX counts1, mcount1, I1;
THPIX *ptr_counts1 = NULL, *ptr_mcount1 = NULL, *ptr_I1 = NULL;

TYPE qtype = shTypeGetFromName(qname);
if (qtype == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - unknown schema for '%s'", name, qname);
	*counts = THNAN;
	return(SH_GENERIC_ERROR);
}
SCHEMA_ELEM *se = NULL;
se = shSchemaElemGetFromType(qtype, "counts");
if (se != NULL) ptr_counts1 = shElemGet(q, se, NULL);
if (ptr_counts1 != NULL) counts1 = *ptr_counts1;

se = shSchemaElemGetFromType(qtype, "mcount");
if (se != NULL) ptr_mcount1 = shElemGet(q, se, NULL);
if (ptr_mcount1 != NULL) mcount1 = *ptr_mcount1;

se = shSchemaElemGetFromType(qtype, "I");
if (se != NULL) ptr_I1 = shElemGet(q, se, NULL);
if (ptr_I1 != NULL) I1 = *ptr_I1;

THPIX counts2 = THNAN;
int counts1_available = 0, counts2_available = 0;
if (ptr_I1 != NULL && ptr_mcount1 != NULL) {
	counts2 = I1 * mcount1;
	counts2_available = 1;
}
if (ptr_counts1 != NULL) counts1_available = 1;
if (counts1_available && counts2_available) {
	if (fabs(counts1) >= fabs(counts2)) {
		*counts = counts1;
	} else {
		*counts = counts1;
	}
} else if (counts1_available) {
	*counts = counts1;
} else if (counts2_available) {
	*counts = counts2;
} else {
	thError("%s: ERROR - not enough keywords available in '%s' to extract counts", name, qname);
	*counts = THNAN;
	return(SH_GENERIC_ERROR);
}

return(SH_SUCCESS);
}


RET_CODE thFftwArrayCountBadPixels(FFTW_COMPLEX **fftw_arr, int prow, int pcol, int *nbadpixel) {
char *name = "thFftwArrayCountBadPixels";
if (fftw_arr == NULL || nbadpixel == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
if (prow <= 0 || pcol <= 0) {
	thError("%s: ERROR - unsuported size for (fftw_complex) array (%d, %d)", name, prow, pcol);
	return(SH_GENERIC_ERROR);
}
int i, j, nbad = 0;
for (i = 0; i < prow; i++) {
	FFTW_COMPLEX *fftw_i = fftw_arr[i];
	for (j = 0; j < pcol; j++) {
		FFTW_COMPLEX entry = fftw_i[j];
		nbad += (isnan(entry.re) || isnan(entry.im) || isinf(entry.re) || isnan(entry.im));	
	}
}
*nbadpixel = nbad;
return(SH_SUCCESS);
}

