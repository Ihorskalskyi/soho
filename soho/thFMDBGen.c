#include "thFMDBGen.h"

static int (*DBFuncLoaded) (void *, char *);
static RET_CODE (*DBUploadValue) (void *, void *);
static RET_CODE (*DBUploadHeader) (void *, void *, void *);
static RET_CODE (*DBSealHeader) (void *, void *, int);
static int (*QGetSizeTable) (void *Q);
static RET_CODE (*QDraw)(void *, int, void *);

RET_CODE FMDBGenInit(void *ptr_DBFuncLoaded, void *ptr_DBUploadValue, void *ptr_DBUploadHeader, void *ptr_DBSealHeader, void *ptr_QDraw, void *ptr_QGetSizeTable) {
char *name = "FMDBGenInit";

static int init = 0;

if (init) {
	if (ptr_DBFuncLoaded != DBFuncLoaded || ptr_DBUploadValue != DBUploadValue || ptr_DBUploadHeader != DBUploadHeader || ptr_DBSealHeader != DBSealHeader || 
	ptr_QDraw != QDraw || QGetSizeTable != ptr_QGetSizeTable) {
		thError("%s: ERROR - cannot initialize with different linkers", name);
		return(SH_GENERIC_ERROR);
	}
	thError("%s: WARNING - you don't need to initialize the module more than once", name);
	return(SH_SUCCESS);
}
if (!init) {
	DBFuncLoaded = ptr_DBFuncLoaded;
	DBUploadValue = ptr_DBUploadValue;
	DBUploadHeader = ptr_DBUploadHeader;
	DBSealHeader = ptr_DBSealHeader;
	QDraw = ptr_QDraw;
	QGetSizeTable = ptr_QGetSizeTable;
	init = 1;
}
return(SH_SUCCESS);
}

RET_CODE thDBGen(void *fmfits, RET_CODE (*f)(void *, void *), void *wq, void *wfm, char *funcname, void *Qtable, char *type) {
char *name = "thFmDBGen";
TYPE qtype;

if (fmfits == NULL || f == NULL || wfm == NULL || 
	funcname == NULL || strlen(funcname) == 0 || 
	Qtable == NULL || 
	type == NULL || strlen(type) == 0) { 
	thError("%s: ERROR - null input, output or insufficient working space", name);
	return(SH_GENERIC_ERROR);
	}

if ((qtype = shTypeGetFromName(type)) == UNKNOWN_SCHEMA) {
	thError("%s: ERROR - unrecognized type (%s)", name, type);
	return(SH_GENERIC_ERROR);
	}
RET_CODE status, status2;
/* check if a functions of the same name has already been uploaded to the database */
/* if it is loaded, issue a warning and hope that the user knows what she is doing */
int instance;
if (instance == DBFuncLoaded(fmfits, funcname)) {
	thError("%s: WARNING - a function of similar name has already been upload to database, hoping you know what you adoing", name);
	} 

/* now get the total number of elements of the table */
int totrow;
totrow = QGetSizeTable(Qtable);

/* now upload the table and fmhdu to fm - this doesn't really write to the fits file*/
void *q, *Q;
if (wq != NULL) {
	q = wq;
} else {
	q = thqNew(type);
}
Q = thQNew(Qtable);
int i = 0;
status = QDraw(Qtable, i, Q);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not obtain element %d of table for %s", name, i, type);
	thqDel(q, type);
	thQDel(Q);
	return(status);
}
status = thqQTrans(q, type, Q);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not tranform Q -> q at row %d of table %s", name, i, type);
	thqDel(q, type);
	thQDel(Q);
	return(status);
	}
status = f(q, wfm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR  - could not obtain function values for %s at index %i of table %s", 
		name, funcname, i, type);
	return(status);
	}	
status = DBUploadHeader(fmfits, Qtable, wfm);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not upload header to database", name);
	return(status);
}
instance++;

/* now loop over the table */
for (i = 0; i < totrow; i++) {
	if (i != 0) {
	status = QDraw(Qtable, i, Q);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not obtain element %d of table for %s", name, i, type);
		status2 = DBSealHeader(fmfits, funcname, instance);
		if (status2 != SH_SUCCESS) thError("%s: ERROR - could not seal the header, unsafe to upload the next function", name);
		thqDel(q, type);
		thQDel(Q);
		return(status);
	}
	status = thqQTrans(q, type, Q);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not tranform Q -> q at row %d of table %s", name, i, type);
		status2 = DBSealHeader(fmfits, funcname, instance);
                if (status2 != SH_SUCCESS) thError("%s: ERROR - could not seal the header, unsafe to upload the next function", name);
		thqDel(q, type);
		thQDel(Q);
		return(status);
	}
	status = f(q, wfm);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR  - could not obtain function values for %s at index %i of table %s", name, funcname, i, type);
		status2 = DBSealHeader(fmfits, funcname, instance);
                if (status2 != SH_SUCCESS) thError("%s: ERROR - could not seal the header, unsafe to upload the next function", name);
		thqDel(q, type);
		thQDel(Q);
		return(status);
	}
	}
	status = DBUploadValue(fmfits, wfm);
	if (status != SH_SUCCESS) {
		thError("%s: ERROR - could not upload function values for %s at index %i of table %s", name, funcname, i, type);
		status2 = DBSealHeader(fmfits, funcname, instance);
                if (status2 != SH_SUCCESS) thError("%s: ERROR - could not seal the header, unsafe to upload the next function", name);
		thqDel(q, type);
		thQDel(Q);
		return(status);
	}
}

/* note that the fmhdu and fmtables should be saved at the very end when all the fm's are uploaded */

/* seal header to make sure - again this doesn't write to the fits file on the disk, 
	it adjusts the total row number on the header and the table */

status2 = DBSealHeader(fmfits, funcname, instance);
if (status2 != SH_SUCCESS) thError("%s: WARNING - could not seal the header for function %s, not sure why, seems safe though", name, funcname);
if (wq == NULL) thqDel(q, type);
thQDel(Q);
/* return */
return(SH_SUCCESS);

}


