commandfile=temp-mle-lrg-data-submit-batch-2301829.cmd
grep CD-sersic-sersic-G ./$commandfile.fitshape > ./$commandfile.fitshape.sersic-sersic
grep CD-sersic-G ./$commandfile.fitshape > ./$commandfile.fitshape.sersic
grep CD-sersic-Exp-G ./$commandfile.fitshape > ./$commandfile.fitshape.sersic-exp
grep CD-deV-G ./$commandfile.fitshape > ./$commandfile.fitshape.deV
grep CD-deV-deV-G ./$commandfile.fitshape > ./$commandfile.fitshape.deV-deV
grep CD-sersic-sersic-G ./$commandfile.fitsize > ./$commandfile.fitsize.sersic-sersic
grep CD-sersic-G ./$commandfile.fitsize > ./$commandfile.fitsize.sersic
grep CD-sersic-Exp-G ./$commandfile.fitsize > ./$commandfile.fitsize.sersic-exp
grep CD-deV-G ./$commandfile.fitsize > ./$commandfile.fitsize.deV
grep CD-deV-deV-G ./$commandfile.fitsize > ./$commandfile.fitsize.deV-deV
paste -d ':' "$commandfile.fitshape.sersic-sersic" "$commandfile.fitshape.sersic" "$commandfile.fitshape.sersic-exp" "$commandfile.fitshape.deV" "$commandfile.fitshape.deV-deV" "$commandfile.fitsize.sersic-sersic" "$commandfile.fitsize.sersic" "$commandfile.fitsize.sersic-exp" "$commandfile.fitsize.deV" "$commandfile.fitsize.deV-deV" > "$commandfile.fitshape.fitsize.allmodels"
shuf "$commandfile.fitshape.fitsize.allmodels" --output "$commandfile.fitshape.fitsize.allmodels.shuf"
sed 's/:/\n/g' "$commandfile.fitshape.fitsize.allmodels.shuf" > "$commandfile.fitshape.fitsize.allmodels.shuf.sep"
