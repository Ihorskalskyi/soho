#ifndef THPROFILES_H
#define THPROFILES_H

#include "dervish.h"
#include "phSpanUtil.h"
#include "thConsts.h"

typedef struct pixelposition {
	THPIX row, col;
	} PIXPOS;

typedef struct quadrature {
	int mrow, mcol;
	THPIX val;
} QUADRATURE;

typedef enum quad_condition {
	CONTINUE_QUADRATURE, INTEGRATED_ALL, 
	NO_QUADRATURE_NEEDED, UNKNOWN_QUAD_CONDITION,
	N_QUAD_CONDITION
} QUAD_CONDITION;

PIXPOS *thPixposNew();
void thPixposDel(PIXPOS *pixpos);

QUADRATURE *thQuadratureNew();
void thQuadratureDel(QUADRATURE *q);

void thPixposSet(PIXPOS *pixpos, THPIX row, THPIX col);
void thPixposGet(PIXPOS *pixpos, THPIX *row, THPIX *col);

#endif
