RET_CODE sersicProf(void *q, THREGION *reg, THPIX *mcount, int index) {

char *name = "sersicProf";
if (reg == NULL && mcount == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}

if (reg != NULL) {
RET_CODE status;
status = sersic_profile_maker(q, &sersic, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
}

THPIX modelcount;
SERSICPARS *qq = (SERSICPARS *) q;
THPIX det = qq->a * qq->c - pow(qq->b, 2);
THPIX n = qq->n;
THPIX k = qq->k;
THPIX Gamma_2n = qq->gamma_2n;
modelcount = (THPIX) (2.0 * PI * n * exp(k) * pow(k, -2.0 * n) * Gamma_2n * pow(det, -0.5)); /* based on Ciotti et al 1999 */ 
qq->mcount = modelcount;
if (mcount != NULL) *mcount = modelcount;

return(SH_SUCCESS);
}


RET_CODE DsersicDaProf(void *q, THREGION *reg, int index) {

char *name = "DsersicDaProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDa, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}


RET_CODE DsersicDbProf(void *q, THREGION *reg, int index) {

char *name = "DsersicDbProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}

RET_CODE status;
status = sersic_profile_maker(q, &DsersicDb, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}


RET_CODE DsersicDcProf(void *q, THREGION *reg, int index) {

char *name = "DsersicDcProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDc, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DsersicDxcProf(void *q, THREGION *reg, int index) {

char *name = "DsersicDxcProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDxc, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DsersicDycProf(void *q, THREGION *reg, int index) {

char *name = "DsersicDycProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDyc, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DsersicDphiProf(void *q, THREGION *reg, int index) {

char *name = "DsersicDphiProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDphi, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DsersicDeProf(void *q, THREGION *reg, int index) {
char *name = "DsersicDeProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDe, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DsersicDreProf(void *q, THREGION *reg, int index) {
char *name = "DsersicDreProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDre, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DsersicDueProf(void *q, THREGION *reg, int index) {
char *name = "DsersicDueProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDue, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DsersicDEProf(void *q, THREGION *reg, int index) {

char *name = "DsersicDEProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDE, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DsersicDvProf(void *q, THREGION *reg, int index) {
char *name = "DsersicDvProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDv, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DsersicDwProf(void *q, THREGION *reg, int index) {
char *name = "DsersicDwProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDw, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DsersicDVProf(void *q, THREGION *reg, int index) {
char *name = "DsersicDVProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDV, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}

RET_CODE DsersicDWProf(void *q, THREGION *reg, int index) {
char *name = "DsersicDWProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DsersicDW, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DDsersicDreDreProf(void *q, THREGION *reg, int index) {
char *name = "DDsersicDreDreProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DDsersicDreDre, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}
RET_CODE DDsersicDueDueProf(void *q, THREGION *reg, int index) {
char *name = "DDsersicDueDueProf";
if (reg == NULL) {
	thError("%s: WARNING - null placeholder for (reg)", name);
	return(SH_SUCCESS);
}
if (q == NULL) {
	thError("%s: ERROR - null (q)", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
status = sersic_profile_maker(q, &DDsersicDueDue, reg, index);
if (status != SH_SUCCESS) {
	thError("%s: ERROR - could not make profile", name);
	return(status);
}
return(SH_SUCCESS);
}


