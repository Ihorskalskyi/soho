#ifndef THASCII_H 
#define THASCII_H

#include "thBasic.h"
#include "thPar.h"
#include "thAsciischema.h"


void thAsciiInit();
int thAsciiNameRead(FILE *fil, char *vname, int *size);
int thAsciiValRead(FILE *fil, void *fieldval, 
		   THASCIITYPE eltype, THASCIIHEAPTYPE elheaptype,
		   int elsize);
int thAsciiCommentRead(FILE *fil, char *comment);
void thAsciiFieldSkip(FILE *fil);

void *thAsciiStructRead(FILE *fil, char *type);
CHAIN *thAsciiChainRead(FILE *fil);

#endif

