pro plot_batch_sim_mle

myroot = "./sim-vs-mle"
simlist_file = "single-galaxy-feb-11.txt" 
rnames = [["COUNTS_EXP", "RE_EXP"], ["COUNTS_DEV", "RE_DEV"]]
rnames = [["E_EXP", "RE_EXP"], ["E_DEV", "RE_DEV"]]
source_model = ["exp", "deV"]
run=1033
camcol = '3'
nsource = 2L
nrecord = 2L

fit2fpCC = [0, 1]
fpCCstr = ["fpC", "fpCC"]
nfpCC = 2L

tolfactor = [1.1, 10.0, 5.0, 2.0]
tolstr=['1p1', '10', '5', '2', '1p1']
ntol = 1L
for isource = 0, nsource -1L, 1 do begin
irecord = 2L * isource

target_model = [source_model[isource], "deV-exp"]
ntarget = 2
for itarget = 0, ntarget-1L, 1 do begin

for itol = 0, ntol-1L, 1 do begin
for ifpCC = 0, nfpCC-1L, 1 do begin

epsfilename=myroot+"/"+fpCCstr[ifpCC]+"-sim-"+source_model[isource]+"-mle-"+target_model[itarget]+"-tol-"+tolstr[itol]+".eps"
poorfilename=myroot+"/"+fpCCstr[ifpCC]+"-poor-sim-"+source_model[isource]+"-mle-"+target_model[itarget]+"-tol-"+tolstr[itol]+".eps"
plot_sdss_mle_sim, target_model=target_model[itarget], source_model = source_model[isource], fit2fpCC = fit2fpCC[ifpCC], $
	rnames = rnames[irecord: irecord + nrecord - 1L], tolfactor = tolfactor[itol], $
	/domle, /dophoto, /doseparate, /dopoormle, /dopoorsdss, $
	camcol = camcol, simlist = simlist_file, $
	epsfilename = epsfilename, poorfilename = poorfilename

endfor
endfor
endfor
endfor

end
