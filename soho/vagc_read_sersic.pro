;+
; NAME:
;   vagc_read_sersic
; PURPOSE:
;   Read the VAGC files and return objects around a location
; CALLING SEQUENCE:
;   objs= vagc_read(ra, dec, radius [, type=])
; INPUTS:
;   ra, dec - central location (J2000 deg)
;   radius - search radius (deg)
; OPTIONAL INPUTS:
;   type - which catalog to extract data from, assuming file exists of
;          form $VAGC_REDUX/object_[type].fits [default 'sdss_imaging']
; OPTIONAL KEYWORDS:
;   /silent - suppress mrdfits verbosity
; COMMENTS:
;   Assumes that VAGC directory is located at $VAGC_REDUX,
;   and that index file (object_radec.fits) has been created
; REVISION HISTORY:
;   12-Jun-2008 MRB, NYU
;-
;------------------------------------------------------------------------------
function vagc_read_sersic, run, camcol, field, id, silent=silent, $
                    opos=opos

common com_vagc_read, index_vagc

type_sdss='sdss_imaging'
if(n_elements(run) gt 1 OR $
   n_elements(field) gt 1 OR $
   n_elements(camcol) gt 1 OR $
   n_elements(id) gt 1) then begin
    print, 'RUN, CAMCOL, FIELD and ID  must be single element in vagc_read_sersic()'
    return, 0
endif

if(n_tags(index_vagc) eq 0) then begin
    index_vagc= mrdfits(getenv('VAGC_REDUX')+ $
                        '/object_' + type_sdss + '.fits', 1, silent=silent)
endif

;; find matching fields
match_count = 0
match_index = where(index_vagc.run eq run AND $
	 index_vagc.camcol eq camcol AND $
	 index_vagc.field eq field AND $
	 index_vagc.id eq id, match_count)


if (match_count eq 0) then begin
	return, 0
endif

nobjs= match_count
opos=match_index

;; which file do we want?
filename= getenv('VAGC_REDUX')+"/sersic/sersic_catalog.fits"

if(nobjs eq 1) then begin
    objs= mrdfits(filename,1,row=opos, /silent)
    return, objs
endif

;; find contiguous index values
isort= sort(opos)
opos=opos[isort]
next= (opos[0L:nobjs-2L]+1L) eq opos[1:nobjs-1L]

;; make output structure
objs0= mrdfits(filename,1,row=0, /silent)
struct_assign, {junk:0}, objs0
objs=replicate(objs0, nobjs)

;; now read in contiguous sections
ist=0L
for i=1L, nobjs-1L do begin
    if(next[i-1] eq 0) then begin
        
        ;; if the next one is not contiguous, read in this range ...
        ind=i-1
        objs[ist:ind]= mrdfits(filename, 1, ra=[opos[ist], opos[ind]], $
                               silent=silent)
        
        ;; ... and reset start
        ist=i
    endif
endfor
ind= nobjs-1L
objs[ist:ind]= mrdfits(filename, 1, ra=[opos[ist], opos[ind]], $
                       silent=silent)

return, objs

end
;------------------------------------------------------------------------------
