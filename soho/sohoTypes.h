enum boolean {FALSE, TRUE};
enum FilterName /* the numbers are the camera row numbers */
  {
    u, g, r, i, z
  };
/*
  typedef enum FilterRow 
  {
  NA, r, i, u, z, g
  }
*/
enum FType 
  {
    /* AP files */
    apObj, asTrans, asTranscol, 
    /* CALIB files */
    calibImage, calibMatch, 
    calibObj, calibObj_Gal, calibObj_Galmoments, calibObj_Star, 
    calibPhotom, calibPhotomGlobal,
    exPhotom,
    fcPCalib,
    /* FP files */
    fpAtlas, fpBIN, fpC, fpFieldStat, fpM, fpObjc,
    /* HOGG files */
    hoggBB, hoggFF, hoggObj,
    hoggBias_log, hoggFlat_log, hoggObj_log, hoggAstrom_log,
    /* ID files */
    idB, idFF, idR, idRR,
    /* KO files */
    koAstrom, koCat, koTycho2,
    /* PCALIB file */
    pcalibMatchObj, pcalibTrimIndx, pcalibTrimObj,
    /* PS files */
    psBB, psCT, psFang, psFF, psField, psKO, psObj,
    /* RE files */
    reLocalRun, /*(deprecated)*/ reGlobalRun, /*(deprecated)*/
    reObj, /*- reObjGlobal if $PHOTO_RESOLVE set, reObjRun otherwise */
    reObjGlobal, reObjRun, reObjTmp,
    resolve_log,
    scFang,
    /* SKY related files */
    skymodel, skyymodel, skyframes, skyvec,
    /* TS files */
    tsFieldInfo, tsObj, tsField, 
    /* WI files */
    wiField, wiRun, wiRunQA, wiScanline
  };

char *FPrefix[] = {
    /* AP files */
    "apObj", "asTrans", "asTranscol", 
    /* CALIB files */
    "calibImage", "calibMatch", 
    "calibObj", "calibObj_Gal", "calibObj_Galmoments", "calibObj_Star", 
    "calibPhotom", "calibPhotomGlobal",
    "exPhotom",
    "fcPCalib",
    /* FP files */
    "fpAtlas", "fpBIN", "fpC", "fpFieldStat", "fpM", "fpObjc",
    /* HOGG files */
    "hoggBB", "hoggFF", "hoggObj",
    "hoggBias_log", "hoggFlat_log", "hoggObj_log", "hoggAstrom_log",
    /* ID files */
    "idB", "idFF", "idR", "idRR",
    /* KO files */
    "koAstrom", "koCat", "koTycho2",
    /* PCALIB file */
    "pcalibMatchObj", "pcalibTrimIndx", "pcalibTrimObj"
    /* PS files */
    "psBB", "psCT", "psFang", "psFF", "psField", "psKO", "psObj",
    /* RE files */
    "reLocalRun", /*(deprecated)*/ "reGlobalRun", /*(deprecated)*/
    "reObj", /*- reObjGlobal if $PHOTO_RESOLVE set, reObjRun otherwise */
    "reObjGlobal", "reObjRun", "reObjTmp",
    "resolve_log",
    "scFang",
    /* SKY related files */
    "skymodel", "skyymodel", "skyframes", "skyvec",
    /* TS files */
    "tsFieldInfo", "tsObj", "tsField", 
    /* WI files */
    "wiField", "wiRun", "wiRunQA", "wiScanline"
  }
