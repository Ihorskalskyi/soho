#include "thFitmaskTypes.h"
#include "thBasic.h"

FITMASK *thFitmaskNew() {
FITMASK *x = shCalloc(1, sizeof(FITMASK));
int i;
for (i = 0; i < NMASK_FITOBJC; i++) {
	x->delta_rms_mask[i] = CHISQ_IS_BAD;
	x->chisq_mask[i] = CHISQ_IS_BAD;
}
return(x);
}

void thFitmaskDel(FITMASK *fitmask) {
if (fitmask == NULL) return;
int i;
for (i = 0; i < NMASK_FITOBJC; i++) {
	OBJMASK *sp = fitmask->span_mask[i];
	if (sp != NULL) phObjmaskDel(sp);
	fitmask->span_mask[i] = NULL;
}
thFree(fitmask);
return;
}

RET_CODE thFitmaskGetNmask(FITMASK *fitmask, int *nmask) {
char *name = "thFitmaskGetNmask";
shAssert(fitmask != NULL);
shAssert(nmask != NULL);
int nmask_this = fitmask->nmask;
if (nmask_this > NMASK_FITOBJC) {
	thError("%s: ERROR - found (nmask = %d) while expecting nomore than (%d)", name, nmask_this, (int) NMASK_FITOBJC);
	return(SH_GENERIC_ERROR);
}
*nmask = nmask_this;
return(SH_SUCCESS);
}

RET_CODE thFitmaskGetSpanmaskByPos(FITMASK *fitmask, int imask, OBJMASK **sp) {
char *name = "thFitmaskGetSpanmaskByPos";
shAssert(fitmask != NULL);
shAssert(sp != NULL);

RET_CODE status;
int nmask = -1;
status = thFitmaskGetNmask(fitmask, &nmask);

if (imask >= nmask || imask < 0) {
	thError("%s: ERROR - found (imask = %d) while (nmask = %d)", name, imask, nmask);
	*sp = NULL;
	return(SH_GENERIC_ERROR);
}
*sp = fitmask->span_mask[imask];
return(SH_SUCCESS);
}

RET_CODE thFitmaskGetRadiusByPos(FITMASK *fitmask, int imask, THPIX *radius) {
char *name = "thFitmaskGetRadiusByPos";
shAssert(fitmask != NULL);
shAssert(radius != NULL);

RET_CODE status;
int nmask = -1;
status = thFitmaskGetNmask(fitmask, &nmask);
if (status != SH_SUCCESS)   {
	thError("%s: ERROR - could not get (namsk) from (fitmask)", name);
	*radius = (THPIX) VALUE_IS_BAD;
	return(status);
}

if (imask >= nmask || imask < 0) {
	thError("%s: ERROR - found (imask = %d) while (nmask = %d)", name, imask, nmask);
	*radius = (THPIX) VALUE_IS_BAD;
	return(SH_GENERIC_ERROR);
}
*radius = fitmask->radius_mask[imask];
return(SH_SUCCESS);
}



RET_CODE thFitmaskPutSpanmaskByPos(FITMASK *fitmask, int imask, OBJMASK *sp) {
char *name = "thFitmaskPutSpanmaskByPos";
if (fitmask == NULL || sp == NULL) {
	thError("%s: ERROR - null input or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
int nmask;
status = thFitmaskGetNmask(fitmask, &nmask);
if (imask < 0 || imask >= nmask) {
	thError("%s: ERROR - found (imask = %d) while (nmask = %d)", name, imask, nmask);
	return(SH_GENERIC_ERROR);
}
if (fitmask->span_mask[imask] != NULL) {
	#if DEBUG_THFITMASKTYPES
	thError("%s: WARNING - found spanmask [%d] already in place, deleting before placing the new spanmask", name, imask);
	#endif
	phObjmaskDel(fitmask->span_mask[imask]);
	fitmask->span_mask[imask] = NULL;
}
fitmask->span_mask[imask] = sp;
return(SH_SUCCESS);
}

RET_CODE thFitmaskPutDeltaRmsByPos(FITMASK *mask, int imask, MLEFL ss){
char *name = "thFitmaskPutDeltaRmsByPos";
if (mask == NULL) {
	thError("%s: ERROR - null inout or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
int nmask;
status = thFitmaskGetNmask(mask, &nmask);
if (imask < 0 || imask >= nmask) {
	thError("%s: ERROR - found (imask = %d) while (nmask = %d)", name, imask, nmask);
	return(SH_GENERIC_ERROR);
}
mask->delta_rms_mask[imask] = ss;
return(SH_SUCCESS);
}

RET_CODE thFitmaskPutChisqByPos(FITMASK *mask, int imask, MLEFL ss) {
char *name = "thFitmaskPutChisqByPos";
if (mask == NULL) {
	thError("%s: ERROR - null inout or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
int nmask;
status = thFitmaskGetNmask(mask, &nmask);
if (imask < 0 || imask >= nmask) {
	thError("%s: ERROR - found (imask = %d) while (nmask = %d)", name, imask, nmask);
	return(SH_GENERIC_ERROR);
}
mask->chisq_mask[imask] = ss;
return(SH_SUCCESS);
}
RET_CODE thFitmaskPutNpixByPos(FITMASK *mask, int imask, int npix) {
char *name = "thFitmaskPutNpixByPos";
if (mask == NULL) {
	thError("%s: ERROR - null inout or output placeholder", name);
	return(SH_GENERIC_ERROR);
}
RET_CODE status;
int nmask;
status = thFitmaskGetNmask(mask, &nmask);
if (imask < 0 || imask >= nmask) {
	thError("%s: ERROR - found (imask = %d) while (nmask = %d)", name, imask, nmask);
	return(SH_GENERIC_ERROR);
}
if (npix < 0) {
	thError("%s: ERROR - found (npix = %d)", name, npix);
	return(SH_GENERIC_ERROR);
}
mask->npix_mask[imask] = npix;
return(SH_SUCCESS);
}

RET_CODE thFitmaskPutNmask(FITMASK *mask, int nmask) {
char *name = "thFitmaskPutNmask";
if (mask == NULL) {
	thError("%s: ERROR - null (fitmask)", name);
	return(SH_GENERIC_ERROR);
}
if (nmask < 0 || nmask > (int) NMASK_FITOBJC) {
	thError("%s: ERROR - (nmask = %d) out of range", name, nmask);
	return(SH_GENERIC_ERROR);
}
if (mask->nmask != 0) {
	thError("%s: WARNING - (fitmask) has a non-zero (nmask = %d)", name, mask->nmask);
}
mask->nmask = nmask;
return(SH_SUCCESS);
}

RET_CODE thFitmaskPutRadiusByPos(FITMASK *mask, int imask, THPIX radius) {
char *name = "thFitmaskPutRadiusByPos";
if (mask == NULL) {
	thError("%s: ERROR - null (fitmask)", name);
	return(SH_GENERIC_ERROR);
}
if (imask < 0 || imask >= mask->nmask) {
	thError("%s: ERROR - (imask = %d) out of range", name, imask);
	return(SH_GENERIC_ERROR);
}
if (radius < 0.0) {
	thError("%s: ERROR - unsupported value for (radius = %g)", name, (float) radius);
	return(SH_GENERIC_ERROR);
}
mask->radius_mask[imask] = radius;
return(SH_SUCCESS);
}

RET_CODE thFitmaskPutObjcPetro(FITMASK *mask, THPIX rPetro_objc, THPIX rmask_objc, THPIX rPetroCoeff_objc, int indexPetro_objc, int npix_objc, THPIX chisq_objc, THPIX chisq_nu_objc, THPIX delta_rms_objc) {
char *name = "thFitmaskPutObjcPetro";
if (mask == NULL) {
	thError("%s: ERROR - null (fitmask)", name);
	return(SH_GENERIC_ERROR);
}

mask->rPetro_objc = rPetro_objc;
mask->rmask_objc = rmask_objc;
mask->rPetroCoeff_objc = rPetroCoeff_objc;
mask->indexPetro_objc = indexPetro_objc;
mask->npix_objc = npix_objc;
mask->chisq_objc = chisq_objc;
mask->chisq_nu_objc = chisq_nu_objc;
mask->delta_rms_objc = delta_rms_objc;

return(SH_SUCCESS);
}


