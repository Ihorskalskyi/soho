#ifndef TH_PARDIV_TYPES_H
#define TH_PARDIV_TYPES_H

#include "dervish.h"
#include "thConsts.h"

typedef enum thpardiv_type {
	LINEAR_DIV, LOGARITHMIC_DIV, N_PARDIV_TYPE, UNKNOWN_PARDIV_TYPE
	} THPARDIV_TYPE;

#endif
