#!/bin/bash

source "./write_parfile.sh"
source "./write_condorjob.sh"
source "./write_condorshell.sh"
source "./write_photocmd.sh"
source "./find_npsf.sh"
source "./counts_files_in_dir.sh"

name="sim-single-script"

SOHO_DIR="/u/khosrow/thesis/opt/soho"

debug=no
double=no
verbose=no
circular=no
sdss=no
testcenter=no

TODAY=$(date +"%m-%d-%Y")
NOW=$(date +"%T") 

logdir="$SOHO_DIR/log"

Options=$@
Optnum=$#

nrow=1489
ncol=2048

loutdir=no
lfield=no
lfield0=no
lrun=no
lrerun=no
lcamcols=no
lbands=no
lLOGFILE=no
lnfield=no
lmodel=no
lexecutable=no
linner_lumcut=no
louter_lumcut=no
lsmearf=no
unzip=no
lunzipdir=no
lnstar=no
lngalaxy=no
lnlrg=no
lgcsource=no
lextraflags=no
lcdflag=no
lmixednoise=no

outdir=empty
field=empty
field0=empty
run=empty
camcols=empty
bands=empty
nfield=empty
model=empty
executable=empty
inner_lumcut=empty
outer_lumcut=empty
smearf=empty
unzipdir=empty
nstar=empty
ngalaxy=empty
nlrg=empty
gcsource=empty
cdflag=empty
extraflags=""

doutdir="$SOHO_DIR/single-galaxy/images/sims"
dfield=101
drun=$DEFAULT_SDSS_RUN
drerun="137"
dcamcols="123456"
dbands="ugriz"
dLOGFILE="$SOHO_DIR/single-galaxy-process-list.txt"
dnfield=5000
dmodel="exp"
dexecutable="do-thwart"
dinner_lumcut="0.0001"
douter_lumcut="0.0001"
tempfile="./temp-condor-submit.output"
dsmearf=0.0
dunzipdir="$PHOTO_TEMP"
dnstar=10000
dngalaxy=10000
dnlrg=10000
dgcsource="'SWEEP_FILE_AND_WRITE'"
dcdflag=""
dextraflags=""

while getopts ':-:' OPTION ; do
  case "$OPTION" in
    -  ) [ $OPTIND -ge 1 ] && optind=$(expr $OPTIND - 1 ) || optind=$OPTIND
         eval OPTION="\$$optind"
         OPTARG=$(echo $OPTION | cut -d'=' -f2)
         OPTION=$(echo $OPTION | cut -d'=' -f1)
         case $OPTION in
	     --debug     ) debug=yes			 ;;
     	     --verbose   ) verbose=yes			 ;; 
	     --docondor  ) docondor=yes			;;
	     --circular  ) circular=yes			;;
	     --sdss	 ) sdss=yes			;;	
	     --testcenter  ) testcenter=yes		;;
	     --outdir      ) loutdir=yes; outdir="$OPTARG"     ;;
	     --field	 ) lfield=yes; field="$OPTARG"   ;;
	     --field0    ) lfield0=yes; field0="$OPTARG" ;;
	     --nfield    ) lnfield=yes; nfield="$OPTARG" ;;
	     --run       ) lrun=yes; run="$OPTARG"	 ;;
	     --rerun     ) lrerun=yes; rerun="$OPTARG"    ;;
	     --camcols    ) lcamcols=yes; camcols="$OPTARG" ;;
	     --bands      ) lbands=yes; bands="$OPTARG"	 ;;
	     --model     ) lmodel=yes; model="$OPTARG"	 ;;
	     --inner_lumcut      ) linner_lumcut=yes; inner_lumcut="$OPTARG"     ;;	
	     --outer_lumcut      ) louter_lumcut=yes; outer_lumcut="$OPTARG"     ;; 
	     --logfile   ) lLOGFILE=yes; LOGFILE="$OPTARG" ;;	
             --commandfile ) lcommandfile=yes; commandfile="$OPTARG" ;;
	     --executable  ) lexecutable=yes; executable="$OPTARG"   ;;
	     --nfield      ) lnfield=yes; nfield="$OPTARG"	;;
	     --smearf	   ) lsmearf=yes; smearf="$OPTARG"	;;
		--unzip	   ) unzip=yes ;;
		--unzipdir ) lunzipdir=yes; unzipdir="$OPTARG"			;;
		--nstar	   ) lnstar=yes; nstar="$OPTARG"	;;
		--ngalaxy  ) lngalaxy=yes; ngalaxy="$OPTARG"	;;
		--nlrg	   ) lnlrg=yes; nlrg="$OPTARG"		;;
		--gcsource ) lgcsource=yes; gcsource="$OPTARG"	;;
		--extraflags ) lextraflags=yes; extraflags="$OPTARG"	;;
		--cD	   ) lcdflag=yes; cdflag="-cD_classify"	;;
		--mixednoise )lmixednoise=yes; noiseflag=""	;;
	* )  echo "$name: invalide options (long)" ;;
         esac
       OPTIND=1
       shift
      ;;
    ? )  echo "$name: invalid options (short) "  ;;
  esac
done

if [ $loutdir == no ]; then
	outdir=$doutdir
	echo "$name: argument (outdir)   set to '$outdir'"
fi
if [ $lfield == no ]; then
	field=$dfield
	echo "$name: argument (field)  set to '$field'"
fi
if [ $lrun == no ]; then
	run=$drun
	echo "$name: argument (run)    set to '$run'"
fi
if [ $lrerun == no ]; then
	rerun=$drerun;
	echo "$name: argument (rerun)  set to '$rerun'"
fi
if [ $lcamcols == no ]; then
	camcols=$dcamcols
	echo "$name: argument (camcol) set to '$camcols'"
fi
if [ $lbands == no ]; then
	bands=$dbands
	echo "$name: argument (bands)   set to '$bands'"
fi
if [ $lLOGFILE == no ]; then
	LOGFILE=$dLOGFILE
	echo "$name: argument (logfile) set to '$LOGFILE'"
fi
if [ $lfield0 == no ]; then
	field0=$field
	echo "$name: argument (field0) set to '$field0'"
fi
if [ $lnfield == no ]; then
	nfield=$dnfield
	echo "$name: argument (nfield) set to '$nfield'"
fi
if [ $nfield -lt 0 ]; then
	echo "$name: argument (nfield) set to invalid value '$nfield'"
	exit
fi
if [ $lmodel == no ]; then
	model=$dmodel
	echo "$name: argument (model) set to '$model'"
fi
if [ $lexecutable == no ]; then 
	executable=$dexecutable
	echo "$name: argument (executable) set to '$executable'"
fi
if [ $linner_lumcut == no ]; then
	inner_lumcut=$dinner_lumcut
	echo "$name: argument (inner_lumcut) set to '$inner_lumcut'"
fi
if [ $louter_lumcut == no ]; then
	outer_lumcut=$douter_lumcut
	echo "$name: argument (outer_lumcut) set to '$outer_lumcut'"
fi
if [ $lsmearf == no ]; then
	smearf=$dsmearf
	echo "$name: argument (smearf) set to '$smearf'"
fi
if [ $lunzipdir == no ]; then
	unzipdir=$dunzipdir
	echo "$name: argument (unzipdir) set to '$unzipdir'"
	dataskydir=$unzipdir
fi
if [ $lnstar == no ]; then
	nstar=$dnstar
	echo "$name: argument (nstar) set to '$nstar'"
fi
if [ $lngalaxy == no ]; then
	ngalaxy=$dngalaxy
	echo "$name: argument (ngalaxy) set to '$ngalaxy'"
fi
if [ $lnlrg == no ]; then
	nlrg=$dnlrg
	echo "$name: argument (nlrg) set to '$nlrg'"
fi
if [ $lgcsource == no ]; then
	gcsource=$dgcsource
	echo "$name: argument (gcsource) set to $gcsource"
fi
if [ $lcdflag == no ]; then
	cdflag=$dcdflag
	echo "$name: argument (cdflag) set to $cdflag"
fi

#extraflags="$extraflags -cdclass 'DEV_GCLASS'"
echo "$name: debug    = $debug"
echo "$name: verbose  = $verbose"
echo "$name: EXTRA_FLAGS = '$extraflags'"

if [ ! -d $datadir ]; then
	echo "$name: making (datadir) '$datadir'"
	mkdir -p $datadir
fi
if [ ! -d $outdir ]; then
	echo "$name: making (outdir) '$outdir'"
	mkdir -p $outdir
fi

srcdir_objcs="$PHOTO_REDUX/$DEFAULT_SDSS_RERUN/$run/objcs"
if [ ! -d $srcdir_objcs ]; then 
	echo "$name: SDSS source directory '$srcdir_objcs' does not exist"
	exit
fi

nband=${#bands}
# for sdss based simulations, all bands are created by one run, so there should be no loops.
#for (( iband=0; iband<$nband; iband++ ))
#do
	band="r" #${bands:$iband:1}
	if [[ $band != "u" && $band != "g" && $band != "r" && $band != "i" && $band != "z" ]]; then
		echo "$name: undefined band ($band) found in (bands = $bands)"
		exit
	fi
#done

ncamcol=${#camcols}
if [ $ncamcol == 0 ]; then
	echo "$name: no camcol list was provided"
fi

declare -a gaussian_noise_dn_a=(5.0 0.5 0.05 0.005)
n_gaussian_dn=${#gaussian_noise_dn_a[@]}
m_gaussian_dn=$((n_gaussian_dn-1))

echo "$name: count of gaussian_noise_dn = $n_gaussian_dn"

for (( icamcol=0; icamcol<$ncamcol; icamcol++ ))
do
	camcol=${camcols:$icamcol:1}
	if [[ $camcol != "1" && $camcol != "2" && $camcol != "3" && $camcol != "4"  && $camcol != "5" && $camcol != "6" ]]; then
		echo "$name: undefined camcol ($camcol) found in (camcolss = $camcols)"
		exit
	fi
	commanddir="$outdir/$camcol/cmd"
	condordir="$outdir/$camcol/condor"
	clogdir="$condordir/log"
	cerrordir="$condordir/error"
	coutputdir="$condordir/output"
	csubmitdir="$condordir/submit"
	cshelldir="$condordir/shell"
	objcdir="$outdir/$camcol/objc"
	parfiledir="$outdir/$camcol/parfiles"
	photodir="$outdir/$camcol/photo"
	tempdir="$outdir/$camcol/temp"
	simdir="$outdir/$camcol/siminput"
	if [ ! -d $commanddir ]; then
		echo "$name: making (commanddir) '$commanddir'"
		mkdir -p $commanddir
	fi
	if [ ! -d $condordir ]; then
		echo "$name: making (condordir) '$condordir'"
		mkdir -p $condordir
	fi
	if [ ! -d $clogdir ]; then
		echo "$name: making (clogdir) '$clogdir'"
		mkdir -p $clogdir
	fi
	if [ ! -d $cerrordir ]; then
		echo "$name: making (cerrordir) '$cerrordir'"
		mkdir -p $cerrordir
	fi
	if [ ! -d $coutputdir ]; then
		echo "$name: making (coutputdir) '$coutputdir'"
		mkdir -p $coutputdir
	fi
	if [ ! -d $csubmitdir ]; then
		echo "$name: making (csubmitdir) '$csubmitdir'"
		mkdir -p $csubmitdir
	fi
	if [ ! -d $cshelldir ]; then
		echo "$name: making (cshelldir) '$cshelldir'"
		mkdir -p $cshelldir
	fi
	if [ ! -d $objcdir ]; then
		echo "$name: making (objcdir) '$objcdir'"
		mkdir -p $objcdir
	fi
	if [ ! -d $parfiledir ]; then 
		echo "$name: making (parfiledir) '$parfiledir'"
		mkdir -p $parfiledir
	fi
	if [ ! -d $photodir ]; then
		echo "$name: making (photodir) '$photodir'"
		mkdir -p $photodir
	fi
	if [ ! -d $tempdir ]; then
		echo "$name: making (tempdir) '$tempdir'"
		mkdir -p $tempdir
	fi
	if [ ! -d $simdir ]; then
		echo "$name: making (simdir) '$simdir'"
		mkdir -p $simdir
	fi
	if [[ $unzip == yes && ! -d $unzipdir ]]; then 
		echo "$name: making (unzipdir) '$unzipdir'"
		mkdir -p $unzipdir
	fi
	


done

echo "DATE: $TODAY, TIME: $NOW" >>$LOGFILE

runstr=`printf "%06d" $run` 
declare -a psf_a=(9999 999 99 90 80 70 60 50 40 30 20 10)
npsf=1

maxseed=4294967295 # the biggest unsigned int that can be passed as a seed to simulator 

ifield=$field
ifield0=$field0
field0=`printf "%04d" $ifield0`

ipsf=0
f1=${psf_a[$ipsf]}
f2=$f1

field0=$(find_f0 --run=$run --camcol="1" --band=$band)
npsf=$(find_npsf --run=$run --camcol="1" --field0=$field0 --band=$band)
echo "$name: npsf = $npsf"
if [ $nfield -gt $npsf ]; then
	echo "$name: nfield = $nfield, converting to $npsf"
	nfield=$npsf
fi
if [ $sdss == yes ]; then 
	#nfield=$npsf
	ntotal=$nfield
	echo "$name: sdss-based simulation mode: n-field = $nfield"
	echo "$name: sdss-based simulation mode: n-total = $ntotal"
fi

echo "$name: n-field set to = $nfield"
njob=0 # number of jobs submitted so far 
ifield=$(( 10#$field0 ))
nsim=$(( $nfield * $ncamcol ))

echo "$name: preparing ($nsim) simulation jobs in ($nfield) fields and ($ncamcol) camcols"
for (( isim=0; isim<$nsim; isim++ ))
do

	jfield=$(( isim % nfield ))
	ifield=$(( jfield + field0 ))
	icamcol=$(( isim / nfield ))
	camcol=${camcols:$icamcol:1}

	condordir="$outdir/$camcol/condor"
	clogdir="$condordir/log"
	cerrordir="$condordir/error"
	coutputdir="$condordir/output"
	csubmitdir="$condordir/submit"
	cshelldir="$condordir/shell"

	commanddir="$outdir/$camcol/cmd"

	objcdir="$outdir/$camcol/objc"
	photodir="$outdir/$camcol/photo"
	tempdir="$outdir/$camcol/temp"

	srcdir_redux="$srcdir_objcs/$camcol"
	srcdir_data="$srcdir_objcs/$camcol"
	srcdir_calib="$srcdir_objcs/$camcol"

	inobjcdir="$srcdir_objcs/$camcol"
	inobjcdir2="$CPHOTO_OBJCS/$DEFAULT_SDSS_RERUN/$run/$camcol"
	if [ $lunzipdir == no ]; then
		inobjcdir2=$tempdir
	else 
		inobjcdir2=$unzipdir
	fi

	if [ $unzip == yes ]; then 
		dataskydir="$srcdir_objcs/$camcol"
		if [ $lunzipdir == no ]; then
			inskydir=$tempdir
		else 
			inskydir=$unzipdir
		fi
	else
		inskydir="$srcdir_objcs/$camcol"
	fi

	simdir="$outdir/$camcol/siminput"

	#psf should not be random in sdss-based simulation
	jpsffield=$jfield
	let "ipsffield = $field0 + $jpsffield"

	#for (( iband=0; iband<$nband; iband++ )) 
	#do

	band=${bands:$iband:1}	
	seed=$(python -S -c "import random; print random.randrange(1,$maxseed)")
	i_gaussian=$(python -S -c "import random; print random.randint(0,$m_gaussian_dn)")
	gaussian_noise_dn=${gaussian_noise_dn_a[$i_gaussian]}
	#designing the file names #
	field=`printf "%04d" $ifield`
	psffield=`printf "%04d" $ipsffield`

	dir="$outdir"
	parfiledir="$outdir/$camcol/parfiles"
	parfilename="ff-sim-sdss-$runstr-$camcol-$field.par"
	targetparfile="$parfiledir/$parfilename"

	photofile="$commanddir/photo-$runstr-$camcol-$field.cmd"

	condorpreprefix="condor-sdss-sim"
	condorprefix="$condorpreprefix-$runstr-$camcol-$field"
	condor_submitfile="$csubmitdir/$condorprefix.submit"
	condor_errorfile="$cerrordir/$condorprefix.error"
	condor_logfile="$clogdir/$condorprefix.log"
	condor_outputfile="$coutputdir/$condorprefix.out"
	condor_shellfile="$cshelldir/$condorprefix.sh"

	# from thParse.c
	#{"setup",     required_argument, NULL, 'a'},
	#{"camcol",    required_argument, NULL, 'b'},
	#{"band",      required_argument, NULL, 'c'},
	#{"noobjcfile", no_argument, NULL, 'd'},
	#{"framefile", required_argument, NULL, 'e'}, 
	#{"ff",	      required_argument, NULL, 'f'},
	#{"smearobjc", no_argument, NULL, 'g'},
	#{"sky", required_argument, NULL, 'h'},
	#{"exp",  required_argument, NULL, 'i'},
	#{"deV",  required_argument, NULL, 'j'},
	#{"star", required_argument, NULL, 'k'}, 
 	#{"nrow",    required_argument, NULL, 'l'},
	#{"ncol",    required_argument, NULL, 'm'},	
	#{"run", 	required_argument, NULL, 'n'},
	#{"rerun", required_argument, NULL, 'o'},
	#{"field", required_argument, NULL, 'p'},
	#{"stripe", required_argument, NULL, 'q'},
	#{"seed", required_argument, NULL, 'r'},
	#/* added on April 27, 2015 */
	#{"exp_inner_lumcut", required_argument, NULL, 's'},
	#{"exp_outer_lumcut", required_argument, NULL, 't'},
	#{"dev_inner_lumcut", required_argument, NULL, 'u'},
	#{"dev_outer_lumcut", required_argument, NULL, 'v'},
	#{"readskyfile", no_argument, NULL, 'w'},
	#{"smearf", required_argument, NULL, 'x'},


	if [ $lmixednoise == yes ]; then
		noiseflag="-gaussian_noise $gaussian_noise_dn"
	fi
	if [ $unzip == no ]; then 
	condor_argument="\"-camcol $camcol -run $run -rerun $rerun -field $field -framefile $targetparfile -smearobjc -smearf $smearf -readskyfile -seed $seed -nstar $nstar -ngalaxy $ngalaxy -nlrg $nlrg $cdflag -gcsource $gcsource $extraflags $noiseflag\""
	else
	condor_argument="-camcol $camcol -run $run -rerun $rerun -field $field -framefile $targetparfile -smearobjc -smearf $smearf -readskyfile -seed $seed -nstar $nstar -ngalaxy $ngalaxy -nlrg $nlrg $cdflag -gcsource $gcsource $extraflags $noiseflag"
	fi
	condor_comment="sdss-based simulation; date: $TODAY, time: $NOW"

	#for sdss based simulations psf should not be copied
	#verify this command
	photo_cmd_band="{u g r i z}"

	if [ $unzip == no ]; then

	write_parfile --simulator --sdsssimulator --simdir="$simdir" --datadir="$photodir" --datadir2="$inobjcdir2"  --objcdir="$objcdir" --field=$field --psffield=$psffield --run=$run --camcol=$camcol --band=$band --outdir="$outdir" --inobjcdir="$inobjcdir" --inobjcdir2="$inobjcdir2" --inskydir="$inskydir" --comment="$comment" --output="$targetparfile"	
	write_photocmd --overwrite --setpsdir --photofile="$photofile" --camcol=$camcol --field=$ifield --logfile=$logfile --fpCDir=$photodir --run=$run --band="$photo_cmd_band"

	write_condorjob --restrict --submitfile=$condor_submitfile --errorfile=$condor_errorfile --logfile=$condor_logfile --outputfile=$condor_outputfile --executable="$executable" --arguments="$condor_argument" --comment="$condor_comment"	

	else

	write_parfile --simulator --sdsssimulator --simdir="$simdir" --datadir="$photodir" --datadir2="$inobjcdir2" --objcdir="$objcdir" --field=$field --psffield=$psffield --run=$run --camcol=$camcol --band=$band --outdir="$outdir" --inobjcdir="$inobjcdir" --inobjcdir2="$inobjcdir2" --inskydir="$inskydir" --comment="$comment" --output="$targetparfile"		
	write_photocmd --overwrite --setpsdir --photofile="$photofile" --camcol=$camcol --field=$ifield --logfile=$logfile --fpCDir=$photodir --run=$run --band="$photo_cmd_band"

	write_condorshell --field=$field --run=$run --camcol=$camcol --shellfile=$condor_shellfile --comment="$condor_comment" --executable="$executable" --photofile="$photofile" --argument="$condor_argument" --inskydir="$dataskydir" --outskydir="$inskydir" --zipfpCC --fpCCDir="$photodir" --fpCDir="$photodir" 

	chmod u+x $condor_shellfile

	write_condorjob --restrict --submitfile=$condor_submitfile --errorfile=$condor_errorfile --logfile=$condor_logfile --outputfile=$condor_outputfile --executable="$condor_shellfile" --arguments="" --comment="$condor_comment"
	

	fi
	if [ $verbose == yes ]; then

	echo "$name: $condor_submitfile"
	echo "condor_arguments: $condor_argument"
	echo "model1: $modelstring1"
	echo "sky:    $modelskystring"
	echo "condor: $condor_submitfile"
	echo "shell:  $condor_shellfile"
	else
		echo -n "."
	fi
		
	if [ $debug == yes ]; then
		echo -n 'e'
	else 
		if [ $docondor == yes ]; then
 		if [ $lcommandfile == yes ]; then
			echo "condor_submit $condor_submitfile >> $tempfile" >> $commandfile
		else
			eval "condor_submit $condor_submitfile >> $tempfile"
		fi
		else
			echo "$name: $executable $condor_argument >> $tempfile"
			eval "$executable $condor_argument >> $tempfile"
		fi
		echo -n "."
	fi
	((njob++))
#done #for band loop	

    
done
echo

#freeing disk space by zipping
if [ $debug == no ]; then
	
	echo ""
	echo "$name: zipping fpC and fpCC files in $dir"
	pattern="fpC*fit"
	path="$dir/$pattern"
	zipcmd="gzip -f $path"
	while $( any_with_ext $dir $pattern ) ; do
		echo -n "-"
		($zipcmd);
		echo -n "."
	done
	echo " "

fi


echo "$name: ($njob) jobs submitted or written to command file"

