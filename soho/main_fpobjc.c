/* Standard C Libs */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "strings.h"
/* SOHO Libs */
#include "sohoEnv.h"
#include "thDebug.h"
#include "thFpobjcIo.h"

int verbose = 0;

static void usage(void);

/***********************************************************/

int 
main(int ac, char *av[])
{
  /* Arguments */
  char *infile, *outfile;		/* input and output filenames */

  while(ac > 1 && (av[1][0] == '-' || av[1][0] == '+')) {
    switch (av[1][1]) {
    case '?':
    case 'h':
      usage();
      exit(0);
      break;
    case 'v':
      verbose++;
      break;
    default:
      shError("Unknown option %s\n",av[1]);
      break;
    }
    ac--;
    av++;
  }
  if(ac <= 2) {
    shError("You must specify an input file, an output file, a row, and a column \n");
    exit(1);
  }
  infile = av[1]; outfile = av[2]; 

  if (thCpFpObjc(infile, outfile) != SH_SUCCESS) {
    exit(1);
  }
  /* successfull run */
  return(0);
}

/*****************************************************************************/

static void
usage(void)
{
   char **line;

   static char *msg[] = {
      "Usage: read_fpobjc [options] input-file output-file r0 c0 r1 c1",
      "Your options are:",
      "       -?      This message",
      "       -b #    Set background level to #",
      "       -c #    Use colour # (0..ncolor-1; default 0)",
      "       -h      This message",
      "       -i      Print an ID string and exit",
      "       -v      Turn up verbosity (repeat flag for more chatter)",
      NULL,
   };

   for(line = msg;*line != NULL;line++) {
      fprintf(stderr,"%s\n",*line);
   }
}

