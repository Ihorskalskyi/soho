#include "thLRG.h"
#include "thDebug.h"

static void get_necessary_rMags(PHPROPS *objc, float *rPetroMag, float *rPetroTheta, float *rPetroMu, float *rPsfMag, float *rModelMag);
static void get_g_r_i_mags(PHPROPS *objc, float *gMag, float *rMag, float *iMag);
static void get_g_r_i_flags(PHPROPS *objc, int *gflag, int *rflag, int *iflag);

static RET_CODE get_c1_c2_for_phobjc(PHPROPS *objc, float *c1, float *c2);
static int pass_LRG_cut1(PHPROPS *objc);
static int pass_LRG_cut2(PHPROPS *objc);

void get_necessary_rMags(PHPROPS *objc, float *rPetroMag, float *rPetroTheta, float *rPetroMu, float *rPsfMag, float *rModelMag) {

shAssert(objc != NULL);
CALIB_WOBJC_IO *cwphobjc = NULL;
CALIB_PHOBJC_IO *cphobjc = NULL;
RET_CODE status = thPhpropsGet(objc, NULL, NULL, &cwphobjc, &cphobjc, NULL);
shAssert(status == SH_SUCCESS);
shAssert(cphobjc != NULL);

float rPetroMag2 = cphobjc->petroMag[R_BAND] - cphobjc->extinction[R_BAND];
float rPetroMagErr2 = cphobjc->petroMagErr[R_BAND];
float rPetroTheta2 = cphobjc->petroTh50[R_BAND];
float rPetroThetaErr2 = cphobjc->petroTh50Err[R_BAND];
float rPetroMu2, rPetroMuErr2;
status = thMuFromMagTheta(rPetroMag2, rPetroMagErr2, rPetroTheta2, rPetroThetaErr2, 
	&rPetroMu2, &rPetroMuErr2);
shAssert(status == SH_SUCCESS);

float rPsfMag2 = cphobjc->psfMag[R_BAND];
float rModelMag2 = cphobjc->modelMag[R_BAND];

if (rPetroMag != NULL) *rPetroMag = rPetroMag2;
if (rPetroTheta != NULL) *rPetroTheta = rPetroTheta2;
if (rPetroMu != NULL) *rPetroMu = rPetroMu2;
if (rPsfMag != NULL) *rPsfMag = rPsfMag2;
if (rModelMag != NULL) *rModelMag = rModelMag2;

return;
}




void get_g_r_i_mags(PHPROPS *objc, float *gMag, float *rMag, float *iMag) {
shAssert(objc != NULL);
CALIB_WOBJC_IO *cwphobjc = NULL;
CALIB_PHOBJC_IO *cphobjc = NULL;
RET_CODE status = thPhpropsGet(objc, NULL, NULL, &cwphobjc, &cphobjc, NULL);
shAssert(status == SH_SUCCESS);
shAssert(cphobjc != NULL);
float rX, iX, gX;
rX = cphobjc->petroMag[R_BAND] - cphobjc->extinction[R_BAND];
iX = cphobjc->petroMag[I_BAND] - cphobjc->extinction[I_BAND];
gX = cphobjc->petroMag[G_BAND] - cphobjc->extinction[G_BAND];
if (gMag != NULL) *gMag = gX;
if (rMag != NULL) *rMag = rX;
if (iMag != NULL) *iMag = iX;
return;
}

void get_g_r_i_flags(PHPROPS *objc, int *gflag, int *rflag, int *iflag) {
shAssert(objc != NULL);
CALIB_WOBJC_IO *cwphobjc = NULL;
CALIB_PHOBJC_IO *cphobjc = NULL;
RET_CODE status = thPhpropsGet(objc, NULL, NULL, &cwphobjc, &cphobjc, NULL);
shAssert(status == SH_SUCCESS);
shAssert(cphobjc != NULL);
int rX, iX, gX;
rX = cphobjc->flags[R_BAND];
iX = cphobjc->flags[I_BAND];
gX = cphobjc->flags[G_BAND];
if (gflag != NULL) *gflag = gX;
if (rflag != NULL) *rflag = rX;
if (iflag != NULL) *iflag = iX;
return;
}


RET_CODE get_c1_c2_for_phobjc(PHPROPS *objc, float *c1, float *c2) {
shAssert(objc != NULL);
float rMag, iMag, gMag, x1, x2;
get_g_r_i_mags(objc, &gMag, &rMag, &iMag);
x1 = (rMag - iMag) - (gMag - rMag) / (float) 4.0 - (float) 0.18;
x2 = ((float) DEFAULT_C_LRG) * (gMag - rMag) + ((float) 1.0 - (float) DEFAULT_C_LRG) * (float) 4.0 * ((rMag - iMag) - (float) 0.18);
if (rMag == (float) VALUE_IS_BAD || iMag == (float) VALUE_IS_BAD || gMag == (float) VALUE_IS_BAD) {
	x1 = (float) VALUE_IS_BAD;
	x2 = (float) VALUE_IS_BAD;
}
if (c1 != NULL) *c1 = x1;
if (c2 != NULL) *c2 = x2;
if (x1 == VALUE_IS_BAD || x2 == VALUE_IS_BAD) {
	return(SH_GENERIC_ERROR);
}
return(SH_SUCCESS);
}

int pass_LRG_cut1(PHPROPS *objc) {
char *name = "pass_LRG_cut1";
shAssert(objc != NULL);
float c1, c2;
CALIB_WOBJC_IO *cwphobjc = NULL;
CALIB_PHOBJC_IO *cphobjc = NULL;
RET_CODE status = thPhpropsGet(objc, NULL, NULL, &cwphobjc, &cphobjc, NULL);
shAssert(status == SH_SUCCESS);
shAssert(cphobjc != NULL);
status = get_c1_c2_for_phobjc(objc, &c1, &c2);
if (status != SH_SUCCESS) return(0);

float rPetroMag, rPetroTheta, rPetroMu, rPsfMag, rModelMag;
int gflag = 0x0, rflag = 0x0, iflag = 0x0;
get_g_r_i_flags(objc, &gflag, &rflag, &iflag);
get_necessary_rMags(objc, &rPetroMag, &rPetroTheta, &rPetroMu, &rPsfMag, &rModelMag);
if (rPetroMag == (float) VALUE_IS_BAD || rPetroMu == (float) VALUE_IS_BAD || rPsfMag == (float) VALUE_IS_BAD || rModelMag == (float) VALUE_IS_BAD) {
	/* 	thError("%s: WARNING - not enough photometric values known", name);
	*/
	return(0);
	return(LRG_STATUS_UNKNOWN);
}

int res = LRG_NOT, d1, d2, d3, d4, d5, d6, d7;
d1 = (rPetroMag < (float) 13.116 + c2 / (float) 0.3);
d2 = (rPetroMag < (float) 19.2);
d3 = (fabs(c1) < (float) 0.2);
d4 = (rPetroMu < (float) 24.2);
d5 = ((rPsfMag - rModelMag) >= (float) 0.24);
d6 = ((rflag & LRG_BINNED) && (iflag & LRG_BINNED));
d7 = (!(rflag & LRG_TOO_BRIGHT) && !(iflag & LRG_TOO_BRIGHT) && !(gflag & LRG_TOO_BRIGHT));

#if DEBUG_LRG
printf("%s: d1, d2, d3, d4, d5, d6, d7 = %d, %d, %d, %d, %d, %d, %d \n", name, d1, d2, d3, d4, d5, d6, d7);
fflush(stdout);
#endif
if (d1 && d2 && d3 && d4 && d5 && d6 && d7) res = LRG_CUT_1;
return(res);
}

int pass_LRG_cut2(PHPROPS *objc) {
char *name = "pass_LRG_cut2";
shAssert(objc != NULL);
float c1, c2;
RET_CODE status = get_c1_c2_for_phobjc(objc, &c1, &c2);
if (status != SH_SUCCESS) return(0);

float rPetroMag, rPetroTheta, rPetroMu, rPsfMag, rModelMag;
get_necessary_rMags(objc, &rPetroMag, &rPetroTheta, &rPetroMu, &rPsfMag, &rModelMag);

float rMag, gMag, iMag;
int gflag = 0x0, rflag = 0x0, iflag = 0x0;
get_g_r_i_flags(objc, &gflag, &rflag, &iflag);
get_g_r_i_mags(objc, &gMag, &rMag, &iMag);
if (rPetroMag == (float) VALUE_IS_BAD || rPetroMu == (float) VALUE_IS_BAD || rPsfMag == (float) VALUE_IS_BAD || rModelMag == (float) VALUE_IS_BAD) return(0);
if (rMag == (float) VALUE_IS_BAD || iMag == (float) VALUE_IS_BAD || gMag == (float) VALUE_IS_BAD) {
	/* 
	thError("%s: WARNING - not enough photometric values known", name);
	*/
	return(0);
	return(LRG_STATUS_UNKNOWN); 
}

int res = LRG_NOT, d1, d2, d3, d4, d5, d6, d7;
d1 = (rPetroMag < (float) 19.5);
d2 = (c1 > ((float) 0.449 - (gMag - rMag) / (float) 6.0));
d3 = ((gMag - rMag) > ((float) 1.296 + ((float) 0.25) * (rMag - iMag)));
d4 = (rPetroMu < (float) 24.2);
d5 = ((rPsfMag - rModelMag) >= (float) 0.4);
d6 = (rflag & LRG_BINNED) && (iflag & LRG_BINNED);
d7 = !(rflag & LRG_TOO_BRIGHT) && !(iflag & LRG_TOO_BRIGHT) && !(gflag & LRG_TOO_BRIGHT);

#if DEBUG_LRG
printf("%s: d1, d2, d3, d4, d5, d6, d7 = %d, %d, %d, %d, %d, %d, %d \n", name, d1, d2, d3, d4, d5, d6, d7);
fflush(stdout);
#endif
if (d1 && d2 && d3 && d4 && d5 && d6 && d7) res = LRG_CUT_2;
return(res);
}



int is_LRG(PHPROPS *objc) {
shAssert(objc != NULL);
int cut1 = pass_LRG_cut1(objc);
int cut2 = pass_LRG_cut2(objc);
int res = cut1 | cut2;
return(res);
}

RET_CODE thLRGSetPhprops(PHPROPS *objc) {
int flagLRG = is_LRG(objc);
RET_CODE status = thPhpropsPutFlagLRG(objc, flagLRG);
return(status);
}

RET_CODE thWObjcIoMakeRandomDeV2(WOBJC_IO *objc) {
char *name = "thWObjcIoMakeRandomDeV2";
shAssert(objc != NULL);
#if USE_FNAL_OBJC_IO

if (objc->objc_type != OBJ_GALAXY) {
	thError("%s: ERROR - expected a 'OBJ_GALAXY' type, received '%s' instead", name, shEnumNameGetFromValue("OBJ_TYPE", objc->objc_type));
	return(SH_GENERIC_ERROR);
}

int ntry = 0, continue_the_loop = 1;

while ((continue_the_loop == 1) && (ntry <= CD_NTRY_MAX)) {
float b_ratio, r_ratio, f_ratio, p1, p2, p3;
p1 = (float) phRandomUniformdev();
p2 = (float) phRandomUniformdev();
p3 = (float) phRandomUniformdev();
r_ratio = CD_R_RATIO0 * pow((float) CD_R_RATIO1 / (float) CD_R_RATIO0, p1);
b_ratio = CD_B_RATIO0 * pow((float) CD_B_RATIO1 / (float) CD_B_RATIO0, p2);
f_ratio = CD_F_RATIO0 * pow((float) CD_F_RATIO1 / (float) CD_F_RATIO0, p3);

#if 0
f_ratio = b_ratio * pow(r_ratio, 2.0)
#else
b_ratio = f_ratio / pow(r_ratio, 2.0);
#endif

int i;
float r, r2, f, f2, sb, sb2;
continue_the_loop = 0;
int nbad_bands = 0;
for (i = 0; i < NCOLOR; i++) {
	float ab = objc->ab_deV[i];
	if (ab > 1.0) ab = 1.0 / ab;
	r = objc->r_deV[i] * sqrt(ab);
	r2 = r_ratio * r;
	objc->r_deV2[i] = r_ratio * objc->r_deV[i];	
	objc->r_deV2Err[i] = r_ratio * objc->r_deVErr[i];
	f = objc->counts_deV[i];
	sb = f / pow(r, 2.0);
	#if 0
	float sb2 = sb * b_ratio;
	float f2 = sb2 * pow(r2, 2.0);
	#else	
	f2 = f * f_ratio;
	sb2 = f2 / pow(r2, 2.0);
	#endif
	objc->counts_deV2[i] = f2;
	objc->counts_model2[i] = objc->counts_deV[i] + objc->counts_deV2[i];
	
	objc->phi_deV2[i] = objc->phi_deV[i];
	objc->phi_deV2Err[i] = (float) 0.0;
	objc->ab_deV2[i] = objc->ab_deV[i];
	objc->ab_deV2Err[i] = (float) 0.0;
	objc->fracPSF[i] = (float) 1.0;	
	
	int bad_band = ((r2 > (float) CD_R_MAX) || (b_ratio >= 1.0)); 
	continue_the_loop |= bad_band;
	if (bad_band) {
		nbad_bands++;
	}
}

ntry += continue_the_loop;
if (ntry > CD_NTRY_MAX) {
	thError("%s: ERROR - tried (%d) times to make a random cD-halo but failed to satisfy the conditions", name, ntry);
	return(SH_GENERIC_ERROR);
	shAssert(ntry <= CD_NTRY_MAX);
}

}
return(SH_SUCCESS);

#else
thError("%s: ERROR - 'OBJC_IO' not supported", name);
return(SH_GENERIC_ERROR);
#endif

}

RET_CODE thWObjcIoMakeRandomPl(WOBJC_IO *objc) {
char *name = "thWObjcIoMakeRandomPl";
shAssert(objc != NULL);
#if USE_FNAL_OBJC_IO

if (objc->objc_type != OBJ_GALAXY) {
	thError("%s: ERROR - expected a 'OBJ_GALAXY' type, received '%s' instead", name, shEnumNameGetFromValue("OBJ_TYPE", objc->objc_type));
	return(SH_GENERIC_ERROR);
}

int ntry = 0, continue_the_loop = 1;

while ((continue_the_loop == 1) && (ntry <= CD_NTRY_MAX)) {
float b_ratio, r_ratio, f_ratio, p1, p2, p3;
p1 = (float) phRandomUniformdev();
p2 = (float) phRandomUniformdev();
p3 = (float) phRandomUniformdev();
r_ratio = CD_R_RATIO0 * pow((float) CD_R_RATIO1 / (float) CD_R_RATIO0, p1);
b_ratio = CD_B_RATIO0 * pow((float) CD_B_RATIO1 / (float) CD_B_RATIO0, p2);
f_ratio = CD_F_RATIO0 * pow((float) CD_F_RATIO1 / (float) CD_F_RATIO0, p3);

#if 0
f_ratio = b_ratio * pow(r_ratio, 2.0)
#else
b_ratio = f_ratio / pow(r_ratio, 2.0);
#endif

int i;
float r, r2, f, f2, sb, sb2;
continue_the_loop = 0;
int nbad_bands = 0;
for (i = 0; i < NCOLOR; i++) {
	float ab = objc->ab_deV[i];
	if (ab > 1.0) ab = 1.0 / ab;
	r = objc->r_deV[i] * sqrt(ab);
	r2 = r_ratio * r;
	objc->r_pl[i] = r_ratio * objc->r_deV[i];	
	objc->r_plErr[i] = r_ratio * objc->r_deVErr[i];
	objc->n_pl[i] = DEFAULT_PL_INDEX;
	objc->n_plErr[i] = 0.0;
	f = objc->counts_deV[i];
	sb = f / pow(r, 2.0);
	#if 0
	float sb2 = sb * b_ratio;
	float f2 = sb2 * pow(r2, 2.0);
	#else	
	f2 = f * f_ratio;
	sb2 = f2 / pow(r2, 2.0);
	#endif
	objc->counts_pl[i] = f2;
	objc->counts_model2[i] = objc->counts_deV[i] + objc->counts_pl[i];
	
	objc->phi_pl[i] = objc->phi_deV[i];
	objc->phi_plErr[i] = (float) 0.0;
	objc->ab_pl[i] = objc->ab_deV[i];
	objc->ab_plErr[i] = (float) 0.0;
	objc->fracPSF[i] = (float) 1.0;	
	
	int bad_band = ((r2 > (float) CD_R_MAX) || (b_ratio >= 1.0)); 
	continue_the_loop |= bad_band;
	if (bad_band) {
		nbad_bands++;
	}
}

ntry += continue_the_loop;
if (ntry > CD_NTRY_MAX) {
	thError("%s: ERROR - tried (%d) times to make a random cD-halo but failed to satisfy the conditions", name, ntry);
	return(SH_GENERIC_ERROR);
	shAssert(ntry <= CD_NTRY_MAX);
}

}
return(SH_SUCCESS);

#else
thError("%s: ERROR - 'OBJC_IO' not supported", name);
return(SH_GENERIC_ERROR);
#endif

}


