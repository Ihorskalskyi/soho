argument="--framefile /u/khosrow/thesis/opt/soho/multi-object/images/mle/sdss-data/CD-coresersic-G-deV-exp-fpC/stage-1/flag-fitindex-fitsize-unconstrained-multi-init-flex-adv-masking-v4-hqn/301/2708/4/parfiles/ff-002708-4-0053.par -seed 2664498490 -nolockre -fitindex -fitsize -gclass DEVEXP_GCLASS -init SDSS_INIT -cD_classify -cdclass SERSIC_GCLASS"

declare -a lr_a=(6 5 4 3 2 1)
n=6

for (( i=0; i<$n; i++ ))
do

	lr=${lr_a[$i]}
	executable="do-mle-lrg.NAG.momentum-060.lrE-$lr"
	output="./nohup.$executable.july-7-2019.out"
	cmd="$executable $argument > $output"
	echo $cmd
	$cmd
	echo "finished"

done

echo "EXIT"
