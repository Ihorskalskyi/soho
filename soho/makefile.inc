# This Makefile in adopted from a FORTRAN makefile.
#!/bin/sh
#New genuine Makefile
# Addresses
NAGDIR = $(HOME)/lib
HEALPIXDIR = $(HOME)/lib/Healpix_1.20
FITSDIR = $(HOME)/lib/cfitsio/cc
LAPACKDIR=$(HOME)/lib/lapack-3.1.0/
MKLDIR=$(MKLpath)/lib/$(MKL_PLA)
###
LAPACK	=  $(HOME)/lib/lapack-3.1.0/SRC
BLAS	=  $(HOME)/lib/lapack-3.1.0/BLAS/SRC
# Linking
FITSlink =  -L$(FITSDIR)/lib -lcfitsio -lm
NAGlink  =  -L$(NAGDIR) -lnagfl90 
LAPACKlink= -L$(LAPACKDIR)  -llapack3e -lblas
HEALPIXlink =  -L$(HEALPIXDIR)/lib -lhealpix
MKLlink =  -L$(MKLDIR) -lmkl_lapack95 -lmkl_blas95 -lmkl_lapack  \
	   -lmkl_ia32 
# Include
FITSinc  = -I$(FITSDIR)/include
NAGinc   = -I$(NAGDIR)/fl90_modules 
LAPACKinc=  -I$(LAPACK)
HEALPIXinc= -I$(HEALPIXDIR)/include
MKLinc = -I$(MKLpath)/include -I$(MKLpath)/interfaces/ -I$(MKLDIR)
# Modules

MKLmod = $(MKLDIR) #$(MKLpath)/interfaces/blas95/obj $(MKLpath)/interfaces/lapack95/obj

#LINK	= $(FITSlink) $(MKLlink) $(NAGlink) -lguide  -lpthread
#INC     = -I. $(FITSinc) $(MKLinc) $(NAGinc)
#MOD     = -module ./mod

F90C     = ifort
F90O	 = ifort -c 
FFLAGS   =  -axKW -Vaxlib #  -axiMKW -Vaxlib for ifc, ifort on della
F90FLAGS = $(FFLAGS) $(INC)

#include $(HOME)/thesis/opt/stopwatch/make.inc

# From Yen-Tin's Act Makefile.global
#ACTUTILS_CFLAGS = -I${ACTUTILS_DIR}/include -I${CFITSIO_DIR}/include -I${FFTW3_DIR}/include -I${CBLAS_DIR}/include -I${BLAS_DIR}/include -I${GSL_DIR}/include -I${PSLIB_DIR}/include/pslib -pthread -I/u/act/products/Linux64/cfitsio/3.006/include -I/u/act/products/Linux64/fftw3/3.1.2/include -I/u/act/products/Linux64/gsl/1.10/include -I/usr/include/python2.4 -I/u/act/products/Linux64/numpy/1.0.4/lib/python2.4/site-packages/numpy/core/include -DUSE_NUMPY=1 -I${WCSLIB_DIR}/include -I${CLAPACK_DIR}/include -I${LAPACK_DIR}/include
#ACTUTILS_LIBS = -L${ACTUTILS_DIR}/lib -lact -L${PSLIB_DIR}/lib -lpslib -pthread -L/u/act/products/Linux64/cfitsio/3.006/lib -lcfitsio -lm -L/u/act/products/Linux64/fftw3/3.1.2/lib -lfftw3f -lm -L/u/act/products/Linux64/gsl/1.10/lib -lgsl -lgslcblas -lm -L${FFTW3_DIR}/lib -lfftw3f -L${CFITSIO_DIR}/lib -lcfitsio -L${CBLAS_DIR}/lib -lcblas -L${BLAS_DIR}/lib -lgoto -L${GSL_DIR}/lib -lgslcblas -L/u/act/products/none/lib -L/usr/lib64/python2.4/config -lpython2.4 -L${WCSLIB_DIR}/lib -lwcs -L${CLAPACK_DIR}/lib -lclapack -L${LAPACK_DIR}/lib -llapack -lm

# MKL LIBRARY
#MKLlink = -L$(MKLlib) -lmkl_solver -lmkl_lapack95 -lmkl_blas95 -lmkl_lapack -lmkl_ipf -lmkl_lapack64 -lmkl -lguide#, -lvml}

#Note that
#libmkl_solver.a contains the sparse solver functions,
#libmkl_lapack.a, or libmkl_lapack32.so and lib_mkl_lapack64.so have the LAPACK functions.
#libmkl_ia32.a, libmkl_em64t.a, and libmkl_ia64.a have the BLAS, Sparse BLAS, GMP, FFT/DFT, VML, VSL, and interval arithmetic functions for IA-32, Intel� EM64T, and Intel Itanium processors respectively.
#libmkl_lapack95.a and libmkl_blas95.a contain LAPACK95 and BLAS95 interfaces respectively. They are not included into the original distribution and should be built before using the interface (see Note for usage of Fortran-95 interfaces and wrappers to LAPACK and BLAS after this section for details on building the libraries).

#The libmkl.so file contains the dynamically loaded versions of these objects except for VML/VSL, which are contained in libvml.so.

#In all cases, appropriate libraries will be loaded at runtime. Below are some specific examples for linking on IA-32 systems with Intel� compilers:

#ifort myprog.f -L$MKLPATH -lmkl_lapack -lmkl_ia32 -lguide -lpthread
#        static linking of user code myprog.f, LAPACK, and kernels. Processor dispatcher will call the appropriate kernel for the system at runtime.
#ifort myprog.f -L$MKLPATH -lmkl_lapack95 -lmkl_lapack -lmkl_ia32 -lguide -lpthread
#        static linking of user code myprog.f, Fortran-95 LAPACK interface, and kernels. Processor dispatcher will call the appropriate kernel for the system at runtime.
#ifort myprog.f -L$MKLPATH -lmkl_blas95 -lmkl_lapack -lmkl_ia32 -lguide -lpthread
#        static linking of user code myprog.f, Fortran-95 BLAS interface, and kernels. Processor dispatcher will call the appropriate kernel for the system at runtime.
#icc myprog.c -L$MKLPATH -lmkl_ia32 -lguide -lpthread -lm
#        static linking of user code myprog.c, BLAS, Sparse BLAS, GMP, VML/VSL, interval arithmetic, and FFT/DFT. Processor dispatcher will call the appropriate kernel for the system at runtime.
#ifort myprog.f -L$MKLPATH -lmkl_solver -lmkl_lapack -lmkl_ia32 -lguide -lpthread
#        static linking of user code myprog.f, the sparse solver, and possibly other routines within Intel MKL (including the kernels needed to support the sparse solver).
#icc myprog.c -L$MKLPATH -lmkl -lguide -lpthread
#        dynamic linking of user code myprog.c, the BLAS or FFTs within Intel MKL.

#Note: If you link libguide statically (discouraged) and

#    * use the Intel compilers, then link in the libguide version that comes with the compiler, that is, use -openmp option.
#    * do not use the Intel compiler, then link in the libguide version that comes with Intel MKL.

#If you use dynamic linking (libguide.so) of Intel MKL (recommended), make sure the LD_LIBRARY_PATH is defined so that exactly this version of libguide is found and used at runtime. 

# default: mapgen

#	$(F90C) $(F90FLAGS) \
#	$@ $(OBJ_MC) $(LINK)

#mapgen:   $(OBJ_MC) mc_alm.o
#	$(F90C) $(F90FLAGS) -o $@ $(OBJ_MC) mc_alm.o $(LINK)

#mc_alm1:   $(OBJ_MC) mc_alm.o
#	$(F90C) $(F90FLAGS) -o $@ $(OBJ_MC) mc_alm.o $(LINK)

#mc_alm2:   $(OBJ_MC) mc_alm.o
#	$(F90C) $(F90FLAGS) -o $@ $(OBJ_MC) mc_alm.o $(LINK)

#wmap_like:   $(OBJ_WM) driver_wmap_qreion.o
#	$(F90C) $(F90FLAGS) -o $@ $(OBJ_WM) driver_wmap_qreion.o $(LINK)

#Q_driver: $(OBJ_Q) Q_driver.o
#	$(FC) -o $@ $(OBJ_Q) Q_driver.o

#.f.o:
#	$(F77C) $(FFLAGS) -c $<

#.F.o:
#	$(F90C) $(FFLAGS) -c $<

#%.o: src/%.f90
#	$(F90C) $(F90FLAGS) -o $*.f90
#
#%.o: src/%.F90
#	$(F90C) $(F90FLAGS) -o $*.F90
#
#%.o: src/%.F
#	$(F90C) $(F90FLAGS) -o $*.F
#
#

