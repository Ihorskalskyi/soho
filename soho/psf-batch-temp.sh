#!/bin/bash

source "./generate_temporary_file.bash"
source "./run_commands_in_file.bash"

#commandfile="./cmd-files/temp-psf-submit-batch-3745225.cmd" #norm-0, norm-1
commandfile="./cmd-files/temp-psf-submit-batch-7454882.cmd" #norm-2, norm-3, March 31, 2015
commandfile="./temp-sim-single-submit-batch-1186897.cmd"

echo
echo "$name: randomly running commands in file '$commandfile'"
run_commands_in_file --commandfile=$commandfile --random

echo
echo "$name: not deleting temp-file = $commandfile"
#rm $commandfile
