#!/bin/bash
inflated=no
zipfile=/u/dss/sas/dr12/boss/photo/redux/301/4797/objcs/1/fpBIN-004797-r1-0133.fit.gz
destination=/scr/depot0/khosrow/scratch/data.sdss3.org/sas/dr10/env/PHOTO_REDUX/301/4797/objcs/1/fpBIN-004797-r1-0133.fit
if [ -e $destination ]; then 
	rm -f $destination
fi
if [ ! -e $destination ]; then
	gunzip < $zipfile > $destination
	inflated=yes
fi

/u/khosrow/thesis/opt/soho/do-thwart -camcol 1 -run 4797 -rerun 301 -field 0133 -framefile /u/khosrow/thesis/opt/soho/multi-object/images/sims/sdss//301/4797/1/parfiles/ff-sim-sdss-004797-1-0133.par -smearobjc -smearf 0.0 -readskyfile -seed 569833436

if [ inflated == yes ]; then
	rm -f $destination
fi
