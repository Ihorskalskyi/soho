#ifndef THREGIONTYPES_H
#define THREGIONTYPES_H

#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "shCAssert.h"
#include "shCErrStack.h"
#include "shCEnvscan.h"
#include "shCUtils.h"

#include "dervish.h"
#include "phSpanUtil.h"

#include "thConsts.h"
#include "thBasic.h"

#include "thPsfTypes.h"

#define thRegion1New thThregion1New
#define thRegion1Del thThregion1Del
#define thRegion2New thThregion2New
#define thRegion2Del thThregion2Del
#define thRegion3New thThregion3New
#define thRegion3Del thThregion3Del
#define thRegNew thThregionNew
#define thRegDel thThregionDel

typedef enum production_flag {
	UNKNOWN_PRODUCTION_FLAG, 
	RUNTIME, SAVED, N_PRODUCTION_FLAG
	} PRODUCTION_FLAG;

typedef enum frabrication_flag {
	UNKNOWN_FABRICATION_FLAG,
	INIT, PREFAB, FAB, PSFCONVOLVED,  
	N_FABRICATION_FLAG 
	} FABRICATION_FLAG;

typedef enum thregion_type {
	UNKNOWN_THREGION_TYPE,
	THREGION1_TYPE, THREGION2_TYPE, THREGION3_TYPE, 
	N_THREGION_TYPE
} THREGION_TYPE;

typedef struct thregion1 {
	REGION *reg;
	int row0, col0;
	int nrow, ncol;	
	/* psf - convolution */
	PSF_CONVOLUTION_INFO *psf_conv_info;
	NAIVE_MASK mask; /* profile generation mask */
	/* in-region's source - is it prefabricated or not */
	PRODUCTION_FLAG production;	
	/* at which level of fabrication is this object */
	FABRICATION_FLAG fab;
	/* total memory */
	MEMFL memory;
} THREGION1;	/* pragma IGNORE */

typedef struct thregion2 {
	REGION *reg[N_PSF_COMPONENT];
	int row0[N_PSF_COMPONENT], col0[N_PSF_COMPONENT];
	int nrow[N_PSF_COMPONENT], ncol[N_PSF_COMPONENT];
	THPIX a[N_PSF_COMPONENT]; /* amplitude of each region */
	/* psf convolution */
	PSF_CONVOLUTION_INFO *psf_conv_info;
	NAIVE_MASK mask[N_PSF_COMPONENT]; /* profile generation mask */
	/* in-regs source fabrication of this object are of the following type */
	PRODUCTION_FLAG production[N_PSF_COMPONENT];
	/* at which level of fabrication this object is */
	FABRICATION_FLAG fab_i[N_PSF_COMPONENT], fab;
	/* memory */
	MEMFL memory_i[N_PSF_COMPONENT], memory;
} THREGION2;	/* pragma IGNORE */

typedef struct thregion3 {
	THPIX *frow, *fcol;
	char *name;
	int row0, col0;
	int nrow, ncol;
	PSF_CONVOLUTION_INFO *psf_conv_info;
	PRODUCTION_FLAG production;
	FABRICATION_FLAG fab;
	MEMFL memory;
} THREGION3;	/* pragma IGNORE */

typedef struct thregion {
	void *threg;
	THREGION_TYPE type;
} THREGION;


THREGION1 *thThregion1New(char *name, TYPE type);
THREGION2 *thThregion2New(char *name, TYPE type);
THREGION3 *thThregion3New(char *name, TYPE type);
THREGION *thThregionNew(char *name, TYPE type, THREGION_TYPE regtype);

void thThregion1Del(THREGION1 *reg1);
void thThregion2Del(THREGION2 *reg2);
void thThregion3Del(THREGION3 *reg3);
void thThregionDel(THREGION *threg);

RET_CODE thRegRenew(THREGION *threg, char *name, TYPE type, THREGION_TYPE regtype);

RET_CODE thThregionDelByProduction(REGION *reg, PRODUCTION_FLAG production);
RET_CODE thRegFreeRowsByProduction(REGION *reg, PRODUCTION_FLAG production);


RET_CODE thReg1Reinit(THREGION1 *threg);
RET_CODE thReg2Reinit(THREGION2 *threg);
RET_CODE thReg3Reinit(THREGION3 *threg);
RET_CODE thRegReinit(THREGION *threg);

RET_CODE thReg1Prefab(THREGION1 *threg, int row0, int col0, int nrow, int ncol, PRODUCTION_FLAG production);
RET_CODE thReg2Prefab(THREGION2 *threg, int row0, int col0, int nrow, int ncol, PRODUCTION_FLAG production, int i_psf_component);
RET_CODE thReg3Prefab(THREGION3 *threg, int row0, int col0, int nrow, int ncol, PRODUCTION_FLAG production);
RET_CODE thRegPrefab(THREGION *threg, int row0, int col0, int nrow, int ncol, PRODUCTION_FLAG production, int i_psf_component);

RET_CODE thReg1PrefabFinalize(THREGION1 *threg1);
RET_CODE thReg2PrefabFinalize(THREGION2 *threg2);
RET_CODE thReg3PrefabFinalize(THREGION3 *threg3);
RET_CODE thRegPrefabFinalize(THREGION *threg);

RET_CODE thReg1Fab(THREGION1 *threg, REGION *source);
RET_CODE thReg2Fab(THREGION2 *threg, REGION *source, int i_psf_component);
RET_CODE thReg3Fab(THREGION3 *threg, void *rsource, void *csource);
RET_CODE thRegFab(THREGION *threg, REGION *source, void *rsource, void *csource, int i_psf_component);
RET_CODE thRegFabFinalize(THREGION *threg);

RET_CODE thRegFreeVectorByProduction(THPIX *fvector, PRODUCTION_FLAG production);
RET_CODE thReg1FreeRows(THREGION1 *threg);
RET_CODE thReg2FreeRows(THREGION2 *threg);
RET_CODE thReg3FreeRows(THREGION3 *threg);
RET_CODE thRegFreeRows(THREGION *threg);

RET_CODE thRegion1Get(THREGION1 *threg, 
		FABRICATION_FLAG *fab, 
		PSF_CONVOLUTION_INFO **psf_info, 
		REGION **reg, NAIVE_MASK **mask, int i);
RET_CODE thRegion2Get(THREGION2 *threg, 
		FABRICATION_FLAG *fab, 
		PSF_CONVOLUTION_INFO **psf_info, 
		REGION **reg, NAIVE_MASK **mask, THPIX *a, int i);
RET_CODE thRegion3Get(THREGION3 *threg, 
		FABRICATION_FLAG *fab, 
		PSF_CONVOLUTION_INFO **psf_info, 
		void **frow, void **fcol, int i);
RET_CODE thRegGet(THREGION *threg, 
	FABRICATION_FLAG *fab,
	PSF_CONVOLUTION_INFO **psf_info,
	REGION **reg, void **frow, void **fcol, 
	NAIVE_MASK **mask, THPIX *a, int i);
RET_CODE thRegGetFabFlag(THREGION *threg, FABRICATION_FLAG *fab);


RET_CODE thRegion1GetProduction(THREGION1 *threg, PRODUCTION_FLAG *production);
RET_CODE thRegion2GetProduction(THREGION2 *threg, PRODUCTION_FLAG *production, int index);
RET_CODE thRegion3GetProduction(THREGION3 *threg, PRODUCTION_FLAG *production);
RET_CODE thRegGetProduction(THREGION *threg, PRODUCTION_FLAG *production, int index);

RET_CODE thRegion1GetPsfConvInfo(THREGION1 *threg, PSF_CONVOLUTION_INFO **psf_info);
RET_CODE thRegion2GetPsfConvInfo(THREGION2 *threg, PSF_CONVOLUTION_INFO **psf_info);
RET_CODE thRegion3GetPsfConvInfo(THREGION3 *threg, PSF_CONVOLUTION_INFO **psf_info);
RET_CODE thRegGetPsfConvInfo(THREGION *threg, PSF_CONVOLUTION_INFO **psf_info);

RET_CODE thRegion1PutPsfConvInfo(THREGION1 *reg, PSF_CONVOLUTION_INFO *psf_info);
RET_CODE thRegion2PutPsfConvInfo(THREGION2 *reg, PSF_CONVOLUTION_INFO *psf_info);
RET_CODE thRegion3PutPsfConvInfo(THREGION3 *reg, PSF_CONVOLUTION_INFO *psf_info);
RET_CODE thRegPutPsfConvInfo(THREGION *threg, PSF_CONVOLUTION_INFO *psF_info);

RET_CODE thRegGetRegMaskA(THREGION *threg, 
			REGION **reg, NAIVE_MASK **mask, THPIX *a, FABRICATION_FLAG *fab, int i);

RET_CODE thReg1GetNComponent(THREGION1 *threg, int *nc);
RET_CODE thReg2GetNComponent(THREGION2 *threg, int *nc);
RET_CODE thReg3GetNComponent(THREGION3 *threg, int *nc);
RET_CODE thRegGetNComponent(THREGION *threg, int *nc);

RET_CODE thRegion1GetMemory(THREGION1 *threg, MEMFL *memory);
RET_CODE thRegion2GetMemory(THREGION2 *threg, MEMFL *memory);
RET_CODE thRegion3GetMemory(THREGION3 *threg, MEMFL *memory);
RET_CODE thRegGetMemory(THREGION *threg, MEMFL *memory);

RET_CODE thRegGetType(THREGION *threg, THREGION_TYPE *type);
RET_CODE thRegGetTypeAndReg(THREGION *threg, THREGION_TYPE *type, void **reg);
#endif
