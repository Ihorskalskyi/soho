#!/bin/bash
root_source=http://data.sdss3.org/sas/dr10/env
photo_redux_source=$root_source/PHOTO_REDUX
declare -a stripe82_run_list=(4797 2700 2703 2708 2709 2960 3565 3434 3437 2968 4207 1742 4247 4252 4253 4263 4288 1863 1887 2570 2578 2579 109 125 211 240 241 250 251 256 259 273 287 297 307 94 5036 5042 5052 2873 2124 1006 1009 1013 1033 1040 1055 1056 1057 4153 4157 4158 4184 4187 4188 4191 4192 2336 2886 2955 3325 3354 3355 3360 3362 3368 2385 2506 2728 4849 4858 4868 4874 4894 4895 4899 4905 4927 4930 4933 4948 2854 2855 2856 2861 2662 3256 3313 3322 4073 4198 4203 3384 3388 3427 3430 4128 4136 4145 2738 2768 2820 1752 1755 1894 2583 2585 2589 2591 2649 2650 2659 2677 3438 3460 3458 3461 3465 7674 7183)
nrun=3
#nrun=1
irun_start=0
declare -a camcol_list=(1 2 3 4 5 6)
ncamcol=6
declare -a band_list=("u" "g" "r" "i" "z")
nband=5
nfield=300

rerun=301
loop_over_field=true
for (( irun=$irun_start; irun < $nrun; irun++ )); do
jrun=$(expr $nrun-$irun-1)
run=${stripe82_run_list[$irun]}
printf -v runstr "%06d" $run

for (( icamcol=0; icamcol < $ncamcol; icamcol++ )); do
camcol=${camcol_list[$icamcol]}

destination=$MYDSS_DIR/PHOTO_REDUX/$rerun/$run/objcs/$camcol
if [ loop_over_fields ] 
then 

	for (( ifield=0; ifield < $nfield; ifield++ )); do
	printf -v field "%04d" $ifield

	psField="psField-$runstr-$camcol-$field.fit"
	#wget -d --random-wait -rkpN -e robots=off --no-parent $photo_redux_source/$rerun/$run/objcs/$camcol/$psField -O $destination/$psField
	file=$destination/$psField 
	wget --random-wait $photo_redux_source/$rerun/$run/objcs/$camcol/$psField -O $file

	fpObjc="fpObjc-$runstr-$camcol-$field.fit"
	#wget --random-wait -rkpN -e robots=off --no-parent $photo_redux_source/$rerun/$run/objcs/$camcol/$fpObjc -O $destination/$fpObjc
	file=$destination/$fpObjc
	wget --random-wait $photo_redux_source/$rerun/$run/objcs/$camcol/$fpObjc -O $file
	for (( iband=0; iband < $nband; iband++ )); do

		band=${band_list[$iband]}
		fpBIN="fpBIN-$runstr-$band$camcol-$field.fit.gz"
		#wget --random-wait -rkpN -e robots=off --no-parent $photo_redux_source/$rerun/$run/objcs/$camcol/$fpBIN -O $destination/$fpBIN
		file=$destination/$fpBIN 
		wget --random-wait $photo_redux_source/$rerun/$run/objcs/$camcol/$fpBIN -O $file
	done

	find $destination -empty -type f -delete
done

	else

	wget --random-wait -rkpN -e robots=off --no-parent -A 'psField*' $photo_redux_source/$rerun/$run/objcs/$camcol/ 
	#wget --random-wait -rkpN -e robots=off --no-parent -A 'fpObj*' $photo_redux_source/$rerun/$run/objcs/$camcol/ 

	find $destination -empty -type f -delete
fi

done

#wget --random-wait -rkpN -e robots=off --no-parent -A 'opECalib*' $photo_redux_source/$rerun/$run/logs/ 
#wget --random-wait -rkpN -e robots=off --no-parent -A 'opConfig*' $photo_redux_source/$rerun/$run/logs/

#wget --random-wait -rkpN -e robots=off --no-parent $photo_calib_source/$rerun/$run/ >> wget.photocalib.out
#wget --random-wait -rkpN -e robots=off --no-parent $photo_data_source/$run/fields/ >> wget.rawdata.out

done
