#ifndef FILENAME_H
#define FILENAME_H
/*****************************************************************************
******************************************************************************
**
** FILE:
**	fileName.h
**
** ABSTRACT:
**	This file contains all necessary definitions, macros, etc., for
**	the routines ...
**
** ENVIRONMENT:
**	ANSI C.
**
** AUTHOR:
**	Creation date: Aug. 17, 1992
**	Foghorn Leghorn
**
******************************************************************************
******************************************************************************
*/

/*----------------------------------------------------------------------------
**
** DEFINITIONS
*/
#define MY_ONE		1	/* Number one */
#define MY_TWO		3	/* Confusing number two */

/*----------------------------------------------------------------------------
**
** MY STRUCTURE
*/
typedef struct my_struct
   {
   int		me;
   int		my;
   int		mine;
   } MY_STRUCT;

#endif
