#!/bin/bash

find . -name "*.a" -print0 | while read -d $'\0' file
do
 nm $file | grep $1
 if [ $? != 1 ]; then
    ls -la $file
 fi
done
