/*
 * This file was machine generated from ../include/region.h ../include/shCHg.h ../include/shCLut.h ../include/shCArray.h ../include/shCAlign.h ../include/shCHdr.h ../include/shCTbl.h ../include/shCSchemaCnv.h ../include/shCSchemaSupport.h ../include/shCSchemaTrans.h ../include/shChain.h ../include/shCVecExpr.h 
 * on Sun Nov 29 13:17:35 2020
 *
 * Don't even think about editing it by hand
 */
#include <stdio.h>
#include <string.h>
#include "dervish_msg_c.h"
#include "region.h"
#include "shCHg.h"
#include "shCLut.h"
#include "shCArray.h"
#include "shCAlign.h"
#include "shCHdr.h"
#include "shCTbl.h"
#include "shCSchemaCnv.h"
#include "shCSchemaSupport.h"
#include "shCSchemaTrans.h"
#include "shChain.h"
#include "shCVecExpr.h"
#include "shCSchema.h"
#include "shCUtils.h"
#include "shCDiskio.h"
#include "prvt/utils_p.h"

#define NSIZE 100		/* size to read strings*/

#define _STRING(S) #S
#define STRING(S) _STRING(S)		/* convert a cpp value to a string*/

static SCHEMA_ELEM schema_elems0[] = { /* PIXDATATYPE */
   { "TYPE_U8", "PIXDATATYPE", (int) TYPE_U8, 0, 0, NULL, 0, 0 },
   { "TYPE_S8", "PIXDATATYPE", (int) TYPE_S8, 0, 0, NULL, 0, 0 },
   { "TYPE_U16", "PIXDATATYPE", (int) TYPE_U16, 0, 0, NULL, 0, 0 },
   { "TYPE_S16", "PIXDATATYPE", (int) TYPE_S16, 0, 0, NULL, 0, 0 },
   { "TYPE_U32", "PIXDATATYPE", (int) TYPE_U32, 0, 0, NULL, 0, 0 },
   { "TYPE_S32", "PIXDATATYPE", (int) TYPE_S32, 0, 0, NULL, 0, 0 },
   { "TYPE_FL32", "PIXDATATYPE", (int) TYPE_FL32, 0, 0, NULL, 0, 0 },
   { "TYPE_FL64", "PIXDATATYPE", (int) TYPE_FL64, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems1[] = { /* REGION_FLAGS */
   { "NO_FLAGS", "REGION_FLAGS", (int) NO_FLAGS, 0, 0, NULL, 0, 0 },
   { "READ_ONLY", "REGION_FLAGS", (int) READ_ONLY, 0, 0, NULL, 0, 0 },
   { "COPY_MASK", "REGION_FLAGS", (int) COPY_MASK, 0, 0, NULL, 0, 0 },
   { "ENLARGE_EXACT", "REGION_FLAGS", (int) ENLARGE_EXACT, 0, 0, NULL, 0, 0 },
   { "COPY_HEADER_DEEP", "REGION_FLAGS", (int) COPY_HEADER_DEEP, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems2[] = { /* MASK */
   { "name", "char", 0, 1, 0, NULL, 0, offsetof(MASK,name) },
   { "nrow", "int", 0, 0, 0, NULL, 0, offsetof(MASK,nrow) },
   { "ncol", "int", 0, 0, 0, NULL, 0, offsetof(MASK,ncol) },
   { "rows", "unsigned char", 0, 2, 0, NULL, 0, offsetof(MASK,rows) },
   { "row0", "int", 0, 0, 0, NULL, 0, offsetof(MASK,row0) },
   { "col0", "int", 0, 0, 0, NULL, 0, offsetof(MASK,col0) },
   { "prvt", "struct mask_p", 0, 1, 0, NULL, 0, offsetof(MASK,prvt) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems3[] = { /* REGION */
   { "name", "char", 0, 1, 0, NULL, 0, offsetof(REGION,name) },
   { "nrow", "int", 0, 0, 0, NULL, 0, offsetof(REGION,nrow) },
   { "ncol", "int", 0, 0, 0, NULL, 0, offsetof(REGION,ncol) },
   { "type", "PIXDATATYPE", 0, 0, 0, NULL, 0, offsetof(REGION,type) },
   { "rows", "U16", 0, 2, 0, NULL, 0, offsetof(REGION,rows) },
   { "rows_u8", "U8", 0, 2, 0, NULL, 0, offsetof(REGION,rows_u8) },
   { "rows_s8", "S8", 0, 2, 0, NULL, 0, offsetof(REGION,rows_s8) },
   { "rows_u16", "U16", 0, 2, 0, NULL, 0, offsetof(REGION,rows_u16) },
   { "rows_s16", "S16", 0, 2, 0, NULL, 0, offsetof(REGION,rows_s16) },
   { "rows_u32", "U32", 0, 2, 0, NULL, 0, offsetof(REGION,rows_u32) },
   { "rows_s32", "S32", 0, 2, 0, NULL, 0, offsetof(REGION,rows_s32) },
   { "rows_fl32", "FL32", 0, 2, 0, NULL, 0, offsetof(REGION,rows_fl32) },
   { "mask", "MASK", 0, 1, 0, NULL, 0, offsetof(REGION,mask) },
   { "row0", "int", 0, 0, 0, NULL, 0, offsetof(REGION,row0) },
   { "col0", "int", 0, 0, 0, NULL, 0, offsetof(REGION,col0) },
   { "hdr", "HDR", 0, 0, 0, NULL, 0, offsetof(REGION,hdr) },
   { "prvt", "struct region_p", 0, 1, 0, NULL, 0, offsetof(REGION,prvt) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems4[] = { /* REGINFO */
   { "modCntr", "unsigned int", 0, 0, 0, NULL, 0, offsetof(REGINFO,modCntr) },
   { "crc", "unsigned int", 0, 0, 0, NULL, 0, offsetof(REGINFO,crc) },
   { "parent", "REGION", 0, 1, 0, NULL, 0, offsetof(REGINFO,parent) },
   { "isSubReg", "unsigned char", 0, 0, 0, NULL, 0, offsetof(REGINFO,isSubReg) },
   { "isPhysical", "unsigned char", 0, 0, 0, NULL, 0, offsetof(REGINFO,isPhysical) },
   { "pxAreContiguous", "unsigned char", 0, 0, 0, NULL, 0, offsetof(REGINFO,pxAreContiguous) },
   { "hasHeader", "unsigned char", 0, 0, 0, NULL, 0, offsetof(REGINFO,hasHeader) },
   { "headerModCntr", "unsigned int", 0, 0, 0, NULL, 0, offsetof(REGINFO,headerModCntr) },
   { "hasMask", "unsigned char", 0, 0, 0, NULL, 0, offsetof(REGINFO,hasMask) },
   { "nSubReg", "unsigned int", 0, 0, 0, NULL, 0, offsetof(REGINFO,nSubReg) },
   { "subRegs", "REGION", 0, 1, 0, STRING(DEFSUBREGSIZE), 0, offsetof(REGINFO,subRegs) },
   { "physicalRegNum", "int", 0, 0, 0, NULL, 0, offsetof(REGINFO,physicalRegNum) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems5[] = { /* MASKINFO */
   { "crc", "unsigned int", 0, 0, 0, NULL, 0, offsetof(MASKINFO,crc) },
   { "parent", "MASK", 0, 1, 0, NULL, 0, offsetof(MASKINFO,parent) },
   { "isSubMask", "unsigned char", 0, 0, 0, NULL, 0, offsetof(MASKINFO,isSubMask) },
   { "pxAreContiguous", "unsigned char", 0, 0, 0, NULL, 0, offsetof(MASKINFO,pxAreContiguous) },
   { "nSubMask", "unsigned int", 0, 0, 0, NULL, 0, offsetof(MASKINFO,nSubMask) },
   { "subMasks", "MASK", 0, 1, 0, STRING(DEFSUBMASKSIZE), 0, offsetof(MASKINFO,subMasks) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems6[] = { /* HG */
   { "id", "int", 0, 0, 0, NULL, 0, offsetof(HG,id) },
   { "minimum", "double", 0, 0, 0, NULL, 0, offsetof(HG,minimum) },
   { "maximum", "double", 0, 0, 0, NULL, 0, offsetof(HG,maximum) },
   { "name", "char", 0, 0, 0, STRING(HGLABELSIZE), 0, offsetof(HG,name) },
   { "xLabel", "char", 0, 0, 0, STRING(HGLABELSIZE), 0, offsetof(HG,xLabel) },
   { "yLabel", "char", 0, 0, 0, STRING(HGLABELSIZE), 0, offsetof(HG,yLabel) },
   { "nbin", "unsigned int", 0, 0, 0, NULL, 0, offsetof(HG,nbin) },
   { "contents", "float", 0, 1, 0, NULL, 0, offsetof(HG,contents) },
   { "error", "float", 0, 1, 0, NULL, 0, offsetof(HG,error) },
   { "binPosition", "float", 0, 1, 0, NULL, 0, offsetof(HG,binPosition) },
   { "underflow", "double", 0, 0, 0, NULL, 0, offsetof(HG,underflow) },
   { "overflow", "double", 0, 0, 0, NULL, 0, offsetof(HG,overflow) },
   { "entries", "unsigned int", 0, 0, 0, NULL, 0, offsetof(HG,entries) },
   { "wsum", "double", 0, 0, 0, NULL, 0, offsetof(HG,wsum) },
   { "sum", "double", 0, 0, 0, NULL, 0, offsetof(HG,sum) },
   { "sum2", "double", 0, 0, 0, NULL, 0, offsetof(HG,sum2) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems7[] = { /* REAL */
   { "value", "double", 0, 0, 0, NULL, 0, offsetof(REAL,value) },
   { "error", "double", 0, 0, 0, NULL, 0, offsetof(REAL,error) },
   { "masked", "unsigned short", 0, 0, 0, NULL, 0, offsetof(REAL,masked) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems8[] = { /* PT */
   { "id", "int", 0, 0, 0, NULL, 0, offsetof(PT,id) },
   { "row", "float", 0, 0, 0, NULL, 0, offsetof(PT,row) },
   { "col", "float", 0, 0, 0, NULL, 0, offsetof(PT,col) },
   { "radius", "float", 0, 0, 0, NULL, 0, offsetof(PT,radius) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems9[] = { /* PGSTATE */
   { "id", "int", 0, 0, 0, NULL, 0, offsetof(PGSTATE,id) },
   { "device", "char", 0, 0, 0, STRING(HG_DEVICE_NAMELEN), 0, offsetof(PGSTATE,device) },
   { "nxwindow", "int", 0, 0, 0, NULL, 0, offsetof(PGSTATE,nxwindow) },
   { "nywindow", "int", 0, 0, 0, NULL, 0, offsetof(PGSTATE,nywindow) },
   { "xfract", "float", 0, 0, 0, NULL, 0, offsetof(PGSTATE,xfract) },
   { "yfract", "float", 0, 0, 0, NULL, 0, offsetof(PGSTATE,yfract) },
   { "ixwindow", "int", 0, 0, 0, NULL, 0, offsetof(PGSTATE,ixwindow) },
   { "iywindow", "int", 0, 0, 0, NULL, 0, offsetof(PGSTATE,iywindow) },
   { "just", "int", 0, 0, 0, NULL, 0, offsetof(PGSTATE,just) },
   { "axis", "int", 0, 0, 0, NULL, 0, offsetof(PGSTATE,axis) },
   { "full", "int", 0, 0, 0, NULL, 0, offsetof(PGSTATE,full) },
   { "xopt", "char", 0, 0, 0, STRING(HG_OPT_LENGTH), 0, offsetof(PGSTATE,xopt) },
   { "yopt", "char", 0, 0, 0, STRING(HG_OPT_LENGTH), 0, offsetof(PGSTATE,yopt) },
   { "xtick", "float", 0, 0, 0, NULL, 0, offsetof(PGSTATE,xtick) },
   { "ytick", "float", 0, 0, 0, NULL, 0, offsetof(PGSTATE,ytick) },
   { "nxsub", "int", 0, 0, 0, NULL, 0, offsetof(PGSTATE,nxsub) },
   { "nysub", "int", 0, 0, 0, NULL, 0, offsetof(PGSTATE,nysub) },
   { "hg", "HG", 0, 1, 0, NULL, 0, offsetof(PGSTATE,hg) },
   { "xSchemaName", "char", 0, 0, 0, STRING(HG_SCHEMA_LENGTH), 0, offsetof(PGSTATE,xSchemaName) },
   { "ySchemaName", "char", 0, 0, 0, STRING(HG_SCHEMA_LENGTH), 0, offsetof(PGSTATE,ySchemaName) },
   { "plotType", "char", 0, 0, 0, STRING(10), 0, offsetof(PGSTATE,plotType) },
   { "plotTitle", "char", 0, 0, 0, STRING(HG_TITLE_LENGTH), 0, offsetof(PGSTATE,plotTitle) },
   { "symb", "int", 0, 0, 0, NULL, 0, offsetof(PGSTATE,symb) },
   { "isLine", "int", 0, 0, 0, NULL, 0, offsetof(PGSTATE,isLine) },
   { "isTime", "int", 0, 0, 0, NULL, 0, offsetof(PGSTATE,isTime) },
   { "icMark", "int", 0, 0, 0, NULL, 0, offsetof(PGSTATE,icMark) },
   { "icLine", "int", 0, 0, 0, NULL, 0, offsetof(PGSTATE,icLine) },
   { "icBox", "int", 0, 0, 0, NULL, 0, offsetof(PGSTATE,icBox) },
   { "icLabel", "int", 0, 0, 0, NULL, 0, offsetof(PGSTATE,icLabel) },
   { "icError", "int", 0, 0, 0, NULL, 0, offsetof(PGSTATE,icError) },
   { "lineWidth", "int", 0, 0, 0, NULL, 0, offsetof(PGSTATE,lineWidth) },
   { "lineStyle", "int", 0, 0, 0, NULL, 0, offsetof(PGSTATE,lineStyle) },
   { "isNewplot", "int", 0, 0, 0, NULL, 0, offsetof(PGSTATE,isNewplot) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems10[] = { /* LUTTYPE */
   { "LUTUNKNOWN", "LUTTYPE", (int) LUTUNKNOWN, 0, 0, NULL, 0, 0 },
   { "LUT8", "LUTTYPE", (int) LUT8, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems11[] = { /* LUT */
   { "name", "char", 0, 1, 0, NULL, 0, offsetof(LUT,name) },
   { "type", "LUTTYPE", 0, 0, 0, NULL, 0, offsetof(LUT,type) },
   { "lutArray", "int", 0, 0, 0, STRING(LUT8TOP), 0, offsetof(LUT,lutArray) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems12[] = { /* ARRAY_DATA */
   { "dataPtr", "unsigned char", 0, 1, 0, NULL, 0, offsetof(ARRAY_DATA,dataPtr) },
   { "schemaType", "TYPE", 0, 0, 0, NULL, 0, offsetof(ARRAY_DATA,schemaType) },
   { "size", "unsigned long", 0, 0, 0, NULL, 0, offsetof(ARRAY_DATA,size) },
   { "align", "unsigned int", 0, 0, 0, NULL, 0, offsetof(ARRAY_DATA,align) },
   { "incr", "unsigned int", 0, 0, 0, NULL, 0, offsetof(ARRAY_DATA,incr) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems13[] = { /* ARRAY */
   { "dimCnt", "int", 0, 0, 0, NULL, 0, offsetof(ARRAY,dimCnt) },
   { "dim", "int", 0, 0, 0, STRING(shArrayDimLim), 0, offsetof(ARRAY,dim) },
   { "subArrCnt", "long", 0, 0, 0, STRING(shArrayDimLim), 0, offsetof(ARRAY,subArrCnt) },
   { "arrayPtr", "void", 0, 1, 0, NULL, 0, offsetof(ARRAY,arrayPtr) },
   { "data", "ARRAY_DATA", 0, 0, 0, NULL, 0, offsetof(ARRAY,data) },
   { "nStar", "int", 0, 0, 0, NULL, 0, offsetof(ARRAY,nStar) },
   { "info", "void", 0, 1, 0, NULL, 0, offsetof(ARRAY,info) },
   { "infoType", "TYPE", 0, 0, 0, NULL, 0, offsetof(ARRAY,infoType) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems14[] = { /* SHALIGN_TYPE */
   { "SHALIGNTYPE_unknown", "SHALIGN_TYPE", (int) SHALIGNTYPE_unknown, 0, 0, NULL, 0, 0 },
   { "SHALIGNTYPE_U8", "SHALIGN_TYPE", (int) SHALIGNTYPE_U8, 0, 0, NULL, 0, 0 },
   { "SHALIGNTYPE_S8", "SHALIGN_TYPE", (int) SHALIGNTYPE_S8, 0, 0, NULL, 0, 0 },
   { "SHALIGNTYPE_U16", "SHALIGN_TYPE", (int) SHALIGNTYPE_U16, 0, 0, NULL, 0, 0 },
   { "SHALIGNTYPE_S16", "SHALIGN_TYPE", (int) SHALIGNTYPE_S16, 0, 0, NULL, 0, 0 },
   { "SHALIGNTYPE_U32", "SHALIGN_TYPE", (int) SHALIGNTYPE_U32, 0, 0, NULL, 0, 0 },
   { "SHALIGNTYPE_S32", "SHALIGN_TYPE", (int) SHALIGNTYPE_S32, 0, 0, NULL, 0, 0 },
   { "SHALIGNTYPE_FL32", "SHALIGN_TYPE", (int) SHALIGNTYPE_FL32, 0, 0, NULL, 0, 0 },
   { "SHALIGNTYPE_FL64", "SHALIGN_TYPE", (int) SHALIGNTYPE_FL64, 0, 0, NULL, 0, 0 },
   { "SHALIGNCNT", "SHALIGN_TYPE", (int) SHALIGNCNT, 0, 0, NULL, 0, 0 },
   { "SHALIGNTYPE_ptrData", "SHALIGN_TYPE", (int) SHALIGNTYPE_ptrData, 0, 0, NULL, 0, 0 },
   { "SHALIGNCNTALL", "SHALIGN_TYPE", (int) SHALIGNCNTALL, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems15[] = { /* HDR */
   { "modCnt", "unsigned int", 0, 0, 0, NULL, 0, offsetof(HDR,modCnt) },
   { "hdrVec", "HDR_VEC", 0, 1, 0, NULL, 0, offsetof(HDR,hdrVec) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems16[] = { /* TBLCOL */
   { "rowCnt", "int", 0, 0, 0, NULL, 0, offsetof(TBLCOL,rowCnt) },
   { "hdr", "HDR", 0, 0, 0, NULL, 0, offsetof(TBLCOL,hdr) },
   { "fld", "CHAIN", 0, 0, 0, NULL, 0, offsetof(TBLCOL,fld) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems17[] = { /* TBLHEAPDSC */
   { "cnt", "int", 0, 0, 0, NULL, 0, offsetof(TBLHEAPDSC,cnt) },
   { "ptr", "unsigned char", 0, 1, 0, NULL, 0, offsetof(TBLHEAPDSC,ptr) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems18[] = { /* TBLFLD_KEYWORDS */
   { "SH_TBL_HEAP", "TBLFLD_KEYWORDS", (int) SH_TBL_HEAP, 0, 0, NULL, 0, 0 },
   { "SH_TBL_TTYPE", "TBLFLD_KEYWORDS", (int) SH_TBL_TTYPE, 0, 0, NULL, 0, 0 },
   { "SH_TBL_TUNIT", "TBLFLD_KEYWORDS", (int) SH_TBL_TUNIT, 0, 0, NULL, 0, 0 },
   { "SH_TBL_TSCAL", "TBLFLD_KEYWORDS", (int) SH_TBL_TSCAL, 0, 0, NULL, 0, 0 },
   { "SH_TBL_TZERO", "TBLFLD_KEYWORDS", (int) SH_TBL_TZERO, 0, 0, NULL, 0, 0 },
   { "SH_TBL_TNULLSTR", "TBLFLD_KEYWORDS", (int) SH_TBL_TNULLSTR, 0, 0, NULL, 0, 0 },
   { "SH_TBL_TNULLINT", "TBLFLD_KEYWORDS", (int) SH_TBL_TNULLINT, 0, 0, NULL, 0, 0 },
   { "SH_TBL_TDISP", "TBLFLD_KEYWORDS", (int) SH_TBL_TDISP, 0, 0, NULL, 0, 0 },
   { "SH_TBL_TDIM", "TBLFLD_KEYWORDS", (int) SH_TBL_TDIM, 0, 0, NULL, 0, 0 },
   { "SH_TBL_TFORM", "TBLFLD_KEYWORDS", (int) SH_TBL_TFORM, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems19[] = { /* TBLFLD_TFORM */
   { "ascFldWidth", "int", 0, 0, 0, NULL, 0, offsetof(TBLFLD_TFORM,ascFldWidth) },
   { "ascFldDecPt", "int", 0, 0, 0, NULL, 0, offsetof(TBLFLD_TFORM,ascFldDecPt) },
   { "ascFldFmt", "char", 0, 0, 0, NULL, 0, offsetof(TBLFLD_TFORM,ascFldFmt) },
   { "ascFldOff", "int", 0, 0, 0, NULL, 0, offsetof(TBLFLD_TFORM,ascFldOff) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems20[] = { /* TBLFLD_PRVT */
   { "datumPtr", "unsigned char", 0, 1, 0, NULL, 0, offsetof(TBLFLD_PRVT,datumPtr) },
   { "schemaCnt", "int", 0, 0, 0, NULL, 0, offsetof(TBLFLD_PRVT,schemaCnt) },
   { "elemCnt", "int", 0, 0, 0, NULL, 0, offsetof(TBLFLD_PRVT,elemCnt) },
   { "dataType", "char", 0, 0, 0, NULL, 0, offsetof(TBLFLD_PRVT,dataType) },
   { "signFlip", "unsigned char", 0, 0, 0, NULL, 0, offsetof(TBLFLD_PRVT,signFlip) },
   { "TFORM", "TBLFLD_TFORM", 0, 0, 0, NULL, 0, offsetof(TBLFLD_PRVT,TFORM) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems21[] = { /* TBLFLD */
   { "Tpres", "unsigned long", 0, 0, 0, NULL, 0, offsetof(TBLFLD,Tpres) },
   { "TTYPE", "char", 0, 0, 0, STRING(69), 0, offsetof(TBLFLD,TTYPE) },
   { "TUNIT", "char", 0, 0, 0, STRING(69), 0, offsetof(TBLFLD,TUNIT) },
   { "TDISP", "char", 0, 0, 0, STRING(69), 0, offsetof(TBLFLD,TDISP) },
   { "TNULLSTR", "char", 0, 0, 0, STRING(69), 0, offsetof(TBLFLD,TNULLSTR) },
   { "TNULLINT", "int", 0, 0, 0, NULL, 0, offsetof(TBLFLD,TNULLINT) },
   { "TSCAL", "double", 0, 0, 0, NULL, 0, offsetof(TBLFLD,TSCAL) },
   { "TZERO", "double", 0, 0, 0, NULL, 0, offsetof(TBLFLD,TZERO) },
   { "array", "ARRAY", 0, 1, 0, NULL, 0, offsetof(TBLFLD,array) },
   { "heap", "ARRAY_DATA", 0, 0, 0, NULL, 0, offsetof(TBLFLD,heap) },
   { "prvt", "TBLFLD_PRVT", 0, 0, 0, NULL, 0, offsetof(TBLFLD,prvt) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems22[] = { /* TRANSTEST */
   { "ia", "int", 0, 0, 0, NULL, 0, offsetof(TRANSTEST,ia) },
   { "ib", "int", 0, 0, 0, STRING(10), 0, offsetof(TRANSTEST,ib) },
   { "dbl", "double", 0, 0, 0, NULL, 0, offsetof(TRANSTEST,dbl) },
   { "fa", "float", 0, 0, 0, NULL, 0, offsetof(TRANSTEST,fa) },
   { "fb", "float", 0, 0, 0, STRING(20), 0, offsetof(TRANSTEST,fb) },
   { "cb", "char", 0, 0, 0, STRING(20), 0, offsetof(TRANSTEST,cb) },
   { "heap", "int", 0, 2, 0, STRING(3) " " STRING(2), 0, offsetof(TRANSTEST,heap) },
   { "ra", "REGION", 0, 1, 0, NULL, 0, offsetof(TRANSTEST,ra) },
   { "rb", "REGION", 0, 2, 0, NULL, 0, offsetof(TRANSTEST,rb) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems23[] = { /* CONTAINER_TYPE */
   { "NOT_A_CONTAINER", "CONTAINER_TYPE", (int) NOT_A_CONTAINER, 0, 0, NULL, 0, 0 },
   { "LIST_TYPE", "CONTAINER_TYPE", (int) LIST_TYPE, 0, 0, NULL, 0, 0 },
   { "ARRAY_TYPE", "CONTAINER_TYPE", (int) ARRAY_TYPE, 0, 0, NULL, 0, 0 },
   { "CHAIN_TYPE", "CONTAINER_TYPE", (int) CHAIN_TYPE, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems24[] = { /* CONVTYPE */
   { "CONVERSION_END", "CONVTYPE", (int) CONVERSION_END, 0, 0, NULL, 0, 0 },
   { "CONVERSION_UNKNOWN", "CONVTYPE", (int) CONVERSION_UNKNOWN, 0, 0, NULL, 0, 0 },
   { "CONVERSION_BY_TYPE", "CONVTYPE", (int) CONVERSION_BY_TYPE, 0, 0, NULL, 0, 0 },
   { "CONVERSION_IGNORE_TYPE", "CONVTYPE", (int) CONVERSION_IGNORE_TYPE, 0, 0, NULL, 0, 0 },
   { "CONVERSION_CONTINUE", "CONVTYPE", (int) CONVERSION_CONTINUE, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems25[] = { /* DSTTYPE */
   { "DST_UNKNOWN", "DSTTYPE", (int) DST_UNKNOWN, 0, 0, NULL, 0, 0 },
   { "DST_STR", "DSTTYPE", (int) DST_STR, 0, 0, NULL, 0, 0 },
   { "DST_CHAR", "DSTTYPE", (int) DST_CHAR, 0, 0, NULL, 0, 0 },
   { "DST_SHORT", "DSTTYPE", (int) DST_SHORT, 0, 0, NULL, 0, 0 },
   { "DST_INT", "DSTTYPE", (int) DST_INT, 0, 0, NULL, 0, 0 },
   { "DST_LONG", "DSTTYPE", (int) DST_LONG, 0, 0, NULL, 0, 0 },
   { "DST_FLOAT", "DSTTYPE", (int) DST_FLOAT, 0, 0, NULL, 0, 0 },
   { "DST_DOUBLE", "DSTTYPE", (int) DST_DOUBLE, 0, 0, NULL, 0, 0 },
   { "DST_SCHAR", "DSTTYPE", (int) DST_SCHAR, 0, 0, NULL, 0, 0 },
   { "DST_UCHAR", "DSTTYPE", (int) DST_UCHAR, 0, 0, NULL, 0, 0 },
   { "DST_USHORT", "DSTTYPE", (int) DST_USHORT, 0, 0, NULL, 0, 0 },
   { "DST_UINT", "DSTTYPE", (int) DST_UINT, 0, 0, NULL, 0, 0 },
   { "DST_ULONG", "DSTTYPE", (int) DST_ULONG, 0, 0, NULL, 0, 0 },
   { "DST_ENUM", "DSTTYPE", (int) DST_ENUM, 0, 0, NULL, 0, 0 },
   { "DST_HEAP", "DSTTYPE", (int) DST_HEAP, 0, 0, NULL, 0, 0 },
   { "DST_STRUCT", "DSTTYPE", (int) DST_STRUCT, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems26[] = { /* SCHTR_STATUS */
   { "SCHTR_VISITED", "SCHTR_STATUS", (int) SCHTR_VISITED, 0, 0, NULL, 0, 0 },
   { "SCHTR_FRESH", "SCHTR_STATUS", (int) SCHTR_FRESH, 0, 0, NULL, 0, 0 },
   { "SCHTR_GOOD", "SCHTR_STATUS", (int) SCHTR_GOOD, 0, 0, NULL, 0, 0 },
   { "SCHTR_VGOOD", "SCHTR_STATUS", (int) SCHTR_VGOOD, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems27[] = { /* SCHEMATRANS_ENTRY */
   { "type", "CONVTYPE", 0, 0, 0, NULL, 0, offsetof(SCHEMATRANS_ENTRY,type) },
   { "src", "char", 0, 1, 0, NULL, 0, offsetof(SCHEMATRANS_ENTRY,src) },
   { "dst", "char", 0, 1, 0, NULL, 0, offsetof(SCHEMATRANS_ENTRY,dst) },
   { "dsttype", "char", 0, 1, 0, NULL, 0, offsetof(SCHEMATRANS_ENTRY,dsttype) },
   { "heaptype", "char", 0, 1, 0, NULL, 0, offsetof(SCHEMATRANS_ENTRY,heaptype) },
   { "heaplen", "char", 0, 1, 0, NULL, 0, offsetof(SCHEMATRANS_ENTRY,heaplen) },
   { "dstDataType", "DSTTYPE", 0, 0, 0, NULL, 0, offsetof(SCHEMATRANS_ENTRY,dstDataType) },
   { "proc", "char", 0, 1, 0, NULL, 0, offsetof(SCHEMATRANS_ENTRY,proc) },
   { "size", "char", 0, 1, 0, NULL, 0, offsetof(SCHEMATRANS_ENTRY,size) },
   { "num", "int", 0, 0, 0, STRING(MAX_INDIRECTION), 0, offsetof(SCHEMATRANS_ENTRY,num) },
   { "srcTodst", "double", 0, 0, 0, NULL, 0, offsetof(SCHEMATRANS_ENTRY,srcTodst) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems28[] = { /* SCHTR_HASH */
   { "totalNum", "int", 0, 0, 0, NULL, 0, offsetof(SCHTR_HASH,totalNum) },
   { "curNum", "int", 0, 0, 0, NULL, 0, offsetof(SCHTR_HASH,curNum) },
   { "entries", "int", 0, 1, 0, NULL, 0, offsetof(SCHTR_HASH,entries) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems29[] = { /* SCHEMATRANS */
   { "totalNum", "int", 0, 0, 0, NULL, 0, offsetof(SCHEMATRANS,totalNum) },
   { "entryNum", "int", 0, 0, 0, NULL, 0, offsetof(SCHEMATRANS,entryNum) },
   { "entryPtr", "SCHEMATRANS_ENTRY", 0, 1, 0, NULL, 0, offsetof(SCHEMATRANS,entryPtr) },
   { "status", "SCHTR_STATUS", 0, 1, 0, NULL, 0, offsetof(SCHEMATRANS,status) },
   { "hash", "SCHTR_HASH", 0, 0, 0, STRING(SCHTRS_HASH_TBLLEN), 0, offsetof(SCHEMATRANS,hash) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems30[] = { /* CHAIN_ADD_FLAGS */
   { "BEFORE", "CHAIN_ADD_FLAGS", (int) BEFORE, 0, 0, NULL, 0, 0 },
   { "AFTER", "CHAIN_ADD_FLAGS", (int) AFTER, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems31[] = { /* CHAIN_WALK_FLAGS */
   { "PREVIOUS", "CHAIN_WALK_FLAGS", (int) PREVIOUS, 0, 0, NULL, 0, 0 },
   { "THIS", "CHAIN_WALK_FLAGS", (int) THIS, 0, 0, NULL, 0, 0 },
   { "NEXT", "CHAIN_WALK_FLAGS", (int) NEXT, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems32[] = { /* CURSOR_STATUS */
   { "NOT_ALLOCATED", "CURSOR_STATUS", (int) NOT_ALLOCATED, 0, 0, NULL, 0, 0 },
   { "ALLOCATED", "CURSOR_STATUS", (int) ALLOCATED, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems33[] = { /* CHAIN_ELEM */
   { "pElement", "void", 0, 1, 0, NULL, 0, offsetof(CHAIN_ELEM,pElement) },
   { "type", "TYPE", 0, 0, 0, NULL, 0, offsetof(CHAIN_ELEM,type) },
   { "pPrev", "CHAIN_ELEM", 0, 1, 0, NULL, 0, offsetof(CHAIN_ELEM,pPrev) },
   { "pNext", "CHAIN_ELEM", 0, 1, 0, NULL, 0, offsetof(CHAIN_ELEM,pNext) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems34[] = { /* CURSOR */
   { "pChainElement", "CHAIN_ELEM", 0, 1, 0, NULL, 0, offsetof(CURSOR,pChainElement) },
   { "pNext", "CHAIN_ELEM", 0, 1, 0, NULL, 0, offsetof(CURSOR,pNext) },
   { "pPrev", "CHAIN_ELEM", 0, 1, 0, NULL, 0, offsetof(CURSOR,pPrev) },
   { "gotFirstElem", "short", 0, 0, 0, NULL, 0, offsetof(CURSOR,gotFirstElem) },
   { "status", "CURSOR_STATUS", 0, 0, 0, NULL, 0, offsetof(CURSOR,status) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems35[] = { /* CHAIN */
   { "nElements", "unsigned int", 0, 0, 0, NULL, 0, offsetof(CHAIN,nElements) },
   { "type", "TYPE", 0, 0, 0, NULL, 0, offsetof(CHAIN,type) },
   { "pFirst", "CHAIN_ELEM", 0, 1, 0, NULL, 0, offsetof(CHAIN,pFirst) },
   { "pLast", "CHAIN_ELEM", 0, 1, 0, NULL, 0, offsetof(CHAIN,pLast) },
   { "pCacheElem", "CHAIN_ELEM", 0, 1, 0, NULL, 0, offsetof(CHAIN,pCacheElem) },
   { "cacheElemPos", "unsigned int", 0, 0, 0, NULL, 0, offsetof(CHAIN,cacheElemPos) },
   { "index", "CHAIN_ELEM", 0, 2, 0, NULL, 0, offsetof(CHAIN,index) },
   { "chainCursor", "CURSOR", 0, 2, 0, NULL, 0, offsetof(CHAIN,chainCursor) },
   { "max_cursors", "int", 0, 0, 0, NULL, 0, offsetof(CHAIN,max_cursors) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems36[] = { /* GENERIC */
   { "dummy", "void", 0, 1, 0, NULL, 0, offsetof(GENERIC,dummy) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems37[] = { /* VECTOR */
   { "name", "char", 0, 0, 0, STRING(VECTOR_NAME_LENGTH), 0, offsetof(VECTOR,name) },
   { "dimen", "int", 0, 0, 0, NULL, 0, offsetof(VECTOR,dimen) },
   { "vec", "double", 0, 1, 0, NULL, 0, offsetof(VECTOR,vec) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems38[] = { /* TYPE */
   { "", "TYPE", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems39[] = { /* CHAR */
   { "", "char", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems40[] = { /* UCHAR */
   { "", "unsigned char", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems41[] = { /* INT */
   { "", "int", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems42[] = { /* UINT */
   { "", "unsigned int", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems43[] = { /* SHORT */
   { "", "short", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems44[] = { /* USHORT */
   { "", "unsigned short", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems45[] = { /* LONG */
   { "", "long", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems46[] = { /* ULONG */
   { "", "unsigned long", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems47[] = { /* FLOAT */
   { "", "float", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems48[] = { /* DOUBLE */
   { "", "double", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems49[] = { /* LOGICAL */
   { "", "logical", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems50[] = { /* STR */
   { "", "char", 0, 1, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems51[] = { /* PTR */
   { "", "ptr", 0, 1, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems52[] = { /* FUNCTION */
   { "", "function", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems53[] = { /* SCHAR */
   { "", "signed char", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA schema[] = {
   { "PIXDATATYPE", ENUM, sizeof(PIXDATATYPE), 8, schema_elems0,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "REGION_FLAGS", ENUM, sizeof(REGION_FLAGS), 5, schema_elems1,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "MASK", STRUCT, sizeof(MASK), 7, schema_elems2,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))shDumpMaskRead,
     (RET_CODE(*)(FILE *,void *))shDumpMaskWrite,
     0,
   },
   { "REGION", STRUCT, sizeof(REGION), 17, schema_elems3,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))shDumpRegRead,
     (RET_CODE(*)(FILE *,void *))shDumpRegWrite,
     1,
   },
   { "REGINFO", STRUCT, sizeof(REGINFO), 12, schema_elems4,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     1,
   },
   { "MASKINFO", STRUCT, sizeof(MASKINFO), 6, schema_elems5,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     1,
   },
   { "HG", STRUCT, sizeof(HG), 16, schema_elems6,
     (void *(*)(void))shHgNew,
     (void *(*)(void *))shHgDel,
     (RET_CODE(*)(FILE *,void *))shDumpGenericRead,
     (RET_CODE(*)(FILE *,void *))shDumpGenericWrite,
     0,
   },
   { "REAL", STRUCT, sizeof(REAL), 3, schema_elems7,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "PT", STRUCT, sizeof(PT), 4, schema_elems8,
     (void *(*)(void))shPtNew,
     (void *(*)(void *))shPtDel,
     (RET_CODE(*)(FILE *,void *))shDumpGenericRead,
     (RET_CODE(*)(FILE *,void *))shDumpGenericWrite,
     0,
   },
   { "PGSTATE", STRUCT, sizeof(PGSTATE), 33, schema_elems9,
     (void *(*)(void))shPgstateNew,
     (void *(*)(void *))shPgstateDel,
     (RET_CODE(*)(FILE *,void *))shDumpGenericRead,
     (RET_CODE(*)(FILE *,void *))shDumpGenericWrite,
     0,
   },
   { "LUTTYPE", ENUM, sizeof(LUTTYPE), 2, schema_elems10,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "LUT", STRUCT, sizeof(LUT), 3, schema_elems11,
     (void *(*)(void))shLutNew,
     (void *(*)(void *))shLutDel,
     (RET_CODE(*)(FILE *,void *))shDumpGenericRead,
     (RET_CODE(*)(FILE *,void *))shDumpGenericWrite,
     0,
   },
   { "ARRAY_DATA", STRUCT, sizeof(ARRAY_DATA), 5, schema_elems12,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "ARRAY", STRUCT, sizeof(ARRAY), 8, schema_elems13,
     (void *(*)(void))shArrayNew,
     (void *(*)(void *))shArrayDel,
     (RET_CODE(*)(FILE *,void *))shDumpGenericRead,
     (RET_CODE(*)(FILE *,void *))shDumpGenericWrite,
     0,
   },
   { "SHALIGN_TYPE", ENUM, sizeof(SHALIGN_TYPE), 12, schema_elems14,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "HDR", STRUCT, sizeof(HDR), 2, schema_elems15,
     (void *(*)(void))shHdrNew,
     (void *(*)(void *))shHdrDel,
     (RET_CODE(*)(FILE *,void *))shDumpHdrRead,
     (RET_CODE(*)(FILE *,void *))shDumpHdrWrite,
     0,
   },
   { "TBLCOL", STRUCT, sizeof(TBLCOL), 3, schema_elems16,
     (void *(*)(void))shTblcolNew,
     (void *(*)(void *))shTblcolDel,
     (RET_CODE(*)(FILE *,void *))shDumpGenericRead,
     (RET_CODE(*)(FILE *,void *))shDumpGenericWrite,
     0,
   },
   { "TBLHEAPDSC", STRUCT, sizeof(TBLHEAPDSC), 2, schema_elems17,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "TBLFLD_KEYWORDS", ENUM, sizeof(TBLFLD_KEYWORDS), 10, schema_elems18,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "TBLFLD_TFORM", STRUCT, sizeof(TBLFLD_TFORM), 4, schema_elems19,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "TBLFLD_PRVT", STRUCT, sizeof(TBLFLD_PRVT), 6, schema_elems20,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "TBLFLD", STRUCT, sizeof(TBLFLD), 11, schema_elems21,
     (void *(*)(void))shTblfldNew,
     (void *(*)(void *))shTblfldDel,
     (RET_CODE(*)(FILE *,void *))shDumpGenericRead,
     (RET_CODE(*)(FILE *,void *))shDumpGenericWrite,
     0,
   },
   { "TRANSTEST", STRUCT, sizeof(TRANSTEST), 9, schema_elems22,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "CONTAINER_TYPE", ENUM, sizeof(CONTAINER_TYPE), 4, schema_elems23,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "CONVTYPE", ENUM, sizeof(CONVTYPE), 5, schema_elems24,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "DSTTYPE", ENUM, sizeof(DSTTYPE), 16, schema_elems25,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "SCHTR_STATUS", ENUM, sizeof(SCHTR_STATUS), 4, schema_elems26,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "SCHEMATRANS_ENTRY", STRUCT, sizeof(SCHEMATRANS_ENTRY), 11, schema_elems27,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "SCHTR_HASH", STRUCT, sizeof(SCHTR_HASH), 3, schema_elems28,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "SCHEMATRANS", STRUCT, sizeof(SCHEMATRANS), 5, schema_elems29,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "CHAIN_ADD_FLAGS", ENUM, sizeof(CHAIN_ADD_FLAGS), 2, schema_elems30,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "CHAIN_WALK_FLAGS", ENUM, sizeof(CHAIN_WALK_FLAGS), 3, schema_elems31,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "CURSOR_STATUS", ENUM, sizeof(CURSOR_STATUS), 2, schema_elems32,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "CHAIN_ELEM", STRUCT, sizeof(CHAIN_ELEM), 4, schema_elems33,
     (void *(*)(void))shChain_elemNew,
     (void *(*)(void *))shChain_elemDel,
     (RET_CODE(*)(FILE *,void *))shDumpChain_elemRead,
     (RET_CODE(*)(FILE *,void *))shDumpChain_elemWrite,
     0,
   },
   { "CURSOR", STRUCT, sizeof(CURSOR), 5, schema_elems34,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "CHAIN", STRUCT, sizeof(CHAIN), 9, schema_elems35,
     (void *(*)(void))shChainNew,
     (void *(*)(void *))shChainDel,
     (RET_CODE(*)(FILE *,void *))shDumpChainRead,
     (RET_CODE(*)(FILE *,void *))shDumpChainWrite,
     0,
   },
   { "GENERIC", STRUCT, sizeof(GENERIC), 1, schema_elems36,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "VECTOR", STRUCT, sizeof(VECTOR), 3, schema_elems37,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "TYPE", PRIM, sizeof(TYPE), 1, schema_elems38,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpTypeRead,
     (RET_CODE(*)(FILE *,void *))shDumpTypeWrite,
     3,
   },
   { "CHAR", PRIM, sizeof(char), 1, schema_elems39,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpCharRead,
     (RET_CODE(*)(FILE *,void *))shDumpCharWrite,
     3,
   },
   { "UCHAR", PRIM, sizeof(unsigned char), 1, schema_elems40,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpUcharRead,
     (RET_CODE(*)(FILE *,void *))shDumpUcharWrite,
     3,
   },
   { "INT", PRIM, sizeof(int), 1, schema_elems41,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpIntRead,
     (RET_CODE(*)(FILE *,void *))shDumpIntWrite,
     3,
   },
   { "UINT", PRIM, sizeof(unsigned int), 1, schema_elems42,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpUintRead,
     (RET_CODE(*)(FILE *,void *))shDumpUintWrite,
     3,
   },
   { "SHORT", PRIM, sizeof(short), 1, schema_elems43,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpShortRead,
     (RET_CODE(*)(FILE *,void *))shDumpShortWrite,
     3,
   },
   { "USHORT", PRIM, sizeof(unsigned short), 1, schema_elems44,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpUshortRead,
     (RET_CODE(*)(FILE *,void *))shDumpUshortWrite,
     3,
   },
   { "LONG", PRIM, sizeof(long), 1, schema_elems45,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpLongRead,
     (RET_CODE(*)(FILE *,void *))shDumpLongWrite,
     3,
   },
   { "ULONG", PRIM, sizeof(unsigned long), 1, schema_elems46,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpUlongRead,
     (RET_CODE(*)(FILE *,void *))shDumpUlongWrite,
     3,
   },
   { "FLOAT", PRIM, sizeof(float), 1, schema_elems47,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpFloatRead,
     (RET_CODE(*)(FILE *,void *))shDumpFloatWrite,
     3,
   },
   { "DOUBLE", PRIM, sizeof(double), 1, schema_elems48,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpDoubleRead,
     (RET_CODE(*)(FILE *,void *))shDumpDoubleWrite,
     3,
   },
   { "LOGICAL", PRIM, sizeof(unsigned char), 1, schema_elems49,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpLogicalRead,
     (RET_CODE(*)(FILE *,void *))shDumpLogicalWrite,
     3,
   },
   { "STR", PRIM, sizeof(char *), 1, schema_elems50,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpStrRead,
     (RET_CODE(*)(FILE *,void *))shDumpStrWrite,
     3,
   },
   { "PTR", PRIM, sizeof(void *), 1, schema_elems51,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpPtrRead,
     (RET_CODE(*)(FILE *,void *))shDumpPtrWrite,
     3,
   },
   { "FUNCTION", PRIM, 0, 1, schema_elems52,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     3,
   },
   { "SCHAR", PRIM, sizeof(signed char), 1, schema_elems53,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpScharRead,
     (RET_CODE(*)(FILE *,void *))shDumpScharWrite,
     3,
   },
   { "", UNKNOWN, 0, 0,  NULL, NULL, NULL, NULL, NULL, SCHEMA_LOCK},
};

RET_CODE
shSchemaLoadFromDervish(void)
{
   return(p_shSchemaLoad(schema));
}

