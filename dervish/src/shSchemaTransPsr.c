/* A Bison parser, made by GNU Bison 3.3.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2019 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with shSCHTRS_yy or shSCHTRS_YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Undocumented macros, especially those whose name start with shSCHTRS_YY_,
   are private implementation details.  Do not rely on them.  */

/* Identify Bison output.  */
#define shSCHTRS_YYBISON 1

/* Bison version.  */
#define shSCHTRS_YYBISON_VERSION "3.3.2"

/* Skeleton name.  */
#define shSCHTRS_YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define shSCHTRS_YYPURE 0

/* Push parsers.  */
#define shSCHTRS_YYPUSH 0

/* Pull parsers.  */
#define shSCHTRS_YYPULL 1




/* First part of user prologue.  */
#line 2 "shSchemaTransPsr.y" /* yacc.c:337  */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
 
#include "libfits.h"
#include "dervish_msg_c.h"
#include "shCErrStack.h"
#include "shCUtils.h"

#include "shCSchemaTrans.h"

#if defined(_POSIX_SOURCE)
  /*#  define gettxt(FILE,MSG) (MSG)*/
#endif
   
typedef struct _junk
{
     char name[256];
}SCHTRS_SYMB;

struct _tmp_entry
{
   CONVTYPE type;
   char fits[256];
   char obj[256];
   char dtype[256];
   char ratio[256];
   char dimen[256];
   char proc[256];
   char htype[256];
   char hlen[256];
} g_entryNames;

SCHEMATRANS* g_xtbl;
unsigned ratioGood = FALSE;
unsigned dimenGood = FALSE;
unsigned procGood = FALSE;
unsigned htypeGood = FALSE;
unsigned hlenGood = FALSE;

#undef shSCHTRS_YYSTYPE
#define shSCHTRS_YYSTYPE SCHTRS_SYMB

extern void switchInit(void);
extern int shSCHTRS_yyparse(void);
extern int shSCHTRS_yylex(void);


#line 122 "y.tab.c" /* yacc.c:337  */
# ifndef shSCHTRS_YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define shSCHTRS_YY_NULLPTR nullptr
#   else
#    define shSCHTRS_YY_NULLPTR 0
#   endif
#  else
#   define shSCHTRS_YY_NULLPTR ((void*)0)
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef shSCHTRS_YYERROR_VERBOSE
# undef shSCHTRS_YYERROR_VERBOSE
# define shSCHTRS_YYERROR_VERBOSE 1
#else
# define shSCHTRS_YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "y.tab.h".  */
#ifndef shSCHTRS_YY_shSCHTRS_YY_Y_TAB_H_INCLUDED
# define shSCHTRS_YY_shSCHTRS_YY_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef shSCHTRS_YYDEBUG
# define shSCHTRS_YYDEBUG 0
#endif
#if shSCHTRS_YYDEBUG
extern int shSCHTRS_yydebug;
#endif

/* Token type.  */
#ifndef shSCHTRS_YYTOKENTYPE
# define shSCHTRS_YYTOKENTYPE
  enum shSCHTRS_yytokentype
  {
    SCHTRS_STRING = 258,
    SCHTRS_IGNORE = 259,
    SCHTRS_NAME = 260,
    SCHTRS_CONT = 261,
    SCHTRS_PROC = 262,
    SCHTRS_DIMEN = 263,
    SCHTRS_RATIO = 264,
    SCHTRS_HEAPTYPE = 265,
    SCHTRS_HEAPLENGTH = 266
  };
#endif
/* Tokens.  */
#define SCHTRS_STRING 258
#define SCHTRS_IGNORE 259
#define SCHTRS_NAME 260
#define SCHTRS_CONT 261
#define SCHTRS_PROC 262
#define SCHTRS_DIMEN 263
#define SCHTRS_RATIO 264
#define SCHTRS_HEAPTYPE 265
#define SCHTRS_HEAPLENGTH 266

/* Value type.  */
#if ! defined shSCHTRS_YYSTYPE && ! defined shSCHTRS_YYSTYPE_IS_DECLARED
typedef int shSCHTRS_YYSTYPE;
# define shSCHTRS_YYSTYPE_IS_TRIVIAL 1
# define shSCHTRS_YYSTYPE_IS_DECLARED 1
#endif


extern shSCHTRS_YYSTYPE shSCHTRS_yylval;

int shSCHTRS_yyparse (void);

#endif /* !shSCHTRS_YY_shSCHTRS_YY_Y_TAB_H_INCLUDED  */



#ifdef short
# undef short
#endif

#ifdef shSCHTRS_YYTYPE_UINT8
typedef shSCHTRS_YYTYPE_UINT8 shSCHTRS_yytype_uint8;
#else
typedef unsigned char shSCHTRS_yytype_uint8;
#endif

#ifdef shSCHTRS_YYTYPE_INT8
typedef shSCHTRS_YYTYPE_INT8 shSCHTRS_yytype_int8;
#else
typedef signed char shSCHTRS_yytype_int8;
#endif

#ifdef shSCHTRS_YYTYPE_UINT16
typedef shSCHTRS_YYTYPE_UINT16 shSCHTRS_yytype_uint16;
#else
typedef unsigned short shSCHTRS_yytype_uint16;
#endif

#ifdef shSCHTRS_YYTYPE_INT16
typedef shSCHTRS_YYTYPE_INT16 shSCHTRS_yytype_int16;
#else
typedef short shSCHTRS_yytype_int16;
#endif

#ifndef shSCHTRS_YYSIZE_T
# ifdef __SIZE_TYPE__
#  define shSCHTRS_YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define shSCHTRS_YYSIZE_T size_t
# elif ! defined shSCHTRS_YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define shSCHTRS_YYSIZE_T size_t
# else
#  define shSCHTRS_YYSIZE_T unsigned
# endif
#endif

#define shSCHTRS_YYSIZE_MAXIMUM ((shSCHTRS_YYSIZE_T) -1)

#ifndef shSCHTRS_YY_
# if defined shSCHTRS_YYENABLE_NLS && shSCHTRS_YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define shSCHTRS_YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef shSCHTRS_YY_
#  define shSCHTRS_YY_(Msgid) Msgid
# endif
#endif

#ifndef shSCHTRS_YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define shSCHTRS_YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define shSCHTRS_YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef shSCHTRS_YY_ATTRIBUTE_PURE
# define shSCHTRS_YY_ATTRIBUTE_PURE   shSCHTRS_YY_ATTRIBUTE ((__pure__))
#endif

#ifndef shSCHTRS_YY_ATTRIBUTE_UNUSED
# define shSCHTRS_YY_ATTRIBUTE_UNUSED shSCHTRS_YY_ATTRIBUTE ((__unused__))
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define shSCHTRS_YYUSE(E) ((void) (E))
#else
# define shSCHTRS_YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && ! defined __ICC && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about shSCHTRS_yylval being uninitialized.  */
# define shSCHTRS_YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define shSCHTRS_YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define shSCHTRS_YY_INITIAL_VALUE(Value) Value
#endif
#ifndef shSCHTRS_YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define shSCHTRS_YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define shSCHTRS_YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef shSCHTRS_YY_INITIAL_VALUE
# define shSCHTRS_YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined shSCHTRS_yyoverflow || shSCHTRS_YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef shSCHTRS_YYSTACK_USE_ALLOCA
#  if shSCHTRS_YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define shSCHTRS_YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define shSCHTRS_YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define shSCHTRS_YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef shSCHTRS_YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define shSCHTRS_YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef shSCHTRS_YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define shSCHTRS_YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define shSCHTRS_YYSTACK_ALLOC shSCHTRS_YYMALLOC
#  define shSCHTRS_YYSTACK_FREE shSCHTRS_YYFREE
#  ifndef shSCHTRS_YYSTACK_ALLOC_MAXIMUM
#   define shSCHTRS_YYSTACK_ALLOC_MAXIMUM shSCHTRS_YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined shSCHTRS_YYMALLOC || defined malloc) \
             && (defined shSCHTRS_YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef shSCHTRS_YYMALLOC
#   define shSCHTRS_YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (shSCHTRS_YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef shSCHTRS_YYFREE
#   define shSCHTRS_YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined shSCHTRS_yyoverflow || shSCHTRS_YYERROR_VERBOSE */


#if (! defined shSCHTRS_yyoverflow \
     && (! defined __cplusplus \
         || (defined shSCHTRS_YYSTYPE_IS_TRIVIAL && shSCHTRS_YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union shSCHTRS_yyalloc
{
  shSCHTRS_yytype_int16 shSCHTRS_yyss_alloc;
  shSCHTRS_YYSTYPE shSCHTRS_yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define shSCHTRS_YYSTACK_GAP_MAXIMUM (sizeof (union shSCHTRS_yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define shSCHTRS_YYSTACK_BYTES(N) \
     ((N) * (sizeof (shSCHTRS_yytype_int16) + sizeof (shSCHTRS_YYSTYPE)) \
      + shSCHTRS_YYSTACK_GAP_MAXIMUM)

# define shSCHTRS_YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables shSCHTRS_YYSIZE and shSCHTRS_YYSTACKSIZE give the old and new number of
   elements in the stack, and shSCHTRS_YYPTR gives the new location of the
   stack.  Advance shSCHTRS_YYPTR to a properly aligned location for the next
   stack.  */
# define shSCHTRS_YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        shSCHTRS_YYSIZE_T shSCHTRS_yynewbytes;                                            \
        shSCHTRS_YYCOPY (&shSCHTRS_yyptr->Stack_alloc, Stack, shSCHTRS_yysize);                    \
        Stack = &shSCHTRS_yyptr->Stack_alloc;                                    \
        shSCHTRS_yynewbytes = shSCHTRS_yystacksize * sizeof (*Stack) + shSCHTRS_YYSTACK_GAP_MAXIMUM; \
        shSCHTRS_yyptr += shSCHTRS_yynewbytes / sizeof (*shSCHTRS_yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined shSCHTRS_YYCOPY_NEEDED && shSCHTRS_YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef shSCHTRS_YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define shSCHTRS_YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define shSCHTRS_YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          shSCHTRS_YYSIZE_T shSCHTRS_yyi;                         \
          for (shSCHTRS_yyi = 0; shSCHTRS_yyi < (Count); shSCHTRS_yyi++)   \
            (Dst)[shSCHTRS_yyi] = (Src)[shSCHTRS_yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !shSCHTRS_YYCOPY_NEEDED */

/* shSCHTRS_YYFINAL -- State number of the termination state.  */
#define shSCHTRS_YYFINAL  2
/* shSCHTRS_YYLAST -- Last index in shSCHTRS_YYTABLE.  */
#define shSCHTRS_YYLAST   23

/* shSCHTRS_YYNTOKENS -- Number of terminals.  */
#define shSCHTRS_YYNTOKENS  13
/* shSCHTRS_YYNNTS -- Number of nonterminals.  */
#define shSCHTRS_YYNNTS  11
/* shSCHTRS_YYNRULES -- Number of rules.  */
#define shSCHTRS_YYNRULES  20
/* shSCHTRS_YYNSTATES -- Number of states.  */
#define shSCHTRS_YYNSTATES  32

#define shSCHTRS_YYUNDEFTOK  2
#define shSCHTRS_YYMAXUTOK   266

/* shSCHTRS_YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by shSCHTRS_yylex, with out-of-bounds checking.  */
#define shSCHTRS_YYTRANSLATE(shSCHTRS_YYX)                                                \
  ((unsigned) (shSCHTRS_YYX) <= shSCHTRS_YYMAXUTOK ? shSCHTRS_yytranslate[shSCHTRS_YYX] : shSCHTRS_YYUNDEFTOK)

/* shSCHTRS_YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by shSCHTRS_yylex.  */
static const shSCHTRS_yytype_uint8 shSCHTRS_yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,    12,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11
};

#if shSCHTRS_YYDEBUG
  /* shSCHTRS_YYRLINE[shSCHTRS_YYN] -- Source line where rule number shSCHTRS_YYN was defined.  */
static const shSCHTRS_yytype_uint8 shSCHTRS_yyrline[] =
{
       0,    70,    70,    71,    74,    91,   110,   111,   114,   114,
     116,   119,   119,   122,   127,   132,   137,   142,   149,   152,
     155
};
#endif

#if shSCHTRS_YYDEBUG || shSCHTRS_YYERROR_VERBOSE || 0
/* shSCHTRS_YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at shSCHTRS_YYNTOKENS, nonterminals.  */
static const char *const shSCHTRS_yytname[] =
{
  "$end", "error", "$undefined", "SCHTRS_STRING", "SCHTRS_IGNORE",
  "SCHTRS_NAME", "SCHTRS_CONT", "SCHTRS_PROC", "SCHTRS_DIMEN",
  "SCHTRS_RATIO", "SCHTRS_HEAPTYPE", "SCHTRS_HEAPLENGTH", "';'", "$accept",
  "entries", "entry", "type", "others", "name_declare", "options",
  "option", "fits_name", "obj_name", "obj_type", shSCHTRS_YY_NULLPTR
};
#endif

# ifdef shSCHTRS_YYPRINT
/* shSCHTRS_YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const shSCHTRS_yytype_uint16 shSCHTRS_yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,    59
};
# endif

#define shSCHTRS_YYPACT_NINF -10

#define shSCHTRS_yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-10)))

#define shSCHTRS_YYTABLE_NINF -1

#define shSCHTRS_yytable_value_is_error(Yytable_value) \
  0

  /* shSCHTRS_YYPACT[STATE-NUM] -- Index in shSCHTRS_YYTABLE of the portion describing
     STATE-NUM.  */
static const shSCHTRS_yytype_int8 shSCHTRS_yypact[] =
{
     -10,     0,   -10,    -2,   -10,   -10,   -10,    -1,   -10,    -9,
     -10,    -5,     1,    -2,   -10,   -10,    10,    11,    12,    13,
      14,     1,   -10,    15,   -10,   -10,   -10,   -10,   -10,   -10,
     -10,   -10
};

  /* shSCHTRS_YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when shSCHTRS_YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const shSCHTRS_yytype_uint8 shSCHTRS_yydefact[] =
{
       2,     0,     1,     0,     6,     7,     3,     0,    19,     0,
      18,     0,     8,     0,     5,     4,     0,     0,     0,     0,
       0,     9,    11,     0,    13,    14,    15,    16,    17,    12,
      20,    10
};

  /* shSCHTRS_YYPGOTO[NTERM-NUM].  */
static const shSCHTRS_yytype_int8 shSCHTRS_yypgoto[] =
{
     -10,   -10,   -10,   -10,   -10,   -10,   -10,     2,   -10,     6,
     -10
};

  /* shSCHTRS_YYDEFGOTO[NTERM-NUM].  */
static const shSCHTRS_yytype_int8 shSCHTRS_yydefgoto[] =
{
      -1,     1,     6,     7,    11,    12,    21,    22,    13,     9,
      31
};

  /* shSCHTRS_YYTABLE[shSCHTRS_YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If shSCHTRS_YYTABLE_NINF, syntax error.  */
static const shSCHTRS_yytype_uint8 shSCHTRS_yytable[] =
{
       2,     8,    10,    14,     3,     4,     5,    15,    16,    17,
      18,    19,    20,    24,    25,    26,    27,    28,    30,    23,
       0,     0,     0,    29
};

static const shSCHTRS_yytype_int8 shSCHTRS_yycheck[] =
{
       0,     3,     3,    12,     4,     5,     6,    12,     7,     8,
       9,    10,    11,     3,     3,     3,     3,     3,     3,    13,
      -1,    -1,    -1,    21
};

  /* shSCHTRS_YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const shSCHTRS_yytype_uint8 shSCHTRS_yystos[] =
{
       0,    14,     0,     4,     5,     6,    15,    16,     3,    22,
       3,    17,    18,    21,    12,    12,     7,     8,     9,    10,
      11,    19,    20,    22,     3,     3,     3,     3,     3,    20,
       3,    23
};

  /* shSCHTRS_YYR1[shSCHTRS_YYN] -- Symbol number of symbol that rule shSCHTRS_YYN derives.  */
static const shSCHTRS_yytype_uint8 shSCHTRS_yyr1[] =
{
       0,    13,    14,    14,    15,    15,    16,    16,    17,    17,
      18,    19,    19,    20,    20,    20,    20,    20,    21,    22,
      23
};

  /* shSCHTRS_YYR2[shSCHTRS_YYN] -- Number of symbols on the right hand side of rule shSCHTRS_YYN.  */
static const shSCHTRS_yytype_uint8 shSCHTRS_yyr2[] =
{
       0,     2,     0,     2,     3,     3,     1,     1,     1,     2,
       3,     1,     2,     2,     2,     2,     2,     2,     1,     1,
       1
};


#define shSCHTRS_yyerrok         (shSCHTRS_yyerrstatus = 0)
#define shSCHTRS_yyclearin       (shSCHTRS_yychar = shSCHTRS_YYEMPTY)
#define shSCHTRS_YYEMPTY         (-2)
#define shSCHTRS_YYEOF           0

#define shSCHTRS_YYACCEPT        goto shSCHTRS_yyacceptlab
#define shSCHTRS_YYABORT         goto shSCHTRS_yyabortlab
#define shSCHTRS_YYERROR         goto shSCHTRS_yyerrorlab


#define shSCHTRS_YYRECOVERING()  (!!shSCHTRS_yyerrstatus)

#define shSCHTRS_YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (shSCHTRS_yychar == shSCHTRS_YYEMPTY)                                        \
      {                                                           \
        shSCHTRS_yychar = (Token);                                         \
        shSCHTRS_yylval = (Value);                                         \
        shSCHTRS_YYPOPSTACK (shSCHTRS_yylen);                                       \
        shSCHTRS_yystate = *shSCHTRS_yyssp;                                         \
        goto shSCHTRS_yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        shSCHTRS_yyerror (shSCHTRS_YY_("syntax error: cannot back up")); \
        shSCHTRS_YYERROR;                                                  \
      }                                                           \
  while (0)

/* Error token number */
#define shSCHTRS_YYTERROR        1
#define shSCHTRS_YYERRCODE       256



/* Enable debugging if requested.  */
#if shSCHTRS_YYDEBUG

# ifndef shSCHTRS_YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define shSCHTRS_YYFPRINTF fprintf
# endif

# define shSCHTRS_YYDPRINTF(Args)                        \
do {                                            \
  if (shSCHTRS_yydebug)                                  \
    shSCHTRS_YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef shSCHTRS_YY_LOCATION_PRINT
# define shSCHTRS_YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define shSCHTRS_YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (shSCHTRS_yydebug)                                                            \
    {                                                                     \
      shSCHTRS_YYFPRINTF (stderr, "%s ", Title);                                   \
      shSCHTRS_yy_symbol_print (stderr,                                            \
                  Type, Value); \
      shSCHTRS_YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on shSCHTRS_YYO.  |
`-----------------------------------*/

static void
shSCHTRS_yy_symbol_value_print (FILE *shSCHTRS_yyo, int shSCHTRS_yytype, shSCHTRS_YYSTYPE const * const shSCHTRS_yyvaluep)
{
  FILE *shSCHTRS_yyoutput = shSCHTRS_yyo;
  shSCHTRS_YYUSE (shSCHTRS_yyoutput);
  if (!shSCHTRS_yyvaluep)
    return;
# ifdef shSCHTRS_YYPRINT
  if (shSCHTRS_yytype < shSCHTRS_YYNTOKENS)
    shSCHTRS_YYPRINT (shSCHTRS_yyo, shSCHTRS_yytoknum[shSCHTRS_yytype], *shSCHTRS_yyvaluep);
# endif
  shSCHTRS_YYUSE (shSCHTRS_yytype);
}


/*---------------------------.
| Print this symbol on shSCHTRS_YYO.  |
`---------------------------*/

static void
shSCHTRS_yy_symbol_print (FILE *shSCHTRS_yyo, int shSCHTRS_yytype, shSCHTRS_YYSTYPE const * const shSCHTRS_yyvaluep)
{
  shSCHTRS_YYFPRINTF (shSCHTRS_yyo, "%s %s (",
             shSCHTRS_yytype < shSCHTRS_YYNTOKENS ? "token" : "nterm", shSCHTRS_yytname[shSCHTRS_yytype]);

  shSCHTRS_yy_symbol_value_print (shSCHTRS_yyo, shSCHTRS_yytype, shSCHTRS_yyvaluep);
  shSCHTRS_YYFPRINTF (shSCHTRS_yyo, ")");
}

/*------------------------------------------------------------------.
| shSCHTRS_yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
shSCHTRS_yy_stack_print (shSCHTRS_yytype_int16 *shSCHTRS_yybottom, shSCHTRS_yytype_int16 *shSCHTRS_yytop)
{
  shSCHTRS_YYFPRINTF (stderr, "Stack now");
  for (; shSCHTRS_yybottom <= shSCHTRS_yytop; shSCHTRS_yybottom++)
    {
      int shSCHTRS_yybot = *shSCHTRS_yybottom;
      shSCHTRS_YYFPRINTF (stderr, " %d", shSCHTRS_yybot);
    }
  shSCHTRS_YYFPRINTF (stderr, "\n");
}

# define shSCHTRS_YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (shSCHTRS_yydebug)                                                  \
    shSCHTRS_yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the shSCHTRS_YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
shSCHTRS_yy_reduce_print (shSCHTRS_yytype_int16 *shSCHTRS_yyssp, shSCHTRS_YYSTYPE *shSCHTRS_yyvsp, int shSCHTRS_yyrule)
{
  unsigned long shSCHTRS_yylno = shSCHTRS_yyrline[shSCHTRS_yyrule];
  int shSCHTRS_yynrhs = shSCHTRS_yyr2[shSCHTRS_yyrule];
  int shSCHTRS_yyi;
  shSCHTRS_YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             shSCHTRS_yyrule - 1, shSCHTRS_yylno);
  /* The symbols being reduced.  */
  for (shSCHTRS_yyi = 0; shSCHTRS_yyi < shSCHTRS_yynrhs; shSCHTRS_yyi++)
    {
      shSCHTRS_YYFPRINTF (stderr, "   $%d = ", shSCHTRS_yyi + 1);
      shSCHTRS_yy_symbol_print (stderr,
                       shSCHTRS_yystos[shSCHTRS_yyssp[shSCHTRS_yyi + 1 - shSCHTRS_yynrhs]],
                       &shSCHTRS_yyvsp[(shSCHTRS_yyi + 1) - (shSCHTRS_yynrhs)]
                                              );
      shSCHTRS_YYFPRINTF (stderr, "\n");
    }
}

# define shSCHTRS_YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (shSCHTRS_yydebug)                          \
    shSCHTRS_yy_reduce_print (shSCHTRS_yyssp, shSCHTRS_yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int shSCHTRS_yydebug;
#else /* !shSCHTRS_YYDEBUG */
# define shSCHTRS_YYDPRINTF(Args)
# define shSCHTRS_YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define shSCHTRS_YY_STACK_PRINT(Bottom, Top)
# define shSCHTRS_YY_REDUCE_PRINT(Rule)
#endif /* !shSCHTRS_YYDEBUG */


/* shSCHTRS_YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef shSCHTRS_YYINITDEPTH
# define shSCHTRS_YYINITDEPTH 200
#endif

/* shSCHTRS_YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   shSCHTRS_YYSTACK_ALLOC_MAXIMUM < shSCHTRS_YYSTACK_BYTES (shSCHTRS_YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef shSCHTRS_YYMAXDEPTH
# define shSCHTRS_YYMAXDEPTH 10000
#endif


#if shSCHTRS_YYERROR_VERBOSE

# ifndef shSCHTRS_yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define shSCHTRS_yystrlen strlen
#  else
/* Return the length of shSCHTRS_YYSTR.  */
static shSCHTRS_YYSIZE_T
shSCHTRS_yystrlen (const char *shSCHTRS_yystr)
{
  shSCHTRS_YYSIZE_T shSCHTRS_yylen;
  for (shSCHTRS_yylen = 0; shSCHTRS_yystr[shSCHTRS_yylen]; shSCHTRS_yylen++)
    continue;
  return shSCHTRS_yylen;
}
#  endif
# endif

# ifndef shSCHTRS_yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define shSCHTRS_yystpcpy stpcpy
#  else
/* Copy shSCHTRS_YYSRC to shSCHTRS_YYDEST, returning the address of the terminating '\0' in
   shSCHTRS_YYDEST.  */
static char *
shSCHTRS_yystpcpy (char *shSCHTRS_yydest, const char *shSCHTRS_yysrc)
{
  char *shSCHTRS_yyd = shSCHTRS_yydest;
  const char *shSCHTRS_yys = shSCHTRS_yysrc;

  while ((*shSCHTRS_yyd++ = *shSCHTRS_yys++) != '\0')
    continue;

  return shSCHTRS_yyd - 1;
}
#  endif
# endif

# ifndef shSCHTRS_yytnamerr
/* Copy to shSCHTRS_YYRES the contents of shSCHTRS_YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for shSCHTRS_yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  shSCHTRS_YYSTR is taken from shSCHTRS_yytname.  If shSCHTRS_YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static shSCHTRS_YYSIZE_T
shSCHTRS_yytnamerr (char *shSCHTRS_yyres, const char *shSCHTRS_yystr)
{
  if (*shSCHTRS_yystr == '"')
    {
      shSCHTRS_YYSIZE_T shSCHTRS_yyn = 0;
      char const *shSCHTRS_yyp = shSCHTRS_yystr;

      for (;;)
        switch (*++shSCHTRS_yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++shSCHTRS_yyp != '\\')
              goto do_not_strip_quotes;
            else
              goto append;

          append:
          default:
            if (shSCHTRS_yyres)
              shSCHTRS_yyres[shSCHTRS_yyn] = *shSCHTRS_yyp;
            shSCHTRS_yyn++;
            break;

          case '"':
            if (shSCHTRS_yyres)
              shSCHTRS_yyres[shSCHTRS_yyn] = '\0';
            return shSCHTRS_yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! shSCHTRS_yyres)
    return shSCHTRS_yystrlen (shSCHTRS_yystr);

  return (shSCHTRS_YYSIZE_T) (shSCHTRS_yystpcpy (shSCHTRS_yyres, shSCHTRS_yystr) - shSCHTRS_yyres);
}
# endif

/* Copy into *shSCHTRS_YYMSG, which is of size *shSCHTRS_YYMSG_ALLOC, an error message
   about the unexpected token shSCHTRS_YYTOKEN for the state stack whose top is
   shSCHTRS_YYSSP.

   Return 0 if *shSCHTRS_YYMSG was successfully written.  Return 1 if *shSCHTRS_YYMSG is
   not large enough to hold the message.  In that case, also set
   *shSCHTRS_YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
shSCHTRS_yysyntax_error (shSCHTRS_YYSIZE_T *shSCHTRS_yymsg_alloc, char **shSCHTRS_yymsg,
                shSCHTRS_yytype_int16 *shSCHTRS_yyssp, int shSCHTRS_yytoken)
{
  shSCHTRS_YYSIZE_T shSCHTRS_yysize0 = shSCHTRS_yytnamerr (shSCHTRS_YY_NULLPTR, shSCHTRS_yytname[shSCHTRS_yytoken]);
  shSCHTRS_YYSIZE_T shSCHTRS_yysize = shSCHTRS_yysize0;
  enum { shSCHTRS_YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *shSCHTRS_yyformat = shSCHTRS_YY_NULLPTR;
  /* Arguments of shSCHTRS_yyformat. */
  char const *shSCHTRS_yyarg[shSCHTRS_YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int shSCHTRS_yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in shSCHTRS_yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated shSCHTRS_yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (shSCHTRS_yytoken != shSCHTRS_YYEMPTY)
    {
      int shSCHTRS_yyn = shSCHTRS_yypact[*shSCHTRS_yyssp];
      shSCHTRS_yyarg[shSCHTRS_yycount++] = shSCHTRS_yytname[shSCHTRS_yytoken];
      if (!shSCHTRS_yypact_value_is_default (shSCHTRS_yyn))
        {
          /* Start shSCHTRS_YYX at -shSCHTRS_YYN if negative to avoid negative indexes in
             shSCHTRS_YYCHECK.  In other words, skip the first -shSCHTRS_YYN actions for
             this state because they are default actions.  */
          int shSCHTRS_yyxbegin = shSCHTRS_yyn < 0 ? -shSCHTRS_yyn : 0;
          /* Stay within bounds of both shSCHTRS_yycheck and shSCHTRS_yytname.  */
          int shSCHTRS_yychecklim = shSCHTRS_YYLAST - shSCHTRS_yyn + 1;
          int shSCHTRS_yyxend = shSCHTRS_yychecklim < shSCHTRS_YYNTOKENS ? shSCHTRS_yychecklim : shSCHTRS_YYNTOKENS;
          int shSCHTRS_yyx;

          for (shSCHTRS_yyx = shSCHTRS_yyxbegin; shSCHTRS_yyx < shSCHTRS_yyxend; ++shSCHTRS_yyx)
            if (shSCHTRS_yycheck[shSCHTRS_yyx + shSCHTRS_yyn] == shSCHTRS_yyx && shSCHTRS_yyx != shSCHTRS_YYTERROR
                && !shSCHTRS_yytable_value_is_error (shSCHTRS_yytable[shSCHTRS_yyx + shSCHTRS_yyn]))
              {
                if (shSCHTRS_yycount == shSCHTRS_YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    shSCHTRS_yycount = 1;
                    shSCHTRS_yysize = shSCHTRS_yysize0;
                    break;
                  }
                shSCHTRS_yyarg[shSCHTRS_yycount++] = shSCHTRS_yytname[shSCHTRS_yyx];
                {
                  shSCHTRS_YYSIZE_T shSCHTRS_yysize1 = shSCHTRS_yysize + shSCHTRS_yytnamerr (shSCHTRS_YY_NULLPTR, shSCHTRS_yytname[shSCHTRS_yyx]);
                  if (shSCHTRS_yysize <= shSCHTRS_yysize1 && shSCHTRS_yysize1 <= shSCHTRS_YYSTACK_ALLOC_MAXIMUM)
                    shSCHTRS_yysize = shSCHTRS_yysize1;
                  else
                    return 2;
                }
              }
        }
    }

  switch (shSCHTRS_yycount)
    {
# define shSCHTRS_YYCASE_(N, S)                      \
      case N:                               \
        shSCHTRS_yyformat = S;                       \
      break
    default: /* Avoid compiler warnings. */
      shSCHTRS_YYCASE_(0, shSCHTRS_YY_("syntax error"));
      shSCHTRS_YYCASE_(1, shSCHTRS_YY_("syntax error, unexpected %s"));
      shSCHTRS_YYCASE_(2, shSCHTRS_YY_("syntax error, unexpected %s, expecting %s"));
      shSCHTRS_YYCASE_(3, shSCHTRS_YY_("syntax error, unexpected %s, expecting %s or %s"));
      shSCHTRS_YYCASE_(4, shSCHTRS_YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      shSCHTRS_YYCASE_(5, shSCHTRS_YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef shSCHTRS_YYCASE_
    }

  {
    shSCHTRS_YYSIZE_T shSCHTRS_yysize1 = shSCHTRS_yysize + shSCHTRS_yystrlen (shSCHTRS_yyformat);
    if (shSCHTRS_yysize <= shSCHTRS_yysize1 && shSCHTRS_yysize1 <= shSCHTRS_YYSTACK_ALLOC_MAXIMUM)
      shSCHTRS_yysize = shSCHTRS_yysize1;
    else
      return 2;
  }

  if (*shSCHTRS_yymsg_alloc < shSCHTRS_yysize)
    {
      *shSCHTRS_yymsg_alloc = 2 * shSCHTRS_yysize;
      if (! (shSCHTRS_yysize <= *shSCHTRS_yymsg_alloc
             && *shSCHTRS_yymsg_alloc <= shSCHTRS_YYSTACK_ALLOC_MAXIMUM))
        *shSCHTRS_yymsg_alloc = shSCHTRS_YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *shSCHTRS_yyp = *shSCHTRS_yymsg;
    int shSCHTRS_yyi = 0;
    while ((*shSCHTRS_yyp = *shSCHTRS_yyformat) != '\0')
      if (*shSCHTRS_yyp == '%' && shSCHTRS_yyformat[1] == 's' && shSCHTRS_yyi < shSCHTRS_yycount)
        {
          shSCHTRS_yyp += shSCHTRS_yytnamerr (shSCHTRS_yyp, shSCHTRS_yyarg[shSCHTRS_yyi++]);
          shSCHTRS_yyformat += 2;
        }
      else
        {
          shSCHTRS_yyp++;
          shSCHTRS_yyformat++;
        }
  }
  return 0;
}
#endif /* shSCHTRS_YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
shSCHTRS_yydestruct (const char *shSCHTRS_yymsg, int shSCHTRS_yytype, shSCHTRS_YYSTYPE *shSCHTRS_yyvaluep)
{
  shSCHTRS_YYUSE (shSCHTRS_yyvaluep);
  if (!shSCHTRS_yymsg)
    shSCHTRS_yymsg = "Deleting";
  shSCHTRS_YY_SYMBOL_PRINT (shSCHTRS_yymsg, shSCHTRS_yytype, shSCHTRS_yyvaluep, shSCHTRS_yylocationp);

  shSCHTRS_YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  shSCHTRS_YYUSE (shSCHTRS_yytype);
  shSCHTRS_YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int shSCHTRS_yychar;

/* The semantic value of the lookahead symbol.  */
shSCHTRS_YYSTYPE shSCHTRS_yylval;
/* Number of syntax errors so far.  */
int shSCHTRS_yynerrs;


/*----------.
| shSCHTRS_yyparse.  |
`----------*/

int
shSCHTRS_yyparse (void)
{
    int shSCHTRS_yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int shSCHTRS_yyerrstatus;

    /* The stacks and their tools:
       'shSCHTRS_yyss': related to states.
       'shSCHTRS_yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow shSCHTRS_yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    shSCHTRS_yytype_int16 shSCHTRS_yyssa[shSCHTRS_YYINITDEPTH];
    shSCHTRS_yytype_int16 *shSCHTRS_yyss;
    shSCHTRS_yytype_int16 *shSCHTRS_yyssp;

    /* The semantic value stack.  */
    shSCHTRS_YYSTYPE shSCHTRS_yyvsa[shSCHTRS_YYINITDEPTH];
    shSCHTRS_YYSTYPE *shSCHTRS_yyvs;
    shSCHTRS_YYSTYPE *shSCHTRS_yyvsp;

    shSCHTRS_YYSIZE_T shSCHTRS_yystacksize;

  int shSCHTRS_yyn;
  int shSCHTRS_yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int shSCHTRS_yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  shSCHTRS_YYSTYPE shSCHTRS_yyval;

#if shSCHTRS_YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char shSCHTRS_yymsgbuf[128];
  char *shSCHTRS_yymsg = shSCHTRS_yymsgbuf;
  shSCHTRS_YYSIZE_T shSCHTRS_yymsg_alloc = sizeof shSCHTRS_yymsgbuf;
#endif

#define shSCHTRS_YYPOPSTACK(N)   (shSCHTRS_yyvsp -= (N), shSCHTRS_yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int shSCHTRS_yylen = 0;

  shSCHTRS_yyssp = shSCHTRS_yyss = shSCHTRS_yyssa;
  shSCHTRS_yyvsp = shSCHTRS_yyvs = shSCHTRS_yyvsa;
  shSCHTRS_yystacksize = shSCHTRS_YYINITDEPTH;

  shSCHTRS_YYDPRINTF ((stderr, "Starting parse\n"));

  shSCHTRS_yystate = 0;
  shSCHTRS_yyerrstatus = 0;
  shSCHTRS_yynerrs = 0;
  shSCHTRS_yychar = shSCHTRS_YYEMPTY; /* Cause a token to be read.  */
  goto shSCHTRS_yysetstate;


/*------------------------------------------------------------.
| shSCHTRS_yynewstate -- push a new state, which is found in shSCHTRS_yystate.  |
`------------------------------------------------------------*/
shSCHTRS_yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  shSCHTRS_yyssp++;


/*--------------------------------------------------------------------.
| shSCHTRS_yynewstate -- set current state (the top of the stack) to shSCHTRS_yystate.  |
`--------------------------------------------------------------------*/
shSCHTRS_yysetstate:
  *shSCHTRS_yyssp = (shSCHTRS_yytype_int16) shSCHTRS_yystate;

  if (shSCHTRS_yyss + shSCHTRS_yystacksize - 1 <= shSCHTRS_yyssp)
#if !defined shSCHTRS_yyoverflow && !defined shSCHTRS_YYSTACK_RELOCATE
    goto shSCHTRS_yyexhaustedlab;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      shSCHTRS_YYSIZE_T shSCHTRS_yysize = (shSCHTRS_YYSIZE_T) (shSCHTRS_yyssp - shSCHTRS_yyss + 1);

# if defined shSCHTRS_yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        shSCHTRS_YYSTYPE *shSCHTRS_yyvs1 = shSCHTRS_yyvs;
        shSCHTRS_yytype_int16 *shSCHTRS_yyss1 = shSCHTRS_yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if shSCHTRS_yyoverflow is a macro.  */
        shSCHTRS_yyoverflow (shSCHTRS_YY_("memory exhausted"),
                    &shSCHTRS_yyss1, shSCHTRS_yysize * sizeof (*shSCHTRS_yyssp),
                    &shSCHTRS_yyvs1, shSCHTRS_yysize * sizeof (*shSCHTRS_yyvsp),
                    &shSCHTRS_yystacksize);
        shSCHTRS_yyss = shSCHTRS_yyss1;
        shSCHTRS_yyvs = shSCHTRS_yyvs1;
      }
# else /* defined shSCHTRS_YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (shSCHTRS_YYMAXDEPTH <= shSCHTRS_yystacksize)
        goto shSCHTRS_yyexhaustedlab;
      shSCHTRS_yystacksize *= 2;
      if (shSCHTRS_YYMAXDEPTH < shSCHTRS_yystacksize)
        shSCHTRS_yystacksize = shSCHTRS_YYMAXDEPTH;

      {
        shSCHTRS_yytype_int16 *shSCHTRS_yyss1 = shSCHTRS_yyss;
        union shSCHTRS_yyalloc *shSCHTRS_yyptr =
          (union shSCHTRS_yyalloc *) shSCHTRS_YYSTACK_ALLOC (shSCHTRS_YYSTACK_BYTES (shSCHTRS_yystacksize));
        if (! shSCHTRS_yyptr)
          goto shSCHTRS_yyexhaustedlab;
        shSCHTRS_YYSTACK_RELOCATE (shSCHTRS_yyss_alloc, shSCHTRS_yyss);
        shSCHTRS_YYSTACK_RELOCATE (shSCHTRS_yyvs_alloc, shSCHTRS_yyvs);
# undef shSCHTRS_YYSTACK_RELOCATE
        if (shSCHTRS_yyss1 != shSCHTRS_yyssa)
          shSCHTRS_YYSTACK_FREE (shSCHTRS_yyss1);
      }
# endif

      shSCHTRS_yyssp = shSCHTRS_yyss + shSCHTRS_yysize - 1;
      shSCHTRS_yyvsp = shSCHTRS_yyvs + shSCHTRS_yysize - 1;

      shSCHTRS_YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long) shSCHTRS_yystacksize));

      if (shSCHTRS_yyss + shSCHTRS_yystacksize - 1 <= shSCHTRS_yyssp)
        shSCHTRS_YYABORT;
    }
#endif /* !defined shSCHTRS_yyoverflow && !defined shSCHTRS_YYSTACK_RELOCATE */

  shSCHTRS_YYDPRINTF ((stderr, "Entering state %d\n", shSCHTRS_yystate));

  if (shSCHTRS_yystate == shSCHTRS_YYFINAL)
    shSCHTRS_YYACCEPT;

  goto shSCHTRS_yybackup;


/*-----------.
| shSCHTRS_yybackup.  |
`-----------*/
shSCHTRS_yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  shSCHTRS_yyn = shSCHTRS_yypact[shSCHTRS_yystate];
  if (shSCHTRS_yypact_value_is_default (shSCHTRS_yyn))
    goto shSCHTRS_yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* shSCHTRS_YYCHAR is either shSCHTRS_YYEMPTY or shSCHTRS_YYEOF or a valid lookahead symbol.  */
  if (shSCHTRS_yychar == shSCHTRS_YYEMPTY)
    {
      shSCHTRS_YYDPRINTF ((stderr, "Reading a token: "));
      shSCHTRS_yychar = shSCHTRS_yylex ();
    }

  if (shSCHTRS_yychar <= shSCHTRS_YYEOF)
    {
      shSCHTRS_yychar = shSCHTRS_yytoken = shSCHTRS_YYEOF;
      shSCHTRS_YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      shSCHTRS_yytoken = shSCHTRS_YYTRANSLATE (shSCHTRS_yychar);
      shSCHTRS_YY_SYMBOL_PRINT ("Next token is", shSCHTRS_yytoken, &shSCHTRS_yylval, &shSCHTRS_yylloc);
    }

  /* If the proper action on seeing token shSCHTRS_YYTOKEN is to reduce or to
     detect an error, take that action.  */
  shSCHTRS_yyn += shSCHTRS_yytoken;
  if (shSCHTRS_yyn < 0 || shSCHTRS_YYLAST < shSCHTRS_yyn || shSCHTRS_yycheck[shSCHTRS_yyn] != shSCHTRS_yytoken)
    goto shSCHTRS_yydefault;
  shSCHTRS_yyn = shSCHTRS_yytable[shSCHTRS_yyn];
  if (shSCHTRS_yyn <= 0)
    {
      if (shSCHTRS_yytable_value_is_error (shSCHTRS_yyn))
        goto shSCHTRS_yyerrlab;
      shSCHTRS_yyn = -shSCHTRS_yyn;
      goto shSCHTRS_yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (shSCHTRS_yyerrstatus)
    shSCHTRS_yyerrstatus--;

  /* Shift the lookahead token.  */
  shSCHTRS_YY_SYMBOL_PRINT ("Shifting", shSCHTRS_yytoken, &shSCHTRS_yylval, &shSCHTRS_yylloc);

  /* Discard the shifted token.  */
  shSCHTRS_yychar = shSCHTRS_YYEMPTY;

  shSCHTRS_yystate = shSCHTRS_yyn;
  shSCHTRS_YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++shSCHTRS_yyvsp = shSCHTRS_yylval;
  shSCHTRS_YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto shSCHTRS_yynewstate;


/*-----------------------------------------------------------.
| shSCHTRS_yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
shSCHTRS_yydefault:
  shSCHTRS_yyn = shSCHTRS_yydefact[shSCHTRS_yystate];
  if (shSCHTRS_yyn == 0)
    goto shSCHTRS_yyerrlab;
  goto shSCHTRS_yyreduce;


/*-----------------------------.
| shSCHTRS_yyreduce -- do a reduction.  |
`-----------------------------*/
shSCHTRS_yyreduce:
  /* shSCHTRS_yyn is the number of a rule to reduce with.  */
  shSCHTRS_yylen = shSCHTRS_yyr2[shSCHTRS_yyn];

  /* If shSCHTRS_YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets shSCHTRS_YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to shSCHTRS_YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that shSCHTRS_YYVAL may be used uninitialized.  */
  shSCHTRS_yyval = shSCHTRS_yyvsp[1-shSCHTRS_yylen];


  shSCHTRS_YY_REDUCE_PRINT (shSCHTRS_yyn);
  switch (shSCHTRS_yyn)
    {
        case 4:
#line 75 "shSchemaTransPsr.y" /* yacc.c:1652  */
    {
              shSchemaTransEntryAdd(g_xtbl,
                g_entryNames.type,
                g_entryNames.fits,
                g_entryNames.obj,
                g_entryNames.dtype,
               (htypeGood)?g_entryNames.htype:NULL,
               (hlenGood)?g_entryNames.hlen:NULL,
               (procGood)?g_entryNames.proc:NULL,
               (dimenGood)?g_entryNames.dimen:NULL,
               (ratioGood)?atof(g_entryNames.ratio):1.0,
                -1);
              switchInit();


          }
#line 1302 "y.tab.c" /* yacc.c:1652  */
    break;

  case 5:
#line 92 "shSchemaTransPsr.y" /* yacc.c:1652  */
    {
             g_entryNames.type = CONVERSION_IGNORE;
             shSchemaTransEntryAdd(g_xtbl,
                g_entryNames.type,
                "",                     /* g_entryNames.fits */
                g_entryNames.obj,
                "int",                     /* g_entryNames.dtype */
               NULL,
               NULL,
               NULL,
               NULL,
               1.0,
               -1);
              
             switchInit();
         }
#line 1323 "y.tab.c" /* yacc.c:1652  */
    break;

  case 6:
#line 110 "shSchemaTransPsr.y" /* yacc.c:1652  */
    { g_entryNames.type = CONVERSION_BY_TYPE;}
#line 1329 "y.tab.c" /* yacc.c:1652  */
    break;

  case 7:
#line 111 "shSchemaTransPsr.y" /* yacc.c:1652  */
    { g_entryNames.type = CONVERSION_CONTINUE;}
#line 1335 "y.tab.c" /* yacc.c:1652  */
    break;

  case 13:
#line 123 "shSchemaTransPsr.y" /* yacc.c:1652  */
    {
                   strcpy(g_entryNames.proc, shSCHTRS_yyvsp[0].name); 
                   procGood = TRUE;
               }
#line 1344 "y.tab.c" /* yacc.c:1652  */
    break;

  case 14:
#line 128 "shSchemaTransPsr.y" /* yacc.c:1652  */
    {
                   strcpy(g_entryNames.dimen, shSCHTRS_yyvsp[0].name);
                   dimenGood = TRUE;
               }
#line 1353 "y.tab.c" /* yacc.c:1652  */
    break;

  case 15:
#line 133 "shSchemaTransPsr.y" /* yacc.c:1652  */
    {
                   strcpy(g_entryNames.ratio, shSCHTRS_yyvsp[0].name);
                   ratioGood = TRUE ;
                }
#line 1362 "y.tab.c" /* yacc.c:1652  */
    break;

  case 16:
#line 138 "shSchemaTransPsr.y" /* yacc.c:1652  */
    {
                   strcpy(g_entryNames.htype, shSCHTRS_yyvsp[0].name);
                   htypeGood = TRUE;
               }
#line 1371 "y.tab.c" /* yacc.c:1652  */
    break;

  case 17:
#line 143 "shSchemaTransPsr.y" /* yacc.c:1652  */
    {
                    strcpy(g_entryNames.hlen, shSCHTRS_yyvsp[0].name);
                    hlenGood = TRUE;
               }
#line 1380 "y.tab.c" /* yacc.c:1652  */
    break;

  case 18:
#line 149 "shSchemaTransPsr.y" /* yacc.c:1652  */
    {strcpy(g_entryNames.fits, shSCHTRS_yyvsp[0].name);}
#line 1386 "y.tab.c" /* yacc.c:1652  */
    break;

  case 19:
#line 152 "shSchemaTransPsr.y" /* yacc.c:1652  */
    {strcpy(g_entryNames.obj, shSCHTRS_yyvsp[0].name);}
#line 1392 "y.tab.c" /* yacc.c:1652  */
    break;

  case 20:
#line 155 "shSchemaTransPsr.y" /* yacc.c:1652  */
    {strcpy(g_entryNames.dtype, shSCHTRS_yyvsp[0].name);}
#line 1398 "y.tab.c" /* yacc.c:1652  */
    break;


#line 1402 "y.tab.c" /* yacc.c:1652  */
      default: break;
    }
  /* User semantic actions sometimes alter shSCHTRS_yychar, and that requires
     that shSCHTRS_yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of shSCHTRS_yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     shSCHTRS_YYABORT, shSCHTRS_YYACCEPT, or shSCHTRS_YYERROR immediately after altering shSCHTRS_yychar or
     if it invokes shSCHTRS_YYBACKUP.  In the case of shSCHTRS_YYABORT or shSCHTRS_YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of shSCHTRS_YYERROR or shSCHTRS_YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  shSCHTRS_YY_SYMBOL_PRINT ("-> $$ =", shSCHTRS_yyr1[shSCHTRS_yyn], &shSCHTRS_yyval, &shSCHTRS_yyloc);

  shSCHTRS_YYPOPSTACK (shSCHTRS_yylen);
  shSCHTRS_yylen = 0;
  shSCHTRS_YY_STACK_PRINT (shSCHTRS_yyss, shSCHTRS_yyssp);

  *++shSCHTRS_yyvsp = shSCHTRS_yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int shSCHTRS_yylhs = shSCHTRS_yyr1[shSCHTRS_yyn] - shSCHTRS_YYNTOKENS;
    const int shSCHTRS_yyi = shSCHTRS_yypgoto[shSCHTRS_yylhs] + *shSCHTRS_yyssp;
    shSCHTRS_yystate = (0 <= shSCHTRS_yyi && shSCHTRS_yyi <= shSCHTRS_YYLAST && shSCHTRS_yycheck[shSCHTRS_yyi] == *shSCHTRS_yyssp
               ? shSCHTRS_yytable[shSCHTRS_yyi]
               : shSCHTRS_yydefgoto[shSCHTRS_yylhs]);
  }

  goto shSCHTRS_yynewstate;


/*--------------------------------------.
| shSCHTRS_yyerrlab -- here on detecting error.  |
`--------------------------------------*/
shSCHTRS_yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  shSCHTRS_yytoken = shSCHTRS_yychar == shSCHTRS_YYEMPTY ? shSCHTRS_YYEMPTY : shSCHTRS_YYTRANSLATE (shSCHTRS_yychar);

  /* If not already recovering from an error, report this error.  */
  if (!shSCHTRS_yyerrstatus)
    {
      ++shSCHTRS_yynerrs;
#if ! shSCHTRS_YYERROR_VERBOSE
      shSCHTRS_yyerror (shSCHTRS_YY_("syntax error"));
#else
# define shSCHTRS_YYSYNTAX_ERROR shSCHTRS_yysyntax_error (&shSCHTRS_yymsg_alloc, &shSCHTRS_yymsg, \
                                        shSCHTRS_yyssp, shSCHTRS_yytoken)
      {
        char const *shSCHTRS_yymsgp = shSCHTRS_YY_("syntax error");
        int shSCHTRS_yysyntax_error_status;
        shSCHTRS_yysyntax_error_status = shSCHTRS_YYSYNTAX_ERROR;
        if (shSCHTRS_yysyntax_error_status == 0)
          shSCHTRS_yymsgp = shSCHTRS_yymsg;
        else if (shSCHTRS_yysyntax_error_status == 1)
          {
            if (shSCHTRS_yymsg != shSCHTRS_yymsgbuf)
              shSCHTRS_YYSTACK_FREE (shSCHTRS_yymsg);
            shSCHTRS_yymsg = (char *) shSCHTRS_YYSTACK_ALLOC (shSCHTRS_yymsg_alloc);
            if (!shSCHTRS_yymsg)
              {
                shSCHTRS_yymsg = shSCHTRS_yymsgbuf;
                shSCHTRS_yymsg_alloc = sizeof shSCHTRS_yymsgbuf;
                shSCHTRS_yysyntax_error_status = 2;
              }
            else
              {
                shSCHTRS_yysyntax_error_status = shSCHTRS_YYSYNTAX_ERROR;
                shSCHTRS_yymsgp = shSCHTRS_yymsg;
              }
          }
        shSCHTRS_yyerror (shSCHTRS_yymsgp);
        if (shSCHTRS_yysyntax_error_status == 2)
          goto shSCHTRS_yyexhaustedlab;
      }
# undef shSCHTRS_YYSYNTAX_ERROR
#endif
    }



  if (shSCHTRS_yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (shSCHTRS_yychar <= shSCHTRS_YYEOF)
        {
          /* Return failure if at end of input.  */
          if (shSCHTRS_yychar == shSCHTRS_YYEOF)
            shSCHTRS_YYABORT;
        }
      else
        {
          shSCHTRS_yydestruct ("Error: discarding",
                      shSCHTRS_yytoken, &shSCHTRS_yylval);
          shSCHTRS_yychar = shSCHTRS_YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto shSCHTRS_yyerrlab1;


/*---------------------------------------------------.
| shSCHTRS_yyerrorlab -- error raised explicitly by shSCHTRS_YYERROR.  |
`---------------------------------------------------*/
shSCHTRS_yyerrorlab:
  /* Pacify compilers when the user code never invokes shSCHTRS_YYERROR and the
     label shSCHTRS_yyerrorlab therefore never appears in user code.  */
  if (0)
    shSCHTRS_YYERROR;

  /* Do not reclaim the symbols of the rule whose action triggered
     this shSCHTRS_YYERROR.  */
  shSCHTRS_YYPOPSTACK (shSCHTRS_yylen);
  shSCHTRS_yylen = 0;
  shSCHTRS_YY_STACK_PRINT (shSCHTRS_yyss, shSCHTRS_yyssp);
  shSCHTRS_yystate = *shSCHTRS_yyssp;
  goto shSCHTRS_yyerrlab1;


/*-------------------------------------------------------------.
| shSCHTRS_yyerrlab1 -- common code for both syntax error and shSCHTRS_YYERROR.  |
`-------------------------------------------------------------*/
shSCHTRS_yyerrlab1:
  shSCHTRS_yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      shSCHTRS_yyn = shSCHTRS_yypact[shSCHTRS_yystate];
      if (!shSCHTRS_yypact_value_is_default (shSCHTRS_yyn))
        {
          shSCHTRS_yyn += shSCHTRS_YYTERROR;
          if (0 <= shSCHTRS_yyn && shSCHTRS_yyn <= shSCHTRS_YYLAST && shSCHTRS_yycheck[shSCHTRS_yyn] == shSCHTRS_YYTERROR)
            {
              shSCHTRS_yyn = shSCHTRS_yytable[shSCHTRS_yyn];
              if (0 < shSCHTRS_yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (shSCHTRS_yyssp == shSCHTRS_yyss)
        shSCHTRS_YYABORT;


      shSCHTRS_yydestruct ("Error: popping",
                  shSCHTRS_yystos[shSCHTRS_yystate], shSCHTRS_yyvsp);
      shSCHTRS_YYPOPSTACK (1);
      shSCHTRS_yystate = *shSCHTRS_yyssp;
      shSCHTRS_YY_STACK_PRINT (shSCHTRS_yyss, shSCHTRS_yyssp);
    }

  shSCHTRS_YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++shSCHTRS_yyvsp = shSCHTRS_yylval;
  shSCHTRS_YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  shSCHTRS_YY_SYMBOL_PRINT ("Shifting", shSCHTRS_yystos[shSCHTRS_yyn], shSCHTRS_yyvsp, shSCHTRS_yylsp);

  shSCHTRS_yystate = shSCHTRS_yyn;
  goto shSCHTRS_yynewstate;


/*-------------------------------------.
| shSCHTRS_yyacceptlab -- shSCHTRS_YYACCEPT comes here.  |
`-------------------------------------*/
shSCHTRS_yyacceptlab:
  shSCHTRS_yyresult = 0;
  goto shSCHTRS_yyreturn;


/*-----------------------------------.
| shSCHTRS_yyabortlab -- shSCHTRS_YYABORT comes here.  |
`-----------------------------------*/
shSCHTRS_yyabortlab:
  shSCHTRS_yyresult = 1;
  goto shSCHTRS_yyreturn;


#if !defined shSCHTRS_yyoverflow || shSCHTRS_YYERROR_VERBOSE
/*-------------------------------------------------.
| shSCHTRS_yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
shSCHTRS_yyexhaustedlab:
  shSCHTRS_yyerror (shSCHTRS_YY_("memory exhausted"));
  shSCHTRS_yyresult = 2;
  /* Fall through.  */
#endif


/*-----------------------------------------------------.
| shSCHTRS_yyreturn -- parsing is finished, return the result.  |
`-----------------------------------------------------*/
shSCHTRS_yyreturn:
  if (shSCHTRS_yychar != shSCHTRS_YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      shSCHTRS_yytoken = shSCHTRS_YYTRANSLATE (shSCHTRS_yychar);
      shSCHTRS_yydestruct ("Cleanup: discarding lookahead",
                  shSCHTRS_yytoken, &shSCHTRS_yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this shSCHTRS_YYABORT or shSCHTRS_YYACCEPT.  */
  shSCHTRS_YYPOPSTACK (shSCHTRS_yylen);
  shSCHTRS_YY_STACK_PRINT (shSCHTRS_yyss, shSCHTRS_yyssp);
  while (shSCHTRS_yyssp != shSCHTRS_yyss)
    {
      shSCHTRS_yydestruct ("Cleanup: popping",
                  shSCHTRS_yystos[*shSCHTRS_yyssp], shSCHTRS_yyvsp);
      shSCHTRS_YYPOPSTACK (1);
    }
#ifndef shSCHTRS_yyoverflow
  if (shSCHTRS_yyss != shSCHTRS_yyssa)
    shSCHTRS_YYSTACK_FREE (shSCHTRS_yyss);
#endif
#if shSCHTRS_YYERROR_VERBOSE
  if (shSCHTRS_yymsg != shSCHTRS_yymsgbuf)
    shSCHTRS_YYSTACK_FREE (shSCHTRS_yymsg);
#endif
  return shSCHTRS_yyresult;
}
#line 158 "shSchemaTransPsr.y" /* yacc.c:1918  */


void switchInit(void)
{
    	ratioGood = FALSE;
 	dimenGood = FALSE;
	procGood = FALSE;
	htypeGood = FALSE;
	hlenGood = FALSE;
}

#if !defined(DARWIN)
   extern int shSCHTRS_yylineno;
#endif
extern char shSCHTRS_yytext[];
extern int shSCHTRS_yyleng;
extern FILE* shSCHTRS_yyin;

void shSCHTRS_yyerror(char* s)
{
#if defined(DARWIN)
   shErrStackPush("%s at line ???\n", s);
#else
   shErrStackPush("%s at line %d\n", s, shSCHTRS_yylineno);
#endif
}


void get_name(int dequote)
{
    
    

    if(!dequote)strcpy(shSCHTRS_yylval.name, shSCHTRS_yytext);
    else 
    {
       strcpy(shSCHTRS_yylval.name, shSCHTRS_yytext+1);
       shSCHTRS_yylval.name[shSCHTRS_yyleng-2] = '\0';
    }
       

}


RET_CODE
shSchemaTransCreateFromFile(SCHEMATRANS *xtbl_ptr,
                   char* fileName)
{
    
    FILE* fp;

    fp = fopen(fileName, "r");
    if(fp == NULL)
    {
        shErrStackPush("Can't open file %s", fileName);
        return SH_GENERIC_ERROR;
    }
    shSCHTRS_yyin = fp;
    g_xtbl = xtbl_ptr;

    if(shSCHTRS_yyparse()!=0) 
    {
       shErrStackPush("Parser failed");
       return SH_GENERIC_ERROR;
    }
    return SH_SUCCESS;
}
