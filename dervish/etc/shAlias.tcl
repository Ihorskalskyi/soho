#
#  This file contains variaous alias declarations for Dervish.
#
alias arrayNew "handleNewFromType ARRAY"
alias arrayDel "handleDelFromType"
#
alias tblColNew "handleNewFromType TBLCOL"
alias tblColDel "handleDelFromType"
#
alias demo "source [envscan \$DERVISH_DIR]/demos/demo.tcl;d_demo"