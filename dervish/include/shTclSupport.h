#ifndef _SHTCLSUPPORT_H
#define _SHTCLSUPPORT_H

/*
 * All dervish TCL support routine function prototypes
 */
/*****************************************************************************
******************************************************************************
**
** FILE:
**      shTclSupport.h
**
** ABSTRACT:
**      This file contains all necessary definitions, macros, etc., for
**      the dervish TCL support routines
**
** ENVIRONMENT:
**      ANSI C.
**
** AUTHOR:
**      Creation date: May. 17, 1992
**
******************************************************************************
******************************************************************************
*/

/*----------------------------------------------------------------------------
**
** DEFINITIONS
*/

/*----------------------------------------------------------------------------
**
** STRUCTURE DEFINITIONS
*/

#endif
