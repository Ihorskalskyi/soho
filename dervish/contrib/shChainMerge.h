#ifndef SHCHAINMERGE_H
#define SHCHAINMERGE_H

/* Pointers to matching objects. */

typedef struct shmerge {
   void *object1;
   void *object2;
   } SHMERGE;   /* pragma SCHEMA */

#endif
