/*
 * This file was machine generated from ../contrib/shChainMerge.h ../contrib/shCProgram.h 
 * on Sun Nov 29 13:17:20 2020
 *
 * Don't even think about editing it by hand
 */
#include <stdio.h>
#include <string.h>
#include "dervish_msg_c.h"
#include "shChainMerge.h"
#include "shCProgram.h"
#include "shCSchema.h"
#include "shCUtils.h"
#include "shCDiskio.h"
#include "prvt/utils_p.h"

#define NSIZE 100		/* size to read strings*/

#define _STRING(S) #S
#define STRING(S) _STRING(S)		/* convert a cpp value to a string*/

static SCHEMA_ELEM schema_elems0[] = { /* SHMERGE */
   { "object1", "void", 0, 1, 0, NULL, 0, offsetof(SHMERGE,object1) },
   { "object2", "void", 0, 1, 0, NULL, 0, offsetof(SHMERGE,object2) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems1[] = { /* PROGEXEC */
   { "PROG_ADD", "PROGEXEC", (int) PROG_ADD, 0, 0, NULL, 0, 0 },
   { "PROG_DEREF", "PROGEXEC", (int) PROG_DEREF, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems2[] = { /* PROGSTEP */
   { "prog", "PROGEXEC", 0, 0, 0, NULL, 0, offsetof(PROGSTEP,prog) },
   { "arg", "int", 0, 0, 0, NULL, 0, offsetof(PROGSTEP,arg) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems3[] = { /* PROGRAM */
   { "nstep", "int", 0, 0, 0, NULL, 0, offsetof(PROGRAM,nstep) },
   { "type", "TYPE", 0, 0, 0, NULL, 0, offsetof(PROGRAM,type) },
   { "nstar", "int", 0, 0, 0, NULL, 0, offsetof(PROGRAM,nstar) },
   { "sdim", "int", 0, 1, 0, NULL, 0, offsetof(PROGRAM,sdim) },
   { "ndim", "int", 0, 0, 0, NULL, 0, offsetof(PROGRAM,ndim) },
   { "dim", "int", 0, 1, 0, NULL, 0, offsetof(PROGRAM,dim) },
   { "nitem", "int", 0, 0, 0, NULL, 0, offsetof(PROGRAM,nitem) },
   { "isize", "int", 0, 0, 0, NULL, 0, offsetof(PROGRAM,isize) },
   { "nout", "int", 0, 0, 0, NULL, 0, offsetof(PROGRAM,nout) },
   { "size", "int", 0, 0, 0, NULL, 0, offsetof(PROGRAM,size) },
   { "prog", "PROGSTEP", 0, 1, 0, NULL, 0, offsetof(PROGRAM,prog) },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems4[] = { /* TYPE */
   { "", "TYPE", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems5[] = { /* CHAR */
   { "", "char", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems6[] = { /* UCHAR */
   { "", "unsigned char", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems7[] = { /* INT */
   { "", "int", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems8[] = { /* UINT */
   { "", "unsigned int", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems9[] = { /* SHORT */
   { "", "short", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems10[] = { /* USHORT */
   { "", "unsigned short", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems11[] = { /* LONG */
   { "", "long", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems12[] = { /* ULONG */
   { "", "unsigned long", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems13[] = { /* FLOAT */
   { "", "float", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems14[] = { /* DOUBLE */
   { "", "double", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems15[] = { /* LOGICAL */
   { "", "logical", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems16[] = { /* STR */
   { "", "char", 0, 1, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems17[] = { /* PTR */
   { "", "ptr", 0, 1, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems18[] = { /* FUNCTION */
   { "", "function", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA_ELEM schema_elems19[] = { /* SCHAR */
   { "", "signed char", 0, 0, 0, NULL, 0, 0 },
   { "", "", -1, 0, 0, NULL, 0 , -1 },
};

static SCHEMA schema[] = {
   { "SHMERGE", STRUCT, sizeof(SHMERGE), 2, schema_elems0,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "PROGEXEC", ENUM, sizeof(PROGEXEC), 2, schema_elems1,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "PROGSTEP", STRUCT, sizeof(PROGSTEP), 2, schema_elems2,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "PROGRAM", STRUCT, sizeof(PROGRAM), 11, schema_elems3,
     (void *(*)(void))NULL,
     (void *(*)(void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     0,
   },
   { "TYPE", PRIM, sizeof(TYPE), 1, schema_elems4,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpTypeRead,
     (RET_CODE(*)(FILE *,void *))shDumpTypeWrite,
     3,
   },
   { "CHAR", PRIM, sizeof(char), 1, schema_elems5,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpCharRead,
     (RET_CODE(*)(FILE *,void *))shDumpCharWrite,
     3,
   },
   { "UCHAR", PRIM, sizeof(unsigned char), 1, schema_elems6,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpUcharRead,
     (RET_CODE(*)(FILE *,void *))shDumpUcharWrite,
     3,
   },
   { "INT", PRIM, sizeof(int), 1, schema_elems7,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpIntRead,
     (RET_CODE(*)(FILE *,void *))shDumpIntWrite,
     3,
   },
   { "UINT", PRIM, sizeof(unsigned int), 1, schema_elems8,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpUintRead,
     (RET_CODE(*)(FILE *,void *))shDumpUintWrite,
     3,
   },
   { "SHORT", PRIM, sizeof(short), 1, schema_elems9,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpShortRead,
     (RET_CODE(*)(FILE *,void *))shDumpShortWrite,
     3,
   },
   { "USHORT", PRIM, sizeof(unsigned short), 1, schema_elems10,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpUshortRead,
     (RET_CODE(*)(FILE *,void *))shDumpUshortWrite,
     3,
   },
   { "LONG", PRIM, sizeof(long), 1, schema_elems11,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpLongRead,
     (RET_CODE(*)(FILE *,void *))shDumpLongWrite,
     3,
   },
   { "ULONG", PRIM, sizeof(unsigned long), 1, schema_elems12,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpUlongRead,
     (RET_CODE(*)(FILE *,void *))shDumpUlongWrite,
     3,
   },
   { "FLOAT", PRIM, sizeof(float), 1, schema_elems13,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpFloatRead,
     (RET_CODE(*)(FILE *,void *))shDumpFloatWrite,
     3,
   },
   { "DOUBLE", PRIM, sizeof(double), 1, schema_elems14,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpDoubleRead,
     (RET_CODE(*)(FILE *,void *))shDumpDoubleWrite,
     3,
   },
   { "LOGICAL", PRIM, sizeof(unsigned char), 1, schema_elems15,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpLogicalRead,
     (RET_CODE(*)(FILE *,void *))shDumpLogicalWrite,
     3,
   },
   { "STR", PRIM, sizeof(char *), 1, schema_elems16,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpStrRead,
     (RET_CODE(*)(FILE *,void *))shDumpStrWrite,
     3,
   },
   { "PTR", PRIM, sizeof(void *), 1, schema_elems17,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpPtrRead,
     (RET_CODE(*)(FILE *,void *))shDumpPtrWrite,
     3,
   },
   { "FUNCTION", PRIM, 0, 1, schema_elems18,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     (RET_CODE(*)(FILE *,void *))NULL,
     3,
   },
   { "SCHAR", PRIM, sizeof(signed char), 1, schema_elems19,
     NULL, NULL,
     (RET_CODE(*)(FILE *,void *))shDumpScharRead,
     (RET_CODE(*)(FILE *,void *))shDumpScharWrite,
     3,
   },
   { "", UNKNOWN, 0, 0,  NULL, NULL, NULL, NULL, NULL, SCHEMA_LOCK},
};

RET_CODE
shSchemaLoadFromContrib(void)
{
   return(p_shSchemaLoad(schema));
}

