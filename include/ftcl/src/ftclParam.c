/*
 *							jms 4jun93
 * functions to support parameter values
 *
 * copied from SDSS, from Robert Lupton, Princeton.
 *
 * Modifications:  
 *     12 may 95  mvw . Changed the call to strtol to strtoul.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static char *get_val(char *);
static int set_defaults_file(char *, char *);

#define ERROR	1
#define OK 	0

#ifndef FILENAME_MAX    
#define FILENAME_MAX    255
#endif

char ftclDefaultsFile[FILENAME_MAX];

/*****************************************************************************/
/*
 * Here are the three public C functions (and one private one, initParams)
 */
int ftclIniPar(defaults_file,home_dir)
char *defaults_file;		/* name of defaults file */
char *home_dir;		/* user's home directory */

{
   return(set_defaults_file(defaults_file,home_dir));
}

int ftclGetParF(char *name, float *value)
{
   char *val;

   if((val = get_val(name)) == NULL) {
      *value = 0.0;
      return(ERROR);
   } else {
      *value = atof(val);
      return(OK);
   }
}

int ftclGetParI(char *name, int *value)
{
   char *val;

   if((val = get_val(name)) == NULL) {
      *value = 0;
      return(ERROR);
   } else {
      *value = strtoul(val,(char **) NULL, 0);
      return(OK);
   }
}

int ftclGetParS(char *name, char *value, int len)
{
   char *val;

   if((val = get_val(name)) == NULL) {
      value[0] = '\0';
      return(ERROR);
   } else {
      strncpy(value,val,len);
      return(OK);
   }
}

/*****************************************************************************/
/*
 * Here are the four public FORTRAN functions 
 */
int ftclinipar_(char *file, char *directory)
{
return(ftclIniPar(file,directory));
}
int ftclgetparf_(char *name, float *value)
{
return(ftclGetParF(name,value));
}
int ftclgetpari_(char *name, int *value)
{
return(ftclGetParI(name,value));
}
int ftclgetpars_(char *name, char *value, int *len)
{
return(ftclGetParS(name,value,*len));
}
/*
/*****************************************************************************/
/*
 * And here is the code to read parameters from disk
 */
#include <string.h>
#include <ctype.h>

#define NPATH 5				/* max number of elements on path */
#define SIZE 1000

char cchar = '#';			/* comment character */
static int verbose = 0;			/* should I be chatty? */
static char *path[NPATH + 1] = {	/* path to search for files */
   "",
   "~",
   NULL,
};

static int set_defaults_file(file,home)
char *file;				/* .pipeline file */
char *home;				/* home directory */
{
   int i;
   char *ptr = NULL;
   static char *s_path = NULL;

   if(file != NULL) strncpy(ftclDefaultsFile,file,FILENAME_MAX);

#if defined(DEFAULT_PATH)
   ptr = DEFAULT_PATH;
#else
   ptr = ". ~";
#endif
   
   if(s_path != NULL) free(s_path);
   if((s_path = malloc(strlen(ptr) + 1)) == NULL) {
      fprintf(stderr,"Can't allocate storage for s_path\n");
      path[0] = ""; path[1] = NULL;
      return(ERROR);
   }
   ptr = strcpy(s_path,ptr);
   for(i = 0;i < NPATH;i++) {
      path[i] = ptr;
      if((ptr = strchr(path[i],' ')) == NULL) {
	 break;
      }
      *ptr++ = '\0';
   }
   path[++i] = NULL;

   for(i = 0;path[i] != NULL;i++) {
#if defined(VMS)
      if(strcmp(path[i],".") == 0) {
	 path[i] = "";
      } else if(strcmp(path[i],"~") == 0) {
	 if(home == NULL) {
	    path[i] = "sys$login:";
	 }
      }
#else
      if(strcmp(path[i],"~") == 0) {
	 if(home == NULL) {
	    if((home = getenv("HOME")) == NULL) {
	       fprintf(stderr,"Can't find home directory\n");
	       path[i] = "";
	       return(ERROR);
	    }
	 }
	 if((path[i] = malloc(strlen(home) + 2)) == NULL) {
	    fprintf(stderr,"Can't allocate storage for home directory\n");
	    path[i] = "";
	    return(ERROR);
	 }
	 (void)sprintf(path[i],"%s/",home);
      }
#endif
   }
   return(OK);
}

/*****************************************************************************/

static char *
get_val(what)
char *what;				/* desired property */
{
   char file[FILENAME_MAX],
	in_line[SIZE],			/* line from file */
	name[SIZE],			/* name of attribute */
	*vptr;				/* pointer to value */
   static char value[SIZE];		/* the desired value */
   FILE *fil;
   int concatenate;			/* concatenate values from .pipeline
					   files?*/
   int i,j,k;
   static int found = 0,		/* have we found a .pipeline file? */
	      warned = 0;		/* have we moaned about !found? */

   vptr = value;
   for(k = 0;path[k] != NULL;k++) {
      if(strcmp(path[k],".") == 0) {
	 strcpy(file,ftclDefaultsFile);
      } else {
	 (void)sprintf(file,"%s%s",path[k],ftclDefaultsFile);
      }
      if((fil = fopen(file,"r")) == NULL) {
	 continue;
      }
      found = 1;			/* we found a .pipeline file */

      concatenate = 0;
      while(fgets(in_line,SIZE,fil) != NULL) {
	 if(in_line[0] == cchar) {		/* a comment */
	    continue;
	 } else if(in_line[0] == '@' || in_line[0] == '+') {
	    (void)sscanf(&in_line[1],"%s",name);
	 } else if ( isspace(in_line[0]) ) {
	    continue;
	 } else {
	    (void)sscanf(in_line,"%s",name);
	 }
	 if(strcmp(name,what) == 0) {
	    if(in_line[0] == '@') {	/* this entry is explicitly missing */
	       (void)fclose(fil);
	       return(vptr > value ? value : NULL);
	    } else if(in_line[0] == '+') {
	       concatenate = 1;
	    }

	    i = strlen(in_line) - 1;
	    if(in_line[i] == '\n') in_line[i] = '\0'; /* remove \n */
	    
	    for(i = 0;i < SIZE && in_line[i] != '\0' &&
		!isspace(in_line[i]);i++) ;	/* skip name */
	    for(;i < SIZE && isspace(in_line[i]);i++) ;	/* find value */
	    for(j = i;in_line[j] != '\0';j++) {
	       if(in_line[j] == cchar) {
		  j--;
		  if(j > i) {
		     while(isspace(in_line[j])) j--;
		  }
		  in_line[++j] = '\0';
		  break;
	       } else if(in_line[j] == '\\') {
		  if(in_line[j+1] == '\\' || in_line[j+1] == cchar) {
		     if(in_line[j + 2] != '\0') {
			j++;		/* skip escaped characters */
		     }
		  }
	       }
	    }
	    if(in_line[i] == '\0' || i == SIZE || i == j) {
	       if(concatenate) {
		  fprintf(stderr,
		   "You must specify a value if you use +name in .pipeline\n");
	       }
	       (void)fclose(fil);
	       return("1");
	    } else {
	       if(vptr > value) *vptr++ = ' ';
	       j = strlen(&in_line[i]);
	       if(vptr + j > value + SIZE) {
		  fprintf(stderr,
			 "Total value for %s in .pipeline is too long\n",what);
		  (void)fclose(fil);
		  return(vptr > value ? value : NULL);
	       }
	       strcpy(vptr,&in_line[i]);
	       vptr += j;
	       if(!concatenate) {
		  (void)fclose(fil);
		  return(value);
	       }
	    }
	 }
      }
      (void)fclose(fil);
   }
   if(!found && verbose && !warned) {
      warned = 1;
      fprintf(stderr,"Can't find a file \"%s\"\n",ftclDefaultsFile);
   }

   if(vptr > value) return(value);
   return(NULL);			/* not found */
}
