/* tclLists.c
 *	It contains routines that convert tcl Lists to 
 *	fortran arrays and vise versa.
 *
 *	21-May-93 PCF
 */
/*============================================================================*/
#include <stdlib.h>
#include <errno.h>
#ifndef VXWORKS
#include <stdio.h>
#endif
#include <string.h>
#include "tcl.h"

/*
 * Deliver a pointer to nfloat floating point numbers, or NULL.
 * If NULL, set interp->result appropriately. The caller is 
 * responsible for freeing the list with free();
 */
float *ftclList2Floats (Tcl_Interp *interp, char *list, int *nfloat, int maxflt) 
{
   int  i, status;
   char **largv;
   double tdouble;
   float  *fvals;

   if (Tcl_SplitList(interp, list, nfloat, &largv) != TCL_OK) { 
      return((float *)0);
   }
   if (maxflt == 0) {
      fvals = (float *)malloc (*nfloat*sizeof(float));
   } else {
      fvals = (float *)malloc (maxflt*sizeof(float));
   }
     
   if (fvals == (float *)0) {
      Tcl_SetResult(interp, "Internal (list2floats): malloc failure", TCL_VOLATILE);
      return (fvals);
   }
   i = 0;
   status = TCL_OK;
   if (*nfloat > 0) {
      while (i<*nfloat && (status = Tcl_GetDouble(interp, largv[i], &tdouble)) == TCL_OK ){
          fvals[i++] = tdouble;     
      }
   }
   free (largv);
   if (status != TCL_OK) {
      free (fvals);
      return ((float *)0);
   }
   return (fvals);
}
/*
 * Deliver a pointer to nint integer numbers, or NULL.
 * If NULL, set interp->result appropriately. The caller is 
 * responsible for freeing the list with free();
 */
int *ftclList2Ints (Tcl_Interp *interp, char *list, int *nint, int maxint) 
{
   int  i, status;
   char **largv;
   int  tint, *ivals;

   if (Tcl_SplitList(interp, list, nint, &largv) != TCL_OK) { 
      return((int *)0);
   }
   if (maxint == 0) {
      ivals = (int *)malloc (*nint*sizeof(int));
   } else {
      ivals = (int *)malloc (maxint*sizeof(int));
   }
     
   if (ivals == (int *)0) {
      Tcl_SetResult(interp, "Internal (list2ints): malloc failure", TCL_VOLATILE);
      return (ivals);
   }
   i = 0;
   status = TCL_OK;
   if (*nint > 0) {
     while (i<*nint && (status = Tcl_GetInt(interp, largv[i], &tint)) == TCL_OK ){
          ivals[i++] = tint;
      }
   }
   free (largv);
   if (status != TCL_OK) {
      free (ivals);
      return ((int *)0);
   }
   return (ivals);
}

/*
 *	It gets the elements of a two dimensional list into an array that is to
 *	be used by fortran routines.
 *      The input is a list as follows:
 *	assume that: float t[2][3]
 *	then the input TCL lists are specifying the values as follows:
 *
 *      {{t(0,0) t(0,1) t(0,2)} {t(1,0) t(1,1) t(1,2)}}
 *
 *	The returned list contains the floating values in the order shown:
 *	t(0,0) t(1,0) t(2,0) t(1,0) t(1,1) t(1,2)
 */
float *ftclLol2FtnFloats (Tcl_Interp *interp, char *list, int *ncount1, int *ncount2) 
{
   int  i, j, k, status;
   char **largv;
   double tdouble;
   float  *fvals;

   char   **argArray;
   int    argCount1, argCount2;

   if (Tcl_SplitList(interp, list, &argCount1, &argArray) != TCL_OK) 
      {
      free (argArray);
      return((float *)0);
      }

   *ncount1 = argCount1;

   for (j = 0; j < argCount1; j++) 
      {
      if (Tcl_SplitList(interp, argArray[j], &argCount2, &largv) != TCL_OK) 
         {
         free (largv);
         free (argArray);
         return((float *)0);
         }
      if (j == 0) 
         {
         *ncount2 = argCount2;
         fvals = (float *)malloc (argCount1*argCount2*(sizeof(float)));
         if (fvals == (float *)0) 
            {
            free (largv);
            free (argArray);
            Tcl_SetResult(interp, "Internal (lol2ffloats): malloc failure", TCL_VOLATILE);
            return (fvals);
            }
         }

      if (argCount2 != *ncount2) 
         {
         free (largv);
         free (argArray);
         fvals = (float *)0;
         Tcl_SetResult(interp, "Array elements must be of equal size", TCL_VOLATILE);
         return (fvals);
         }

      i = 0;
      while  (i<argCount2 && (status = Tcl_GetDouble(interp, largv[i], &tdouble)) == TCL_OK ){
         k = (argCount1*i) + j;
         fvals[k] = (float)tdouble;
         i++;
         }
      }

   free (largv);
   free (argArray);
   if (status != TCL_OK) 
      {
      free (fvals);
      return ((float *)0);
      }
   return (fvals);
}

/*
 *	It gets the elements of a two dimensional list into an array that is to
 *	be used by fortran routines.
 *      The input is a list as follows:
 *	assume that: int t[2][3]
 *	then the input TCL lists are specifying the values as follows:
 *
 *      {{t(0,0) t(0,1) t(0,2)} {t(1,0) t(1,1) t(1,2)}}
 *
 *	The returned list contains the floating values in the order shown:
 *	t(0,0) t(1,0) t(2,0) t(1,0) t(1,1) t(1,2)
 */
int *ftclLol2FtnInts (Tcl_Interp *interp, char *list, int *ncount1, int *ncount2) 
{
   int  i, j, k, status;
   char **largv;
   int  tint, *ivals;

   char   **argArray;
   int    argCount1, argCount2;

   if (Tcl_SplitList(interp, list, &argCount1, &argArray) != TCL_OK) 
      {
      free (argArray);
      return((int *)0);
      }

   *ncount1 = argCount1;

   for (j = 0; j < argCount1; j++) 
      {
      if (Tcl_SplitList(interp, argArray[j], &argCount2, &largv) != TCL_OK) 
         {
         free (largv);
         free (argArray);
         return((int *)0);
         }
      if (j == 0) 
         {
         *ncount2 = argCount2;
         ivals = (int *)malloc (argCount1*argCount2*(sizeof(int)));
         if (ivals == (int *)0) 
            {
            free (largv);
            free (argArray);
            Tcl_SetResult(interp, "Internal (lol2fints): malloc failure", TCL_VOLATILE);
            return (ivals);
            }
         }

      if (argCount2 != *ncount2) 
         {
         free (largv);
         free (argArray);
         ivals = (int *)0;
         Tcl_SetResult(interp, "Array elements must be of equal size", TCL_VOLATILE);
         return (ivals);
         }

      i = 0;
      while  (i<argCount2 && (status = Tcl_GetInt(interp, largv[i], &tint)) == TCL_OK ){
         k = (argCount1*i) + j;
         ivals[k] = tint;
         i++;
         }
      }

   free (largv);
   free (argArray);
   if (status != TCL_OK) 
      {
      free (ivals);
      return ((int *)0);
      }
   return (ivals);
}

/* Always returns TCL_OK.
 * Set Interp->result to a list representing fvals.
 * if "integerize" is not 0, then make the list an integer list.
 */
int ftclFloats2List (Tcl_Interp *interp, float *floats, int nfloats, int integerize) 
{
   int i, tmp;
   char s[300];
   char *format;
 
   for (i=0; i<nfloats; i++) {
      if (integerize) {
          sprintf (s, "%d", (int)floats[i]);
      }else{
          sprintf (s, "%f",      floats[i]);
      }  
      Tcl_AppendElement(interp, s);
   }
   return (TCL_OK);
}
