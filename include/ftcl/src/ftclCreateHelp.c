/* ================================================================================= */
/* This is a basic code to invoke the ftclHelpDisplay function from a C program      */

#include "ftcl.h"

/* --------------------------------------------------------------------------------- */
int cmdHelpDisplay(clientData, interp, argc, argv)
    ClientData clientData;
    Tcl_Interp *interp;
    int argc;
    char *argv[];
{

    char helpcmd[255];
    int i =0;
/* -------------------------------------------------------------------------------- */

    sprintf (helpcmd, "");		/* Initialize string  */
    
    if (argc == 1)
    {
       sprintf (helpcmd, "");
    }
    else if ( (strcmp (argv[1], "ftcl") == 0) )
    {
       sprintf (helpcmd, "ftcl");
    }
    else if (strcmp (argv[1], "help") == 0) 
    {
       sprintf (helpcmd, "help_cmd");
    }

    else if (argc > 1)

/* Encode all the arguments passed     */
    {   

      for (i=1; i<argc; i++) 
      {
        strcat (helpcmd, argv[i]);
        strcat (helpcmd, " ");
	
      }
    }
    ftclHelpDisplay (interp, helpcmd);

     return TCL_OK;
}
/* ================================================================================= */
/* ================================================================================= */

int ftclCreateHelp(interp)
    Tcl_Interp *interp;

{

/* Creating the help command using the new help				*/

     ftclHelpInit (interp); 		/* creating the needed procedures for help */

     Tcl_CreateCommand(interp,"help",cmdHelpDisplay, (ClientData) 0,
	    (Tcl_CmdDeleteProc *) NULL);

     return TCL_OK;
}
/* ================================================================================= */
