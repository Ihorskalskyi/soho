/* Modifications: 12-may-95 mvw: changed to call to strtol to strtoul. */

#include "symLib.h"
#include "tcl.h"

extern SYMTAB_ID sysSymTbl;  /* VxWorks System table. */


/****************************************************************************** 
* @+Public+@
* ROUTINE: getSymInt:                                                        */

int getSymInt
    (
    char *sym,       /* IN: The symbol to locate. */
    int *rvalue      /* OUT: The integer value of the symbol. */
    )
/*
 * DESCRIPTION
 *       Calls the specified VxWorks global routine.  Similar to the Unix "system" call.
 *
 * RETURN VALUES:       The return from symFindByName.
 *
 * SIDE EFFECTS:        None
 *
 * @-Public-@
 ******************************************************************************/
{  /* getSymInt */

  int rstatus;

  int  ival[10];     /* Symbol value. */
  SYM_TYPE ptype;    /* Symbol Type. */

  /* Locate the symbol and return it's integer value if it exists. */

  rstatus  = symFindByName(sysSymTbl, sym,  (void *) ival,  &ptype);
  if (rstatus != ERROR)
    {
    *rvalue = ival[0];
    }
  return rstatus;
} /* getSymInt */

/******************************************************************************
 * @+Public+@                                                                 *
 * ROUTINE: ftclVxSystemCmd:                                                 */

int ftclVxSystemCmd(cmd, pcnt, params)
    char *cmd;             /* IN: Command name to execute.  This must resolve to
                              a global VxWorks symbol.  Note that VxWorks
                              routine global symbols typically have "_" prepended.
                              This must be done by the caller. */
   int  pcnt;              /* IN: Number of parameters in param. */
   char *params;           /* IN: From 0 to 12 string parameters to pass to the command.
                              This routine will attempt to convert what appears
			      to be numbers to binary.  If a parameter is all
			      digits including '0x', it is assumed to be binary. */
/*
 * DESCRIPTION
 *       Calls the specified VxWorks global routine.  Similar to the Unix "system" call.
 *
 * RETURN VALUES:       The result of the VxWorks call.  If it does not exist,
 *                      then ERROR is return.
 *
 * SIDE EFFECTS:        None
 *
 * SYSTEM CONCERNS:     None.
 * @-Public-@
 ******************************************************************************/  


{  /* ftclVxSystemCmd */
  char **p;
  int (*fptr)();
  int pa[12];
  int pndx;
  char *ucmd;
  int rstatus;
  int isnum;    /* True if a parameter is a number. */

  rstatus = getSymInt(cmd, (void *) &fptr);   /* Locate the symbol. */
  if (rstatus == ERROR)
    {  /* Did not get it, try to prepend "_" as per vxworks standard. */
    if (ucmd = (char *) malloc(strlen(cmd) + 2))
      {
      strcpy(ucmd, "_");
      strcat(ucmd, cmd);
      rstatus = getSymInt(ucmd, (void *) &fptr);
      free(ucmd);
      }
    }

  p = &params;

  /* Convert all numbers to binary.  If the parameter is all digits including
     0x, we assume it's a number to be converted. */

  for (pndx = 0; pndx < 12; pndx ++)
    {

    if (pndx < pcnt) 
      {

      /* Check to see if it's a hex or decimal number. */
      isnum = FALSE;
      isnum = ( (! bcmp(p[pndx], "0x", 2) || ! bcmp(p[pndx],"0X", 2)) && (strcspn(&p[pndx][2], "0123456789ABCDEF") == 0) );
      if (!isnum) isnum = (strcspn(p[pndx], "0123456789") == 0);  /* Check for decimal. */
      if (!isnum) pa[pndx] = (int) p[pndx];   /* Non-number. */
      else pa[pndx] = strtoul(p[pndx]);  /* It's a number. */
      }
    else pa[pndx] = 0;  /* Zero unspecified parameters. */
  }  /* for ... */

  /* If function found, then call the function. */

  if (rstatus!= ERROR)
    {
    return (*fptr)(pa[0], pa[1], pa[2], pa[3], pa[4], pa[5],pa[6],pa[7],pa[8],pa[9],pa[10],pa[11]);
    }
  return rstatus;
}  /* ftclVxSystemCmd */




/******************************************************************************
 * @+Public+@                                                                 *
 * ROUTINE: ftclVxSystemCmdTCL:                                                 */

int  ftclVxSystemCmdTCL(clientData, interp, argc, argv)
   ClientData clientData;
   Tcl_Interp *interp;
   int argc;
   char *argv[];                   /* [1] = command to execute, [2..13] = parameters. */

/*
 * DESCRIPTION
 *       TCL interface to ftclVxSystemCmd
 *
 * RETURN VALUES:       None.
 *
 * SIDE EFFECTS:        None
 *
 * SYSTEM CONCERNS:     None.
 * @-Public-@
 ******************************************************************************/  

{  /* ftclVxSystemCmdTCL */

int rstatus = TCL_OK;

if ( (argc < 2) || (argc > 14)) 
    {
    printf("Usage: %s vxcmd [p1 p2...p12]\n", argv[0]);
    rstatus = TCL_ERROR;
    }

if (ftclVxSystemCmd(argv[1], argc - 2, argv[2], argv[3], argv[4], argv[5], argv[6], argv[7], argv[8], argv[9], argv[10], argv[11], argv[12], argv[13]) == ERROR) rstatus = TCL_ERROR;

return rstatus;
}  /* ftclVxSystemCmdTCL */



/******************************************************************************
 * @+Public+@                                                                 *
 * ROUTINE: ftclVxSystemCmdDeclare:                                                 */

void        ftclVxSystemCmdDeclare
   (
   Tcl_Interp *interp,    /* IN: TCL Interpeter to declare it in. */
   char       *cmd        /* IN: TCL command to use. */
   )
/*
 * DESCRIPTION
 *       Declares a TCL command to call ftclVxSystemCmd
 *
 * RETURN VALUES:       None.
 *
 * SIDE EFFECTS:        None
 *
 * SYSTEM CONCERNS:     None.
 * @-Public-@
 ******************************************************************************/  

{  /* ftclVxSystemCmdDeclare */

Tcl_CreateCommand(interp, cmd, ftclVxSystemCmdTCL, (ClientData) 0,  (void *) NULL);

}  /* ftclVxSystemCmdDeclare */






