/* ftclParams.c */

/*
 * tcl interfaces to the configuration parameter file
 *
*/

#include <stdio.h>
#include "tclExtend.h"

int ftclInitParam(ClientData clientdata, Tcl_Interp *interp, int argc, char **argv) {
/* Set the defaults file */

  switch ( argc ) {
     case 3: ftclIniPar(argv[1],argv[2]); break;
     case 2: ftclIniPar(argv[1],NULL); break;
     default: Tcl_AppendResult(interp, "Usage: ", argv[0], " <defaults_file> [home_dir]",(char *)NULL);
              return(TCL_ERROR);
	      break;
  };
  return(TCL_OK);
}


int ftclGetParamF(ClientData clientdata, Tcl_Interp *interp, int argc, char **argv) {
/* gets the value of a parameter from the database */

  float value;
  static char s[20];

  if (argc != 2) {
    Tcl_AppendResult(interp, argv[0], ": parametername",(char *)NULL);
    return(TCL_ERROR);
  }

  if(ftclGetParF(argv[1], &value)){
    Tcl_AppendResult(interp, argv[0], " error for par ",argv[1],(char *)NULL);
    return(TCL_ERROR);
    }

  sprintf(s, "%f", value);

  Tcl_AppendResult(interp, "", s, (char *)NULL);

  return(TCL_OK);

}

int ftclGetParamI(ClientData clientdata, Tcl_Interp *interp, int argc, char **argv) {
/* gets the value of a parameter from the database */

  int value;
  static char s[10];

  if (argc != 2) {
    Tcl_AppendResult(interp, argv[0], ": parametername",(char *)NULL);
    return(TCL_ERROR);
  }

  if(ftclGetParI(argv[1], &value)){
    Tcl_AppendResult(interp, argv[0], " error for par ",argv[1],(char *)NULL);
    return(TCL_ERROR);
    }

  sprintf(s, "%d", value);

  Tcl_AppendResult(interp, "", s, (char *)NULL);

  return(TCL_OK);

}

int ftclGetParamS(ClientData clientdata, Tcl_Interp *interp, int argc, char **argv) {
/* gets the value of a parameter from the database */

  static char s[FILENAME_MAX];

  if (argc != 2) {
    Tcl_AppendResult(interp, argv[0], ": parametername",(char *)NULL);
    return(TCL_ERROR);
  }

  if(ftclGetParS(argv[1], s, FILENAME_MAX)){
    Tcl_AppendResult(interp, argv[0], " error for par ",argv[1],(char *)NULL);
    return(TCL_ERROR);
    }

  Tcl_AppendResult(interp, "", s, (char *)NULL);

  return(TCL_OK);

}
