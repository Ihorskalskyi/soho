/* =========================================================================
 * file: ftclTemplate.c --
 *
 * Main to run the Tcl shell.  This file is a useful template for custom
 * applications that wish to have Tcl as the top level command language.
 * It provides a default source to create an ftcl interactive session
 * and for UNIX it calls Tcl_AppInit to be used to provide customized 
 * initializations; a template for this is found in ftclAppInit.c . 
 *---------------------------------------------------------------------------
 * Copyright 1992 Karl Lehenbauer and Mark Diekhans.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose and without fee is hereby granted, provided
 * that the above copyright notice appear in all copies.  Karl Lehenbauer and
 * Mark Diekhans make no representations about the suitability of this
 * software for any purpose.  It is provided "as is" without express or
 * implied warranty.
 *
 * Fermi Modifications: 	added call to ftclCreateCommands to create Fermi
 *				specific commands.
 *							M.Vittone Jan 7th,93
 *
 *				Unified template for VxWorks and UNIX          
 *					  	     M.Vittone May 16th,1995   
 *---------------------------------------------------------------------------  */

/*
 * This file is optional.
 */
#include "patchlevel.h"

#include "ftcl.h"

#ifndef VXWORKS

#include "tclExtdInt.h"

extern int (*tclShellCmdEvalProc) ();

#include "XParseCmdLine.c" 	/* Since ParseCmdLine() is declared */
				/* "static", it has to be included  */
				/* rather than linked. Please see   */
				/* the comments in XParseCmdLine.c  */
				/*		-- C.-H. Huang      */
	

/* Current Tcl_AppInit() is borrowed from tclX */

int Tcl_AppInit(Tcl_Interp *);


/* Tcl_AppInit() could be provided by each application program.
 * If you are writing your own, you may not need to include
 * "XParseCmdLine.c". However, ParseCmdLine() is not a bad idea
 * to start with.
 */

#else		/* VxWorks */

char *initCmd =
    "if [file exists [info library]/init.tcl] {source [info library]/init.tcl}";

#endif



/* ----------------------------------------------------------------------- */

#ifndef VXWORKS
 int main(int argc, char **argv)
#else
 int ftcl()
#endif

{
    Tcl_Interp *interp;

    /*
     * If history is to be used, then set the eval procedure pointer that
     * Tcl_CommandLoop so that history will be recorded.  This reference
     * also brings in history from libtcl.a.
     */
#ifndef VXWORKS

#ifndef TCL_NOHISTORY
     tclShellCmdEvalProc = Tcl_RecordAndEval;
#endif

#endif
    /* 
     * Create a Tcl interpreter for the session, with all extended commands
     * initialized.  This can be replaced with Tcl_CreateInterp followed
     * by a subset of the extended command initializaton procedures if 
     * desired.
     */

	interp = Tcl_CreateInterp();

	/* Processing the command line arguments (of ftcl) and prepare
	 * for the Tcl_AppInit(), which is borrowed from tclX source :-)
	 */

#ifndef VXWORKS
	ParseCmdLine(interp, argc, argv);
#endif

    /*
     *   >>>>>> INITIALIZE APPLICATION SPECIFIC COMMANDS HERE <<<<<<
     */

/*  Call the function to create Fermi implemente tcl commands            */

     ftclCreateCommands (interp);

     ftclCreateHelp (interp);
     ftclMsgDefine  (interp);
     ftclParseInit (interp);

     ftcl_Interactive (interp, argc, argv);

#ifndef VXWORKS

	if (Tcl_AppInit(interp) == TCL_ERROR)
		TclX_ErrorExit (interp, 255);
    /*
     * Load the tcl startup code, this should pull in all of the tcl
     * procs, paths, command line processing, autoloads, packages, etc.
     * If Tcl was invoked interactively, Tcl_Startup will give it
     * a command loop.
     */



	ftcl_CommandLoop (interp, stdin, stdout, Tcl_Eval, 0);
    /* 
     * Delete the interpreter (not neccessary under Unix, but we do
     * it if TCL_MEM_DEBUG is set to better enable us to catch memory
     * corruption problems)
     */

#ifdef TCL_MEM_DEBUG
    Tcl_DeleteInterp(interp);
#endif

#ifdef TCL_SHELL_MEM_LEAK
    printf (" >>> Dumping active memory list to mem.lst <<<\n");
    if (Tcl_DumpActiveMemory ("mem.lst") != TCL_OK)
        panic ("error accessing `mem.lst': %s", strerror (errno));
    exit(0);

#endif

#else		/* VxWorks interactive loop */

#ifndef TCL_GENERIC_ONLY
    result = Tcl_Eval(interp, initCmd );

    if (result != TCL_OK)
    {
        printf("%s\n", interp->result);

        return(1);
    }
#endif

    ftclVxLoop (interp); 

#endif

}

/* ======================================================================= */
