/*
 * Some typical test TCL procedures. Hook it in to your favorite TCL 
 * application for testing purposes.
 */

#include <stdlib.h>
#include <string.h>

#include "ftcl.h"
#include "tk.h"

static int tclTestParse(ClientData, Tcl_Interp *, int, char **);
static int tkTestParse(ClientData, Tcl_Interp *, int, char **);

/*
 * Global variables used in THIS source file only
 */
static ftclArgvInfo g_argTable[10];


static int tkTestParse(ClientData unused, Tcl_Interp *a_interp,
                        int argc, char **argv)
{
   int    i;
   int    debugFlag,
          constantFlag;
   int    mag;
   int    xFlag, o_argc, argv0;
   double precisionFlag;
   char  *pProcess;
   Tk_ArgvInfo argTable[6];

   i = 0;
   debugFlag = 0;
   constantFlag = 0;
   precisionFlag = 0.0;
   pProcess = NULL;
   xFlag = 2;
   mag = 0;
   argv0 = 0;

   argTable[i].key = "-debug"; argTable[i].type = TK_ARGV_INT;
   argTable[i].src = "2"; argTable[i].dst = (char *) &debugFlag;
   argTable[i].help = "Specify debug level";
   i++;

   argTable[i].key = "-precision"; argTable[i].type = TK_ARGV_FLOAT;
   argTable[i].src = "0.85"; argTable[i].dst = (char *) &precisionFlag;
   argTable[i].help = "Specify precision level";
   i++;

   argTable[i].key = "-process"; argTable[i].type = TK_ARGV_STRING;
   argTable[i].src = "Process 1"; argTable[i].dst = (char *) &pProcess;
   argTable[i].help = "Specify precision level";
   i++;

   argTable[i].key = "-X"; argTable[i].type = TK_ARGV_CONSTANT;
   argTable[i].src = (char *) 10; argTable[i].dst = (char *) &xFlag;
   argTable[i].help = "Toggle X factor";
   i++;

   argTable[i].key = "-tkTestParse"; argTable[i].type = TK_ARGV_INT;
   argTable[i].src = "1"; argTable[i].dst = (char *) &argv0;
   argTable[i].help = "Do not treat me special";
   i++;

   argTable[i].key = NULL; argTable[i].type = TK_ARGV_END;
   argTable[i].src = argTable[i].dst = argTable[i].help = NULL;


   o_argc = argc;
   for (i = 0; i < o_argc; i++)  {
        if (i == 0)
            printf("Before\n");
        if (argv[i] == NULL)
            printf("  argv[%d] = [null]\n", i);
        else
            printf("  argv[%d] = [%s]\n", i, argv[i]);
   }

   Tk_ParseArgv(a_interp, (Tk_Window) NULL, &argc, argv, argTable, 
                TK_ARGV_DONT_SKIP_FIRST_ARG);

   for (i = 0; i < o_argc; i++)  {
        if (i == 0)
            printf("After\n");
        if (argv[i] == NULL)
            printf("  argv[%d] = [null]\n", i);
        else
            printf("  argv[%d] = [%s]\n", i, argv[i]);
   }

   printf("\ndebugFlag     = %d\n", debugFlag);
   printf("precisionFlag = %f\n", precisionFlag);
   printf("pProcess      = %s\n", pProcess);
   printf("mag           = %d\n", mag);
   printf("xFlag         = %d\n", xFlag);
   printf("tkTestParse   = %d\n", argv0);

   return TCL_OK;
}

static int tclTestParse(ClientData unused, Tcl_Interp *a_interp,
                        int argc, char **argv)
{
   int    i;
   int    debugFlag,
          constantFlag,
          flags;
   int    mag;
   int    xFlag, o_argc;
   double precisionFlag;
   char  *pProcess, *hndl;

   i = 0;
   debugFlag = 0;
   constantFlag = 0;
   precisionFlag = 0.0;
   pProcess = hndl = NULL;
   xFlag = 2;
   mag = 0;
   flags = 0;

   g_argTable[i].key = "<handle>"; g_argTable[i].type = FTCL_ARGV_STRING;
   g_argTable[i].src = NULL; g_argTable[i].dst = &hndl;
   g_argTable[i].help = "Handle to data";
   i++;

   g_argTable[i].key = "[mag]"; g_argTable[i].type = FTCL_ARGV_INT;
   g_argTable[i].src = "19"; g_argTable[i].dst = &mag;
   g_argTable[i].help = "Specify magnification factor";
   i++;

   g_argTable[i].key = "-debug"; g_argTable[i].type = FTCL_ARGV_INT;
   g_argTable[i].src = "2"; g_argTable[i].dst = &debugFlag;
   g_argTable[i].help = "Specify debug level";
   i++;

   g_argTable[i].key = "-precision"; g_argTable[i].type = FTCL_ARGV_DOUBLE;
   g_argTable[i].src = "0.85"; g_argTable[i].dst = &precisionFlag;
   g_argTable[i].help = "Specify precision level";
   i++;

   g_argTable[i].key = "-process"; g_argTable[i].type = FTCL_ARGV_STRING;
   g_argTable[i].src = "Process 1"; g_argTable[i].dst = &pProcess;
   g_argTable[i].help = "Specify precision level";
   i++;

   g_argTable[i].key = "-X"; g_argTable[i].type = FTCL_ARGV_CONSTANT;
   g_argTable[i].src = (void *) 10; g_argTable[i].dst = &xFlag;
   g_argTable[i].help = "Toggle X factor";
   i++;

   g_argTable[i].key = NULL; g_argTable[i].type = FTCL_ARGV_END;
   g_argTable[i].src = g_argTable[i].dst = g_argTable[i].help = NULL;

   o_argc = argc;
   for (i = 0; i < o_argc; i++)  {
        if (i == 0)
            printf("Before\n");
        if (argv[i] == NULL)
            printf("  argv[%d] = [null]\n", i);
        else
            printf("  argv[%d] = [%s]\n", i, argv[i]);
   }

   if (ftcl_ParseArgv(a_interp, &argc, argv, g_argTable, flags)
       != FTCL_ARGV_SUCCESS)  {
       Tcl_AppendResult(a_interp,
          ftcl_GetUsage(a_interp, g_argTable, flags, "testParse", 0, 
                        "\nUsage:", "\n"),
          ftcl_GetArgInfo(a_interp, g_argTable, flags, "testParse", 8), 
                          (char *)NULL);
       return TCL_ERROR;
   }

   for (i = 0; i < o_argc; i++)  {
        if (i == 0)
            printf("After\n");
        if (argv[i] == NULL)
            printf("  argv[%d] = [null]\n", i);
        else
            printf("  argv[%d] = [%s]\n", i, argv[i]);
   }
 
   printf("\ndebugFlag     = %d\n", debugFlag);
   printf("precisionFlag = %f\n", precisionFlag);
   printf("pProcess      = %s\n", pProcess);
   printf("mag           = %d\n", mag);
   printf("handle        = %s\n", hndl);
   printf("xFlag         = %d\n", xFlag);

   i = ftcl_ArgIsPresent(argc, argv, "-debug", g_argTable);
   switch (i)  {
      case  1 : printf("-debug was present\n"); break;
      case  0 : printf("-debug was NOT present\n"); break;
      case -1 : printf("-debug not in argTable\n"); break;
   }

   i = ftcl_ArgIsPresent(argc, argv, "-precision", g_argTable);
   switch(i)  {
      case  1 : printf("-precision was present\n"); break;
      case  0 : printf("-precision was NOT present\n"); break;
      case -1 : printf("-precision not in argTable\n"); break;
   }

   i = ftcl_ArgIsPresent(argc, argv, "-process", g_argTable);
   switch (i)  {
       case  1 : printf("-process was present\n"); break;
       case  0 : printf("-process was NOT present\n"); break;
       case -1 : printf("-process not in argTable\n"); break;
   }

   i = ftcl_ArgIsPresent(argc, argv, "-X", g_argTable);
   switch (i)  {
       case  1 : printf("-X was present\n"); break;
       case  0 : printf("-X was NOT present\n"); break;
       case -1 : printf("-X not in argTable\n"); break;
   }

   i = ftcl_ArgIsPresent(argc, argv, "-pro", g_argTable);
   switch (i)  {
       case  1 : printf("-pro was present\n"); break;
       case  0 : printf("-pro was NOT present\n"); break;
       case -1 : printf("-pro not in argTable\n"); break;
   }

   i = ftcl_ArgIsPresent(argc, argv, "<how>", g_argTable);
   switch (i)  {
       case  1 : printf("<how> was present\n"); break;
       case  0 : printf("<how> was NOT present\n"); break;
       case -1 : printf("<how> not in argTable\n"); break;
   }

   i = ftcl_ArgIsPresent(argc, argv, "[mag]", g_argTable);
   switch (i)  {
       case  1 : printf("[mag] was present\n"); break;
       case  0 : printf("[mag] was NOT present\n"); break;
       case -1 : printf("[mag] not in argTable\n"); break;
   }

   i = ftcl_ArgIsPresent(argc, argv, "<handle>", g_argTable);
   switch (i)  {
       case  1 : printf("<handle> was present\n"); break;
       case  0 : printf("<handle> was NOT present\n"); break;
       case -1 : printf("<handle> not in argTable\n"); break;
   }

   return TCL_OK;
}
