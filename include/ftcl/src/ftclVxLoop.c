/* ========================================================================= *
 * file: ftclVxLoop.c           					     *
 *				M.Vittone May 16, 1995			     *
 * 									     *
 * 				Interactive  loop for VxWorks		     *
 *									     *
 * ========================================================================= */

#include <vxWorks.h>
#include <stdioLib.h>
#include <taskLib.h>
#include "ftcl.h"
#include "ftclCmdLine.h"

/* ------------------------------------------------------------------------- */

int ftclVxLoop(Tcl_Interp *interp)
{

    Tcl_DString cmdBuf;

    char       inputBuf[256];
    int        topLevel = TRUE;
    int        result;
    char      *cmd;


    CMD_EDITHNDL cmdHandle;
    CMD l_line;
    int lineEntered;
    CMD_EDITHIST history;
    unsigned long sstatus;

    int  tclstat;

    char        partbuf[256];
    char        *ret_get;
    char        program_prompt[50];

/* ------------------------------------------------------------------------- */

     Tcl_DStringInit (&cmdBuf);

     ret_get  = Tcl_GetVar(interp,"programName",TCL_LEAVE_ERR_MSG);

/* If programName is not defined, then just give a simple ftcl prompt	     */

     if (ret_get == NULL)
     {
		sprintf (program_prompt, "ftcl");
     }
     else
     {
		sprintf (program_prompt, "%s",ret_get);
     }

     strcat (program_prompt," > "); 

/* Initialize history  */

     ftclCmd_HistInit(&history,interp); 

     strcpy (partbuf,"");	/* Cleaning up buffer */

     while(1)		/* Loop forever */
     {

        clearerr (stdin);
        clearerr (stdout);

/* ===== editing stuff   ===========*/

   /* Get ready for line entry */
/*
 * Output a prompt and input a command: it allows line continuation if the command
 * is split over multiple lines
*/

	 if (topLevel == FALSE) (ftclCmd_LineStart(&cmdHandle, "=>", &history, 0) );  
	 if (topLevel == TRUE) (ftclCmd_LineStart(&cmdHandle, program_prompt, &history, 0) );

	 do			/* Loop until line entry is complete */
	 {

   /* Get single character input from stdin */

	         ftclCmd_GetChar(&cmdHandle);

   /* Now process the entered character */
	         lineEntered = ftclCmd_ProcChar(&cmdHandle, l_line);

           } while (!lineEntered);

	   strcpy (inputBuf,l_line);
	   strcat (inputBuf,"\n");

	   strcat (partbuf,l_line);		
	   strcat (partbuf,"\n");


	   Tcl_DStringAppend (&cmdBuf, inputBuf, -1);
	   if (!Tcl_CommandComplete(Tcl_DStringValue(&cmdBuf))) 
	   {
	            topLevel = FALSE;

   	   	    lineEntered = 0;		/* resetting line */

	            continue;  /* Next line */
           }

           if ( (!strcmp(l_line, "exit")) |
                (!strcmp(l_line, "quit")) |
                (!strcmp(l_line, "logout")) )
           {
		Tcl_DStringFree (&cmdBuf);
		Tcl_DeleteInterp(interp,taskIdSelf()); 

                return(0);
           }
	   
	   topLevel = TRUE; 		/* reset flag */

           tclstat = Tcl_RecordAndEval(interp, partbuf, 0 );

	   strcpy (partbuf,"");			   


           if (tclstat != TCL_OK)
      	   {
	      printf("TCL Error");
	      if (*interp->result != 0)
	      {printf(": %s\n", interp->result);}

	      else
              {printf("\n");}
      	   }
   	   else
           {
	      if (*interp->result != 0)
              {printf("%s\n", interp->result);}
 	   }

   	   lineEntered = 0;

   } /* while(1)... */
}
/* ============================================================================ */
