/* ftclEnvscan.c */

/*
 *	tcl interface to envscan.c	mvw,jms 6jan93
 *
 * 	substitutes a string for ~ and $ shell values.
 *	result is put in interp->result
*/

#include "ftcl.h"

/* ---------------------------------------------------------------------------- */
int
ftclEnvscan(clientdata, interp, argc, argv)
    ClientData clientdata;
    Tcl_Interp *interp;
    int argc;
    char *argv[];
{
    char *string,*envscan();

    if (argc != 2) {
        Tcl_AppendResult(interp, "wrong number of args: should be \"", argv[0],
                " string\"", (char *) NULL);
        return TCL_ERROR;
    }
    if((string=envscan(argv[1])) == NULL){
        Tcl_AppendResult(interp, "envscan returned NULL for ", argv[1],
                (char *) NULL);
        return TCL_ERROR;
    }

    Tcl_AppendResult(interp,string,(char *) NULL);
    
    /* now free the string */
    envfree(string);

    return TCL_OK;
}

