'\"
'\" Copyright (c) 1993 The Regents of the University of California.
'\" Copyright (c) 1994 Sun Microsystems, Inc.
'\"
'\" See the file "license.terms" for information on usage and redistribution
'\" of this file, and for a DISCLAIMER OF ALL WARRANTIES.
'\" 
'\" @(#) pwd.n 1.3 95/05/06 15:19:11
'\" 
.so man.macros
.TH pwd n "" Tcl "Tcl Built-In Commands"
.BS
'\" Note:  do not modify the .SH NAME line immediately below!
.SH NAME
pwd \- Return the current working directory
.SH SYNOPSIS
\fBpwd\fR
.BE

.SH DESCRIPTION
.PP
Returns the path name of the current working directory.

.SH KEYWORDS
working directory
