'\"
'\" Copyright (c) 1993 The Regents of the University of California.
'\" Copyright (c) 1994 Sun Microsystems, Inc.
'\"
'\" See the file "license.terms" for information on usage and redistribution
'\" of this file, and for a DISCLAIMER OF ALL WARRANTIES.
'\" 
'\" @(#) puts.n 1.3 95/05/06 15:19:10
'\" 
.so man.macros
.TH puts n "" Tcl "Tcl Built-In Commands"
.BS
'\" Note:  do not modify the .SH NAME line immediately below!
.SH NAME
puts \- Write to a file
.SH SYNOPSIS
\fBputs \fR?\fB\-nonewline\fR? ?\fIfileId\fR? \fIstring\fR
.BE

.SH DESCRIPTION
.PP
Writes the characters given by \fIstring\fR to the file given
by \fIfileId\fR.
\fIFileId\fR must have been the return
value from a previous call to \fBopen\fR, or it may be
\fBstdout\fR or \fBstderr\fR to refer to one of the standard I/O
channels; it must refer to a file that was opened for
writing.
If no \fIfileId\fR is specified then it defaults to \fBstdout\fR.
\fBPuts\fR normally outputs a newline character after \fIstring\fR,
but this feature may be suppressed by specifying the \fB\-nonewline\fR
switch.
Output to files is buffered internally by Tcl; the \fBflush\fR
command may be used to force buffered characters to be output.

.SH KEYWORDS
file, newline, output, write
