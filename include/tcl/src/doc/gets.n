'\"
'\" Copyright (c) 1993 The Regents of the University of California.
'\" Copyright (c) 1994 Sun Microsystems, Inc.
'\"
'\" See the file "license.terms" for information on usage and redistribution
'\" of this file, and for a DISCLAIMER OF ALL WARRANTIES.
'\" 
'\" @(#) gets.n 1.4 95/05/06 15:18:44
'\" 
.so man.macros
.TH gets n "" Tcl "Tcl Built-In Commands"
.BS
'\" Note:  do not modify the .SH NAME line immediately below!
.SH NAME
gets \- Read a line from a file
.SH SYNOPSIS
\fBgets \fIfileId\fR ?\fIvarName\fR?
.BE

.SH DESCRIPTION
.PP
This command reads the next line from the file given by \fIfileId\fR
and discards the terminating newline character.
If \fIvarName\fR is specified then the line is placed in the variable
by that name and the return value is a count of the number of characters
read (not including the newline).
If the end of the file is reached before reading
any characters then \-1 is returned and \fIvarName\fR is set to an
empty string.
If \fIvarName\fR is not specified then the return value will be
the line (minus the newline character) or an empty string if
the end of the file is reached before reading any characters.
An empty string will also be returned if a line contains no characters
except the newline, so \fBeof\fR may have to be used to determine
what really happened.
If the last character in the file is not a newline character then
\fBgets\fR behaves as if there were an additional newline character
at the end of the file.
\fIFileId\fR must be \fBstdin\fR or the return value from a previous
call to \fBopen\fR; it must refer to a file that was opened
for reading.
.VS
Any existing end-of-file or error condition on the file is cleared at
the beginning of the \fBgets\fR command.
.VE

.SH KEYWORDS
file, line, read
