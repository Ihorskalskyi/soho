'\"
'\" Copyright (c) 1993 The Regents of the University of California.
'\" Copyright (c) 1994 Sun Microsystems, Inc.
'\"
'\" See the file "license.terms" for information on usage and redistribution
'\" of this file, and for a DISCLAIMER OF ALL WARRANTIES.
'\" 
'\" @(#) read.n 1.4 95/05/06 15:19:13
'\" 
.so man.macros
.TH read n "" Tcl "Tcl Built-In Commands"
.BS
'\" Note:  do not modify the .SH NAME line immediately below!
.SH NAME
read \- Read from a file
.SH SYNOPSIS
\fBread \fR?\fB\-nonewline\fR? \fIfileId\fR
.br
\fBread \fIfileId numBytes\fR
.BE

.SH DESCRIPTION
.PP
In the first form, all of the remaining bytes are read from the file
given by \fIfileId\fR; they are returned as the result of the command.
If the \fB\-nonewline\fR switch is specified then the last
character of the file is discarded if it is a newline.
In the second form, the extra argument specifies how many bytes to read;
exactly this many bytes will be read and returned, unless there are fewer than
\fInumBytes\fR bytes left in the file; in this case, all the remaining
bytes are returned.
\fIFileId\fR must be \fBstdin\fR or the return
value from a previous call to \fBopen\fR; it must
refer to a file that was opened for reading.
.VS
Any existing end-of-file or error condition on the file is cleared at
the beginning of the \fBread\fR command.
.VE

.SH KEYWORDS
file, read
