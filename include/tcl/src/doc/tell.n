'\"
'\" Copyright (c) 1993 The Regents of the University of California.
'\" Copyright (c) 1994 Sun Microsystems, Inc.
'\"
'\" See the file "license.terms" for information on usage and redistribution
'\" of this file, and for a DISCLAIMER OF ALL WARRANTIES.
'\" 
'\" @(#) tell.n 1.3 95/05/06 15:19:37
'\" 
.so man.macros
.TH tell n "" Tcl "Tcl Built-In Commands"
.BS
'\" Note:  do not modify the .SH NAME line immediately below!
.SH NAME
tell \- Return current access position for an open file
.SH SYNOPSIS
\fBtell \fIfileId\fR
.BE

.SH DESCRIPTION
.PP
Returns a decimal string giving the current access position in
\fIfileId\fR.
\fIFileId\fR must have been the return
value from a previous call to \fBopen\fR, or it may be \fBstdin\fR,
\fBstdout\fR, or \fBstderr\fR to refer to one of the standard I/O
channels.

.SH KEYWORDS
access position, file
