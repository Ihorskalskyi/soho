'\"
'\" Copyright (c) 1993 The Regents of the University of California.
'\" Copyright (c) 1994 Sun Microsystems, Inc.
'\"
'\" See the file "license.terms" for information on usage and redistribution
'\" of this file, and for a DISCLAIMER OF ALL WARRANTIES.
'\" 
'\" @(#) time.n 1.3 95/05/06 15:19:39
'\" 
.so man.macros
.TH time n "" Tcl "Tcl Built-In Commands"
.BS
'\" Note:  do not modify the .SH NAME line immediately below!
.SH NAME
time \- Time the execution of a script
.SH SYNOPSIS
\fBtime \fIscript\fR ?\fIcount\fR?
.BE

.SH DESCRIPTION
.PP
This command will call the Tcl interpreter \fIcount\fR
times to evaluate \fIscript\fR (or once if \fIcount\fR isn't
specified).  It will then return a string of the form
.DS
\fB503 microseconds per iteration\fR
.DE
which indicates the average amount of time required per iteration,
in microseconds.
Time is measured in elapsed time, not CPU time.

.SH KEYWORDS
script, time
