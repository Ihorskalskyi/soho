'\"
'\" Copyright (c) 1993 The Regents of the University of California.
'\" Copyright (c) 1994 Sun Microsystems, Inc.
'\"
'\" See the file "license.terms" for information on usage and redistribution
'\" of this file, and for a DISCLAIMER OF ALL WARRANTIES.
'\" 
'\" @(#) upvar.n 1.7 95/05/28 13:24:55
'\" 
.so man.macros
.TH upvar n "" Tcl "Tcl Built-In Commands"
.BS
'\" Note:  do not modify the .SH NAME line immediately below!
.SH NAME
upvar \- Create link to variable in a different stack frame
.SH SYNOPSIS
\fBupvar \fR?\fIlevel\fR? \fIotherVar myVar \fR?\fIotherVar myVar \fR...?
.BE

.SH DESCRIPTION
.PP
This command arranges for one or more local variables in the current
procedure to refer to variables in an enclosing procedure call or
to global variables.
\fILevel\fR may have any of the forms permitted for the \fBuplevel\fR
command, and may be omitted if the first letter of the first \fIotherVar\fR
isn't \fB#\fR or a digit (it defaults to \fB1\fR).
For each \fIotherVar\fR argument, \fBupvar\fR makes the variable
by that name in the procedure frame given by \fIlevel\fR (or at
global level, if \fIlevel\fR is \fB#0\fR) accessible
in the current procedure by the name given in the corresponding
\fImyVar\fR argument.
The variable named by \fIotherVar\fR need not exist at the time of the
call;  it will be created the first time \fImyVar\fR is referenced, just like
an ordinary variable.
.VS
\fIMyVar\fR may not refer to an element of an array, but \fIotherVar\fR
may refer to an array element.
.VE
\fBUpvar\fR returns an empty string.
.PP
The \fBupvar\fR command simplifies the implementation of call-by-name
procedure calling and also makes it easier to build new control constructs
as Tcl procedures.
For example, consider the following procedure:
.DS
.ta 1c 2c 3c
\fBproc add2 name {
    upvar $name x
    set x [expr $x+2]
}\fR
.DE
\fBAdd2\fR is invoked with an argument giving the name of a variable,
and it adds two to the value of that variable.
Although \fBadd2\fR could have been implemented using \fBuplevel\fR
instead of \fBupvar\fR, \fBupvar\fR makes it simpler for \fBadd2\fR
to access the variable in the caller's procedure frame.
.PP
.VS
If an upvar variable is unset (e.g. \fBx\fR in \fBadd2\fR above), the
\fBunset\fR operation affects the variable it is linked to, not the
upvar variable.  There is no way to unset an upvar variable except
by exiting the procedure in which it is defined.  However, it is
possible to retarget an upvar variable by executing another \fBupvar\fR
command.
.VE

.SH KEYWORDS
context, frame, global, level, procedure, variable
