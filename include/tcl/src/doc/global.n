'\"
'\" Copyright (c) 1993 The Regents of the University of California.
'\" Copyright (c) 1994 Sun Microsystems, Inc.
'\"
'\" See the file "license.terms" for information on usage and redistribution
'\" of this file, and for a DISCLAIMER OF ALL WARRANTIES.
'\" 
'\" @(#) global.n 1.3 95/05/06 15:18:46
'\" 
.so man.macros
.TH global n "" Tcl "Tcl Built-In Commands"
.BS
'\" Note:  do not modify the .SH NAME line immediately below!
.SH NAME
global \- Access global variables
.SH SYNOPSIS
\fBglobal \fIvarname \fR?\fIvarname ...\fR?
.BE

.SH DESCRIPTION
.PP
This command is ignored unless a Tcl procedure is being interpreted.
If so then it declares the given \fIvarname\fR's to be global variables
rather than local ones.  For the duration of the current procedure
(and only while executing in the current procedure), any reference to
any of the \fIvarname\fRs will refer to the global variable by the same
name.

.SH KEYWORDS
global, procedure, variable
