'\"
'\" Copyright (c) 1990-1994 The Regents of the University of California.
'\" Copyright (c) 1994 Sun Microsystems, Inc.
'\"
'\" See the file "license.terms" for information on usage and redistribution
'\" of this file, and for a DISCLAIMER OF ALL WARRANTIES.
'\" 
'\" @(#) tkvars.n 1.16 95/06/07 21:15:37
'\" 
.so man.macros
.TH tkvars n 4.0 Tk "Tk Built-In Commands"
.BS
'\" Note:  do not modify the .SH NAME line immediately below!
.SH NAME
tkvars \- Variables used or set by Tk
.BE

.SH DESCRIPTION
.PP
The following Tcl variables are either set or used by Tk at various times
in its execution:
.TP 15
\fBtk_library\fR
This variable holds the name of a directory containing a library
of Tcl scripts related to Tk.  These scripts include an initialization
file that is normally processed whenever a Tk application starts up,
plus other files containing procedures that implement default behaviors
for widgets.
.VS
If the variable isn't already set when a Tk application is created,
Tk sets it from the TK_LIBRARY environment variable, if one exists,
or else from a default value compiled into Tk.
The variable can be modified by an application to switch to a different
library.
.VE
.TP
\fBtk_patchLevel\fR
Contains a decimal integer giving the current patch level for Tk.
The patch level is incremented for each new release or patch, and
it uniquely identifies an official version of Tk.
.br
.TP
\fBtkPriv\fR
.VS
This variable is an array containing several pieces of information
that are private to Tk.  The elements of \fBtkPriv\fR are used by
.VE
Tk library procedures and default bindings.
They should not be accessed by any code outside Tk.
.TP
\fBtk_strictMotif\fR
This variable is set to zero by default.
If an application sets it to one, then Tk attempts to adhere as
closely as possible to Motif look-and-feel standards.
For example, active elements such as buttons and scrollbar
sliders will not change color when the pointer passes over them.
.TP 15
\fBtk_version\fR
Tk sets this variable in the interpreter for each application.
The variable holds the current version number of the Tk
library in the form \fImajor\fR.\fIminor\fR.  \fIMajor\fR and
\fIminor\fR are integers.  The major version number increases in
any Tk release that includes changes that are not backward compatible
(i.e. whenever existing Tk applications and scripts may have to change to
work with the new release).  The minor version number increases with
each new release of Tk, except that it resets to zero whenever the
major version number changes.

.SH KEYWORDS
variables, version
