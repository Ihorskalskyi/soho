# This file is a Tcl script to test out Tk's "after" command.
# It is organized in the standard fashion for Tcl tests.
#
# Copyright (c) 1994 The Regents of the University of California.
# Copyright (c) 1994-1995 Sun Microsystems, Inc.
#
# See the file "license.terms" for information on usage and redistribution
# of this file, and for a DISCLAIMER OF ALL WARRANTIES.
#
# @(#) after.test 1.7 95/06/21 15:08:11

if {[string compare test [info procs test]] == 1} {
    source defs
}

test after-1.1 {after command} {
    list [catch {after} msg] $msg
} {1 {wrong # args: should be "after milliseconds ?command? ?arg arg ...?" or "after cancel id|command"}}
test after-1.2 {after command} {
    list [catch {after 2x} msg] $msg
} {1 {expected integer but got "2x"}}
test after-1.3 {after command} {
    list [catch {after gorp} msg] $msg
} {1 {bad argument "gorp": must be cancel, idle, or a number}}
test after-1.4 {after command} {
    set x before
    after 400 {set x after}
    after 200
    update
    set y $x
    after 400
    update
    list $y $x
} {before after}
test after-1.5 {after command} {
    set x before
    after 300 set x after
    after 200
    update
    set y $x
    after 200
    update
    list $y $x
} {before after}

test after-2.1 {cancel option} {
    list [catch {after cancel} msg] $msg
} {1 {wrong # args: should be "after cancel id|command"}}
test after-2.2 {cancel option} {
    list [catch {after cancel after#44x} msg] $msg
} {1 {expected integer but got "44x"}}
test after-2.3 {cancel option} {
    after cancel after#1
} {}
test after-2.4 {cancel option} {
    after cancel {foo bar}
} {}
test after-2.5 {cancel option} {
    set x before
    set y [after 100 set x after]
    after cancel $y
    after 200
    update
    set x
} {before}
test after-2.6 {cancel option} {
    set x before
    after 100 set x after
    after cancel {set x after}
    after 200
    update
    set x
} {before}
test after-2.7 {cancel option} {
    set x before
    after 100 set x after
    after 300 set x after
    after cancel {set x after}
    after 200
    update
    set y $x
    set x cleared
    after 200
    update
    list $y $x
} {after cleared}
test after-2.8 {cancel option} {
    set x first
    after idle lappend x second
    after idle lappend x third
    set i [after idle lappend x fourth]
    after cancel {lappend x second}
    after cancel $i
    update idletasks
    set x
} {first third}
test after-2.9 {cancel option, multiple arguments for command} {
    set x first
    after idle lappend x second
    after idle lappend x third
    set i [after idle lappend x fourth]
    after cancel lappend x second
    after cancel $i
    update idletasks
    set x
} {first third}
test after-2.10 {cancel option, cancel during handler, used to dump core} {
    set id [
	after 100 {
	    set x done
	    after cancel $id
	}
    ]
    tkwait variable x
} {}

test after-3.1 {idle option} {
    list [catch {after idle} msg] $msg
} {1 {wrong # args: should be "after idle script script ..."}}
test after-3.2 {idle option} {
    set x before
    after idle {set x after}
    set y $x
    update idletasks
    list $y $x
} {before after}
test after-3.3 {idle option} {
    set x before
    after idle set x after
    set y $x
    update idletasks
    list $y $x
} {before after}

test after-4.1 {AfterProc procedure} {
    set x before
    proc foo {} {
	set x untouched
	after 100 {set x after}
	after 200
	update
	return $x
    }
    list [foo] $x
} {untouched after}
test after-4.2 {AfterProc procedure} {
    catch {rename tkerror {}}
    proc tkerror msg {
	global x errorInfo
	set x [list $msg $errorInfo]
    }
    set x empty
    after 100 {error "After error"}
    after 200
    set y $x
    update
    catch {rename tkerror {}}
    list $y $x
} {empty {{After error} {After error
    while executing
"error "After error""
    ("after" script)}}}

if {[info commands testfevent] == "testfevent"} {
    test after-5.1 {TkEventCleanupProc procedure} {
	testfevent create
	testfevent cmd {after 200 {
	    lappend x after
	    puts "test after-4.1, part 1: this message should not appear"
	}}
	after 200 {lappend x after2}
	testfevent cmd {after 200 {
	    lappend x after3
	    puts "test after-4.1, part 2: this message should not appear"
	}}
	after 200 {lappend x after4}
	testfevent cmd {after 200 {
	    lappend x after5
	    puts "test after-4.1, part 3: this message should not appear"
	}}
	testfevent delete
	set x before
	after 300
	update
	set x
    } {before after2 after4}
}
