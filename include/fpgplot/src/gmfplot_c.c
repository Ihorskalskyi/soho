#include <string.h>

#define NO_ERR          0
#define OPEN_ERR        1
#define DEVICE_OPEN_ERR 2
#define PRECISION_ERR   3
#define CMD_ERR         4
#define SET_3D_MODE_ERR 5
#define VERTICES_ERR    6
#define ILLEGAL_CMD_ERR 7

#define MAXSIZE 128

void main(int argc, char **argv)
{
long int i, ierr, rstatus;
int dlen, flen;
char fileName[MAXSIZE], deviceName[MAXSIZE];

if (argc < 3) 
{
	printf("Usage: %s: <filename> <devicename>\n", argv[0]);
	exit(1);
}

strcpy(fileName, argv[1]);
strcpy(deviceName, argv[2]);

/* Space fill the array, the entire length is used by the fortran */
dlen = strlen(deviceName);
flen = strlen(fileName);
for ( i = dlen ; i < MAXSIZE ; ++i)
   {deviceName[i] = ' ';}
for ( i = flen ; i < MAXSIZE ; ++i)
   {fileName[i] = ' ';}

rstatus = gmfplot_(fileName, deviceName, &ierr);

/* Put the NULLs back at the end of the string so they can be printed. */
deviceName[dlen] = '\0';
fileName[flen] = '\0';

switch (rstatus)
   {
   case NO_ERR:
      break;
   case OPEN_ERR:
      printf("OPEN ERROR - %d - while opening file - %s.\n", ierr, fileName);
      break;
   case DEVICE_OPEN_ERR:
      printf("OPEN ERROR - %d - while opening device - %s.\n", ierr,
	     deviceName);
      break;
   case PRECISION_ERR:
      printf("BEGIN_METAFILE:command does not request 15-bit precision.\n");
      break;
   case CMD_ERR:
      printf("BEGIN_METAFILE : command is misplaced.\n");
      break;
   case SET_3D_MODE_ERR:
      printf("SET_3D_MODE command not allowed.\n");
   case VERTICES_ERR:
      printf ("DRAW_POLYGON : command has more than 512 vertices.\n");
      break;
   case ILLEGAL_CMD_ERR:
      printf("GMFPLOT, unrecognized command %O4.\n", ierr);
      break;
   }
}

