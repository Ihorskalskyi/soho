#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include "pgplot_c.h"

/* pgplot_c.c
 *
 *    C binding for the fortran routines in PGPLOT.  most of the bindings are
 *    part of the pgplot product.  These are the ones unique to fermilab.
 *
 *  4-Feb-1993        Penelope Constanta-Fanourakis
 *
 */

/************************  C binding to pgplot routines  *********************/
/* return the stored name of the pgplot metafile */
long int pgplot_getmfile (char *metaFile)
{
  long int rstatus;

  rstatus = getmfile_ (metaFile);
  return(rstatus);
}
/* convert the metafile to be displayed on any supported pgplot device */
long int pgplot_gmfplot (char *fileName, char *deviceName, long int *ierr)
{
  long int rstatus;

  int save = 0;
  int restore = 1;

  svstate_ (&save);                /* save current state of PGPLOT_GET/SAVE */
  rstatus = gmfplot_ (fileName, deviceName, ierr);
  svstate_ (&restore);             /* restore state */
  return(rstatus);
}

void pgplot_pgconx (float *a, int *idim, int *jdim, int *i1, int *i2,
              int *j1, int *j2, float *c, int *nc, 
              void (*plot)(int *vs, float *x, float *y, float *z))
{
   pgconx_(a, idim, jdim, i1, i2, j1, j2, c, nc, plot);
}

void pgplot_pgfunt (float (*fx)(float *t), float (*fy)(float *t), 
                    int *n, float *tmin, float *tmax, int *flag)
{
   pgfunt_(fx, fy, n, tmin, tmax, flag);
}

void pgplot_pgfunx (float (*fy)(float *x), int *n, 
                    float *xmin, float *xmax, int *pgflag)
{
   pgfunx_(fy, n, xmin, xmax, pgflag);
}

void pgplot_pgfuny (float (*fx)(float *y), int *n, 
                    float *ymin, float *ymax, int *pgflag)
{
   pgfuny_(fx, n, ymin, ymax, pgflag);
}
