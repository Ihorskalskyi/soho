C
C   pgGetMFile.f
C
C      This routine will return the metafile file name which is stored in a
C      FORTRAN common block.
C
C      RETURNS:   0 if there is no metafile name stored.
C
C  24-jun-1995  Move to fpgplot product (efb)
C  9-dec-1993   Eileen Berman
C
      INTEGER FUNCTION GETMFILE(a_buf)
C
      character*(*) a_buf
C
      integer*2  i
      INTEGER*2  PGPLOT_SAVE, PGPLOT_GET
      CHARACTER  UFNAME_BUF*50
      INTEGER*2  UFNAME_LEN
      COMMON /UFNAME/ UFNAME_BUF, UFNAME_LEN, PGPLOT_SAVE, PGPLOT_GET
C
C
C   See if there is a filename in the buffer.  If there is, the length
C     parameter will be greater than 0.
C
      if (UFNAME_LEN .gt. 0) then
         DO 100 i = 1, UFNAME_LEN
            a_buf(i:i) = UFNAME_BUF(i:i)
 100     END DO
      end if
C
C   Return the length of the file name
C
      GETMFILE = UFNAME_LEN
C
      return
      end




