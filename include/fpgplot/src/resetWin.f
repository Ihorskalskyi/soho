C
C  Send the new geometry values to the pgxwin_server and then save them
C  in values to be used by the routine GRBPIC.
C
      SUBROUTINE FPGRESETWIN(a_width, a_height, a_x, a_y)
      INTEGER a_width, a_height, a_x, a_y
C
      INCLUDE 'grpckg1.inc'
      INCLUDE 'fpgplot.inc'
C
      INTEGER IFUNC, NBUF, LCHR
      REAL    RBUF(4)
      CHARACTER*20 CHR
C
C  Send the values to the pgxwin_server so the window can be reset.
C
      IF (GRGTYP.GT.0) THEN
         IFUNC = 11
         RBUF(1) = a_width
         RBUF(2) = a_height
         RBUF(3) = a_x
         RBUF(4) = a_y
         CALL GREXEC(GRGTYP,IFUNC,RBUF,NBUF,CHR,LCHR)
      ENDIF
C
C  Save values so can be used in grbpic
C
      GRXMXA(GRCIDE) = a_width
      GRYMXA(GRCIDE) = a_height
      GRXPOS(GRCIDE) = a_x
      GRYPOS(GRCIDE) = a_y
C
      RETURN
      END
