C svstate.f
C
C	This routine does the following -
C           * if the passed value = 0, save the current state of
C               PGPLOT_SAVE and PGPLOT_GET and set them as follows :
C                   PGPLOT_SAVE = 0,  PGPLOT_GET = 0
C             thus the new concurrent saving of plot commands to a
C             disk metafile will not occur.
C           * if the passed value is 1, restore the values from the
C             saved ones. 
C
C  24-jun-1993  Moved to fpgplot product
C  13-dec-1993  Eileen Berman
C
      subroutine svstate(a_func)
C
      integer a_func
C
      integer l_pgplot_save
      integer l_pgplot_get
C
      INTEGER*2  PGPLOT_SAVE, PGPLOT_GET
      CHARACTER  UFNAME_BUF*50
      INTEGER*2  UFNAME_SIZE, UFNAME_LEN
      PARAMETER  (UFNAME_SIZE = 50)
      COMMON /UFNAME/ UFNAME_BUF, UFNAME_LEN, PGPLOT_SAVE, PGPLOT_GET
C
      if (a_func .eq. 0) then
         l_pgplot_save = PGPLOT_SAVE
         l_pgplot_get = PGPLOT_GET
         PGPLOT_SAVE = 0
         PGPLOT_GET = 0
      else
         PGPLOT_SAVE = l_pgplot_save
         PGPLOT_GET = l_pgplot_get
      endif
C
      return
      end








