/*****************************************************************************
******************************************************************************
**
** FILE:
**	pgGeom.c
**
** ABSTRACT:
**	This file contains the following two routines.  They are used in
**      association with the Tcl extensions which are located in Dervish.
**
** ENTRY POINT		SCOPE	DESCRIPTION
** -------------------------------------------------------------------------
** pgGeomGet            public  Get the user specifies pgplot window geometry
** pgGeomSet            public  Set the user specifies pgplot window geometry
**
** ENVIRONMENT:
**	ANSI C.
**
** REQUIRED PRODUCTS:
**	pgplot
**
** AUTHORS:
**	Creation date:  July 28, 1995
**	Eileen Berman
**
******************************************************************************
******************************************************************************
*/
#include <pgGeom.h>
#include <stdio.h>
#include <X11/Xlib.h>

/*============================================================================  
**============================================================================
**
** LOCAL MACROS, DEFINITIONS, ETC.
**
**============================================================================
*/

/*----------------------------------------------------------------------------
**
** LOCAL DEFINITIONS
*/

/*----------------------------------------------------------------------------
**
** GLOBAL VARIABLES
*/
/* Variables used to store the geometry string information input by the
 * user
 */
static unsigned int g_width;
static unsigned int g_height;
static int          g_x;
static int          g_y;
static int          g_userGeom = FALSE;

static Display *display;


/*============================================================================
**============================================================================
**
** ROUTINE: pgGeomSet
**
** DESCRIPTION:
**	Set the window geometry global variables with the user preferences
**      and send the new preferences to the pgxwin server.
**
** RETURN VALUES:
**	None
**
** CALLS TO:
**	fpgresetwin
**
** GLOBALS REFERENCED:
**
**============================================================================
*/
void pgGeomSet
     (
     unsigned int a_width,       /* IN: width of window in pixels */
     unsigned int a_height,      /* IN: height of window in pixels */
     int          a_x,           /* IN: upper left corner position in x */
     int          a_y            /* IN: upper left corner position in y */
     )
{
int   devNum;
int   ifunc, *nbuf, *lchr, *mode;
int   len=2, ident;
float rbuf[2];
char  *chr;

/* Ask the xwindows driver to reset the size of the screen */
fpgresetwin_(&a_width, &a_height, &a_x, &a_y);

/* Mark that we have user entered values */
g_userGeom = TRUE;

/* Store the user values in global variables */
g_width = a_width;
g_height = a_height;
g_x = a_x;
g_y = a_y;
}


/*============================================================================
**============================================================================
**
** ROUTINE: pgGeomSet
**
** DESCRIPTION:
**	Get the window geometry user set preferences. First get the values
**      set by the user.  If none are set, get those from the Xdefaults file.
**
** RETURN VALUES:
**	0 : Error
**      1 : everything cool
**
** CALLS TO:
**
**
** GLOBALS REFERENCED:
**
**============================================================================
*/
int pgGeomGet
     (
     unsigned int *a_width,       /* OUT: width of window in pixels */
     unsigned int *a_height,      /* OUT: height of window in pixels */
     int          *a_x,           /* OUT: upper left corner position in x */
     int          *a_y            /* OUT: upper left corner position in y */
     )
{
char *geometry;
char *dname = NULL;
int  rstatus = 1;                 /* Assume success */
int  geomMask;

/* First look for user entered geometry */
if (g_userGeom == TRUE)
   {
   *a_width = g_width;
   *a_height = g_height;
   *a_x = g_x;
   *a_y = g_y;
   }
else
   {
   /* Now see if there are any values set in the Xdefaults file. We first have
      to create a display.  We will delete it before we are done. */
   if ((display = XOpenDisplay(dname)) == NULL)
      {
      /* Error */
      (void) fprintf (stderr, "XWDRIV: cannot connect to X server\n");
      rstatus = 0;
      }
   else
      {
      /* Get the defaults geometry string. */
      geometry = XGetDefault(display, PROGRAM_NAME, ARG_GEOMETRY);
      if (geometry != NULL)
	 {
	 /* Parse the string to separate the individual values. */
	 geomMask = XParseGeometry(geometry, a_x, a_y, a_width, a_height);
	 }
      else
	 {
	 /* Error getting geometry string */
	 rstatus = 0;
	 }

      /* Close the display */
      XCloseDisplay(display);
      }
   }

return(rstatus);
}









