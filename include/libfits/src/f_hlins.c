/*                                                         f_hlins
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_hlins(header, size, lineno, line)
                char *header[], *line;
                int size, lineno;

Purpose:        Adds a line to the header

Inputs:         'header' is the FITS header
                'size' is the size of the header array
                'lineno' is the line number after which to insert
                        the new header line (or the 0 referenced
                        number of the new header line)
                'line' is the line to add

Returns:        TRUE on success,
                FALSE on failure.

Notes:          If an invalid lineno value is used, a reasonable
                value is chosen.

History:        1986 July 30, Alan Uomoto, LSU Physics & Astronomy

------------------------------------------------------------------

*/
#include <string.h>
#include "libfits.h"

static int gethmem(char **header, int n)
{

        extern char *shMalloc();

        header[n] = shMalloc(82);
	header[n][0] = '\0';

        return(TRUE);

}

static int getln(int n, int lineno, char **header)
{

        int endline, naxis;
        extern int f_debug(int flag), f_ikey(int *ival, char **header, char *keyword);

        if (f_ikey(&naxis, header, "NAXIS")) {
                if (lineno < naxis + 3) {
                        lineno = naxis + 3;
                }
        }

        if ((endline = f_lnum(header, "END")) >= 0) {
                if (lineno > endline) {
                        lineno = endline;
                }
        }
        else if (lineno < 0) {
                lineno = 0;
        }
        else if (lineno > n - 1) {
                lineno = n - 1;
        }
        return(lineno);
}

static void makespac(char **header, int lineno, int nlines)
{

        int i, j;

        for (i = nlines - 1; i > lineno; i--) {
                for (j = 0; j < 82; j++) {
                        header[i][j] = header[i - 1][j];
                }
        }
}

static int nlines(char **header)
{

        register int i;

        for (i = 0; header[i] != NULL; i++)
                ;

        return(i);

}

int f_hlins(char **header, int size, int lineno, char *line)
{

        int     i,              /* loop index                           */
                len,            /* length of line to insert             */
                n,              /* number of lines in the header        */
                nlines(),       /* finds number of lines in header      */
                gethmem(),      /* get header memory                    */
                getln();        /* get line number to insert at         */
        void    makespac();     /* pushes header lines down             */
        extern int f_debug(int flag);   /* error messages on or off             */

        n = nlines(header);
        if (n >= size) {
                if (f_debug(QUERY)) {
                        fprintf(stderr,
                                "f_hlins: header full, can't add more lines\n");
                }
                return(FALSE);
        }
        if (! gethmem(header, n)) {
                return(FALSE);
        }
        n++;
        lineno = getln(n, lineno, header);
        makespac(header, lineno, n);
        len = strlen(line);
	if(len > 82) len = 82;		/* for some reason 82, not 81,
					   bytes are malloced */
        for (i = 0; i < len; i++) {
                header[lineno][i] = line[i];
        }
        for (i = len; i < 82; i++) {
                header[lineno][i] = ' ';
        }
        header[lineno][80] = '\0';
        return(TRUE);

}


#ifdef DEBUG

#define HEADERSIZE (36 * 4)

main(argc, argv)        /* passed this test 1986 August 16      */
int argc;
char *argv[];

{

        int i, lnum;
        FILE *fp, *f_rdfits();
        extern int f_setup(), f_debug();
        char *line = "This is a new header line";
        char *header[HEADERSIZE];

        f_setup();
        f_debug(TRUE);

        if (argc != 2) {
                printf("usage: f_hlins input\n");
                exit(1);
        }
        fp = f_rdfits(argv[1], header, HEADERSIZE);
        if (fp == NULL) {
                printf("Can't open input\n");
                exit(1);
        }

        for (i = 0; header[i] != NULL; i++) {
                printf("%s\n", header[i]);
        }
        printf("\nline number? ");
        scanf("%d", &lnum);

        f_hlins(header, HEADERSIZE, lnum, line);
        for (i = 0; header[i] != NULL; i++) {
                printf("%s\n", header[i]);
        }
}

#endif
