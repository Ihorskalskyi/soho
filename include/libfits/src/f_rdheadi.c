/*                                                       f_rdheadi
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_rdheadi(header, size, fp)
                char *header[];
                int size;
                FILE *fp;

Purpose:        Reads the header without any checks

Inputs:         'fp' is the file pointer to an open file
                size is the dimension of header

Returns:        TRUE on success, with 'header' filled with the
                header

                FALSE on failure

Notes:          1. does not rewind fp first.
                2. Allocates memory for header (82 bytes per line,
                   terminated with '\0' at position 80) and sets the
                   next pointer to NULL. If the pointer is not NULL
                   to start with, it frees the memory.
                3. Leaves the file at the top of data position.
                4. The mechanism for searching through the header
                   requires that we know what the next header line
                   at which to start a search is. This line is
                   marked with a '.'.

History:        1993 Dec 3, Tom Nicinski, Fermilab (modelled after f_rdhead).

------------------------------------------------------------------

*/

#include "libfits.h"

#define ERROR   0
#define END     -1
#define NOTEND  1

int f_rdheadi(char **header, int size, FILE *fp)
{

        char    buf[2880];      /* temporary buffer                     */
        int     line,           /* line number to start loading         */
                n,              /* temporary variable                   */
                f_debug(int flag),      /* checks for debug on or off           */
                f_unpk(char **header, char *buf, int size, int line);         /* unpacks a record into header         */


        if (fp == NULL) {
                if (f_debug(QUERY)) {
                        fprintf(stderr, "f_rdheadi: Passed a NULL pointer\n");
                }
                return(FALSE);
        }

        if (! f_rfr(buf, fp)) {
                if (f_debug(QUERY)) {
                        fprintf(stderr, "f_rdheadi: Not a FITS file\n");
                }
                return(FALSE);
        }

        line = 0;
        while ((n = f_unpk(header, buf, size, line)) != END) {
                if (n == ERROR) {
                        return(FALSE);
                }
                if (! f_rfr(buf, fp)) {
                        if (f_debug(QUERY)) {
                                fprintf(stderr, "f_rdheadi: Unexpected EOF\n");
                        }
                        return(FALSE);
                }
                line += 36;     /* 36 card images per FITS record       */
        }

        return(TRUE);

}


#ifdef DEBUG    /* passed this test 1986 August 15      */

#define HEADERSIZE (36 * 2)

main(argc, argv)
int argc;
char *argv[];

{

        char *header[HEADERSIZE];
        int i;
        FILE *fp, *f_fopen();

        if (argc != 2) {
                printf("usage: f_rdheadi filename\n");
                exit(1);
        }

        f_setup();
        f_debug(TRUE);
        if ((fp = f_fopen(argv[1], "r")) == NULL) {
                printf("f_rdheadi: can't open input\n");
                exit(1);
        }

        if (! f_rdheadi(header, HEADERSIZE, fp)) {
                printf("f_rdheadi: Can't get header\n");
                exit(1);
        }
        for (i = 0; header[i] != NULL; i++) {
                printf("%s\n", header[i]);
        }
}

#endif
