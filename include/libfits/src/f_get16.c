/*                                                         f_get16
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_get16(data, count, fp)
                int16 *data;
                int count;
                FILE *fp;

Purpose:        Reads 16 bit data from the stream fp

Inputs:         'data' is a pointer to at least count 16 bit words
                'count' is the number of 16 bit words to get
                'fp' is the file stream pointer

Returns:        TRUE on success, with data in 'data'
                FALSE on failure

Example:        #include "libfits.h"
                int16 data[2000];
                if (! f_get16(data, 2000, fp)) {
                        printf("can't get data");
                }

Notes:          Fastest with VMS fixed record format files, about
                2.5 times slower with VMS stream files

History:        1986 July 16, Alan Uomoto, LSU Physics & Astronomy
                1986 November 14, Added f_BYTESWAP

------------------------------------------------------------------

*/
#include <stdio.h>
#include "libfits.h"

int f_get16(register short int *data, int count, FILE *fp)
{

        register int16  *dp;    /* temporary data pointer               */
        register int    i,      /* loop index                           */
                        n;      /* temporary variable                   */
        int     nleft,          /* number of 16 bit words left in record*/
                nrec,           /* number of new records to read        */
                findex;         /* the file index                       */
        extern int
                f_debug(int flag),      /* error messages on or off             */
                f_findex(FILE *fp),     /* gets file index from stream pointer  */
                f_rfr(char *bp, FILE *fp);        /* read fits record                     */
        extern void f_bswap2(register short int *in); /* byte swap                            */
        extern struct f_finfo f_files;  /* global file info             */
        extern int f_BYTESWAP;

        findex = f_findex(fp);
        nleft = (2880 - f_files.nused[findex]) / 2;
        if (nleft > 0) {
                dp = (int16 *) (f_files.bp[findex] + f_files.nused[findex]);
                n = (count < nleft) ? count : nleft;
                for (i = 0; i < n; i++) {
                        *data++ = *dp++;
                }
                f_files.nused[findex] += n * 2;
                count -= n;
        }
        nrec = count / 1440;
        for (i = 0; i < nrec; i++) {
                if (! f_rfr((char *) data, fp)) {
                        if (f_debug(QUERY)) {
                                fprintf(stderr,
                                        "f_get16: error reading file\n");
                        }
                        return(FALSE);
                }
                if (f_BYTESWAP) {
                        f_bswap2(data);
                }
                data += 1440;
                count -= 1440;
        }
        if (count > 0) {
                if (! f_rfr((char *) f_files.bp[findex], fp)) {
                        if (f_debug(QUERY)) {
                                fprintf(stderr,
                                        "f_get16: error reading file\n");
                        }
                        return(FALSE);
                }
                if (f_BYTESWAP) {
                        f_bswap2((int16 *)f_files.bp[findex]);
                }
                n = count;
                dp = (int16 *) f_files.bp[findex];
                for (i = 0; i < n; i++) {
                        *data++ = *dp++;
                }
                f_files.nused[findex] = n * 2;
        }
        return(TRUE);
}

#ifdef DEBUG            /* passed this test 1986 July 29 */

#define HEADERLINES (36 * 2) 

main(argc, argv)
int argc;
char *argv[];

{

        long t0, t1;
        register int i;
        int bitpix, naxis1, naxis2;
        int16 *data;
        char *header[HEADERLINES];
        FILE *fp, *f_rdfits();

        if (argc != 2) {
                printf("usage: f_get16 filename\n");
                exit(1);
        }

        f_setup();
        f_debug(1);

        if ((fp = f_rdfits(argv[1], header, HEADERLINES)) == NULL) {
                printf("Can't open %s\n", argv[1]);
                exit(1);
        }

        if (! f_ikey(&naxis1, header, "NAXIS1")) {
                printf("Can't find naxis1\n");
                exit(1);
        }
        if (! f_ikey(&naxis2, header, "NAXIS2")) {
                printf("Can't find naxis2\n");
                exit(1);
        }
        if (! f_ikey(&bitpix, header, "BITPIX")) {
                printf("Can't find bitpix\n");
                exit(1);
        }
        if (bitpix != 16) {
                printf("Bitpix is %d, must be 16\n", bitpix);
                exit(1);
        }

        for (i = 0; header[i] != NULL; i++) {
                printf("%s\n", header[i]);
        }

        data = (int16 *) shMalloc(naxis1 * naxis2 * 2);

        printf("reading data...");
        fflush(stdout);

        time(&t0);
        f_get16(data, naxis1 * naxis2, fp);
        time(&t1);

        printf("time: %d seconds\n", t1 - t0);
        fflush(stdout);

        f_fclose(fp);

        for (i = 0; i < naxis1 * naxis2; i++) {
                if ((i % 10) == 0) {
                        printf("\n");
                }
                printf("%8d", data[i]);
        }

}

#endif /* DEBUG */
