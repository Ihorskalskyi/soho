/*                                                         f_get8
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_get8(data, count, fp)
                int8 *data;
                int count;
                FILE *fp;

Purpose:        Reads 8 bit data from the stream fp

Inputs:         'data' is a pointer to at least count 8 bit words
                'count' is the number of 8 bit words to get
                'fp' is the file stream pointer

Returns:        TRUE on success, with data in 'data'
                FALSE on failure

Example:        #include "libfits.h"
                int8 data[2000];
                if (! f_get8(data, 2000, fp)) {
                        printf("can't get data");
                }

Notes:          Fastest with VMS fixed record format files, about
                2.5 times slower with VMS stream files
                Not tested

History:        1986 July 16, Alan Uomoto, LSU Physics & Astronomy

------------------------------------------------------------------

*/

#include "libfits.h"

int f_get8(register unsigned char *data, int count, FILE *fp)
{

        register int8   *dp;    /* temporary data pointer               */
        register int    i,      /* loop index                           */
                        n;      /* temporary variable                   */
        int     nleft,          /* number of 8 bit words left in record*/
                nrec,           /* number of new records to read        */
                findex;         /* the file index                       */
        extern int
                f_findex(FILE *fp),     /* gets file index from stream pointer  */
                f_rfr(char *bp, FILE *fp);        /* read fits record                     */
        extern struct f_finfo f_files;  /* global file info             */

        findex = f_findex(fp);
        nleft = 2880 - f_files.nused[findex];
        if (nleft > 0) {
                dp = (int8 *) (f_files.bp[findex] + f_files.nused[findex]);
                n = (count < nleft) ? count : nleft;
                for (i = 0; i < n; i++) {
                        *data++ = *dp++;
                }
                f_files.nused[findex] += n;
                count -= n;
        }
        nrec = count / 2880;
        for (i = 0; i < nrec; i++) {
                if (! f_rfr((char *) data, fp)) {
                        if (f_debug(QUERY)) {
                                fprintf(stderr,
                                        "f_get8: error reading file\n");
                        }
                        return(FALSE);
                }
                data += 2880;
                count -= 2880;
        }
        if (count > 0) {
                if (! f_rfr((char *) f_files.bp[findex], fp)) {
                        if (f_debug(QUERY)) {
                                fprintf(stderr,
                                        "f_get8: error reading file\n");
                        }
                        return(FALSE);
                }
                n = count;
                dp = (int8 *) f_files.bp[findex];
                for (i = 0; i < n; i++) {
                        *data++ = *dp++;
                }
                f_files.nused[findex] = n;
        }
        return(TRUE);
}
