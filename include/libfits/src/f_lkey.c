/*                                                          f_lkey
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_lkey(lval, header, keyword);
                int *lval;
                char *header[], *keyword;

Purpose:        Retrieves a logical keyword value from the header

Inputs:         'lval' - pointer to an integer
                'header' - the file header
                'keyword' - the header keyword to look for

Returns:        TRUE on success, with lval either TRUE or FALSE
                FALSE on failure

Example:        int lval;
                if (f_lkey(&lval, header, "SIMPLE")) {
                        printf("Simple = %c", lval ? 'T' : 'F');
                }

Notes:

History:        1986 July 6, Alan Uomoto, LSU Physics & Astronomy


------------------------------------------------------------------

*/

#include <string.h>
#include "libfits.h"

int f_lkey(int *lval, char **header, char *keyword)
{

        char    line[81],       /* place to put the header line         */
                temp[81];       /* place to put the logical string      */
        int     i, j;           /* loop indices                         */
        extern int
                f_debug(int flag),      /* error messages on or off             */
                f_getlin(char *line, char **header, char *keyword);     /* gets header lines                    */

        if (! f_getlin(line, header, keyword)) {
                return(FALSE);
        }

        for (i = 9; i < 80; i++) {      /* clear leading blanks         */
                if (line[i] != ' ')
                        break;
        }
                                        /* copy logical string          */
        for (j = 0; line[i] != ' ' && line[i] != '/' && i < 80; j++) {
                temp[j] = line[i++];
        }
        temp[j] = '\0';

        if (strlen(temp) == 1) {        /* get the value                */
                switch (temp[0]) {
                        case 'T': *lval = TRUE;
                                  return(TRUE);
                        case 'F': *lval = FALSE;
                                  return(TRUE);
                        default:  break;
                }
        }

        if (f_debug(QUERY)) {           /* bad or missing value         */
                fprintf(stderr, 
                        "f_lkey: '%s' is a bad logical value for '%s'\n",
                        temp, keyword);
        }

        return(FALSE);

}


#ifdef DEBUG

#define HEADERSIZE (36 * 2)

main(argc, argv)        /* passed this test 1986 August 16 */
int argc;
char *argv[];

{

        char *header[HEADERSIZE], keyword[80];
        int i, lval;
        FILE *fp, *f_rdfits();

        if (argc != 2) {
                printf("usage: f_lkey filename\n");
                exit(1);
        }

        f_setup();
        f_debug(TRUE);

        fp = f_rdfits(argv[1], header, HEADERSIZE);
        if (fp == NULL) {
                printf("Can't open %s\n", argv[1]);
                exit(1);
        }
        for (i = 0; header[i] != NULL; i++) {
                printf("%s\n", header[i]);
        }
        for (;;) {
                printf("keyword: ");
                if (scanf("%s", keyword) != 1) {
                        exit(1);
                }
                if (f_lkey(&lval, header, keyword)) {
                        if (lval) {
                                printf("TRUE\n");
                        }
                        else {
                                printf("FALSE\n");
                        }
                }
        }
}

#endif
