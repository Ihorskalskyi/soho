/*                                                         f_fopen
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                FILE *f_fopen(name, mode)
                char *name, *mode;

Purpose:        Opens a data file on disk

Inputs:         'name' - A character pointer to the file name
                'mode' - "r" for read,
                         "w" for write,
                         "w+" for write update, etc.

Returns:        The file pointer on success,
                NULL on error

Example:        FILE *fp, *f_fopen();
                fp = f_fopen("input.dat", "r");
                if (fp == NULL) {
                        printf("Can't open input.dat");
                }

Notes:          System dependent. VMS fixed record files
                are faster than stream files so the open for
                write call opens a fixed record file on VMS.

History:        1986 July 9, Alan Uomoto, LSU Physics & Astronomy

------------------------------------------------------------------

*/

#include "libfits.h"

#if vms

FILE *f_fopen(name, mode)
char *name, *mode;

{

        FILE *fp;               /* temporary file pointer               */
        extern int f_debug();   /* error messages on or off             */
        extern FILE *fopen();   /* standard i/o library routine         */

        fp = NULL;

        if (mode[0] == 'r') {
                fp = fopen(name, mode);
        }

        else if (mode[0] == 'w') {      /* VAXC specialty        */
                fp = fopen(name, mode, "rfm=fix", "mrs=2880");
        }

        if (fp == NULL) {
                if (f_debug(-1)) {
                        printf("f_fopen: Can't open '%s'\n", name);
                }
        }

        return(fp);

}

#else

FILE *f_fopen(char *name, char *mode)
{

        FILE *fp;               /* temporary file pointer               */
        extern int f_debug(int flag);   /* error messages on or off             */
        extern FILE *fopen(const char *, const char *);   /* standard i/o library routine         */

	load_libfits(1);		/* force ld to load some symbols */

        fp = fopen(name, mode);

        if (fp == NULL) {
                if (f_debug(-1)) {
                        printf("f_fopen: Can't open '%s'\n", name);
                }
        }

        return(fp);

}

#endif

#ifdef DEBUG            /* passed this test 1986 August 3       */

main(argc, argv)
int argc;
char *argv[];

{

        FILE *f_fopen(), *fp;

        if (argc != 3) {
                printf("usage: f_fopen readfile writefile\n");
                exit(1);
        }

        if ((fp = f_fopen(argv[1], "r")) == NULL) {
                printf("Can't find '%s'\n", argv[1]);
                exit(1);
        }

        if ((fp = f_fopen(argv[2], "w")) == NULL) {
                printf("Can't open '%s'\n", argv[2]);
                exit(1);
        }
}

#endif
