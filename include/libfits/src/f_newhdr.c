/*                                                        f_newhdr
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
		f_newhdr(header, nlines, simple, bitpix, naxis,
			naxisn, comment)
		char *header[], *comment;
		int nlines, simple, bitpix, naxis, naxisn[];

Purpose:        Generate a new FITS header.

Inputs:         'header' is the FITS file header
		'nlines' is the size of header
		'simple' TRUE or FALSE
		'bitpix' 8, 16, or 32
		'naxis'  the appropriate value
		'naxisn' the values of naxis1, naxis2, etc.,
			 with naxisn[0] containing the number
			 of naxisN values.
		'comment' the comment for the SIMPLE line

Returns:        TRUE on success, with 'aval' pointing to the
                string

                FALSE on failure

History:        1989 December 14, Alan Uomoto, JHU Physics & Astronomy

------------------------------------------------------------------

*/

#include <string.h>
#include "libfits.h"

/* forward reference */
static void addhdr(char *header[], char line[], int lineno); 


f_newhdr(char **header, int nlines, int simple, int bitpix, int naxis, int *naxisn, char *comment)
{

     char line[82], temp[25], *shMalloc(), *f_mkikey(char *ostring, char *key, int value, char *comment), *f_mklkey(char *ostring, char *key, int value, char *comment);
     int i, n;

     for (i = 0; i < nlines; i++) {
          header[i] = NULL;
     }
     n = naxisn[0] + 4;
     for (i = 0; i < n; i++) {
          header[i] = shMalloc((unsigned) 82);
	  header[i][0] = '\0';
     }
     f_mklkey(line, "SIMPLE", simple, comment);
     addhdr(header, line, 0);
     f_mkikey(line, "BITPIX", bitpix, "");
     addhdr(header, line, 1);
     f_mkikey(line, "NAXIS", naxis, "");
     addhdr(header, line, 2);
     for (i = 1; i <= naxisn[0]; i++) {
          (void) sprintf(temp, "NAXIS%d", i);
          f_mkikey(line, temp, naxisn[i], "");
          addhdr(header, line, i+2);
     }
     (void) strcpy(line, "END");
     addhdr(header, line, i+2);
     return(TRUE);
}

static void addhdr(char **header, char *line, int lineno)
{

     int i;

     (void) strcpy(header[lineno], line);
     for (i = strlen(line); i < 82; i++) {
          header[lineno][i] = ' ';
     }
     header[lineno][80] = '\0';
}
