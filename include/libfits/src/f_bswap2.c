/*                                                        f_bswap2
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                void f_bswap2(bp)
                register int16 *bp;

Purpose:        Byte swap a FITS record of 16 bit integers

Inputs:         'bp' is a pointer to 1440 16-bit integers

Returns:        Byte swapped integers in 'bp'

Notes:          There might be a faster way to do this.

                This routine is only for those machines whose
                architecture requires a byte swap on 16-bit
                integers (PDP, VAX, for example).

History:        1986 July 7, Alan Uomoto, LSU Physics & Astronomy
                1986 August 6, renamed f_bswap2 from f_bswap

------------------------------------------------------------------

*/

#include "libfits.h"

void f_bswap2(register short int *in)
{

        register int i;         /* loop index                   */
        union {                 /* place to do the byte swap    */
                int8 a[2];
                int16 inval;
        } temp;
        register char c;        /* temporary variable           */

        for (i = 0; i < 1440; i++) {
                temp.inval = *in;
                c = temp.a[0];
                temp.inval >>= 8;
                temp.a[1] = c;
                *in++ = temp.inval;
        }
}
