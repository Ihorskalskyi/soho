/*                                                          f_npts
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_npts(header)
                char *header[];

Purpose:        Get the total number of data points

Inputs:         'header' is the FITS file header

Returns:        The number of data points or -1 on error

Notes:          

History:        1986 July 31, Alan Uomoto, LSU Physics & Astronomy

------------------------------------------------------------------

*/

#include "libfits.h"

/*	Forward references  */

static int getgrp(int *groups, int *pcount, int *gcount, char *header[]);	/* forward reference */
static void getnaxn(int naxis, int naxisn[], char *header[]);
static int getnaxis(char *header[]);


int f_npts(char **header)
{

        int     i,              /* loop index                   */
                naxis,          /* FITS number of axes          */
                naxisn[999],    /* number in each axis          */
                npts,           /* number of points in the file */
                groups,         /* groups flag                  */
                pcount,         /* FITS parameter count         */
                gcount;         /* FITS group count             */
        int     getnaxis(), getgrp();
        void    getnaxn();
        extern  int f_debug(int flag);  /* error messages on or off     */

        if ((naxis = getnaxis(header)) < 0) {
                return(-1);
        }
        getnaxn(naxis, naxisn, header);
        if (! getgrp(&groups, &pcount, &gcount, header)) {
                return(-1);
        }
        npts = 1;
        if (groups) {
                if (naxisn[1] != 0) {
                        if (f_debug(QUERY)) {
                                fprintf(stderr,
                                        "f_npts: naxis not 0 with groups\n");
                        }
                        return(-1);
                }
                for (i = 2; i <= naxis; i++) {
                        if (naxisn[i] != 0) {
                                npts *= naxisn[i];
                        }
                }
                npts += pcount * gcount;
                npts *= gcount;
        }
        else {
                for (i = 1; i <= naxis; i++) {
                        if (naxisn[i] != 0) {
                                npts *= naxisn[i];
                        }
                }
        }
        return(npts);
}

static int getgrp(int *groups, int *pcount, int *gcount, char **header)
{

        extern int f_ikey(int *ival, char **header, char *keyword), f_lkey(int *lval, char **header, char *keyword);

        if (! f_lkey(groups, header, "GROUPS")) {
                *groups = FALSE;
        }

        if (*groups) {
                if (! f_ikey(pcount, header, "PCOUNT")) {
                        if (f_debug(QUERY)) {
                                fprintf(stderr, "f_npts: Can't find PCOUNT\n");
                        }
                        return(FALSE);
                }
                if (! f_ikey(gcount, header, "GCOUNT")) {
                        if (f_debug(QUERY)) {
                                fprintf(stderr, "f_npts: Can't find GCOUNT\n");
                        }
                        return(FALSE);
                }
        }
        else {
                *pcount = 0;
                *gcount = 0;
        }
        return(TRUE);
}

static void getnaxn(int naxis, int *naxisn, char **header)
{

        char temp[9];
        int i;
        extern int f_debug(int flag), f_ikey(int *ival, char **header, char *keyword);

        for (i = 1; i <= naxis; i++) {
                (void) sprintf(temp, "NAXIS%d", i);
                if (! f_ikey(&naxisn[i], header, temp)) {
                        if (f_debug(QUERY)) {
                                fprintf(stderr,
                                        "f_npts: Can't find NAXIS%d\n", i);
                        }
                        return;
                }
        }
}

static int getnaxis(char **header)
{

        int naxis;
        extern int f_debug(int flag), f_ikey(int *ival, char **header, char *keyword);

        if (! f_ikey(&naxis, header, "NAXIS")) {
                if (f_debug(QUERY)) {
                        fprintf(stderr, "f_npts: Can't find NAXIS\n");
                }
                return(-1);
        }
        if (naxis > 999 || naxis < 0) {
                if (f_debug(QUERY)) {
                        fprintf(stderr,
                                "f_npts: NAXIS out of range: %d\n", naxis);
                }
                return(-1);
        }

        return(naxis);
}

#ifdef DEBUG    /* passed this test 1986 august 6, no groups */

#define HEADERSIZE (36 * 2)

main(argc, argv)
int argc;
char *argv[];

{

        FILE *fp, *f_rdfits();
        char *header[HEADERSIZE];

        if (argc != 2) {
                printf("usage: f_npts filename\n");
                exit();
        }

        f_setup();
        f_debug(TRUE);

        if ((fp = f_rdfits(argv[1], header, HEADERSIZE)) == NULL) {
                printf("can't open input '%s'\n", argv[1]);
                exit();
        }

        printf("npts = %d\n", f_npts(header));

}

#endif
