/*                                                        f_mkakey
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                char *f_mkakey(string, key, value, comment)
                char *string, *key, *value, *comment;

Purpose:        Builds an alphanumeric FITS header line

Inputs:         'string' - place to put the new line
                'key'    - the keyword
                'value'  - the string related to the keyword
                'comment'- the header line comment (may be null)

Returns:        pointer to string

History:        1986 Dec 9, Alan Uomoto, JHU Physics & Astronomy
		1990 May 10, AU: Fixed a bug found by wvd
		1992 July 8, AU: Fixed comment bug found by Joel Fan
------------------------------------------------------------------

*/

#include <string.h>
#include <assert.h>
#include "libfits.h"

char 	*f_mkakey(char *ostring, char *key, char *value, char *comment)
{
	int		i;		/* loop index                   */
	int		ll;		/* strlen			*/
	register int	oo;		/* output index			*/
extern  int		f_debug(int flag);	/* checks LIBFITS debug status  */

    ll = strlen( key );
    if (ll > 8)
    {   if (f_debug(QUERY))
	{   fprintf( stderr, "f_mkakey: keyword too long [%s]\n", key );
	}
	key[8] = '\0';
	ll = 8;
    }

    oo = 0;
    for (i=0; i<ll; ) ostring[oo++] = key[i++]; /* copy key */
    for (   ; oo<8; ) ostring[oo++] = ' ';	/* pad if need be */
    ostring[oo++] = '=';
    ostring[oo++] = ' ';
    ostring[oo++] = '\'';
    for (i=0; oo < 80 && (i<68) && (value[i]!='\0'); ) /* limit copy */
    {   ostring[oo++] = value[i++];		/* copy value */
    }
    for (   ; oo<19; ) ostring[oo++] = ' ';	/* pad if need be */
    ostring[oo++] = '\'';
    for (   ; oo<30; ) ostring[oo++] = ' ';	/* pad of rest of val field */

    if (comment[0] != 0 && oo < (80 - 3))
    {   ostring[oo++] = ' ';
	ostring[oo++] = '/';
	ostring[oo++] = ' ';

	for (i=0; oo < 80 && (i<37) && (comment[i]!='\0'); ) /* limit copy */
	{   ostring[oo++] = comment[i++];	/* copy comment */
	}
    }

    while (oo<80) ostring[oo++] = ' ';
    ostring[oo] = '\0';

    assert(oo <= 80);			/* n.b. this can fail for long
					   value/comment pairs. The code
					   should be fixed. RHL */

    return(ostring);
}

#ifdef DEBUG

main(argc, argv)
int argc;
char *argv[];

{

        char str[82], *f_mkakey();

        if (argc != 4) {
                printf("usage: mkakey key value comment\n");
                exit(1);
        }

        printf("%s\n", f_mkakey(str, argv[1], argv[2], argv[3]));

}

#endif
