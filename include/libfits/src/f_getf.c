/*                                                          f_getf
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_getf(data, npts, header, fp)
                float *data;
                int npts;
                char *header[];
                FILE *fp;

Purpose:        Gets float data from a fits file

Inputs:         'data' points to at least npts floats
                'npts' is the number of points to transfer
                'header' is the FITS header
                'fp' is the file pointer to read from

Returns:        TRUE on success, with unscaled data in data
                FALSE on failure

Notes:          Assumes that floats are at least as big as int32,
                Does not apply bzero and bscale to the data

History:        1986 Aug 7, Alan Uomoto, LSU Physics and Astronomy

------------------------------------------------------------------

*/

#include "libfits.h"

/*      Forward references */
 
static int get16(float *data, int npts, FILE *fp);
static int get32(float *data, int npts, FILE *fp);
static int get8 (float *data, int npts, FILE *fp);
static int getbp(char *header[]);
 
int f_getf(float *data, int npts, char **header, FILE *fp)
{

        int     getbp(),        /* returns bitpix from header   */
                get8(),         /* gets 8 bit float data        */
                get16(),        /* gets 16 bit float data       */
                get32();        /* gets 32 bit float data       */

        switch (getbp(header)) {

                case 8:   return(get8(data, npts, fp));

                case 16:  return(get16(data, npts, fp));

                case 32:  return(get32(data, npts, fp));

                default:  if (f_debug(QUERY)) {
                                fprintf(stderr,
                                        "f_getf: bad BITPIX value\n");
                          }
                          return(FALSE);
        }
}

static int get16(float *data, int npts, FILE *fp)
{

        register int i;
        register int16 *dptr;
        register float *fptr;
        extern int f_get16(register short int *data, int count, FILE *fp), f_debug(int flag);

        if (! f_get16((int16 *)data, npts, fp)) {
                if (f_debug(QUERY)) {
                        fprintf(stderr,
                                "f_getf: Can't get 16 bit data\n");
                }
                return(FALSE);
        }
        dptr = (int16 *) data + npts - 1;
        fptr = data + npts - 1;
        for (i = 0; i < npts; i++) {
                *fptr-- = (float) *dptr--;
        }

        return(TRUE);

}

static int get32(float *data, int npts, FILE *fp)
{

        register int i;
        register int32 *dptr;
        register float *fptr;
        extern int f_get32(register int *data, int count, FILE *fp), f_debug(int flag);

        if (! f_get32((int32 *)data, npts, fp)) {
                if (f_debug(QUERY)) {
                        fprintf(stderr,
                                "f_getf: Can't get 32 bit data\n");
                }
                return(FALSE);
        }
        dptr = (int32 *) data + npts - 1;
        fptr = data + npts - 1;
        for (i = 0; i < npts; i++) {
                *fptr-- = (float) *dptr--;
        }

        return(TRUE);

}

static int get8(float *data, int npts, FILE *fp)
{

        register int i;
        register int8 *dptr;
        register float *fptr;
        extern int f_get8(register unsigned char *data, int count, FILE *fp), f_debug(int flag);

        if (! f_get8((int8 *)data, npts, fp)) {
                if (f_debug(QUERY)) {
                        fprintf(stderr,
                                "f_getf: Can't get 8 bit data\n");
                }
                return(FALSE);
        }
        dptr = (int8 *) data + npts - 1;
        fptr = data + npts - 1;
        for (i = 0; i < npts; i++) {
                *fptr-- = *dptr--;
        }

        return(TRUE);

}

static int getbp(char **header)
{

        int bitpix;
        extern int f_ikey(int *ival, char **header, char *keyword), f_debug(int flag);

        /* This return was added for dervish, as the dervish code checks the
           bitpix value and strips it from the header before reading in
           the pixels. The rest of the routine was commented out too. */
        int sh_bitpix = 32;

        return(sh_bitpix);



/*        if (! f_ikey(&bitpix, header, "BITPIX")) {
                if (f_debug(QUERY)) {
                        fprintf(stderr,
                                "f_getf: Can't find BITPIX\n");
                }
                return(0);
        }
        return(bitpix);
*/
}

#ifdef DEBUG    /* passed this test for 16 and 32-bits, August 7, 1986 */

#define HEADERSIZE (36 * 2)

main(argc, argv)
int argc;
char *argv[];

{

        int i;
        FILE *fp, *f_rdfits();
        char *header[HEADERSIZE];
        float fdata[100];

        if (argc != 2) {
                printf("usage: f_getf filename\n");
                exit(1);
        }

        f_setup();
        f_debug(TRUE);

        fp = f_rdfits(argv[1], header, HEADERSIZE);
        if (fp == NULL) {
                printf("can't open '%s'\n", argv[1]);
                exit(1);
        }
        if (! f_getf(fdata, 100, header, fp)) {
                printf("error getting data\n");
                exit(1);
        }
        else {
                for (i = 1; i <= 100; i++) {
                        printf((i % 4) ? "%18.9E" : "%18.9E\n", fdata[i-1]);
                }
        }
}

#endif
