/*                                                       libfits.h
------------------------------------------------------------------

Synopsis:       #include "libfits.h"

Purpose:        header file for libfits

Notes:          There are three things to select before intalling
                the library: Computer, Compiler, and Operating
                System.  Set the appropriate macro in the
                selections area below. 

History:        Alan Uomoto, LSU Physics & Astronomy

------------------------------------------------------------------

*/

#ifndef LIBFITS         /* prevent multiple inclusion   */

/* -------------------- BEGIN SELECTIONS ---------------------- */

#ifndef vms
#define BSD42	1	/* 0 for SYSTEM V */
#endif

/* --------------------- END SELECTIONS ----------------------- */

#ifndef vms
#include <stdio.h>
#include <ctype.h>

#if BSD42
#include <sys/time.h>
/*### #define strchr  index  ###*/
/*### #define strrchr rindex ###*/
#else
#include <time.h>
#endif
#ifdef _POSIX_SOURCE
#include <time.h>
#endif

#endif

void load_libfits(int one);

#if defined(vms)
#include stdio
#include ctype
#include time
#endif

#define int8    unsigned char
#define int16   short

/*  Change this from long to int.  Under OSF1, longs are 64 bits. Under IRIX
    and SunOS, long is the same as int so nothing should break */
#define int32   int

#ifndef TRUE
#define TRUE            1
#endif
#ifndef FALSE
#define FALSE           0
#endif

#define QUERY           -1      /* for f_debug queries                  */

#define RECORDSIZE      2880    /* number of bytes in a FITS record     */
#define MAXFILES        20      /* number of open FITS files allowed    */
#define READ            0       /* read and write mode for files        */
#define WRITE           1

struct f_finfo {
        int     bitpix[MAXFILES],
                mode[MAXFILES],         /* read or write ?      */
                nused[MAXFILES];        /* bytes used in bp     */
        FILE    *streams[MAXFILES];
        int8    *bp[MAXFILES];
};

/*
 * Type of FITS header data units (HDUs).
 *
 *   o	f_hduUnknown   M U S T   B E   zero (0).
 *   o	f_hduCnt       M U S T   B E   last (count of HDU types).
 *
 * NOTE:  Code/data is dependent upon the order of these header types.
 *        If the order is changed, make sure that those changes are
 *        reflected in code also (such as f_hinfo.c).
 */

typedef enum f_hdutype
   {
   f_hduUnknown = 0,			/* Unknown HDU               */
   f_hduPrimary,			/* Primary                   */
   f_hduGroups,				/* Random Groups             */
   f_hduTABLE,				/* ASCII Table extension     */
   f_hduBINTABLE,			/* Binary Table extension    */
   f_hduIMAGE,				/* IMAGE extension    */
   f_hduCnt				/* M U S T   B E   L A S T   */
   } FITSHDUTYPE;

/* the only instance of the above structure is in f_global.c */

#define LIBFITS 1


#ifdef __cplusplus
extern "C"
{
#endif  /* ifdef cpluplus */

extern struct f_finfo f_files;
extern int f_BYTESWAP, f_WORDSWAP;

int f_p_flush(int idx, FILE *fp);	/* Private */

int  f_akey(char *aval, char *header[], char *keyword);
int  f_debug(int flag);
int  f_dkey(double *fval, char *header[], char *keyword);
int  f_fclose(FILE *fp);
int  f_findex(FILE *fp);
int  f_lose(FILE *fp);
int  f_get16(int16 *data, int count, FILE *fp);
int  f_get32(int32 *data, int count, FILE *fp);
int  f_get8(int8 *data, int count, FILE *fp);
int  f_getd(double *data, int npts, char *header[], FILE *fp);
int  f_getf(float *data, int npts, char *header[], FILE *fp);
int  f_getlin(char *line, char *header[], char *keyword);
int  f_hdel(char *header[]);
int  f_hinfo(char *header[], unsigned int hdupos,
             enum f_hdutype *hdutype,
             unsigned long int *datasize, unsigned long int *rsasize,
             unsigned long int *heapoff,  unsigned long int *heapsize);
int  f_hldel(char *header[], int linenno);
int  f_hlins(char *header[], int size, int linenno, char *line);
int  f_hlrep(char *header[], int linenno, char *line);
int  f_ikey(int *ival, char *header[], char *keyword);
int  f_kdel(char *header[], char *keyword);
int  f_lkey(int *lval, char *header[], char *keyword);
int  f_lnum(char *header[], char *keyword);
int  f_npts(char *header[]);
int  f_put16(int16 *data, int count, FILE *fp);
int  f_put32(int32 *data, int count, FILE *fp);
int  f_put8(int8 *data, int count, FILE *fp);
int  f_rdhead(char *header[], int size, FILE *fp);
int  f_rdheadi(char *header[], int size, FILE *fp);
int  f_rdtbhd(char *header[], int size, FILE *fp);
int  f_rfr(char *bp, FILE *fp);
int  f_save(FILE *fp, char *header[], char *mode);
int  f_smatch(char *long_str, char *short_str);
int  f_wfr(char *buf, FILE *fp);
int  f_wrhead(char *header[], FILE *fp);
int  f_newhdr(char *header[], int nlines, int simple, int bitpix, int naxis,
              int naxisn[], char *comments);

FILE *f_fopen(char *name, char *mode);
FILE *f_rdfits(char *name, char *header[], int size);
FILE *f_wrfits(char *name, char *header[]);

void f_bswap2(register int16 *bp);
void f_bswap4(register int32 *bp);
void f_movmem(unsigned char *dest, unsigned char *source, int nbytes);
void f_init(void);

char *f_date(char *datestr); 
char *f_mkakey(char *string, char *key, char *value, char *comment);
char *f_mkdkey(char *string, char *key, double value, char *comment);
char *f_mkfkey(char *string, char *key, float value, char *comment);
char *f_mkikey(char *string, char *key, int value, char *comment);
char *f_mklkey(char *string, char *key, int value, char *comment);
void  f_setup(void);
#ifdef __cplusplus
}
#endif  /* ifdef cpluplus */


#endif  /* LIBFITS */
