/*                                                          f_save
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_save(fp, header, mode)
                FILE *fp;
                char *header[], *mode;

Purpose:        Assigns file index, gets buffer memory

Inputs:         'fp' is the file stream to save
                'header' is the FITS header
                'mode' is the read/write mode

Returns:        TRUE on success,
                FALSE on failure

Notes:          Saves the file pointer (for identification) and
                sets the value of f_file.nused.

History:        1986 July 7, Alan Uomoto, LSU Physics & Astronomy

------------------------------------------------------------------

*/

#include "libfits.h"

int f_save(FILE *fp, char **header, char *mode)
{

        int     i;                      /* file index                   */
        extern int
                f_ikey(int *ival, char **header, char *keyword),               /* gets integer keywords        */
                f_debug(int flag);              /* error messages on or off     */
        extern struct f_finfo f_files;  /* where everything is          */
        extern char *shMalloc();          /* standard library routine     */

        for (i = 0; i < MAXFILES; i++) {
                if (f_files.streams[i] == NULL) {
                        break;
                }
        }
        if (i == MAXFILES) {
                if (f_debug(QUERY)) {
                        fprintf(stderr, "f_save: Too many files open.");
                        fprintf(stderr,
                                "        Maximum allowed = %d\n", i = MAXFILES);
                }
                return(FALSE);
        }

        f_files.streams[i] = fp;
        if (mode[0] == 'r') {
                f_files.mode[i] = READ;
                f_files.nused[i] = 2880;
        }
        else if (mode[0] == 'w') {
                f_files.mode[i] = WRITE;
                f_files.nused[i] = 0;
        }
        else {
                if (f_debug(QUERY)) {
                        fprintf(stderr,
                                "f_save: Passed a bad mode: '%s'\n", mode);
                }
                return(FALSE);
        }

        if (! f_ikey(&f_files.bitpix[i], header, "BITPIX")) {
                if (f_debug(QUERY)) {
                        fprintf(stderr, "f_save: Can't find BITPIX\n");
                }
                return(FALSE);
        }

        f_files.bp[i] = (int8 *) shMalloc(2880);

        return(TRUE);

}

int f_lose(FILE *fp)
{

        int     i;                      /* file index                   */
        extern int f_debug(int flag);	/* error messages on or off     */
        extern struct f_finfo f_files;  /* where everything is          */
        extern char *shMalloc();	/* standard library routine     */

        for (i = 0; i < MAXFILES; i++) {
                if (f_files.streams[i] == fp) {
                        break;
                }
        }
        if (i == MAXFILES) {
                if (f_debug(QUERY)) {
                        fprintf(stderr, "f_lose: cannot find 0x%p", fp);
                }
                return(FALSE);
        }

	if(f_p_flush(i, fp) != TRUE) {
	   if (f_debug(QUERY)) {
	      printf("f_fclose: Error flushing buffers\n");
	   }
	   return(FALSE);
	}

	f_files.bitpix[i] = 0;
	f_files.streams[i] = NULL;
	shFree((char *) f_files.bp[i]);
	f_files.bp[i] = NULL;

        return(TRUE);

}
