#include "libfits.h"
struct f_finfo f_files;
int f_BYTESWAP, f_WORDSWAP;

/*
 * This routine is called by f_fopen() to encourage the loader to
 * see these symbols; needed under e.g. Mac OS/X
 */
void
load_libfits(int one) {
  if(one != 1) {
    printf("Impossible error in load_libfits()\n");
  }
}		/* force ld to load this file */
