#******************************************************************************#
#                                                                              #
# This LIBFITS Makefile must be invoked from ${DERVISH_DIR}/bin/sdssmake rather  #
# than from make directly.                                                     #
#                                                                              #
# This code is not ANSI C (it's K&R), so only SDSS_CFLAGS should be used. The  #
# other sdssmake macros contain definitions and flags that cause strict ANSI C #
# checking to occur.                                                           #
#                                                                              #
#******************************************************************************#

.c.o :
	$(CC) -c $(SDSS_CFLAGS) $*.c

LIB=../lib
OBJ1 = f_akey.o f_bswap2.o f_bswap4.o f_date.o f_debug.o
OBJ2 = f_fkey.o f_dkey.o f_fclose.o f_findex.o f_fopen.o f_get16.o
OBJ3 = f_get32.o f_get8.o f_getd.o f_getf.o f_getlin.o
OBJ4 = f_global.o f_hldel.o f_hlins.o f_hlrep.o f_ikey.o
OBJ5 = f_kdel.o f_lkey.o f_lnum.o f_mkakey.o f_mkdkey.o
OBJ6 = f_mkfkey.o f_mkikey.o f_mklkey.o f_movmem.o f_newhdr.o f_npts.o
OBJ7 = f_put16.o f_put32.o f_put8.o f_rdfits.o f_rdhead.o f_rdtbhd.o
OBJ8 = f_rfr.o f_save.o f_setup.o f_smatch.o f_wfr.o
OBJ9 = f_wrfits.o f_wrhead.o f_hdel.o f_hinfo.o f_rdheadi.o

default:
	@ echo '*** Invoke this Makefile using ${DERVISH_DIR}/bin/sdssmake ***' >&2

all: ../lib/libfits.a

../lib/libfits.a : $(OBJ9) $(OBJ8) $(OBJ7) $(OBJ6) $(OBJ5) $(OBJ4) $(OBJ3) $(OBJ2) $(OBJ1)
	$(AR) $(ARFLAGS) $(LIB)/libfits.a $?
	- if [ "$(RANLIB)" != "" -a -x "$(RANLIB)" ]; then \
		$(RANLIB) $(LIB)/libfits.a; \
	fi

f_akey.o : f_akey.c libfits.h

f_bswap2.o : f_bswap2.c libfits.h

f_bswap4.o : f_bswap4.c libfits.h

f_date.o : f_date.c libfits.h

f_debug.o : f_debug.c libfits.h

f_fkey.o : f_fkey.c libfits.h

f_dkey.o : f_dkey.c libfits.h

f_fclose.o : f_fclose.c libfits.h

f_findex.o : f_findex.c libfits.h

f_fopen.o : f_fopen.c libfits.h

f_get16.o : f_get16.c libfits.h

f_get32.o : f_get32.c libfits.h

f_get8.o : f_get8.c libfits.h

f_getd.o : f_getd.c libfits.h

f_getf.o : f_getf.c libfits.h

f_getlin.o : f_getlin.c libfits.h

f_global.o : f_global.c libfits.h

f_hdel.o : f_hdel.c libfits.h

f_hinfo.o : f_hinfo.c libfits.h

f_hldel.o : f_hldel.c libfits.h

f_hlins.o : f_hlins.c libfits.h

f_hlrep.o : f_hlrep.c libfits.h

f_ikey.o : f_ikey.c libfits.h

f_kdel.o : f_kdel.c libfits.h

f_lkey.o : f_lkey.c libfits.h

f_lnum.o : f_lnum.c libfits.h

f_mkakey.o : f_mkakey.c libfits.h

f_mkdkey.o : f_mkdkey.c libfits.h

f_mkfkey.o : f_mkfkey.c libfits.h

f_mkikey.o : f_mkikey.c libfits.h

f_mklkey.o : f_mklkey.c libfits.h

f_movmem.o : f_movmem.c libfits.h

f_newhdr.o : f_newhdr.c libfits.h

f_npts.o : f_npts.c libfits.h

f_put16.o : f_put16.c libfits.h

f_put32.o : f_put32.c libfits.h

f_put8.o : f_put8.c libfits.h

f_rdfits.o : f_rdfits.c libfits.h

f_rdhead.o : f_rdhead.c libfits.h

f_rdheadi.o : f_rdheadi.c libfits.h

f_rdtbhd.o : f_rdtbhd.c libfits.h

f_rfr.o : f_rfr.c libfits.h

f_save.o : f_save.c libfits.h

f_setup.o : f_setup.c libfits.h

f_smatch.o : f_smatch.c libfits.h

f_wfr.o : f_wfr.c libfits.h

f_wrfits.o : f_wrfits.c libfits.h

f_wrhead.o : f_wrhead.c libfits.h

clean :
	rm -f core *~ *.bak *.jou MAIN.0 MAIN.0.* *.orig *.old .'#'* '#'*'#'
	rm -f *.o

install :
	@ rm -rf $(LIBFITS_INSTALL_DIR)/src
	@ mkdir  $(LIBFITS_INSTALL_DIR)/src 
	@ cp -p *.c *.h Makefile README $(LIBFITS_INSTALL_DIR)/src
