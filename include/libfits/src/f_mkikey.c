/*                                                        f_mkikey
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                char *f_mkikey(string, key, value, comment)
                char *string, *key, *comment;
                int value;

Purpose:        Builds FITS header line with integer keyword

Inputs:         'string' - place to put the new line
                'key'    - the keyword
                'value'  - the value of the keyword
                'comment'- the header line comment (may be null)

Returns:        pointer to string

History:        1986 Dec 9, Alan Uomoto, JHU Physics & Astronomy

------------------------------------------------------------------

*/

#include <string.h>
#include "libfits.h"

char	*f_mkikey(char *ostring, char *key, int value, char *comment)
{
	int		i;		/* loop index                   */
	register int	oo;		/* output index			*/


    if (strlen(key) > 8)
    {   if (f_debug(QUERY))
	{   fprintf( stderr, "f_mkikey: keyword too long [%s]\n", key );
	}
	key[8] = '\0';
    }

    (void)sprintf( ostring, "%-8s=           %10d", key, value );

    oo = 30;
    if (comment[0] != 0)
    {   ostring[oo++] = ' ';
	ostring[oo++] = '/';
	ostring[oo++] = ' ';

	for (i=0; (i<37) && (comment[i]!='\0'); )	/* limit copy */
	{   ostring[oo++] = comment[i++];	/* copy comment */
	}
    }

    while (oo<80) ostring[oo++] = ' ';
    ostring[oo++] = '\0';

    return(ostring);
}

#ifdef DEBUG

main(argc, argv)
int argc;
char *argv[];

{

        char str[82], *f_mkikey();

        if (argc != 4) {
                printf("usage: f_mkikey key value comment\n");
                exit(1);
        }

        printf("%s\n", f_mkikey(str, argv[1], atoi(argv[2]), argv[3]));

}

#endif
