/*                                                        f_fclose
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                f_fclose(fp)
                FILE *fp;

Purpose:        Closes a fits data file.

Inputs:         'fp' - A file pointer

Returns:        TRUE on success,
                FALSE on failure

History:        1986 July 19, Alan Uomoto, LSU Physics & Astronomy
                1986 November 11, Added f_BYTESWAP, f_WORDSWAP

------------------------------------------------------------------

*/

#include "libfits.h"

int f_fclose(FILE *fp)
{

        register int i;         /* loop index                           */
        int     idx;            /* place to put file index              */
        extern int
                f_findex(FILE *fp),     /* file index finder                    */
                f_debug(int flag),      /* error messages on or off             */
                fclose(FILE *);       /* standard i/o library routine         */
        extern void
                f_bswap2(register short int *in),     /* byte swap routine                    */
                f_bswap4(register int *buffer);     /* word reverser                        */
        extern struct f_finfo f_files;
        extern int f_BYTESWAP, f_WORDSWAP;

        if ((idx = f_findex(fp)) < 0) {
                if (f_debug(QUERY)) {
                        printf("f_fclose: Can't find index for pointer\n");
                        printf("          File not opened with f_ fopen?\n");
                }
                return(FALSE);
        }

	if(f_p_flush(idx, fp) != TRUE) {
	   if (f_debug(QUERY)) {
	      printf("f_fclose: Error flushing buffers\n");
	   }
	   return(FALSE);
	}

        if (fclose(fp) == 0) {
                f_files.bitpix[idx] = 0;
                f_files.streams[idx] = NULL;
                shFree((char *) f_files.bp[idx]);
                f_files.bp[idx] = NULL;
                return(TRUE);
        }
        else {
                return(FALSE);
        }
}

/*
 * flush the write buffers if required
 */
int
f_p_flush(int idx,
	  FILE *fp)
{
   int i;
   
   if (f_files.mode[idx] != WRITE || f_files.nused[idx] == 0) {
      return(TRUE);
   }
   
   if (f_files.bitpix[idx] == 16 && f_BYTESWAP) {
      f_bswap2((int16 *) f_files.bp[idx]);
   } else if (f_files.bitpix[idx] == 32 && f_BYTESWAP && f_WORDSWAP) {
      f_bswap4((int32 *) f_files.bp[idx]);
   }
   
   for (i = f_files.nused[idx]; i < 2880; i++) {
      f_files.bp[idx][i] = 0;
   }
   
   if (! f_wfr((char *)f_files.bp[idx], fp)) {
      if (f_debug(QUERY)) {
	 printf("f_fclose: Error writing last buffer\n");
      }
      return(FALSE);
   }
   f_files.nused[idx] = 0;

   return(TRUE);
}

#ifdef DEBUG

#define HEADERSIZE (36 * 2)

main(argc, argv)        /* passed this limited test 1986 August 17      */
int argc;
char *argv[];

{

        FILE *fp, *f_rdfits(), *f_wrfits();
        int npts, bitpix;
        char *header[HEADERSIZE], *shMalloc();
        int16 *data;

        if (argc != 3) {
                printf("usage: f_fclose input output\n");
                exit(1);
        }

        f_setup();
        f_debug(TRUE);
        fp = f_rdfits(argv[1], header, HEADERSIZE);
        if (fp == NULL) {
                printf("Can't get input file\n");
                exit(1);
        }
        if (! f_ikey(&bitpix, header, "BITPIX")) {
                printf("Can't find BITPIX\n");
                exit(1);
        }
        if (bitpix != 16) {
                printf("This test only works with BITPIX = 16\n");
                exit(1);
        }
        npts = f_npts(header);
        data = (int16 *) shMalloc(npts * sizeof(float));
        if (! f_get16(data, npts, fp)) {
                printf("Can't read data\n");
                exit(1);
        }
        if (! f_fclose(fp)) {
                printf("Can't close input file\n");
                exit(1);
        }
        fp = f_wrfits(argv[2], header);
        if (fp == NULL) {
                printf("Can't open output file\n");
                exit(1);
        }
        if (! f_put16(data, npts, fp)) {
                printf("Error writing data\n");
                exit(1);
        }
        if (! f_fclose(fp)) {
                printf("Can't close output file\n");
                exit(1);
        }
}

#endif
