/*                                                        f_getlin
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_getlin(line, header, keyword)
                char line[81], *header[], *keyword;

Purpose:        Extracts the line from the header with keyword

Inputs:         'header' is the file header
                'keyword' is the FITS keyword requested

Returns:        TRUE on success ('line' holding the header line)
                FALSE on failure

Notes:          'line' must be at least 81 bytes long.

History:        1986 July 6, Alan Uomoto, LSU Physics & Astronomy

------------------------------------------------------------------

*/

#include "libfits.h"

int f_getlin(char *line, char **header, char *keyword)
{

        int     linenum;        /* line number                          */
        extern int f_lnum(char **header, char *keyword);    /* finds line number                    */
        extern char *strcpy(char *, const char *);  /* standard library routine             */

        if ((linenum = f_lnum(header, keyword)) < 0) {
                line[0] = '\0';
                return(FALSE);
        }
        (void) strcpy(line, header[linenum]);
        return(TRUE);

}

#ifdef DEBUG

#define HEADERSIZE (36 * 2)

main(argc, argv)
int argc;
char *argv[];

{

        char keyword[80], line[81], *header[HEADERSIZE];
        int i;
        FILE *fp, *f_rdfits();

        if (argc != 2) {
                printf("usage: f_getlin filename\n");
                exit(1);
        }

        f_setup();
        f_debug(TRUE);

        fp = f_rdfits(argv[1], header, HEADERSIZE);
        if (fp == NULL) {
                printf("f_getlin: can't open input\n");
                exit(1);
        }

        for (i = 0; header[i] != NULL; i++) {
                printf("%s\n", header[i]);
        }

        for (;;) {
                printf("keyword: ");
                if (scanf("%s", keyword) != 1) {
                        exit(1);
                }
                if (f_getlin(line, header, keyword)) {
                        printf("%s\n", line);
                }
                else {
                        printf("No keyword\n");
                }
        }
}

#endif
