/*                                                        f_movmem

------------------------------------------------------------------

Synopsis: void f_movmem(dest, source, nbytes)
          unsigned char *dest, *source;
          int nbytes;

Purpose:  moves nbytes from source to dest

Returns:  nothing

Notes:    This exists for compatibility with the Lattice C
          function of the same name. The Lattice function uses a
          block memory move for speed on a PC, so it's useful
          there.

History:  1985 September 15, Alan Uomoto, LSU Physics & Astronomy
          1986 July 11, renamed it f_movmem and added to LIBFITS
	  1988 July 4, added memcpy() for VAXC - au

------------------------------------------------------------------

*/

#include "libfits.h"

void f_movmem(unsigned char *dest, unsigned char *source, register int nbytes)
{

#ifdef LATTICE

        movmem((char *)dest, (char *)source);
	return;

#endif

#ifdef vms

	void *memcpy();
	(void) memcpy((char *)dest, (char *)source, (size_t) nbytes);

#else
        register int i;

        for (i = 0; i < nbytes; i++)
                *dest++ = *source++;

#endif

}

#ifdef DEBUG

main()
{
        struct {
                double a, b;
        } item1, item2;

        item1.a = 12.0;
        item1.b = 23.1;
        f_movmem((int8 *) &item2, (int8 *) &item1, sizeof(item1));
        printf("%lf %lf", item2.a, item2.b);
}

#endif  /* DEBUG */
