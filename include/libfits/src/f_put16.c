/*                                                         f_put16
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_put16(data, count, fp)
                int16 *data;
                int count;
                FILE *fp;

Purpose:        Writes 16 bit data to fp

Inputs:         'data' is a pointer to at least count 16 bit words
                'count' is the number of 16 bit words to get
                'fp' is the file stream pointer

Returns:        TRUE on success
                FALSE on failure

Example:        #include "libfits.h"
                int16 data[2000];
                makedata(data);
                if (! f_put16(data, 2000, fp)) {
                        printf("can't write data");
                }

History:        1986 July 16, Alan Uomoto, LSU Physics & Astronomy

------------------------------------------------------------------

*/

#include "libfits.h"

#define POINTS_PER_REC 1440

static int wrtbuf(short int *dp, FILE *fp)
{

        extern int f_debug(int flag), f_wfr(char *buf, FILE *fp);
        extern void f_bswap2(register short int *in);
        extern int f_BYTESWAP;

        if (f_BYTESWAP) {
                f_bswap2(dp);
        }
        if (! f_wfr((char *)dp, fp)) {
                if (f_debug(QUERY)) {
                        fprintf(stderr, "f_put16: error writing file\n");
                }
                return(FALSE);
        }
        return(TRUE);

}

int f_put16(register short int *data, int count, FILE *fp)
{

        register int16  *dp;    /* temporary data pointer               */
        register int    i,      /* loop index                           */
                        n;      /* temporary variable                   */
        int16   *buf;           /* temporary data pointer               */
        int     j,              /* loop index                           */
                findex,         /* file index number                    */
                nleft,          /* number of 16 bit words left in record*/
                nrec,           /* number of new records to read        */
                wrtbuf();       /* writes a FITS record                 */
        extern int
                f_findex(FILE *fp);     /* gets file index from stream pointer  */
        extern struct f_finfo f_files;  /* global file info             */

        findex = f_findex(fp);
        buf = (int16 *) f_files.bp[findex];

        nleft = (2880 - f_files.nused[findex]) / sizeof(int16);
        if (nleft > 0) {
                dp = buf + f_files.nused[findex] / sizeof(int16);
                n = (count < nleft) ? count : nleft;
                for (i = 0; i < n; i++) {
                        *dp++ = *data++;
                }
                f_files.nused[findex] += n * sizeof(int16);
                count -= n;
        }
        if (f_files.nused[findex] < 2880) {
                return(TRUE);
        }
        else {
                if (! wrtbuf(buf, fp)) {
                        return(FALSE);
                }
                f_files.nused[findex] = 0;
        }
        nrec = count / POINTS_PER_REC;
        for (j = 0; j < nrec; j++) {
                dp = buf;
                for (i = 0; i < POINTS_PER_REC; i++) {
                        *dp++ = *data++;
                }
                if (! wrtbuf(buf, fp)) {
                        return(FALSE);
                }
                count -= POINTS_PER_REC;
        }
        if (count > 0) {
                dp = buf;
                for (i = 0; i < count; i++) {
                        *dp++ = *data++;
                }
                f_files.nused[findex] = count * sizeof(int16);
        }
        return(TRUE);
}

#ifdef DEBUG

#define HEADERLINES 73

main(argc, argv)
int argc;
char *argv[];

{

        long t0, t1;
        register int i;
        int bitpix, naxis1, naxis2;
        int16 *data;
        char *header[HEADERLINES];
        FILE *fp, *f_rdfits(), *f_wrfits();

        if (argc != 3) {
                printf("usage: f_put16 input output\n");
                exit(1);
        }

        f_setup();
        f_debug(TRUE);

        if ((fp = f_rdfits(argv[1], header, HEADERLINES)) == NULL) {
                printf("Can't open %s\n", argv[1]);
                exit(1);
        }
        if (! f_ikey(&naxis1, header, "NAXIS1")) {
                printf("Can't get naxis1\n");
                exit(1);
        }
        if (! f_ikey(&naxis2, header, "NAXIS2")) {
                printf("Can't get naxis2\n");
                exit(1);
        }
        if (! f_ikey(&bitpix, header, "BITPIX")) {
                printf("Can't get bitpix\n");
                exit(1);
        }
        if (bitpix != 16) {
                printf("Bitpix must be 16 (it's %d)\n", bitpix);
                exit(1);
        }
        data = (int16 *) shMalloc(naxis1 * naxis2 * 2);

        printf("reading data...");
        fflush(stdout);

        time(&t0);
        if (! f_get16(data, naxis1 * naxis2, fp)) {
                printf("Error reading data\n");
                fflush(stdout);
        }
        time(&t1);

        printf("time: %ld seconds\n", t1 - t0);

        f_fclose(fp);

        fp = f_wrfits(argv[2], header);
        if (fp == NULL) {
                printf("Can't open %s\n",argv[2]);
                exit(1);
        }

        printf("writing data...");
        fflush(stdout);

        time(&t0);
        if (! f_put16(data, naxis1 * naxis2, fp)) {
                printf("Error writing data\n");
                exit(1);
        }
        f_fclose(fp);
        time(&t1);

        printf("time: %ld seconds\n", t1 - t0);
}

#endif /* DEBUG */
