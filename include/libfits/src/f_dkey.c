/*                                                          f_dkey
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_dkey(fval, header, keyword)
                double *fval;
                char *header[], keyword;

Purpose:        Get the value of a double (precision) keyword.

Inputs:         'header' is the FITS file header
                'keyword' is the FITS keyword to search for

Returns:        TRUE and the value in fval on success,
                FALSE on failure

Example:        double ra;
                if (f_dkey(&ra, header, "RA")) {
                        printf("RA = %lf", ra);
                }

Notes:          The header array must have been filled with the
                header before calling this routine (usually with
                f_rdfits).

History:        1986 July 6, Alan Uomoto, LSU Physics & Astronomy

------------------------------------------------------------------

*/

#include <string.h>
#include "libfits.h"
static void emess(char *temp, char *keyword)
{

        int f_debug(int flag);          /* checks for error message flag        */

        if (f_debug(-1)) {
                printf("f_dkey: '%s' is a bad floating point value for '%s'\n",
                        temp, keyword);
        }
}

static int rstring(char *string)      /* validates double strings */
             

{

        char    *values = "0123456789.+-eE";
        int     i,
                len;

        len = strlen(string);

        for (i = 0; i < len; i++) {
                if (string[i] == 'D')
                        string[i] = 'E';
                if (strchr(values, string[i]) == NULL) {
                        return(FALSE);
                }
        }
        return(TRUE);
}


f_dkey(double *fval, char **header, char *keyword)
{

        char    line[81],       /* put the header line here             */
                temp[81];       /* put the float string here            */
        int     i, j,           /* loop indices                         */
                rstring();      /* checks if float string is valid      */
        void    emess();        /* prints error messages                */
        extern int  f_getlin(char *line, char **header, char *keyword); /* gets the header line                 */
        extern double  atof();  /* alpha to double conversion           */

        if (! f_getlin(line, header, keyword)) {
                return(FALSE);
        }

        for (i = 9; i < 80; i++) {      /* clear leading blanks         */
                if (line[i] != ' ')
                        break;
        }
                                        /* copy the double string       */
        for (j = 0; line[i] != ' ' && line[i] != '/' && i < 80; j++) {
                temp[j] = line[i++];
        }
        temp[j] = '\0';

        if (! rstring(temp)) {          /* validate double string       */
                emess(temp, keyword);
                return(FALSE);
        }

        *fval = atof(temp);
        return(TRUE);

}


#ifdef DEBUG

#define HEADERSIZE (36 * 2)

main(argc, argv)        /* passed this test 1986 August 16 */
int argc;
char *argv[];

{

        char *header[HEADERSIZE], keyword[80];
        int i;
        double rval;
        FILE *fp, *f_rdfits();

        if (argc != 2) {
                printf("usage: f_dkey filename\n");
                exit(1);
        }

        f_setup();
        f_debug(TRUE);

        fp = f_rdfits(argv[1], header, HEADERSIZE);
        if (fp == NULL) {
                printf("Can't open %s\n", argv[1]);
                exit(1);
        }
        for (i = 0; header[i] != NULL; i++) {
                printf("%s\n", header[i]);
        }
        for (;;) {
                printf("keyword: ");
                if (scanf("%s", keyword) != 1) {
                        exit(1);
                }
                if (f_dkey(&rval, header, keyword)) {
                        printf("%le\n", rval);
                }
        }
}
#endif
