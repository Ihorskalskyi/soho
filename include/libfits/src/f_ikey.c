/*                                                          f_ikey
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_ikey(ival, header, keyword);
                int *ival;
                char *header[], *keyword;

Purpose:        Get an integer keyword value

Inputs:         'ival' - pointer to an integer
                'header' is the file header
                'keyword' is the header keyword to look for

Returns:        TRUE on success, with the integer in ival
                FALSE on failure

Example:        int ival;
                if (f_ikey(&ival, header, "BITPIX")) {
                        printf("bitpix = %d", ival);
                }
                else {
                        printf("BITPIX not in header\n");
                }

Notes:          The header array must have been filled prior to
                using this function, usually with f_rdfits.

History:        1986 July 6, Alan Uomoto, LSU Physics & Astronomy

------------------------------------------------------------------

*/

#include <string.h>
#include "libfits.h"

static void emess(char *temp, char *keyword)
{

        int f_debug(int flag);          /* checks for error message flag        */

        if (f_debug(QUERY)) {
                fprintf(stderr,
                        "f_ikey: '%s' is not an integer value for '%s'\n",
                        temp, keyword);
        }
}


int f_ikey(int *ival, char **header, char *keyword)
{

        char    line[81],       /* place to keep the header line        */
                temp[81];       /* place to put the integer string      */
        int     i, j;           /* loop indices                         */
        extern int
                f_getlin(char *line, char **header, char *keyword);     /* extracts a line from the header      */
        void    emess();        /* prints error messages                */

        if (! f_getlin(line, header, keyword)) {
                return(FALSE);
        }

        for (i = 9; i < 80; i++) {      /* clear leading blanks         */
                if (line[i] != ' ')
                        break;
        }
        if (i == 80) {
                emess("", keyword);
                return(FALSE);
        }

                                        /* copy the token               */
        for (j = 0; line[i] != ' ' && line[i] != '/' && i < 80; j++) {
                temp[j] = line[i++];
        }
        temp[j] = '\0';

                                        /* check if valid integer       */
        if ((! isdigit(temp[0])) && temp[0] != '+' && temp[0] != '-') {
                emess(temp, keyword);
                return(FALSE);
        }
        j = strlen(temp);
        for (i = 1; i < j; i++) {
                if (! isdigit(temp[i])) {
                        emess(temp, keyword);
                        return(FALSE);
                }
        }

        *ival = atoi(temp);
        return(TRUE);

}


#ifdef DEBUG

#define HEADERSIZE 145

main(argc, argv)        /* passed this test 1986 August 16 */
int argc;
char *argv[];

{

        char *header[HEADERSIZE], keyword[80];
        int i, ival;
        FILE *fp, *f_rdfits();

        if (argc != 2) {
                printf("usage: f_ikey filename\n");
                exit();
        }

        f_setup();
        f_debug(TRUE);

        fp = f_rdfits(argv[1], header, HEADERSIZE);
        if (fp == NULL) {
                printf("Can't open %s\n", argv[1]);
                exit();
        }
        for (i = 0; header[i] != NULL; i++) {
                printf("%s\n", header[i]);
        }
        for (;;) {
                printf("keyword: ");
                if (scanf("%s", keyword) != 1) {
                        exit();
                }
                if (f_ikey(&ival, header, keyword)) {
                        printf("%d\n", ival);
                }
        }
}
#endif











