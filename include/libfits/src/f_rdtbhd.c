/*                                                        f_rdtbhd
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_rdtbhd(header, size, fp)
                char *header[];
                int size;
                FILE *fp;

Purpose:        Reads the header assuming a Standard ASCCI Table Extension

Inputs:         'fp' is the file pointer to an open file
                size is the dimension of header

Returns:        TRUE on success, with 'header' filled with the
                header

                FALSE on failure

Notes:          1. does not rewind fp first.
                2. Allocates memory for header (82 bytes per line,
                   terminated with '\0' at position 80) and sets the
                   next pointer to NULL. If the pointer is not NULL
                   to start with, it frees the memory.
                3. Leaves the file at the top of data position.
                4. The mechanism for searching through the header
                   requires that we know what the next header line
                   at which to start a search is. This line is
                   marked with a '.'.

History:        1986 July 6, Alan Uomoto, LSU Physics & Astronomy
                1986 Aug 21, Removed rewind first, aku
		1992 Oct 9,  modified f_rdhead to make f_rdtbhd, jms

------------------------------------------------------------------

*/

#include "libfits.h"


#define ERROR	0
#define END	-1
#define NOTEND	1


int f_rdtbhd(char **header, int size, FILE *fp)
{

        FILE *f_rdfits(char *name, char **header, int size);		/* use existing f_rdfits */
	int last_record;		/* place in primary record of END */
	int lval;			/* temporary variable */
	char buf[2880];			/* temporary buffer */
	int line;			/* temporary variable */
	int n;				/* temporary variable */

        if (! f_rdhead(header, size, fp)) {
		if (f_debug(QUERY)) {
                	fprintf(stderr,"f_rdtbhd: Not a FITS file\n");
		}
                return(FALSE);
        }
	/* find last record */
        for (n = 0; header[n] != NULL; n++) {
        }
	last_record = n;

	if( f_lkey(&lval, header, "EXTEND") == 0){
		if (f_debug(QUERY)) {
			fprintf(stderr,"f_rdtbhd: no EXTEND card\n");
		}
		return(FALSE);
	}

	if( lval != 1){
		if (f_debug(QUERY)) {
			fprintf(stderr,"f_rdtbhd: EXTEND card not T\n");
		}
		return(FALSE);
	}

	/* now get the extended header, and check it */
	if(! f_rfr(buf,fp)) {
		if (f_debug(QUERY)) {
			fprintf(stderr,"f_rdtbhd: Extended Header not present\n");
		}
		return(FALSE);
	}

	if(! f_smatch(buf, "XTENSION= 'TABLE   '")){
		if (f_debug(QUERY)) {
			fprintf(stderr,"f_rdtbhd: XTENSION TABLE not first key\n");
		}
		return(FALSE);
	}

	if(! f_smatch(buf+80, "BITPIX  =                    8")){
		if (f_debug(QUERY)) {
			fprintf(stderr,"f_rdtbhd: BITPIX not 8\n");
		}
		return(FALSE);
	}

	if(! f_smatch(buf+160,"NAXIS   =                    2")){
		if (f_debug(QUERY)) {
			fprintf(stderr,"f_rdtbhd: NAXIS not 2\n");
		}
		return(FALSE);
	}

	if(! f_smatch(buf+240,"NAXIS1  = ")){
		if (f_debug(QUERY)) {
			fprintf(stderr,"f_rdtbhd: NAXIS1 not present\n");
		}
		return(FALSE);
	}

	if(! f_smatch(buf+320,"NAXIS2  = ")){
		if (f_debug(QUERY)) {
			fprintf(stderr,"f_rdtbhd: NAXIS2 not present\n");
		}
		return(FALSE);
	}

	if(! f_smatch(buf+400,"PCOUNT  =                    0")){
		if (f_debug(QUERY)) {
			fprintf(stderr,"f_rdtbhd: PCOUNT not 0\n");
		}
		return(FALSE);
	}

	if(! f_smatch(buf+480,"GCOUNT  =                    1")){
		if (f_debug(QUERY)) {
			fprintf(stderr,"f_rdtbhd: GCOUNT not 1\n");
		}
		return(FALSE);
	}

	if(! f_smatch(buf+560,"TFIELDS = ")){
		if (f_debug(QUERY)) {
			fprintf(stderr,"f_rdtable: TFIELDS not present\n");
		}
		return(FALSE);
	}

	/* need to loop to see if header is > 1 record */
        line = 0;
        while ((n = f_unpk(&header[last_record], buf, sizeof(buf), line)) != END) {
                if (n == ERROR) {
			if (f_debug(QUERY)) {
				fprintf(stderr, "f_rdtbhd: Error reading file\n");
			}
			return(FALSE);
                }
                if (! f_rfr(buf, fp)) {
                        if (f_debug(QUERY)) {
                                fprintf(stderr, "f_rdtbhd: Unexpected EOF\n");
                        }
                        return(FALSE);
                }
                line += 36;     /* 36 card images per FITS record       */
        }

	/* I do not know whether the missing return value is TRUE or
         * FALSE. Assume it is TRUE. -- Chih-Hao
	 */

	return(TRUE);
}


#ifdef DEBUG	/* passed this test 9 oct 92 */

#define HEADERLINES 145

main(argc, argv)
int argc;
char *argv[];

{

        char *header[HEADERLINES];
        int i;
        FILE *fp,*f_fopen();

        if (argc != 2) {
                printf("usage: f_rdtbhd filename\n");
                exit(1);
        }
        for (i = 0; i < HEADERLINES; i++) {
                header[i] = NULL;
        }

        f_setup();
        f_debug(TRUE);

	if ((fp = f_fopen(argv[1], "r")) == NULL) {
                printf("f_rdtbhd: can't open input\n");
                exit(1);
        }

	if(! f_rdtbhd(header, HEADERLINES, fp)) {
		printf(" f_rdtbhd: Can't get header\n");
		exit(1);
	}

	for (i = 0; header[i] != NULL; i++) {
		printf("%s\n", header[i]);
	}
}

#endif
