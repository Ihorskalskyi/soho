/*                                                         f_debug
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_debug(flag)
                int flag;

Purpose:        Sets the error messages on or off for program
                development.

Inputs:         'flag' = 1 sets error messages on.
                'flag' = 0 sets error messages off.
                'flag' = -1 (QUERY) to test for debug state.

Returns:        The current status of the debug flag (TRUE or
                FALSE) if flag is not 0 or 1, or the requested
                state if flag is 1 or 0.

Example:        f_debug(1);
                if (f_debug(-1)) {
                        printf("debug message here");
                }

Notes:          The default situation is no debug messages. If
                flag is not 0 or 1, the status of the debug flag
                is returned.

History:        1986 July 6, Alan Uomoto, LSU Physics & Astronomy

------------------------------------------------------------------

*/

#include <stdio.h>
#include "libfits.h"

int f_debug(int flag)
{

        static int status = FALSE;
        extern int f_BYTESWAP, f_WORDSWAP;

        if (flag == 1) {
                printf("f_debug: Error messages turned on\n");
                printf("f_debug: BYTESWAP is %s",
                        f_BYTESWAP ? "on, " : "off, ");
                printf("WORDSWAP is %s\n",
                        f_WORDSWAP ? "on" : "off");
                status = TRUE;
        }

        else if (flag == 0) {
                status = FALSE;
        }

        return(status);

}
