/*                                                        f_findex
------------------------------------------------------------------

Synopsis:       #include "libfits.h"
                int f_findex(fp)
                FILE *fp;

Purpose:        Gets the file index associated with fp

Inputs:         'fp' is the file stream pointer to check out

Returns:        The file index on success,
                -1 on failure

History:        1986 July 11, Alan Uomoto, LSU Physics & Astronomy

------------------------------------------------------------------

*/

#include "libfits.h"

int f_findex(FILE *fp)
{

        int     i;              /* loop index           */
        extern int f_debug(int flag);   /* debug flag           */
        extern struct f_finfo
                f_files;        /* global file info     */

        for (i = 0; i < MAXFILES; i++) {
                if (f_files.streams[i] == fp) {
                        return(i);
                }
        }

        if (f_debug(QUERY)) {
                printf("f_findex: Couldn't find file pointer\n");
        }

        return(-1);

}
